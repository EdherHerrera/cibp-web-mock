package com.teknowledge.tekportal.teksecurity;

import java.io.DataInputStream;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.teknowledge.tekportal.tekportalutils.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class TekSecurityClient {

	private TekSecurityClient() {
	}

	public static String encryptString(String s) throws Exception {
		try {
			PublicKey publickey = loadPublicKey("TekPublic.key");
			Cipher cipher = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding", new BouncyCastleProvider());
			cipher.init(1, publickey);
			byte abyte0[] = s.getBytes("UTF8");
			int i = abyte0.length;
			int j = 117;// cipher.getBlockSize(); hardcoded because in java 1.5
						// default block size value is zero
			int k = cipher.getOutputSize(j);
			byte abyte1[] = null;
			int l = 0;
			if (i <= j) {
				abyte1 = cipher.doFinal(abyte0);
				l = abyte1.length;
			} else {
				if (k < j)
					throw new Exception("encryptString(): Buffer Size=" + k + "<" + j + "...");
				abyte1 = new byte[((i - 1) / j + 1) * k];
				int i1 = 0;
				do {
					int j1 = i - i1;
					if (j1 <= 0)
						break;
					if (j1 > j)
						j1 = j;
					int k1 = cipher.update(abyte0, i1, j1, abyte1, l);
					if (k1 == 0) {
						k1 = cipher.doFinal(abyte1, l);
						if (k1 == 0)
							throw new Exception("encryptString(): Zero for Block of " + j1 + "...");
					}
					l += k1;
					i1 += j1;
				} while (true);
			}
			return new String(Base64.encode(abyte1));
		} catch (Exception exception) {
			throw new Exception("encryptString(): " + exception.toString());
		}
	}

	private static PublicKey loadPublicKey(String s) throws Exception {
		Cipher cipher = null;
		SecretKeySpec secretkeyspec = null;
		try {
			secretkeyspec = new SecretKeySpec("FinancialInternetSolutionsCorporation".substring(0, 24).getBytes(),
					"DESede");
			cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
			cipher.init(1, secretkeyspec);
		} catch (Exception exception) {
			throw new Exception("Unable to create Wrap Key Cipher: " + exception.toString());
		}
		PublicKey publickey = null;
		byte abyte0[];
		java.io.InputStream inputstream = null;
		try {
			Class<?> class1 = Class.forName("com.teknowledge.tekportal.teksecurity.TekSecurityClient");
			ClassLoader classloader = class1.getClassLoader();
			inputstream = classloader.getResourceAsStream("key/" + s);
			DataInputStream datainputstream = new DataInputStream(inputstream);
			abyte0 = new byte[datainputstream.available()];
			datainputstream.readFully(abyte0);
		} catch (Exception exception1) {
			throw new Exception("Could not read key from file: " + s + ", " + exception1.toString());
		}finally {
			if(inputstream != null) {
				inputstream.close();
			}
		}
		byte abyte1[] = null;
		try {
			cipher.init(2, secretkeyspec);
			byte abyte2[] = cipher.doFinal(abyte0);
			abyte1 = new byte[abyte2.length - ((abyte2.length - 1) / 8 + 1)];
			int i = 0;
			int j = 0;
			for (; i < abyte2.length; i++)
				if (i % 8 != 0)
					abyte1[j++] = abyte2[i];

		} catch (Exception exception2) {
			throw new Exception("Unable to decrypt Key: " + exception2.toString());
		}
		String s1 = new String(abyte1);
		String s2 = "---------- (c) 2001. Financial Internet Solutions Corporation. All Rights Reserved. ---------- Created for Teknowledge Corporation. ----------";
		if (s1.indexOf(s2) <= 0)
			throw new Exception("Invalid Key File");
		byte abyte3[] = abyte1;
		int k;
		for (k = 0; k < abyte3.length; k++)
			if ((char) abyte3[k] == '-')
				break;

		byte abyte4[] = new byte[k];
		for (int l = 0; l < k; l++)
			abyte4[l] = abyte3[l];

		int i1 = Integer.parseInt(new String(abyte3, k + 8, 1));
		if (i1 != 4)
			throw new Exception("Invalid Key File Type");
		k += Integer.parseInt(new String(abyte4));
		byte abyte5[] = new byte[abyte3.length - k];
		for (int j1 = k; j1 < abyte3.length; j1++)
			abyte5[j1 - k] = abyte3[j1];

		try {
			X509EncodedKeySpec x509encodedkeyspec = new X509EncodedKeySpec(abyte5);
			KeyFactory keyfactory = KeyFactory.getInstance("RSA");
			publickey = keyfactory.generatePublic(x509encodedkeyspec);
		} catch (Exception exception3) {
			throw new Exception("Unable to create Public Key: " + exception3.toString());
		}
		return publickey;
	}

	public static void main(String args[]) {
		String s = null;
		if (args.length > 0 && args[0].length() > 0)
			try {
				s = encryptString(args[0]);
			} catch (Exception exception) {
				s = "Error: " + exception.toString() + "...";
			}
		else
			s = "Error: Missing Argument...";
		System.out.println(s);
	}

}
