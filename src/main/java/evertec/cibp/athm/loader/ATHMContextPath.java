package evertec.cibp.athm.loader;

import javax.servlet.ServletContext;

import org.springframework.web.context.ServletContextAware;

public class ATHMContextPath implements ServletContextAware {

	private ServletContext servletCtx = null;

	public void setServletContext(ServletContext servletContext) {
		// TODO Auto-generated method stub
		servletCtx = servletContext;
	}

	public String getContextPath() {
		return this.servletCtx.getRealPath("");
	}
}
