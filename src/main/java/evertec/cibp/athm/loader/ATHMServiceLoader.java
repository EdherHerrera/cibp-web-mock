package evertec.cibp.athm.loader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import evertec.cibp.core.services.CustomerServices;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.AthmTermsReq;
import evertec.otherservices.responses.AthmTermsResp;

public class ATHMServiceLoader implements ATHMServices {

	public static final Logger logger = Logger.getLogger(ATHMServices.class);

	protected CustomerServices customerService;
	private String filepath = "";
	private String fiId = "";
	private String fiToken = "";
	private ATHMContextPath contextPath;

	public void updateTermsAndConditions() {
		logger.info("ATHM Terms and Conditions executed.");
		logger.info("Filepath: " + contextPath.getContextPath() + "/WEB-INF/" + filepath);
		AthmTermsReq request = new AthmTermsReq();
		request.setFiId(fiId);
		request.setFiToken(fiToken);
		request.setLanguage("en");

		AthmTermsResp athTerms = customerService.getTerms(request);

		if (athTerms != null && athTerms.getTerms() != null && athTerms.getResponseStatus() == ResponseStatus.SUCCESS) {
			logger.info("Terms: " + athTerms.getTerms());
			String text = "#set($layout = \"layout/popuplayout.vm\")";
			text = text + "<div style=\"margin:10px 5px 15px 20px;\">";
			text = text + athTerms.getTerms();
			text = text + "</div>";
			writeFile(contextPath.getContextPath() + "/WEB-INF/" + filepath + "athmTerms-en.vm", text);
		}

		request.setLanguage("es");
		athTerms = customerService.getTerms(request);

		if (athTerms != null && athTerms.getTerms() != null && athTerms.getResponseStatus() == ResponseStatus.SUCCESS) {
			logger.info("Terms: " + athTerms.getTerms());
			String text = "#set($layout = \"layout/popuplayout.vm\")";
			text = text + "<div style=\"margin:10px 5px 15px 20px;\">";
			text = text + athTerms.getTerms();
			text = text + "</div>";
			writeFile(contextPath.getContextPath() + "/WEB-INF/" + filepath + "athmTerms-es.vm", text);
		}

		request = null;
		athTerms = null;
	}

	@Autowired
	private void writeFile(String fileName, String data) {

	
		PrintWriter out = null;
		try {
			// System.out.println(logData);
			File log = new File(fileName);
			if (log.exists()) {
				out = new PrintWriter(new BufferedWriter(new FileWriter(log, false)));
				out.write(data);
				out.flush();
				out.close();
			} else {

				out = new PrintWriter(new BufferedWriter(new FileWriter(log)));
				out.write(data);
				out.flush();
				out.close();
			}
		} catch (Exception e) {
			logger.error("Error writting ATH Movil Terms and Conditions file: " + e.getMessage());
		}finally {
			  if (out != null) { 
					  out.close (); 
			  }  
		}	
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public String getFilepath() {
		if (filepath != null)
			return filepath.trim();

		return filepath;
	}

	public void setFilepath(String filepath) {
		if (filepath != null)
			filepath = filepath.trim();
		this.filepath = filepath;
	}

	public ATHMContextPath getContextPath() {
		return contextPath;
	}

	public void setContextPath(ATHMContextPath contextPath) {
		this.contextPath = contextPath;
	}

	public String getFiId() {
		return fiId;
	}

	public void setFiId(String fiId) {
		this.fiId = fiId;
	}

	public String getFiToken() {
		return fiToken;
	}

	public void setFiToken(String fiToken) {
		this.fiToken = fiToken;
	}

}
