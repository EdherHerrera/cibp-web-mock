package evertec.cibp.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import evertec.cibp.web.form.bean.SSDSEnrollmentBean;
import evertec.cibp.ws.bean.EnrollmentStatus;

@WebService()
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL)
public interface CibpService {

	@WebMethod(operationName = "enrollCustomer")
	@WebResult(name = "enrollCustomerResponse")
	public EnrollmentStatus enrollCustomer(@WebParam(name = "customer") SSDSEnrollmentBean enrollmentBean);

}
