package evertec.cibp.ws;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;

import evertec.cibp.core.models.customer.CustomerAuthMethod;
import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.enrollment.EnrollableAccount;
import evertec.cibp.core.models.enrollment.EnrollmentInfo;
import evertec.cibp.core.models.enrollment.EnrollmentType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.CardPINAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.ConnectionInfoValidationRequest;
import evertec.cibp.core.services.requests.DepositAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.EnrollmentInformationRequest;
import evertec.cibp.core.services.requests.PayrollCardAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.PersonalInformationValidationRequest;
import evertec.cibp.core.services.responses.ConnectionInfoValidationResponse;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse.EnrollmentValidationStatus;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse.AccountValidationStatus;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.form.bean.SSDSEnrollmentBean;
import evertec.cibp.web.session.EnrollmentSession;
import evertec.cibp.ws.bean.EnrollmentStatus;

public class DefaultCibpService implements CibpService {
	protected final Log logger = LogFactory.getLog(getClass());
	private CustomerServices customerService;
	private boolean allowAccentedCharacters;
	private String user;
	private String pass;

	public void setUser(String user) {
		this.user = user;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public boolean isAllowAccentedCharacters() {
		return allowAccentedCharacters;
	}

	public void setAllowAccentedCharacters(boolean allowAccentedCharacters) {
		this.allowAccentedCharacters = allowAccentedCharacters;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public EnrollmentStatus enrollCustomer(SSDSEnrollmentBean enrollmentBean) {
		EnrollmentStatus status = new EnrollmentStatus();
		try {

			if (!enrollmentBean.getCibpServiceUser().equalsIgnoreCase(user)
					|| !enrollmentBean.getCibpServicePass().equalsIgnoreCase(pass)) {
				status.setEnrolled(false);
				status.setDescription("Invalid user/password");
				status.setExit(1);
				logger.error("Error Migration Service: Invalid web-service user/password");
				return status;
			}
			boolean enrollTelepago;
			if (enrollmentBean.getEnrollTelepago() == null) {
				status.setEnrolled(false);
				status.setDescription("Must Specify true/false value for EnrollTelepago");
				status.setExit(1);
				logger.error("Error Migration Service: Must Specify true/false value for EnrollTelepago");
				return status;
			}

			enrollTelepago = enrollmentBean.isEnrollTelepago();

			// EnrollmentSession enrollmentSession = (EnrollmentSession)
			// WebUtils.getSessionAttribute(request, "enrollmentSession");
			EnrollmentSession enrollmentSession = new EnrollmentSession();

			String taxId = enrollmentBean.getTaxId();
			int year = enrollmentBean.getDobyear();
			int month = enrollmentBean.getDobmonth();
			int day = enrollmentBean.getDobday();

			String accountnumber = enrollmentBean.getAccountnumber();
			String athnumber = enrollmentBean.getAthnumber();
			String accountType = enrollmentBean.getAccountInfoType();
			ValidateAccountStatusResponse validateAccountInfo = null;

			// String rePwd1 = "^[a-zA-Z0-9]+$";
			String username1 = enrollmentBean.getUsername();

			GregorianCalendar dob = new GregorianCalendar(year, month, day);
			enrollmentSession.setSubmitedDob(dob);
			enrollmentSession.setValidatedTaxId(taxId);

			PersonalInformationValidationRequest personalRequest = new PersonalInformationValidationRequest(null,
					taxId, dob, enrollmentSession.isFailed(), false);
			PersonalInformationValidationResponse customerEnrollment = customerService
					.isEnrollableCustomer(personalRequest);
			EnrollmentValidationStatus status1 = customerEnrollment.getStatus();

			boolean isenroll = false;
			if (customerEnrollment.getExistingAccounts() != null)
				for (Iterator<EnrollableAccount> iterator = customerEnrollment.getExistingAccounts().iterator(); iterator
						.hasNext();) {
					EnrollableAccount type = (EnrollableAccount) iterator.next();
					if (type.getAccountId().equals(accountnumber)) {
						isenroll = true;
					}
				}

			// end enrollment. Customer exists on the database or counter
			// reached blocked count limit
			if (status1.equals(PersonalInformationValidationResponse.EnrollmentValidationStatus.ALREADY_ENROLL)
					|| isenroll) {// customerEnrollment.isHasExistingOnlineAccount())
									// {
				status.setDescription("Already enrolled, please login");
				status.setAlreadyExists(true);
				status.setEnrolled(false);
				return status;
			} else /*
					 * if (status.equals(PersonalInformationValidationResponse.
					 * EnrollmentValidationStatus.ENROLLABLE))
					 */{
				enrollmentBean.setAccounts(customerEnrollment.getEnrollableAccounts());
				enrollmentSession.setSubmitedDob(dob);
				enrollmentSession.setValidatedTaxId(taxId);
			}

			if (accountType.equalsIgnoreCase("IDA")) {
				DepositAccountInformationValidationRequest depReq = new DepositAccountInformationValidationRequest(
						null, accountnumber, athnumber, taxId, null, false, true);
				validateAccountInfo = customerService.validateDepositAccountInfo(depReq);
			} else if (accountType.equalsIgnoreCase("CCA") || accountType.equalsIgnoreCase("AMEX")) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d+")) {
					status.setDescription("Invalid account Number");
					status.setEnrolled(false);
					return status;
				}
				CardPINAccountInformationValidationRequest cardPinReq = new CardPINAccountInformationValidationRequest(
						null, accountnumber, taxId, "", "", "", "", false, false, false);
				validateAccountInfo = customerService.validateCrediCardPINAccountInfo(cardPinReq);

			} else if (accountType.equalsIgnoreCase("PFC")) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{16}")) {
					status.setDescription("Invalid account Number");
					status.setEnrolled(false);
					return status;
				}
				PayrollCardAccountInformationValidationRequest payrollReq = new PayrollCardAccountInformationValidationRequest(
						null, accountnumber, taxId, "", false);
				validateAccountInfo = customerService.validatePayrollCardAccountInfo(payrollReq);
			}

			// ValidateInfo status... validation...
			if (validateAccountInfo.getStatus() == AccountValidationStatus.VALID) {
				enrollmentSession.setValidatedAcct(accountnumber);
				enrollmentSession.setValidatedAcctType(accountType);
				enrollmentBean.setPermId(validateAccountInfo.getPermId());
				enrollmentSession.setValidatedAcctProductId(validateAccountInfo.getAccountProductId());
			} else {
				enrollmentSession.setValidatedAcct(accountnumber);
				enrollmentSession.setValidatedAcctType(accountType);
			}

			ConnectionInfoValidationRequest conectionRequest = new ConnectionInfoValidationRequest(null, username1,
					enrollmentBean.getTaxId(), null, "sp", false);
			conectionRequest.setValidateInRsa(false);
			ConnectionInfoValidationResponse validateConnectionInformation = customerService
					.validateConnectionInformation(conectionRequest);
			if (!validateConnectionInformation.isUserNameValid()) {
				status.setDescription("Invalid Username");
				status.setEnrolled(false);
				return status;
			}

			EnrollmentInfo ei = new EnrollmentInfo();
			ei.setPermId(enrollmentBean.getPermId());
			ei.setTaxId(enrollmentSession.getValidatedTaxId());
			ei.setCreationDate(new Date());
			ei.setActivationDate(new Date());
			ei.setBirthDate(enrollmentSession.getSubmitedDob().getTime());
			ei.setEmail(enrollmentBean.getEmail());
			ei.setFirstName(enrollmentBean.getFirstname());
			ei.setLastName(enrollmentBean.getLastname());
			ei.setUsername(enrollmentBean.getUsername().toLowerCase());
			int genereatedRandomSalt = PasswordHashUtil.genereateRandomSalt();

			String pass = "bppr" + ei.getTaxId().substring(ei.getTaxId().length() - 4, ei.getTaxId().length());
			ei.setPasswordHash(PasswordHashUtil.generateSaltedHash(pass, genereatedRandomSalt));
			ei.setPasswordSalt(genereatedRandomSalt);
			if (enrollmentSession.isFailed() || enrollmentSession.getDobCounter() >= 3
					|| enrollmentSession.getActCounter() >= 3 || enrollmentSession.getActInfCounter() >= 3) {

				if (enrollmentSession.getDobCounter() >= 3)
					ei.setDobFail(enrollmentSession.getSubmitedDob().getTime());
				else if (enrollmentSession.getActCounter() >= 3 || enrollmentSession.getActInfCounter() >= 3) {
					ei.setAccountTypeFail(enrollmentSession.getValidatedAcctType());
					ei.setAccountNumberFail(enrollmentBean.getAccountnumber());
					if (enrollmentBean.getAccountInfoType().equals("IDA"))
						ei.setAthNumberFail(enrollmentBean.getAthnumber());
				}

				ei.setInitialCustomerStatus(CustomerStatus.PENDING_CSR_ACTIVATION);
				enrollmentBean.setValidated(false);
				status.setDescription("Customer Information not found in our records.");
				status.setEnrolled(false);
				return status;

			} else {
				ei.setInitialCustomerStatus(CustomerStatus.ENROLLED);
			}
			ei.setEnrollmentSource(CustomerEnrollmentSource.SSDS);
			ei.setInitialAuthenticationMethod(CustomerAuthMethod.PASSWD_RSA);
			ei.setEnrollmentStatus(evertec.cibp.core.models.enrollment.EnrollmentStatus.COMPLETE);
			ei.setEnrollmentAccountNumber(enrollmentSession.getValidatedAcct());
			ei.setEnrollmentAccountSubType(enrollmentSession.getValidatedAcctType());
			ei.setEnrollmentAccountProductId(enrollmentSession.getValidatedAcctProductId());
			if (enrollmentBean.getAccountInfoType().equalsIgnoreCase("IDA"))
				ei.setEnrollmentPlasticNumber(enrollmentBean.getAthnumber());
			else
				ei.setEnrollmentPlasticNumber(enrollmentBean.getAthnumber());

			List<CustomerFlagType> flags = new LinkedList<CustomerFlagType>();
			ei.setCustomerFlagTypes(flags);

			PreferredLanguage lang;
			lang = PreferredLanguage.ENGLISH;

			EnrollmentInformationRequest enrollReq = new EnrollmentInformationRequest(null, false, ei,
					EnrollmentType.ACTIVE, null, null, null, null, null, null, null, false, true);
			enrollReq.setRsaLang("en");
			CustomerProfile profile;
			if (enrollTelepago)
				profile = customerService.enrollUserNoRSA(enrollReq, lang);
			else
				profile = customerService.enrollUserNoRSANoTelepago(enrollReq, lang);

		} catch (Exception e) {
			status.setEnrolled(false);
			status.setDescription("ERROR: " + e.getMessage());
			status.setExit(0);
			logger.error("Error Migration Service: " + e.getMessage());
			return status;
		}
		status.setEnrolled(true);
		status.setDescription("SUCCESSFULLY ENROLLED");
		status.setExit(0);
		return status;
	}

}
