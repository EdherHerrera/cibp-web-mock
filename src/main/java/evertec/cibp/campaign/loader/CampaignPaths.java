package evertec.cibp.campaign.loader;

import javax.servlet.ServletContext;

import org.springframework.web.context.ServletContextAware;

public class CampaignPaths implements ServletContextAware {

	private ServletContext servletCtx = null;
	private String imagePath = null;
	private String filePath = null;

	public void setServletContext(ServletContext servletContext) {
		// TODO Auto-generated method stub
		servletCtx = servletContext;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getImagePath() {
		return this.servletCtx.getRealPath("") + "/" + imagePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFilePath() {
		return this.servletCtx.getRealPath("") + "/WEB-INF/" + filePath;
	}

}
