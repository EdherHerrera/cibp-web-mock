package evertec.cibp.campaign.loader;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ReloadCampaigns {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {
				"classpath:/evertec/cibp/core/services/applicationContext.xml",
				"classpath:/evertec/cibp/db/hibernate/jndi-datasource-context.xml",
				"classpath:/evertec/cibp/db/ebpp/hibernate/jndi-datasource-context.xml",
				"classpath:/evertec/cibp/campaign/loader/applicationContext-jobs.xml" });

		CampaignServices serv = (CampaignServices) context.getBean("campaignLoaderService");
		serv.updateCampaignAndImages();

	}

}
