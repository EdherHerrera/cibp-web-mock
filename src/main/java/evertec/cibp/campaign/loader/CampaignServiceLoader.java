package evertec.cibp.campaign.loader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.CharEncoding;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;

import evertec.cibp.core.models.BinaryData.BinaryRecord;
import evertec.cibp.core.models.BinaryData.BinaryStatus;
import evertec.cibp.core.models.BinaryData.BinaryTypes;
import evertec.cibp.core.models.campaign.ActiveCampaignView;
import evertec.cibp.core.models.campaign.Campaign;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.campaign.LoginCampaign;
import evertec.cibp.core.models.campaign.WantToDoCampaign;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.utils.ImageUtils;

public class CampaignServiceLoader implements CampaignServices {

	public static final Logger logger = Logger.getLogger(CampaignServices.class);
	private String filepath = "";
	private String imagepath = "";
	private Integer maxNumberCampaign;
	protected CustomerServices customerService;
	private CampaignPaths paths;

	public void updateCampaignAndImages() {
		logger.info("Updating Portal Campaign.");
		filepath = getPaths().getFilePath();
		imagepath = getPaths().getImagePath();

		getLoginBackgroungImage();
		getLoginCampaigns();
	}

	public void updateWantToDo() {
		filepath = getPaths().getFilePath();
		imagepath = getPaths().getImagePath();
		getWantToDoArea();
	}

	private void getLoginCampaigns() {

		/*
		 * mpicado, CIBP-1806, lo primero que hace el metodo es eliminar todos
		 * los archivos de campanas que existan en el directorio donde ellas se
		 * guardan, para luego proceder a crearlas (si es que hay campanas), la
		 * idea de borrar todos los archivos antes de crearlos o caerles encima,
		 * la idea es que no hayan archicvos que en realidad no se vayan a usar,
		 * por ejemplo si la ultima vez hubo 3 campanas y ahora hay 2, el
		 * archivo de la campana 3 va quedar en el path a pesar de que no se
		 * vaya a usar, por lo que lo ideal es mejor borrarlo para evitar
		 * confusion y ademas para evitar que el path se llene de archivos que
		 * no se usan
		 */
		// File dir = new File(this.filepath);
		// String[] listaArchivos = dir.list();
		// if (listaArchivos != null) {
		// for (int i = 0; i < listaArchivos.length; i++) {
		// if (listaArchivos[i].contains("WantToDo"))
		// continue;
		// File f = new File(this.filepath + listaArchivos[i]);
		// if (!f.getName().equals(".svn")) {// esta condicion es para que
		// // no tome en cuenta el
		// // directorio .svn que
		// // siempre hay en esta
		// // carpeta...
		// boolean resp = f.delete();
		// if (resp == false)
		// System.out.println("no se pudo borrar el archivo: " + f.getName());
		// }
		// }
		// }
		String englishCamps = "";
		String spanishCamps = "";

		try {
			String divEnCampaign = "";
			String divEsCampaign = "";
			Integer maxCampaign = maxNumberCampaign;

			List<Campaign> fixedList = customerService.getRunnigCampaignByType(CampaignType.FIXED_LOGIN_CAMPAIGN);
			LoginCampaign fixedCampaign = null;

			/* CIBP-2353 Crea el div para el fixed */
			if (!fixedList.isEmpty()) {
				maxCampaign = maxNumberCampaign - 1;// Resto el Fixed
				fixedCampaign = new LoginCampaign(fixedList.get(0));
				String imagename = "";
				BinaryRecord rec = customerService.getImagebyID(fixedCampaign.getCampaign().getImageId());
				byte[] image = new byte[] {};
				if (rec != null) {
					int length = (int) rec.getData().length();
					image = rec.getData().getBytes(1, length);
					imagename = fixedCampaign.getCampaign().getId() + ".jpg";
					ImageUtils.writeImage(imagepath + imagename, image);
				}

				divEnCampaign = "<div width=\"205px;\" style=\"float:left;background-image: url(../img/adSpacewithLine.jpg);background-repeat:no-repeat;\">\n"
						+ "      <table width=\"205px;\" cellspacing=\"2\" style=\"margin-left:5px;\">\n"
						+ "      <colgroup><col width=\"130px;\"><col width=\"75px;\"></colgroup>\n"
						+ "      <tr><td nowrap align=\"left\" colspan=\"2\" style=\"font-family:Arial,sans-serif,Sans;font-style:normal;font-size:12px;color:#33628C;\"><b>"
						+ fixedCampaign.getEnglishTitle()
						+ "</b></td></tr>\n"
						+ "      <tr><td align=\"left\" style=\"font-family:Arial, sans-serif, Sans;font-style:normal;font-size:11px;color:#58595B;\">"
						+ fixedCampaign.getEnglishBody()
						+ "</td>\n"
						+ "          <td style=\"padding-right:10px;\"><img src=\"../img/cm/"
						+ imagename
						+ "\" width=\"70px;\" height=\"50px;\"/></td></tr>\n"
						+ "      <tr><td align=\"left\" colspan=\"2\" style=\"font-family:Arial, sans-serif, Sans;font-style:underline;font-size:11px;color:#3385CD;v-align:top;\">\n ";
				if (fixedCampaign.getLinkable() && !GenericValidator.isBlankOrNull(fixedCampaign.getEnglishLink())) {
					divEnCampaign = divEnCampaign + "<a onclick=\"setStat('" + fixedCampaign.getCampaign().getId()
							+ "', 'LOGIN_CAMPAIGN', 'CLICK');\" href=\'" + fixedCampaign.getEnglishLink()
							+ "\' target=\"_blank\" style=\"color:#3385CD;\"><span style=\"color:#3385CD;\">"
							+ fixedCampaign.getEnglishAction() + "</span></a>\n";
				} else
					divEnCampaign = divEnCampaign + fixedCampaign.getEnglishAction();

				divEnCampaign = divEnCampaign + "      </td></tr>\n" + "      </table>\n" + "      </div>\n";

				divEsCampaign = "<div width=\"205px;\" style=\"float:left;background-image: url(../img/adSpacewithLine.jpg);background-repeat:no-repeat;\">\n"
						+ "      <table width=\"205px;\" cellspacing=\"2\" style=\"margin-left:5px;\">\n"
						+ "      <colgroup><col width=\"130px;\"><col width=\"75px;\"></colgroup>\n"
						+ "      <tr><td nowrap align=\"left\" colspan=\"2\" style=\"font-family:Arial,sans-serif,Sans;font-style:normal;font-size:12px;color:#33628C;\"><b>"
						+ fixedCampaign.getSpanishTitle()
						+ "</b></td></tr>\n"
						+ "      <tr><td align=\"left\" style=\"font-family:Arial, sans-serif, Sans;font-style:normal;font-size:11px;color:#58595B;\">"
						+ fixedCampaign.getSpanishBody()
						+ "</td>\n"
						+ "          <td style=\"padding-right:10px;\"><img src=\"../img/cm/"
						+ imagename
						+ "\" width=\"70px;\" height=\"50px;\"/></td></tr>\n"
						+ "      <tr><td align=\"left\" colspan=\"2\" style=\"font-family:Arial, sans-serif, Sans;font-style:underline;font-size:11px;color:#3385CD;v-align:top;\"> \n";
				if (fixedCampaign.getLinkable() && !GenericValidator.isBlankOrNull(fixedCampaign.getSpanishLink())) {
					divEsCampaign = divEsCampaign + "<a onclick=\"setStat('" + fixedCampaign.getCampaign().getId()
							+ "', 'LOGIN_CAMPAIGN', 'CLICK');\" href=\'" + fixedCampaign.getSpanishLink()
							+ "\' target=\"_blank\" style=\"color:#3385CD;\"><span style=\"color:#3385CD;\">"
							+ fixedCampaign.getSpanishAction() + "</span></a>\n";
				} else
					divEsCampaign = divEsCampaign + fixedCampaign.getSpanishAction();

				divEsCampaign = divEsCampaign + "      </td></tr>\n" + "      </table>\n" + "      </div>\n";

				writeFile(filepath + "fixedEnCampaign.vm", divEnCampaign);
				writeFile(filepath + "fixedEsCampaign.vm", divEsCampaign);
			} else {// Borrar las campania fixed si existen.
				try {
					File f = new File(filepath + "fixedEnCampaign.vm");
					f.delete();
					f = new File(filepath + "fixedEsCampaign.vm");
					f.delete();
				} catch (Exception e) {
					System.out.println("Error borrando antiguas fixed Campaigns");
				}

			}// Fin if fixed

			List<ActiveCampaignView> list = customerService.getActiveCampaignByType(CampaignType.LOGIN_CAMPAIGN,
					maxCampaign);

			String startText1 = "<input type=\"hidden\" name=\"pages\" id=\"pages\" value=\"" + list.size() + "\">\n";
			// mpicado, CIBP-1806, se prepara el texto(codigo HTML) que tiene el
			// archivo de campana global
			startText1 += "#set( $list = $campaignTool.obtieneLista(" + list.size() + "))\n";

			// mpicado, CIBP-1806, esto que sigue depende del idioma por lo que
			// no se puede concatenar de una vez
			// "#set($path=\"vm/cm/divEsCampaign\")\n" +

			String startText2 = "#set($path2=\".vm\")\n" + "#foreach( $l in $list)\n" + "#parse(\"$path$l$path2\")\n"
					+ "#end";

			englishCamps = startText1 + "#set($path=\"vm/cm/divEnCampaign\")\n" + startText2;
			spanishCamps = startText1 + "#set($path=\"vm/cm/divEsCampaign\")\n" + startText2;

			writeFile(filepath + "loginCampaign_en.vm", englishCamps);
			writeFile(filepath + "loginCampaign_es.vm", spanishCamps);

			// System.out.println("I'm here: " +
			// sf.format(Calendar.getInstance().getTime()));
			List<Campaign> list2 = customerService.getRunnigCampaignByType(CampaignType.LOGIN_CAMPAIGN, maxCampaign);

			// mpicado, CIBP-1806
			divEnCampaign = "";
			divEsCampaign = "";

			for (int i = 0; i < list2.size(); i++) {
				LoginCampaign name = new LoginCampaign(list2.get(i));
				String imagename = "";
				BinaryRecord rec = customerService.getImagebyID(name.getCampaign().getImageId());
				byte[] image = new byte[] {};
				if (rec != null) {
					int length = (int) rec.getData().length();
					image = rec.getData().getBytes(1, length);
					imagename = name.getCampaign().getId() + ".jpg";
					ImageUtils.writeImage(imagepath + imagename, image);
				}

				divEnCampaign = "<div width=\"205px;\" style=\"float:left;background-image: url(../img/adSpacewithLine.jpg);background-repeat:no-repeat;\">\n"
						+ "		<table width=\"205px;\" cellspacing=\"2\" style=\"margin-left:5px;\">\n"
						+ "		<colgroup><col width=\"130px;\"><col width=\"75px;\"></colgroup>\n"
						+ "		<tr><td nowrap align=\"left\" colspan=\"2\" style=\"font-family:Arial,sans-serif,Sans;font-style:normal;font-size:12px;color:#33628C;\"><b>"
						+ name.getEnglishTitle()
						+ "</b></td></tr>\n"
						+ "		<tr><td align=\"left\" style=\"font-family:Arial, sans-serif, Sans;font-style:normal;font-size:11px;color:#58595B;\">"
						+ name.getEnglishBody()
						+ "</td>\n"
						+ "			<td style=\"padding-right:10px;\"><img src=\"../img/cm/"
						+ imagename
						+ "\" width=\"70px;\" height=\"50px;\"/></td></tr>\n"
						+ "		<tr><td align=\"left\" colspan=\"2\" style=\"font-family:Arial, sans-serif, Sans;font-style:underline;font-size:11px;color:#3385CD;v-align:top;\">\n ";
				if (name.getLinkable() && !GenericValidator.isBlankOrNull(name.getEnglishLink())) {
					divEnCampaign = divEnCampaign + "<a onclick=\"setStat('" + name.getCampaign().getId()
							+ "', 'LOGIN_CAMPAIGN', 'CLICK');\" href=\'" + name.getEnglishLink()
							+ "\' target=\"_blank\" style=\"color:#3385CD;\"><span style=\"color:#3385CD;\">"
							+ name.getEnglishAction() + "</span></a>\n";
				} else
					divEnCampaign = divEnCampaign + name.getEnglishAction();

				divEnCampaign = divEnCampaign + "		</td></tr>\n" + "		</table>\n" + "		</div>\n";

				divEsCampaign = "<div width=\"205px;\" style=\"float:left;background-image: url(../img/adSpacewithLine.jpg);background-repeat:no-repeat;\">\n"
						+ "		<table width=\"205px;\" cellspacing=\"2\" style=\"margin-left:5px;\">\n"
						+ "		<colgroup><col width=\"130px;\"><col width=\"75px;\"></colgroup>\n"
						+ "		<tr><td nowrap align=\"left\" colspan=\"2\" style=\"font-family:Arial,sans-serif,Sans;font-style:normal;font-size:12px;color:#33628C;\"><b>"
						+ name.getSpanishTitle()
						+ "</b></td></tr>\n"
						+ "		<tr><td align=\"left\" style=\"font-family:Arial, sans-serif, Sans;font-style:normal;font-size:11px;color:#58595B;\">"
						+ name.getSpanishBody()
						+ "</td>\n"
						+ "			<td style=\"padding-right:10px;\"><img src=\"../img/cm/"
						+ imagename
						+ "\" width=\"70px;\" height=\"50px;\"/></td></tr>\n"
						+ "		<tr><td align=\"left\" colspan=\"2\" style=\"font-family:Arial, sans-serif, Sans;font-style:underline;font-size:11px;color:#3385CD;v-align:top;\"> \n";
				if (name.getLinkable() && !GenericValidator.isBlankOrNull(name.getSpanishLink())) {
					divEsCampaign = divEsCampaign + "<a onclick=\"setStat('" + name.getCampaign().getId()
							+ "', 'LOGIN_CAMPAIGN', 'CLICK');\" href=\'" + name.getSpanishLink()
							+ "\' target=\"_blank\" style=\"color:#3385CD;\"><span style=\"color:#3385CD;\">"
							+ name.getSpanishAction() + "</span></a>\n";
				} else
					divEsCampaign = divEsCampaign + name.getSpanishAction();

				divEsCampaign = divEsCampaign + "		</td></tr>\n" + "		</table>\n" + "		</div>\n";

				/*
				 * mpicado, CIBP-1806, aqui se escribe cada uno de los archivos
				 * por separado igles y espanol de las campanas que vayan
				 * iterando, ya que se va a manejar un archivo por campana para
				 * poderlos ordenar aleatoriamente
				 */
				writeFile(filepath + "divEnCampaign" + (i + 1) + ".vm", divEnCampaign);
				writeFile(filepath + "divEsCampaign" + (i + 1) + ".vm", divEsCampaign);
			}

		} catch (SQLException e) {
			logger.error("Error al crear las campaigns de login: " + e.getMessage());
		}

	}

	private void getLoginBackgroungImage() {

		try {
			BinaryRecord prback = customerService.getImagebyTypeandStatus(BinaryTypes.LOGIN_IMAGE_PR,
					BinaryStatus.CURRENT);
			BinaryRecord viback = customerService.getImagebyTypeandStatus(BinaryTypes.LOGIN_IMAGE_VI,
					BinaryStatus.CURRENT);

			byte[] image = new byte[] {};
			if (prback != null) {
				int length = (int) prback.getData().length();
				image = prback.getData().getBytes(1, length);
				ImageUtils.writeImage(imagepath + "imgLoginPR.jpg", image);
			}

			byte[] image2 = new byte[] {};
			if (viback != null) {
				int length = (int) viback.getData().length();
				image2 = viback.getData().getBytes(1, length);
				ImageUtils.writeImage(imagepath + "imgLoginVI.jpg", image2);
			}

		} catch (SQLException e) {
			logger.error("Error al escribir el backgroundImage: " + e.getMessage());
		}

	}

	private void getWantToDoArea() {

		try {
			/*
			 * File dir = new File(this.filepath); String[] listaArchivos =
			 * dir.list(); if (listaArchivos != null) { for (int i = 0; i <
			 * listaArchivos.length; i++) { if
			 * (!listaArchivos[i].contains("WantToDo")) continue; File f = new
			 * File(this.filepath + listaArchivos[i]); if
			 * (!f.getName().equals(".svn")) {// esta condicion es para // que
			 * no tome en cuenta // el directorio .svn // que siempre hay en //
			 * esta carpeta... boolean resp = f.delete(); if (resp == false)
			 * System.out.println("no se pudo borrar el archivo tipo WantToDo: "
			 * + f.getName()); } } }
			 */
			List<Campaign> loginc = customerService.getRunnigCampaignByType(CampaignType.WANT_TO_DO);
			if (!(loginc == null || loginc.size() < 1)) {
				WantToDoCampaign name = new WantToDoCampaign((Campaign) loginc.get(0));

				BinaryRecord space1 = customerService.getImagebyID(name.getCampaign().getImageId());
				BinaryRecord space2 = customerService.getImagebyID(name.getCampaign().getImageId2());
				String imageName = "";
				byte[] image = new byte[] {};
				if (space1 != null) {
					int length = (int) space1.getData().length();
					image = space1.getData().getBytes(1, length);
					String contentType = space1.getContentType();
					imageName = "imgWantToDo." + contentType.substring(contentType.indexOf('/') + 1);
					ImageUtils.writeImage(imagepath + imageName, image);
				}
				String imageName2 = "";
				byte[] image2 = new byte[] {};
				if (space2 != null) {
					int length = (int) space2.getData().length();
					image2 = space2.getData().getBytes(1, length);
					String contentType = space2.getContentType();
					imageName2 = "img2WantToDo." + contentType.substring(contentType.indexOf('/') + 1);
					ImageUtils.writeImage(imagepath + imageName2, image2);
				}

				String divEnWantToDo = "";
				String divEsWantToDo = "";

				divEnWantToDo = "<table>\n"
						+ "<tr class=\"row1\"><td colspan=\"2\">&nbsp;&nbsp;&nbsp;What do you want to do?</td></tr>\n"
						+ "<tr class=\"row2\"><td style=\"padding-left:10px;width:34px;\"><img src=\"../img/cm/"
						+ imageName + "\" border=\"0\"></td>\n" + "<td>" + name.getSpace_1_EnglishBody() + "&nbsp;";
				if (name.getSpace_1_Linkable() && !GenericValidator.isBlankOrNull(name.getSpace_1_EnglishLink())) {
					divEnWantToDo += "<a onclick=\"setStat('" + name.getCampaign().getId()
							+ "', 'WANT_TO_DO', 'CLICK');\" href=\"" + name.getSpace_1_EnglishLink()
							+ "\" style=\"color:#3385CD;\" target=\"_blank\">" + name.getSpace_1_EnglishAction()
							+ "</a></td>\n</tr>\n";
				} else
					divEnWantToDo += name.getSpace_1_EnglishAction() + "</td>\n</tr>\n";

				divEnWantToDo += "<tr class=\"row3\"><td style=\"padding-left:10px;width:34px;\"><img src=\"../img/cm/"
						+ imageName2 + "\" border=\"0\"></td>\n" + "<td>" + name.getSpace_2_EnglishBody() + "&nbsp;";

				if (name.getSpace_2_Linkable() && !GenericValidator.isBlankOrNull(name.getSpace_2_EnglishLink())) {
					divEnWantToDo += "<a onclick=\"setStat('" + name.getCampaign().getId()
							+ "', 'WANT_TO_DO', 'CLICK');\" href=\"" + name.getSpace_2_EnglishLink()
							+ "\" style=\"color:#3385CD;\" target=\"_blank\">" + name.getSpace_2_EnglishAction()
							+ "</a></td>\n</tr>\n</table>\n";
				} else
					divEnWantToDo += name.getSpace_2_EnglishAction() + "</td>\n</tr>\n</table>\n";

				divEsWantToDo = "<table>\n"
						+ "<tr class=\"row1\"><td colspan=\"2\">&nbsp;&nbsp;&nbsp;&iquest;Qu&eacute; quieres hacer hoy?</td></tr>\n"
						+ "<tr class=\"row2\"><td style=\"padding-left:10px;width:34px;\"><img src=\"../img/cm/"
						+ imageName + "\" border=\"0\"></td>\n" + "<td>" + name.getSpace_1_SpanishBody() + "&nbsp;";
				if (name.getSpace_1_Linkable() && !GenericValidator.isBlankOrNull(name.getSpace_1_SpanishLink())) {
					divEsWantToDo += "<a onclick=\"setStat('" + name.getCampaign().getId()
							+ "', 'WANT_TO_DO', 'CLICK');\" href=\"" + name.getSpace_1_SpanishLink()
							+ "\" style=\"color:#3385CD;\" target=\"_blank\">" + name.getSpace_1_SpanishAction()
							+ "</a></td>\n</tr>\n";
				} else
					divEsWantToDo += name.getSpace_1_SpanishAction() + "</td>\n</tr>\n";

				divEsWantToDo += "<tr class=\"row3\"><td style=\"padding-left:10px;width:34px;\"><img src=\"../img/cm/"
						+ imageName2 + "\" border=\"0\"></td>\n" + "<td>" + name.getSpace_2_SpanishBody() + "&nbsp;";

				if (name.getSpace_2_Linkable() && !GenericValidator.isBlankOrNull(name.getSpace_2_SpanishLink())) {
					divEsWantToDo += "<a onclick=\"setStat('" + name.getCampaign().getId()
							+ "', 'WANT_TO_DO', 'CLICK');\" href=\"" + name.getSpace_2_SpanishLink()
							+ "\" style=\"color:#3385CD;\" target=\"_blank\">" + name.getSpace_2_SpanishAction()
							+ "</a></td>\n</tr>\n</table>\n";
				} else
					divEsWantToDo += name.getSpace_2_SpanishAction() + "</td>\n</tr>\n</table>\n";

				writeFile(filepath + "divEnWantToDo.vm", divEnWantToDo);
				writeFile(filepath + "divEsWantToDo.vm", divEsWantToDo);
			} else {
				/*
				 * writeFile(filepath + "divEnWantToDo.vm", "");
				 * writeFile(filepath + "divEsWantToDo.vm", ""); logger.error(
				 * "Error al escribir WantTodo Area: Campaign no devuelto de la base de datos."
				 * );
				 */
			}

		} catch (SQLException e) {
			logger.error("Error al escribir WantTodo Area: " + e.getMessage());
		}

	}

	// New writeFile method to set the correct encoding
	private void writeFile(String fileName, String data) {

		FileOutputStream fos = null;
		try {
			File log = new File(fileName);
			fos = new FileOutputStream(log, false);
			Writer out = new OutputStreamWriter(fos, CharEncoding.UTF_8);
			out.write(data);
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error("Error al escribir el archivo de campana: " + e.getMessage());
		}finally {
			if(fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
				}
			}
		}
	}

	/**
	 * Metodo insertGuideSetupCampaign Metodo que inserta la campana para el
	 * guided setup de clientes nuevos, es una campana fija por lo que se llama
	 * al iniciar el server para estar seguros de que existe en DB. Si no existe
	 * hay que verificar esto para no generar inconsistencia de datos en la DB.
	 * 
	 * @author mpicado, CIBP-2218
	 */
	public void insertGuideSetupCampaign() {
		boolean result = this.customerService.insertGuidedSetupCampaign();
		if (!result)
			logger.error("REVISAR PORQUE RAZON NO SE CREO LA CAMPANA PARA EL GUIDED SETUP DE CLIENTES NUEVOS!!!");
	}

	public String getImagepath() {
		if (imagepath != null)
			return imagepath.trim();

		return imagepath;
	}

	public void setImagepath(String imagepath) {
		if (imagepath != null)
			imagepath = imagepath.trim();
		this.imagepath = imagepath;
	}

	public String getFilepath() {
		if (filepath != null)
			return filepath.trim();

		return filepath;
	}

	public void setFilepath(String filepath) {
		if (filepath != null)
			filepath = filepath.trim();
		this.filepath = filepath;
	}

	public Integer getMaxNumberCampaign() {
		return maxNumberCampaign;
	}

	public void setMaxNumberCampaign(Integer maxNumberCampaign) {
		this.maxNumberCampaign = maxNumberCampaign;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public void setPaths(CampaignPaths paths) {
		this.paths = paths;
	}

	public CampaignPaths getPaths() {
		return paths;
	}

}