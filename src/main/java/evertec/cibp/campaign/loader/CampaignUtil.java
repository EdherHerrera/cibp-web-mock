/**
 * 
 */
package evertec.cibp.campaign.loader;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase evertec.cibp.campaign.loader.CampaignUtil Sistema: MiBanco Descripcion:
 * Clase que representa las utilidades de campaign que se ocupen en la capa de
 * presentacion, especificamente en velocity
 * 
 * @author Ing. Michael Picado Fuentes Creada el 10/06/2010 10:40:25 a.m.
 */
public class CampaignUtil {

	/* Constructor de la clase CampaignUtil */
	public CampaignUtil() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Metodo obtieneLista metodo que prepara y ordenada de forma aleatoria la
	 * lista de campanas a mostrar. La lista trae solo el numero del archivo de
	 * campana a mostrar
	 * 
	 * @param tamano
	 *            , numero de campanas
	 * @return
	 * @author mpicado
	 */
	public List<Integer> obtieneLista(Integer tamano) {
		List<Integer> lista = new ArrayList<Integer>();
		List<Integer> listaOrdenada = new ArrayList<Integer>();

		// agrego los numeros en orden desde el 1 hasta el numero de campanas
		// que vengan
		for (int i = 1; i <= tamano; i++)
			lista.add(i);

		/*
		 * mpicado, el ordenamiento random de una lista se puede hacer de muchas
		 * maneras, lo hice de esta forma para no tener que lidiar con las
		 * posiciones que no han salido(en el random) por lo que preferi hacer
		 * un random por posicion e ir borrando esa posicion en la otra lista,
		 * me parece que trabaja bien con la cantidad de datos que se manejan,
		 * ya que no son muchos...
		 */
		while (lista.size() > 0) {
			int aleat = (int) (Math.random() * lista.size());
			listaOrdenada.add(lista.get(aleat));
			lista.remove(aleat);
		}

		return listaOrdenada;
	}

}
