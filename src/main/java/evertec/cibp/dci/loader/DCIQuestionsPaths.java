package evertec.cibp.dci.loader;

import javax.servlet.ServletContext;

import org.springframework.web.context.ServletContextAware;

public class DCIQuestionsPaths implements ServletContextAware {

	private ServletContext servletCtx = null;
	private String filePath = null;

	public void setServletContext(ServletContext servletContext) {
		servletCtx = servletContext;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFilePath() {
		return this.servletCtx.getRealPath("") + "/WEB-INF/" + filePath;
	}

}
