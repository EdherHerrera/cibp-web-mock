package evertec.cibp.dci.loader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;

import evertec.cibp.core.models.accountopening.dci.DCIQuestion;
import evertec.cibp.core.models.accountopening.dci.DCIQuestionAnswer;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;

public class DCIQuestionsServiceLoader implements DCIQuestionsServices {

	public static final Logger logger = Logger.getLogger(DCIQuestionsServiceLoader.class);
	private String filepath = "";
	private Boolean generate;
	protected CustomerServices customerService;
	private DCIQuestionsPaths paths;
	// MBDP-2443 DCI Update wire transfer
	private String[] answersRefNumbers;
	// END MBDP-2443


	public void updateDCIQuestionsFile() {
		this.filepath = getPaths().getFilePath();
		File dir = new File(this.filepath);
		String[] listaArchivos = dir.list();
		// after deploy DCI VMs dont exist, flow has to take place
		if (listaArchivos.length == 1) {
			getDCIQuestions();
		} else {
			GlobalEntitlement dciQuestionsGlobalEntl = customerService
					.getGlobalEntitlement(EntitlementType.DCI_QUESTIONS);
			if (dciQuestionsGlobalEntl.getStatus().equals(EntitlementStatus.PENDING_UPDATE)) {
				getDCIQuestions();
				customerService.updateGlobalEntitlementStatus(dciQuestionsGlobalEntl, EntitlementStatus.UPDATED);
			}
		}
	}

	private void getDCIQuestions() {
		if (this.generate != null && this.generate) {
			File dir = new File(this.filepath);
			String[] listaArchivos = dir.list();
			if (listaArchivos != null) {
				for (int i = 0; i < listaArchivos.length; i++) {
					File f = new File(this.filepath + listaArchivos[i]);
					if (!f.getName().equals(".svn")) {// esta condicion es para
														// que
														// no tome en cuenta el
														// directorio .svn que
														// siempre hay en esta
														// carpeta...
						boolean resp = f.delete();
						if (resp == false)
							System.out.println("no se pudo borrar el archivo: " + f.getName());
					}
				}
			}

			try {
				List<DCIQuestion> questions = this.customerService.getDCIQuestions();
				List<DCIQuestionAnswer> qas = this.customerService.getDCIQuestionAnswer();

				String postExe = "\n<script type=\"text/javascript\">\nfunction display(id, value){";
				String hidden;
				String questionS = "";
				String questionE = "";
				String multiSelectsS = "";
				String multiSelectsE = "";
				String defaultLoadS = "";
				String defaultLoadE = "";
				String alternateLoadS = "";
				String alternateLoadE = "";
				String loadDefaultS = "";
				String loadDefaultE = "";
				for (int i = 0; i < questions.size(); i++) {
					DCIQuestion actual = questions.get(i);
					String sort = actual.getString(DCIQuestion.SORT_SPANISH);
					String display = actual.getString(DCIQuestion.ANSWER_TYPE);
					if (display.equals(DCIQuestion.ANSWER_SINGLE)) {
						hidden = "";
						Boolean depends = actual.getBoolean(DCIQuestion.DEPENDS_ON);
						if (depends) {
							questionS += "\n\n<div id=\"q"
									+ actual.getId()
									+ "\" style=\"display:none\">\n<table>\n<tr>"
									+ "\n<td width=\"250\"  style=\"padding-left: 5px;padding-bottom: 5px;padding-top: 5px;min-width:250px\">\n<label>"
									+ actual.getSpanish() + "</label>" + "\n<br/><span class=\"error\" id=\"eq"
									+ actual.getId() + "\"></span>" + "</td>";
							questionE += "\n\n<div id=\"q"
									+ actual.getId()
									+ "\" style=\"display:none\">\n<table>\n<tr>"
									+ "\n<td width=\"250\" style=\"padding-left: 5px;padding-bottom: 5px;padding-top: 5px;min-width:250px\">\n<label>"
									+ actual.getEnglish() + "</label>" + "\n<br/><span class=\"error\" id=\"eq"
									+ actual.getId() + "\"></span>" + "</td>";
						} else {
							questionS += "\n\n<div id=\"q"
									+ actual.getId()
									+ "\" >\n<table>\n<tr>\n<td width=\"250\" style=\"padding-left: 5px;padding-bottom: 5px;padding-top: 5px;min-width:250px\">"
									+ "\n<label>" + actual.getSpanish()
									+ "</label>\n<br/><span class=\"error\" id=\"eq" + actual.getId()
									+ "\"></span></td>";
							questionE += "\n\n<div id=\"q"
									+ actual.getId()
									+ "\" >\n<table>\n<tr>\n<td width=\"250\" style=\"padding-left: 5px;padding-bottom: 5px;padding-top: 5px;min-width:250px\">"
									+ "\n<label>" + actual.getEnglish()
									+ "</label>\n<br/><span class=\"error\" id=\"eq" + actual.getId()
									+ "\"></span></td>";
						}

						String otro = actual.getString(DCIQuestion.SHOW_OTHER);
						if (otro != null) {
							questionS += "\n<td width=\"450\" style=\"padding: 5px;\">" + "\n<select name=\"aq"
									+ actual.getId() + "\" " + "onchange=\"showOthers('" + actual.getId()
									+ "', this.options[this.selectedIndex].text);" + "hideDiv('hq" + actual.getId()
									+ "');display('q" + actual.getId()
									+ "',this.options[this.selectedIndex].value);\" " + "id=\"aq" + actual.getId()
									+ "\">";
							questionE += "\n<td width=\"450\" style=\"padding: 5px;\">" + "\n<select name=\"aq"
									+ actual.getId() + "\" " + "onchange=\"showOthers('" + actual.getId()
									+ "', this.options[this.selectedIndex].text);" + "hideDiv('hq" + actual.getId()
									+ "');display('q" + actual.getId()
									+ "',this.options[this.selectedIndex].value);\" " + "id=\"aq" + actual.getId()
									+ "\">";
						} else {
							questionS += "\n<td width=\"450\" style=\"padding: 5px;\">" + "\n<select name=\"aq"
									+ actual.getId() + "\" " + "onchange=\"hideDiv('hq" + actual.getId()
									+ "');display('q" + actual.getId()
									+ "',this.options[this.selectedIndex].value);\" " + "id=\"aq" + actual.getId()
									+ "\">";
							questionE += "\n<td width=\"450\" style=\"padding: 5px;\">" + "\n<select name=\"aq"
									+ actual.getId() + "\" " + "onchange=\"hideDiv('hq" + actual.getId()
									+ "');display('q" + actual.getId()
									+ "',this.options[this.selectedIndex].value);\" " + "id=\"aq" + actual.getId()
									+ "\">";
						}
						questionS += "\n<option value=\"null\" selected=\"selected\">Seleccione</option>";
						questionE += "\n<option value=\"null\" selected=\"selected\">Select</option>";
						// WEALTH CODES
						String alternate = actual.getString(DCIQuestion.ALTERNATE_ANSWER);
						String codes = null;
						if (alternate != null) {
							alternateLoadS += "\n\nfunction loadAlternateSelectValues" + actual.getId() + "(){"
									+ "\nvar select1 = document.getElementById('aq" + actual.getId() + "');";
							defaultLoadS += "\n\nfunction loadDefaultSelectValues" + actual.getId() + "(){"
									+ "\nvar select1 = document.getElementById('aq" + actual.getId() + "');";
							alternateLoadE += "\n\nfunction loadAlternateSelectValues" + actual.getId() + "(){"
									+ "\nvar select1 = document.getElementById('aq" + actual.getId() + "');";
							defaultLoadE += "\n\nfunction loadDefaultSelectValues" + actual.getId() + "(){"
									+ "\nvar select1 = document.getElementById('aq" + actual.getId() + "');";
							List<DCIQuestionAnswer> actualsE = this.getRama(actual, qas);
							List<DCIQuestionAnswer> actualsS = new ArrayList<DCIQuestionAnswer>();
							actualsS.addAll(actualsE);
							if (sort != null) {
								Collections.sort(actualsS, new Comparator<DCIQuestionAnswer>() {
									public int compare(DCIQuestionAnswer ans1, DCIQuestionAnswer ans2) {
										return ans1.getAnswer().getSpanish().compareTo(ans2.getAnswer().getSpanish());
									}
								});
							}
							for (int j = 0; j < actualsE.size(); j++) {
								if (actualsE.get(j).getAttributeData() != null
										&& actualsE.get(j).getString(DCIQuestionAnswer.ALTERNATE_CODE_ANSWER) != null) {
									codes = actualsE.get(j).getString(DCIQuestionAnswer.ALTERNATE_CODE_ANSWER);
									alternateLoadS += "\nvar opt = document.createElement('option');"
											+ "\nopt.innerHTML = '" + actualsS.get(j).getAnswer().getSpanish() + "';"
											+ "\nopt.value = '" + actualsS.get(j).getAnswer().getId() + "';"
											+ "\nselect1.appendChild(opt);";
									alternateLoadE += "\nvar opt = document.createElement('option');"
											+ "\nopt.innerHTML = '" + actualsE.get(j).getAnswer().getEnglish() + "';"
											+ "\nopt.value = '" + actualsE.get(j).getAnswer().getId() + "';"
											+ "\nselect1.appendChild(opt);";
								} else {
									defaultLoadS += "\nvar opt = document.createElement('option');"
											+ "\nopt.innerHTML = '" + actualsE.get(j).getAnswer().getSpanish() + "';"
											+ "\nopt.value = '" + actualsE.get(j).getAnswer().getId() + "';"
											+ "\nselect1.appendChild(opt);";
									defaultLoadE += "\nvar opt = document.createElement('option');"
											+ "\nopt.innerHTML = '" + actualsS.get(j).getAnswer().getEnglish() + "';"
											+ "\nopt.value = '" + actualsS.get(j).getAnswer().getId() + "';"
											+ "\nselect1.appendChild(opt);";
								}
								if (actualsE.get(j).getAttributeData() != null) {
									String sIndex = actualsE.get(j).getString(DCIQuestionAnswer.INDEX);
									if (sIndex != null) {
										postExe += "\nif(value=='" + actualsE.get(j).getAnswer().getId()
												+ "' && id=='q" + actual.getId() + "'){";
										Integer index = Integer.valueOf(sIndex);
										for (int k = 0; k < index; k++) {
											String nextQ = actualsE.get(j).getString(
													DCIQuestionAnswer.NEXT_QUESTION + (k + 1));
											if (nextQ != null) {
												postExe += "document.getElementById('q" + nextQ
														+ "').style.display=\"block\";";
												if (hidden.equals("")) {
													hidden += "q" + nextQ;
												} else {
													if (!hidden.contains(nextQ)) {
														hidden += "," + "q" + nextQ;
													}
												}
											}
										}
										postExe += "}";
									}
								}
							}
							alternateLoadS += "\n}";
							alternateLoadE += "\n}";
							defaultLoadS += "\n}";
							defaultLoadE += "\n}";
							questionS += "\n</select>";
							questionE += "\n</select>";
							loadDefaultS += "\n#if($codes == \"" + codes
									+ "\")\n<script type=\"text/javascript\">loadAlternateSelectValues"
									+ actual.getId() + "();</script>";
							loadDefaultS += "\n#else\n<script type=\"text/javascript\">loadDefaultSelectValues"
									+ actual.getId() + "();</script>#end";
							loadDefaultE += "\n#if($codes == \"" + codes
									+ "\")\n<script type=\"text/javascript\">loadAlternateSelectValues"
									+ actual.getId() + "();</script>";
							loadDefaultE += "\n#else\n<script type=\"text/javascript\">loadDefaultSelectValues"
									+ actual.getId() + "();</script>#end";
						} else {
							List<DCIQuestionAnswer> actualsE = this.getRama(actual, qas);
							List<DCIQuestionAnswer> actualsS = new ArrayList<DCIQuestionAnswer>();
							actualsS.addAll(actualsE);
							if (sort != null) {
								Collections.sort(actualsS, new Comparator<DCIQuestionAnswer>() {
									public int compare(DCIQuestionAnswer ans1, DCIQuestionAnswer ans2) {
										return ans1.getAnswer().getSpanish().compareTo(ans2.getAnswer().getSpanish());
									}
								});
							}
							for (int j = 0; j < actualsE.size(); j++) {
								questionS += "\n<option value=\"" + actualsS.get(j).getAnswer().getId() + "\">"
										+ actualsS.get(j).getAnswer().getSpanish() + "</option>";
								questionE += "\n<option value=\"" + actualsE.get(j).getAnswer().getId() + "\">"
										+ actualsE.get(j).getAnswer().getEnglish() + "</option>";

								if (actualsE.get(j).getAttributeData() != null) {
									String sIndex = actualsE.get(j).getString(DCIQuestionAnswer.INDEX);
									if (sIndex != null) {
										postExe += "\nif(value=='" + actualsE.get(j).getAnswer().getId()
												+ "' && id=='q" + actual.getId() + "'){";
										Integer index = Integer.valueOf(sIndex);
										for (int k = 0; k < index; k++) {
											String nextQ = actualsE.get(j).getString(
													DCIQuestionAnswer.NEXT_QUESTION + (k + 1));
											if (nextQ != null) {
												postExe += "document.getElementById('q" + nextQ
														+ "').style.display=\"block\";";
												if (hidden.equals("")) {
													hidden += "q" + nextQ;
												} else {
													if (!hidden.contains(nextQ)) {
														hidden += "," + "q" + nextQ;
													}
												}
											}
										}
										postExe += "}";
									}
								}
							}
							questionS += "\n</select>";
							questionE += "\n</select>";
						}
						if (actual.getSpanishTooltip() != null) {
							questionS += "<a class=\"icon-help-dci\" onmouseover=\""
									+ "Tip('"
									+ actual.getSpanishTooltip()
									+ "', "
									+ "BGCOLOR,'#E8F4F8', BORDERCOLOR,'#9BB9CF', WIDTH,140, ABOVE,true, OFFSETX,-17, FADEIN,150, FADEOUT,150, PADDING,8)\" "
									+ "onmouseout=\"UnTip()\"></a>";
							questionE += "<a class=\"icon-help-dci\" onmouseover=\""
									+ "Tip('"
									+ actual.getEnglishTooltip()
									+ "', "
									+ "BGCOLOR,'#E8F4F8', BORDERCOLOR,'#9BB9CF', WIDTH,140, ABOVE,true, OFFSETX,-17, FADEIN,150, FADEOUT,150, PADDING,8)\" "
									+ "onmouseout=\"UnTip()\"></a>";
						}
						if (!hidden.equals("")) {
							questionS += "\n<input type=\"hidden\" value=\"" + hidden + "\" id=\"hq" + actual.getId()
									+ "\"/>";
							questionE += "\n<input type=\"hidden\" value=\"" + hidden + "\" id=\"hq" + actual.getId()
									+ "\"/>";
						}

						questionS += "\n</td>\n</tr>\n</tr>\n<td style=\"padding-left: 5px;padding-bottom: 5px;padding-top: 5px;min-width:250px\">\n</td>"
								+ "\n<td width=\"450\" style=\"padding: 5px;\">"
								+ "\n<textarea style=\"display: none;\" name=\"oq"
								+ actual.getId()
								+ "\" id=\"oq"
								+ actual.getId()
								+ "\" maxlength=\"255\" autocomplete=\"off\" rows=\"4\" cols=\"55\"></textarea>";
						questionS += "\n</td>\n</tr>\n</table>\n</div>";

						questionE += "\n</td>\n</tr>\n</tr>\n<td style=\"padding-left: 5px;padding-bottom: 5px;padding-top: 5px;min-width:250px\">\n</td>"
								+ "\n<td width=\"450\" style=\"padding: 5px;\">"
								+ "\n<textarea style=\"display: none;\" name=\"oq"
								+ actual.getId()
								+ "\" id=\"oq"
								+ actual.getId()
								+ "\" maxlength=\"255\" autocomplete=\"off\" rows=\"4\" cols=\"55\"></textarea>";
						questionE += "\n</td>\n</tr>\n</table>\n</div>";

					} else if (display.equals(DCIQuestion.ANSWER_CHECKBOX)) {
						List<DCIQuestionAnswer> actuals = this.getRama(actual, qas);
						Boolean depends = actual.getBoolean(DCIQuestion.DEPENDS_ON);
						if (depends) {
							questionS += "\n\n<div id=\"q" + actual.getId()
									+ "\" style=\"display:none\">\n<table width=\"700\">\n<tr>" + "\n<td colspan=\""
									+ actuals.size() + "\" style=\"padding: 5px;\">\n<label>" + actual.getSpanish()
									+ "</label>" + "\n<br/><span class=\"error\" id=\"eq" + actual.getId()
									+ "\"></span>" + "\n</td>\n</tr>\n<tr>";
							questionE += "\n\n<div id=\"q" + actual.getId()
									+ "\" style=\"display:none\">\n<table width=\"700\">\n<tr>" + "\n<td colspan=\""
									+ actuals.size() + "\" style=\"padding: 5px;\">\n<label>" + actual.getEnglish()
									+ "</label>" + "\n<br/><span class=\"error\" id=\"eq" + actual.getId()
									+ "\"></span>" + "\n</td>\n</tr>\n<tr>";
						} else {
							questionS += "\n\n<div id=\"q" + actual.getId() + "\" >\n<table width=\"700\">\n<tr>\n"
									+ "<td colspan=\"" + actuals.size() + "\" style=\"padding: 5px;\">\n<label>"
									+ actual.getSpanish() + "</label>\n" + "\n<br/><span class=\"error\" id=\"eq"
									+ actual.getId() + "\"></span></td>\n</tr>\n<tr>";
							questionE += "\n\n<div id=\"q" + actual.getId() + "\" >\n<table width=\"700\">\n<tr>\n"
									+ "<td colspan=\"" + actuals.size() + "\" style=\"padding: 5px;\">\n<label>"
									+ actual.getEnglish() + "</label>\n" + "\n<br/><span class=\"error\" id=\"eq"
									+ actual.getId() + "\"></span></td>\n</tr>\n<tr>";
						}
						for (int j = 0; j < actuals.size(); j++) {
							String otro = actual.getString(DCIQuestion.SHOW_OTHER);
							if (otro != null
									&& (actuals.get(j).getAnswer().getSpanish().equalsIgnoreCase("otro") || actuals
											.get(j).getAnswer().getEnglish().equalsIgnoreCase("other"))) {
								questionS += "\n<td>\n<table><tr><td><input type=\"checkbox\" name=\"cb"
										+ actual.getId() + "\" " + "value=\"" + actuals.get(j).getAnswer().getId()
										+ "\" " + "onClick=\"if(this.checked){showOthers('" + actual.getId() + "', '"
										+ actuals.get(j).getAnswer().getSpanish() + "');}else{showOthers('"
										+ actual.getId() + "', 'null');}\"/></td><td>"
										+ actuals.get(j).getAnswer().getSpanish();
								questionS += "\n<textarea maxlength=\"255\" style=\"display:none\" name=\"oq"
										+ actual.getId() + "\" id=\"oq" + actual.getId()
										+ "\" utocomplete=\"off\" rows=\"6\" cols=\"30\"></textarea>";
								questionE += "\n<td>\n<table><tr><td><input type=\"checkbox\" name=\"cb"
										+ actual.getId() + "\" " + "value=\"" + actuals.get(j).getAnswer().getId()
										+ "\" " + "onClick=\"if(this.checked){showOthers('" + actual.getId() + "', '"
										+ actuals.get(j).getAnswer().getEnglish() + "');}else{showOthers('"
										+ actual.getId() + "', 'null');}\"/></td><td>"
										+ actuals.get(j).getAnswer().getEnglish();
								questionE += "\n<textarea maxlength=\"255\" style=\"display:none\" name=\"oq"
										+ actual.getId() + "\" id=\"oq" + actual.getId()
										+ "\" utocomplete=\"off\" rows=\"6\" cols=\"30\"></textarea>";
							} else {
								questionS += "\n<td>\n<table><tr><td><input type=\"checkbox\" name=\"cb"
										+ actual.getId() + "\" " + "value=\"" + actuals.get(j).getAnswer().getId()
										+ "\"";
								questionE += "\n<td>\n<table><tr><td><input type=\"checkbox\" name=\"cb"
										+ actual.getId() + "\" " + "value=\"" + actuals.get(j).getAnswer().getId()
										+ "\"";

								if (actuals.get(j).getAttributeData() != null) {
									String sIndex = actuals.get(j).getString(DCIQuestionAnswer.INDEX);
									if (sIndex != null) {
										Integer index = Integer.valueOf(sIndex);
										for (int k = 0; k < index; k++) {
											String nextQ = actuals.get(j).getString(
													DCIQuestionAnswer.NEXT_QUESTION + (k + 1));
											if (nextQ != null) {
												questionS += " onClick=\"if(this.checked){" + "display('q"
														+ actual.getId() + "','" + actuals.get(j).getAnswer().getId()
														+ "');}" + "else{hideDiv('hqc" + nextQ + "');}\"/></td><td>"
														+ actuals.get(j).getAnswer().getSpanish();
												questionS += "\n<input type=\"hidden\" value=\"q" + nextQ + "\" "
														+ "id=\"hqc" + nextQ + "\"/>";
												questionE += " onClick=\"if(this.checked){" + "display('q"
														+ actual.getId() + "','" + actuals.get(j).getAnswer().getId()
														+ "');}" + "else{hideDiv('hqc" + nextQ + "');}\"/></td><td>"
														+ actuals.get(j).getAnswer().getEnglish();
												questionE += "\n<input type=\"hidden\" value=\"q" + nextQ + "\" "
														+ "id=\"hqc" + nextQ + "\"/>";
											}
										}
									} else {
										questionS += " /></td><td>" + actuals.get(j).getAnswer().getSpanish();
										questionE += " /></td><td>" + actuals.get(j).getAnswer().getEnglish();
									}
								} else {
									questionS += " /></td><td>" + actuals.get(j).getAnswer().getSpanish();
									questionE += " /></td><td>" + actuals.get(j).getAnswer().getEnglish();
								}
							}

							if (j != actuals.size() - 1) {
								questionS += "\n</td></tr></table></td>";
								questionE += "\n</td></tr></table></td>";
							}

							if (actuals.get(j).getAttributeData() != null) {
								String sIndex = actuals.get(j).getString(DCIQuestionAnswer.INDEX);
								if (sIndex != null) {
									Integer index = Integer.valueOf(sIndex);
									for (int k = 0; k < index; k++) {
										String nextQ = actuals.get(j).getString(
												DCIQuestionAnswer.NEXT_QUESTION + (k + 1));
										if (nextQ != null) {
											postExe += "\nif(value=='" + actuals.get(j).getAnswer().getId()
													+ "' && id=='q" + actual.getId() + "'){"
													+ "document.getElementById('q" + nextQ
													+ "').style.display=\"block\";" + "}";
										}
									}
								}
							}
						}
						if (actual.getSpanishTooltip() != null) {
							questionS += "<a class=\"icon-help-dci\" onmouseover=\""
									+ "Tip('"
									+ actual.getSpanishTooltip()
									+ "', "
									+ "BGCOLOR,'#E8F4F8', BORDERCOLOR,'#9BB9CF', WIDTH,140, ABOVE,true, OFFSETX,-17, FADEIN,150, FADEOUT,150, PADDING,8)\" "
									+ "onmouseout=\"UnTip()\"></a>";
							questionE += "<a class=\"icon-help-dci\" onmouseover=\""
									+ "Tip('"
									+ actual.getEnglishTooltip()
									+ "', "
									+ "BGCOLOR,'#E8F4F8', BORDERCOLOR,'#9BB9CF', WIDTH,140, ABOVE,true, OFFSETX,-17, FADEIN,150, FADEOUT,150, PADDING,8)\" "
									+ "onmouseout=\"UnTip()\"></a>";
						}
						questionS += "\n</td></tr></table></td>\n</tr>\n</table>\n</div>";
						questionE += "\n</td></tr></table></td>\n</tr>\n</table>\n</div>";

					} else if (display.equals(DCIQuestion.ANSWER_MULTIPLE)) {
						hidden = "";

						multiSelectsS += "\n\nvar ms" + actual.getId() + " = " + "new MSDList('aq" + actual.getId()
								+ "', 'Todos', 'Filtrar', 'Seleccione', 'Todos seleccionados');" + "\nms"
								+ actual.getId() + ".render();";

						multiSelectsE += "\n\nvar ms" + actual.getId() + " = " + "new MSDList('aq" + actual.getId()
								+ "', 'All', 'Filter', 'Select', 'All selected');" + "\nms" + actual.getId()
								+ ".render();";
						
						//gdiaz et56356
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE3)){
							multiSelectsS += "\nms" + actual.getId() + ".unselectAllItems();";
							multiSelectsE += "\nms" + actual.getId() + ".unselectAllItems();";
						}
						
						Boolean depends = actual.getBoolean(DCIQuestion.DEPENDS_ON);
						if (depends) {
							questionS += "\n\n<div id=\"q"
									+ actual.getId()
									+ "\" style=\"display:none\">\n<table>\n<tr>\n<td width=\"250\"  style=\"padding-left: 5px;padding-bottom: 5px;padding-top: 5px;min-width:250px\">\n"
									+ "<label>" + actual.getSpanish() + "</label>"
									+ "\n<br/><span class=\"error\" id=\"eq" + actual.getId() + "\"></span>" + "</td>";
							questionE += "\n\n<div id=\"q"
									+ actual.getId()
									+ "\" style=\"display:none\">\n<table>\n<tr>\n<td width=\"250\"  style=\"padding-left: 5px;padding-bottom: 5px;padding-top: 5px;min-width:250px\">\n"
									+ "<label>" + actual.getEnglish() + "</label>"
									+ "\n<br/><span class=\"error\" id=\"eq" + actual.getId() + "\"></span>" + "</td>";
						} else {
							questionS += "\n\n<div id=\"q"
									+ actual.getId()
									+ "\" >\n<table>\n<tr>\n<td width=\"250\"  style=\"padding-left: 5px;padding-bottom: 5px;padding-top: 5px;min-width:250px\">"
									+ "\n<label>" + actual.getSpanish()
									+ "</label>\n<br/><span class=\"error\" id=\"eq" + actual.getId()
									+ "\"></span></td>";
							questionE += "\n\n<div id=\"q"
									+ actual.getId()
									+ "\" >\n<table>\n<tr>\n<td width=\"250\"  style=\"padding-left: 5px;padding-bottom: 5px;padding-top: 5px;min-width:250px\">"
									+ "\n<label>" + actual.getEnglish()
									+ "</label>\n<br/><span class=\"error\" id=\"eq" + actual.getId()
									+ "\"></span></td>";
						}
						questionS += "\n<td width=\"450\"  style=\"padding: 5px;\">" + "\n<select name=\"aq"
								+ actual.getId() + "\" " + "onchange=\"showOthers('" + actual.getId()
								+ "', this.options[this.selectedIndex].text);" + "hideDiv('hq" + actual.getId()
								+ "');display('q" + actual.getId() + "',this.options[this.selectedIndex].value);\" "
								+ "id=\"aq" + actual.getId() + "\">";
						questionE += "\n<td width=\"450\"  style=\"padding: 5px;\">" + "\n<select name=\"aq"
								+ actual.getId() + "\" " + "onchange=\"showOthers('" + actual.getId()
								+ "', this.options[this.selectedIndex].text);" + "hideDiv('hq" + actual.getId()
								+ "');display('q" + actual.getId() + "',this.options[this.selectedIndex].value);\" "
								+ "id=\"aq" + actual.getId() + "\">";
						List<DCIQuestionAnswer> actuals = this.getRama(actual, qas);
						for (int j = 0; j < actuals.size(); j++) {
							questionS += "\n<option value=\"" + actuals.get(j).getAnswer().getId() + "\">"
									+ actuals.get(j).getAnswer().getSpanish() + "</option>";
							questionE += "\n<option value=\"" + actuals.get(j).getAnswer().getId() + "\">"
									+ actuals.get(j).getAnswer().getEnglish() + "</option>";

							if (actuals.get(j).getAttributeData() != null) {
								String sIndex = actuals.get(j).getString(DCIQuestionAnswer.INDEX);
								if (sIndex != null) {
									postExe += "\nif(value=='" + actuals.get(j).getAnswer().getId() + "' && id=='q"
											+ actual.getId() + "'){";
									Integer index = Integer.valueOf(sIndex);
									for (int k = 0; k < index; k++) {
										String nextQ = actuals.get(j).getString(
												DCIQuestionAnswer.NEXT_QUESTION + (k + 1));
										if (nextQ != null) {
											postExe += "document.getElementById('q" + nextQ
													+ "').style.display=\"block\";";
											if (hidden.equals("")) {
												hidden += "q" + nextQ;
											} else {
												if (!hidden.contains(nextQ)) {
													hidden += "," + "q" + nextQ;
												}
											}
										}
									}
									postExe += "}";
								}
							}
						}
						questionS += "\n</select>";
						questionE += "\n</select>";
						if (actual.getSpanishTooltip() != null) {
							questionS += "<a class=\"icon-help-dci-dci\" onmouseover=\""
									+ "Tip('"
									+ actual.getSpanishTooltip()
									+ "', "
									+ "BGCOLOR,'#E8F4F8', BORDERCOLOR,'#9BB9CF', WIDTH,140, ABOVE,true, OFFSETX,-17, FADEIN,150, FADEOUT,150, PADDING,8)\" "
									+ "onmouseout=\"UnTip()\"></a>";
							questionE += "<a class=\"icon-help-dci-dci\" onmouseover=\""
									+ "Tip('"
									+ actual.getEnglishTooltip()
									+ "', "
									+ "BGCOLOR,'#E8F4F8', BORDERCOLOR,'#9BB9CF', WIDTH,140, ABOVE,true, OFFSETX,-17, FADEIN,150, FADEOUT,150, PADDING,8)\" "
									+ "onmouseout=\"UnTip()\"></a>";
						}
						if (!hidden.equals("")) {
							questionS += "\n<input type=\"hidden\" value=\"" + hidden + "\" id=\"hq" + actual.getId()
									+ "\"/>";
							questionE += "\n<input type=\"hidden\" value=\"" + hidden + "\" id=\"hq" + actual.getId()
									+ "\"/>";
						}
						questionS += "\n<input maxlength=\"255\" style=\"display:none\" type=\"text\" name=\"oq"
								+ actual.getId() + "\" id=\"oq" + actual.getId() + "\"/>";
						questionS += "\n</td>\n</tr>\n</table>\n</div>";

						questionE += "\n<input maxlength=\"255\" style=\"display:none\" type=\"text\" name=\"oq"
								+ actual.getId() + "\" id=\"oq" + actual.getId() + "\"/>";
						questionE += "\n</td>\n</tr>\n</table>\n</div>";
					}

				}

				postExe += "\n}" + "\n\nfunction hideDiv(id){" + "\nif(document.getElementById(id)){"
						+ "\nvar qdiv = document.getElementById(id).value;" + "\nvar qdivs = qdiv.split(\",\");"
						+ "\nfor(var i = 0; i < qdivs.length; i++){" + "\nvar qdiv1 = qdivs[i];"
						+ "\nif(document.getElementById(qdiv1)){"
						+ "\ndocument.getElementById(qdiv1).style.display=\"none\";" +

						"\nif(document.getElementById('a'+qdiv1)){"
						+ "\nif(document.getElementById('a'+qdiv1).tagName.toUpperCase()=='SELECT'){"
						+ "\ndocument.getElementById('a'+qdiv1).selectedIndex = 0;" + "\n}"
						+ "\nif(document.getElementById('a'+qdiv1).tagName.toUpperCase()=='INPUT'){"
						+ "\nif(document.getElementById('alla'+qdiv1)){"
						+ "\ndocument.getElementById('alla'+qdiv1).checked = true;"
						+ "\ndocument.getElementById('alla'+qdiv1).click();" + "\n}" + "\n}" + "\n}" +

						"\nvar nqid = 'h'+qdiv1;" + "\nif(document.getElementById(nqid)){" + "\nhideDiv(nqid);}}}}"
						+ "\n}" +

						"\n\nfunction showOthers(id, value){" + "\nvar nid = 'oq'+id;"
						+ "\nif(value.toUpperCase()=='OTHER' || value.toUpperCase()=='OTRO'){"
						+ "\ndocument.getElementById(nid).value = '';"
						+ "\ndocument.getElementById(nid).style.display = 'inline';" + "\n}" + "\nelse{"
						+ "\ndocument.getElementById(nid).value = ''"
						+ "\ndocument.getElementById(nid).style.display = 'none'" + "\n}" + "\n}";

				questionS += postExe + multiSelectsS + alternateLoadS + defaultLoadS + "\n</script>";
				questionE += postExe + multiSelectsE + alternateLoadE + defaultLoadE + "\n</script>";

				questionS += "\n\n" + loadDefaultS;
				questionE += "\n\n" + loadDefaultE;

				writeFile(filepath + "dciQuestionsEnglish.vm", questionE);
				writeFile(filepath + "dciQuestionsSpanish.vm", questionS);

			} catch (Exception e) {
				logger.error("Error al crear lso archivos de DCI: " + e.getMessage());
			}
		}
	}

	private void writeFile(String fileName, String data) {

		OutputStream os = null;
		PrintWriter out = null;
		try {
			// System.out.println(logData);
			File log = new File(fileName);
			if (log.exists()) {
				os = new FileOutputStream(log);
				out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
				// PrintWriter out = new PrintWriter(new BufferedWriter(new
				// FileWriter(log, false)).);
				out.write(data);
				out.flush();
				out.close();
			} else {
				os = new FileOutputStream(log);
				out = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
				// PrintWriter out = new PrintWriter(new BufferedWriter(new
				// FileWriter(log)));
				out.write(data);
				out.flush();
				out.close();
			}
		} catch (Exception e) {
			logger.error("Error al crear lso archivos de DCI: " + e.getMessage());
		}finally {
			if(os != null ) {
				try {
					os.close();
				} catch (IOException ex) {
					// TODO Auto-generated catch block
					logger.error("Error cerrar el :OutputStream os  " + ex.getMessage());
				}
			}
			if(out != null) {
				out.close();
			}
		}
	}

	public List<DCIQuestionAnswer> getRama(DCIQuestion actual, List<DCIQuestionAnswer> questions) {
		List<DCIQuestionAnswer> resp = new ArrayList<DCIQuestionAnswer>();
		for (int i = 0; i < questions.size(); i++) {
			// MBDP-2443 DCI Update wire transfer
			boolean breakAllLoop = false;
			for (String answersRefNumber : answersRefNumbers) {
				if (answersRefNumber.equals(questions.get(i).getAnswer().getAnswerRefNum().toString())) {
					break;
				}
				if (questions.get(i).getQuestion().equals(actual)) {
					resp.add(questions.get(i));
					questions.remove(i);
					i--;
					break;
				} else {
					if (!resp.isEmpty()) {
						breakAllLoop = true;
						break;
					}
				}
			}
			if(breakAllLoop) {
			break;	
			}	
			// END MBDP-2443
		}
		return resp;
	}

	public String getFilepath() {
		if (filepath != null)
			return filepath.trim();

		return filepath;
	}

	public void setFilepath(String filepath) {
		if (filepath != null)
			filepath = filepath.trim();
		this.filepath = filepath;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public void setPaths(DCIQuestionsPaths paths) {
		this.paths = paths;
	}

	public DCIQuestionsPaths getPaths() {
		return paths;
	}

	public Boolean getGenerate() {
		return generate;
	}

	public void setGenerate(Boolean generate) {
		this.generate = generate;
	}

	//MBDP-2443 DCI Update wire transfer
	public String[] getAnswersRefNumbers() {
		return answersRefNumbers;
	}

	public void setAnswersRefNumbers(String[] answersRefNumbers) {
		this.answersRefNumbers = answersRefNumbers;
	}
	//END MBDP-2443
	
}
