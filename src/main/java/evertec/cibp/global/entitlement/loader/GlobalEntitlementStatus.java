package evertec.cibp.global.entitlement.loader;

import java.util.EnumMap;
import java.util.Map;

import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;

/**
 * 
 * @author Michael Ortiz - ET56883
 * 
 * GlobalEntitlementStatus is a single instance class that will give the status 
 * of the entitlements set by the GlobalEntitlementLoader cron job every 30 seconds.
 *
 */
public class GlobalEntitlementStatus {
	
    /**globalEntitlements **/
	private static EnumMap<EntitlementType, GlobalEntitlement> globalEntitlements;
	
	private GlobalEntitlementStatus() {
	  
	}

	/**
	 * @author Michael Ortiz - ET56883
	 * 
	 * @param entitlementType enum
	 * @return returns true if the entitlement in the database is not null and ENABLED.
	 * 
	 */
	public static boolean isGlobalEntitlementEnabled(EntitlementType entitlementType) {
		// Returns default true until quartz cron job makes first call
		if (globalEntitlements == null) {
			return true;
		}
		
		GlobalEntitlement globalEntitlement = globalEntitlements.get(entitlementType);
		
		return globalEntitlement != null && globalEntitlement.getStatus() != null 
				&& globalEntitlement.getStatus() == EntitlementStatus.ENABLED;
	}
	
	
	/**
	 * @author Michael Ortiz - ET56883
	 * @param entitlementType enum
	 * @return returns global entitlement object from the database.
	 */
	public static GlobalEntitlement getGlobalEntitlement(EntitlementType entitlementType) {
		
		if (globalEntitlements == null || globalEntitlements.get(entitlementType) == null) {
			return new GlobalEntitlement();
		}
		
		return globalEntitlements.get(entitlementType);
	}

	public static void setGlobalEntitlements(Map<EntitlementType, GlobalEntitlement> entitlements) {
		globalEntitlements = (EnumMap<EntitlementType, GlobalEntitlement>) entitlements;
	}
}