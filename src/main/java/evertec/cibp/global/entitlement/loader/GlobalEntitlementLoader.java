package evertec.cibp.global.entitlement.loader;

import java.util.EnumMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;

/**
 * 
 * @author Michael Ortiz - ET56883
 * 
 * GlobalEntitlementLoader will execute by a cron job every 60 seconds
 * to fetching the real time values of the global entitlements and assigning it 
 * to a single instance class that can be accessed within the cibp-web project.
 * 
 * Due to the Global_Entitlements table being cached by hibernate, this job has been
 * added to prevent the database from being hit constantly to get the real time status 
 * of the entitlement and prevent a delays when getting the status when requiring a downtime.
 * 
 */

public class GlobalEntitlementLoader {

	protected final Log logger = LogFactory.getLog(getClass());
	
	protected CustomerServices customerService;

	public void executeGlobalEntitlementLoader() {
	
		try {
			
			EnumMap<EntitlementType, GlobalEntitlement> map = new EnumMap<>(EntitlementType.class);
			
			for (GlobalEntitlement globalEntitlement : customerService.getGlobalEntitlements()) {
				
				if (globalEntitlement == null || globalEntitlement.getType() == null) {
					continue;
				}
				
				map.put(globalEntitlement.getType(), globalEntitlement);
			}
			
			GlobalEntitlementStatus.setGlobalEntitlements(map);
			
		} catch (Exception e) {
			logger.error("Cron Job: Could not refresh global entitlements from the database. Using entitlements in memory.");
		}
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}
	
}