package evertec.cibp.web.response;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

public class TsysLoyaltyRewardsRedemptionResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5213847707710815995L;

	private String redemptionDate = null;

	private RewardsCurrencyAmount rewardsBalance = null;

	private String confirmationNumber = null;

	private Map<String, Object> identifiers = null;

	private CurrencyAmount cashbackAmount = null;

	/**
	 * Date when the redemption was made.
	 * 
	 * @return redemptionDate
	 **/
	public String getRedemptionDate() {
		return redemptionDate;
	}

	public void setRedemptionDate(String redemptionDate) {
		this.redemptionDate = redemptionDate;
	}

	/**
	 * Get rewardsBalance
	 * 
	 * @return rewardsBalance
	 **/
	public RewardsCurrencyAmount getRewardsBalance() {
		return rewardsBalance;
	}

	public void setRewardsBalance(RewardsCurrencyAmount rewardsBalance) {
		this.rewardsBalance = rewardsBalance;
	}

	/**
	 * Number indicating a successful redemption.
	 * 
	 * @return confirmationNumber
	 **/
	public String getConfirmationNumber() {
		return confirmationNumber;
	}

	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}

	/**
	 * Returns the path parameter(s) which will contain the accountId
	 * 
	 * @return identifiers
	 **/
	public Map<String, Object> getIdentifiers() {
		return identifiers;
	}

	public void setIdentifiers(Map<String, Object> identifiers) {
		this.identifiers = identifiers;
	}

	/**
	 * Get cashbackAmount
	 * 
	 * @return cashbackAmount
	 **/
	public CurrencyAmount getCashbackAmount() {
		return cashbackAmount;
	}

	public void setCashbackAmount(CurrencyAmount cashbackAmount) {
		this.cashbackAmount = cashbackAmount;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TsysLoyaltyRewardsRedemptionResponse directDepositResponse = (TsysLoyaltyRewardsRedemptionResponse) o;
		return Objects.equals(this.redemptionDate, directDepositResponse.redemptionDate)
				&& Objects.equals(this.rewardsBalance, directDepositResponse.rewardsBalance)
				&& Objects.equals(this.confirmationNumber, directDepositResponse.confirmationNumber)
				&& Objects.equals(this.identifiers, directDepositResponse.identifiers)
				&& Objects.equals(this.cashbackAmount, directDepositResponse.cashbackAmount);
	}

	@Override
	public int hashCode() {
		return Objects.hash(redemptionDate, rewardsBalance, confirmationNumber, identifiers, cashbackAmount);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class DirectDepositResponse {\n");
		sb.append("    redemptionDate: ").append(toIndentedString(redemptionDate)).append("\n");
		sb.append("    rewardsBalance: ").append(toIndentedString(rewardsBalance)).append("\n");
		sb.append("    confirmationNumber: ").append(toIndentedString(confirmationNumber)).append("\n");
		sb.append("    identifiers: ").append(toIndentedString(identifiers)).append("\n");
		sb.append("    cashbackAmount: ").append(toIndentedString(cashbackAmount)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	public static class CurrencyAmount {
		/**
		 * Currency code for the monetary amount.
		 */
		public enum CurrencyEnum {
			
			USD("USD");

			private String value;

			CurrencyEnum(String value) {
				this.value = value;
			}

			public String getValue() {
				return value;
			}

			@Override
			public String toString() {
				return String.valueOf(value);
			}
		}

		private CurrencyEnum currency = null;

		private String value = null;

		/**
		 * Currency code for the monetary amount.
		 * 
		 * @return currency
		 **/
		public CurrencyEnum getCurrency() {
			return currency;
		}

		public void setCurrency(CurrencyEnum currency) {
			this.currency = currency;
		}

		/**
		 * Monetary amount.
		 * 
		 * @return value
		 **/
		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		@Override
		public boolean equals(java.lang.Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			CurrencyAmount currencyAmount = (CurrencyAmount) o;
			return Objects.equals(this.currency, currencyAmount.currency)
					&& Objects.equals(this.value, currencyAmount.value);
		}

		@Override
		public int hashCode() {
			return Objects.hash(currency, value);
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("class CurrencyAmount {\n");
			sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
			sb.append("    value: ").append(toIndentedString(value)).append("\n");
			sb.append("}");
			return sb.toString();
		}

		/**
		 * Convert the given object to string with each line indented by 4 spaces
		 * (except the first line).
		 */
		private String toIndentedString(java.lang.Object o) {
			if (o == null) {
				return "null";
			}
			return o.toString().replace("\n", "\n    ");
		}
	}
}
