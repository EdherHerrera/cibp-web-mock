/**
 * 
 */
package evertec.cibp.web.response;

import evertec.cibp.web.enums.RecurringCashBackType;

import java.io.Serializable;

/**
 * Response object for creditcards-service jar
 *
 * @author Stephanie Diaz Ocasio
 * @since 1.0
 * @version 1.0 
 * @see Serializable
 */

public class TLRRSCreditResponse implements Serializable {

	/**
	 * serial UID
	 */
    private static final long serialVersionUID = -2293408015854852189L; // serial

    /**
     * Minimum rewards needed in an account before a recurring statement credit will be completed.
     */
    private RewardsCurrencyAmount redemptionValue; //

    /**
     * Container for the amount of rewards the customer has on their account.
     */
    private RewardsCurrencyAmount rewardsBalance; //

    /**
     * The item the customer has chosen to spend their rewards on.
     */
    private String itemCode; //

    /**
     * Recurring CashBack Type String Response
     * @see TLRRSCreditRequest.RecurringCashBackType
     */
    private RecurringCashBackType recurringCashBackType; //

    /**
     * Contain the account information.
     */
    private AccountIdentifier identifiers; //

    /**
     * @return current redemptionValue
     */
    public RewardsCurrencyAmount getRedemptionValue () {
        return redemptionValue;
    }

    /**
     *
     * @param redemptionValue redemptionValue to set
     */
    public void setRedemptionValue (RewardsCurrencyAmount redemptionValue) {
        this.redemptionValue = redemptionValue;
    }

    /**
     * @return current rewardsBalance
     */
    public RewardsCurrencyAmount getRewardsBalance () {
        return rewardsBalance;
    }

    /**
     * @param rewardsBalance rewardsBalance to set
     */
    public void setRewardsBalance (RewardsCurrencyAmount rewardsBalance) {
        this.rewardsBalance = rewardsBalance;
    }

    /**
     * @return current itemCode
     */
    public String getItemCode () {
        return itemCode;
    }

    /**
     * @param strItemCode itemCode to set
     */
    public void setItemCode (String strItemCode) {
        this.itemCode = strItemCode;
    }

    /**
     * @return current recurringCashBackType
     */
    public RecurringCashBackType getRecurringCashBackType () {
        return recurringCashBackType;
    }

    /**
     * @param recurringCashBackType recurringCashBackType to set
     */
    public void setRecurringCashBackType (RecurringCashBackType recurringCashBackType) {
        this.recurringCashBackType = recurringCashBackType;
    }

    /**
     * @return current identifiers
     */
    public AccountIdentifier getIdentifiers () {
        return identifiers;
    }

    /**
     * @param identifiers identifier to set
     */
    public void setIdentifiers (AccountIdentifier identifiers) {
        this.identifiers = identifiers;
    } 
}
