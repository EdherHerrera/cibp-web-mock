package evertec.cibp.web.response;

import java.util.Objects;

public class RewardsCurrencyAmount {
	private String rewardscurrency = "";

	private String value = "";

	/**
	 * Currency code for the reward balance. Posible values: POINTS, REWARDS, CASH.
	 * For Cash back (VPCBK) CCA the value should be CASH.
	 * 
	 * @return rewardscurrency
	 **/
	public String getRewardscurrency() {
		return rewardscurrency;
	}

	public void setRewardscurrency(String rewardscurrency) {
		this.rewardscurrency = rewardscurrency;
	}

	/**
	 * Get value
	 * 
	 * @return value
	 **/
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		RewardsCurrencyAmount rewardsCurrencyAmount = (RewardsCurrencyAmount) o;
		return Objects.equals(this.rewardscurrency, rewardsCurrencyAmount.rewardscurrency)
				&& Objects.equals(this.value, rewardsCurrencyAmount.value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(rewardscurrency, value);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class RewardsCurrencyAmount {\n");

		sb.append("    rewardscurrency: ").append(toIndentedString(rewardscurrency)).append("\n");
		sb.append("    value: ").append(toIndentedString(value)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
