package evertec.cibp.web.response;

import java.math.BigDecimal;
import java.util.Calendar;

public class Payment {
	
    private AccountExt accountFrom;

	private AccountExt accountTo;

	private String paymentId;

	private String payeeId;

	private String payeeName;

	private String payeeNickname;

	private String referenceNumber;

	private BigDecimal amount;

	private Calendar processingDate;

	private Calendar effectiveDate;

	private String statusCode;

	private String rejectCode;

	private String retries;

	private String employeeId;

	private String employeeName;

	private String vruIdCode;

	private String frontEndId;

	private String billerNumber;

	private boolean reverseable;
    
	/**
	 * @return the accountFrom
	 */
	public AccountExt getAccountFrom() {
		return accountFrom;
	}
	/**
	 * @param accountFrom the accountFrom to set
	 */
	public void setAccountFrom(AccountExt accountFrom) {
		this.accountFrom = accountFrom;
	}
	/**
	 * @return the accountTo
	 */
	public AccountExt getAccountTo() {
		return accountTo;
	}
	/**
	 * @param accountTo the accountTo to set
	 */
	public void setAccountTo(AccountExt accountTo) {
		this.accountTo = accountTo;
	}
	/**
	 * @return the paymentId
	 */
	public String getPaymentId() {
		return paymentId;
	}
	/**
	 * @param paymentId the paymentId to set
	 */
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	/**
	 * @return the payeeId
	 */
	public String getPayeeId() {
		return payeeId;
	}
	/**
	 * @param payeeId the payeeId to set
	 */
	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}
	/**
	 * @return the payeeName
	 */
	public String getPayeeName() {
		return payeeName;
	}
	/**
	 * @param payeeName the payeeName to set
	 */
	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}
	/**
	 * @return the payeeNickname
	 */
	public String getPayeeNickname() {
		return payeeNickname;
	}
	/**
	 * @param payeeNickname the payeeNickname to set
	 */
	public void setPayeeNickname(String payeeNickname) {
		this.payeeNickname = payeeNickname;
	}
	/**
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}
	/**
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * @return the processingDate
	 */
	public Calendar getProcessingDate() {
		return processingDate;
	}
	/**
	 * @param processingDate the processingDate to set
	 */
	public void setProcessingDate(Calendar processingDate) {
		this.processingDate = processingDate;
	}
	/**
	 * @return the effectiveDate
	 */
	public Calendar getEffectiveDate() {
		return effectiveDate;
	}
	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(Calendar effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @return the rejectCode
	 */
	public String getRejectCode() {
		return rejectCode;
	}
	/**
	 * @param rejectCode the rejectCode to set
	 */
	public void setRejectCode(String rejectCode) {
		this.rejectCode = rejectCode;
	}
	/**
	 * @return the retries
	 */
	public String getRetries() {
		return retries;
	}
	/**
	 * @param retries the retries to set
	 */
	public void setRetries(String retries) {
		this.retries = retries;
	}
	/**
	 * @return the employeeId
	 */
	public String getEmployeeId() {
		return employeeId;
	}
	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	/**
	 * @return the employeeName
	 */
	public String getEmployeeName() {
		return employeeName;
	}
	/**
	 * @param employeeName the employeeName to set
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	/**
	 * @return the vruIdCode
	 */
	public String getVruIdCode() {
		return vruIdCode;
	}
	/**
	 * @param vruIdCode the vruIdCode to set
	 */
	public void setVruIdCode(String vruIdCode) {
		this.vruIdCode = vruIdCode;
	}
	/**
	 * @return the frontEndId
	 */
	public String getFrontEndId() {
		return frontEndId;
	}
	/**
	 * @param frontEndId the frontEndId to set
	 */
	public void setFrontEndId(String frontEndId) {
		this.frontEndId = frontEndId;
	}
	/**
	 * @return the billerNumber
	 */
	public String getBillerNumber() {
		return billerNumber;
	}
	/**
	 * @param billerNumber the billerNumber to set
	 */
	public void setBillerNumber(String billerNumber) {
		this.billerNumber = billerNumber;
	}

	public boolean isReverseable() {
		return reverseable;
	}

	public void setReverseable(boolean reverseable) {
		this.reverseable = reverseable;
	}

}
