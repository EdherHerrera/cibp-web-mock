package evertec.cibp.web.response;

import java.util.List;

import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.web.enums.MobileResponseStatus;

public class MobileTransactionResponse {

	private List<AbstractTransaction> transactions = null;
	private List<AbstractTransaction> inprocess = null;
	private MobileResponseStatus status;
	
	public List<AbstractTransaction> getTransactions() {
		return transactions;
	}
	public void setTransactions(List<AbstractTransaction> transactions) {
		this.transactions = transactions;
	}
	public List<AbstractTransaction> getInprocess() {
		return inprocess;
	}
	public void setInprocess(List<AbstractTransaction> inprocess) {
		this.inprocess = inprocess;
	}
	public MobileResponseStatus getStatus() {
		return status;
	}
	public void setStatus(MobileResponseStatus status) {
		this.status = status;
	}

}
