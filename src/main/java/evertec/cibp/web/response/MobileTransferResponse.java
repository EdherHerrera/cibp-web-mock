package evertec.cibp.web.response;

import evertec.cibp.web.enums.MobileResponseStatus;
import evertec.cibp.web.utils.requests.MobileTransferRequest;

public class MobileTransferResponse {

	private MobileTransferRequest trfRequest;
	private MobileResponseStatus status;
	private String description;
	

	public MobileResponseStatus getStatus() {
		return status;
	}
	public void setStatus(MobileResponseStatus status) {
		this.status = status;
	}
	public MobileTransferRequest getTrfRequest() {
		return trfRequest;
	}
	public void setTrfRequest(MobileTransferRequest trfRequest) {
		this.trfRequest = trfRequest;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
