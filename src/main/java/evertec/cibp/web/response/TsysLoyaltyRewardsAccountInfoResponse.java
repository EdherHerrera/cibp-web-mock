package evertec.cibp.web.response;

import java.io.Serializable;
import java.util.Objects;

import org.apache.velocity.tools.generic.NumberTool;

/**
 * TsysLoyaltyRewardsAccountInfoResponse
 * @author ET56802
 * @since 1.2
 * @version 1.0 
 */
public class TsysLoyaltyRewardsAccountInfoResponse implements Serializable {

    /** serialVersionUID **/
	private static final long serialVersionUID = 6484250581991559191L;
	
	/**
	 * This field may be null if the rewardsAccountStatus is not 'OPEN'. Ex:
	 * CLOSED_TEMPORARY, CLOSED_PERMANENT.
	 */
	private RewardsEarned rewardsBalance = null;

	private Boolean canRedeemRewards = false;

	private String rewardsAccountStatus = "";
	
	private RewardsEarned pooledRewardsBalance = null; /** pooledRewardsBalance **/

	/**
	 * Get rewardsBalance
	 * 
	 * @return rewardsBalance
	 **/
	public RewardsEarned getRewardsBalance() {
		return rewardsBalance;
	}

	public void setRewardsBalance(RewardsEarned rewardsBalance) {
		this.rewardsBalance = rewardsBalance;
	}

	public TsysLoyaltyRewardsAccountInfoResponse canRedeemRewards(Boolean canRedeemRewards) {
		this.canRedeemRewards = canRedeemRewards;
		return this;
	}

	/**
	 * Indicates if the account is eligible to redeem their rewards.
	 * 
	 * @return canRedeemRewards
	 **/
	public Boolean getCanRedeemRewards() {
		return canRedeemRewards;
	}

	public void setCanRedeemRewards(Boolean canRedeemRewards) {
		this.canRedeemRewards = canRedeemRewards;
	}

	public TsysLoyaltyRewardsAccountInfoResponse rewardsAccountStatus(String rewardsAccountStatus) {
		this.rewardsAccountStatus = rewardsAccountStatus;
		return this;
	}

	/**
	 * Value indicating if the account is opened or closed. Values: OPEN,
	 * CLOSED_TEMPORARY, CLOSED_PERMANENT.
	 * 
	 * @return rewardsAccountStatus
	 **/
	public String getRewardsAccountStatus() {
		return rewardsAccountStatus;
	}

	public void setRewardsAccountStatus(String rewardsAccountStatus) {
		this.rewardsAccountStatus = rewardsAccountStatus;
	}
	
	/**
	 * Get pooledRewardsBalance.
	 * @return pooledRewardsBalnce
	 */
	public RewardsEarned getPooledRewardsBalance() {
		return pooledRewardsBalance;
	}

	public void setPooledRewardsBalance(RewardsEarned pooledRewardsBalance) {
		this.pooledRewardsBalance = pooledRewardsBalance;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TsysLoyaltyRewardsAccountInfoResponse accountInformation = (TsysLoyaltyRewardsAccountInfoResponse) o;
		return Objects.equals(this.rewardsBalance, accountInformation.rewardsBalance)
				&& Objects.equals(this.canRedeemRewards, accountInformation.canRedeemRewards)
				&& Objects.equals(this.rewardsAccountStatus, accountInformation.rewardsAccountStatus)
				&& Objects.equals(this.pooledRewardsBalance, accountInformation.pooledRewardsBalance);
	}

	@Override
	public int hashCode() {
		return Objects.hash(rewardsBalance, canRedeemRewards, rewardsAccountStatus);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class AccountInformation {").append(System.lineSeparator());
		sb.append("    rewardsBalance: ").append(toIndentedString(rewardsBalance)).append(System.lineSeparator());
		sb.append("    canRedeemRewards: ").append(toIndentedString(canRedeemRewards)).append(System.lineSeparator());
		sb.append("    rewardsAccountStatus: ").append(toIndentedString(rewardsAccountStatus)).append(System.lineSeparator());
		sb.append("    pooledRewardsBalance: ").append(toIndentedString(pooledRewardsBalance)).append(System.lineSeparator());
		sb.append("}");
		return sb.toString();
	}
	
	public String toJsonString() {
		
		NumberTool numberTool = new NumberTool();
		
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append(System.lineSeparator());
		
		if (rewardsBalance != null) {
			sb.append("    \"availableRewardsBalance\": \"");
			sb.append(toIndentedString(numberTool.format("currency", rewardsBalance.getAvailable().getValue()) + "\","));
		} else {
			sb.append("    \"availableRewardsBalance\": null,");
		}
		sb.append(System.lineSeparator());
		
		sb.append("    \"canRedeemRewards\": ").append(toIndentedString(canRedeemRewards) + ",");
		sb.append(System.lineSeparator());
		sb.append("    \"rewardsAccountStatus\": ").append(toIndentedString("\"" + rewardsAccountStatus + "\""));
		sb.append(System.lineSeparator());
		
		if (pooledRewardsBalance != null) {
			sb.append("    \"availablePooledRewardsBalance\": \"");
			sb.append(toIndentedString(numberTool.format("currency", pooledRewardsBalance.getAvailable().getValue()) + "\","));
		} else {
			sb.append("    \"availablePooledRewardsBalance\": null,");
		}
		
		sb.append(System.lineSeparator()).append("}");
		
		return sb.toString();
		
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	/**
	 * RewardsEarned
	 * @author ET56802
	 * @since 1.2
	 * @version 1.0 
	 */
	public static class RewardsEarned {

		private RewardsCurrencyAmount available = null;

		public RewardsEarned available(RewardsCurrencyAmount available) {
			this.available = available;
			return this;
		}

		/**
		 * Get available
		 * 
		 * @return available
		 **/
		public RewardsCurrencyAmount getAvailable() {
			return available;
		}

		public void setAvailable(RewardsCurrencyAmount available) {
			this.available = available;
		}

		@Override
		public boolean equals(java.lang.Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			RewardsEarned rewardsEarned = (RewardsEarned) o;
			return Objects.equals(this.available, rewardsEarned.available);
		}

		@Override
		public int hashCode() {
			return Objects.hash(available);
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("class RewardsEarned {\n");
			sb.append("    available: ").append(toIndentedString(available)).append("\n");
			sb.append("}");
			return sb.toString();
		}

		/**
		 * Convert the given object to string with each line indented by 4 spaces
		 * (except the first line).
		 */
		private String toIndentedString(java.lang.Object o) {
			if (o == null) {
				return "null";
			}
			return o.toString().replace("\n", "\n    ");
		}

	}
}
