package evertec.cibp.web.response;

import java.io.Serializable;

import evertec.cibp.vs.beans.accounts.CardMagDataInfo;
import evertec.cibp.vs.beans.accounts.CardMotoAcctInfo;

public class AccountExt  extends Account implements Serializable{
	
	//credit card account extra info
		private CardMagDataInfo cardMagData;
		private CardMotoAcctInfo ccMotoAcct;
		private String cardPANId;
		private String sequence;

		//deposit account extra info
		private String acctKey;
		private String acctCur;
		private String sectionInd;

		public 	AccountExt(){}

		
		/**
		 * @return the cardMagData
		 */
		public CardMagDataInfo getCardMagData() {
			return cardMagData;
		}

		/**
		 * @param cardMagData the cardMagData to set
		 */
		public void setCardMagData(CardMagDataInfo cardMagData) {
			this.cardMagData = cardMagData;
		}

		/**
		 * @return the ccMotoAcct
		 */
		public CardMotoAcctInfo getCcMotoAcct() {
			return ccMotoAcct;
		}

		/**
		 * @param ccMotoAcct the ccMotoAcct to set
		 */
		public void setCcMotoAcct(CardMotoAcctInfo ccMotoAcct) {
			this.ccMotoAcct = ccMotoAcct;
		}

		/**
		 * @return the cardPANId
		 */
		public String getCardPANId() {
			return cardPANId;
		}

		/**
		 * @param cardPANId the cardPANId to set
		 */
		public void setCardPANId(String cardPANId) {
			this.cardPANId = cardPANId;
		}

		/**
		 * @return the sequence
		 */
		public String getSequence() {
			return sequence;
		}

		/**
		 * @param sequence the sequence to set
		 */
		public void setSequence(String sequence) {
			this.sequence = sequence;
		}

		/**
		 * @return the acctKey
		 */
		public String getAcctKey() {
			return acctKey;
		}

		/**
		 * @param acctKey the acctKey to set
		 */
		public void setAcctKey(String acctKey) {
			this.acctKey = acctKey;
		}

		/**
		 * @return the acctCur
		 */
		public String getAcctCur() {
			return acctCur;
		}

		/**
		 * @param acctCur the acctCur to set
		 */
		public void setAcctCur(String acctCur) {
			this.acctCur = acctCur;
		}

		/**
		 * @return the sectionInd
		 */
		public String getSectionInd() {
			return sectionInd;
		}

		/**
		 * @param sectionInd the sectionInd to set
		 */
		public void setSectionInd(String sectionInd) {
			this.sectionInd = sectionInd;
		}

}
