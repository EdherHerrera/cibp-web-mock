package evertec.cibp.web.response;

import java.io.Serializable;
import java.util.List;

import evertec.cibp.web.form.bean.RetirementPlanBean;

/**
*
* @author S681718
* @version 1.0
* @since 1.0
* @see RetirementPlanInfoResponse
*/
public class RetirementPlanInfoResponse implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2709807544958961179L;
	
	List<RetirementPlanBean> plans;

	public List<RetirementPlanBean> getPlans() {
		return plans;
	}

	public void setPlans(List<RetirementPlanBean> plans) {
		this.plans = plans;
	}
		
}
