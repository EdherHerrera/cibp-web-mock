/**
 * author jocampo
 */
package evertec.cibp.web.response;

import java.util.List;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import evertec.cibp.core.models.account.TVFrontendAccount;

public class MobileCashAccountResponse {

	private TVFrontendAccount account;
	private List<CardPlasticAccountRecord> atmCards;
	//REMOVE WITH MBMT-286
	private String atmCard;
	
	public MobileCashAccountResponse(){}
	
	public MobileCashAccountResponse(TVFrontendAccount account, List<CardPlasticAccountRecord> atmCards){
		this.account = account;
		this.atmCards = atmCards;
	}
	
	//REMOVE WITH MBMT-286
	public MobileCashAccountResponse(TVFrontendAccount account, String atmCard){
		this.account = account;
		this.atmCard = atmCard;
	}

	public TVFrontendAccount getAccount() {
		return account;
	}

	public void setAccount(TVFrontendAccount account) {
		this.account = account;
	}

	public List<CardPlasticAccountRecord> getAtmCards() {
		return atmCards;
	}

	public void setAtmCard(List<CardPlasticAccountRecord> atmCards) {
		this.atmCards = atmCards;
	}

	public String getAtmCard() {
		return atmCard;
	}

	public void setAtmCard(String atmCard) {
		this.atmCard = atmCard;
	}
	
}
