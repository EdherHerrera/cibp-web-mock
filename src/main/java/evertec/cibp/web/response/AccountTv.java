package evertec.cibp.web.response;

import java.io.Serializable;

import bppr.tv.beans.accounts.Account;
/**
 * AccountTv
 * @author et55596
 * @since 1.2
 * @version 1.0 
 */
public class AccountTv extends Account implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	/**status **/
	private String status;

	/**
	 * getStatus
	 * @return status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * setStatus
	 * @param status String
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
}
