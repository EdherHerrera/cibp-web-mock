/**
 * 
 */
package evertec.cibp.web.response;


/**
 * Contain the account information for creditcards-service jar
 * and execute Direct Deposit
 * @author Stephanie Diaz Ocasio
 * @since 1.0
 * @version 1.1 Added Javadoc
 * @see OtherClass
 */
public class AccountIdentifier {

    /**
     * Last 4 digits of the customer's account or card number. 
     */
    private String cardLast4; // Last 4 digits of the customer's account or card number.

    
    /**
     * Unique identifier assigned to the account.
     */
    private String accountId; // Unique identifier assigned to the account.

    /**
     *
     * @return current cardLast4
     */
    public String getCardLast4 () {
        return cardLast4;
    }

    /**
     *
     * @param strCardLast4 cardLast4 to set
     */
    public void setCardLast4 (String strCardLast4) {
        this.cardLast4 = strCardLast4;
    }

    /**
     *
     * @return current accountId
     */
    public String getAccountId () {
        return accountId;
    }

    /**
     *
     * @param strAccountId accountId to set
     */
    public void setAccountId (String strAccountId) {
        this.accountId = strAccountId;
    }
}
