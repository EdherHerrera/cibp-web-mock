package evertec.cibp.web.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.validator.GenericValidator;
import org.modelmapper.TypeToken;

import com.evertec.cibp.api.client.model.Error;
import com.evertec.cibp.api.client.model.MicroServiceResponse;

import evertec.cibp.core.dto.PushTokensDto;
import evertec.cibp.web.enums.MobileResponseStatus;
import evertec.cibp.web.mapper.PushTokensModelMapperFactory;


public class PushTokensResponse implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The global status. */
	private MobileResponseStatus status;

	/** The data list. */
	private List<PushTokensDto> data = new ArrayList<>();
	
	/** The error list. */
	private String error;
	
	private List<Error> errors = new ArrayList<>();

	/**
	 * Default Constructor
	 */
	public PushTokensResponse() {
		super();
	}
	
	/**
	 * Custom Constructor
	 */
	public PushTokensResponse(MicroServiceResponse<PushTokensDto> response, int success_status) {
		super();
		if (response.getStatus() == success_status) {
			this.setStatus(MobileResponseStatus.SUCCESS);
			List<PushTokensDto> data = PushTokensModelMapperFactory.getMapper().map( response.getData(), new TypeToken<List<PushTokensDto>>() {}.getType());
			if (data != null) {
				this.setData(data);				
			}
		} else {
			this.setStatus(MobileResponseStatus.ERROR);
			if (response.getErrors() != null) {
				this.setErrors(response.getErrors());
				if (!GenericValidator.isBlankOrNull(response.getErrors().get(0).getTitle())) {
					this.setError(response.getErrors().get(0).getTitle());				
				}
			}
		}	

	}

	
	/**
	 * Gets the data list size.
	 *
	 * @return the data size
	 */
	public Integer getDataSize() {
		return this.getData().size();
	}
	
	/**
	 * Checks if the response has data.
	 *
	 * @return the boolean
	 */
	public Boolean hasData() {
		
		Boolean result = true;
		
		if ( 0 == this.getDataSize() ) {
			result = false;
		}
		
		return result;
	}
	
	/**
	 * Gets the errors list size.
	 *
	 * @return the errors size
	 */
	public Integer getErrorsSize() {
		return this.getErrors().size();
	}
	
	/**
	 * Checks if the response has errors.
	 *
	 * @return the boolean
	 */
	public Boolean hasErrors() {
		
		Boolean result = true;
		
		if ( 0 == this.getErrorsSize() ) {
			result = false;
		}
		
		return result;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public List<PushTokensDto> getData() {
		if ( null == data ) {
			data = new ArrayList<PushTokensDto>();
		}
		return data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(List<PushTokensDto> data) {
		this.data = data;
	}

	/**
	 * Gets the errors.
	 *
	 * @return the errors
	 */
	public String getError() {
		
		return error;
	}

	/**
	 * Sets the errors.
	 *
	 * @param errors the new errors
	 */
	public void setError(String error) {
		this.error = error;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public MobileResponseStatus getStatus() {
		
		if ( null == status ) {
			if ( this.hasData() ) {
				status = MobileResponseStatus.SUCCESS;
			} else if ( this.hasErrors() ) {
				status = MobileResponseStatus.ERROR;
			} else {
				status = MobileResponseStatus.NOT_FOUND;
			}
		}
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(MobileResponseStatus status) {
		this.status = status;
	}

	/**
	 * @return the errors
	 */
	public List<Error> getErrors() {
		return errors;
	}

	/**
	 * @param errors the errors to set
	 */
	public void setErrors(List<Error> errors) {
		this.errors = errors;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PushTokensResponse [status=" + this.getStatus() + ", data=" + data + ", errors=" + error + "]";
	}

}
