package evertec.cibp.web.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

public class RecurringPaymentResponse extends Payment implements Serializable  {
	
	

    /**
	 * 
	 */
	private String frequency;
    private long remainingInstances;
    private BigDecimal finalAmount;
    private Calendar finalProcessingDate;
    private Calendar firstPmDt;
    private String pmtDay1;
    private String pmtDay2;
    private boolean authPayFlag;
    private BigDecimal fixedPmtCurAmt;
    private boolean reverseable;
    
    
	/**
	 * @return the frequency
	 */
	public String getFrequency() {
		return frequency;
	}
	/**
	 * @param frequency the frequency to set
	 */
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	/**
	 * @return the remainingInstances
	 */
	public long getRemainingInstances() {
		return remainingInstances;
	}
	/**
	 * @param remainingInstances the remainingInstances to set
	 */
	public void setRemainingInstances(long remainingInstances) {
		this.remainingInstances = remainingInstances;
	}
	/**
	 * @return the finalAmount
	 */
	public BigDecimal getFinalAmount() {
		return finalAmount;
	}
	/**
	 * @param finalAmount the finalAmount to set
	 */
	public void setFinalAmount(BigDecimal finalAmount) {
		this.finalAmount = finalAmount;
	}
	/**
	 * @return the finalProcessingDate
	 */
	public Calendar getFinalProcessingDate() {
		return finalProcessingDate;
	}
	/**
	 * @param finalProcessingDate the finalProcessingDate to set
	 */
	public void setFinalProcessingDate(Calendar finalProcessingDate) {
		this.finalProcessingDate = finalProcessingDate;
	}
	/**
	 * @return the firstPmDt
	 */
	public Calendar getFirstPmDt() {
		return firstPmDt;
	}
	/**
	 * @param firstPmDt the firstPmDt to set
	 */
	public void setFirstPmDt(Calendar firstPmDt) {
		this.firstPmDt = firstPmDt;
	}
	/**
	 * @return the pmtDay1
	 */
	public String getPmtDay1() {
		return pmtDay1;
	}
	/**
	 * @param pmtDay1 the pmtDay1 to set
	 */
	public void setPmtDay1(String pmtDay1) {
		this.pmtDay1 = pmtDay1;
	}
	/**
	 * @return the pmtDay2
	 */
	public String getPmtDay2() {
		return pmtDay2;
	}
	/**
	 * @param pmtDay2 the pmtDay2 to set
	 */
	public void setPmtDay2(String pmtDay2) {
		this.pmtDay2 = pmtDay2;
	}
	/**
	 * @return the authPayFlag
	 */
	public boolean isAuthPayFlag() {
		return authPayFlag;
	}
	/**
	 * @param authPayFlag the authPayFlag to set
	 */
	public void setAuthPayFlag(boolean authPayFlag) {
		this.authPayFlag = authPayFlag;
	}
	/**
	 * @return the fixedPmtCurAmt
	 */
	public BigDecimal getFixedPmtCurAmt() {
		return fixedPmtCurAmt;
	}
	/**
	 * @param fixedPmtCurAmt the fixedPmtCurAmt to set
	 */
	public void setFixedPmtCurAmt(BigDecimal fixedPmtCurAmt) {
		this.fixedPmtCurAmt = fixedPmtCurAmt;
	}

	public boolean isReverseable() {
		return reverseable;
	}

	public void setReverseable(boolean reverseable) {
		this.reverseable = reverseable;
	}

}
