package evertec.cibp.web.response;

import evertec.cibp.web.enums.WebResponseStatus;
import evertec.cibp.web.request.RDCEnrollmentInfoRequest;

public class RDCEnrollmentInfoResponse {

	private RDCEnrollmentInfoRequest rdcEnrollmentRequest;
	private WebResponseStatus responseStatus;
	private evertec.otherservices.enums.ResponseStatus serviceResponseStatus;

	public RDCEnrollmentInfoRequest getRdcEnrollmentRequest() {
		return rdcEnrollmentRequest;
	}

	public void setRdcEnrollmentRequest(RDCEnrollmentInfoRequest rdcEnrollmentRequest) {
		this.rdcEnrollmentRequest = rdcEnrollmentRequest;
	}

	public WebResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(WebResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	public evertec.otherservices.enums.ResponseStatus getServiceResponseStatus() {
		return serviceResponseStatus;
	}

	public void setServiceResponseStatus(evertec.otherservices.enums.ResponseStatus serviceResponseStatus) {
		this.serviceResponseStatus = serviceResponseStatus;
	}

}
