package evertec.cibp.web.response;

import evertec.cibp.core.models.entitlement.EntitlementStatus;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

/**
 * Copyright 2019 Evertec, Inc. All Rights Reserved.
 *
 * @author Henry Terrero
 * @version 1.0
 * @since 1.0
 */
public class GlobalEntitlementResponse implements Serializable {

    /**
     * This is a property for mapping
     * type
     * ID
     */
    private String strType;

    /**
     * This is a property for mapping
     * Enum for EntitlementStatus
     * Type String
     */
    private EntitlementStatus status;


    /**
     * This is a property for mapping
     * application
     */
    private String application;

    /**
     * This is a property for mapping
     * type Date
     * eventDate
     */
    private Date eventDate;

    /**
     * Gets the strType.
     *
     * @return the strType
     */
    public String getStrType() {
        return strType;
    }

    /**
     * Sets the tool tip strType.
     *
     * @param strType  the strType of the tool tip
     */
    public void setStrType(String strType) {
        this.strType = strType;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public EntitlementStatus getStatus() {
        return status;
    }

    /**
     * Sets the tool tip status.
     *
     * @param status  the status of the tool tip
     */
    public void setStatus(EntitlementStatus status) {
        this.status = status;
    }

    /**
     * Gets the application.
     *
     * @return the application
     */
    public String getApplication() {
        return application;
    }

    /**
     * Sets the tool tip application.
     *
     * @param application  the application of the tool tip
     */
    public void setApplication(String application) {
        this.application = application;
    }

    /**
     * Gets the eventDate.
     *
     * @return the eventDate
     */
    public Date getEventDate() {
        return Optional.ofNullable(eventDate).map(Date::getTime).map(Date::new).orElse(null);
    }

    /**
     * Sets the tool tip eventDate.
     *
     * @param eventDate  the eventDate of the tool tip
     */
    public void setEventDate(Date eventDate) {
        this.eventDate = Optional.ofNullable(eventDate).map(Date::getTime).map(Date::new).orElse(null);
    }
}
