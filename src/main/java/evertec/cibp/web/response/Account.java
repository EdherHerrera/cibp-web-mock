package evertec.cibp.web.response;

import java.io.Serializable;

import evertec.cibp.vs.beans.accounts.BankInfo;

public class Account implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 428798201983523346L;
	
	private String accountNumber="";
	private String accountType="";
	private BankInfo bankInfo;
	private String dpType="";
	private Object backendObject = null;


	public Account(){}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	/**
	 * @return the accountType
	 */
	public String getAccountType() {
		return accountType;
	}

	public void setBankInfo(BankInfo bankInfo) {
		this.bankInfo = bankInfo;
	}

	public BankInfo getBankInfo() {
		return bankInfo;
	}

	public Object getBackendObject() {
		return backendObject;
	}

	public void setDpType(String dpType) {
		this.dpType = dpType;
	}

	public String getDpType() {
		return dpType;
	}

}
