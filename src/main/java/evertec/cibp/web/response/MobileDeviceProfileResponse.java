package evertec.cibp.web.response;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.devices.CustomerDevice;
import evertec.cibp.web.enums.MobileResponseStatus;

public class MobileDeviceProfileResponse {

	private CustomerDevice device;
	private CustomerProfile profile;
	private MobileResponseStatus status;
	
	public CustomerDevice getDevice() {
		return device;
	}
	public void setDevice(CustomerDevice device) {
		this.device = device;
	}
	public CustomerProfile getProfile() {
		return profile;
	}
	public void setProfile(CustomerProfile profile) {
		this.profile = profile;
	}
	public MobileResponseStatus getStatus() {
		return status;
	}
	public void setStatus(MobileResponseStatus status) {
		this.status = status;
	}
	
}
