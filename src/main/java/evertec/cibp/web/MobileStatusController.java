package evertec.cibp.web;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.MobileApiSettingsBean;

/**
 * Helper endpoints provide session/uptime status for mobile apps. Used by iOS
 * app to determine if webapp is up before attempting to log in.
 * 
 * @author hector
 * 
 */
public class MobileStatusController extends MultiActionController implements MessageSourceAware {

	private static final Logger logger = Logger.getLogger(WebAppController.class);
	private MessageSource messageSource;
	private CustomerServices customerService;
	private MobileApiSettingsBean mobileAppGatewaySettings;

	private String[] locatorActiveLayers;

	public void initialize() {
		// TODO Auto-generated method stub
		String layersString = mobileAppGatewaySettings.getLocatorActiveLayers();
		locatorActiveLayers = layersString.split(",");
	}

	protected MobileApiSettingsBean getMobileAppGatewaySettings() {
		return mobileAppGatewaySettings;
	}

	public void setMobileAppGatewaySettings(MobileApiSettingsBean mobileAppGatewaySettings) {
		this.mobileAppGatewaySettings = mobileAppGatewaySettings;
	}

	protected CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/*
	 * Hector iPhone app - added this as a way of validating if the requestor
	 * has a valid, active session
	 */
	public ModelAndView globalStatus(HttpServletRequest request, HttpServletResponse response) throws Exception {
		final String retirementPlan = "RETIREMENT_PLAN"; // Retplan literal value
		Map<String, Object> model = new HashMap<>();

		List<String> globalEntitlements = new LinkedList<String>();
		globalEntitlements.add("BANKING");
		globalEntitlements.add("RDC");
		globalEntitlements.add("BILLPAY");
		globalEntitlements.add("TRANSFERS");
		globalEntitlements.add("EBILLS");
		globalEntitlements.add("AGGREGATION");
		globalEntitlements.add("MOBILE_BANKING");
		globalEntitlements.add("ALERTS");
		globalEntitlements.add("SIGNON");
		globalEntitlements.add("ENROLLMENT");
		globalEntitlements.add("ACCOUNT_OPENING");
		globalEntitlements.add("EZSTMT");
		globalEntitlements.add("OAO");
		globalEntitlements.add("IPHONE_API");
		globalEntitlements.add("ATHMOVIL");
		globalEntitlements.add("MOBILE_CASH");
		globalEntitlements.add("MOBILE_CASH_FOR_OTHERS");
		globalEntitlements.add(retirementPlan);

		Map<String, Boolean> dbEntitlementsStatus = new HashMap<String, Boolean>();
		Map<String, Boolean> entitlementsStatus = new HashMap<String, Boolean>();
		
		List<GlobalEntitlement> entitlements = customerService.getGlobalEntitlements();
		for(int i= 0; i< entitlements.size(); i++){
			if(entitlements.get(i) != null && entitlements.get(i).getType() != null){
				dbEntitlementsStatus.put(entitlements.get(i).getStrType(), entitlements.get(i).getStatus() == EntitlementStatus.ENABLED);
			}
		}
		
		for(int i=0; i< globalEntitlements.size(); i++){
			String key = globalEntitlements.get(i);
			if(dbEntitlementsStatus.containsKey(key)){
				entitlementsStatus.put(key, dbEntitlementsStatus.get(key));
			}else{
				entitlementsStatus.put(globalEntitlements.get(i), false);
			}
		}
		
		if(!StaticMessageSource.isFlagEnabled(StaticMessageSource.MOBILE_CASH)){
			entitlementsStatus.put("MOBILE_CASH", false);
		}
		
		entitlementsStatus.put("MOBILE_SDK_RSA", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC615_MOBILE_SDK_DATA));
		
		model.put("entitlements", entitlementsStatus);
		model.put("locatorActiveLayers", locatorActiveLayers);

		String isdown = messageSource.getMessage("downPage", null, "UP", Locale.ENGLISH);
		model.put("downtime", (isdown != null && isdown.equalsIgnoreCase("DOWN")));

		model.put("latestVersion", mobileAppGatewaySettings.getLatestVersionIphoneApp());
		model.put("minimumVersion", mobileAppGatewaySettings.getMinimumRequiredVersionIphoneApp());

		return new ModelAndView("globalStatus", model);
	}
}
