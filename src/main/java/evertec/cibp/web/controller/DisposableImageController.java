package evertec.cibp.web.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Normalizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.EncodingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.exceptions.crypto.AesDencryptException;
import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.core.services.mobiusview.client.navigation.client.MobiusViewClient;
import evertec.cibp.web.session.UserSession;

@Controller
@RequestMapping(value = "/mbimage")
public class DisposableImageController {
	
	//http://localhost:8080/cibp-web/pdfjs/web/viewer.html?file=http://localhost:8080/cibp-web/actions/mbimage/view/55c09e818bd64bfe3f08ebcf5fd64a86c574b6380ae4f8340f4726888065f497
	
	private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger
			.getLogger(DisposableImageController.class);
	
	@Autowired
	MobiusViewClient mobiusViewClient;
	
	
	public MobiusViewClient getMobiusViewClient() {
		return mobiusViewClient;
	}

	public void setMobiusViewClient(MobiusViewClient mobiusViewClient) {
		this.mobiusViewClient = mobiusViewClient;
	}
	
	public String generateSanitizedContentDisposition(String filename,String ext) {
		String base = "";
		String sanitizedFileName = filename + "." + ext;
		try {
			sanitizedFileName = ESAPI.encoder().encodeForURL(filename);
		} catch (EncodingException e) {
		}
		
		base = base + "filename=\""+sanitizedFileName+"\"; filename*=UTF-8''"+sanitizedFileName;
		return base;	
	}
	
//	@RequestMapping(method = RequestMethod.GET, value = "/download")
//	public void downloadImageGet(HttpServletRequest request, 
//            HttpServletResponse response, @RequestParam(value = "key") String key) throws IOException {
//		downloadImage(request,response,key);
//	}

	@RequestMapping(method = RequestMethod.POST, value = "/download/{key}")
			public void downloadImage(HttpServletRequest request, 
                    HttpServletResponse response, @PathVariable(value = "key") String key) throws IOException {
			    
				logger.info("DisposableImageController downloadImage key: " + key);
		
				String tmpDir = obtainTmpDir();
			    
			    String fileName = "";
			    String sanitizedFileName = "";
			    UserSession userSession = KiuwanUtils.cast(UserSession.class,WebUtils.getSessionAttribute(request, "userSession"));
			    key =  Normalizer.normalize(key, Normalizer.Form.NFKC); //Capricho de Kiuwan
			    if (key.matches("")) {
			        throw new IllegalStateException();
			    } else {
			    	logger.debug("DisposableImageController downloadImage Empty key");
			    }
			    try  {
			    	fileName = MobiusViewClient.decrypt(key);
			    	sanitizedFileName = new File(fileName).getName(); //Capricho de Kiuwan
			    } catch (AesDencryptException e) {
			    	logger.warn("Invalid Key: " + key + "User: " + userSession.getUsername());
			    }
			    Path file = Paths.get(tmpDir, sanitizedFileName);
			    
			    logger.info("DisposableImageController downloadImage key: " + key + "path " + tmpDir + "\"" + sanitizedFileName);
				    
		        if (!fileName.isEmpty() && Files.exists(file)) 
		        {
		        	ESAPI.httpUtilities().addHeader(response, "Content-Disposition", "attachment; " + generateSanitizedContentDisposition(sanitizedFileName,"pdf")); 
		            KiuwanUtils.setContentType(response,"application/octet-stream");
		            		
		            try
		            {
		                Files.copy(file, response.getOutputStream());
		                File tempDir = new File(tmpDir);
		                tmpDir = tempDir.getCanonicalPath();
		                
		                new File(tmpDir, sanitizedFileName).delete();
		                response.getOutputStream().flush();
		            } 
		            catch (IOException ex) {
		            }
		        } else {
		        	response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		        	response.getOutputStream().flush();
		        }
			}

	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public String openImage(HttpServletRequest request, 
            HttpServletResponse response, @RequestParam(value = "key") String key) throws IOException {
	    String tmpDir = obtainTmpDir();
	    
	    String fileName = "";
	    String sanitizedFileName = "";
	    UserSession userSession = KiuwanUtils.cast(UserSession.class,WebUtils.getSessionAttribute(request, "userSession"));
	    key =  Normalizer.normalize(key, Normalizer.Form.NFKC); //Capricho de Kiuwan
	    if (key.matches("")) {
	        throw new IllegalStateException();
	    } else {
	    	logger.debug("DisposableImageController openImage Empty key");
	    }
	    
	    try  {
	    	fileName = MobiusViewClient.decrypt(key);
	    	sanitizedFileName = new File(fileName).getName(); //Capricho de Kiuwan
	    } catch (AesDencryptException e) {
	    	logger.warn("Invalid Key: " + key + "User: " + userSession.getUsername());
	    }
	    Path file = Paths.get(tmpDir, sanitizedFileName);
	    
        if (!fileName.isEmpty() && Files.exists(file)) 
        {
       	 	KiuwanUtils.setContentType(response,"application/pdf");
       	 	ESAPI.httpUtilities().addHeader(response, "Content-Disposition", "inline; " + generateSanitizedContentDisposition(sanitizedFileName,"pdf"));

       	 	try
            {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            } 
            catch (IOException ex) {
            }
        } else {
        	response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        	response.getOutputStream().flush();
        }
		return null;
	}

	public String obtainTmpDir() {
		return Normalizer.normalize(KiuwanUtils.getSysProperty("java.io.tmpdir"), Normalizer.Form.NFKC); //Capricho de Kiuwan
	}
//	
//	@Autowired
//	MobiusViewCredentialsService mobiusCredentialsService;
//    @RequestMapping(method = RequestMethod.GET, value = "/test")
//	public String test(HttpServletRequest request, 
//            HttpServletResponse response, @RequestParam(value = "key", required = false) String key) throws IOException {
//    	try {
//			MobiusViewCredentials mvc = mobiusCredentialsService.getMobiusViewCredentials();
//			System.out.println(mvc.toString());
//    	} catch (DataAccessException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return "";
//    }
    
   
    
				
	
}
