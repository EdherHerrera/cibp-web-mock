/**
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @created 2020-02-26 10:25
 */

package evertec.cibp.web.controller;

/**
 * CIBPWebController - Created bean in replace of xml bean.
 *
 * @author john doe <jdoe@myorg.com>
 * @since 1.2
 * @version 1.8 Changes done
 * @see MobileChangeEmailController
 */

public abstract class CIBPWebController {
    /**
     * Cache seconds variable defined on xml bean
     */

    private int cacheSeconds; //Cache seconds variable defined on xml bean

    /**
     * Use expires header variable defined on xml bean
     */

    private boolean useExpiresHeader; // Use expires header variable defined on xml bean


    public int getCacheSeconds() {
        return cacheSeconds;
    }

    public void setCacheSeconds(int cacheSeconds) {
        this.cacheSeconds = cacheSeconds;
    }

    public boolean isUseExpiresHeader() {
        return useExpiresHeader;
    }

    public void setUseExpiresHeader(boolean useExpiresHeader) {
        this.useExpiresHeader = useExpiresHeader;
    }
}
