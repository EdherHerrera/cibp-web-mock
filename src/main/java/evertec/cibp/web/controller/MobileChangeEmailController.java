/**
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @created 2020-02-12 17:28
 */
package evertec.cibp.web.controller;

import evertec.cibp.web.controller.bean.EmailData;
import evertec.cibp.web.controller.dto.mapper.ErrorsMapper;
import evertec.cibp.web.controller.dto.response.ErrorMessage;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

import org.apache.commons.lang.StringEscapeUtils;
import org.owasp.csrfguard.CsrfGuardException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping(value = "/mobileChangeEmail")

/**
 * The Mobile Change Email Controller
 *
 */
public class MobileChangeEmailController extends CIBPWebController {

    /**
     * User session variable name
     */
    private static final String USER_SESSION = "userSession"; // User session variable name
    /**
     * Success value
     */
    private static final String SUCCESS_MESSAGE = "SUCCESS"; // Success value

    /**
     * View relative path and name
     */
    public static final String VIEW_RELATIVE_PATH = "changeEmailMobile";//View relative path and name

    /**
     * Current email field name
     */
    public static final String CURRENT_EMAIL_FIELD_NAME = "currentEmail"; //Current email field name

    /**
     * Time interval field name
     */
    public static final String TIME_INTERVAL_FIELD_NAME = "timeInterval"; //Time interval field name

    /**
     * Time interval used to show the popup
     */
    @Value(value = "${oob.time.interval.seconds}")
    private int timeInterval; // Time interval used to show the popup

    /**
     * Mapper from mapstruct library to map error values
     */
    private ErrorsMapper errorsMapper; // Mapper from mapstruct library to map error values

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setDisallowedFields("administrator");
    }

    /**
     * The autowired mapper constructor
     *
     * @param errorsMapper
     */
    @Autowired
    public MobileChangeEmailController(ErrorsMapper errorsMapper) {
        this.errorsMapper = errorsMapper;
    }

    /**
     * The get method to have the initialization parameters
     *
     * @param request  Get all the request attributes and properties
     * @param modelMap to return to the view
     * @return the name of the view
     * @throws CsrfGuardException 
     */
    @RequestMapping(method = RequestMethod.GET)
    public String mobileChangeEmail(HttpServletRequest request, ModelMap modelMap) throws CsrfGuardException {

        modelMap.put(TIME_INTERVAL_FIELD_NAME, timeInterval);

        final Object retrievedSession = WebUtils.getSessionAttribute(request, USER_SESSION); // Session object to get the email data

        if (retrievedSession instanceof UserSession) {
            final UserSession userSession = (UserSession) retrievedSession; // Casted Session to get the email data
            Utils.generateTokenCsrfGuard(request);
            if (!Objects.isNull(userSession.getCustomerProfile())) {
                final String currentEmail = userSession.getCustomerProfile().getEmail(); // Current email variable to show on view

                modelMap.put(CURRENT_EMAIL_FIELD_NAME, currentEmail);
            }
        }

        return VIEW_RELATIVE_PATH;
    }

    /**
     * Validate the fields in the form
     *
     * @param emailData the object retrieved from the view and validated with javax hibernate
     * @param result    retrieved from the javax validations
     * @return the successfull or with errors (using HTTP code).
     */
    @RequestMapping(value = "/validate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> validateFields(@Valid @ModelAttribute(value = "emailData") EmailData emailData,
                                            BindingResult result) {

        List<ErrorMessage> errorMessages = errorsMapper.toErrorResponseList(result.getFieldErrors());

        if(result.hasErrors()){
            List<ErrorMessage> sanitizedMessages = sanitizeMessage(errorMessages);

            ResponseEntity<List<ErrorMessage>> errorResponse = new ResponseEntity<List<ErrorMessage>>(sanitizedMessages, HttpStatus.UNPROCESSABLE_ENTITY);

            return errorResponse;
        }else{

            String safeString = StringEscapeUtils.unescapeHtml(SUCCESS_MESSAGE);
            ResponseEntity<String> successResponse = new ResponseEntity<String>(safeString, HttpStatus.OK);

            return successResponse;
        }
    }

     private List<ErrorMessage> sanitizeMessage(List<ErrorMessage> listErrors) {

         List<ErrorMessage> sanitizeList = new ArrayList<>();

         for (ErrorMessage error : listErrors) {

             String safeField = StringEscapeUtils.unescapeHtml(error.getFieldName());

             String safeMessage = StringEscapeUtils.unescapeHtml(error.getMessage());

             sanitizeList.add(new ErrorMessage(safeField,safeMessage));
        }

        return sanitizeList;
    }

    public int getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(int timeInterval) {
        this.timeInterval = timeInterval;
    }

    public ErrorsMapper getErrorsMapper() {
        return errorsMapper;
    }

    public void setErrorsMapper(ErrorsMapper errorsMapper) {
        this.errorsMapper = errorsMapper;
    }
}
