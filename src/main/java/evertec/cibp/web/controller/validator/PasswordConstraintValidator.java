package evertec.cibp.web.controller.validator;

import org.passay.CharacterRule;
import org.passay.DictionarySubstringRule;
import org.passay.EnglishCharacterData;
import org.passay.EnglishSequenceData;
import org.passay.HistoryRule;
import org.passay.IllegalSequenceRule;
import org.passay.LengthRule;
import org.passay.MessageResolver;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.PropertiesMessageResolver;
import org.passay.RuleResult;
import org.passay.SourceRule;
import org.passay.WhitespaceRule;
import org.passay.dictionary.ArrayWordList;
import org.passay.dictionary.WordListDictionary;
import org.passay.dictionary.sort.ArraysSort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.web.utils.ChallengeInformationUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * 
 * @author andrea.manrique
 *
 */
public class PasswordConstraintValidator {

	private final static String message = "message";
	private final static String validate = "validate";
	private static final Logger LOGGER = LoggerFactory.getLogger(PasswordConstraintValidator.class); // The LOGGER
	private String validateValue = "false";

	public Map<String, String> validateRules(Map<String, String> paramsPass, CustomerProfile profile, Boolean isEnroll) {

		String messageTemplate = "";
		InputStream inputStream = null;
		Map<String, String> paramsResult = new HashMap<String, String>();

		try {

			Properties props = new Properties();
			String wordList = paramsPass.get(ChallengeInformationUtils.WORD_LIST) + "";
			String[] arrayWordList = wordList.split(";");

			inputStream = paramsPass.get(ChallengeInformationUtils.LANGUAGE).equalsIgnoreCase("en")
					? getClass().getClassLoader().getResourceAsStream("passay.properties")
					: getClass().getClassLoader().getResourceAsStream("passay_es.properties");

			props.load(inputStream);

			MessageResolver resolver = new PropertiesMessageResolver(props);
			WordListDictionary wordListDictionary = new WordListDictionary(
					new ArrayWordList(arrayWordList, false, new ArraysSort()));

			PasswordValidator validator = new PasswordValidator(resolver, Arrays.asList(

					// length between 8 and 15 characters
					new LengthRule(8, 15),

					// at least one upper-case character
					new CharacterRule(EnglishCharacterData.UpperCase, 1),

					// at least one lower-case character
					new CharacterRule(EnglishCharacterData.LowerCase, 1),

					// at least one digit character
					new CharacterRule(EnglishCharacterData.Digit, 1),

					// at least one symbol (special character)
					new CharacterRule(EnglishCharacterData.Special, 1),

					// no whitespace
					new WhitespaceRule(),

					// Dictionary word list
					new DictionarySubstringRule(wordListDictionary),

					new SourceRule(),

					new HistoryRule(),
					// rejects passwords that contain a sequence of >= 5 characters alphabetical
					// (e.g. abcdef)
					new IllegalSequenceRule(EnglishSequenceData.Alphabetical, 4, false),

					// rejects passwords that contain a sequence of >= 4 characters numerical
					// (e.g.// 12345)
					new IllegalSequenceRule(EnglishSequenceData.Numerical, 4, false)));

			String passNew = paramsPass.get(ChallengeInformationUtils.NEW_PASS);


			PasswordData passwordData = new PasswordData(passNew);
			if (!isEnroll) {
				String passOld = paramsPass.get(ChallengeInformationUtils.OLD_PASS);
				passwordData.setPasswordReferences(new PasswordData.SourceReference("source", passOld),
						new PasswordData.HistoricalReference(passOld));
			}
			RuleResult result = validator.validate(passwordData);
			String usernameValidate = profile.getUsername() + ";" + profile.getLastName() + ";"
					+ profile.getFirstName();

			messageTemplate = validateCustomerInf(usernameValidate, passwordData,
					paramsPass.get(ChallengeInformationUtils.LANGUAGE));

			if (result.isValid() && !validateValue.equals("false")) {
				validateValue = "true";
			} else {
				validateValue = "false";
			}

			List<String> messages = validator.getMessages(result);
			messageTemplate += String.join(" ", messages);
			paramsResult.put(message, messageTemplate);

		} catch (IOException e) {
			LOGGER.error("There is a exception on the validation");
		} finally {
			if (!inputStream.equals(null)) {
				try {
					inputStream.close();
				} catch (IOException e) {
					LOGGER.error("There is a exception on the validation");
				}
			}
		}

		paramsResult.put(validate, validateValue);
		return paramsResult;
	}

	private String validateCustomerInf(String usernameValidate, PasswordData passwordData, String language) {

		String message = "";
		WordListDictionary wordListDictionary = new WordListDictionary(
				new ArrayWordList(usernameValidate.split(";"), false, new ArraysSort()));
		PasswordValidator validatorUsername = new PasswordValidator(
				Arrays.asList(new DictionarySubstringRule(wordListDictionary)));
		RuleResult resultUsername = validatorUsername.validate(passwordData);

		if (resultUsername.isValid()) {
			validateValue = "true";
		} else {
			if (language.equals("en")) {
				message = "Password does not allow the use of the customer's name, surname, or username. ";
			} else {
				message = "Contraseña no permite usar nombre, apellido o usuario del cliente. ";
			}
			validateValue = "false";
		}
		return message;
	}
}