/**
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @created 2020-02-27 16:00
 */

package evertec.cibp.web.controller.validator;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import java.util.Objects;

/**
 * The validator to check the field match
 *
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @version 1.8
 * @created 2020-02-26 10:35
 * @see FieldMatch
 * @since 1.0
 */
public class FieldMatchValidator implements ConstraintValidator<FieldMatch, Object> {

    /**
     * The LOGGER
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(FieldMatchValidator.class); // The LOGGER

    /**
     * First Field name to validate
     */
    private String firstFieldName; // First Field name to validate
    /**
     * Second Field name to validate
     */
    private String secondFieldName; //Second Field name to validate
    /**
     * Message to show to final user
     */
    private String message; // Message to show to final user

    /**
     * Initialize the validator
     *
     * @param constraintAnnotation the name values that comes from the annotation
     */
    @Override
    public void initialize(final FieldMatch constraintAnnotation) {
        firstFieldName = constraintAnnotation.first();
        secondFieldName = constraintAnnotation.second();
        message = constraintAnnotation.message();
    }

    /**
     * @param value   of the object to validate
     * @param context on the spring framework
     * @return if is valid or not
     */
    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
        boolean valid = true; // Variable to respond to see if is valid the values
        try {
            final Object firstObj = BeanUtils.getProperty(value,
                    firstFieldName); // First Object variable retrieved from the object container
            final Object secondObj = BeanUtils.getProperty(value,
                    secondFieldName); // Second Object variable retrieved from the object container

            final boolean objectsAreEqual = areObjectsEquals(firstObj, secondObj); // If the objects are equals

            valid = Objects.isNull(firstObj) && Objects.isNull(secondObj) ||
                    (!Objects.isNull(firstObj) && objectsAreEqual);
        } catch (final Exception ignore) {
            LOGGER.error("There is a exception on the validation");
            valid = false;
        }

        if (!valid) {
            context.buildConstraintViolationWithTemplate(message).
                    addNode(secondFieldName).
                    addConstraintViolation().disableDefaultConstraintViolation();
        }

        return valid;
    }


    /**
     * Comparison like objects and special case for strings
     *
     * @param firstObj  to compare
     * @param secondObj to compare
     * @return if are equals
     */
    private static boolean areObjectsEquals(Object firstObj, Object secondObj) {
        boolean objectsAreEqual = false; // The response

        if ((firstObj instanceof String) && (secondObj instanceof String)) {
            final String strFirstField = (String) firstObj; //Final first value to compare
            final String strSecondField = (String) secondObj; //Final second value to compare

            objectsAreEqual = strFirstField.equals(strSecondField);

        } else {
            objectsAreEqual = firstObj.equals(secondObj);
        }
        return objectsAreEqual;
    }

}
