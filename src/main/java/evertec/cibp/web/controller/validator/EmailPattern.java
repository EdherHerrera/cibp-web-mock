package evertec.cibp.web.controller.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Email pattern check using Apache Commons EmailValidator
 * as the Hibernate EmailValidator is limited.
 *
 * The string has to be a well-formed email address.
 *
 */
@Constraint(validatedBy = EmailValidator.class)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
public @interface EmailPattern {

  String message() default "{org.hibernate.validator.constraints.Email.message}";
	Class<?>[] groups() default { };
	Class<? extends Payload>[] payload() default { };
}