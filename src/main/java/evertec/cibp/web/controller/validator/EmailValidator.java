package evertec.cibp.web.controller.validator;

import org.apache.commons.lang.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Check a string if it's a valid eMail using {@see org.apache.commons.validator.routines.EmailValidator}
 * You need commons-validator-1.4 or later as a dependency.
 *
 */
public class EmailValidator implements ConstraintValidator<EmailPattern, String> {

  /**
   * Initialize the validator in preparation for isValid calls.
   * The constraint annotation for a given constraint declaration
   * is passed.
   * <p/>
   * This method is guaranteed to be called before any use of this instance for
   * validation.
   *
   * @param annotation annotation instance for a given constraint declaration
   */
  public void initialize(EmailPattern annotation) {
  }

  /**
   * {@inheritDoc} check if given string is a valid mail.
   *
   * @see javax.validation.ConstraintValidator#isValid(java.lang.Object,
   *      javax.validation.ConstraintValidatorContext)
   */
  public boolean isValid(final String value, final ConstraintValidatorContext context) {
    if (StringUtils.isEmpty(value)) {
      return true;
    }
    return org.apache.commons.validator.routines.EmailValidator.getInstance().isValid(value);
  }
}