/**
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @created 2020-02-28 10:21
 */
package evertec.cibp.web.controller.dto.mapper;

import evertec.cibp.web.controller.dto.response.ErrorMessage;
import org.springframework.validation.FieldError;

import java.util.List;

/**
 * The errors mapper
 *
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @version 1.8
 * @created 2020-02-28 10:21
 * @created 2020-02-12 17:28
 * @see evertec.cibp.web.controller.MobileChangeEmailController
 * @since 1.0
 */
public interface ErrorsMapper {

    /**
     * @param error on the Errors class
     * @return ErrorMessage
     */
    ErrorMessage toErrorResponse(FieldError error);

    /**
     * Map using a list
     *
     * @param lstFieldErrors
     * @return a list of ErrorMessage
     */
    List<ErrorMessage> toErrorResponseList(List<FieldError> lstFieldErrors);
}
