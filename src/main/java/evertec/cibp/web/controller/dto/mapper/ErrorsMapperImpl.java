package evertec.cibp.web.controller.dto.mapper;

import evertec.cibp.web.controller.dto.response.ErrorMessage;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;

/**
 * The errors mapper implementation
 *
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @created 5/11/21 8:37 AM
 * @see evertec.cibp.web.controller.MobileChangeEmailController
 * * @since 1.0
 */

@Component
public class ErrorsMapperImpl implements ErrorsMapper {

    /**
     * @param error on the Errors class
     * @return ErrorMessage
     */
    public ErrorMessage toErrorResponse(FieldError error) {
        if (error == null) {
            return null;
        }

        ErrorMessage errorMessage = new ErrorMessage();

        errorMessage.setMessage(error.getDefaultMessage());
        errorMessage.setFieldName(error.getField());

        return errorMessage;
    }

    /**
     * Map using a list
     *
     * @param lstFieldErrors
     * @return a list of ErrorMessage
     */
    public List<ErrorMessage> toErrorResponseList(List<FieldError> lstFieldErrors) {
        if (lstFieldErrors == null) {
            return null;
        }

        List<ErrorMessage> list = new ArrayList<ErrorMessage>(lstFieldErrors.size());
        for (FieldError fieldError : lstFieldErrors) {
            list.add(toErrorResponse(fieldError));
        }

        return list;
    }
}
