/**
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @created 2020-02-28 10:06
 */

package evertec.cibp.web.controller.dto.response;



/**
 * Error Message - Errors message for data validations
 *
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @version 1.0 Changes done
 * @since 1.0
 * @version 1.8
 * @see evertec.cibp.web.controller.MobileChangeEmailController
 */
public class ErrorMessage {

    /**
     * Field name to map
     */

    private String fieldName;//field name mapping

    /**
     * Message to show to the final client
     */

    private String message;//message to display

    public ErrorMessage() {
    }

    public ErrorMessage(String fieldName, String message) {
        this.fieldName = fieldName;
        this.message = message;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
