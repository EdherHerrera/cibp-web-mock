/**
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @created 2020-02-27 06:57
 */

package evertec.cibp.web.controller.bean;

import evertec.cibp.web.controller.validator.EmailPattern;
import evertec.cibp.web.controller.validator.FieldMatch;


import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotEmpty;


@FieldMatch(first = "newEmail", second = "confirmEmail", message = EmailData.EMAIL_TEXT_VALIDATION_NOT_MATCH_KEY)
/**
 * The email data for the change email controller
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @created 2020-02-27 06:57
 * @since 1.0
 * @version 1.8
 *
 */
public class EmailData {

    /**
     * Email text validation required Resource name expressed in Spring injected format
     */
    public static final String EMAIL_TEXT_VALIDATION_REQUIRED_KEY = "{email.text.validation.required}";

    /**
     * Email text validation not match Resource name expressed in Spring injected format
     */
    public static final String EMAIL_TEXT_VALIDATION_NOT_MATCH_KEY =
            "{email.text.validation.notMatch}";// Email text validation not match Resource name expressed in Spring injected format

    /**
     * Email text validation invalid format Resource name expressed in Spring injected format
     */
    public static final String EMAIL_TEXT_VALIDATION_INVALID_KEY =
            "{email.text.validation.invalid}"; // Email text validation invalid format Resource name.

    /**
     * Email text validation invalid format Resource name expressed in Spring injected format
     */
    public static final String EMAIL_TEXT_VALIDATION_REQ_CONF_KEY =
            "{email.text.validation.required.confirmation}"; // Email text validation invalid format Resource name.

    @NotEmpty(message = EMAIL_TEXT_VALIDATION_REQUIRED_KEY)
    @EmailPattern(message = EMAIL_TEXT_VALIDATION_INVALID_KEY)
    @Pattern(regexp="^(.+)@(.+)$", message=EMAIL_TEXT_VALIDATION_INVALID_KEY)
    
    /**
     * New email to change
     */
    private String newEmail; // New email to change

    @NotEmpty(message = EMAIL_TEXT_VALIDATION_REQ_CONF_KEY)

    /**
     * Email confirmation
     */
    private String confirmEmail; // Email confirmation

    public EmailData() {
    }

    public EmailData(@Pattern(regexp = "^(.+)@(.+)$", message = EMAIL_TEXT_VALIDATION_INVALID_KEY) String newEmail, String confirmEmail) {
        this.newEmail = newEmail;
        this.confirmEmail = confirmEmail;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public String getConfirmEmail() {
        return confirmEmail;
    }

    public void setConfirmEmail(String confirmEmail) {
        this.confirmEmail = confirmEmail;
    }
}
