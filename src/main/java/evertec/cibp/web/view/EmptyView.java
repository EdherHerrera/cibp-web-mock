package evertec.cibp.web.view;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.View;

public class EmptyView implements View {

	public String getContentType() {

		return "text/html";
	}

	public void render(Map model, HttpServletRequest request, HttpServletResponse response) throws Exception {

	}

}
