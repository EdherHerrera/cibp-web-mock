package evertec.cibp.web.view;

import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.web.servlet.View;

import com.eloan.util.captcha.ImageCaptcha;

public class CaptchaView implements View {

	public String getContentType() {
		return "image/jpg";
	}

	public void render(Map model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		ImageCaptcha captcha = (ImageCaptcha) model.get("captcha");
		ServletOutputStream outputStream = response.getOutputStream();
		captcha.getJpegImage();

		outputStream.write(captcha.getJpegImage());
		IOUtils.closeQuietly(outputStream);

	}

}
