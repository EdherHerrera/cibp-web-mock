package evertec.cibp.web.view;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;
import org.springframework.web.servlet.View;

import bppr.tv.beans.Owner;

import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

import evertec.cibp.core.models.account.TVFrontendAccount;

public class SignaturecardView implements View {

	private static final String respondHeader = "attachment; filename=" + "signaturecard.pdf";

	private static final String contentPosition = "Content-disposition";

	public String getContentType() {
		return "application/pdf";
	}

	@SuppressWarnings("unchecked")
	public void render(Map model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		ESAPI.httpUtilities().setCurrentHTTP(request, response);

		ESAPI.httpUtilities().addHeader(response, "Content-disposition", "inline;filename=signaturecard.pdf");
		TVFrontendAccount account = null;
		TVFrontendAccount details = null;
		if(model.get("account") instanceof TVFrontendAccount ){
			account = (TVFrontendAccount) model.get("account");
		}
		if(model.get("accountdetails") instanceof TVFrontendAccount) {
			details = (TVFrontendAccount) model.get("accountdetails");
		}

		ESAPI.httpUtilities().setContentType();

		ESAPI.httpUtilities().setHeader(contentPosition,respondHeader);
		PdfReader reader = new PdfReader("signaturecard.pdf");

		PdfStamper stamper = new PdfStamper(reader, ESAPI.httpUtilities().getCurrentResponse().getOutputStream());
		AcroFields form = stamper.getAcroFields();
		form.setFieldProperty("AcctTp", "textsize", 30, null);
		form.setField("AcctNo", account.getAccountNumber());
		Owner[] owners = details.getTVAccount().getRecords().getOwners();

		form.setField("AcctName", owners[0].getLegalName());
		form.setField("Branch", account.getAccountNumber().substring(0, 3));
		form.setField("NoBnk", account.getBankId());
		int i = 0;
		for (Owner owner : owners) {
			i++;
			form.setField("Name" + i, owner.getLegalName());// owner.getPerson().getPersonName().getFirstName()+" "+owner.getPerson().getPersonName().getLastName());
			String taxID = owner.getTinInfo().getTaxID();
			form.setField("SocSec" + i, "XXX-XX-" + taxID.substring(taxID.length() - 4, taxID.length()));
		}
		form.setField("CSign", String.valueOf(i));
		form.setField("AcctTp", "081 E-ACCOUNT");
		form.setField("Date", SimpleDateFormat.getInstance().format(Calendar.getInstance().getTime()));
		form.setField("Trans", "CUENTA NUEVA");
		form.getFieldCache();
		stamper.setFormFlattening(true);
		stamper.close();

	}

}