package evertec.cibp.web.view;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.velocity.tools.generic.NumberTool;

import evertec.cibp.core.models.pfm.PFMAccountWrapper;
import evertec.cibp.core.models.pfm.PFMCategoryWrapper;
import evertec.cibp.core.models.pfm.PFMSortOrderWrapper;
import evertec.cibp.core.models.pfm.PFMTagWrapper;
import evertec.cibp.core.models.pfm.PFMTransactionRecordWrapper;
import evertec.cibp.core.models.pfm.PFMTransactionWrapper;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.session.UserSession;
import org.owasp.esapi.ESAPI;

public class PfmCsvFileView implements org.springframework.web.servlet.View {

	public String getContentType() {
		return "application/binary";
	}

	@SuppressWarnings("unchecked")
	public void render(Map map, HttpServletRequest request, HttpServletResponse response) throws Exception {

		ESAPI.httpUtilities().addHeader(response, "Content-Type", getContentType());

		// Get the model items
		PFMTransactionRecordWrapper record = (PFMTransactionRecordWrapper) map.get("record");
		PFMAccountWrapper pfmAccount = (PFMAccountWrapper) map.get("pfmAccount");
		String startDate = (String) map.get("startDate");
		String endDate = (String) map.get("endDate");
		String tagId = (String) map.get("tagId");
		String categoryId = (String) map.get("categoryId");
		UserSession userSession = (UserSession) map.get("userSession");
		CustomerServices customerService = (CustomerServices) map.get("customerService");

		boolean isEnglish = userSession.getLanguage().equalsIgnoreCase("en");

		try {
			// Header
			String accountNumber = (pfmAccount.getAccountId().endsWith("CARD") ? pfmAccount.getAccountId().substring(0,
					16) : pfmAccount.getAccountId().substring(0, 9));
			PrintWriter writer = response.getWriter();
			ESAPI.httpUtilities().addHeader(response, "Content-disposition", "inline;filename=EXPORT.CSV");
			StringBuffer buff = new StringBuffer();

			if (isEnglish)
				buff.append("Account,Start Date,End Date,Tag,Category\n");
			else
				buff.append("Cuenta,Fecha inicial,Fecha final,Etiqueta,Categoria\n");

			buff.append("x" + accountNumber.substring(accountNumber.length() - 4));
			buff.append("," + startDate + "," + endDate + ",");

			if (tagId.equals("0")) {
				if (isEnglish)
					buff.append("All,");
				else
					buff.append("Todas,");
			} else {
				for (Iterator<PFMTagWrapper> iterator = userSession.getPfmTags().iterator(); iterator.hasNext();) {
					PFMTagWrapper tag = (PFMTagWrapper) iterator.next();
					if (tag.getTagId() == Integer.parseInt(tagId)) {
						if (isEnglish)
							buff.append(tag.getDescription_en() + ",");
						else
							buff.append(tag.getDescription_es() + ",");
					}
				}
			}

			if (categoryId.equals("0")) {
				if (isEnglish)
					buff.append("All");
				else
					buff.append("Todas");
			} else {
				for (Iterator<PFMCategoryWrapper> iterator = userSession.getPfmCategories().iterator(); iterator
						.hasNext();) {
					PFMCategoryWrapper cat = (PFMCategoryWrapper) iterator.next();
					if (cat.getCategoryId() == Integer.parseInt(categoryId)) {
						if (isEnglish)
							buff.append(cat.getDescription_en());
						else
							buff.append(cat.getDescription_es());
					}
				}
			}
			buff.append("\n\n\n");

			if (isEnglish)
				buff.append("Date,Description,Category,Amount\n");
			else
				buff.append("Fecha,Descripcion,Categoria,Cantidad\n");

			writer.write(buff.toString());
			writer.flush();

			// How many pages is the record?
			int pages = (record.getTotalRecords() / record.getPagingRecords())
					+ ((record.getTotalRecords() % record.getPagingRecords() > 0 ? 1 : 0));

			// If it is only one, output these records
			if (pages == 1) {
				writer.write(processRecords(record).toString());
				writer.flush();
			} else {
				// If it is more than one page, get 100 transactions at a time,
				// starting with the current record
				int page = 0;
				boolean recordsExhausted = false;

				PFMSortOrderWrapper sortOrder = new PFMSortOrderWrapper();
				sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);

				while (!recordsExhausted) {
					page++;
					PFMTransactionRecordWrapper pfmTransactionRecord = customerService.getPfmTransactionsByAccount(
							userSession.getPfmId(), pfmAccount, tagId, categoryId, startDate, endDate, page, 100,
							sortOrder);
					writer.write(processRecords(pfmTransactionRecord).toString());
					writer.flush();

					if ((pfmTransactionRecord.getCurrentPage() * pfmTransactionRecord.getPagingRecords()) >= pfmTransactionRecord
							.getTotalRecords())
						recordsExhausted = true;
				}
			}

			writer.close();
		} catch (Exception e) {
			System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
		}
	}

	private StringBuffer processRecords(PFMTransactionRecordWrapper record) {
		NumberTool numberTool = new NumberTool();
		StringBuffer buff2 = new StringBuffer();

		for (Iterator<PFMTransactionWrapper> iterator = record.getPfmTransactions().iterator(); iterator.hasNext();) {
			PFMTransactionWrapper trx = (PFMTransactionWrapper) iterator.next();
			buff2.append(trx.getPostedDate_en() + ",");
			buff2.append(trx.getDescription() + ",");
			buff2.append(trx.getSubcategoryDescription_en() + ",");
			if (!trx.getCreditAmount().equals("0"))
				buff2.append(numberTool.format("currency", trx.getCreditAmount()).replace(",", ""));
			else
				buff2.append("-" + numberTool.format("currency", trx.getDebitAmount()).replace(",", ""));
			buff2.append("\n");
		}
		return buff2;
	}
}
