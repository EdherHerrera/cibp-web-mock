package evertec.cibp.web.view;

import java.util.Map;
import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.session.UserSession;
import evertec.otherservices.beans.stmt.OSStmtPDF;

public class eStatementPDFView implements org.springframework.web.servlet.View {

	private static final Logger logger = Logger.getLogger(eStatementPDFView.class);

	public String getContentType() {
		return "application/pdf";
	}

	@SuppressWarnings("rawtypes")
	public void render(Map map, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			response.setContentType(getContentType());
			ServletOutputStream writer = response.getOutputStream();
			response.setHeader("Content-disposition", "attachment;filename=file.pdf");
			response.setHeader("Cache-Control", "maxage=3600");
			response.setHeader("Pragma", "public");
			UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			userSession.setCurrentVaultStatements(null);// Reset GreenBox
			userSession.setCcaCurrentStatements(null);
			userSession.setDdaCurrentStatements(null);
			Boolean eBillsFlag = (Boolean) map.get("eBillsFlag");
			if (eBillsFlag != null) {
				OSStmtPDF interestPdf = (OSStmtPDF) map.get("interestPdf");
				if (interestPdf != null) {
					ESAPI.httpUtilities().addHeader(response, "Accept-Header", String.valueOf(interestPdf.getPdfImage().length));
					writer.write(interestPdf.getPdfImage());
					writer.flush();
				}
			} else {

				CustomerServices customerService = userSession.getCustomerService();

				String pdfId = (String) map.get("pdfId");
				if (pdfId != null) {
					OSStmtPDF interestPdf = customerService.getStatementPDF(pdfId);
					if (interestPdf != null) {
						ESAPI.httpUtilities().addHeader(response, "Accept-Header", String.valueOf(interestPdf.getPdfImage().length));
						writer.write(interestPdf.getPdfImage());
						writer.flush();
					}
				}
			}
			writer.close();
		} catch (Exception e) {
			logger.error("Can't render pdf, reason: " + e.getMessage());
		}

	}

}
