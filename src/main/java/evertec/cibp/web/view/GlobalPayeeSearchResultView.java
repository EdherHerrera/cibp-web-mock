package evertec.cibp.web.view;

import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.View;

import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.index.globalpayee.SearchResult;

public class GlobalPayeeSearchResultView implements View {

	public void render(Map model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		SearchResult result = (SearchResult) model.get("searchResult");

		PrintWriter writer = response.getWriter();

		StringBuffer sb = new StringBuffer();

		GlobalPayee[] matches = result.getMatches(0);
		sb.append(result.getNumberOfMatches() + " total matches<br><br>");

		writer.write(sb.toString());
		writer.flush();

	}

	public String getContentType() {

		return "text/html";
	}

}
