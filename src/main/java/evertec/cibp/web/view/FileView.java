package evertec.cibp.web.view;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.transactions.Transaction;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractStatement;
import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.models.customer.CustomerActions;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.TransactionRequest;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * FileView
 * 
 * @author et55596
 * @since 1.2
 * @version 1.0
 */
public class FileView implements org.springframework.web.servlet.View {

	/** constant dateFormat **/
	public SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

	/** constant INTUIT_ORG_POS **/
	private static int INTUIT_ORG_POS = 0;

	/** constant INTUIT_BID_POS **/
	private static int INTUIT_BID_POS = 1;

	/** constant INTUIT_FID_POS **/
	private static int INTUIT_FID_POS = 2;

	/**
	 * getContentType
	 * 
	 * @return application/binary
	 */
	public String getContentType() {
		return "application/binary";
	}

	public synchronized String synchronizedFormatter(Date date) {
		return dateFormat.format(date);
	}

	@SuppressWarnings("unchecked")
	public void render(Map map, HttpServletRequest request, HttpServletResponse response) throws Exception {

		response.setContentType(getContentType());

		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyyMMddHHmmss");
		TimeZone timezone = TimeZone.getTimeZone("UTC");
		simpledateformat.setTimeZone(timezone);
		BigDecimal endbl = new BigDecimal(0.0d);
		int totalPages = 1;
		int pagereq = 1;

		TVFrontendAccount account = (TVFrontendAccount) map.get("account");
		List<AbstractStatement> availableStatements = (List<AbstractStatement>) map.get("availableStatements");
		String searchType = (String) map.get("searchType");
		String cycle = (String) map.get("cycle");
		Calendar startDate = (Calendar) map.get("startDate");
		Calendar endDate = (Calendar) map.get("endDate");
		int maxTransactions = (Integer) map.get("maxTransactions");
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		CustomerServices customerService = userSession.getCustomerService();
		BankingSession bankingSession = userSession.getBankingSession();
		CustomerProfile customerProfile = userSession.getCustomerProfile();

		String expType = (String) map.get("expType");
		PrintWriter writer = response.getWriter();

		if (expType != null) {
			if (expType.equals("EXC")) {
				// writer = new
				// PrintWriter("EXPORT.CSV");//response.getWriter();
				ESAPI.httpUtilities().setHeader(response, "Content-disposition", "inline;filename=EXPORT.CSV");
				StringBuffer buff = new StringBuffer();
				buff.append("Date,Description,Amount\n");
				writer.write(buff.toString());
				writer.flush();
			} else if (expType.equals("QIF")) {
				// writer = new PrintWriter("EXPORT.QIF");
				ESAPI.httpUtilities().setHeader(response, "Content-disposition", "inline;filename=EXPORT.QIF");
				StringBuffer buff = new StringBuffer();
				buff.append("!Type:Bank\r\n");
				writer.write(buff.toString());
				writer.flush();
			} else if (expType.equals("OFX") || expType.equals("QFX") || expType.equals("QBO")) {

				ESAPI.httpUtilities().addHeader(response, "Content-disposition", "inline;filename=EXPORT." + expType);

				if (searchType != null && searchType.equals("ByStmt") && (cycle != null && !cycle.equals(""))
						&& !cycle.equals("1")) // Means No Current Transactions
				{
					AbstractStatement stm = getStatementById(cycle, availableStatements);
					if (startDate == null) {
						startDate = Calendar.getInstance();
						if (stm.getStartDate() != null)
							startDate.setTime(stm.getStartDate());
					}

					if (endDate == null) {
						endDate = Calendar.getInstance();
						if (stm.getEndDate() != null)
							endDate.setTime(stm.getEndDate());
					}
				} else {
					if (startDate == null)
						startDate = Calendar.getInstance();
					if (endDate == null)
						endDate = Calendar.getInstance();
				}
				map.put("startDate", startDate);
				map.put("endDate", endDate);
				StringBuffer buff = new StringBuffer();
				// This creates the header of the message before the
				// transactions
				buff = startExportOFX(map);

				writer.write(buff.toString());
				writer.flush();
			}

			TransactionsResponse transactionResponse = null;
			boolean bySpecificDate = false;
			boolean isCredit = account.getSubtype().equals("CCA");

			// Means no current transactions
			if (searchType.equals("ByStmt") && cycle != null && !cycle.equals("1")) {
				// get transactions by statement
				// get statements transactions
				AbstractStatement stm = getStatementById(cycle, availableStatements);

				while (pagereq <= totalPages) {
					TransactionRequest req = new TransactionRequest(Utils.generateCustomerInteractionEvent(request),
							stm, customerProfile, bankingSession, account, maxTransactions, pagereq, false, null, null,
							null);
					if (isCredit)
						transactionResponse = customerService.getCreditCardStatementTransactions(req);
					else
						transactionResponse = customerService.getDepositStatementTransactions(req);

					// if(transactionResponse.getStatusResult() !=
					// ResultStatus.SUCCESS)
					// return showForm(request, response, error);
					List<AbstractTransaction> transactions = transactionResponse.getTransactions();

					if (pagereq == 1) {
						totalPages = transactionResponse.getPagesAvailable();
					}

					if (transactions != null && transactions.size() > 0) {
						if (expType.equals("EXC")) {
							StringBuffer buffer = generateExcelExport(transactions);
							writer.write(ESAPI.encoder().decodeForHTML(buffer.toString()));
							writer.flush();
						} else if (expType.equals("QIF")) {
							StringBuffer buff = generateQuickenExport(transactions);
							writer.write(ESAPI.encoder().decodeForHTML(buff.toString()));
							writer.flush();
						} else if (expType.equals("OFX") || expType.equals("QFX") || expType.equals("QBO")) {
							Calendar str = Calendar.getInstance();
							str.setTime(stm.getStartDate());

							Calendar end = Calendar.getInstance();
							end.setTime(stm.getEndDate());
							if (stm.getEndBalance() != null) {
								endbl = stm.getEndBalance();
								if (isCredit) {
									BigDecimal factor = new BigDecimal(-1);
									endbl = endbl.multiply(factor);
								}
							}

							StringBuffer buff = addExportTransactions(transactions, simpledateformat,
									account.getAccountNumber());
							writer.write(ESAPI.encoder().decodeForHTML(buff.toString()));
							writer.flush();
						}
					}
					pagereq++;
				} // end while

			} else {
				// SearchType == ByDate or Current
				// get current transactions or date transactions
				// TODO buscar si la fecha de startdate esta setada y cuantos
				// estados hay que buscar.
				while (pagereq <= totalPages) {
					TransactionRequest depreq2 = null;
					// CIBP-1465 bgarcia
					if (!cycle.equals("1") && startDate != null && endDate != null
							&& (!"OFX".equalsIgnoreCase(expType) || (("OFX".equalsIgnoreCase(expType)
									|| "QBO".equalsIgnoreCase(expType) || "QFX".equalsIgnoreCase(expType))
									&& !"ByStmt".equalsIgnoreCase(searchType)))) {
						depreq2 = new TransactionRequest(Utils.generateCustomerInteractionEvent(request), null,
								customerProfile, bankingSession, account, maxTransactions, pagereq, false, startDate,
								endDate, null);
						bySpecificDate = true;
					} else
						depreq2 = new TransactionRequest(Utils.generateCustomerInteractionEvent(request), null,
								customerProfile, bankingSession, account, maxTransactions, pagereq, false, null, null,
								null);

					if (isCredit)
						transactionResponse = customerService.getCreditCardCurrentTransactions(depreq2);
					else
						transactionResponse = customerService.getDepositCurrentTransactions(depreq2);

					if (pagereq == 1) {
						totalPages = transactionResponse.getPagesAvailable();
					}
					List<AbstractTransaction> transactions = transactionResponse.getTransactions();
					if (bySpecificDate && (transactions == null || transactions.size() == 0)) {
						break;
					}
					if (transactions != null && transactions.size() > 0) {
						if (expType.equals("EXC")) {
							StringBuffer buff = generateExcelExport(transactions);
							writer.write(ESAPI.encoder().decodeForHTML(buff.toString()));
							writer.flush();
						} else if (expType.equals("QIF")) {
							StringBuffer buff = generateQuickenExport(transactions);
							writer.write(ESAPI.encoder().decodeForHTML(buff.toString()));
							writer.flush();
						} else if (expType.equals("OFX") || expType.equals("QFX") || expType.equals("QBO")) {
							StringBuffer buff = addExportTransactions(transactions, simpledateformat,
									account.getAccountNumber());
							writer.write(ESAPI.encoder().decodeForHTML(buff.toString()));
							writer.flush();
						}
					}
					pagereq++;
				} // end while

			}

			if (expType != null && (expType.equals("OFX") || expType.equals("QFX") || expType.equals("QBO"))) {

				StringBuffer buff = new StringBuffer();
				if (account.getSubtype().equals("IDA") || account.getSubtype().equals("PFC"))
					buff = endexportOfxOther(simpledateformat, endDate, endbl);
				else
					buff = endexportOfxCredit(simpledateformat, endDate, endbl);
				writer.write(buff.toString());
				writer.flush();
			}

			writer.close();

			// Log usage statistics
			CustomerActions act = null;
			if (expType.equals("EXC"))
				act = CustomerActions.EXPORT_EXCEL_FORMAT;
			else if (expType.equals("OFX"))
				act = CustomerActions.EXPORT_MONEY_FORMAT;
			else if (expType.equals("QIF"))
				act = CustomerActions.EXPORT_QUICKEN_FORMAT;
			else if (expType.equals("QBO"))
				act = CustomerActions.EXPORT_QUICKBOOKS_FORMAT;

			if (act != null)
				userSession.getCustomerService().logCustomerUsageData(userSession.getCustomerProfile(), act);

		}

	}

	/**
	 * getStatementById
	 * 
	 * @param id         String
	 * @param statements List
	 * @return AbstractStatement
	 */
	public AbstractStatement getStatementById(String id, List<AbstractStatement> statements) {
		for (Iterator<AbstractStatement> iterator = statements.iterator(); iterator.hasNext();) {
			AbstractStatement act = (AbstractStatement) iterator.next();
			if (act.getFrontEndId().equals(id))
				return act;
		}
		return null;
	}

	/**
	 * cleanText
	 * 
	 * @param text String
	 * @return String
	 */
	private String cleanText(String text) {
		if (text == null) {
			return "";
		} else {
			String value = text.trim();

			value = value.replaceAll("\u00e1", "a");
			value = value.replaceAll("\u00e9", "e");
			value = value.replaceAll("\u00ed", "i");
			value = value.replaceAll("\u00f3", "o");
			value = value.replaceAll("\u00fa", "u");
			value = value.replaceAll("\u00fc", "u");
			value = value.replaceAll("\u00f1", "n");
			value = value.replaceAll("\u00d1", "N");
			value = value.replaceAll("\u00c1", "A");
			value = value.replaceAll("\u00c9", "E");
			value = value.replaceAll("\u00cd", "I");
			value = value.replaceAll("\u00d3", "O");
			value = value.replaceAll("\u00dc", "U");
			value = value.replaceAll("\u00da", "U");

			value = value.replaceAll("#", "");
			value = value.replaceAll("&", "");
			value = value.replaceAll("]", "");
			value = value.replaceAll("@", "");
			value = value.replaceAll("'", "");
			value = value.replaceAll(">", "");
			value = value.replaceAll("<", "");

			// value = value.replaceAll("[^\\sa-zA-Z0-9#\\&\\-\\.\\,@']", "");
			// value = value.replaceAll("[^\\s#\\&\\-\\.\\,@']", "");
			// value = CharSetUtils.squeeze(value, " ");
			return value;
		}
	}

	/**
	 * generateExcelExport
	 * 
	 * @param transactions List
	 * @return StringBuffer
	 */
	public StringBuffer generateExcelExport(List<AbstractTransaction> transactions) {

		try {
			StringBuffer buffer = new StringBuffer();
			for (Iterator<AbstractTransaction> iterator = transactions.iterator(); iterator.hasNext();) {
				AbstractTransaction name = (AbstractTransaction) iterator.next();

				// Adding transaction date, description and amount
				buffer.append(this.synchronizedFormatter(name.getDate().getTime()));
				buffer.append(",");
				if (name.getDescription() != null)
					buffer.append("\"" + cleanText(name.getDescription()) + "\"");
				else
					buffer.append("\"\"");

				buffer.append(",");

				if (name.getAmount() != null) {
					if (name.getType().equals("DB"))
						buffer.append("-");

					buffer.append(name.getAmount());
				} else
					buffer.append("0");
				buffer.append("\n");
			}
			return buffer;
		} catch (Exception e) {
			// TODO definir que hacer en caso de una exception
			// e.printStackTrace();
			return new StringBuffer();

		}
	}

	/**
	 * generateQuickenExport
	 * 
	 * @param transactions List
	 * @return StringBuffer
	 */
	public StringBuffer generateQuickenExport(List<AbstractTransaction> transactions) {

		try {

			StringBuffer buffer = new StringBuffer();
			for (Iterator<AbstractTransaction> iterator = transactions.iterator(); iterator.hasNext();) {
				AbstractTransaction tran = (AbstractTransaction) iterator.next();

				if (tran.getDescription() != null
						&& tran.getDescription().toUpperCase().startsWith(new String("In Process").toUpperCase()))
					continue;

				if (tran.getDate() != null) {
					buffer.append('D');
					buffer.append(this.synchronizedFormatter(tran.getDate().getTime()));
					buffer.append("\r\n");
				} else {
					buffer.append('D');
					buffer.append("");
					buffer.append("\r\n");
				}
				if (tran.getDescription() != null) {
					String tempDesc = tran.getDescription();
					tempDesc = cleanText(tempDesc);
					buffer.append('P');
					buffer.append(tempDesc);
					buffer.append("\r\n");
					buffer.append('M');
					buffer.append(tempDesc);
					buffer.append("\r\n");
				}
				// TODO define in core
				String s = ((Transaction) tran.getBackendObject()).getCheckNumber();
				if (s != null && s.length() > 0 && tran.getDescription().indexOf("DEPOSIT") == -1) {
					buffer.append('N');
					buffer.append(s);
					buffer.append("\r\n");
				}
				buffer.append('T');
				if (tran.getType().equals("DB"))
					buffer.append("-");
				buffer.append(tran.getAmount());
				buffer.append("\r\n^\r\n");

			}
			return buffer;

		} catch (Exception e) {
			// TODO definir que hacer en caso de una exception
			// e.printStackTrace();
			return new StringBuffer();

		}
	}

	/**
	 * endexportOfxCredit
	 * 
	 * @param simpledateformat SimpleDateFormat
	 * @param endDate          Calendar
	 * @param closeBalance     BigDecimal
	 * @return StringBuffer
	 */
	private StringBuffer endexportOfxCredit(SimpleDateFormat simpledateformat, Calendar endDate,
			BigDecimal closeBalance) {

		StringBuffer stringbuffer = new StringBuffer();

		stringbuffer.append("</BANKTRANLIST>\r\n");
		stringbuffer.append("<LEDGERBAL>\r\n");
		if (closeBalance == null)
			stringbuffer.append("<BALAMT>0\r\n");
		else
			stringbuffer.append("<BALAMT>" + closeBalance + "\r\n");
		stringbuffer.append("<DTASOF>" + simpledateformat.format(endDate.getTime()) + "\r\n");
		stringbuffer.append("</LEDGERBAL>\r\n");
		stringbuffer.append("</CCSTMTRS>\r\n");
		stringbuffer.append("</CCSTMTTRNRS>\r\n");
		stringbuffer.append("</CREDITCARDMSGSRSV1>\r\n");
		stringbuffer.append("</OFX>\r\n");

		return stringbuffer;

	}

	/**
	 * startExportOFX
	 * 
	 * @param map Map
	 * @return StringBuffer
	 */
	private StringBuffer startExportOFX(Map<Object, Object> map) {

		TVFrontendAccount account = (TVFrontendAccount) map.get("account");
		Calendar startDate = (Calendar) map.get("startDate");
		Calendar endDate = (Calendar) map.get("endDate");
		String exportType = (String) map.get("expType");

		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyyMMddHHmmss");
		simpledateformat.setTimeZone(TimeZone.getTimeZone("UTC"));
		GregorianCalendar gregoriancalendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));

		String s = simpledateformat.format(gregoriancalendar.getTime());

		boolean isCredit = !(account.getSubtype().equals("IDA") || account.getSubtype().equals("PFC"));
		StringBuffer stringbuffer = new StringBuffer();

		stringbuffer.append("OFXHEADER:100\r\n");
		stringbuffer.append("DATA:OFXSGML\r\n");
		stringbuffer.append("VERSION:102\r\n");
		stringbuffer.append("SECURITY:NONE\r\n");
		stringbuffer.append("ENCODING:USASCII\r\n");
		stringbuffer.append("CHARSET:1252\r\n");
		stringbuffer.append("COMPRESSION:NONE\r\n");
		stringbuffer.append("OLDFILEUID:NONE\r\n");
		stringbuffer.append("NEWFILEUID:NONE\r\n");
		stringbuffer.append("\r\n");
		stringbuffer.append("<OFX>\r\n");
		stringbuffer.append("<SIGNONMSGSRSV1>\r\n");
		stringbuffer.append("<SONRS>\r\n");
		stringbuffer.append("<STATUS>\r\n");
		stringbuffer.append("<CODE>0\r\n");
		stringbuffer.append("<SEVERITY>INFO\r\n");
		stringbuffer.append("</STATUS>\r\n");
		stringbuffer.append("<DTSERVER>" + s + "\r\n");
		stringbuffer.append("<LANGUAGE>ENG\r\n");

		if (exportType.equals("QFX") || exportType.equals("QBO")) {
			String intuitCredentials = (String) map.get("intuitCredentials");
			String[] credentials = intuitCredentials.split(",");
			stringbuffer.append("<FI>\r\n");
			stringbuffer.append("<ORG>" + credentials[INTUIT_ORG_POS] + "\r\n");
			stringbuffer.append("<FID>" + credentials[INTUIT_FID_POS] + "\r\n");
			stringbuffer.append("</FI>\r\n");
			stringbuffer.append("<INTU.BID>" + credentials[INTUIT_BID_POS] + "\r\n");
		}

		stringbuffer.append("</SONRS>\r\n");
		stringbuffer.append("</SIGNONMSGSRSV1>\r\n");

		if (isCredit) {
			stringbuffer.append("<CREDITCARDMSGSRSV1>\r\n");
			stringbuffer.append("<CCSTMTTRNRS>\r\n");
		} else {
			stringbuffer.append("<BANKMSGSRSV1>\r\n");
			stringbuffer.append("<STMTTRNRS>\r\n");
		}

		if (exportType.equals("QFX") || exportType.equals("QBO"))
			stringbuffer.append("<TRNUID>0\r\n");
		else
			stringbuffer.append("<TRNUID>1\r\n");

		stringbuffer.append("<STATUS>\r\n");
		stringbuffer.append("<CODE>0\r\n");
		stringbuffer.append("<SEVERITY>INFO\r\n");
		stringbuffer.append("</STATUS>\r\n");

		if (isCredit)
			stringbuffer.append("<CCSTMTRS>\r\n");
		else
			stringbuffer.append("<STMTRS>\r\n");

		stringbuffer.append("<CURDEF>USD\r\n");

		if (isCredit) {
			stringbuffer.append("<CCACCTFROM>\r\n");
			stringbuffer.append("<ACCTID>" + account.getAccountNumber() + "\r\n");
			stringbuffer.append("</CCACCTFROM>\r\n");
			stringbuffer.append("<BANKTRANLIST>\r\n");
			stringbuffer.append("<DTSTART>" + simpledateformat.format(startDate.getTime()) + "\r\n");
			stringbuffer.append("<DTEND>" + simpledateformat.format(endDate.getTime()) + "\r\n");
		} else {
			stringbuffer.append("<BANKACCTFROM>\r\n");
			if (exportType.equals("QFX") || exportType.equals("QBO")) {
				stringbuffer.append("<BANKID>021502011\r\n");
			} else {
				stringbuffer.append(
						"<BANKID>" + ((Account) account.getBackendObject()).getBankInformation().getBankId() + "\r\n");
			}
			// Para poder cargar cuentas de ahorros en "Money" donde el numero
			// de
			// esta tambien se utiliza para el area de cheques.
			if (account.getSubtype().equals("IDA") && account.getAccountSection().equals("S") && (account
					.getAccountProductId().equals("021") || account.getAccountProductId().equals("023")
					|| account.getAccountProductId().equals("024") || account.getAccountProductId().equals("025")
					|| account.getAccountProductId().equals("026") || account.getAccountProductId().equals("027")
					|| account.getAccountProductId().equals("069") || account.getAccountProductId().equals("073")
					|| account.getAccountProductId().equals("080") || account.getAccountProductId().equals("086"))) {
				stringbuffer.append("<ACCTID>" + "SAV " + account.getAccountNumber() + "\r\n");
			} else if (account.getSubtype().equals("IDA") && account.getAccountSection().equals("R") && (account
					.getAccountProductId().equals("021") || account.getAccountProductId().equals("023")
					|| account.getAccountProductId().equals("024") || account.getAccountProductId().equals("025")
					|| account.getAccountProductId().equals("026") || account.getAccountProductId().equals("027")
					|| account.getAccountProductId().equals("069") || account.getAccountProductId().equals("073")
					|| account.getAccountProductId().equals("080") || account.getAccountProductId().equals("086"))) {
				stringbuffer.append("<ACCTID>" + "RES " + account.getAccountNumber() + "\r\n");
			} else {
				stringbuffer.append("<ACCTID>" + account.getAccountNumber() + "\r\n");
			}

			// ACCTTYPE = "CHECKING", "SAVINGS", "CREDITLINE","MONEYMRKT",
			// "CREDITCARD"
			if (account.getSubtype().equals("CCA"))
				stringbuffer.append("<ACCTTYPE>" + "CREDITCARD" + "\r\n");
			else if (account.getSubtype().equals("IDA") || account.getSubtype().equals("PFC")) {
				String ind = account.getAccountSection();
				if (ind.equals("C"))
					stringbuffer.append("<ACCTTYPE>" + "CHECKING" + "\r\n");
				else if (ind.equals("S"))
					stringbuffer.append("<ACCTTYPE>" + "SAVINGS" + "\r\n");
				else if (ind.equals("R"))
					stringbuffer.append("<ACCTTYPE>" + "CREDITLINE" + "\r\n");
				else
					stringbuffer.append("<ACCTTYPE>" + "CHECKING" + "\r\n");
			} else
				stringbuffer.append("<ACCTTYPE>" + "CHECKING" + "\r\n");

			stringbuffer.append("</BANKACCTFROM>\r\n");
			stringbuffer.append("<BANKTRANLIST>\r\n");
			stringbuffer.append("<DTSTART>" + simpledateformat.format(startDate.getTime()) + "\r\n");
			stringbuffer.append("<DTEND>" + simpledateformat.format(endDate.getTime()) + "\r\n");
		}

		return stringbuffer;
	}

	/**
	 * endexportOfxOther
	 * 
	 * @param simpledateformat SimpleDateFormat
	 * @param endDate          Calendar
	 * @param closeBalance     BigDecimal
	 * @return StringBuffer
	 */
	private StringBuffer endexportOfxOther(SimpleDateFormat simpledateformat, Calendar endDate,
			BigDecimal closeBalance) {
		StringBuffer stringbuffer = new StringBuffer();

		stringbuffer.append("</BANKTRANLIST>\r\n");
		stringbuffer.append("<LEDGERBAL>\r\n");
		if (closeBalance == null)
			stringbuffer.append("<BALAMT>0\r\n");
		else
			stringbuffer.append("<BALAMT>" + closeBalance + "\r\n");
		stringbuffer.append("<DTASOF>" + simpledateformat.format(endDate.getTime()) + "\r\n");
		stringbuffer.append("</LEDGERBAL>\r\n");
		stringbuffer.append("</STMTRS>\r\n");
		stringbuffer.append("</STMTTRNRS>\r\n");
		stringbuffer.append("</BANKMSGSRSV1>\r\n");
		stringbuffer.append("</OFX>\r\n");

		return stringbuffer;
	}

	/**
	 * addExportTransactions
	 * 
	 * @param transactions     List
	 * @param simpledateformat SimpleDateFormat
	 * @param accountNumber    String
	 * @return StringBuffer
	 */
	private StringBuffer addExportTransactions(List<AbstractTransaction> transactions,
			SimpleDateFormat simpledateformat, String accountNumber) {
		StringBuffer stringbuffer = new StringBuffer();

		for (Iterator<AbstractTransaction> iterator = transactions.iterator(); iterator.hasNext();) {

			AbstractTransaction transaction = (AbstractTransaction) iterator.next();
			if (transaction.getDescription() != null
					&& transaction.getDescription().toUpperCase().startsWith(new String("In Process").toUpperCase()))
				continue;

			stringbuffer.append("<STMTTRN>\r\n");
			stringbuffer.append("<TRNTYPE>");
			// { "CREDIT", "DEBIT", "INT", "DIV","FEE", "SRVCHG", "DEP", "ATM",
			// "POS", "XFER", "CHECK", "PAYMENT","CASH", "DIRECTDEP",
			// "DIRECTDEBIT", "REPEATPMT", "OTHER" };
			if (transaction.getType().equalsIgnoreCase("CR"))
				stringbuffer.append("CREDIT");
			else if (transaction.getType().equals("DB"))
				stringbuffer.append("DEBIT");
			else
				stringbuffer.append("OTHER");

			stringbuffer.append("\r\n");
			if (transaction.getDate() == null) {
				stringbuffer.append("<DTPOSTED>unknown\r\n");
			} else {
				stringbuffer.append("<DTPOSTED>");
				stringbuffer.append(simpledateformat.format(transaction.getDate().getTime()));
				stringbuffer.append("\r\n");
			}
			stringbuffer.append("<TRNAMT>");
			if (transaction.getType().equals("DB"))
				stringbuffer.append("-");

			stringbuffer.append(transaction.getAmount());
			stringbuffer.append("\r\n");

			stringbuffer.append("<FITID>");
			String id = ((Transaction) transaction.getBackendObject()).getTraceId();
			// String id = transaction.getDate().getTimeInMillis() + "" +
			// accountNumber + transaction.getAmount().toString().replace(".",
			// "") + transaction.getType();
			stringbuffer.append(id);
			stringbuffer.append("\r\n");

			String s = ((Transaction) transaction.getBackendObject()).getCheckNumber();
			if (s != null && s.length() > 0 && !s.startsWith("0000")) {
				stringbuffer.append("<CHECKNUM>");
				stringbuffer.append(s);
				stringbuffer.append("\r\n");
			}
			String s1 = transaction.getDescription();
			s1 = cleanText(s1);
			if (s1 != null && s1.length() > 0) {
				stringbuffer.append("<NAME>");
				String transactionName = s1.trim();
				if (transactionName != null && transactionName.length() > 32)
					transactionName = transactionName.substring(0, 31);
				stringbuffer.append(transactionName);
				stringbuffer.append("\r\n");
				stringbuffer.append("<MEMO>");
				stringbuffer.append(s1);
				stringbuffer.append("\r\n");
			}
			stringbuffer.append("</STMTTRN>\r\n");
		}
		return stringbuffer;
	}
}
