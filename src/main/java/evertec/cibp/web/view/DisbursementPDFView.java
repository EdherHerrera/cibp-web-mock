package evertec.cibp.web.view;

import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.util.WebUtils;

import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.facade.DocumentsFacade;
import evertec.cibp.web.session.UserSession;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.GetDocumentRequest;
import evertec.otherservices.responses.GetDocumentResp;

public class DisbursementPDFView implements org.springframework.web.servlet.View {

	public String getContentType() {
		return "application/pdf";
	}

	@SuppressWarnings("unchecked")
	public void render(Map map, HttpServletRequest request, HttpServletResponse response) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();

		String docId = (String) map.get("docId");
		if (docId != null) {

			GetDocumentRequest documentumRequest = new GetDocumentRequest();
			documentumRequest.setDocumentId(docId);
			GetDocumentResp documentumResponse = DocumentsFacade.getInstance().getDocumentFile(documentumRequest,userSession);

			if (documentumResponse.getResponseStatus() == ResponseStatus.SUCCESS) {
				response.setContentType(getContentType());
				ServletOutputStream writer = response.getOutputStream();
				String docName = map.get("docName").toString().replace(" ", "_").replace(",", "").replace(".", "");
				response.setHeader("Content-disposition", "attachment;filename=" + docName + ".pdf");
				response.setHeader("Cache-Control", "maxage=3600");
				response.setHeader("Pragma", "public");
				response.addHeader("Accept-Header", String.valueOf(documentumResponse.getDocumentByteArray().length));
				writer.write(documentumResponse.getDocumentByteArray());
				writer.flush();
				writer.close();
			}
		}
	}

}
