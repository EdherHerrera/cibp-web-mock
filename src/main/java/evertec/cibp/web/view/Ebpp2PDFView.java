package evertec.cibp.web.view;

import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.util.WebUtils;

import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.session.UserSession;
import evertec.otherservices.requests.GetStatementReq;
import evertec.otherservices.responses.GetStatementResp;

public class Ebpp2PDFView implements org.springframework.web.servlet.View {

	public String getContentType() {
		return "application/pdf";
	}

	@SuppressWarnings("unchecked")
	public void render(Map map, HttpServletRequest request, HttpServletResponse response) throws Exception {

		response.setContentType(getContentType());
		ServletOutputStream writer = response.getOutputStream();
		response.setHeader("Content-disposition", "attachment;filename=file.pdf");
		response.setHeader("Cache-Control", "maxage=3600");
		response.setHeader("Pragma", "public");

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();

		String statementId = (String) map.get("id");
		String billerAccountId = (String) map.get("uai");
		if (statementId != null && billerAccountId != null) {

			GetStatementReq req = new GetStatementReq();
			req.setBillerAccountId(Integer.parseInt(billerAccountId));
			req.setStatementId(Integer.parseInt(statementId));
			req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
			GetStatementResp getStatementResp = customerService.getEbpp2Statement(req);

			if (getStatementResp != null && getStatementResp.getStatement() != null
					&& getStatementResp.getStatement().getPdfImage() != null) {

				response.addHeader("Accept-Header",
						String.valueOf(getStatementResp.getStatement().getPdfImage().length));
				writer.write(getStatementResp.getStatement().getPdfImage());
				writer.flush();
			}
		}
		writer.close();
	}

}
