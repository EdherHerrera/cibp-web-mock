package evertec.cibp.web.signon;

import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.web.LoginCallController;
import evertec.cibp.web.form.LoginPendingEnrollForm;
import evertec.cibp.web.form.LoginPinCvvForm;
import evertec.cibp.web.form.LoginRsaEnrollForm;
import evertec.cibp.web.form.LoginRsaEnrollFromOAOForm;
import evertec.cibp.web.form.LoginSSDSForcedForm;
import evertec.cibp.web.form.OOBChallengeForm;
import evertec.cibp.web.form.PasswordForm;
import evertec.cibp.web.form.QuestionChallengeForm;
import evertec.cibp.web.form.SessionInitializationController;
import evertec.cibp.web.form.interruptions.InterruptionForm;
import evertec.cibp.web.session.UserSession;

public class SignonSequenceUtils {

	public static String getNextView(UserSession userSession) {

		if (userSession.getTasksList().size() == 0) {
			return "login";
		}

		ASK_FOR ask = userSession.getTasksList().get(0);

		return getAskView(ask);
	}

	public static String getAskView(ASK_FOR ask) {
		switch (ask) {
		case SESSION_INIT:
			return "redirect:welcome";
		case QUESTION_CHALLENGE:
			return "redirect:question";
		case DUMMY_QUESTIONS:
			return "redirect:question";
		case OOB_CHALLENGE:
			return "redirect:loginoob";
		case RECOVERY_CODE:
			return "redirect:loginoob";
		case CSR_ACTION:
			return "redirect:logincall";
		case VALIDATE_PASSWORD:
			return "redirect:password";
		case SSDS_FORCED_LOGIN:
			return "redirect:loginSsdsForced";
		case RSA_ENROLL:
			return "redirect:loginRsaEnroll";

		case RSA_ENROLL_USERNAME:
			return "redirect:loginSsdsForced";
		case RSA_ENROLL_FROM_OAO:
			return "redirect:loginRsaEnrollFromOAO";

			// DOB O VALIDATE_ENROLLACCOUNT are used in pending enrollment
		case VALIDATE_DOB:
			return "redirect:loginPendEnroll";
		case VALIDATE_ENROLLACCOUNT:
			return "redirect:loginPendEnroll";
		case VALIDATE_PIN:
			return "redirect:loginPinCvv";
		case LOGIN_INTERRUPTION_PAGE:
			return "loginInterruptionPage";
		default:
			return "error";
		}
	}

	public static Class getAskClass(ASK_FOR ask) {

		switch (ask) {
		case SESSION_INIT:
			return SessionInitializationController.class;
		case QUESTION_CHALLENGE:
			return QuestionChallengeForm.class;
		case DUMMY_QUESTIONS:
			return QuestionChallengeForm.class;
		case OOB_CHALLENGE:
			return OOBChallengeForm.class;
		case RECOVERY_CODE:
			return OOBChallengeForm.class;
		case CSR_ACTION:
			return LoginCallController.class;
		case VALIDATE_PASSWORD:
			return PasswordForm.class;
		case SSDS_FORCED_LOGIN:
			return LoginSSDSForcedForm.class;
		case RSA_ENROLL:
			return LoginRsaEnrollForm.class;

		case RSA_ENROLL_USERNAME:
			return LoginSSDSForcedForm.class;

		case RSA_ENROLL_FROM_OAO:
			return LoginRsaEnrollFromOAOForm.class;

			// DOB O VALIDATE_ENROLLACCOUNT are used in pending enrollment
		case VALIDATE_DOB:
			return LoginPendingEnrollForm.class;
		case VALIDATE_ENROLLACCOUNT:
			return LoginPendingEnrollForm.class;
		case VALIDATE_PIN:
			return LoginPinCvvForm.class;
		case LOGIN_INTERRUPTION_PAGE:
			return InterruptionForm.class;
		default:
			return null;

		}

	}

}
