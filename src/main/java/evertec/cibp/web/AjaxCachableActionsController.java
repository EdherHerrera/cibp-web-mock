package evertec.cibp.web;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.owasp.esapi.ESAPI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.campaign.ActiveCampaignView;
import evertec.cibp.core.models.campaign.Campaign;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.campaign.LoginCampaign;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.resolver.types.AjaxError;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.LoginCampaignSort;
import evertec.cibp.web.utils.Utils;

public class AjaxCachableActionsController extends MultiActionController implements AjaxError {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	protected CustomerServices customerService;
	private Integer maxNumberCampaign;

	public ModelAndView logincampaign(HttpServletRequest request, HttpServletResponse response) throws Exception {

		/**
		 * 0. a.getCampaign().getUploadedByUser(); 1.
		 * a.getCampaign().getCampaignName(); 2. a.getCampaign().getStartDate();
		 * 3. a.getCampaign().getEndDate(); 4. a.getCampaign().getImageId();
		 * else a.getCampaign().getId();
		 */
//		Random r = new Random();
		// int sortby = Math.abs(r.nextInt()) % 5; // According to google, could
		// be negative
		int sortby = ESAPI.randomizer().getRandomInteger(0, 5);
//		int sortby = r.nextInt(Integer.MAX_VALUE) % 5;
		// System.out.println(sortby);
		Integer maxNumber = maxNumberCampaign;

		List<ActiveCampaignView> list = customerService.getActiveCampaignByType(CampaignType.LOGIN_CAMPAIGN);
		List<LoginCampaign> lc = new java.util.LinkedList<LoginCampaign>();
		List<Campaign> fixedList = customerService.getRunnigCampaignByType(CampaignType.FIXED_LOGIN_CAMPAIGN);
		if (!fixedList.isEmpty()) {
			// Resto el fixed
			maxNumber -= 1;
		}
		for (Iterator<ActiveCampaignView> iterator = list.iterator(); iterator.hasNext();) {
			LoginCampaign loginCampaign = new LoginCampaign(iterator.next());
			if (loginCampaign != null && lc.size() < maxNumber)
				lc.add(loginCampaign);
		}
		boolean isEnglish = Utils.getRSALanguage(request).equalsIgnoreCase("en");

		Map<String, Object> model = new HashMap<>();
		model.put("isenglish", isEnglish);

		LoginCampaignSort sort = new LoginCampaignSort();
		sort.setSortby(sortby);
		Collections.sort(lc, sort);
		model.put("campaigns", lc);

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		if (userSession != null)
			userSession.setLogincampaigns(lc);

		return new ModelAndView("logincampaign", model);
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	/**
	 * @return the maxNumberCampaign
	 */
	public Integer getMaxNumberCampaign() {
		return maxNumberCampaign;
	}

	/**
	 * @param maxNumberCampaign
	 *            the maxNumberCampaign to set
	 */
	public void setMaxNumberCampaign(Integer maxNumberCampaign) {
		this.maxNumberCampaign = maxNumberCampaign;
	}
}
