package evertec.cibp.web;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.services.requests.TransactionRequest;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.AlertTypesUtils;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.HasSectionUtil;
import evertec.cibp.web.utils.ImageUtils;
import evertec.cibp.web.utils.PFMUtil;
import evertec.cibp.web.utils.PrivateBankerImageInfo;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.utils.DeviceUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.velocity.tools.generic.NumberTool;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.CookieGenerator;
import org.springframework.web.util.WebUtils;

import com.pingidentity.opentoken.Agent;
import com.pingidentity.opentoken.TokenException;
import com.pingidentity.opentoken.util.UrlHelper;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import bppr.tv.beans.Owner;
import bppr.tv.beans.accounts.AccountSubtypes;
import bppr.tv.beans.deposits.records.DepositItemImageRecord;
import bppr.tv.beans.deposits.records.DepositTicketImageRecord;
import edu.emory.mathcs.backport.java.util.Collections;
import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.accountopening.AccountApplication;
import evertec.cibp.core.models.accountopening.AccountApplications;
import evertec.cibp.core.models.accountopening.EaccountMessage;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.base.AbstractStatement;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerEntitlement4Hibernate;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerProfileLite;
import evertec.cibp.core.models.customer.CustomerWealthPreferred;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.applications.ApplicationStatus;
import evertec.cibp.core.models.customer.applications.CustomerApplication;
import evertec.cibp.core.models.customer.applications.CustomerApplicationType;
import evertec.cibp.core.models.customer.devices.CustomerDevice;
import evertec.cibp.core.models.customer.devices.DeviceStatus;
import evertec.cibp.core.models.customer.devices.ProductType;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.EStatementInteraction;
import evertec.cibp.core.models.documents.CIBPConfigurableTexts;
import evertec.cibp.core.models.documents.CIBPConfigurableTextsTypes;
import evertec.cibp.core.models.documents.ConfigurableTextSubSections;
import evertec.cibp.core.models.documents.CustomerDocuments;
import evertec.cibp.core.models.documents.CustomerDocumentsPackage;
import evertec.cibp.core.models.ebill.Ebpp2BillerProductIdResponse;
import evertec.cibp.core.models.enrollment.EnrollmentLiteStatus;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.BillPayEntitlement;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.entitlement.PFMEntitlement;
import evertec.cibp.core.models.entitlement.RDCEntitlement;
import evertec.cibp.core.models.entitlement.SMSEntitlement;
import evertec.cibp.core.models.fraud.prevention.IncomingChecksCustDecision;
import evertec.cibp.core.models.payee.GlobalPayeePaymentType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.remotedeposit.RemoteDepositHistory;
import evertec.cibp.core.models.signup.SignupCampaign;
import evertec.cibp.core.models.statements.AccountPDFStmtInfo;
import evertec.cibp.core.models.statements.AccountStmtInfo;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.DeleteSMSProfileRequest;
import evertec.cibp.core.services.requests.ModifyCustomerAlertsRequest;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.requests.opac.CISOpacCustomerInfoRequest;
import evertec.cibp.core.services.responses.CustomerDeviceResponse;
import evertec.cibp.core.services.responses.PaymentHistoryResponse;
import evertec.cibp.core.services.responses.SmsAlertsResponse;
import evertec.cibp.core.services.responses.SmsAlertsResponse.Result;
import evertec.cibp.core.services.responses.opac.CISOpacCustomerInfoResponse;
import evertec.cibp.core.services.smsAlerts.SmsBean;
import evertec.cibp.core.utils.FraudPreventionUtils;
import evertec.cibp.core.utils.InternationalCharacterUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.mobilecash.core.services.MobileCashServices;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.models.PaymentFilterType;
import evertec.cibp.web.facade.AccountsFacade;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.request.PaymentHistoryRequest;
import evertec.cibp.web.services.modules.SignOnCustomerServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.session.UserSessionType;
import evertec.cibp.web.view.EmptyView;
import evertec.dp.service.tvstatement.CreditCardStatement_Type;
import evertec.dp.service.tvstatement.DepositStatement_Type;
import evertec.otherservices.beans.AccountSummary;
import evertec.otherservices.beans.UserAccount;
import evertec.otherservices.beans.RDC.HistoryTransaction;
import evertec.otherservices.beans.RDC.RDCImages;
import evertec.otherservices.beans.RDC.RequestDirection;
import evertec.otherservices.beans.RDC.RequestSize;
import evertec.otherservices.beans.RDC.RequestUnit;
import evertec.otherservices.beans.stmt.OSStmtPDF;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.CreateUserReq;
import evertec.otherservices.requests.GetDepositHistoryReq;
import evertec.otherservices.requests.GetImagesReq;
import evertec.otherservices.requests.RegisterUserAccountReq;
import evertec.otherservices.responses.ImagesResp;
import evertec.otherservices.responses.RegisterUserAccountResp;
import evertec.otherservices.responses.TransactionHistoryResp;
import evertec.statementview.StatementViewService;
import evertec.statementview.beans.StatementViewRequest;
import evertec.statementview.beans.StatementViewResponse;
import evertec.zap.service.bean.EmployeeInfoResponse;

public class WebAppController extends MultiActionController implements MessageSourceAware {

    private static final Logger logger = Logger.getLogger(WebAppController.class);
    private MessageSource messageSource;
    /**
     * atributo <code>eaccountAgeLimit del tipo Integer</code> atributo que
     * define la edad minima a tener para optar por una eaccount, mpicado,
     * cibp-2137
     */
    private Integer eaccountMinAgeLimit;
    private String wealthCodes;
    private String preferredCodes;
    private PrivateBankerImageInfo privateBankerImg = null;
    private String telenominaID;
    private String vendorId;
    private String rdcBid;
    private boolean pfmAutomaticEntitlementCreation;
    private String[] ezStatementInformativaProductIds;
    private String[] ezStatementMortgageProductIds;
    private String ezStatementInformativaIRAQuarterlyProductId;
    private String ezStatementInformativaCAEQuarterlyProductId;
    private String ezStatementInformativaIRAYearlyProductId;
    private String ezStatementInformativaCAEYearlyProductId;
    private String alertsAccountTypes;
    private String opacUrl;
    private String mobilePhoneProviders;
    private String delayParameter;
    private CustomerServices customerServices;
    private MobileCashServices mobileCashService;
    private String[] productId;
    private String[] relationshipType;
    private String pingfederateSsoUrl;
    private String pingfederateSsoEndpoint;
    //MBSFE 297
    private String pcbCCANotEbillAllowed;
    private String remoteDepositAmountCharge;

    public static final int TRANSACTIONS_CCA_OO_IDA_PAGE_SIZE = 50;

    //MBDP-2226
    private String[] opacCountries;

    public String getOpacUrl() {
        return opacUrl;
    }

    public void setOpacUrl(String opacUrl) {
        this.opacUrl = opacUrl;
    }

    public String getTelenominaID() {
        return telenominaID;
    }

    public void setTelenominaID(String telenominaID) {
        this.telenominaID = telenominaID;
    }

    public void setMessageSource(MessageSource messagesource) {
        this.messageSource = messagesource;
    }

    public Integer getEaccountMinAgeLimit() {
        return eaccountMinAgeLimit;
    }

    public void setEaccountMinAgeLimit(Integer eaccountMinAgeLimit) {
        this.eaccountMinAgeLimit = eaccountMinAgeLimit;
    }

    public ModelAndView business(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // Se usará para que se verifique segun criterio que esconda o no los
        // registros commerciales
        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        String lang = Utils.getRSALanguage(request);
        boolean hasAcctToEnroll = false;
        List<String> acctArray = new ArrayList<>();
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE)) {
            List<CustomerApplication> listCustApp = Utils.getCustomerAppbyType(userSession.getCustomerApplications(), CustomerApplicationType.VIEW_HISTORY_CHECKS_MODIFY, ApplicationStatus.APPROVED,ApplicationStatus.NEW);
            acctArray = FraudPreventionUtils.getAccountsbyAppType(listCustApp);

            String fpsAdd = request.getParameter("fpsAdd");
            ApplicationStatus showIntFp = ApplicationStatus.APPROVED;
            if (fpsAdd != null && fpsAdd.equalsIgnoreCase("true"))
                showIntFp = userSession.getFraudPrevAppStatus();

            model.put("statusFraudPrevApp", showIntFp);
        }

        String visibility = "false";
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)) {
            visibility = "true";
        }
        Boolean eCommAcct = false;
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.COMMERCIAL_STATEMENTS)) {

            Set<TVAccountTransferSource> accountsPayrollFrom = userSession.getAccountsPaymentFrom();

            for (Iterator<TVAccountTransferSource> iterator = accountsPayrollFrom.iterator(); iterator.hasNext(); ) {
                TVAccountTransferSource acctfrom = iterator.next();
                TVFrontendAccount accfront = Utils.getAccountById(acctfrom.getFrontEndId(), userSession.getAccounts());
                String accountNumber = accfront.getAccountNumber();
                if (!accfront.getHide()) {
                    if (accfront != null && accfront.getPortalBalance() != null && accfront.getSubtype() != null
                            && Utils.isCommercialAccount(accfront, productId)) {
                        String relationship = Utils.getRelationshipTypeByAcct(userSession, accfront, relationshipType);
                        if (!relationship.equalsIgnoreCase("")) {
                            eCommAcct = true;
                        }

                        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE)) {
                            List<String> notAllowedAccts = HasSectionUtil.hasAccountsSectionNotAllowed(HasSectionUtil.FRAUD_PREVENTION_SERVICE_SECTION, userSession);
                            if (!acctArray.contains(accountNumber) && !notAllowedAccts.contains(accountNumber))
                                hasAcctToEnroll = true;

                        }
                    }
                }
            }
        }
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE)) {
            String fpsAdd = request.getParameter("fpsAdd");
            if (!hasAcctToEnroll && fpsAdd != null && fpsAdd.equalsIgnoreCase("true"))
                userSession.setStatusmessage(this.getMessageSource().getMessage("fraudpreventionservices.enrollment.notaccount", null, new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang)));
        }


        model.put("hasAcctToEnroll", hasAcctToEnroll);
        model.put("haseCommAcct", eCommAcct);
        model.put("eCommercialStatement", StaticMessageSource.isFlagEnabled(StaticMessageSource.COMMERCIAL_STATEMENTS));
        model.put("visibility", visibility);
        model.put("statusPayrollApp", userSession.getPayrollAppStatus());
        model.put("isEvendorActivated", StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR));
        model.put("isFraudPrevActivate", StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE));
        return new ModelAndView("business", model);
    }

    public ModelAndView ssodude(HttpServletRequest request, HttpServletResponse response)
            throws IOException, TokenException {

        final String strUserSession = "userSession";//user session string
        UserSession userSession = new UserSession();//user session

        if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
            userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession); //user session
        }

        final String profileId = userSession.getCustomerProfile().getProfileId(); //profile id
        Map<String, Object> model = new HashMap<>(); //model result

        //MBFM-99
        final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
        model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
        //END MBFM-99

        if (request.getParameter("agreement") != null) {
            return new ModelAndView("ssoagreement", model);
        }
        if (request.getParameter("agreed") != null) {
            userSession.getCustomerService().setSSOAccepted(profileId);
            return new ModelAndView("redirect:" + pingfederateSsoUrl + pingfederateSsoEndpoint);
        }

        Agent agent = new Agent("agent-config.txt");
        // String username = "ff8081811ac45574011ac456b2a20d62";//5263006325
        Map<String, String> userInfo = new HashMap<String, String>();
        userInfo.put(Agent.TOKEN_SUBJECT, profileId);
        // String returnUrl = "https://devsso.bancopopular.com:9031" +
        // request.getParameter("resume"); //TEST
        String returnUrl = pingfederateSsoUrl + request.getParameter("resume"); // PROD

        try {
            UrlHelper urlHelper = new UrlHelper(returnUrl);
            agent.writeToken(userInfo, response, urlHelper, false);
            returnUrl = (String) urlHelper.toString();
        } catch (TokenException e) {
            logger.error("Error: Exception is " + e.getMessage());
        }
        return new ModelAndView("redirect:" + returnUrl);
    }

    public ModelAndView eStatementPDF(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<>(); /*Model variable for return*/

        String header = request.getHeader("user-agent");
        final String accountStr = "Account: "; /*String variable for account*/
        final String dateStr = "Date: "; /*String variable for date*/
        final String pdfStr = "PDF Id: "; /*String variable for pdf id*/
        final String enterLine = "<br/>"; /*String variable for break line*/
        String id = request.getParameter("pdfId"); /*String variable for pdfid parameter*/
        StringBuilder builder = new StringBuilder(); /*String Builder variable for append*/

        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        StringBuilder messg = new StringBuilder();
        Map<String, Object> modelr = new HashMap<>();

        AccountPDFStmtInfo accountPDFStmtInfo = userSession.getPDFInfoById(id);

        if (accountPDFStmtInfo != null) {
            model.put("pdfId", accountPDFStmtInfo.getDocId());
            return new ModelAndView(new evertec.cibp.web.view.eStatementPDFView(), model);
        }

        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
            if (id != null && !id.isEmpty()) {
                CustomerServices customerService = userSession.getCustomerService();
                OSStmtPDF interestPdf = customerService.getStatementPDF(id);
                if (interestPdf != null && interestPdf.getPdfImage() != null && interestPdf.getPdfImage().length > 0) {

                    HashMap<String, AccountPDFStmtInfo> accountsPdfs = userSession.getPdfStatementAccount(
                            userSession.getCcaEbills(request), userSession.getAccountsStatements(request));

                    AccountPDFStmtInfo pdfAccount = Utils.getStatementByPdfId(id, accountsPdfs);

                    model.put("interestPdf", interestPdf);
                    model.put("eBillsFlag", true);
                    CustomerInteractionEvent cie = Utils.generateCustomerInteractionEvent(request,
                            userSession.getUsername());

                    cie.setCustomStringData(EStatementInteraction.VIEW_PDF_STMT_SHOW,
                            (builder.append(pdfStr).append(id).append(enterLine)
                                    .append(accountStr.concat(pdfAccount.getAccountNumber()))
                                    .append(enterLine).append(dateStr.concat(pdfAccount.getInvoice_date()))
                                    .append(enterLine).append(DeviceUtils.getBrowserInfo(header, userSession))).toString());

                    userSession.getCustomerService().logViewPDFStatement(cie);
                    // Implement statement viewed service that alters the viewed
                    // flag in NTIP
                    StatementViewRequest statementViewedRequest = new StatementViewRequest();

                    if (pdfAccount != null && pdfAccount.getAccountNumber() != null) {
                        List<TVFrontendAccount> allAccounts = new ArrayList<>();
                        allAccounts.addAll(userSession.getAccounts());
                        allAccounts.addAll(userSession.getControlHAccounts());
                        allAccounts.addAll(userSession.getClosedAccounts());
                        TVFrontendAccount acc = Utils.getAccountByAccountNumber(pdfAccount.getAccountNumber(),
                                allAccounts);
                        if (acc != null) {
                            try {
                                if (acc.getSubtype().equalsIgnoreCase("CCA")) {
                                    statementViewedRequest.setAccountClassCd("CARD");
                                    statementViewedRequest.setAccountTypeCd(acc.getSubtype());

                                } else if (acc.getSubtype().equalsIgnoreCase("IDA")) {
                                    statementViewedRequest.setAccountClassCd("");
                                    statementViewedRequest.setAccountTypeCd("");
                                }

                                statementViewedRequest.setAccountId(acc.getAccountNumber());
                                statementViewedRequest.setBankId(acc.getBankId());
                                statementViewedRequest.setSessKey(userSession.getBankingSession().getSessionKey());
                                statementViewedRequest
                                        .setPDFViewPartyId(userSession.getBankingSession().getPermId().longValue());
                                statementViewedRequest.setRqUID(UUID.randomUUID().toString());
                                Date stringDate = pdfAccount.getDueDate();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                String endDate = sdf.format(stringDate);
                                statementViewedRequest.setStatementDt(endDate);
                                String permId = String.valueOf(userSession.getCustomerProfile().getPermId());
                                statementViewedRequest.setCustPermId(permId);
                                StatementViewService stamentService = userSession.getCustomerService()
                                        .StatementViewedService();
                                StatementViewResponse statementViewedResponse = stamentService
                                        .StatementViewed(statementViewedRequest);
                                userSession.setCurrentVaultStatements(null);//Reset greenbox
                                if (statementViewedResponse != null
                                        && statementViewedResponse.getSeverity().indexOf("SUCCESS") < 0) {
                                    logger.error(statementViewedResponse);
                                }
                            } catch (Exception e) {
                                logger.error(e.getMessage());
                            }
                        } else {
                            logger.error("PDF account not found");
                        }
                        return new ModelAndView(new evertec.cibp.web.view.eStatementPDFView(), model);
                    } else {
                        userSession.getCustomerService().logViewPDFStatementError(
                                Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));

                        return new ModelAndView("notfound");
                    }

                } else {
                    if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MOD_COMMERCIAL_STATEMENTS)) {
                        messg.append("status~PdfNotFound|");
                        if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
                            messg.append("successMessage~No e-Statement available for the selected period.|");
                        else
                            messg.append("successMessage~No tienes un e-Statement disponible para el periodo seleccionado.|");

                        modelr.put("text", messg.toString());
                        WebUtils.setSessionAttribute(request, "userSession", userSession);
                        userSession.getCustomerService().logViewPDFStatementError(Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));
                        return new ModelAndView("text", modelr);

                    } else {
                        userSession.getCustomerService().logViewPDFStatementError(Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));
                        return new ModelAndView("notfound");
                    }
                }

            } else {
                if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MOD_COMMERCIAL_STATEMENTS)) {
                    messg.append("status~PdfNotFound|");
                    if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
                        messg.append("successMessage~No e-Statement available for the selected period.|");
                    else
                        messg.append("successMessage~No tienes un e-Statement disponible para el periodo seleccionado.|");

                    modelr.put("text", messg.toString());
                    WebUtils.setSessionAttribute(request, "userSession", userSession);
                    userSession.getCustomerService().logViewPDFStatementError(Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));
                    return new ModelAndView("text", modelr);

                } else {

                    logger.error("Pdf Id not found");
                }

            }
        }
        userSession.getCustomerService()
                .logViewPDFStatementError(Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));

        return new ModelAndView("notfound");
    }

    public ModelAndView disbursementdocument(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> model = new HashMap<>();

        String id = request.getParameter("docId");
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

        // Validation to make sure the document belongs to the client
        if (!GenericValidator.isBlankOrNull(id)) {
            List<CustomerDocumentsPackage> disbList = userSession.getDisbursementsList(false);
            CustomerDocuments selectedDocument = this.getPDFDocument(disbList, id);
            if (selectedDocument == null) {
                disbList = userSession.getDisbursementsAfterAcceptance();
                if (disbList != null) {
                    selectedDocument = this.getPDFDocument(disbList, id);
                }
            }

            if (selectedDocument != null) {

                model.put("docId", id);
                model.put("docName", selectedDocument.getFrontEndText());
                return new ModelAndView(new evertec.cibp.web.view.DisbursementPDFView(), model);

            }
        }
        userSession.getCustomerService()
                .logViewPDFStatementError(Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));
        return new ModelAndView("notfound");

    }

    private CustomerDocuments getPDFDocument(List<CustomerDocumentsPackage> list, String docId) {
        for (CustomerDocumentsPackage pack : list) {
            if (pack.getCustomerDocuments() != null) {
                for (CustomerDocuments docs : pack.getCustomerDocuments()) {
                    if (docs.getDocSearchId().equals(docId)) {
                        return docs;
                    }
                }
            }
        }
        return null;
    }

    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // Login page invalidates session using filter but just in case someone
        // changes it in the future:
        request.getSession().invalidate();
        Map<String, Object> map = new HashMap<>();

        if (request.getParameter("expired") != null)
            map.put("expired", "true");

        return new ModelAndView("redirect:login", map);
    }

    public ModelAndView notfound(HttpServletRequest request, HttpServletResponse response) throws Exception {

        return new ModelAndView("notfound");
    }

    public ModelAndView pfmPortal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerServices customerService = userSession.getCustomerService();
        CustomerProfile profile = userSession.getCustomerProfile();

        PFMUtil pfmUtil = new PFMUtil();
        boolean showUpdateInProcess = false;
        CustomerEntitlement entitlementByType = null;
        PFMEntitlement pfmentitlement = null;

        if (!profile.hasEntitlement(EntitlementType.PFM)) {
            if (pfmAutomaticEntitlementCreation || (!pfmAutomaticEntitlementCreation
                    && userSession.isNewUserForAutomaticPFMEntitlementCreation())) {
                pfmUtil.createPfmEntitlement(userSession.getCustomerService(), profile);
                entitlementByType = profile.getEntitlementByType(EntitlementType.PFM);
                pfmentitlement = new PFMEntitlement(entitlementByType);
            }
            // First time in PFM
            if (userSession.getPfmFirstTime() == null)
                userSession.setPfmFirstTime("Y");
        } else {

            // If the PFM user id in the entitlement does not have a
            // value, set it to blank
            entitlementByType = profile.getEntitlementByType(EntitlementType.PFM);

            pfmentitlement = new PFMEntitlement(entitlementByType);
            if (entitlementByType.getString("PFM_USER_ID") == null
                    || entitlementByType.getString("PFM_USER_ID").equals(""))
                pfmentitlement.setPFM_USER_ID("");

            if (userSession.getPfmFirstTime() == null)
                userSession.setPfmFirstTime("N");
        }

        if (customerService.getGlobalEntitlement(EntitlementType.PFM).getStatus() == EntitlementStatus.ENABLED) {

            if (entitlementByType != null) {
                // If the system is in update AND the pfm user id does
                // not exist, then we present the update image
                if (entitlementByType.getString("PFM_USER_ID").equals("") && userSession.isInPfmUpdatedLookedUp()
                        && userSession.isInPfmUpdate()) {
                    model.put("viewPfmPortal", "N");
                    model.put("viewPfm", "N");
                    model.put("pfmEnabled", "N");
                    showUpdateInProcess = true;
                } else {
                    model.put("viewPfmPortal", "Y");
                    model.put("viewPfm", "Y");
                    model.put("pfmEnabled", "Y");

                    // PFM - Get the past 24 months
                    Date[] past24Months = new Date[24];
                    Calendar cal = Calendar.getInstance();
                    past24Months[0] = cal.getTime();
                    model.put("past24Months_0", past24Months[0]);

                    for (int i = 1; i < 24; i++) {
                        cal.add(Calendar.MONTH, -1);
                        past24Months[i] = cal.getTime();
                        model.put("past24Months_" + i, past24Months[i]);
                    }

                    // PFM - Get the language
                    boolean isEnglish = userSession.getLanguage().equalsIgnoreCase("en");
                    model.put("isenglish", isEnglish);
                }
            } else {
                model.put("viewPfmPortal", "N");
                model.put("viewPfm", "N");
                model.put("pfmEnabled", "N");
            }
        } else {
            model.put("viewPfmPortal", "N");
            model.put("viewPfm", "N");
            model.put("pfmEnabled", "N");
        }
        model.put("showUpdateInProcess", showUpdateInProcess);

        return new ModelAndView("pfmPortal", model);
    }

    public ModelAndView portalAoa(HttpServletRequest request, HttpServletResponse response) throws Exception {

        return new ModelAndView("portalAoa");
    }


    public ModelAndView deleteSmsProfile(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerProfile customer = userSession.getCustomerProfile();
        CustomerServices customerService = userSession.getCustomerService();
        Map<String, Object> model = new HashMap<>();
        model.put("smsDeleted", true);
        try {
            // MBSD-218 OOB LALICEA
            boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);
            if (oobFlag && userSession.getOobEnroll()) {
                WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
                        ChallengeInformationUtils.MOBILE_DELETE_OOB);
                WebUtils.setSessionAttribute(request, ChallengeInformationUtils.MOBILE_DELETE_OOB, true);
                return new ModelAndView("redirect:personalinformation");
            }
            // MBSD-218 OOB LALICEA END
            DeleteSMSProfileRequest delreq = new DeleteSMSProfileRequest(
                    Utils.generateCustomerInteractionEvent(request), customer,
                    new SMSEntitlement(customer.getEntitlementByType(EntitlementType.MOBILE_BANKING)),
                    userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)));
            customerService.deleteSMSProfile(delreq);
            userSession.setSmsProfile(null);
            userSession.resetMobileBanking();
            // CIBP-1857 ISSUE BANCA MOBILE BGARCIA
            // SE LIMPIAN LAS ALERTAS POR CUENTA
            userSession.setSmsaccounts(null);// blas
            // CIBP-1857 ISSUE BANCA MOBILE BGARCIA se eliminan las alertas sms
            // de mi banco
            AlertsEntitlement entitlement = null;
            entitlement = new AlertsEntitlement(
                    userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS));
            Set<AlertType> customeralerts = new HashSet<AlertType>();
            if (entitlement != null && entitlement.getAlerts() != null)
                customeralerts = entitlement.getAlerts();
            if (customeralerts.contains(AlertType.PAYMENTS_SMS))
                customeralerts.remove(AlertType.PAYMENTS_SMS);
            if (customeralerts.contains(AlertType.TRANSFERS_SMS))
                customeralerts.remove(AlertType.TRANSFERS_SMS);
            if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_SMS))
                customeralerts.remove(AlertType.PAYMENTS_CANCEL_SMS);
            if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_SMS))
                customeralerts.remove(AlertType.TRANSFERS_CANCEL_SMS);
            if (customeralerts.contains(AlertType.PASSWORD_BLOCK_SMS))
                customeralerts.remove(AlertType.PASSWORD_BLOCK_SMS);
            if (customeralerts.contains(AlertType.RSA_BLOCK_SMS))
                customeralerts.remove(AlertType.RSA_BLOCK_SMS);
            if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_FUTURE_SMS))
                customeralerts.remove(AlertType.PAYMENTS_CANCEL_FUTURE_SMS);
            if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_SMS))
                customeralerts.remove(AlertType.TRANSFERS_CANCEL_FUTURE_SMS);
            if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_SMS))
                customeralerts.remove(AlertType.PAYMENTS_MODIFY_SMS);
            if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_SMS))
                customeralerts.remove(AlertType.TRANSFERS_MODIFY_SMS);
            if (customeralerts.contains(AlertType.CHANGE_PASS_SMS))
                customeralerts.remove(AlertType.CHANGE_PASS_SMS);
            if (customeralerts.contains(AlertType.ACCESS_BLOCK_SMS))
                customeralerts.remove(AlertType.ACCESS_BLOCK_SMS);
            if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_SMS))
                customeralerts.remove(AlertType.QUESTIONS_BLOCK_SMS);
            if (customeralerts.contains(AlertType.QUESTIONS_EDIT_SMS))
                customeralerts.remove(AlertType.QUESTIONS_EDIT_SMS);
            if (customeralerts.contains(AlertType.EMAIL_EDIT_SMS))
                customeralerts.remove(AlertType.EMAIL_EDIT_SMS);
            if (customeralerts.contains(AlertType.PHONE_EDIT_SMS))
                customeralerts.remove(AlertType.PHONE_EDIT_SMS);
            if (customeralerts.contains(AlertType.ADRESS_EDIT_SMS))
                customeralerts.remove(AlertType.ADRESS_EDIT_SMS);
            if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_SMS))
                customeralerts.remove(AlertType.MOBILE_PHONE_EDIT_SMS);
            if (customeralerts.contains(AlertType.NEW_DEVICE_SMS))
                customeralerts.remove(AlertType.NEW_DEVICE_SMS);
            if (customeralerts.contains(AlertType.NEW_FINGERPRINT_SMS))
                customeralerts.remove(AlertType.NEW_FINGERPRINT_SMS);

            AccountMaskUtil maskutil = new AccountMaskUtil();

            List<TVFrontendAccount> accounts = userSession.getAccounts();
            List<SmsBean> smsBeans = new LinkedList<SmsBean>();
            for (TVFrontendAccount account : accounts) {
                String suffix;
                if (account != null && account.getAccountNumberSuffix() != null)
                    suffix = account.getAccountNumberSuffix();
                else
                    suffix = "";
                if ((!suffix.equalsIgnoreCase("")) && suffix.equalsIgnoreCase("RES") || suffix.equalsIgnoreCase("SAV"))
                    continue;

                boolean cont = false;
                if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE231)) {
                    cont = Utils.haveType(this.alertsAccountTypes, account.getAccountProductId());
                } else {
                    cont = this.alertsAccountTypes.contains(account.getAccountProductId());
                }
                if (account != null && !account.getHide() && cont) {
                    try {
                        CardPlasticFrontend[] plastics = userSession.getCardPlastics(account);
                        if (plastics == null || plastics.length == 0) {
                            continue;
                        }
                        if (plastics.length > 0 && !(account.getCurrentAthSmsAlerts() != null
                                && account.getCurrentAthSmsAlerts().size() > 0)) {
                            HashMap<Object, Object> smsInfo = new HashMap<>();
                            for (int i = 0; i < plastics.length; i++) {
                                SmsBean smsBeanRequest = new SmsBean();
                                smsBeanRequest.setAcctNum(account.getAccountNumber());
                                smsBeanRequest.setTaxId(customer.getTaxId());
                                smsBeanRequest.setCardNum(plastics[i].getCardPlasticAccountRecord().getCardPANId());

                                SmsAlertsResponse smsAlertsResponse = customerService.getSmsAlertInfo(smsBeanRequest,
                                        Utils.generateCustomerInteractionEvent(request));
                                if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
                                        && smsAlertsResponse.getResponseSmsBean() != null) {
                                    smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
                                            smsAlertsResponse.getResponseSmsBean());
                                    smsBeans.add(smsAlertsResponse.getResponseSmsBean());
                                } else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
                                        && smsAlertsResponse.getResponseSmsBean() == null) {
                                    smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
                                    if (!smsAlertsResponse.isAuditable())
                                        logger.error(
                                                "Error Removing SMS Option while Unregistering Cel #: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
                                                        + maskutil.mask(account.getAccountNumber()) + "\ncard: "
                                                        + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + smsAlertsResponse.getAuditString());
                                    else
                                        logger.error(
                                                "Error Removing SMS Option while Unregistering Cel #: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
                                                        + maskutil.mask(account.getAccountNumber()) + "\ncard: "
                                                        + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId());
                                } else if (smsAlertsResponse.getResult().equals(Result.ERROR)
                                        && smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
                                    smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
                                    if (!smsAlertsResponse.isAuditable())
                                        logger.error(
                                                "Error Removing SMS Option while Unregistering Cel #: Unable to send ATH SmsInfo request for \nacct: "
                                                        + maskutil.mask(account.getAccountNumber()) + "\ncard: "
                                                        + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId());
                                    else
                                        logger.error(
                                                "Error Removing SMS Option while Unregistering Cel #: Unable to send ATH SmsInfo request for \nacct: "
                                                        + maskutil.mask(account.getAccountNumber()) + "\ncard: "
                                                        + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n"
                                                        + smsAlertsResponse.getAuditString());
                                } else if (smsAlertsResponse.getResult().equals(Result.ERROR)
                                        && (smsAlertsResponse.getException() != null || !GenericValidator
                                        .isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
                                    smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
                                    if (!smsAlertsResponse.isAuditable()
                                            && !GenericValidator
                                            .isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
                                            && smsAlertsResponse.getException() != null)
                                        logger.error(
                                                "Error Removing SMS Option while Unregistering Cel #: calling ATH SmsInfo \nacct: "
                                                        + maskutil.mask(account.getAccountNumber()) + "\ncard: "
                                                        + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n"
                                                        + smsAlertsResponse.getSmsAlertsException() + "\n"
                                                        + smsAlertsResponse.getException());
                                    else if (!smsAlertsResponse.isAuditable()
                                            && !GenericValidator
                                            .isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
                                            && smsAlertsResponse.getException() == null) {
                                        logger.error(
                                                "Error Removing SMS Option while Unregistering Cel #: calling ATH SmsInfo \nacct: "
                                                        + maskutil.mask(account.getAccountNumber()) + "\ncard: "
                                                        + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n"
                                                        + smsAlertsResponse.getSmsAlertsException());
                                    } else if (!smsAlertsResponse.isAuditable()
                                            && GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
                                            && smsAlertsResponse.getException() != null) {
                                        logger.error(
                                                "Error Removing SMS Option while Unregistering Cel #: calling ATH SmsInfo \nacct: "
                                                        + maskutil.mask(account.getAccountNumber()) + "\ncard: "
                                                        + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n"
                                                        + smsAlertsResponse.getException());
                                    } else if (smsAlertsResponse.isAuditable()
                                            && !GenericValidator
                                            .isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
                                            && smsAlertsResponse.getException() != null)
                                        logger.error(
                                                "Error Removing SMS Option while Unregistering Cel #: calling ATH SmsInfo \nacct: "
                                                        + maskutil.mask(account.getAccountNumber()) + "\ncard: "
                                                        + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n"
                                                        + smsAlertsResponse.getSmsAlertsException() + "\n"
                                                        + smsAlertsResponse.getAuditString());
                                    else {
                                        logger.error(
                                                "Error Removing SMS Option while Unregistering Cel #: calling ATH SmsInfo \nacct: "
                                                        + maskutil.mask(account.getAccountNumber()) + "\ncard: "
                                                        + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n"
                                                        + smsAlertsResponse.getAuditString());
                                    }
                                } else {
                                    smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
                                    logger.error(
                                            "Error Removing SMS Option while Unregistering Cel #: calling ATH SmsInfo \nacct: "
                                                    + maskutil.mask(account.getAccountNumber()) + "\ncard: "
                                                    + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
                                                    + customer.getFullName() + "\nProfileID: " + customer.getProfileId()
                                                    + "\n");
                                }
                            }
                            if (smsInfo.size() > 0)
                                account.setCurrentAthSmsAlerts(smsInfo);
                        }

                        if (plastics.length > 0 && account.getCurrentAthSmsAlerts() != null
                                && account.getCurrentAthSmsAlerts().size() > 0) {
                            // CardPlasticAccountRecord[]
                            // plastics=account.getCardPlasticAccountRecords();
                            for (int i = 0; i < plastics.length; i++) {
                                if (account.getCurrentAthSmsAlerts()
                                        .containsKey(plastics[i].getCardPlasticAccountRecord().getCardPANId())
                                        && (account.getCurrentAthSmsAlerts().get(
                                        plastics[i].getCardPlasticAccountRecord().getCardPANId()) != null)) {
                                    smsBeans.add((SmsBean) account.getCurrentAthSmsAlerts()
                                            .get(plastics[i].getCardPlasticAccountRecord().getCardPANId()));
                                }
                            }
                        }

                        List<SmsBean> forUpdating = new LinkedList<SmsBean>();
                        List<SmsBean> forDeleting = new LinkedList<SmsBean>();

                        if (smsBeans != null && smsBeans.size() > 0) {
                            for (SmsBean sms : smsBeans) {

                                sms.setPurchase_thr_chksms("0");
                                if (GenericValidator.isBlankOrNull(sms.getPurchase_thr_chkemail())
                                        || sms.getPurchase_thr_chkemail().equalsIgnoreCase("0"))
                                    sms.setAlertsPurchaseminamt("");
                                sms.setWithdraw_thr_chksms("0");
                                if (GenericValidator.isBlankOrNull(sms.getWithdraw_thr_chkemail())
                                        || sms.getWithdraw_thr_chkemail().equalsIgnoreCase("0"))
                                    sms.setAlertsWithdrawminamt("");

                                sms.setPurchase_all_chksms("0");
                                sms.setWithdraw_all_chksms("0");

                                sms.setDlyPurchasetotlschksms("0");
                                if (GenericValidator.isBlankOrNull(sms.getDlyPurchasetotlschkemail())
                                        || sms.getDlyPurchasetotlschkemail().equalsIgnoreCase("0"))
                                    sms.setDlyPurchasetotlsminamt("");

                                sms.setDlyWithdrawtotlschksms("0");
                                if (GenericValidator.isBlankOrNull(sms.getDlyWithdrawtotlschkemail())
                                        || sms.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0"))
                                    sms.setDlyWithdrawtotlsminamt("");

                                // org.apache.commons.beanutils.BeanUtils.copyProperties(smsBeanRequest,
                                // a);

                                if ((GenericValidator.isBlankOrNull(sms.getPurchase_thr_chkemail())
                                        || sms.getPurchase_thr_chkemail().equalsIgnoreCase("0"))
                                        && (GenericValidator.isBlankOrNull(sms.getPurchase_thr_chksms())
                                        || sms.getPurchase_thr_chksms().equalsIgnoreCase("0"))
                                        && (GenericValidator.isBlankOrNull(sms.getWithdraw_thr_chkemail())
                                        || sms.getWithdraw_thr_chkemail().equalsIgnoreCase("0"))
                                        && (GenericValidator.isBlankOrNull(sms.getWithdraw_thr_chksms())
                                        || sms.getWithdraw_thr_chksms().equalsIgnoreCase("0"))
                                        && (GenericValidator.isBlankOrNull(sms.getDlyPurchasetotlschkemail())
                                        || sms.getDlyPurchasetotlschkemail().equalsIgnoreCase("0"))
                                        && (GenericValidator.isBlankOrNull(sms.getDlyPurchasetotlschksms())
                                        || sms.getDlyPurchasetotlschksms().equalsIgnoreCase("0"))
                                        && (GenericValidator.isBlankOrNull(sms.getDlyWithdrawtotlschkemail())
                                        || sms.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0"))
                                        && (GenericValidator.isBlankOrNull(sms.getDlyWithdrawtotlschksms())
                                        || sms.getDlyWithdrawtotlschksms().equalsIgnoreCase("0"))
                                        && (GenericValidator.isBlankOrNull(sms.getPurchase_all_chkemail())
                                        || sms.getPurchase_all_chkemail().equalsIgnoreCase("0"))
                                        && (GenericValidator.isBlankOrNull(sms.getPurchase_all_chksms())
                                        || sms.getPurchase_all_chksms().equalsIgnoreCase("0"))
                                        && (GenericValidator.isBlankOrNull(sms.getWithdraw_all_chkemail())
                                        || sms.getWithdraw_all_chkemail().equalsIgnoreCase("0"))
                                        && (GenericValidator.isBlankOrNull(sms.getWithdraw_all_chksms())
                                        || sms.getWithdraw_all_chksms().equalsIgnoreCase("0"))) {
                                    forDeleting.add(sms);
                                } else
                                    forUpdating.add(sms);
                            }
                        }

                        if (forDeleting != null && forDeleting.size() > 0) {

                            for (SmsBean sms : forDeleting) {
                                SmsBean newBean = new SmsBean();

                                newBean.setAcctNum(sms.getAcctNum());
                                newBean.setTaxId(sms.getTaxId());
                                newBean.setCardNum(sms.getCardNum());
                                SmsAlertsResponse smsAlertsResponse = customerService.deleteSmsAlert(newBean,
                                        Utils.generateCustomerInteractionEvent(request));
                                if (smsAlertsResponse.getResult().equals(Result.SUCCESS)) {
                                    account.getCurrentAthSmsAlerts().remove(sms.getCardNum());

                                    // Delete record from database here
                                    try {
                                        boolean removed = customerService.removeATHSmsAlertProfile(
                                                customer.getProfileId(), sms.getAcctNum(), sms.getCardNum());
                                        if (!removed) {
                                            logger.error(
                                                    "Removed from PAXUS but ERROR eliminating ATH SMS ALERTS PROFILE from cibp database for profileID: "
                                                            + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                            + maskutil.mask(sms.getCardNum()) + "\n"
                                                            + customer.getFullName() + "\n" + customer.getProfileId()
                                                            + "\nRecord might not exist...");
                                        }
                                    } catch (Exception e) {
                                        logger.error(
                                                "Error eliminating ATH SMS ALERTS PROFILE from cibp database for profileID: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\n" + customer.getProfileId() + "\n"
                                                        + ExceptionUtils.getFullStackTrace(e));
                                    }

                                    AlertTypesUtils.removeAlertEveryTransactionProfileById(customerService, customer, sms, logger);

                                    try {
                                        boolean removed = customerService.removeATHAnyAlertProfile(
                                                customer.getProfileId(), sms.getAcctNum(), sms.getCardNum());
                                        if (!removed) {
                                            logger.error(
                                                    "Could not remove ATHAnyAlert Record. DOES NOT exist in cibp database for profileID: "
                                                            + customer.getProfileId() + "\nacct: "
                                                            + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                            + maskutil.mask(sms.getCardNum()) + "\n"
                                                            + customer.getFullName());
                                        }
                                    } catch (Exception e) {
                                        logger.error(
                                                "Error eliminating ATHAnyAlert PROFILE from cibp database for profileID: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\n" + customer.getProfileId() + "\n"
                                                        + ExceptionUtils.getFullStackTrace(e));
                                    }

                                } else if (smsAlertsResponse.getResult().equals(Result.ERROR)
                                        && smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
                                    logger.error(
                                            "Error Deleting SMS Profile while Unregistering Cel #: Unable to send ATH SmsDelete request for \nacct: "
                                                    + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                    + maskutil.mask(sms.getCardNum()) + "\n" + customer.getFullName()
                                                    + "\nProfileID: " + customer.getProfileId());
                                } else {

                                    logger.error(
                                            "Error Deleting SMS Profile while Unregistering Cel #: (Profile not found) for \nacct: "
                                                    + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                    + maskutil.mask(sms.getCardNum()) + "\n" + customer.getFullName()
                                                    + "\nProfileID: " + customer.getProfileId());
                                }

                            }

                        }

                        if (forUpdating != null && forUpdating.size() > 0) {

                            for (SmsBean sms : forUpdating) {
                                SmsBean newBean = new SmsBean();

                                org.apache.commons.beanutils.BeanUtils.copyProperties(newBean, sms);

                                SmsAlertsResponse smsAlertsResponse = customerService.updateSmsAlert(newBean,
                                        Utils.generateCustomerInteractionEvent(request));
                                if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
                                        && smsAlertsResponse.getResponseSmsBean() != null) {
                                    account.getCurrentAthSmsAlerts().put(sms.getCardNum(),
                                            smsAlertsResponse.getResponseSmsBean());
                                    try {
                                        boolean removed = customerService.removeATHSmsAlertProfile(
                                                customer.getProfileId(), sms.getAcctNum(), sms.getCardNum());
                                        if (!removed) {
                                            logger.error(
                                                    "UPDATED IN PAXUS, Removing sms options but ERROR eliminating ATH SMS ALERTS PROFILE from cibp database for profileID: "
                                                            + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                            + maskutil.mask(sms.getCardNum()) + "\n"
                                                            + customer.getFullName() + "\n" + customer.getProfileId()
                                                            + "\nRecord might not exist...");
                                        }
                                    } catch (Exception e) {
                                        logger.error(
                                                "Error eliminating ATH SMS ALERTS PROFILE from cibp database for profileID: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\n" + customer.getProfileId() + "\n"
                                                        + ExceptionUtils.getFullStackTrace(e));
                                    }

                                    if ((GenericValidator.isBlankOrNull(sms.getPurchase_all_chksms())
                                            || sms.getPurchase_all_chksms().equalsIgnoreCase("0"))
                                            && (GenericValidator.isBlankOrNull(sms.getWithdraw_all_chksms())
                                            || sms.getWithdraw_all_chksms().equalsIgnoreCase("0"))
                                            && (GenericValidator.isBlankOrNull(sms.getPurchase_all_chkemail())
                                            || sms.getPurchase_all_chkemail().equalsIgnoreCase("0"))
                                            && (GenericValidator.isBlankOrNull(sms.getWithdraw_all_chkemail())
                                            || sms.getWithdraw_all_chkemail().equalsIgnoreCase("0"))) {

                                        AlertTypesUtils.removeAlertEveryTransactionProfileById(customerService, customer, sms, logger);
                                    }

                                } else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
                                        && smsAlertsResponse.getResponseSmsBean() == null) {
                                    try {
                                        boolean removed = customerService.removeATHSmsAlertProfile(
                                                customer.getProfileId(), sms.getAcctNum(), sms.getCardNum());
                                        if (!removed) {
                                            logger.error(
                                                    "UPDATED IN PAXUS, Removing sms options but ERROR eliminating ATH SMS ALERTS PROFILE from cibp database for profileID: "
                                                            + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                            + maskutil.mask(sms.getCardNum()) + "\n"
                                                            + customer.getFullName() + "\n" + customer.getProfileId()
                                                            + "\nRecord might not exist...");
                                        }
                                    } catch (Exception e) {
                                        logger.error(
                                                "Error eliminating ATH SMS ALERTS PROFILE from cibp database for profileID: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\n" + customer.getProfileId() + "\n"
                                                        + ExceptionUtils.getFullStackTrace(e));
                                    }

                                    if ((GenericValidator.isBlankOrNull(sms.getPurchase_all_chksms())
                                            || sms.getPurchase_all_chksms().equalsIgnoreCase("0"))
                                            && (GenericValidator.isBlankOrNull(sms.getWithdraw_all_chksms())
                                            || sms.getWithdraw_all_chksms().equalsIgnoreCase("0"))
                                            && (GenericValidator.isBlankOrNull(sms.getPurchase_all_chkemail())
                                            || sms.getPurchase_all_chkemail().equalsIgnoreCase("0"))
                                            && (GenericValidator.isBlankOrNull(sms.getWithdraw_all_chkemail())
                                            || sms.getWithdraw_all_chkemail().equalsIgnoreCase("0"))) {

                                        AlertTypesUtils.removeAlertEveryTransactionProfileById(customerService, customer, sms, logger);

                                    }
                                    if (!smsAlertsResponse.isAuditable())
                                        logger.error(
                                                "Error Updating SMS Profile while Unregistering Cel #: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + smsAlertsResponse.getAuditString());
                                    else
                                        logger.error(
                                                "Error Updating SMS Profile while Unregistering Cel #: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId());
                                } else if (smsAlertsResponse.getResult().equals(Result.ERROR)
                                        && smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
                                    if (!smsAlertsResponse.isAuditable())
                                        logger.error(
                                                "Error Updating SMS Profile while Unregistering Cel #: Unable to send ATH smsUpdate request for \nacct: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId());
                                    else
                                        logger.error(
                                                "Error Updating SMS Profile while Unregistering Cel #: Unable to send ATH smsUpdate request for \nacct: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n"
                                                        + smsAlertsResponse.getAuditString());
                                } else if (smsAlertsResponse.getResult().equals(Result.ERROR)
                                        && (smsAlertsResponse.getException() != null || !GenericValidator
                                        .isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
                                    if (!smsAlertsResponse.isAuditable()
                                            && !GenericValidator
                                            .isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
                                            && smsAlertsResponse.getException() != null)
                                        logger.error(
                                                "Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n"
                                                        + smsAlertsResponse.getSmsAlertsException() + "\n"
                                                        + smsAlertsResponse.getException());
                                    else if (!smsAlertsResponse.isAuditable()
                                            && !GenericValidator
                                            .isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
                                            && smsAlertsResponse.getException() == null) {
                                        logger.error(
                                                "Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n"
                                                        + smsAlertsResponse.getSmsAlertsException());
                                    } else if (!smsAlertsResponse.isAuditable()
                                            && GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
                                            && smsAlertsResponse.getException() != null) {
                                        logger.error(
                                                "Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n"
                                                        + smsAlertsResponse.getException());
                                    } else if (smsAlertsResponse.isAuditable()
                                            && !GenericValidator
                                            .isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
                                            && smsAlertsResponse.getException() != null)
                                        logger.error(
                                                "Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n"
                                                        + smsAlertsResponse.getSmsAlertsException() + "\n"
                                                        + smsAlertsResponse.getAuditString());
                                    else {
                                        logger.error(
                                                "Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n"
                                                        + smsAlertsResponse.getAuditString());
                                    }
                                } else {

                                    if (!smsAlertsResponse.isAuditable())
                                        logger.error(
                                                "Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n");
                                    else {
                                        logger.error(
                                                "Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
                                                        + maskutil.mask(sms.getAcctNum()) + "\ncard: "
                                                        + maskutil.mask(sms.getCardNum()) + "\n"
                                                        + customer.getFullName() + "\nProfileID: "
                                                        + customer.getProfileId() + "\n"
                                                        + smsAlertsResponse.getAuditString());
                                    }
                                }

                            }

                        }
                        smsBeans.clear();
                    } catch (Exception e) {
                        smsBeans.clear();
                        logger.error("Error Removing SMS Option while Unregistering Cel #: \nacct: "
                                + maskutil.mask(account.getAccountNumber()) + "\n" + "\nProfileID: "
                                + customer.getProfileId(), e);
                    }

                }
            }

            ModifyCustomerAlertsRequest req = new ModifyCustomerAlertsRequest();
            req.setCustomerProfile(userSession.getCustomerProfile());
            req.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
            req.setAlerts(customeralerts);
            userSession.setCustomerProfile(customerService.modifyCustomerAlerts(req));

            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                userSession.setStatusmessage("Your mobile banking phone number has been deleted.");
            else
                userSession.setStatusmessage("Su n&uacute;mero de tel&eacute;fono ha sido borrado.");
            return new ModelAndView("sms", model);

        } catch (Exception e) {

            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                userSession.setStatusmessage(
                        "Unable to delete your phone number at this time, please try later.");
            else
                userSession.setStatusmessage(
                        "No se pudo borrar su n&uacute;mero de tel&eacute;fono. Favor intentar luego.");
            return new ModelAndView("sms", model);

        }

    }

    public ModelAndView pendingPayment(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        //CustomerServices customerService = userSession.getCustomerService();
        List<TVPayment> pendingPayments = new LinkedList<TVPayment>();
        Map<String, Object> model = new HashMap<>();

        String ac = request.getParameter("accountId");
        TVFrontendAccount account = null;
        if (ac != null) {
            account = Utils.getAccountById(ac, userSession.getAccounts());
        }

        TVFrontEndPayee selectedPayee = null;
        if (account != null) {
            if (!userSession.isLoadedDelinquencyPmtInfo()) {

                // Fix Incidente 1503790 pass true to preload favorite payees
                selectedPayee = Utils.getPayeebyAccountNumber(account.getAccountNumber(), userSession.getPayees(true));

                if (selectedPayee != null) {
                    account.setPayeeFrontEndId(selectedPayee.getFrontEndId());
                    List<Integer> payeeIds = selectedPayee.getCustomerPayeeCodes();
                    String[] ids = new String[payeeIds.size()];
                    for (int l = 0; l < payeeIds.size(); l++) {
                        ids[l] = payeeIds.get(l).toString();
                    }

                    PaymentHistoryRequest paymentHistoryRequest = new
                    		PaymentHistoryRequest(userSession.getCustomerProfile(), userSession, PaymentFilterType.PENDING, 15, ids, null);

                    PaymentHistoryResponse transferResponse = PaymentsFacade.getInstance().getPaymentHistory(paymentHistoryRequest);
                    if (transferResponse.getInprocessPayments() != null) {
                        pendingPayments.addAll(transferResponse.getInprocessPayments());
                    }
                }
                account.setPendingPayments(pendingPayments);
            } else {
                pendingPayments = account.getPendingPayments();
            }
        }

        NumberTool tool = new NumberTool();
        if (pendingPayments != null && pendingPayments.size() > 0)
            model.put("text", tool.format("currency", pendingPayments.get(0).getAmount()));
        else
            model.put("text", "");

        return new ModelAndView("text", model);
    }

   
    /**
     * 
     * The depositItems, is used to display the deposit check receipt first. 
     * 
     * Then, it will load the check deposit items (actual check image).
     * 
     * Expected GET parameters are accountId and key.
     * 
     * accountId: the front end account ID representing the account.
     * key: A key identifying the check image CIMS_KEY, Topic Item or Image Reference Id.
     * The value is stored in user session and the image must belong to the client before 
     * making the encrypted call to IMAG.
     * 
     * @param request
     * @param response
     * @return modelAndView
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	public ModelAndView depositItems(HttpServletRequest request, HttpServletResponse response) throws Exception {

    	final Map<String, Object> model = new HashMap<>();
    	
        final UserSession userSession =  KiuwanUtils.cast(UserSession.class, WebUtils.getSessionAttribute(request, "userSession"));
        final CustomerServices customerService = userSession.getCustomerService();
        
        String encryptedUrl = "";
        
        try {
            
        	/**
             * Get Deposit Check Ticket (Slip)
             */
        	
        	final String accountId = request.getParameter("accountId");
            final String key = request.getParameter("key");
            String imageReferenceId = "";

            model.put("page", 1);

            final TVFrontendAccount account = Utils.getAccountById(accountId, userSession.getAccounts());
            
            if (account != null)
                model.put("account", account);

            // Validate that the traceId belong to the userSession.
            if (account != null && userSession.getCheckImageMap() != null && !userSession.getCheckImageMap().isEmpty()) {

            	imageReferenceId = userSession.getCheckImageMap().get(key);

                String hostBaseURL = request.getRequestURL().toString().split("/cibp-web")[0] + "/cibp-web";

            	encryptedUrl = customerService.obtainEncryptedImageUrl(hostBaseURL, imageReferenceId, userSession.getCustomerProfile().getUsername());
            }
            
            if (!GenericValidator.isBlankOrNull(encryptedUrl) && !GenericValidator.isBlankOrNull(imageReferenceId)) {
            
            	model.put("ticketUrl", encryptedUrl);
            	
                customerService.logViewDepositImageItems(Utils.generateCustomerInteractionEvent(request));
            }

            /**
             * Get Deposit Check Image Items
             */
            if (account != null && !GenericValidator.isBlankOrNull(imageReferenceId)) {
                
            	final List<DepositTicketImageRecord> records = customerService.getDepositTicketandItemInfo(
                        userSession.getCustomerProfile(), 
                        userSession.getBankingSession(), 
                        account,
                        imageReferenceId);
                
                if (records != null && records.size() > 0) {
                    
                	final DepositTicketImageRecord record = KiuwanUtils.cast(DepositTicketImageRecord.class, records.get(0));
                    
                    if (record != null && record.getDepositItems() != null && record.getDepositItems().size() > 0) {
                        
                    	model.put("Items", record.getDepositItems());
                        
                        final List<DepositItemImageRecord> recordList = record.getDepositItems();
                        
                        int counter = 1;
                        
                        for (DepositItemImageRecord depositItemImageRecord : recordList) {
                        	
                        	if (userSession.getCheckImageMap() == null) {
                                userSession.setCheckImageMap(new HashMap<>());
                        	}

                            userSession.getCheckImageMap().put("item" + counter, depositItemImageRecord.getShortCIMSKey());
                            
                            counter++;
                        }
                    }
                }
            }
           
        } catch (Exception e) {
        		logger.error(StringEscapeUtils.escapeJava("Error getting deposit image url and deposit items: " + ExceptionUtils.getFullStackTrace(e)));
    	}

        return new ModelAndView("depositItems", model);
    }

    /**
     * 
     * The check image, is used to display a single check image at a time.
     * 
     * Expected GET parameters are accountId and key.
     * 
     * accountId: the frontEndAccountId
     * key: A key identifying the check image CIMS_KEY, Topic Item or Image Reference Id.
     * The value is stored in user session and the check image must belong to the client before 
     * making the encrypted call to IMAG.
     * 
     * @param request
     * @param response
     * @return modelAndView
     * @throws Exception
     */
    public ModelAndView checkImage(HttpServletRequest request, HttpServletResponse response) throws Exception {

        final UserSession userSession = KiuwanUtils.cast(UserSession.class, WebUtils.getSessionAttribute(request, "userSession"));
        final CustomerServices customerService = userSession.getCustomerService();
        final Map<String, Object> model = new HashMap<>();
        
        String encryptedUrl = "";

        try {
            
        	final String accountId = request.getParameter("accountId");
        	final String key = request.getParameter("key");

            TVFrontendAccount account = Utils.getAccountById(accountId, userSession.getAccounts());

            if (account != null && userSession.getCheckImageMap() != null && !userSession.getCheckImageMap().isEmpty()) {
            	
            	String imageReferenceId = userSession.getCheckImageMap().get(key);

            	String hostBaseURL = request.getRequestURL().toString().split("/cibp-web")[0] + "/cibp-web";

            	encryptedUrl = customerService.obtainEncryptedImageUrl(hostBaseURL, imageReferenceId, userSession.getCustomerProfile().getUsername());
            	
            	if (!GenericValidator.isBlankOrNull(encryptedUrl) && !GenericValidator.isBlankOrNull(imageReferenceId)) {
                	
                    customerService.entryUserCheckImageView(userSession.getCustomerProfile(), account, imageReferenceId);
                    customerService.logViewCheckImage(Utils.generateCustomerInteractionEvent(request));
                }
            }

        } catch (Exception e) {
            logger.error("Error getting check image url: ", e);
        }

        model.put("text", encryptedUrl);
        return new ModelAndView("text", model);

    }

    public ModelAndView changelanguage(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        Map<String, Object> model = new HashMap<>();
        String newlang = request.getParameter("newlang");
        String query = request.getParameter("query").replace(',', '&');

        if (GenericValidator.isBlankOrNull(query)) {
            query = query + "?lang=" + newlang;
        } else if (!GenericValidator.isBlankOrNull(query) && query.indexOf("lang=") != -1) {
            query = "?" + query.substring(0, query.indexOf("lang=") + 5) + newlang
                    + query.substring(query.indexOf("lang=") + 7, query.length());
        } else {
            query = "?" + query + "&lang=" + newlang;
        }
        if (userSession != null) {
            userSession.setLanguage(newlang);
            if (userSession.getLastLoginDate() != null) {
                String[] date = userSession.getLastLoginDate().split("/");
                userSession.setLastLoginDate(date[1] + "/" + date[0] + "/" + date[2]);
            }
        }

        model.put("text", query);
        return new ModelAndView("text", model);

    }

    public ModelAndView changesize(HttpServletRequest request, HttpServletResponse response) throws Exception {

    	Map<String, Object> model = new HashMap<>();
        String newsize = request.getParameter("newsize");
        String query = request.getParameter("query");
        if (GenericValidator.isBlankOrNull(query)) {
            query = query + "?text=" + newsize;
        } else if (!GenericValidator.isBlankOrNull(query) && query.indexOf("text=") != -1) {
            query = "?" + query.substring(0, query.indexOf("text=") + 5) + newsize
                    + query.substring((query.indexOf("text=themebig") != -1) ? 13 : 10, query.length());
        } else {
            query = "?" + query + "&text=" + newsize;
        }

        model.put("text", query);
        return new ModelAndView("text", model);

    }

    public ModelAndView defaultpaymentaccount(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<>();
        String accountkey = StringEscapeUtils.escapeHtml(request.getParameter("accountid"));
        String defaultaccount = StringEscapeUtils.escapeHtml(request.getParameter("defaultaccount"));
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerServices customerService = userSession.getCustomerService();
        CustomerProfile customerProfile = userSession.getCustomerProfile();

        boolean isPayrollActivate = StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL);

        boolean fromPayroll = false;

        boolean setdefault = (defaultaccount != null && defaultaccount.equalsIgnoreCase("true")) ? true : false;
        TVAccountTransferSource account = null;

        if (isPayrollActivate)
            fromPayroll = request.getParameter("fromPayroll") != null
                    ? Boolean.valueOf(request.getParameter("fromPayroll")) : false;

        try {
            if (accountkey != null && !accountkey.equalsIgnoreCase("multiple"))
                account = Utils.getAccountSource(accountkey, userSession.getAccountsPaymentFrom());

            if (customerProfile.hasEntitlement(EntitlementType.BILLPAY) && accountkey != null) {

                BillPayEntitlement billpayEntitlement = new BillPayEntitlement(
                        customerProfile.getEntitlementByType(EntitlementType.BILLPAY));
                if (!fromPayroll) {
                    if (account != null)
                        customerProfile = customerService.setDefaultPaymentAccount(customerProfile, billpayEntitlement,
                                setdefault, account.getAccount().getAccountKey());
                    else
                        customerProfile = customerService.setDefaultPaymentAccount(customerProfile, billpayEntitlement,
                                setdefault, accountkey);
                } else {
                    if (account != null)
                        customerProfile = customerService.setDefaultPayrollAccount(customerProfile, billpayEntitlement,
                                setdefault, account.getAccount().getAccountKey());
                    else
                        customerProfile = customerService.setDefaultPayrollAccount(customerProfile, billpayEntitlement,
                                setdefault, accountkey);
                }

                userSession.setCustomerProfile(customerProfile);
                model.put("text", setdefault);
            } else
                model.put("text", !setdefault);

            return new ModelAndView("text", model);

        } catch (Exception e) {
            model.put("text", !setdefault);
            return new ModelAndView("text", model);
        }

    }

    public ModelAndView detailPaymentView(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Object> model = new HashMap<>();

        String detail = StringEscapeUtils.escapeHtml(request.getParameter("detail"));
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerServices customerService = userSession.getCustomerService();
        CustomerProfile customerProfile = userSession.getCustomerProfile();

        boolean setdefault = (detail != null && detail.equalsIgnoreCase("true")) ? true : false;

        try {

            if (customerProfile.hasEntitlement(EntitlementType.BILLPAY)) {

                BillPayEntitlement billpayEntitlement = new BillPayEntitlement(
                        customerProfile.getEntitlementByType(EntitlementType.BILLPAY));
                customerProfile = customerService.setDetailPaymentView(customerProfile, billpayEntitlement, setdefault);

                userSession.setCustomerProfile(customerProfile);
                model.put("text", true);
            } else
                model.put("text", false);

            return new ModelAndView("text", model);

        } catch (Exception e) {
            model.put("text", false);
            return new ModelAndView("text", model);
        }

    }

    public ModelAndView paymentType(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Object> model = new HashMap<>();
        try {
            String payeeId = request.getParameter("payeeId");

            UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
            TVFrontEndPayee payee = Utils.getPayeebyId(payeeId, userSession.getPayessBasicEbpp(true));
            if (payee != null && payee.getGlobalPayee() != null && payee.getGlobalPayee().getPaymentType() != null) {
                String language = Utils.getRSALanguage(request);
                Locale locale = "en".equalsIgnoreCase(language) ? Locale.ENGLISH : new Locale("es");

                String text = null;

                if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138)) {
                    text = "<span class=\"blue\" style=\"cursor:pointer;\" title=\'" + messageSource.getMessage(
                            "addpayee.paymentType.description." + payee.getGlobalPayee().getPaymentType().name(), null,
                            "?", locale) + "\'>";
                } else {
                    text = "<span class=\"blue\" style=\"cursor:pointer;\" title=\'" + messageSource.getMessage(
                            "old.addpayee.paymentType.description." + payee.getGlobalPayee().getPaymentType().name(),
                            null, "?", locale) + "\'>";
                }

                if (payee.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO
                        || payee.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PUBLICO)
                    text = text + "<img class=\"LinkImage\" src=\'../img/icnSmallCheck.gif\'>";
                else if (payee.getGlobalPayeeId() == 13330 || payee.getGlobalPayeeId() == 279
                        || payee.getGlobalPayeeId() == 1368 || payee.getGlobalPayeeId() == 63
                        || payee.getGlobalPayeeId() == 11211 || payee.getGlobalPayeeId() == 11473
                        || payee.getGlobalPayeeId() == 12508)
                    text = text + messageSource.getMessage("addpayee.paymentType.INTERNO", null, "?", locale);
                else {
                    if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138)) {
                        text = text + messageSource.getMessage(
                                "addpayee.paymentType." + payee.getGlobalPayee().getPaymentType().name(), null, "?",
                                locale);
                    } else {
                        text = text + messageSource.getMessage(
                                "old.addpayee.paymentType." + payee.getGlobalPayee().getPaymentType().name(), null, "?",
                                locale);
                    }
                }

                text = text + "</span>";

                if (text != null && !text.trim().equals(""))
                    model.put("text", text);
                else
                    model.put("text", " ");
            } else {
                model.put("text", " ");

            }
        } catch (RuntimeException e) {
            model.put("text", " ");
        }
        return new ModelAndView("text", model);
    }

    public ModelAndView employeeNameAndId(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Object> model = new HashMap<>();
        model.put("telenominaID", telenominaID);
        try {
            String payeeId = request.getParameter("payeeId");

            UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
            TVFrontEndPayee payee = Utils.getPayeebyId(payeeId, userSession.getPayessBasicEbpp(true));
            // CIBP-2548 JOCAMPO
            if (payee != null && payee.getGlobalPayeeIdString() != null
                    && payee.getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
                String employeeName = "";
                String employeeId = "";

                if (payee.getEmployeeName() != null)
                    employeeName = payee.getEmployeeName();
                if (payee.getEmployeeId() != null)
                    employeeId = payee.getEmployeeId();
                String language = Utils.getRSALanguage(request);
                Locale locale = "en".equalsIgnoreCase(language) ? Locale.ENGLISH : new Locale("es");
                String text = "&nbsp;&nbsp;&nbsp;&nbsp;<b>"
                        + messageSource.getMessage("editpayee.employeename.label", null, "Name:", locale) + "</b>&nbsp;"
                        + employeeName;

                text = text + "&nbsp;&nbsp;&nbsp;&nbsp;<b>"
                        + messageSource.getMessage("editpayee.employeeID.label", null, "ID Number:", locale)
                        + "</b>&nbsp;" + employeeId;

                if (text != null && !text.trim().equals(""))
                    model.put("text", text);
                else
                    model.put("text", " ");
            } else {
                model.put("text", " ");

            }
        } catch (RuntimeException e) {
            model.put("text", " ");
        }
        return new ModelAndView("text", model);
    }

    public ModelAndView getBankNameFromRoutingNumber(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerServices customerService = userSession.getCustomerService();

        String routingNumber = request.getParameter("routingNumber");
        String bankName;
        if (GenericValidator.matchRegexp(routingNumber, "^\\d{9}$"))
            bankName = customerService.getBankName(routingNumber);
        else
            bankName = "";

        if (bankName.equals("")) {
            String language = Utils.getRSALanguage(request);
            if (language.equals("sp"))
                bankName = "No se encontraron resultados";
            else
                bankName = "No matches were found";
        }

        model.put("text", bankName);
        return new ModelAndView("text", model);
    }

    public ModelAndView estimateReceivedDate(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        Map<String, Object> model = new HashMap<>();
        SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy");
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerServices customerService = userSession.getCustomerService();
        String text = "";
        String received = "";
        String check = "";
        String sameday = "";
        String newReceivedDate = "";
        boolean isVendorActive = StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR);
        try {
            String language = Utils.getRSALanguage(request);
            Locale locale = "es".equalsIgnoreCase(language) || "sp".equalsIgnoreCase(language) ? new Locale("es")
                    : Locale.ENGLISH;
            received = messageSource.getMessage("makepayments.receivedpayment.label", null, "Received:", locale);
            check = messageSource.getMessage("makepayments.sendcheckpayment.label", null, "Check will be sent: ",
                    locale);
            sameday = messageSource.getMessage("makepayments.samedaypayment.label", null, "Same Day: ", locale);
            String effectiveDate = request.getParameter("effdt");
            String id = request.getParameter("payeeid");

            GregorianCalendar cal = null;
            if (effectiveDate != null && effectiveDate.length() == 10
                    && GenericValidator.matchRegexp(effectiveDate, "^\\d{2}/\\d{2}/\\d{4}$")) {
                String[] dt = effectiveDate.split("/");
                cal = new GregorianCalendar(Integer.parseInt(dt[2]), Integer.parseInt(dt[0]) - 1,
                        Integer.parseInt(dt[1]));
            }

            if (cal == null)
                model.put("text", "");

            TVFrontEndPayee tar = Utils.getPayeebyId(id, userSession.getPayees(true));
            if (tar != null && tar.getGlobalPayee() != null && tar.getGlobalPayee().getPaymentType() != null) {
                text = Utils.getEstimateDate(customerService.getHolidays(), cal, tar.getGlobalPayee().getPaymentType());
                if (tar.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO
                        || tar.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PUBLICO)
                    text = check + " " + text;
                else if (tar.getGlobalPayeeId() == 13330 || tar.getGlobalPayeeId() == 279
                        || tar.getGlobalPayeeId() == 1368 || tar.getGlobalPayeeId() == 63
                        || tar.getGlobalPayeeId() == 11211 || tar.getGlobalPayeeId() == 11473
                        || tar.getGlobalPayeeId() == 12508)
                    text = sameday + " " + text;
                else {
                    if (isVendorActive) {
                        if (tar.getGlobalPayeeId() == Integer.valueOf(telenominaID)) {
                            newReceivedDate = messageSource.getMessage("commercial.newlabelpayroll.receivingdate", null, "Employee will receive payroll on: ", locale);
                            text = newReceivedDate + " " + text;
                        } else if (tar.getGlobalPayeeId() == Integer.valueOf(vendorId)) {
                            newReceivedDate = messageSource.getMessage("commercial.newlabelvendor.receivingdate", null, "Vendor will receive payroll on: ", locale);
                            text = newReceivedDate + " " + text;
                        } else {
                            text = received + " " + text;
                        }
                    } else {
                        text = received + " " + text;
                    }
                }
            } else {
                text = received + " "
                        + Utils.getEstimateDate(customerService.getHolidays(), cal, GlobalPayeePaymentType.INTERNO);
            }

            if (text != null && !text.trim().equals("")) {
                model.put("text", text);
            } else
                model.put("text", received + " " + dateformat.format(cal.getTime()));

        } catch (RuntimeException e) {
            model.put("text", "");
        }
        return new ModelAndView("text", model);

    }

    // RealTimePayments
    public ModelAndView estimateReceivedDateRealTimePayments(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        Map<String, Object> model = new HashMap<>();
        SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy");
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerServices customerService = userSession.getCustomerService();
        String text = "";
        String received = "";
        String check = "";
        String sameday = "";
        try {
            String language = Utils.getRSALanguage(request);
            Locale locale = "es".equalsIgnoreCase(language) || "sp".equalsIgnoreCase(language) ? new Locale("es")
                    : Locale.ENGLISH;
            received = messageSource.getMessage("makepayments.receivedpayment.label", null, "Received:", locale);
            check = messageSource.getMessage("makepayments.sendcheckpayment.label", null, "Check will be sent: ",
                    locale);
            sameday = messageSource.getMessage("makepayments.samedaypayment.label", null, "Same Day: ", locale);
            String effectiveDate = request.getParameter("effdt");
            String id = request.getParameter("payeeid");
            String frequency = request.getParameter("frequency");

            GregorianCalendar cal = null;
            if (effectiveDate != null && effectiveDate.length() == 10
                    && GenericValidator.matchRegexp(effectiveDate, "^\\d{2}/\\d{2}/\\d{4}$")) {
                String[] dt = effectiveDate.split("/");
                cal = new GregorianCalendar(Integer.parseInt(dt[2]), Integer.parseInt(dt[0]) - 1,
                        Integer.parseInt(dt[1]));
            }

            if (cal == null)
                model.put("text", "");

            TVFrontEndPayee tar = Utils.getPayeebyId(id, userSession.getPayees(true));
            if (tar != null && tar.getGlobalPayee() != null && tar.getGlobalPayee().getPaymentType() != null) {
                text = Utils.getEstimateDate(customerService.getHolidays(), cal, tar.getGlobalPayee().getPaymentType(),
                        frequency);
                if (tar.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO
                        || tar.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PUBLICO)
                    text = check + " " + text;
                else if (tar.getGlobalPayeeId() == 13330 || tar.getGlobalPayeeId() == 279
                        || tar.getGlobalPayeeId() == 1368 || tar.getGlobalPayeeId() == 63
                        || tar.getGlobalPayeeId() == 11211 || tar.getGlobalPayeeId() == 11473
                        || tar.getGlobalPayeeId() == 12508)
                    text = sameday + " " + text;
                else
                    text = received + " " + text;
            } else {
                text = received + " " + Utils.getEstimateDate(customerService.getHolidays(), cal,
                        GlobalPayeePaymentType.INTERNO, frequency);
            }

            if (text != null && !text.trim().equals("")) {
                model.put("text", text);
            } else
                model.put("text", received + " " + dateformat.format(cal.getTime()));

        } catch (RuntimeException e) {
            model.put("text", "");
        }
        return new ModelAndView("text", model);

    }

    public ModelAndView paymentTypeRealTimePayments(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<>();
        try {
            String payeeId = request.getParameter("payeeId");
            String date = request.getParameter("date");
            String frequency = request.getParameter("frequency");

            GregorianCalendar cal = null;
            if (date != null && date.length() == 10 && GenericValidator.matchRegexp(date, "^\\d{2}/\\d{2}/\\d{4}$")) {
                String[] dt = date.split("/");
                cal = new GregorianCalendar(Integer.parseInt(dt[2]), Integer.parseInt(dt[0]) - 1,
                        Integer.parseInt(dt[1]));
            }

            UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
            TVFrontEndPayee payee = Utils.getPayeebyId(payeeId, userSession.getPayessBasicEbpp(true));
            if (payee != null && payee.getGlobalPayee() != null && payee.getGlobalPayee().getPaymentType() != null
                    && cal != null) {
                String language = Utils.getRSALanguage(request);
                Locale locale = "en".equalsIgnoreCase(language) ? Locale.ENGLISH : new Locale("es");

                String text = null;
                boolean realTimeDays = false;

                if ((GlobalPayeePaymentType.MULTIPAGO_MENSUALIDAD.equals(payee.getGlobalPayee().getPaymentType())
                        || GlobalPayeePaymentType.MULTIPAGO_CUOTA.equals(payee.getGlobalPayee().getPaymentType())
                        || GlobalPayeePaymentType.MULTIPAGO_MATRICULA.equals(payee.getGlobalPayee().getPaymentType())
                        || GlobalPayeePaymentType.MASTER_CARD_RPPS.equals(payee.getGlobalPayee().getPaymentType())
                        || GlobalPayeePaymentType.GARANTIZADO.equals(payee.getGlobalPayee().getPaymentType()))
                        && Utils.isRealTime(cal.getTime(), frequency)) {
                    text = "<span class=\"blue\" style=\"cursor:pointer;\" title=\'" + messageSource.getMessage(
                            "addpayee.paymentType.description." + payee.getGlobalPayee().getPaymentType().name(), null,
                            "?", locale) + "\'>";
                    realTimeDays = true;
                } else {
                    text = "<span class=\"blue\" style=\"cursor:pointer;\" title=\'" + messageSource.getMessage(
                            "old.addpayee.paymentType.description." + payee.getGlobalPayee().getPaymentType().name(),
                            null, "?", locale) + "\'>";
                }

                if (payee.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO
                        || payee.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PUBLICO)
                    text = text + "<img class=\"LinkImage\" src=\'../img/icnSmallCheck.gif\'>";
                else if (payee.getGlobalPayeeId() == 13330 || payee.getGlobalPayeeId() == 279
                        || payee.getGlobalPayeeId() == 1368 || payee.getGlobalPayeeId() == 63
                        || payee.getGlobalPayeeId() == 11211 || payee.getGlobalPayeeId() == 11473
                        || payee.getGlobalPayeeId() == 12508)
                    text = text + messageSource.getMessage("addpayee.paymentType.INTERNO", null, "?", locale);
                else {
                    if (realTimeDays) {
                        text = text + messageSource.getMessage(
                                "addpayee.paymentType." + payee.getGlobalPayee().getPaymentType().name(), null, "?",
                                locale);
                    } else {
                        text = text + messageSource.getMessage(
                                "old.addpayee.paymentType." + payee.getGlobalPayee().getPaymentType().name(), null, "?",
                                locale);
                    }
                }

                text = text + "</span>";

                if (text != null && !text.trim().equals(""))
                    model.put("text", text);
                else
                    model.put("text", " ");
            } else {
                model.put("text", " ");

            }
        } catch (RuntimeException e) {
            model.put("text", " ");
        }
        return new ModelAndView("text", model);
    }
    // RealTimePayments

    public ModelAndView error(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UserSession userSession = new UserSession();//user session
        final String strUserSession = "userSession";//user session string

        if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
            userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
        }
        Map<String, Object> model = new HashMap<>(); //model result

        //MBFM-99
        final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
        model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
        //END MBFM-99

        return new ModelAndView("error", model);
    }

    public ModelAndView athmTerms(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String language = Utils.getRSALanguage(request);
        if (language.equalsIgnoreCase("sp")) {
            return new ModelAndView("athm/athmTerms-es");
        }

        return new ModelAndView("athm/athmTerms-en");
    }

    public ModelAndView removeMessage(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        userSession.setStatusmessage("");

        model.put("text", true);
        return new ModelAndView("text", model);
    }

    public ModelAndView ebppsorry(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> model = new HashMap<>();
        return new ModelAndView("ebppsorry", model);
    }

    public ModelAndView termsChargegard(HttpServletRequest request, HttpServletResponse response) throws Exception {

        return new ModelAndView("terms-chargegard-" + Utils.getRSALanguage(request));

    }

    public ModelAndView ebpp2PrevHistoryView(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        // Call from portal ebppinbox, with database information
        Map<String, Object> model = new HashMap<>();
        return new ModelAndView("ebpp", model);
    }

    public ModelAndView ebppLatestHistory(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // Call from portal ebppinbox, with database information
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

        // The user has an EBPP2 ID
        if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals(""))
            return new ModelAndView("redirect:ebpp2LatestHistory");
        else
            return new ModelAndView("ebppdown");
    }

    public ModelAndView ebpp2LatestHistory(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // Call from portal ebppinbox, with database information
        Map<String, Object> model = new HashMap<>(); //model result
        UserSession userSession = new UserSession();//user session
        final String strUserSession = "userSession";//user session string

        if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
            userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
        }
        CustomerServices customerService = userSession.getCustomerService();
        CustomerProfile customerProfile = userSession.getCustomerProfile();

        //MBFM-99
        final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
        model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
        //END MBFM-99

        if (customerProfile.hasEntitlement(EntitlementType.EBILLS) && isEbppAvailable() && customerService
                .getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {

            // Get the EBPP2 accounts and summaries
            List<UserAccount> ebpp2AccountsAndSummariesList = userSession.getEbpp2UserAccountsAndSummaries();
            List<UserAccount> ebppHistoryEbills = new LinkedList<>();
            List<UserAccount> ebppHistoryEstatements = new LinkedList<>();

            if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
                //List<AccountStmtInfo> vaultEbillsHistory = userSession.getCcaEbills(request);
                List<CreditCardStatement_Type> ccahistory = userSession.getCcaHistoryStatements();
                List<AccountStmtInfo> vaultEbillsHistory = Utils.toAccountStmtInfo(ccahistory, userSession);
                userSession.resetCcaEbills();
                userSession.setCcaAvailbleToEbill(null);
                model.put("vaultEbillsHistory", vaultEbillsHistory);

                //List<AccountPDFStmtInfo> vaultStatementsHistory = userSession.getAccountsCurrentStatements(request);

                List<DepositStatement_Type> ddahistory = userSession.getDdaHistoryStatements();
                List<AccountPDFStmtInfo> vaultStatementsHistory = Utils.toAccountPDFStmtInfo(ddahistory, userSession);
                model.put("vaultStatementsHistory", vaultStatementsHistory);
                //Reset greenbox accounts
                userSession.setCurrentVaultStatements(null);
                userSession.setCcaCurrentStatements(null);
                userSession.setDdaCurrentStatements(null);
            }

            // Separate these into e-Bills and e-Statements
            if (ebpp2AccountsAndSummariesList != null && !ebpp2AccountsAndSummariesList.isEmpty()) {
                for (UserAccount userAccount : ebpp2AccountsAndSummariesList) {
                    if (userAccount.getAccountStatements() != null && !userAccount.getAccountStatements().isEmpty()) {
                        // Biller ID > 1 = e-Bill
                        if (userAccount.getBillerId() > 1)
                            ebppHistoryEbills.add(userAccount);
                            // Biller ID = 1 && userAccount biller product ID is 55,
                            // 59 or 60 or 61 = Credit Card e-Bill
                        else if (userAccount.getBillerId() == 1 && (userAccount.getBillerProductId() == 55
                                || userAccount.getBillerProductId() == 59 || userAccount.getBillerProductId() == 60
                                || userAccount.getBillerProductId() == 61))
                            ebppHistoryEbills.add(userAccount);
                            // Else, e-Statement
                        else
                            ebppHistoryEstatements.add(userAccount);
                    }
                }
            }

            // Place them in the model
            model.put("ebppHistoryEbills", ebppHistoryEbills);
            model.put("ebppHistoryEstatements", ebppHistoryEstatements);

            // New Interest Process
            // Get accounts to check which are RES, LVA, RVA, IDA
            List<AccountPDFStmtInfo> lastHistoryInterest = userSession.getInterestStatementsLastStmt();
            List<AccountPDFStmtInfo> lastHistoryInterestTmp = new LinkedList<>(lastHistoryInterest);
            List<AccountPDFStmtInfo> lastHistoryInterestIRA = new LinkedList<>();
            List<AccountPDFStmtInfo> lastHistoryMLA = new LinkedList<>();
            List<AccountPDFStmtInfo> lastHistoryInterestIRACAEQuarterlyAnnual = new LinkedList<>();

            boolean hasMLAStatements = false;
            boolean hasNonIRAInterest = false;
            boolean hasIRAInterest = false;
            boolean hadIRACAEQuarterlyAnnual = false;

            // Separate them into IRA, CAE or non-IRA
            if (lastHistoryInterest != null && !lastHistoryInterest.isEmpty()) {
                for (Iterator<AccountPDFStmtInfo> iterator = lastHistoryInterest.iterator(); iterator.hasNext(); ) {
                    AccountPDFStmtInfo intStmtInfo = iterator.next();
                    if (intStmtInfo.getEzStmtProductId().equals(ezStatementInformativaIRAQuarterlyProductId)
                            || intStmtInfo.getEzStmtProductId().equals(ezStatementInformativaCAEQuarterlyProductId)
                            || intStmtInfo.getEzStmtProductId().equals(ezStatementInformativaIRAYearlyProductId)
                            || intStmtInfo.getEzStmtProductId().equals(ezStatementInformativaCAEYearlyProductId)) {

                        lastHistoryInterestIRACAEQuarterlyAnnual.add(intStmtInfo);
                        hadIRACAEQuarterlyAnnual = true;
                    } else if (intStmtInfo.getAccount_type().equals("IIM")
                            || intStmtInfo.getAccount_type().equals("IDA")) {
                        lastHistoryInterestIRA.add(intStmtInfo);
                        hasIRAInterest = true;
                    } else if (Arrays.asList(ezStatementMortgageProductIds)
                            .contains(intStmtInfo.getEzStmtProductId())) {
                        lastHistoryMLA.add(intStmtInfo);
                        lastHistoryInterestTmp.remove(intStmtInfo);
                        hasMLAStatements = true;
                    } else {
                        hasNonIRAInterest = true;
                    }
                }
            }
            if (hasNonIRAInterest)
                model.put("ebppHistoryInterest", lastHistoryInterestTmp);

            if (hasIRAInterest) {
                model.put("ebppHistoryInterestIRA", lastHistoryInterestIRA);
                model.put("iraDisclaimer", true);
            }

            if (hasMLAStatements)
                model.put("ebppHistoryMLA", lastHistoryMLA);

            if (hadIRACAEQuarterlyAnnual) {
                model.put("ebppHistoryIRACAEQuarterlyAnnual", lastHistoryInterestIRACAEQuarterlyAnnual);
                model.put("iraQuarterlyEzStmtProductId", ezStatementInformativaIRAQuarterlyProductId);
                model.put("caeQuarterlyEzStmtProductId", ezStatementInformativaCAEQuarterlyProductId);
                model.put("iraYearlyEzStmtProductId", ezStatementInformativaIRAYearlyProductId);
                model.put("caeYearlyEzStmtProductId", ezStatementInformativaCAEYearlyProductId);
            }

            //MBSFE 297
            model.put("pcbCCANotEbillAllowed", this.pcbCCANotEbillAllowed);
            model.put("MBSFE297", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE297));
            //END MBSFE 297

            return new ModelAndView("ebpp2LatestHistory", model);
        } else {
            return new ModelAndView("ebppdown");
        }
    }

    public ModelAndView ebppViewBiller(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // FROM TRANSACTIONS

        Map<String, Object> model = new HashMap<>(); //model result
        UserSession userSession = new UserSession();//user session
        final String strUserSession = "userSession";//user session string

        if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
            userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
        }
        CustomerProfile customerProfile = userSession.getCustomerProfile(); //customer profile

        //MBFM-99
        final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
        model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
        //END MBFM-99

        String id = (String) request.getParameter("id");
        String trxCycle = (String) request.getParameter("cycle");
        String ebppCycle = trxCycle;
        String pageReq = (String) request.getParameter("pageReq");

        boolean cycleWasChanged = false;
        String origCycleSelected = "";

        String estatementMessageEnglish = "No e-Statement available for the selected period.";
        String estatementMessageSpanish = "No tienes un e-Statement disponible para el periodo seleccionado.";
        String ebillMessageEnglish = "No e-Bill available for the selected period.";
        String ebillMessageSpanish = "No tienes un e-Bill disponible para el periodo seleccionado.";
        String noStatementMessage = "";

        // Find the EBPP2 user if it exists
        // The EBPP2 user id is not yet in session; add it
        if (!userSession.isEbpp2UserIdLookedUp()) {
            EbppEntitlement ent = null;
            if (customerProfile.getEntitlementByType(EntitlementType.EBILLS) != null) {
                ent = new EbppEntitlement(customerProfile.getEntitlementByType(EntitlementType.EBILLS));

                // If the username is blank, we need to look in the current EBPP
                // accounts ONLY
                if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
                    userSession.setEbpp2UserId("");
                } else {
                    userSession.setEbpp2UserId(ent.getUsername());
                }
                userSession.setEbpp2UserIdLookedUp(true);
            }
        }

        if (pageReq == null || pageReq.equals(""))
            pageReq = "1";

        if (ebppCycle == null || ebppCycle.equals("") || ebppCycle.equalsIgnoreCase("none"))
            ebppCycle = "1";

        TVFrontendAccount account = null;
        if (id != null)
            account = Utils.getAccountById(id, userSession.getAccounts());
        if (account != null) {
            logger.debug("View Biller: account = " + (new AccountMaskUtil()).mask(account.getAccountNumber()));

            if (account.getSubtype().equals(AccountSubtypes.DEPOSIT)
                    || account.getSubtype().equals(AccountSubtypes.CREDIT_CARD)) {
                logger.debug("View Biller: account = " + (new AccountMaskUtil()).mask(account.getAccountNumber())
                        + " is a " + account.getSubtype());

                if (account.getSubtype().equals(AccountSubtypes.DEPOSIT)) {
                    if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
                        noStatementMessage = estatementMessageEnglish;
                    else
                        noStatementMessage = estatementMessageSpanish;

                    // The user has an EBPP2 ID
                    if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

                        // Get the list that will contain all the accounts and
                        // their respective summaries
                        List<UserAccount> ebpp2UserAccountsAndSummaries = userSession
                                .getEbpp2UserAccountsAndSummaries();

                        // Now loop through all the summaries and select the one
                        // for this account

                        if (ebpp2UserAccountsAndSummaries != null && ebpp2UserAccountsAndSummaries.size() > 0) {
                            account.setEbpp2UserAccount(
                                    Utils.getEbpp2UserAccountsByAccount(account.getAccountNumber(), userSession));
                        }
                    }
                } else {
                    if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
                        noStatementMessage = ebillMessageEnglish;
                    else
                        noStatementMessage = ebillMessageSpanish;

                    // The user has an EBPP2 ID
                    if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

                        // Get the list that will contain all the accounts and
                        // their respective summaries
                        List<UserAccount> ebpp2UserAccountsAndSummaries = userSession
                                .getEbpp2UserAccountsAndSummaries();

                        // Now loop through all the summaries and select the one
                        // for this account
                        if (ebpp2UserAccountsAndSummaries != null && ebpp2UserAccountsAndSummaries.size() > 0) {
                            account.setEbpp2UserAccount(
                                    Utils.getEbpp2UserAccountsByAccount(account.getAccountNumber(), userSession));
                        }
                    }
                }

                if (ebppCycle.equals("1")) {
                    origCycleSelected = ebppCycle;
                    cycleWasChanged = true;
                    ebppCycle = "2";
                }

                // The user has an EBPP2 ID
                if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

                    if (account.getEbpp2UserAccount() != null) {

                        // Deactivated or pending activation
                        if (!account.getEbpp2UserAccount().isActive()
                                || account.getEbpp2UserAccount().getIsRegistered() == 2) {
                            model.put("text", "editaccount?id=" + id);
                            return new ModelAndView("text", model);
                        }
                        // Active...
                        else if (account.getEbpp2UserAccount().getIsRegistered() == 1) {

                            // We are looking for a previous cycle
                            if (ebppCycle != null && !ebppCycle.equalsIgnoreCase("1")
                                    && !ebppCycle.equalsIgnoreCase("2")) {

                                AbstractStatement stm = Utils.getStatementById(ebppCycle, account.getStatements());
                                AccountSummary prevBillToview = null;
                                boolean prevHistFound = false;

                                if (cycleWasChanged)
                                    ebppCycle = origCycleSelected;

                                if (stm != null && stm.getEndDate() != null) {
                                    Calendar end = Calendar.getInstance();
                                    end.setTime(stm.getEndDate());

                                    // Go through the entire e-Bill history
                                    if (account.getEbpp2UserAccount().getAccountStatements() != null
                                            && account.getEbpp2UserAccount().getAccountStatements().size() > 0) {

                                        for (AccountSummary accountSummary : account.getEbpp2UserAccount()
                                                .getAccountStatements()) {
                                            Calendar summaryEnd = Calendar.getInstance();
                                            summaryEnd.setTime(accountSummary.getInvoiceDate());
                                            if (end.get(Calendar.YEAR) == summaryEnd.get(Calendar.YEAR)
                                                    && end.get(Calendar.MONTH) == summaryEnd.get(Calendar.MONTH)
                                                    && end.get(Calendar.DATE) == summaryEnd.get(Calendar.DATE)) {
                                                prevBillToview = accountSummary;
                                                prevHistFound = true;
                                                break;
                                            }
                                        }

                                        if (prevHistFound) {

                                            // If we have a valid statement
                                            // ID, look for this PDF
                                            if (prevBillToview.getStatementID() > 0) {

                                                model.put("text",
                                                        "ebpp2ViewStatement?id=" + prevBillToview.getStatementID()
                                                                + "&uai="
                                                                + account.getEbpp2UserAccount().getBillerAccountID());
                                                return new ModelAndView("text", model);
                                            }
                                            // The statement was not found
                                            else if (trxCycle != null) {
                                                userSession.setStatusmessage(noStatementMessage);
                                                model.put("text", "transaction?account=" + id + "&cycle=" + trxCycle
                                                        + "&pageReq=" + pageReq);
                                                return new ModelAndView("text", model);
                                            } else {
                                                userSession.setStatusmessage(noStatementMessage);
                                                model.put("text", "ebppnostatement?account=" + id + "&cycle=" + trxCycle
                                                        + "&pageReq=" + pageReq);
                                                return new ModelAndView("text", model);
                                            }
                                        }
                                        // The statement was not found
                                        else if (trxCycle != null) {
                                            userSession.setStatusmessage(noStatementMessage);
                                            model.put("text", "transaction?account=" + id + "&cycle=" + trxCycle
                                                    + "&pageReq=" + pageReq);
                                            return new ModelAndView("text", model);
                                        } else {
                                            userSession.setStatusmessage(noStatementMessage);
                                            model.put("text", "ebppnostatement?account=" + id + "&cycle=" + trxCycle
                                                    + "&pageReq=" + pageReq);
                                            return new ModelAndView("text", model);
                                        }
                                    }
                                    // The statement was not found
                                    else if (trxCycle != null) {
                                        userSession.setStatusmessage(noStatementMessage);
                                        model.put("text", "transaction?account=" + id + "&cycle=" + trxCycle
                                                + "&pageReq=" + pageReq);
                                        return new ModelAndView("text", model);
                                    } else {
                                        userSession.setStatusmessage(noStatementMessage);
                                        model.put("text", "ebppnostatement?account=" + id + "&cycle=" + trxCycle
                                                + "&pageReq=" + pageReq);
                                        return new ModelAndView("text", model);
                                    }
                                }
                                // The statement was not found
                                else if (trxCycle != null) {
                                    userSession.setStatusmessage(noStatementMessage);
                                    model.put("text",
                                            "transaction?account=" + id + "&cycle=" + trxCycle + "&pageReq=" + pageReq);
                                    return new ModelAndView("text", model);
                                } else {
                                    userSession.setStatusmessage(noStatementMessage);
                                    model.put("text", "ebppnostatement?account=" + id + "&cycle=" + trxCycle
                                            + "&pageReq=" + pageReq);
                                    return new ModelAndView("text", model);
                                }
                            }
                            // We are looking for the last cycle
                            else {
                                if (account.getEbpp2UserAccount().getAccountStatements() != null
                                        && account.getEbpp2UserAccount().getAccountStatements().size() > 0
                                        && account.getEbpp2UserAccount().getAccountStatements().get(0)
                                        .getStatementID() > 0) {
                                    model.put("text",
                                            "ebpp2ViewStatement?id="
                                                    + account.getEbpp2UserAccount().getAccountStatements().get(0)
                                                    .getStatementID()
                                                    + "&uai=" + account.getEbpp2UserAccount().getBillerAccountID());
                                    return new ModelAndView("text", model);
                                }
                                // The statement is not available
                                else {
                                    if (trxCycle != null) {
                                        userSession.setStatusmessage(noStatementMessage);
                                        model.put("text", "transaction?account=" + id + "&cycle=" + trxCycle
                                                + "&pageReq=" + pageReq);

                                        return new ModelAndView("text", model);
                                    } else {
                                        model.put("text", "ebppnostatement?account=" + id + "&cycle=" + trxCycle
                                                + "&pageReq=" + pageReq);

                                        return new ModelAndView("text", model);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        model.put("text", "ebppLatestHistory");
        return new ModelAndView("text", model);

    }

    public ModelAndView ebpp2ViewStatement(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> model = new HashMap<>();
        String id = (String) request.getParameter("id");
        String uai = (String) request.getParameter("uai");
        model.put("id", id);
        model.put("uai", uai);
        return new ModelAndView(new evertec.cibp.web.view.Ebpp2PDFView(), model);
    }

    public ModelAndView aggregation(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerServices customerService = userSession.getCustomerService();

        try {
            Integer aggcustomerid = customerService.getAggregationCustomerIdByUsername(userSession.getUsername());
            model.put("aggregatedAccounts", customerService.getAggregationAccountsByCustomerId(aggcustomerid));
        } catch (Exception e) {
            logger.error("aggregation(HttpServletRequest, HttpServletResponse)", e); //$NON-NLS-1$

            model.put("aggregationNoAccounts", e.getMessage());
        }

        return new ModelAndView("aggregation", model);
    }

    public ModelAndView ebppRedirect(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> model = new HashMap<>();
        String id = request.getParameter("id");
        model.put("docid", id);

        return new ModelAndView("ebppRedirect", model);
    }

    public ModelAndView ebppRedirectPrevious(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> model = new HashMap<>();
        String id = request.getParameter("id");
        model.put("docid", id);

        return new ModelAndView("ebppRedirectPrevious", model);
    }

    public ModelAndView productselection(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        Boolean noOpen = (Boolean) WebUtils.getSessionAttribute(request, "noopen");

        List<CIBPConfigurableTexts> textlist;
        Map<String, String> creditProducts = new LinkedHashMap<String, String>();

        if (userSession.hasVirginIslandAccount()) {
            model.put("profileCountry", "vi");
            textlist = userSession.getDBConfiguredTextList(CIBPConfigurableTextsTypes.UNICA,
                    ConfigurableTextSubSections.CREDIT_PRODUCTS_VI);
        } else if (userSession.hasBritishVirginIslandAccount()) {
            model.put("profileCountry", "bvi");
            textlist = userSession.getDBConfiguredTextList(CIBPConfigurableTextsTypes.UNICA,
                    ConfigurableTextSubSections.CREDIT_PRODUCTS_TORT);
        } else {
            model.put("profileCountry", "pr");
            textlist = userSession.getDBConfiguredTextList(CIBPConfigurableTextsTypes.UNICA,
                    ConfigurableTextSubSections.CREDIT_PRODUCTS_PR);
        }

        for (CIBPConfigurableTexts text : textlist) {
            if (userSession.getPreferredLanguage() == PreferredLanguage.ENGLISH) {
                creditProducts.put(text.getTextCode(), text.getEnglishDescription());
            } else {
                creditProducts.put(text.getTextCode(), text.getSpanishDescription());
            }
        }

        model.put("creditProducts", creditProducts);
        model.put("textlist", textlist);

        if (noOpen != null && noOpen) {
            WebUtils.setSessionAttribute(request, "noopen", null);
            model.put("noOpen", true);
        } else {
            String pending = request.getParameter("pending");
            model.put("pending", pending);
            boolean hasdepositaccount = Utils.hasAtLeastOneDepositAccount(userSession.getAccounts());
            model.put("hasdepositaccount", hasdepositaccount);
            // mpicado cibp-2137
            if (userSession.retornaEdad(userSession.getCustomerProfile().getBirthDate()) >= this.eaccountMinAgeLimit)
                model.put("hasAgeforEaccount", true);
            else
                model.put("hasAgeforEaccount", false);

            model.put("canEAccount", true);
            model.put("OPAC", false);
            if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC)) {
                model.put("OPAC", true);
                if (userSession.hasBritishVirginIslandAccount() || userSession.isComercialCustomer()) {
                    model.put("canEAccount", false);
                } else if (userSession.getCountry().toUpperCase().equals("PR")
                        || userSession.getCountry().toUpperCase().equals("VI")
                        || userSession.getCountry().toUpperCase().equals("USVI")) {
                    model.put("canEAccount", true);
                } else {
                    model.put("canEAccount", false);
                }
            }

            CustomerServices customerService = userSession.getCustomerService();

            if (customerService.getGlobalEntitlement(EntitlementType.OAO).getStatus() == EntitlementStatus.ENABLED)
                model.put("oao_up", true);
            else
                model.put("oao_up", false);

            // mpicado, cibp-1867
            if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
                model.put("Lang", "en");// ingles
            else
                model.put("Lang", "sp");// espanol

            // VALIDATION IF THE USER CAN OPEN A DEPOSIT ACCOUNT
            // IF IS NULL VALIDATION HAS NOT TAKEN PLACE
            if (userSession.getCanOpenDepositAccount() == null) {
                if (userSession.getBankingSession() != null) {
                    BigInteger permId = userSession.getBankingSession().getPermId();

                    if (permId == null) {
                        permId = new BigInteger("0");
                    }

                    userSession.setCanOpenDepositAccount(userSession.getCustomerService().canOpenDepositAccount(
                            userSession.getCustomerProfile(), String.valueOf(permId),
                            userSession.getCustomerWealthPreferred(this.wealthCodes, this.preferredCodes)));
                }
            }
            model.put("openAccount", userSession.getCanOpenDepositAccount());
            model.put("noOpen", false);
        }

        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC)) {
            model.put("opac", true);
        }
        return new ModelAndView("productselection", model);

    }

    public ModelAndView cancelPendingAccount(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        Map<String, Object> model = new HashMap<>();
        String pendingAccountId = ServletRequestUtils.getRequiredStringParameter(request, "id");
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        String statusMessage = null;
        boolean found = false;
        AccountApplications pendingApplication = userSession.getAccountApplications(true);
        String language = Utils.getRSALanguage(request);
        Locale locale = "es".equalsIgnoreCase(language) || "sp".equalsIgnoreCase(language) ? new Locale("es")
                : Locale.ENGLISH;

        for (AccountApplication pendingAccountApplication : pendingApplication.getPendingApplications()) {
            if (pendingAccountId.equals(pendingAccountApplication.getFrontEndId())
                    && pendingAccountApplication.isIncomplete()) {
                found = true;

                /**
                 * Cancel Pending Application Integration
                 * */
                boolean result = AccountsFacade.getInstance().cancelPendingApplication(userSession, pendingAccountApplication.getApplicationId());

                if (result)
                    statusMessage = messageSource.getMessage("portal.pending.cancelled", null,
                            "Account application cancelled", locale);
                else
                    statusMessage = messageSource.getMessage("portal.pending.notcancelled", null,
                            "Unable to cancel account application", locale);
                break;
            }
        }

        if (!found)
            statusMessage = messageSource.getMessage("portal.pending.notcancelled", null,
                    "Unable to cancel account application", locale);
        else {

            userSession.resetAccountApplications();
            if (userSession.getAccountApplications(true).getPendingApplications().size() == 0) {
                if (userSession.getUserSessionType().equals(UserSessionType.USERNAME_RESERVATION)) {
                    // TODO: DESTROY User and delete all pending remaingin apps
                    // from database and nerds
                	/**
					 * MBNA-3046
					 * Method deleteUserReservation (cancelUsernameReservation): CIBP-WEB microservice integration
					 */
                	SignOnCustomerServiceModule.getInstance().deleteUserReservation(userSession);
                    model.put("deletedUser", true);
                }

            }

        }

        userSession.setStatusmessage(statusMessage);

        return new ModelAndView("redirect:portal");

    }

    public ModelAndView pendingAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String pendingAccountId = ServletRequestUtils.getRequiredStringParameter(request, "id");
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

        AccountApplications pendingApplication = userSession.getAccountApplications(true);
        for (AccountApplication pendingAccountApplication : pendingApplication.getPendingApplications()) {
            if (pendingAccountId.equals(pendingAccountApplication.getFrontEndId())
                    && pendingAccountApplication.isIncomplete()) {
                String token;
                if (userSession.getUserSessionType().equals(UserSessionType.CUSTOMER_PROFILE))
                    token = userSession.getCustomerService().tokenGenerateForPendingApplication(
                            userSession.getCustomerProfile(), pendingAccountApplication.getApplicationId());
                else
                    token = userSession.getCustomerService().tokenGenerateForPendingApplication(
                            userSession.getUsernameReservation().getUsername(),
                            pendingAccountApplication.getApplicationId());

                if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                    return new ModelAndView("redirect:" + nerdsPrefillUrl + "?rtoken=" + token);
                else
                    return new ModelAndView("redirect:" + nerdsPrefillUrl + "?rtoken=" + token + "&lang=es");
            }
        }

        return new ModelAndView("portal");
    }

    public ModelAndView openAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

        if (!StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC) && !userSession.isCanOpenAdditionalAccounts()) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                userSession.setStatusmessage("<span style='color:red'>Please, verify for errors</span>");
            else
                userSession.setStatusmessage("<span style='color:red'>Favor de verificar errores</span>");

            return new ModelAndView("productselection", "pending", "1");
        }

        // OAO CODES VALIDATION
        if (!StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC)) {
            if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE179)) {
                if (userSession.getEaccountMessage() == null) {
                    userSession.setEaccountMessage(userSession.getCustomerService().getEaccountOpeningMessage(
                            userSession.getCustomerProfile().getTaxId(), userSession.getBankingSession().getPermId()));
                }
                if (!userSession.getEaccountMessage().equals(EaccountMessage.NO_MESSAGE)) {
                    Map<String, Object> model = new HashMap<>();
                    model.put("eaccountMessage", userSession.getEaccountMessage());
                    model.put("noOpenOAOCode", "true");
                    return new ModelAndView("productselection", model);
                }
            }
        }

        int owners = 0;
        String accountType = "checking"; // ServletRequestUtils.getRequiredStringParameter(request,"accountType");
        String ownershipType = ServletRequestUtils.getRequiredStringParameter(request, "ownershipType");

        // TODO: VALIDATE Values

        if (ownershipType.equalsIgnoreCase("joint")) {
            owners = ServletRequestUtils.getRequiredIntParameter(request, "owners");
        }

        ModifyCustomerRequest modifyCustomerRequest = new ModifyCustomerRequest(
                Utils.generateCustomerInteractionEvent(request), userSession.getCustomerProfile().getProfileId(),
                CustomerFlagType.PENDING_ACCOUNT_APPLICATION);
        CustomerFacade.getInstance().addCustomerFlag(userSession, modifyCustomerRequest);

        String token = "";

        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC)) {
            token = userSession.getCustomerService()
                    .tokenGenerateForProductSelectionUUID(userSession.getCustomerProfile(), accountType, ownershipType, owners);
            if (userSession.hasBritishVirginIslandAccount() || userSession.isComercialCustomer()) {
                return new ModelAndView("productselection");
            }

            if (userSession.getForeignCustomer() == null) {

            	try {

            		userSession.setForeignCustomer(false);

            		CISOpacCustomerInfoRequest cisRequest = new CISOpacCustomerInfoRequest();
        			CustomerProfile customerProfile = userSession.getCustomerProfile();
        			cisRequest.setCustomerPermId(customerProfile.getPermId().toString());
        			boolean canOpenAccount = false;

            		CISOpacCustomerInfoResponse cisResponse = userSession.getCustomerService().getCISCustomerInfo(cisRequest);

    				if (customerProfile.getPermId()!= null && cisResponse != null){
    					for(String opacCountry: opacCountries){
    						if( cisResponse.getCountry().equalsIgnoreCase(opacCountry)
    								&& StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC)
    								&& !(customerProfile.getUserInterface() == UserInterface.COMMERCIAL)
    								&& (userSession.retornaEdad(cisResponse.getDateOFBirth()) >= this.eaccountMinAgeLimit) ){
    							canOpenAccount= true;
    							break;
    						}
    					}
                        if(canOpenAccount){

                        	if(userSession.getCanOpenDepositAccount() == null) {
                        		userSession.setCanOpenDepositAccount(userSession.getCustomerService().canOpenDepositAccount(
                        				userSession.getCustomerProfile(), String.valueOf(userSession.getCustomerProfile().getPermId()),
                        				userSession.getCustomerWealthPreferred(this.wealthCodes, this.preferredCodes)));
                        	}

                        	userSession.setForeignCustomer(!userSession.getCanOpenDepositAccount());

                        }

    				}
            	}catch (Exception e) {
    				logger.error("Error al obtener informacion de apertura de cuenta: " + e.getMessage());
				}

            }

            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                return new ModelAndView("redirect:" + opacUrl + "?rtoken=" + token + "&foreignCustomer=" + userSession.getForeignCustomer() + "&language=en");
            else
                return new ModelAndView("redirect:" + opacUrl + "?rtoken=" + token + "&foreignCustomer=" + userSession.getForeignCustomer() + "&language=es");
        } // No DCI Flag required

        if (userSession.hasBritishVirginIslandAccount() || userSession.isComercialCustomer()) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                return new ModelAndView("redirect:" + nerdsPrefillUrl);
            else
                return new ModelAndView("redirect:" + nerdsPrefillUrl + "?lang=es");
        } else {
            token = userSession.getCustomerService()
                    .tokenGenerateForProductSelection(userSession.getCustomerProfile(), accountType, ownershipType, owners);
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                return new ModelAndView("redirect:" + nerdsPrefillUrl + "?rtoken=" + token);
            else
                return new ModelAndView("redirect:" + nerdsPrefillUrl + "?rtoken=" + token + "&lang=es");
        }

    }

    public ModelAndView portalonlyApps(HttpServletRequest request, HttpServletResponse response) throws Exception {

        return new ModelAndView("portalonlyApps");
    }

    public ModelAndView experianagreement(HttpServletRequest request, HttpServletResponse response) throws Exception {

        return new ModelAndView("experianagreement");
    }

    public ModelAndView welcomepage(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String pendingAccountId = ServletRequestUtils.getRequiredStringParameter(request, "id");
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        List<TVFrontendAccount> accounts = userSession.getAccounts();
        Map<String, Object> model = new HashMap<>();

        TVFrontendAccount account = Utils.getAccountById(pendingAccountId, accounts);
        if (account != null && !account.isNewAccount())
            account = null;

        if (account != null) {
            model.put("account", account);
            if (account.getPortalBalance() != null)
                model.put("showSignature", true);

            if (account.getTVAccount() != null && account.getTVAccount().getRecords() != null
                    && account.getTVAccount().getRecords().getOwners() != null) {
                for (int j = 0; j < account.getTVAccount().getRecords().getOwners().length; j++) {
                    Owner ow = account.getTVAccount().getRecords().getOwners()[j];
                    if (ow.getRelationshipType() == null || !ow.getRelationshipType().equalsIgnoreCase("005")) {
                        model.put("jointapp", true);
                        break;
                    }

                }
            }

            AccountApplications apps = userSession.getAccountApplications(true);

            List<AccountApplication> comaps = new LinkedList<AccountApplication>();
            if (apps != null)
                comaps = apps.getCompletedApplications();

            if (comaps != null)
                for (Iterator<AccountApplication> iterator = comaps.iterator(); iterator.hasNext(); ) {
                    AccountApplication ap = (AccountApplication) iterator.next();
                    if (account.getAccountNumber().equals(ap.getAccountNumber())) {
                        Date instance = ap.getCreationDate();
                        if (instance != null) {
                            Calendar date60 = Calendar.getInstance();
                            date60.setTime(instance);
                            date60.add(Calendar.DAY_OF_MONTH, 60);
                            model.put("infoAvailableExpiration", date60);
                            break;
                        }
                    }
                }
        }
        return new ModelAndView("welcomepage", model);
    }

    public ModelAndView personalinformation(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

        String showAlerts = (String) request.getParameter("showAlerts");
        if (GenericValidator.isBlankOrNull(showAlerts))
            model.put("showAlerts", false);
        else
            model.put("showAlerts", true);

        model.put("customer", userSession.getCustomerProfile());

        if (userSession.getCustomerProfile() != null) {
            if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
                SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
                if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
                        && smsprofile.getCtns().size() > 0) {
                    model.put("EnrollSMS", true);
                    model.put("smsPhone", ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn());
                } else
                    model.put("EnrollSMS", false);
            } else
                model.put("EnrollSMS", false);
        }

        int account_total = 0;
        List<TVFrontendAccount> accts = userSession.getAccounts();
        Map<String, TVFrontendAccount> filteraccounts = new LinkedHashMap<String, TVFrontendAccount>();
        for (Iterator<TVFrontendAccount> iterator = accts.iterator(); iterator.hasNext(); ) {
            TVFrontendAccount frontendAccount = (TVFrontendAccount) iterator.next();
            if (filteraccounts.get(frontendAccount.getAccountNumber()) == null)
                account_total++;
        }

        model.put("account_total", account_total);

        return new ModelAndView("personalinformation", model);
    }

    public ModelAndView alerttypes(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

        String showAlerts = (String) request.getParameter("showAlerts");
        if (GenericValidator.isBlankOrNull(showAlerts))
            model.put("showAlerts", false);
        else
            model.put("showAlerts", true);

        model.put("customer", userSession.getCustomerProfile());

        if (userSession.getCustomerProfile() != null) {
            if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
                SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
                if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
                        && smsprofile.getCtns().size() > 0) {
                    model.put("EnrollSMS", true);
                    model.put("smsPhone", ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn());
                } else
                    model.put("EnrollSMS", false);
            } else
                model.put("EnrollSMS", false);
        }

        int account_total = 0;
        List<TVFrontendAccount> accts = userSession.getAccounts();
        Map<String, TVFrontendAccount> filteraccounts = new LinkedHashMap<String, TVFrontendAccount>();
        for (Iterator<TVFrontendAccount> iterator = accts.iterator(); iterator.hasNext(); ) {
            TVFrontendAccount frontendAccount = (TVFrontendAccount) iterator.next();
            if (filteraccounts.get(frontendAccount.getAccountNumber()) == null)
                account_total++;
        }

        model.put("account_total", account_total);

        return new ModelAndView("alerttypes", model);
    }

    public ModelAndView statusmessage(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UserSession usersession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        String statusmessage = "";
        if (usersession != null)
            statusmessage = usersession.getStatusmessage();

        if (!GenericValidator.isBlankOrNull(statusmessage)) {
            usersession.setStatusmessage("");
            return new ModelAndView("statusmessage", "statusmessage", statusmessage);
        }
        return null;
    }

    public ModelAndView privateBankerInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

        CustomerWealthPreferred custWealthPreferred = userSession.getCustomerWealthPreferred(this.wealthCodes,
                this.preferredCodes);
        EmployeeInfoResponse bankerInfo = new EmployeeInfoResponse();
        String bankerImgPath = this.getPrivateBankerImg().getImagePath();
        if (custWealthPreferred != null) {
            bankerInfo = userSession.getPrivateBankerInfo(custWealthPreferred.getPrivateBankerNum());
            if (bankerInfo != null) {
                if (!userSession.isPrivateBankerImgLoaded()
                        && !ImageUtils.checkIfImageExists(bankerImgPath + bankerInfo.getEmployeeId() + ".jpg")) {
                    byte[] img = ImageUtils.getImageFromURL(bankerInfo.getPicture());
                    if (img != null) {
                        ImageUtils.writeImage(bankerImgPath + bankerInfo.getEmployeeId() + ".jpg", img);
                    }
                    userSession.setPrivateBankerImgLoaded(true);
                }
            }
        }

        if (bankerInfo != null) {
            model.put("bankerInfo", bankerInfo);
            model.put("bankerImg", bankerInfo.getEmployeeId() + ".jpg");
        }
        return new ModelAndView("portal.privatebanker", model);
    }

    @SuppressWarnings("unchecked")
	public ModelAndView ebppPortalInbox(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerProfile profile = userSession.getCustomerProfile();

        // If the EZ Statement codes are not yet set, set them
        if (userSession.getEzStatementInformativaProductIds() == null)
            userSession.setEzStatementInformativaProductIds(getEzStatementInformativaProductIds());

        if (userSession.getEzStatementMortgageProductIds() == null)
            userSession.setEzStatementMortgageProductIds(getEzStatementMortgageProductIds());

        // The EBPP2 user id is not yet in session; add it
        if (!userSession.isEbpp2UserIdLookedUp()) {
            EbppEntitlement ent = null;

            // If the EBPP2 entitlement is not created, create it
            if (profile.getEntitlementByType(EntitlementType.EBILLS) == null) {
                // Create new ebpp2 user
                CreateUserReq userReq = new CreateUserReq();
                userReq.setEmail(userSession.getCustomerProfile().getEmail());
                userReq.setFirstName(userSession.getCustomerProfile().getFirstName());
                userReq.setLastName(userSession.getCustomerProfile().getLastName());
                userReq.setUserName(userSession.getCustomerProfile().getUsername());
                int userId = userSession.getCustomerService().createEbpp2User(userReq);
                String ebpp2UserId = (userId > 0) ? Integer.toString(userId) : "";
                ent = new EbppEntitlement(profile.getTaxId(), profile.getEnrollmentAccountNumber(), ebpp2UserId,
                        EntitlementStatus.ENABLED);
            }

            if (profile.getEntitlementByType(EntitlementType.EBILLS) != null) {
                ent = new EbppEntitlement(profile.getEntitlementByType(EntitlementType.EBILLS));

                // If the username is blank, create one
                if ("".equals(ent.getUsername()) || null == ent.getUsername()) {

                    // Create new ebpp2 user
                    CreateUserReq userReq = new CreateUserReq();
                    userReq.setEmail(userSession.getCustomerProfile().getEmail());
                    userReq.setFirstName(userSession.getCustomerProfile().getFirstName());
                    userReq.setLastName(userSession.getCustomerProfile().getLastName());
                    userReq.setUserName(userSession.getCustomerProfile().getUsername());
                    int userId = userSession.getCustomerService().createEbpp2User(userReq);
                    String ebpp2UserId = (userId > 0) ? Integer.toString(userId) : "";
                    ent.setUsername(ebpp2UserId);
                    CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(ent.getCustomerEntitlement()));
                    userSession.setEbpp2UserId(ebpp2UserId);
                } else {
                    userSession.setEbpp2UserId(ent.getUsername());
                }
                userSession.setEbpp2UserIdLookedUp(true);
            }
        }

        // The user has an EBPP2 ID
        if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals(""))
            return new ModelAndView("redirect:ebpp2PortalInbox");

        return new ModelAndView("ebppInbox", model);
    }

    public ModelAndView ebpp2PortalInbox(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerServices customerService = userSession.getCustomerService();
        CustomerProfile profile = userSession.getCustomerProfile();
        BankingSession bankingSession = userSession.getBankingSession();

        boolean foundEbpp2Statements = false;
        boolean foundInterestStmt = false;
        boolean foundVaultStatements = false;

        if (profile.hasEntitlement(EntitlementType.EBILLS) && isEbppAvailable() && customerService
                .getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {

            List<UserAccount> ebpp2StatementInbox = new LinkedList<UserAccount>();

            // The user has an EBPP2 ID
            if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

                // This is the list that will contain all the accounts and their
                // respective summaries
                List<UserAccount> ebpp2UserAccountsAndSummaries = userSession.getEbpp2UserAccountsAndSummaries();
                boolean refresh = false;

                // Get the EBPP2 biller product information for e-accounts
                Ebpp2BillerProductIdResponse ebpp2BillerProductIdResponse = customerService
                        .getEbpp2BillerProductIdInformationDepositAccount("081");

                int billerProductId = ebpp2BillerProductIdResponse.getBillerProductId();

                // Find all the user's e-accounts
                if (userSession.isHasEAccounts()) {
                    for (TVFrontendAccount account : userSession.getAccounts()) {
                        if (account.getAccountProductId() != null && account.getAccountProductId().equals("081")) {
                            if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
                                /**
                                 * Move to Ajax
                                 * @see AjaxActionsController#activateEAccount(HttpServletRequest request, HttpServletResponse response)
                                 *
                                 * */
                            } else {

                                boolean found = false;

                                // Is this account already active?
                                for (UserAccount userAccount : userSession.getEbpp2UserAccounts()) {
                                    if (userAccount.getAccountNumber().replaceFirst("^0+(?!$)", "")
                                            .equals(account.getAccountNumber().replaceFirst("^0+(?!$)", ""))) {
                                        found = true;
                                        break;
                                    }
                                }

                                // If the account is not active in EBPP2,
                                // activate
                                // it
                                if (!found) {
                                    boolean continueProcess = customerService.activateEbpp2AccountInIDA(bankingSession,
                                            account);
                                    if (continueProcess) {
                                        RegisterUserAccountReq req = new RegisterUserAccountReq();
                                        req.setAccountNumber(account.getAccountNumber());
                                        req.setBillerId(1);
                                        req.setBillerProductId(billerProductId);
                                        req.setLabel(account.getNickname());
                                        req.setNss(profile.getTaxId());
                                        req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
                                        req.setZip("00000");
                                        RegisterUserAccountResp resp = customerService.registerEbpp2UserAccount(req);
                                        if (!resp.getResponseStatus().equals(ResponseStatus.ERROR)
                                                && resp.getRegisterResponse().equals("0"))
                                            refresh = true;
                                    }
                                }
                            }
                        }
                    }

                    // If we registered any e-accounts, refresh the user
                    // accounts and summaries
                    if (refresh) {
                        userSession.setEbpp2UserAccounts(null);
                        userSession.setEbpp2UserAccountsAndSummaries(null);
                        ebpp2UserAccountsAndSummaries = userSession.getEbpp2UserAccountsAndSummaries();
                    }
                }

                // Added statements from vault.
                if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
                    /*
                     * Change for new services
                     *
                     * */


                    List<AccountStmtInfo> vaultAccountEbillsNotViewed = new ArrayList<>(); //Json
                    List<AccountPDFStmtInfo> vaultAccountEbillsStatementNotViewed = new ArrayList<>();
                    List<AccountPDFStmtInfo> vaultAccountStatementNotViewed = new LinkedList<>();

                    try {
                        List<CreditCardStatement_Type> ccaStatements = userSession.getCcaCurrentStatements();
                        if (ccaStatements != null && !ccaStatements.isEmpty()) {
                            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                            for (Iterator<CreditCardStatement_Type> iterator = ccaStatements.iterator(); iterator
                                    .hasNext(); ) {
                                CreditCardStatement_Type ccs = iterator.next();
                                AccountPDFStmtInfo pdfstm = new AccountPDFStmtInfo();
                                TVFrontendAccount acc = Utils.getAccountByAccountNumber(ccs.getAccountId(),
                                        userSession.getAccounts());
                                if (acc == null) {
                                    continue;//Not in the relationships
                                }
                                foundVaultStatements = true;
                                pdfstm.setAccount_name(acc.getDescription());
                                pdfstm.setAccount_nickname(acc.getNickname());
                                pdfstm.setInvoice_date(format.format(ccs.getStatementCycleDt().getTime()));
                                vaultAccountEbillsStatementNotViewed.add(pdfstm);
                                AccountStmtInfo accStmtInfo = new AccountStmtInfo();
                                List<AccountPDFStmtInfo> accountStatements = new ArrayList<>();
                                accountStatements.add(pdfstm);
                                if (ccs.getStatementCycleDt() != null) {
                                    accStmtInfo.setLastPaymentDate(format.format(ccs.getStatementCycleDt().getTime()));
                                }
                                if (ccs.getStmtMinPmtDueAmt() == null)
                                    ccs.setStmtMinPmtDueAmt(new BigDecimal(0));
                                accStmtInfo.setMinimumAmountDue(ccs.getStmtMinPmtDueAmt());

                                accStmtInfo.setAmountDue(ccs.getStmtEndingBal());
                                if (ccs.getStmtDueDt() != null) {
                                    accStmtInfo.setDueDate(format.format(ccs.getStmtDueDt().getTime()));
                                }
                                accStmtInfo.setLastStatementDate("");
                                accStmtInfo.setAccountStatements(accountStatements);
                                accStmtInfo.setBillerAccountID(acc.hashCode());
                                accStmtInfo.setAccountLabel(acc.getNickname());
                                accStmtInfo.setAccountNumber(acc.getAccountNumber());
                                vaultAccountEbillsNotViewed.add(accStmtInfo);
                            }

                        }
                        List<DepositStatement_Type> ddaStatements = userSession.getDdaCurrentStatements();
                        if (ddaStatements != null && !ddaStatements.isEmpty()) {
                            for (Iterator<DepositStatement_Type> iterator = ddaStatements.iterator(); iterator.hasNext(); ) {
                                DepositStatement_Type ddaStmt = iterator.next();

                                AccountPDFStmtInfo pdfstm = new AccountPDFStmtInfo();
                                TVFrontendAccount acc = Utils.getAccountByAccountNumber(ddaStmt.getAccountId(),
                                        userSession.getAccounts());
                                if (acc == null) {
                                    continue;//Not in the relationships
                                }
                                foundVaultStatements = true;
                                pdfstm.setAccount_name(acc.getDescription());
                                pdfstm.setAccount_nickname(acc.getNickname());
                                vaultAccountStatementNotViewed.add(pdfstm);// Add Statements
                            }
                        }
                    } catch (Exception e) {
                        logger.error("Current Statements error:" + e.getMessage());
                    }
                    model.put("vaultAccountCurrentEbills", vaultAccountEbillsStatementNotViewed);
                    model.put("vaulteBillsNotViewed", vaultAccountEbillsNotViewed);
                    model.put("vaultAccountCurrentStatements", vaultAccountStatementNotViewed);
                    model.put("eBillsFlag", true);
                }

                // Now loop through all the summaries and pick out the ones to
                // show in the inbox
                if (ebpp2UserAccountsAndSummaries != null && ebpp2UserAccountsAndSummaries.size() > 0) {
                    for (UserAccount userAccount : ebpp2UserAccountsAndSummaries) {
                        if (userAccount.isActive()) {
                            if (userAccount.getAccountStatements() != null
                                    && userAccount.getAccountStatements().size() > 0) {
                                if (!userAccount.isLastStatementViewed() && !userAccount.isLastEbillPaid()
                                        && !userAccount.isLastEbillViewed()) {
                                    foundEbpp2Statements = true;
                                    ebpp2StatementInbox.add(userAccount);
                                }
                            }
                        }
                    }
                }
            }

            if (foundEbpp2Statements)
                model.put("ebpp2StatementInbox", ebpp2StatementInbox);

            // Interest Statements
            List<AccountPDFStmtInfo> intStmtALLInbox = userSession.getInterestStatementNewInboxNotification();
            List<AccountPDFStmtInfo> intStmtInbox = new LinkedList<AccountPDFStmtInfo>();
            Date dt = new Date();
            SimpleDateFormat dtformat = new SimpleDateFormat("MM");
            if (intStmtALLInbox != null) {
                for (Iterator<AccountPDFStmtInfo> iterator = intStmtALLInbox.iterator(); iterator.hasNext(); ) {
                    AccountPDFStmtInfo intStmtInfo = (AccountPDFStmtInfo) iterator.next();
                    if (intStmtInfo.getAccount_type().equals("IIM") || intStmtInfo.getAccount_type().equals("IDA")) {
                        // If it is an IRA, add it (this only shows the ones not
                        // viewed)
                        intStmtInbox.add(intStmtInfo);
                    } else if (intStmtInfo.getAccount_type().equals("MLA") && Arrays
                            .asList(ezStatementMortgageProductIds).contains(intStmtInfo.getEzStmtProductId())) {
                        if (stmtHasNotViewLess13(userSession, intStmtInfo))
                            intStmtInbox.add(intStmtInfo);
                    } else {
                        // If is is mortgage/auto/leasing, only show it if it is
                        // between the start and end months
                        if (Integer.parseInt(dtformat.format(dt)) >= Integer.parseInt(ezStmtMonthStart)
                                && Integer.parseInt(dtformat.format(dt)) <= Integer.parseInt(ezStmtMonthEnd)) {
                            intStmtInbox.add(intStmtInfo);
                        }
                    }
                }
            }

            if (intStmtInbox != null && intStmtInbox.size() > 0) {
                model.put("interestStmtInbox", intStmtInbox);
                foundInterestStmt = true;
                model.put("iraQuarterlyEzStmtProductId", ezStatementInformativaIRAQuarterlyProductId);
                model.put("caeQuarterlyEzStmtProductId", ezStatementInformativaCAEQuarterlyProductId);
                model.put("iraYearlyEzStmtProductId", ezStatementInformativaIRAYearlyProductId);
                model.put("caeYearlyEzStmtProductId", ezStatementInformativaCAEYearlyProductId);
                model.put("ezStatementMortgageProductIds", ezStatementMortgageProductIds);
            }

            if (!foundInterestStmt && !foundEbpp2Statements && !foundVaultStatements) {
                model.put("noInbox", "Yes");
            }

        } else if (!profile.hasEntitlement(EntitlementType.EBILLS)) {
            model.put("noBillers", "Yes");
        }

        return new ModelAndView("ebpp2Inbox", model);
    }

    public boolean stmtHasNotViewLess13(UserSession userSession, AccountPDFStmtInfo accountPDFStmtInfo) {
        List<AccountPDFStmtInfo> stmtList = null;
        stmtList = userSession.getIntPrevPDFStmtsByAccountAndProductId(accountPDFStmtInfo);

        if (!accountPDFStmtInfo.getViewed())
            return true;
        if (stmtList != null && stmtList.size() > 0) {
            int count = 0;
            for (Iterator<AccountPDFStmtInfo> iterator = stmtList.iterator(); iterator.hasNext();) {
            	count++;
                AccountPDFStmtInfo intStmtInfo = (AccountPDFStmtInfo) iterator.next();
                if (count < 12 && !intStmtInfo.getViewed()) {
                    return true;
                }
            }
        }

        return false;
    }

    public ModelAndView estatementcampaignfail(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        UserSession userSession = new UserSession();//user session
        final String strUserSession = "userSession";//user session string

        if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
            userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
        }
        Map<String, Object> model = new HashMap<>(); //model result

        //MBFM-99
        final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
        model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
        //END MBFM-99

        return new ModelAndView("estatementcampaignfail", model);
    }

    public ModelAndView estatementcampaignsuccess(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        List<SignupCampaign> signupCampaignInfoByProfileId = userSession.getCustomerService()
                .getSignupCampaignInfoByProfileId(userSession.getCustomerProfile());
        SignupCampaign signupCampaign = null;

        //MBFM-99
        final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
        model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
        //END MBFM-99

        for (Iterator<SignupCampaign> iter = signupCampaignInfoByProfileId.iterator(); iter.hasNext(); ) {
            SignupCampaign camp = (SignupCampaign) iter.next();
            if (camp.getCampaignType().equals(CampaignType.ESTMT.toString())) {
                signupCampaign = camp;
                break;
            }
        }
        if (signupCampaign != null)
            model.put("addedAccounts", signupCampaign.getAddedAccounts());
        model.put("email", userSession.getCustomerProfile().getEmail());
        return new ModelAndView("estatementcampaignsuccess", model);
    }

    public ModelAndView sessionping(HttpServletRequest request, HttpServletResponse response) throws Exception {

        return new ModelAndView(new EmptyView());
    }

    public ModelAndView ebppdown(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        Map<String, Object> model = new HashMap<>();

        model.put("lang", userSession.getPreferredLanguage().getCode());

        return new ModelAndView("ebppdown", model);
    }

    public ModelAndView ebppnostatement(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> model = new HashMap<>();
        String account = request.getParameter("account");
        String cycle = request.getParameter("cycle");
        String pageReq = request.getParameter("pageReq");

        model.put("account", account);
        model.put("cycle", cycle);
        model.put("pageReq", pageReq);

        return new ModelAndView("ebppnostatement");
    }

    public ModelAndView terms(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return new ModelAndView("terms-" + Utils.getRSALanguage(request));
    }

    public ModelAndView billpaydowntime(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UserSession userSession = new UserSession();//user session
        final String strUserSession = "userSession";//user session string

        if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
            userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
        }
        Map<String, Object> model = new HashMap<>(); //model result

        //MBFM-99
        final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
        model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
        //END MBFM-99

        return new ModelAndView("billpaydowntime", model);
    }

    public ModelAndView transfersdowntime(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UserSession userSession = new UserSession();//user session
        final String strUserSession = "userSession";//user session string

        if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
            userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
        }
        Map<String, Object> model = new HashMap<>(); //model result

        //MBFM-99
        final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
        model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
        //END MBFM-99

        return new ModelAndView("transfersdowntime", model);
    }

    public ModelAndView enrollmentdowntime(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UserSession userSession = new UserSession();//user session
        final String strUserSession = "userSession";//user session string

        if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
            userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
        }
        Map<String, Object> model = new HashMap<>(); //model result

        //MBFM-99
        final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
        model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
        //END MBFM-99

        return new ModelAndView("enrollmentdowntime", model);
    }

    public ModelAndView signondowntime(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UserSession userSession = new UserSession();//user session
        final String strUserSession = "userSession";//user session string

        if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
            userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
        }
        Map<String, Object> model = new HashMap<>(); //model result

        //MBFM-99
        final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
        model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
        //END MBFM-99

        return new ModelAndView("signondowntime", model);
    }

    public ModelAndView usave_contract1_en(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return new ModelAndView("usave_contract1_en");
    }

    public ModelAndView usave_contract2_en(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return new ModelAndView("usave_contract2_en");
    }

    public ModelAndView usave_contract1_es(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return new ModelAndView("usave_contract1_es");
    }

    public ModelAndView usave_contract2_es(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return new ModelAndView("usave_contract2_es");
    }

    public ModelAndView mobileSwitch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = InternationalCharacterUtils
                .sanatizeInputs(ServletRequestUtils.getStringParameter(request, "target", "login"), false);
        String version = InternationalCharacterUtils
                .sanatizeInputs(ServletRequestUtils.getStringParameter(request, "version", "classic"), false);

        if ("classic".equalsIgnoreCase(version)) {

            CookieGenerator cg = new CookieGenerator();
            cg.setCookieName("client-version");
            cg.setCookieMaxAge(-1);
            cg.addCookie(response, "classic");

        } else if ("mobile".equalsIgnoreCase(version)) {

            CookieGenerator cg = new CookieGenerator();
            cg.setCookieName("client-version");
            // cg.removeCookie(response);
            cg.setCookieMaxAge(-1);
            cg.addCookie(response, "mobile");

        }
        
        String RTOKEN = Optional.ofNullable(request.getParameter("rtoken")).orElse(StringUtils.EMPTY);        
        return redirectMBDP(target, RTOKEN);
    }
    
    
	public ModelAndView redirectMBDP(String target, String rtoken) {

		final String REDIRECT = "redirect:";// the variable to redirect to another url
		final String RTOKEN_URL = "?rtoken=";// the rtoken used to come from another system

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBDP2013)) {
			if ("portal".equalsIgnoreCase(target)) {
				return new ModelAndView("redirect:portal");
			} else if (!rtoken.isEmpty()) {
				return new ModelAndView(REDIRECT + target + RTOKEN_URL + rtoken);//
			} else {
				return new ModelAndView("redirect:" + target);// CIBP-1904 bgarcia
			}
		} else {
			if ("portal".equalsIgnoreCase(target)) {
				return new ModelAndView("redirect:portal");
			} else {
				return new ModelAndView("redirect:" + target);// CIBP-1904 bgarcia
			}
		}

	}

    public ModelAndView reg_e_es(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return new ModelAndView("reg_e_es");
    }

    public ModelAndView reg_e_en(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return new ModelAndView("reg_e_en");
    }

    // cibp-2218
    public ModelAndView gsVideo(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<>();
        String pageRequest = request.getParameter("page");
        model.put("gsPage", pageRequest);
        return new ModelAndView("gsVideo", model);
    }

    public ModelAndView gsVistazoFrame(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<>();
        model.put("context", request.getContextPath());
        return new ModelAndView("gsVistazoFrame", model);
    }

    private String nerdsPrefillUrl;
    private String ezStmtMonthStart;
    private String ezStmtMonthEnd;

    public String getNerdsPrefillUrl() {
        return nerdsPrefillUrl;
    }

    public void setNerdsPrefillUrl(String nerdsPrefillUrl) {
        this.nerdsPrefillUrl = nerdsPrefillUrl;
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    /**
     * @param ezStmtMonthStart the ezStmtMonthStart to set
     */
    public void setEzStmtMonthStart(String ezStmtMonthStart) {
        this.ezStmtMonthStart = ezStmtMonthStart;
    }

    /**
     * @return the ezStmtMonthStart
     */
    public String getEzStmtMonthStart() {
        return ezStmtMonthStart;
    }

    /**
     * @param ezStmtMonthEnd the ezStmtMonthEnd to set
     */
    public void setEzStmtMonthEnd(String ezStmtMonthEnd) {
        this.ezStmtMonthEnd = ezStmtMonthEnd;
    }

    /**
     * @return the ezStmtMonthEnd
     */
    public String getEzStmtMonthEnd() {
        return ezStmtMonthEnd;
    }

    public void setWealthCodes(String wealthCodes) {
        this.wealthCodes = wealthCodes;
    }

    public String getWealthCodes() {
        return wealthCodes;
    }

    public void setPrivateBankerImg(PrivateBankerImageInfo privateBankerImg) {
        this.privateBankerImg = privateBankerImg;
    }

    public PrivateBankerImageInfo getPrivateBankerImg() {
        return privateBankerImg;
    }

    public void setPreferredCodes(String preferredCodes) {
        this.preferredCodes = preferredCodes;
    }

    public String getPreferredCodes() {
        return preferredCodes;
    }

    private boolean isEbppAvailable() {
        boolean ebppStatus = false;

        if (this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
                .equals(EntitlementStatus.ENABLED.toString())) {
            ebppStatus = true;
        } else {
            ebppStatus = false;
        }

        return ebppStatus;
    }

    // Remote Deposit
    public ModelAndView customerEntitlements(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerProfile customer = userSession.getCustomerProfile();

        List<CustomerEntitlement4Hibernate> entitlements = customer.getCustEntitlements();
        model.put("entitlements", entitlements);
        return new ModelAndView("customerEntitlements", model);
    }


    public ModelAndView getRemoteDepositEasyAcceptTerms(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerProfile customerProfile = userSession.getCustomerProfile();

        if (customerProfile.getEntitlementByType(EntitlementType.RDC) != null) {
            RDCEntitlement ent = new RDCEntitlement(
                    customerProfile.getEntitlementByType(EntitlementType.RDC));
            model.put("acceptTerms", ent.getRdcEasyDepositAcceptedTerms());
            model.put("dateAcceptTerms", ent.getRdcDateEasyDepositAcceptedTerms());
            model.put("amountChargeTerms", getRemoteDepositAmountCharge());
        } else {
            model.put("acceptTerms", false);
            model.put("dateAcceptTerms", "");
            model.put("amountChargeTerms", "");
        }

        return new ModelAndView("getRemoteDepositEasyAcceptTerms", model);

    }


    @SuppressWarnings("unchecked")
    public ModelAndView remotedeposithistoryimages(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerServices customerService = userSession.getCustomerService();

        String deposit = request.getParameter("depositId");

        if (deposit != null) {
            RDCEntitlement rdcEnt = userSession.getFisInfo();
            GetImagesReq req = new GetImagesReq();
            req.setBid(rdcBid);
            req.setUserID(rdcEnt.getRdcCustomerId());
            long[] id = new long[1];
            id[0] = Long.parseLong(request.getParameter("depositId"));
            req.setDepositID(id);

            logger.info("Init FIS Getting images of deposit:" + deposit + " for user:" + userSession.getUsername());
            ImagesResp resp = customerService.getImages(req);
            logger.info("End FIS getting images of deposit:" + deposit + " for user:" + userSession.getUsername());
            if (resp != null && resp.getResponseStatus() == evertec.otherservices.enums.ResponseStatus.SUCCESS) {
                logger.info("Images were retrieved successfully for deposit:" + deposit + " for user:"
                        + userSession.getUsername());
                logger.info("Init the images convertion for images of deposit:" + deposit + " for user:"
                        + userSession.getUsername());
                if (resp.getImages() != null) {
                    List<RDCImages> images = CollectionUtils.arrayToList(resp.getImages());

                    if (images != null && images.size() > 0) {

                        if (images.get(0).getFrontImage() != null && images.get(0).getFrontImage().length > 0) {
                            try {
                                String frontImage = new String(Base64.encodeBase64(images.get(0).getFrontImage()));
                                model.put("frontImage", frontImage);
                            } catch (Exception e) {
                                logger.error("Could not get the front image for:" + deposit + " for user:"
                                        + userSession.getUsername());
                            }
                        }

                        if (images.get(0).getBackImage() != null && images.get(0).getBackImage().length > 0) {
                            try {
                                String backImage = new String(Base64.encodeBase64(images.get(0).getBackImage()));
                                model.put("backImage", backImage);
                            } catch (Exception e) {
                                logger.error("Could not get the back image for:" + deposit + " for user:"
                                        + userSession.getUsername());
                            }
                        }
                    }
                }
                logger.info("End the images convertion for images of deposit:" + deposit + " for user:"
                        + userSession.getUsername());
            }
            model.put("depositId", deposit);
        }
        return new ModelAndView("remotedeposithistoryimages", model);
    }

    @SuppressWarnings("unchecked")
    public ModelAndView remotedeposithistory(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerServices customerService = userSession.getCustomerService();
        RDCEntitlement rdcEnt = userSession.getFisInfo();

        GetDepositHistoryReq req = new GetDepositHistoryReq();
        req.setBid(rdcBid);
        req.setUserID(rdcEnt.getRdcCustomerId());
        req.setSize(RequestSize.THIRTY);
        req.setDirection(RequestDirection.next);
        req.setUnit(RequestUnit.day);

        logger.info("Init getting FIS Deposit History for user:" + userSession.getUsername());
        TransactionHistoryResp depositReceipts = customerService.depositHistory(req);
        logger.info("End getting FIS Deposit History for user:" + userSession.getUsername());

        List<RemoteDepositHistory> pending = new LinkedList<RemoteDepositHistory>();
        List<RemoteDepositHistory> transfers = new LinkedList<RemoteDepositHistory>();
        if (depositReceipts != null
                && depositReceipts.getResponseStatus() == evertec.otherservices.enums.ResponseStatus.SUCCESS
                && depositReceipts.getTransferHistory() != null && depositReceipts.getTransferHistory().length > 0) {
            List<HistoryTransaction> receipts = CollectionUtils.arrayToList(depositReceipts.getTransferHistory());
            List<TVFrontendAccount> accounts = userSession.getFisFilterAccounts();

            int transferCount = 0;
            for (Iterator<HistoryTransaction> iterator = receipts.iterator(); iterator.hasNext(); ) {
                RemoteDepositHistory depositHistory = new RemoteDepositHistory();
                HistoryTransaction name = iterator.next();
                TVFrontendAccount acc = Utils.getAccountByAccountNumberAndType(name.getAccountType(),
                        name.getAccountNumber(), accounts, userSession.getCustomerService());
                depositHistory.setAccount(acc);
                depositHistory.setHistTransaction(name);
                if (name.getSummaryStatus().equals("Submitted")) {
                    pending.add(depositHistory);
                } else {
                    if (transferCount < 15) {
                        transfers.add(depositHistory);
                        transferCount += 1;
                    }
                }
                depositHistory = null;
            }
        }
        logger.info("FIS Deposit History Records Retrived for user: " + userSession.getUsername() + " was: "
                + (pending.size() + transfers.size()));

        model.put("pendingTransfers", pending);
        model.put("processedTransfers", transfers);

        return new ModelAndView("remotedeposithistory", model);

    }

    public ModelAndView mobilePhoneProviders(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<>();

        String jsonProviders = this.getMobilePhoneProviders();
        model.put("providers", jsonProviders);
        return new ModelAndView("mobilePhoneProviders", model);
    }

    // Customer profile lite
    public ModelAndView createLiteProfile(HttpServletRequest request, HttpServletResponse response) {
        String viewName = "createLiteProfile";
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        Map<String, Object> model = new HashMap<>();

        if (userSession == null) {
            model.put("error", true);
            model.put("status", EnrollmentLiteStatus.NO_SESSION_ERROR.getCode());
            return new ModelAndView(viewName, model);
        }

        if (userSession.getCustomerProfile() == null || (userSession
                .getUserSessionType() == UserSessionType.CUSTOMER_PROFILE_LITE
                && (userSession.getCustomerProfile().getCustomerPhone() == null || GenericValidator
                .isBlankOrNull(userSession.getCustomerProfile().getCustomerPhone().getPhoneNumber())))) {

            model.put("error", true);
            model.put("status", EnrollmentLiteStatus.MISSING_INFO.getCode());
            return new ModelAndView(viewName, model);
        }

        String phoneNumber = userSession.getCustomerProfile().getCustomerPhone().getPhoneNumber();

        if (GenericValidator.isBlankOrNull(phoneNumber)) {
            model.put("error", true);
            model.put("status", EnrollmentLiteStatus.MISSING_INFO.getCode());
            return new ModelAndView(viewName, model);
        } else { // Eliminate data from previous
            if (userSession.getUserSessionType() == UserSessionType.CUSTOMER_PROFILE_LITE) {
                userSession.setMobileCashService(mobileCashService);
                Utils.deletePreviousEnrollmentData(userSession, phoneNumber);

                CustomerProfileLite customerProfileLite = enrollNonCustomer(userSession);
                if (customerProfileLite != null && customerProfileLite.getCustomerDevice() != null) {
                    model.put("token", customerProfileLite.getCustomerDevice().getDeviceMbopId());
                }
            } else {
                enrollCustomer(userSession);
            }

            model.put("status", EnrollmentLiteStatus.ENROLL_COMPLETE.getCode());
            return new ModelAndView(viewName, model);
        }
    }

    private void enrollCustomer(UserSession userSession) {
        // TODO: Test this LALR
        GlobalEntitlement globalEntitlement = userSession.getCustomerService()
                .getGlobalEntitlement(EntitlementType.CASH_DROP);

        CustomerEntitlement cashDropEntitlement = new CustomerEntitlement(EntitlementType.CASH_DROP,
                EntitlementStatus.ENABLED);
        cashDropEntitlement.setGlobalEntitlement(globalEntitlement);
        cashDropEntitlement.setCustomerProfile(userSession.getCustomerProfile());
        cashDropEntitlement.setProfileId(userSession.getCustomerProfile().getProfileId());
        cashDropEntitlement.setType(EntitlementType.CASH_DROP);
        cashDropEntitlement.putString("STATUS", EntitlementStatus.ENABLED.toString());

        userSession.getCustomerService().getCustomerProfileService().addEntitlement(userSession.getCustomerProfile(),
                cashDropEntitlement);

        if (CustomerFacade.getInstance().addCustomerPhone(userSession))
            userSession.getCustomerService().sendMobileCashPhoneBoundEmail(userSession.getCustomerProfile(),
                    userSession.getPreferredLanguage());

        CustomerFacade.getInstance().addCustomerDevice(userSession, userSession.getCustomerProfile().getCustomerDevice());

    }

    private CustomerProfileLite enrollNonCustomer(UserSession userSession) {
        // Enroll in customer profile lite, Customer phone and customer device
        CustomerProfileLite customerProfileLite = userSession.getCustomerService()
                .enrollCustomerProfileLite(Utils.getLiteFromCustomerProfile(userSession.getCustomerProfile()));
        userSession.setCustomerProfile(Utils.getCustomerProfileFromLite(customerProfileLite));
        return customerProfileLite;
    }

    public ModelAndView hasMobilePhoneAlerts(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        boolean error = false;
        int status = 0;

        SMSCTN customerPhone = new SMSCTN();
        customerPhone.setCtn("");
        try {

            if (userSession == null) {
                logger.error("Null userSession");
                error = true;
                status = EnrollmentLiteStatus.NO_SESSION_ERROR.getCode();
            } else {
                customerPhone = userSession.getAlertsPhoneNumber(request);
                if (GenericValidator.isBlankOrNull(customerPhone.getCtn())) {
                    customerPhone.setCtn("");
                    status = EnrollmentLiteStatus.NO_CUSTOMER_PHONE_FOUND.getCode();
                } else {
                    status = EnrollmentLiteStatus.CUSTOMER_PHONE_FOUND.getCode();
                }
            }
        } catch (Exception e) {
            error = true;
            status = EnrollmentLiteStatus.BACKEND_ERROR.getCode();
            return new ModelAndView("hasMobilePhoneAlerts", model);
        }

        if (status == EnrollmentLiteStatus.CUSTOMER_PHONE_FOUND.getCode()
                && !GenericValidator.isBlankOrNull(customerPhone.getCtn())) {
            CustomerDeviceResponse phoneResponse = customerServices.isPhoneNumberInBlacklist(customerPhone.getCtn());

            if (phoneResponse != null && phoneResponse.getResponseStatus() != null
                    && phoneResponse.getResponseStatus().equals(EnrollmentLiteStatus.PHONE_IN_BLACKLIST)) {
                status = EnrollmentLiteStatus.PHONE_IN_BLACKLIST.getCode();
                error = true;
            }
        }

        model.put("error", error);
        model.put("status", status);
        model.put("alertsPhoneNumber", customerPhone.getCtn());
        return new ModelAndView("hasMobilePhoneAlerts", model);
    }

    /**
     * Service that generates an SMS code and sends it to the provided phone
     *
     * @param request  The HttpServletRequest request
     * @param response The HttpServletResponse response
     * @return The status of the response
     */
    public ModelAndView generateSmsCode(HttpServletRequest request, HttpServletResponse response) {
        String viewName = "generateSmsCode";
        Map<String, Object> model = new HashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        String resendCode = request.getParameter("resendCode");

        if (userSession == null) {
            model.put("error", true);
            model.put("status", EnrollmentLiteStatus.NO_SESSION_ERROR.getCode());
            return new ModelAndView(viewName, model);
        }

        if (userSession.getCustomerProfile() == null || userSession.getCustomerProfile().getCustomerPhone() == null
                || GenericValidator
                .isBlankOrNull(userSession.getCustomerProfile().getCustomerPhone().getPhoneNumber())) {
            model.put("error", true);
            model.put("status", EnrollmentLiteStatus.MISSING_INFO.getCode());
            return new ModelAndView(viewName, model);
        }

        int randCode = ESAPI.randomizer().getRandomInteger(1000, 8999);
        String newCode = String.valueOf(randCode);
        userSession.setSmsGeneratedCode(newCode);

        if (GenericValidator.isBlankOrNull(resendCode) || "false".equalsIgnoreCase(resendCode)) {
            userSession.setSmsSendRetries(0);
            userSession.setSmsGlobalRetries(0);

        } else {
            int resentCount = userSession.getSmsSendRetries();
            if (resentCount == 3) {
                model.put("error", false);
                model.put("status", EnrollmentLiteStatus.SMS_RESEND_LIMIT.getCode());
                return new ModelAndView(viewName, model);
            } else {
                userSession.setSmsSendRetries(userSession.getSmsSendRetries() + 1);
            }
        }

        String code = userSession.getSmsGeneratedCode();

        if (GenericValidator.isBlankOrNull(code)) {
            model.put("error", true);
            model.put("status", EnrollmentLiteStatus.MISSING_INFO.getCode());

        } else if (Utils.sendSms(userSession.getCustomerProfile().getCustomerPhone(), userSession, request, code)) {
            model.put("error", false);
            model.put("status", EnrollmentLiteStatus.SMS_SERVICE_REQUESTED.getCode());
        } else {
            model.put("error", true);
            model.put("status", EnrollmentLiteStatus.SMS_SERVICE_FAILED.getCode());
        }
        return new ModelAndView(viewName, model);
    }

    public ModelAndView validateSmsCode(HttpServletRequest request, HttpServletResponse response) {
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

        String viewName = "validateSmsCode";
        Map<String, Object> model = new HashMap<>();

        if (userSession == null) {
            model.put("error", true);
            model.put("status", EnrollmentLiteStatus.NO_SESSION_ERROR.getCode());
            return new ModelAndView(viewName, model);
        }

        String userEntryCode = request.getParameter("code");
        String sessionCode = userSession.getSmsGeneratedCode();

        if (GenericValidator.isBlankOrNull(userEntryCode) || GenericValidator.isBlankOrNull(sessionCode)) {
            model.put("error", true);
            model.put("status", EnrollmentLiteStatus.MISSING_INFO.getCode());
            return new ModelAndView(viewName, model);
        }
        model.put("error", false);
        if (userEntryCode.equals(sessionCode)) {
            model.put("status", EnrollmentLiteStatus.VALIDATION_SUCCESS.getCode());
        } else {

            userSession.setSmsGlobalRetries(userSession.getSmsGlobalRetries() + 1);

            if (userSession.getSmsGlobalRetries() >= 3) {
                model.put("status", EnrollmentLiteStatus.SMS_ERROR_LIMIT_REACHED.getCode());
            } else {
                model.put("status", EnrollmentLiteStatus.VALIDATION_FAILED.getCode());
            }
        }

        return new ModelAndView(viewName, model);
    }

    public ModelAndView isCustomerLiteEnrolled(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<>();
        String token = request.getParameter("token");

        if (GenericValidator.isBlankOrNull(token)) {
            model.put("status", EnrollmentLiteStatus.MISSING_INFO.getCode());
            return new ModelAndView("isCustomerLiteEnrolled", model);
        }

        CustomerServices customerServices = this.getCustomerServices();
        CustomerDeviceResponse phone = customerServices.getCustomerDeviceByDeviceMbopId(token);
        if (phone == null || phone.getDevice() == null) {
            model.put("status", EnrollmentLiteStatus.CUSTOMER_DEVICE_NOT_FOUND.getCode());
            return new ModelAndView("isCustomerLiteEnrolled", model);
        }

        CustomerProfileService customerProfileService = this.getCustomerServices().getCustomerProfileService();
        UserSession userSessionLite = Utils.getUserSessionLite(customerProfileService,
                phone.getDevice().getProfileId());
        CustomerProfile profile = userSessionLite.getCustomerProfile();
        if (profile == null || GenericValidator.isBlankOrNull(profile.getProfileId())) {
            model.put("status", EnrollmentLiteStatus.BACKEND_ERROR.getCode());
            return new ModelAndView("isCustomerLiteEnrolled", model);
        }

        userSessionLite.setCustomerService(customerServices);
        userSessionLite.getCustomerProfile().setCustomerDevice(phone.getDevice());
        WebUtils.setSessionAttribute(request, "userSession", userSessionLite);

        model.put("status", EnrollmentLiteStatus.CUSTOMER_LITE_FOUND.getCode());
        return new ModelAndView("isCustomerLiteEnrolled", model);
    }

    public ModelAndView addCustomerDevice(HttpServletRequest request, HttpServletResponse response) {
        String viewName = "addCustomerDevice";
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        Map<String, Object> model = new HashMap<>();

        String token = request.getParameter("token");
        String productType = request.getParameter("productType");
        String deviceNickname = request.getParameter("nickname");
        String unboundDevice = request.getParameter("unboundDevice");

        if (GenericValidator.isBlankOrNull(deviceNickname) || GenericValidator.isBlankOrNull(productType)
                || GenericValidator.isBlankOrNull(unboundDevice)) {
            model.put("status", EnrollmentLiteStatus.MISSING_INFO.getCode());
            logger.error("Error: Missing parameters in addCustomerDevice");
            return new ModelAndView("viewName", model);
        }

        ProductType product = ProductType.valueOf(productType);
        EnrollmentLiteStatus status = EnrollmentLiteStatus.BACKEND_ERROR;
        CustomerDevice device = new CustomerDevice();
        device.setProfileId(userSession.getCustomerProfile().getProfileId());
        device.setDeviceNickname(deviceNickname);
        device.setProductType(product);

        if ("true".equalsIgnoreCase(unboundDevice)) {
            return this.unboundDevice(userSession, model, request);
        } else {
            if (GenericValidator.isBlankOrNull(token)) {
                model.put("status", EnrollmentLiteStatus.MISSING_INFO.getCode());
                logger.error("Error: Token is null in addCustomerDevice");
                return new ModelAndView("viewName", model);
            }

            userSession.setFingerprintId(token);
            status = Utils.doesUserHasFingerprintRegistered(userSession, delayParameter).getStatus();

            if (EnrollmentLiteStatus.FINGERPRINT_NOT_REGISTERED.equals(status)) {
                device.setDeviceRegDate(new Date());
                device.setDeviceStatus(DeviceStatus.ACTIVE);
                device.setDeviceMbopId(token);
                Utils.addCustomerDevice(request, device, delayParameter);
                status = Utils.doesUserHasFingerprintRegistered(userSession, delayParameter).getStatus();
            }

            if (EnrollmentLiteStatus.FINGERPRINT_DISABLED.equals(status)) {
                CustomerDevice sessionDevice = Utils.getSessionCustomerDeviceByProductTypeAndToken(userSession, product,
                        token);
                if (sessionDevice == null) {
                    model.put("status", EnrollmentLiteStatus.BACKEND_ERROR.getCode());
                    logger.error("Error: getSessionCustomerDeviceByProductType is null for FingerprintDisabled");
                    return new ModelAndView("viewName", model);
                }

                sessionDevice.setDeviceStatus(DeviceStatus.ACTIVE);
                sessionDevice.setDeviceRegDate(new Date());
                Utils.updateCustomerDevice(request, sessionDevice);
                status = Utils.doesUserHasFingerprintRegistered(userSession, delayParameter).getStatus();
            }

            model.put("status", status.getCode());
            return new ModelAndView(viewName, model);
        }
    }

    private ModelAndView unboundDevice(UserSession userSession, Map<String, Object> model, HttpServletRequest request) {
        String viewName = "addCustomerDevice";

        String token = request.getParameter("token");
        String productType = request.getParameter("productType");
        String phoneNumber = request.getParameter("phoneNumber");
        ProductType product = ProductType.valueOf(productType);
        EnrollmentLiteStatus status = EnrollmentLiteStatus.BACKEND_ERROR;

        if (!GenericValidator.isBlankOrNull(phoneNumber) && ProductType.CASHDROP.equals(product)) {
            CustomerDevice sessionDevice = Utils.getSessionCustomerDeviceByProductType(userSession, product);

            if (sessionDevice == null) {
                model.put("status", EnrollmentLiteStatus.BACKEND_ERROR.getCode());
                logger.error("Error: getSessionCustomerDeviceByProductType is null");
                return new ModelAndView("viewName", model);
            }

            userSession.setMobileCashService(mobileCashService);
            Utils.deletePreviousEnrollmentData(userSession, phoneNumber);
            model.put("status", EnrollmentLiteStatus.DEVICE_UPDATE_SUCCESS.getCode());
            return new ModelAndView(viewName, model);
        }

        if (GenericValidator.isBlankOrNull(token)) {
            model.put("status", EnrollmentLiteStatus.MISSING_INFO.getCode());
            logger.error("Error: Token is null in addCustomerDevice");
            return new ModelAndView("viewName", model);
        }

        if (ProductType.FINGERPRINT.equals(product)) {
            CustomerDevice sessionDevice = Utils.getSessionCustomerDeviceByProductTypeAndToken(userSession, product,
                    token);

            if (sessionDevice == null) {
                model.put("status", EnrollmentLiteStatus.BACKEND_ERROR.getCode());
                logger.error("Error: getSessionCustomerDeviceByProductTypeAndToken is null");
                return new ModelAndView("viewName", model);
            }

            sessionDevice.setDeviceStatus(DeviceStatus.DISABLED);
            sessionDevice.setDeviceRegDate(new Date());
            status = Utils.updateCustomerDevice(request, sessionDevice);

            userSession.setFingerprintId(token);
            status = Utils.doesUserHasFingerprintRegistered(userSession, delayParameter).getStatus();
        }

        model.put("status", status.getCode());
        return new ModelAndView(viewName, model);
    }

    public ModelAndView getDecisionsbyCheck(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, Object> model = new HashMap<>();
        String checkId = request.getParameter("checkId");
        StringBuilder messg = new StringBuilder();
        String varConc = "#";
        int i = 1;
        List<IncomingChecksCustDecision> incoCheckDecisions = customerServices.getIncomingChecksDecisions(checkId);
        for (Iterator<IncomingChecksCustDecision> iteratorDecisions = incoCheckDecisions.iterator(); iteratorDecisions.hasNext(); ) {
            IncomingChecksCustDecision decisions = iteratorDecisions.next();
            if (incoCheckDecisions.size() == i)
                varConc = "";
            String newDate = new SimpleDateFormat("MM/d/yyyy h:mm a").format(decisions.getDecisionDate());
            messg.append("dateD~" + newDate + "|dispocision~" + decisions.getDecision() + "|username~" + decisions.getDecisionUsername() + varConc);
            i++;
        }

        model.put("text", messg.toString());
        return new ModelAndView("text", model);
    }

    public ModelAndView forgotpasswordsec(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	Map<String, Object> model = new HashMap<String, Object>();
    		model.put("mustChangeUsername", WebUtils.getSessionAttribute(request, "mustChangeUsername"));
        return new ModelAndView("forgotpasswordsec",model);
    }

    /**
     *
     * Show the detail of a transaction
     *
     * Expected GET parameters are accountId and traceId.
     *
     * accountId: the frontEndAccountId
     * traceId: use by identification of the transaction
     *
     * @param request
     * @param response
     * @return modelAndView
     * @throws Exception
     */
    public ModelAndView transAddScreen(HttpServletRequest request, HttpServletResponse response) throws Exception {
        final Map<String, Object> model = new HashMap<>();
        final UserSession userSession = KiuwanUtils.cast(UserSession.class, WebUtils.getSessionAttribute(request, "userSession"));
        final BankingSession bankingSession = userSession.getBankingSession();
        final CustomerProfile customerProfile = userSession.getCustomerProfile();
        final String accountId = request.getParameter("account");
        final String traceId = request.getParameter("trace");
        String type = "";
        String payee = "";
        String location = "";
        String regex = "ON\\s[0-9]{2}/[0-9]{2}/[0-9]{2}\\s";
        String regex2 = "ON\\s[0-9]{2}/[0-9]{2}/[0-9]{2}";

        final CustomerServices customerService = userSession.getCustomerService();
        if (StringUtils.isNotBlank(accountId) && StringUtils.isNotBlank(traceId)) {
            TVFrontendAccount account = Utils.getAccountById(accountId, userSession.getAccounts());
            Optional<AbstractTransaction> transOptional = null;
            boolean transactionFound = false;
            if (!Objects.isNull(account)) {



                TransactionRequest req = new TransactionRequest(Utils.generateCustomerInteractionEvent(request),
                        null, customerProfile, bankingSession, account, TRANSACTIONS_CCA_OO_IDA_PAGE_SIZE, 1, false,//yerme
                        null, null, null);
                TransactionsResponse transactions = Utils.getTransactionsCCAOrIDA(customerService, req, account);
                transOptional = Utils.getTransactionByTraceId(transactions.getTransactions(), traceId);
                int pagesAvailable = transactions.getPagesAvailable();
                if (transOptional.isPresent()) {
                    transactionFound = true;
                } else {
                    for (int i = 2; i <=  pagesAvailable; i++) {
                        req.setPageNumber(i);
                        transactions = Utils.getTransactionsCCAOrIDA(customerService, req, account);
                        transOptional = Utils.getTransactionByTraceId(transactions.getTransactions(), traceId);
                        if (transOptional.isPresent()) {
                            transactionFound = true;
                            break;
                        }
                    }
                }


                if (transactionFound) {

                    String descTransaction = transOptional.get().getDescription();

                    if (descTransaction.contains("PURCHASE") && descTransaction.contains("ON")) {

                        type = "PURCHASE";
                        String payeeDescription = descTransaction.replaceAll("PURCHASE\\s", "").trim();

                        String[] splitTransactionDesc = payeeDescription.split(regex);

                        if (splitTransactionDesc.length >= 2) {
                            payee = splitTransactionDesc[0];
                            location = splitTransactionDesc[1];
                        }else {
                            payee = splitTransactionDesc[0].replaceAll(regex2, "");
                        }


                    }else if(descTransaction.contains("ATH WITHDRAWAL") && descTransaction.contains("ON")){

                        type = "ATH WITHDRAWAL";
                        String payeeDescription = descTransaction.replaceAll("ATH WITHDRAWAL\\s", "").trim();

                        String[] splitTransactionDesc = payeeDescription.split(regex);

                        if (splitTransactionDesc.length >= 2) {
                            payee = splitTransactionDesc[0];
                            location = splitTransactionDesc[1];
                        }else {
                            payee = splitTransactionDesc[0].replaceAll(regex2, "");
                        }

                    } else if (descTransaction.contains("EFT")) {
                        String space = " ";
                        if (descTransaction.contains("PMT")) {
                            type = "EFT_PAYMENT";
                            String result =  descTransaction.replace("EFT PMT", "").trim();
                            String[] values =  result.split(space);
                            payee = "";
                            for(int i =0; i<values.length-1; i++) {
                                payee = payee + values[i] + space;
                            }
                            payee = payee.trim();
                            location = values[values.length-1];
                            model.put("reference", location);
                        }else if (descTransaction.contains("DEPOSIT")) {
                            type = "EFT_DEPOSIT";
                            String result =  descTransaction.replace("EFT DEPOSIT", "").trim();
                            String[] values=  result.split(space);
                            payee = "";
                            for(int i =0; i<values.length-1; i++) {
                                payee = payee + values[i] + space;
                            }
                            payee = payee.trim();
                            location = values[values.length-1];
                            model.put("reference", location);
                        }
                    }

                    model.put("payee", payee);
                    model.put("location", location);
                    model.put("type", type);
                    model.put("transaction", transOptional.get());
                }
            }

        }

        deviceType(model, userSession);

        return new ModelAndView("transAddScreen", model);
    }

    private void deviceType(Map<String, Object> model, UserSession userSession) {
        if(userSession.getUserAgentType() == UserAgentType.IPHONE  || userSession.getUserAgentType() == UserAgentType.ANDROID || userSession.getUserAgentType() == UserAgentType.IPAD){
            model.put("deviceType", userSession.getUserAgentType());
        }
    }

    /**
     * Regain Access email block view.
     * 
     * @param request
     * @param response
     * @return
     */
    public ModelAndView regainaccessEmailBlocked(HttpServletRequest request, HttpServletResponse response) {
    	final String regainBlockedEmail = "regainaccessEmailBlocked";//
    	
        return new ModelAndView(regainBlockedEmail);
    }

    public ModelAndView doubleprofileValidationScreen(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("doubleprofileValidationScreen");
    }
    
    public ModelAndView bsmartEnrollAlert(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("bsmartEnrollAlert");
    }

    public ModelAndView oobrecomendationsec(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	WebUtils.setSessionAttribute(request, Utils.SHOW_OOB, Boolean.TRUE);
    	return new ModelAndView("forward:personalinformation");
    }

    /**
     * This method redirect to desktop version of MiBanco & open a privacy settings toggle in my information.
     * @author ET58344, Evertec Inc.
     * @since 03-15-2021
     * @implNote Modified 03-30-2021
     * @param request http request
     * @param response http response
     * @return ModelAndView result
     */
    public ModelAndView gdprPrivacySettings(HttpServletRequest request, HttpServletResponse response) {
        CookieGenerator cg; //cookie generator
        cg = new CookieGenerator(); //cookie generator
        cg.setCookieName("client-version");
        cg.setCookieMaxAge(-1);
        cg.addCookie(response, "classic");

        WebUtils.setSessionAttribute(request, Utils.SHOW_GDPR, Boolean.TRUE); //set show gdpr flag
        //forward string
        final String personalInformationForwardStr = "redirect:personalinformation";
        return new ModelAndView(personalInformationForwardStr);
    }

    public String getRdcBid() {
        return rdcBid;
    }

    public void setRdcBid(String rdcBid) {
        this.rdcBid = rdcBid;
    }

    public String[] getEzStatementInformativaProductIds() {
        return ezStatementInformativaProductIds;
    }

    public void setEzStatementInformativaProductIds(String[] ezStatementInformativaProductIds) {
        this.ezStatementInformativaProductIds = ezStatementInformativaProductIds;
    }

    public String[] getEzStatementMortgageProductIds() {
        return ezStatementMortgageProductIds;
    }

    public void setEzStatementMortgageProductIds(String[] ezStatementMortgageProductIds) {
        this.ezStatementMortgageProductIds = ezStatementMortgageProductIds;
    }

    public String getEzStatementInformativaIRAQuarterlyProductId() {
        return ezStatementInformativaIRAQuarterlyProductId;
    }

    public void setEzStatementInformativaIRAQuarterlyProductId(String ezStatementInformativaIRAQuarterlyProductId) {
        this.ezStatementInformativaIRAQuarterlyProductId = ezStatementInformativaIRAQuarterlyProductId;
    }

    public String getEzStatementInformativaIRAYearlyProductId() {
        return ezStatementInformativaIRAYearlyProductId;
    }

    public void setEzStatementInformativaIRAYearlyProductId(String ezStatementInformativaIRAYearlyProductId) {
        this.ezStatementInformativaIRAYearlyProductId = ezStatementInformativaIRAYearlyProductId;
    }

    public String getEzStatementInformativaCAEQuarterlyProductId() {
        return ezStatementInformativaCAEQuarterlyProductId;
    }

    public void setEzStatementInformativaCAEQuarterlyProductId(String ezStatementInformativaCAEQuarterlyProductId) {
        this.ezStatementInformativaCAEQuarterlyProductId = ezStatementInformativaCAEQuarterlyProductId;
    }

    public String getEzStatementInformativaCAEYearlyProductId() {
        return ezStatementInformativaCAEYearlyProductId;
    }

    public void setEzStatementInformativaCAEYearlyProductId(String ezStatementInformativaCAEYearlyProductId) {
        this.ezStatementInformativaCAEYearlyProductId = ezStatementInformativaCAEYearlyProductId;
    }

    public boolean isPfmAutomaticEntitlementCreation() {
        return pfmAutomaticEntitlementCreation;
    }

    public void setPfmAutomaticEntitlementCreation(boolean pfmAutomaticEntitlementCreation) {
        this.pfmAutomaticEntitlementCreation = pfmAutomaticEntitlementCreation;
    }

    public String getAlertsAccountTypes() {
        return alertsAccountTypes;
    }

    public void setAlertsAccountTypes(String alertsAccountTypes) {
        this.alertsAccountTypes = alertsAccountTypes;
    }

    public String getMobilePhoneProviders() {
        return mobilePhoneProviders;
    }

    public void setMobilePhoneProviders(String mobilePhoneProviders) {
        this.mobilePhoneProviders = mobilePhoneProviders;
    }

    public CustomerServices getCustomerServices() {
        return customerServices;
    }

    public void setCustomerServices(CustomerServices customerService) {
        this.customerServices = customerService;
    }

    public MobileCashServices getMobileCashService() {
        return mobileCashService;
    }

    public void setMobileCashService(MobileCashServices mobileCashService) {
        this.mobileCashService = mobileCashService;
    }

    public String getDelayParameter() {
        return delayParameter;
    }

    public void setDelayParameter(String delayParameter) {
        this.delayParameter = delayParameter;
    }

    public String[] getProductId() {
        return productId;
    }

    public void setProductId(String[] productId) {
        this.productId = productId;
    }

    public String[] getRelationshipType() {
        return relationshipType;
    }

    public void setRelationshipType(String[] relationshipType) {
        this.relationshipType = relationshipType;
    }

    public String getPingfederateSsoUrl() {
        return pingfederateSsoUrl;
    }

    public void setPingfederateSsoUrl(String pingfederateSsoUrl) {
        this.pingfederateSsoUrl = pingfederateSsoUrl;
    }

    public String getPingfederateSsoEndpoint() {
        return pingfederateSsoEndpoint;
    }

    public void setPingfederateSsoEndpoint(String pingfederateSsoEndpoint) {
        this.pingfederateSsoEndpoint = pingfederateSsoEndpoint;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public void setPcbCCANotEbillAllowed(String pcbCCANotEbillAllowed) {
        this.pcbCCANotEbillAllowed = pcbCCANotEbillAllowed;
    }

	public String getRemoteDepositAmountCharge() {
		return remoteDepositAmountCharge;
	}

	public void setRemoteDepositAmountCharge(String remoteDepositAmountCharge) {
		this.remoteDepositAmountCharge = remoteDepositAmountCharge;
	}
}
