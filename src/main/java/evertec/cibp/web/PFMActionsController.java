package evertec.cibp.web;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import edu.emory.mathcs.backport.java.util.Collections;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.models.base.RunningBalanceTransaction;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.PFMEntitlement;
import evertec.cibp.core.models.pfm.PFMAccountWrapper;
import evertec.cibp.core.models.pfm.PFMCategoryWrapper;
import evertec.cibp.core.models.pfm.PFMChartCategoryItemWrapper;
import evertec.cibp.core.models.pfm.PFMChartCategoryRecordWrapper;
import evertec.cibp.core.models.pfm.PFMChartNetIncomeItemWrapper;
import evertec.cibp.core.models.pfm.PFMCustomRuleWrapper;
import evertec.cibp.core.models.pfm.PFMSortOrderWrapper;
import evertec.cibp.core.models.pfm.PFMSubcategoryWrapper;
import evertec.cibp.core.models.pfm.PFMTagWrapper;
import evertec.cibp.core.models.pfm.PFMTransactionRecordWrapper;
import evertec.cibp.core.models.pfm.PFMTransactionWrapper;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.TransactionRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.resolver.types.AjaxError;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.PFMUtil;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.view.PfmCsvFileView;

public class PFMActionsController extends MultiActionController implements AjaxError {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	protected CustomerServices customerService;

	private final int INCOME_CATEGORY_ID = 4;

	protected boolean pfmAutomaticEntitlementCreation;

	private boolean getPfmUserId(UserSession userSession) {

		if (userSession == null)
			return false;

		// First up, set the language
		userSession.setPfmSpanish(!"EN".equalsIgnoreCase(userSession.getLanguage()));

		if (userSession.getPfmId() == null) {

			PFMUtil pfmUtil = new PFMUtil();

			// We do not have the entitlement set up yet
			if (!userSession.getCustomerProfile().hasEntitlement(EntitlementType.PFM)) {
				pfmUtil.createPfmEntitlement(userSession.getCustomerService(), userSession.getCustomerProfile());
			}

			CustomerEntitlement entitlementByType = userSession.getCustomerProfile().getEntitlementByType(
					EntitlementType.PFM);

			PFMEntitlement pfmentitlement = new PFMEntitlement(entitlementByType);

			// If the entitlement exists but there is no value, then we need to
			// create the PFM user
			String pfmId = entitlementByType.getString("PFM_USER_ID");

			if (pfmId == null || pfmId.equals("")) {
				pfmId = pfmUtil.createPfmUser(customerService, userSession.getCustomerProfile());
				pfmentitlement.setPFM_USER_ID(pfmId);
				CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(pfmentitlement.getCustomerEntitlement()));
			}

			userSession.setPfmId(entitlementByType.getString("PFM_USER_ID"));
		}

		if (GenericValidator.isBlankOrNull(userSession.getPfmId()))
			return false;
		else
			return true;
	}

	private boolean getPfmAccounts(UserSession userSession) {

		if (userSession.getPfmAccounts() == null) {
			List<PFMAccountWrapper> pfmAccounts = customerService.getPfmAccounts(userSession.getPfmId(),
					userSession.getAccounts());

			// Set up the indices
			for (int i = 0; i < pfmAccounts.size(); i++)
				pfmAccounts.get(i).setIndex(i);

			userSession.setPfmAccounts(pfmAccounts);
		}

		return true;
	}

	private boolean getPfmCategories(UserSession userSession) {

		if (userSession.getPfmCategories() == null) {
			List<PFMCategoryWrapper> pfmCategories = customerService.getPfmCategories(userSession.getPfmId());

			// Language (initially sorted in Spanish)
			if (!userSession.isPfmSpanish()) {
				pfmCategories = sortCategoryList(pfmCategories, "en");
				userSession.setPfmCategoriesSortedSpanish(false);
			}

			userSession.setPfmCategories(pfmCategories);
		} else {

			// Should we re-sort?
			userSession.setPfmSpanish(!"EN".equalsIgnoreCase(userSession.getLanguage()));

			if (userSession.isPfmSpanish() && !userSession.isPfmCategoriesSortedSpanish()) {
				userSession.setPfmCategoriesSortedSpanish(true);
				userSession.setPfmCategories(sortCategoryList(userSession.getPfmCategories(), "es"));
			} else if (!userSession.isPfmSpanish() && userSession.isPfmCategoriesSortedSpanish()) {
				userSession.setPfmCategoriesSortedSpanish(false);
				userSession.setPfmCategories(sortCategoryList(userSession.getPfmCategories(), "en"));
			}
		}

		return true;
	}

	private boolean getPfmTags(UserSession userSession) {

		if (userSession.getPfmTags() == null) {
			List<PFMTagWrapper> pfmTags = customerService.getPfmTags(userSession.getPfmId());

			// Language (initially sorted in Spanish)
			if (!userSession.isPfmSpanish()) {
				pfmTags = sortTagList(pfmTags, "en");
				userSession.setPfmTagsSortedSpanish(false);
			}

			userSession.setPfmTags(pfmTags);
		} else {

			// Should we re-sort?
			userSession.setPfmSpanish(!"EN".equalsIgnoreCase(userSession.getLanguage()));

			if (userSession.isPfmSpanish() && !userSession.isPfmTagsSortedSpanish()) {
				userSession.setPfmTagsSortedSpanish(true);
				userSession.setPfmTags(sortTagList(userSession.getPfmTags(), "es"));
			} else if (!userSession.isPfmSpanish() && userSession.isPfmTagsSortedSpanish()) {
				userSession.setPfmTagsSortedSpanish(false);
				userSession.setPfmTags(sortTagList(userSession.getPfmTags(), "en"));
			}
		}

		return true;
	}

	private boolean getPfmCustomRules(UserSession userSession) {
		if (userSession.getPfmCustomRules() == null) {
			List<PFMCustomRuleWrapper> pfmCustomRules = customerService.getPfmCustomRules(userSession.getPfmId());
			int index = 0;

			// Two last items - set the account nickname for easier displaying
			for (Iterator<PFMCustomRuleWrapper> iterator = pfmCustomRules.iterator(); iterator.hasNext();) {
				PFMCustomRuleWrapper thisRule = (PFMCustomRuleWrapper) iterator.next();
				for (Iterator<PFMAccountWrapper> iterator2 = userSession.getPfmAccounts().iterator(); iterator2
						.hasNext();) {
					PFMAccountWrapper thisAccount = (PFMAccountWrapper) iterator2.next();
					if (thisRule.getAccountNumber().length() == 9 && thisAccount.getAccountId().endsWith("DEP")
							&& thisAccount.getAccountId().substring(0, 9).equals(thisRule.getAccountNumber())) {
						pfmCustomRules.get(index).setAccountNickname(thisAccount.getNickname());
						break;
					} else if (thisRule.getAccountNumber().length() == 16
							&& thisAccount.getAccountId().endsWith("CARD")
							&& thisAccount.getAccountId().substring(0, 16).equals(thisRule.getAccountNumber())) {
						pfmCustomRules.get(index).setAccountNickname(thisAccount.getNickname());
						break;
					}
				}
				index++;
			}

			// ...and set the subcategory display names for easier displaying
			index = 0;
			for (Iterator<PFMCustomRuleWrapper> iterator = pfmCustomRules.iterator(); iterator.hasNext();) {
				PFMCustomRuleWrapper thisRule = (PFMCustomRuleWrapper) iterator.next();
				for (Iterator<PFMCategoryWrapper> iterator2 = userSession.getPfmCategories().iterator(); iterator2
						.hasNext();) {
					boolean foundCurrentSubcategories = false;
					boolean foundOriginalSubcategories = false;
					PFMCategoryWrapper thisCategory = (PFMCategoryWrapper) iterator2.next();
					if (thisCategory.getSubcategories() != null && thisCategory.getSubcategories().size() > 0) {
						for (Iterator<PFMSubcategoryWrapper> iterator3 = thisCategory.getSubcategories().iterator(); iterator3
								.hasNext();) {
							PFMSubcategoryWrapper thisSubcategory = (PFMSubcategoryWrapper) iterator3.next();
							if (!foundCurrentSubcategories
									&& thisRule.getSubcategoryId() == thisSubcategory.getCategoryId()) {
								pfmCustomRules.get(index).setSubcategory_description_en(
										thisSubcategory.getDescription_en());
								pfmCustomRules.get(index).setSubcategory_description_es(
										thisSubcategory.getDescription_es());
								foundCurrentSubcategories = true;
							}
							if (!foundOriginalSubcategories && thisRule.getOriginalSubcategoryId() > 0
									&& thisRule.getOriginalSubcategoryId() == thisSubcategory.getCategoryId()) {
								pfmCustomRules.get(index).setOriginalSubcategory_description_en(
										thisSubcategory.getDescription_en());
								pfmCustomRules.get(index).setOriginalSubcategory_description_es(
										thisSubcategory.getDescription_es());
								foundOriginalSubcategories = true;
							}
							if (foundCurrentSubcategories && foundOriginalSubcategories)
								break;
						}
					}
				}
				index++;
			}

			userSession.setPfmCustomRules(sortCustomRuleList(pfmCustomRules));
		}

		return true;
	}

	private boolean getChartInformation(UserSession userSession, String startDate, String endDate, String infoType,
			String presentationType, boolean firstTimeLoading, String account) {

		// Should we filter by one particular account? (transaction view)
		List<PFMAccountWrapper> oneAccount = new LinkedList<PFMAccountWrapper>();
		if (account != null && !account.equals("")) {
			// Loop through all the accounts and send only this one
			for (Iterator<PFMAccountWrapper> iterator = userSession.getPfmAccounts().iterator(); iterator.hasNext();) {
				PFMAccountWrapper pfmAccount = (PFMAccountWrapper) iterator.next();
				if ((pfmAccount.getAccountId().endsWith("DEP") && account.length() == 9 && pfmAccount.getAccountId()
						.substring(0, 9).equals(account))
						|| (account.length() == 15 && pfmAccount.getAccountId().substring(1, 16).equals(account))
						|| (account.length() == 16 && pfmAccount.getAccountId().substring(0, 16).equals(account))) {
					pfmAccount.setIncludeFlag(true);
					oneAccount.add(pfmAccount);
					break;
				}
			}
		}

		// If this is the "first time" loading AND we already have the chart
		// information in session, use that
		if (firstTimeLoading) {

			// Make sure to clear the other two charts, though
			userSession.setPfmChartInfo(null);
			userSession.setPfmPreviousChartInfo(null);

			if (account != null && !account.equals("")) {
				PFMChartCategoryRecordWrapper recordWrapper = customerService.getPfmChartInfo(userSession.getPfmId(),
						oneAccount, userSession.getPfmCategories(), userSession.getPfmTags(), startDate, endDate,
						infoType, presentationType);
				userSession.setPfmOriginalChartInfo(recordWrapper);
			} else {
				PFMChartCategoryRecordWrapper recordWrapper = customerService.getPfmChartInfo(userSession.getPfmId(),
						userSession.getPfmAccounts(), userSession.getPfmCategories(), userSession.getPfmTags(),
						startDate, endDate, infoType, presentationType);
				userSession.setPfmOriginalChartInfo(recordWrapper);
			}
		}
		// If not, verify the previous chart record and if it matches the
		// requested one, use that one
		else {
			if (presentationType.equals("netincome")) {
				if (account != null && !account.equals("")) {
					List<PFMChartNetIncomeItemWrapper> pfmChartInfo = customerService.getPfmNetIncomeChartInfo(
							userSession.getPfmId(), oneAccount, startDate, endDate, userSession.getPfmCategories());
					userSession.setPfmChartNetIncomeInfo(pfmChartInfo);
				} else {
					List<PFMChartNetIncomeItemWrapper> pfmChartInfo = customerService.getPfmNetIncomeChartInfo(
							userSession.getPfmId(), userSession.getPfmAccounts(), startDate, endDate,
							userSession.getPfmCategories());
					userSession.setPfmChartNetIncomeInfo(pfmChartInfo);
				}

				// Clear the other previous charts since we need to reload any
				// data
				userSession.setPfmChartInfo(null);
				userSession.setPfmPreviousChartInfo(null);
			} else {
				if (userSession.getPfmChartInfo() == null)
					userSession.setPfmPreviousChartInfo(userSession.getPfmOriginalChartInfo());
				else
					userSession.setPfmPreviousChartInfo(userSession.getPfmChartInfo());

				if (account != null && !account.equals("")) {
					PFMChartCategoryRecordWrapper recordWrapper = customerService.getPfmChartInfo(
							userSession.getPfmId(), oneAccount, userSession.getPfmCategories(),
							userSession.getPfmTags(), startDate, endDate, infoType, presentationType);
					userSession.setPfmChartInfo(recordWrapper);
				} else {
					PFMChartCategoryRecordWrapper recordWrapper = customerService.getPfmChartInfo(
							userSession.getPfmId(), userSession.getPfmAccounts(), userSession.getPfmCategories(),
							userSession.getPfmTags(), startDate, endDate, infoType, presentationType);
					userSession.setPfmChartInfo(recordWrapper);
				}
			}
		}

		return true;
	}

	public ModelAndView pfmLogin(HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		if (!getPfmUserId(userSession)) {
			model.put("hidePfm", "true");
			return new ModelAndView("pfmLogin", model);
		}

		model.put("hidePfm", "false");
		return new ModelAndView("pfmLogin", model);
	}

	public ModelAndView pfmAccounts(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Log the person into PFM if not logged in
		if (!getPfmUserId(userSession)) {
			model.put("hidePfm", "true");
			return new ModelAndView("pfmAccounts", model);
		}

		// Send the request for accounts
		getPfmAccounts(userSession);
		model.put("accounts", userSession.getPfmAccounts());
		return new ModelAndView("pfmAccounts", model);
	}

	public ModelAndView pfmCategories(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Log the person into PFM if not logged in
		if (!getPfmUserId(userSession)) {
			model.put("hidePfm", "true");
			return new ModelAndView("pfmCategories", model);
		}

		// Send the request for categories
		getPfmCategories(userSession);
		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");
		model.put("categories", userSession.getPfmCategories());
		return new ModelAndView("pfmCategories", model);
	}

	public ModelAndView pfmCreditSubcategories(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Log the person into PFM if not logged in
		if (!getPfmUserId(userSession)) {
			model.put("hidePfm", "true");
			return new ModelAndView("pfmCategories", model);
		}

		// Send the request for categories
		getPfmCategories(userSession);
		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");
		for (Iterator<PFMCategoryWrapper> iterator = userSession.getPfmCategories().iterator(); iterator.hasNext();) {
			PFMCategoryWrapper wrapper = (PFMCategoryWrapper) iterator.next();
			if (wrapper.getCategoryId() == INCOME_CATEGORY_ID) {
				model.put("category", wrapper);
				break;
			}
		}

		return new ModelAndView("pfmCreditSubcategories", model);
	}

	public ModelAndView pfmTags(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Log the person into PFM if not logged in
		if (!getPfmUserId(userSession)) {
			model.put("hidePfm", "true");
			return new ModelAndView("pfmTags", model);
		}

		// Send the request for tags
		getPfmTags(userSession);
		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");
		model.put("tags", userSession.getPfmTags());
		return new ModelAndView("pfmTags", model);
	}

	public ModelAndView pfmSummaryAndChart(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the values for this chart
		String startDate = ServletRequestUtils.getStringParameter(request, "startDate");
		String endDate = ServletRequestUtils.getStringParameter(request, "endDate");
		String infoType = ServletRequestUtils.getStringParameter(request, "infoType");
		String presentationType = ServletRequestUtils.getStringParameter(request, "presentationType");
		boolean firstTimeLoading = ServletRequestUtils.getBooleanParameter(request, "firstTimeLoading");

		// Are we filtering by this one account? (transaction view)
		String frontendid = ServletRequestUtils.getStringParameter(request, "frontendid");
		String account = "";
		if (frontendid != null && !"".equals(frontendid)) {
			TVFrontendAccount tvaccount = Utils.getAccountById(frontendid, userSession.getAccounts());
			if (tvaccount != null)
				account = tvaccount.getAccountNumber();
		}

		// Format the dates
		startDate = startDate.substring(3) + "-" + startDate.substring(0, 2) + "-01";
		endDate = endDate.substring(3) + "-" + endDate.substring(0, 2) + "-01";

		// Parse the data in the user session's chart information linked list
		String graphData = "";
		double total = 0.0;

		// Send the request for accounts, categories and tags
		getPfmUserId(userSession);
		getPfmAccounts(userSession);
		getPfmCategories(userSession);
		getPfmTags(userSession);

		// Send the request to get the initial chart information
		getChartInformation(userSession, startDate, endDate, infoType, presentationType, firstTimeLoading, account);

		// Net income report
		if (presentationType.equals("netincome")) {
			if (userSession.getPfmChartNetIncomeInfo() != null) {
				for (Iterator<PFMChartNetIncomeItemWrapper> iterator = userSession.getPfmChartNetIncomeInfo()
						.iterator(); iterator.hasNext();) {
					PFMChartNetIncomeItemWrapper item = (PFMChartNetIncomeItemWrapper) iterator.next();
					graphData += ("EN".equalsIgnoreCase(userSession.getLanguage()) ? item.getDescription_en() : item
							.getDescription_es()) + "|" + item.getIncomeAmount() + "|" + item.getExpenseAmount() + "|";
					total += (item.getIncomeAmount().doubleValue() + item.getExpenseAmount().doubleValue());
				}
			}
		} else {
			if (firstTimeLoading) {
				if (userSession.getPfmOriginalChartInfo() != null
						&& userSession.getPfmOriginalChartInfo().getPfmTransactions() != null) {
					for (Iterator<PFMChartCategoryItemWrapper> iterator = userSession.getPfmOriginalChartInfo()
							.getPfmTransactions().iterator(); iterator.hasNext();) {
						PFMChartCategoryItemWrapper item = (PFMChartCategoryItemWrapper) iterator.next();
						graphData += ("EN".equalsIgnoreCase(userSession.getLanguage()) ? item.getDescription_en()
								: item.getDescription_es()) + "|" + item.getAmount() + "|";
						total += Double.parseDouble(item.getAmount());
					}
				}
			} else {
				for (Iterator<PFMChartCategoryItemWrapper> iterator = userSession.getPfmChartInfo()
						.getPfmTransactions().iterator(); iterator.hasNext();) {
					PFMChartCategoryItemWrapper item = (PFMChartCategoryItemWrapper) iterator.next();
					graphData += ("EN".equalsIgnoreCase(userSession.getLanguage()) ? item.getDescription_en() : item
							.getDescription_es()) + "|" + item.getAmount() + "|";
					total += Double.parseDouble(item.getAmount());
				}
			}
		}

		// Add the total
		graphData += "Total|" + Double.toString(total);

		model.put("graphData", graphData);
		return new ModelAndView("pfmSummaryAndChart", model);
	}

	public ModelAndView pfmSummaryAndChartDrilldown(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the values for this chart
		String monthsChanged = ServletRequestUtils.getStringParameter(request, "monthsChanged");
		String startDate = ServletRequestUtils.getStringParameter(request, "startDate");
		String endDate = ServletRequestUtils.getStringParameter(request, "endDate");
		String infoType = ServletRequestUtils.getStringParameter(request, "infoType");
		String presentationType = ServletRequestUtils.getStringParameter(request, "presentationType");
		int index = ServletRequestUtils.getIntParameter(request, "index");

		// Are we filtering by this one account? (transaction view)
		String frontendid = ServletRequestUtils.getStringParameter(request, "frontendid");
		String account = "";
		if (frontendid != null && !"".equals(frontendid)) {
			TVFrontendAccount tvaccount = Utils.getAccountById(frontendid, userSession.getAccounts());
			if (tvaccount != null)
				account = tvaccount.getAccountNumber();
		}

		// Format the dates
		startDate = startDate.substring(3) + "-" + startDate.substring(0, 2) + "-01";
		endDate = endDate.substring(3) + "-" + endDate.substring(0, 2) + "-01";

		// Get the category id
		int categoryId = 0;
		if (monthsChanged.equals("true")) {
			categoryId = userSession.getPfmCategoryId();
		} else {
			if (userSession.getPfmChartInfo() != null) {
				categoryId = userSession.getPfmChartInfo().getPfmTransactions().get(index).getCategoryId();
			} else {
				categoryId = userSession.getPfmOriginalChartInfo().getPfmTransactions().get(index).getCategoryId();
			}
			userSession.setPfmCategoryId(categoryId);
		}

		// Call PFM to obtain the subcategory information for this category
		if (userSession.getPfmChartInfo() == null)
			userSession.setPfmPreviousChartInfo(userSession.getPfmOriginalChartInfo());
		else
			userSession.setPfmPreviousChartInfo(userSession.getPfmChartInfo());

		// Should we filter by one particular account? (transaction view)
		List<PFMAccountWrapper> oneAccount = new LinkedList<PFMAccountWrapper>();
		if (account != null && !account.equals("")) {
			// Loop through all the accounts and send only this one
			for (Iterator<PFMAccountWrapper> iterator = userSession.getPfmAccounts().iterator(); iterator.hasNext();) {
				PFMAccountWrapper pfmAccount = (PFMAccountWrapper) iterator.next();
				if ((pfmAccount.getAccountId().endsWith("DEP") && account.length() == 9 && pfmAccount.getAccountId()
						.substring(0, 9).equals(account))
						|| (account.length() == 15 && pfmAccount.getAccountId().substring(1, 16).equals(account))
						|| (account.length() == 16 && pfmAccount.getAccountId().substring(0, 16).equals(account))) {
					pfmAccount.setIncludeFlag(true);
					oneAccount.add(pfmAccount);
					break;
				}
			}
		}

		if (account != null && !account.equals("")) {
			PFMChartCategoryRecordWrapper recordWrapper = customerService.getPfmChartInfoByCategory(
					userSession.getPfmId(), oneAccount, categoryId, startDate, endDate, infoType, presentationType);
			userSession.setPfmChartInfo(recordWrapper);
		} else {
			PFMChartCategoryRecordWrapper recordWrapper = customerService.getPfmChartInfoByCategory(
					userSession.getPfmId(), userSession.getPfmAccounts(), categoryId, startDate, endDate, infoType,
					presentationType);
			userSession.setPfmChartInfo(recordWrapper);
		}

		// Parse the data in the user session's chart information linked list
		String graphData = "";
		double total = 0.0;

		for (Iterator<PFMChartCategoryItemWrapper> iterator = userSession.getPfmChartInfo().getPfmTransactions()
				.iterator(); iterator.hasNext();) {
			PFMChartCategoryItemWrapper item = (PFMChartCategoryItemWrapper) iterator.next();
			graphData += ("EN".equalsIgnoreCase(userSession.getLanguage()) ? item.getDescription_en() : item
					.getDescription_es()) + "|" + item.getAmount() + "|";
			total += Double.parseDouble(item.getAmount());
		}

		// Add the total
		graphData += "Total|" + Double.toString(total);

		model.put("graphData", graphData);
		return new ModelAndView("pfmSummaryAndChartDrilldown", model);
	}

	public ModelAndView pfmTransactions(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the values of the month(s), account(s) and filter(s)
		String monthsChanged = ServletRequestUtils.getStringParameter(request, "monthsChanged");
		String startDate = ServletRequestUtils.getStringParameter(request, "startDate");
		String endDate = ServletRequestUtils.getStringParameter(request, "endDate");
		int index = ServletRequestUtils.getIntParameter(request, "index");

		// Are we filtering by this one account? (transaction view)
		String frontendid = ServletRequestUtils.getStringParameter(request, "frontendid");
		String account = "";
		if (frontendid != null && !"".equals(frontendid)) {
			TVFrontendAccount tvaccount = Utils.getAccountById(frontendid, userSession.getAccounts());
			if (tvaccount != null)
				account = tvaccount.getAccountNumber();
		}

		// Find the last day of the end month
		Calendar cal = Calendar.getInstance();
		cal.set(Integer.parseInt(endDate.substring(3)), Integer.parseInt(endDate.substring(0, 2)) - 1, 1);
		int maxday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		// Format the dates
		startDate = startDate.substring(3) + "-" + startDate.substring(0, 2) + "-01";
		endDate = endDate.substring(3) + "-" + endDate.substring(0, 2) + "-" + String.valueOf(maxday);

		// Get the sort order
		String sortOrderString = ServletRequestUtils.getStringParameter(request, "sortOrder");

		int categoryId = 0;
		if (monthsChanged.equals("true")) {
			categoryId = userSession.getPfmCategoryId();
		} else {
			if (userSession.getPfmPreviousChartInfo() != null
					&& userSession.getPfmPreviousChartInfo().getPfmTransactions() != null) {
				categoryId = userSession.getPfmPreviousChartInfo().getPfmTransactions().get(index).getCategoryId();
			} else if (userSession.getPfmChartInfo() != null
					&& userSession.getPfmChartInfo().getPfmTransactions() != null) {
				categoryId = userSession.getPfmChartInfo().getPfmTransactions().get(0).getCategoryId();
			}
			userSession.setPfmCategoryId(categoryId);
		}

		// Send the request for transactions
		// Original page and paging length: 50
		int page = 1;
		int paging = 50;
		if (userSession.getPfmPaging() == null) {
			userSession.setPfmPaging(Integer.toString(paging));
		} else {
			paging = Integer.parseInt(userSession.getPfmPaging());
		}

		// Should we filter by one particular account? (transaction view)
		List<PFMAccountWrapper> oneAccount = new LinkedList<PFMAccountWrapper>();
		if (account != null && !account.equals("")) {
			// Loop through all the accounts and send only this one
			for (Iterator<PFMAccountWrapper> iterator = userSession.getPfmAccounts().iterator(); iterator.hasNext();) {
				PFMAccountWrapper pfmAccount = (PFMAccountWrapper) iterator.next();
				if ((pfmAccount.getAccountId().endsWith("DEP") && account.length() == 9 && pfmAccount.getAccountId()
						.substring(0, 9).equals(account))
						|| (account.length() == 15 && pfmAccount.getAccountId().substring(1, 16).equals(account))
						|| (account.length() == 16 && pfmAccount.getAccountId().substring(0, 16).equals(account))) {
					pfmAccount.setIncludeFlag(true);
					oneAccount.add(pfmAccount);
					break;
				}
			}
		}

		PFMTransactionRecordWrapper pfmTransactionRecord = new PFMTransactionRecordWrapper();
		PFMSortOrderWrapper sortOrder = new PFMSortOrderWrapper();
		if (sortOrderString == null || (sortOrderString != null && sortOrderString.equals("")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
		else if ((sortOrderString != null && sortOrderString.equals("0,0")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_ASCENDING);
		else if ((sortOrderString != null && sortOrderString.equals("0,1")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
		else if ((sortOrderString != null && (sortOrderString.equals("4,0") || sortOrderString.equals("3,0"))))
			sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_DESCENDING);
		else if ((sortOrderString != null && (sortOrderString.equals("4,1") || sortOrderString.equals("3,1"))))
			sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_ASCENDING);

		if (account != null && !account.equals("")) {
			pfmTransactionRecord = customerService.getPfmTransactions(userSession.getPfmId(), oneAccount, categoryId,
					startDate, endDate, page, paging, sortOrder);
		} else {
			pfmTransactionRecord = customerService.getPfmTransactions(userSession.getPfmId(),
					userSession.getPfmAccounts(), categoryId, startDate, endDate, page, paging, sortOrder);
		}

		// One final step - add the account nickname, front end id and custom
		// rule flag to the wrapper
		if (pfmTransactionRecord != null) {
			if (pfmTransactionRecord.getPfmTransactions() != null
					&& pfmTransactionRecord.getPfmTransactions().size() > 0) {
				int index2 = 0;
				for (Iterator<PFMTransactionWrapper> iterator = pfmTransactionRecord.getPfmTransactions().iterator(); iterator
						.hasNext();) {
					PFMTransactionWrapper wrapper = (PFMTransactionWrapper) iterator.next();
					for (Iterator<PFMAccountWrapper> iterator2 = userSession.getPfmAccounts().iterator(); iterator2
							.hasNext();) {
						PFMAccountWrapper accWrapper = (PFMAccountWrapper) iterator2.next();
						if (wrapper.getAccountId().equals(accWrapper.getAccountId())) {
							pfmTransactionRecord.getPfmTransactions().get(index2).setAccount(accWrapper.getNickname());
							pfmTransactionRecord.getPfmTransactions().get(index2)
									.setFrontendId(accWrapper.getFrontendId());
							pfmTransactionRecord
									.getPfmTransactions()
									.get(index2)
									.setHasCustomRule(
											findCustomRule(userSession,
													pfmTransactionRecord.getPfmTransactions().get(index2)));
							break;
						}
					}
					index2++;
				}
				userSession.setPfmTransactionRecord(pfmTransactionRecord);

				int startnumber = 1 + ((pfmTransactionRecord.getCurrentPage() - 1) * pfmTransactionRecord
						.getPagingRecords());
				int endnumber = pfmTransactionRecord.getCurrentPage() * pfmTransactionRecord.getPagingRecords();
				if (endnumber > pfmTransactionRecord.getTotalRecords())
					endnumber = pfmTransactionRecord.getTotalRecords();

				userSession.setCheckImageMap(pfmTransactionRecord.getImageKeyMap());
				userSession.getCheckImageMap();

				model.put("transactions", pfmTransactionRecord.getPfmTransactions());
				model.put("currentpage", pfmTransactionRecord.getCurrentPage());
				model.put("prevpage", pfmTransactionRecord.getCurrentPage() - 1);
				model.put("nextpage", pfmTransactionRecord.getCurrentPage() + 1);
				model.put("totalrecords", pfmTransactionRecord.getTotalRecords());
				model.put("pagesize", pfmTransactionRecord.getPagingRecords());
				model.put("startnumber", startnumber);
				model.put("endnumber", endnumber);
			} else {
				userSession.setPfmTransactionRecord(null);
				model.put("transactions", null);
				model.put("currentpage", "1");
				model.put("prevpage", "0");
				model.put("nextpage", "2");
				model.put("totalrecords", "0");
				model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
				model.put("startnumber", "0");
				model.put("endnumber", "0");
			}
		} else {
			userSession.setPfmTransactionRecord(null);
			model.put("transactions", null);
			model.put("currentpage", "1");
			model.put("prevpage", "0");
			model.put("nextpage", "2");
			model.put("totalrecords", "0");
			model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
			model.put("startnumber", "0");
			model.put("endnumber", "0");
		}

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");

		model.put("categoryId", categoryId);
		model.put("showaccounts", "true");
		model.put("tags", userSession.getPfmTags());

		// Null out the subcategory record in order to edit the correct trx
		userSession.setPfmTransactionSubcategoryRecord(null);

		return new ModelAndView("pfmTransactions", model);
	}

	public ModelAndView pfmIncomeTransactions(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the values of the month(s), account(s) and filter(s)
		String startDate = ServletRequestUtils.getStringParameter(request, "startDate");
		String endDate = ServletRequestUtils.getStringParameter(request, "endDate");

		// Are we filtering by this one account? (transaction view)
		String frontendid = ServletRequestUtils.getStringParameter(request, "frontendid");
		String account = "";
		if (frontendid != null && !"".equals(frontendid)) {
			TVFrontendAccount tvaccount = Utils.getAccountById(frontendid, userSession.getAccounts());
			if (tvaccount != null)
				account = tvaccount.getAccountNumber();
		}

		// Find the last day of the end month
		Calendar cal = Calendar.getInstance();
		cal.set(Integer.parseInt(endDate.substring(3)), Integer.parseInt(endDate.substring(0, 2)) - 1, 1);
		int maxday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		// Format the dates
		startDate = startDate.substring(3) + "-" + startDate.substring(0, 2) + "-01";
		endDate = endDate.substring(3) + "-" + endDate.substring(0, 2) + "-" + String.valueOf(maxday);

		// Get the sort order
		String sortOrderString = ServletRequestUtils.getStringParameter(request, "sortOrder");

		// Paging
		int newPaging = ServletRequestUtils.getIntParameter(request, "newPaging");
		int newPage = ServletRequestUtils.getIntParameter(request, "newPage");

		// Send the request for transactions
		// Current page and paging
		int page = 1;
		int paging = 50;
		if (userSession.getPfmTransactionRecord() != null) {
			page = userSession.getPfmTransactionRecord().getCurrentPage();
			paging = userSession.getPfmTransactionRecord().getPagingRecords();
		}

		if (newPaging > 0) {
			page = 1;
			paging = newPaging;
			userSession.setPfmPaging(Integer.toString(paging));
		}

		if (newPage > 0) {
			page = newPage;
		}

		// Should we filter by one particular account? (transaction view)
		List<PFMAccountWrapper> oneAccount = new LinkedList<PFMAccountWrapper>();
		if (account != null && !account.equals("")) {
			// Loop through all the accounts and send only this one
			for (Iterator<PFMAccountWrapper> iterator = userSession.getPfmAccounts().iterator(); iterator.hasNext();) {
				PFMAccountWrapper pfmAccount = (PFMAccountWrapper) iterator.next();
				if ((pfmAccount.getAccountId().endsWith("DEP") && account.length() == 9 && pfmAccount.getAccountId()
						.substring(0, 9).equals(account))
						|| (account.length() == 15 && pfmAccount.getAccountId().substring(1, 16).equals(account))
						|| (account.length() == 16 && pfmAccount.getAccountId().substring(0, 16).equals(account))) {
					pfmAccount.setIncludeFlag(true);
					oneAccount.add(pfmAccount);
					break;
				}
			}
		}

		// Find the category that belongs to income
		int categoryId = INCOME_CATEGORY_ID;

		// Set up the category to make sure only the included sub categories are
		// sent
		PFMCategoryWrapper incomeCategoryWrapper = new PFMCategoryWrapper();
		for (Iterator<PFMCategoryWrapper> iterator = userSession.getPfmCategories().iterator(); iterator.hasNext();) {
			incomeCategoryWrapper = (PFMCategoryWrapper) iterator.next();
			if (incomeCategoryWrapper.getCategoryId() == categoryId)
				break;
		}

		PFMTransactionRecordWrapper pfmTransactionRecord = new PFMTransactionRecordWrapper();
		PFMSortOrderWrapper sortOrder = new PFMSortOrderWrapper();
		if (sortOrderString == null || (sortOrderString != null && sortOrderString.equals("")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
		else if ((sortOrderString != null && sortOrderString.equals("0,0")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_ASCENDING);
		else if ((sortOrderString != null && sortOrderString.equals("0,1")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
		else if ((sortOrderString != null && (sortOrderString.equals("4,0") || sortOrderString.equals("3,0"))))
			sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_DESCENDING);
		else if ((sortOrderString != null && (sortOrderString.equals("4,1") || sortOrderString.equals("3,1"))))
			sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_ASCENDING);

		if (account != null && !account.equals("")) {
			pfmTransactionRecord = customerService.getPfmTransactions(userSession.getPfmId(), oneAccount, categoryId,
					startDate, endDate, page, paging, sortOrder, incomeCategoryWrapper);
		} else {
			pfmTransactionRecord = customerService.getPfmTransactions(userSession.getPfmId(),
					userSession.getPfmAccounts(), categoryId, startDate, endDate, page, paging, sortOrder,
					incomeCategoryWrapper);
		}

		// One final step - add the account nickname and front end id to the
		// wrapper
		if (pfmTransactionRecord != null) {
			if (pfmTransactionRecord.getPfmTransactions() != null
					&& pfmTransactionRecord.getPfmTransactions().size() > 0) {
				int index2 = 0;
				for (Iterator<PFMTransactionWrapper> iterator = pfmTransactionRecord.getPfmTransactions().iterator(); iterator
						.hasNext();) {
					PFMTransactionWrapper wrapper = (PFMTransactionWrapper) iterator.next();
					for (Iterator<PFMAccountWrapper> iterator2 = userSession.getPfmAccounts().iterator(); iterator2
							.hasNext();) {
						PFMAccountWrapper accWrapper = (PFMAccountWrapper) iterator2.next();
						if (wrapper.getAccountId().equals(accWrapper.getAccountId())) {
							pfmTransactionRecord.getPfmTransactions().get(index2).setAccount(accWrapper.getNickname());
							pfmTransactionRecord.getPfmTransactions().get(index2)
									.setFrontendId(accWrapper.getFrontendId());
							pfmTransactionRecord
									.getPfmTransactions()
									.get(index2)
									.setHasCustomRule(
											findCustomRule(userSession,
													pfmTransactionRecord.getPfmTransactions().get(index2)));
							break;
						}
					}
					index2++;
				}
				userSession.setPfmTransactionRecord(pfmTransactionRecord);

				int startnumber = 1 + ((pfmTransactionRecord.getCurrentPage() - 1) * pfmTransactionRecord
						.getPagingRecords());
				int endnumber = pfmTransactionRecord.getCurrentPage() * pfmTransactionRecord.getPagingRecords();
				if (endnumber > pfmTransactionRecord.getTotalRecords())
					endnumber = pfmTransactionRecord.getTotalRecords();

				userSession.setCheckImageMap(pfmTransactionRecord.getImageKeyMap());
				userSession.getCheckImageMap();

				model.put("transactions", pfmTransactionRecord.getPfmTransactions());
				model.put("currentpage", pfmTransactionRecord.getCurrentPage());
				model.put("prevpage", pfmTransactionRecord.getCurrentPage() - 1);
				model.put("nextpage", pfmTransactionRecord.getCurrentPage() + 1);
				model.put("totalrecords", pfmTransactionRecord.getTotalRecords());
				model.put("pagesize", pfmTransactionRecord.getPagingRecords());
				model.put("startnumber", startnumber);
				model.put("endnumber", endnumber);
			} else {
				userSession.setPfmTransactionRecord(null);
				model.put("transactions", null);
				model.put("currentpage", "1");
				model.put("prevpage", "0");
				model.put("nextpage", "2");
				model.put("totalrecords", "0");
				model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
				model.put("startnumber", "0");
				model.put("endnumber", "0");
			}
		} else {
			userSession.setPfmTransactionRecord(null);
			model.put("transactions", null);
			model.put("currentpage", "1");
			model.put("prevpage", "0");
			model.put("nextpage", "2");
			model.put("totalrecords", "0");
			model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
			model.put("startnumber", "0");
			model.put("endnumber", "0");
		}

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");

		model.put("categoryId", categoryId);
		model.put("showaccounts", "true");
		model.put("tags", userSession.getPfmTags());

		// Null out the subcategory record in order to edit the correct trx
		userSession.setPfmTransactionSubcategoryRecord(null);

		return new ModelAndView("pfmTransactions", model);
	}

	public ModelAndView pfmSubcategoryTransactions(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the values for this chart
		String monthsChanged = ServletRequestUtils.getStringParameter(request, "monthsChanged");
		String startDate = ServletRequestUtils.getStringParameter(request, "startDate");
		String endDate = ServletRequestUtils.getStringParameter(request, "endDate");
		int index = ServletRequestUtils.getIntParameter(request, "index");

		// Are we filtering by this one account? (transaction view)
		String frontendid = ServletRequestUtils.getStringParameter(request, "frontendid");
		String account = "";
		if (frontendid != null && !"".equals(frontendid)) {
			TVFrontendAccount tvaccount = Utils.getAccountById(frontendid, userSession.getAccounts());
			if (tvaccount != null)
				account = tvaccount.getAccountNumber();
		}

		// Find the last day of the end month
		Calendar cal = Calendar.getInstance();
		cal.set(Integer.parseInt(endDate.substring(3)), Integer.parseInt(endDate.substring(0, 2)) - 1, 1);
		int maxday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		// Format the dates
		startDate = startDate.substring(3) + "-" + startDate.substring(0, 2) + "-01";
		endDate = endDate.substring(3) + "-" + endDate.substring(0, 2) + "-" + String.valueOf(maxday);

		// Get the subcategory id
		int subcategoryId = 0;
		if (monthsChanged.equals("true")) {
			subcategoryId = userSession.getPfmSubcategoryId();
		} else {
			if (userSession.getPfmChartInfo() != null) {
				subcategoryId = userSession.getPfmChartInfo().getPfmTransactions().get(index).getCategoryId();
			} else {
				subcategoryId = userSession.getPfmOriginalChartInfo().getPfmTransactions().get(index).getCategoryId();
			}
			userSession.setPfmSubcategoryId(subcategoryId);
		}

		// Get the sort order
		String sortOrderString = ServletRequestUtils.getStringParameter(request, "sortOrder");

		// Send the request for transactions
		// Original page and paging length: 50
		int page = 1;
		int paging = 50;
		if (userSession.getPfmPaging() == null) {
			userSession.setPfmPaging(Integer.toString(paging));
		} else {
			paging = Integer.parseInt(userSession.getPfmPaging());
		}

		// Should we filter by one particular account? (transaction view)
		List<PFMAccountWrapper> oneAccount = new LinkedList<PFMAccountWrapper>();
		if (account != null && !account.equals("")) {
			// Loop through all the accounts and send only this one
			for (Iterator<PFMAccountWrapper> iterator = userSession.getPfmAccounts().iterator(); iterator.hasNext();) {
				PFMAccountWrapper pfmAccount = (PFMAccountWrapper) iterator.next();
				if ((pfmAccount.getAccountId().endsWith("DEP") && account.length() == 9 && pfmAccount.getAccountId()
						.substring(0, 9).equals(account))
						|| (account.length() == 15 && pfmAccount.getAccountId().substring(1, 16).equals(account))
						|| (account.length() == 16 && pfmAccount.getAccountId().substring(0, 16).equals(account))) {
					pfmAccount.setIncludeFlag(true);
					oneAccount.add(pfmAccount);
					break;
				}
			}
		}

		PFMTransactionRecordWrapper pfmTransactionSubcategoryRecord = new PFMTransactionRecordWrapper();
		PFMSortOrderWrapper sortOrder = new PFMSortOrderWrapper();
		if (sortOrderString == null || (sortOrderString != null && sortOrderString.equals("")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
		else if ((sortOrderString != null && sortOrderString.equals("0,0")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_ASCENDING);
		else if ((sortOrderString != null && sortOrderString.equals("0,1")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
		else if ((sortOrderString != null && (sortOrderString.equals("4,0") || sortOrderString.equals("3,0"))))
			sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_DESCENDING);
		else if ((sortOrderString != null && (sortOrderString.equals("4,1") || sortOrderString.equals("3,1"))))
			sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_ASCENDING);

		if (account != null && !account.equals("")) {
			pfmTransactionSubcategoryRecord = customerService.getPfmTransactionsSubcategory(userSession.getPfmId(),
					oneAccount, subcategoryId, startDate, endDate, page, paging, sortOrder);
		} else {
			pfmTransactionSubcategoryRecord = customerService.getPfmTransactionsSubcategory(userSession.getPfmId(),
					userSession.getPfmAccounts(), subcategoryId, startDate, endDate, page, paging, sortOrder);
		}

		// One final step - add the account nickname and front end id to the
		// wrapper
		if (pfmTransactionSubcategoryRecord != null) {
			if (pfmTransactionSubcategoryRecord.getPfmTransactions() != null
					&& pfmTransactionSubcategoryRecord.getPfmTransactions().size() > 0) {
				int index2 = 0;
				for (Iterator<PFMTransactionWrapper> iterator = pfmTransactionSubcategoryRecord.getPfmTransactions()
						.iterator(); iterator.hasNext();) {
					PFMTransactionWrapper wrapper = (PFMTransactionWrapper) iterator.next();
					for (Iterator<PFMAccountWrapper> iterator2 = userSession.getPfmAccounts().iterator(); iterator2
							.hasNext();) {
						PFMAccountWrapper accWrapper = (PFMAccountWrapper) iterator2.next();
						if (wrapper.getAccountId().equals(accWrapper.getAccountId())) {
							pfmTransactionSubcategoryRecord.getPfmTransactions().get(index2)
									.setAccount(accWrapper.getNickname());
							pfmTransactionSubcategoryRecord.getPfmTransactions().get(index2)
									.setFrontendId(accWrapper.getFrontendId());
							pfmTransactionSubcategoryRecord
									.getPfmTransactions()
									.get(index2)
									.setHasCustomRule(
											findCustomRule(userSession, pfmTransactionSubcategoryRecord
													.getPfmTransactions().get(index2)));
							break;
						}
					}
					index2++;
				}
				userSession.setPfmTransactionSubcategoryRecord(pfmTransactionSubcategoryRecord);

				int startnumber = 1 + ((pfmTransactionSubcategoryRecord.getCurrentPage() - 1) * pfmTransactionSubcategoryRecord
						.getPagingRecords());
				int endnumber = pfmTransactionSubcategoryRecord.getCurrentPage()
						* pfmTransactionSubcategoryRecord.getPagingRecords();
				if (endnumber > pfmTransactionSubcategoryRecord.getTotalRecords())
					endnumber = pfmTransactionSubcategoryRecord.getTotalRecords();

				userSession.setCheckImageMap(pfmTransactionSubcategoryRecord.getImageKeyMap());
				userSession.getCheckImageMap();

				model.put("transactions", pfmTransactionSubcategoryRecord.getPfmTransactions());
				model.put("currentpage", pfmTransactionSubcategoryRecord.getCurrentPage());
				model.put("nextpage", pfmTransactionSubcategoryRecord.getCurrentPage() + 1);
				model.put("prevpage", pfmTransactionSubcategoryRecord.getCurrentPage() - 1);
				model.put("totalrecords", pfmTransactionSubcategoryRecord.getTotalRecords());
				model.put("pagesize", pfmTransactionSubcategoryRecord.getPagingRecords());
				model.put("startnumber", startnumber);
				model.put("endnumber", endnumber);
			} else {
				userSession.setPfmTransactionSubcategoryRecord(null);
				model.put("transactions", null);
				model.put("currentpage", "1");
				model.put("nextpage", "2");
				model.put("prevpage", "0");
				model.put("totalrecords", "0");
				model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
				model.put("startnumber", "0");
				model.put("endnumber", "0");
			}
		} else {
			userSession.setPfmTransactionSubcategoryRecord(null);
			model.put("transactions", null);
			model.put("currentpage", "1");
			model.put("nextpage", "2");
			model.put("prevpage", "0");
			model.put("totalrecords", "0");
			model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
			model.put("startnumber", "0");
			model.put("endnumber", "0");
		}

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");

		model.put("subcategoryId", subcategoryId);
		model.put("showaccounts", "true");
		model.put("tags", userSession.getPfmTags());
		return new ModelAndView("pfmTransactions", model);
	}

	public ModelAndView pfmResetCategoryTransactions(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the original transactions and re send them
		PFMTransactionRecordWrapper pfmTransactionRecord = userSession.getPfmTransactionRecord();
		if (pfmTransactionRecord != null) {
			int startnumber = 1 + ((pfmTransactionRecord.getCurrentPage() - 1) * pfmTransactionRecord
					.getPagingRecords());
			int endnumber = pfmTransactionRecord.getCurrentPage() * pfmTransactionRecord.getPagingRecords();
			if (endnumber > pfmTransactionRecord.getTotalRecords())
				endnumber = pfmTransactionRecord.getTotalRecords();

			userSession.setCheckImageMap(pfmTransactionRecord.getImageKeyMap());
			userSession.getCheckImageMap();

			model.put("transactions", pfmTransactionRecord.getPfmTransactions());
			model.put("currentpage", pfmTransactionRecord.getCurrentPage());
			model.put("nextpage", pfmTransactionRecord.getCurrentPage() + 1);
			model.put("prevpage", pfmTransactionRecord.getCurrentPage() - 1);
			model.put("totalrecords", pfmTransactionRecord.getTotalRecords());
			model.put("pagesize", pfmTransactionRecord.getPagingRecords());
			model.put("startnumber", startnumber);
			model.put("endnumber", endnumber);
		} else {
			model.put("transactions", null);
			model.put("currentpage", "1");
			model.put("nextpage", "2");
			model.put("prevpage", "0");
			model.put("totalrecords", "0");
			model.put("pagesize", userSession.getPfmPaging());
			model.put("startnumber", "0");
			model.put("endnumber", "0");
		}

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");

		model.put("showaccounts", "true");
		model.put("tags", userSession.getPfmTags());
		return new ModelAndView("pfmTransactions", model);
	}

	public ModelAndView pfmReloadCategoryTransactions(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the values of the month(s), account(s) and filter(s)
		int newPaging = ServletRequestUtils.getIntParameter(request, "newPaging");
		int newPage = ServletRequestUtils.getIntParameter(request, "newPage");
		String startDate = ServletRequestUtils.getStringParameter(request, "startDate");
		String endDate = ServletRequestUtils.getStringParameter(request, "endDate");

		// Are we filtering by this one account? (transaction view)
		String frontendid = ServletRequestUtils.getStringParameter(request, "frontendid");
		String account = "";
		if (frontendid != null && !"".equals(frontendid)) {
			TVFrontendAccount tvaccount = Utils.getAccountById(frontendid, userSession.getAccounts());
			if (tvaccount != null)
				account = tvaccount.getAccountNumber();
		}

		// Find the last day of the end month
		Calendar cal = Calendar.getInstance();
		cal.set(Integer.parseInt(endDate.substring(3)), Integer.parseInt(endDate.substring(0, 2)) - 1, 1);
		int maxday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		// Format the dates
		startDate = startDate.substring(3) + "-" + startDate.substring(0, 2) + "-01";
		endDate = endDate.substring(3) + "-" + endDate.substring(0, 2) + "-" + String.valueOf(maxday);

		int categoryId = userSession.getPfmCategoryId();

		// Get the sorting
		String sortOrderString = ServletRequestUtils.getStringParameter(request, "sortOrder");

		// Send the request for transactions
		// Current page and paging
		int page = userSession.getPfmTransactionRecord().getCurrentPage();
		int paging = userSession.getPfmTransactionRecord().getPagingRecords();

		if (newPaging > 0) {
			page = 1;
			paging = newPaging;
			userSession.setPfmPaging(Integer.toString(paging));
		}

		if (newPage > 0) {
			page = newPage;
		}

		// Should we filter by one particular account? (transaction view)
		List<PFMAccountWrapper> oneAccount = new LinkedList<PFMAccountWrapper>();
		if (account != null && !account.equals("")) {
			// Loop through all the accounts and send only this one
			for (Iterator<PFMAccountWrapper> iterator = userSession.getPfmAccounts().iterator(); iterator.hasNext();) {
				PFMAccountWrapper pfmAccount = (PFMAccountWrapper) iterator.next();
				if ((pfmAccount.getAccountId().endsWith("DEP") && account.length() == 9 && pfmAccount.getAccountId()
						.substring(0, 9).equals(account))
						|| (account.length() == 15 && pfmAccount.getAccountId().substring(1, 16).equals(account))
						|| (account.length() == 16 && pfmAccount.getAccountId().substring(0, 16).equals(account))) {
					pfmAccount.setIncludeFlag(true);
					oneAccount.add(pfmAccount);
					break;
				}
			}
		}
		PFMTransactionRecordWrapper pfmTransactionRecord = new PFMTransactionRecordWrapper();

		PFMSortOrderWrapper sortOrder = new PFMSortOrderWrapper();
		if (sortOrderString == null || (sortOrderString != null && sortOrderString.equals("")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
		else if ((sortOrderString != null && sortOrderString.equals("0,0")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_ASCENDING);
		else if ((sortOrderString != null && sortOrderString.equals("0,1")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
		else if ((sortOrderString != null && (sortOrderString.equals("4,0") || sortOrderString.equals("3,0"))))
			sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_DESCENDING);
		else if ((sortOrderString != null && (sortOrderString.equals("4,1") || sortOrderString.equals("3,1"))))
			sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_ASCENDING);

		if (account != null && !account.equals("")) {
			pfmTransactionRecord = customerService.getPfmTransactions(userSession.getPfmId(), oneAccount, categoryId,
					startDate, endDate, page, paging, sortOrder);
		} else {
			pfmTransactionRecord = customerService.getPfmTransactions(userSession.getPfmId(),
					userSession.getPfmAccounts(), categoryId, startDate, endDate, page, paging, sortOrder);
		}

		// One final step - add the account nickname and front end id to the
		// wrapper
		if (pfmTransactionRecord != null) {
			if (pfmTransactionRecord.getPfmTransactions() != null
					&& pfmTransactionRecord.getPfmTransactions().size() > 0) {
				int index2 = 0;
				for (Iterator<PFMTransactionWrapper> iterator = pfmTransactionRecord.getPfmTransactions().iterator(); iterator
						.hasNext();) {
					PFMTransactionWrapper wrapper = (PFMTransactionWrapper) iterator.next();
					for (Iterator<PFMAccountWrapper> iterator2 = userSession.getPfmAccounts().iterator(); iterator2
							.hasNext();) {
						PFMAccountWrapper accWrapper = (PFMAccountWrapper) iterator2.next();
						if (wrapper.getAccountId().equals(accWrapper.getAccountId())) {
							pfmTransactionRecord.getPfmTransactions().get(index2).setAccount(accWrapper.getNickname());
							pfmTransactionRecord.getPfmTransactions().get(index2)
									.setFrontendId(accWrapper.getFrontendId());
							pfmTransactionRecord
									.getPfmTransactions()
									.get(index2)
									.setHasCustomRule(
											findCustomRule(userSession,
													pfmTransactionRecord.getPfmTransactions().get(index2)));
							break;
						}
					}
					index2++;
				}
				userSession.setPfmTransactionRecord(pfmTransactionRecord);

				int startnumber = 1 + ((pfmTransactionRecord.getCurrentPage() - 1) * pfmTransactionRecord
						.getPagingRecords());
				int endnumber = pfmTransactionRecord.getCurrentPage() * pfmTransactionRecord.getPagingRecords();
				if (endnumber > pfmTransactionRecord.getTotalRecords())
					endnumber = pfmTransactionRecord.getTotalRecords();

				userSession.setCheckImageMap(pfmTransactionRecord.getImageKeyMap());
				userSession.getCheckImageMap();

				model.put("transactions", pfmTransactionRecord.getPfmTransactions());
				model.put("currentpage", pfmTransactionRecord.getCurrentPage());
				model.put("nextpage", pfmTransactionRecord.getCurrentPage() + 1);
				model.put("prevpage", pfmTransactionRecord.getCurrentPage() - 1);
				model.put("totalrecords", pfmTransactionRecord.getTotalRecords());
				model.put("pagesize", pfmTransactionRecord.getPagingRecords());
				model.put("startnumber", startnumber);
				model.put("endnumber", endnumber);
			} else {
				userSession.setPfmTransactionRecord(null);
				model.put("transactions", null);
				model.put("currentpage", "1");
				model.put("nextpage", "2");
				model.put("prevpage", "0");
				model.put("totalrecords", "0");
				model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
				model.put("startnumber", "0");
				model.put("endnumber", "0");
			}
		} else {
			userSession.setPfmTransactionRecord(null);
			model.put("transactions", null);
			model.put("currentpage", "1");
			model.put("nextpage", "2");
			model.put("prevpage", "0");
			model.put("totalrecords", "0");
			model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
			model.put("startnumber", "0");
			model.put("endnumber", "0");
		}

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");

		model.put("categoryId", categoryId);
		model.put("showaccounts", "true");
		model.put("tags", userSession.getPfmTags());

		// Null out the subcategory record in order to edit the correct trx
		userSession.setPfmTransactionSubcategoryRecord(null);

		return new ModelAndView("pfmTransactions", model);
	}

	public ModelAndView pfmReloadSubcategoryTransactions(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the values for this chart
		int newPaging = ServletRequestUtils.getIntParameter(request, "newPaging");
		int newPage = ServletRequestUtils.getIntParameter(request, "newPage");
		String startDate = ServletRequestUtils.getStringParameter(request, "startDate");
		String endDate = ServletRequestUtils.getStringParameter(request, "endDate");

		// Are we filtering by this one account? (transaction view)
		String frontendid = ServletRequestUtils.getStringParameter(request, "frontendid");
		String account = "";
		if (frontendid != null && !"".equals(frontendid)) {
			TVFrontendAccount tvaccount = Utils.getAccountById(frontendid, userSession.getAccounts());
			if (tvaccount != null)
				account = tvaccount.getAccountNumber();
		}

		// Find the last day of the end month
		Calendar cal = Calendar.getInstance();
		cal.set(Integer.parseInt(endDate.substring(3)), Integer.parseInt(endDate.substring(0, 2)) - 1, 1);
		int maxday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		// Format the dates
		startDate = startDate.substring(3) + "-" + startDate.substring(0, 2) + "-01";
		endDate = endDate.substring(3) + "-" + endDate.substring(0, 2) + "-" + String.valueOf(maxday);

		// Get the subcategory id
		int subcategoryId = userSession.getPfmSubcategoryId();

		// Get the sorting
		String sortOrderString = ServletRequestUtils.getStringParameter(request, "sortOrder");

		// Send the request for transactions
		// Current page and paging
		int page = userSession.getPfmTransactionSubcategoryRecord().getCurrentPage();
		int paging = userSession.getPfmTransactionSubcategoryRecord().getPagingRecords();

		if (newPaging > 0) {
			page = 1;
			paging = newPaging;
			userSession.setPfmPaging(Integer.toString(paging));
		}

		if (newPage > 0) {
			page = newPage;
		}

		// Should we filter by one particular account? (transaction view)
		List<PFMAccountWrapper> oneAccount = new LinkedList<PFMAccountWrapper>();
		if (account != null && !account.equals("")) {
			// Loop through all the accounts and send only this one
			for (Iterator<PFMAccountWrapper> iterator = userSession.getPfmAccounts().iterator(); iterator.hasNext();) {
				PFMAccountWrapper pfmAccount = (PFMAccountWrapper) iterator.next();
				if ((pfmAccount.getAccountId().endsWith("DEP") && account.length() == 9 && pfmAccount.getAccountId()
						.substring(0, 9).equals(account))
						|| (account.length() == 15 && pfmAccount.getAccountId().substring(1, 16).equals(account))
						|| (account.length() == 16 && pfmAccount.getAccountId().substring(0, 16).equals(account))) {
					pfmAccount.setIncludeFlag(true);
					oneAccount.add(pfmAccount);
					break;
				}
			}
		}

		PFMTransactionRecordWrapper pfmTransactionSubcategoryRecord = new PFMTransactionRecordWrapper();

		PFMSortOrderWrapper sortOrder = new PFMSortOrderWrapper();
		if (sortOrderString == null || (sortOrderString != null && sortOrderString.equals("")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
		else if ((sortOrderString != null && sortOrderString.equals("0,0")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_ASCENDING);
		else if ((sortOrderString != null && sortOrderString.equals("0,1")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
		else if ((sortOrderString != null && (sortOrderString.equals("4,0") || sortOrderString.equals("3,0"))))
			sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_DESCENDING);
		else if ((sortOrderString != null && (sortOrderString.equals("4,1") || sortOrderString.equals("3,1"))))
			sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_ASCENDING);

		if (account != null && !account.equals("")) {
			pfmTransactionSubcategoryRecord = customerService.getPfmTransactionsSubcategory(userSession.getPfmId(),
					oneAccount, subcategoryId, startDate, endDate, page, paging, sortOrder);
		} else {
			pfmTransactionSubcategoryRecord = customerService.getPfmTransactionsSubcategory(userSession.getPfmId(),
					userSession.getPfmAccounts(), subcategoryId, startDate, endDate, page, paging, sortOrder);
		}

		// One final step - add the account nickname and front end id to the
		// wrapper
		if (pfmTransactionSubcategoryRecord != null) {
			if (pfmTransactionSubcategoryRecord.getPfmTransactions() != null
					&& pfmTransactionSubcategoryRecord.getPfmTransactions().size() > 0) {
				int index2 = 0;
				for (Iterator<PFMTransactionWrapper> iterator = pfmTransactionSubcategoryRecord.getPfmTransactions()
						.iterator(); iterator.hasNext();) {
					PFMTransactionWrapper wrapper = (PFMTransactionWrapper) iterator.next();
					for (Iterator<PFMAccountWrapper> iterator2 = userSession.getPfmAccounts().iterator(); iterator2
							.hasNext();) {
						PFMAccountWrapper accWrapper = (PFMAccountWrapper) iterator2.next();
						if (wrapper.getAccountId().equals(accWrapper.getAccountId())) {
							pfmTransactionSubcategoryRecord.getPfmTransactions().get(index2)
									.setAccount(accWrapper.getNickname());
							pfmTransactionSubcategoryRecord.getPfmTransactions().get(index2)
									.setFrontendId(accWrapper.getFrontendId());
							pfmTransactionSubcategoryRecord
									.getPfmTransactions()
									.get(index2)
									.setHasCustomRule(
											findCustomRule(userSession, pfmTransactionSubcategoryRecord
													.getPfmTransactions().get(index2)));
							break;
						}
					}
					index2++;
				}
				userSession.setPfmTransactionSubcategoryRecord(pfmTransactionSubcategoryRecord);

				// Update the category transaction record in order to keep the
				// two lists synched
				int catindex = 0;
				for (Iterator<PFMTransactionWrapper> iterator = userSession.getPfmTransactionSubcategoryRecord()
						.getPfmTransactions().iterator(); iterator.hasNext();) {
					catindex = 0;
					PFMTransactionWrapper subcategoryTrx = (PFMTransactionWrapper) iterator.next();
					for (Iterator<PFMTransactionWrapper> iterator2 = userSession.getPfmTransactionRecord()
							.getPfmTransactions().iterator(); iterator2.hasNext();) {
						PFMTransactionWrapper categoryTrx = (PFMTransactionWrapper) iterator2.next();
						if (subcategoryTrx.getTransactionId().equals(categoryTrx.getTransactionId())) {
							userSession.getPfmTransactionRecord().getPfmTransactions().set(catindex, subcategoryTrx);
							break;
						}
						catindex++;
					}
				}

				int startnumber = 1 + ((pfmTransactionSubcategoryRecord.getCurrentPage() - 1) * pfmTransactionSubcategoryRecord
						.getPagingRecords());
				int endnumber = pfmTransactionSubcategoryRecord.getCurrentPage()
						* pfmTransactionSubcategoryRecord.getPagingRecords();
				if (endnumber > pfmTransactionSubcategoryRecord.getTotalRecords())
					endnumber = pfmTransactionSubcategoryRecord.getTotalRecords();

				userSession.setCheckImageMap(pfmTransactionSubcategoryRecord.getImageKeyMap());
				userSession.getCheckImageMap();

				model.put("transactions", pfmTransactionSubcategoryRecord.getPfmTransactions());
				model.put("currentpage", pfmTransactionSubcategoryRecord.getCurrentPage());
				model.put("prevpage", pfmTransactionSubcategoryRecord.getCurrentPage() - 1);
				model.put("nextpage", pfmTransactionSubcategoryRecord.getCurrentPage() + 1);
				model.put("totalrecords", pfmTransactionSubcategoryRecord.getTotalRecords());
				model.put("pagesize", pfmTransactionSubcategoryRecord.getPagingRecords());
				model.put("startnumber", startnumber);
				model.put("endnumber", endnumber);
			} else {
				userSession.setPfmTransactionSubcategoryRecord(null);
				model.put("transactions", null);
				model.put("currentpage", "1");
				model.put("prevpage", "0");
				model.put("nextpage", "2");
				model.put("totalrecords", "0");
				model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
				model.put("startnumber", "0");
				model.put("endnumber", "0");
			}
		} else {
			userSession.setPfmTransactionSubcategoryRecord(null);
			model.put("transactions", null);
			model.put("currentpage", "1");
			model.put("prevpage", "0");
			model.put("nextpage", "2");
			model.put("totalrecords", "0");
			model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
			model.put("startnumber", "0");
			model.put("endnumber", "0");
		}

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");

		model.put("subcategoryId", subcategoryId);
		model.put("showaccounts", "true");
		model.put("tags", userSession.getPfmTags());
		return new ModelAndView("pfmTransactions", model);
	}

	public ModelAndView pfmTagTransactions(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the values of the month(s), account(s) and filter(s)
		String monthsChanged = ServletRequestUtils.getStringParameter(request, "monthsChanged");
		String startDate = ServletRequestUtils.getStringParameter(request, "startDate");
		String endDate = ServletRequestUtils.getStringParameter(request, "endDate");
		String presentationType = ServletRequestUtils.getStringParameter(request, "presentationType");
		int index = ServletRequestUtils.getIntParameter(request, "index");

		// Are we filtering by this one account? (transaction view)
		String frontendid = ServletRequestUtils.getStringParameter(request, "frontendid");
		String account = "";
		if (frontendid != null && !"".equals(frontendid)) {
			TVFrontendAccount tvaccount = Utils.getAccountById(frontendid, userSession.getAccounts());
			if (tvaccount != null)
				account = tvaccount.getAccountNumber();
		}

		// Find the last day of the end month
		Calendar cal = Calendar.getInstance();
		cal.set(Integer.parseInt(endDate.substring(3)), Integer.parseInt(endDate.substring(0, 2)) - 1, 1);
		int maxday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		// Format the dates
		startDate = startDate.substring(3) + "-" + startDate.substring(0, 2) + "-01";
		endDate = endDate.substring(3) + "-" + endDate.substring(0, 2) + "-" + String.valueOf(maxday);

		int tagId = 0;

		// Get the sort order
		String sortOrderString = ServletRequestUtils.getStringParameter(request, "sortOrder");

		if (monthsChanged.equals("true")) {
			tagId = userSession.getPfmTagId();
		} else {
			if (userSession.getPfmChartInfo() != null) {
				tagId = userSession.getPfmChartInfo().getPfmTransactions().get(index).getCategoryId();
			} else {
				tagId = userSession.getPfmOriginalChartInfo().getPfmTransactions().get(index).getCategoryId();
			}
			userSession.setPfmTagId(tagId);
		}

		// Send the request for transactions
		// Original page and paging length: 50
		int page = 1;
		int paging = 50;
		if (userSession.getPfmPaging() == null) {
			userSession.setPfmPaging(Integer.toString(paging));
		} else {
			paging = Integer.parseInt(userSession.getPfmPaging());
		}

		// Should we filter by one particular account? (transaction view)
		List<PFMAccountWrapper> oneAccount = new LinkedList<PFMAccountWrapper>();
		if (account != null && !account.equals("")) {
			// Loop through all the accounts and send only this one
			for (Iterator<PFMAccountWrapper> iterator = userSession.getPfmAccounts().iterator(); iterator.hasNext();) {
				PFMAccountWrapper pfmAccount = (PFMAccountWrapper) iterator.next();
				if ((pfmAccount.getAccountId().endsWith("DEP") && account.length() == 9 && pfmAccount.getAccountId()
						.substring(0, 9).equals(account))
						|| (account.length() == 15 && pfmAccount.getAccountId().substring(1, 16).equals(account))
						|| (account.length() == 16 && pfmAccount.getAccountId().substring(0, 16).equals(account))) {
					pfmAccount.setIncludeFlag(true);
					oneAccount.add(pfmAccount);
					break;
				}
			}
		}

		PFMTransactionRecordWrapper pfmTransactionRecord = new PFMTransactionRecordWrapper();
		PFMSortOrderWrapper sortOrder = new PFMSortOrderWrapper();
		if (sortOrderString == null || (sortOrderString != null && sortOrderString.equals("")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
		else if ((sortOrderString != null && sortOrderString.equals("0,0")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_ASCENDING);
		else if ((sortOrderString != null && sortOrderString.equals("0,1")))
			sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
		else if ((sortOrderString != null && (sortOrderString.equals("4,0") || sortOrderString.equals("3,0"))))
			sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_DESCENDING);
		else if ((sortOrderString != null && (sortOrderString.equals("4,1") || sortOrderString.equals("3,1"))))
			sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_ASCENDING);

		if (account != null && !account.equals("")) {
			pfmTransactionRecord = customerService.getPfmTransactionsTag(userSession.getPfmId(), oneAccount, tagId,
					startDate, endDate, page, paging, sortOrder, presentationType, userSession.getPfmCategories());
		} else {
			pfmTransactionRecord = customerService.getPfmTransactionsTag(userSession.getPfmId(),
					userSession.getPfmAccounts(), tagId, startDate, endDate, page, paging, sortOrder, presentationType,
					userSession.getPfmCategories());
		}

		// One final step - add the account nickname and front end id to the
		// wrapper
		if (pfmTransactionRecord != null) {
			if (pfmTransactionRecord.getPfmTransactions() != null
					&& pfmTransactionRecord.getPfmTransactions().size() > 0) {
				int index2 = 0;
				for (Iterator<PFMTransactionWrapper> iterator = pfmTransactionRecord.getPfmTransactions().iterator(); iterator
						.hasNext();) {
					PFMTransactionWrapper wrapper = (PFMTransactionWrapper) iterator.next();
					for (Iterator<PFMAccountWrapper> iterator2 = userSession.getPfmAccounts().iterator(); iterator2
							.hasNext();) {
						PFMAccountWrapper accWrapper = (PFMAccountWrapper) iterator2.next();
						if (wrapper.getAccountId().equals(accWrapper.getAccountId())) {
							pfmTransactionRecord.getPfmTransactions().get(index2).setAccount(accWrapper.getNickname());
							pfmTransactionRecord.getPfmTransactions().get(index2)
									.setFrontendId(accWrapper.getFrontendId());
							pfmTransactionRecord
									.getPfmTransactions()
									.get(index2)
									.setHasCustomRule(
											findCustomRule(userSession,
													pfmTransactionRecord.getPfmTransactions().get(index2)));
							break;
						}
					}
					index2++;
				}
				userSession.setPfmTransactionRecord(pfmTransactionRecord);

				int startnumber = 1 + ((pfmTransactionRecord.getCurrentPage() - 1) * pfmTransactionRecord
						.getPagingRecords());
				int endnumber = pfmTransactionRecord.getCurrentPage() * pfmTransactionRecord.getPagingRecords();
				if (endnumber > pfmTransactionRecord.getTotalRecords())
					endnumber = pfmTransactionRecord.getTotalRecords();

				userSession.setCheckImageMap(pfmTransactionRecord.getImageKeyMap());
				userSession.getCheckImageMap();

				model.put("transactions", pfmTransactionRecord.getPfmTransactions());
				model.put("currentpage", pfmTransactionRecord.getCurrentPage());
				model.put("nextpage", pfmTransactionRecord.getCurrentPage() + 1);
				model.put("prevpage", pfmTransactionRecord.getCurrentPage() - 1);
				model.put("totalrecords", pfmTransactionRecord.getTotalRecords());
				model.put("pagesize", pfmTransactionRecord.getPagingRecords());
				model.put("startnumber", startnumber);
				model.put("endnumber", endnumber);
			} else {
				userSession.setPfmTransactionRecord(null);
				model.put("transactions", null);
				model.put("currentpage", "1");
				model.put("nextpage", "2");
				model.put("prevpage", "0");
				model.put("totalrecords", "0");
				model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
				model.put("startnumber", "0");
				model.put("endnumber", "0");
			}
		} else {
			userSession.setPfmTransactionRecord(null);
			model.put("transactions", null);
			model.put("currentpage", "1");
			model.put("nextpage", "2");
			model.put("prevpage", "0");
			model.put("totalrecords", "0");
			model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
			model.put("startnumber", "0");
			model.put("endnumber", "0");
		}

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");

		model.put("categoryId", 0);
		model.put("tagId", tagId);
		model.put("showaccounts", "true");
		model.put("tags", userSession.getPfmTags());

		// Null out the subcategory record in order to edit the correct trx
		userSession.setPfmTransactionSubcategoryRecord(null);

		return new ModelAndView("pfmTransactions", model);
	}

	public ModelAndView pfmTransactionCategories(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		if (userSession.isInPfmUpdatedLookedUp() && userSession.isInPfmUpdate()) {
			return new ModelAndView("pfmUpdateInProgress", model);
		} else {
			// Get all the categories if they have not been loaded into memory
			// yet
			getPfmCategories(userSession);

			if ("EN".equalsIgnoreCase(userSession.getLanguage()))
				model.put("lang", "en");
			else
				model.put("lang", "es");

			model.put("categories", userSession.getPfmCategories());
			model.put("catIdToHighlight", userSession.getPfmCategoryIdToHighlight());
			// If the category to highlight is the income/credits, show only
			// that one
			// If not, show all EXCEPT income/credits
			if (userSession.getPfmCategoryIdToHighlight() == INCOME_CATEGORY_ID)
				model.put("showOnlyCredit", true);
			else
				model.put("showOnlyCredit", false);
			return new ModelAndView("pfmTransactionCategories", model);
		}
	}

	public ModelAndView pfmManageSubcategories(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get all the categories if they have not been loaded into memory yet
		getPfmCategories(userSession);

		// Get the id being sent in
		int categoryId = ServletRequestUtils.getIntParameter(request, "categoryId");

		// Get only the subcategories that belong to this category
		for (Iterator<PFMCategoryWrapper> iterator = userSession.getPfmCategories().iterator(); iterator.hasNext();) {
			PFMCategoryWrapper category = (PFMCategoryWrapper) iterator.next();
			if (category.getCategoryId() == categoryId) {
				model.put("category", category);
				break;
			}
		}

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");
		return new ModelAndView("pfmManageSubcategories", model);
	}

	public ModelAndView pfmManageTags(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Set up the tags
		getPfmTags(userSession);
		model.put("tags", userSession.getPfmTags());

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");
		return new ModelAndView("pfmManageTags", model);
	}

	public ModelAndView pfmManageRules(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String page = ServletRequestUtils.getStringParameter(request, "page");
		String newpaging = ServletRequestUtils.getStringParameter(request, "paging");

		// Set up the rules...
		getPfmCustomRules(userSession);

		int paging = 0;
		if (newpaging == null)
			paging = userSession.getPfmCustomRulePaging() == null ? 10 : Integer.parseInt(userSession
					.getPfmCustomRulePaging());
		else {
			userSession.setPfmCustomRulePaging(newpaging);
			paging = Integer.parseInt(newpaging);
		}

		int currentpage = (page == null) ? 1 : Integer.parseInt(page);
		int startnumber = ((currentpage - 1) * paging) + 1;
		int endnumber = currentpage * paging;
		if (endnumber > userSession.getPfmCustomRules().size())
			endnumber = userSession.getPfmCustomRules().size();

		// Only get custom rules from 1 to the end number
		List<PFMCustomRuleWrapper> pfmCustomRules = new LinkedList<PFMCustomRuleWrapper>();
		for (int i = (startnumber - 1); i < endnumber; i++) {
			pfmCustomRules.add(userSession.getPfmCustomRules().get(i));
		}

		model.put("customrules", pfmCustomRules);
		model.put("currentpage", currentpage);
		model.put("nextpage", (currentpage + 1));
		model.put("prevpage", (currentpage - 1));
		model.put("totalrecords", userSession.getPfmCustomRules().size());
		model.put("pagesize", paging);
		model.put("startnumber", startnumber);
		model.put("endnumber", endnumber);

		// Get the categories for the drop-down
		model.put("categories", userSession.getPfmCategories());

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");

		return new ModelAndView("pfmManageRules", model);
	}

	public ModelAndView pfmEditTransaction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Set up the tags
		getPfmTags(userSession);
		model.put("tags", userSession.getPfmTags());

		// Set up the rules...
		getPfmCustomRules(userSession);

		// Get the transaction information...
		String transactionId = userSession.getPfmTransactionIdToEdit();

		// If the transaction exists...
		boolean found = false;
		PFMTransactionWrapper wrapper = new PFMTransactionWrapper();

		if (transactionId != null) {
			// Find this transaction and post it to the model
			// First, look in the subcategory record
			if (userSession.getPfmTransactionSubcategoryRecord() != null
					&& userSession.getPfmTransactionSubcategoryRecord().getPfmTransactions() != null
					&& userSession.getPfmTransactionSubcategoryRecord().getPfmTransactions().size() > 0) {
				for (Iterator<PFMTransactionWrapper> iterator = userSession.getPfmTransactionSubcategoryRecord()
						.getPfmTransactions().iterator(); iterator.hasNext();) {
					wrapper = (PFMTransactionWrapper) iterator.next();
					if (wrapper.getTransactionId().equals(transactionId)) {
						model.put("transaction", wrapper);
						found = true;
						break;
					}
				}
			}
			// If we have not found the transaction, look in the regular
			// transaction record
			if (!found) {
				for (Iterator<PFMTransactionWrapper> iterator = userSession.getPfmTransactionRecord()
						.getPfmTransactions().iterator(); iterator.hasNext();) {
					wrapper = (PFMTransactionWrapper) iterator.next();
					if (wrapper.getTransactionId().equals(transactionId)) {
						model.put("transaction", wrapper);
						found = true;
						break;
					}
				}
			}
		}

		// If a custom rule was found, obtain it
		PFMCustomRuleWrapper customRule = findEntireCustomRule(userSession, wrapper, false);
		if (customRule != null) {
			model.put("customrule", "true");
			model.put("subcategory",
					("EN".equalsIgnoreCase(userSession.getLanguage()) ? customRule.getSubcategory_description_en()
							: customRule.getSubcategory_description_es()));

			String originalSubcategory = "";
			if (customRule.getOriginalSubcategoryId() > 0)
				originalSubcategory = "EN".equalsIgnoreCase(userSession.getLanguage()) ? customRule
						.getOriginalSubcategory_description_en() : customRule.getOriginalSubcategory_description_es();

			model.put("originalsubcategory", originalSubcategory);
		} else {
			model.put("customrule", "false");
			userSession.setPfmCurrentRuleId(0);
		}

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");
		return new ModelAndView("pfmEditTransaction", model);
	}

	public ModelAndView pfmResetTransaction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the transaction information...
		String transactionId = ServletRequestUtils.getStringParameter(request, "transactionId");
		PFMTransactionWrapper wrapper = new PFMTransactionWrapper();
		boolean found = false;

		// Find this transaction and post it to the model
		if (!transactionId.equals("undefined")) {

			// First, look in the subcategory record
			if (userSession.getPfmTransactionSubcategoryRecord() != null
					&& userSession.getPfmTransactionSubcategoryRecord().getPfmTransactions() != null
					&& userSession.getPfmTransactionSubcategoryRecord().getPfmTransactions().size() > 0) {
				for (Iterator<PFMTransactionWrapper> iterator = userSession.getPfmTransactionSubcategoryRecord()
						.getPfmTransactions().iterator(); iterator.hasNext();) {
					wrapper = (PFMTransactionWrapper) iterator.next();
					if (wrapper.getTransactionId().equals(transactionId)) {
						model.put("transaction", wrapper);
						found = true;
						break;
					}
				}
			}
			// If we have not found the transaction, look in the regular
			// transaction record
			if (!found) {
				for (Iterator<PFMTransactionWrapper> iterator = userSession.getPfmTransactionRecord()
						.getPfmTransactions().iterator(); iterator.hasNext();) {
					wrapper = (PFMTransactionWrapper) iterator.next();
					if (wrapper.getTransactionId().equals(transactionId)) {
						model.put("transaction", wrapper);
						found = true;
						break;
					}
				}
			}
		} else
			model.put("transaction", "");

		// Get the tags to display (for the tooltip)
		String tooltipTags = "";
		if (found) {
			if (wrapper.getTags() != null && wrapper.getTags().length > 0) {
				boolean inEnglish = ("EN".equalsIgnoreCase(userSession.getLanguage()));
				for (Iterator<PFMTagWrapper> iterator = userSession.getPfmTags().iterator(); iterator.hasNext();) {
					PFMTagWrapper pfmTag = (PFMTagWrapper) iterator.next();
					for (int i = 0; i < wrapper.getTags().length; i++) {
						if (pfmTag.getTagId() == Integer.parseInt(wrapper.getTags()[i])) {
							if (inEnglish) {
								if (tooltipTags.equals(""))
									tooltipTags = pfmTag.getDescription_en();
								else
									tooltipTags += "," + pfmTag.getDescription_en();
							} else {
								if (tooltipTags.equals(""))
									tooltipTags = pfmTag.getDescription_es();
								else
									tooltipTags += "," + pfmTag.getDescription_es();
							}
						}
					}
				}
			}
		}
		model.put("tooltipTags", tooltipTags);

		// Do we still have a custom rule?
		model.put("customrule", findCustomRule(userSession, wrapper));

		return new ModelAndView("pfmResetTransaction", model);
	}

	public ModelAndView pfmResetTransactionDescription(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the transaction information...
		String transactionId = ServletRequestUtils.getStringParameter(request, "transactionId");
		PFMTransactionWrapper transaction = new PFMTransactionWrapper();

		// Find this transaction and post it to the model
		if (!transactionId.equals("undefined") && userSession.getPfmTransactionRecord() != null) {
			for (Iterator<PFMTransactionWrapper> iterator = userSession.getPfmTransactionRecord().getPfmTransactions()
					.iterator(); iterator.hasNext();) {
				transaction = (PFMTransactionWrapper) iterator.next();
				if (transaction.getTransactionId().equals(transactionId)) {
					break;
				}
			}
			// Send the transaction modification request
			customerService.resetPFMTransactionDescription(userSession.getPfmId(), userSession.getPfmAccounts(),
					transaction);
		}

		model.put("text", "");
		return new ModelAndView("text", model);
	}

	public ModelAndView pfmUpdateTransaction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the transaction information...
		String transactionId = ServletRequestUtils.getStringParameter(request, "transactionId");
		PFMTransactionWrapper transaction = new PFMTransactionWrapper();

		// Find this transaction...
		// First, look in the subcategory record
		boolean found = false;
		if (userSession.getPfmTransactionSubcategoryRecord() != null
				&& userSession.getPfmTransactionSubcategoryRecord().getPfmTransactions() != null
				&& userSession.getPfmTransactionSubcategoryRecord().getPfmTransactions().size() > 0) {
			for (Iterator<PFMTransactionWrapper> iterator = userSession.getPfmTransactionSubcategoryRecord()
					.getPfmTransactions().iterator(); iterator.hasNext();) {
				PFMTransactionWrapper wrapper = (PFMTransactionWrapper) iterator.next();
				if (wrapper.getTransactionId().equals(transactionId)) {
					transaction = wrapper;
					found = true;
					break;
				}
			}
		}
		// If we have not found the transaction, look in the regular transaction
		// record
		if (!found) {
			for (Iterator<PFMTransactionWrapper> iterator = userSession.getPfmTransactionRecord().getPfmTransactions()
					.iterator(); iterator.hasNext();) {
				PFMTransactionWrapper wrapper = (PFMTransactionWrapper) iterator.next();
				if (wrapper.getTransactionId().equals(transactionId)) {
					transaction = wrapper;
					found = true;
					break;
				}
			}
		}

		if (transaction.getTransactionId() != null) {
			String desc = ServletRequestUtils.getStringParameter(request, "desc");
			String tags = ServletRequestUtils.getStringParameter(request, "tags");
			String comment = ServletRequestUtils.getStringParameter(request, "comment");
			String[] existingTags = transaction.getTags();

			String tagsToAdd = "";
			String tagsToDel = "";

			// Create two strings, one for the tags to add, one for the tags to
			// delete
			if (tags != null) {
				found = false;
				String formTags[] = tags.split("\\|");
				if (existingTags == null || existingTags.length == 0) {
					tagsToAdd = tags;
				} else {
					for (int i = 0; i < formTags.length; i++) {
						// Loop through all the transaction's tags; if the
						// formTag exists, do not add it
						found = false;
						for (int j = 0; j < existingTags.length; j++) {
							if (existingTags[j].equals(formTags[i])) {
								found = true;
								break;
							}
						}
						if (!found) {
							tagsToAdd += formTags[i] + "|";
						}
					}

					// Now loop through all the existing tags...
					for (int i = 0; i < existingTags.length; i++) {
						found = false;
						for (int j = 0; j < formTags.length; j++) {
							if (existingTags[i].equals(formTags[j])) {
								found = true;
								break;
							}
						}
						if (!found) {
							tagsToDel += existingTags[i] + "|";
						}
					}
				}
			}

			// If the descriptions are different, add a {M} to the front in
			// order to verify that it has been changed
			if (!transaction.getDescription().equals(desc) || transaction.isDescriptionModified()) {
				desc = "{M}" + desc;
			}

			// Send the transaction modification request
			customerService.updatePFMTransaction(userSession.getPfmId(), userSession.getPfmAccounts(), transaction,
					desc, tagsToAdd, tagsToDel, comment);
			model.put("text", desc);
		}

		return new ModelAndView("text", model);
	}

	public ModelAndView pfmUpdateTransactionCategorySubcategory(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the transaction information...
		String transactionId = ServletRequestUtils.getStringParameter(request, "transactionId");
		String categoryId = ServletRequestUtils.getStringParameter(request, "categoryId");
		String subcategoryId = ServletRequestUtils.getStringParameter(request, "subcategoryId");
		PFMTransactionWrapper transaction = new PFMTransactionWrapper();
		PFMSubcategoryWrapper subcat = new PFMSubcategoryWrapper();
		boolean reloadTransactions = true;

		// Find the subcategory name...
		for (Iterator<PFMCategoryWrapper> iterator = userSession.getPfmCategories().iterator(); iterator.hasNext();) {
			PFMCategoryWrapper thisCategory = (PFMCategoryWrapper) iterator.next();
			if (thisCategory.getCategoryId() == Integer.parseInt(categoryId)) {
				for (Iterator<PFMSubcategoryWrapper> iterator2 = thisCategory.getSubcategories().iterator(); iterator2
						.hasNext();) {
					subcat = (PFMSubcategoryWrapper) iterator2.next();
					if (subcat.getCategoryId() == Integer.parseInt(subcategoryId))
						break;
				}
				break;
			}
		}

		// Find this transaction...
		PFMTransactionWrapper wrapper = new PFMTransactionWrapper();

		// First, look in the subcategory record
		boolean found = false;
		if (userSession.getPfmTransactionSubcategoryRecord() != null
				&& userSession.getPfmTransactionSubcategoryRecord().getPfmTransactions() != null
				&& userSession.getPfmTransactionSubcategoryRecord().getPfmTransactions().size() > 0) {
			for (Iterator<PFMTransactionWrapper> iterator = userSession.getPfmTransactionSubcategoryRecord()
					.getPfmTransactions().iterator(); iterator.hasNext();) {
				wrapper = (PFMTransactionWrapper) iterator.next();
				if (wrapper.getTransactionId().equals(transactionId)) {
					transaction = wrapper;
					found = true;
					break;
				}
			}
		}
		// If we have not found the transaction, look in the regular transaction
		// record
		if (!found) {
			for (Iterator<PFMTransactionWrapper> iterator = userSession.getPfmTransactionRecord().getPfmTransactions()
					.iterator(); iterator.hasNext();) {
				wrapper = (PFMTransactionWrapper) iterator.next();
				if (wrapper.getTransactionId().equals(transactionId)) {
					transaction = wrapper;
					found = true;
					break;
				}
			}
		}

		if (transaction.getTransactionId() != null) {

			// Are the categories the same? If they are, there's no need to
			// reload the transactions
			reloadTransactions = (transaction.getCategoryId() != Integer.parseInt(categoryId));

			// Send the transaction modification request
			customerService.updatePFMTransactionCategorySubcategory(userSession.getPfmId(),
					userSession.getPfmAccounts(), transaction, categoryId, subcategoryId);

			// Reset the custom rules so they reload next time they are needed
			userSession.setPfmCustomRules(null);

			// Find any custom rules that match this rule (in order to update
			// it)
			getPfmCustomRules(userSession);
			String foundRule = "FALSE";

			userSession.setPfmTransactionToUseAsCustomRuleOriginalSubcategory(transaction.getSubcategoryId());
			transaction.setCategoryId(Integer.parseInt(categoryId));
			transaction.setSubcategoryId(Integer.parseInt(subcategoryId));
			userSession.setPfmTransactionToUseAsCustomRule(transaction);

			// Checks and transfers automatically get the custom rule question
			// (no checking from custom rules)
			if (transaction.getTransactionDescShort().equals("CHEQUENUMERO")) {
				String checkTransferRuleStatus = findcheckTransferRuleStatus(userSession, transaction);
				foundRule = "CHECK" + checkTransferRuleStatus;
			} else if (transaction.getTransactionDescShort().equals("TTRANSFERENCIAAC")) {
				String checkTransferRuleStatus = findcheckTransferRuleStatus(userSession, transaction);
				foundRule = "TRANSFER" + checkTransferRuleStatus;
			} else {
				// Is there a rule that goes along with this transaction?
				if (userSession.getPfmCustomRules() != null && userSession.getPfmCustomRules().size() > 0) {
					for (Iterator<PFMCustomRuleWrapper> iterator2 = userSession.getPfmCustomRules().iterator(); iterator2
							.hasNext();) {
						PFMCustomRuleWrapper test = (PFMCustomRuleWrapper) iterator2.next();
						String wrapperAccount = (transaction.getAccountId().endsWith("DEP")) ? transaction
								.getAccountId().substring(0, 9) : transaction.getAccountId().substring(0, 16);
						// If the account id and the short name match...
						if (wrapperAccount.equals(test.getAccountNumber())
								&& transaction.getTransactionDescShort().equals(test.getTransactionShortDesc())) {
							// For other cases, a rule exists; now we only would
							// match a rule with a min of 0.00 and a max of
							// 999999999999.99
							if (test.getAmount().equals(new BigDecimal("0"))
									&& test.getAmountMax().setScale(2, BigDecimal.ROUND_UP)
											.equals(new BigDecimal("999999999999.99"))) {
								// If the rule's category and subcategory are
								// the same, we should not ask to add it
								if (test.getCategoryId() == Integer.parseInt(categoryId)
										&& test.getSubcategoryId() == Integer.parseInt(subcategoryId))
									foundRule = "SAME";
								else
									foundRule = "TRUE";
								break;
							}
						}
					}
				}
			}

			model.put(
					"text",
					reloadTransactions
							+ "|"
							+ foundRule
							+ "|"
							+ transaction.getDescriptionForCustomRule()
							+ "|"
							+ ("EN".equalsIgnoreCase(userSession.getLanguage()) ? subcat.getDescription_en() : subcat
									.getDescription_es())
							+ "|"
							+ transaction.getTransactionId()
							+ "|"
							+ categoryId
							+ "|"
							+ subcategoryId
							+ "|"
							+ (transaction.getDebitAmount().equals("0") ? new BigDecimal(transaction.getCreditAmount())
									.setScale(2, BigDecimal.ROUND_UP) : new BigDecimal(transaction.getDebitAmount())
									.setScale(2, BigDecimal.ROUND_UP)));
		}

		return new ModelAndView("text", model);
	}

	public ModelAndView pfmAccountDetailsTransactions(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		boolean thisMonth = false;

		// Get the account
		String frontendid = ServletRequestUtils.getStringParameter(request, "frontendid");
		model.put("frontendid", frontendid);
		TVFrontendAccount tvaccount = Utils.getAccountById(frontendid, userSession.getAccounts());
		if (tvaccount != null && !GenericValidator.isBlankOrNull(tvaccount.getSubtype())
				&& tvaccount.getSubtype().equals("CCA")) {
			model.put("selectedAccount", "CCA");
		}
		String account = tvaccount.getAccountNumber();

		// Should we draw the entire display or just update the transactions
		// list?
		String firstTime = ServletRequestUtils.getStringParameter(request, "firstTime");

		// Paging
		int newPaging = ServletRequestUtils.getIntParameter(request, "newPaging");
		int newPage = ServletRequestUtils.getIntParameter(request, "newPage");

		// Get the month (format it for beginning and end)
		String month = ServletRequestUtils.getStringParameter(request, "month");
		String startDate = "";
		String endDate = "";

		if (month.equals("0")) {
			thisMonth = true;
			Calendar cal = Calendar.getInstance();
			startDate = cal.get(Calendar.YEAR) + "-" + ((cal.get(Calendar.MONTH) + 1) < 10 ? "0" : "")
					+ (cal.get(Calendar.MONTH) + 1) + "-01";
			int maxday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			endDate = cal.get(Calendar.YEAR) + "-" + ((cal.get(Calendar.MONTH) + 1) < 10 ? "0" : "")
					+ (cal.get(Calendar.MONTH) + 1) + "-" + String.valueOf(maxday);
			;
		} else {
			// Find the last day of the end month
			Calendar cal = Calendar.getInstance();
			cal.set(Integer.parseInt(month.substring(3)), Integer.parseInt(month.substring(0, 2)) - 1, 1);
			int maxday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

			// Format the dates
			startDate = month.substring(3) + "-" + month.substring(0, 2) + "-01";
			endDate = month.substring(3) + "-" + month.substring(0, 2) + "-" + String.valueOf(maxday);

			// Are we searching for the current month?
			Calendar testToday = Calendar.getInstance();
			String testString = cal.get(Calendar.YEAR) + "-" + ((cal.get(Calendar.MONTH) + 1) < 10 ? "0" : "")
					+ (cal.get(Calendar.MONTH) + 1) + "-01";
			if (testString.equals(startDate))
				thisMonth = true;
		}

		// Send the request for transactions
		// Current page and paging
		int page = 1;
		int paging = 50;
		if (userSession.getPfmPaging() == null) {
			userSession.setPfmPaging(Integer.toString(paging));
		} else {
			paging = Integer.parseInt(userSession.getPfmPaging());
		}

		if (newPaging > 0) {
			page = 1;
			paging = newPaging;
			userSession.setPfmPaging(Integer.toString(paging));
		}

		if (newPage > 0) {
			page = newPage;
		}

		// Get the tag and category
		String tagId = ServletRequestUtils.getStringParameter(request, "tagId");
		String categoryId = ServletRequestUtils.getStringParameter(request, "categoryId");

		// Get the sorting
		String sortOrderString = ServletRequestUtils.getStringParameter(request, "sortOrder");

		// Send the request for accounts, categories and tags
		getPfmUserId(userSession);
		getPfmAccounts(userSession);
		getPfmCategories(userSession);
		getPfmTags(userSession);

		// Find this account and simply send it
		PFMAccountWrapper pfmAccount = new PFMAccountWrapper();
		for (Iterator<PFMAccountWrapper> iterator = userSession.getPfmAccounts().iterator(); iterator.hasNext();) {
			PFMAccountWrapper test = (PFMAccountWrapper) iterator.next();
			if (test.getAccountId().endsWith("CARD")) {
				if (test.getAccountId().substring(0, 16).equals(account)) {
					pfmAccount = test;
					break;
				}
			} else {
				if (test.getAccountId().substring(0, 9).equals(account)) {
					pfmAccount = test;
					// Checking, savings or reserve?
					if (tvaccount.getAccountSection() != null && tvaccount.getAccountSection().equals("S"))
						pfmAccount.setSection("S");
					else if (tvaccount.getAccountSection() != null && tvaccount.getAccountSection().equals("R"))
						pfmAccount.setSection("R");
					else
						pfmAccount.setSection("C");
					break;
				}
			}
		}

		// If the account exist, get the transactions
		if (pfmAccount != null && pfmAccount.getAccountId() != null) {

			// If the account is IDA and we are looking at the current month,
			// get its in-process transactions
			if (pfmAccount.getAccountId().endsWith("DEP") && thisMonth) {
				if (userSession.getDisplayTransactionDowntime()) {
					model.put("transactiondowntime", "true");
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[1]);
					} else {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[0]);
					}
				} else {

					List<AbstractTransaction> inprocess = new LinkedList<AbstractTransaction>();
					List<AbstractTransaction> transactions = new LinkedList<AbstractTransaction>();
					BigDecimal totalInprocess = new BigDecimal(0.0d);
					RunningBalanceTransaction name = null;
					BigDecimal availBal = null;

					// Checking, savings or reserve?
					for (Iterator<TVFrontendAccount> iterator = userSession.getAccounts().iterator(); iterator
							.hasNext();) {
						tvaccount = (TVFrontendAccount) iterator.next();
						if (tvaccount.getAccountNumber().equals(account)) {
							if (pfmAccount.getSection().equals("S") && tvaccount.getAccountSection() != null
									&& tvaccount.getAccountSection().equals("S"))
								break;
							else if (pfmAccount.getSection().equals("R") && tvaccount.getAccountSection() != null
									&& tvaccount.getAccountSection().equals("R"))
								break;
							if (pfmAccount.getSection().equals("C") && tvaccount.getAccountSection() != null
									&& tvaccount.getAccountSection().equals("C"))
								break;
						}
					}

					if (tvaccount.getProductDefinition().getProductType().equals("IDA")
							|| tvaccount.getProductDefinition().getProductType().equals("PFC")) {

						TransactionRequest depreq2 = new TransactionRequest(
								Utils.generateCustomerInteractionEvent(request), null,
								userSession.getCustomerProfile(), userSession.getBankingSession(), tvaccount, 100, 1,
								true, null, null, null);
						TransactionsResponse transactionResponse = customerService
								.getDepositCurrentTransactions(depreq2);

						if (transactionResponse.getStatusResult() == ResultStatus.SUCCESS)
							transactions.addAll(transactionResponse.getTransactions());

						for (Iterator<AbstractTransaction> iterator = transactions.iterator(); iterator.hasNext();) {
							name = (RunningBalanceTransaction) iterator.next();
							if (name.getDate() == null
									|| name.getDescription().toUpperCase().indexOf("IN PROCESS") != -1) {
								inprocess.add(name);
								if (name.getType().equals("CR"))
									totalInprocess = totalInprocess.add(name.getAmount());
								else
									totalInprocess = totalInprocess.subtract(name.getAmount());
							}
						}

						if (inprocess != null && inprocess.size() > 0) {
							transactions.removeAll(inprocess);
							model.put("inprocess", inprocess);
							if (totalInprocess.doubleValue() >= 0.0d)
								model.put("inprocessType", "CR");
							else
								model.put("inprocessType", "DB");
							model.put("totalInprocess", totalInprocess.abs());
						}

						if (tvaccount != null)
							availBal = tvaccount.getPortalBalance();

						if (availBal != null
								&& name != null
								&& name.getBalance() != null
								&& ((availBal.abs().add(name.getBalance().abs().negate()).abs().doubleValue()) >= 0.01d)) {
							RunningBalanceTransaction other = new RunningBalanceTransaction();
							other.setDescription("IN PROCESS OTHER TRANSACTIONS");
							BigDecimal difamt = availBal.add(name.getBalance().negate());
							other.setAmount(difamt.abs());
							if (difamt.doubleValue() >= 0.01d)
								other.setType("CR");
							else
								other.setType("DB");
							inprocess.add(other);
							if (other.getType().equals("CR"))
								totalInprocess = totalInprocess.add(other.getAmount());
							else
								totalInprocess = totalInprocess.subtract(other.getAmount());
							model.put("inprocess", inprocess);
							model.put("totalInprocess", totalInprocess.abs());
							if (totalInprocess.doubleValue() >= 0.0d)
								model.put("inprocessType", "CR");
							else
								model.put("inprocessType", "DB");

						}
						model.put("availableBalance", tvaccount.getPortalBalance());
					}
				}
			}
			PFMSortOrderWrapper sortOrder = new PFMSortOrderWrapper();
			if (sortOrderString == null || (sortOrderString != null && sortOrderString.equals("")))
				sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
			else if ((sortOrderString != null && sortOrderString.equals("0,0")))
				sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_ASCENDING);
			else if ((sortOrderString != null && sortOrderString.equals("0,1")))
				sortOrder.setSortOrder(PFMSortOrderWrapper.DATE_DESCENDING);
			else if ((sortOrderString != null && (sortOrderString.equals("4,0") || sortOrderString.equals("3,0"))))
				sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_DESCENDING);
			else if ((sortOrderString != null && (sortOrderString.equals("4,1") || sortOrderString.equals("3,1"))))
				sortOrder.setSortOrder(PFMSortOrderWrapper.AMOUNT_ASCENDING);

			PFMTransactionRecordWrapper pfmTransactionRecord = customerService.getPfmTransactionsByAccount(
					userSession.getPfmId(), pfmAccount, tagId, categoryId, startDate, endDate, page, paging, sortOrder);
			userSession.setPfmTransactionRecord(pfmTransactionRecord);

			// If we have set up a tv account, it means these transactions may
			// have check images...
			// Set up the front end id in order to view the check images
			if (pfmTransactionRecord != null && pfmTransactionRecord.getPfmTransactions() != null
					&& pfmTransactionRecord.getPfmTransactions().size() > 0 && tvaccount != null) {
				for (int i = 0; i < pfmTransactionRecord.getPfmTransactions().size(); i++) {
					pfmTransactionRecord.getPfmTransactions().get(i).setFrontendId(tvaccount.getFrontEndId());
				}
			}

			// Do these transactions have custom rules?
			if (pfmTransactionRecord != null) {
				if (pfmTransactionRecord.getPfmTransactions() != null
						&& pfmTransactionRecord.getPfmTransactions().size() > 0) {
					for (int index2 = 0; index2 < pfmTransactionRecord.getPfmTransactions().size(); index2++) {
						pfmTransactionRecord
								.getPfmTransactions()
								.get(index2)
								.setHasCustomRule(
										findCustomRule(userSession,
												pfmTransactionRecord.getPfmTransactions().get(index2)));
					}
				}
			}

			// Show the transactions
			if (pfmTransactionRecord != null) {
				if (pfmTransactionRecord.getPfmTransactions() != null
						&& pfmTransactionRecord.getPfmTransactions().size() > 0) {

					int startnumber = 1 + ((pfmTransactionRecord.getCurrentPage() - 1) * pfmTransactionRecord
							.getPagingRecords());
					int endnumber = pfmTransactionRecord.getCurrentPage() * pfmTransactionRecord.getPagingRecords();
					if (endnumber > pfmTransactionRecord.getTotalRecords())
						endnumber = pfmTransactionRecord.getTotalRecords();

					userSession.setCheckImageMap(pfmTransactionRecord.getImageKeyMap());
					userSession.getCheckImageMap();

					model.put("transactions", pfmTransactionRecord.getPfmTransactions());
					model.put("currentpage", pfmTransactionRecord.getCurrentPage());
					model.put("nextpage", pfmTransactionRecord.getCurrentPage() + 1);
					model.put("prevpage", pfmTransactionRecord.getCurrentPage() - 1);
					model.put("totalrecords", pfmTransactionRecord.getTotalRecords());
					model.put("pagesize", pfmTransactionRecord.getPagingRecords());
					model.put("startnumber", startnumber);
					model.put("endnumber", endnumber);
				} else {
					userSession.setPfmTransactionRecord(null);
					model.put("transactions", null);
					model.put("currentpage", "1");
					model.put("nextpage", "2");
					model.put("prevpage", "0");
					model.put("totalrecords", "0");
					model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
					model.put("startnumber", "0");
					model.put("endnumber", "0");
				}
			} else {
				userSession.setPfmTransactionRecord(null);
				model.put("transactions", null);
				model.put("currentpage", "1");
				model.put("nextpage", "2");
				model.put("prevpage", "0");
				model.put("totalrecords", "0");
				model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
				model.put("startnumber", "0");
				model.put("endnumber", "0");
			}
		} else {
			userSession.setPfmTransactionRecord(null);
			model.put("transactions", null);
			model.put("currentpage", "1");
			model.put("nextpage", "2");
			model.put("prevpage", "0");
			model.put("totalrecords", "0");
			model.put("pagesize", (userSession.getPfmPaging() == null ? paging : userSession.getPfmPaging()));
			model.put("startnumber", "0");
			model.put("endnumber", "0");
		}

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");

		model.put("tags", userSession.getPfmTags());

		// Null out the subcategory record in order to edit the correct trx
		userSession.setPfmTransactionSubcategoryRecord(null);

		if (firstTime.equals("true")) {
			model.put("categoryId", categoryId);
			model.put("tagId", tagId);

			model.put("categories", userSession.getPfmCategories());
			model.put("tags", userSession.getPfmTags());

			// For the transaction calendar
			Date[] past18Months = new Date[18];
			Calendar pfmcal = Calendar.getInstance();
			past18Months[0] = pfmcal.getTime();
			model.put("past18Months_0", past18Months[0]);

			for (int i = 1; i < 18; i++) {
				pfmcal.add(Calendar.MONTH, -1);
				past18Months[i] = pfmcal.getTime();
				model.put("past18Months_" + i, past18Months[i]);
			}
			model.put("accountnumber", account);
			return new ModelAndView("pfmAccountDetailsTransactions", model);
		} else {
			model.put("showaccounts", "false");
			return new ModelAndView("pfmTransactions", model);
		}
	}

	// Export the transactions
	public ModelAndView pfmExport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the account
		String frontendid = ServletRequestUtils.getStringParameter(request, "frontendid");
		TVFrontendAccount tvaccount = Utils.getAccountById(frontendid, userSession.getAccounts());
		String account = tvaccount.getAccountNumber();

		// Get the month (format it for beginning and end)
		String month = ServletRequestUtils.getStringParameter(request, "month");
		String startDate = "";
		String endDate = "";

		if (month.equals("0")) {
			Calendar cal = Calendar.getInstance();
			startDate = cal.get(Calendar.YEAR) + "-" + ((cal.get(Calendar.MONTH) + 1) < 10 ? "0" : "")
					+ (cal.get(Calendar.MONTH) + 1) + "-01";
			int maxday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			endDate = cal.get(Calendar.YEAR) + "-" + ((cal.get(Calendar.MONTH) + 1) < 10 ? "0" : "")
					+ (cal.get(Calendar.MONTH) + 1) + "-" + String.valueOf(maxday);
			;
		} else {
			// Find the last day of the end month
			Calendar cal = Calendar.getInstance();
			cal.set(Integer.parseInt(month.substring(3)), Integer.parseInt(month.substring(0, 2)) - 1, 1);
			int maxday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

			// Format the dates
			startDate = month.substring(3) + "-" + month.substring(0, 2) + "-01";
			endDate = month.substring(3) + "-" + month.substring(0, 2) + "-" + String.valueOf(maxday);
		}

		// Get the tag and category
		String tagId = ServletRequestUtils.getStringParameter(request, "tagId");
		String categoryId = ServletRequestUtils.getStringParameter(request, "categoryId");

		// Find this account and simply send it
		PFMAccountWrapper pfmAccount = new PFMAccountWrapper();
		for (Iterator<PFMAccountWrapper> iterator = userSession.getPfmAccounts().iterator(); iterator.hasNext();) {
			PFMAccountWrapper test = (PFMAccountWrapper) iterator.next();
			if (test.getAccountId().endsWith("CARD")) {
				if (test.getAccountId().substring(0, 16).equals(account)) {
					pfmAccount = test;
					break;
				}
			} else {
				if (test.getAccountId().substring(0, 9).equals(account)) {
					pfmAccount = test;
					// Checking, savings or reserve?
					if (tvaccount.getAccountSection() != null && tvaccount.getAccountSection().equals("S"))
						pfmAccount.setSection("S");
					else if (tvaccount.getAccountSection() != null && tvaccount.getAccountSection().equals("R"))
						pfmAccount.setSection("R");
					else
						pfmAccount.setSection("C");
					break;
				}
			}
		}

		// If the account exist, get the transactions
		if (pfmAccount != null) {

			// If we currently have all the transactions visible, simply export
			// them
			PFMTransactionRecordWrapper pfmTransactionRecord = userSession.getPfmTransactionRecord();
			model.put("record", pfmTransactionRecord);
			model.put("pfmAccount", pfmAccount);
			model.put("startDate", startDate);
			model.put("endDate", endDate);
			model.put("tagId", tagId);
			model.put("categoryId", categoryId);
			model.put("userSession", userSession);
			model.put("customerService", customerService);
		}

		return new ModelAndView(new PfmCsvFileView(), model);
	}

	public ModelAndView pfmSaveAccountConfiguration(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<PFMAccountWrapper> pfmAccountsToUpdate = new LinkedList<PFMAccountWrapper>();

		// Get the list of account status
		String accountIds = ServletRequestUtils.getStringParameter(request, "accountIds");

		// Parse this list into an array
		String[] activeAccounts = accountIds.split("\\|");

		// Loop through the session's accounts and determine which ones to send
		// to PFM (should only send the ones that don't match)
		int index = 0;
		for (Iterator<PFMAccountWrapper> iterator = userSession.getPfmAccounts().iterator(); iterator.hasNext();) {
			PFMAccountWrapper pfmAccount = (PFMAccountWrapper) iterator.next();
			if (pfmAccount.isIncludeFlag() && activeAccounts[index].equals("false")
					|| (!pfmAccount.isIncludeFlag() && activeAccounts[index].equals("true"))) {

				pfmAccount.setIncludeFlag(!pfmAccount.isIncludeFlag());
				pfmAccountsToUpdate.add(pfmAccount);
			}
			index++;
		}

		// Send this list to PFM
		if (pfmAccountsToUpdate.size() > 0) {
			if (customerService.updatePFMAccounts(userSession.getPfmId(), pfmAccountsToUpdate)) {
				// Update the session values
				for (Iterator<PFMAccountWrapper> iterator = pfmAccountsToUpdate.iterator(); iterator.hasNext();) {
					PFMAccountWrapper pfmAccount = (PFMAccountWrapper) iterator.next();
					userSession.getPfmAccounts().get(pfmAccount.getIndex()).setIncludeFlag(pfmAccount.isIncludeFlag());
				}
			}
		}

		// Update the account popup box
		model.put("accounts", userSession.getPfmAccounts());
		return new ModelAndView("pfmAccounts", model);
	}

	public ModelAndView pfmSaveCategoryConfiguration(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the list of category status
		String catIds = ServletRequestUtils.getStringParameter(request, "catIds");

		// Parse this list into an array
		String[] activeCats = catIds.split("\\|");

		// Loop through the session's categories and determine which ones to
		// alter
		int index = 0;
		for (Iterator<PFMCategoryWrapper> iterator = userSession.getPfmCategories().iterator(); iterator.hasNext();) {
			PFMCategoryWrapper pfmCategory = (PFMCategoryWrapper) iterator.next();
			// Always ignore credit
			if (pfmCategory.getCategoryId() != INCOME_CATEGORY_ID) {
				if (pfmCategory.isIncludeFlag() && activeCats[index].equals("false")
						|| (!pfmCategory.isIncludeFlag() && activeCats[index].equals("true"))) {

					userSession.getPfmCategories().get(index).setIncludeFlag(activeCats[index].equals("true"));
				}
			}
			index++;
		}

		// Update the categories popup box
		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");
		model.put("categories", userSession.getPfmCategories());
		return new ModelAndView("pfmCategories", model);
	}

	public ModelAndView pfmSaveCreditSubcategoryConfiguration(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the list of category status
		String catIds = ServletRequestUtils.getStringParameter(request, "catIds");

		// Parse this list into an array
		String[] activeCats = catIds.split("\\|");

		// Loop through the session's categories and determine which ones to
		// alter
		int index = 0;
		for (Iterator<PFMCategoryWrapper> iterator = userSession.getPfmCategories().iterator(); iterator.hasNext();) {
			PFMCategoryWrapper pfmCategory = (PFMCategoryWrapper) iterator.next();
			// Only select credit
			if (pfmCategory.getCategoryId() == INCOME_CATEGORY_ID) {
				int index2 = 0;
				for (Iterator<PFMSubcategoryWrapper> iterator2 = pfmCategory.getSubcategories().iterator(); iterator2
						.hasNext();) {
					PFMSubcategoryWrapper subcategory = (PFMSubcategoryWrapper) iterator2.next();
					if (subcategory.isIncludeFlag() && activeCats[index2].equals("false")
							|| (!subcategory.isIncludeFlag() && activeCats[index2].equals("true"))) {

						userSession.getPfmCategories().get(index).getSubcategories().get(index2)
								.setIncludeFlag(activeCats[index2].equals("true"));
					}
					index2++;
				}
				break;
			}
			index++;
		}

		// Update the categories popup box
		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");
		model.put("category", userSession.getPfmCategories().get(index));
		return new ModelAndView("pfmCreditSubcategories", model);
	}

	public ModelAndView pfmSaveTagConfiguration(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the list of tag status
		String tagIds = ServletRequestUtils.getStringParameter(request, "tagIds");

		// Parse this list into an array
		String[] activeTags = tagIds.split("\\|");

		// Loop through the session's tags and determine which ones to alter
		int index = 0;
		for (Iterator<PFMTagWrapper> iterator = userSession.getPfmTags().iterator(); iterator.hasNext();) {
			PFMTagWrapper pfmTag = (PFMTagWrapper) iterator.next();
			if (pfmTag.isIncludeFlag() && activeTags[index].equals("false")
					|| (!pfmTag.isIncludeFlag() && activeTags[index].equals("true"))) {

				userSession.getPfmTags().get(index).setIncludeFlag(activeTags[index].equals("true"));
			}
			index++;
		}

		// Update the tags popup box
		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");
		model.put("tags", userSession.getPfmTags());
		return new ModelAndView("pfmTags", model);
	}

	public ModelAndView pfmSaveNewSubcategory(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the subcategory and category id
		String newSubCat = ServletRequestUtils.getStringParameter(request, "newSubCat").trim();
		int categoryId = ServletRequestUtils.getIntParameter(request, "categoryId");

		// Send this new category to PFM to add
		int newSubcategoryId = customerService.saveNewPFMSubcategory(userSession.getPfmId(), newSubCat, categoryId);

		if (newSubcategoryId > 0)
			userSession.setPfmCategories(null);

		// Refresh the categories
		getPfmCategories(userSession);
		model.put("text", "OK");

		return new ModelAndView("text", model);
	}

	public ModelAndView pfmUpdateSubcategory(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the subcategory and subcategory id
		String newSubCat = ServletRequestUtils.getStringParameter(request, "newSubCat").trim();
		int subcategoryId = ServletRequestUtils.getIntParameter(request, "subcategoryId");

		// Send this new category to PFM to add
		if (customerService.updatePFMSubcategory(userSession.getPfmId(), newSubCat, subcategoryId)) {
			userSession.setPfmCategories(null);
			getPfmCategories(userSession);
			model.put("text", "OK");
		}

		return new ModelAndView("text", model);

	}

	public ModelAndView pfmDeleteSubcategory(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the subcategory id
		int subcategoryId = ServletRequestUtils.getIntParameter(request, "subcategoryId");

		// Send this new category to PFM to delete
		if (customerService.deletePFMSubcategory(userSession.getPfmId(), subcategoryId)) {
			userSession.setPfmCategories(null);
			getPfmCategories(userSession);
			model.put("text", "OK");
		}

		return new ModelAndView("text", model);
	}

	public ModelAndView pfmUpdateSubcategoryPopup(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the category id from the request
		int categoryId = ServletRequestUtils.getIntParameter(request, "categoryId");

		for (Iterator<PFMCategoryWrapper> iterator = userSession.getPfmCategories().iterator(); iterator.hasNext();) {
			PFMCategoryWrapper category = (PFMCategoryWrapper) iterator.next();
			if (category.getCategoryId() == categoryId) {
				model.put("category", category);
				break;
			}
		}

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");
		return new ModelAndView("pfmUpdateSubcategoryPopup", model);
	}

	public ModelAndView pfmSaveNewTag(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the tag
		String newTag = ServletRequestUtils.getStringParameter(request, "newTag").trim();

		// Send this new category to PFM to add
		int newTagId = customerService.saveNewPFMTag(userSession.getPfmId(), newTag);

		// Refresh the tags
		userSession.setPfmTags(null);
		getPfmTags(userSession);

		model.put("tags", userSession.getPfmTags());
		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");
		return new ModelAndView("pfmSaveNewTag", model);
	}

	public ModelAndView pfmUpdateTag(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the tag and tag id
		String newTag = ServletRequestUtils.getStringParameter(request, "newTag").trim();
		String tagIdString = ServletRequestUtils.getStringParameter(request, "tagId");
		int tagId = 0;

		if (tagIdString.startsWith("tag-"))
			tagId = Integer.parseInt(tagIdString.substring(4));
		else
			tagId = Integer.parseInt(tagIdString);

		// Send this tag to PFM to update
		if (customerService.updatePFMTag(userSession.getPfmId(), newTag, tagId)) {
			userSession.setPfmTags(null);
			getPfmTags(userSession);
			model.put("text", "OK");
		}

		return new ModelAndView("text", model);

	}

	public ModelAndView pfmDeleteTag(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the tag id
		int tagId = ServletRequestUtils.getIntParameter(request, "tagId");

		// Send this new tag to PFM to delete
		if (customerService.deletePFMTag(userSession.getPfmId(), tagId)) {
			userSession.setPfmTags(null);
			getPfmTags(userSession);
			model.put("text", "OK");
		}

		return new ModelAndView("text", model);
	}

	public ModelAndView pfmUpdateTagPopup(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Get the transaction information...
		String transactionId = userSession.getPfmTransactionIdToEdit();

		if (transactionId != null) {
			// Find this transaction and post it to the model
			for (Iterator<PFMTransactionWrapper> iterator = userSession.getPfmTransactionRecord().getPfmTransactions()
					.iterator(); iterator.hasNext();) {
				PFMTransactionWrapper wrapper = (PFMTransactionWrapper) iterator.next();
				if (wrapper.getTransactionId().equals(transactionId)) {
					model.put("transaction", wrapper);
					break;
				}
			}
		}

		if ("EN".equalsIgnoreCase(userSession.getLanguage()))
			model.put("lang", "en");
		else
			model.put("lang", "es");
		model.put("tags", userSession.getPfmTags());
		return new ModelAndView("pfmUpdateTagPopup", model);
	}

	public ModelAndView pfmAddCustomRule(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String categoryId = ServletRequestUtils.getStringParameter(request, "categoryId");
		String subcategoryId = ServletRequestUtils.getStringParameter(request, "subcategoryId");

		// Find this transaction and send it to PFM
		PFMTransactionWrapper wrapper = userSession.getPfmTransactionToUseAsCustomRule();
		String originalSubcategoryId = Integer.toString(userSession
				.getPfmTransactionToUseAsCustomRuleOriginalSubcategory());

		if (originalSubcategoryId == null)
			originalSubcategoryId = "0";

		if (wrapper != null) {
			// If this is a check or transfer...
			if (wrapper.getTransactionDescShort().equals("CHEQUENUMERO")
					|| wrapper.getTransactionDescShort().equals("TTRANSFERENCIAAC")) {
				PFMCustomRuleWrapper customRule = findEntireCustomRule(userSession, wrapper, true);
				// The rule does not exist; add it
				if (customRule == null) {
					if (customerService.addPFMCustomRule(userSession.getPfmId(), wrapper, userSession.getPfmAccounts(),
							categoryId, subcategoryId, originalSubcategoryId)) {
						userSession.setPfmCustomRules(null);
						getPfmCustomRules(userSession);
						model.put("text", "OK");
					} else
						model.put("text", "OK");
				}
				// The rule already exists; modify it
				else {
					if (customerService.updatePFMCustomRule(userSession.getPfmId(), customRule, categoryId,
							subcategoryId)) {
						userSession.setPfmCustomRules(null);
						getPfmCustomRules(userSession);
						model.put("text", "OK");
					} else
						model.put("text", "OK");
				}
			} else {
				if (customerService.addPFMCustomRule(userSession.getPfmId(), wrapper, userSession.getPfmAccounts(),
						categoryId, subcategoryId, originalSubcategoryId)) {
					userSession.setPfmCustomRules(null);
					getPfmCustomRules(userSession);
					model.put("text", "OK");
				} else
					model.put("text", "OK");
			}
		} else
			model.put("text", "OK");

		return new ModelAndView("text", model);
	}

	public ModelAndView pfmDeleteCustomRule(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String ruleIndex = ServletRequestUtils.getStringParameter(request, "ruleIndex");

		// Send this rule to PFM to delete
		if (ruleIndex != null) {
			PFMCustomRuleWrapper rule = new PFMCustomRuleWrapper();
			for (int i = 0; i < userSession.getPfmCustomRules().size(); i++) {
				if (userSession.getPfmCustomRules().get(i).getRuleId().equals(ruleIndex)) {
					rule = userSession.getPfmCustomRules().get(i);
					if (customerService.deletePFMCustomRule(userSession.getPfmId(), rule)) {
						int paging = (userSession.getPfmCustomRulePaging() == null) ? 10 : Integer.parseInt(userSession
								.getPfmCustomRulePaging());
						int page = (i / paging) + 1;
						userSession.setPfmCustomRules(null);
						getPfmCustomRules(userSession);
						model.put("text", page);

						// Do we need to update or delete this custom rule in
						// the parent's (edit transaction) window?
						if (rule.getRuleId().equals(Integer.toString(userSession.getPfmCurrentRuleId()))) {
							userSession.setPfmUpdateCurrentRule(false);
							userSession.setPfmDeleteCurrentRule(true);
						}
					}
				}
			}

		}

		return new ModelAndView("text", model);
	}

	public ModelAndView pfmUpdateCustomRule(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String ruleIndex = ServletRequestUtils.getStringParameter(request, "ruleIndex");
		String amount = ServletRequestUtils.getStringParameter(request, "amount");

		// Send this rule to PFM to update
		if (ruleIndex != null) {
			PFMCustomRuleWrapper rule = new PFMCustomRuleWrapper();
			for (int i = 0; i < userSession.getPfmCustomRules().size(); i++) {
				if (userSession.getPfmCustomRules().get(i).getRuleId().equals(ruleIndex)) {
					rule = userSession.getPfmCustomRules().get(i);
					if (customerService.updatePFMCustomRuleAmount(userSession.getPfmId(), rule, amount)) {
						int paging = (userSession.getPfmCustomRulePaging() == null) ? 10 : Integer.parseInt(userSession
								.getPfmCustomRulePaging());
						int page = (i / paging) + 1;
						userSession.setPfmCustomRules(null);
						getPfmCustomRules(userSession);
						model.put("text", page);
					}
					break;
				}
			}
		}

		return new ModelAndView("text", model);
	}

	public ModelAndView pfmUpdateRuleCategorySubcategory(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String transactionId = ServletRequestUtils.getStringParameter(request, "transactionId");
		String categoryId = ServletRequestUtils.getStringParameter(request, "categoryId");
		String subcategoryId = ServletRequestUtils.getStringParameter(request, "subcategoryId");
		String ruleIndex = ServletRequestUtils.getStringParameter(request, "ruleIndex");

		int ruleId = userSession.getPfmCurrentRuleId();

		if (transactionId != null) {
			// Find the rule
			boolean ruleFound = false;
			getPfmCustomRules(userSession);
			PFMCustomRuleWrapper rule = new PFMCustomRuleWrapper();

			if (ruleId > 0) {
				if (userSession.getPfmCustomRules() != null) {
					for (Iterator<PFMCustomRuleWrapper> riterator = userSession.getPfmCustomRules().iterator(); riterator
							.hasNext();) {
						rule = (PFMCustomRuleWrapper) riterator.next();
						if (rule.getRuleId().equals(Integer.toString(ruleId))) {
							ruleFound = true;
							break;
						}
					}
				}
			}

			if (ruleFound) {
				// Find this transaction and get its category/subcategory
				if (customerService.updatePFMCustomRule(userSession.getPfmId(), rule, categoryId, subcategoryId)) {
					userSession.setPfmCustomRules(null);
					getPfmCustomRules(userSession);
					model.put("text", "OK");
				}
			} else {
				PFMTransactionWrapper wrapper = userSession.getPfmTransactionToUseAsCustomRule();
				PFMCustomRuleWrapper customRule = findEntireCustomRule(userSession, wrapper, true);
				if (customRule != null) {
					if (customerService.updatePFMCustomRule(userSession.getPfmId(), customRule, categoryId,
							subcategoryId)) {
						userSession.setPfmCustomRules(null);
						getPfmCustomRules(userSession);
						model.put("text", "OK");
					}
				}
			}
		} else {
			if (ruleIndex != null) {
				PFMCustomRuleWrapper rule = new PFMCustomRuleWrapper();
				for (int i = 0; i < userSession.getPfmCustomRules().size(); i++) {
					if (userSession.getPfmCustomRules().get(i).getRuleId().equals(ruleIndex)) {
						rule = userSession.getPfmCustomRules().get(i);

						if (customerService
								.updatePFMCustomRule(userSession.getPfmId(), rule, categoryId, subcategoryId)) {
							userSession.setPfmCustomRules(null);
							getPfmCustomRules(userSession);
							model.put("text", "OK");
						}
						break;
					}
				}
			}
		}

		return new ModelAndView("text", model);
	}

	public ModelAndView pfmUpdateParentWindowRule(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		if (!userSession.isPfmDeleteCurrentRule() && !userSession.isPfmUpdateCurrentRule()) {
			model.put("text", "OK");
		} else if (userSession.isPfmDeleteCurrentRule()) {
			model.put("text", " ");
			userSession.setPfmCurrentRuleId(0);
			userSession.setPfmUpdateCurrentRule(false);
			userSession.setPfmDeleteCurrentRule(false);
		} else if (userSession.isPfmUpdateCurrentRule()) {
			if (userSession.getPfmCustomRules() != null && userSession.getPfmCustomRules().size() > 0) {
				PFMCustomRuleWrapper test = new PFMCustomRuleWrapper();
				for (Iterator<PFMCustomRuleWrapper> iterator2 = userSession.getPfmCustomRules().iterator(); iterator2
						.hasNext();) {
					test = (PFMCustomRuleWrapper) iterator2.next();
					if (test.getRuleId().equals(Integer.toString(userSession.getPfmCurrentRuleId()))) {
						if ("EN".equalsIgnoreCase(userSession.getLanguage()))
							model.put("text", "Categorize as: " + test.getSubcategory_description_en());
						else
							model.put("text", "Categorizar como: " + test.getSubcategory_description_es());
						break;
					}
				}
				userSession.setPfmUpdateCurrentRule(false);
				userSession.setPfmDeleteCurrentRule(false);
			}
		}

		return new ModelAndView("text", model);
	}

	public ModelAndView pfmSetupCategoryIdToHighlight(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String transactionId = ServletRequestUtils.getStringParameter(request, "trxId");

		// Loop through all the transactions and get this one's category id
		int catIdToHighlight = 0;
		boolean found = false;
		for (Iterator<PFMTransactionWrapper> iterator = userSession.getPfmTransactionRecord().getPfmTransactions()
				.iterator(); iterator.hasNext();) {
			PFMTransactionWrapper trx = (PFMTransactionWrapper) iterator.next();
			if (trx.getTransactionId().equals(transactionId)) {
				catIdToHighlight = trx.getCategoryId();
				found = true;
				break;
			}
		}

		// If the trx was not found, loop through the subcategory record
		// transactions
		if (!found) {
			if (userSession.getPfmTransactionSubcategoryRecord() != null
					&& userSession.getPfmTransactionSubcategoryRecord().getPfmTransactions() != null) {
				for (Iterator<PFMTransactionWrapper> iterator = userSession.getPfmTransactionSubcategoryRecord()
						.getPfmTransactions().iterator(); iterator.hasNext();) {
					PFMTransactionWrapper trx = (PFMTransactionWrapper) iterator.next();
					if (trx.getTransactionId().equals(transactionId)) {
						catIdToHighlight = trx.getCategoryId();
						found = true;
						break;
					}
				}
			}
		}

		if (found)
			userSession.setPfmCategoryIdToHighlight(catIdToHighlight);

		model.put("text", "");
		return new ModelAndView("text", model);
	}

	public ModelAndView pfmSetupTransactionIdToEdit(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String transactionId = ServletRequestUtils.getStringParameter(request, "transactionId");
		if (transactionId != null)
			userSession.setPfmTransactionIdToEdit(transactionId);

		model.put("text", "");
		return new ModelAndView("text", model);
	}

	public ModelAndView pfmAccountsLabelUpdate(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String textAllAccts = "Included Accounts = All";
		String textSomeAccts = "Included Accounts = ";

		if (userSession == null) {
			model.put("text", textAllAccts);
		} else {
			// Loop through all the accounts and get the "active" count
			int activeAccounts = 0;
			if (userSession.getPfmAccounts() != null) {
				for (Iterator<PFMAccountWrapper> iterator = userSession.getPfmAccounts().iterator(); iterator.hasNext();) {
					PFMAccountWrapper thisAccount = (PFMAccountWrapper) iterator.next();
					if (thisAccount.isIncludeFlag())
						activeAccounts++;
				}
			}

			if (!"EN".equalsIgnoreCase(userSession.getLanguage())) {
				textAllAccts = "Cuentas incluidas = Todas";
				textSomeAccts = "Cuentas incluidas = ";
			}

			if (userSession.getPfmAccounts() == null || (activeAccounts == userSession.getPfmAccounts().size()))
				model.put("text", textAllAccts);
			else {
				model.put("text", textSomeAccts + Integer.toString(activeAccounts));
			}
		}

		return new ModelAndView("text", model);
	}

	public ModelAndView pfmCategoriesLabelUpdate(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String textAllAccts = "Included Categories = All";
		String textSomeAccts = "Included Categories = ";

		if (userSession == null) {
			model.put("text", textAllAccts);
		} else {
			// Loop through all the categories and get the "active" count
			int activeCategories = 0;
			if (userSession.getPfmCategories() != null) {
				for (Iterator<PFMCategoryWrapper> iterator = userSession.getPfmCategories().iterator(); iterator
						.hasNext();) {
					PFMCategoryWrapper thisCategory = (PFMCategoryWrapper) iterator.next();
					if (thisCategory.isIncludeFlag())
						activeCategories++;
				}
			}

			if (!"EN".equalsIgnoreCase(userSession.getLanguage())) {
				textAllAccts = "Categor&iacute;as incluidas = Todas";
				textSomeAccts = "Categor&iacute;as incluidas = ";
			}

			if (userSession.getPfmCategories() == null || (activeCategories == userSession.getPfmCategories().size()))
				model.put("text", textAllAccts);
			else
				model.put("text", textSomeAccts + Integer.toString(activeCategories - 1));

		}

		return new ModelAndView("text", model);
	}

	public ModelAndView pfmCreditSubcategoriesLabelUpdate(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Loop through all the categories and find the income one
		int activeSubcategories = 0;
		PFMCategoryWrapper thisCategory = new PFMCategoryWrapper();

		if (userSession.getPfmCategories() != null) {
			for (Iterator<PFMCategoryWrapper> iterator = userSession.getPfmCategories().iterator(); iterator.hasNext();) {
				thisCategory = (PFMCategoryWrapper) iterator.next();
				if (thisCategory.getCategoryId() == INCOME_CATEGORY_ID) {
					// Loop through these subcategories and find the active ones
					for (Iterator<PFMSubcategoryWrapper> iterator2 = thisCategory.getSubcategories().iterator(); iterator2
							.hasNext();) {
						PFMSubcategoryWrapper subcat = (PFMSubcategoryWrapper) iterator2.next();
						if (subcat.isIncludeFlag())
							activeSubcategories++;
					}
					break;
				}
			}
		}

		if ("EN".equalsIgnoreCase(userSession.getLanguage())) {
			if (thisCategory.getSubcategories() != null) {
				if (activeSubcategories == thisCategory.getSubcategories().size())
					model.put("text", "Included Subcategories = All");
				else
					model.put("text", "Included Subcategories = " + Integer.toString(activeSubcategories));
			} else {
				model.put("text", "Included Subcategories = " + Integer.toString(activeSubcategories));
			}
		} else {
			if (thisCategory.getSubcategories() != null) {
				if (activeSubcategories == thisCategory.getSubcategories().size())
					model.put("text", "Subcategor&iacute;as incluidas = Todas");
				else
					model.put("text", "Subcategor&iacute;as incluidas = " + Integer.toString(activeSubcategories));
			} else {
				model.put("text", "Subcategor&iacute;as incluidas = " + Integer.toString(activeSubcategories));
			}
		}

		return new ModelAndView("text", model);
	}

	public ModelAndView pfmTagsLabelUpdate(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		// Loop through all the tags and get the "active" count
		int activeTags = 0;
		if (userSession.getPfmTags() != null && userSession.getPfmTags().size() > 0) {
			for (Iterator<PFMTagWrapper> iterator = userSession.getPfmTags().iterator(); iterator.hasNext();) {
				PFMTagWrapper thisTag = (PFMTagWrapper) iterator.next();
				if (thisTag.isIncludeFlag())
					activeTags++;
			}

			if ("EN".equalsIgnoreCase(userSession.getLanguage())) {
				if (activeTags == userSession.getPfmTags().size())
					model.put("text", "Included Tags = All");
				else
					model.put("text", "Included Tags = " + Integer.toString(activeTags));
			} else {
				if (activeTags == userSession.getPfmTags().size())
					model.put("text", "Etiquetas incluidas = Todas");
				else
					model.put("text", "Etiquetas incluidas = " + Integer.toString(activeTags));
			}
		}

		return new ModelAndView("text", model);
	}

	@SuppressWarnings("unchecked")
	public ModelAndView pfmUpdateEntitlementViewPortal(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();

		// Get the value
		String view_portal = ServletRequestUtils.getStringParameter(request, "view_portal");

		// Update the entitlement
		PFMEntitlement pfmEntitlement = null;
		if (!profile.hasEntitlement(EntitlementType.PFM)) {
			if (pfmAutomaticEntitlementCreation) {
				PFMEntitlement pfmEntitlementToAdd = new PFMEntitlement();
				pfmEntitlementToAdd.setEmptyEntitlement();
				customerService.getCustomerProfileService().addEntitlement(profile,
						pfmEntitlementToAdd.getCustomerEntitlement());
			} else {
				model.put("text", "OK");
				return new ModelAndView("text", model);
			}
		}

		pfmEntitlement = new PFMEntitlement(profile.getEntitlementByType(EntitlementType.PFM));
		pfmEntitlement.setVIEW_PORTAL(view_portal);
		CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(pfmEntitlement.getCustomerEntitlement()));
		model.put("text", "OK");
		return new ModelAndView("text", model);
	}

	@SuppressWarnings("unchecked")
	public ModelAndView pfmUpdateEntitlementViewAdvTrx(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();

		// Get the value
		String view_advtrx = ServletRequestUtils.getStringParameter(request, "view_advtrx");

		// Update the entitlement
		PFMEntitlement pfmEntitlement = null;
		if (!profile.hasEntitlement(EntitlementType.PFM)) {
			if (pfmAutomaticEntitlementCreation) {
				PFMEntitlement pfmEntitlementToAdd = new PFMEntitlement();
				pfmEntitlementToAdd.setEmptyEntitlement();
				customerService.getCustomerProfileService().addEntitlement(profile,
						pfmEntitlementToAdd.getCustomerEntitlement());
			} else {
				model.put("text", "OK");
				return new ModelAndView("text", model);
			}
		}
		pfmEntitlement = new PFMEntitlement(profile.getEntitlementByType(EntitlementType.PFM));
		pfmEntitlement.setVIEW_ADV_TRX(view_advtrx);
	
		CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(pfmEntitlement.getCustomerEntitlement()));
		model.put("text", "OK");
		return new ModelAndView("text", model);
	}

	public ModelAndView pfmRedirectLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		response.setContentType("text/javascript");
		return new ModelAndView("pfmRedirectLogin", model);
	}

	private String findcheckTransferRuleStatus(UserSession userSession, PFMTransactionWrapper wrapper) {
		String status = "";

		if (userSession.getPfmCustomRules() != null && userSession.getPfmCustomRules().size() > 0) {
			for (Iterator<PFMCustomRuleWrapper> iterator = userSession.getPfmCustomRules().iterator(); iterator
					.hasNext();) {
				PFMCustomRuleWrapper test = (PFMCustomRuleWrapper) iterator.next();
				String wrapperAccount = (wrapper.getAccountId().endsWith("DEP")) ? wrapper.getAccountId().substring(0,
						9) : wrapper.getAccountId().substring(0, 16);

				// If the account id and the short name match...
				if (wrapperAccount.equals(test.getAccountNumber())) {
					if (wrapper.getTransactionDescShort() != null && test.getTransactionShortDesc() != null) {
						if (wrapper.getTransactionDescShort().equals(test.getTransactionShortDesc())) {
							// We need to match the amounts exactly
							BigDecimal amount = new BigDecimal(
									wrapper.getCreditAmount().equals("0") ? wrapper.getDebitAmount()
											: wrapper.getCreditAmount());
							if (test.getAmount().setScale(2, BigDecimal.ROUND_UP)
									.equals(amount.setScale(2, BigDecimal.ROUND_UP))
									&& test.getAmountMax().setScale(2, BigDecimal.ROUND_UP)
											.equals(amount.setScale(2, BigDecimal.ROUND_UP))) {

								// Finally, if subcategories match, then this is
								// the same exact rule - no need to do anything
								if (wrapper.getSubcategoryId() == test.getSubcategoryId()) {
									status = "SAME";
								} else {
									status = "EXISTS";
								}
								break;
							}
						}
					}
				}
			}
		}
		return status;
	}

	private boolean findCustomRule(UserSession userSession, PFMTransactionWrapper wrapper) {

		getPfmCustomRules(userSession);
		boolean foundRule = false;

		// Is there a rule that goes along with this transaction?
		if (userSession.getPfmCustomRules() != null && userSession.getPfmCustomRules().size() > 0) {
			for (Iterator<PFMCustomRuleWrapper> iterator2 = userSession.getPfmCustomRules().iterator(); iterator2
					.hasNext();) {
				PFMCustomRuleWrapper test = (PFMCustomRuleWrapper) iterator2.next();
				String wrapperAccount = (wrapper.getAccountId().endsWith("DEP")) ? wrapper.getAccountId().substring(0,
						9) : wrapper.getAccountId().substring(0, 16);

				// If the account id and the short name match...
				if (wrapperAccount.equals(test.getAccountNumber())) {
					if (wrapper.getTransactionDescShort() != null && test.getTransactionShortDesc() != null) {
						if (wrapper.getTransactionDescShort().equals(test.getTransactionShortDesc())
								&& wrapper.getSubcategoryId() == test.getSubcategoryId()) {
							// For a check, we need to match the amounts exactly
							if (wrapper.getTransactionDescShort().equals("CHEQUENUMERO")) {
								BigDecimal amount = new BigDecimal(
										wrapper.getCreditAmount().equals("0") ? wrapper.getDebitAmount()
												: wrapper.getCreditAmount());
								if (test.getAmount().setScale(2, BigDecimal.ROUND_UP)
										.equals(amount.setScale(2, BigDecimal.ROUND_UP))
										&& test.getAmountMax().setScale(2, BigDecimal.ROUND_UP)
												.equals(amount.setScale(2, BigDecimal.ROUND_UP))) {
									// There is a match...
									foundRule = true;
									break;
								}
							}
							// For a transfer, we need to match the amounts
							// exactly
							else if (wrapper.getTransactionDescShort().equals("TTRANSFERENCIAAC")) {
								BigDecimal amount = new BigDecimal(
										wrapper.getCreditAmount().equals("0") ? wrapper.getDebitAmount()
												: wrapper.getCreditAmount());
								if (test.getAmount().setScale(2, BigDecimal.ROUND_UP)
										.equals(amount.setScale(2, BigDecimal.ROUND_UP))
										&& test.getAmountMax().setScale(2, BigDecimal.ROUND_UP)
												.equals(amount.setScale(2, BigDecimal.ROUND_UP))) {
									// There is a match...
									foundRule = true;
									break;
								}
							}
							// For other cases, a rule exists; now we only would
							// match a rule with a min of 0.00 and a max of
							// 999999999999.99
							else if (test.getAmount().equals(new BigDecimal("0"))
									&& test.getAmountMax().setScale(2, BigDecimal.ROUND_UP)
											.equals(new BigDecimal("999999999999.99"))) {
								// There is a match...
								foundRule = true;
								break;
							}
						}
					}
				}
			}
		}
		return foundRule;
	}

	private PFMCustomRuleWrapper findEntireCustomRule(UserSession userSession, PFMTransactionWrapper wrapper,
			boolean ignoreTransactionSubcategory) {

		getPfmCustomRules(userSession);
		boolean found = false;
		PFMCustomRuleWrapper customRule = new PFMCustomRuleWrapper();

		// Is there a rule that goes along with this transaction?
		if (userSession.getPfmCustomRules() != null && userSession.getPfmCustomRules().size() > 0) {
			for (Iterator<PFMCustomRuleWrapper> iterator2 = userSession.getPfmCustomRules().iterator(); iterator2
					.hasNext();) {
				PFMCustomRuleWrapper test = (PFMCustomRuleWrapper) iterator2.next();
				String wrapperAccount = (wrapper.getAccountId().endsWith("DEP")) ? wrapper.getAccountId().substring(0,
						9) : wrapper.getAccountId().substring(0, 16);

				// If the account id, subcategory id and the short name match...
				if (wrapperAccount.equals(test.getAccountNumber())
						&& wrapper.getTransactionDescShort().equals(test.getTransactionShortDesc())) {

					// Make sure to match debit/credit...
					if ((test.getDebitCreditInd().equals("DB") && wrapper.getCreditAmount().equals("0"))
							|| (test.getDebitCreditInd().equals("CR") && wrapper.getDebitAmount().equals("0"))) {

						// Should we ignore subcategory?
						boolean continueSearch = (ignoreTransactionSubcategory) ? true
								: (wrapper.getSubcategoryId() == test.getSubcategoryId());

						if (continueSearch) {
							// For a check, we need to match the amounts exactly
							if (wrapper.getTransactionDescShort().equals("CHEQUENUMERO")) {
								BigDecimal amount = new BigDecimal(
										wrapper.getCreditAmount().equals("0") ? wrapper.getDebitAmount()
												: wrapper.getCreditAmount());
								if (test.getAmount().setScale(2, BigDecimal.ROUND_UP)
										.equals(amount.setScale(2, BigDecimal.ROUND_UP))
										&& test.getAmountMax().setScale(2, BigDecimal.ROUND_UP)
												.equals(amount.setScale(2, BigDecimal.ROUND_UP))) {
									// There is a match...
									customRule = test;
									found = true;
									break;
								}
							}
							// For a transfer, we need to match the amounts
							// exactly
							else if (wrapper.getTransactionDescShort().equals("TTRANSFERENCIAAC")) {
								BigDecimal amount = new BigDecimal(
										wrapper.getCreditAmount().equals("0") ? wrapper.getDebitAmount()
												: wrapper.getCreditAmount());
								if (test.getAmount().setScale(2, BigDecimal.ROUND_UP)
										.equals(amount.setScale(2, BigDecimal.ROUND_UP))
										&& test.getAmountMax().setScale(2, BigDecimal.ROUND_UP)
												.equals(amount.setScale(2, BigDecimal.ROUND_UP))) {
									// There is a match...
									customRule = test;
									found = true;
									break;
								}
							}
							// For other cases, a rule exists; now we only would
							// match a rule with a min of 0.00 and a max of
							// 999999999999.99
							else if (test.getAmount().equals(new BigDecimal("0"))
									&& test.getAmountMax().setScale(2, BigDecimal.ROUND_UP)
											.equals(new BigDecimal("999999999999.99"))) {
								// There is a match...
								customRule = test;
								found = true;
								break;
							}
						}
					}
				}
			}
		}
		if (found)
			return customRule;
		else
			return null;
	}

	// Sort the category list by Spanish descriptions
	private List<PFMCategoryWrapper> sortCategoryList(List<PFMCategoryWrapper> list, String language) {
		for (int i = list.size(); --i >= 0;) {
			for (int j = 0; j < i; j++) {
				String current = language.equals("es") ? list.get(j).getDescription_es() : list.get(j)
						.getDescription_en();
				String test = language.equals("es") ? list.get(j + 1).getDescription_es() : list.get(j + 1)
						.getDescription_en();
				if (current.compareToIgnoreCase(test) > 0) {
					PFMCategoryWrapper item = list.get(j);
					list.set(j, list.get(j + 1));
					list.set(j + 1, item);
				}
			}
		}

		// Now do the same for their subcategories
		for (int i2 = 0; i2 < list.size(); i2++) {
			PFMCategoryWrapper cat = list.get(i2);
			if (cat.getSubcategories() != null && cat.getSubcategories().size() > 0) {
				List<PFMSubcategoryWrapper> subcategoryList = cat.getSubcategories();
				for (int i = subcategoryList.size(); --i >= 0;) {
					for (int j = 0; j < i; j++) {
						String current = language.equals("es") ? subcategoryList.get(j).getDescription_es()
								: subcategoryList.get(j).getDescription_en();
						String test = language.equals("es") ? subcategoryList.get(j + 1).getDescription_es()
								: subcategoryList.get(j + 1).getDescription_en();
						if (current.compareToIgnoreCase(test) > 0) {
							PFMSubcategoryWrapper item = subcategoryList.get(j);
							subcategoryList.set(j, subcategoryList.get(j + 1));
							subcategoryList.set(j + 1, item);
						}
					}
				}
			}
		}

		return list;
	}

	// Sort the tags list by language
	private List<PFMTagWrapper> sortTagList(List<PFMTagWrapper> list, String language) {
		for (int i = list.size(); --i >= 0;) {
			for (int j = 0; j < i; j++) {
				String current = language.equals("es") ? list.get(j).getDescription_es() : list.get(j)
						.getDescription_en();
				String test = language.equals("es") ? list.get(j + 1).getDescription_es() : list.get(j + 1)
						.getDescription_en();
				if (current.compareToIgnoreCase(test) > 0) {
					PFMTagWrapper item = list.get(j);
					list.set(j, list.get(j + 1));
					list.set(j + 1, item);
				}
			}
		}

		return list;
	}

	// Sort the custom rules by ID in descending order
	private List<PFMCustomRuleWrapper> sortCustomRuleList(List<PFMCustomRuleWrapper> list) {
		for (int i = list.size(); --i >= 0;) {
			for (int j = 0; j < i; j++) {
				int current = Integer.parseInt(list.get(j).getRuleId());
				int test = Integer.parseInt(list.get(j + 1).getRuleId());
				if (current < test) {
					PFMCustomRuleWrapper item = list.get(j);
					list.set(j, list.get(j + 1));
					list.set(j + 1, item);
				}
			}
		}

		return list;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public boolean isPfmAutomaticEntitlementCreation() {
		return pfmAutomaticEntitlementCreation;
	}

	public void setPfmAutomaticEntitlementCreation(boolean pfmAutomaticEntitlementCreation) {
		this.pfmAutomaticEntitlementCreation = pfmAutomaticEntitlementCreation;
	}
}
