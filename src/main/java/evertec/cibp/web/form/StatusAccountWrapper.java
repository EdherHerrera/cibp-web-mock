package evertec.cibp.web.form;

import evertec.cibp.core.models.account.TVFrontendAccount;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


/**
 * Wrapper of {@link TVFrontendAccount}
 *
 * @author et57856
 * @version 1.0
 * @see {@link ActivateAthIntForm}
 * @since 1.0
 */

public class StatusAccountWrapper {

    /**
     * Accounts
     */
    private TVFrontendAccount accounts; //Account

    /**
     * Boolean that indicate if link Active is show
     */
    private boolean showLink; //Boolean that indicate if link Active is show

    /**
     * Inactive Status
     */
    private static final String STATUS_INACTIVE = "INACTIVE"; //Inactive Status

    /**
     * 5 M Undefined Status
     */
    private static final String STATUS_5_M_UNDEFINED = "5 M UNDEFINED"; //5 M Undefined Status

    /**
     * 5 Z Undefined Status
     */
    private static final String STATUS_5_Z_UNDEFINED = "5 Z UNDEFINED"; //5 Z Undefined Status

    /**
     * 5 B Blocked Status
     */
    private static final String STATUS_5_B_BLOCKED = "5 B BLOCKED"; //5 B Blocked Status

    /**
     * STATUS_ATH Collections
     */
    private static final Set<String> STATUS_ATH; //STATUS_ATH Collections

    /**
     * Constant NUM
     */
    private static final Integer NUM = 4; //Constant NUM

    static {

        /**
         * Collection that define ATH International status permited tu show link to activate
         */
        Set<String> set = new HashSet<String>(NUM); //
        set.add(STATUS_INACTIVE);
        set.add(STATUS_5_M_UNDEFINED);
        set.add(STATUS_5_Z_UNDEFINED);
        set.add(STATUS_5_B_BLOCKED);

        /**
         * Collection that charge ATH International status permited tu show link to activate
         */
        STATUS_ATH = Collections.unmodifiableSet(set);
    }

    /**
     * Default Constructor method
     *
     * @param accounts
     */
    public StatusAccountWrapper () {
        this(null);
    }


    /**
     * Constructor method
     *
     * @param accounts
     */
    public StatusAccountWrapper (TVFrontendAccount accounts) {
        this.accounts = accounts;
        this.showLink = STATUS_ATH.contains(accounts.getStatus());
    }


    /**
     * getAccounts
     *
     * @return a TVFrontendAccount
     */
    public TVFrontendAccount getAccounts () {
        return accounts;
    }


    /**
     * isShowLink
     *
     * @return a boolean
     */
    public boolean isShowLink () {
        return showLink;
    }

}

