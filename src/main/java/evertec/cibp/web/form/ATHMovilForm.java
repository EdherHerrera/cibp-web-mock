package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rsa.csd.ws.EventType;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import bppr.tv.beans.accounts.types.DepositAccount;

import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;

import edu.emory.mathcs.backport.java.util.Arrays;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.ATHMovilEntitlement;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.ValidationQuestionResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.athm.TransferInfo;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ATHMovilBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AthmUtils;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.CookieUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.athm.OsPhoneInfo;
import evertec.otherservices.beans.athm.OsTransactionInfo;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.AthmCustomerInfoReq;
import evertec.otherservices.requests.AthmStatusReq;
import evertec.otherservices.requests.AthmTargetPhoneReq;
import evertec.otherservices.requests.AthmTransHistoryReq;
import evertec.otherservices.responses.AthmCustomerInfoResp;
import evertec.otherservices.responses.AthmStatusResp;
import evertec.otherservices.responses.AthmTransHistoryResp;

public class ATHMovilForm extends CancellableFormController implements MessageSourceAware {

	protected final Log logger = LogFactory.getLog(getClass());

	private MessageSource messageSource = null;
	protected CustomerServices customerService = null;
	private String fiId = null;
	private String fiToken = null;

	// MBSFE-691
	private String invalidEmailDomains;
	// END MBSFE-691

	private class Actions {
		static public final String ADD_FRIEND = "ADDFRIEND";
		static public final String EDIT_FRIEND = "EDITFRIEND";
		static public final String DELETE_FRIEND = "DELETEFRIEND";
		static public final String EDIT_ACCOUNT = "EDITACCOUNT";
		static public final String EDIT_RECEIVE_SMS = "EDITRECEIVESMS";
		static public final String EDIT_EMAIL = "EDITEMAIL";

		private Actions() {
		};
	}

	public ATHMovilForm() {
		super();
		setCommandClass(ATHMovilBean.class);
		setCommandName("athmovil");
		setFormView("athmovil");
		setBindOnNewForm(true);
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		ATHMovilEntitlement entitlement = new ATHMovilEntitlement(userSession.getCustomerProfile()
				.getEntitlementByType(EntitlementType.ATHMOVIL));

		ATHMovilBean athMovilBean = (ATHMovilBean) command;

		if (Actions.ADD_FRIEND.equalsIgnoreCase(athMovilBean.getAction())) {
			boolean error = false;
			if (athMovilBean.getFriendName() == null || athMovilBean.getFriendName().trim().length() <= 0) {
				errors.rejectValue("friendName", "athmovil.contactname.required", "Name is required.");
				error = true;
			}
			if (athMovilBean.getFriendPhone() == null || athMovilBean.getFriendPhone().trim().length() <= 0) {
				errors.rejectValue("friendPhone", "athmovil.contactphone.required", "Phone number is required");
				error = true;
			} else if (AthmUtils.getNumericPhoneNumber(athMovilBean.getFriendPhone()) == null
					|| athMovilBean.getFriendPhone().startsWith("0")) {
				errors.rejectValue("friendPhone", "athmovil.phone.invalid", "Invalid phone number");
				error = true;
			} else if (AthmUtils.getNumericPhoneNumber(athMovilBean.getFriendPhone()).equals(
					AthmUtils.getNumericPhoneNumber(entitlement.getPhoneNumber()))) {
				errors.rejectValue("friendPhone", "athmovil.phone.invalidphone",
						"The number must be different from the one you are subscribed with.");
				error = true;
			}

			if (!error) {

				OsPhoneInfo friendList[] = userSession.getAthmContacts(fiId, fiToken, entitlement.getUserId());
				if (friendList != null && friendList.length > 0) {
					for (OsPhoneInfo friend : friendList) {
						if (friend.getPhoneNumber().equals(
								AthmUtils.getNumericPhoneNumber(athMovilBean.getFriendPhone()))) {
							errors.rejectValue("friendPhone", "athmovil.friend.already.added",
									"Phone number already added");
							break;
						}
					}
				}
			}

		} else if (Actions.EDIT_RECEIVE_SMS.equalsIgnoreCase(athMovilBean.getAction())) {
			if (athMovilBean.getReceiveSmsAlerts() == null
					|| athMovilBean.getReceiveSmsAlerts().trim().length() <= 0
					|| ("Y".equalsIgnoreCase(athMovilBean.getReceiveSmsAlerts()) && "N".equalsIgnoreCase(athMovilBean
							.getReceiveSmsAlerts()))) {
				errors.rejectValue("receiveSmsAlerts", "athmovil.option.invalid", "Invalid option");
			} else {
				AthmCustomerInfoReq req = new AthmCustomerInfoReq();
				req.setReceiveSMS("Y".equalsIgnoreCase(athMovilBean.getReceiveSmsAlerts()));
				req.setFiID(fiId);
				req.setFiToken(fiToken);
				req.setUserID(entitlement.getUserId());

				AthmCustomerInfoResp editReceiveSmsResp = customerService.editReceiveSMS(req);

				if (editReceiveSmsResp == null
						|| !editReceiveSmsResp.getResponseStatus().equals(ResponseStatus.SUCCESS)
						|| !("200".equals(editReceiveSmsResp.getStatusCode()))) {
					errors.rejectValue("receiveSmsAlerts", "athmovil.option.failed", "Unable to set option");
				}
			}									
		} else if (Actions.EDIT_ACCOUNT.equalsIgnoreCase(athMovilBean.getAction())) {
			if ("none".equalsIgnoreCase(athMovilBean.getAccount())) {
				errors.rejectValue("account", "athmovil.account.required", "Account is required");
			} else {
				try {
					List<TVFrontendAccount> cardPlasticAccountList = userSession.getCardPlasticAccounts();

					TVFrontendAccount frontendAccount = Utils.getAccountById(athMovilBean.getAccount(),
							userSession.getAccounts());

					AbstractAccount accountDetails = userSession.getCustomerService().getCardPlasticAccount(
							userSession.getCustomerProfile(), userSession.getBankingSession(), frontendAccount, true);

					DepositAccount depositAccount = (DepositAccount) accountDetails.getBackendObject();

					CardPlasticAccountRecord[] cardPlasticAccountRecordList = depositAccount
							.getCardPlasticAccountRecords();

					CardPlasticAccountRecord cardPlasticAccountRecord = null;

					TVFrontendAccount frontendCardPlasticAccount = null;

					if (cardPlasticAccountList != null && cardPlasticAccountRecordList != null
							&& cardPlasticAccountRecordList.length > 0) {
						for (int i = 0; i < cardPlasticAccountRecordList.length; i++) {
							cardPlasticAccountRecord = cardPlasticAccountRecordList[i];

							for (Iterator<TVFrontendAccount> iterator = cardPlasticAccountList.iterator(); iterator
									.hasNext();) {
								frontendCardPlasticAccount = iterator.next();

								if (cardPlasticAccountRecord.getCardPANId() != null
										&& !cardPlasticAccountRecord.getCardPANId().startsWith("4549")
										&& cardPlasticAccountRecord.getCardPANId().equals(
												frontendCardPlasticAccount.getAccountNumber())) {
									break;
								} else {
									frontendCardPlasticAccount = null;
								}
							}

							if (frontendCardPlasticAccount != null) {
								break;
							}
						}
					}

					if (frontendCardPlasticAccount == null) {
						errors.rejectValue("account", "athmovil.account.validation.invalid", "Invalid account");
					} else {
						accountDetails = userSession.getCustomerService().getCardPlasticAccount(
								userSession.getCustomerProfile(), userSession.getBankingSession(),
								frontendCardPlasticAccount, true);

						depositAccount = (DepositAccount) accountDetails.getBackendObject();

						cardPlasticAccountRecordList = depositAccount.getCardPlasticAccountRecords();

						if (cardPlasticAccountRecordList == null || cardPlasticAccountRecordList.length <= 0) {
							errors.rejectValue("account", "athmovil.account.validation.invalid", "Invalid account");
						} else {
							cardPlasticAccountRecord = cardPlasticAccountRecordList[0];

							String fiPlasticAccount = null;
							String fiPlasticAccountType = null;
							String fiPlasticExpirationDate = null;
							String fiPlasticNumber = null;
							AthmStatusResp updateAccount = null;

							if (frontendAccount.getAccountNumber() != null) {
								fiPlasticAccount = frontendAccount.getAccountNumber();
							}

							if (frontendAccount.getAccountSection() != null) {
								if (DepositAccount.CHECKING_SECTION.equals(frontendAccount.getAccountSection())) {
									fiPlasticAccountType = "1";
								} else if (DepositAccount.SAVINGS_SECTION.equals(frontendAccount.getAccountSection())) {
									fiPlasticAccountType = "2";
								}
							}

							if (cardPlasticAccountRecord.getExpirationDate() != null) {
								SimpleDateFormat yearMonthFormat = new SimpleDateFormat("yyyyMM");

								Calendar calendar = cardPlasticAccountRecord.getExpirationDate();

								fiPlasticExpirationDate = yearMonthFormat.format(calendar.getTime());
							}

							if (cardPlasticAccountRecord.getCardPANId() != null
									&& cardPlasticAccountRecord.getCardPANId().trim().length() > 0) {
								fiPlasticNumber = cardPlasticAccountRecord.getCardPANId();
							}

							if (fiPlasticNumber != null && fiPlasticNumber.equals(entitlement.getPlasticNumber()))
								errors.rejectValue("account", "athmovil.account.validation.used", "Invalid account");
							else {
								
								Boolean informationChallenge = false;
								if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)) {
									if (!userSession.getOobEnroll()) {
										informationChallenge = userSession.isInformationChallenge();
										if (!informationChallenge && userSession.getInformationQuestion() == null)
											this.getRSAChallenge(request, userSession);
									} else {
										if (userSession.isOobAuthenticated())
											informationChallenge = true;
									}
								} else {
									informationChallenge = userSession.isInformationChallenge();
									if (!informationChallenge && userSession.getInformationQuestion() == null) {
										this.getRSAChallenge(request, userSession);
									}
								}
								
								if (!informationChallenge) {
									Map<String, String> parameters = new HashMap<String, String>();
									parameters.put(ChallengeInformationUtils.ATHM_ACCT_TYPE, fiPlasticAccountType);
									parameters.put(ChallengeInformationUtils.ATHM_EXP_DATE, fiPlasticExpirationDate);
									parameters.put(ChallengeInformationUtils.ATHM_PLASTIC, fiPlasticNumber);
									parameters.put(ChallengeInformationUtils.ATHM_ACCT, fiPlasticAccount);
									WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER,
											true);
									WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
											ChallengeInformationUtils.ATHM_ACCT_MOD);
									WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ATHM_ACCT_MOD, true);
									WebUtils.setSessionAttribute(request,
											ChallengeInformationUtils.INFORMATION_PARAMETERS, parameters);
									errors.rejectValue("", "", "");
								} else {
									AthmStatusReq updateAcctReq = new AthmStatusReq();
									updateAcctReq.setAccSection(fiPlasticAccountType);
									updateAcctReq.setExpDate(fiPlasticExpirationDate);
									updateAcctReq.setFiID(fiId);
									updateAcctReq.setFiToken(fiToken);
									updateAcctReq.setPlasticNumber(fiPlasticNumber);
									updateAcctReq.setUserId(entitlement.getUserId());
									updateAccount = customerService.changePrimaryCard(updateAcctReq);

									if (updateAccount == null || updateAccount.getResponseStatus() == null
											|| !updateAccount.getResponseStatus().equals(ResponseStatus.SUCCESS)
											|| !("200".equals(updateAccount.getStatusCode()))) {
										errors.rejectValue("account", "athmovil.account.validation.failed",
												"Unable to validate account");
									} else {
										entitlement.setPlasticAccount(fiPlasticAccount);
										entitlement.setPlasticAccountType(fiPlasticAccountType);
										entitlement.setPlasticExpirationDate(fiPlasticExpirationDate);
										entitlement.setPlasticNumber(fiPlasticNumber);

										CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(entitlement.getCustomerEntitlement()));
									}
								}

							}
						}
					}
				} catch (Exception e) {
					errors.rejectValue("account", "athmovil.account.validation.failed", "Unable to validate account");

					logger.error("Account validation failed", e);
				}
			}
		} else if (Actions.EDIT_EMAIL.equalsIgnoreCase(athMovilBean.getAction())) {

			// MBSFE-691
			String[] domain = athMovilBean.getEmail().split("@");
			// END MBSFE-691
			
			if (GenericValidator.isBlankOrNull(athMovilBean.getEmail())) {
				errors.rejectValue("email", "athmovil.email.required", "Email is required");
			} else if (!GenericValidator.isEmail(athMovilBean.getEmail())) {
				errors.rejectValue("email", "athmovil.email.invalid", "Invalid Email");
			} else if (!athMovilBean.getEmail().equals(athMovilBean.getConfirmEmail())) {
				errors.rejectValue("confirmEmail", "athmovil.email.confirm.error", "Fields must match");
			
			// MBSFE-691
			} else if (Utils.haveType(this.invalidEmailDomains, domain[1])
					&& StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE691)) {
				errors.rejectValue("email", "email.invaliddomain",
						"Your e-mail address is incorrect. Please try again.");
			// END MBSFE-691
				
			} else {

				AthmCustomerInfoReq editEmailRequest = new AthmCustomerInfoReq();
				editEmailRequest.setEmail(athMovilBean.getEmail());
				editEmailRequest.setFiID(fiId);
				editEmailRequest.setFiToken(fiToken);
				editEmailRequest.setUserID(entitlement.getUserId());

				AthmCustomerInfoResp updateEmailResponse = customerService.editEmail(editEmailRequest);

				if (updateEmailResponse == null
						|| !updateEmailResponse.getResponseStatus().equals(ResponseStatus.SUCCESS)
						|| !("200".equals(updateEmailResponse.getStatusCode()))) {
					errors.rejectValue("email", "athmovil.email.failed", "Unable to update the email");
				}
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<String, Object> model = new HashMap<>();
		ATHMovilBean athMovilBean = (ATHMovilBean) command;
		ATHMovilEntitlement entitlement = new ATHMovilEntitlement(userSession.getCustomerProfile()
				.getEntitlementByType(EntitlementType.ATHMOVIL));

		if (Actions.ADD_FRIEND.equalsIgnoreCase(athMovilBean.getAction())) {
			AthmTargetPhoneReq addFriendReq = new AthmTargetPhoneReq();
			addFriendReq.setFiID(fiId);
			addFriendReq.setFiToken(fiToken);
			addFriendReq.setUserID(entitlement.getUserId());
			addFriendReq.setName(athMovilBean.getFriendName().trim());
			addFriendReq.setPhoneNumber(AthmUtils.getStringNumericPhoneNumber(athMovilBean.getFriendPhone()));
			customerService.addCustomerTargetPhone(addFriendReq);
			userSession.resetAthMovilContacts();
		} else if (Actions.EDIT_FRIEND.equals(athMovilBean.getAction())) {

			OsPhoneInfo friendToEdit = userSession.getAthmContacts(fiId, fiId, entitlement.getUserId())[Integer
					.parseInt(athMovilBean.getFriendId())];
			Long friendPhone = AthmUtils.getNumericPhoneNumber(athMovilBean.getFriendPhone());
			if (!GenericValidator.isBlankOrNull(athMovilBean.getFriendPhone()) && friendPhone != null
					&& !friendPhone.equals(AthmUtils.getNumericPhoneNumber(entitlement.getPhoneNumber()))) {
				// NOT EQUALS ATHM PHONE
				if (AthmUtils.getNumericPhoneNumber(athMovilBean.getFriendPhone()).equals(
						AthmUtils.getNumericPhoneNumber(entitlement.getPhoneNumber()))) {
					model.put("text", AthmUtils.formatPhoneNumberDashes(friendToEdit.getPhoneNumber()));
					return new ModelAndView("text", model);
				}
				// NOT EQUALS ANY OTHER FRIEND HPONE
				OsPhoneInfo friendList[] = userSession.getAthmContacts(fiId, fiToken, entitlement.getUserId());
				if (friendList != null && friendList.length > 0) {
					for (OsPhoneInfo friend : friendList) {
						if (friend.getPhoneNumber().equals(
								AthmUtils.getNumericPhoneNumber(athMovilBean.getFriendPhone()))) {
							model.put("text", AthmUtils.formatPhoneNumberDashes(friendToEdit.getPhoneNumber()));
							return new ModelAndView("text", model);
						}
					}
				}
				AthmTargetPhoneReq editFriendReq = new AthmTargetPhoneReq();
				editFriendReq.setFiID(fiId);
				editFriendReq.setFiToken(fiToken);
				editFriendReq.setUserID(entitlement.getUserId());
				editFriendReq.setName(friendToEdit.getName());
				editFriendReq.setNewName(friendToEdit.getName());
				editFriendReq.setPhoneNumber(AthmUtils.getStringNumericPhoneNumber(friendToEdit.getPhoneNumber()));
				editFriendReq.setNewPhoneNumber(athMovilBean.getFriendPhone().replace("-", ""));
				customerService.editCustomerTargetPhone(editFriendReq);
				userSession.resetAthMovilContacts();
				userSession.getAthmContacts(fiId, fiToken, entitlement.getUserId());
				model.put("text", AthmUtils.formatPhoneNumberDashes(athMovilBean.getFriendPhone()));
			} else {
				model.put("text", AthmUtils.formatPhoneNumberDashes(friendToEdit.getPhoneNumber()));
			}
			return new ModelAndView("text", model);

		} else if (Actions.DELETE_FRIEND.equals(athMovilBean.getAction())) {

			AthmTargetPhoneReq deleteFriendReq = new AthmTargetPhoneReq();
			deleteFriendReq.setFiID(fiId);
			deleteFriendReq.setFiToken(fiToken);
			deleteFriendReq.setUserID(entitlement.getUserId());
			deleteFriendReq.setPhoneNumber(AthmUtils.getStringNumericPhoneNumber(athMovilBean.getFriendPhone()));

			customerService.deleteCustomerTargetPhone(deleteFriendReq);
			userSession.resetAthMovilContacts();

		}

		Boolean rsaModAccount = (Boolean) WebUtils
				.getSessionAttribute(request, ChallengeInformationUtils.ATHM_ACCT_MOD);
		if (rsaModAccount != null && rsaModAccount) {
			Map<String, String> parameters = (Map<String, String>) WebUtils.getSessionAttribute(request,
					ChallengeInformationUtils.INFORMATION_PARAMETERS);
			String accSection = parameters.get(ChallengeInformationUtils.ATHM_ACCT_TYPE);
			String expDate = parameters.get(ChallengeInformationUtils.ATHM_EXP_DATE);
			String plasticNumber = parameters.get(ChallengeInformationUtils.ATHM_PLASTIC);
			String plasticAccount = parameters.get(ChallengeInformationUtils.ATHM_ACCT);
			ModelAndView rsaModelAndView = new ModelAndView();
			
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)) {	
				if (!userSession.getOobEnroll()) {
					rsaModelAndView = this.validateRSA(request, response, error);
				} else {
					if (userSession.isOobAuthenticated()) {
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ATHM_ACCT_MOD, null);
						rsaModelAndView = new ModelAndView("text", "text", "success-");
					}
				}
			} else {
				rsaModelAndView = this.validateRSA(request, response, error);
			}
		
			if ("success-".equals(rsaModelAndView.getModel().get("text"))) {
				AthmStatusReq updateAcctReq = new AthmStatusReq();
				updateAcctReq.setAccSection(accSection);
				updateAcctReq.setExpDate(expDate);
				updateAcctReq.setFiID(fiId);
				updateAcctReq.setFiToken(fiToken);
				updateAcctReq.setPlasticNumber(plasticNumber);
				updateAcctReq.setUserId(entitlement.getUserId());

				AthmStatusResp updateAccount = customerService.changePrimaryCard(updateAcctReq);

				if (updateAccount == null || updateAccount.getResponseStatus() == null
						|| !updateAccount.getResponseStatus().equals(ResponseStatus.SUCCESS)
						|| !("200".equals(updateAccount.getStatusCode()))) {
					error.rejectValue("account", "athmovil.account.validation.failed", "Unable to validate account");
				} else {
					entitlement.setPlasticAccount(plasticAccount);
					entitlement.setPlasticAccountType(accSection);
					entitlement.setPlasticExpirationDate(expDate);
					entitlement.setPlasticNumber(plasticNumber);

					//customerService.getCustomerProfileService().updateEntitlement(userSession.getCustomerProfile(),
					//		entitlement.getCustomerEntitlement());
					CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(entitlement.getCustomerEntitlement()));
				}
			}
			return rsaModelAndView;
		}

		return showForm(request, response, error, model);
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<String, Object> model = new HashMap<>();
		Map<String, Object> datemap = new LinkedHashMap<String, Object>();
		Map<String, Object> yesNoMap = new LinkedHashMap<String, Object>();
		Map<String, Object> athmAccountsMap = new LinkedHashMap<String, Object>();
		Map<String, List<TransferInfo>> transfersInfoMap = null;

		ATHMovilEntitlement entitlement = new ATHMovilEntitlement(userSession.getCustomerProfile()
				.getEntitlementByType(EntitlementType.ATHMOVIL));

		loadFriendsData(request, command, model);
		model.put("unregistered", userSession.isAthmUnregistered());

		if (!userSession.isAthmUnregistered()) {
			ATHMovilBean athMovilFormBean = (ATHMovilBean) command;

			String language = "sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "es" : Utils
					.getRSALanguage(request);

			Locale locale = new Locale(language);

			if (athMovilFormBean.getTabNum() == null) {
				athMovilFormBean.setTabNum(request.getParameter("tab"));
			}

			Calendar now = Calendar.getInstance();

			SimpleDateFormat sdfYearAndMonth = new SimpleDateFormat("yyyyMM");
			SimpleDateFormat sdfMonthName = new SimpleDateFormat("MMMMM");

			String currentDate = sdfYearAndMonth.format(now.getTime());

			if ("en".equalsIgnoreCase(language)) {
				datemap.put(currentDate, "Recent transactions");
			} else {
				datemap.put(currentDate, "Transacciones recientes");
				sdfMonthName = new SimpleDateFormat("MMMMM", new Locale("es", "ES"));
			}

			
			for(int i = 1; i< 13; i++) {
				
				now.add(Calendar.MONTH, -1);

				datemap.put(sdfYearAndMonth.format(now.getTime()),
						sdfMonthName.format(now.getTime()) + " " + now.get(Calendar.YEAR));

			}

			model.put("dateMap", datemap);

			String id = entitlement.getPlasticAccount();
			String accountDisplay = "";

			if (id != null && id.trim().length() > 0) {
				TVFrontendAccount frontendAccount = AthmUtils.getAccountByAccountNumber(id, userSession);

				if (frontendAccount != null) {
					Map<String, String> accountDetail = AthmUtils.getAccountDetails(frontendAccount);

					if (accountDetail != null) {
						accountDisplay = accountDetail.get("accountName") + " " + accountDetail.get("accountLast4Num");
					}
				}
			}

			model.put("account", accountDisplay);

			if (userSession.getAthmAccounts() != null && userSession.getAthmAccounts().size() > 0) {
				for (Iterator<TVAccountTransferSource> iterator = userSession.getAthmAccounts().iterator(); iterator
						.hasNext();) {
					TVAccountTransferSource frontendAccount = (TVAccountTransferSource) iterator.next();
					Map<String, String> accountText = AthmUtils.getAccountText(frontendAccount.getAccount());
					if (accountText != null) {
						athmAccountsMap.put(frontendAccount.getFrontEndId(), accountText.get("text"));
						model.put("accounts", athmAccountsMap);
					}
				}
			}

			// RSA Challenge **************************
			model.put("editInfo", false);
			model.put("oobchallenge", false);
			
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)) {

				Boolean rsaModAccount = (Boolean) WebUtils.getSessionAttribute(request,
						ChallengeInformationUtils.ATHM_ACCT_MOD);

				if (rsaModAccount != null && rsaModAccount) {

					if (!userSession.getOobEnroll()) {
						if (userSession.getInformationQuestion() != null) {
							String question = (String) userSession.getInformationQuestion().values().toArray()[0];
							model.put("question", question);
							model.put("editInfo", rsaModAccount);
						}
					} else {
						if (!userSession.isOobAuthenticated()) {
							model.put("oobchallenge", true);
							model.put("editInfo", rsaModAccount);
							List<ASK_FOR_OOB> listNavOOB = new ArrayList<ASK_FOR_OOB>();
							listNavOOB.add(ASK_FOR_OOB.OOB_CHALLENGE);
							listNavOOB.add(ASK_FOR_OOB.ATH_MOVIL);
							userSession.setTasksListOOB(listNavOOB);
						}
					}
				}
			} else { 
				Boolean rsaModAccount = (Boolean) WebUtils.getSessionAttribute(request,
						ChallengeInformationUtils.ATHM_ACCT_MOD);
				if (rsaModAccount != null && rsaModAccount) {
					if (userSession.getInformationQuestion() != null) {
						String question = (String) userSession.getInformationQuestion().values().toArray()[0];
						model.put("question", question);
						model.put("editInfo", rsaModAccount);
					}
				}
			}

			if (athMovilFormBean.getDate() != null && athMovilFormBean.getDate().trim().length() > 0) {
				currentDate = athMovilFormBean.getDate().trim();
			}
			transfersInfoMap = getTransfersInfo(entitlement, currentDate, userSession.getCustomerProfile(), userSession);

			if (transfersInfoMap != null) {
				if (transfersInfoMap.containsKey("transfersSentList")) {
					model.put("transfersSentList", transfersInfoMap.get("transfersSentList"));
				}

				if (transfersInfoMap.containsKey("transfersReceivedList")) {
					model.put("transfersReceivedList", transfersInfoMap.get("transfersReceivedList"));
				}
			}

			yesNoMap.put("Y", messageSource.getMessage("athmovil.yes.label", null, locale));
			yesNoMap.put("N", messageSource.getMessage("athmovil.no.label", null, locale));

			model.put("yesnooptions", yesNoMap);

			AthmCustomerInfoReq athReq = new AthmCustomerInfoReq();
			athReq.setFiID(fiId);
			athReq.setFiToken(fiToken);
			athReq.setUserID(entitlement.getUserId());
			AthmCustomerInfoResp athResp = customerService.getCustomerInfo(athReq);

			if ("en".equalsIgnoreCase(language)) {
				model.put("receivesms", athResp.isReceiveSMS() ? "Yes" : "No");
			} else {
				model.put("receivesms", athResp.isReceiveSMS() ? "Si" : "No");
			}

			model.put("email", athResp.getEmail());
			if (!GenericValidator.isBlankOrNull(athResp.getPrimaryPhone())
					&& !entitlement.getPhoneNumber().equals(
							AthmUtils.getStringNumericPhoneNumber(athResp.getPrimaryPhone()))) {
				entitlement.setPhoneNumber(AthmUtils.getStringNumericPhoneNumber(athResp.getPrimaryPhone()));
				//customerService.getCustomerProfileService().updateEntitlement(userSession.getCustomerProfile(),
				//		entitlement.getCustomerEntitlement());
				CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(entitlement.getCustomerEntitlement()));
			}
			model.put("custPhone", AthmUtils.formatPhoneNumberDashes(entitlement.getPhoneNumber()));
			if (request.getParameter("page") != null)
				model.put("page", Integer.valueOf(request.getParameter("page")));

		}
		return model;
	}

	private HashMap<String, List<TransferInfo>> getTransfersInfo(ATHMovilEntitlement entitlement, String transferMonth,
			CustomerProfile customerProfile, UserSession userSession) {

		HashMap<String, List<TransferInfo>> transfersInfoHash = new HashMap<String, List<TransferInfo>>();

		HashMap<String, List<TransferInfo>> sessionATHMTransfers = userSession.getAthmTransfersInfoHash();

		if (sessionATHMTransfers == null) {
			sessionATHMTransfers = this.allTransferInfo(entitlement, customerProfile, userSession);
			userSession.setAthmTransfersInfoHash(sessionATHMTransfers);
		}

		List<TransferInfo> filteredSentTransfers = new ArrayList<TransferInfo>();
		if (!GenericValidator.isBlankOrNull(transferMonth) && sessionATHMTransfers != null) {
			if (sessionATHMTransfers.get("transfersSentList") != null
					&& sessionATHMTransfers.get("transfersSentList").size() > 0) {
				List<TransferInfo> sentTransfers = sessionATHMTransfers.get("transfersSentList");

				for (TransferInfo info : sentTransfers) {
					int filterMonth = Integer.parseInt(transferMonth.substring(4));
					int filterYear = Integer.parseInt(transferMonth.substring(0, 4));
					Calendar infoDate = AthmUtils.convertStringToCal(info.getDate());
					if (infoDate.get(Calendar.MONTH) == filterMonth && infoDate.get(Calendar.YEAR) == filterYear) {
						filteredSentTransfers.add(info);
					}
				}
			}
		} else {
			filteredSentTransfers = sessionATHMTransfers.get("transfersSentList");
		}
		transfersInfoHash.put("transfersSentList", filteredSentTransfers);

		List<TransferInfo> filteredReceivedTransfers = new ArrayList<TransferInfo>();
		if (!GenericValidator.isBlankOrNull(transferMonth) && sessionATHMTransfers != null) {
			if (sessionATHMTransfers.get("transfersReceivedList") != null
					&& sessionATHMTransfers.get("transfersReceivedList").size() > 0) {
				List<TransferInfo> receivedTransfers = sessionATHMTransfers.get("transfersReceivedList");

				for (TransferInfo info : receivedTransfers) {
					int filterMonth = Integer.parseInt(transferMonth.substring(4));
					int filterYear = Integer.parseInt(transferMonth.substring(0, 4));
					Calendar infoDate = AthmUtils.convertStringToCal(info.getDate());
					if (infoDate.get(Calendar.MONTH) == filterMonth && infoDate.get(Calendar.YEAR) == filterYear) {
						filteredReceivedTransfers.add(info);
					}
				}
			}
		} else {
			filteredReceivedTransfers = sessionATHMTransfers.get("transfersReceivedList");
		}
		transfersInfoHash.put("transfersReceivedList", filteredReceivedTransfers);

		return transfersInfoHash;
	}

	@SuppressWarnings("unchecked")
	private HashMap<String, List<TransferInfo>> allTransferInfo(ATHMovilEntitlement entitlement,
			CustomerProfile customerProfile, UserSession userSession) {

		HashMap<String, List<TransferInfo>> allATHMTransfersInfo = new HashMap<String, List<TransferInfo>>();

		AthmTransHistoryReq transHistoryReq = new AthmTransHistoryReq();
		transHistoryReq.setFiID(fiId);
		transHistoryReq.setFiToken(fiToken);
		transHistoryReq.setMaxResults(15);
		transHistoryReq.setUserID(entitlement.getUserId());

		AthmTransHistoryResp transferHistoryResp = customerService.getTransactionHistory(transHistoryReq);

		OsPhoneInfo[] contacts = userSession.getAthmContacts(fiId, fiToken, entitlement.getUserId());
		List<OsPhoneInfo> athmContacts = null;
		List<TransferInfo> outTransfersInfoList = new ArrayList<TransferInfo>();

		if (contacts != null)
			athmContacts = Arrays.asList(contacts);

		if (transferHistoryResp != null && transferHistoryResp.getOutgoingTransHistory() != null
				&& transferHistoryResp.getOutgoingTransHistory().length > 0) {

			OsTransactionInfo[] transfers = transferHistoryResp.getOutgoingTransHistory();
			List<OsTransactionInfo> sentTransfer = null;
			if (transfers != null)
				sentTransfer = Arrays.asList(transfers);

			if (sentTransfer != null) {
				for (OsTransactionInfo osTransferInfo : sentTransfer) {
					TransferInfo transferInfo = new TransferInfo();

					transferInfo.setDate(AthmUtils.formatDate(osTransferInfo.getDate()));

					boolean isFriend = false;
					if (athmContacts != null && !athmContacts.isEmpty()) {
						for (OsPhoneInfo contact : athmContacts) {
							if (AthmUtils.getStringNumericPhoneNumber(contact.getPhoneNumber()).equals(
									AthmUtils.getStringNumericPhoneNumber(osTransferInfo.getTarget()))) {
								transferInfo.setTarget(contact.getName());
								isFriend = true;
								break;
							}
						}
					}
					if (!isFriend) {
						transferInfo.setTarget(AthmUtils.formatPhoneNumber(osTransferInfo.getTarget()));
					}
					transferInfo.setSource(osTransferInfo.getSource());
					transferInfo.setAmount(AthmUtils.formatAmount(new BigDecimal(osTransferInfo.getAmount())));
					transferInfo.setCard(AthmUtils.formatPlasticNumber(osTransferInfo.getSource()));
					transferInfo.setReferenceNumber(osTransferInfo.getReferenceNumber());
					transferInfo.setStatus(osTransferInfo.getTransactionStatus());
					transferInfo.setMessage(AthmUtils.divideLongMessageWord(osTransferInfo.getMessage()));

					outTransfersInfoList.add(transferInfo);
				}
				allATHMTransfersInfo.put("transfersSentList", outTransfersInfoList);
			}

		}

		if (transferHistoryResp != null && transferHistoryResp.getIncomingTransHistory() != null
				&& transferHistoryResp.getIncomingTransHistory().length > 0) {

			OsTransactionInfo[] transfers = transferHistoryResp.getIncomingTransHistory();
			List<OsTransactionInfo> inTransfer = null;
			if (transfers != null)
				inTransfer = Arrays.asList(transfers);

			List<TransferInfo> inTransfersInfoList = new ArrayList<TransferInfo>();

			if (inTransfer != null) {
				for (OsTransactionInfo osTransferInfo : inTransfer) {
					TransferInfo transferInfo = new TransferInfo();

					transferInfo.setDate(AthmUtils.formatDate(osTransferInfo.getDate()));

					boolean isFriend = false;
					if (athmContacts != null && !athmContacts.isEmpty()) {
						for (OsPhoneInfo contact : athmContacts) {
							if (AthmUtils.getStringNumericPhoneNumber(contact.getPhoneNumber()).equals(
									AthmUtils.getStringNumericPhoneNumber(osTransferInfo.getSource()))) {
								transferInfo.setSource(contact.getName());
								isFriend = true;
								break;
							}
						}
					}
					if (!isFriend) {
						transferInfo.setSource(AthmUtils.formatPhoneNumber(osTransferInfo.getSource()));
					}
					transferInfo.setTarget(osTransferInfo.getTarget());
					transferInfo.setAmount(AthmUtils.formatAmount(new BigDecimal(osTransferInfo.getAmount())));
					transferInfo.setCard(AthmUtils.formatPlasticNumber(osTransferInfo.getTarget()));
					transferInfo.setMessage(osTransferInfo.getMessage());
					transferInfo.setReferenceNumber(osTransferInfo.getReferenceNumber());
					transferInfo.setStatus(osTransferInfo.getTransactionStatus());
					transferInfo.setMessage(osTransferInfo.getMessage());
					// Condition in case a giant "word" is entered, the layout
					// does not get affected
					if (osTransferInfo.getMessage() != null && !osTransferInfo.getMessage().contains(" ")
							&& osTransferInfo.getMessage().length() > 24) {
						transferInfo.setMessage(osTransferInfo.getMessage().substring(0, 24) + " "
								+ osTransferInfo.getMessage().substring(24, osTransferInfo.getMessage().length()));
					}
					inTransfersInfoList.add(transferInfo);
				}
				allATHMTransfersInfo.put("transfersReceivedList", inTransfersInfoList);
			}

		}

		return allATHMTransfersInfo;
	}

	private void loadFriendsData(HttpServletRequest request, Object command, Map<String, Object> model) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		ATHMovilEntitlement entitlement = new ATHMovilEntitlement(userSession.getCustomerProfile()
				.getEntitlementByType(EntitlementType.ATHMOVIL));

		OsPhoneInfo[] contactList = userSession.getAthmContacts(fiId, fiToken, entitlement.getUserId());
		if (contactList != null && contactList.length > 0) {
			Map<String, Map<String, String>> friendsInfoMap = new TreeMap<String, Map<String, String>>();
			for (int i = 0; i < contactList.length; i++) {
				OsPhoneInfo friend = contactList[i];
				Map<String, String> details = new HashMap<String, String>();

				details.put("id", Integer.toString(i));
				details.put("name", friend.getName());
				details.put("phone", friend.getPhoneNumber().toString());
				details.put("frontEndPhone", AthmUtils.formatPhoneNumberDashes(friend.getPhoneNumber()));

				friendsInfoMap.put(String.valueOf(i), details);
			}
			model.put("friendsmap", friendsInfoMap);
			model.put("friendsmapSize", friendsInfoMap.size());
		}
	}
	
	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public String getFiId() {
		return fiId;
	}

	public void setFiId(String fiId) {
		this.fiId = fiId;
	}

	public String getFiToken() {
		return fiToken;
	}

	public void setFiToken(String fiToken) {
		this.fiToken = fiToken;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	// MBSFE-691
	public String getInvalidEmailDomains() {
		return invalidEmailDomains;
	}

	public void setInvalidEmailDomains(String invalidEmailDomains) {
		this.invalidEmailDomains = invalidEmailDomains;
	}
	// END MBSFE-691
	
	private void getRSAChallenge(HttpServletRequest request, UserSession userSession) throws Exception {
		// ////////////////////

		String language = Utils.getRSALanguage(request);
		String username = userSession.getUsername();

		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), request);
		RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
		AnalyzeResponse anResp = userSession.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

		deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
		deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

		userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
		userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

		userSession.setDeviceRequest(deviceRequest);

		/*
		 * if (deviceRequest != null && cookie != null){
		 * cookie.setValue(deviceRequest.getDeviceTokenCookie());
		 * cookie.setPath("/"); response.addCookie(cookie); }
		 */
		if (!(ActionCode.CHALLENGE == anResp.getRiskResult().getTriggeredRule().getActionCode())) {
			userSession.setInformationChallenge(true);
		} else {
			String email = null;
			if (userSession.getCustomerProfile() != null)
				email = userSession.getCustomerProfile().getEmail();

			QuestionResponse personalQuestion;

			if (userSession.getInformationQuestion() == null) {
				RSAActionRequest actionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(request,
						userSession.getUsername()), userSession.getUsername(), "", email, 0, userSession.isRemember(),
						userSession.getDeviceRequest());

				if (userSession.getCustomerProfile() != null)
					actionRequest.setUsername(userSession.getCustomerProfile().getUsername());
				else if (userSession.getUsernameReservation() != null)
					actionRequest.setUsername(userSession.getUsernameReservation().getUsername());
				else
					actionRequest.setUsername(username);

				actionRequest.setDeviceRequest(userSession.getDeviceRequest());
				actionRequest.setRsaSessionId(userSession.getRsaSessionId());
				actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());

				actionRequest.setRsaLang(Utils.getRSALanguage(request));

				personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

			} else {
				personalQuestion = new QuestionResponse();
				personalQuestion.setStatusResult(ResultStatus.SUCCESS);
				personalQuestion.setQuestion(userSession.getInformationQuestion());
			}
			if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
				userSession.setInformationQuestion(personalQuestion.getQuestion());

			} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
				throw new ModelAndViewDefiningException(new ModelAndView("error"));
			} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
				throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
			}
		}

		// //////////////////////
	}

	private ModelAndView validateRSA(HttpServletRequest request, HttpServletResponse response, BindException errors) {
		Map<String, Object> model = new LinkedHashMap<>();
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();
		boolean dummyQuestions = (profile == null);

		String answer = request.getParameter("answer");

		if (GenericValidator.isBlankOrNull(answer)) {
			model.put("text", "empty-");
		}

		else {
			RSAActionRequest rsaActionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(request,
					session.getUsername()), session.getUsername(), answer, dummyQuestions ? null : profile.getEmail(),
					0, session.isRemember(), session.getDeviceRequest());
			String username = session.getUsername();
			if (session.getCustomerProfile() != null)
				rsaActionRequest.setUsername(session.getCustomerProfile().getUsername());
			else if (session.getUsernameReservation() != null)
				rsaActionRequest.setUsername(session.getUsernameReservation().getUsername());
			else
				rsaActionRequest.setUsername(username);

			rsaActionRequest.setRsaSessionId(session.getRsaSessionId());
			rsaActionRequest.setRsaTransactionId(session.getRsaTransactionId());
			rsaActionRequest.setQuestion(session.getInformationQuestion());
			rsaActionRequest.setRsaLang(null);
			ValidationQuestionResponse responseAnswer = session.getCustomerService().getSignOnCustomerServiceModule().validateRSAQuestion(
					rsaActionRequest);
			session.setQuestion(null);
			session.setRsaSessionId(responseAnswer.getRsaSessionId());
			session.setRsaTransactionId(responseAnswer.getRsaTransactionId());

			Cookie cookie = WebUtils.getCookie(request, "PMData");
			if (cookie == null)
				cookie = new Cookie("PMData", "");

			cookie.setPath("/");
			cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
			ESAPI.httpUtilities().addCookie(response, cookie);

			if (ResultStatus.SUCCESS.equals(responseAnswer.getStatusResult())) {
				cookie.setPath("/");
				cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
				ESAPI.httpUtilities().addCookie(response, cookie);
				session.setInformationChallenge(true);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ATHM_ACCT_MOD, null);
				model.put("text", "success-");
			} else if (ResultStatus.BLOCKED.equals(responseAnswer.getStatusResult())) {
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ATHM_ACCT_MOD, null);
				/* CIBP-1104 */
				boolean hasalert = false;
				AlertsEntitlement ent = new AlertsEntitlement(session.getCustomerProfile().getEntitlementByType(
						EntitlementType.ALERTS));
				if (ent != null && ent.getAlerts() != null) {
					Set<AlertType> alerts = ent.getAlerts();
					if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
							|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
						hasalert = true;
					}
				}
				/*
				 * CIBP-1104 se activa envio alerta de bloqueo de preguntas
				 * secretas
				 */
				if (hasalert) {
					session.getCustomerService().sendQuestionsBlockedConfirmation(session.getCustomerProfile(),
							session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
							session.getPreferredLanguage());
				}
				/* FIN--- CIBP-1104 */

				// errors.rejectValue("answer", "account.block.message",
				// "Account Blocked");
				CookieUtils.addCookie(response, 60 * 60 * 24 * 365, "username", username);

				model.put("text", "blocked-");
				// return new ModelAndView("forward:login", model);
				// return showForm(request, response, errors);
			} else if (ResultStatus.EXCEPTION.equals(responseAnswer.getStatusResult())) {
				cookie.setPath("/");
				cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
				ESAPI.httpUtilities().addCookie(response, cookie);
				model.put("text", "exception-");
				// return new ModelAndView("forward:login", model);
			} else if (ResultStatus.ERROR.equals(responseAnswer.getStatusResult())) {
				errors.rejectValue("answer", "login.question.error.message", "Invalid answer");
				cookie.setPath("/");
				cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
				ESAPI.httpUtilities().addCookie(response, cookie);

				String email = null;
				if (session.getCustomerProfile() != null)
					email = session.getCustomerProfile().getEmail();

				QuestionResponse personalQuestion;

				RSAActionRequest actionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(request,
						session.getUsername()), session.getUsername(), "", email, 0, session.isRemember(),
						session.getDeviceRequest());

				if (session.getCustomerProfile() != null)
					actionRequest.setUsername(session.getCustomerProfile().getUsername());
				else if (session.getUsernameReservation() != null)
					actionRequest.setUsername(session.getUsernameReservation().getUsername());
				else
					actionRequest.setUsername(username);

				// session.setRsaSessionId(anResp.getIdentificationData().getSessionId());
				// session.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

				actionRequest.setDeviceRequest(session.getDeviceRequest());
				actionRequest.setRsaSessionId(session.getRsaSessionId());
				actionRequest.setRsaTransactionId(session.getRsaTransactionId());

				actionRequest.setRsaLang(Utils.getRSALanguage(request));

				personalQuestion = session.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

				if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
					String question = (String) personalQuestion.getQuestion().values().toArray()[0];
					session.setInformationQuestion(personalQuestion.getQuestion());
					model.put("text", "error-" + question);

				} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
					// throw new ModelAndViewDefiningException(new
					// ModelAndView("error"));
					model.put("text", "exception-");
				} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ATHM_ACCT_MOD, null);
					/* CIBP-1104 */
					boolean hasalert = false;
					AlertsEntitlement ent = new AlertsEntitlement(session.getCustomerProfile().getEntitlementByType(
							EntitlementType.ALERTS));
					if (ent != null && ent.getAlerts() != null) {
						Set<AlertType> alerts = ent.getAlerts();
						if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
								|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
							hasalert = true;
						}
					}
					/*
					 * CIBP-1104 se activa envio alerta de bloqueo de preguntas
					 * secretas
					 */
					if (hasalert) {
						session.getCustomerService().sendQuestionsBlockedConfirmation(session.getCustomerProfile(),
								session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
								session.getPreferredLanguage());
					}
					/* FIN--- CIBP-1104 */

					// errors.rejectValue("answer", "account.block.message",
					// "Account Blocked");
					CookieUtils.addCookie(response, 60 * 60 * 24 * 365, "username", username);

					model.put("text", "blocked-");
				}
			}
		}
		return new ModelAndView("text", model);
	}
}
