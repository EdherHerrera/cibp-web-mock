package evertec.cibp.web.form;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.ContactUsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class ContactUsForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	protected Map<String, String> departmentList = new LinkedHashMap<String, String>();
	protected Map<String, String> categoryList = new LinkedHashMap<String, String>();
	protected Map<String, String> subinfo = new LinkedHashMap<String, String>();
	protected Map<String, String> subclaim = new LinkedHashMap<String, String>();
	protected Map<String, String> subpremia = new LinkedHashMap<String, String>();

	public ContactUsForm() {
		super();
		setCommandClass(ContactUsBean.class);
		setCommandName("contactus");
		setFormView("contactus");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		ContactUsBean bean = (ContactUsBean) command;
		String toname = bean.getToinfo();
		String toemail = null;
		String subcategory = bean.getSubcategory();
		String subject = "";

		if (toname != null) {

			if (toname.equals("premia")) {
				toemail = "premia@bppr.com";
				subject = subpremia.get(subcategory);
			} else if (toname.equals("feedback")) {
				toemail = "feedback@bppr.com";
				subject = departmentList.get(toname);
			}
			if (toname.equals("info")) {
				toemail = "internet@bppr.com";
				subject = subinfo.get(subcategory);
			} else if (toname != null && toname.equals("claim")) {
				toemail = "internet@bppr.com";
				subject = subclaim.get(subcategory);
			} else if (toname.equals("Popular Securities"))
				toemail = "securities@bppr.com";
			else if (toname.equals("Popular Mortgage"))
				toemail = "pm-web@popularmortgagepr.com";
			else if (toname.equals("Popular Auto"))
				toemail = "leasing@popularautopr.com";
			else if (toname.equals("Popular Insurance"))
				toemail = "popularinsurance@bppr.com";
			else
				toemail = "internet@bppr.com";
		} else
			toemail = "internet@bppr.com";

		if (subject == null || subject.equals("none") || subject.equals("")) {
			subject = departmentList.get(toname);
		}

		if (toname != null) {
			if (toname.equals("info")) {
				subject = " 1 - " + subject;
			} else if (toname.equals("claim")) {
				subject = " 2 - " + subject;
			}
		}

		String body = bean.getMessage();
		TVFrontendAccount account = Utils.getAccountById(bean.getAccount(), userSession.getAccounts());

		String date = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, Locale.US).format(new Date());
		String telephone = bean.getTelephone1() + bean.getTelephone2() + bean.getTelephone3();
		String subtype = "";
		if (account.getAccountNumberSuffix() != null
				&& (account.getAccountNumberSuffix().equalsIgnoreCase("SAV") || account.getAccountNumberSuffix()
						.equalsIgnoreCase("RES"))) {
			subtype = " " + account.getAccountNumberSuffix();
		}
		String message = "";
		// mask accounts
		AccountMaskUtil utils = new AccountMaskUtil();
		String permId = userSession.getCustomerProfile().getPermId().toString();
		message = "\nEnrollment Account Number:" + utils.mask(profile.getEnrollmentAccountNumber())
				+ "\nAccount Number: " + utils.mask(account.getAccountNumber()) + subtype + "\nDate: " + date
				+ "\nTel: " + telephone + "\nCIS:  " + permId.substring(0, permId.length() - 6) + "\n";

		userSession.getCustomerService().sendKanaEmail(profile, toname, toemail, message + body,
				userSession.getPreferredLanguage(), subject);
		Map<String, Object> model = new HashMap<>();
		model.put("confirm", true);

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return showForm(request, response, error, model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		model.put("customer", userSession.getCustomerProfile());

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		List<TVFrontendAccount> userAccounts = userSession.getAccounts();
		AccountMaskUtil mask = new AccountMaskUtil();
		Map<String, String> stmtAccountsMap = new LinkedHashMap<String, String>();

		for (Iterator<TVFrontendAccount> iter = userAccounts.iterator(); iter.hasNext();) {
			TVFrontendAccount element = (TVFrontendAccount) iter.next();
			if (element.getFeatures().isShowStatement() && !element.getHide()) {
				String key = StringUtils.abbreviate(element.getNickname(), 15)
						+ " "
						+ mask.mask(element.getAccountNumber())
						+ ((element.getAccountNumberSuffix() != null && !element.getAccountNumberSuffix().equals("")) ? " "
								+ element.getAccountNumberSuffix()
								: "");
				if (element.getPortalBalance() != null) {
					if (element.getProductDefinition().getProductType().equals("PRM"))
						key = key + " " + new NumberTool().format("integer", element.getPortalBalance());
					else
						key = key + " " + new NumberTool().format("currency", element.getPortalBalance());
				}

				stmtAccountsMap.put(String.valueOf(element.getFrontEndId()), key);
			}
		}
		model.put("stmtAccountsMap", stmtAccountsMap);
		boolean isEng = Utils.getRSALanguage(request).equalsIgnoreCase("EN");

		departmentList = new LinkedHashMap<String, String>();
		if (isEng)
			departmentList.put("info", "Information about our services");
		else
			departmentList.put("info", "Orientaci&oacute;n sobre nuestros servicios");
		if (isEng)
			departmentList.put("claim", "Claims");
		else
			departmentList.put("claim", "Reclamaciones");
		if (isEng)
			departmentList.put("feedback", "Feedback");
		else
			departmentList.put("feedback", "Sugerencias");
		if (isEng)
			departmentList.put("premia", "Premia");
		else
			departmentList.put("premia", "Premia");
		departmentList.put("Popular Securities", "Popular Securities");
		departmentList.put("Popular Mortgage", "Popular Mortgage");
		departmentList.put("Popular Auto", "Popular Auto");
		departmentList.put("Popular Insurance", "Popular Insurance");
		model.put("departmentList", departmentList);

		subinfo = new LinkedHashMap<String, String>();
		if (isEng)
			subinfo.put("none", "Select one");
		else
			subinfo.put("none", "Selecciona una");
		if (isEng)
			subinfo.put("addr", "Address Change");
		else
			subinfo.put("addr", "Cambio de Direcci&oacute;n");
		if (isEng)
			subinfo.put("close", "Account Closing");
		else
			subinfo.put("close", "Cerrar cuentas");
		if (isEng)
			subinfo.put("login", "Internet Banking log in information");
		else
			subinfo.put("login", "Informaci&oacute;n de conexi&oacute;n a banca por internet");
		if (isEng)
			subinfo.put("mobile", "Mobile Banking");
		else
			subinfo.put("mobile", "Banca M&oacute;vil");
		if (isEng)
			subinfo.put("ebill", "e-Bills and e-Statements");
		else
			subinfo.put("ebill", "e-Bills y e-Statements");
		if (isEng)
			subinfo.put("pmts", "Payments and Transfers");
		else
			subinfo.put("pmts", "Pagos y Transferencias ");
		if (isEng)
			subinfo.put("other", "Other");
		else
			subinfo.put("other", "Otro");
		model.put("subinfo", subinfo);

		subclaim = new LinkedHashMap<String, String>();
		if (isEng)
			subclaim.put("none", "Select one");
		else
			subclaim.put("none", "Selecciona una");
		if (isEng)
			subclaim.put("nocredit", "Unaccredited transactions");
		else
			subclaim.put("nocredit", "Transacciones no acreditadas");
		if (isEng)
			subclaim.put("noauth", "Unauthorized charges");
		else
			subclaim.put("noauth", "Cargos no autorizados");
		if (isEng)
			subclaim.put("duplicate", "Duplicated transactions");
		else
			subclaim.put("duplicate", "Transacci&oacute;n duplicada");
		if (isEng)
			subclaim.put("atm", "ATMs");
		else
			subclaim.put("atm", "Cajeros Autom&aacute;ticos");
		if (isEng)
			subclaim.put("error", "Internet Banking errors");
		else
			subclaim.put("error", "Errores en Banca por internet");
		if (isEng)
			subclaim.put("other", "Other");
		else
			subclaim.put("other", "Otro");
		model.put("subclaim", subclaim);

		subpremia = new LinkedHashMap<String, String>();
		if (isEng)
			subpremia.put("none", "Select one");
		else
			subpremia.put("none", "Selecciona una");
		if (isEng)
			subpremia.put("member", "Membership and fee");
		else
			subpremia.put("member", "Membres&iacute;a y cuota");
		if (isEng)
			subpremia.put("redempt", "Points redemption");
		else
			subpremia.put("redempt", "Redenci&oacute;n de puntos");
		if (isEng)
			subpremia.put("info", "General information");
		else
			subpremia.put("info", "Informaci&oacute;n general");
		model.put("subpremia", subpremia);

		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		String requiredField = "Requerido";
		String invalidField = "Inv&aacute;lido";

		boolean isEng = Utils.getRSALanguage(request).equalsIgnoreCase("EN");

		if (isEng) {
			requiredField = "Required";
			invalidField = "Invalid";
		}

		ContactUsBean bean = (ContactUsBean) command;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "toinfo", "", requiredField);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "account", "", requiredField);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "message", "", requiredField);

		if (!GenericValidator.matchRegexp(bean.getTelephone1(), "^\\d{3}$")
				|| !GenericValidator.matchRegexp(bean.getTelephone2(), "^\\d{3}$")
				|| !GenericValidator.matchRegexp(bean.getTelephone3(), "^\\d{4}$"))
			errors.rejectValue("telephone3", "", invalidField);

	}
}
