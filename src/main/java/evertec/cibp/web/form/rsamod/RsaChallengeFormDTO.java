package evertec.cibp.web.form.rsamod;

import java.util.Objects;

/**
 * @author Saul Castro
 * @since 6/27/2022
 */
public class RsaChallengeFormDTO {

    private String sq1;
    private String ans1;
    private String cans1;
    private String sq2;
    private String ans2;
    private String cans2;
    private String sq3;
    private String ans3;
    private String cans3;
    private String typeDevice;
    private String _;

    public String getSq1() {
        return sq1;
    }

    public RsaChallengeFormDTO setSq1(String sq1) {
        this.sq1 = sq1;
        return this;
    }

    public String getAns1() {
        return ans1;
    }

    public RsaChallengeFormDTO setAns1(String ans1) {
        this.ans1 = ans1;
        return this;
    }

    public String getCans1() {
        return cans1;
    }

    public RsaChallengeFormDTO setCans1(String cans1) {
        this.cans1 = cans1;
        return this;
    }

    public String getSq2() {
        return sq2;
    }

    public RsaChallengeFormDTO setSq2(String sq2) {
        this.sq2 = sq2;
        return this;
    }

    public String getAns2() {
        return ans2;
    }

    public RsaChallengeFormDTO setAns2(String ans2) {
        this.ans2 = ans2;
        return this;
    }

    public String getCans2() {
        return cans2;
    }

    public RsaChallengeFormDTO setCans2(String cans2) {
        this.cans2 = cans2;
        return this;
    }

    public String getSq3() {
        return sq3;
    }

    public RsaChallengeFormDTO setSq3(String sq3) {
        this.sq3 = sq3;
        return this;
    }

    public String getAns3() {
        return ans3;
    }

    public RsaChallengeFormDTO setAns3(String ans3) {
        this.ans3 = ans3;
        return this;
    }

    public String getCans3() {
        return cans3;
    }

    public RsaChallengeFormDTO setCans3(String cans3) {
        this.cans3 = cans3;
        return this;
    }

    public String getTypeDevice() {
        return typeDevice;
    }

    public RsaChallengeFormDTO setTypeDevice(String typeDevice) {
        this.typeDevice = typeDevice;
        return this;
    }

    public String get_() {
        return _;
    }

    public RsaChallengeFormDTO set_(String _) {
        this._ = _;
        return this;
    }

    @Override
    public String toString() {
        return "RsaChallengeFormDTO{" +
                "sq1='" + sq1 + '\'' +
                ", ans1='" + ans1 + '\'' +
                ", cans1='" + cans1 + '\'' +
                ", sq2='" + sq2 + '\'' +
                ", ans2='" + ans2 + '\'' +
                ", cans2='" + cans2 + '\'' +
                ", sq3='" + sq3 + '\'' +
                ", ans3='" + ans3 + '\'' +
                ", cans3='" + cans3 + '\'' +
                ", typeDevice='" + typeDevice + '\'' +
                ", _='" + _ + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof RsaChallengeFormDTO) {
            RsaChallengeFormDTO that = (RsaChallengeFormDTO) o;
            return Objects.equals(sq1, that.sq1) && Objects.equals(ans1, that.ans1) && Objects.equals(cans1, that.cans1) && Objects.equals(sq2, that.sq2) && Objects.equals(ans2, that.ans2) && Objects.equals(cans2, that.cans2) && Objects.equals(sq3, that.sq3) && Objects.equals(ans3, that.ans3) && Objects.equals(cans3, that.cans3) && Objects.equals(typeDevice, that.typeDevice) && Objects.equals(_, that._);
        }
        return Boolean.FALSE;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sq1, ans1, cans1, sq2, ans2, cans2, sq3, ans3, cans3, typeDevice, _);
    }

}
