package evertec.cibp.web.form.rsamod;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.EventType;
import com.rsa.csd.ws.SoapFaultType;
import com.rsa.csd.ws.UpdateUserRequest;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerActions;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.interaction.CustomerInteraction;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.oob.CustomerOOBChallengeType;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.RSAEntitlement;
import evertec.cibp.core.models.entitlement.SMSEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.DeleteUserRSARequest;
import evertec.cibp.core.services.requests.ModifyRSAEntitlementRequest;
import evertec.cibp.core.services.requests.SMSProfileModifyRequest;
import evertec.cibp.core.services.requests.SendSmsGeneratedCodeRequest;
import evertec.cibp.core.services.requests.SmsEnrollUserRequest;
import evertec.cibp.core.services.requests.adaptiveauth.ChallengeOobRequest;
import evertec.cibp.core.services.requests.adaptiveauth.EnrollmentInformationinRSARequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RsaChallengeRequest;
import evertec.cibp.core.services.requests.adaptiveauth.ValidateOobChallengeRequest;
import evertec.cibp.core.services.responses.OobRecoveryCodeStatusResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.RsaQuestionsUpdateResponse;
import evertec.cibp.core.services.responses.SmsAlertsResponse;
import evertec.cibp.core.services.responses.SmsAlertsResponse.Result;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.ValidationQuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.ChallengeOobResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionSelectionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.RsaOobResponse;
import evertec.cibp.core.utils.ApplicationsDefinition;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.ChallengeTypeOOBForm;
import evertec.cibp.web.form.RecoveryCodeOOBForm;
import evertec.cibp.web.form.SmsOOBForm;
import evertec.cibp.web.form.bean.ChangeOOBBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import evertec.rsa.RSAConnector.Lang;

/**
 * RSA Mobile Redesign
 *
 * @author Misael Rodriguez
 */
public class RSAChallengeForm extends MultiActionController {

	private static final String MESSAGE_ERROR = "messageError";

	private static final String REQUIRED_SPANISH = "Requerido";

	private static final String REQUIRED_ENGLISH = "Required";

	private static final String SPANISH_LOWERCASE = "es";

	private static final String SUCCESS = "success";

	private static final String ONE_TIME_CODE = "oneTimeCode";

	private static final String INSCRIPCION_2_PASOS_EXITOSAMENTE = "Tu inscripci&oacute;n de Verificaci&oacute;n de 2 Pasos ha sido completada exitosamente.";

	private static final String ENROLLMENT_2_STEP_VERIFICATION__SUCCESSFULLY = "Your enrollment in 2 Step Verification has been completed successfully.";

	private static final String ALLOW = "ALLOW";

	private static final String DEVICE_TOKEN = "devicetoken";

	private static final String TRUE_LOWERCASE = "true";

	private static final String ERROR_MESSAGE_SMS_UNABLE = "rsa.error.message.sms.unable";

	private static final String ENGLISH_LOWERCASE = "en";

	private static final String PRIMARY_NUMBER = "primaryNumber";

	private static final String TIME_INTERVAL = "timeInterval";

	private static final String RSA_ENTITLEMENT_MISSING_FOR_USER = "RSA Entitlement missing for user ";

	private static final String AFTER_RECOVERY_CODE_ENTRY = " after recovery code entry.";

	private static final String ERROR_DELETING_RSA_USER = "Error deleting RSA user ";

	private static final String GENERATE_SMS_OR_CALL = "N";

	private static final String MESSAGE_REQUEST_NEW_CALL = "rsa.text.requestNewCall";

	private static final String PRIMARY_PHONE = "primaryPhone";

	private static final String ADD_PHONE_NUMBER = "addPhoneNumber";

	private static final String ALTERNATE_PROVIDER = "alternateProvider";

	private static final String ALTERNATE_PHONE = "alternatePhone";

	private static final String FREQUENCY2 = "frequency";

	private static final String CHALLENGE_TYPE = "challengeType";

	private static final String NO_USER_SESSION = "No user session";

	private static final String FAIL = "fail";

	private static final String TEXT = "text";

	private static final String ENROLL_OOB_REGISTER_SMS_ERRORR = "enrollOOB.register.sms.errorr";

	private static final String ENROLL_OOB_REGISTER_SMS_ERROR = "enrollOOB.register.sms.error";

	private static final String FORWARD_LOGIN = "forward:login";

	private static final String SECURITY_QUESTION = "securityQuestion";

	private static final String ERROR = "error";

	private static final String EN = "EN";

	private static final String BLOCKED_BY_OOB_CALL_CHALLENGE = " blocked by OOB call challenge";

	private static final String RSA_ERROR_MESSAGE_CALL_UNABLE = "rsa.error.message.call.unable";

	private static final String N = "N";

	private static final String LOGIN_OOB_REC_CODE_ERROR_MESSAGE = "login.oob.recCodeError.message";

	private static final String ERROR_MESSAGE_RECOVERYCODE_BLOCK = "rsa.error.message.recoverycode.block";

	private static final String MESSAGE_CODE_SENT = "rsa.message.codeSent";

	private static final String RESULT = "result";

	private static final String MESSAGES_CORRECT_CODE = "rsa.messages.correctCode";

	private static final String ERROR_OOB_CALL = "rsa.error.oob.call";

	private static final String BLOCKED_BY_OOB_CHALLENGE_TRIES = " blocked by OOB challenge tries";

	private static final String ERROR_MESSAGE_CODE = "rsa.error.message.code";

	private static final String ERROR_MESSAGE_CODE_EXPIRED = "rsa.error.message.code.expired";

	private static final String ERROR_MESSAGE_CODE_VALIDATION = "rsa.error.message.code.validation";

	private static final int _4_0_9 = 409;

	private static final String CHALLENGE_METHOD = "challengeMethod";

	private static final String IS_OOB_AUTHENTICATED = "isOobAuthenticated";

	private static final String VM_RSA_EDIT_QUESTIONS = "vm/rsaEditQuestions.vm";

	private static final String VM_RSA_EDIT_OOB = "vm/rsaEditOOB.vm";

	private static final String VM_RSA_QUESTION = "vm/rsaQuestion.vm";

	private static final String VM_RSA_ENROLL = "vm/rsaEnroll.vm";

	private static final String VM_RSA_NON_CODES = "vm/rsaNonCodes.vm";

	private static final String VM_RSA_MESSAGE = "vm/rsaMessage.vm";

	private static final String VM_RSA_RECOVERY_CODE = "vm/rsaRecoveryCode.vm";

	private static final String VM_RSA_OTHER = "vm/rsaOther.vm";

	private static final String VM_RSA_CALL_ALT = "vm/rsaCallAlt.vm";

	private static final String VM_RSA_SMS_ALT = "vm/rsaSMSAlt.vm";

	private static final String VM_RSA_CALL = "vm/rsaCall.vm";

	private static final String VM_RSA_SMS = "vm/rsaSMS.vm";

	private static final String IS_MOBILE = "isMobile";

	/** Indicator to redirect to the login screen */
	private static final String REDIRECT_TO_LOGIN = "redirectLogin";

	/** Logger */
	public final Log log = LogFactory.getLog(getClass());

	/** Block by RSA */
	private boolean redirectToBlock = false;

	/** RSA does not answer */
	private boolean redirectToLogin = false;

	/** User RSA */
	private static final String RSA_USR = "RSA User: ";

	/** Customer Service */
	private CustomerServices customerService;

	/** Get Messages */
	private MessageSource messageSource;

	/** Recovery Code block 60 minutes*/
	private int recCodeBlockLength;

	/** Recovery Code maximum attempts */
	private int recCodeMaxRetries;

	/** Wait time in messages */
	private int timeInterval;

	/** Recovery Code retry 60 minutes */
	private int recCodeRetryTimePeriod;

	private static final String USRSSESION = "userSession";
	private static final String RSAUIFORM = "rsaForm";
	private static final String OOB_CODE = "oobCode";
	private static final String CODEUSR = "codeUsr";

	/** Recovery Code by user*/
	private static final String RECOVERY_CODE_USR = "recoveryCodeUsr";
	private static final String PHONE = "phone";
	private static final String RSA_MESSAGE = "message";
	private static final String SMS_TYPE = "sms";

	/** ÒOB Call */
	private static final String CALL_TYPE = "call";
	private static final String SMS_ALT_TYPE = "smsAlt";
	private static final String CALL_ALT_TYPE = "callAlt";
	private static final String OTHER_TYPE = "other";
	private static final String RECOVERY_CODE_TYPE = "recoveryCode";
	private static final String JSON_TYPE = "json";
	private static final String NON_CODES = "nonCodes";
	private static final String ENROLL = "enroll";
	private static final String ENROLL_MOBILE = "enrollM";
	private static final String QUESTION = "question";
	private static final String OOBEDIT = "oob";
	private static final String LAYOUT_TYPE = "rsalayout";
	private static final String FORM_LAYOUT = "layout/rsa-layout.vm";
	private static final String JSON_LAYOUT = "layout/json/json-layout.vm";
	private static final String VIEW_PATH = "view";
	private static final String HAS_ALT_PHONE = "hasAltPhone";

	private static final String ONSEN_LIBS = "onsenUI";
	private static final String EDIT_RSA_PAGE = "rsaEdit";
	private static final String USRAGENT = "user-agent";
	private static final String RSA_EDIT_QUESTIONS = "rsaEditQuestions";
	private static final String LANGUAGE = "language";

	private static final Map<String, String> views = setViews();
	private static final String PROVIDER_ARRAY = "US3,US4,PR7,US5,PR1";

	/** Next page after the oob correct answer  */
	private static final String NEXT_SCREEN = "nextScreen";
	private static final String FOR_ALT_PHONE = "isAltPhone";

	/** Default Carrier **/
	private static final String DEFAULT_CARRIER = "NOC";
	private static final String MBSD3686 = "MBSD3686";
	private static final String DEFAULT_CARRIER_VAR = "defCarrier";
	private static enum ChallengeType {
		OOBSMS, OOBPHONE, QUESTIONS
	}

	private String phone_ = "";

	private static final String NAME_FUNCTION = "function";

	private Locale lastLang; // last language for user session

	private static final String CANCEL_SCREEN = "cancelButton";

    private static final String REQUEST_POST_METHOD = "POST";
	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView initRsa(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> model = new HashMap<>(); //model result
		final UserSession userSession = getRequestUserSession(request); //user session

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99
		
		boolean redirect = false;
		final String errorMessage = "";
		String method = getDisplayChallenge(getChallengeMethod(request, userSession));
		String nextScreen = ServletRequestUtils.getStringParameter(request, NEXT_SCREEN);
		String newCode = ServletRequestUtils.getStringParameter(request, "newCode");
		final String cancelButton = ServletRequestUtils.getStringParameter(request, CANCEL_SCREEN);
		final String function = ServletRequestUtils.getStringParameter(request, NAME_FUNCTION);

		if (!(newCode != null && newCode.equalsIgnoreCase(GENERATE_SMS_OR_CALL)))
			validateQuestionOption(request, model, userSession, errorMessage, method);
		else {
			final String phoneMask = ServletRequestUtils.getStringParameter(request, PHONE);
			model.put(PHONE, phoneMask);
		}

		model.put(LAYOUT_TYPE, FORM_LAYOUT);
		model.put(VIEW_PATH, views.get(method));
		model.put(NEXT_SCREEN, nextScreen);
		model.put(CANCEL_SCREEN,cancelButton);
		model.put(NAME_FUNCTION, function);
		if (!errorMessage.isEmpty())
			model.put(RSA_MESSAGE, errorMessage);

		if (redirectToLogin || redirectToBlock) {
			redirect = true;
			model.put(USRSSESION, userSession.getUsername());
			redirectToBlock = false;
			redirectToLogin = false;
		}
		model.put(REDIRECT_TO_LOGIN, redirect);
		model.put(IS_MOBILE, isMobile(userSession));
		model.put(CHALLENGE_METHOD, method);
		return new ModelAndView(RSAUIFORM, model);
	}

	private void validateQuestionOption(HttpServletRequest request, Map<String, Object> model, UserSession userSession, String errorMessage, String method) throws ModelAndViewDefiningException, RemoteException {
		if (method.equalsIgnoreCase(QUESTION)) {
			QuestionResponse personalQuestion = null;
			personalQuestion = getRsaQuestion(request, userSession);
			userSession.setInformationChallenge(false);
			if (personalQuestion != null && ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
				userSession.setInformationQuestion(personalQuestion.getQuestion());

				String question = "";
				if(userSession.getInformationQuestion().values().toArray()[0] instanceof String) {
					question = (String) userSession.getInformationQuestion().values().toArray()[0];
					model.put(SECURITY_QUESTION, question);
				}

			} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())
					|| ResultStatus.EXCEPTION.equals(personalQuestion.getStatusResult())) {
				throw new ModelAndViewDefiningException(new ModelAndView(ERROR));
			} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
				throw new ModelAndViewDefiningException(new ModelAndView(FORWARD_LOGIN));
			}
			model.put(DEVICE_TOKEN, userSession.getDeviceRequest().getDeviceTokenCookie());
		} else {
			generateCode(request, model, method, errorMessage, false);
		}
	}


	private static Map<String, String> setViews() {
		Map<String, String> views = new HashMap<>();
		views.put(SMS_TYPE, VM_RSA_SMS);
		views.put(CALL_TYPE, VM_RSA_CALL);
		views.put(SMS_ALT_TYPE, VM_RSA_SMS_ALT);
		views.put(CALL_ALT_TYPE, VM_RSA_CALL_ALT);
		views.put(OTHER_TYPE, VM_RSA_OTHER);
		views.put(RECOVERY_CODE_TYPE, VM_RSA_RECOVERY_CODE);//Pantalla recovery code
		views.put(JSON_TYPE, VM_RSA_MESSAGE);
		views.put(NON_CODES, VM_RSA_NON_CODES);
		views.put(ENROLL, VM_RSA_ENROLL);
		views.put(ENROLL_MOBILE, VM_RSA_ENROLL);
		views.put(QUESTION, VM_RSA_QUESTION);
		views.put(OOBEDIT, VM_RSA_EDIT_OOB);
		views.put(RSA_EDIT_QUESTIONS, VM_RSA_EDIT_QUESTIONS);
		return views;
	}

	/*
	 * RSA challenge (OOB) ---------------------------------------------------------
	 */

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView verifyRsaCode(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		final String CALL_TYPE = RSAChallengeForm.CALL_TYPE;
		final String SMS_TYPE = RSAChallengeForm.SMS_TYPE;
		UserSession userSession = getRequestUserSession(request);
		final CustomerServices customerService = userSession.getCustomerService();
		final CustomerProfile customerProfile = userSession.getCustomerProfile();
		final String userAgent = request.getHeader(USRAGENT);
		final MessageSource messageSource = this.messageSource;
		final Locale language= RequestContextUtils.getLocale(request);
		String customerDevices = "";
		if(!userAgent.isEmpty()) {
			customerDevices = DeviceUtils.getCustomerDevice(userAgent).toString();
		}

		final String codeEnter = request.getParameter(CODEUSR);
		final String method = getRsaChallengeMethod(request);

		// Validate SMS & voice call challenges
		if (method.equalsIgnoreCase(SMS_TYPE) || method.equalsIgnoreCase(CALL_TYPE)) {

			ValidateOobChallengeRequest validateRequest = new ValidateOobChallengeRequest();
			validateRequest.setUsername(userSession.getUsername());
			validateRequest.setDeviceRequest(userSession.getDeviceRequest());
			validateRequest.setRsaSessionId(userSession.getRsaSessionId());
			validateRequest.setRsaTransactionId(userSession.getRsaTransactionId());
			validateRequest.setDeviceTokenCookie(userSession.getDeviceRequest().getDeviceTokenCookie());
			validateRequest.setAnswer(codeEnter);

			if (method.equalsIgnoreCase(SMS_TYPE)){
				validateRequest.setCredentialType(CredentialType.USER_DEFINED);
				validateRequest.setBindDevice(true);
			}else {
				validateRequest.setCredentialType(CredentialType.OOBPHONE);
				validateRequest.setBindDevice(false);
				validateRequest.setLanguage(getLanguage(request));
			}
			redirectToBlock = false;
			final ResultStatus statusResult = customerService.rsaValidateOobChallenge(validateRequest);

			userSession.getDeviceRequest().setDeviceTokenCookie(validateRequest.getDeviceTokenCookie());
			userSession.getDeviceRequest().setDeviceTokenFSO(validateRequest.getDeviceTokenCookie());
			if (statusResult != null) {
				if (!ResultStatus.RSA_ALLOW.equals(statusResult)) {
					final CustomerProfileService customerProfileService = customerService.getCustomerProfileService();
					if (ResultStatus.RSA_UNABLE.equals(statusResult)) {
						model.put(RSA_MESSAGE, messageSource.getMessage(ERROR_MESSAGE_CODE_VALIDATION, null, language));
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754) && userSession.isOobLogin()) {
							customerProfileService.failSignon(customerProfile, CustomerInteraction.OBB_ERROR, customerDevices);
						}
					} else if (ResultStatus.RSA_EXPIRED.equals(statusResult)) {
						model.put(RSA_MESSAGE, messageSource.getMessage(ERROR_MESSAGE_CODE_EXPIRED, null, language));
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754) && userSession.isOobLogin()) {
							customerProfileService.failSignon(customerProfile, CustomerInteraction.OBB_ERROR, customerDevices);
						}
					} else if (ResultStatus.RSA_BLOCKED.equals(statusResult)) {
						redirectToBlock = true;
						model.put(RESULT, ResultStatus.BLOCKED.toString());
						log.info(RSA_USR + userSession.getUsername() + BLOCKED_BY_OOB_CHALLENGE_TRIES);
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754) && userSession.isOobLogin()) {
							customerProfileService.failSignon(customerProfile, CustomerInteraction.RSA_BLOCKED, customerDevices);
						}
					} else if (ResultStatus.RSA_FAILED.equals(statusResult)) {
						if (method.equalsIgnoreCase(SMS_TYPE)) {
							model.put(RSA_MESSAGE, messageSource.getMessage(ERROR_MESSAGE_CODE, null, language));
							if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754) && userSession.isOobLogin()) {
								customerProfileService.failSignon(customerProfile, CustomerInteraction.OBB_ERROR, customerDevices);
							}
						} else {
							model.put(RSA_MESSAGE, messageSource.getMessage(ERROR_OOB_CALL, null, language));
							if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754) && userSession.isOobLogin()) {
								customerProfileService.failSignon(customerProfile, CustomerInteraction.OBB_ERROR, customerDevices);
							}
						}
					}
				}else {
					model.put(RESULT, true);
					model.put(RSA_MESSAGE, messageSource.getMessage(MESSAGES_CORRECT_CODE, null, language));
					//If the user did the OOB challenge and it was success, set flag to true
					userSession.setOobAuthenticated(true);
					log.info("USER OOB AUTHENTICATED:" + userSession.isOobAuthenticated());
				}
			}
		}
		final boolean isMobile = isMobile(userSession);
		if (isMobile) {
			model.put(IS_MOBILE,isMobile);
			model.put(ONSEN_LIBS, true);
			model.put(LAYOUT_TYPE, JSON_LAYOUT);
			model.put(VIEW_PATH, views.get(JSON_TYPE));
		} else {
			model.put(LAYOUT_TYPE, FORM_LAYOUT);
			model.put(VIEW_PATH, views.get(SMS_TYPE));
		}
		return new ModelAndView(RSAUIFORM, model);
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView sendNewCode(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		final String errorMessage="";
		final String forAltPhone = ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE);
		final boolean isAltPhone = (GenericValidator.isBlankOrNull(forAltPhone)) ? false : true;
		generateCode(request, model,SMS_TYPE, errorMessage, isAltPhone);
		final Locale language= RequestContextUtils.getLocale(request);
		final String finalErrorMessage = errorMessage.isEmpty() ? messageSource.getMessage(MESSAGE_CODE_SENT, null, language) : errorMessage;
		model.put(RSA_MESSAGE, finalErrorMessage);
		if (redirectToLogin || redirectToBlock) {
			model.put(RESULT, ResultStatus.BLOCKED.toString());
			redirectToBlock = false;
			redirectToLogin = false;
		} else {
			model.put(RESULT, true);
		}

		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		return new ModelAndView(RSAUIFORM, model);
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView requestNewCall(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		final UserSession userSession = getRequestUserSession(request);
		final String errorMessage="";
		response.getLocale();
		final String forAltPhone = ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE);
		final boolean isAltPhone = (GenericValidator.isBlankOrNull(forAltPhone)) ? false : true;
		generateCode(request,model, CALL_TYPE, errorMessage, isAltPhone);
		final Locale language= RequestContextUtils.getLocale(request);
		final String finalErrorMessage = errorMessage.isEmpty() ? messageSource.getMessage(MESSAGE_REQUEST_NEW_CALL, null, language) : errorMessage;
		model.put(RSA_MESSAGE, finalErrorMessage);
		if (redirectToLogin || redirectToBlock) {
			model.put(USRSSESION, userSession.getUsername());
			model.put(RESULT, ResultStatus.BLOCKED.toString());
			redirectToBlock = false;
			redirectToLogin = false;
		} else {
			model.put(RESULT, true);
		}
		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		model.put(IS_MOBILE,isMobile(userSession));
		return new ModelAndView(RSAUIFORM, model);
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView nonMainPhone(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		final UserSession userSession = getRequestUserSession(request);
		final CustomerProfile profile = userSession.getCustomerProfile();
		final CustomerEntitlement rsaEnt = profile.getEntitlementByType(EntitlementType.RSA);
		final String altPhone = rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE);
		final String function = ServletRequestUtils.getStringParameter(request, NAME_FUNCTION);
		model.put(NAME_FUNCTION,function);
		final String nextScreen = ServletRequestUtils.getStringParameter(request, NEXT_SCREEN);
		model.put(NEXT_SCREEN,nextScreen);
		final String phoneMask = ServletRequestUtils.getStringParameter(request, PHONE);
		model.put(PHONE,phoneMask);
		model.put(HAS_ALT_PHONE, altPhone != null && !altPhone.equalsIgnoreCase(N));
		//this line will be erased when rsa works properly
		model.put(LAYOUT_TYPE, FORM_LAYOUT);
		model.put(VIEW_PATH, views.get(OTHER_TYPE));

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return new ModelAndView(RSAUIFORM, model);
	}


	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView recoveryCode(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		response.getLocale();
		final UserSession userSession = getRequestUserSession(request);
		final CustomerProfile profile = userSession.getCustomerProfile();
		final CustomerEntitlement rsaEnt = profile .getEntitlementByType(EntitlementType.RSA);
		final String altPhone = rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE);
		model.put(HAS_ALT_PHONE, altPhone != null && !altPhone.equalsIgnoreCase(N));


		//this line will be erased when rsa works properly
		model.put(HAS_ALT_PHONE, true);
		model.put(LAYOUT_TYPE, FORM_LAYOUT);
		model.put(VIEW_PATH, views.get(RECOVERY_CODE_TYPE));

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return new ModelAndView(RSAUIFORM, model);
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView nonCodes(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> model = new HashMap<>(); //model result
		final UserSession userSession = getRequestUserSession(request); //user session
		final String function = ServletRequestUtils.getStringParameter(request, NAME_FUNCTION);
		model.put(NAME_FUNCTION,function);
		final String nextScreen = ServletRequestUtils.getStringParameter(request, NEXT_SCREEN);
		model.put(NEXT_SCREEN,nextScreen);
		final String phoneMask = ServletRequestUtils.getStringParameter(request, PHONE);
		model.put(PHONE,phoneMask);
		model.put(LAYOUT_TYPE, FORM_LAYOUT);
		model.put(VIEW_PATH, views.get(NON_CODES));

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return new ModelAndView(RSAUIFORM, model);
	}

	/**
	 * Validate if
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView verifyRecoveryCode(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		boolean redirect = false;
		response.getLocale();
		final UserSession userSession = getRequestUserSession(request);
		final CustomerProfile profile = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		final String userAgent = request.getHeader("user-agent");
		String customerDevices = "";
		final Log log = LogFactory.getLog(getClass());
		if(!userAgent.isEmpty()) {
			customerDevices = DeviceUtils.getCustomerDevice(userAgent).toString();
		}
		final String codeEnter = ServletRequestUtils.getStringParameter(request, RECOVERY_CODE_USR);
		if (profile.hasEntitlement(EntitlementType.RSA)) {
			final CustomerEntitlement rsaEnt = profile.getEntitlementByType(EntitlementType.RSA);
			customerService = userSession.getCustomerService();
			final OobRecoveryCodeStatusResponse recCodeResponse = customerService.getOOBRecoveryCodeStatus(profile, getRecCodeBlockLength());
			final Locale language= RequestContextUtils.getLocale(request);
			if (recCodeResponse != null && recCodeResponse.isBlocked())
				model.put(RSA_MESSAGE, messageSource.getMessage(ERROR_MESSAGE_RECOVERYCODE_BLOCK, new Object[] { recCodeResponse.getRemainingMinutes() }, language));
			else if (validateRecoveryCode(codeEnter, rsaEnt)) {
				DeleteUserRSARequest deleteUserRSARequest = new DeleteUserRSARequest(
						userSession.getCustomerProfile().getUsername(), ApplicationsDefinition.CIBP);
				deleteUserRSARequest.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
				deleteUserRSARequest.setRemovedRecoveryCode(true);

				// Delete RSA user
				/**
				 * MBNA-1967
				 * Method deleteUserRSA : CIBP-WEB microservice integration
				 */
				boolean rsaDeleted = customerService.getEnrollmentService().getCommonService().deleteUserRSA(deleteUserRSARequest);

				// Reset RSA entitlement
				if (rsaDeleted) {
					customerService.getCustomerProfileService().removeEntitlement(profile, rsaEnt);
					RSAUtils.createRSAEntitlement(customerService, profile);

					// Increment Removal By Recovery Code count for Monthly Report
					customerService.logCustomerUsageData(profile, CustomerActions.OOB_RECCODE_DEL);

					// Redirect to login if it's in session
					if (userSession.isLoginCompleteSuccess()) {
						redirectToLogin = true;
						// Otherwise add RSA enrollment to sign on flow
					} else {
						List<ASK_FOR> tasks = userSession.getTasksList();
						if (profile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)
								|| profile.hasFlag(CustomerFlagType.MUST_CHANGE_PASSWORD)) {
							tasks.add(tasks.indexOf(ASK_FOR.SESSION_INIT), ASK_FOR.RSA_ENROLL_USERNAME);
						} else {
							tasks.add(tasks.indexOf(ASK_FOR.SESSION_INIT), ASK_FOR.RSA_ENROLL);
						}
					}

				} else {
					redirectToLogin = true;
					log.info(ERROR_DELETING_RSA_USER + userSession.getUsername() + AFTER_RECOVERY_CODE_ENTRY);
				}

			} else {
				customerService.logFailOOBRecoveryCode(profile, getRecCodeMaxRetries(),getRecCodeRetryTimePeriod());
				model.put(RSA_MESSAGE, messageSource.getMessage(LOGIN_OOB_REC_CODE_ERROR_MESSAGE, null, language));

				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754) && userSession.isOobLogin()) {
					final CustomerProfileService customerProfileService = customerService.getCustomerProfileService();
					customerProfileService.failSignon(profile, CustomerInteraction.OBB_ERROR, customerDevices);
				}
			}

		} else {
			redirectToLogin = true;
			log.info(RSA_ENTITLEMENT_MISSING_FOR_USER + userSession.getUsername() + AFTER_RECOVERY_CODE_ENTRY);
		}

		if (redirectToLogin) {
			redirect = true;
			redirectToLogin = false;
		}

		model.put(REDIRECT_TO_LOGIN, redirect);
		if (isMobile(userSession)) {
			model.put(ONSEN_LIBS, true);
			model.put(LAYOUT_TYPE, JSON_LAYOUT);
			model.put(VIEW_PATH, views.get(JSON_TYPE));
		} else {
			model.put(LAYOUT_TYPE, FORM_LAYOUT);
			model.put(VIEW_PATH, views.get(RECOVERY_CODE_TYPE));
		}
		return new ModelAndView(RSAUIFORM, model);
	}


	/**
	 * @param bean
	 * @param rsaent
	 * @return boolean
	 */
	private boolean validateRecoveryCode(String recoCode, CustomerEntitlement rsaEnt) {
		final String entCodeHash = rsaEnt.getString(RSAEntitlement.CODEHASH);
		final String entCodeSalt = rsaEnt.getString(RSAEntitlement.CODESALT);
		boolean haveCode;
		if (!entCodeHash.equals(N) && !entCodeSalt.equals(N)){
			final String codeHashVerify = (PasswordHashUtil.generateSaltedHash(recoCode, Integer.parseInt(entCodeSalt)));
			haveCode = codeHashVerify.equals(entCodeHash);
		}else {
			haveCode = false;
		}
		return haveCode;
	}
   /**
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView rsaAltPhone(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		final UserSession userSession = getRequestUserSession(request);
		String method = getDisplayChallenge(getChallengeMethod(request, userSession));
		final String nextScreen = ServletRequestUtils.getStringParameter(request, NEXT_SCREEN);
		final String function = ServletRequestUtils.getStringParameter(request, NAME_FUNCTION);
		model.put(NAME_FUNCTION,function);
		final String errorMessage = "";
		boolean redirect = false;

		generateCode(request,model,method, errorMessage, true);

		if(!errorMessage.isEmpty())
			model.put(RSA_MESSAGE, errorMessage);

		if (method.equalsIgnoreCase(SMS_TYPE)) {
			method = SMS_ALT_TYPE;
		} else {
			method = CALL_ALT_TYPE;
		}
		model.put(LAYOUT_TYPE, FORM_LAYOUT);
		model.put(VIEW_PATH, views.get(method));
		model.put(NEXT_SCREEN,nextScreen);


		if (redirectToLogin || redirectToBlock) {
			redirect = true;
			model.put(USRSSESION, userSession.getUsername());
			redirectToBlock = false;
			redirectToLogin = false;
		}
		model.put(REDIRECT_TO_LOGIN, redirect);
		model.put(IS_MOBILE,isMobile(userSession));

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return new ModelAndView(RSAUIFORM, model);
	}

	/*
	 * END RSA challenge (OOB) ---------------------------------------------------------
	 */
	/*
	 * RSA ENROLL ----------------------------------------------------------------------
	 */

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView rsaEnroll(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		response.getLocale();
		final UserSession userSession = getRequestUserSession(request);
		model.put(LAYOUT_TYPE, FORM_LAYOUT);
		model.put(VIEW_PATH, views.get(ENROLL));
		model.put(TIME_INTERVAL, timeInterval);
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)) {
			model.put(MBSD3686, true);
			model.put(DEFAULT_CARRIER_VAR , DEFAULT_CARRIER);
		}
		if (isMobile(userSession)) {
			model.put(ONSEN_LIBS, true);
			model.put(EDIT_RSA_PAGE, isRsaEnrolled(userSession));
		}

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return new ModelAndView(RSAUIFORM, model);
	}



	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView rsaAddMobile(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		response.getLocale();
		final UserSession userSession = getRequestUserSession(request);
		model.put(LAYOUT_TYPE, FORM_LAYOUT);
		model.put(VIEW_PATH, views.get(ENROLL));
		if (isMobile(userSession)){
			model.put(ONSEN_LIBS, true);
		}

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return new ModelAndView(RSAUIFORM, model);
	}



	/*
	 * END RSA ENROLL ----------------------------------------------------------------------
	 */


	/*
	 * OOB Edit ----------------------------------------------------------------------
	 */

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView rsaEditOOB(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>(); //model result
		final UserSession userSession = getRequestUserSession(request); //user session
		response.getLocale();
		request.getLocale();
		final String primaryPhone = "787-999-9999";
		model.put(PRIMARY_NUMBER, primaryPhone);
		model.put(LAYOUT_TYPE, FORM_LAYOUT);
		model.put(VIEW_PATH, views.get(OOBEDIT));
		model.put(TIME_INTERVAL, timeInterval);
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)) {
			model.put(MBSD3686, true);
			model.put(DEFAULT_CARRIER_VAR , DEFAULT_CARRIER);
		}

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return new ModelAndView(RSAUIFORM, model);
	}
	/*
	 * End OOB EDIT
	 * */


	/*
	 * Utils RSA --------------------------------------------------------------------
	 */

	/**
	 *
	 * @param request
	 * @param model
	 * @param method
	 * @return
	 */
	private void generateCode(HttpServletRequest request, Map<String, Object> model, String method, String errorMessage, boolean isAlternate) {
		final UserSession userSession = getRequestUserSession(request);
		final CustomerProfile customer = userSession.getCustomerProfile();
		final String CALL_TYPE = RSAChallengeForm.CALL_TYPE;
		final String SMS_TYPE = RSAChallengeForm.SMS_TYPE;
		final CustomerEntitlement rsaEnt = customer.getEntitlementByType(EntitlementType.RSA);
		String phone_ = this.phone_;
		String altPhone = null;
		if (rsaEnt != null && isAlternate) {
			altPhone = rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE);
		}
		if (customer.getEntitlementByType(EntitlementType.MOBILE_BANKING) != null && (method.equalsIgnoreCase(SMS_TYPE) || method.equalsIgnoreCase(CALL_TYPE))) {
			if(isAlternate && altPhone != null && !altPhone.equalsIgnoreCase(N)) {
				phone_ = altPhone;
				final String provider = rsaEnt.getString(RSAEntitlement.ALTERNATEPROVIDER);
				if (method.equalsIgnoreCase(SMS_TYPE)){
					generateRsaCode(request, phone_, provider, true, errorMessage);
				}else if (method.equalsIgnoreCase(CALL_TYPE)){
					this.makeCall(request, phone_, true, errorMessage);
				}
			} else if (!isAlternate) {
				final SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
				if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
						&& !smsprofile.getCtns().isEmpty()) {
					phone_ = ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn();

					String provider = "";
					if(smsprofile.getCtns().iterator().next() instanceof SMSCTN &&
							((SMSCTN) smsprofile.getCtns().iterator().next()).getProvider() instanceof  String) {
						provider = ((SMSCTN) smsprofile.getCtns().iterator().next()).getProvider();
					}
					if (method.equalsIgnoreCase(SMS_TYPE)){
						generateRsaCode(request, phone_, provider, true, errorMessage);
					}else if (method.equalsIgnoreCase(CALL_TYPE)){
						this.makeCall(request, phone_, true, errorMessage);
					}
				}
			}
			model.put(PHONE, RSAUtils.setPhoneMask(phone_));
		}
		this.phone_ = phone_;

	}

	/**
	 *
	 * @param request
	 * @param model
	 * @param method
	 * @return
	 */
	private void makeCall(HttpServletRequest request, String phone, boolean newChallenge, String errorMessage) {

		UserSession userSession = getRequestUserSession(request);
		final Locale language= RequestContextUtils.getLocale(request);

		final String user = userSession.getUsername();
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request);

		ChallengeOobRequest challengeOobRequest = new ChallengeOobRequest();
		challengeOobRequest.setCredentialType(CredentialType.OOBPHONE);
		challengeOobRequest.setChallengephone(phone);
		challengeOobRequest.setDeviceRequest(deviceRequest);
		challengeOobRequest.setUsername(user);
		challengeOobRequest.setRsaSessionId(userSession.getRsaSessionId());
		challengeOobRequest.setRsaTransactionId(userSession.getRsaTransactionId());
		challengeOobRequest.setLanguage(getLanguage(request));
		if (userSession.isLoginCompleteSuccess()) {
			challengeOobRequest.setNewChallenge(true);
		} else {
			challengeOobRequest.setNewChallenge(newChallenge);
		}
		try {

			/**
			 * MBNA-1973
			 * Method challengeOob : CIBP-WEB microservice integration
			 */
			final ChallengeOobResponse challengeOobResponse = customerService.getSignOnCustomerServiceModule().challengeOob(challengeOobRequest);

			deviceRequest.setDeviceTokenCookie(challengeOobResponse.getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(challengeOobResponse.getDeviceTokenCookie());
			userSession.setDeviceRequest(deviceRequest);
			userSession.setRsaSessionId(challengeOobResponse.getRsaSessionId());
			userSession.setRsaTransactionId(challengeOobResponse.getRsaTransactionId());

			if (ResultStatus.RSA_UNABLE.equals(challengeOobResponse.getResultStatus())) {
				redirectToLogin = true;
				errorMessage = messageSource.getMessage(RSA_ERROR_MESSAGE_CALL_UNABLE, null, language);
				WebUtils.setSessionAttribute(request, OOB_CODE, null);

			} else if (ResultStatus.RSA_BLOCKED.equals(challengeOobResponse.getResultStatus())) {
				redirectToBlock = true;
				WebUtils.setSessionAttribute(request, OOB_CODE, null);
				log.info(RSA_USR + userSession.getUsername() + BLOCKED_BY_OOB_CALL_CHALLENGE);
			}

			WebUtils.setSessionAttribute(request, OOB_CODE, challengeOobResponse.getToken());

		} catch (Exception e) {
			errorMessage = messageSource.getMessage(RSA_ERROR_MESSAGE_CALL_UNABLE, null, language);
			WebUtils.setSessionAttribute(request, OOB_CODE, null);

		}
	}

	private Lang getLanguage(HttpServletRequest request) {
		return EN.equalsIgnoreCase(Utils.getRSALanguage(request)) ? Lang.en : Lang.es;
	}


	/**
	 *
	 * @param request
	 * @param phone
	 * @param provider
	 * @param newChallenge
	 * @param errorMessage
	 */
	private void generateRsaCode(HttpServletRequest request, String phone, String provider, boolean newChallenge, String errorMessage) {
		UserSession userSession = getRequestUserSession(request);
		final CustomerProfile customer = userSession.getCustomerProfile();
		final Locale language = RequestContextUtils.getLocale(request);

		final String user = userSession.getUsername();

		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);

		ChallengeOobRequest challengeOobRequest = new ChallengeOobRequest();
		challengeOobRequest.setCredentialType(CredentialType.USER_DEFINED);
		challengeOobRequest.setChallengephone(phone);
		challengeOobRequest.setDeviceRequest(deviceRequest);
		challengeOobRequest.setUsername(user);
		challengeOobRequest.setRsaSessionId(userSession.getRsaSessionId());
		challengeOobRequest.setRsaTransactionId(userSession.getRsaTransactionId());
		challengeOobRequest.setLanguage(getLanguage(request));
		if (userSession.isLoginCompleteSuccess()) {
			challengeOobRequest.setNewChallenge(true);
		} else {
			challengeOobRequest.setNewChallenge(newChallenge);
		}
		try {

			/**
			 * MBNA-1973 Method challengeOob : CIBP-WEB microservice integration
			 */
			final ChallengeOobResponse challengeOobResponse = userSession.getCustomerService().getSignOnCustomerServiceModule()
					.challengeOob(challengeOobRequest);

			deviceRequest.setDeviceTokenCookie(challengeOobResponse.getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(challengeOobResponse.getDeviceTokenCookie());
			userSession.setDeviceRequest(deviceRequest);
			userSession.setRsaSessionId(challengeOobResponse.getRsaSessionId());
			userSession.setRsaTransactionId(challengeOobResponse.getRsaTransactionId());

			if (ResultStatus.RSA_UNABLE.equals(challengeOobResponse.getResultStatus())) {
				redirectToLogin = true;
				errorMessage = messageSource.getMessage(RSA_ERROR_MESSAGE_CALL_UNABLE, null, language);
				WebUtils.setSessionAttribute(request, OOB_CODE, null);

			} else if (ResultStatus.RSA_BLOCKED.equals(challengeOobResponse.getResultStatus())) {
				redirectToBlock = true;
				WebUtils.setSessionAttribute(request, OOB_CODE, null);
				log.info(RSA_USR + userSession.getUsername() + BLOCKED_BY_OOB_CALL_CHALLENGE);

			}
			final String code = challengeOobResponse.getOtp();
			final SendSmsGeneratedCodeRequest codereq = new SendSmsGeneratedCodeRequest(Utils.generateCustomerInteractionEvent(request), customer, code, phone, provider);

			if (ENGLISH_LOWERCASE.equalsIgnoreCase(Utils.getRSALanguage(request))) {
				customerService.sendGeneratedCode(codereq, PreferredLanguage.ENGLISH);
			} else {
				customerService.sendGeneratedCode(codereq, PreferredLanguage.SPANISH);
			}
			WebUtils.setSessionAttribute(request, OOB_CODE, code);
		} catch (Exception e) {
			errorMessage = messageSource.getMessage(ERROR_MESSAGE_SMS_UNABLE, null, language);
			WebUtils.setSessionAttribute(request, OOB_CODE, null);

		}
	}


	/**
	 *
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private String getRsaChallengeMethod (HttpServletRequest request) throws Exception  {
		final String CALL_TYPE = RSAChallengeForm.CALL_TYPE;
		final String SMS_TYPE = RSAChallengeForm.SMS_TYPE;
		final String code = ServletRequestUtils.getStringParameter(request, CALL_TYPE);
		if (code != null && (code.equalsIgnoreCase(CALL_TYPE) || code.equalsIgnoreCase(TRUE_LOWERCASE))) {
			return CALL_TYPE;
		} else {
			return SMS_TYPE;
		}
	}

	/**
	 *
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private String getDisplayChallenge (String challenge) throws Exception  {
		if (challenge != null && (challenge.equalsIgnoreCase(CustomerOOBChallengeType.OOBPHONE.toString()))) {
			return RSAChallengeForm.CALL_TYPE;
		} else if (challenge != null && (challenge.equalsIgnoreCase(CustomerOOBChallengeType.OOBSMS.toString()))) {
			return RSAChallengeForm.SMS_TYPE;
		} else if (challenge != null && (challenge.equalsIgnoreCase(CustomerOOBChallengeType.QUESTIONS.toString()))) {
			return RSAChallengeForm.QUESTION;

		} else { return RSAChallengeForm.QUESTION; }

	}


	/**
	 *
	 * @param request
	 * @param model
	 * @param method
	 * @throws Exception
	 * @return
	 */
	public ModelAndView rsaChallengeMethod(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		final UserSession userSession = getRequestUserSession(request);
		model.put(CHALLENGE_METHOD, getChallengeMethod(request, userSession));
		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		model.put(IS_OOB_AUTHENTICATED, userSession.getOobEnroll() && userSession.isOobAuthenticated());
		model.put(ONSEN_LIBS, isMobile(userSession));
		return new ModelAndView(RSAUIFORM, model);
	}

	/**
	 * Get the type of Challenge method configured in OOB
	 * @param request
	 * @param model
	 * @param method
	 * @return
	 */
	private String getChallengeMethod(HttpServletRequest request, UserSession userSession) {
		final DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);

		/**
		 * MBNA-1985
		 * Method getRsaChallengeMethod : CIBP-WEB microservice integration
		 *
		 */
		//Microservice implementation
		final RsaChallengeRequest rsaChallengeRequest = new RsaChallengeRequest(userSession.getUsername(),deviceRequest);
		final CredentialType credentialType = customerService.rsaGetChallengeMethod(rsaChallengeRequest);

		userSession.setDeviceRequest(deviceRequest);
		String challengeMethod = null;
		if (credentialType != null && credentialType.equals(CredentialType.OOBPHONE)) {
			challengeMethod = CustomerOOBChallengeType.OOBPHONE.toString();
		} else if (credentialType != null && credentialType.equals(CredentialType.USER_DEFINED)) {
			challengeMethod = CustomerOOBChallengeType.OOBSMS.toString();
		} else if (credentialType != null && credentialType.equals(CredentialType.QUESTION)) {
			challengeMethod = CustomerOOBChallengeType.QUESTIONS.toString();
		}
		return challengeMethod;
	}



	/**
	 * Verify if comes from mobile native app petition
	 * @param userSession
	 * @param header
	 * @return true if is a webview
	 */
	private boolean isMobile(UserSession userSession) {
		return (userSession.getUserAgentType() == UserAgentType.IPHONE ||
		        userSession.getUserAgentType() == UserAgentType.IPAD ||
		        userSession.getUserAgentType() == UserAgentType.ANDROID);
	}

	/**
	 *
	 * @param userSession
	 * @throws Exception
	 * @return
	 */
	private boolean isRsaEnrolled (UserSession userSession) throws Exception{
		return userSession.getOobEnroll();
	}

	/*
	 * END Utils RSA --------------------------------------------------------------------
	 */

	/**
	 *
	 * @param request
	 * @param response
	 * @throws Exception
	 * @return true if is a webview
	 */
	public ModelAndView rsaQuestion(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		response.getLocale();
		UserSession userSession = getRequestUserSession(request);
//		userSession.setInformationChallenge(false);
		QuestionResponse personalQuestion = null;
//		if (userSession.getInformationQuestion() == null) {
			DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
					userSession.getDeviceRequest().getDeviceTokenCookie(), request);
			/**
			 * MBNA-1955
			 * Method rsaAnalyze : CIBP-WEB microservice integration
			 *
			 */
			//Microservice implementation
			final RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, Utils.getRSALanguage(request), userSession.getUsername(), EventType.CLIENT_DEFINED.getValue());
			final AnalyzeResponse anResp = customerService.rsaAnalyze(rsaAnalyzeRequest);

			if (_4_0_9 != anResp.getStatusHeader().getStatusCode()) {
				deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
				deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
				userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
				userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

				userSession.setDeviceRequest(deviceRequest);
			} else {
				throw new ModelAndViewDefiningException(new ModelAndView(ERROR));
			}
			String email = null;
			if (userSession.getCustomerProfile() != null)
				email = userSession.getCustomerProfile().getEmail();

			RSAActionRequest actionRequest = new RSAActionRequest(
					Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
					userSession.getUsername(), "", email, 0, userSession.isRemember(),
					userSession.getDeviceRequest());

			actionRequest.setDeviceRequest(userSession.getDeviceRequest());
			actionRequest.setRsaSessionId(userSession.getRsaSessionId());
			actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());
			actionRequest.setRsaLang(Utils.getRSALanguage(request));

			personalQuestion = customerService.getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

			if (personalQuestion != null && personalQuestion.getDeviceRequest() != null)
				userSession.getDeviceRequest()
						.setDeviceTokenCookie(personalQuestion.getDeviceRequest().getDeviceTokenCookie());

//		}
//		else {
//			personalQuestion = new QuestionResponse();
//			personalQuestion.setStatusResult(ResultStatus.SUCCESS);
//			personalQuestion.setQuestion(userSession.getInformationQuestion());
//		}

		if (personalQuestion != null && ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {

			userSession.setInformationQuestion(personalQuestion.getQuestion());

			String question = "";
			if(userSession.getInformationQuestion().values().toArray()[0] instanceof String) {
				question = (String) userSession.getInformationQuestion().values().toArray()[0];
				model.put(SECURITY_QUESTION, question);
			}

		} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())
				|| ResultStatus.EXCEPTION.equals(personalQuestion.getStatusResult())) {
			throw new ModelAndViewDefiningException(new ModelAndView(ERROR));
		} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
			throw new ModelAndViewDefiningException(new ModelAndView(FORWARD_LOGIN));
		}

		model.put(DEVICE_TOKEN, userSession.getDeviceRequest().getDeviceTokenCookie());

		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		if (isMobile(userSession))
			model.put(ONSEN_LIBS, true);
		return new ModelAndView(RSAUIFORM, model);
	}

	private QuestionResponse getRsaQuestion(HttpServletRequest request, UserSession userSession)
			throws ModelAndViewDefiningException, SoapFaultType, RemoteException, RemoteException {
		QuestionResponse personalQuestion = null;
		verifyChangeLanguageQuestion(userSession, request);
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);
		/**
		 * MBNA-1955
		 * Method rsaAnalyze : CIBP-WEB microservice integration
		 *
		 */
		//Microservice implementation
		final RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, Utils.getRSALanguage(request), userSession.getUsername(), EventType.CLIENT_DEFINED.getValue());
		final AnalyzeResponse anResp = customerService.rsaAnalyze(rsaAnalyzeRequest);
		if (_4_0_9 != anResp.getStatusHeader().getStatusCode()) {
			deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
			userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
			userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

			userSession.setDeviceRequest(deviceRequest);
		} else {
			throw new ModelAndViewDefiningException(new ModelAndView(ERROR));
		}
		String email = null;
		if (userSession.getCustomerProfile() != null)
			email = userSession.getCustomerProfile().getEmail();

		RSAActionRequest actionRequest = new RSAActionRequest(
				Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
				userSession.getUsername(), "", email, 0, userSession.isRemember(),
				userSession.getDeviceRequest());

		actionRequest.setDeviceRequest(userSession.getDeviceRequest());
		actionRequest.setRsaSessionId(userSession.getRsaSessionId());
		actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());
		actionRequest.setRsaLang(Utils.getRSALanguage(request));

		personalQuestion =customerService.getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

		if (personalQuestion != null && personalQuestion.getDeviceRequest() != null)
			userSession.getDeviceRequest()
					.setDeviceTokenCookie(personalQuestion.getDeviceRequest().getDeviceTokenCookie());

		return personalQuestion;
	}


	/**
	 * Enroll in OOB
	 * @param request
	 * @param response
	 * @throws Exception
	 * @return
	 */
	public ModelAndView rsaOOBEnroll(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		response.getLocale();
		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		final Log log = LogFactory.getLog(getClass());
		UserSession userSession = getRequestUserSession(request);
		final CustomerServices customerService = userSession.getCustomerService();
		final CustomerProfile customerProfile = userSession.getCustomerProfile();
		final Locale language = RequestContextUtils.getLocale(request);
		// Validate is enrolled in mi banco movil
		if (userSession.getCustomerProfile() != null) {
			if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
				final SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
				if (!(smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
						&& !smsprofile.getCtns().isEmpty())) {
					userSession.setOobEnroll(false);
					model.put(TEXT, FAIL);
					model.put(ERROR, messageSource.getMessage(ENROLL_OOB_REGISTER_SMS_ERROR, null, language));
				}
			} else {
				model.put(TEXT, FAIL);
				model.put(ERROR, messageSource.getMessage(ENROLL_OOB_REGISTER_SMS_ERRORR, null, language));
			}
		} else {
			model.put(TEXT, FAIL);
			model.put(ERROR, NO_USER_SESSION);
		}
		//Enroll on OOB
		try {

			/**
			* MBNA-1961
			* Method addCustomerOOBEnroll : CIBP-WEB microservice integration
			*
			*/
			customerService.addCustomerOOBEnroll(customerProfile);
			
			String challengeType = ChallengeType.OOBSMS.toString();
			boolean frequency = false;
			String alternatePhone = N;
			String alternateProvider = N;
			if(request.getParameter(CHALLENGE_TYPE) != null) {
				challengeType = request.getParameter(CHALLENGE_TYPE);
			}
			if(request.getParameter(FREQUENCY2) != null) {
				frequency = String.valueOf(request.getParameter(FREQUENCY2)).equalsIgnoreCase(ALLOW)  ? false : true;
			}
			if(request.getParameter(ALTERNATE_PHONE) != null) {
				alternatePhone = request.getParameter(ALTERNATE_PHONE);
			}
			if(request.getParameter(ALTERNATE_PROVIDER) != null) {
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)) {
					alternateProvider = DEFAULT_CARRIER;
				} else {
					alternateProvider = request.getParameter(ALTERNATE_PROVIDER);
				}

			}

			final String recoveryCode = RSAUtils.generateOOBCode();// Must save to display once
			final Boolean alwaysChallenge = frequency;

			final DeleteUserRSARequest deleteUserRSARequest = new DeleteUserRSARequest(
					userSession.getCustomerProfile().getUsername(), ApplicationsDefinition.CIBP);

			// Try to delete RSA user
			boolean rsaUserDeleted = false;
			RsaOobResponse enrollResp = new RsaOobResponse();

			/**
			 * MBNA-1967 Method deleteUserRSA : CIBP-WEB microservice integration
			 *
			 */
			rsaUserDeleted = customerService.getEnrollmentService().getCommonService().deleteUserRSA(deleteUserRSARequest);


			// Enroll user in RSA w/ OOB
			if (rsaUserDeleted) {
				String email = null;
				if (userSession.getCustomerProfile() != null)
					email = userSession.getCustomerProfile().getEmail();

				final RSAActionRequest actionRequest = new RSAActionRequest(
						Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
						userSession.getUsername(), "", email, 0, userSession.isRemember(),
						userSession.getDeviceRequest());

				final DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
						userSession.getDeviceRequest().getDeviceTokenCookie(), request);
				enrollResp = customerService.getSignOnCustomerServiceModule().enrollRSAOOB(actionRequest, challengeType);
			}

			// Update RSA entitlement
			if (enrollResp.getResultStatus() != null && enrollResp.getResultStatus().equals(ResultStatus.SUCCESS)) {

				final Integer codeSalt = PasswordHashUtil.genereateRandomSalt();
				final String codeHash = (PasswordHashUtil.generateSaltedHash(recoveryCode, codeSalt));

				final CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request);
				final ModifyRSAEntitlementRequest modifyRequest = new ModifyRSAEntitlementRequest(interactionEvent,
						customerProfile, alternatePhone, alternateProvider, alwaysChallenge, true, codeHash,
						codeSalt.toString());
				customerService.modifyCustomerRSAEntitlement(modifyRequest);

				customerService.cleanOOBRecoveryCodeBlock(customerProfile);

				userSession.getDeviceRequest().setDeviceTokenCookie(enrollResp.getDeviceTokenCookie());

				customerService.sendOobEnrollEmail(customerProfile, userSession.getPreferredLanguage());

				userSession.setOobEnroll(true);

				// Increment OOB counts for Monthly Report
				if (challengeType.equalsIgnoreCase(CustomerOOBChallengeType.OOBPHONE.toString())) {
					customerService.logCustomerUsageData(customerProfile, CustomerActions.OOB_VOICE_ENROLL);
				} else {
					customerService.logCustomerUsageData(customerProfile, CustomerActions.OOB_SMS_ENROLL);
				}

				if (alwaysChallenge) {
					customerService.logCustomerUsageData(customerProfile, CustomerActions.OOB_ALWAYS_FREQ);
				} else {
					customerService.logCustomerUsageData(customerProfile, CustomerActions.OOB_ASREQ_FREQ);
				}
				if (!alternatePhone.equals(N))
					customerService.logCustomerUsageData(customerProfile, CustomerActions.OOB_ALT_PHONE);

				if (ENGLISH_LOWERCASE.equalsIgnoreCase(Utils.getRSALanguage(request))) {
					userSession.setStatusmessage(
							ENROLLMENT_2_STEP_VERIFICATION__SUCCESSFULLY);
				} else {
					userSession.setStatusmessage(
							INSCRIPCION_2_PASOS_EXITOSAMENTE);
				}
				model.put(ONE_TIME_CODE, recoveryCode);
				model.put(RESULT, SUCCESS);
			} else {
				userSession.setOobEnroll(false);
				model.put(RESULT, FAIL);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			userSession.setOobEnroll(false);
			model.put(RESULT, FAIL);
		}

		return new ModelAndView(RSAUIFORM, model);
	}

	/**
	 * Validate rsa question
	 * @param request
	 * @param response
	 * @throws Exception
	 * @return
	 */
	public ModelAndView validateQuestion(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = getRequestUserSession(request);
//		if (!userSession.isInformationChallenge()) {

			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);

	    	final CustomerProfile profile = userSession.getCustomerProfile();

			boolean dummyQuestions = (profile == null);

			String answer = ServletRequestUtils.getStringParameter(request, "answer");

			if (GenericValidator.isBlankOrNull(answer)) {
				String msjError = "";
				Locale lang= RequestContextUtils.getLocale(request);
				if(SPANISH_LOWERCASE.equalsIgnoreCase(lang.getLanguage())) {
					msjError = REQUIRED_SPANISH;
				}else {
					msjError = REQUIRED_ENGLISH;
				}
				model.put(MESSAGE_ERROR, msjError);
			} else {

				RSAActionRequest rsaActionRequest = new RSAActionRequest(
						Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
						userSession.getUsername(), answer, dummyQuestions ? null : profile.getEmail(), 0,
						userSession.isRemember(), userSession.getDeviceRequest());

				rsaActionRequest.setRsaSessionId(userSession.getRsaSessionId());
				rsaActionRequest.setRsaTransactionId(userSession.getRsaTransactionId());
				rsaActionRequest.setQuestion(userSession.getInformationQuestion());
				rsaActionRequest.setRsaLang(Utils.getRSALanguage(request));

				String header = userSession.getDeviceRequest().getUserAgent();
				DeviceUtils.getCustomerDevice(header);
				rsaActionRequest.setCustomerDevices(DeviceUtils.getCustomerDevice(header).toString());

				ValidationQuestionResponse responseAnswer = customerService.getSignOnCustomerServiceModule().validateRSAQuestion(rsaActionRequest);

				userSession.setQuestion(null);
				userSession.setRsaSessionId(responseAnswer.getRsaSessionId());
				userSession.setRsaTransactionId(responseAnswer.getRsaTransactionId());

				Cookie cookie = WebUtils.getCookie(request, "PMData");
				if (cookie == null)
					cookie = new Cookie("PMData", "");

				cookie.setPath("/");
				cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
				ESAPI.httpUtilities().addCookie(response, cookie);

				if (ResultStatus.SUCCESS.equals(responseAnswer.getStatusResult())) {
					userSession.setInformationChallenge(true);
					userSession.setInformationQuestion(null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
					model.put(RESULT, ResultStatus.SUCCESS.toString());
				} else if (ResultStatus.BLOCKED.equals(responseAnswer.getStatusResult())) {
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
					boolean hasalert = false;
					AlertsEntitlement ent = new AlertsEntitlement(
							userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS));
					if (ent != null && ent.getAlerts() != null) {
						Set<AlertType> alerts = ent.getAlerts();
						if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
								|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
							hasalert = true;
						}
					}
					if (hasalert) {
						userSession.getCustomerService().sendQuestionsBlockedConfirmation(
								userSession.getCustomerProfile(),
								userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
								userSession.getPreferredLanguage());
					}
					model.put(RESULT, ResultStatus.BLOCKED.toString());
				} else if (ResultStatus.EXCEPTION.equals(responseAnswer.getStatusResult())) {
					model.put(RESULT, ResultStatus.EXCEPTION.toString());
				} else if (ResultStatus.ERROR.equals(responseAnswer.getStatusResult())) {
					String msjError = "";
					Locale lang= RequestContextUtils.getLocale(request);
					if(SPANISH_LOWERCASE.equalsIgnoreCase(lang.getLanguage())) {
						msjError = "Respuesta Invalida";
					}else {
						msjError = "Invalid answer";
					}
					model.put(MESSAGE_ERROR, msjError);
					String email = null;
					if (userSession.getCustomerProfile() != null)
						email = userSession.getCustomerProfile().getEmail();
					QuestionResponse personalQuestion;
					RSAActionRequest actionRequest = new RSAActionRequest(
							Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
							userSession.getUsername(), "", email, 0, userSession.isRemember(),
							userSession.getDeviceRequest());

					actionRequest.setDeviceRequest(userSession.getDeviceRequest());
					actionRequest.setRsaSessionId(userSession.getRsaSessionId());
					actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());

					actionRequest.setRsaLang(Utils.getRSALanguage(request));

					personalQuestion = customerService.getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

					if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
						userSession.setInformationQuestion(personalQuestion.getQuestion());

					} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
						model.put(RESULT, ResultStatus.ERROR.toString());
					} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS,
								null);

						boolean hasalert = false;
						AlertsEntitlement ent = new AlertsEntitlement(
								userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS));
						if (ent != null && ent.getAlerts() != null) {
							Set<AlertType> alerts = ent.getAlerts();
							if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
									|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
								hasalert = true;
							}
						}
						if (hasalert) {
							userSession.getCustomerService()
									.sendQuestionsBlockedConfirmation(userSession.getCustomerProfile(),
											userSession
													.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
											userSession.getPreferredLanguage());
						}
						model.put(RESULT, ResultStatus.BLOCKED.toString());
					}
				}
			}
//		}

		if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
			SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
			if (smsprofile != null && smsprofile.getSmsProfileId() != null
					&& smsprofile.getCtns() != null && !smsprofile.getCtns().isEmpty()) {
				model.put(ADD_PHONE_NUMBER, false);

				SMSCTN ctn = null;
				if(smsprofile.getCtns() != null && smsprofile.getCtns().iterator().next() instanceof SMSCTN) {
					ctn = (SMSCTN) smsprofile.getCtns().iterator().next();
				}

				model.put(PRIMARY_PHONE, ctn.getCtn());
			}else {
				model.put(ADD_PHONE_NUMBER, true);
			}
		}else {
			model.put(ADD_PHONE_NUMBER, true);
		}

		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		if (isMobile(userSession))
			model.put(ONSEN_LIBS, true);
		return new ModelAndView(RSAUIFORM, model);
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws Exception
	 * @return
	 */
	public ModelAndView rsaSMSCode(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = getRequestUserSession(request);
		response.getLocale();
		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		if (isMobile(userSession))
			model.put(ONSEN_LIBS, true);
		CustomerProfile customer = userSession.getCustomerProfile();
		//Send Code
		Calendar cal = Calendar.getInstance();
		long mil = cal.getTimeInMillis();
		String code = String.valueOf(mil);
		code = code.substring(code.length() - 4, code.length());
		Boolean resend = false;
		String phone = null;
		String provider = null;
		if(request.getParameter("resend") != null) {
			resend = Boolean.valueOf(request.getParameter("resend"));
		}
		if(resend) {;
			phone = (String) WebUtils.getSessionAttribute(request, "smsPhonereceived");
			provider = (String) WebUtils.getSessionAttribute(request, "smsProvider");
		}else {
			if(request.getParameter("mainNumberInp") != null) {
				phone = request.getParameter("mainNumberInp");
			}

			if(request.getParameter("carrier") != null) {
				provider = request.getParameter("carrier");
			}
		}
		if(GenericValidator.isLong(phone) && (PROVIDER_ARRAY.contains(provider)
				|| (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686) && provider.equalsIgnoreCase(DEFAULT_CARRIER)))){
			WebUtils.setSessionAttribute(request, "smsGeneratedCode", code);
			WebUtils.setSessionAttribute(request, "smsPhonereceived", phone);
			WebUtils.setSessionAttribute(request, "smsProvider", provider);
			String msgerr = "";
			try {
				SendSmsGeneratedCodeRequest codereq = new SendSmsGeneratedCodeRequest(
						Utils.generateCustomerInteractionEvent(request), customer, code, phone,
						provider);
				if (ENGLISH_LOWERCASE.equalsIgnoreCase(Utils.getRSALanguage(request))) {
					customerService.sendGeneratedCode(codereq, PreferredLanguage.ENGLISH);
				} else {
					customerService.sendGeneratedCode(codereq, PreferredLanguage.SPANISH);
				}
				model.put(RESULT, SUCCESS);
			} catch (Exception e) {
				logger.error(e.getMessage());
				if (ENGLISH_LOWERCASE.equalsIgnoreCase(Utils.getRSALanguage(request))) {
					msgerr = "Unable to send code. Please try later.";
				} else {
					msgerr = "Problemas al enviar tu c&oaccute;digo. Favor intentar luego.";
				}
				model.put(RESULT, FAIL);
				model.put(MESSAGE_ERROR, msgerr);
			}
		}else {
			model.put(RESULT, FAIL);
		}
		return new ModelAndView(RSAUIFORM,model);
	}

	/**
	 * Enroll in SMS
	 * @param request
	 * @param response
	 * @throws Exception
	 * @return
	 */
	public ModelAndView rsaSMSEnroll(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		response.getLocale();
		UserSession userSession = getRequestUserSession(request);
		CustomerProfile customer = userSession.getCustomerProfile();
		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		if (isMobile(userSession))//Two Unit test test user agent
			model.put(ONSEN_LIBS, true);
		String code = (String) WebUtils.getSessionAttribute(request, "smsGeneratedCode");
		String codeNumberInp = null;
		String phone = null;
		String provider = null;
		phone = (String) WebUtils.getSessionAttribute(request, "smsPhonereceived");
		provider = (String) WebUtils.getSessionAttribute(request, "smsProvider");
		if (request.getParameter("codeNumberInp") != null) {//Test here
			codeNumberInp = request.getParameter("codeNumberInp");
		}

		if (GenericValidator.isBlankOrNull(codeNumberInp) || GenericValidator.isBlankOrNull(code)
				|| !codeNumberInp.equals(code)) {//Test here
			model.put(RESULT, FAIL);
			return new ModelAndView(RSAUIFORM, model);
		}

		try {//at least two more unit + / -

			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)) {
				provider = DEFAULT_CARRIER;
			}
			SmsEnrollUserRequest smsenr = new SmsEnrollUserRequest(Utils.generateCustomerInteractionEvent(request),
					customer, phone, provider);
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD2267)) {
				smsenr.setEnrollmentSource("CIBP");
			}
			// Microservice orchestration
			ResultStatus status = customerService.smsEnrollUser(smsenr);
			if (status == ResultStatus.SUCCESS) {//two more
				CustomerInteractionEvent generateCustomerInteractionEvent = Utils
						.generateCustomerInteractionEvent(request);
				SMSProfile smsprofile = userSession.getSmsProfile(generateCustomerInteractionEvent);
				if (ENGLISH_LOWERCASE.equalsIgnoreCase(Utils.getRSALanguage(request))) {//two more
					userSession.setStatusmessage("Your inscription has been completed successfully.");
					customerService.sendSMSWelcome(customer, smsprofile, PreferredLanguage.ENGLISH);
				} else {
					userSession.setStatusmessage("Su inscripci&oacute;n ha sido completada exitosamente.");
					customerService.sendSMSWelcome(customer, smsprofile, PreferredLanguage.SPANISH);
				}
				model.put("smsSuccessEnroll", true);

				if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
					request.getSession().setAttribute("smsExito", true);
				}
				model.put(PRIMARY_PHONE, phone);
				model.put(RESULT, SUCCESS);

			} else {
				if (ENGLISH_LOWERCASE.equalsIgnoreCase(Utils.getRSALanguage(request))) {
					userSession.setStatusmessage("Unable to enroll your phone number at this time. Please try later.");
				} else {
					userSession.setStatusmessage(
							"Problemas al inscribir tu n&uacute;mero de tel&eacute;fono en este momento. Favor intentar luego.");
				}
				model.put(RESULT, FAIL);
			}
		} catch (Exception e) {//Test language
			logger.error(e.getMessage());
			if (ENGLISH_LOWERCASE.equalsIgnoreCase(Utils.getRSALanguage(request))) {
				userSession.setStatusmessage("Unable to enroll your phone number at this time. Please try later.");
			} else {
				userSession.setStatusmessage(
						"Problemas al inscribir tu n&uacute;mero de tel&eacute;fono en este momento. Favor intentar luego.");
			}
		}

		return new ModelAndView(RSAUIFORM, model);
	}

	/**
	 * Get one rsa question
	 * @param request
	 * @param response
	 * @throws Exception
	 * @return
	 */
	public ModelAndView rsaGetQuestions(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		response.getLocale();
		UserSession userSession = getRequestUserSession(request);
		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		if (isMobile(userSession))
			model.put(ONSEN_LIBS, true);
		/**
		 * Get new questions for user
		 */
		CustomerInteractionEvent generateCustomerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
		RSAActionRequest actionrequest = new RSAActionRequest(generateCustomerInteractionEvent, userSession.getCustomerProfile()
				.getUsername().toLowerCase(), "", userSession.getCustomerProfile().getEmail(), 0, false, userSession.getDeviceRequest());
		actionrequest.setRsaLang("sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "ES" : EN);

		/**
		 * MBNA-2536
		 * Method getQuestionsSelection : CIBP-WEB microservice integration
		 */
		QuestionSelectionResponse questionResponse = customerService.rsaGetQuestionsSelection(actionrequest);
		
		userSession.getDeviceRequest().setDeviceTokenCookie(questionResponse.getRsaCookie());

		if (questionResponse.getStatusResult().equals(ResultStatus.SUCCESS)) {
			String selectquestiontext = "Select a question:";
			if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
				selectquestiontext = "Seleccione una pregunta:";

			// Use this builder to construct a Gson instance when you need to set configuration options other than the default.
			GsonBuilder gsonMapBuilder = new GsonBuilder();

			Gson gsonObject = gsonMapBuilder.create();


			Map<Object, Object> q = new HashMap<Object, Object>();
			q.put("", selectquestiontext);
			q.putAll(questionResponse.getQuestionMap1());
			
			model.put("qse1", gsonObject.toJson(q));

			Map<Object, Object> q2 = new HashMap<Object, Object>();
			q2.put("", selectquestiontext);
			q2.putAll(questionResponse.getQuestionMap2());
			
			model.put("qse2", gsonObject.toJson(q2));

			Map<Object, Object> q3 = new HashMap<Object, Object>();
			q3.put("", selectquestiontext);
			q3.putAll(questionResponse.getQuestionMap3());
			
			model.put("qse3", gsonObject.toJson(q3));

			model.put(RESULT, SUCCESS);
		} else {

			model.put(RESULT, FAIL);
		}
		model.put(LANGUAGE, userSession.getLanguage());
		return new ModelAndView(RSAUIFORM, model);
	}

	/**
	 * Deactivate OOB
	 * @param request
	 * @param response
	 * @throws Exception
	 * @return
	 */
	public ModelAndView rsaOOBDeactivate(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (REQUEST_POST_METHOD.equalsIgnoreCase(request.getMethod())) {
			Map<String, Object> model = new HashMap<>();
			model.put(TIME_INTERVAL, timeInterval);
			UserSession userSession = getRequestUserSession(request);
			CustomerProfile customer = userSession.getCustomerProfile();
			Log log = LogFactory.getLog(getClass());
			MessageSource messageSource = this.messageSource;
			model.put(LAYOUT_TYPE, JSON_LAYOUT);
			model.put(VIEW_PATH, views.get(JSON_TYPE));
			String collect = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			Locale language = RequestContextUtils.getLocale(request);
			
			if (isMobile(userSession))
				model.put(ONSEN_LIBS, true);
			try {
				ObjectMapper mapper = new ObjectMapper();
				RsaChallengeFormDTO rsaChallengeFormDTO = mapper.readValue(collect, RsaChallengeFormDTO.class);
				String sq1 = rsaChallengeFormDTO.getSq1();
				String ans1 = rsaChallengeFormDTO.getAns1();
				String cans1 = rsaChallengeFormDTO.getCans1();
				String sq2 = rsaChallengeFormDTO.getSq2();
				String ans2 = rsaChallengeFormDTO.getAns2();
				String cans2 = rsaChallengeFormDTO.getCans2();
				String sq3 = rsaChallengeFormDTO.getSq3();
				String ans3 = rsaChallengeFormDTO.getAns3();
				String cans3 = rsaChallengeFormDTO.getCans3();
				String typeDevice = rsaChallengeFormDTO.getTypeDevice();
				boolean saveDevice = false;
				
				if (typeDevice.equalsIgnoreCase("private"))
					saveDevice = true;
						
				if(sq1 != null && !sq1.isEmpty() && sq2 != null && !sq2.isEmpty() && sq3 != null && !sq3.isEmpty() &&
					ans1 != null && !ans1.isEmpty() && cans1 != null && !cans1.isEmpty() && ans1.equalsIgnoreCase(cans1) &&
					ans2 != null && !ans2.isEmpty() && cans2 != null && !cans2.isEmpty() && ans2.equalsIgnoreCase(cans2) &&
					ans3 != null && !ans3.isEmpty() && cans3 != null && !cans3.isEmpty() && ans3.equalsIgnoreCase(cans3) ) {
					CustomerServices customerService = userSession.getCustomerService();
		
		
					EnrollmentInformationinRSARequest eirsa = new EnrollmentInformationinRSARequest(
							Utils.generateCustomerInteractionEvent(request), userSession.getUsername(),
							userSession.getCustomerProfile().getEmail(), saveDevice, userSession.getDeviceRequest());
					eirsa.setQuestionsAndAnswer( sq1,sq2, sq3, ans1, ans2, ans3);
		
					RsaQuestionsUpdateResponse updateRSAresponse = customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(eirsa);
					
					if(!updateRSAresponse.getStatus().equals(ResultStatus.SUCCESS)){
						model.put(RESULT, FAIL);
						model.put(MESSAGE_ERROR, messageSource.getMessage("rsa.text.deactivateRSA.fail", null, language));
						log.error("Deactivate OOB: Unable to delete RSA user.");
						return new ModelAndView(RSAUIFORM, model);
					}
		
					userSession.getDeviceRequest().setDeviceTokenCookie(updateRSAresponse.getDeviceTokenCookie());
					// Increment OOB Removed By Edit count for Monthly Report
					customerService.logCustomerUsageData(userSession.getCustomerProfile(), CustomerActions.OOB_EDIT_DEL);
		
					// Reset RSA entitlement
					CustomerEntitlement rsaEnt = customer.getEntitlementByType(EntitlementType.RSA);
					if (rsaEnt != null)
						customerService.getCustomerProfileService().removeEntitlement(customer, rsaEnt);
					RSAUtils.createRSAEntitlement(customerService, customer);
					userSession.setOobEnroll(false);
		
					// Send confirmation email
					customerService.sendOobDeactivationEmail(userSession.getCustomerProfile(), userSession.getPreferredLanguage());
		
					Cookie cookie = WebUtils.getCookie(request, "PMData");
					if (cookie == null)
						cookie = new Cookie("PMData", "");
					cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
					cookie.setPath("/");
					ESAPI.httpUtilities().addCookie(response, cookie);
					model.put(DEVICE_TOKEN, userSession.getDeviceRequest().getDeviceTokenCookie());
					model.put(RESULT, SUCCESS);
				} else {
					model.put(RESULT, FAIL);
					model.put(MESSAGE_ERROR, messageSource.getMessage("rsa.text.deactivateRSA.fail", null, language));
					log.error("Deactivate OOB: Unable to delete RSA user.");
				}
				return new ModelAndView(RSAUIFORM, model);
			} catch (IOException ex) {
	            model.put(RESULT, FAIL);
				model.put(MESSAGE_ERROR, messageSource.getMessage("rsa.text.deactivateRSA.fail", null, language));
				log.error("Deactivate OOB: Unable to delete RSA user.");
	            return new ModelAndView(RSAUIFORM, model);
	        }
		}

		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
		return new ModelAndView(RSAUIFORM, null);
	}

	/**
	 * @param request
	 * @param response
	 * @return
     * @throws Exception
	 */
	public ModelAndView rsaModifyQuestions(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (REQUEST_POST_METHOD.equalsIgnoreCase(request.getMethod())) {
			Map<String, Object> model = new HashMap<>();
			UserSession userSession = getRequestUserSession(request);
			CustomerProfile customer = userSession.getCustomerProfile();
			Log log = LogFactory.getLog(getClass());
			model.put(LAYOUT_TYPE, JSON_LAYOUT);
			model.put(VIEW_PATH, views.get(JSON_TYPE));
			model.put(TIME_INTERVAL, timeInterval);
			String collect = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			Locale language = RequestContextUtils.getLocale(request);

			try {
				ObjectMapper mapper = new ObjectMapper();
				RsaChallengeFormDTO rsaChallengeFormDTO = mapper.readValue(collect, RsaChallengeFormDTO.class);

				String sq1 = rsaChallengeFormDTO.getSq1();
				String ans1 = rsaChallengeFormDTO.getAns1();
				String cans1 = rsaChallengeFormDTO.getCans1();
				String sq2 = rsaChallengeFormDTO.getSq2();
				String ans2 = rsaChallengeFormDTO.getAns2();
				String cans2 = rsaChallengeFormDTO.getCans2();
				String sq3 = rsaChallengeFormDTO.getSq3();
				String ans3 = rsaChallengeFormDTO.getAns3();
				String cans3 = rsaChallengeFormDTO.getCans3();
				String typeDevice = rsaChallengeFormDTO.getTypeDevice();
				if (isMobile(userSession))
					model.put(ONSEN_LIBS, true);

				boolean saveDevice = false;
				if (typeDevice.equalsIgnoreCase("private")) saveDevice = true;
				if (sq1 != null && !sq1.isEmpty() && sq2 != null && !sq2.isEmpty() && sq3 != null && !sq3.isEmpty() &&
						ans1 != null && !ans1.isEmpty() && cans1 != null && !cans1.isEmpty() && ans1.equalsIgnoreCase(cans1) &&
						ans2 != null && !ans2.isEmpty() && cans2 != null && !cans2.isEmpty() && ans2.equalsIgnoreCase(cans2) &&
						ans3 != null && !ans3.isEmpty() && cans3 != null && !cans3.isEmpty() && ans3.equalsIgnoreCase(cans3)) {
					CustomerServices customerService = userSession.getCustomerService();


					EnrollmentInformationinRSARequest eirsa = new EnrollmentInformationinRSARequest(
							Utils.generateCustomerInteractionEvent(request), userSession.getUsername(),
							userSession.getCustomerProfile().getEmail(), saveDevice, userSession.getDeviceRequest());
					eirsa.setQuestionsAndAnswer(sq1, sq2, sq3, ans1, ans2, ans3);

					RsaQuestionsUpdateResponse updateRSAresponse = customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(eirsa);

					if (ResultStatus.SUCCESS.equals(updateRSAresponse.getStatus())) {
						model.put(RESULT, SUCCESS);
						model.put("message", messageSource.getMessage("rsa.text.editQuestions.success", null, language));
						userSession.getDeviceRequest().setDeviceTokenCookie(updateRSAresponse.getDeviceTokenCookie());

						Cookie cookie = WebUtils.getCookie(request, "PMData");
						if (cookie == null)
							cookie = new Cookie("PMData", "");
						String strCookie = createCookieHeader(cookie.getName(), userSession.getDeviceRequest().getDeviceTokenCookie(), -1, null, "/", true, true);
						ESAPI.httpUtilities().addHeader(response, "Set-Cookie", strCookie);
						model.put(DEVICE_TOKEN, userSession.getDeviceRequest().getDeviceTokenCookie());
						AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
						if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
							Set<AlertType> alerts = ent.getAlerts();
							if (alerts.contains(AlertType.QUESTIONS_EDIT_EMAIL) || alerts.contains(AlertType.QUESTIONS_EDIT_SMS)
									|| alerts.contains(AlertType.QUESTIONS_EDIT_PUSH)) {
								customerService.sendQuestionsEditConfirmation(customer,
										userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
										userSession.getPreferredLanguage());

							}
						}
					} else {
						model.put(RESULT, FAIL);
						model.put(MESSAGE_ERROR, messageSource.getMessage("rsa.text.editQuestions.fail", null, language));
						log.error("Change Questions: Unable to change user security questions.");
					}


				} else {
					model.put(RESULT, FAIL);
					model.put(MESSAGE_ERROR, messageSource.getMessage("rsa.text.editQuestions.fail", null, language));
					log.error("Change Questions: Unable to change user security questions.");
				}
				return new ModelAndView(RSAUIFORM, model);
			} catch (IOException ex) {
				model.put(RESULT, FAIL);
				model.put(MESSAGE_ERROR, messageSource.getMessage("rsa.text.editQuestions.fail", null, language));
				log.error("Change Questions: Unable to change user security questions.");
				return new ModelAndView(RSAUIFORM, model);
			}
		}

		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
		return new ModelAndView(RSAUIFORM, null);
	}

	private String createCookieHeader(String name, String value, int maxAge, String domain, String path, boolean secure, boolean httpOnly) {
		String header = name + "=" + value;
		header = header + "; Max-Age=" + maxAge;
		if (domain != null) {
			header = header + "; Domain=" + domain;
		}

		if (path != null) {
			header = header + "; Path=" + path;
		}

		if (secure || ESAPI.securityConfiguration().getForceSecureCookies()) {
			header = header + "; Secure";
		}

		if (httpOnly || ESAPI.securityConfiguration().getForceHttpOnlyCookies()) {
			header = header + "; HttpOnly";
		}

		return header;
	}


	/**
	 *
	 * @param request
	 * @param response
	 * @throws Exception
	 * @return
	 */
	public ModelAndView rsaEditQuestions(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		response.getLocale();
		UserSession userSession = getRequestUserSession(request);
		model.put(LAYOUT_TYPE, FORM_LAYOUT);
		model.put(VIEW_PATH, views.get(RSA_EDIT_QUESTIONS));
		if (isMobile(userSession)){
			model.put(ONSEN_LIBS, true);
		}

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return new ModelAndView(RSAUIFORM, model);
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws Exception
	 * @return
	 */
	public ModelAndView rsaCheckStatus(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		response.getLocale();
		UserSession userSession = getRequestUserSession(request);

		model.put("oobEnrolled", userSession.getOobEnroll());
		model.put("responder_name", "rsaCheckStatus");
		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		if (isMobile(userSession)) {
			model.put(ONSEN_LIBS, true);
		}
		return new ModelAndView(RSAUIFORM, model);
	}

	/**
	 *
	 * @param request
	 * @param model
	 * @param method
	 * @return
	 */
	public ModelAndView rsaGetInfo(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> model = new HashMap<>();
		response.getLocale();
		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		UserSession userSession = getRequestUserSession(request);
		// Is enrolled in Mobile_Banking
		String enrollSMS = "EnrollSMS";
		if (userSession.getCustomerProfile() != null) {
			if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
				SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
				if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
						&& !smsprofile.getCtns().isEmpty()) {
					model.put(enrollSMS, true);
					String ctn = ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn();
					model.put(PRIMARY_PHONE, RSAUtils.setPhoneMask(ctn));
				} else {
					model.put(enrollSMS, false);
				}
			} else {
				model.put(enrollSMS, false);
			}
		}else {
			model.put(RESULT, false);
			return new ModelAndView(RSAUIFORM, model);
		}

		if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.RSA) != null) {
			CustomerEntitlement rsaEntitlement = userSession.getCustomerProfile()
					.getEntitlementByType(EntitlementType.RSA);

			// Get the ALTERNATE_PHONE
			String altPhone = rsaEntitlement.getString("ALTERNATE_PHONE");
			if (!(altPhone == null || altPhone.equals("") || altPhone.equals(N))) {
				model.put("altPhone", RSAUtils.setPhoneMask(altPhone));
				model.put("altPhoneHide", altPhone);
			}

			String altProvider = rsaEntitlement.getString("ALTERNATE_PROVIDER");
			if (!(altProvider == null || altProvider.equals("") || altProvider.equals(N))) {
				model.put("altProvider", altProvider);
			}

			String frecuency = rsaEntitlement.getString("ALWAYSCHALLENGE");
			if (frecuency == null || frecuency.equals("")) {
				model.put("frecuencyOOB", "FALSE");
			} else {
				model.put("frecuencyOOB", frecuency);
			}

			DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
					userSession.getDeviceRequest().getDeviceTokenCookie(), request);

			/**
			 * MBNA-1985 Method getRsaChallengeMethod : CIBP-WEB microservice integration
			 *
			 */
			// Microservice implementation
			RsaChallengeRequest rsaChallengeRequest = new RsaChallengeRequest(userSession.getUsername(), deviceRequest);

			CredentialType challengeType = customerService.rsaGetChallengeMethod(rsaChallengeRequest);
			userSession.setDeviceRequest(deviceRequest);
			if (deviceRequest != null && deviceRequest.getDeviceTokenCookie() != null) {
				model.put(DEVICE_TOKEN, deviceRequest.getDeviceTokenCookie());
			}
			if (challengeType != null && challengeType.equals(CredentialType.OOBPHONE)) {
				model.put(CHALLENGE_METHOD, CustomerOOBChallengeType.OOBPHONE.toString());
			} else {
				model.put(CHALLENGE_METHOD, CustomerOOBChallengeType.OOBSMS.toString());
			}
			model.put(RESULT, true);

		} else {
			model.put(RESULT, false);
		}

		return new ModelAndView(RSAUIFORM, model);

	}

	/**
	 *
	 * @param request
	 * @param model
	 * @param method
	 * @return
	 */
	public ModelAndView newRecoveryCodeOOB (HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> model = new HashMap<>();
		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		response.getLocale();
		UserSession userSession = getRequestUserSession(request);
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		CustomerEntitlement ent = customer.getEntitlementByType(EntitlementType.RSA);

		if (ent != null) {
			CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request);
			//Generate and save Recovery Code
			String recoveryCode = RSAUtils.generateOOBCode();

			Integer codeSalt = PasswordHashUtil.genereateRandomSalt();
			String codeHash = (PasswordHashUtil.generateSaltedHash(recoveryCode, codeSalt));

			Boolean alwaysChallenge = (ent.getString("ALWAYSCHALLENGE").equalsIgnoreCase("FALSE")) ? false : true;
			ModifyRSAEntitlementRequest modifyRequest = new ModifyRSAEntitlementRequest(interactionEvent,
					customer, ent.getString("ALTERNATE_PHONE"),
					ent.getString("ALTERNATE_PROVIDER"), alwaysChallenge, false, codeHash, codeSalt.toString());
			customerService.modifyCustomerRSAEntitlement(modifyRequest);

			WebUtils.setSessionAttribute(request, "oobRecoveryCode", recoveryCode);
			model.put("recoveryCode", recoveryCode);
			customerService.cleanOOBRecoveryCodeBlock(customer);
			// Send confirmation email when generate new recovery code.
			boolean emailSent = customerService.sendOobUpdateEmail(customer,
					userSession.getPreferredLanguage(),ChangeOOBBean.ACTION.GENERATE_RECOVERY_CODE.name());
			if(!emailSent) {
				this.logger.error(RecoveryCodeOOBForm.class.getName()+":"+"Could not send new oob recovery code confirmation email to " + customer.getUsername() + " ("
						+ customer.getEmail() + ")");
			}
			model.put(RESULT, SUCCESS);
		}else {
			model.put("formError", true);
		}
		return new ModelAndView(RSAUIFORM, model);
}

	/**
	 *
	 * @param request
	 * @param model
	 * @param method
	 * @return
	 */
	public ModelAndView updateRsaMobileProfile (HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> model = new HashMap<>();
		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		model.put(TIME_INTERVAL, timeInterval);
		MessageSource messageSource = this.messageSource;
		Log log = LogFactory.getLog(getClass());
		UserSession userSession = getRequestUserSession(request);
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		String profileID = "\nProfileID: ";
		String nCard = "\ncard: ";
		String errorMsgLog = "Error Updating SMS OOB Phone # while editing Cel #: calling ATH SmsInfo \nacct: ";
		String errorMsgLogUpdate = "Error Updating SMS OOB Phone # while editing Cel #: calling ATH smsUpdate \nacct: ";
		Locale language= RequestContextUtils.getLocale(request);
		String phone = null;
		String provider = null;
		phone = (String) WebUtils.getSessionAttribute(request, "smsPhonereceived");
		provider = (String) WebUtils.getSessionAttribute(request, "smsProvider");

		if (isMobile(userSession))
			model.put(ONSEN_LIBS, true);
		String code = (String) WebUtils.getSessionAttribute(request, "smsGeneratedCode");
		String codeNumberInp = null;

		if (request.getParameter("codeNumberInp") != null) {//Test here
			codeNumberInp = request.getParameter("codeNumberInp");
		}

		if (GenericValidator.isBlankOrNull(codeNumberInp) || GenericValidator.isBlankOrNull(code)
				|| !codeNumberInp.equals(code)) {//Test here
			model.put(RESULT, FAIL);
			return new ModelAndView(RSAUIFORM, model);
		}

		if (customer.getEntitlementByType(EntitlementType.MOBILE_BANKING) != null
				&& userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)) != null) {
			// means Customer is enrolled, must modify phone number
			SMSEntitlement entMobile = new SMSEntitlement(
					customer.getEntitlementByType(EntitlementType.MOBILE_BANKING));
			try {

				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)) {
					provider = DEFAULT_CARRIER;
				}

				SMSProfileModifyRequest modreq = new SMSProfileModifyRequest(
						Utils.generateCustomerInteractionEvent(request), customer,
						userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)), entMobile, phone,
						provider);
				customerService.modifySMSUserProfile(modreq);
				model.put("message", messageSource.getMessage("rsa.text.editRsa.rsaphone.modified.success", null, language));


				WebUtils.setSessionAttribute(request, "smsUpdateSuccess", true);

				/* CIBP-1104 */
				boolean hasalert = false;
				AlertsEntitlement alertEnt = new AlertsEntitlement(
						customer.getEntitlementByType(EntitlementType.ALERTS));
				if (alertEnt != null && alertEnt.getAlerts() != null) {
					Set<AlertType> alerts = alertEnt.getAlerts();
					if (alerts.contains(AlertType.MOBILE_PHONE_EDIT_EMAIL) || alerts.contains(AlertType.MOBILE_PHONE_EDIT_SMS)
							|| alerts.contains(AlertType.MOBILE_PHONE_EDIT_PUSH)) {
						hasalert = true;
					}
				}
				/*
				 * CIBP-1104 se activa envio alerta de cambio de
				 * telefono banca movil
				 */
				if (hasalert) {
					customerService.sendMobilePhoneEditConfirmation(customer,
							userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
							userSession.getPreferredLanguage());
				}
				/* FIN--- CIBP-1104 */

				/*
				 * MBSD-1171 Send a email always the customer edit the
				 * primary phone on OOB.
				 */
				boolean emailSent = customerService.sendOobUpdateEmail(customer,
						userSession.getPreferredLanguage(), ChangeOOBBean.ACTION.EDIT_PRIMARY_PHONE.name());
				if (!emailSent) {
					this.log.error(SmsOOBForm.class.getName() + ":"
							+ "Could not send oob update frequency confirmation email to "
							+ customer.getUsername() + " (" + customer.getEmail() + ")");
				}
				/* FIN--- MBSD-1171 */


				AccountMaskUtil maskutil = new AccountMaskUtil();
				List<TVFrontendAccount> accounts = userSession.getAccounts();
				List<evertec.cibp.core.services.smsAlerts.SmsBean> smsBeans = new LinkedList<>();


				for (TVFrontendAccount account : accounts) {
					String suffix;
					if (account != null && account.getAccountNumberSuffix() != null) {
						suffix = account.getAccountNumberSuffix();
					} else {
						suffix = "";
					}
					if ((!suffix.equalsIgnoreCase("")) && suffix.equalsIgnoreCase("RES")
							|| suffix.equalsIgnoreCase("SAV"))
						continue;
					if (account != null && !account.getHide()
							&& ("009".equalsIgnoreCase(account.getAccountProductId())// Acceso
																						// Popular
									|| "015".equalsIgnoreCase(account.getAccountProductId())// Cuenta
																							// Popular
									|| "017".equalsIgnoreCase(account.getAccountProductId())// Chequera
																							// Popular
									|| "020".equalsIgnoreCase(account.getAccountProductId())// Avance
																							// 50
									|| "023".equalsIgnoreCase(account.getAccountProductId())// Multicuenta
																							// Staff
									|| "024".equalsIgnoreCase(account.getAccountProductId())// Popular
																							// Plus
									|| "025".equalsIgnoreCase(account.getAccountProductId())// Multicuenta
																							// Popular
									|| "026".equalsIgnoreCase(account.getAccountProductId())// IDEAL
									|| "028".equalsIgnoreCase(account.getAccountProductId())// MaxiPremium
									|| "029".equalsIgnoreCase(account.getAccountProductId())// Private
																							// Management
																							// Account
									|| "049".equalsIgnoreCase(account.getAccountProductId())// Ahorro
																							// a
																							// Toda
																							// Hora
																							// Staff
									|| "051".equalsIgnoreCase(account.getAccountProductId())// Popular
																							// Savings
									|| "052".equalsIgnoreCase(account.getAccountProductId())// Ahorro
																							// a
																							// Toda
																							// Hora
									|| "060".equalsIgnoreCase(account.getAccountProductId())// ATH
																							// POP
									|| "062".equalsIgnoreCase(account.getAccountProductId())// ENVIA
																							// ATH
									|| "073".equalsIgnoreCase(account.getAccountProductId())// Acceso
																							// ETA
									|| "075".equalsIgnoreCase(account.getAccountProductId())// Popular
																							// Sec
																							// Investors
																							// Plus
									|| "078".equalsIgnoreCase(account.getAccountProductId())// Payroll
																							// Card
									|| "080".equalsIgnoreCase(account.getAccountProductId())// Acceso
																							// Universitario
									|| "081".equalsIgnoreCase(account.getAccountProductId())// e-account
									|| "082".equalsIgnoreCase(account.getAccountProductId())// Redi
																							// Equity
									|| "083".equalsIgnoreCase(account.getAccountProductId())// Redi
																							// Equity
																							// Staff
									|| "087".equalsIgnoreCase(account.getAccountProductId())// CIMA
									|| "089".equalsIgnoreCase(account.getAccountProductId())))// Tarjeta
																								// de
																								// Nomina
					{

						try {

							CardPlasticFrontend[] plastics = userSession.getCardPlastics(account);
							int plasticLength = plastics.length;
							if (plasticLength > 0 && !(account.getCurrentAthSmsAlerts() != null
									&& account.getCurrentAthSmsAlerts().size() > 0)) {
								HashMap<Object, Object> smsInfo = new HashMap<>();

								for (int i = 0; i < plasticLength; i++) {
									evertec.cibp.core.services.smsAlerts.SmsBean smsBeanRequest = new evertec.cibp.core.services.smsAlerts.SmsBean();
									smsBeanRequest.setAcctNum(account.getAccountNumber());
									smsBeanRequest.setTaxId(customer.getTaxId());
									smsBeanRequest.setCardNum(
											plastics[i].getCardPlasticAccountRecord().getCardPANId());

									SmsAlertsResponse smsAlertsResponse = customerService.getSmsAlertInfo(
											smsBeanRequest, Utils.generateCustomerInteractionEvent(request));
									if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
											&& smsAlertsResponse.getResponseSmsBean() != null) {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
												smsAlertsResponse.getResponseSmsBean());
										smsBeans.add(smsAlertsResponse.getResponseSmsBean());
									} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
											&& smsAlertsResponse.getResponseSmsBean() == null) {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
												null);
										if (!smsAlertsResponse.isAuditable()) {
											log.error(
													"Error Updating SMS OOB Phone # while editing Cel #: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(smsBeanRequest.getCardNum()) + System.getProperty("line.separator")
															+ customer.getFullName() + profileID
															+ customer.getProfileId()
															+ smsAlertsResponse.getAuditString());
										}
										else {
											log.error(
													"Error Updating SMS OOB Phone # while editing Cel #: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(smsBeanRequest.getCardNum()) + System.getProperty("line.separator")
															+ customer.getFullName() + profileID
															+ customer.getProfileId());
										}
									} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
											&& smsAlertsResponse.getErrorString()
													.contains("ERROR: SmsInfo NOT SENT.")) {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
												null);
										if (!smsAlertsResponse.isAuditable()) {
											log.error(
													"Error Updating SMS OOB Phone # while editing Cel #: Unable to send ATH SmsInfo request for \nacct: "
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(smsBeanRequest.getCardNum()) + System.getProperty("line.separator")
															+ customer.getFullName() + profileID
															+ customer.getProfileId());
										} else {
											log.error(
													"Error Updating SMS OOB Phone # while editing Cel #: Unable to send ATH SmsInfo request for \nacct: "
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(smsBeanRequest.getCardNum()) + System.getProperty("line.separator")
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + System.getProperty("line.separator")
															+ smsAlertsResponse.getAuditString());
										}
									} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
											&& (smsAlertsResponse.getException() != null
													|| !GenericValidator.isBlankOrNull(
															smsAlertsResponse.getSmsAlertsException()))) {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
												null);
										if (!smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(
														smsAlertsResponse.getSmsAlertsException())
												&& smsAlertsResponse.getException() != null) {
											log.error(errorMsgLog + maskutil.mask(account.getAccountNumber())
													+ nCard + maskutil.mask(smsBeanRequest.getCardNum()) + System.getProperty("line.separator")
													+ customer.getFullName() + profileID
													+ customer.getProfileId() + System.getProperty("line.separator")
													+ smsAlertsResponse.getSmsAlertsException() + System.getProperty("line.separator")
													+ smsAlertsResponse.getException());
										} else if (!smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(
														smsAlertsResponse.getSmsAlertsException())
												&& smsAlertsResponse.getException() == null) {
											log.error(errorMsgLog + maskutil.mask(account.getAccountNumber())
													+ nCard + maskutil.mask(smsBeanRequest.getCardNum()) + System.getProperty("line.separator")
													+ customer.getFullName() + profileID
													+ customer.getProfileId() + System.getProperty("line.separator")
													+ smsAlertsResponse.getSmsAlertsException());
										} else if (!smsAlertsResponse.isAuditable()
												&& GenericValidator.isBlankOrNull(
														smsAlertsResponse.getSmsAlertsException())
												&& smsAlertsResponse.getException() != null) {
											log.error(errorMsgLog + maskutil.mask(account.getAccountNumber())
													+ nCard + maskutil.mask(smsBeanRequest.getCardNum()) + System.getProperty("line.separator")
													+ customer.getFullName() + profileID
													+ customer.getProfileId() + System.getProperty("line.separator")
													+ smsAlertsResponse.getException());
										} else if (smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(
														smsAlertsResponse.getSmsAlertsException())
												&& smsAlertsResponse.getException() != null) {
											log.error(errorMsgLog + maskutil.mask(account.getAccountNumber())
													+ nCard + maskutil.mask(smsBeanRequest.getCardNum()) + System.getProperty("line.separator")
													+ customer.getFullName() + profileID
													+ customer.getProfileId() + System.getProperty("line.separator")
													+ smsAlertsResponse.getSmsAlertsException() + System.getProperty("line.separator")
													+ smsAlertsResponse.getAuditString());
										} else {
											log.error(errorMsgLog + maskutil.mask(account.getAccountNumber())
													+ nCard + maskutil.mask(smsBeanRequest.getCardNum()) + System.getProperty("line.separator")
													+ customer.getFullName() + profileID
													+ customer.getProfileId() + System.getProperty("line.separator")
													+ smsAlertsResponse.getAuditString());
										}
									} else {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
												null);
										log.error(errorMsgLog + maskutil.mask(account.getAccountNumber())
												+ nCard + maskutil.mask(smsBeanRequest.getCardNum()) + System.getProperty("line.separator")
												+ customer.getFullName() + profileID + customer.getProfileId()
												+ "\n");
									}
								}
								if (smsInfo.size() > 0)
									account.setCurrentAthSmsAlerts(smsInfo);
							}
							plasticLength = plastics.length;
							if (plasticLength > 0 && account.getCurrentAthSmsAlerts() != null
									&& account.getCurrentAthSmsAlerts().size() > 0) {
								for (int i = 0; i < plasticLength; i++) {
									if (account.getCurrentAthSmsAlerts().containsKey(
											plastics[i].getCardPlasticAccountRecord().getCardPANId())
											&& (account.getCurrentAthSmsAlerts().get(plastics[i]
													.getCardPlasticAccountRecord().getCardPANId()) != null)) {
										smsBeans.add((evertec.cibp.core.services.smsAlerts.SmsBean) account
												.getCurrentAthSmsAlerts()
												.get(plastics[i].getCardPlasticAccountRecord().getCardPANId()));
									}
								}
							}

							List<evertec.cibp.core.services.smsAlerts.SmsBean> forUpdating = new LinkedList<>();

							if (smsBeans != null && !smsBeans.isEmpty()) {
								for (evertec.cibp.core.services.smsAlerts.SmsBean sms : smsBeans) {
									sms.setPrimaryphone(phone);
									sms.setPrimaryPhoneCarrier(provider);
									forUpdating.add(sms);
								}
							}

							if (forUpdating != null && !forUpdating.isEmpty()) {

								for (evertec.cibp.core.services.smsAlerts.SmsBean sms : forUpdating) {
									evertec.cibp.core.services.smsAlerts.SmsBean newBean = new evertec.cibp.core.services.smsAlerts.SmsBean();

									org.apache.commons.beanutils.BeanUtils.copyProperties(newBean, sms);

									/** MBNA-3529
									 * Method updateSmsAlert : CIBP-WEB microservice integration
									 */
									SmsAlertsResponse smsAlertsResponse = customerService.updateSmsAlert(
											newBean, Utils.generateCustomerInteractionEvent(request));
									if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
											&& smsAlertsResponse.getResponseSmsBean() != null) {
										account.getCurrentAthSmsAlerts().put(sms.getCardNum(),
												smsAlertsResponse.getResponseSmsBean());
									} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
											&& smsAlertsResponse.getResponseSmsBean() == null) {
										if (!smsAlertsResponse.isAuditable()) {
											log.error(
													"Error Updating SMS OOB Phone # while editing Cel #: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(sms.getCardNum()) + System.getProperty("line.separator")
															+ customer.getFullName() + profileID
															+ customer.getProfileId()
															+ smsAlertsResponse.getAuditString());
										}else {
											log.error(
													"Error Updating SMS OOB Phone # while editing Cel #: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(sms.getCardNum()) + System.getProperty("line.separator")
															+ customer.getFullName() + profileID
															+ customer.getProfileId());
										}
									} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
											&& smsAlertsResponse.getErrorString()
													.contains("ERROR: SmsInfo NOT SENT.")) {
										if (!smsAlertsResponse.isAuditable()) {
											log.error(
													"Error Updating SMS OOB Phone # while editing Cel #: Unable to send ATH smsUpdate request for \nacct: "
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(sms.getCardNum()) + System.getProperty("line.separator")
															+ customer.getFullName() + profileID
															+ customer.getProfileId());
										}else {
											log.error(
													"Error Updating SMS OOB Phone # while editing Cel #: Unable to send ATH smsUpdate request for \nacct: "
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(sms.getCardNum()) + System.getProperty("line.separator")
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + System.getProperty("line.separator")
															+ smsAlertsResponse.getAuditString());
										}
									} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
											&& (smsAlertsResponse.getException() != null
													|| !GenericValidator.isBlankOrNull(
															smsAlertsResponse.getSmsAlertsException()))) {
										if (!smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(
														smsAlertsResponse.getSmsAlertsException())
												&& smsAlertsResponse.getException() != null) {
											log.error(errorMsgLogUpdate
													+ maskutil.mask(account.getAccountNumber()) + nCard
													+ maskutil.mask(sms.getCardNum()) + System.getProperty("line.separator")
													+ customer.getFullName() + profileID
													+ customer.getProfileId() + System.getProperty("line.separator")
													+ smsAlertsResponse.getSmsAlertsException() + System.getProperty("line.separator")
													+ smsAlertsResponse.getException());
										}else if (!smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(
														smsAlertsResponse.getSmsAlertsException())
												&& smsAlertsResponse.getException() == null) {
											log.error(errorMsgLogUpdate
													+ maskutil.mask(account.getAccountNumber()) + nCard
													+ maskutil.mask(sms.getCardNum()) + System.getProperty("line.separator")
													+ customer.getFullName() + profileID
													+ customer.getProfileId() + System.getProperty("line.separator")
													+ smsAlertsResponse.getSmsAlertsException());
										} else if (!smsAlertsResponse.isAuditable()
												&& GenericValidator.isBlankOrNull(
														smsAlertsResponse.getSmsAlertsException())
												&& smsAlertsResponse.getException() != null) {
											log.error(errorMsgLogUpdate
													+ maskutil.mask(account.getAccountNumber()) + nCard
													+ maskutil.mask(sms.getCardNum()) + System.getProperty("line.separator")
													+ customer.getFullName() + profileID
													+ customer.getProfileId() + System.getProperty("line.separator")
													+ smsAlertsResponse.getException());
										} else if (smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(
														smsAlertsResponse.getSmsAlertsException())
												&& smsAlertsResponse.getException() != null) {
											log.error(errorMsgLogUpdate
													+ maskutil.mask(account.getAccountNumber()) + nCard
													+ maskutil.mask(sms.getCardNum()) + System.getProperty("line.separator")
													+ customer.getFullName() + profileID
													+ customer.getProfileId() + System.getProperty("line.separator")
													+ smsAlertsResponse.getSmsAlertsException() + System.getProperty("line.separator")
													+ smsAlertsResponse.getAuditString());
										}else {
											log.error(errorMsgLogUpdate
													+ maskutil.mask(account.getAccountNumber()) + nCard
													+ maskutil.mask(sms.getCardNum()) + System.getProperty("line.separator")
													+ customer.getFullName() + profileID
													+ customer.getProfileId() + System.getProperty("line.separator")
													+ smsAlertsResponse.getAuditString());
										}
									} else {

										if (!smsAlertsResponse.isAuditable()) {
											log.error(errorMsgLogUpdate
													+ maskutil.mask(account.getAccountNumber()) + nCard
													+ maskutil.mask(sms.getCardNum()) + System.getProperty("line.separator")
													+ customer.getFullName() + profileID
													+ customer.getProfileId() + System.getProperty("line.separator"));
										}else {
											log.error(errorMsgLogUpdate
													+ maskutil.mask(account.getAccountNumber()) + nCard
													+ maskutil.mask(sms.getCardNum()) + System.getProperty("line.separator")
													+ customer.getFullName() + profileID
													+ customer.getProfileId() + System.getProperty("line.separator")
													+ smsAlertsResponse.getAuditString());
										}
									}

								}

							}

						} catch (Exception e) {
							log.error("Error Updating SMS OOB Phone # while editing Cel #: \nacct: "
									+ maskutil.mask(account.getAccountNumber()) + "\n" + profileID
									+ customer.getProfileId(), e);
						}

					}
				}


				model.put(RESULT, SUCCESS);
				model.put(PRIMARY_PHONE, phone);
			} catch (Exception e) {
				model.put(MESSAGE_ERROR, messageSource.getMessage("rsa.text.editRsa.rsaphone.modified.fail", null, language));


				model.put(RESULT, FAIL);
			}
		} else {
			model.put(MESSAGE_ERROR, messageSource.getMessage("rsa.text.editRsa.rsaphone.modified.fail", null, language));
			model.put(RESULT, FAIL);
		}

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		cookie.setPath("/");
		ESAPI.httpUtilities().addCookie(response, cookie);
		return new ModelAndView(RSAUIFORM, model);
	}

	/**
	 *
	 * @param request
	 * @param model
	 * @param method
	 * @return
	 */
	public ModelAndView updateRsaProfile (HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> model = new HashMap<>();
		model.put(LAYOUT_TYPE, JSON_LAYOUT);
		model.put(VIEW_PATH, views.get(JSON_TYPE));
		MessageSource messageSource = this.messageSource;
		UserSession userSession = getRequestUserSession(request);
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerEntitlement rsaEnt = customer.getEntitlementByType(EntitlementType.RSA);
		CustomerInteractionEvent  event = Utils.generateCustomerInteractionEvent(request, userSession.getUsername());
		Map<String, String> optionsToUpdate = new HashMap<>();
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);
		String challengeType = null;
		/**
		 * MBNA-1985
		 * Method getRsaChallengeMethod : CIBP-WEB microservice integration
		 *
		 */
		//Microservice implementation
		RsaChallengeRequest rsaChallengeRequest = new RsaChallengeRequest(userSession.getUsername(),deviceRequest);

		CredentialType challengeType2 = customerService.rsaGetChallengeMethod(rsaChallengeRequest);

		userSession.setDeviceRequest(deviceRequest);
		if (deviceRequest.getDeviceTokenCookie() != null) {
			model.put(DEVICE_TOKEN, deviceRequest.getDeviceTokenCookie());
		}
		if (challengeType2 != null && challengeType2.equals(CredentialType.OOBPHONE)) {
			challengeType = CustomerOOBChallengeType.OOBPHONE.toString();
		} else {
			challengeType = CustomerOOBChallengeType.OOBSMS.toString();
		}
		boolean frequency = (rsaEnt.getString("ALWAYSCHALLENGE").equalsIgnoreCase("FALSE")) ? false : true;
		String alternatePhone = rsaEnt.getString("ALTERNATE_PHONE");
		String alternateProvider = rsaEnt.getString("ALTERNATE_PROVIDER");
		boolean modifyProfileAction = false;
		boolean primaryPhoneUpd = false;
		StringBuilder message = new StringBuilder();
		StringBuilder messageError = new StringBuilder();
		String result = "";


		if(request.getParameter("primaryPhoneUpd") != null) {
			primaryPhoneUpd = Boolean.valueOf(request.getParameter("primaryPhoneUpd"));
			if(primaryPhoneUpd) {
				modifyProfileAction = true;
				optionsToUpdate.put(ChangeOOBBean.ACTION.EDIT_PRIMARY_PHONE.name(), "Could not send oob primary phome confirmation email to ");
			}
		}
		if(request.getParameter(CHALLENGE_TYPE) != null && !request.getParameter(CHALLENGE_TYPE).equalsIgnoreCase(challengeType)) {
			challengeType = request.getParameter(CHALLENGE_TYPE);
			modifyProfileAction = true;
			optionsToUpdate.put(ChangeOOBBean.ACTION.EDIT_CHALLENGE_TYPE.name(), "Could not send oob update delivery method confirmation email to ");
		}
		boolean freq = String.valueOf(request.getParameter(FREQUENCY2)).equalsIgnoreCase(ALLOW)  ? false : true;
		if(request.getParameter(FREQUENCY2) != null && freq != frequency) {
			frequency = freq;
			modifyProfileAction = true;
			optionsToUpdate.put(ChangeOOBBean.ACTION.EDIT_CHALLENGE_FREQUENCY.name(), "Could not send oob update frequency confirmation email to ");

		}
		if((request.getParameter(ALTERNATE_PHONE) != null && !request.getParameter(ALTERNATE_PHONE).equalsIgnoreCase(alternatePhone))
				|| (request.getParameter(ALTERNATE_PROVIDER) != null && !request.getParameter(ALTERNATE_PROVIDER).equalsIgnoreCase(alternateProvider))) {
			alternatePhone = request.getParameter(ALTERNATE_PHONE);
			alternateProvider = request.getParameter(ALTERNATE_PROVIDER);
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)) {
				alternateProvider = DEFAULT_CARRIER;
			}
			modifyProfileAction = true;
			optionsToUpdate.put(ChangeOOBBean.ACTION.EDIT_BACKUP_PHONE.name(), "Could not send oob update alternate phone confirmation email to ");

		}
		if(modifyProfileAction) {
			if(optionsToUpdate.containsKey(ChangeOOBBean.ACTION.EDIT_CHALLENGE_FREQUENCY.name()) || optionsToUpdate.containsKey(ChangeOOBBean.ACTION.EDIT_BACKUP_PHONE.name())) {
				if(updateRsaProfile(frequency, alternatePhone, alternateProvider,  rsaEnt, event, customer, optionsToUpdate, userSession)) {
					message.append(messageSource.getMessage("rsa.text.editRsa.rsaProfile.modified.success", null, RequestContextUtils.getLocale(request)));
					result = SUCCESS;
				} else {
					messageError.append(messageSource.getMessage("rsa.text.editRsa.rsaProfile.modified.fail", null, RequestContextUtils.getLocale(request)));
					result = FAIL;
				}
			}
			if(optionsToUpdate.containsKey(ChangeOOBBean.ACTION.EDIT_CHALLENGE_TYPE.name())) {
				if (saveChallengeType(request, userSession, challengeType, optionsToUpdate)) {
					message.append(", ").append(messageSource.getMessage("rsa.text.editRsa.challengeType.modified.success", null, RequestContextUtils.getLocale(request)));
					result = SUCCESS;
				} else {
					messageError.append(", ").append(messageSource.getMessage("rsa.text.editRsa.challengeType.modified.fail", null, RequestContextUtils.getLocale(request)));
					result = FAIL;
				}
			}
			if(optionsToUpdate.containsKey(ChangeOOBBean.ACTION.EDIT_PRIMARY_PHONE.name())) {
				message.append(", ").append(messageSource.getMessage("rsa.text.editRsa.rsaProfile.modified.success", null, RequestContextUtils.getLocale(request)));
				result = SUCCESS;
			}
		} else {
			messageError.append(messageSource.getMessage("rsa.text.error.update", null, RequestContextUtils.getLocale(request)));
			result = "noChanges";
		}
		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		cookie.setPath("/");
		ESAPI.httpUtilities().addCookie(response, cookie);
		model.put(MESSAGE_ERROR, messageError);

		model.put(TIME_INTERVAL, timeInterval);
		model.put("message", message);
		model.put(RESULT, result);
		return new ModelAndView(RSAUIFORM, model);
	}

	private boolean saveChallengeType(HttpServletRequest request, UserSession userSession,
			String challengeType, Map<String, String> optionsToUpdate) {
		UpdateUserRequest updRequest = new UpdateUserRequest();
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);
		updRequest.setDeviceRequest(deviceRequest);
		Log log = LogFactory.getLog(getClass());

		/**
		* MBNA-2530
		* Method updateRSAOOB : CIBP-WEB microservice integration
		*
		*/
		final RSAActionRequest actionRequest = new RSAActionRequest(userSession.getUsername(), Utils.getRSALanguage(request), deviceRequest);
		RsaOobResponse updResponse = customerService.rsaUpdateUserOOB(actionRequest, challengeType);

		if (updResponse.getResultStatus().equals(ResultStatus.SUCCESS)) {
			deviceRequest.setDeviceTokenCookie(updResponse.getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(updResponse.getDeviceTokenCookie());
			userSession.setDeviceRequest(deviceRequest);
			sendChangeProfileEmail(ChangeOOBBean.ACTION.EDIT_CHALLENGE_TYPE.name(), userSession, optionsToUpdate.get(ChangeOOBBean.ACTION.EDIT_CHALLENGE_TYPE.name()));
			return true;
		} else {
			log.error(ChallengeTypeOOBForm.class.getName()+":"+"Could update challenge type " + userSession.getCustomerProfile().getUsername() + " ("
					+ userSession.getCustomerProfile().getEmail() + ")");
			return false;
		}
	}

	private boolean updateRsaProfile(boolean frequency ,String alternatePhone ,String alternateProvider, CustomerEntitlement rsaEnt, CustomerInteractionEvent  event,
			CustomerProfile customer, Map<String, String> optionsToUpdate, UserSession userSession) {
		try {
			ModifyRSAEntitlementRequest modifyRequest = new ModifyRSAEntitlementRequest(event,
					customer, alternatePhone, alternateProvider, frequency, false, rsaEnt.getString("CODE_HASH"), rsaEnt.getString("CODE_SALT"));
			customerService.modifyCustomerRSAEntitlement(modifyRequest);

			if(optionsToUpdate.containsKey(ChangeOOBBean.ACTION.EDIT_BACKUP_PHONE.name())) {
				// Increment alternate phone counter for Monthly Report
				if (rsaEnt.getString("ALTERNATE_PHONE").equalsIgnoreCase(N))
					customerService.logCustomerUsageData(customer, CustomerActions.OOB_ALT_PHONE);
				sendChangeProfileEmail(ChangeOOBBean.ACTION.EDIT_BACKUP_PHONE.name(), userSession, optionsToUpdate.get(ChangeOOBBean.ACTION.EDIT_BACKUP_PHONE.name()));
			} else if (optionsToUpdate.containsKey(ChangeOOBBean.ACTION.EDIT_CHALLENGE_FREQUENCY.name())) {
				sendChangeProfileEmail(ChangeOOBBean.ACTION.EDIT_CHALLENGE_FREQUENCY.name(), userSession, optionsToUpdate.get(ChangeOOBBean.ACTION.EDIT_CHALLENGE_FREQUENCY.name()));
			}
		} catch (Exception e) {
			this.log.error(ChallengeTypeOOBForm.class.getName()+":"+"Could update rsa profile " + userSession.getCustomerProfile().getUsername() + " ("
					+ userSession.getCustomerProfile().getEmail() + "frequency: " + frequency + ", alternate number:" + alternatePhone.replaceAll("\r", "_").replaceAll("\n", "_") + ")");
			return false;
		}
		return true;
	}

	/**
	 * @param
	 * @return
	 */
	private boolean sendChangeProfileEmail(String oobOption, UserSession userSession, String errorMsg) {
		boolean emailSent = customerService.sendOobUpdateEmail(userSession.getCustomerProfile(),
				userSession.getPreferredLanguage(),oobOption);
		if(!emailSent) {
			this.log.error(ChallengeTypeOOBForm.class.getName()+":"+errorMsg + userSession.getCustomerProfile().getUsername() + " ("
					+ userSession.getCustomerProfile().getEmail() + ")");
		}
		return emailSent;
	}

	/**
	 * verify Lenguage to prevent question keep previous language
	 * @param userSession
	 * @param request
	 * abstract, assert, boolean, break, byte, case, catch, char, class, const, continue, continue, default, do, double, else, enum, extends, false, final, finally, float, for, goto, if, implements, import, instanceof, int, interface, long, native, new, null, package, private, protected, public, return, short, static, strictfp, super, switch, synchronized, this, throw, throws, transient, true, try, void, volatile, while
	 */
	private void verifyChangeLanguageQuestion(UserSession userSession, HttpServletRequest request) {

		final Locale newLang = RequestContextUtils.getLocale(request);	// user language on request
		if(lastLang == null) {
			lastLang =  newLang;
		} else if (!lastLang.equals(newLang)){
			userSession.setInformationQuestion(null);
			lastLang =  newLang;
		}
	}
	
	
	
	/**
	 * @param request
	 * @return
	 */
	private UserSession getRequestUserSession(HttpServletRequest request) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USRSSESION);
		return userSession;
	}

	/**
	 * @return the customerService
	 */
	public CustomerServices getCustomerService() {
		return customerService;
	}

	/**
	 * @param customerService the customerService to set
	 */
	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * @return the recCodeBlockLength
	 */
	public int getRecCodeBlockLength() {
		return recCodeBlockLength;
	}

	/**
	 * @param recCodeBlockLength the recCodeBlockLength to set
	 */
	public void setRecCodeBlockLength(int recCodeBlockLength) {
		this.recCodeBlockLength = recCodeBlockLength;
	}

	/**
	 * @return the recCodeMaxRetries
	 */
	public int getRecCodeMaxRetries() {
		return recCodeMaxRetries;
	}

	/**
	 * @param recCodeMaxRetries the recCodeMaxRetries to set
	 */
	public void setRecCodeMaxRetries(int recCodeMaxRetries) {
		this.recCodeMaxRetries = recCodeMaxRetries;
	}

	/**
	 * @return the recCodeRetryTimePeriod
	 */
	public int getRecCodeRetryTimePeriod() {
		return recCodeRetryTimePeriod;
	}

	/**
	 * @param recCodeRetryTimePeriod the recCodeRetryTimePeriod to set
	 */
	public void setRecCodeRetryTimePeriod(int recCodeRetryTimePeriod) {
		this.recCodeRetryTimePeriod = recCodeRetryTimePeriod;
	}

	/**
	 * @return the timeInterval
	 */
	public int getTimeInterval() {
		return timeInterval;
	}

	/**
	 * @param timeInterval the timeInterval to set
	 */
	public void setTimeInterval(int timeInterval) {
		this.timeInterval = timeInterval;
	}

}
