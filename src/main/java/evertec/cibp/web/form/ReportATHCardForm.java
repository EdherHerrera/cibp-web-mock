package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.requests.ReportATHCardRequest;
import evertec.cibp.core.services.responses.ReportATHCardResponse;
import evertec.cibp.web.form.bean.ReportATHCardBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.AthmUtils;
import evertec.cibp.web.utils.Utils;


/**
 * @author jocampo
 */
public class ReportATHCardForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	
	private static final String VIEW_NAME = "reportATHCard";
	
	private String accountSubtypes;
	private String accountProductIds;

	public ReportATHCardForm() {
		super();
		setCommandClass(ReportATHCardBean.class);
		setCommandName(VIEW_NAME);
		setFormView(VIEW_NAME);
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		
		boolean errorForm = false;
		boolean successForm = false;

		ReportATHCardBean bean = (ReportATHCardBean) command;
		ReportATHCardRequest reportRequest = new ReportATHCardRequest();
		
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		TVFrontendAccount account = Utils.getAccountById(bean.getAccount(), userSession.getAccounts());
		CardPlasticAccountRecord plastic = userSession.getCardPlastic(bean.getAccount(), bean.getCardNumber());
		
		if(account != null && plastic != null){				
			reportRequest.setCardAccount(plastic);
			reportRequest.setCustomerProfile(userSession.getCustomerProfile());
			reportRequest.setAccount(account);
			reportRequest.setDateOfNotification(bean.getDateOfNotification());
			reportRequest.setLastPurchaseDate(bean.getLastPurchaseDate());
			reportRequest.setLastPurchaseLocation(bean.getLastPurchaseLocation());
			reportRequest.setLastPurchaseCity(bean.getLastPurchaseCity());
			
			reportRequest.setDisputeNumber(bean.getDisputeNumber());
			reportRequest.setNewCardRequested(Boolean.FALSE);
			reportRequest.setMaintenanceReason(bean.getReason());
			reportRequest.setPlaceOfNotification(bean.getPlaceOfNotification());
			reportRequest.setReceivedBy("");
			reportRequest.setSignedBy("");
			reportRequest.setCardWasSigned(Boolean.FALSE);
			reportRequest.setPoliceWasNotified(bean.isPoliceNotified());
			reportRequest.setCardValidated(Boolean.FALSE);
			
			reportRequest.setLanguage((Utils.getRSALanguage(request).equals("sp") ? "S" : "E"));
			
			reportRequest.setBankingSession(userSession.getBankingSession());
			
			CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request);
			
			ReportATHCardResponse reportResponse = userSession.getCustomerService().reportATHCard(reportRequest, event);
			
			if(reportResponse.getStatus().equals(ReportATHCardResponse.STATUS.ERROR)){
				errorForm = true;
			}
			else{
				successForm = true;
			}
		}
		else{
			errorForm = true;
		}
		model.put("errorForm", errorForm);
		model.put("successForm", successForm);
		return showForm(request, error, VIEW_NAME, model);
	}
	

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		ReportATHCardBean bean = (ReportATHCardBean) command;
		
		if (bean.getAccount().equals("none") || bean.getCardNumber().equals("none")){
			errors.rejectValue("cardNumber", "report.ath.card.account.required", "Please select an account and card number");
		}
		if (!(bean.getReason().equals(AthmUtils.REPORT_REASON_LOST) || bean.getReason().equals(AthmUtils.REPORT_REASON_STOLEN))){
			errors.rejectValue("reason", "report.ath.card.required", "Required");
		}
		if (GenericValidator.isBlankOrNull(bean.getLastPurchaseLocation())){
			errors.rejectValue("lastPurchaseLocation", "report.ath.card.required", "Required");
		}
		if (GenericValidator.isBlankOrNull(bean.getLastPurchaseCity())){
			errors.rejectValue("lastPurchaseCity", "report.ath.card.required", "Required");
		}
		if (bean.getLastPurchaseDate() == null){
			errors.rejectValue("lastPurchaseDate", "report.ath.card.required", "Required");
		}
		if(bean.isPoliceNotified() || bean.getReason().equals(AthmUtils.REPORT_REASON_STOLEN)){
			bean.setPoliceNotified(Boolean.TRUE);
			if (GenericValidator.isBlankOrNull(bean.getPlaceOfNotification())){
				errors.rejectValue("placeOfNotification", "report.ath.card.required", "Required");
			}
			if (GenericValidator.isBlankOrNull(bean.getDisputeNumber())){
				errors.rejectValue("disputeNumber", "report.ath.card.required", "Required");
			}
			if (bean.getDateOfNotification() == null){
				errors.rejectValue("dateOfNotification", "report.ath.card.required", "Required");
			}
		}
	}
	
	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {
		
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<Object, Object> accountsMap = new LinkedHashMap<Object, Object>();
		
		AccountMaskUtil maskutil = new AccountMaskUtil();
		Map<Object, Object> reasons = new LinkedHashMap<Object, Object>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")){
			accountsMap.put("none", "Select an account");
			reasons.put(AthmUtils.REPORT_REASON_LOST, "Lost");
			reasons.put(AthmUtils.REPORT_REASON_STOLEN, "Stolen");
		}
		else{
			accountsMap.put("none", "Seleccione una cuenta");
			reasons.put(AthmUtils.REPORT_REASON_LOST, "P&eacute;rdida");
			reasons.put(AthmUtils.REPORT_REASON_STOLEN, "Robada");
		}
		
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		for (TVFrontendAccount account : accounts) {
			String suffix;
			if (account != null && account.getAccountNumberSuffix() != null)
				suffix = account.getAccountNumberSuffix();
			else
				suffix = "";
			if ((!suffix.equalsIgnoreCase("")) && suffix.equalsIgnoreCase("RES") || suffix.equalsIgnoreCase("SAV"))
				continue;
			
			boolean allowSubtype = Utils.haveType(this.accountSubtypes, account.getSubtype());
			boolean allowProductId = Utils.haveType(this.accountProductIds, account.getAccountProductId());
			
			if (account != null && !account.getHide() && allowSubtype && allowProductId) {
				accountsMap.put(account.getFrontEndId(),
						account.getNickname() + " " + maskutil.mask(account.getAccountNumber()));
			}
		}
		model.put("accounts", accountsMap);
		model.put("reasons", reasons);
		model.put("lang", Utils.getRSALanguage(request));
		
		return model;
	}

	public void setAccountSubtypes(String accountSubtypes) {
		this.accountSubtypes = accountSubtypes;
	}

	public void setAccountProductIds(String accountProductIds) {
		this.accountProductIds = accountProductIds;
	}
	
}
