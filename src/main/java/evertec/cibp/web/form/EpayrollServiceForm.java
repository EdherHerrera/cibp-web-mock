package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.applications.ApplicationStatus;
import evertec.cibp.core.models.customer.applications.EpayrollApplication;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.CibpValidationUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.EpayrollServiceBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.Utils;

public class EpayrollServiceForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	private String[] productId;
	private MessageSource messageSource;

	public EpayrollServiceForm() {
		super();
		setCommandClass(EpayrollServiceBean.class);
		setCommandName("epayrollservice");
		setFormView("epayrollservice");

	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		EpayrollServiceBean bean = (EpayrollServiceBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		String lang = Utils.getRSALanguage(request);
		CustomerServices customerService = userSession.getCustomerService();
		boolean hideSSN = StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLLENHA);
		TVFrontendAccount acct = Utils.getAccountById(bean.getAccount(), userSession.getAccounts());
		CardPlasticFrontend[] plasticFrontendArray = userSession.getCardPlastics(acct);

		boolean isEvendorActivated = StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR);
		StringBuilder ath = new StringBuilder();

		if (plasticFrontendArray != null && plasticFrontendArray.length > 0) {
			ath.append(plasticFrontendArray[0].getCardPlasticAccountRecord().getCardPANId());

		} else {
			ath.append("ATH_DOES_NOT_EXIST");
		}

		String name = bean.getName();
		String phone1 = bean.getPhone1();
		String phone2 = bean.getPhone2();
		String phone3 = bean.getPhone3();
		String businessName = bean.getBusinessName();
		String businessPhone1 = bean.getBusinessPhone1();
		String businessPhone2 = bean.getBusinessPhone2();
		String businessPhone3 = bean.getBusinessPhone3();

		String address1 = bean.getResidentialAddress1();
		String address2 = bean.getResidentialAddress2();
		String city = bean.getResidentialCity();
		String state = bean.getResidentialState();
		String zip1 = bean.getResidentialZipCode1();
		String zip2 = bean.getResidentialZipCode2();

		String faxPhone1 = bean.getFaxPhone1() != null ? bean.getFaxPhone1() : "";
		String faxPhone2 = bean.getFaxPhone2() != null ? bean.getFaxPhone2() : "";
		String faxPhone3 = bean.getFaxPhone3() != null ? bean.getFaxPhone3() : "";

		String ssne1 = bean.getSsne1();
		String ssne2 = bean.getSsne2();
		String ssne3 = bean.getSsne3();
		String account = acct.getAccountNumber();

		String payrollEstimatedAmount = bean.getPayrollEstimatedAmount();
		String vendorEstimatedAmount = bean.getVendorEstimatedAmount();

		String phone = phone1 + phone2 + phone3;
		String fax = faxPhone1 + faxPhone2 + faxPhone3;
		String businessPhone = businessPhone1 + businessPhone2 + businessPhone3;

		StringBuilder ssne = new StringBuilder();

		if (hideSSN) {
			ssne.append(customer.getTaxId());
		} else {
			ssne.append(ssne1 + ssne2 + ssne3);
		}

		String zip = zip1.equalsIgnoreCase("") ? zip2 : zip1;

		boolean saveApplication;
		EpayrollApplication epayrollapp;
		if (isEvendorActivated) {
			epayrollapp = new EpayrollApplication(name, phone, fax, businessName, businessPhone, address1, address2,
					city, state, zip, ssne.toString(), account, ath.toString(), payrollEstimatedAmount,
					vendorEstimatedAmount, ApplicationStatus.NEW);
		} else {
			epayrollapp = new EpayrollApplication(name, phone, fax, businessName, businessPhone, address1, address2,
					city, state, zip, ssne.toString(), account, ath.toString(), payrollEstimatedAmount, "0",
					ApplicationStatus.NEW);
		}

		try {
			saveApplication = CustomerFacade.getInstance().saveEpayrollApplicationNew(epayrollapp, request);

			if (saveApplication) {
				userSession.setPayrollAppStatus(ApplicationStatus.NOW);
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLLENHA)) {
					customerService.sendEpayrollAppConfirm(customer, epayrollapp, userSession.getPreferredLanguage());
				}
			}
		} catch (RuntimeException e) {
			saveApplication = false;
		}

		model.put("completed", true);
		if (!saveApplication) {
			userSession.setStatusmessage(this.messageSource.getMessage("epayrollservice.enrollment.fail", null,
					new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang)));
		} else {
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR)) 
				userSession.setStatusmessage(this.messageSource.getMessage("epayrollservice.enrollment.success", null, new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang)));
			else
				userSession.setStatusmessage(this.messageSource.getMessage("epayrollservice.oldenrollment.success", null, new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang)));
		}

		model.put("saveApplication", saveApplication);		
		return showForm(request, error, "epayrollservice", model);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {

		EpayrollServiceBean bean = (EpayrollServiceBean) command;

		boolean hideSSN = StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLLENHA);

		boolean isEvendorActivated = StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR);

		String phone1 = bean.getPhone1();
		String phone2 = bean.getPhone2();
		String phone3 = bean.getPhone3();

		String businessPhone1 = bean.getBusinessPhone1();
		String businessPhone2 = bean.getBusinessPhone2();
		String businessPhone3 = bean.getBusinessPhone3();

		String address1 = bean.getResidentialAddress1();
		String address2 = bean.getResidentialAddress2();
		String city = bean.getResidentialCity();
		String state = bean.getResidentialState();
		String zip1 = bean.getResidentialZipCode1();
		String zip2 = bean.getResidentialZipCode2();

		String ssne1 = bean.getSsne1();
		String ssne2 = bean.getSsne2();
		String ssne3 = bean.getSsne3();
		String account = bean.getAccount();
		String payrollEstimatedAmount = bean.getPayrollEstimatedAmount();
		String vendorEstimatedAmount = bean.getVendorEstimatedAmount();
		String terms = bean.getTermsConfirmation();



		if (GenericValidator.isBlankOrNull(phone1) || !GenericValidator.matchRegexp(phone1, "^\\d{3}$")
					|| GenericValidator.isBlankOrNull(phone2) || !GenericValidator.matchRegexp(phone2, "^\\d{3}$")
					|| GenericValidator.isBlankOrNull(phone3) || !GenericValidator.matchRegexp(phone3, "^\\d{4}$")) {

				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("phone3", "", "Please enter a home phone number.");
				else
					errors.rejectValue("phone3", "", "Por favor entre un tel&eacute;fono residencial.");
			}
		if (GenericValidator.isBlankOrNull(address1) || !CibpValidationUtils.isValidAddress(address1)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("residentialAddress1", "", "Please enter a valid address.");
			else
				errors.rejectValue("residentialAddress1", "", "Favor entrar una direcci&oacute;n valida.");
		}

		if (!GenericValidator.isBlankOrNull(address2) && !CibpValidationUtils.isValidAddress(address2)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("residentialAddress2", "", "Please enter a valid address.");
			else
				errors.rejectValue("residentialAddress2", "", "Favor entrar una direcci&oacute;n valida.");
		}
		if (GenericValidator.isBlankOrNull(city) || !GenericValidator.matchRegexp(city, "^[A-Za-z ]+$")) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("residentialCity", "", "Please enter a<br/> valid city.");
			else
				errors.rejectValue("residentialCity", "", "Favor entrar una<br/> ciudad valida.");
		}

		if (GenericValidator.isBlankOrNull(state) || !GenericValidator.matchRegexp(state, "^[A-Za-z ]+$")) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("residentialState", "", "Please enter a valid state.");
			else
				errors.rejectValue("residentialState", "", "Favor entrar un estado valido.");
		}

		if (GenericValidator.isBlankOrNull(zip1) || !GenericValidator.matchRegexp(zip1, "^\\d{5}$")) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("residentialZipCode2", "", "Please enter a valid zipcode.");
			else
				errors.rejectValue("residentialZipCode2", "", "Favor entrar un c&oacute;digo<br/> postal valido.");
		} else if (!GenericValidator.isBlankOrNull(zip2) && !GenericValidator.matchRegexp(zip2, "^\\d{4}$")) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("residentialZipCode2", "", "Please enter a valid zipcode.");
			else
				errors.rejectValue("residentialZipCode2", "", "Favor entrar un c&oacute;digo<br/> postal valido.");
		}

		if (GenericValidator.isBlankOrNull(businessPhone1)
					|| !GenericValidator.matchRegexp(businessPhone1, "^\\d{3}$")
					|| GenericValidator.isBlankOrNull(businessPhone2)
					|| !GenericValidator.matchRegexp(businessPhone2, "^\\d{3}$")
					|| GenericValidator.isBlankOrNull(businessPhone3)
					|| !GenericValidator.matchRegexp(businessPhone3, "^\\d{4}$")) {

				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("businessPhone3", "", "Please enter a Business phone number.");
				else
					errors.rejectValue("businessPhone3", "", "Por favor entre un tel&eacute;fono del Negocio.");
		}

		if (!hideSSN) {
			if (GenericValidator.isBlankOrNull(ssne1) || GenericValidator.isBlankOrNull(ssne2)
					|| GenericValidator.isBlankOrNull(ssne3))
				if (GenericValidator.isBlankOrNull(ssne1) || !GenericValidator.matchRegexp(ssne1, "^\\d{3}$")
						|| GenericValidator.isBlankOrNull(ssne2) || !GenericValidator.matchRegexp(ssne2, "^\\d{3}$")
						|| GenericValidator.isBlankOrNull(ssne3) || !GenericValidator.matchRegexp(ssne3, "^\\d{4}$")) {

					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("ssne3", "", "Please enter a Social Security Employers.");
					else
						errors.rejectValue("ssne3", "", "Por favor entre un Seguro Social Patronal.");
				}
		}

		if (GenericValidator.isBlankOrNull(account) || account.equals("none")) {

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("account", "", "Please select a Valid Account.");
			else
				errors.rejectValue("account", "", "Por favor seleccione una Cuenta V&aacute;lida.");
		}
		if (GenericValidator.isBlankOrNull(payrollEstimatedAmount) || !GenericValidator.isInt(payrollEstimatedAmount)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("payrollEstimatedAmount", "", "Please enter a e-Payroll estimated Amount.");
			else
				errors.rejectValue("payrollEstimatedAmount", "", "Por favor entre un Monto Estimado de e-Payroll.");
		}
		if (isEvendorActivated) {
			if (GenericValidator.isBlankOrNull(vendorEstimatedAmount)
					|| !GenericValidator.isInt(vendorEstimatedAmount)) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("vendorEstimatedAmount", "", "Please enter a Vendor estimated Amount.");
				else
					errors.rejectValue("vendorEstimatedAmount", "", "Por favor entre un Monto Estimado de Suplidor.");
			}
		}
		if (GenericValidator.isBlankOrNull(terms) || !terms.equalsIgnoreCase("on")) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("termsConfirmation", "", "Please agree.");
			else
				errors.rejectValue("termsConfirmation", "", "Por favor acepte los t&eacute;rminos y condiciones.");
		}
	}

	@SuppressWarnings("unused")
	private void loadTermsData(HttpServletRequest request, Object command, Map<String, Object> model) {
		Map<String, String> termsMap = new LinkedHashMap<>();

		termsMap.put("termsConfirmation", "");

		model.put("termsConfirmation", termsMap);
	}

	private Map<String, Object> loadPersonalInformation(HttpServletRequest request, Map<String, Object> model,
			UserSession session) {

		model.put("customer", session.getCustomerProfile());
		return model;
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, userSession.isInformationChallenge());

		model.put("isEvendorActivated", StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR));

		String username = userSession.getUsername();
		model.put("username", username);

		model.put("states", this.getStates());

		EpayrollServiceBean bean = (EpayrollServiceBean) command;
		String name = bean.getName();
		if (bean.getName() == null) {
			bean.setName(userSession.getCustomerProfile().getFullName());
		}

		if (GenericValidator.isBlankOrNull(name) || !GenericValidator.matchRegexp(name, "^\\d+$")) {
			name = (String) request.getParameter("name");
		}

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setPath("/");
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		if (cookie != null)
			model.put("devicetoken", cookie.getValue());

		if (userSession.getInformationQuestion() != null) {
			String question = (String) userSession.getInformationQuestion().values().toArray()[0];
			model.put("question", question);
		}

		model = this.loadPersonalInformation(request, model, userSession);

		AccountMaskUtil mask = new AccountMaskUtil();
		NumberTool numberTool = new NumberTool();

		Set<TVAccountTransferSource> accountsPayrollFrom = userSession.getAccountsPaymentFrom();
		Map<String, String> accountsFromPayrollMap = new LinkedHashMap<String, String>();
		if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
			accountsFromPayrollMap.put("none", "Select an account");
		else
			accountsFromPayrollMap.put("none", "Selecciona una cuenta");

		for (Iterator<TVAccountTransferSource> iterator = accountsPayrollFrom.iterator(); iterator.hasNext();) {
			TVAccountTransferSource acctfrom = (TVAccountTransferSource) iterator.next();
			TVFrontendAccount accfront = acctfrom.getAccount();

			if (!accfront.getHide()) {
				if (accfront != null && accfront.getPortalBalance() != null && accfront.getSubtype() != null
						&& isCommercialAccount(accfront) && !isTortolaAccount(accfront)) {

					accountsFromPayrollMap.put(acctfrom.getFrontEndId(),
							StringUtils.abbreviate(accfront.getNickname(), 15) + " "
									+ mask.mask(accfront.getAccountNumber()) + " "
									+ numberTool.format("currency", accfront.getPortalBalance())
									+ ((accfront.getAccountNumberSuffix() != null
											&& !accfront.getAccountNumberSuffix().equals(""))
													? " " + accfront.getAccountNumberSuffix() : ""));

				}
			}
		}
		model.put("frompayrollaccounts", accountsFromPayrollMap);

		model.put("hideSSN", StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLLENHA));

		return model;
	}

	public String[] getProductId() {
		return productId;
	}

	public void setProductId(String[] productId) {
		this.productId = productId;
	}

	private boolean isCommercialAccount(TVFrontendAccount account) {
		for (int i = 0; i < productId.length; i++) {
			if (account.getProductDefinition().getProductId().equals(productId[i])) {
				return true;
			}
		}
		return false;
	}

	private Map<Object, String> getStates() {
		Map<Object, String> states = new LinkedHashMap<Object, String>();
		states.put("PR", "PR");
		states.put("USVI", "USVI");

		return states;

	}

    public boolean isTortolaAccount(TVFrontendAccount accfront){
	   return accfront.getAccountNumber().startsWith("185");

    }

	/** @return regresa el messageSource */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource
	 *            el messageSource a establecer
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

}
