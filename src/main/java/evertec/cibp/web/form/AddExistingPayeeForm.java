package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import com.evertecinc.www.PremiaPointsActivation.PointsCostCenter;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.EStatementInteraction;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.GlobalPayeePreferences;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyAllCustomerEmailsRequest;
import evertec.cibp.core.services.requests.PremiaPointsRequest;
import evertec.cibp.core.services.responses.AddPayeeResponse;
import evertec.cibp.core.services.responses.AddPayeeResponse.Result;
import evertec.cibp.core.utils.CibpValidationUtils;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.mobilecash.core.template.utils.AccountMaskUtil;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.AddExistingPayeeBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.prophit.service.bean.CustomerInquiryBean;
import evertec.prophit.service.bean.EbillIndType;
import evertec.prophit.service.bean.MaintenceEbillBean;
import evertec.prophit.service.bean.MaintenceEbillResponse;

public class AddExistingPayeeForm extends CancellableFormController
		implements GlobalEntitlementAwareController, MessageSourceAware {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;
	private String telenominaID;
	private String evendorPayeeId;
	
	// MBSFE-691
	private String invalidEmailDomains;
	
	public String getInvalidEmailDomains() {
		return invalidEmailDomains;
	}

	public void setInvalidEmailDomains(String invalidEmailDomains) {
		this.invalidEmailDomains = invalidEmailDomains;
	}
	// END MBSFE-691
	
	public String getTelenominaID() {
		return telenominaID;
	}

	public void setTelenominaID(String telenominaID) {
		this.telenominaID = telenominaID;
	}
	
	public String getEvendorPayeeId() {
		return evendorPayeeId;
	}
	
	public void setEvendorPayeeId(String evendorPayeeId) {
		this.evendorPayeeId = evendorPayeeId;
	}

	public AddExistingPayeeForm() {
		super();
		setCommandClass(AddExistingPayeeBean.class);
		setCommandName("cp");
		setFormView("addexistingpayee");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();
		AddExistingPayeeBean payeeBean = (AddExistingPayeeBean) command;
		BankingSession bankingSession = userSession.getBankingSession();
		model.put("telenominaID", telenominaID);
		if (payeeBean.getPayee() == null)
			return showForm(request, response, errors, model);

		GlobalPayee global = PaymentsFacade.getInstance().getGlobalPayeeById(new Integer(payeeBean.getPayee()), userSession);
		
		if (global == null)
			return showForm(request, response, errors, model);

		PaymentsFacade.getInstance().getGlobalPayeePreference(global, userSession);
		
		model.put("globalPayee", global);

		if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			userSession.getGuidedSetupSession().setResubmitParent(true);

		}

		// The EBPP2 user id is not yet in session; add it
		if (!userSession.isEbpp2UserIdLookedUp()) {
			EbppEntitlement ent = null;
			if (profile.getEntitlementByType(EntitlementType.EBILLS) != null) {
				ent = new EbppEntitlement(profile.getEntitlementByType(EntitlementType.EBILLS));

				// If the username is blank, we need to look in the current EBPP
				// accounts ONLY
				if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
					userSession.setEbpp2UserId("");
				} else {
					userSession.setEbpp2UserId(ent.getUsername());
				}
				userSession.setEbpp2UserIdLookedUp(true);
			}
		}

		String nick = payeeBean.getNickname();
		if (nick == null || nick.equals(""))
			nick = global.getName();

		String billingAct = payeeBean.getBillingNumber();

		String email = payeeBean.getEmail();
		if (StringUtils.isNotEmpty(email) && GenericValidator.isEmail(email)) {

			// The user has an EBPP2 ID
			ModifyAllCustomerEmailsRequest macer = new ModifyAllCustomerEmailsRequest(
					Utils.generateCustomerInteractionEvent(request), profile, customerService,
					userSession.getAccounts(), bankingSession, userSession.getEbpp2UserId(),
					userSession.getPreferredLanguage(), payeeBean.getEmail());
			profile = CustomerUtils.modifyAllEmails(macer);
			userSession.setCustomerProfile(profile);
		}

		String zipcode = payeeBean.getZipcode();
		if (zipcode == null || zipcode.equals(""))
			zipcode = "00000";
		AddPayeeResponse res;

		boolean isHighRiskPayee = false;
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1701) && global.getPreference() != null) {
			isHighRiskPayee = global.getPreference().getHighRisk();
		}

		try {
			if (payeeBean.getActivateEbill()) {

				// Universal credit cards fix
				if (payeeBean.getPayee().equals("359") || payeeBean.getPayee().equals("517")
						|| payeeBean.getPayee().equals("518") || payeeBean.getPayee().equals("1983")
						|| payeeBean.getPayee().equals("223")) {

					TVFrontendAccount account = Utils.getAccountByAccountNumber(payeeBean.getBillingNumber(),
							userSession.getAccounts());

					if (account != null) {
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)
								&& account.getSubtype().equals("CCA")) {
							res = customerService.addCustomerPayeeWithEbill(profile, userSession.getBankingSession(),
									global, billingAct, nick, false, userSession.getEbpp2UserId(), zipcode,
									Utils.generateCustomerInteractionEvent(request), payeeBean.getPayee(), isHighRiskPayee);
							// activate the cca ebills on ida
							String acctNum = account.getAccountNumber();
							CustomerInquiryBean responseTsys;
							try {
								responseTsys = customerService.prophItService()
										.makeCustomerInquiry(acctNum);
							} catch (Exception e) {
								logger.error("Tsys Error:" + e.getMessage());
								responseTsys = null;
							}
							boolean mant = false;
							if (responseTsys != null && responseTsys.getEbillInd() !=null) {
								String ebillInd = "";
								if(res.getErrorCode() == 0){
									if (responseTsys.getEbillInd().equals(EbillIndType.ACTIVE.toString())) {
										ebillInd = EbillIndType.DEACTIVE.toString();
										mant = false;// Already Active
									} else if (responseTsys.getEbillInd().equals(EbillIndType.ELIGIBLE.toString())) {
										ebillInd = EbillIndType.ENROLL.toString();
										mant = true;
									}
								}
								if (mant) {
									MaintenceEbillBean bean = new MaintenceEbillBean();
									bean.setAcctNum(acctNum);
									bean.setCaseNumber(responseTsys.getCaseNumber());
									bean.setChannelId(responseTsys.getChannelId());
									bean.setEbillInd(ebillInd);
									bean.setImpersonateId(responseTsys.getImpersonateId());
									bean.setKeepCaseOpen(responseTsys.getKeepCaseOpen());
									bean.setNewContact(responseTsys.getNewContact());
									bean.setOrgId(responseTsys.getOrgId());
									bean.setOrgLevel(responseTsys.getOrgLevel());
									MaintenceEbillResponse responseMant = customerService.prophItService()
											.maintenceEBill(bean);
									if (responseMant != null) {
										if (responseMant.getMessage().indexOf("Successfull") != -1) {
											//Premia Report
											PremiaPointsRequest premiaRequest = new PremiaPointsRequest(null,
													String.valueOf(profile.getPermId()),acctNum, profile.getTaxId(),
													"Anadir Factura Electronica", "098", PointsCostCenter.INSFE, profile.getUsername(),"MiBanco", true);	
											customerService.premiaPointsActivation(premiaRequest);
											CustomerInteractionEvent customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
											customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS, "Account: "+  acctNum + " activate.");
											customerService.logUpdateStatementStatus(customerInteractionEvent, InteractionSeverity.INFO);
											res.setResult(Result.SUCCESS);
											customerService.sendEbillsCCAEmailActivationSuccess(
													userSession.getCustomerProfile(), userSession.getPreferredLanguage(),
													account.getNickname());
											Utils.registerStatusEbillHistoryLog(request, acctNum, userSession,
													customerService, EStatementInteraction.ACTIVATE_STATUS);
										} else {
											res.setResult(Result.ERROR);
											customerService.sendEbillsCCAEmailActivationFailure(
													userSession.getCustomerProfile(), userSession.getPreferredLanguage(),
													account.getNickname());
											CustomerInteractionEvent customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
											customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
													"unable to activate, reason: " + responseMant.getMessage());
											customerService.logUpdateStatementStatus(customerInteractionEvent,
													InteractionSeverity.INFO);
										}
									} else {
										res.setResult(Result.ERROR);
										customerService.sendEbillsCCAEmailActivationFailure(
												userSession.getCustomerProfile(), userSession.getPreferredLanguage(),
												account.getNickname());
										CustomerInteractionEvent customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"unable to activate, reason: " + "tsys response null");
										customerService.logUpdateStatementStatus(customerInteractionEvent,
												InteractionSeverity.INFO);
									}
								}
							}else{//Set error por ebill activation
								if(res.getResult().equals(Result.SUCCESS)){
									res.setResult(Result.SUCCESS_NOEBILL);
									res.setErrorCode(Long.parseLong("99999"));
									if(res.getErrorEBA() == null){
										res.setErrorEBA("Error TSYS");
									}
									customerService.sendEbillsCCAEmailActivationFailure(
											userSession.getCustomerProfile(), userSession.getPreferredLanguage(),
											account.getNickname());
								}
							}
						} else {
							res = customerService.addCustomerPayeeWithEbill(profile, userSession.getBankingSession(),
									global, billingAct, nick, true, userSession.getEbpp2UserId(), zipcode,
									Utils.generateCustomerInteractionEvent(request), payeeBean.getPayee(), isHighRiskPayee);
							userSession.resetEbpp2Accounts();
						}
					} else
						res = customerService.addCustomerPayeeWithEbill(profile, userSession.getBankingSession(),
								global, billingAct, nick, true, userSession.getEbpp2UserId(), zipcode,
								Utils.generateCustomerInteractionEvent(request), payeeBean.getPayee(), isHighRiskPayee);
					userSession.resetEbpp2Accounts();
				} else{
					res = customerService.addCustomerPayeeWithEbill(profile, userSession.getBankingSession(), global,
							billingAct, nick, true, userSession.getEbpp2UserId(), zipcode,
							Utils.generateCustomerInteractionEvent(request), payeeBean.getPayee(), isHighRiskPayee);
				}
				userSession.resetEbpp2Accounts();

				if (res.getErrorCode() != 0) {

					String er = getTelepagoErrorDescriptor(res.getErrorEBA(),
							Utils.getRSALanguage(request).toLowerCase(), res.getErrorString());
					String error = "";
					if (er == null || er.equals("")) {
						userSession.setErrorDetail(res.getErrorString());
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							error = "Payee " + nick + " - " + billingAct
									+ " was added but e-bill could not be activated."
									+ "<a href='javascript:Show()' > Learn More</a>";
							userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
						} else {
							error = "A&ntilde;adimos su comercio " + nick + " - " + billingAct
									+ " pero no se pudo activar el e-bill."
									+ "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
							userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
						}
						userSession.resetAccounts();
					} else {
						userSession.setErrorDetail(er);

						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							error = "Unable to add your payee " + nick + " - " + billingAct
									+ " at this time nor could the e-bill be activated."
									+ "<a href='javascript:Show()' > Learn More</a>";
							userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());

						} else {
							userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
							error = "No se pudo a&ntilde;adir su comercio " + nick + " - " + billingAct
									+ " en este momento ni se pudo activar el e-bill."
									+ "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
						}
					}
					
					if (userSession.getUserAgentType().equals(UserAgentType.JSON)) {
						model.put("errorCode", res.getErrorEBA());
						model.put("errorMessage", er);
						model.put("statusMessage", error);
					} else {
						userSession.setStatusmessage(error);
					}

				} else if (res.getErrorCode() == 0) {

					// Make it a frequent payee
					List<TVFrontEndPayee> payees = userSession.getPayees(true);
					TVFrontEndPayee payee = Utils.getPayeebyAccountNumber(payeeBean.getBillingNumber(), payees);

					if (payee != null) {
						customerService.modifyPayeeFrequentPreference(profile, bankingSession, payee, true);
						payee.getCustomerPayeePreferences().setFavorite(true);
						userSession.resetFavoritePayments();
					}

					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Your payee " + nick + " - " + billingAct + " was added.");
					else
						userSession.setStatusmessage(
								"Su comercio " + nick + " - " + billingAct + " fue a&ntilde;adido exitosamente.");
					PreferredLanguage preferredLanguage = PreferredLanguage.SPANISH;
					if(Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						preferredLanguage = PreferredLanguage.ENGLISH;
					}
					CustomerProfile customerProfile = userSession.getCustomerProfile();
					AccountMaskUtil mask = new AccountMaskUtil();
					Map<Object, Object> models = new HashMap<>();
					models.put("name", customerProfile.getFirstName());
					models.put("can", mask.mask(customerProfile.getEnrollmentAccountNumber()));
					models.put("payeeName", global.getName());
					models.put("nickName", nick);
					models.put("payeeNumber", mask.mask(billingAct));
					models.put("sucessful", true);
					customerService.sendEmailPayee(customerProfile, preferredLanguage, models );					
					userSession.resetAccounts();
				} else {
					String error = "";
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						error = "Unable to add your payee " + nick + " - " + billingAct + " at this time.";
					else
						error = "Error a&ntilde;adiendo su comercio " + nick + " - " + billingAct + " en este momento.";
					userSession.setStatusmessage(error);
					if (userSession.getUserAgentType().equals(UserAgentType.JSON)) {
						model.put("statusMessage", error);
						return showForm(request, errors, "addexistingpayee", model);
					} else if (!userSession.getGuidedSetupSession().isGuidedSetupMode()) {
						return new ModelAndView("redirect:addexistingpayee?payee=" + payeeBean.getPayee());
					}

				}
			}

			
			// ePAYROLL
			
			
			
			else {
				boolean telenomina;
				String employeeName;
				String employeeId;
				String employeeRoutingNumber;
				String accounttype;
				if (global.getIdString().equalsIgnoreCase(telenominaID)) {
					telenomina = true;
					employeeName = payeeBean.getEmployeeName();
					employeeId = payeeBean.getEmployeeId();
					employeeRoutingNumber = payeeBean.getEmployeeRoutingNumber();
					accounttype = payeeBean.getAccounttype();
				} else {
					telenomina = false;
					employeeName = null;
					employeeId = null;
					employeeRoutingNumber = null;
					accounttype = null;
				}

				boolean verifyPass = true;
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)) {
					if (!request.getParameter("payee").equals(telenominaID)
							|| (request.getParameter("payee").equals(telenominaID)
									&& (!userSession.getCustomerProfile().useCommercialInterface()))
							|| (request.getParameter("payee").equals(telenominaID)
									&& (userSession.getCustomerProfile().useCommercialInterface()))) {
						res = customerService.addCustomerPayeeWithoutEbillWithErrorDescription(profile,
								userSession.getBankingSession(), global, billingAct, nick,
								Utils.generateCustomerInteractionEvent(request), employeeName, employeeId,
								employeeRoutingNumber, accounttype, telenomina, isHighRiskPayee);
					} else {
						res = new AddPayeeResponse();
						verifyPass = false;
						String error = "";
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							error = "Payee " + nick + " - " + billingAct + " wasn't added.";
						} else {
							error = "No A&ntilde;adimos su comercio " + nick + " - " + billingAct;
						}
						userSession.setStatusmessage(error);
					}
				} else {
					res = customerService.addCustomerPayeeWithoutEbillWithErrorDescription(profile,
							userSession.getBankingSession(), global, billingAct, nick,
							Utils.generateCustomerInteractionEvent(request), employeeName, employeeId,
							employeeRoutingNumber, accounttype, telenomina, isHighRiskPayee);
				}
				if (res.getErrorCode() == 0 && verifyPass) {
					if (request.getParameter("payee").equals(telenominaID))
						if (userSession.getCustomerProfile().useCommercialInterface()) {
							if (userSession.getCustomerProfile().useCommercialInterface()) {
								CustomerProfile customer = userSession.getCustomerProfile();
								Map<Object, Object> models = new HashMap<>();
								models.put("idEmployee", payeeBean.getEmployeeId());
								models.put("employeeName", payeeBean.getEmployeeName());
								models.put("nickName", payeeBean.getNickname());
								models.put("rt", payeeBean.getEmployeeRoutingNumber());
								models.put("employeeCan", payeeBean.getBillingNumber());
								String bankName = "";
								if (request.getParameter("bank_Name") != null) {
									bankName = request.getParameter("bank_Name");
								}
								models.put("rtName", bankName);
                                //SMS method moved to make this a generic method
								customerService.sendEPayrollConfirmation(customer, userSession.getPreferredLanguage(),
										models, userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)));
							}
						}
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Your payee " + nick + " - " + billingAct + " was added.");
					else
						userSession.setStatusmessage(
								"Su comercio " + nick + " - " + billingAct + " fue a&ntilde;adido exitosamente.");
					//Enviar email
					PreferredLanguage preferredLanguage = PreferredLanguage.SPANISH;
					if(Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						preferredLanguage = PreferredLanguage.ENGLISH;
					}
					CustomerProfile customerProfile = userSession.getCustomerProfile();
					AccountMaskUtil mask = new AccountMaskUtil();
					Map<Object, Object> models = new HashMap<>();
					models.put("name", customerProfile.getFirstName());
					models.put("can", mask.mask(customerProfile.getEnrollmentAccountNumber()));
					models.put("payeeName", global.getName());
					models.put("nickName", nick);
					models.put("payeeNumber", mask.mask(billingAct));
					models.put("sucessful", true);
					customerService.sendEmailPayee(customerProfile, preferredLanguage, models );
					userSession.resetAccounts();
				} else {
					/*
					 * CIBP-1680 bgarcia Se agrega la logica para poner el
					 * detalle del mensaje
					 */
					String er = getTelepagoErrorDescriptor(res.getErrorEBA(),
							Utils.getRSALanguage(request).toLowerCase(), res.getErrorString());
					String error = "";
					if (verifyPass) {
						if (er == null || er.equals("")) {
							userSession.setErrorDetail(res.getErrorString());
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								error = "Unable to add your payee " + nick + " - " + billingAct + " at this time."
										+ "<a  href='javascript:Show()'> Learn More</a>";
								userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
							} else {
								error = "Error a&ntilde;adiendo su comercio " + nick + " - " + billingAct
										+ " en este momento." + "<a  href='javascript:Show()'> Conoce M&aacute;s</a>";
								userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
							}
						} else {
							userSession.setErrorDetail(er);
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								error = "Unable to add your payee " + nick + " - " + billingAct + " at this time."
										+ "<a  href='javascript:Show()'> Learn More</a>";
								userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
							} else {
								userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
								error = "Error a&ntilde;adiendo su comercio " + nick + " - " + billingAct
										+ " en este momento." + "<a  href='javascript:Show()'> Conoce M&aacute;s</a>";

							}
						}
						if (userSession.getUserAgentType().equals(UserAgentType.JSON)) {
							model.put("errorCode", res.getErrorEBA());
							model.put("errorMessage", er);
							model.put("statusMessage", error);
						} else {
							userSession.setStatusmessage(error);
						}
					}
					
					if (userSession.getUserAgentType().equals(UserAgentType.JSON)) {
						return showForm(request, errors, "addexistingpayee", model);
					} else if (!userSession.getGuidedSetupSession().isGuidedSetupMode()) {
						return new ModelAndView("redirect:addexistingpayee?payee=" + payeeBean.getPayee());
					}
				}
			}
			userSession.resetPayees();
			model.put("successAdded", true);

		} catch (RuntimeException e) {
			String error = "";
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				error = "Unable to add your payee " + nick + " - " + billingAct + " at this time.";
			else
				error = "Error a&ntilde;adiendo su comercio " + nick + " - " + billingAct + " en este momento.";
			
			if (userSession.getUserAgentType().equals(UserAgentType.JSON)) {
				model.put("statusMessage", error);
				return showForm(request, errors, "addexistingpayee", model);
			} else if (!userSession.getGuidedSetupSession().isGuidedSetupMode()) {
				userSession.setStatusmessage(error);
				return new ModelAndView("redirect:addexistingpayee?payee=" + payeeBean.getPayee());
			}
		}

		if (userSession.getUserAgentType().equals(UserAgentType.JSON))
			return showForm(request, errors, "addexistingpayee", model);
		else {
			// si esta en modo guided setup deberia redirigir a addpayees
			if (userSession.getGuidedSetupSession().isGuidedSetupMode())
				return new ModelAndView("redirect:addpayee");
	
			return new ModelAndView("redirect:editpayee");
		}

	}

	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {

		AddExistingPayeeBean payeeBean = new AddExistingPayeeBean();

		if (GenericValidator.isBlankOrNull(payeeBean.getEmployeeRoutingNumber())) {
			payeeBean.setEmployeeRoutingNumber("021502011");
		}
		
		if (GenericValidator.isBlankOrNull(payeeBean.getRoutingNumber())) {
			payeeBean.setRoutingNumber("021502011");
		}

		return payeeBean;
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		AddExistingPayeeBean payeeBean = (AddExistingPayeeBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		
		String lang = Utils.getRSALanguage(request).equals("sp") ? "es" : "en";
		model.put("lang", lang);
		model.put("customeremail", userSession.getCustomerProfile().getEmail());
		model.put("telenominaID", telenominaID);
		model.put("evendorPayeeId", evendorPayeeId);
		model.put("states", this.getStates());
		String id = payeeBean.getPayee();

		if (GenericValidator.isBlankOrNull(id) || !GenericValidator.matchRegexp(id, "^\\d+$")) {
			id = request.getParameter("payee");
		}
		model.put("payeeId", id);
		if(id != null && (id.equalsIgnoreCase(telenominaID) || id.equalsIgnoreCase(evendorPayeeId)))
		{
			if(userSession.isAddEmployeeOobAut())
			{		
				model.put("OOBAut", true);
				userSession.setOobAddEmployeeAut(false);
			}				
			else
			{
				model.put("OOBAut", false);
			}
		}
		else
		{
			model.put("OOBAut", true);
		}

		boolean resubmit = true;
		if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			if (request.getSession().getAttribute("payeebean") != null
					&& !request.getSession().getAttribute("payeebean").equals("")) {
				id = request.getSession().getAttribute("payeebean").toString();

				resubmit = false;
				if (userSession.getGuidedSetupSession().isResubmitParent()) {
					model.put("submitgs", true);
					userSession.getGuidedSetupSession().setResubmitParent(false);
				}
			}
			if(id != null && id.matches("[a-zA-Z0-9 ]")) {
				request.getSession().setAttribute("payeebean", id);
			}
			
		}
		/*
		 * aqui establezco el valor de el MessageSource para que lo pueda usar
		 * el bean del core, me parece que podria hacerse accesando el context
		 * pero tuve algunos problemas al hacerlo.. mpicado CIBP-1799
		 */
		customerService.setEBPPStatus(this.getEBPPMessageSourceStatus());

		if (GenericValidator.isBlankOrNull(id) || !GenericValidator.matchRegexp(id, "^\\d+$")) {
			model.put("payeeSelectionError", true);
			return model;
		}

		// if(GenericValidator.isBlankOrNull(payeeBean.getEmployeeRoutingNumber())){
		// payeeBean.setEmployeeRoutingNumber("021502011");
		// }

		GlobalPayee global = PaymentsFacade.getInstance().getGlobalPayeeById(new Integer(id), userSession);
		if (global == null)
			return model;

		PaymentsFacade.getInstance().getGlobalPayeePreference(global, userSession);
		if (global.getPreference() != null) {
			model.put("haspref", true);
			GlobalPayeePreferences pref = global.getPreference();
			if (pref.isSet(pref.EBILL) && pref.getEbill() && !new Integer(id).equals(37))
				model.put("askforebill", true);
			if (pref.isSet(pref.VALIDATION_CODE) && pref.getValidationCode())
				model.put("askforzipcode", true);
			if (lang.equalsIgnoreCase("en") && pref.isSet(pref.HELP_EN))
				if (pref.getHelpEn() != null && userSession.getUserAgentType().equals(UserAgentType.JSON))
					model.put("help", pref.getHelpEn().replace("\"", "\\\""));
				else
					model.put("help", pref.getHelpEn());
			else if (lang.equalsIgnoreCase("es") && pref.isSet(pref.HELP_ES))
				if (pref.getHelpEs() != null && userSession.getUserAgentType().equals(UserAgentType.JSON))
					model.put("help", pref.getHelpEs().replace("\"", "\\\""));
				else
					model.put("help", pref.getHelpEs());

		}

		model.put("activateEbill", payeeBean.getActivateEbill());

		model.put("globalPayee", global);
		model.put("emailerror", payeeBean.getEmailError());

		// mpicado, cibp-2218
		if (userSession.getGuidedSetupSession().isGuidedSetupMode() && !errors.hasErrors() && resubmit) {
			if (userSession.getGuidedSetupSession().isResubmitParent()) {
				model.put("submitgs", true);
				userSession.getGuidedSetupSession().setResubmitParent(false);
			}

		}

		// MBSFE-7 Eliminar opcion de ebill de AEE
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
			if (new Integer(id).equals(359) ||new Integer(id).equals(1983) || new Integer(id).equals(223)) {
				model.put("isCCA", true);
			} else {
				model.put("isCCA", false);
			}
				model.put("eBillsFlag", true);
		} else {
			model.put("isCCA", false);
			model.put("eBillsFlag", false);
		}
		
		// MBSFE-8 Modificar mensaje de activar ebills de billers en añadir
		// comercios
		model.put("MBSFE8", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE8).toString());
		model.put("isEpayrollActivated", StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL).toString());
		model.put("isEVendorActivated", StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR).toString());

		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		AddExistingPayeeBean payeeBean = (AddExistingPayeeBean) command;
		payeeBean.setEmailError(false);

		if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			if (request.getSession().getAttribute("payeebean") != null
					&& !request.getSession().getAttribute("payeebean").equals("")) {
				request.getSession().setAttribute("payeebeanform", command);
			}
			if (request.getSession().getAttribute("payeebeanform") != null) {
				command = request.getSession().getAttribute("payeebeanform");
			}
		}

		if (GenericValidator.isBlankOrNull(payeeBean.getPayee()))
			errors.rejectValue("payee", "addpayee.invalidpayee.error", "Invalid payee");

		String id = payeeBean.getPayee();
		if (GenericValidator.isBlankOrNull(id) || !GenericValidator.matchRegexp(id, "^\\d+$")) {
			errors.rejectValue("payee", "addpayee.payee.invalid", "Invalid payee");
			return;
		}

		GlobalPayee global = PaymentsFacade.getInstance().getGlobalPayeeById(new Integer(payeeBean.getPayee()), userSession);
		if (global == null)
			errors.rejectValue("payee", "addpayee.payee.invalid", "Invalid payee");

		String accountNumber = payeeBean.getBillingNumber();
		String nickname = payeeBean.getNickname();

		if (global != null && !GenericValidator.isBlankOrNull(global.getIdString())
				&& global.getIdString().equalsIgnoreCase(telenominaID)) {
			String bank = "";
			if (!GenericValidator.isBlankOrNull(payeeBean.getEmployeeRoutingNumber()))
				bank = customerService.getBankName(payeeBean.getEmployeeRoutingNumber());

			if (GenericValidator.isBlankOrNull(accountNumber)
					|| !CibpValidationUtils.isValidPayeeAccountNumber(accountNumber)) {
				errors.rejectValue("billingNumber", "addpayee.billernumber.invalid",
						"Invalid account number. Please note that special characters are not allowed.");
			}
			if (GenericValidator.isBlankOrNull(payeeBean.getBillingNumberConf()))
				errors.rejectValue("billingNumberConf", "addpayee.employeeAcct.requiered",
						"Please enter the employee bank deposit account number in this field");
			if (!GenericValidator.isBlankOrNull(payeeBean.getBillingNumber())
					&& !GenericValidator.isBlankOrNull(payeeBean.getBillingNumberConf())
					&& !payeeBean.getBillingNumber().equals(payeeBean.getBillingNumberConf()))
				errors.rejectValue("billingNumberConf", "addpayee.employeeAcctConfirmation.notmatch",
						"Employee bank acct number and employee bank acct number confirmation must be equal");

			if (GenericValidator.isBlankOrNull(payeeBean.getEmployeeName())
					|| !CibpValidationUtils.isValidPayeeNickname(payeeBean.getEmployeeName())) {
				errors.rejectValue("employeeName", "addpayee.employeeName.invalid",
						"Please enter a valid employee name");
			}
			if (GenericValidator.isBlankOrNull(payeeBean.getEmployeeId())
					|| !CibpValidationUtils.isValidPayeeNickname(payeeBean.getEmployeeId())) {
				errors.rejectValue("employeeId", "addpayee.employeeId.invalid", "Please enter a valid employee ID");
			}
			if (GenericValidator.isBlankOrNull(payeeBean.getEmployeeIdConf())
					|| !CibpValidationUtils.isValidPayeeNickname(payeeBean.getEmployeeIdConf())) {
				errors.rejectValue("employeeIdConf", "addpayee.employeeId.required", "Confirm employee ID");
			}

			if (!GenericValidator.isBlankOrNull(payeeBean.getEmployeeId())
					&& !GenericValidator.isBlankOrNull(payeeBean.getEmployeeIdConf())
					&& !payeeBean.getEmployeeId().equals(payeeBean.getEmployeeIdConf()))
				errors.rejectValue("employeeIdConf", "addpayee.employeeIDConfirmation.notmatch",
						"Employee ID and employee ID confirmation must be equal");

			if (GenericValidator.isBlankOrNull(payeeBean.getEmployeeRoutingNumber())
					|| !CibpValidationUtils.isValidPayeeAccountNumber(payeeBean.getEmployeeRoutingNumber())
					|| bank.equals("")) {
				errors.rejectValue("employeeRoutingNumber", "addpayee.employeeRoutingNumber.invalid",
						"Please enter a valid routing number");
			}
			if (GenericValidator.isBlankOrNull(payeeBean.getAccounttype())) {
				errors.rejectValue("accounttype", "addpayee.accounttype.invalid", "Please specify the account type");
			}
		} else {
			if (GenericValidator.isBlankOrNull(accountNumber)
					|| !CibpValidationUtils.isValidPayeeAccountNumber(accountNumber)) {
				errors.rejectValue("billingNumber", "addpayee.billernumber.invalid",
						"Please enter a valid billing number");
			}
			if (GenericValidator.isBlankOrNull(payeeBean.getBillingNumberConf()))
				errors.rejectValue("billingNumberConf", "addpayee.billerConfirmation.notmatch",
						"Please enter your billing Number in this field");
			if (!GenericValidator.isBlankOrNull(payeeBean.getBillingNumber())
					&& !GenericValidator.isBlankOrNull(payeeBean.getBillingNumberConf())
					&& !payeeBean.getBillingNumber().equals(payeeBean.getBillingNumberConf()))
				errors.rejectValue("billingNumberConf", "addpayee.billerConfirmation.notmatch",
						"Your biller number and biller number confirmation must be equal");
		}

		if (GenericValidator.isBlankOrNull(nickname) || !CibpValidationUtils.isValidPayeeNickname(nickname)) {
			errors.rejectValue("nickname", "addpayee.nickname.invalid", "Please enter a valid payee name");
		}

		PaymentsFacade.getInstance().getGlobalPayeePreference(global, userSession);

		if (global != null && global.getPreference() != null) {
			GlobalPayeePreferences pref = global.getPreference();

			if (payeeBean.getActivateEbill() && pref.isSet(pref.EBILL) && pref.getEbill()
					&& request.getParameter("editemail") != null && request.getParameter("editemail").equals("none")) {

				String email = payeeBean.getEmail();
				String emailconfirm = payeeBean.getEmailConf();

				if (GenericValidator.isBlankOrNull(email)) {
					errors.rejectValue("email", "addpayee.email.requiered", "Required");
					payeeBean.setEmailError(true);
				}
				if (!GenericValidator.isBlankOrNull(email) && !GenericValidator.isEmail(email)) {

					errors.rejectValue("email", "addpayee.email.invalid", "Invalid Email");
					payeeBean.setEmailError(true);

				} else {

					if (!GenericValidator.isBlankOrNull(email) && !email.equals(emailconfirm)) {

						errors.rejectValue("emailConf", "addpayee.confirmationEmail.notmatch",
								"Email values must match");
						payeeBean.setEmailError(true);
					}
				}
				// MBSFE-691
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE691) && email != null && !payeeBean.getEmailError()){
					String[] domain = email.split("@");
					if(domain.length > 1 && Utils.haveType(this.invalidEmailDomains, domain[1])) {
						errors.rejectValue("emailConf", "email.invaliddomain", "Your e-mail address is incorrect. Please try again.");
						payeeBean.setEmailError(true);
					}
				}
				// END MBSFE-691
			}
			if (payeeBean.getActivateEbill() && pref.isSet(pref.VALIDATION_CODE) && pref.getValidationCode()
					&& (GenericValidator.isBlankOrNull(payeeBean.getZipcode())
							|| !GenericValidator.matchRegexp(payeeBean.getZipcode(), "\\d{5}"))) {
				errors.rejectValue("zipcode", "addpayee.zipcode.requiered", "zipcode is required for this payee");
			}
		}

	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.BILLPAY;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

	/*
	 * CIBP-1680 bgarcia Obtiene el texto de error actualizado de los archivo
	 */
	private String getTelepagoErrorDescriptor(String codigo, String len, String errorCode) {
		String status = "";
		try {
			if (!"en".equals(len))
				len = "es";

			status = this.messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
		} catch (Exception e) {
			status = errorCode;
		}

		return status;
	}

	private Map<Object, String> getStates() {
		Map<Object, String> states = new LinkedHashMap<>();
		states.put("PR", "PR");
		states.put("USVI", "USVI");
		states.put("BVI", "BVI");
		states.put("AL", "AL"); 
		states.put("AK", "AK"); 
		states.put("AZ", "AZ"); 
		states.put("AR", "AR"); 
		states.put("CA", "CA");
		states.put("CO", "CO"); 
		states.put("CT", "CT"); 
		states.put("DE", "DE"); 
		states.put("DC", "DC"); 
		states.put("FL", "FL");
		states.put("GA", "GA"); 
		states.put("HI", "HI"); 
		states.put("ID", "ID"); 
		states.put("IL", "IL"); 
		states.put("IN", "IN");
		states.put("IA", "IA"); 
		states.put("KS", "KS"); 
		states.put("KY", "KY"); 
		states.put("LA", "LA"); 
		states.put("ME", "ME");
		states.put("MD", "MD"); 
		states.put("MA", "MA"); 
		states.put("MI", "MI"); 
		states.put("MN", "MN"); 
		states.put("MS", "MS");
		states.put("MO", "MO");
		states.put("MT", "MT"); 
		states.put("MI", "MI"); 
		states.put("NE", "NE"); 
		states.put("NV", "NV"); 
		states.put("NH", "NH");
		states.put("NJ", "NJ"); 
		states.put("NM", "NM"); 
		states.put("NY", "NY"); 
		states.put("NC", "NC"); 
		states.put("ND", "ND");
		states.put("OH", "OH"); 
		states.put("OK", "OK"); 
		states.put("OR", "OR"); 
		states.put("PA", "PA"); 
		states.put("RI", "RI");
		states.put("SC", "SC"); 
		states.put("SD", "SD"); 
		states.put("TN", "TN"); 
		states.put("TX", "TX"); 
		states.put("UT", "UT");
		states.put("VT", "VT"); 
		states.put("VA", "VA"); 
		states.put("WA", "WA"); 
		states.put("WV", "WV"); 
		states.put("WI", "WI");
		states.put("WY", "WY");

		return states;

	}

}
