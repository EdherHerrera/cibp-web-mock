package evertec.cibp.web.form;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.ActivationCardAccountResponse;
import evertec.cibp.core.services.responses.ActivationCardAccountResponse.Status;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.encryptedPin.PinEncryptionTool;
import evertec.cibp.web.form.bean.ActivateATHBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * Class that manages input for the Activate ATH form functionality
 */
public class ActivateATHForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private PinEncryptionTool pinTool;
	
	private static final String VIEW_NAME = "activateath";
	private static final String ACTIVATED = "activated";
	private static final String NOT_ACTIVATED = "notactivated";
	private static final String MESSAGE = "message";

	/**
	 * Constructor for the Activate ATH form
	 * View and bean definition
	 */
	public ActivateATHForm() {
		super();
		setCommandClass(ActivateATHBean.class);
		setCommandName(VIEW_NAME);
		setFormView(VIEW_NAME);
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		ActivateATHBean bean = (ActivateATHBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		//MBSE 1560
		CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request);
		String athnum = bean.getAthNumber();
		String pin = bean.getPin();
		String encryptedPin = pinTool.encrypt(pin);

		boolean validation;
		model.put("athnum", athnum);

		ActivationCardAccountResponse activationResponse = customerService.activatePlasticAccount(customer,
				userSession.getBankingSession(), athnum, encryptedPin, event);
		if (activationResponse != null) {
			if (activationResponse.getResponseStatus() == Status.ACTIVATED)
				validation = true;
			else
				validation = false;
		} else
			validation = false;
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE301)){
			if(athnum.startsWith("4549")){
				if (!validation) {
					model.put(NOT_ACTIVATED, true);

					if (activationResponse != null && activationResponse.getResponseStatus() == Status.CARD_NOT_FOUND) {
						if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
							model.put(MESSAGE, "Your request to assign a personal identification number &#40;PIN&#41; was not authorized.");
						} else {
							model.put(MESSAGE, "Tu solicitud de asignar un n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; no fue autorizada.");
						}
					} else if (activationResponse != null && activationResponse.getResponseStatus() == Status.PIN_ALREADY_VALIDATED) {
						if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
							model.put(MESSAGE, "Your International ATH Visa card has been succesfully activated and is ready for use.<br/><br/>"
									+ "This International ATH Visa card already has a personal identification number &#40;PIN&#41; assigned.&nbsp;"
									+ "If you want to change or don&#39;t remember your PIN, visit any of our branches.");
						} else {
							model.put(MESSAGE, "Tu tarjeta ATH Internacional Visa est&aacute; activada  y lista para utilizarse.<br/><br/>"
									+ "La tarjeta tiene n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; asignado anteriormente.&nbsp;"
									+ "Si deseas cambiar o no recuerdas tu PIN, visita cualquiera de nuestras sucursales.");
						}
					} else {
						if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
							model.put(MESSAGE, "Your request to activate your International ATH Visa card cannot be processed at this moment.");
						} else {
							model.put(MESSAGE, "Tu solicitud para activar tu tarjeta ATH Internacional VISA no pudo ser procesada en este momento.");
						}
					}
				} else {
					model.put(ACTIVATED, true);
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						model.put(MESSAGE, "This International ATH Visa card has been succesfully activated and  your personal identification number &#40;PIN&#41; has been assigned.");
					} else {
						model.put(MESSAGE, "Tu tarjeta ATH Internacional Visa ha sido activada y su n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; fue asignado exitosamente.");
					}

				}
			}
			else {
				if (!validation) {
					model.put(NOT_ACTIVATED, true);

					if (activationResponse != null && activationResponse.getResponseStatus() == Status.CARD_NOT_FOUND) {
						if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
							model.put(MESSAGE, "Your request to assign a personal identification number &#40;PIN&#41; was not authorized.");
						} else {
							model.put(MESSAGE, "Tu solicitud de asignar un n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; no fue autorizada.");
						}
					} else if (activationResponse != null && activationResponse.getResponseStatus() == Status.PIN_ALREADY_VALIDATED) {
						if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
							model.put(MESSAGE, "Your ATH Card is active and is ready for use.<br/><br/>"
									+ "This ATH card already has a personal identification number &#40;PIN&#41; assigned.&nbsp;"
									+ "If you want to change or don&#39;t remember your PIN, visit any of our branches.");
						} else {
							model.put(MESSAGE, "Tu tarjeta ATH est&aacute; activada y lista para utilizarse.<br/><br/>"
									+ "La tarjeta tiene n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; asignado anteriormente.&nbsp;"
									+ "Si deseas cambiar o no recuerdas tu PIN, visita cualquiera de nuestras sucursales.");
						}
					} else {
						if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
							model.put(MESSAGE, "Your request to assign a personal identification number &#40;PIN&#41; cannot be processed at this moment.");
						} else {
							model.put(MESSAGE, "Tu solicitud de asignar un n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; no pudo ser procesada en este momento.");
						}
					}
				} else {
					model.put(ACTIVATED, true);
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						model.put(MESSAGE, "Your personal identification number &#40;PIN&#41; was successfully assigned and is ready for use.");
					} else {
						model.put(MESSAGE, "Tu n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; fue asignado exitosamente y est&aacute; lista para utilizarse.");
					}

				}
			}
		}
		else{
			if (!validation) {
				model.put(NOT_ACTIVATED, true);

				if (activationResponse != null && activationResponse.getResponseStatus() == Status.CARD_NOT_FOUND) {
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						model.put(MESSAGE, "ATH activation not authorized.");
					} else {
						model.put(MESSAGE, "Activaci&oacute;n de ATH no autorizada");
					}
				} else if (activationResponse != null && activationResponse.getResponseStatus() == Status.PIN_ALREADY_VALIDATED) {
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						model.put(MESSAGE,
								"This ATH card is already validated. To change your PIN visit any of our branches");
					} else {
						model.put(MESSAGE,
								"Esta tarjeta ATH ya est&aacute; validada. Para cambiar tu PIN visita cualquiera de nuestras sucursales.");
					}
				} else {
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						model.put(MESSAGE, "Unable to activate your ATH card this time.");
					} else {
						model.put(MESSAGE, "Su solicitud de activar su ATH no pudo ser procesada en este momento.");
					}
				}
			} else {
				model.put(ACTIVATED, true);
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
					model.put(MESSAGE, "Your ATH card has been validated.");
				} else {
					model.put(MESSAGE, "Su ATH ha sido validada exitosamente.");
				}

			}
		}

		return showForm(request, error, VIEW_NAME, model);
	}
	

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		ActivateATHBean bean = (ActivateATHBean) command;

		if (!GenericValidator.matchRegexp(bean.getAthNumber(), "\\d{16}") || !(bean.getAthNumber().startsWith("0215") || bean.getAthNumber().startsWith("4549"))) {
			errors.rejectValue("athNumber", "activateath.accountnumber.invalid", "Please enter a card number");
			bean.setFormError(true);
		}
		if (!GenericValidator.matchRegexp(bean.getPin(), "\\d{4}")) {
			errors.rejectValue("pin", "activateath.pin.invalid", "Please enter a valid PIN");
			bean.setFormError(true);
		}
		if (GenericValidator.isBlankOrNull(bean.getPinconfirm()) || !bean.getPin().equals(bean.getPinconfirm())) {
			errors.rejectValue("pinconfirm", "activateath.pinconfirmation.invalid",
					"Please enter a valid PIN confirmation");
			bean.setFormError(true);
		}
	}
	
	
	public PinEncryptionTool getPinTool() {
		return pinTool;
	}

	public void setPinTool(PinEncryptionTool pinTool) {
		this.pinTool = pinTool;
	}
}
