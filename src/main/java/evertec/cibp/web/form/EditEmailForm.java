package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rsa.csd.ws.EventType;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyAllCustomerEmailsRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.EditEmailBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

public class EditEmailForm extends CancellableFormController implements MessageSourceAware {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;
	protected CustomerServices customerService;
	
	// MBSFE-691
	private String invalidEmailDomains;
	// END MBSFE-691

	public EditEmailForm() {
		super();
		setCommandClass(EditEmailBean.class);
		setCommandName("editemail");
		setFormView("editemail");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new HashMap<>();
		EditEmailBean dude = (EditEmailBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		try {

			if(!Utils.validateTokenCsrfGuard(request)) {
				throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
			}
			//OOB : verify if come from OOB Challenge
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && userSession.getOobEnroll() && userSession.isOobAuthenticated() 
					&& userSession.getTasksListOOB() != null && userSession.getTasksListOOB().contains(ASK_FOR_OOB.EDIT_EMAIL )) {
				//get the information that changes if exists.
				@SuppressWarnings("unchecked")
				Map<String, Object> parameters = (Map<String, Object>) WebUtils.getSessionAttribute(request,
						ChallengeInformationUtils.INFORMATION_PARAMETERS);
				if(parameters != null && parameters.get(ChallengeInformationUtils.EMAIL_ACTION) != null) {
					
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
				}
			}
			
			/* CIBP-1104 */
			CustomerProfile customer = userSession.getCustomerProfile();

			// CIBP-2089 bgarcia
			if (getEBPPMessageSourceStatus()
					&& customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {
				
				ModifyAllCustomerEmailsRequest macer = new ModifyAllCustomerEmailsRequest(
						Utils.generateCustomerInteractionEvent(request), customer, 
						customerService, userSession.getAccounts(), userSession.getBankingSession(), 
						userSession.getEbpp2UserId(), userSession.getPreferredLanguage(), 
						dude.getEmail()); 
				customer = CustomerUtils.modifyAllEmails(macer);
				userSession.setCustomerProfile(customer);
				
				model.put("confirm", true);
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Email has been changed.");
				else
					userSession.setStatusmessage("E-mail ha sido cambiado.");

			} else {// CIBP-2089 bgarcia
				model.put("confirm", true);
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("We are sorry, the system isn&acute;t available. Please try later.");
				else
					userSession
							.setStatusmessage("Lo sentimos, el sistema no est&aacute; disponible. Por favor intenta mas tarde.");
			}

		} catch (Exception e) {
			error.rejectValue("email", "", "Error!");
			model.put("confirm", true);
			model.put("editEmailMobile", "Service invocation failed!");
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Unable to change email.");
			else
				userSession.setStatusmessage("Email no pudo ser cambiado.");
		}
		Cookie cookie = WebUtils.getCookie(request, "PMData") != null ? WebUtils.getCookie(request, "PMData") : new Cookie("PMData", "");
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		ESAPI.httpUtilities().addCookie(response, cookie);

		return showForm(request, response, error, model);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		EditEmailBean dude = (EditEmailBean) command;
		
		// MBSFE-691
		String[] domain = dude.getEmail().split("@");
		// END MBSFE-69
				
		if (GenericValidator.isBlankOrNull(dude.getEmail()))
			errors.rejectValue("email", "editemail.required", "Required");
		else if (!GenericValidator.isEmail(dude.getEmail()))
			errors.rejectValue("email", "editemail.invalid", "Invalid format");
		else if (!dude.getEmail().equals(dude.getEmailconfirm()))
			errors.rejectValue("emailconfirm", "editemail.mustmatch", "Fields must match");
		
		// MBSFE-691
		else if (Utils.haveType(this.invalidEmailDomains, domain[1])
				&& StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE691)) {
			errors.rejectValue("email", "email.invaliddomain", "Your e-mail address is incorrect. Please try again.");
		}
		// END MBSFE-691
		
		if (errors.getAllErrors().size() == 0) {
			Boolean informationChallenge = session.isInformationChallenge();
			
			//execute oob challenge
			String username = session.getUsername();
			
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)) {
				if (!session.getOobEnroll()) {
					informationChallenge = session.isInformationChallenge();
				} else {
					informationChallenge = session.isOobAuthenticated();
					session.setOobChallenge(true);
				}
			} else {
				informationChallenge = session.isInformationChallenge();
			}
		 if (informationChallenge != null
					&& !informationChallenge
					&& !dude.getSkipRSA()
					&& (session.getGuidedSetupSession() == null || !session.getGuidedSetupSession().isGuidedSetupMode())) {

				// ////////////////////

				String language = Utils.getRSALanguage(request);
//				String username = session.getUsername();
				

				if (session.getInformationQuestion() == null  && !session.getOobEnroll()) {

					if (informationChallenge != null && !informationChallenge) {

						DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(session.getPm_fp(),session.getDeviceRequest().getDeviceTokenCookie(), request);
						RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
						AnalyzeResponse anResp = session.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

						deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
						deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

						session.setRsaSessionId(anResp.getIdentificationData().getSessionId());
						session.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

						session.setDeviceRequest(deviceRequest);

						if (!(ActionCode.CHALLENGE == anResp.getRiskResult().getTriggeredRule().getActionCode())) {
							session.setInformationChallenge(true);
						} else {
							String email = null;
							if (session.getCustomerProfile() != null)
								email = session.getCustomerProfile().getEmail();

							QuestionResponse personalQuestion;

							if (session.getInformationQuestion() == null) {
								RSAActionRequest actionRequest = new RSAActionRequest(
										Utils.generateCustomerInteractionEvent(request, session.getUsername()),
										session.getUsername(), "", email, 0, session.isRemember(),
										session.getDeviceRequest());

								if (session.getCustomerProfile() != null)
									actionRequest.setUsername(session.getCustomerProfile().getUsername());
								else if (session.getUsernameReservation() != null)
									actionRequest.setUsername(session.getUsernameReservation().getUsername());
								else
									actionRequest.setUsername(username);

								actionRequest.setDeviceRequest(session.getDeviceRequest());
								actionRequest.setRsaSessionId(session.getRsaSessionId());
								actionRequest.setRsaTransactionId(session.getRsaTransactionId());

								actionRequest.setRsaLang(Utils.getRSALanguage(request));

								personalQuestion = session.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

							} else {
								personalQuestion = new QuestionResponse();
								personalQuestion.setStatusResult(ResultStatus.SUCCESS);
								personalQuestion.setQuestion(session.getInformationQuestion());
							}
							if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
								session.setInformationQuestion(personalQuestion.getQuestion());

							} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
								throw new ModelAndViewDefiningException(new ModelAndView("error"));
							} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
								throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
							}
						}
					}
				}

				errors.rejectValue("", "", "");
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put(ChallengeInformationUtils.EMAIL_ACTION, command);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
						ChallengeInformationUtils.EMAIL_ACTION);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, parameters);
			}
		}
	}

	// CIBP-2089 bgarcia
	public CustomerServices getCustomerService() {
		return customerService;
	}

	// CIBP-2089 bgarcia
	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	// CIBP-2089 bgarcia
	public MessageSource getMessageSource() {
		return messageSource;
	}

	// CIBP-2089 bgarcia
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	// MBSFE-691
	public String getInvalidEmailDomains() {
		return invalidEmailDomains;
	}

	public void setInvalidEmailDomains(String invalidEmailDomains) {
		this.invalidEmailDomains = invalidEmailDomains;
	}
	// END MBSFE-691

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	// CIBP-2089 bgarcia
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		Utils.generateTokenCsrfGuard(request);
	
	
		EditEmailBean dude = (EditEmailBean) command;
		if (dude.getSkipRSA() == null) {
			dude.setSkipRSA(false);
			String vm = request.getParameter("vm");
			if (vm != null && vm.equals("cu")) {
				dude.setSkipRSA(true);
			}
		}

		if ((userSession.getGuidedSetupSession() != null && userSession.getGuidedSetupSession().isGuidedSetupMode())
				|| dude.getSkipRSA()) {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, true);
		} else {
			// FFEIC + SMS
			String username = userSession.getUsername();
			model.put("username", username);

			Cookie cookie = WebUtils.getCookie(request, "PMData");
			if (cookie == null)
				cookie = new Cookie("PMData", "");

			cookie.setPath("/");
			cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
			if (cookie != null)
				model.put("devicetoken", cookie.getValue());

//			OOB and RSA
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)) {
				if (userSession.getOobEnroll()) {
				
						List<ASK_FOR_OOB> listNavOOB = new ArrayList<ASK_FOR_OOB>();
						// add the navigation to CHANGE_OOB in cancel case
						if (userSession.getTasksListOOB() != null
								&& userSession.getTasksListOOB().contains(ASK_FOR_OOB.ALERT_TYPES)) {
							model.put("oobEnrolled", true);
							model.put("oobChallenge", "loginoob");
							userSession.setInformationChallenge(userSession.isOobAuthenticated());
							listNavOOB.addAll(userSession.getTasksListOOB());
							if(!userSession.getTasksListOOB().contains(ASK_FOR_OOB.EDIT_EMAIL))
								listNavOOB.add(ASK_FOR_OOB.EDIT_EMAIL);
							if (!userSession.getTasksListOOB().contains(ASK_FOR_OOB.OOB_CHALLENGE))
								listNavOOB.add(0, ASK_FOR_OOB.OOB_CHALLENGE);
							userSession.setInformationChallenge(userSession.isOobAuthenticated());
						} else if (!userSession.isOobAuthenticated() && userSession.getOobChallenge() != null && userSession.getOobChallenge()) {
							model.put("oobchallenge", true);
							// select the next step after OOB Challenge
							listNavOOB.add(0, ASK_FOR_OOB.PERSONAL_INFORMATION);
							// add the challenge OOB steep navigation is
							// optional
							listNavOOB.add(0, ASK_FOR_OOB.OOB_CHALLENGE);
							userSession.setOobChallenge(false);
						}

						userSession.setTasksListOOB(listNavOOB);
					} else if (userSession.getInformationQuestion() != null) {
						String question = (String) userSession.getInformationQuestion().values().toArray()[0];
						model.put("question", question);
					}
			} else {
				if (userSession.getInformationQuestion() != null) {
					String question = (String) userSession.getInformationQuestion().values().toArray()[0];
					model.put("question", question);
				}
			}
			
			// /////////////////////////////////////
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, userSession.isInformationChallenge());
		}

		
		return model;
	}

}
