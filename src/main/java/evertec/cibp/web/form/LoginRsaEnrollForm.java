package evertec.cibp.web.form;

import com.rsa.csd.ws.DeviceRequest;
import edu.emory.mathcs.backport.java.util.Arrays;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.CardCVVAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.CardPINAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.DepositAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.NonTransAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.PayrollCardAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.adaptiveauth.EnrollmentInformationinRSARequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse.AccountValidationStatus;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionSelectionResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.encryptedPin.PinEncryptionTool;
import evertec.cibp.web.form.bean.ValidateInfoBean;
import evertec.cibp.web.form.bean.ValidateInfoBean.ACTION;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.utils.ValidationUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
/**
 * LoginRsaEnrollForm
 * @author et55084
 * @since 1.2
 * @version 1.0 
 */
public class LoginRsaEnrollForm extends AbstractWizardFormController {
	
    /** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private PinEncryptionTool pinTool;
	private MessageSource messageSource;
	private String paymentValidationRange;
	private String nonTransProductFlow;
	
	// MBSFE-691
	private String invalidEmailDomains;
	// END MBSFE-691

	public LoginRsaEnrollForm() {
		super();
		setCommandClass(ValidateInfoBean.class);
		setCommandName("loginRsaEnroll");
		setPages(new String[] { "loginRsaEnroll", "loginRsaEnroll", "loginRsaEnroll", "loginRsaEnroll" });
	}
	
	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		ValidateInfoBean info = (ValidateInfoBean) super.formBackingObject(request);
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.RSA_MOBILE)) {
			String type = ServletRequestUtils.getStringParameter(request, "agentType", "web");
			if ("android".equalsIgnoreCase(type) || "ios".equalsIgnoreCase(type)) {
				info.setMobileAppType(type);
				info.setMobileApp(Boolean.TRUE);
			}
		}
		return info;
	}

	@Override
	protected int getTargetPage(HttpServletRequest request, Object command, Errors errors, int currentPage) {

		if (currentPage == 0) {
			UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			CustomerProfile prof = session.getCustomerProfile();
			if (prof.hasFlag(CustomerFlagType.OVERRIDE_RSA_VALIDATION)) {
				ValidateInfoBean info = (ValidateInfoBean) command;
				if (!info.actionsProcessed.contains(ACTION.PIN) && !info.actionsProcessed.contains(ACTION.EXPDATE)) {
					info.actionsProcessed.add(ACTION.PIN);
					info.actionsProcessed.add(ACTION.EXPDATE);
				}
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA228) && !info.actionsProcessed.contains(ACTION.NONTRANSACT))
				{
					info.actionsProcessed.add(ACTION.NONTRANSACT);
				}
				return 2;
			} else
				return super.getTargetPage(request, command, errors, currentPage);
		}

		return super.getTargetPage(request, command, errors, currentPage);
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		ValidateInfoBean info = (ValidateInfoBean) command;
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		DeviceRequest deviceRequest = session.getDeviceRequest();

		// Add to RSA
		String q1 = info.getQuestion1();
		String q2 = info.getQuestion2();
		String q3 = info.getQuestion3();
		String a1 = info.getAnswer1();
		String a2 = info.getAnswer2();
		String a3 = info.getAnswer3();
		String computerType = info.getComputerType();
		boolean saveDevice = info.isSaveDevice();
		if ("private".equals(computerType))
			saveDevice = true;

		String username = session.getCustomerProfile().getUsername().toLowerCase();
		EnrollmentInformationinRSARequest req = new EnrollmentInformationinRSARequest(
				Utils.generateCustomerInteractionEvent(request), username,
				session.getCustomerProfile().getEmail(), saveDevice,deviceRequest);
		req.setQuestionsAndAnswer( q1,q2, q3, a1, a2, a3);

		/**
		 * MBNA-2518
		 * Method enrollRSA : CIBP-WEB microservice integration
		 * 
		 */
		ModifyCustomerProfileResponse enrollRSAresponse = session.getCustomerService().getSignOnCustomerServiceModule().enrollRSA(req);
		if (!ResultStatus.SUCCESS.equals(enrollRSAresponse.getStatus())) {
			error.reject("enrollment.invalid", "Please verify your input information");
			return showPage(request, error, 2);
		}

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
		cookie.setPath("/");
		
		ESAPI.httpUtilities().addCookie(response, cookie);
		info.actionsProcessed.add(ACTION.QUESTIONS);
		info.actionsProcessed.add(ACTION.CONTRACT);

		List<ASK_FOR> tasksList = session.getTasksList();
		if (tasksList.contains(ASK_FOR.RSA_ENROLL)) {
			List<ACTION> removetasks = info.getActionsProcessed();
			if (removetasks.contains(ACTION.EMAIL)
					&& (removetasks.contains(ACTION.PIN) || removetasks.contains(ACTION.EXPDATE) || removetasks.contains(ACTION.NONTRANSACT))
					&& removetasks.contains(ACTION.QUESTIONS) && removetasks.contains(ACTION.CONTRACT)) {
				// All tasks for SSDS or FORCED first time login were validated
				tasksList.remove(ASK_FOR.RSA_ENROLL);

			}
		}

		return new ModelAndView(SignonSequenceUtils.getNextView(session));
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		ValidateInfoBean info = (ValidateInfoBean) command;
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();
		CustomerServices customerService = session.getCustomerService();

		ValidateInfoBean.ACTION action = info.getAction();

		if (page == 0) {
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE13)){
				if (action == ValidateInfoBean.ACTION.EMAIL) {
					String email = info.getEmail();
					String emailconf = info.getEmailconfirm();
					if (GenericValidator.isBlankOrNull(email)) {
						errors.rejectValue("email", "editemail.required", "Required");
					} else if (!GenericValidator.isEmail(email)) {
						errors.rejectValue("email", "editemail.invalid", "Invalid Email");
					} else if (!email.equalsIgnoreCase(emailconf)) {
						errors.rejectValue("emailconfirm", "editemail.mustmatch", "Email values must match");
					} else {
						if (email.equalsIgnoreCase(profile.getEmail()))
							info.actionsProcessed.add(ACTION.EMAIL);
						else
							errors.rejectValue("email", "enrollment.email.invalid", "Invalid Email");
					}
				}
			}
			else{
				if (action == ValidateInfoBean.ACTION.EMAIL) {
					String email = info.getEmail();
					String emailconf = info.getEmailconfirm();
					
					// MBSFE-691
					String[] domain = email.split("@");
					// END MBSFE-69
					
					if (GenericValidator.isBlankOrNull(email)) {
						errors.rejectValue("email", "editemail.required", "Required");
					} else if (!GenericValidator.isEmail(email)) {
						errors.rejectValue("email", "editemail.invalid", "Invalid Email");
					} else if (!email.equals(emailconf)) {
						errors.rejectValue("emailconfirm", "editemail.mustmatch", "Email values must match");		
						
					// MBSFE-691
					} else if (Utils.haveType(this.invalidEmailDomains, domain[1])
							&& StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE691)) {
						errors.rejectValue("email", "email.invaliddomain",
								"Your e-mail address is incorrect. Please try again.");
					// END MBSFE-691
					
					} else {
						if (email.equals(profile.getEmail()))
							info.actionsProcessed.add(ACTION.EMAIL);
						else
							errors.rejectValue("email", "enrollment.email.invalid", "Invalid Email");
					}
				}
			}
		}

		else if (page == 1) {

			if (action == ValidateInfoBean.ACTION.PIN) {
				String pin = info.getPin();
				ValidateAccountStatusResponse res = null;
				if (GenericValidator.isBlankOrNull(pin) || !GenericValidator.matchRegexp(pin, "\\d{4}")) {
					errors.rejectValue("pin", "enrollment.pin.invalid", "Invalid Personal Identification Number");
					return;
				} else if (profile.getEnrollmentAccountSubtype().equalsIgnoreCase("AMEX")) {
					// Add the 0 to the front of the AMEX card
					String formattedAccountNumber = (profile.getEnrollmentAccountNumber().startsWith("0377") ? profile
							.getEnrollmentAccountNumber() : "0" + profile.getEnrollmentAccountNumber());
					String encryptedPin = pinTool.encrypt(pin);
					CardPINAccountInformationValidationRequest req = new CardPINAccountInformationValidationRequest(
							Utils.generateCustomerInteractionEvent(request), formattedAccountNumber,
							profile.getTaxId(), encryptedPin, "", "", "", true, false, false);
					//Microservice implementation
					 res = customerService.validateCrediCardPINAccountInfo(req);
				} else if (profile.getEnrollmentAccountSubtype().equalsIgnoreCase("CCA")
						&& profile.getEnrollmentAccountNumber().startsWith("0377")) {
					// Add the 0 to the front of the AMEX card
					String formattedAccountNumber = profile.getEnrollmentAccountNumber();
					String encryptedPin = pinTool.encrypt(pin);
					CardPINAccountInformationValidationRequest req = new CardPINAccountInformationValidationRequest(
							Utils.generateCustomerInteractionEvent(request), formattedAccountNumber,
							profile.getTaxId(), encryptedPin, "", "", "", true, false, false);
					//Microservice implementation
					res = customerService.validateCrediCardPINAccountInfo(req);
				} else if (profile.getEnrollmentAccountSubtype().equalsIgnoreCase("PFC")) {
					String encryptedPin = pinTool.encrypt(pin);
					PayrollCardAccountInformationValidationRequest req = new PayrollCardAccountInformationValidationRequest(
							Utils.generateCustomerInteractionEvent(request), profile.getEnrollmentAccountNumber(),
							profile.getTaxId(), encryptedPin, true);
					res = customerService.validatePayrollCardAccountInfo(req);
				} else
				// WE ASUME IDA
				// if(profile.getEnrollmentAccountSubtype().equalsIgnoreCase("IDA"))
				{
					String encryptedPin = pinTool.encrypt(pin);
					DepositAccountInformationValidationRequest req = new DepositAccountInformationValidationRequest(
							Utils.generateCustomerInteractionEvent(request), profile.getEnrollmentAccountNumber(), "",
							profile.getTaxId(), encryptedPin, true, false);
					res = customerService.validateDepositAccountInfo(req);
				}

				if (res.getStatus() != null && res.getStatus() == AccountValidationStatus.VALID) {
					info.actionsProcessed.add(ACTION.PIN);
				} else if (res.getStatus() != null && res.getStatus() == AccountValidationStatus.ACCOUNT_BLOCKED) {
					info.setPinCounter(3);
					info.actionsProcessed.add(ACTION.PIN);
				} else {
					info.setPinCounter(info.getPinCounter() + 1);
					errors.rejectValue("pin", "loginrsa.accountinfo.invalid", "Please verify input");
				}
			} else if (action == ValidateInfoBean.ACTION.EXPDATE) {
				String cvv = info.getCvv();
				String expMonth = info.getExpMonth();
				String expYear = info.getExpYear();

				boolean err = false;
				if (GenericValidator.isBlankOrNull(cvv) || !GenericValidator.matchRegexp(cvv, "\\d{3}")) {
					errors.rejectValue("cvv", "loginrsa.cvv.invalid", "Invalid CVV code");
					err = true;
				}
				if (GenericValidator.isBlankOrNull(expMonth) || expMonth.equals("0")) {
					errors.rejectValue("expYear", "loginrsa.expDate.invalid", "Invalid exp month");
					err = true;
				} else if (GenericValidator.isBlankOrNull(expYear) || !GenericValidator.matchRegexp(expYear, "\\d{4}")) {
					errors.rejectValue("expYear", "loginrsa.expDate.invalid", "Invalid exp year");
					err = true;
				}

				if (err)
					return;

				if ("CCA".equalsIgnoreCase(profile.getEnrollmentAccountSubtype())) {
					CardCVVAccountInformationValidationRequest req = new CardCVVAccountInformationValidationRequest(
							Utils.generateCustomerInteractionEvent(request), profile.getEnrollmentAccountNumber(),
							profile.getTaxId(), cvv, expMonth, expYear, "", false);
					ValidateAccountStatusResponse res = customerService.validateCrediCardCVVAccountInfo(req);
					if (res.getStatus() == AccountValidationStatus.VALID) {
						info.actionsProcessed.add(ACTION.EXPDATE);
					} else if (res.getStatus() == AccountValidationStatus.ACCOUNT_BLOCKED) {
						info.setPinCounter(3);
						info.actionsProcessed.add(ACTION.EXPDATE);
					} else {
						info.setPinCounter(info.getPinCounter() + 1);
						errors.rejectValue("cvv", "loginrsa.accountinfo.invalid", "Invalid Information");
					}
				}
			//Added NONTRANSACT for MLA, RVA, LEA, etc...
			} else if  (action == ValidateInfoBean.ACTION.NONTRANSACT && StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA228) 
					&& getAllowedNonTransactionalAccts().contains(profile.getEnrollmentAccountSubtype())){
				
				Integer originationYear = info.getOriginationYear();
				BigDecimal paymentAmount = info.getPaymentAmount();
				String accountNumber = profile.getEnrollmentAccountNumber();
				
				boolean error = !ValidationUtil.validAccountNumber(profile.getEnrollmentAccountSubtype(), accountNumber) || !ValidationUtil.validLoanFields(originationYear, paymentAmount);
					
				if(error) {
					errors.rejectValue("error", "loginrsa.nontrans.accountinfo.invalid", "Please verify your account information.");
					return;
				}
					
				NonTransAccountInformationValidationRequest ntReq = new NonTransAccountInformationValidationRequest(
								null, profile.getEnrollmentAccountNumber(), profile.getTaxId(), true, originationYear, 
								paymentAmount, paymentValidationRange, true, profile.getEnrollmentAccountSubtype());
						
				ValidateAccountStatusResponse res = customerService.validateNonTransAccountInfo(ntReq);
					
				if (res.getStatus() == AccountValidationStatus.VALID) {
					info.actionsProcessed.add(ACTION.NONTRANSACT);
				} else if (res.getStatus() == AccountValidationStatus.ACCOUNT_BLOCKED || (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA782) && info.getPinCounter() >= 2)) {
					info.setPinCounter(3);
					info.actionsProcessed.add(ACTION.NONTRANSACT);
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA782) && !profile.hasFlag(CustomerFlagType.PASSWORD_RESET_BLOCK)) {
						customerService.blockCustomerResetPassword(profile, Utils.generateCustomerInteractionEvent(request), session.getDeviceId());	
					}
								
				} else {
					info.setPinCounter(info.getPinCounter() + 1);
					errors.rejectValue("error", "loginrsa.nontrans.accountinfo.invalid", "Please verify your account information.");
				}
			}
		}

		else if (page == 2) {

			if (action == ValidateInfoBean.ACTION.QUESTIONS) {

				String q1 = info.getQuestion1();
				String q2 = info.getQuestion2();
				String q3 = info.getQuestion3();
				String a1 = info.getAnswer1();
				String a2 = info.getAnswer2();
				String a3 = info.getAnswer3();
				String a1c = info.getAnswer1confirm();
				String a2c = info.getAnswer2confirm();
				String a3c = info.getAnswer3confirm();
				String computerType = info.getComputerType();
				String re = "^\\w+(\\s{1}\\w+)*$";

				if (GenericValidator.isBlankOrNull(q1)) {
					errors.rejectValue("question1", "enrollment.question.required", "Required");
				}
				if (GenericValidator.isBlankOrNull(q2)) {
					errors.rejectValue("question2", "enrollment.question.required", "Required");
				}
				if (GenericValidator.isBlankOrNull(q3)) {
					errors.rejectValue("question3", "enrollment.question.required", "Required");
				}

				if (GenericValidator.isBlankOrNull(a1) || !GenericValidator.matchRegexp(a1, re)) {
					errors.rejectValue("answer1", "enrollment.answer.invalid", "Please enter a valid answer");

				} else if (!GenericValidator.isBlankOrNull(a1) && !a1.equals(a1c)) {
					errors.rejectValue("answer1confirm", "enrollment.answerconfirm.notmatch",
							"Answers values must match");

				}

				if (GenericValidator.isBlankOrNull(a2) || !GenericValidator.matchRegexp(a2, re)) {
					errors.rejectValue("answer2", "enrollment.answer.invalid", "Please enter a valid answer");

				} else if (!GenericValidator.isBlankOrNull(a2) && !a2.equals(a2c)) {
					errors.rejectValue("answer2confirm", "enrollment.answerconfirm.notmatch",
							"Answers values must match");

				}

				if (GenericValidator.isBlankOrNull(a3) || !GenericValidator.matchRegexp(a3, re)) {
					errors.rejectValue("answer3", "enrollment.answer.invalid", "Please enter a valid answer.");

				} else if (!GenericValidator.isBlankOrNull(a3) && !a3.equals(a3c)) {
					errors.rejectValue("answer3confirm", "enrollment.answerconfirm.notmatch",
							"Answers values must match");

				}
				if (!"private".equals(computerType) && !"public".equals(computerType)) {
					errors.rejectValue("computerType", "enrollment.computerType.required",
							"Please select a computer type");

				}
			}
		}

		else if (page == 3) {

			if (GenericValidator.isBlankOrNull(info.getAgreement()) || !info.getAgreement().equalsIgnoreCase("agree")) {
				errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree");
			}
		}
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();
		Map<String, Object> model = new HashMap<>(0);
		ValidateInfoBean info = (ValidateInfoBean) command;

		WebUtils.setSessionAttribute(request, Utils.DEEP_LINK, null);
		
		model.put("customername", profile.getFirstName());

		List<ASK_FOR> tasksList = session.getTasksList();
		for (ASK_FOR thing : tasksList) {
			switch (thing) {
			case RSA_ENROLL:
				model.put("validateemail", true);
				if (profile.getEnrollmentAccountSubtype().equals("IDA")
						|| profile.getEnrollmentAccountSubtype().equals("AMEX")
						|| profile.getEnrollmentAccountSubtype().equals("PFC")
						|| ("CCA".equalsIgnoreCase(profile.getEnrollmentAccountSubtype()) && profile
								.getEnrollmentAccountNumber().startsWith("0377"))) {
					model.put("validatepin", true);
				}
				else if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA228) 
						&& getAllowedNonTransactionalAccts().contains(profile.getEnrollmentAccountSubtype())) {
					model.put("validateNonTrans", true);
				}
				else {
					model.put("validatecvvexpiration", true);
				}

				model.put("selectquestions", true);
				model.put("showcontract", true);
				break;
			default:
				break;
			}
		}

		model.put("page", Integer.toString(page));
		if (info.getPinCounter() >= 3 || (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA782) 
				&& profile.hasFlag(CustomerFlagType.PASSWORD_RESET_BLOCK))) {
			model.put("blocked", true);
		}

		Map<String, String> agree = new HashMap<String, String>(0);
		agree.put("agree", "");
		model.put("agree", agree);

		// poner reference para cvv y exp date
		// expYears
		Map<Object, String> expYears = new LinkedHashMap<Object, String>(0);
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			expYears.put("", "A&ntilde;os:");
		else
			expYears.put("", "Year:");
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		Calendar cal = GregorianCalendar.getInstance();
		for (int i = 0; i <= 8; i++) {
			String year = df.format(cal.getTime());
			expYears.put(Integer.parseInt(year), year);
			cal.add(Calendar.YEAR, 1);
		}
		model.put("expYears", expYears);

		// expMonths
		Map<Object, String> expMonths = new LinkedHashMap<Object, String>(0);
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			expMonths.put("", "Mes:");
		else
			expMonths.put("", "Month:");
		df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
		cal = GregorianCalendar.getInstance();
		cal.set(Calendar.MONTH, 0);
		for (int i = 1; i <= 12; i++) {
			String month = df.format(cal.getTime());
			expMonths.put(i, month);
			cal.add(Calendar.MONTH, 1);

		}
		model.put("expMonths", expMonths);

		// Questions
		if (page == 2) {

			Map<String, String> questions = new LinkedHashMap<String, String>(0);
			String selectquestiontext = "Select a question:";
			if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
				selectquestiontext = "Seleccione una pregunta:";
			questions.put("", selectquestiontext);

			//Add model, to show default value into the view
			model.put("questionsLabel", selectquestiontext);

			CustomerInteractionEvent generateCustomerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
			RSAActionRequest actionrequest = new RSAActionRequest(generateCustomerInteractionEvent, profile
					.getUsername().toLowerCase(), "", profile.getEmail(), 0, false, session.getDeviceRequest());
			actionrequest.setRsaLang("sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "ES" : "EN");
			
			/**
			 * MBNA-2536
			 * Method getQuestionsSelection : CIBP-WEB microservice integration
			 */
			QuestionSelectionResponse questionResponse = session.getCustomerService().rsaGetQuestionsSelection(actionrequest);

			Cookie cookie = WebUtils.getCookie(request, "PMData");
			if (cookie == null)
				cookie = new Cookie("PMData", "");
			cookie.setPath("/");
			cookie.setValue(questionResponse.getRsaCookie());

			if (questionResponse.getStatusResult().equals(ResultStatus.SUCCESS)) {
				Map<Object, Object> q = new HashMap<Object, Object>(0);
				q.put("", selectquestiontext);
				q.putAll(questionResponse.getQuestionMap1());
				model.put("questions1", q);

				Map<Object, Object> q2 = new HashMap<Object, Object>(0);
				q2.put("", selectquestiontext);
				q2.putAll(questionResponse.getQuestionMap2());
				questions = new LinkedHashMap<String, String>();
				questions.put("", selectquestiontext);
				model.put("questions2", q2);
				
				Map<Object, Object> q3 = new HashMap<Object, Object>(0);
				q3.put("", selectquestiontext);
				q3.putAll(questionResponse.getQuestionMap3());
				questions = new LinkedHashMap<String, String>(0);
				questions.put("", selectquestiontext);
				model.put("questions3", q3);
			}

		}
		model.put("epayrollEnhFlag", StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLLENHA));
		
		model.put("mobileApp", info.isMobileApp());
		
		return model;
	}
	
	@SuppressWarnings("unchecked")
	private List<String> getAllowedNonTransactionalAccts() {
		String[] ntAccountsArray = getNonTransProductFlow().split(",");
		return Arrays.asList(ntAccountsArray);
	}

	public PinEncryptionTool getPinTool() {
		return pinTool;
	}

	public void setPinTool(PinEncryptionTool pinTool) {
		this.pinTool = pinTool;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	private boolean isEbppAvailable() {
		boolean ebppStatus = false;

		if (this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
				.equals(EntitlementStatus.ENABLED.toString())) {
			ebppStatus = true;
		} else {
			ebppStatus = false;
		}

		return ebppStatus;
	}
	
	public String getPaymentValidationRange() {
		return paymentValidationRange;
	}

	public void setPaymentValidationRange(String paymentValidationRange) {
		this.paymentValidationRange = paymentValidationRange;
	}

	// MBSFE-691
	public String getInvalidEmailDomains() {
		return invalidEmailDomains;
	}

	public void setInvalidEmailDomains(String invalidEmailDomains) {
		this.invalidEmailDomains = invalidEmailDomains;
	}
	// END MBSFE-691

	public String getNonTransProductFlow() {
		return nonTransProductFlow;
	}

	public void setNonTransProductFlow(String nonTransProductFlow) {
		this.nonTransProductFlow = nonTransProductFlow;
	}
	
}
