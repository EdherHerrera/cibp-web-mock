package evertec.cibp.web.form;

import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerPassword;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.enrollment.EnrollableAccount;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.MessagingService;
import evertec.cibp.core.services.requests.CardCVVAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.CardPINAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.CustomerDOBValidationRequest;
import evertec.cibp.core.services.requests.DepositAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.ModifyBankingEntitlementRequest;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.requests.NonTransAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.PayrollCardAccountInformationValidationRequest;
import evertec.cibp.core.services.responses.PersonalDOBValidationResponse;
import evertec.cibp.core.services.responses.PersonalDOBValidationResponse.ValidationStatus;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse.AccountValidationStatus;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.encryptedPin.PinEncryptionTool;
import evertec.cibp.template.utils.AccountMaskUtil;
import evertec.cibp.web.form.bean.UpdateEnrollAcctBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.utils.ValidationUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class UpdateEnrollAcctForm extends AbstractWizardFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private String cancelView;
	private CustomerServices customerService;
	private PinEncryptionTool pinTool;
	private String nerdsPrefillUrl;
	private String comercialProducts;
	private MessageSource messageSource;
	private MessagingService customerMessagingService;
	private String nonTransEnrollAccs;

	// jocampo FFEIC
	private Integer maxPasswordRecords;
	
	//MBFIS113 - mdias
	private String paymentValidationRange;
	
	private static final String ENROLLMENT_PASSWORD_CONTAINS_USERNAME = "enrollment.pwd.contains.username";
	private static final String PASSWORD_ERROR = "password";
	private static final String PASSWORD_CONTAINS_USERNAME = "Password must not contains username";

	public PinEncryptionTool getPinTool() {
		return pinTool;
	}

	public void setPinTool(PinEncryptionTool pinTool) {
		this.pinTool = pinTool;
	}

	public UpdateEnrollAcctForm() {
		super();
		setCommandClass(UpdateEnrollAcctBean.class);
		setCommandName("updateEnrollAcct");
		setPages(new String[] { "updEnrollAcct", "updEnrollAcct" });

	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		UpdateEnrollAcctBean bean = (UpdateEnrollAcctBean) command;

		UserSession sessionEnrollUpdate = (UserSession) WebUtils.getSessionAttribute(request, "sessionEnrollUpdate");

		if (bean.getAction() != null && sessionEnrollUpdate != null) {
			if (bean.getAction().equals("PASSWORDV")) {
				// FFEIC
				@SuppressWarnings("unchecked")
				List<CustomerPassword> passwords = (List<CustomerPassword>) WebUtils.getSessionAttribute(request,
						"passwords");
				// FIN FFEIC

				CustomerProfile p = sessionEnrollUpdate.getCustomerProfile();

				int newpasswordsalt = PasswordHashUtil.genereateRandomSalt();
				String newpasswordhash = PasswordHashUtil.generateSaltedHash(bean.getPassword(), newpasswordsalt);

				ModifyCustomerRequest modifyCustomerRequest = new ModifyCustomerRequest(
						new CustomerInteractionEvent(request.getRemoteAddr(), request.getRequestedSessionId(), "-", "",
								Utils.getServerHostname()),
						p.getProfileId(), p.getPasswordHash(), p.getPasswordSalt(), p.getUsername(), newpasswordsalt,
						newpasswordhash, p.getStatus(), CustomerFlagType.PASSWORD_BLOCK, null);
				ModifyCustomerProfileResponse modifyCustomerResponse = customerService
						.modifyCustomerPassword(modifyCustomerRequest);
				if (modifyCustomerResponse.getStatus() == ResultStatus.SUCCESS) {
					try {
						// jocampo FFEIC
						CustomerPassword cp = new CustomerPassword(p.getProfileId(), p.getPasswordHash(), new Date(),
								p.getPasswordSalt());
						passwords.add(0, cp);
						List<CustomerPassword> passwordsDelete = null;
						if (passwords.size() > this.maxPasswordRecords) {
							passwordsDelete = passwords.subList(this.maxPasswordRecords, passwords.size());
							passwords = passwords.subList(0, this.maxPasswordRecords);
						}
						this.customerService.getCustomerProfileService().updateCustomerPasswords(passwords,
								passwordsDelete);
						// FIN FFEIC
						ModifyCustomerProfileResponse resetCustomerFlag = customerService
								.resetCustomerFlag(modifyCustomerRequest);
						if (resetCustomerFlag.getStatus() == ResultStatus.SUCCESS) {
							model.put("passwordchangeSuccess", true);
							model.put("username", p.getUsername());
						}
					} catch (Exception e) {
					}
				}
			}
			return showForm(request, errors, "updEnrollAcct", model);
		} else {
			
			EnrollableAccount account = bean.getAccounts()
					.get(Integer.parseInt(bean.getAccountnumber()));
									

			if (!this.updateEnrollAccount(request, userSession, account)) {
				WebUtils.setSessionAttribute(request, "customerEnrollment", null);
				userSession.resetAccounts();
				return new ModelAndView("forward:welcome", model);
			}

			return new ModelAndView("forward:login", model);
		}
	}

	@Override
	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		// If user is non-transactional we save the skip date and increment the skip
		// count.
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA_UPDT_ACCTS)) {
			UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			if (userSession == null)
				throw new ModelAndViewDefiningException(new ModelAndView("redirect:login", "noSessionError", null));

			if (AccountUtils.getIsTransactional(userSession.getCustomerProfile()).equalsIgnoreCase("N")) {
				userSession.getCustomerService().setLastNonTransToTransSkipDate(userSession.getCustomerProfile());
				userSession.getCustomerService().setNonTransToTransSkipCount(userSession.getCustomerProfile());
				userSession.setLoginCompleteSuccess(true);
				return new ModelAndView("forward:portal");
			}
		}
		return new ModelAndView(cancelView);
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		UpdateEnrollAcctBean updEnrollAcctBean = (UpdateEnrollAcctBean) command;

		UserSession sessionEnrollUpdate = (UserSession) WebUtils.getSessionAttribute(request, "sessionEnrollUpdate");

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		if (updEnrollAcctBean.getAction() != null && sessionEnrollUpdate != null) {
			if (updEnrollAcctBean.getAction().equals("PASSWORD")) {
				WebUtils.setSessionAttribute(request, "customerEnrollment", null);
				WebUtils.setSessionAttribute(request, "page", "1");
				errors.rejectValue("accountnumber", "updateEnrollAcct.account.multipleAccounts.error",
						"Please select an account.");
			} else if (updEnrollAcctBean.getAction().equals("PASSWORDV")) {
				String password = updEnrollAcctBean.getPassword();
				String passwordconfirm = updEnrollAcctBean.getPasswordconfirm();

				if (GenericValidator.isBlankOrNull(password)) {
					errors.rejectValue("password", "resetpassword.pwd.required", "Required");
				}
				else if (!(GenericValidator.maxLength(password, 15) && GenericValidator.minLength(password, 8))) {
					errors.rejectValue("password", "resetpassword.pwd.invalid", "Invalid Password");
				}
				else if (!Utils.isValidPassword(password)) {
					errors.rejectValue("password", "resetpassword.pwd.invalid", "Invalid Password");
				}
				else if (!password.equals(passwordconfirm)) {
					errors.rejectValue("password", "resetpassword.pwd.mustmatch", "Password must match");
				}
				else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989)
						&& password.toLowerCase().contains(userSession.getCustomerProfile().getUsername().toLowerCase())) {
					errors.rejectValue(PASSWORD_ERROR, ENROLLMENT_PASSWORD_CONTAINS_USERNAME, PASSWORD_CONTAINS_USERNAME);
				}
				else {
					// FFEIC
					@SuppressWarnings("unchecked")
					List<CustomerPassword> passwords = (List<CustomerPassword>) WebUtils.getSessionAttribute(request,
							"passwords");
					if (passwords == null) {
						passwords = customerService.getCustomerProfileService()
								.getCustomerPasswords(sessionEnrollUpdate.getCustomerProfile());
						WebUtils.setSessionAttribute(request, "passwords", passwords);
					}
					// NO EQUALS ACTUAL PASS
					int originalSalt = sessionEnrollUpdate.getCustomerProfile().getPasswordSalt();
					String originalHash = sessionEnrollUpdate.getCustomerProfile().getPasswordHash();
					String newHash = PasswordHashUtil.generateSaltedHash(password, originalSalt);
					if (newHash.equals(originalHash)) {
						errors.rejectValue("password", "changepassword.password.previously.used", "Incorrect Password");
					}
					// NO EQUALS LAST PASS
					else {
						for (int i = 0; i < passwords.size(); i++) {
							String newPassOldHash = PasswordHashUtil.generateSaltedHash(password,
									passwords.get(i).getPasswordSalt());
							if (passwords.get(i).getPasswordHash().equals(newPassOldHash)) {
								errors.rejectValue("password", "changepassword.password.previously.used",
										"Incorrect Password");
								break;
							}
						}
					}
					// FIN FFEIC
				}
			}
		} // MBFIS-72 / 73 / 74 - Mariela Dias
		else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP) && CustomerUtils.isSecurities(userSession.getCustomerProfile())) {
			if (page == 0) {
				// If user has more than one account we validate the chosen index.
				if (updEnrollAcctBean.getAccounts().size() > 1) {

					try {
						int index = Integer.parseInt(updEnrollAcctBean.getAccountnumber());

						if (index < 0 || index >= updEnrollAcctBean.getAccounts().size()) {
							errors.rejectValue("accountnumber", "updateEnrollAcct.account.multipleAccounts.error",
									"Please select an account.");
						}
					} catch (Exception e) {
						errors.rejectValue("accountnumber", "updateEnrollAcct.account.multipleAccounts.error",
								"Please select an account.");
					}
				}
			} else if (page == 1) {		
				boolean error = false;
				
				
				EnrollableAccount account = updEnrollAcctBean.getAccounts()
						.get(Integer.parseInt(updEnrollAcctBean.getAccountnumber()));
										
				ValidateAccountStatusResponse validateAccountInfo = null;

				String taxId = userSession.getCustomerProfile().getTaxId();

				if (account.getAccountType().equalsIgnoreCase(Utils.IDA)) {

					String athNumber = updEnrollAcctBean.getAthNumber();
					String pin = updEnrollAcctBean.getPin();
					String accountNum = account.getAccountId();
					String agreement = updEnrollAcctBean.getAgreement();
					
					if (!ValidationUtil.validATHNumber(athNumber)) {
						error = true;
						errors.rejectValue("athNumber", "updateEnrollAcct.accountInfo.athnumber.error",
								"The ATH<sup>&reg;</sup> card number must contain 16 digits. No spaces or hyphens.");
					}
					if (!ValidationUtil.validPin(pin)) {
						error = true;
						errors.rejectValue("pin", "updateEnrollAcct.accountInfo.pin.error",
								"updateEnrollAcct.accountInfo.pin.error");
					}
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA577)
							&& !ValidationUtil.validAgreement(agreement)) {
						error = true;
						errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
					}

					if (!error) {
						String encryptedPin = pinTool.encrypt(pin);
						DepositAccountInformationValidationRequest depReq = new DepositAccountInformationValidationRequest(
								null, accountNum, athNumber, taxId, encryptedPin, true, true);
						validateAccountInfo = customerService.validateDepositAccountInfo(depReq);

						if (validateAccountInfo == null || validateAccountInfo.getStatus() == null
								|| validateAccountInfo.getStatus() != AccountValidationStatus.VALID) {
							errors.rejectValue("athNumber", "updateEnrollAcct.accountInfo.generic.error",
									"The information entered does not match our records.  Please try again.");
						}
					}
				} else if (account.getAccountType().equalsIgnoreCase(Utils.CCA)
						|| account.getAccountType().equalsIgnoreCase(Utils.AMEX)) {

					String expMonth = updEnrollAcctBean.getExpMonth();
					String expYear = updEnrollAcctBean.getExpYear();
					String cvv = updEnrollAcctBean.getCvv();
					String zipCode = updEnrollAcctBean.getZipCode();
					String pin = updEnrollAcctBean.getPin();
					String agreement = updEnrollAcctBean.getAgreement();

					if (!ValidationUtil.validExpirationMonth(expMonth)
							|| !ValidationUtil.validExpirationYear(expYear)) {
						error = true;
						errors.rejectValue("expYear", "updateEnrollAcct.accountInfo.expDate.error",
								"Invalid expiration date");
					}

					// CVV field only shown for CCA accounts
					if (account.getAccountType().equalsIgnoreCase(Utils.CCA) && !ValidationUtil.validCVV(cvv)) {
						error = true;
						errors.rejectValue("cvv", "updateEnrollAcct.accountInfo.cvv.error",
								"The card's verification code must contain 3 digits. No spaces or hyphens.");
					}

					// PIN field only shown for AMEX accounts
					if (account.getAccountType().equalsIgnoreCase(Utils.AMEX) && !ValidationUtil.validPin(pin)) {
						error = true;
						errors.rejectValue("pin", "updateEnrollAcct.accountInfo.pin.error",
								"The PIN number must contain 4 digits. No letters, spaces or hyphens.");
					}

					if (!ValidationUtil.validZipCode(zipCode)) {
						error = true;
						errors.rejectValue("zipCode", "updateEnrollAcct.accountInfo.zipCode.error",
								"The billing zip code must contain 5 digits. No spaces or hyphens.");
					}
					
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA577)
							&& !ValidationUtil.validAgreement(agreement)) {
						error = true;
						errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
					}

					if (!error) {
						if (account.getAccountType().equalsIgnoreCase(Utils.CCA)) {
							CardCVVAccountInformationValidationRequest cvvReq = new CardCVVAccountInformationValidationRequest(
									null, account.getAccountId(), userSession.getCustomerProfile().getTaxId(), cvv,
									expMonth, expYear, zipCode, true);
							validateAccountInfo = customerService.validateCrediCardCVVAccountInfo(cvvReq);
						} else {
							String encryptedPin = pinTool.encrypt(pin);
							CardPINAccountInformationValidationRequest cardPinReq = new CardPINAccountInformationValidationRequest(
									null, account.getAccountId(), userSession.getCustomerProfile().getTaxId(),
									encryptedPin, expMonth, expYear, zipCode, true, true, true);
							validateAccountInfo = customerService.validateCrediCardPINAccountInfo(cardPinReq);
						}

						if (validateAccountInfo == null || validateAccountInfo.getStatus() == null
								|| validateAccountInfo.getStatus() != AccountValidationStatus.VALID) {
							errors.rejectValue("expYear", "updateEnrollAcct.accountInfo.generic.error",
									"The information entered does not match our records.  Please try again.");
						}
					}
				} else if (account.getAccountType().equalsIgnoreCase(Utils.PFC)) {
					// ATH number reused from IDA view
					String athNumber = updEnrollAcctBean.getAthNumber();
					String pin = updEnrollAcctBean.getPin();
					String agreement = updEnrollAcctBean.getAgreement();

					if (!ValidationUtil.validATHNumber(athNumber)) {
						error = true;
						errors.rejectValue("athNumber", "updateEnrollAcct.accountInfo.athnumber.error",
								"The ATH<sup>&reg;</sup> card number must contain 16 digits. No spaces or hyphens.");
					}
					if (!ValidationUtil.validPin(pin)) {
						error = true;
						errors.rejectValue("pin", "updateEnrollAcct.accountInfo.pin.error",
								"updateEnrollAcct.accountInfo.pin.error");
					}
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA577)
							&& !ValidationUtil.validAgreement(agreement)) {
						error = true;
						errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
					}

					if (!error) {
						String encryptedPin = pinTool.encrypt(pin);
						PayrollCardAccountInformationValidationRequest payreq = new PayrollCardAccountInformationValidationRequest(
								null, athNumber, taxId, encryptedPin, true);
						validateAccountInfo = customerService.validatePayrollCardAccountInfo(payreq);

						if (validateAccountInfo == null || validateAccountInfo.getStatus() == null
								|| validateAccountInfo.getStatus() != AccountValidationStatus.VALID) {
							errors.rejectValue("athNumber", "updateEnrollAcct.accountInfo.generic.error",
									"The information entered does not match our records.  Please try again.");
						}
					}
				}
				//MBFIS-113	- Mariela Dias			
				else if (account.getAccountType().equalsIgnoreCase(Utils.MLA)) {
					//validate form element with MLA account type
					
					//Regex to validate number or number with comma only
					String valRegExp = "(\\$)?([1-9]{1}[0-9]{0,2})(\\,\\d{3})*$|^(\\$)?([1-9]{1}[0-9]{0,2})(\\d{3})*$";
					
					String loanAccountNumber = updEnrollAcctBean.getLoanAccountNumber();
					Integer accountOpenYear = updEnrollAcctBean.getAccountOpenYear();
					BigDecimal monthlyPayment = updEnrollAcctBean.getMonthlyPayment();
					String agreement = updEnrollAcctBean.getAgreement();
						
					if (GenericValidator.isBlankOrNull(loanAccountNumber) || !GenericValidator.matchRegexp(loanAccountNumber, "\\d{10}"))  {
						errors.rejectValue("loanAccountNumber", "enrollment.accountnumber.invalid", "Invalid account Number.");
						error = true;
					}
					if((accountOpenYear == null || !accountOpenYear.toString().matches("\\d{4}")) && !error) {
						errors.rejectValue("loanAccountNumber", "enrollment.accountnumber.invalid", "Invalid open year.");
						error = true;
					} 
					if((monthlyPayment == null || !monthlyPayment.toPlainString().matches(valRegExp)) && !error) {
							errors.rejectValue("loanAccountNumber", "enrollment.accountnumber.invalid", "Invalid monthly payment.");
							error = true;
					}
					
					if (!ValidationUtil.validAgreement(agreement)) {
						error = true;
						errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
					}

					if (!error) {	
						NonTransAccountInformationValidationRequest ntReq = new NonTransAccountInformationValidationRequest(
								null, loanAccountNumber,taxId,true, accountOpenYear, monthlyPayment,paymentValidationRange, true, account.getAccountType());
						validateAccountInfo = customerService.validateNonTransAccountInfo(ntReq);
						
						if (validateAccountInfo == null || validateAccountInfo.getStatus() == null
								|| validateAccountInfo.getStatus() != AccountValidationStatus.VALID) {
							errors.rejectValue("loanAccountNumber", "updateEnrollAcct.accountInfo.generic.error",
									"The information entered does not match our records.  Please try again.");
						}
					}
				}
				else if (account.getAccountType().equalsIgnoreCase(Utils.RVA) || account.getAccountType().equalsIgnoreCase(Utils.LEA) || account.getAccountType().equalsIgnoreCase("RVA|LEA")) {
				
					String valRegExp = "(\\$)?([1-9]{1}[0-9]{0,2})(\\,\\d{3})*$|^(\\$)?([1-9]{1}[0-9]{0,2})(\\d{3})*$";
					String loanAccountNumber = updEnrollAcctBean.getLoanAccountNumber();
					Integer accountOpenYear = updEnrollAcctBean.getAccountOpenYear();
					BigDecimal monthlyPayment = updEnrollAcctBean.getMonthlyPayment();
					String agreement = updEnrollAcctBean.getAgreement();
					String accountType = account.getAccountType();
					
					if (GenericValidator.isBlankOrNull(loanAccountNumber) || !GenericValidator.matchRegexp(loanAccountNumber, "\\d{17}"))  {
						errors.rejectValue("loanAccountNumber", "enrollment.accountnumber.invalid", "Invalid account Number.");
						error = true;
					}
					if((accountOpenYear == null || !accountOpenYear.toString().matches("\\d{4}")) && !error) {
						errors.rejectValue("loanAccountNumber", "enrollment.accountnumber.invalid", "Invalid open year.");
						error = true;
					} 
					if((monthlyPayment == null || !monthlyPayment.toPlainString().matches(valRegExp)) && !error) {
						errors.rejectValue("loanAccountNumber", "enrollment.accountnumber.invalid", "Invalid monthly payment.");
						error = true;
					}
					if (!ValidationUtil.validAgreement(agreement)) {
						error = true;
						errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
					}
					
					if (!error) {
						NonTransAccountInformationValidationRequest ntReq = new NonTransAccountInformationValidationRequest(
								null, loanAccountNumber,taxId,true, accountOpenYear, monthlyPayment,paymentValidationRange, true, accountType);			
						
						validateAccountInfo = customerService.validateNonTransAccountInfo(ntReq);
						if (validateAccountInfo == null || validateAccountInfo.getStatus() == null
								|| validateAccountInfo.getStatus() != AccountValidationStatus.VALID) {
							errors.rejectValue("loanAccountNumber", "updateEnrollAcct.accountInfo.generic.error",
									"The information entered does not match our records.  Please try again.");
						}
					}
				
				}//END MBFIS-113

				// This means an invalid Account sub-type was selected
				else {
					throw new ModelAndViewDefiningException(
							new ModelAndView("redirect:login", "InvalidAccountError", null));
				}
			}
		}
		//MBCA Flow
		else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA_UPDT_ACCTS)) {

			if (page == 0) {
				// If user has more than one account we validate the chosen index.
				if (updEnrollAcctBean.getAccounts().size() > 1) {

					try {
						int index = Integer.parseInt(updEnrollAcctBean.getAccountnumber());

						if (index < 0 || index >= updEnrollAcctBean.getAccounts().size()) {
							errors.rejectValue("accountnumber", "updateEnrollAcct.account.multipleAccounts.error",
									"Please select an account.");
						}
					} catch (Exception e) {
						errors.rejectValue("accountnumber", "updateEnrollAcct.account.multipleAccounts.error",
								"Please select an account.");
					}
				}
			} else if (page == 1) {

				EnrollableAccount account = updEnrollAcctBean.getAccounts()
						.get(Integer.parseInt(updEnrollAcctBean.getAccountnumber()));

				boolean error = false;
				ValidateAccountStatusResponse validateAccountInfo = null;
				// MBCA-27
				String taxId = userSession.getCustomerProfile().getTaxId();

				if (account.getAccountType().equalsIgnoreCase(Utils.IDA)) {

					String athNumber = updEnrollAcctBean.getAthNumber();
					String pin = updEnrollAcctBean.getPin();
					String accountNum = account.getAccountId();
					String agreement = updEnrollAcctBean.getAgreement();
					if (!ValidationUtil.validATHNumber(athNumber)) {
						error = true;
						errors.rejectValue("athNumber", "updateEnrollAcct.accountInfo.athnumber.error",
								"The ATH<sup>&reg;</sup> card number must contain 16 digits. No spaces or hyphens.");
					}
					if (!ValidationUtil.validPin(pin)) {
						error = true;
						errors.rejectValue("pin", "updateEnrollAcct.accountInfo.pin.error",
								"updateEnrollAcct.accountInfo.pin.error");
					}

					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA577)
							&& !ValidationUtil.validAgreement(agreement)) {
						error = true;
						errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
					}

					if (!error) {
						String encryptedPin = pinTool.encrypt(pin);
						DepositAccountInformationValidationRequest depReq = new DepositAccountInformationValidationRequest(
								null, accountNum, athNumber, taxId, encryptedPin, true, true);
						validateAccountInfo = customerService.validateDepositAccountInfo(depReq);

						if (validateAccountInfo == null || validateAccountInfo.getStatus() == null
								|| validateAccountInfo.getStatus() != AccountValidationStatus.VALID) {
							errors.rejectValue("athNumber", "updateEnrollAcct.accountInfo.generic.error",
									"The information entered does not match our records.  Please try again.");
						}
					}
				} else if (account.getAccountType().equalsIgnoreCase(Utils.CCA)
						|| account.getAccountType().equalsIgnoreCase(Utils.AMEX)) {

					String expMonth = updEnrollAcctBean.getExpMonth();
					String expYear = updEnrollAcctBean.getExpYear();
					String cvv = updEnrollAcctBean.getCvv();
					String zipCode = updEnrollAcctBean.getZipCode();
					String pin = updEnrollAcctBean.getPin();
					String agreement = updEnrollAcctBean.getAgreement();

					if (!ValidationUtil.validExpirationMonth(expMonth)
							|| !ValidationUtil.validExpirationYear(expYear)) {
						error = true;
						errors.rejectValue("expYear", "updateEnrollAcct.accountInfo.expDate.error",
								"Invalid expiration date");
					}

					// CVV field only shown for CCA accounts
					if (account.getAccountType().equalsIgnoreCase(Utils.CCA) && !ValidationUtil.validCVV(cvv)) {
						error = true;
						errors.rejectValue("cvv", "updateEnrollAcct.accountInfo.cvv.error",
								"The card's verification code must contain 3 digits. No spaces or hyphens.");
					}

					// PIN field only shown for AMEX accounts
					if (account.getAccountType().equalsIgnoreCase(Utils.AMEX) && !ValidationUtil.validPin(pin)) {
						error = true;
						errors.rejectValue("pin", "updateEnrollAcct.accountInfo.pin.error",
								"The PIN number must contain 4 digits. No letters, spaces or hyphens.");
					}

					if (!ValidationUtil.validZipCode(zipCode)) {
						error = true;
						errors.rejectValue("zipCode", "updateEnrollAcct.accountInfo.zipCode.error",
								"The billing zip code must contain 5 digits. No spaces or hyphens.");
					}

					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA577)
							&& !ValidationUtil.validAgreement(agreement)) {
						error = true;
						errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
					}

					if (!error) {
						if (account.getAccountType().equalsIgnoreCase(Utils.CCA)) {
							CardCVVAccountInformationValidationRequest cvvReq = new CardCVVAccountInformationValidationRequest(
									null, account.getAccountId(), userSession.getCustomerProfile().getTaxId(), cvv,
									expMonth, expYear, zipCode, true);
							validateAccountInfo = customerService.validateCrediCardCVVAccountInfo(cvvReq);
						} else {
							String encryptedPin = pinTool.encrypt(pin);
							CardPINAccountInformationValidationRequest cardPinReq = new CardPINAccountInformationValidationRequest(
									null, account.getAccountId(), userSession.getCustomerProfile().getTaxId(),
									encryptedPin, expMonth, expYear, zipCode, true, true, true);
							validateAccountInfo = customerService.validateCrediCardPINAccountInfo(cardPinReq);
						}

						if (validateAccountInfo == null || validateAccountInfo.getStatus() == null
								|| validateAccountInfo.getStatus() != AccountValidationStatus.VALID) {
							errors.rejectValue("expYear", "updateEnrollAcct.accountInfo.generic.error",
									"The information entered does not match our records.  Please try again.");
						}
					}
				} else if (account.getAccountType().equalsIgnoreCase(Utils.PFC)) {
					// ATH number reused from IDA view
					String athNumber = updEnrollAcctBean.getAthNumber();
					String pin = updEnrollAcctBean.getPin();
					String agreement = updEnrollAcctBean.getAgreement();

					if (!ValidationUtil.validATHNumber(athNumber)) {
						error = true;
						errors.rejectValue("athNumber", "updateEnrollAcct.accountInfo.athnumber.error",
								"The ATH<sup>&reg;</sup> card number must contain 16 digits. No spaces or hyphens.");
					}
					if (!ValidationUtil.validPin(pin)) {
						error = true;
						errors.rejectValue("pin", "updateEnrollAcct.accountInfo.pin.error",
								"updateEnrollAcct.accountInfo.pin.error");
					}
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA577)
							&& !ValidationUtil.validAgreement(agreement)) {
						error = true;
						errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
					}

					if (!error) {
						String encryptedPin = pinTool.encrypt(pin);
						PayrollCardAccountInformationValidationRequest payreq = new PayrollCardAccountInformationValidationRequest(
								null, athNumber, taxId, encryptedPin, true);
						validateAccountInfo = customerService.validatePayrollCardAccountInfo(payreq);

						if (validateAccountInfo == null || validateAccountInfo.getStatus() == null
								|| validateAccountInfo.getStatus() != AccountValidationStatus.VALID) {
							errors.rejectValue("athNumber", "updateEnrollAcct.accountInfo.generic.error",
									"The information entered does not match our records.  Please try again.");
						}
					}
				}

				// This means an invalid Account sub-type was selected
				else {
					throw new ModelAndViewDefiningException(
							new ModelAndView("redirect:login", "InvalidAccountError", null));
				}
			}
		}
		else {

			// Old Flag Code
			if (updEnrollAcctBean.getAccountnumber().equals("-1")) {
				errors.rejectValue("accountnumber", "updateEnrollAcct.account.multipleAccounts.error",
						"Please select an account.");
			} else {

				// Object account = request.getParameter("accountnumber");
				// updEnrollAcctBean.getAccounts();
				// updEnrollAcctBean.getAccountnumber();
				EnrollableAccount account = updEnrollAcctBean.getAccounts()
						.get(Integer.parseInt(updEnrollAcctBean.getAccountnumber()));
				if (sessionEnrollUpdate != null) {
					if (!this.updateEnrollAccount(request, sessionEnrollUpdate, account)) {
						WebUtils.setSessionAttribute(request, "customerEnrollment", null);
						WebUtils.setSessionAttribute(request, "page", "1");
						errors.rejectValue("accountnumber", "updateEnrollAcct.account.multipleAccounts.error",
								"Please select an account.");
						customerService.customerResetPasswordSuccess(sessionEnrollUpdate.getCustomerProfile(), null,
								Utils.generateCustomerInteractionEvent(request));
					}
				}
			}
		}
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		model.put("page", page);
		model.put("openaccounturl", this.nerdsPrefillUrl);

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		model.put("callFrom", "update");

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA577)) {
			Map<String, String> agree = new HashMap<String, String>();
			agree.put("agree", "");
			model.put("agree", agree);
			model.put("MBCA577", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA577));
		}

		UserSession sessionEnrollUpdate = (UserSession) WebUtils.getSessionAttribute(request, "sessionEnrollUpdate");
		if (userSession == null) {
			userSession = sessionEnrollUpdate;
			model.put("callFrom", "reset");
		}

		boolean updateAccount = false;
		updateAccount = WebUtils.getSessionAttribute(request, "updateAccount") != null ? true : false;

		String pageSession = (String) WebUtils.getSessionAttribute(request, "page");
		if (pageSession != null && sessionEnrollUpdate != null) {
			model.put("page", pageSession);
			model.put("username", sessionEnrollUpdate.getCustomerProfile().getUsername());
			model.put("passwordchangeSuccess", false);

			return model;
		}

		if (userSession == null) {
			throw new ModelAndViewDefiningException(new ModelAndView("redirect:login", "noSessionError", null));
		}

		if (userSession.getCustomerProfile().isIfxError() == true){
			model.put("ifxError", true);
			return  model;
		}

		// Avoids a nullPointer when the user enters from regain access
		userSession.setNonTransEnrollAccsSet(nonTransEnrollAccs);

		UpdateEnrollAcctBean bean = (UpdateEnrollAcctBean) command;

		PersonalInformationValidationResponse customerEnrollment = (PersonalInformationValidationResponse) WebUtils
				.getSessionAttribute(request, "customerEnrollment");
		GregorianCalendar birth = new GregorianCalendar();
		birth.setTime(userSession.getCustomerProfile().getBirthDate());

		CustomerInteractionEvent generateCustomerInteractionEvent = Utils.generateCustomerInteractionEvent(request,
				userSession.getCustomerProfile().getUsername());

		CustomerDOBValidationRequest req = new CustomerDOBValidationRequest(generateCustomerInteractionEvent,
				userSession.getCustomerProfile().getPermId(), birth);

		PersonalDOBValidationResponse res = customerService.validateCustomerDOB(req);
		String loginLoop = "F";
		if (customerEnrollment == null || userSession == null || !customerEnrollment.getStatus()
				.equals(PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE)) {
			/* Start Incident 2712087 Login Loop */
			String statusError = "";
			String comUser = "F";
			StringBuilder error = new StringBuilder();
			if ((res.getStatus() == ValidationStatus.DOB_BLOCKED) || (res.getStatus() == ValidationStatus.DOB_INVALID)
					|| (customerEnrollment.getStatus().equals(
							PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE_DOB_INVALID))
					|| (customerEnrollment.getStatus().equals(
							PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE_DOB_BLOCKED))) {

				if (res.getStatus() == ValidationStatus.DOB_BLOCKED) {
					statusError = res.getStatus().toString();
					if (userSession.getCustomerProfile().getBirthDate() != null) {
						error.append(statusError).append(": CODE: ").append(res.getErrorCode()).append("-")
								.append("Day Of Birth Blocked:")
								.append((userSession.getCustomerProfile().getBirthDate().toString()).substring(0, 10));
					} else {
						error.append(statusError).append(": CODE: ").append(res.getErrorCode()).append("-")
								.append("Day Of Birth Blocked: Null");
					}

				}
				if (res.getStatus() == ValidationStatus.DOB_INVALID) {
					statusError = res.getStatus().toString();
					if (userSession.getCustomerProfile().getBirthDate() != null) {
						error.append(statusError).append(": CODE: ").append(res.getErrorCode()).append("-")
								.append("Day Of Birth Invalid:")
								.append((userSession.getCustomerProfile().getBirthDate().toString()).substring(0, 10));
					} else {
						error.append(statusError).append(": CODE: ").append(res.getErrorCode()).append("-")
								.append("Day Of Birth Invalid: Null");
					}
				}
				if (customerEnrollment.getStatus().equals(
						PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE_DOB_INVALID)) {
					statusError = customerEnrollment.getStatus().toString();
					if (userSession.getCustomerProfile().getBirthDate() != null) {
						error.append(statusError).append(": CODE: ").append(customerEnrollment.getErrorCode())
								.append("-").append("Enrollable Day Of Birth Invalid:")
								.append((userSession.getCustomerProfile().getBirthDate().toString()).substring(0, 10));
					} else {
						error.append(statusError).append(": CODE: ").append(customerEnrollment.getErrorCode())
								.append("-").append("Enrollable Day Of Birth Invalid:: Null");
					}
				} else {
					statusError = customerEnrollment.getStatus().toString();
					if (userSession.getCustomerProfile().getBirthDate() != null) {
						error.append(statusError).append(": CODE: ").append(res.getErrorCode()).append("-")
								.append("Enrollable Day Of Birth Blocked:")
								.append((userSession.getCustomerProfile().getBirthDate().toString()).substring(0, 10));
					} else {
						error.append(statusError).append(": CODE: ").append(customerEnrollment.getErrorCode())
								.append("-").append("Enrollable Day Of Birth Blocked:: Null");
					}
				}
				if (userSession.getCustomerProfile().useCommercialInterface()) {
					comUser = "T";
				}
				loginLoop = "T";
				model.put("loginLoop", loginLoop);
				ModelAndView modelAndView = new ModelAndView("redirect:login");
				modelAndView.addObject("LoginErrorResponse", loginLoop);
				modelAndView.addObject("user", userSession.getCustomerProfile().getUsername());
				modelAndView.addObject("comUser", comUser);
				generateCustomerInteractionEvent.setCustomStringData(
						CustomerInteractionEventType.ERROR_ENROLL_ACCT.toString(), error.toString());
				userSession.getCustomerService().logErrorEnrollmentAccount(generateCustomerInteractionEvent,
						InteractionSeverity.ERROR);
				throw new ModelAndViewDefiningException(modelAndView);
			}
			/* End Incident 2712087 Login Loop */
			throw new ModelAndViewDefiningException(new ModelAndView("redirect:login", "noSessionError", null));
		} else {

			bean.setAccounts(customerEnrollment.getEnrollableAccounts());

			// REMOVING ACCOUNTS THAT APPEARS TWICE (CHK, SAV, RES)
			List<String> enrollableAccts = new ArrayList<String>();
			for (int i = 0; i < customerEnrollment.getEnrollableAccounts().size(); i++) {
				if (enrollableAccts.contains(customerEnrollment.getEnrollableAccounts().get(i).getAccountId())) {
					customerEnrollment.getEnrollableAccounts().remove(i);
					i--;
				} else {
					enrollableAccts.add(customerEnrollment.getEnrollableAccounts().get(i).getAccountId());
				}
			}

			boolean comercial = false;
			boolean already = false;
			boolean noAccount = false;
			boolean update = false;
			boolean success = false;

			String oldAccount = userSession.getCustomerProfile().getEnrollmentAccountNumber()
					.substring(userSession.getCustomerProfile().getEnrollmentAccountNumber().length() - 4);
			model.put("oldAccount", "x" + oldAccount);

			if (userSession.getCustomerProfile().useCommercialInterface()) {
				comercial = true;
			}
			if (comercial) {
				// REMOVE ALL NON COMERCIAL PRODUCTS
				for (int i = 0; i < customerEnrollment.getEnrollableAccounts().size(); i++) {
					if (this.comercialProducts
							.indexOf(customerEnrollment.getEnrollableAccounts().get(i).getProductId()) == -1
							&& !"086".equalsIgnoreCase(
									customerEnrollment.getEnrollableAccounts().get(i).getProductId())) {
						customerEnrollment.getEnrollableAccounts().remove(i);
						i--;
					}
				}

				// REMOVE BSMART ACCOUNTS FROM EXISTING ACCOUNTS BECAUSE IT'S ALREADY ENROLLED
				for (int i = 0; i < customerEnrollment.getExistingAccounts().size(); i++) {
					if ("086".equalsIgnoreCase(customerEnrollment.getExistingAccounts().get(i).getProductId())) {
						customerEnrollment.getExistingAccounts().remove(i);
						i--;
					}
				}

			} else {
				// REMOVE COMERCIAL PRODUCTS
				for (int i = 0; i < customerEnrollment.getEnrollableAccounts().size(); i++) {
					if (this.comercialProducts
							.indexOf(customerEnrollment.getEnrollableAccounts().get(i).getProductId()) != -1) {
						customerEnrollment.getEnrollableAccounts().remove(i);
						i--;
					}
				}
			}

			// REMOVE ALL ALREADY ENROLLED ACCOUNTS
			for (int i = 0; i < customerEnrollment.getExistingAccounts().size(); i++) {
				for (int j = 0; j < customerEnrollment.getEnrollableAccounts().size(); j++) {
					if (customerEnrollment.getExistingAccounts().get(i).getAccountId()
							.equalsIgnoreCase(customerEnrollment.getEnrollableAccounts().get(j).getAccountId())) {
						customerEnrollment.getEnrollableAccounts().remove(j);
						j--;
						already = true;
					}
				}
			}
			// MBFIS-72 / 73 / 74
			// If the customer has other non transactional accounts available/open, the non
			// transactional accounts should not be presented in the dropdown field.
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP) && CustomerUtils.isSecurities(userSession.getCustomerProfile())) {
				// Remove Non-Transactional Accounts if user has Enrollable Transactional
				// Accounts
				for (int i = 0; i < customerEnrollment.getEnrollableAccounts().size(); i++) {
					if ((Utils.hasActiveEnrollableTransAcc(customerEnrollment) && userSession.getNonTransEnrollAccsSet()
							.contains(customerEnrollment.getEnrollableAccounts().get(i).getAccountType()))
							|| customerEnrollment.getEnrollableAccounts().get(i).getStatus().equalsIgnoreCase("I")) {
						customerEnrollment.getEnrollableAccounts().remove(i);
						i--;
					}
				}
			}else {
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA_UPDT_ACCTS)) {
					// Remove Non-Transactional Accounts if user has Enrollable Transactional
					// Accounts
					for (int i = 0; i < customerEnrollment.getEnrollableAccounts().size(); i++) {
						if ((Utils.hasActiveEnrollableTransAcc(customerEnrollment) && userSession.getNonTransEnrollAccsSet()
								.contains(customerEnrollment.getEnrollableAccounts().get(i).getAccountType()))
								|| customerEnrollment.getEnrollableAccounts().get(i).getStatus().equalsIgnoreCase("I")) {
							customerEnrollment.getEnrollableAccounts().remove(i);
							i--;
						}
					}
				} else {
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA31)) {
						// Remove all non transactional enrollable accounts: Flag is off
						for (int i = 0; i < customerEnrollment.getEnrollableAccounts().size(); i++) {
							if (userSession.getNonTransEnrollAccsSet()
									.contains(customerEnrollment.getEnrollableAccounts().get(i).getAccountType())) {
								customerEnrollment.getEnrollableAccounts().remove(i);
								i--;
							}
						}
					}
				}
			}

			// CUSTOMER DONT HAVE ANY OTHER ENROLLABLE ACCOUNT
			if (customerEnrollment.getEnrollableAccounts().isEmpty()) {
				WebUtils.setSessionAttribute(request, "userToDelete", userSession.getCustomerProfile().getUsername());
				noAccount = true;
				if (!StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
					// Dont remove session if ebills proyect is enable
					WebUtils.setSessionAttribute(request, "customerEnrollment", null);
					WebUtils.setSessionAttribute(request, "userSession", null);
					WebUtils.setSessionAttribute(request, "sessionEnrollUpdate", null);
				}
				if (comercial) {
					CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request,
							userSession.getCustomerProfile().getUsername());
					ModifyCustomerRequest modifyCustomerRequest = new ModifyCustomerRequest();
					modifyCustomerRequest.setCustomerStatus(CustomerStatus.DISABLED);
					modifyCustomerRequest.setProfileId(userSession.getCustomerProfile().getProfileId());
					modifyCustomerRequest.setInteractionEvent(interactionEvent);
					this.customerService.modifyCustomerStatus(modifyCustomerRequest);
					this.customerService.logUpdateEnrollAcctDisabledInteraction(interactionEvent);
				}
			}
			// Has An Enrollable Account
			else {
				//Validate if it is a Securities client
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP) && CustomerUtils.isSecurities(userSession.getCustomerProfile()) ) {
					boolean mustValidateAccount = false;
					if (Utils.hasActiveEnrollableTransAcc(customerEnrollment) && AccountUtils
							.getIsTransactional(userSession.getCustomerProfile()).equalsIgnoreCase("N")) {
						model.put("nonTransToTrans", true);
						mustValidateAccount = true;
					} else if (!Utils.hasActiveEnrollableTransAcc(customerEnrollment) && AccountUtils
							.getIsTransactional(userSession.getCustomerProfile()).equalsIgnoreCase("N")) {
						model.put("nonTransToNonTransSec", true);
						mustValidateAccount = true;
					} else if (!Utils.hasActiveEnrollableTransAcc(customerEnrollment) && AccountUtils
							.getIsTransactional(userSession.getCustomerProfile()).equalsIgnoreCase("Y")) {
						model.put("transToNonTrans", true);
						//mustValidateAccount = true;
					}

					if (page == 0) {

						if (customerEnrollment.getEnrollableAccounts().size() == 1 && !comercial && !updateAccount) {

							model.put(
									"newAccount", "x"
											+ customerEnrollment.getEnrollableAccounts().get(0).getAccountId()
													.substring(customerEnrollment.getEnrollableAccounts().get(0)
															.getAccountId().length() - 4)
											+ (mustValidateAccount ? "" : "."));

							if (mustValidateAccount) {
								// Don't Auto Enroll, account must be validated first.
								bean.setAccountnumber("0");
								update = true;
								model.put("oneAccount", true);
							} else {
								if (!this.updateEnrollAccount(request, userSession,
										customerEnrollment.getEnrollableAccounts().get(0))) {
									success = true;
									WebUtils.setSessionAttribute(request, "customerEnrollment", null);
									userSession.resetAccounts();

								}
								if (sessionEnrollUpdate != null) {
									customerService.customerResetPasswordSuccess(
											sessionEnrollUpdate.getCustomerProfile(), null,
											Utils.generateCustomerInteractionEvent(request));
								}
							}
						} else {
							update = true;
							Map<String, String> accountsMap = new LinkedHashMap<String, String>();
							AccountMaskUtil mask = new AccountMaskUtil();
							if (!updateAccount) {
								if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
									accountsMap.put("-1", "Seleccione una cuenta");
								else
									accountsMap.put("-1", "Select an account");
								for (int i = 0; i < customerEnrollment.getEnrollableAccounts().size(); i++) {
									String key = mask
											.mask(customerEnrollment.getEnrollableAccounts().get(i).getAccountId());
									accountsMap.put(String.valueOf(i), key);
								}
							}

							model.put("oneAccount", false);
							model.put("accounts", accountsMap);
						}
					} else if (page == 1) {

						boolean hasIDA = false;
						boolean hasCCA = false;
						boolean hasAMEX = false;
						boolean hasPFC = false;
						boolean hasMLA = false;
						boolean hasRVA_LEA = false;

											
						EnrollableAccount account = bean.getAccounts().get(Integer.parseInt(bean.getAccountnumber()));
					
						model.put("newAccount",
								"x" + account.getAccountId().substring(account.getAccountId().length() - 4));

						if (errors.hasErrors()) {
							model.put("errorValidation", true);
						}

						if (account.getAccountType().equalsIgnoreCase(Utils.IDA)) {
							hasIDA = true;
						} else if ((account.getAccountType().equalsIgnoreCase(Utils.CCA)
								|| account.getAccountType().equalsIgnoreCase(Utils.AMEX))) {
							// expMonths for card
							Map<Object, String> months = new LinkedHashMap<>();
							SimpleDateFormat df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
							Calendar cal = GregorianCalendar.getInstance();
							cal.set(Calendar.MONTH, 0);

							if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
								months.put("-1", "Mes:");
							else
								months.put("-1", "Month:");

							for (int i = 1; i <= 12; i++) {
								String month = df.format(cal.getTime());
								months.put(i, month);
								cal.add(Calendar.MONTH, 1);
							}
							model.put("expMonths", months);

							// expYears for card
							Map<Object, String> expYears = new LinkedHashMap<>();

							if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
								expYears.put("-1", "A&ntilde;o:");
							else
								expYears.put("-1", "Year:");

							df = new SimpleDateFormat("yyyy");
							cal = GregorianCalendar.getInstance();
							for (int i = 1; i <= 9; i++) {
								String year = df.format(cal.getTime());
								expYears.put(Integer.parseInt(year), year);
								cal.add(Calendar.YEAR, 1);
							}

							model.put("expYears", expYears);

							if (account.getAccountType().equalsIgnoreCase(Utils.CCA)) {
								hasCCA = true;
							} else {
								hasAMEX = true;
							}
						} else if (account.getAccountType().equalsIgnoreCase(Utils.PFC)) {
							hasPFC = true;
						}else if (account.getAccountType().equalsIgnoreCase(Utils.MLA)) {
							hasMLA = true;
						}else if (account.getAccountType().equalsIgnoreCase(Utils.RVA) || account.getAccountType().equalsIgnoreCase(Utils.LEA) || account.getAccountType().equalsIgnoreCase("RVA||LVA")) {
							hasRVA_LEA = true;
						}

						model.put("hasIDA", hasIDA);
						model.put("hasCCA", hasCCA);
						model.put("hasAMEX", hasAMEX);
						model.put("hasPFC", hasPFC);
						model.put("hasMLA",hasMLA);
						model.put("hasRVA_LEA",hasRVA_LEA);
					}
				} //END MBFIS-72 / 73 / 74
				else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA_UPDT_ACCTS)) {
					boolean mustValidateAccount = false;
					if (Utils.hasActiveEnrollableTransAcc(customerEnrollment) && AccountUtils
							.getIsTransactional(userSession.getCustomerProfile()).equalsIgnoreCase("N")) {
						model.put("nonTransToTrans", true);
						mustValidateAccount = true;
					} else if (!Utils.hasActiveEnrollableTransAcc(customerEnrollment) && AccountUtils
							.getIsTransactional(userSession.getCustomerProfile()).equalsIgnoreCase("N")) {
						model.put("nonTransToNonTrans", true);
					} else if (!Utils.hasActiveEnrollableTransAcc(customerEnrollment) && AccountUtils
							.getIsTransactional(userSession.getCustomerProfile()).equalsIgnoreCase("Y")) {
						model.put("transToNonTrans", true);
					}

					if (page == 0) {

						if (customerEnrollment.getEnrollableAccounts().size() == 1 && !comercial && !updateAccount) {

							model.put(
									"newAccount", "x"
											+ customerEnrollment.getEnrollableAccounts().get(0).getAccountId()
													.substring(customerEnrollment.getEnrollableAccounts().get(0)
															.getAccountId().length() - 4)
											+ (mustValidateAccount ? "" : "."));

							if (mustValidateAccount) {
								// Don't Auto Enroll, account must be validated first.
								bean.setAccountnumber("0");
								update = true;
								model.put("oneAccount", true);
							} else {
								if (!this.updateEnrollAccount(request, userSession,
										customerEnrollment.getEnrollableAccounts().get(0))) {
									success = true;
									WebUtils.setSessionAttribute(request, "customerEnrollment", null);
									userSession.resetAccounts();

								}
								if (sessionEnrollUpdate != null) {
									customerService.customerResetPasswordSuccess(
											sessionEnrollUpdate.getCustomerProfile(), null,
											Utils.generateCustomerInteractionEvent(request));
								}
							}
						} else {
							update = true;
							Map<String, String> accountsMap = new LinkedHashMap<String, String>();
							AccountMaskUtil mask = new AccountMaskUtil();
							if (!updateAccount) {
								if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
									accountsMap.put("-1", "Seleccione una cuenta");
								else
									accountsMap.put("-1", "Select an account");
								for (int i = 0; i < customerEnrollment.getEnrollableAccounts().size(); i++) {
									String key = mask
											.mask(customerEnrollment.getEnrollableAccounts().get(i).getAccountId());
									accountsMap.put(String.valueOf(i), key);
								}
							}

							model.put("oneAccount", false);
							model.put("accounts", accountsMap);
						}
					} else if (page == 1) {

						boolean hasIDA = false;
						boolean hasCCA = false;
						boolean hasAMEX = false;
						boolean hasPFC = false;

						EnrollableAccount account = bean.getAccounts().get(Integer.parseInt(bean.getAccountnumber()));

						model.put("newAccount",
								"x" + account.getAccountId().substring(account.getAccountId().length() - 4));

						if (errors.hasErrors()) {
							model.put("errorValidation", true);
						}

						if (account.getAccountType().equalsIgnoreCase(Utils.IDA)) {
							hasIDA = true;
						} else if ((account.getAccountType().equalsIgnoreCase(Utils.CCA)
								|| account.getAccountType().equalsIgnoreCase(Utils.AMEX))) {
							// expMonths for card
							Map<Object, String> months = new LinkedHashMap<>();
							SimpleDateFormat df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
							Calendar cal = GregorianCalendar.getInstance();
							cal.set(Calendar.MONTH, 0);

							if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
								months.put("-1", "Mes:");
							else
								months.put("-1", "Month:");

							for (int i = 1; i <= 12; i++) {
								String month = df.format(cal.getTime());
								months.put(i, month);
								cal.add(Calendar.MONTH, 1);
							}
							model.put("expMonths", months);

							// expYears for card
							Map<Object, String> expYears = new LinkedHashMap<>();

							if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
								expYears.put("-1", "A&ntilde;o:");
							else
								expYears.put("-1", "Year:");

							df = new SimpleDateFormat("yyyy");
							cal = GregorianCalendar.getInstance();
							for (int i = 1; i <= 9; i++) {
								String year = df.format(cal.getTime());
								expYears.put(Integer.parseInt(year), year);
								cal.add(Calendar.YEAR, 1);
							}

							model.put("expYears", expYears);

							if (account.getAccountType().equalsIgnoreCase(Utils.CCA)) {
								hasCCA = true;
							} else {
								hasAMEX = true;
							}
						} else if (account.getAccountType().equalsIgnoreCase(Utils.PFC)) {
							hasPFC = true;
						}

						model.put("hasIDA", hasIDA);
						model.put("hasCCA", hasCCA);
						model.put("hasAMEX", hasAMEX);
						model.put("hasPFC", hasPFC);
					}
				}
				else {
					// Flag Old Code
					if (customerEnrollment.getEnrollableAccounts().size() == 1 && !comercial && !updateAccount) {
						if (!this.updateEnrollAccount(request, userSession,
								customerEnrollment.getEnrollableAccounts().get(0))) {
							model.put("newAccount",
									"x" + customerEnrollment.getEnrollableAccounts().get(0).getAccountId().substring(
											customerEnrollment.getEnrollableAccounts().get(0).getAccountId().length()
													- 4)
											+ ".");
							success = true;
							WebUtils.setSessionAttribute(request, "customerEnrollment", null);
							userSession.resetAccounts();
						}
						if (sessionEnrollUpdate != null) {
							customerService.customerResetPasswordSuccess(sessionEnrollUpdate.getCustomerProfile(), null,
									Utils.generateCustomerInteractionEvent(request));
						}
					} else {
						update = true;
						Map<String, String> accountsMap = new LinkedHashMap<>();
						AccountMaskUtil mask = new AccountMaskUtil();
						if (!updateAccount) {
							if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
								accountsMap.put("-1", "Seleccione una cuenta");
							else
								accountsMap.put("-1", "Select an account");

							for (int i = 0; i < customerEnrollment.getEnrollableAccounts().size(); i++) {
								String key = mask
										.mask(customerEnrollment.getEnrollableAccounts().get(i).getAccountId());
								accountsMap.put(String.valueOf(i), key);
							}
						}

						model.put("accounts", accountsMap);
					}
				}
			}

			model.put("success", success);
			model.put("comercial", comercial);
			model.put("already", already);
			model.put("noAccount", noAccount);
			model.put("update", update);
		}

		model.put("loginLoop", loginLoop);
		return model;
	}

	private boolean updateEnrollAccount(HttpServletRequest request, UserSession userSession,
			EnrollableAccount account) {
		boolean updateError = false;
		boolean isDowngrading = false;
		boolean isUpgrading = false;
		String isTransactional = null;
		String isSecurities = null;
		int step = 0;

		CustomerProfile p = userSession.getCustomerProfile();
		CustomerProfile originalProfile = new CustomerProfile();
		originalProfile.setProfileId(p.getProfileId());
		originalProfile.setEnrollmentAccountNumber(p.getEnrollmentAccountNumber());
		originalProfile.setTaxId(p.getTaxId());
		originalProfile.setEnrollmentAccountSubtype(p.getEnrollmentAccountSubtype());
		originalProfile.setEnrollmentAccountProductId(p.getEnrollmentAccountProductId());
		originalProfile.setIsTransactional(AccountUtils.getIsTransactional(p));
		originalProfile.setIsSecurities(p.getIsSecurities());

		String originalIsTransactional = AccountUtils.getIsTransactional(p);

		CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request, p.getUsername());

		// backup info
		BankingEntitlement origBankEntitlement = new BankingEntitlement(
				p.getEntitlementByType(EntitlementType.BANKING));
		// EbppEntitlement origEbillEntitlement = new
		// EbppEntitlement(p.getEntitlementByType(EntitlementType.EBILLS));
		ModifyCustomerRequest origCustomerRequest = new ModifyCustomerRequest(interactionEvent, p.getProfileId(),
				p.getEnrollmentAccountNumber(), p.getEnrollmentAccountSubtype(), p.getEnrollmentAccountProductId(), originalProfile.getIsTransactional(), originalProfile.getIsSecurities());

		try {
			// update banking entitlement
			step = 2;
			ModifyBankingEntitlementRequest modifyCustomerBankEntitlement = new ModifyBankingEntitlementRequest(
					interactionEvent, p, p.getTaxId(), account.getAccountId(), account.getAccountType());

			CustomerProfile profileNewBank = customerService
					.modifyCustomerBankingEntitlement(modifyCustomerBankEntitlement);
			CustomerEntitlement bankEntitlement = profileNewBank.getEntitlementByType(EntitlementType.BANKING);
			String newBankAcct = bankEntitlement.getString("ACCOUNT_NUMBER");
			String newBankAcctType = bankEntitlement.getString("ACCOUNT_TYPE");

			if (!newBankAcct.equals(account.getAccountId()) || !newBankAcctType.equals(account.getAccountType())) {
				updateError = true;
				customerService.logUpdateBankingEntitlementFail(interactionEvent);
			} else {
				p = profileNewBank;
				customerService.logUpdateBankingEntitlementSuccess(interactionEvent);
			}

			if (!updateError) {
				// update customerprofile
				step = 1;
				// MBFIS-72 / 73 / 74
				// Enrollment Profile Indicator must be updated to Transactional (Y).
				// Set isTransactional indicator if meets the conditions
				// Set isSecurities to N				
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP) && CustomerUtils.isSecurities(p)) {				
					if (AccountUtils.isAccountSubtypeTransactional(account.getAccountType(),
							userSession.getNonTransEnrollAccsSet())
							&& AccountUtils.getIsTransactional(p).equalsIgnoreCase("N")) {
							isUpgrading = true;	
							isTransactional = "Y";
							isSecurities = "N";
					} else if (!AccountUtils.isAccountSubtypeTransactional(account.getAccountType(),
							userSession.getNonTransEnrollAccsSet())
							&& AccountUtils.getIsTransactional(p).equalsIgnoreCase("Y")) {
						isDowngrading = true;
						isTransactional = "N";
						isSecurities = "Y";
					}else if (!AccountUtils.isAccountSubtypeTransactional(account.getAccountType(),
							userSession.getNonTransEnrollAccsSet())
							&& AccountUtils.getIsTransactional(p).equalsIgnoreCase("N")) {
						isUpgrading = true;
						isTransactional = "N";
						isSecurities = "N";
					}
				}
				// Set isTransactional indicator if meets the conditions
				else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA_UPDT_ACCTS)) {
					if (AccountUtils.isAccountSubtypeTransactional(account.getAccountType(),
							userSession.getNonTransEnrollAccsSet())
							&& AccountUtils.getIsTransactional(p).equalsIgnoreCase("N")) {
						isUpgrading = true;
						customerService.setIsTransactional(p, "Y");
					} else if (!AccountUtils.isAccountSubtypeTransactional(account.getAccountType(),
							userSession.getNonTransEnrollAccsSet())
							&& AccountUtils.getIsTransactional(p).equalsIgnoreCase("Y")) {
						isDowngrading = true;
						customerService.setIsTransactional(p, "N");
					}
				}

				ModifyCustomerRequest modifyCustomerRequest = new ModifyCustomerRequest(interactionEvent,
						p.getProfileId(), account.getAccountId(), account.getAccountType(), account.getProductId(), isTransactional, isSecurities);
				modifyCustomerRequest.setOriginalProfile(originalProfile);
				ModifyCustomerProfileResponse modifyCustomerResponse = customerService
						.modifyCustomerEnrollAcct(modifyCustomerRequest);

				p = modifyCustomerResponse.getCustomerProfile();
				if (modifyCustomerResponse.getStatus() != ResultStatus.SUCCESS) {
					updateError = true;
				}

			}

		} catch (Exception e) {
			updateError = true;
		} // if there was an error, revert info
		if (updateError) {
			userSession.getCustomerService().logUpdateEnrollmentAccount(interactionEvent,
					CustomerInteractionEventType.UPDATE_ENROLL_ACCT, InteractionSeverity.ERROR);

			switch (step) {
			case 1:
				customerService.setIsTransactional(p, originalIsTransactional);
				customerService.modifyCustomerEnrollAcct(origCustomerRequest);
			case 2:
				ModifyBankingEntitlementRequest revertCustomerBankEntitlement = new ModifyBankingEntitlementRequest(
						interactionEvent, p, origBankEntitlement.getSsn(), origBankEntitlement.getAccountNumber(),
						origBankEntitlement.getAccountType());
				customerService.modifyCustomerBankingEntitlement(revertCustomerBankEntitlement);
				break;
			}
		} else {
			// SMS PROFILE IS SENT NULL COZ SMS ALERT IS OUT OF SCOPE,
			// EITHER WAY ALERT IS IMPLEMENTED IN THE CORE
			// IF SMS ALERT IS REQUIRED AS NEW REQUIREMENT JUST SEND THE SMS
			// PROFILE LIKE IN ANY OTHER ALERT
			// MBCA121 sent upgrade Account Email from NonTransactional to transactional
			// Account only
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA121) && isUpgrading
					&& originalIsTransactional.equalsIgnoreCase("N")) {
				if (p.getEmail() != null && !p.getEmail().trim().equals(""))
					userSession.getCustomerService().sendEmailUpgradeNontransToTransAccount(p,
							userSession.getPreferredLanguage());
			} else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA122) && isDowngrading) {
				if (p.getEmail() != null && !p.getEmail().trim().equals(""))
					userSession.getCustomerService().sendEmailDowngradeTransToNonTransAccount(p,
							userSession.getPreferredLanguage());
			} else {
				userSession.getCustomerService().sendUpdatedEnrollmentAccountConfirmation(p, null,
						userSession.getPreferredLanguage());
			}

			this.logUpdateEnrollment(interactionEvent, userSession, originalProfile, p);

			userSession.setCustomerProfile(p);
		}
		
		return updateError;
	}

	/**
	 * This function takes care of logging 4 types of customer enrollment updates
	 * including upgrades or downgrades from non-transactional customers or
	 * horizontal customer enrollment updates.
	 * 
	 *
	 * @param interactionEvent:
	 *            recieves the interaction event.
	 * @param userSession:
	 *            recieves the userSession to get customerService
	 * @param originalProfile:
	 *            Recieves a partial original profile object to get the original
	 *            data prior from being updated.
	 * @param updatedProfile:
	 *            The profile with updated enrollment information Story: MBCA-584
	 * @author Michael Ortiz
	 */

	private void logUpdateEnrollment(CustomerInteractionEvent interactionEvent, UserSession userSession,
			CustomerProfile originalProfile, CustomerProfile updatedProfile) {

		try {

			interactionEvent.setCustomStringData("PREVIOUSENROLLMENTACCOUNTNUMBER",
					originalProfile.getEnrollmentAccountNumber());
			interactionEvent.setCustomStringData("PREVIOUSENROLLMENTACCOUNTSUBTYPE",
					originalProfile.getEnrollmentAccountSubtype());
			interactionEvent.setCustomStringData("PREVIOUSENROLLMENTACCOUNTPRODUCTID",
					originalProfile.getEnrollmentAccountProductId());
			interactionEvent.setCustomStringData("NEWENROLLMENTACCOUNTNUMBER",
					updatedProfile.getEnrollmentAccountNumber());
			interactionEvent.setCustomStringData("NEWENROLLMENTACCOUNTSUBTYPE",
					updatedProfile.getEnrollmentAccountSubtype());
			interactionEvent.setCustomStringData("NEWENROLLMENTACCOUNTPRODUCTID",
					updatedProfile.getEnrollmentAccountProductId());

			CustomerInteractionEventType updateType = null;

			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA_ENROLLMENT_LOG) ||StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP)) { 
				if (originalProfile.getIsTransactional().equals("Y")
						&& updatedProfile.getIsTransactional().equals("Y")) {
					updateType = CustomerInteractionEventType.ENROLL_UPDATE_TRANS;
				} else if (originalProfile.getIsTransactional().equals("Y")
						&& updatedProfile.getIsTransactional().equals("N")) {
					updateType = CustomerInteractionEventType.ENROLL_DOWNGRADE;
				} else if (originalProfile.getIsTransactional().equals("N")
						&& updatedProfile.getIsTransactional().equals("Y")) {
					updateType = CustomerInteractionEventType.ENROLL_UPGRADE;
				} else if (originalProfile.getIsTransactional().equals("N")
						&& updatedProfile.getIsTransactional().equals("N")) {
					updateType = CustomerInteractionEventType.ENROLL_UPDATE_NONTRAS;
				}
			} else {
				updateType = CustomerInteractionEventType.UPDATE_ENROLL_ACCT;
			}

			userSession.getCustomerService().logUpdateEnrollmentAccount(interactionEvent, updateType,
					InteractionSeverity.INFO);

		} catch (Exception e) {
			logger.error("Log Update Enrollment Error: " + e);
		}
	}
	
	public String getCancelView() {
		return cancelView;
	}

	public void setCancelView(String cancelView) {
		this.cancelView = cancelView;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public String getNerdsPrefillUrl() {
		return nerdsPrefillUrl;
	}

	public void setNerdsPrefillUrl(String nerdsPrefillUrl) {
		this.nerdsPrefillUrl = nerdsPrefillUrl;
	}

	public String getComercialProducts() {
		return comercialProducts;
	}

	public void setComercialProducts(String comercialProducts) {
		this.comercialProducts = comercialProducts;
	}

	public Integer getMaxPasswordRecords() {
		return maxPasswordRecords;
	}

	public void setMaxPasswordRecords(Integer maxPasswordRecords) {
		this.maxPasswordRecords = maxPasswordRecords;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public MessagingService getCustomerMessagingService() {
		return customerMessagingService;
	}

	public void setCustomerMessagingService(MessagingService customerMessagingService) {
		this.customerMessagingService = customerMessagingService;
	}

	public String getNonTransEnrollAccs() {
		return nonTransEnrollAccs;
	}

	public void setNonTransEnrollAccs(String nonTransEnrollAccs) {
		this.nonTransEnrollAccs = nonTransEnrollAccs;
	}

	public String getPaymentValidationRange() {
		return paymentValidationRange;
	}

	public void setPaymentValidationRange(String paymentValidationRange) {
		this.paymentValidationRange = paymentValidationRange;
	}


	
}
