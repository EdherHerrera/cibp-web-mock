package evertec.cibp.web.form;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.accountopening.AccountApplication;
import evertec.cibp.core.models.accountopening.AccountApplicationStatus;
import evertec.cibp.core.models.accountopening.dci.DCICustomer;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.campaign.CampaignGenericBean;
import evertec.cibp.core.models.customer.CustomerActions;
import evertec.cibp.core.models.customer.CustomerCodes;
import evertec.cibp.core.models.customer.CustomerDevices;
import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.PayrollEntitlement;
import evertec.cibp.core.models.entitlement.RDCEntitlement;
import evertec.cibp.core.models.epayroll.PayrollStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.CompleteLoginProcessRequest;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.requests.PersonalInformationValidationRequest;
import evertec.cibp.core.services.requests.outreach.CISOutreachRequest;
import evertec.cibp.core.services.responses.BankingSessionResponse;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse;
import evertec.cibp.core.services.responses.outreach.CISOutreachResponse;
import evertec.cibp.core.services.responses.outreach.CISOutreachResponse.OutreachValidationStatus;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.facade.VaultAccountsFacade;
import evertec.cibp.web.form.interruptions.OOBInterruptionForm;
import evertec.cibp.web.premia.control.PremiaUtil;
import evertec.cibp.web.request.VaultAccountsRequest;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.session.UserSessionType;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.GoogleAnalyticsTracking;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.utils.requests.GoogleAnalyticsTrackingRequest;

public class SessionInitializationController extends AbstractController {
    /** Logger */
    protected final Log logger = LogFactory.getLog(getClass());

    private String analyticsId;
    private String wealthCodes;
    private String preferredCodes;
    private String iraProductIds;
    private CampaignGenericBean campaignGenericBean;
    private Integer maxStatements;
    private String productComercial;
    private String[] allowedAthCardStatuses;
    private String[] allowedOnOffAthBins;
    private String[] excludeOnOffCcaBins;

    private Integer fpsShowNewBubbleAlertDay;
    private String evendorId;
    private String epayrollId;

    // MBSFE-1610
    private String[] allowCodes;
    private String[] denyCodesRetail;
    private String[] denyBanks;
    private String[] denyAccounts;
    private String[] denyCodesComercial;
    private Integer accountOldMonths;
    private String enrollableAccounts;

    /**
     * CSV of non transactional enrollable accounts.
     * 
     * @author Hector Ortiz ET56802
     */
    private String nonTransEnrollAccs;

    /**
     * Number of days to wait before requesting a user to upgrade an account from
     * non-transactional to transactional.
     * 
     * @author Hector Ortiz ET56802
     */
    private int nonTransUpgrdWaitTime;
    
    public static final String NON_PREMIA_STATE_FLAG = "nonPremiaState";


    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        Map<String, Object> model = new HashMap<String, Object>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerServices customerService = userSession.getCustomerService();
        userSession.setIraProductIds(iraProductIds);
        userSession.setCampaignGenericBean(campaignGenericBean);
        userSession.setMaxStatements(maxStatements);
        userSession.setNonTransEnrollAccsSet(nonTransEnrollAccs);
        userSession.setEpayrollId(epayrollId);
        userSession.setEvendorId(evendorId);
        CustomerProfile profile = userSession.getCustomerProfile();

        /**
         * Set in session class the list of massCompromiseAccounts MBSFE-665
         */
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE665)) {
            userSession.setMassCompromiseFileAccounts(profile.getMassCompromiseFileAccounts());
        }

        if (userSession.isLoginCompleteSuccess())
            return new ModelAndView("forward:portal");

        if (profile == null && userSession.getUsernameReservation() == null) {
            throw new ProgrammingError("Customer profile and username reservation null");
        } else if (profile == null && userSession.getUsernameReservation() != null) {
            userSession.setUserSessionType(UserSessionType.USERNAME_RESERVATION);
            userSession.setLoginCompleteSuccess(true);
            // Username is reserve, redirigirlo a la pagina de pending
            // applications = fake portal

            String remoteToken = userSession.getRemoteToken();

            if (!StringUtils.isEmpty(remoteToken)) {
                userSession.setStatusmessage(
                        "You already have a pending account application. <br>If you need assistance please call us at (787) 724-3650.");

            }

            return new ModelAndView("forward:portal", model);
        }

        if (profile != null && profile.getEntitlementByType(EntitlementType.BANKING) == null)
            throw new ProgrammingError("Customer profile banking entitlement null");

        if (userSession.getCustomerProfile() != null)
            userSession.setUserSessionType(UserSessionType.CUSTOMER_PROFILE);

        if (userSession.getBankingSession() == null
                && userSession.getUserSessionType().equals(UserSessionType.CUSTOMER_PROFILE)) {

            // Verify if the Customer initially sent a signon token

            String remoteToken = userSession.getRemoteToken();

            if (!StringUtils.isEmpty(remoteToken)) {

                if (!userSession.isCanOpenAdditionalAccounts())
                    userSession.setStatusmessage(
                            "You already have a pending account application. <br>If you need assistance please call us at (787) 724-3650.");
                else {
                    // DCI
                    ModifyCustomerRequest modifyCustomerRequest = new ModifyCustomerRequest(
                            Utils.generateCustomerInteractionEvent(request),
                            userSession.getCustomerProfile().getProfileId(), CustomerFlagType.DCI_QUESTIONS_REQUIRED);
                    CustomerFacade.getInstance().addCustomerFlag(userSession, modifyCustomerRequest);
                    DCICustomer dciCustomer = new DCICustomer(profile, Calendar.getInstance().getTime(), null, null,
                            null, null, null);
                    userSession.getCustomerService().addDCICustomer(dciCustomer,
                            Utils.generateCustomerInteractionEvent(request));
                    String url = customerService.tokenGetAuthorizedReturnUrl(profile, remoteToken);
                    return new ModelAndView("redirect:" + url);
                }
            }

            // MBSFE-36 / MBSFE-19 / MBSFE-31
            if (userSession.getCustomerProfile().hasFlag(CustomerFlagType.PENDING_COMERCIAL_ACCEPTANCE)) {
                return new ModelAndView("forward:enrollmentComercial");
            }

            // MBCA119 flag OAO inscription customer records and Flag as a
            // Transactionals
            if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA119)
                    && userSession.getCustomerProfile().getIsTransactional() == null
                    && profile.getEnrollmentSource().equals(CustomerEnrollmentSource.ONLINE_AO)
                    && profile.getStatus().equals(CustomerStatus.ACTIVE)) {
                customerService.setIsTransactional(profile, "Y");
            }

            //
            CustomerEntitlement ent = userSession.getCustomerProfile().getEntitlementByType(EntitlementType.BANKING);
            if (ent != null) {
                evertec.cibp.core.models.entitlement.BankingEntitlement entt = new BankingEntitlement(ent);
                // BankingSessionRequest sessionRequest = new BankingSessionRequest(
                // Utils.generateCustomerInteractionEvent(request),
                // userSession.getCustomerProfile(), null, entt,
                // false);
                //
                // BankingSessionResponse bankingresponse = userSession.getCustomerService()
                // .getBankingSession(sessionRequest);

                VaultAccountsRequest vaultRequest = new VaultAccountsRequest();
                vaultRequest.setUserSession(userSession);
                vaultRequest.setRequest(request);
                vaultRequest.setBankingEntitlement(entt);
                BankingSessionResponse bankingresponse = VaultAccountsFacade.getInstance().vaultSignIn(vaultRequest);

                if (bankingresponse != null && bankingresponse.getErrorCode() == 0) {
                    userSession.setBankingSession(bankingresponse.getBankingSession());
                    if (userSession.getCustomerProfile() != null
                            && userSession.getCustomerProfile().getPermId() == null) {
                        userSession.getCustomerService().updateCustomerPermId(userSession.getCustomerProfile(),
                                bankingresponse.getBankingSession().getPermId());
                    }
                    if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_RENEWPERMID)) {
                        // validates if the current PermId in DB is the same as
                        // the one in NTIP, set flag to update if they're
                        // different
                        if (userSession.getCustomerProfile() != null
                                && userSession.getCustomerProfile().getPermId() != null
                                && !userSession.getCustomerProfile().getPermId()
                                .equals(bankingresponse.getBankingSession().getPermId())) {
                            userSession.setRenewPermId(true);
                        }
                    }
                    // trying to get the CIS info for the customer gender
                    if (GenericValidator.isBlankOrNull(userSession.getCustomerProfile().getGender())) {
                        String[] data = customerService
                                .getDataCustomerProfile(userSession.getBankingSession().getPermId().toString());
                        char gender = ' ';
                        if (data != null && data.length != 0 && data[4] != null && data[4].length() > 0) {
                            gender = data[4].toLowerCase().charAt(0);
                        } else {
                            data = new String[6];
                            gender = customerService
                                    .getGenderCustomerProfile(userSession.getBankingSession().getPermId().toString());
                        }
                        if (gender != ' ') {
                            profile.setGender(String.valueOf(gender).toUpperCase());
                        }
                    } else {
                        profile.setGender(userSession.getCustomerProfile().getGender());
                    }
                } else if (bankingresponse.getErrorCode() == 1880) {
                    model.put("username", userSession.getCustomerProfile().getUsername());
                    model.put("tvblocked", true);
                    request.setAttribute("firstTime", false);
                    return new ModelAndView("forward:login", model);
                } else if (bankingresponse.getErrorCode() == 1740 && userSession.getCustomerProfile() != null
                        && userSession.getAccountApplications(true).getCompletedApplications() != null
                        && userSession.getAccountApplications(true).getCompletedApplications().size() >= 1) {
                    return new ModelAndView("portalAoa");
                } else if (bankingresponse.getErrorCode() == 1740 && userSession.getCustomerProfile() != null) {
                    // account used to enroll is closed or was transferred to
                    // another account.
                    Calendar custBirthDate = Calendar.getInstance();
                    custBirthDate.setTime(userSession.getCustomerProfile().getBirthDate());

                    PersonalInformationValidationRequest personalRequest = new PersonalInformationValidationRequest(
                            null, userSession.getCustomerProfile().getTaxId(), custBirthDate, false, false);
                    personalRequest.setValidateMultipleProfiles(true);
                    PersonalInformationValidationResponse customerEnrollment = customerService
							.isEnrollableCustomer(personalRequest);
                    WebUtils.setSessionAttribute(request, "customerEnrollment", customerEnrollment);
                    Utils.logEnrollmentAccount(request, userSession,
                            "Vault error 1740. Account " + entt.getAccountNumber() + ". SSN " + entt.getSsn() + ".");
					if (userSession.getUsername() != null) {
						logger.error("account used to enroll is closed or was transferred to another account, flag4:" + userSession.getUsername());
					}
                    return new ModelAndView("forward:updEnrollAcct");
                } else
                    throw new ProgrammingError(
                            "Unable to get Customer Banking Session. Reason:" + bankingresponse.getErrorCode());
            } else
                throw new ProgrammingError("Customer Banking Entitlement is not defined.");
        }

        String mbopId = userSession.getCustomerProfile().getMbopId();

        CompleteLoginProcessRequest modReq = new CompleteLoginProcessRequest(
                Utils.generateCustomerInteractionEvent(request), profile, null, false);
        // FFEIC
        String header = request.getHeader("user-agent");
        CustomerDevices cd = DeviceUtils.getCustomerDevice(header);
        modReq.getInteractionEvent().putString("DEVICE", cd.toString());
        // FIN FFEIC
        modReq.getInteractionEvent().putString("DEVICE", cd.toString());

        if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)) {

            if(userSession.getUserAgentType() == UserAgentType.WEB ||  userSession.getUserAgentType() == UserAgentType.IPHONE ||  userSession.getUserAgentType() == UserAgentType.ANDROID) {

                modReq.getInteractionEvent().setCustomStringData(DeviceUtils.CONNECTION_INFO, DeviceUtils.getBrowserInfo(header, userSession));

            } else if (userSession.getUserAgentType() == UserAgentType.JSON) {

                modReq.getInteractionEvent().setCustomStringData(DeviceUtils.CONNECTION_INFO, DeviceUtils.getMobileInfo(header, userSession.getDeviceInfo()));
            }

        }
        
		/**
         * Feature for Premia Catalog SSO and Mi Banco MBCA 1559
         * Set in session class the list of premiaAccounts MBCA 1963
         * Verify Customer codes for premia products MBCA 2055
         */
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)) {
        	userSession.getCustomerCodes(wealthCodes, preferredCodes);
        	userSession.getAccounts();
            if (userSession.getCustWealthPreferred() != null && userSession.getPremiaAccountList() != null 
            		&& !userSession.getPremiaAccountList().isEmpty()) {
                userSession.setListRewardPoints(null);
                userSession.setConsultRewardsPoints(true);
                boolean hasPremiaCustomerCodes = PremiaUtil.hasPremiaProductsByCustomerCodes(userSession.getCustWealthPreferred().getCodes(), customerService.getPremiaCustomerCodes());
                userSession.setHasPremia(hasPremiaCustomerCodes);
                if (userSession.isRewardPoints()) {
                    //-- MBCA-1926
                    if (hasPremiaCustomerCodes) {                    	
                        List<TVFrontendAccount> premiaAccounts = userSession.getPremiaAccountList()
        						.stream().filter(tvFrontendAccount -> tvFrontendAccount.isPrimaryAccount())
        						.collect(Collectors.toList());

                        TVFrontendAccount account = userSession.getCustomerService().getPriorityPremiaAccount(premiaAccounts);//Execute SSO
                        model.put("cardNumber", account.getFrontEndId());
                        model.put("redirectToPremia", true);
                        model.put(NON_PREMIA_STATE_FLAG, false);
                        userSession.setNonPremiaState(false);
                    } else {
                        model.put(NON_PREMIA_STATE_FLAG, true);
						userSession.setNonPremiaState(true);
                    }
                }
            } else {
            	userSession.setHasPremia(false);
            }
        }
        
                // ------ MBFIS-13 ------
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP) && CustomerUtils.isSecurities(profile)
                && userSession.getCustomerProfile().getStatus().equals(CustomerStatus.ENROLLED)) {
            return new ModelAndView("redirect:accountinformationsec");
        }

        ModifyCustomerProfileResponse modRes = customerService.completeLoginProcess(modReq);
        userSession.setCustomerProfile(modRes.getCustomerProfile());

        userSession.getCustomerProfile().setMbopId(mbopId);

        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.ATH_ONOFF) || StaticMessageSource.isFlagEnabled(StaticMessageSource.CCA_ONOFF)) {
            userSession.setAllowedAthCardStatuses(getAllowedAthCardStatuses());
            userSession.setAllowedOnOffAthBins(getAllowedOnOffAthBins());
            userSession.setExcludeOnOffCcaBins(getExcludeOnOffCcaBins());
        }

        List<TVFrontendAccount> accounts = userSession.getAccounts();

        if (accounts == null)
            throw new ProgrammingError("Unable to get user accounts");

        // MBFIS-72 / 73 / 74
        // Verify if a user is non-transactional, securities and has an enrollable
        // transactional account
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP) && CustomerUtils.isSecurities(profile)) { // isSecurities
            Calendar custBirthDate = Calendar.getInstance();
            custBirthDate.setTime(userSession.getCustomerProfile().getBirthDate());

            PersonalInformationValidationRequest personalRequest = new PersonalInformationValidationRequest(null,
                    userSession.getCustomerProfile().getTaxId(), custBirthDate, false, false);
            personalRequest.setValidateMultipleProfiles(true);
            PersonalInformationValidationResponse customerEnrollment = customerService
					.isEnrollableCustomer(personalRequest);

            // Boolean to know if enough days have passed to ask again to
            // upgrade.
            boolean askAgain = true;
            Date lastUpgrdSkipDate = userSession.getCustomerService()
                    .getLastNonTransToTransSkipDate(userSession.getCustomerProfile());
            // if lastUpgrdSkipDate is null the user has never skiped the
            // process
            if (lastUpgrdSkipDate != null) {
                // Has current date.
                Calendar calToday = Calendar.getInstance();
                calToday.setTime(new Date());

                // Has the date in which to start asking again.
                Calendar calAskDate = Calendar.getInstance();
                calAskDate.setTime(lastUpgrdSkipDate);
                calAskDate.add(Calendar.DATE, nonTransUpgrdWaitTime);

                // set false if enough days haven't passed.
                askAgain = calToday.compareTo(calAskDate) >= 0;
            }

            //Used before for upgrading (NO TRANS TO TRANS)
            //boolean hasActiveTransAcc = Utils.hasActiveEnrollableTransAcc(customerEnrollment);
            // If user qualifies the upgrade process is presented
            if (customerEnrollment != null 
                    //MBFIS 113 - validate is client has an enrollable account besides SEC
                    && customerEnrollment.isHasEnrollableAccounts() 
                    && customerEnrollment.getStatus() != null
                    && customerEnrollment.getStatus().equals(PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE)
                    && askAgain) {
                WebUtils.setSessionAttribute(request, "customerEnrollment", customerEnrollment);
				if (userSession.getUsername() != null) {
					logger.error("If user qualifies the upgrade process is presented: flag5," + userSession.getUsername());
				}
                return new ModelAndView("forward:updEnrollAcct");
            }
        } // END MBFIS-72 / 73 / 74 

        // Verify if a user is non-transactional and has an enrollable
        // transactional account
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA_UPDT_ACCTS)
                && AccountUtils.getIsTransactional(profile).equalsIgnoreCase("N")) {
            Calendar custBirthDate = Calendar.getInstance();
            custBirthDate.setTime(userSession.getCustomerProfile().getBirthDate());

            PersonalInformationValidationRequest personalRequest = new PersonalInformationValidationRequest(null,
                    userSession.getCustomerProfile().getTaxId(), custBirthDate, false, false);
            personalRequest.setValidateMultipleProfiles(true);
            PersonalInformationValidationResponse customerEnrollment = customerService
					.isEnrollableCustomer(personalRequest);

            // Boolean to know if enough days have passed to ask again to
            // upgrade.
            boolean askAgain = true;
            Date lastUpgrdSkipDate = userSession.getCustomerService()
                    .getLastNonTransToTransSkipDate(userSession.getCustomerProfile());
            // if lastUpgrdSkipDate is null the user has never skiped the
            // process
            if (lastUpgrdSkipDate != null) {
                // Has current date.
                Calendar calToday = Calendar.getInstance();
                calToday.setTime(new Date());

                // Has the date in which to start asking again.
                Calendar calAskDate = Calendar.getInstance();
                calAskDate.setTime(lastUpgrdSkipDate);
                calAskDate.add(Calendar.DATE, nonTransUpgrdWaitTime);

                // set false if enough days haven't passed.
                askAgain = calToday.compareTo(calAskDate) >= 0;
            }

            // If user qualifies the upgrade process is presented
            if (customerEnrollment != null && customerEnrollment.getStatus() != null
                    && Utils.hasActiveEnrollableTransAcc(customerEnrollment)
                    && customerEnrollment.getStatus()
                    .equals(PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE)
                    && askAgain) {
				if (userSession.getUsername() != null) {
					logger.error("info: SessionInitializationController: (If user qualifies the upgrade process is presented), flag3: " + userSession.getUsername());
				}
                WebUtils.setSessionAttribute(request, "customerEnrollment", customerEnrollment);
                return new ModelAndView("forward:updEnrollAcct");
            }
        }

        // MBSFE-213
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE213)) {
            List<TVFrontendAccount> accountsToValidateEnrollAcct = new LinkedList<>();
            accountsToValidateEnrollAcct.addAll(accounts);
            accountsToValidateEnrollAcct.addAll(userSession.getPendingCloseAccounts());

            if (!Utils.enrollmentAccountExists(accountsToValidateEnrollAcct, profile, userSession)) {
				if (userSession.getUsername() != null) {
					logger.error("Info: SessionInitializationController: MBSFE-213> !enrollmentAccountExists, flag1: " + userSession.getUsername());
				}
                Calendar custBirthDate = Calendar.getInstance();
                custBirthDate.setTime(userSession.getCustomerProfile().getBirthDate());

                PersonalInformationValidationRequest personalRequest = new PersonalInformationValidationRequest(null,
                        userSession.getCustomerProfile().getTaxId(), custBirthDate, false, false);
                personalRequest.setValidateMultipleProfiles(true);
                PersonalInformationValidationResponse customerEnrollment = customerService
						.isEnrollableCustomer(personalRequest);
                WebUtils.setSessionAttribute(request, "customerEnrollment", customerEnrollment);
                Utils.logEnrollmentAccount(request, userSession, "Enrollment account do not exists. Account "
                        + profile.getEnrollmentAccountNumber() + " - Status C or I.");
                return new ModelAndView("forward:updEnrollAcct");
            }
        } else {
            if (!Utils.enrollmentAccountExists(accounts, profile)) {
				if (userSession.getUsername() != null) {
					logger.error("Info: SessionInitializationController: !enrollmentAccountExists, flag2: " + userSession.getUsername());
				}
                Calendar custBirthDate = Calendar.getInstance();
                custBirthDate.setTime(userSession.getCustomerProfile().getBirthDate());

                PersonalInformationValidationRequest personalRequest = new PersonalInformationValidationRequest(null,
                        userSession.getCustomerProfile().getTaxId(), custBirthDate, false, false);
                personalRequest.setValidateMultipleProfiles(true);
                PersonalInformationValidationResponse customerEnrollment = customerService
						.isEnrollableCustomer(personalRequest);
                WebUtils.setSessionAttribute(request, "customerEnrollment", customerEnrollment);
                Utils.logEnrollmentAccount(request, userSession, "Enrollment account do not exists. Account "
                        + profile.getEnrollmentAccountNumber() + " - Status C or I.");
                return new ModelAndView("forward:updEnrollAcct");
            }
        }

        /*
         * Remove Esign // ESIGN ENFORCEMENT if
         * (customerService.getGlobalEntitlement(EntitlementType.EBILLS). getStatus() ==
         * EntitlementStatus.ENABLED) { if
         * (profile.hasFlag(CustomerFlagType.ESIGN_ENFORCEMENT) &&
         * !userSession.geteSignLookUp()) { if(userSession.getUserAgentType() ==
         * UserAgentType.JSON){//Fix Apps return new
         * ModelAndView("forward:eSignEnforcement"); } return new
         * ModelAndView("forward:eSignForm"); } } End remove Esign
         */

        // Se buscan los cookies para encontrar si tiene el client-version
        Cookie versionCookie = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null && versionCookie == null) {
            for (int i = 0; i < cookies.length; i++) {
                Cookie cookie = cookies[i];
                if (cookie.getName().equalsIgnoreCase("client-version")) {
                    versionCookie = cookie;
                    break;
                }
            }
        }

        // OUTREACH MBSD-1173
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OUTREACH)) {
            if (!userSession.isAlreadyShownOutreach()) {
                List<CustomerCodes> customer_codes = customerService.getOutreachCustomerCodes();
                if (customer_codes != null && !customer_codes.isEmpty()) {
                    CISOutreachRequest ccRequest = new CISOutreachRequest(
                            Utils.generateCustomerInteractionEvent(request),
                            userSession.getBankingSession().getPermId(), userSession.getCustomerProfile().getTaxId(),
                            userSession.getCustomerCodes(wealthCodes, preferredCodes));
                    CISOutreachResponse ccResponse = customerService.validateCustomerCodeOutreach(ccRequest);
                    if (ccResponse.getStatus() == OutreachValidationStatus.ELIGIBLE) {
                        WebUtils.setSessionAttribute(request, "userSession", userSession);
                        userSession.setElegibleToOutreach(true);
                        customerService.logCustomerUsageData(profile, CustomerActions.OUTREACH_INTERRUPTION);
                        if (userSession.getUserAgentType() != UserAgentType.JSON)
                            return new ModelAndView("forward:outreach");

                    } else if (ccResponse.getStatus() == OutreachValidationStatus.NOT_ELIGIBLE) {
                        userSession.setAlreadyShownOutreach(Boolean.TRUE);
                    } else if (ccResponse.getStatus() == OutreachValidationStatus.EXCEPTION) {
                        userSession.setAlreadyShownOutreach(Boolean.TRUE);
                        logger.error("Error while validating Customer Codes: " + ccResponse.generateErrorReport());
                    }
                } else {
                    userSession.setAlreadyShownOutreach(Boolean.TRUE);
                }
            }
        }
        //

        // NO DCI FOR MOBILE
        if (profile.hasFlag(CustomerFlagType.DCI_QUESTIONS_REQUIRED)) {
            List<DCICustomer> pendingDciCustomer = userSession.getCustomerService()
                    .getDCICustomersByProfilePending(profile);
            if (pendingDciCustomer != null && !pendingDciCustomer.isEmpty()) {
                for (int k = 0; k < pendingDciCustomer.size(); k++) {
                    DCICustomer dciCustomer = pendingDciCustomer.get(k);
                    if (dciCustomer.getAccount() != null) {
                        if (userSession.getUserAgentType() == UserAgentType.WEB
                                || userSession.getUserAgentType() == UserAgentType.IPAD
                                || userSession.getUserAgentType() == UserAgentType.MOBILE_DESKTOP) {
                            if (dciCustomer.getFirstShow() == null) {
                                dciCustomer.setFirstShow(Calendar.getInstance().getTime());
                                userSession.getCustomerService().updateDCICustomer(dciCustomer);
                                WebUtils.setSessionAttribute(request, "noSkip", true);
                            }
                            userSession.setDciCustomer(dciCustomer);
                            return new ModelAndView("forward:dciQuestions");
                        }
                    } else {
                        Boolean hasNoCompleted = false;
                        List<AccountApplication> pendingApplications = userSession.getCustomerService()
                                .getPendingApplication(profile, dciCustomer);
                        AccountApplication app = null;
                        for (int i = 0; i < pendingApplications.size(); i++) {
                            if (pendingApplications.get(i).getStatus().equals(AccountApplicationStatus.COMPLETE)) {
                                app = pendingApplications.get(i);
                                break;
                            } else {
                                hasNoCompleted = true;
                            }
                        }
                        if (app != null) {
                            dciCustomer.setAccount(app.getAccountNumber());
                            dciCustomer.setAccounType(app.getAccountProductId());
                            dciCustomer.setAccounOpenedDate(app.getCreationDate());
                            if (userSession.getUserAgentType() == UserAgentType.WEB
                                    || userSession.getUserAgentType() == UserAgentType.IPAD
                                    || userSession.getUserAgentType() == UserAgentType.MOBILE_DESKTOP) {
                                dciCustomer.setFirstShow(Calendar.getInstance().getTime());
                            }
                            dciCustomer.setApplicationId(app.getApplicationId());
                            userSession.getCustomerService().updateDCICustomer(dciCustomer);
                            userSession.setDciCustomer(dciCustomer);
                            if (userSession.getUserAgentType() == UserAgentType.WEB
                                    || userSession.getUserAgentType() == UserAgentType.IPAD
                                    || userSession.getUserAgentType() == UserAgentType.MOBILE_DESKTOP) {
                                WebUtils.setSessionAttribute(request, "noSkip", true);
                                return new ModelAndView("forward:dciQuestions");
                            }
                        } else {
                            if (!hasNoCompleted) {
                                userSession.getCustomerService().deleteDCICustomer(dciCustomer);
                                pendingDciCustomer.remove(dciCustomer);
                                k--;
                                if (pendingDciCustomer.isEmpty()) {
                                    ModifyCustomerRequest modifyCustomerRequest = new ModifyCustomerRequest(
                                            Utils.generateCustomerInteractionEvent(request),
                                            userSession.getCustomerProfile().getProfileId(),
                                            CustomerFlagType.DCI_QUESTIONS_REQUIRED);
                                    userSession.getCustomerService().resetCustomerFlag(modifyCustomerRequest);
                                }
                            }
                        }
                    }
                }
            } else {
                ModifyCustomerRequest modifyCustomerRequest = new ModifyCustomerRequest(
                        Utils.generateCustomerInteractionEvent(request),
                        userSession.getCustomerProfile().getProfileId(), CustomerFlagType.DCI_QUESTIONS_REQUIRED);
                userSession.getCustomerService().resetCustomerFlag(modifyCustomerRequest);
            }
        }

        userSession.setLoginCompleteSuccess(true);

        // AUTOMATED RDC - DFM ENROLL
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1610)) {
            // ONLY FOR APP LOGIN
            // AND NOT REGISTER IN RDC - DFM
            if (userSession.getUserAgentType() != null && userSession.getUserAgentType().equals(UserAgentType.JSON)
                    && profile.getEntitlementByType(EntitlementType.RDC) == null) {
                List<TVFrontendAccount> dfmAccounts = Utils.getDFMDepositAccounts(userSession.getPortalAccounts(),
                        this.enrollableAccounts);
                List<String> codes = userSession.getCustomerCodes(this.wealthCodes, this.preferredCodes);
                boolean allowByCode = this.isAllowedCode(codes);
                boolean denyByCode = this.isDeniedCode(codes,
                        userSession.getCustomerProfile().useCommercialInterface());
                boolean denyByBank = this.isDeniedBank(dfmAccounts);
                boolean denyByAccount = this.isDeniedAccount(dfmAccounts);
                boolean allowByTime;
                if (!dfmAccounts.isEmpty() && !denyByAccount) {
                    if (allowByCode) {
                        this.createRDCEntitlement(userSession);
                    } else {
                        if (!denyByCode && !denyByBank) {
                            allowByTime = Utils.isAccountOldEnough(this.accountOldMonths, dfmAccounts,
                                userSession);
                            if (allowByTime) {
                                this.createRDCEntitlement(userSession);
                            }
                        }
                    }
                }
            }
        }

        // the analytics process takes places once login is completed and just
        // once
        if (!userSession.getPortalAnalytics()) {
            this.analytyicsMeasure(userSession, request);
        }

        // OPAC
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC)) {
            model.put("opac", true);
        }

        //Update All Email Alerts Personal Information Changes 
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3336)) {
            Runnable runnable = () -> {
                setAllAlertsPersonalInformationChanges(userSession);
            };
            Thread t = new Thread(runnable);
            t.start();
        }

        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)) {
            WebUtils.setSessionAttribute(request, "sessionInitializacionComplete", true);

            CustomerEntitlement ent = userSession.getCustomerProfile().getEntitlementByType(EntitlementType.PAYROLL);
            evertec.cibp.core.models.entitlement.PayrollEntitlement entt = null;
            boolean fromEnrollment = WebUtils.getSessionAttribute(request, "fromEnrollment") != null
                    ? (boolean) WebUtils.getSessionAttribute(request, "fromEnrollment")
                            : false;
                    boolean hasBsmart = false;
                    boolean isModify = false;
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                    if (profile.getEnrollmentAccountProductId() == null
                            || profile.getEnrollmentAccountProductId().equalsIgnoreCase("")) {
                        modifyCustomerAccountProductIdSession(profile, userSession, request);
                    }

                    if (!fromEnrollment) { // Si ya se encontraba inscrito e inicia
                        // sesión y no es un cliente COMERCIAL o
                        // WEALTH
                        if (profile.getUserInterface() != UserInterface.COMMERCIAL
                                && !userSession.isWealth(wealthCodes, preferredCodes)) {
                            if (ent == null) {
                                if (profile.getEnrollmentAccountProductId().equalsIgnoreCase("086")) {
                                    // Si su cuenta de inscripcion es una BSmart y no
                                    // había pasado por el flujo
                                    // para mostrar el POPUP de cambiar a cliente
                                    // comercial.
                                    hasBsmart = true;
                                    entt = new PayrollEntitlement(String.valueOf(accounts.size()), String.valueOf(hasBsmart),
                                            PayrollStatus.NEW.toString(), "", EntitlementStatus.ENABLED);
                                    isModify = true;
                                    model.put("popUpBsmartAcceptTerms", "T");
                                }
                            } else if (ent != null) {
                                entt = new PayrollEntitlement(ent);
                                if (entt.getAcceptterms().equalsIgnoreCase(PayrollStatus.NEW.toString())
                                        || entt.getAcceptterms().equalsIgnoreCase(PayrollStatus.REMINDME.toString())) {
                                    // Si ya ha pasado por el flujo y es un nuevo
                                    // Entitlement o presiono para recordar luego
                                    // y aún no es un cliente comercial.
                                    model.put("popUpBsmartAcceptTerms", "T");
                                }
                            }
                        }
                    } else { // Si es un enrollment, se procede a crear el entitlment de
                        // acuerdo
                        // a si eligio ser un cliente comercial o retail.
                        if (!profile.hasEntitlement(EntitlementType.PAYROLL)) {
                            Date date = new Date();
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date); // Configuramos la fecha que se recibe
                            calendar.add(Calendar.DAY_OF_YEAR, 90); // numer
                            date = calendar.getTime();

                            if (profile.getEnrollmentAccountProductId().equalsIgnoreCase("086")
                                    && profile.getUserInterface() != UserInterface.COMMERCIAL) {
                                hasBsmart = true;
                                isModify = true;
                                entt = new PayrollEntitlement(String.valueOf(accounts.size()), String.valueOf(hasBsmart),
                                        PayrollStatus.REJECT.toString(), format.format(date), EntitlementStatus.ENABLED);
                            } else if (profile.getEnrollmentAccountProductId().equalsIgnoreCase("086")
                                    && profile.getUserInterface() == UserInterface.COMMERCIAL) {
                                hasBsmart = true;
                                isModify = true;
                                entt = new PayrollEntitlement(String.valueOf(accounts.size()), String.valueOf(hasBsmart),
                                        PayrollStatus.ACEPT.toString(), format.format(date), EntitlementStatus.ENABLED);
                            } else if (profile.getUserInterface() == UserInterface.COMMERCIAL) {
                                hasBsmart = false;
                                isModify = true;
                                entt = new PayrollEntitlement(String.valueOf(accounts.size()), String.valueOf(hasBsmart),
                                        PayrollStatus.ACEPT.toString(), format.format(date), EntitlementStatus.ENABLED);

                            }
                        }
                    }
                    if (isModify) {
                        if (profile.getUserInterface() == UserInterface.COMMERCIAL) {
                            if (StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE)) {
                                entt.setFPSDateFirstSignon(format.format(new Date()));
                                entt.setFPSShowNewBubbleAlert(true);
                                WebUtils.setSessionAttribute(request, "fpsShowNewBubbleAlert", entt.isFPSShowNewBubbleAlert());
                            }
                        }
                        customerService.addEntitlement(profile, entt.getCustomerEntitlement());
                        userSession.setCustomerProfile(profile);
                        WebUtils.setSessionAttribute(request, "userSession", userSession);
                    }

                    if (StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE)) {
                        if (profile.getUserInterface() == UserInterface.COMMERCIAL && !isModify) {
                            userSession.setFraudPrevAppStatus(
                                    Utils.getCustomerAppStatus(userSession.getCustomerApplications()).get("fraudStatus"));
                            if (entt == null) {
                                entt = new PayrollEntitlement(ent);
                            }
                            String fpsDateFirstSignon;
                            try {
                                fpsDateFirstSignon = entt.getFPSDateFirstSignon();
                            }catch(Exception e){
                                fpsDateFirstSignon = null;
                            }
                            if (fpsDateFirstSignon == null || fpsDateFirstSignon.length() == 0) {
                                entt.setFPSDateFirstSignon(format.format(new Date()));
                                entt.setFPSShowNewBubbleAlert(true);
                                CustomerFacade.getInstance().updateEntitlement(userSession,
                                        Collections.singletonList(entt.getCustomerEntitlement()));
                            } else if (fpsDateFirstSignon != null && entt.isFPSShowNewBubbleAlert() && fpsDateFirstSignon.length() > 0) {
                                Date dateFirstSignon = format.parse(entt.getFPSDateFirstSignon());
                                long diff = daysBetween(dateFirstSignon, new Date());
                                if (diff > fpsShowNewBubbleAlertDay.intValue()) {
                                    entt.setFPSShowNewBubbleAlert(false);
                                    CustomerFacade.getInstance().updateEntitlement(userSession,
                                            Collections.singletonList(entt.getCustomerEntitlement()));
                                }
                            }
                            WebUtils.setSessionAttribute(request, "fpsShowNewBubbleAlert", entt.isFPSShowNewBubbleAlert());
                        }

                    }

        }
         	
        ModelAndView interruptionView = OOBInterruptionForm.applyForInterruptionPageWebVersion(userSession);
        if (interruptionView != null) {
        	return interruptionView;
        }

        return new ModelAndView("forward:portal", model);
    }

    private void setAllAlertsPersonalInformationChanges(UserSession userSession) {

        AlertsEntitlement entitlement = new AlertsEntitlement(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS));

        Set<AlertType> customeralerts = null;
        if (entitlement.getCustomerEntitlement() != null && 
                entitlement.getAlerts() != null) {
            customeralerts = new HashSet<AlertType>();
            customeralerts = entitlement.getAlerts();
        }

        if(customeralerts != null && !customeralerts.isEmpty()) {

            boolean isEntitlementChanged = false;
            if (!customeralerts.contains(AlertType.CHANGE_PASS_EMAIL)) {
                customeralerts.add(AlertType.CHANGE_PASS_EMAIL);
                isEntitlementChanged = true;
            }
            if (!customeralerts.contains(AlertType.ACCESS_BLOCK_EMAIL)) {
                customeralerts.add(AlertType.ACCESS_BLOCK_EMAIL);
                isEntitlementChanged = true;
            }
            if (!customeralerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL)) {
                customeralerts.add(AlertType.QUESTIONS_BLOCK_EMAIL);
                isEntitlementChanged = true;
            }
            if (!customeralerts.contains(AlertType.QUESTIONS_EDIT_EMAIL)) {
                customeralerts.add(AlertType.QUESTIONS_EDIT_EMAIL);
                isEntitlementChanged = true;
            }
            if (!customeralerts.contains(AlertType.EMAIL_EDIT_EMAIL)) {
                customeralerts.add(AlertType.EMAIL_EDIT_EMAIL);
                isEntitlementChanged = true;
            }
            if (!customeralerts.contains(AlertType.PHONE_EDIT_EMAIL)) {
                customeralerts.add(AlertType.PHONE_EDIT_EMAIL);
                isEntitlementChanged = true;
            }
            if (!customeralerts.contains(AlertType.ADRESS_EDIT_EMAIL)) {
                customeralerts.add(AlertType.ADRESS_EDIT_EMAIL);
                isEntitlementChanged = true;
            }
            if (!customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_EMAIL)) {
                customeralerts.add(AlertType.MOBILE_PHONE_EDIT_EMAIL);
                isEntitlementChanged = true;
            }
            if (!customeralerts.contains(AlertType.NEW_FINGERPRINT_EMAIL)) {
                customeralerts.add(AlertType.NEW_FINGERPRINT_EMAIL);
                isEntitlementChanged = true;
            }

            if (isEntitlementChanged) {
                entitlement.setAlerts(customeralerts);
                try {
                    CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(entitlement.getCustomerEntitlement()));
                } catch (RuntimeException e) {
                    logger.error("Error in Update all email alerts personal information changes ", e);
                }
            }
        }
    }

    /*
     * Es un workaround temporal, se debe modificar por un servicio que escriba
     * directamente en base de datos
     */
    private boolean modifyCustomerAccountProductIdSession(CustomerProfile customerProfile, UserSession userSession,
            HttpServletRequest request) {
        List<TVFrontendAccount> accounts = userSession.getAccounts();

        for (TVFrontendAccount acct : accounts) {
            if (customerProfile.getEnrollmentAccountNumber().equalsIgnoreCase(acct.getAccountNumber())) {
                customerProfile.setEnrollmentAccountProductId(acct.getAccountProductId());
                userSession.setCustomerProfile(customerProfile);
                WebUtils.setSessionAttribute(request, "userSession", userSession);
                userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
                return true;
            }
        }
        customerProfile.setEnrollmentAccountProductId("");
        userSession.setCustomerProfile(customerProfile);
        WebUtils.setSessionAttribute(request, "userSession", userSession);
        userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        return false;
    }

    // get all the information for anayltics
    private void analytyicsMeasure(UserSession userSession, HttpServletRequest request) {
        try {
            GoogleAnalyticsTrackingRequest gaRequest = new GoogleAnalyticsTrackingRequest();

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            if (userSession.getCustomerProfile().getGender() != null) {
                gaRequest.setCd3(userSession.getCustomerProfile().getGender());
            } else {
                gaRequest.setCd3("U");
            }

            Calendar dob = Calendar.getInstance();
            dob.setTime(userSession.getCustomerProfile().getBirthDate());
            Calendar now = Calendar.getInstance();
            int age = now.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
            if (dob.get(Calendar.MONTH) > now.get(Calendar.MONTH) || (dob.get(Calendar.MONTH) == now.get(Calendar.MONTH)
                    && dob.get(Calendar.DAY_OF_MONTH) > now.get(Calendar.DAY_OF_MONTH))) {
                age--;
            }
            gaRequest.setCd4(String.valueOf(age));

            gaRequest.setCd5(String.valueOf(dob.get(Calendar.YEAR)));

            gaRequest.setCd6(df.format(userSession.getCustomerProfile().getCreationDate()));

            if (userSession.getCustomerProfile().useCommercialInterface()) {
                gaRequest.setCd7("Corporate");
            } else if (userSession.isWealth(wealthCodes, preferredCodes)) {
                gaRequest.setCd7("One");
            } else {
                gaRequest.setCd7("Personal");
            }

            TVFrontendAccount enrollAccount = null;
            String products = "";
            List<String> productsAdded = new ArrayList<String>();
            for (int i = 0; i < userSession.getAccounts().size(); i++) {
                TVFrontendAccount account = userSession.getAccounts().get(i);
                if (enrollAccount == null && account.getTVAccount().getAccountID()
                        .equals(userSession.getCustomerProfile().getEnrollmentAccountNumber())) {
                    if (account.getSubtype().equalsIgnoreCase("IDA")) {
                        if (account.getAccountNumberSuffix() == null || (!account.getAccountNumberSuffix().equals("SAV")
                                && !account.getAccountNumberSuffix().equals("RES"))) {
                            enrollAccount = account;
                        }
                    } else {
                        enrollAccount = account;
                    }
                }
                if (!productsAdded.contains(account.getSubtype())) {
                    productsAdded.add(account.getSubtype());
                    if (products.equals("")) {
                        products += account.getSubtype();
                    } else {
                        products += "|" + account.getSubtype();
                    }
                }
            }

            gaRequest.setCd8(products);

            if (enrollAccount != null && enrollAccount.getPortalBalance() != null) {
                String[] balances = enrollAccount.getPortalBalance().toString().split("\\.");
                gaRequest.setEv(balances[0]);
            } else {
                gaRequest.setEv("0");
            }

            Date fss = userSession.getCustomerProfile().getSignonStats().getFirstSignOnSuccess();
            if (fss != null) {
                gaRequest.setCd9(df.format(fss));
            } else {
                gaRequest.setCd9("null");
            }

            if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE288)) {
                gaRequest.setCd2(userSession.getCustomerProfile().getPermId().toString());
            } else {
                gaRequest.setCd2(userSession.getCustomerProfile().getMbopId());
            }

            Boolean newMbopId = (Boolean) WebUtils.getSessionAttribute(request, "newMbopId");
            if (newMbopId != null && newMbopId) {
                gaRequest.setEa("FirstLogin");
            } else {
                gaRequest.setEa("Login");
            }
            WebUtils.setSessionAttribute(request, "newMbopId", null);
            userSession.setPortalAnalytics(true);

            gaRequest.setV("1");
            gaRequest.setTid(this.analyticsId);
            gaRequest.setT("event");
            gaRequest.setEc("MiBanco");
            gaRequest.setEl("success");

            Cookie ga = null;
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    Cookie cookie = cookies[i];
                    if (cookie.getName().equalsIgnoreCase("_ga")) {
                        ga = cookie;
                        break;
                    }
                }
            }
            if (ga != null && ga.getValue() != null) {
                String[] gas = ga.getValue().split("\\.");
                gaRequest.setCid(gas[2] + "." + gas[3]);
                Thread analytics = new Thread(new GoogleAnalyticsTracking(gaRequest));
                analytics.start();
            }
        } catch (Exception e) {
            logger.error("Error while sending analytics data: " + e.getMessage());
        }

    }

    private long daysBetween(Date startDate, Date endDate) {
        long diffInMillies = Math.abs(startDate.getTime() - endDate.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        return diff;
    }

    public String getAnalyticsId() {
        return analyticsId;
    }

    public void setAnalyticsId(String analyticsId) {
        this.analyticsId = analyticsId;
    }

    public String getWealthCodes() {
        return wealthCodes;
    }

    public void setWealthCodes(String wealthCodes) {
        this.wealthCodes = wealthCodes;
    }

    public String getPreferredCodes() {
        return preferredCodes;
    }

    public void setPreferredCodes(String preferredCodes) {
        this.preferredCodes = preferredCodes;
    }

    public String getIraProductIds() {
        return iraProductIds;
    }

    public void setIraProductIds(String iraProductIds) {
        this.iraProductIds = iraProductIds;
    }

    public CampaignGenericBean getCampaignGenericBean() {
        return campaignGenericBean;
    }

    public void setCampaignGenericBean(CampaignGenericBean campaignGenericBean) {
        this.campaignGenericBean = campaignGenericBean;
    }

    public Integer getMaxStatements() {
        return maxStatements;
    }

    public void setMaxStatements(Integer maxStatements) {
        this.maxStatements = maxStatements;
    }

    public String getProductComercial() {
        return productComercial;
    }

    public void setProductComercial(String productComercial) {
        this.productComercial = productComercial;
    }

    /**
     * @return CSV of non transactional enrollable accounts.
     * @author Hector Ortiz ET56802
     */
    public String getNonTransEnrollAccs() {
        return nonTransEnrollAccs;
    }

    /**
     * Set a CSV of non transactional enrollable accounts to be passed to
     * UserSession via setNonTransEnrollAccsSet().
     * 
     * @param nonTransEnrollAccs
     *              CSV of non transactional enrollable accounts
     * @author Hector Ortiz ET56802
     */
    public void setNonTransEnrollAccs(String nonTransEnrollAccs) {
        this.nonTransEnrollAccs = nonTransEnrollAccs;
    }

    public int getNonTransUpgrdWaitTime() {
        return nonTransUpgrdWaitTime;
    }

    public void setNonTransUpgrdWaitTime(int nonTransUpgrdWaitTime) {
        this.nonTransUpgrdWaitTime = nonTransUpgrdWaitTime;
    }

    public Integer getFpsShowNewBubbleAlertDay() {
        return fpsShowNewBubbleAlertDay;
    }

    public void setFpsShowNewBubbleAlertDay(Integer fpsShowNewBubbleAlertDay) {
        this.fpsShowNewBubbleAlertDay = fpsShowNewBubbleAlertDay;
    }

    private boolean isAllowedCode(List<String> codes) {
        for (int j = 0; j < this.allowCodes.length; j++) {
            if (codes.contains(this.allowCodes[j])) {
                return true;
            }
        }
        return false;
    }

    private boolean isDeniedCode(List<String> codes, boolean isComercal) {
        if (isComercal) {
            for (int j = 0; j < this.denyCodesComercial.length; j++) {
                if (codes.contains(this.denyCodesComercial[j])) {
                    return true;
                }
            }
        } else {
            for (int j = 0; j < this.denyCodesRetail.length; j++) {
                if (codes.contains(this.denyCodesRetail[j])) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isDeniedBank(List<TVFrontendAccount> accounts) {
        for (int i = 0; i < accounts.size(); i++) {
            boolean deny = false;
            for (String denyBank : this.denyBanks) {
                if (accounts.get(i).getBankId().equals(denyBank)) {
                    deny = true;
                    break;
                }
            }
            if (!deny) {
                return false;
            }
        }
        return true;
    }

    private boolean isDeniedAccount(List<TVFrontendAccount> accounts) {
        for (int i = 0; i < accounts.size(); i++) {
            boolean deny = false;
            for (String denyAccount : this.denyAccounts) {
                if (accounts.get(i).getAccountNumber().startsWith(denyAccount)) {
                    deny = true;
                    break;
                }
            }
            if (!deny) {
                return false;
            }
        }
        return true;
    }

    private void createRDCEntitlement(UserSession userSession) {
        RDCEntitlement rdc = new RDCEntitlement("", "", "", Boolean.TRUE, Boolean.FALSE, EntitlementStatus.ENABLED);
        userSession.getCustomerService().addEntitlement(userSession.getCustomerProfile(), rdc.getCustomerEntitlement());
    }

    public void setAllowCodes(String[] allowCodes) {
        this.allowCodes = allowCodes;
    }

    public void setDenyCodesRetail(String[] denyCodesRetail) {
        this.denyCodesRetail = denyCodesRetail;
    }

    public void setDenyBanks(String[] denyBanks) {
        this.denyBanks = denyBanks;
    }

    public void setDenyAccounts(String[] denyAccounts) {
        this.denyAccounts = denyAccounts;
    }

    public void setDenyCodesComercial(String[] denyCodesComercial) {
        this.denyCodesComercial = denyCodesComercial;
    }

    public void setAccountOldMonths(Integer accountOldMonths) {
        this.accountOldMonths = accountOldMonths;
    }

    public void setEnrollableAccounts(String enrollableAccounts) {
        this.enrollableAccounts = enrollableAccounts;
    }

    public String getEvendorId() {
        return evendorId;
    }

    public void setEvendorId(String evendorId) {
        this.evendorId = evendorId;
    }

    public String getEpayrollId() {
        return epayrollId;
    }

    public void setEpayrollId(String epayrollId) {
        this.epayrollId = epayrollId;
    }

    public String[] getAllowedAthCardStatuses() {
        return allowedAthCardStatuses;
    }

    public void setAllowedAthCardStatuses(String[] allowedAthCardStatuses) {
        this.allowedAthCardStatuses = allowedAthCardStatuses;
    }

    public String[] getAllowedOnOffAthBins() {
        return allowedOnOffAthBins;
    }

    public void setAllowedOnOffAthBins(String[] allowedOnOffAthBins) {
        this.allowedOnOffAthBins = allowedOnOffAthBins;
    }

    public String[] getExcludeOnOffCcaBins() {
        return excludeOnOffCcaBins;
    }

    public void setExcludeOnOffCcaBins(String[] excludeOnOffCcaBins) {
        this.excludeOnOffCcaBins = excludeOnOffCcaBins;
    }
}
