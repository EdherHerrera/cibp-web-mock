
package evertec.cibp.web.form.mobile;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import com.google.common.base.Enums;
import com.google.gson.Gson;

import bppr.sms.beans.SMSAccount;
import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.campaign.ActiveCampaignView;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.campaign.ViewCampaign;
import evertec.cibp.core.models.campaign.ViewCampaignId;
import evertec.cibp.core.models.customer.AlertCondition;
import evertec.cibp.core.models.customer.CustomerBalanceAlert;
import evertec.cibp.core.models.customer.CustomerEmailBalanceAlert;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.EmailAlertCondition;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.SMSEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.AddSMSAccountRequest;
import evertec.cibp.core.services.requests.ModifyCustomerAlertsRequest;
import evertec.cibp.core.services.responses.SmsAlertsResponse;
import evertec.cibp.core.services.responses.SmsAlertsResponse.Result;
import evertec.cibp.core.services.smsAlerts.SmsBean;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.AlertsBalanceFacade;
import evertec.cibp.web.facade.AlertsFacade;
import evertec.cibp.web.form.mobile.bean.MobileAlert;
import evertec.cibp.web.form.mobile.bean.MobileAlertAccount;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.AlertTypesUtils;
import evertec.cibp.web.utils.Utils;

public class MobileAlertsController extends MultiActionController {
	
	private static Gson gson = new Gson();
	
	private String alertsProductTypes;
	private String tsysAlertsCreditCardFilterCode;
	private String tsysAlertsAddIsCCAFilterCodeComercial;
	private String tsysAlertsAddIsCCAFilterCodeRetail;
	private String tsysAlertsCreditCardFilterNTIPCode;
	
	private static final String ALERTS_JSON = "mobileAlertsJson";
	private static final String ALERTS_VIEW = "mobileAlerts";
	
	private static final Logger LOG = Logger.getLogger(MobileAlertsController.class);
	
	
	public ModelAndView mobileAlerts(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> model = new HashMap<>();
				
		UserSession session = getSession(request);

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, session != null && session.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99
		
		model.put("hasDDA", session.hasVisibleDDA());
		model.put("isPhase2Enabled", Utils.isPushPhase2Enabled());
		model.put("hasCCA", session.hasVisibleCCA());
		model.put("hasPremia", session.hasVisiblePremia());	
		model.put("useCommercialInterface", session.getCustomerProfile().useCommercialInterface());
		model.put("showMenuPayroll", StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL));
		model.put("isEvendorActive", StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR));
		model.put("isNonTransactional", !StringUtils.equals(AccountUtils.getIsTransactional(session.getCustomerProfile()), "Y"));
		model.put("showFraudPreventionService", StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE));
		model.put("isAlertsMSEnabled", true);
		model.put("hasOnOffElegibleAccount", session.isHasOnOffEligibleAccount());

		return new ModelAndView(ALERTS_VIEW, model);
	}
	
	public ModelAndView mobileAlertsGetBalanceAlerts(HttpServletRequest request, HttpServletResponse response) throws Exception {

		UserSession session = getSession(request);
		
		List<MobileAlertAccount> depositAccounts = new LinkedList<>();
		List<MobileAlertAccount> creditAccounts = new LinkedList<>();
		List<MobileAlertAccount> premiaAccounts = new LinkedList<>();
		CustomerServices customerService = session.getCustomerService();
		CustomerProfile profile = session.getCustomerProfile();
		AlertsEntitlement entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));
		CustomerInteractionEvent customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
		//Retrieve all email balance alerts
		List<CustomerEmailBalanceAlert> emailAccounts = session.getEmailaccounts(customerInteractionEvent);
		//Retrieve all SMS accounts
		List<SMSAccount> smsAccounts = session.getSmsaccounts(customerInteractionEvent);
		//Retrieve all email balance alerts
		List<CustomerBalanceAlert> balanceAlerts = session.getBalanceAlerts();

		addEntitlementIfNewAlert(profile, customerService);
		
		Set<AlertType> customerAlerts = new HashSet<>();

		if (entitlement != null && entitlement.getAlerts() != null)
			customerAlerts = entitlement.getAlerts();
		
		for (TVFrontendAccount account : session.getAccounts()) {
			
			if (!isIneligibleAccount(account) && Utils.haveType(alertsProductTypes, account.getSubtype())) {
				
				MobileAlertAccount alertAccount = new MobileAlertAccount(account);
				
				List<MobileAlert> alerts = new LinkedList<>();
				//Add all current email balance alerts
				addEmailAlerts(account, emailAccounts, alerts, customerAlerts);
				//Add all current email/push/SMS alerts (new implementation)
				addBalanceAlerts(account, balanceAlerts, alerts);
				//Add all current SMS balance Alerts
				addSMSAlerts(account, smsAccounts, alerts, profile);
				
				alertAccount.setAlerts(alerts);
			
				switch (account.getSubtype()) {
					case MobileAlert.IDA:
						depositAccounts.add(alertAccount);
						break;
					case MobileAlert.CCA:
						creditAccounts.add(alertAccount);
						break;
					case MobileAlert.PRM:
						premiaAccounts.add(alertAccount);
						break;
					default:
						LOG.error("Invalid account subtype: " + account.getSubtype() + " for Balance Alert");
						break;
				}
			}
		}
		
		Map<String, Object> model = new HashMap<>();
		model.put("depositAccounts", gson.toJson(depositAccounts));
		model.put("creditAccounts", gson.toJson(creditAccounts));
		model.put("premiaAccounts", gson.toJson(premiaAccounts));
		model.put("showSegmentBar", showSegmentBar(session));
		
		return new ModelAndView(ALERTS_JSON, model);
	}
	
	public ModelAndView mobileAlertsGetAccountsWithPlastic(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession session = getSession(request);
		CustomerServices customerService = session.getCustomerService();
		CustomerProfile profile = session.getCustomerProfile();

		AccountMaskUtil maskUtil = new AccountMaskUtil();
		List<TVFrontendAccount> accounts = session.getAccounts();
		List<MobileAlertAccount> accountsList = new ArrayList<>();

		if (accounts != null) {
			for (TVFrontendAccount account : accounts) {

				CardPlasticFrontend[] plasticsArray = session.getCardPlastics(account);
				MobileAlertAccount accountWithPlastics = new MobileAlertAccount();
				String suffix = account.getAccountNumberSuffix() != null ? account.getAccountNumberSuffix() : "";
				if ((!suffix.equalsIgnoreCase("")) && (suffix.equalsIgnoreCase("RES") || suffix.equalsIgnoreCase("SAV"))
						|| (plasticsArray == null || plasticsArray.length == 0) || account.getHide() || !account.getFeatures().isMobileBanking()) {
					continue;
				}
				accountWithPlastics.setNickname(account.getNickname());
				accountWithPlastics.setId(account.getFrontEndId());
				accountWithPlastics.setNumber(maskUtil.mask(account.getAccountNumber()));

				List<MobileAlertAccount> plasticsList = new ArrayList<>();
				
				for (CardPlasticFrontend plasticItem : plasticsArray) {

					MobileAlertAccount plastic = new MobileAlertAccount();
					String plasticPanId = plasticItem.getCardPlasticAccountRecord().getCardPANId();
					String cardType;

					switch (maskUtil.ATHType(plasticPanId)) {
						case AccountMaskUtil.ATH_REGULAR:
							cardType = "ATH\u00AE Regular";
							break;
						case AccountMaskUtil.ATH_INTERNATIONAL:
							cardType = "ATH\u00AE Internacional";
							break;
						case AccountMaskUtil.DISCOVER:
							cardType = "ATH \u002D Discover";
							break;
						default:
							cardType = "";
					}
					
					SmsBean smsBean = new SmsBean();
				    smsBean.setAcctNum(account.getAccountNumber());
				    smsBean.setTaxId(profile.getTaxId());
				    smsBean.setCardNum(plasticPanId);
				    
				    try {
				    	SmsAlertsResponse smsAlertsResponse = 
				    			customerService.getSmsAlertInfo(smsBean, Utils.generateCustomerInteractionEvent(request));
				    	
				    	if(smsAlertsResponse != null && Result.SUCCESS.equals(smsAlertsResponse.getResult())) {
				    		List<MobileAlert> alertList = new ArrayList<>();
				    		
				    		SmsBean info =  smsAlertsResponse.getResponseSmsBean();
							boolean hasAthDaily = info.getPurchase_thr_chksmsSpecified();
							boolean hasAthTotal = info.getDlyPurchasetotlschksmsSpecified();
							boolean hasAtmDaily = info.getWithdraw_thr_chksmsSpecified();
							boolean hasAtmTotal = info.getDlyWithdrawtotlschksmsSpecified();

							if (hasAthDaily && !info.getPurchase_thr_chksms().equalsIgnoreCase("0")) {
								MobileAlert alert = new MobileAlert();
								alert.setType("athdaily");
								alert.setActive(true);
								if (info.getPurchaseminamtSpecified()) {
									alert.setAmount(info.getAlertsPurchaseminamt());
								}
								alertList.add(alert);
							
							}
							if (hasAthTotal && !info.getDlyPurchasetotlschksms().equalsIgnoreCase("0")) {
								MobileAlert alert = new MobileAlert();
								alert.setType("athtotal");
								alert.setActive(true);
								if (info.getDlyPurchasetotlsminamtSpecified()) {
									alert.setAmount(info.getDlyPurchasetotlsminamt());
								}
								alertList.add(alert);

							}
							if (hasAtmDaily && !info.getWithdraw_thr_chksms().equalsIgnoreCase("0") ) {
								MobileAlert alert = new MobileAlert();
								alert.setType("atmdaily");
								alert.setActive(true);
								if (info.getWithdrawminamtSpecified()) {
									alert.setAmount(info.getAlertsWithdrawminamt());
								}
								alertList.add(alert);
							}
							if (hasAtmTotal && !info.getDlyWithdrawtotlschksms().equalsIgnoreCase("0")) {
								MobileAlert alert = new MobileAlert();
								alert.setType("atmtotal");
								alert.setActive(true);
								if (info.getDlyWithdrawtotlsminamtSpecified()) {
									alert.setAmount(info.getDlyWithdrawtotlsminamt());
								}
								alertList.add(alert);
							}
				    		
							if (!alertList.isEmpty()) {
								plastic.setAlerts(alertList);
							}
				    	}
				    	
				    } catch (Exception e) {
				    	LOG.error("There was an error getting the alerts", e);
				    }

					plastic.setId(plasticItem.getFrontEndId());
					plastic.setNickname(cardType);
					plastic.setNumber(maskUtil.mask(plasticPanId));
					
					plasticsList.add(plastic);

				}
				accountWithPlastics.setPlastics(plasticsList);
				accountsList.add(accountWithPlastics);
			}
			model.put("depositAccounts", gson.toJson(accountsList));
		}
		return new ModelAndView(ALERTS_JSON, model);
	}
	
	public ModelAndView mobileAlertsGetCCAlerts(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> model = new HashMap<>();
		
		UserSession userSession = getSession(request);
		CustomerServices customerService = userSession.getCustomerService();
		
		AccountMaskUtil acctMask = new AccountMaskUtil();
		
		List<AlertsAccount> mobileAccounts = new LinkedList<>();
		
		String filterComercial = getTsysAlertsAddIsCCAFilterCodeComercial();
		String filterRetail = getTsysAlertsAddIsCCAFilterCodeRetail();
		String filterCode = getTsysAlertsCreditCardFilterCode();
		String filterNTIPCode = getTsysAlertsCreditCardFilterNTIPCode();
		
		List<TVFrontendAccount> accountsLists = AlertTypesUtils.getCCAAccountsListForAlertsFiltered(userSession, filterComercial, filterRetail, filterCode, filterNTIPCode);
		
		for (TVFrontendAccount account : accountsLists) {
			if (isIneligibleAccount(account))
				continue;
			
			AlertsAccount mobileAccount = new AlertsAccount();
			
			mobileAccount.setId(account.getFrontEndId());
			mobileAccount.setNickname(account.getNickname());
			String suffix = account.getAccountNumberSuffix() == null ?  "" : account.getAccountNumberSuffix();
			mobileAccount.setNumber(acctMask.mask(account.getAccountNumber()) + " " + suffix);
			
			CardPlasticFrontend[] ccaPlastics = null;
			ccaPlastics = Utils.getCCACardPlastics(customerService, userSession.getCustomerProfile(),
					userSession.getBankingSession(), account);

			for (CardPlasticFrontend plastic : ccaPlastics) {
								
				AlertCard accountCard = new AlertCard();
				accountCard.nickname = plastic.getCardPlasticAccountRecord().getEmbossedName();
				accountCard.number = acctMask.mask(plastic.getCardPlasticAccountRecord().getCardPANId());
				
				mobileAccount.accountCards.add(accountCard);
			}
			
			mobileAccounts.add(mobileAccount);
			
		}
	
		model.put("creditAccounts", gson.toJson(mobileAccounts));
		
		return new ModelAndView(ALERTS_JSON, model);
	}
	
	public ModelAndView mobileAlertsGetEntitlementData(HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<String> pushAlerts = new ArrayList<>();
		Map<String, Object> model = new HashMap<>();

		UserSession session = getSession(request);
		CustomerProfile profile = session.getCustomerProfile();

		// For Personal Information, Payments and Transfers
		AlertsEntitlement entitlement = null;
		if (profile.hasEntitlement(EntitlementType.ALERTS))
			entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		if (entitlement != null && entitlement.getAlerts() != null) {
			Set<AlertType> customerAlerts = entitlement.getAlerts();

			for (AlertType alert : customerAlerts) {
				if (alert.name().contains(MobileAlert.PUSH) || alert.name().contains(MobileAlert.EMAIL)) {
					pushAlerts.add(alert.name());
				}
			}
		}

		model.put("entitlementData", gson.toJson(pushAlerts));
		
		return new ModelAndView(ALERTS_JSON, model);
	}
	
	/**
	 * CRUD method for balance alerts
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ModelAndView mobileAlertsUpdateBalanceAlert(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		UserSession userSession = getSession(request);
		Map<String, Object> model = new HashMap<>();
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();
		boolean updateEmail = Boolean.parseBoolean(request.getParameter("updateEmail"));
		boolean isEmailActive = Boolean.parseBoolean(request.getParameter("isEmailActive"));
		boolean isPushActive = Boolean.parseBoolean(request.getParameter("isPushActive"));
		String alertType = request.getParameter("alertType");
		String amount = request.getParameter("amount");

		AlertsEntitlement entitlement = null;
		boolean success = true;
		addEntitlementIfNewAlert(profile, customerService);
		entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));
		Set<AlertType> customeralerts = new HashSet<>();

		if (entitlement.getAlerts() != null)
			customeralerts = entitlement.getAlerts();
		
		ModifyCustomerAlertsRequest req = new ModifyCustomerAlertsRequest();
		req.setCustomerProfile(userSession.getCustomerProfile());
		CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request);
		req.setInteractionEvent(event);
		
		TVFrontendAccount account = Utils.getAccountById(request.getParameter("id"), userSession.getAccounts());
		
		if (updateEmail) {
			CustomerEmailBalanceAlert alert = new CustomerEmailBalanceAlert(account, Utils.getRSALanguage(request));
			alert.setCustomerProfile(profile);
			alert.setPermId(profile.getPermId());
			
			
			String alertIdEmail = Utils.findEmailAlert(userSession.getEmailaccounts(event), 
					account.getAccountNumber(),
					account.getAccountSection(), 
					account.getProductDefinition().getProductType(), 
					account.getProductDefinition().getProductId(), 
					alertType);
			
			if (isEmailActive) { 
				//If alert is to be activate, add balance alert type
				addBalanceAlertType(userSession, customeralerts, req);
				success = insertOrUpdateEmailBalanceAlert(userSession, event, alertIdEmail, alert, amount, alertType, account);
				
			} else { //Delete email alert
				success = deleteEmailBalanceAlert(alertIdEmail, alert, amount);
			}
			
			userSession.resetEmailAccounts();
		}
		

		// Update or create Balance alert 
		CustomerBalanceAlert balanceAlert = deleteCustomerBalanceAlertIfExist(userSession, event , account, alertType, isEmailActive, isPushActive);
		
		boolean isSavedPushActive = balanceAlert.isChannelPushActive();

		if (StringUtils.isNotEmpty(amount) && (isEmailActive || isPushActive || balanceAlert.isChannelSMSActive())) {//update alert
			AlertCondition alertCondition = new AlertCondition(alertType, new BigDecimal(amount));
			
			balanceAlert = Utils.updateCustomerBalanceAlert(balanceAlert, profile, Utils.getAlertLanguage(request), account);
			balanceAlert.setAlertCondition(alertCondition);
			balanceAlert.setIsChannelEmailActive(isEmailActive);
			balanceAlert.setIsChannelPushActive(isPushActive);
			if (isPushActive){
				balanceAlert.setIsChannelSMSActive(false);
				
				//delete SMSAccount (SMS alert)
				String smsAlertId = Utils.findSmsAlert(userSession.getSmsaccounts(event), account.getAccountNumber(),
						account.getAccountSection(), account.getProductDefinition().getProductType(),
						account.getProductDefinition().getProductId(), alertType);
				
				if (StringUtils.isNotEmpty(smsAlertId)) {
					customerService.DeleteSMSAccount(userSession.getSmsProfile(event), smsAlertId);
					userSession.resetSmsAccounts();
				}

			}
			
			boolean saveSuccess = true;
			
			try {
				AlertsBalanceFacade.getInstance().insertCustomerBalanceAlert(userSession, balanceAlert);
			} catch (Exception ex) {
				saveSuccess = false;
			}
						
			if (isSavedPushActive && !isPushActive) {
				//if is deleting an active push alert, log delete push
				userSession.getCustomerService().logPushNotificationDeleteEvent(Utils.updateCustomerBalanceAlertInteractionEvent(event, balanceAlert), saveSuccess);
			} else if (!isSavedPushActive && isPushActive) {
				// log insert push alert
				userSession.getCustomerService().logPushNotificationAddEvent(Utils.updateCustomerBalanceAlertInteractionEvent(event, balanceAlert), saveSuccess);
			} else if (isSavedPushActive) {
				// log modified alert
				userSession.getCustomerService().logPushNotificationUpdateEvent(Utils.updateCustomerBalanceAlertInteractionEvent(event, balanceAlert), saveSuccess);
			}
			
		}
		
		userSession.resetBalanceAlerts();

		model.put("success", success);
		return new ModelAndView(ALERTS_JSON, model);
	}
	
	/** Save Push Welcome Splash Decision 
	 * 
	 * @param request
	 * @param response
	 * @return Success response when the operations are completed
	 */
	public ModelAndView mobileAlertsSaveSplashDecision(HttpServletRequest request, HttpServletResponse response) {

		UserSession userSession = getSession(request);
		String decision = request.getParameter("decision");
		List<ActiveCampaignView> campaigns = userSession.getCustomerService().getActiveCampaignByType(CampaignType.PUSH_ALERTS);
		
		campaigns.forEach(c -> {
			
			switch (decision) {
			
			case "Y":
				//Save decision first, then migrate user if needed
				saveSplashStatus(userSession, ViewCampaign.VISTA, c.getId());
				//Migrate user alerts from SMS to Push, if costumer has MOBILE_BANKING
				if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)) {
					migrateAlertsFromSmsToPush(userSession, request);
				}
				break;
			case "N":
				saveSplashStatus(userSession, ViewCampaign.NO_GRACIAS, c.getId());
				break;     
			default: //Any other decision will be saved as skipped
				saveSplashStatus(userSession, ViewCampaign.OMITIDA, c.getId());
				break;
			}
			
		});
		
		//Success will be returned, letting the APP know the process is done 
		return new ModelAndView(ALERTS_JSON, Collections.singletonMap("success", true));
	}
	
	/** Migrate all customer alerts from SMS to Push
	 * 
	 * @param userSession
	 * @param request
	 */
	private void migrateAlertsFromSmsToPush(UserSession userSession, HttpServletRequest request) {

		CustomerProfile profile = userSession.getCustomerProfile();
		AlertsEntitlement entitlement = profile.hasEntitlement(EntitlementType.ALERTS) ? 
				new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS)) : null ;
		String errorMessage = "There was an error saving/updating one or more alerts, saving error to try again in batch proccess";
		
		if (entitlement != null && CollectionUtils.isNotEmpty(entitlement.getAlerts())) {
			//Migrate Personal Information and Payments|Transfers Alerts
			boolean success = migratePaymentsAndPersonalInfoAlerts(entitlement.getAlerts(), userSession, request);
			if (!success) {
				//save error in DB to trigger midnight migration
				LOG.error(errorMessage);
			}
		}
		
		//Now migrate Balance Alerts
		boolean balanceSuccess = migrateBalanceAlerts(userSession, request);
		if (!balanceSuccess) {
			//save error in DB to trigger midnight migration
			LOG.error(errorMessage);
		}
		
		//Now Migrate TSYS Alerts
		
		
		//Now Migrate PAXUS Alerts
		
		
	}
	
	
	/** Migrate Personal Information & Payments|Transfers Alerts
	 * 
	 * @param currentAlerts
	 * @param userSession
	 * @param request
	 * @return boolean if migration was successful
	 */
	private boolean migratePaymentsAndPersonalInfoAlerts(Set<AlertType> currentAlerts, UserSession userSession, HttpServletRequest request) {
		
		Set<AlertType> migratedAlerts = new HashSet<>(currentAlerts);
		boolean success = true;
		
		/**
		 * SMS and Push naming convention must be the same for this dynamic migration,
		 * As an example : CHANGE_PASSWORD_SMS -> CHANGE_PASSWORD_PUSH
		 */
		
		currentAlerts.forEach(alertType -> {
			
			if (alertType.name().endsWith(MobileAlert.SMS)) {
				String migratedAlert = alertType.name().replaceFirst(MobileAlert.SMS + "$", MobileAlert.PUSH);
				//After changing the name, check if the new alert exist in the AlertType class
				if (Enums.getIfPresent(AlertType.class, migratedAlert).isPresent()) {
					migratedAlerts.remove(alertType);
					migratedAlerts.add(AlertType.valueOf(migratedAlert));
				}
			}
				
		});

		//If both collections are not equals, that means an alert has been migrated, proceed to save 
		if (!CollectionUtils.isEqualCollection(currentAlerts, migratedAlerts)) {
			ModifyCustomerAlertsRequest req = new ModifyCustomerAlertsRequest();
			req.setCustomerProfile(userSession.getCustomerProfile());
			req.setAlerts(migratedAlerts);
			req.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
			try {
				userSession.setCustomerProfile(userSession.getCustomerService().modifyCustomerAlerts(req));
				LOG.info("Personal Information & Payments|Transfers Alerts Migrated from: " + currentAlerts.toString() 
				+ " , to: " + migratedAlerts.toString() + ".");
			} catch (Exception e) {
				//Saving failed, return false to trigger migration at a later time
				success = false;
			}
		}
		return success;
	}
	
	/** Migrate Balance Alerts from SMS to Push
	 * 
	 * @param userSession
	 * @param request
	 * @return boolean if migration was successful
	 */
	private boolean migrateBalanceAlerts(UserSession userSession, HttpServletRequest request) {
		
		CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request);
		CustomerServices services = userSession.getCustomerService();
		SMSProfile smsProfile = userSession.getSmsProfile(event);
		List<SMSAccount> smsAccounts = userSession.getSmsaccounts(event);
		
		boolean success = true;
		
		for (TVFrontendAccount account : userSession.getAccounts()) {
			
			if (!account.getFeatures().isMobileBanking()) continue;
			
			//Iterate all costumer SMS accounts, and check if there's a match with any TVFrontendAccount account
			for (SMSAccount smsAccount : smsAccounts) {
				
				if (Utils.matchAccountWithBalanceAlert(account, smsAccount)) {
					//Delete SMS account in Old DB Table
					services.DeleteSMSAccount(smsProfile, smsAccount.getId()); 
					
					String operator = smsAccount.getAlertCondition().getOperator();
					CustomerBalanceAlert balanceAlert = getBalanceAlert(account, userSession, operator, request);
					balanceAlert.setAlertCondition(MobileAlert.getBalanceAlertCondition(smsAccount, operator));
					//Turn on Push Notifications
					balanceAlert.setIsChannelPushActive(true);
					//Turn off SMS
					balanceAlert.setIsChannelSMSActive(false);
					
					try {
						// Insert|Update the configured balance alert
						AlertsBalanceFacade.getInstance().insertCustomerBalanceAlert(userSession, balanceAlert);
						services.logPushNotificationAddEvent(Utils.updateCustomerBalanceAlertInteractionEvent(event, balanceAlert), true);
						LOG.info("Inserting/updating balance Alert from SMS to Push, with id: " + balanceAlert.getId() + ".");
					} catch (Exception e) {
						success = false;
						services.logPushNotificationAddEvent(Utils.updateCustomerBalanceAlertInteractionEvent(event, balanceAlert), success);
						LOG.error("There was an error updating/creating the alert: " + balanceAlert.getId() + ".");
					}	
				}
				
			}
		}
		
		userSession.resetSmsAccounts();
		userSession.resetBalanceAlerts();
		return success;	
	}
	
	/** Get the current Balance Alert for the customer
	 * @param account
	 * @param session
	 * @param operator
	 * @param req
	 * @return CustomerBalanceAlert
	 */
	private CustomerBalanceAlert getBalanceAlert(TVFrontendAccount account, UserSession session, String operator, HttpServletRequest req) {
		
		CustomerBalanceAlert balanceAlert = Utils.findBalanceAlert(session.getBalanceAlerts(), account.getAccountNumber(),
				account.getAccountSection(), operator);
		
		if (balanceAlert == null)
			balanceAlert = new CustomerBalanceAlert();
			
		return Utils.updateCustomerBalanceAlert(balanceAlert, session.getCustomerProfile(), 
				Utils.getAlertLanguage(req), account);
	}
	
	
	/**
	 * Update SMS Alert amount if exist
	 * @param account, the account to update
	 * @param amount, the amount to be updated
	 * @param alertType, "GT or "LT"
	 * @param session, the user session
	 * @param event, the Customer Interaction Event
	 */
	private void updateSmsAlertAmount(TVFrontendAccount account, String amount, String alertType, UserSession session, CustomerInteractionEvent event) {
		
		String alertId = Utils.findSmsAlert(session.getSmsaccounts(event), account.getAccountNumber(),
				account.getAccountSection(), 
				account.getProductDefinition().getProductType(), 
				account.getProductDefinition().getProductId(), 
				alertType);
		
		SMSEntitlement smsEnt = null;
		CustomerProfile profile = session.getCustomerProfile();
		CustomerServices services = session.getCustomerService();
		if (profile.hasEntitlement(EntitlementType.MOBILE_BANKING))
			smsEnt = new SMSEntitlement(profile.getEntitlementByType(EntitlementType.MOBILE_BANKING));
		
		if (StringUtils.isNotEmpty(alertId) && smsEnt != null) {
			SMSProfile smsProfile = session.getSmsProfile(event);
			AddSMSAccountRequest alert = new AddSMSAccountRequest(event, profile,
					smsProfile, smsEnt, account.getAccountNumber(), account.getSubtype(),
					account.getAccountSection(), account.getNickname(), account.getProductDefinition().getProductId(), "X");
			alert.setCondition(alertType);
			if (StringUtils.equals(alertType, MobileAlert.GREATER_THAN)) {
				alert.setConditionHigh(new BigDecimal(amount));
			} else if (StringUtils.equals(alertType, MobileAlert.LESS_THAN)) {
				alert.setConditionLow(new BigDecimal(amount));
			}
			services.DeleteSMSAccount(smsProfile, alertId);
			services.addSMSAccountWithAlertCondition(alert);
			session.resetSmsAccounts();
		}
	}
	
	/**
	 * Insert or Update an email balance alert
	 * 
	 * @param session
	 * @param event
	 * @param alertIdEmail
	 * @param alert
	 * @param amount
	 * @param alertType
	 * @param account
	 * @return true if the process was successful
	 */
	private boolean insertOrUpdateEmailBalanceAlert(UserSession session, CustomerInteractionEvent event, String alertIdEmail, CustomerEmailBalanceAlert alert,
			String amount, String alertType, TVFrontendAccount account) {
		boolean success = true;
		alert.setAlertCondition(createAlertCondition(alertType, amount));
		if (StringUtils.isNotEmpty(alertIdEmail)) {
			alert.setId(alertIdEmail);
			CustomerEmailBalanceAlert confirmation = AlertsFacade.getInstance().updateCustomerEmailBalanceAlert(alert, session);;
			if (confirmation != null) 
				updateSmsAlertAmount(account, amount, alertType, session, event);
			else 
				success = false;
		} else { 
			/**
			 * MBNA-3499
			 * Method InsertCustomerEmailBalanceAlert : CIBP-WEB microservice integration
			 */
			CustomerEmailBalanceAlert confirmation = AlertsFacade.getInstance().insertCustomerEmailBalanceAlert(alert, session);
			if (confirmation != null)
				updateSmsAlertAmount(account, amount, alertType, session, event);
			else
				success = false;
		}
		
		return success;
	}
	
	/**
	 * Delete the current email balance alert
	 * 
	 * @param alertIdEmail
	 * @param alertType
	 * @param alert
	 * @param amount
	 * @return true if the alert was successfully deleted
	 */
	private boolean deleteEmailBalanceAlert(String alertIdEmail, CustomerEmailBalanceAlert alert, String amount) {
		boolean success = true;
		if (StringUtils.isNotEmpty(alertIdEmail)) {
			alert.setId(alertIdEmail);
			/**
			 * MBNA-3493
			 * Method DeleteCustomerEmailBalanceAlert :CIBP-WEB microservice integration
			 * 
			 * */
			success = AlertsFacade.getInstance().deleteCustomerEmailBalanceAlert(alert);
		} else {
			success = false;
		}
		return success;
	}
	
	/**
	 * Add the Balance AlertType if setting an alert for the first time
	 * @param session
	 * @param customeralerts
	 * @param req
	 */
	private void addBalanceAlertType(UserSession session, Set<AlertType> customeralerts, ModifyCustomerAlertsRequest req) {
		if (!customeralerts.contains(AlertType.BALANCE_EMAIL)) {
			customeralerts.add(AlertType.BALANCE_EMAIL);
			req.setAlerts(customeralerts);
			session.setCustomerProfile(session.getCustomerService().modifyCustomerAlerts(req));
		}
	}
	
	
	/**
	 * Add all current email balance alerts to the passed MobileAlert list
	 * @param account
	 * @param emailAccounts
	 * @param alerts
	 * @param customerAlerts
	 */
	private void addEmailAlerts(TVFrontendAccount account, List<CustomerEmailBalanceAlert> emailAccounts, List<MobileAlert> alerts, Set<AlertType> customerAlerts) {
		if (customerAlerts.contains(AlertType.BALANCE_EMAIL)) {
			for (CustomerEmailBalanceAlert emailAlert : emailAccounts) {
				if (Utils.matchAccountWithBalanceAlert(account, emailAlert)) {
					alerts.add(MobileAlert.getMobileAlert(emailAlert));
				}
			}
		}
	}
	
	/**
	 * Add all current email/push/SMS balance alerts to the passed MobileAlert list
	 * @param account
	 * @param balanceAlerts
	 * @param alerts
	 */
	private void addBalanceAlerts(TVFrontendAccount account, List<CustomerBalanceAlert> balanceAlerts, List<MobileAlert> alerts) {
		if (balanceAlerts != null && !balanceAlerts.isEmpty()) {
			for (CustomerBalanceAlert balanceAlert : balanceAlerts) {
				if (Utils.matchAccountWithBalanceAlert(account, balanceAlert)) {
					alerts.add(MobileAlert.getMobileAlert(balanceAlert));
				}
			}
		}
	}
	
	/**
	 * Add all current SMS Balance Alerts and add it to the provided MobileAlert List
	 * @param account
	 * @param smsAccounts
	 * @param alerts
	 * @param profile
	 */
	private void addSMSAlerts(TVFrontendAccount account, List<SMSAccount> smsAccounts, List<MobileAlert> alerts, CustomerProfile profile) {
		if (profile.hasEntitlement(EntitlementType.MOBILE_BANKING)) {
			for (SMSAccount smsAlert : smsAccounts) {
				if (Utils.matchAccountWithBalanceAlert(account, smsAlert)) {
					alerts.add(MobileAlert.getMobileAlert(smsAlert));
				}
			}
		}
	}
	
	/**
	 * Show Alerts View segment bar if has more than two account types
	 * 
	 * @param session
	 * @return boolean, if the segment bar should be shown
	 */
	private boolean showSegmentBar (UserSession session) {
		boolean hasDDA = session.hasVisibleDDA();
		boolean hasCCA = session.hasVisibleCCA();
		boolean hasPremia = session.hasVisiblePremia();
		//Awesome logic that's hard to understand... but awesome!
		return (hasDDA == hasCCA) ? hasDDA : hasPremia;
	}
	
	/**
	 * Add Alerts entitlement if customer doesn't have it
	 * @param profile
	 * @param services
	 */
	private void addEntitlementIfNewAlert(CustomerProfile profile, CustomerServices services) {
		if (!profile.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement alert = new AlertsEntitlement();
			services.getCustomerProfileService().addEntitlement(profile, alert.getCustomerEntitlement());
		}
	}
	
	/**
	 * Checks if account is null, hidden or is not mobile banking
	 * @param account
	 * @return
	 */
	private boolean isIneligibleAccount(TVFrontendAccount account) {
		return account == null || account.getHide() || !account.getFeatures().isMobileBanking();
	}
	
	/**
	 * Delete the current CustomerBalanceAlert if exist && push, email and SMS are disabled
	 * @param userSession
	 * @param account
	 * @param alertType
	 * @param isEmailActive
	 * @param isPushActive
	 * @return a CustomerBalanceAlert object
	 */
	private CustomerBalanceAlert deleteCustomerBalanceAlertIfExist(UserSession userSession, CustomerInteractionEvent event, TVFrontendAccount account,
			String alertType, boolean isEmailActive, boolean isPushActive) {
		
		CustomerBalanceAlert balanceAlert = Utils.findBalanceAlert(userSession.getBalanceAlerts(), account.getAccountNumber(),
				account.getAccountSection(), alertType);
	
		if (balanceAlert != null) {
			if (!isEmailActive && !isPushActive && !balanceAlert.isChannelSMSActive()) {
				//delete alert

				boolean success = AlertsBalanceFacade.getInstance().deleteCustomerBalanceAlert(userSession, balanceAlert);
				
				if (balanceAlert.isChannelPushActive() && !isPushActive) {
					//if is deleting an active push alert, log delete push
					userSession.getCustomerService().logPushNotificationDeleteEvent(Utils.updateCustomerBalanceAlertInteractionEvent(event, balanceAlert), success);
				}
			}						
		} else {
			balanceAlert = new CustomerBalanceAlert();
		}
		return balanceAlert;
	}
	
	
	
	/**
	 * Creates an EmailAlertCondition depending on the passed alertType
	 * @param alertType, can be "GT" or "LT"
	 * @param amount
	 * @return an EmailAlertCondition Object
	 */
	private EmailAlertCondition createAlertCondition(String alertType, String amount) {
		EmailAlertCondition alertCond = new EmailAlertCondition();
		if (StringUtils.equals(alertType, MobileAlert.GREATER_THAN)) {
			alertCond.setAmountHigh(new BigDecimal(amount));
		} else {
			alertCond.setAmountLow(new BigDecimal(amount));
		}
		alertCond.setOperator(alertType);
		return alertCond;
	}
	
	private boolean saveSplashStatus(UserSession userSession, Integer statusCampaign, String campaignId) {
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		
		ViewCampaignId id = new ViewCampaignId();
		id.setIdCampaign(campaignId);
		id.setProfileId(customerProfile.getProfileId());

		ViewCampaign vc = new ViewCampaign();
		vc.setId(id);
		vc.setStatus(statusCampaign);
		vc.setType(CampaignType.PUSH_ALERTS);
		vc.setViewDate(Calendar.getInstance().getTime());
		
		try {
			ViewCampaign vcExist = customerService.getViewCampaign(customerProfile, vc.getId().getIdCampaign());
			if (vcExist != null) {
				vc.setCount(vcExist.getCount() + NumberUtils.INTEGER_ONE);
			} else {
				vc.setCount(NumberUtils.INTEGER_ONE);
			}
			customerService.mergeViewCampaign(vc);

			return true;

		} catch (Exception e) {
			LOG.error("Exception saving Push Alerts Campaign decision: " + e.getMessage());
			return false;
		}
	}
	
	
	public String getAlertsProductTypes() {
		return alertsProductTypes;
	}

	public void setAlertsProductTypes(String alertsProductTypes) {
		this.alertsProductTypes = alertsProductTypes;
	}
	
	public String getTsysAlertsCreditCardFilterCode() {
		return tsysAlertsCreditCardFilterCode;
	}

	public void setTsysAlertsCreditCardFilterCode(String tsysAlertsCreditCardFilterCode) {
		this.tsysAlertsCreditCardFilterCode = tsysAlertsCreditCardFilterCode;
	}

	public String getTsysAlertsAddIsCCAFilterCodeComercial() {
		return tsysAlertsAddIsCCAFilterCodeComercial;
	}

	public void setTsysAlertsAddIsCCAFilterCodeComercial(String tsysAlertsAddIsCCAFilterCodeComercial) {
		this.tsysAlertsAddIsCCAFilterCodeComercial = tsysAlertsAddIsCCAFilterCodeComercial;
	}

	public String getTsysAlertsAddIsCCAFilterCodeRetail() {
		return tsysAlertsAddIsCCAFilterCodeRetail;
	}

	public void setTsysAlertsAddIsCCAFilterCodeRetail(String tsysAlertsAddIsCCAFilterCodeRetail) {
		this.tsysAlertsAddIsCCAFilterCodeRetail = tsysAlertsAddIsCCAFilterCodeRetail;
	}

	public String getTsysAlertsCreditCardFilterNTIPCode() {
		return tsysAlertsCreditCardFilterNTIPCode;
	}

	public void setTsysAlertsCreditCardFilterNTIPCode(String tsysAlertsCreditCardFilterNTIPCode) {
		this.tsysAlertsCreditCardFilterNTIPCode = tsysAlertsCreditCardFilterNTIPCode;
	}
	
	/**
	 * Returns User Session
	 * @param request
	 * @return the current user session
	 */
	private UserSession getSession(HttpServletRequest request) {
		return  (UserSession) WebUtils.getSessionAttribute(request, "userSession"); 
	}

	public class AlertsAccount {
		private String id;
		private String nickname;
		private String number;
		private List<AlertCard> accountCards;

		public AlertsAccount() {
			accountCards = new LinkedList<>();
		}
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getNickname() {
			return nickname;
		}
		public void setNickname(String nickname) {
			this.nickname = nickname;
		}
		public String getNumber() {
			return number;
		}
		public void setNumber(String number) {
			this.number = number;
		}
		public List<AlertCard> getAccountCards() {
			return accountCards;
		}		
		public void setAccountCards(List<AlertCard> accountCards) {
			this.accountCards = accountCards;
		}

	}
	
	public class AlertCard {
		private String id;
		private String nickname;
		private String number;
		private List<AlertCCA> cardAlerts;

		public AlertCard() {
			cardAlerts = new LinkedList<>();
		}
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getNickname() {
			return nickname;
		}
		public void setNickname(String nickname) {
			this.nickname = nickname;
		}
		public String getNumber() {
			return number;
		}
		public void setNumber(String number) {
			this.number = number;
		}
		public List<AlertCCA> getCardAlerts() {
			return cardAlerts;
		}
		public void setCardAlerts(List<AlertCCA> cardAlerts) {
			this.cardAlerts = cardAlerts;
		}
		
	}
	
	public class AlertCCA {
		private String id;
		private String nickname;
		private String amount;
		private boolean isPushActive;
		
		public AlertCCA() {
		}
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getNickname() {
			return nickname;
		}
		public void setNickname(String nickname) {
			this.nickname = nickname;
		}
		public String getNumber() {
			return amount;
		}
		public void setNumber(String amount) {
			this.amount = amount;
		}
		public boolean isPushActive() {
			return isPushActive;
		}
		public void setPushActive(boolean isPushActive) {
			this.isPushActive = isPushActive;
		}
	}

}
