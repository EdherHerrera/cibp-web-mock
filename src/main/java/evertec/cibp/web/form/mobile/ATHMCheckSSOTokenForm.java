package evertec.cibp.web.form.mobile;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.ATHMovilEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.mobile.bean.ATHMCheckSSOTokenBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.AthmSsoTokenStatusReq;
import evertec.otherservices.responses.AthmSsoTokenStatusResp;

public class ATHMCheckSSOTokenForm extends SimpleFormController {
	
	private static final String VIEW_NAME = "athmssotoken";
	
	protected CustomerServices customerService;	
	
	public ATHMCheckSSOTokenForm() {
		super();
		setCommandClass(ATHMCheckSSOTokenBean.class);
		setCommandName(VIEW_NAME);
		setFormView(VIEW_NAME);
	}
	
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		
		Map<String, Object> model = new LinkedHashMap<>();
		ATHMCheckSSOTokenBean bean = (ATHMCheckSSOTokenBean) command;
		Boolean downtime = false;
		Boolean bound = false;
		
		Boolean flag = StaticMessageSource.isFlagEnabled(StaticMessageSource.ATHM_SSO);
		model.put("flag", flag);
		if (!flag)
			return showForm(request, error, VIEW_NAME, model);
		
		GlobalEntitlement entitlement = customerService.getGlobalEntitlement(EntitlementType.ATHMOVIL);
		if (entitlement == null || entitlement.getStatus() != EntitlementStatus.ENABLED) {
			model.put("downtime", true);
			return showForm(request, error, VIEW_NAME, model);
		}
		
		// Get SSO token status
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		AthmSsoTokenStatusReq athmReq = new AthmSsoTokenStatusReq();
		athmReq.setAppUserId(this.getCustomerToken(userSession, false));
		athmReq.setAppToken(bean.getAppToken());
		
		AthmSsoTokenStatusResp athmResp = customerService.getSsoTokenStatus(athmReq);
		
		if (athmResp != null && athmResp.getResponseStatus().equals(ResponseStatus.SUCCESS) &&
				("200".equals(athmResp.getStatusCode()) || "98".equals(athmResp.getStatusCode()))) {
			
			// Generate a new user token in case it was intentionally deactivated in ATHM
			if ("98".equals(athmResp.getStatusCode())) {
				if (bean.isTokenGenerationActive()) {
					athmReq.setAppUserId(this.getCustomerToken(userSession, true));
				} else {
					this.deleteEntitlement(userSession.getCustomerProfile());
					athmReq.setAppUserId(null);
				}
			}
			
			model.put("token", athmReq.getAppUserId());
			bound = athmResp.getStatus();
				
		} else {	
			logger.error(this.getErrorText(athmResp));
			downtime = true;
		}
		
		model.put("bound", bound);
		model.put("downtime", downtime);
		
		return showForm(request, error, VIEW_NAME, model);
	}
	
	private String getCustomerToken(UserSession userSession, Boolean generateToken) {
		
		ATHMovilEntitlement entitlement = null;
		
		if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.ATHMOVIL)) {
			entitlement = new ATHMovilEntitlement(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ATHMOVIL));
			
			if (GenericValidator.isBlankOrNull(entitlement.getCustomerToken()) || generateToken) {
				entitlement.setCustomerToken(Utils.generateUUID());
				CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(entitlement.getCustomerEntitlement()));
			}
			
		} else {
			entitlement = new ATHMovilEntitlement(Utils.generateUUID());
			customerService.addEntitlement(userSession.getCustomerProfile(), entitlement.getCustomerEntitlement());
		}
		
		return entitlement.getCustomerToken();
	}
	
	private void deleteEntitlement(CustomerProfile profile) {
		
		if (profile.hasEntitlement(EntitlementType.ATHMOVIL)) {
			ATHMovilEntitlement entitlement = new ATHMovilEntitlement(profile.getEntitlementByType(EntitlementType.ATHMOVIL));
			
			if (GenericValidator.isBlankOrNull(entitlement.getPhoneNumber()))
				customerService.getCustomerProfileService().removeEntitlement(profile, entitlement.getCustomerEntitlement());
		}
	}

	private String getErrorText(AthmSsoTokenStatusResp athmResp) {
		
		String errorText = "ATH Movil SSO token status check generated an exception.";
		
		if (athmResp != null) {
			if (!GenericValidator.isBlankOrNull(athmResp.getResponseStatus().toString()))
				errorText += " responseStatus: " + athmResp.getResponseStatus().toString();
			if (!GenericValidator.isBlankOrNull(athmResp.getStatusCode()))
				errorText += " statusCode: " + athmResp.getStatusCode();
			if (!GenericValidator.isBlankOrNull(athmResp.getStatusDescription()))
				errorText += " statusDescription: " + athmResp.getStatusDescription();
		}
		
		return errorText;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

}
