package evertec.cibp.web.form.mobile;

import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.EventType;

import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.ValidationQuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.web.form.bean.QuestionChallengeBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MobileCheckRsaChallengeForm extends CancellableFormController {
	
	private static final String VIEW_NAME = "mobileCheckRsaChallenge";
	private static final String LOGIN_REDIRECT = "forward:login";
	
	private final static Logger log = Logger.getLogger(MobileCheckRsaChallengeForm.class);
	
	public MobileCheckRsaChallengeForm() {
		super();
		setCommandClass(QuestionChallengeBean.class);
		setCommandName(VIEW_NAME);
		setFormView(VIEW_NAME);
	}
	
	
	@Override
	protected ModelAndView showForm(HttpServletRequest request, HttpServletResponse response, BindException errors) 
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		String challenge = "";
		if (session.getOobEnroll()) {
			if (!session.isOobAuthenticated()) {
				challenge = "OOB";
			}
		} else {
			if (!session.isInformationChallenge()) {
				boolean showQuestion;
				if (session.getInformationQuestion() == null)
					showQuestion = questionChallenge(request, session);
				else
					showQuestion = true;
				
				if (showQuestion) {
					challenge = "QUESTION";
					model.putAll(this.getQuestion(request, session));
				}
			}
		}
		
		if (model.containsKey("rsa_blocked") && (boolean) model.get("rsa_blocked"))
			return new ModelAndView(LOGIN_REDIRECT, model);
			
		model.put("challenge", challenge);
		
		return showForm(request, errors, VIEW_NAME, model);
	}
	
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		
		Map<String, Object> model = new HashMap<>();
		QuestionChallengeBean bean = (QuestionChallengeBean) command;
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();

		String answer = bean.getAnswer();
		if (GenericValidator.isBlankOrNull(answer)) {
			errors.rejectValue("answer", "login.username.required", "Required");
			return showForm(request, response, errors);
		}
		
		boolean dummyQuestions = (profile == null);
		
		RSAActionRequest rsaActionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(
				request, session.getUsername()), session.getUsername(), answer, dummyQuestions ? null
						: profile.getEmail(), 0, session.isRemember(), session.getDeviceRequest());
		
		rsaActionRequest.setUsername(session.getUsername());
		rsaActionRequest.setRsaSessionId(session.getRsaSessionId());
		rsaActionRequest.setRsaTransactionId(session.getRsaTransactionId());
		rsaActionRequest.setQuestion(session.getInformationQuestion());
		rsaActionRequest.setRsaLang(null);
		
		ValidationQuestionResponse responseAnswer = session.getCustomerService().getSignOnCustomerServiceModule().validateRSAQuestion(rsaActionRequest);
		session.setQuestion(null);
		session.setRsaSessionId(responseAnswer.getRsaSessionId());
		session.setRsaTransactionId(responseAnswer.getRsaTransactionId());

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");
		cookie.setPath("/");
		cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie()); 
		ESAPI.httpUtilities().addCookie(response, cookie);

		
		if (ResultStatus.SUCCESS.equals(responseAnswer.getStatusResult())) {
			session.setInformationChallenge(true);
			session.setQuestionAnswered(true);
			model.put("success", true);
			
		} else if (ResultStatus.BLOCKED.equals(responseAnswer.getStatusResult())) {
			boolean hasalert = false;
			AlertsEntitlement ent = new AlertsEntitlement(session.getCustomerProfile().getEntitlementByType(
					EntitlementType.ALERTS));
			if (ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
				Set<AlertType> alerts = ent.getAlerts();
				if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
						|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
					hasalert = true;
				}
			}
			
			if (hasalert) {
				session.getCustomerService().sendQuestionsBlockedConfirmation(session.getCustomerProfile(),
						session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
						session.getPreferredLanguage());
			}

			return new ModelAndView("redirect:login?username=" + session.getUsername());
			
		} else if (ResultStatus.EXCEPTION.equals(responseAnswer.getStatusResult())) {
			return new ModelAndView(LOGIN_REDIRECT, model);
		
		} else if (ResultStatus.ERROR.equals(responseAnswer.getStatusResult())) {
			session.setInformationQuestion(null);
			model.put("success", false);
			model.putAll(this.getQuestion(request, session));
			
		}
		
		return showForm(request, errors, VIEW_NAME, model);
	}

	private boolean questionChallenge(HttpServletRequest request, UserSession session) {

		String language = Utils.getRSALanguage(request);

		final String deviceInfoRsa = request.getParameter("device_info_rsa");
		if (!GenericValidator.isBlankOrNull(request.getParameter("device_info_rsa"))) {
			if (!GenericValidator.isBlankOrNull(request.getParameter("rsa_cookie"))) {
				session.getDeviceRequest().setDeviceTokenCookie(request.getParameter("rsa_cookie"));
			}
		}
		
		final boolean isSDKMobileRequest = !GenericValidator.isBlankOrNull(deviceInfoRsa);
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(session.getPm_fp(),session.getDeviceRequest().getDeviceTokenCookie(), 
				deviceInfoRsa, request, isSDKMobileRequest);

		RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, session.getUsername(), EventType.SESSION_SIGNIN.getValue());
		AnalyzeResponse anResp = session.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

		deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
		deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

		session.setRsaSessionId(anResp.getIdentificationData().getSessionId());
		session.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());
		session.setDeviceRequest(deviceRequest);

		if (ActionCode.CHALLENGE == anResp.getRiskResult().getTriggeredRule().getActionCode()) {
			return true;
		} else {
			session.setInformationChallenge(true);
			return false;
		}

	}
	
	private Map<String, Object> getQuestion(HttpServletRequest request, UserSession userSession) {
		
		Map<String, Object> model = new HashMap<>();
		String question = "";
		Boolean error = false;
		Boolean rsaBlocked = false;
		
		String email = null;
		if (userSession.getCustomerProfile() != null)
			email = userSession.getCustomerProfile().getEmail();

		QuestionResponse personalQuestion;

		if (userSession.getInformationQuestion() == null) {
			RSAActionRequest actionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(request,
					userSession.getUsername()), userSession.getUsername(), "", email, 0, userSession.isRemember(),
					userSession.getDeviceRequest());

			actionRequest.setUsername(userSession.getUsername());
			actionRequest.setDeviceRequest(userSession.getDeviceRequest());
			actionRequest.setRsaSessionId(userSession.getRsaSessionId());
			actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());

			actionRequest.setRsaLang(Utils.getRSALanguage(request));

			personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);
			
			model.put("rsa_cookie", actionRequest.getDeviceRequest().getDeviceTokenCookie());
		} else {
			personalQuestion = new QuestionResponse();
			personalQuestion.setStatusResult(ResultStatus.SUCCESS);
			personalQuestion.setQuestion(userSession.getInformationQuestion());
			model.put("rsa_cookie", userSession.getDeviceRequest().getDeviceTokenCookie());
		}
		
		if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
			userSession.setInformationQuestion(personalQuestion.getQuestion());
			question = (String) personalQuestion.getQuestion().values().toArray()[0];
		} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
			error = true;
		} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
			rsaBlocked = true;
		}
		
		model.put("question", question);
		model.put("error", error);
		model.put("rsa_blocked", rsaBlocked);
		
		return model;
		
	}

}
