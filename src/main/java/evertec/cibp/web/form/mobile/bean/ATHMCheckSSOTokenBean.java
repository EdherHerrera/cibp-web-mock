package evertec.cibp.web.form.mobile.bean;

public class ATHMCheckSSOTokenBean {
	
	private String appToken;
	
	private boolean tokenGenerationActive;

	public String getAppToken() {
		return appToken;
	}

	public void setAppToken(String appToken) {
		this.appToken = appToken;
	}

	public boolean isTokenGenerationActive() {
		return tokenGenerationActive;
	}

	public void setTokenGenerationActive(boolean tokenGenerationActive) {
		this.tokenGenerationActive = tokenGenerationActive;
	}
	
}
