package evertec.cibp.web.form.mobile.bean;

public class MobileRefreshBalancesBean extends MobileAbstractBean {
	
	private String accountFrontEndId;

	public String getAccountFrontEndId() {
		return accountFrontEndId;
	}

	public void setAccountFrontEndId(String accountFrontEndId) {
		this.accountFrontEndId = accountFrontEndId;
	}

}
