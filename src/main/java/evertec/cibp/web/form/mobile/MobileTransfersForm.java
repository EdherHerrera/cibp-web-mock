package evertec.cibp.web.form.mobile;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.models.transfer.TVAccountTransferTarget;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.MobileResponseStatus;
import evertec.cibp.web.form.mobile.bean.MobileTransfersBean;
import evertec.cibp.web.response.MobileDeviceProfileResponse;
import evertec.cibp.web.response.MobileTransferResponse;
import evertec.cibp.web.utils.MobileUtil;
import evertec.cibp.web.utils.requests.GetAccountsRequest;
import evertec.cibp.web.utils.requests.MobileTransferRequest;

public class MobileTransfersForm extends CancellableFormController {

	protected final Log logger = LogFactory.getLog(getClass());
	private CustomerServices customerService;
	
	private MessageSource messageSource;

	public MobileTransfersForm() {
		super();
		setCommandClass(MobileTransfersBean.class);
		setCommandName("mobileMakeTransfer");
		setFormView("mobileMakeTransfer");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object submit,
			BindException error) throws Exception {
		List<TVFrontendAccount> accounts = new LinkedList<TVFrontendAccount>();
		Map<String, Object> model = new HashMap<>();

		MobileTransfersBean mobileTransferBean = (MobileTransfersBean) submit;

		if(customerService == null) {
			logger.error("CustomerService is null");
		} else if(GenericValidator.isBlankOrNull(mobileTransferBean.getDeviceToken()) 
				|| GenericValidator.isBlankOrNull(mobileTransferBean.getAmount())
				 || GenericValidator.isBlankOrNull(mobileTransferBean.getFromAccountNumber())
				  || GenericValidator.isBlankOrNull(mobileTransferBean.getFromAccountProductId())
				   || GenericValidator.isBlankOrNull(mobileTransferBean.getFromAccountSection())
				    || GenericValidator.isBlankOrNull(mobileTransferBean.getFromAccountType())
				     || GenericValidator.isBlankOrNull(mobileTransferBean.getToAccountNumber())
				      || GenericValidator.isBlankOrNull(mobileTransferBean.getToAccountProductId())
				       || GenericValidator.isBlankOrNull(mobileTransferBean.getToAccountSection())
				        || GenericValidator.isBlankOrNull(mobileTransferBean.getToAccountType())
				) {
			logger.error("All fields are required");
		} else {
			
			MobileDeviceProfileResponse deviceProfileRespnse = MobileUtil.getProfileByDeviceToken(request, mobileTransferBean.getDeviceToken(), customerService);
			
			if(deviceProfileRespnse != null && deviceProfileRespnse.getProfile() != null && deviceProfileRespnse.getStatus() == MobileResponseStatus.SUCCESS) {
				BankingSession bankingSession = MobileUtil.getBankingSession(request, deviceProfileRespnse.getProfile(), customerService);

				accounts = MobileUtil.getAccounts(request, bankingSession, deviceProfileRespnse.getProfile(), customerService);
				
				if(accounts != null && !accounts.isEmpty()) {
					String fromAccountNumber = mobileTransferBean.getFromAccountNumber();
					if(!GenericValidator.isBlankOrNull(fromAccountNumber) && fromAccountNumber.length() > 4) {
						fromAccountNumber = fromAccountNumber.substring(fromAccountNumber.length() - 4);
					}
					String fromAccountProductId = mobileTransferBean.getFromAccountProductId();
					String fromAccountSection = mobileTransferBean.getFromAccountSection();
					String fromAccountType = mobileTransferBean.getFromAccountType();

					String toAccountNumber = mobileTransferBean.getToAccountNumber();
					if(!GenericValidator.isBlankOrNull(toAccountNumber) && toAccountNumber.length() > 4) {
						toAccountNumber = toAccountNumber.substring(toAccountNumber.length() - 4);
					}
					String toAccountProductId = mobileTransferBean.getToAccountProductId();
					String toAccountSection = mobileTransferBean.getToAccountSection();
					String toAccountType = mobileTransferBean.getToAccountType();

					String amount = mobileTransferBean.getAmount();

					if(!GenericValidator.isBlankOrNull(fromAccountNumber) || !GenericValidator.isBlankOrNull(toAccountNumber)) {
						GetAccountsRequest utilRequest = new GetAccountsRequest();
						utilRequest.setAccounts(accounts);

						utilRequest.setAccountNumber(fromAccountNumber);
						utilRequest.setProductId(fromAccountProductId);
						utilRequest.setSection(fromAccountSection);
						utilRequest.setType(fromAccountType);

						TVFrontendAccount theSourceAccount = MobileUtil.getAccountByAccountNumberAndTypeAndProductIdAndSection(utilRequest);

						utilRequest.setAccountNumber(toAccountNumber);
						utilRequest.setProductId(toAccountProductId);
						utilRequest.setSection(toAccountSection);
						utilRequest.setType(toAccountType);

						TVFrontendAccount theToAccount = MobileUtil.getAccountByAccountNumberAndTypeAndProductIdAndSection(utilRequest);

						if(theSourceAccount != null && theToAccount != null) {
							MobileTransferRequest trfRequest = this.createMobileTransferRequest(theSourceAccount, theToAccount, amount, deviceProfileRespnse.getProfile());

							if(trfRequest != null) {
								MobileTransferResponse transferResponse = MobileUtil.makeMobileTransfer(request, trfRequest, bankingSession, 
										customerService, deviceProfileRespnse.getProfile(), messageSource);

								if(transferResponse != null && transferResponse.getStatus() != null) {
									model.put("description", transferResponse.getDescription());
									model.put("status", transferResponse.getStatus());

								} else {
									logger.error("Transfer Response was null");
								}
							} else {
								logger.error("Could not create Transfer Request correctly");
							}
						} else {
							logger.error("One of the accounts were not found in the relations. Source:" +fromAccountNumber+" Target:"+toAccountNumber);
						}

					} else {
						logger.error("The accounts number source and target are required.");
					}

				} else {
					logger.error("Accounts List was empty");
				}
				bankingSession = null;
			} else {
				logger.error("There was an error retrieving the device for the customer. Device Token: "+mobileTransferBean.getDeviceToken());
			}
		}

		
		return showForm(request, error, "mobileMakeTransfer", model);
	}
	
	private MobileTransferRequest createMobileTransferRequest(TVFrontendAccount source, TVFrontendAccount target, String amount, CustomerProfile profile) {
		
		try{
			MobileTransferRequest trfRequest = new MobileTransferRequest();

			TVAccountTransferSource theSource = new TVAccountTransferSource(source);
			TVAccountTransferTarget theTarget = new TVAccountTransferTarget(target);
			BigDecimal theAmount = new BigDecimal(amount);

			trfRequest.setSou(theSource);
			trfRequest.setTar(theTarget);
			trfRequest.setProfile(profile);
			trfRequest.setAmount(theAmount);
			
			return trfRequest;
		} catch (Exception e) {
			logger.error("There was an error creating MobileTransferRequest Object: "+e);
		}
		
		
		return null;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
}
