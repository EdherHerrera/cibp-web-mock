package evertec.cibp.web.form.mobile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.http.HttpHost;
import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.script.mustache.SearchTemplateRequest;
import org.elasticsearch.script.mustache.SearchTemplateResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

import evertec.cibp.core.models.customer.CustomerViewedNotification;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.UnicaApplication;
import evertec.cibp.web.models.customer.CustomerMessageContent;
import evertec.cibp.web.models.customer.CustomerMessageContentType;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.UnicaUtils;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.unica.UnicaApplicationInfo;

/**
 * @author Raul Mora
 * @version 1.0
 * @since 1.0
 * @see
 *
 */
public class MobileNotificationCenterController extends MultiActionController {

	private static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
	private static final Logger LOGGER = Logger.getLogger(MobileNotificationCenterController.class);
	private static final String PUSH_NOTIFICATION_MSG_PROPERTY = "transaction.request.send.messages.push.message.notification.body";

	private MessageSource messageSource;
	private String elasticsearchEndpoint;
	private String elasticsearchIndexPush;
	private String elasticsearchTemplatePush;
	private long pushRetrievalDays;
	private String unicaLoanProducts;
	private String unicaCreditProducts;
	private String marketplaceProducts;
	private String unicaAllowedStatus;
	private String marketplaceAllowedStatus;
	
	/**
	 * Default constructor
	 */
	public MobileNotificationCenterController() {
		
	}
	
	/**
	 * Properties injection constructor
	 */
	public MobileNotificationCenterController(MessageSource messageSource, String elasticsearchEndpoint,
			String elasticsearchIndexPush, String elasticsearchTemplatePush, long pushRetrievalDays,
			String unicaLoanProducts, String unicaCreditProducts, String marketplaceProducts,
			String unicaAllowedStatus, String marketplaceAllowedStatus) {
		this.messageSource = messageSource;
		this.elasticsearchEndpoint = elasticsearchEndpoint;
		this.elasticsearchIndexPush = elasticsearchIndexPush;
		this.elasticsearchTemplatePush = elasticsearchTemplatePush;
		this.pushRetrievalDays = pushRetrievalDays;
		this.unicaLoanProducts = unicaLoanProducts;
		this.unicaCreditProducts = unicaCreditProducts;
		this.marketplaceProducts = marketplaceProducts;
		this.unicaAllowedStatus = unicaAllowedStatus;
		this.marketplaceAllowedStatus = marketplaceAllowedStatus;
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
    public ModelAndView notificationCenter (HttpServletRequest request, HttpServletResponse response) throws Exception {
        final Logger log = LOGGER;

        Map<String, Object> model = new HashMap<>(1);
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        List<CustomerMessageContent> customerMessageContents = userSession.getMessagesList();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99
        
        addCreditApplications(request, customerMessageContents);
        
        boolean isNewNotification = false;
        
        //GET CUSTOMER VIEWED NOTIFICATIONS FROM DB
        
        if(userSession.getViewedNotification() == null){
            final List<CustomerViewedNotification> viewedNotifications = 
                    userSession.getCustomerService().getCustomerViewedNotifications(userSession.getCustomerProfile());
            if(!viewedNotifications.isEmpty()) {
                userSession.setViewedNotification(viewedNotifications.get(0));
            }
        }
        
        if(userSession.getViewedNotification() != null ) {
        	
        	CustomerViewedNotification viewedNotification = userSession.getViewedNotification();
        	
        	final Properties attributes = this.getAttributes(viewedNotification.getAttributeData());
            	
        	for(int i = 0; i < customerMessageContents.size(); i++) {
                
            	try {
            	
            		final String viewedDate =  attributes.getProperty(customerMessageContents.get(i).getId());
            		
            		if(GenericValidator.isBlankOrNull(viewedDate)) {
            			isNewNotification = true;
                     	customerMessageContents.get(i).setMessageCSS(Utils.NC_CSS_NEW_MESSAGE);
                 	} else {
                     	customerMessageContents.get(i).setMessageCSS(Utils.NC_CSS_VIEWED_MESSAGE);
                    }
            	
            	} catch (Exception e) {
            		log.error("NotificationCenter ERROR : " + customerMessageContents + " ," + e);
            		continue;
				}
               
        	}
           
            
        } else if (!CollectionUtils.isEmpty(customerMessageContents)){
            customerMessageContents.forEach( noti -> noti.setMessageCSS(Utils.NC_CSS_NEW_MESSAGE));
        	isNewNotification = true;
        }
       
        
        Collections.sort(customerMessageContents, new Comparator<CustomerMessageContent>() {
			public int compare(CustomerMessageContent o1, CustomerMessageContent o2) {
				if (o1.getCampaignDate() == null || o2.getCampaignDate() == null)
					return 0; 
				return o2.getCampaignDate().compareTo(o1.getCampaignDate());
			}
		});

        
        if(!userSession.getUserAgentType().equals(UserAgentType.JSON) && !customerMessageContents.isEmpty()) {
            
        	CustomerViewedNotification viewedNotification = new CustomerViewedNotification(userSession.getCustomerProfile().getProfileId());
            for(int i = 0; i < customerMessageContents.size(); i++) {
                viewedNotification.putString(customerMessageContents.get(i).getId(), String.valueOf(Calendar.getInstance().getTimeInMillis()));
            }
            
            if(viewedNotification.getAttributeData() != null){
            	userSession.getCustomerService().updateCustomerViewedNotification(viewedNotification);
                userSession.setViewedNotification(viewedNotification);	
            }
            
        }
       
        userSession.setMessagesList(customerMessageContents);
        
        userSession.setNewNotifications(isNewNotification);
        model.put("notificationsList", customerMessageContents);
        model.put("newNotifications", userSession.getNewNotifications());
        model.put("noMessages", customerMessageContents.isEmpty());
        model.put("messageContentEnum", CustomerMessageContentType.values());
        model.put(Utils.NC_CSS_NEW_MESSAGE, Utils.NC_CSS_NEW_MESSAGE);
        return new ModelAndView("notificationCenter", model);
    }

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public void notificationCenterPush(HttpServletRequest request, HttpServletResponse response) throws IOException {
		final UserSession userSession = Utils.getUserSession(request);
		final Gson gson = new Gson();
		final String jsonList = gson.toJson(getPushNotificationsList(userSession));

		response.getOutputStream().write((jsonList).getBytes());
	}

	/**
	 * @param request
	 * @param messages
	 */
	private void addCreditApplications(HttpServletRequest request, List<CustomerMessageContent> messages) {
		UserSession userSession = Utils.getUserSession(request);

		if (userSession.getUnicaApplications() == null) {

			List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, Boolean.FALSE.toString(), 
					unicaAllowedStatus, unicaLoanProducts, unicaCreditProducts, marketplaceAllowedStatus, marketplaceProducts);

			Locale language = new Locale(userSession.getPreferredLanguage().getCode());
			
			if (applications != null) {

				int messageIndex = 0;
				for (UnicaApplication application : applications) {

					if (!application.isCredit() || !application.isFromMarketplace()) {
						continue;
					}

					UnicaApplicationInfo applicationInfo = application.getUnicaApplicationInfo();

					String messageId = String.join("-", Utils.CUSTOMER_MESSAGE_UNICA_APPLICATION, 
							applicationInfo.getApplicationId(), 
							applicationInfo.getApplStatusDesc().toUpperCase());

					String message = getCreditApplicationMessage(applicationInfo, language);

					if (!message.isEmpty()) {
						CustomerMessageContent customerMessageContent = new CustomerMessageContent(messageId,
								CustomerMessageContentType.INFORMATION, 
								message, 
								Utils.CUSTOMER_MESSAGE_INFORMATION_ICON);

						messages.add(messageIndex, customerMessageContent);
						messageIndex++;
					}
				}
			}
		}
	}

	/**
	 * @param status the credit card application status
	 * @param language the message language
	 * @return the credit card application message
	 */
	private String getCreditApplicationMessage(UnicaApplicationInfo applicationInfo, Locale language) {
		String message = StringUtils.EMPTY;
		String status = applicationInfo.getApplStatusDesc();

		if (status.contains("inprocess")) {
			message = messageSource.getMessage("notification.center.unica.cc.inprocess", null, language);

		} else if (status.contains("approved")) {
			message = messageSource.getMessage("notification.center.unica.cc.approved", new Object[] { applicationInfo.getApplicationId() }, language);

		} else if (status.contains("turneddown")) {
			message = messageSource.getMessage("notification.center.unica.cc.turneddown", null, language);
			
		} else if (status.contains("booked")) {
			message = messageSource.getMessage("notification.center.unica.cc.booked", null, language);
		}

		return message;
	}

    /**
     * @param attributesData
     * @return Properties
     */
    private Properties getAttributes (String attributesData) {
        if (attributesData == null || attributesData.equals(StringUtils.EMPTY)) {
            return new Properties();
        } else {
            final XStream xstream = new XStream();

            return (Properties) xstream.fromXML(attributesData);
        }
    }

    /**
     * @param userSession
     * @return the push notifications history list
     */
    private List<CustomerMessageContent> getPushNotificationsList (UserSession userSession) {
        List<CustomerMessageContent> msgList = new LinkedList<>();

        try (RestHighLevelClient restClient = esClient()) {

            final SearchTemplateResponse stResponse = restClient.searchTemplate(
                    getPushSearchRequest(userSession.getCustomerProfile().getProfileId()), 
                    RequestOptions.DEFAULT);
            final SearchHits hits = stResponse.getResponse().getHits();
            
            for (SearchHit hit : hits) {
                final CustomerMessageContent item = getNotificationCenterPushItem(hit, 
                        userSession.getPreferredLanguage());
                if (item != null) {
                    msgList.add(item);
                }
            }

        } catch (Exception e) {
            final Logger log = LOGGER;
            
            log.error("Notification Center - Error retrieving push notifications: ", e);
        }
        
        return msgList;
    }

	/**
	 * @return Elasticsearch REST client
	 */
    private RestHighLevelClient esClient () {
        return new RestHighLevelClient(
              RestClient.builder(HttpHost.create(elasticsearchEndpoint)));
    }

	/**
	 * @param dateFormatter
	 * @param profileId
	 * @return the Elasticsearch search template request
	 */
    private SearchTemplateRequest getPushSearchRequest (String profileId) {
        SearchTemplateRequest stRequest = new SearchTemplateRequest();
        
        final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN);
        final String earliestTime =  dateFormatter.format(LocalDateTime.now().minusDays(pushRetrievalDays));
        final String latestTime = dateFormatter.format(LocalDateTime.now());
        Map<String, Object> params = new HashMap<>(1);
        
        params.put("from", earliestTime);
        params.put("to", latestTime);
        params.put("profileId", profileId);
        
        stRequest.setRequest(new SearchRequest(elasticsearchIndexPush));
        stRequest.setScriptType(ScriptType.STORED);
        stRequest.setScript(elasticsearchTemplatePush);
        stRequest.setScriptParams(params);
        
        return stRequest;
    }

    /**
     * @param searchHit
     * @param prefLanguage
     * @return the push notification item for the notification center 
     */
    private CustomerMessageContent getNotificationCenterPushItem (SearchHit searchHit, PreferredLanguage prefLanguage) {
        final Logger log = LOGGER;
        final Map<String, Object> source = searchHit.getSourceAsMap();
        
        final String id = (source.get("transactionId") instanceof String) ? 
                (String) source.get("transactionId") : StringUtils.EMPTY;
         
        final String rawDate = (source.get("transactionDate") instanceof String)
			? (String) source.get("transactionDate")
			: StringUtils.EMPTY;
        final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN);
        
        String date = StringUtils.EMPTY;
        try {
            final LocalDateTime localDate = LocalDateTime.parse(rawDate, dateFormatter);
            date = getFormattedDate(localDate, prefLanguage);
        } catch (DateTimeParseException e) {
            log.error(StringEscapeUtils.escapeJava("Error parsing date " + rawDate + ":" + ExceptionUtils.getFullStackTrace(e)));
        }
        
        CustomerMessageContent item = null;
        
        try {
            final String message = (String) PropertyUtils.getProperty(source, PUSH_NOTIFICATION_MSG_PROPERTY);
            
            item = new CustomerMessageContent(id, message, date, Utils.CUSTOMER_MESSAGE_PUSH_ICON);
            
        } catch (Exception e) {
            log.error("Error retrieving push notification message: ", e);
        }
        
        return item;
        
    }

    /**
     * @param date
     * @param language
     * @return the formatted date for the notification center
     */
    private String getFormattedDate(LocalDateTime date, PreferredLanguage language) {
        
        final Locale locale = new Locale(language.getCode(), Locale.US.getCountry());
        final LocalDateTime now = LocalDateTime.now();
        final long hours = date.until(now, ChronoUnit.HOURS);
        final long minutes = date.until(now, ChronoUnit.MINUTES);
        final long seconds = date.until(now, ChronoUnit.SECONDS);
        
        String formattedDate = StringUtils.EMPTY;

        if (minutes < 1) {
        	formattedDate = getFormattedDateText("seconds.ago", seconds, locale);
        } else if (hours < 1) {
        	formattedDate = getFormattedDateText("minutes.ago", minutes, locale);
        } else if (hours < 24) {
        	formattedDate = getFormattedDateText("hours.ago", hours, locale);
        } else {
            formattedDate = date.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG, FormatStyle.SHORT).withLocale(locale));
        }

        return formattedDate;
    }

	/**
	 * @param textId
	 *                   the id in text properties
	 * @param number
	 *                   the number of seconds, minutes, or hours
	 * @param locale
	 *                   the language locale
	 * @return the date text
	 */
	private String getFormattedDateText(String textId, long number, Locale locale) {
		return messageSource.getMessage(textId, new Object[] { number, number == 1 ? StringUtils.EMPTY : "s" }, locale);
	}

	/**
	 * @param elasticsearchEndpoint
	 *                                  the Elasticsearch endpoint
	 */
	public void setElasticsearchEndpoint(String elasticsearchEndpoint) {
		this.elasticsearchEndpoint = elasticsearchEndpoint;
	}

	/**
	 * @param elasticsearchIndexPush
	 *                                   the Elasticsearch index for push
	 *                                   notifications
	 */
	public void setElasticsearchIndexPush(String elasticsearchIndexPush) {
		this.elasticsearchIndexPush = elasticsearchIndexPush;
	}

	/**
	 * @param elasticsearchTemplatePush
	 *                                      Elasticsearch search template for push
	 *                                      notifications history
	 */
	public void setElasticsearchTemplatePush(String elasticsearchTemplatePush) {
		this.elasticsearchTemplatePush = elasticsearchTemplatePush;
	}

}
