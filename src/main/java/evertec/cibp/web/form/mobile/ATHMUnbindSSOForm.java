package evertec.cibp.web.form.mobile;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.form.mobile.bean.ATHMCheckSSOTokenBean;
import evertec.cibp.web.session.UserSession;

public class ATHMUnbindSSOForm extends SimpleFormController {

	private static final String VIEW_NAME = "athmssounbind";

	protected CustomerServices customerService = null;

	public ATHMUnbindSSOForm() {
		super();
		setCommandClass(ATHMCheckSSOTokenBean.class);
		setCommandName(VIEW_NAME);
		setFormView(VIEW_NAME);
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		
		Map<String, Object> model = new LinkedHashMap<>();
		return showForm(request, error, VIEW_NAME, model);
	}
	
	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		
		boolean success = false;
		Map<String, Object> model = new LinkedHashMap<>();

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		if (userSession != null) {
			CustomerProfile profile = userSession.getCustomerProfile();
			
			if (profile != null && profile.hasEntitlement(EntitlementType.ATHMOVIL)) {
				CustomerEntitlement entitlement = profile.getEntitlementByType(EntitlementType.ATHMOVIL);
				customerService.getCustomerProfileService().removeEntitlement(profile, entitlement);
				success = true;
			}
		}
		
		model.put("success", success);
		return model;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

}
