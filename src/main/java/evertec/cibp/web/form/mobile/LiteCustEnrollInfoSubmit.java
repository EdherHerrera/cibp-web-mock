package evertec.cibp.web.form.mobile;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerPhone;
import evertec.cibp.core.models.customer.CustomerProfileType;
import evertec.cibp.core.models.customer.devices.CustomerDevice;
import evertec.cibp.core.models.customer.devices.DeviceStatus;
import evertec.cibp.core.models.customer.devices.ProductType;
import evertec.cibp.core.models.enrollment.EnrollmentLiteStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.CustomerDeviceResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.mobilecash.core.services.MobileCashServices;
import evertec.cibp.web.form.mobilecash.bean.LiteEnrollmentVerificationBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;


public class LiteCustEnrollInfoSubmit extends CancellableFormController {

	protected final Log logger = LogFactory.getLog(getClass());
	private static final String VIEW_NAME = "liteCustomerEnrollmentInfoSubmit";
	private CustomerServices customerServices;
	private MobileCashServices mobileCashService;

	public LiteCustEnrollInfoSubmit() {
		super();
		setCommandClass(LiteEnrollmentVerificationBean.class);
		setCommandName(VIEW_NAME);
		setFormView(VIEW_NAME);
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object submit,
			BindException error) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		boolean hasError = false;
		Map<String, Object> model = new LinkedHashMap<String, Object>();
		LiteEnrollmentVerificationBean liteEnrollmentBean = (LiteEnrollmentVerificationBean) submit;
		EnrollmentLiteStatus status = EnrollmentLiteStatus.BACKEND_ERROR;

		if(customerServices == null || customerServices.getCustomerProfileService() == null){
			logger.error("Null customerServices or customerProfileService object");
			hasError = true;
		} else{
			if(GenericValidator.isBlankOrNull(liteEnrollmentBean.getPhoneNumber())
					|| GenericValidator.isBlankOrNull(liteEnrollmentBean.getDeviceId())){

				status = EnrollmentLiteStatus.MISSING_INFO;
				logger.error("Empty required parameters");
				hasError = true;
				model.put("error", hasError);
				model.put("status", status.getCode());
				return showForm(request, error, VIEW_NAME, model);
			}
		}
		
		CustomerDeviceResponse phoneResponse = customerServices.isPhoneNumberInBlacklist(liteEnrollmentBean.getPhoneNumber());
		
		if( phoneResponse != null && phoneResponse.getResponseStatus() != null && 
				phoneResponse.getResponseStatus().equals(ResultStatus.PHONE_IN_BLACKLIST)) {
			status = EnrollmentLiteStatus.PHONE_IN_BLACKLIST;
			hasError = true;
		}

		if(!hasError){
			userSession.setMobileCashService(mobileCashService);
			
			Utils.deletePreviousEnrollmentData(userSession, liteEnrollmentBean.getPhoneNumber());
			status = customerValidation(request, userSession, liteEnrollmentBean);
		}

		model.put("error", hasError);
		model.put("status", status.getCode());
		return showForm(request, error, VIEW_NAME, model);
	}


	/**
	 * Method that manages a customer phone enrollment
	 * @author et55498
	 * @param request The HttpServletRequest
	 * @param liteEnrollmentBean Object that contains the user input
	 * @return EnrollmentLiteStatus enum
	 */
	private EnrollmentLiteStatus customerValidation(HttpServletRequest request, UserSession userSession, LiteEnrollmentVerificationBean liteEnrollmentBean)
	{
		CustomerPhone customerPhone = new CustomerPhone();
		customerPhone.setProfileId(userSession.getCustomerProfile().getProfileId());
		customerPhone.setPhoneNumber(liteEnrollmentBean.getPhoneNumber());
		customerPhone.setPhoneProvider(liteEnrollmentBean.getPhoneProvider());
		customerPhone.setCustomerProfileType(CustomerProfileType.REGULAR);
		userSession.getCustomerProfile().setCustomerPhone(customerPhone);
		
		CustomerDevice customerDevice = new CustomerDevice();
		customerDevice.setProfileId(userSession.getCustomerProfile().getProfileId());
		customerDevice.setDeviceMbopId(userSession.getDeviceId());
		customerDevice.setDeviceStatus(DeviceStatus.ACTIVE);
		customerDevice.setDeviceLastUsageDate(new Date());
		customerDevice.setDeviceRegDate(new Date());
		customerDevice.setProductType(ProductType.CASHDROP);
		userSession.getCustomerProfile().setCustomerDevice(customerDevice);
		
		return EnrollmentLiteStatus.DISPLAY_TERMS_AND_CONDITIONS;
	}

	public CustomerServices getCustomerServices() {
		return customerServices;
	}

	public void setCustomerServices(CustomerServices customerService) {
		this.customerServices = customerService;
	}
	
	public MobileCashServices getMobileCashService() {
		return mobileCashService;
	}

	public void setMobileCashService(MobileCashServices mobileCashService) {
		this.mobileCashService = mobileCashService;
	}

}