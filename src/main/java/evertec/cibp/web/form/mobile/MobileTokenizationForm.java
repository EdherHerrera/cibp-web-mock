package evertec.cibp.web.form.mobile;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import bppr.tv.beans.accounts.types.DepositAccount;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.enrollment.EnrollmentLiteStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.FingerprintRegistrationResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.MobileResponseStatus;
import evertec.cibp.web.form.mobile.bean.MobileTokenizationBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AthmUtils;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.tokenization.ResponseStatus;
import evertec.otherservices.beans.tokenization.RetailerType;
import evertec.otherservices.beans.tokenization.TerminalType;
import evertec.otherservices.requests.TokenServReq;
import evertec.otherservices.responses.TokenServRes;

public class MobileTokenizationForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	
	private String acqInstitutionID;
	private String banner;
	private String terminalCountry;
	private String terminalCity;
	private String terminalId;
	private String terminalState;
	private String terminalOwner;
	private String retailerRegion;
	private String retailerGroup;
	private String retailerId;
	private String replaceChar;
	private String allowedCCardStatus = null;
	private String allowedCCards = null;
	private String vtsDelay;

	public MobileTokenizationForm() {
		super();
		setCommandClass(MobileTokenizationBean.class);
		setCommandName("mobileTokenization");
		setFormView("mobileTokenization");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerServices = userSession.getCustomerService();
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		
		String panReferenceID = request.getParameter("panReferenceID");
		String walletAccountID = request.getParameter("walletAccountID");
		String tokenRequestorID = request.getParameter("tokenRequestorID");
		String deviceID = request.getParameter("deviceID");
		String panLast4 = request.getParameter("panLast4");
		String tokenReferenceID = request.getParameter("tokenReferenceID");
		String tokenReqName = request.getParameter("tokenReqName");
		
		if(GenericValidator.isBlankOrNull(panReferenceID) || GenericValidator.isBlankOrNull(walletAccountID) 
				|| GenericValidator.isBlankOrNull(tokenRequestorID) || GenericValidator.isBlankOrNull(tokenReferenceID)
				|| GenericValidator.isBlankOrNull(deviceID) || GenericValidator.isBlankOrNull(panLast4)){
			logger.error("Tokenization parameter null.");
			logger.error("panReferenceID="+panReferenceID);
			logger.error("walletAccountID="+walletAccountID);
			logger.error("tokenRequestorID="+tokenRequestorID);
			logger.error("tokenReferenceID="+tokenReferenceID);
			logger.error("deviceID="+deviceID);
			logger.error("panLast4="+panLast4);
			model.put("status", MobileResponseStatus.ERROR);
			return showForm(request, error, "mobileTokenization", model);
		}
		
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT315)) {
			FingerprintRegistrationResponse fingerprintResponse = Utils.doesUserHasFingerprintRegistered(userSession, vtsDelay);
			if (!fingerprintResponse.getStatus().equals(EnrollmentLiteStatus.FINGERPRINT_VALIDATION_SUCCESS)) {
				model.put("status", fingerprintResponse.getStatus());
				
				if (fingerprintResponse.getStatus().equals(EnrollmentLiteStatus.FINGERPRINT_DELAY_FAILED))
					model.put("minutesLeft", fingerprintResponse.getMinutesLeft());
				
				return showForm(request, error, "mobileTokenization", model);
			}
		}

		AbstractAccount accountDetails = null;
		DepositAccount depositAccount = null;
		CardPlasticAccountRecord[] cardPlasticAccountRecordList = null;
		boolean found = false;
		for(int j = 0; j < userSession.getCardPlasticAccounts().size(); j++){
			TVFrontendAccount plasticAccount = userSession.getCardPlasticAccounts().get(j);
			//PLASTIC IS INT
			if(plasticAccount.getAccountNumber().startsWith(allowedCCards)) {
				//PLASTIC BELONGS TO THE USER
				String athLast4 = plasticAccount.getAccountNumber().substring(plasticAccount.getAccountNumber().length() - 4);
				if(athLast4.equals(panLast4)){
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT508)) {
						try{
							accountDetails = userSession.getCustomerService().getCardPlasticAccount(
									customerProfile, userSession.getBankingSession(),plasticAccount, true);
							if(accountDetails != null && accountDetails.getBackendObject() != null){
								depositAccount = (DepositAccount) accountDetails.getBackendObject();
								if (depositAccount != null) {
									cardPlasticAccountRecordList = depositAccount.getCardPlasticAccountRecords();
									if(cardPlasticAccountRecordList != null && cardPlasticAccountRecordList.length != 0){
										for(int x = 0; x < cardPlasticAccountRecordList.length; x++){
											CardPlasticAccountRecord plastic = cardPlasticAccountRecordList[x];
											if (plastic != null && AthmUtils.isAthmCardStatusAllowed(plastic.getStatus(), allowedCCardStatus)) {
												found = true;
												break;
											}
										}
										if (found){
											break;
										}
									}
								}
							}
						}
						catch(Exception e){
							logger.error("Mobile token service, error getting ATH VTS data.");
						}						
					}else {
						found = true;
						try{
							accountDetails = userSession.getCustomerService().getCardPlasticAccount(
									customerProfile, userSession.getBankingSession(),plasticAccount, true);
							if(accountDetails != null && accountDetails.getBackendObject() != null){
								depositAccount = (DepositAccount) accountDetails.getBackendObject();
								cardPlasticAccountRecordList = depositAccount.getCardPlasticAccountRecords();
								if(cardPlasticAccountRecordList != null && cardPlasticAccountRecordList.length != 0){
									break;
								}
							}
						}
						catch(Exception e){
							logger.error("Mobile token service, error getting ATH VTS data.");
						}
					}
				}
			}
		}
		if(!found){
			model.put("status", MobileResponseStatus.NOT_FOUND);
			return showForm(request, error, "mobileTokenization", model);
		}
		if(cardPlasticAccountRecordList == null || cardPlasticAccountRecordList.length == 0){
			model.put("status", MobileResponseStatus.NOT_FOUND);
			return showForm(request, error, "mobileTokenization", model);
		}
		
		SimpleDateFormat df = new SimpleDateFormat("yyMM");
		String expiryDate = df.format(cardPlasticAccountRecordList[0].getExpirationDate().getTime());
		Long pan = Long.valueOf(cardPlasticAccountRecordList[0].getCardPANId());
		String cardHolder = cardPlasticAccountRecordList[0].getEmbossedName();
		
		TerminalType terminal = new TerminalType(terminalId, terminalOwner, terminalCity, terminalState.replace(this.replaceChar, " "), terminalCountry);
		RetailerType retailer = new RetailerType(retailerId, retailerGroup.replace(this.replaceChar, " "), retailerRegion.replace(this.replaceChar, " "));
		TokenServReq tokenRequest = new TokenServReq(null, panReferenceID, tokenRequestorID, tokenReferenceID, 
									panLast4, deviceID, walletAccountID, expiryDate, this.acqInstitutionID, terminal, retailer, pan);
		
		
		TokenServRes tokenResponse = customerServices.tokenService(tokenRequest);
		
		CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request, customerProfile.getUsername());
		interactionEvent.resetProperties();
		interactionEvent.putString("deviceID", deviceID);
		interactionEvent.putString("tokenRequestorID", tokenRequestorID);
		interactionEvent.putString("walletAccountID", walletAccountID);
		interactionEvent.putString("tokenReferenceID", tokenReferenceID);
		interactionEvent.putString("panLast4", panLast4);
		interactionEvent.putString("expiryDate", expiryDate);
		
		if(tokenResponse.getStatus().equals(ResponseStatus.ERROR)){
			logger.error(tokenResponse.getStatus());
			logger.error(tokenResponse.getResponseDescription());
			
			if(tokenResponse.getAuthRespCode() != null){
				interactionEvent.putString("errorCode", tokenResponse.getAuthRespCode());
				interactionEvent.putString("errorDescription", tokenResponse.getResponseDescription());
				logger.error(tokenResponse.getAuthRespCode());
				model.put("status", MobileResponseStatus.ERROR_VTS);
			} else {
				model.put("status", MobileResponseStatus.ERROR);
			}
			
			userSession.getCustomerService().logTokenizationActivationFailed(interactionEvent, InteractionSeverity.ERROR);
			return showForm(request, error, "mobileTokenization", model);
		}
		else{
			Map<Object, Object> models = new HashMap<Object, Object>();
			models.put("cardHolder", cardHolder);
			models.put("last4Digits", "x" + panLast4);
			models.put("tokenReqName", tokenReqName);
			models.put("banner", this.banner);
			customerServices.sendTokenServiceConfirmation(customerProfile, models, userSession.getPreferredLanguage());
			model.put("status", MobileResponseStatus.SUCCESS);
			userSession.getCustomerService().logTokenizationActivationSuccess(interactionEvent, InteractionSeverity.INFO);
			return showForm(request, error, "mobileTokenization", model);
		}
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		return model;
	}

	public String getAcqInstitutionID() {
		return acqInstitutionID;
	}

	public void setAcqInstitutionID(String acqInstitutionID) {
		this.acqInstitutionID = acqInstitutionID;
	}

	public String getBanner() {
		return banner;
	}

	public void setBanner(String banner) {
		this.banner = banner;
	}

	public String getTerminalCountry() {
		return terminalCountry;
	}

	public void setTerminalCountry(String terminalCountry) {
		this.terminalCountry = terminalCountry;
	}

	public String getTerminalCity() {
		return terminalCity;
	}

	public void setTerminalCity(String terminalCity) {
		this.terminalCity = terminalCity;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getTerminalState() {
		return terminalState;
	}

	public void setTerminalState(String terminalState) {
		this.terminalState = terminalState;
	}

	public String getTerminalOwner() {
		return terminalOwner;
	}

	public void setTerminalOwner(String terminalOwner) {
		this.terminalOwner = terminalOwner;
	}

	public String getRetailerRegion() {
		return retailerRegion;
	}

	public void setRetailerRegion(String retailerRegion) {
		this.retailerRegion = retailerRegion;
	}

	public String getRetailerGroup() {
		return retailerGroup;
	}

	public void setRetailerGroup(String retailerGroup) {
		this.retailerGroup = retailerGroup;
	}

	public String getRetailerId() {
		return retailerId;
	}

	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}

	public String getReplaceChar() {
		return replaceChar;
	}

	public void setReplaceChar(String replaceChar) {
		this.replaceChar = replaceChar;
	}
	
	/**
	 * @return the allowedCCardStatus
	 */
	public String getAllowedCCardStatus() {
		return allowedCCardStatus;
	}

	/**
	 * @param allowedCCardStatus the allowedCCardStatus to set
	 */
	public void setAllowedCCardStatus(String allowedCCardStatus) {
		this.allowedCCardStatus = allowedCCardStatus;
	}
	
	/**
	 * @return the allowedCCards
	 */
	public String getAllowedCCards() {
		return allowedCCards;
	}

	/**
	 * @param allowedCCards the allowedCCards to set
	 */
	public void setAllowedCCards(String allowedCCards) {
		this.allowedCCards = allowedCCards;
	}

	public String getVtsDelay() {
		return vtsDelay;
	}

	public void setVtsDelay(String vtsDelay) {
		this.vtsDelay = vtsDelay;
	}

}
