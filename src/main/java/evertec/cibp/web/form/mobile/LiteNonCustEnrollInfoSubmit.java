package evertec.cibp.web.form.mobile;

import java.text.ParseException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.csr.CustomerSearchFilter;
import evertec.cibp.core.models.customer.CustomerPhone;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerProfileType;
import evertec.cibp.core.models.customer.EasyOfacCustomer;
import evertec.cibp.core.models.customer.EasyOfacCustomerStatus;
import evertec.cibp.core.models.customer.MultipleCustomerProfileResponse;
import evertec.cibp.core.models.customer.devices.CustomerDevice;
import evertec.cibp.core.models.customer.devices.DeviceStatus;
import evertec.cibp.core.models.customer.devices.ProductType;
import evertec.cibp.core.models.enrollment.EnrollmentLiteStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.CustomerDeviceResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.mobilecash.bean.LiteEnrollmentVerificationBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.session.UserSessionType;
import evertec.cibp.web.utils.Utils;
import evertec.easyofac.models.CustomerSearch;
import evertec.easyofac.models.ResponseOfac;
import evertec.easyofac.models.ResponseStatus;

public class LiteNonCustEnrollInfoSubmit extends CancellableFormController {

	protected final Log logger = LogFactory.getLog(getClass());
	private static final String VIEW_NAME = "liteEnrollmentVerification";
	private CustomerServices customerServices;
	private boolean easyOfacSuccess = false;

	public LiteNonCustEnrollInfoSubmit() {
		super();
		setCommandClass(LiteEnrollmentVerificationBean.class);
		setCommandName(VIEW_NAME);
		setFormView(VIEW_NAME);
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object submit,
			BindException error) throws Exception {

		boolean hasError = false;
		Map<String, Object> model = new LinkedHashMap<String, Object>();
		LiteEnrollmentVerificationBean liteEnrollmentBean = (LiteEnrollmentVerificationBean) submit;
		EnrollmentLiteStatus status = EnrollmentLiteStatus.BACKEND_ERROR;

		if(customerServices == null || customerServices.getCustomerProfileService() == null){
			logger.error("Null customerServices or customerProfileService object");
			hasError = true;
			
		} else if(GenericValidator.isBlankOrNull(liteEnrollmentBean.getPhoneNumber()) 
				|| GenericValidator.isBlankOrNull(liteEnrollmentBean.getPhoneProvider())
				|| GenericValidator.isBlankOrNull(liteEnrollmentBean.getEmail())
				|| GenericValidator.isBlankOrNull(liteEnrollmentBean.getDeviceId())
				|| GenericValidator.isBlankOrNull(liteEnrollmentBean.getFirstName())
				|| GenericValidator.isBlankOrNull(liteEnrollmentBean.getLastName())){

			status = EnrollmentLiteStatus.MISSING_INFO;
			logger.error("Empty required parameters");
			hasError = true;
			
		}else{
			status = nonCustomerValidation(request, liteEnrollmentBean);
			
		}

		model.put("error", hasError);
		model.put("status", status.getCode());
		return showForm(request, error, VIEW_NAME, model);

	}


	private EnrollmentLiteStatus nonCustomerValidation(HttpServletRequest request, LiteEnrollmentVerificationBean liteEnrollmentBean) throws ParseException
	{
		CustomerSearchFilter filter = new CustomerSearchFilter();
		filter.setEmail(liteEnrollmentBean.getEmail());
		MultipleCustomerProfileResponse customerProfiles = customerServices.getCustomerProfileService().getCustomerProfileByFilter(filter);
		
		CustomerDeviceResponse phoneResponse = customerServices.isPhoneNumberInBlacklist(liteEnrollmentBean.getPhoneNumber());
		
		if(phoneResponse != null && phoneResponse.getResponseStatus().equals(ResultStatus.PHONE_IN_BLACKLIST)) {
			return EnrollmentLiteStatus.PHONE_IN_BLACKLIST;
		}
		
		EnrollmentLiteStatus status = null;

		String ofacId = "";
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EASY_OFAC)) {
			String name = Utils.replaceAccents(liteEnrollmentBean.getFirstName());
			String lastName = Utils.replaceAccents(liteEnrollmentBean.getLastName());
			
			ResponseOfac ofacResp = new ResponseOfac();
			CustomerSearch search = new CustomerSearch();
			search.setCountry(liteEnrollmentBean.getCountry());
			search.setFirstName(name);
			search.setLastName(lastName);
			ofacId = customerServices.getCustomerProfileService().getNextEasyOfacId();
			search.setId(ofacId);
			
			ofacResp = customerServices.addCustomerEasyOfac(search);
			
			if (ofacResp == null || ofacResp.getStatus() == null || ofacResp.getStatus().equals(ResponseStatus.ERROR) || ofacResp.getStatus().equals(ResponseStatus.RESPONSE_NOT_SUCCESS) ||
					ofacResp.getStatus().equals(ResponseStatus.MISSING_INFO)) {
				logger.error(ofacResp.getResponseDesc());
				return EnrollmentLiteStatus.BACKEND_ERROR;
			}
			
			EasyOfacCustomerStatus ofacStatus = Utils.convertEasyOfacStatus(ofacResp.getStatus()); 

			EasyOfacCustomer cust = new EasyOfacCustomer();
			cust.setFirstName(name);
			cust.setLastName(lastName);
			cust.setCountry(liteEnrollmentBean.getCountry());
			cust.setStatus(ofacStatus);
			cust.setRegisteredDate(new Date());
			cust.setValidatedDate(new Date());
			cust.setEasyOfacId(ofacId);
			cust.setPhoneNumber(liteEnrollmentBean.getPhoneNumber());
			
			if (customerServices.addEasyOfacCustomerInDB(cust)) {
				this.easyOfacSuccess = true;
		        switch (ofacStatus) {
	            case EXCEPTION:
	            	break;
	            case SAFE:
	            	break;
	            case UNSAFE:
	            	search = new CustomerSearch();
					search.setId(ofacId);
					ofacResp = customerServices.removeCustomerEasyOfac(search);
					if (ofacResp != null && ofacResp.getStatus() != null && ofacResp.getStatus().equals(ResponseStatus.SUCCESS)) {
						return EnrollmentLiteStatus.NAME_MATCH;
					}
					return EnrollmentLiteStatus.NAME_MATCH;
	            case COUNTRY_MATCH:
	            	search = new CustomerSearch();
					search.setId(ofacId);
					ofacResp = customerServices.removeCustomerEasyOfac(search);
					if (ofacResp != null && ofacResp.getStatus() != null && ofacResp.getStatus().equals(ResponseStatus.SUCCESS)) {
						return EnrollmentLiteStatus.COUNTRY_MATCH;
					}
					return EnrollmentLiteStatus.COUNTRY_MATCH;
	            default:
	            	return EnrollmentLiteStatus.BACKEND_ERROR;
		        }
			}
		}

		if(customerProfiles != null && customerProfiles.getCustomerProfiles().size() > 0){
			status = EnrollmentLiteStatus.CUSTOMER_EMAIL_FOUND;
		} else{
			status =  EnrollmentLiteStatus.DISPLAY_TERMS_AND_CONDITIONS;
		}
		
		UserSession userSession = new UserSession();
		userSession.setUserSessionType(UserSessionType.CUSTOMER_PROFILE_LITE);
		CustomerProfile customerProfileLite = new CustomerProfile();
		customerProfileLite.setEmail(liteEnrollmentBean.getEmail());
		customerProfileLite.setFirstName(liteEnrollmentBean.getFirstName());
		customerProfileLite.setLastName(liteEnrollmentBean.getLastName());
		customerProfileLite.setCountry(liteEnrollmentBean.getCountry());

		if (!StaticMessageSource.isFlagEnabled(StaticMessageSource.EASY_OFAC)) {
			ofacId = customerServices.getCustomerProfileService().getNextEasyOfacId();
		}
		customerProfileLite.setOfacId(ofacId);

		CustomerPhone customerPhone = new CustomerPhone();
		customerPhone.setPhoneNumber(liteEnrollmentBean.getPhoneNumber());
		customerPhone.setPhoneProvider(liteEnrollmentBean.getPhoneProvider());
		customerPhone.setCustomerProfileType(CustomerProfileType.LITE);
		customerProfileLite.setCustomerPhone(customerPhone);

		CustomerDevice customerDevice = new CustomerDevice();
		customerDevice.setDeviceId(liteEnrollmentBean.getDeviceId());
		customerDevice.setDeviceStatus(DeviceStatus.ACTIVE);
		customerDevice.setDeviceLastUsageDate(new Date());
		customerDevice.setDeviceRegDate(new Date());
		customerDevice.setProductType(ProductType.CASHDROP);

		customerProfileLite.setCustomerDevice(customerDevice);
		userSession.setCustomerService(customerServices);
		userSession.setLoginCompleteSuccess(true);

		userSession.setCustomerProfile(customerProfileLite);
		if (this.easyOfacSuccess && StaticMessageSource.isFlagEnabled(StaticMessageSource.EASY_OFAC)) { userSession.setNewOfacCustomer(true); }
		
		WebUtils.setSessionAttribute(request, "userSession", userSession);
		
		return status;
	}

	public CustomerServices getCustomerServices() {
		return customerServices;
	}

	public void setCustomerServices(CustomerServices customerService) {
		this.customerServices = customerService;
	}

	public boolean isEasyOfacSuccess() {
		return easyOfacSuccess;
	}

	public void setEasyOfacSuccess(boolean easyOfacSuccess) {
		this.easyOfacSuccess = easyOfacSuccess;
	}

}