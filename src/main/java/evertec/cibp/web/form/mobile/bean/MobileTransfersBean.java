package evertec.cibp.web.form.mobile.bean;

public class MobileTransfersBean extends MobileAbstractBean {

	private String fromAccountNumber;
	private String fromAccountType;
	private String fromAccountProductId;
	private String fromAccountSection;

	private String toAccountNumber;
	private String toAccountType;
	private String toAccountProductId;
	private String toAccountSection;
	
	private String amount;

	public String getFromAccountNumber() {
		return fromAccountNumber;
	}

	public void setFromAccountNumber(String fromAccountNumber) {
		this.fromAccountNumber = fromAccountNumber;
	}

	public String getFromAccountType() {
		return fromAccountType;
	}

	public void setFromAccountType(String fromAccountType) {
		this.fromAccountType = fromAccountType;
	}

	public String getFromAccountProductId() {
		return fromAccountProductId;
	}

	public void setFromAccountProductId(String fromAccountProductId) {
		this.fromAccountProductId = fromAccountProductId;
	}

	public String getFromAccountSection() {
		return fromAccountSection;
	}

	public void setFromAccountSection(String fromAccountSection) {
		this.fromAccountSection = fromAccountSection;
	}

	public String getToAccountNumber() {
		return toAccountNumber;
	}

	public void setToAccountNumber(String toAccountNumber) {
		this.toAccountNumber = toAccountNumber;
	}

	public String getToAccountType() {
		return toAccountType;
	}

	public void setToAccountType(String toAccountType) {
		this.toAccountType = toAccountType;
	}

	public String getToAccountProductId() {
		return toAccountProductId;
	}

	public void setToAccountProductId(String toAccountProductId) {
		this.toAccountProductId = toAccountProductId;
	}

	public String getToAccountSection() {
		return toAccountSection;
	}

	public void setToAccountSection(String toAccountSection) {
		this.toAccountSection = toAccountSection;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	
}
