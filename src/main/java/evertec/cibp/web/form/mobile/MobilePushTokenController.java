package evertec.cibp.web.form.mobile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import com.google.common.collect.Iterables;

import edu.emory.mathcs.backport.java.util.Arrays;
import evertec.cibp.core.dto.PushTokensDto;
import evertec.cibp.core.enums.PushTokenStatus;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.PushTokenInteraction;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.PushTokensRequest;
import evertec.cibp.web.WebAppController;
import evertec.cibp.web.enums.MobileResponseStatus;
import evertec.cibp.web.facade.PushTokensFacade;
import evertec.cibp.web.request.PushTokensMobileRequest;
import evertec.cibp.web.response.PushTokensResponse;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.PushTokenConstants;
import evertec.cibp.web.utils.Utils;

/**
 * @author MBM
 * 
 * Controller used by mobile applications to check, update, or create a push token object
 *
 */
public class MobilePushTokenController extends MultiActionController implements MessageSourceAware {

	private static final Logger LOG = Logger.getLogger(WebAppController.class);
	private static final String PUSH_TOKEN_RESPONSE = "pushTokenResponse";
	private static final String STATUS = "status";
	private static final String USER_SESSION = "userSession";

	private MessageSource messageSource;
	private CustomerServices customerService;

	protected CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Method that checks if a token should be disabled, updated or created with a default status
	 * 
	 * @param request
	 * @param response
	 * @return a view with disabled status if need to disable the push toggle
	 * @throws Exception
	 */
	public ModelAndView mobileCheckPushTokenStatus(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> model = new HashMap<>(NumberUtils.INTEGER_ONE);
		List<PushTokensRequest> putRequest = new ArrayList<>(NumberUtils.INTEGER_ONE);
		MobileResponseStatus status = MobileResponseStatus.SUCCESS;
		Boolean isInsertInitialToken = false;

		final UserSession session = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION); 

		final String profileId = session.getCustomerProfile().getProfileId();

		final PushTokensMobileRequest statusRequest = new PushTokensMobileRequest(request);

		PushTokensRequest getRequest = new PushTokensRequest(); 
		getRequest.setProfileId(profileId);

		if (StringUtils.isBlank(statusRequest.getToken())) {
			LOG.error("Error: CheckPushTokenStatus - No token received");
			model.put(STATUS, MobileResponseStatus.ERROR);
			return new ModelAndView(PUSH_TOKEN_RESPONSE, model);
		}

		if (StringUtils.isBlank(statusRequest.getInvalidToken())) {

			getRequest.setToken(statusRequest.getToken());
			final PushTokensResponse getResponse = PushTokensFacade.getInstance().getTokens(getRequest, session);

			PushTokensRequest pushTokenRequest = new PushTokensRequest();

			// Update Last Sign-On Date & Device Name if PushToken exists
			if (responseHasData(getResponse)) {

				final PushTokensDto pushToken = Iterables.getFirst(getResponse.getData(), null);

				//Check if push token has old unused status from phase 1 like DELETED_BY_USER, and update it with default status
				updateOldPhaseTwoStatus(pushToken);

				final PushTokenStatus[] validStatus = {PushTokenStatus.ACTIVE, PushTokenStatus.INACTIVE};
				if (!Arrays.asList(validStatus).contains(PushTokenStatus.valueOf(pushToken.getStatus()))) {
					status = MobileResponseStatus.DISABLE;

					//If token has default or purged status, don't return view since the object will be updated
					if (!StringUtils.equals(PushTokenStatus.INITIAL.name(), pushToken.getStatus()) &&
							!StringUtils.equals(PushTokenStatus.PURGED.name(), pushToken.getStatus())) {
						model.put(STATUS, status);
						return new ModelAndView(PUSH_TOKEN_RESPONSE, model);

					}

				}

				pushTokenRequest.copyPushDtoProperties(pushToken);
				pushTokenRequest.setUpdateLastSignonDate(true);
				pushTokenRequest.setDeviceName(statusRequest.getDeviceName());
				pushTokenRequest.setStatus(statusRequest.getStatus());

				setTokenStatus(pushTokenRequest, pushToken, statusRequest);

			} else {

				//Token does not exist, create new token with default (INITIAL) status, if device has permissions
				isInsertInitialToken = createDefaultToken(profileId, statusRequest, pushTokenRequest);
				
				// If device has no permissions, no need to do anything else
				if (!isInsertInitialToken) {
					model.put(STATUS, MobileResponseStatus.ERROR);
					return new ModelAndView(PUSH_TOKEN_RESPONSE, model);
				}
			}

			putRequest.add(pushTokenRequest);

		} else {

			getRequest.setToken(statusRequest.getInvalidToken());

			// Invalid old token
			final PushTokensResponse getResponse = PushTokensFacade.getInstance().getTokens(getRequest, session);
			invalidateOldToken(putRequest, getResponse);

			// Create put request for new token
			final PushTokensRequest pushTokenRequest = new PushTokensRequest(profileId);
			BeanUtils.copyProperties(statusRequest, pushTokenRequest);
			putRequest.add(pushTokenRequest);
		}

		final PushTokensResponse putResponse = PushTokensFacade.getInstance().putTokens(putRequest, session);
		status = checkIfResponseHasErrors(status, putResponse);

		logTokenStatus(request, status, isInsertInitialToken, session, statusRequest);
		
		model.put(STATUS, status);
		return new ModelAndView(PUSH_TOKEN_RESPONSE, model);

	}

	/**
	 * @param pushToken
	 */
	private void updateOldPhaseTwoStatus(PushTokensDto pushToken) {
		if (StringUtils.equals(PushTokenStatus.DELETED_BY_USER.name(), pushToken.getStatus())) {
			pushToken.setStatus(PushTokenStatus.INITIAL.name());
		}
	}

	/**
	 * Add a token to the list request to be invalidated
	 * 
	 * @param putRequest
	 * @param getResponse
	 */
	private void invalidateOldToken(List<PushTokensRequest> putRequest, PushTokensResponse getResponse) {
		if (responseHasData(getResponse)) {
			final PushTokensDto pushToken = Iterables.getFirst(getResponse.getData(), null);
			PushTokensRequest pushTokenRequest = new PushTokensRequest();

			BeanUtils.copyProperties(pushToken, pushTokenRequest);
			pushTokenRequest.setStatus(PushTokenStatus.INVALID);
			putRequest.add(pushTokenRequest);
		}
	}

	/**
	 * Check if the response received contains any errors
	 * 
	 * @param status
	 * @param putResponse
	 * @return the MobileResponseStatus 
	 */
	private MobileResponseStatus checkIfResponseHasErrors(MobileResponseStatus status, PushTokensResponse putResponse) {
		if (StringUtils.isNotBlank(putResponse.getError()) || putResponse.hasErrors()) {
			LOG.error("Error: CheckPushTokenStatus - PUT error: " + putResponse.getError());
			return MobileResponseStatus.ERROR;
		}
		return status;
	}

	/**
	 * Create a push token object with a default status
	 * 
	 * @param isInsertInitialToken
	 * @param profileId
	 * @param statusRequest
	 * @param pushTokenRequest
	 * @return if the object is in need to be inserted O.o
	 */
	private boolean createDefaultToken(String profileId, PushTokensMobileRequest statusRequest, PushTokensRequest pushTokenRequest) {
		if (statusRequest.hasPermissions()) {
			BeanUtils.copyProperties(statusRequest, pushTokenRequest);
			pushTokenRequest.setStatus(PushTokenStatus.INITIAL);
			pushTokenRequest.setProfileId(profileId);
			return true;
		}
		return false;
	}

	/**
	 * Log a push event in history logs when a token is created with default status
	 * 
	 * @param request
	 * @param status
	 * @param isInsertInitialToken, if a token was created to insert
	 * @param session, the user session
	 * @param statusRequest , the push mobile request
	 */
	private CustomerInteractionEvent logPushEvent(HttpServletRequest request, boolean isInsertInitialToken,
			UserSession session, PushTokensMobileRequest statusRequest) {

		CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request, session.getUsername());
		event.setCustomStringData(PushTokenInteraction.TOKEN, statusRequest.getToken());
		event.setCustomStringData(PushTokenInteraction.DEVICE_TYPE, statusRequest.getDeviceType());
		event.setCustomStringData(PushTokenInteraction.DEVICE_MODEL, statusRequest.getDeviceModel());
		event.setCustomStringData(PushTokenInteraction.DEVICE_NAME, statusRequest.getDeviceName());
		event.setCustomStringData(PushTokenInteraction.STATUS, isInsertInitialToken ?  PushTokenStatus.INITIAL.name() : statusRequest.getStatus().name());

		return event;
	}
	
	/**
	 * @param request
	 * @param status
	 * @param isInsertInitialToken
	 * @param session
	 * @param statusRequest
	 */
	private void logTokenStatus(HttpServletRequest request, MobileResponseStatus status, Boolean isInsertInitialToken,
			final UserSession session, final PushTokensMobileRequest statusRequest) {
		if (shouldLogInitialPushEvent(isInsertInitialToken, status)) {
			final CustomerInteractionEvent event = logPushEvent(request, isInsertInitialToken, session, statusRequest);

			session.getCustomerService().logPushTokenEvent(event, isInsertInitialToken, true);
		}
	}

	/**
	 * Method used to save a push token when the user enable/disable the push token
	 * 
	 * @param request
	 * @param response
	 * @return A view and a success status if the object was successfully saved
	 * @throws Exception
	 */
	public ModelAndView mobileSavePushToken(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> model = new HashMap<>(NumberUtils.INTEGER_ONE);
		List<PushTokensRequest> putRequest = new ArrayList<>(NumberUtils.INTEGER_ONE);
		boolean isInsert = false;

		final PushTokensMobileRequest mobileRequest = new PushTokensMobileRequest(request);
		MobileResponseStatus status = MobileResponseStatus.SUCCESS;
		final UserSession session = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION); 
		final String profileId = session.getCustomerProfile().getProfileId();

		if (StringUtils.isBlank(mobileRequest.getToken())) {
			LOG.error("Error: SavePushToken - No token received");
			model.put(STATUS, MobileResponseStatus.ERROR);
			return new ModelAndView(PUSH_TOKEN_RESPONSE, model);

		}

		// Get PushToken
		PushTokensRequest getRequest = new PushTokensRequest();
		getRequest.setProfileId(profileId);
		getRequest.setToken(mobileRequest.getToken());
		final PushTokensResponse getResponse = PushTokensFacade.getInstance().getTokens(getRequest, session);

		// Update PushToken
		PushTokensRequest pushTokenRequest = new PushTokensRequest();
		if (getResponse != null && getResponse.hasData()) {
			final PushTokensDto pushToken = Iterables.getFirst(getResponse.getData(), null);
			pushTokenRequest.copyPushDtoProperties(pushToken);
			pushTokenRequest.setDeviceName(mobileRequest.getDeviceName());
			pushTokenRequest.setStatus(mobileRequest.getStatus());

		// Insert PushToken
		} else {
			pushTokenRequest = new PushTokensRequest(profileId);
			BeanUtils.copyProperties(mobileRequest, pushTokenRequest);
			isInsert = true;
		}

		putRequest.add(pushTokenRequest);

		if (!mobileRequest.hasPermissions() && (mobileRequest.getStatus().equals(PushTokenStatus.ACTIVE) 
				|| mobileRequest.getStatus().equals(PushTokenStatus.INITIAL))) {
			pushTokenRequest.setStatus(PushTokenStatus.INACTIVE);
		}

		final PushTokensResponse putResponse = PushTokensFacade.getInstance().putTokens(putRequest, session);

		CustomerInteractionEvent event = logPushEvent(request, false, session, mobileRequest);
		if (PushTokenStatus.INITIAL.equals(pushTokenRequest.getStatus())) {
			event.setCustomStringData(PushTokenInteraction.STATUS_CHANGE_REASON, PushTokenConstants.PUSH_TOGGLE_DISABLED);
		}

		if (StringUtils.isBlank(putResponse.getError()) || !putResponse.hasErrors()) {
			//Send Push alert, and then log the push object log
			sendAndLogPushAlert(isInsert, mobileRequest, session, event);

		} else {
			status = MobileResponseStatus.ERROR;
			LOG.error("Error: CheckPushTokenStatus - PUT error: " + putResponse.getError());
			session.getCustomerService().logPushTokenEvent(event, isInsert, false);
		}

		model.put(STATUS, status);
		return new ModelAndView(PUSH_TOKEN_RESPONSE, model);
	}

	/**
	 * Send a push/email alert, and then log the push object
	 * 
	 * @param isInsert
	 * @param mobileRequest
	 * @param session
	 * @param event
	 */
	private void sendAndLogPushAlert(boolean isInsert, PushTokensMobileRequest mobileRequest, UserSession session,
			CustomerInteractionEvent event) {

		Map<Object, Object> alertParams = new HashMap<>(NumberUtils.INTEGER_ONE);
		alertParams.put("deviceName", mobileRequest.getDeviceName());
		alertParams.put("name", session.getCustomerProfile().getFirstName());

		new Thread(() -> { 
			//Send|log alert in a new Thread, since customer does not need to wait for it to be sent
			customerService.sendPushDeviceAlert(session.getCustomerProfile(), alertParams, 
					session.getPreferredLanguage(), mobileRequest.isActive());
			//Log push event
			session.getCustomerService().logPushTokenEvent(event, isInsert, true);
		}).start();
	}

	/**
	 * Set the status for the token provided, depending on the current object status
	 * 
	 * @param pushRequest
	 * @param pushToken
	 * @param statusRequest
	 */
	private void setTokenStatus(PushTokensRequest pushRequest, PushTokensDto pushToken, PushTokensMobileRequest statusRequest) {
		if (statusRequest.hasPermissions()) {
			// If token was invalid, and has permissions, set to default or active depending on push toggle
			if (StringUtils.equals(PushTokenStatus.INACTIVE.name(), pushToken.getStatus())) {
				pushRequest.setStatus(statusRequest.isActive() ? PushTokenStatus.ACTIVE : PushTokenStatus.INITIAL);
			} else if (StringUtils.equals(PushTokenStatus.PURGED.name(), pushToken.getStatus())) {
				pushRequest.setStatus(PushTokenStatus.INITIAL);
			}
		} else if (StringUtils.equals(PushTokenStatus.INITIAL.name(), pushToken.getStatus())) {
			pushRequest.setStatus(PushTokenStatus.INACTIVE);
		}
		
	}

	/**
	 * Check if the response has any data available
	 * 
	 * @param response
	 * @return if the response has data
	 */
	private boolean responseHasData(PushTokensResponse response) {
		return response != null && response.hasData();
	}

	private boolean shouldLogInitialPushEvent(boolean isInsertInitialToken, MobileResponseStatus status) {
		return isInsertInitialToken && status == MobileResponseStatus.SUCCESS ;

	}

}
