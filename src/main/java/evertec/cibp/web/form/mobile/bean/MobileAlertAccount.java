package evertec.cibp.web.form.mobile.bean;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.web.utils.AccountMaskUtil;

public class MobileAlertAccount {

	private String id;
	private String nickname;
	private String number;
	private List<MobileAlertAccount> plastics;
	private List<MobileAlert> alerts;
	
	public MobileAlertAccount() {
	}
	
	public MobileAlertAccount(TVFrontendAccount account) {
		this.id = account.getFrontEndId();
		this.nickname = account.getNickname();
		String suffix = StringUtils.defaultIfEmpty(account.getAccountNumberSuffix(), StringUtils.EMPTY);
		this.number = new AccountMaskUtil().mask(account.getAccountNumber()) + " " + suffix;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}

	public List<MobileAlertAccount> getPlastics() {
		return plastics;
	}

	public void setPlastics(List<MobileAlertAccount> plastics) {
		this.plastics = plastics;
	}

	public List<MobileAlert> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<MobileAlert> alerts) {
		this.alerts = alerts;
	}
	
	
	
}
