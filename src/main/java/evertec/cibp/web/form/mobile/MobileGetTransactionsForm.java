package evertec.cibp.web.form.mobile;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.MobileResponseStatus;
import evertec.cibp.web.form.mobile.bean.MobileGetTransactionsBean;
import evertec.cibp.web.response.MobileDeviceProfileResponse;
import evertec.cibp.web.response.MobileTransactionResponse;
import evertec.cibp.web.utils.MobileUtil;
import evertec.cibp.web.utils.requests.GetAccountsRequest;

public class MobileGetTransactionsForm extends CancellableFormController {

	protected final Log logger = LogFactory.getLog(getClass());
	private CustomerServices customerService;
	

	public MobileGetTransactionsForm() {
		super();
		setCommandClass(MobileGetTransactionsBean.class);
		setCommandName("mobileTransactionInfo");
		setFormView("mobileTransactionInfo");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object submit,
			BindException error) throws Exception {
		
		Map<String, Object> model = new HashMap<>();

		List<AbstractTransaction> transactions = new LinkedList<AbstractTransaction>();
		List<AbstractTransaction> inprocess = new LinkedList<AbstractTransaction>();

		MobileGetTransactionsBean mobileTransactionBean = (MobileGetTransactionsBean) submit;

		if(customerService == null) {
			logger.error("CustomerService is null");
		} if(GenericValidator.isBlankOrNull(mobileTransactionBean.getDeviceToken()) ||
				GenericValidator.isBlankOrNull(mobileTransactionBean.getAccountNumber()) ||
				GenericValidator.isBlankOrNull(mobileTransactionBean.getProductId()) ||
				GenericValidator.isBlankOrNull(mobileTransactionBean.getSection()) ||
				GenericValidator.isBlankOrNull(mobileTransactionBean.getAccountType())) {
			logger.error("Device token, Account Number, Product Id, Section or Account Type were null and all are required");
		} else {
			
			MobileDeviceProfileResponse deviceProfileRespnse = MobileUtil.getProfileByDeviceToken(request, mobileTransactionBean.getDeviceToken(), customerService);
			
			if(deviceProfileRespnse != null && deviceProfileRespnse.getProfile() != null && deviceProfileRespnse.getStatus() == MobileResponseStatus.SUCCESS) {
				BankingSession bankingSession = MobileUtil.getBankingSession(request, deviceProfileRespnse.getProfile(), customerService);

				List<TVFrontendAccount> accounts = MobileUtil.getAccounts(request, bankingSession, deviceProfileRespnse.getProfile(), customerService);
				
				String accountNumber = mobileTransactionBean.getAccountNumber();
				if(!GenericValidator.isBlankOrNull(accountNumber) && accountNumber.length() > 4) {
					accountNumber = accountNumber.substring(accountNumber.length() - 4);
				}
				String productId = mobileTransactionBean.getProductId();
				String section = mobileTransactionBean.getSection();
				String type = mobileTransactionBean.getAccountType();

				GetAccountsRequest utilRequest = new GetAccountsRequest();
				utilRequest.setAccountNumber(accountNumber);
				utilRequest.setAccounts(accounts);
				utilRequest.setProductId(productId);
				utilRequest.setSection(section);
				utilRequest.setType(type);

				TVFrontendAccount theAccount = MobileUtil.getAccountByAccountNumberAndTypeAndProductIdAndSection(utilRequest);

				if(theAccount != null) {
					MobileTransactionResponse transactionsResponse = MobileUtil.getAccountTransactions(request, bankingSession, deviceProfileRespnse.getProfile(), theAccount, customerService);
					if(transactionsResponse != null && transactionsResponse.getStatus() == MobileResponseStatus.SUCCESS) {
						if(transactionsResponse.getTransactions() != null) {
							transactions = transactionsResponse.getTransactions();
						}
						if(transactionsResponse.getInprocess() != null) {
							inprocess = transactionsResponse.getInprocess();
						}
					}
				} else {
					logger.error("The account was not found. AccountNumber: "+mobileTransactionBean.getAccountNumber());
				}	
				bankingSession = null;
			} else {
				logger.error("There was an error retrieving the device for the customer. Device Token: "+mobileTransactionBean.getDeviceToken());
			}
		}
		model.put("transactions", transactions);
		model.put("inprocess", inprocess);
		
		return showForm(request, error, "mobileTransactionInfo", model);
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}
}
