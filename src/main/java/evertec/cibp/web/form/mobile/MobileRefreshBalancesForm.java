package evertec.cibp.web.form.mobile;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.web.form.mobile.bean.MobileRefreshBalancesBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;


public class MobileRefreshBalancesForm extends CancellableFormController {
	
	private static final String VIEW_NAME = "mobileRefreshBalances";
	
	public MobileRefreshBalancesForm() {
		super();
		setCommandClass(MobileRefreshBalancesBean.class);
		setCommandName(VIEW_NAME);
		setFormView(VIEW_NAME);
	}
	
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object submit,
			BindException error) throws Exception {
		
		boolean balancesChanged = false;
		Map<String, Object> model = new LinkedHashMap<>();
		MobileRefreshBalancesBean bean = (MobileRefreshBalancesBean) submit;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		if (GenericValidator.isBlankOrNull(bean.getAccountFrontEndId())) {
			logger.error("AccountFrontEndId is null.");
			return showForm(request, error, VIEW_NAME, model);
		}
		
		List<TVFrontendAccount> frontEndAcct2Update = new LinkedList<>();
		TVFrontendAccount account = Utils.getAccountById(bean.getAccountFrontEndId(), userSession.getAccounts());
		frontEndAcct2Update.add(account);
		BigDecimal oldBal = account.getPortalBalance();
		userSession.updateAccountBalance(frontEndAcct2Update, "0");
		userSession.setMobileCashAccounts(null);
		
		account = Utils.getAccountByKey(account.getAccountKey(), userSession.getAccounts());
		BigDecimal newBal = account.getPortalBalance();
		if (oldBal.compareTo(newBal) != 0)
			balancesChanged = true;
		
		model.put("balancesChanged", balancesChanged);
		
		return showForm(request, error, VIEW_NAME, model);
		
	}
	
}
