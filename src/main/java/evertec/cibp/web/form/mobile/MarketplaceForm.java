/**
 *  Controller for Marketplace 
 */
package evertec.cibp.web.form.mobile;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.UriUtils;

import com.google.gson.JsonObject;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.services.interfaces.CustomerInteractionService;
import evertec.cibp.crypto.CryptoUtils;
import evertec.cibp.web.WebAppController;
import evertec.cibp.web.enums.OtpChallengeStatus;
import evertec.cibp.web.enums.UnicaAuthCode;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.OtpChallengeUtils;
import evertec.cibp.web.utils.Utils;
import evertec.solicitudunica.response.UnicaAccessTokenResp;


/**
 * @author Raul Mora
 * @version 1.0.0
 * @since 1.2
 * @see MarketplaceForm
 */
public class MarketplaceForm extends MultiActionController {
	
	private int resendCodeMaxRetries;// Cibp Properties Max Resend Retries
	private int resendCodeExpiredPeriod;// Cibp Properties Expired Period Minutes
	private int validateCodeMaxRetries;// Cibp Properties Max Validate Retries
    private int limitProfileCreatedDays; 
    private int limitInfoProfileEditedDays; 
    private int limitOobEditedDays;
    private int accountOldMonths;
    private String unicaUrl;
    private String unicaEncryptionKey;
    private String unicaEncryptionInitVector;
    
    private static final String UNICA_AUTH_CODE = "UNICA_AUTHORIZATION_CODE";
    private static final String UNICA_REFERRER = "UNICA_REFERRER";
    private static final String UNICA_ACCESS_TOKEN = "UNICA_ACCESS_TOKEN";
	
	/**
	 * Default constructor.
	 */
	public MarketplaceForm () {
		this.resendCodeMaxRetries = NumberUtils.INTEGER_ZERO;
		this.resendCodeExpiredPeriod = NumberUtils.INTEGER_ZERO;
		this.validateCodeMaxRetries = NumberUtils.INTEGER_ZERO;
    	this.limitProfileCreatedDays = NumberUtils.INTEGER_ZERO;
    	this.limitInfoProfileEditedDays = NumberUtils.INTEGER_ZERO;
    	this.limitOobEditedDays = NumberUtils.INTEGER_ZERO;
    	this.accountOldMonths = NumberUtils.INTEGER_ZERO;
    	this.unicaUrl = StringUtils.EMPTY;
    	this.unicaEncryptionKey = StringUtils.EMPTY;
    	this.unicaEncryptionInitVector = StringUtils.EMPTY;
	}
	

	/**
	 * Constructor for Servlet constructor-arg
	 * 
	 * @param resendCodeMaxRetries
	 * @param resendCodeExpiredPeriod
	 * @param validateCodeMaxRetries
	 * @param limitDaysProfileCreated
	 * @param limitInfoProfileEditedDays
	 * @param limitOobEditedDays
	 * @param unicaUrl
	 */
	public MarketplaceForm (int resendCodeMaxRetries, int resendCodeExpiredPeriod, int validateCodeMaxRetries,
			int limitDaysProfileCreated, int limitInfoProfileEditedDays, int limitOobEditedDays, int accountOldMonths,
			String unicaUrl, String unicaEncryptionKey, String unicaEncryptionInitVector) {
		this.resendCodeMaxRetries = resendCodeMaxRetries;
		this.resendCodeExpiredPeriod = resendCodeExpiredPeriod;
		this.validateCodeMaxRetries = validateCodeMaxRetries;
		this.limitProfileCreatedDays = limitDaysProfileCreated;
		this.limitInfoProfileEditedDays = limitInfoProfileEditedDays;
		this.limitOobEditedDays = limitOobEditedDays;
		this.accountOldMonths = accountOldMonths;
		this.setUnicaUrl(unicaUrl);
		this.setUnicaEncryptionKey(unicaEncryptionKey);
		this.setUnicaEncryptionInitVector(unicaEncryptionInitVector);
	}
    
    enum MarketeplaceAction {
  	  ERROR,
	  NO_CHALLENGE_PRESENTED,
	  OOB_CHALLENGE,
	  EMAIL_CHALLENGE,
	}

	/**
	 * Logs a customer event depending on the Marketplace product
	 * 
	 * @param request
	 * @param response
	 * @return the model and view
	 */
	public ModelAndView acceptMarketplaceTerms (HttpServletRequest request, HttpServletResponse response) {

		final String noProductError = "Marketplace Accept event could not be logged: No product received"; // no product error message
		final String noEventError = "No Marketplace Accept event for product %s: %s"; // no event error message
		final String view = "acceptMarketplaceTerms"; // the view name

		final String product = request.getParameter("product"); // the marketplace product
		final Logger log = Logger.getLogger(WebAppController.class); // the logger
		final HttpSession httpSession = request.getSession(); // the http session

		Map<String, Object> model = new HashMap<>(1); // the model
		Boolean success = false; // the event logged success flag

		if (StringUtils.isNotEmpty(product) && product.matches("[a-zA-Z0-9_]+")) {
			final String event = product.toUpperCase() + "_ACCEPT_TC"; // the event to be logged
			httpSession.setAttribute(UNICA_REFERRER, product);

			try {

				if (CustomerInteractionEventType.fromString(event) instanceof CustomerInteractionEventType) {

					final CustomerInteractionEventType e = (CustomerInteractionEventType) CustomerInteractionEventType.fromString(event); // event
					logEvent(request, e);
					
					success = true;
				}

			} catch (IllegalArgumentException e) {
				log.error(StringEscapeUtils.escapeJava(String.format(noEventError, product, e.getMessage())));
			}
		} else {
			log.error(noProductError);
		}

		model.put("success", success);

		return new ModelAndView(view, model);
	}
	
	/**
     * Service that generates an OTP Email code and sends it to customer Email
     *
     * @param request  The HttpServletRequest request
     * @param response The HttpServletResponse response
     * @return The model and view
     */
	public ModelAndView generateOtpCode(HttpServletRequest request, HttpServletResponse response) {

		final UserSession userSession = Utils.getUserSession(request); // the user session

		final String viewName = "generateOtpCode";//generateOtpCode view
		Map<String, Object> model = new HashMap<>();//HashMap model

		String resendCode = request.getParameter("resendCode");

		final boolean isResendCode = !GenericValidator.isBlankOrNull(resendCode) && "true".equalsIgnoreCase(resendCode);

		if (isResendCode && OtpChallengeUtils.hasReachedResendLimit(userSession, resendCodeMaxRetries)) {
			model.put("error", false);
			model.put("status", OtpChallengeStatus.RESEND_LIMIT_EXCEEDED.toString());
		} else if (OtpChallengeUtils.sendChallenge(userSession)) {
			model.put("error", false);

			//Validates if it is the last Resend Code available
			if(OtpChallengeUtils.hasReachedResendLimit(userSession, resendCodeMaxRetries)) {
				model.put("status", OtpChallengeStatus.RESEND_LIMIT_REACHED.toString());
			} else {
				model.put("status", OtpChallengeStatus.OTP_SERVICE_SUCCESS.toString());
			}
		} else {
			model.put("error", true);
			model.put("status",  OtpChallengeStatus.OTP_SERVICE_FAILED.toString());
		}
			
		return new ModelAndView(viewName, model);
	}

    
    /**
     * Service that validates an OTP Email request code with UserSession Code
     *
     * @param request  The HttpServletRequest request
     * @param response The HttpServletResponse response
     * @return The model and view
     */
    public ModelAndView validateOtpCode(HttpServletRequest request, HttpServletResponse response) {
        
        final UserSession userSession = Utils.getUserSession(request); // the user session
        final HttpSession httpSession = request.getSession(); // the http session
        
        final String viewName = "validateOtpCode";//validateOtpCode view
        Map<String, Object> model = new HashMap<>();//HashMap model
        
        String userEntryCode = request.getParameter("code"); //Otp Code to Validate
        String sessionCode = userSession.getOtpChallengeCode(); //Otp Code Sent

        final boolean isMissingInfo = GenericValidator.isBlankOrNull(userEntryCode) || GenericValidator.isBlankOrNull(sessionCode);

        if (isMissingInfo) {
        	model.put("error", true);
        	model.put("status", OtpChallengeStatus.MISSING_INFO.toString());
        } else if (OtpChallengeUtils.hasCodeExpired(userSession, resendCodeExpiredPeriod)) {
        	//If code received has expired
        	model.put("error", false);

        	if (validateLimitReached(userSession) || OtpChallengeUtils.hasReachedResendLimit(userSession, resendCodeMaxRetries)) {
        		model.put("status", OtpChallengeStatus.VALIDATION_LIMIT.toString());
        		setOtpAuthenticationResult(userSession, CustomerInteractionEventType.EMAIL_AUTHENTICATION_FAIL, request);
        		httpSession.setAttribute(UNICA_AUTH_CODE, UnicaAuthCode.EMAIL_AUTHENTICATION_FAIL);
        	} else {
        		model.put("status", OtpChallengeStatus.CODE_EXPIRED.toString());
        	}
        }
        else if (userEntryCode.equals(sessionCode)) {
        	//If code the same
        	model.put("error", false);
        	model.put("status", OtpChallengeStatus.VALIDATION_SUCCESS.toString());

        	setOtpAuthenticationResult(userSession, CustomerInteractionEventType.EMAIL_AUTHENTICATION_SUCCESS, request);
        	httpSession.setAttribute(UNICA_AUTH_CODE, UnicaAuthCode.EMAIL_AUTHENTICATION_SUCCESS);
        } else {
        	//If code is not the same add to counter
        	model.put("error", false);

        	if (validateLimitReached(userSession)) {
        		model.put("status", OtpChallengeStatus.VALIDATION_LIMIT.toString());
        		setOtpAuthenticationResult(userSession, CustomerInteractionEventType.EMAIL_AUTHENTICATION_FAIL, request);
        		httpSession.setAttribute(UNICA_AUTH_CODE, UnicaAuthCode.EMAIL_AUTHENTICATION_FAIL);
        	} else {
        		model.put("status", OtpChallengeStatus.VALIDATION_FAILED.toString());
        	}
        }
        
        return new ModelAndView(viewName, model);
    }
    
	/**
	 * @param request
	 * @param response
	 * @return the model and view
	 */
	public ModelAndView determineAuthenticationChallenge (HttpServletRequest request, HttpServletResponse response) {

		final String view = "determineAuthenticationChallenge"; // the view name
		final String error = "Error determineAuthenticationChallenge"; // no event error message
		final Logger log = Logger.getLogger(WebAppController.class); // the logger
		String action = MarketeplaceAction.ERROR.toString();
		Map<String, Object> model = new HashMap<>(1); // the model

		try {

			final UserSession userSession = Utils.getUserSession(request); // the user session
			final HttpSession httpSession = request.getSession(); // the http session

			CustomerProfile customerProfile = userSession.getCustomerProfile();
			Date creationDateProfile = customerProfile.getCreationDate();
			Date todayDate = Calendar.getInstance().getTime();

			final long diffTime = todayDate.getTime() - creationDateProfile.getTime();
			long diffDays = diffTime / (1000 * 60 * 60 * 24);

			if (httpSession.getAttribute(UNICA_AUTH_CODE) != null ||
					diffDays <= limitProfileCreatedDays ||
					OtpChallengeUtils.hasReachedResendLimit(userSession, resendCodeMaxRetries) ||
					!Utils.isAccountOldEnough(accountOldMonths, userSession.getPortalAccounts(), userSession)) {
				action = setEventNoChallengePresented(request,httpSession);
			}
			else if (validateOobAuthenticatedAndModified(userSession)) {
				action = MarketeplaceAction.NO_CHALLENGE_PRESENTED.toString();
				logEvent(request, CustomerInteractionEventType.OOB_AUTHENTICATION_SUCCESS);
				httpSession.setAttribute(UNICA_AUTH_CODE, UnicaAuthCode.OOB_AUTHENTICATION_SUCCESS);
				
			} else if (validateGetOobEnrollAndIsOobModified(userSession)) {
				action = MarketeplaceAction.OOB_CHALLENGE.toString();
				
			}
			else if (isEmailModified(userSession)) {
				action = setEventNoChallengePresented(request,httpSession);
			}
			else {
			
				action = MarketeplaceAction.EMAIL_CHALLENGE.toString();
			}

		} catch (IllegalArgumentException e) {
			action = MarketeplaceAction.ERROR.toString();
			log.error(StringEscapeUtils.escapeJava(String.format(error, e.getMessage())));
		} 

		model.put("action", action);

		return new ModelAndView(view, model);
	}
	
	private Boolean validateOobAuthenticatedAndModified(UserSession userSession) {
		return userSession.isOobAuthenticated() && !isOobModified(userSession);
	}
	
	private Boolean validateGetOobEnrollAndIsOobModified(UserSession userSession) {
		return userSession.getOobEnroll() && !isOobModified(userSession);
	}
	
	private String setEventNoChallengePresented(HttpServletRequest request, HttpSession httpSession) {
		String eventAction = MarketeplaceAction.NO_CHALLENGE_PRESENTED.toString();
		logEvent(request, CustomerInteractionEventType.NO_AUTHENTICATION_PRESENTED);
		
		if (httpSession.getAttribute(UNICA_AUTH_CODE) == null) {
			httpSession.setAttribute(UNICA_AUTH_CODE, UnicaAuthCode.NO_AUTHENTICATION_PRESENTED);
		}
		
		return eventAction;
	}

	/**
	 * Service that retrieves Unica URL
	 *
	 * @param request  The HttpServletRequest request
	 * @param response The HttpServletResponse response
	 * @return The model and view
	 */
	public ModelAndView getUnicaUrl(HttpServletRequest request, HttpServletResponse response) {

		final Logger log = Logger.getLogger(MarketplaceForm.class); // the logger
		final HttpSession httpSession = request.getSession(); // the http session
		final UserSession userSession = Utils.getUserSession(request); // the user session
		final String viewName = "getUnicaUrl";
		
		Map<String, Object> model = new HashMap<>();
		UnicaAccessTokenResp token = null;

		if (httpSession.getAttribute(UNICA_REFERRER) == null) {
			log.error("No referrer for Unica web");

		} else {

			if (httpSession.getAttribute(UNICA_ACCESS_TOKEN) instanceof UnicaAccessTokenResp) {
				token = (UnicaAccessTokenResp) httpSession.getAttribute(UNICA_ACCESS_TOKEN);

				if (token.isExpired()) {
					token = userSession.getCustomerService().getUnicaAccessToken();
				}
			} else {
				token = userSession.getCustomerService().getUnicaAccessToken();
			}

			if (token == null || StringUtils.isEmpty(token.getAccessToken())) {
				log.error("No access token for Unica web");

			} else {

				String url = generateUnicaUrl(request, userSession, log);

				httpSession.setAttribute(UNICA_ACCESS_TOKEN, token);

				if (StringUtils.isNotEmpty(url)) {
					model.put("url", url);
					model.put("token", token.getAccessToken());
				}

			}
		}
		return new ModelAndView(viewName, model);
	}

	private String generateUnicaUrl(HttpServletRequest request, final UserSession userSession, final Logger log) {	
		String url = StringUtils.EMPTY;
		boolean isOobAuthenticated = Boolean.valueOf(request.getParameter("isOobAuthenticated"));
		HttpSession httpSession = request.getSession();
		
		String referrer = httpSession.getAttribute(UNICA_REFERRER).toString().toLowerCase();
		
		UnicaAuthCode authCode = null;
		
		if (isOobAuthenticated && userSession.isOobAuthenticated()) {
			authCode = UnicaAuthCode.OOB_AUTHENTICATION_SUCCESS;
			httpSession.setAttribute(UNICA_AUTH_CODE, UnicaAuthCode.OOB_AUTHENTICATION_SUCCESS);
			logEvent(request, CustomerInteractionEventType.OOB_AUTHENTICATION_SUCCESS);
			
		} else if (httpSession.getAttribute(UNICA_AUTH_CODE) != null) {
			authCode = UnicaAuthCode.valueOf(httpSession.getAttribute(UNICA_AUTH_CODE).toString());
		}

		if (authCode != null) {
			UriComponentsBuilder refDataBuilder = UriComponentsBuilder.newInstance()
					.queryParam("referrer", referrer)
					.queryParam("language", userSession.getPreferredLanguage().getCode().toLowerCase())
					.queryParam("authCode", authCode.getCode());

			String ssoToken = getSsoTokenForUnica(userSession, refDataBuilder.build().getQuery(), log);
			
			refDataBuilder.replaceQueryParam("authCode"); //Remove Query used to save in DB

			if (StringUtils.isNotEmpty(ssoToken)) {
				UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(this.unicaUrl)
						.query(refDataBuilder.build().getQuery())
						.queryParam("code", ssoToken);
				
				url = uriBuilder.build().toString();
			}
		}
		return url;
	}
	
	private String getSsoTokenForUnica(UserSession userSession, String referenceData, final Logger log) {
		CryptoUtils cryptoUtils = new CryptoUtils();
		JsonObject json = new JsonObject();
		String ssoToken = StringUtils.EMPTY;
		
		ssoToken = userSession.getCustomerService().tokenGenerate(
				userSession.getCustomerProfile(), StringUtils.EMPTY, referenceData);
		
		if (StringUtils.isNotEmpty(ssoToken)) { 
			json.addProperty("token", ssoToken);

			ssoToken = cryptoUtils.encrypt(unicaEncryptionKey, unicaEncryptionInitVector, json.toString());

			if (StringUtils.isNotEmpty(ssoToken)) { 
				try {
					ssoToken = UriUtils.encodeQueryParam(ssoToken, StandardCharsets.UTF_8.name());
				} catch (UnsupportedEncodingException e) {
					ssoToken = StringUtils.EMPTY;
					log.error(String.format("Encypted data could not be encoded: %s", e.getMessage()));
				}
			}
		}
		
		return ssoToken;
	}
    
    private void resetOtpChallenge(UserSession userSession) {
    	userSession.setOtpChallengeCode(null);
    	userSession.setOtpLastChallengeDate(null);
    	userSession.setOtpSendRetries(0);
    	userSession.setOtpValidateRetries(0);
    }
    
    private boolean validateLimitReached (UserSession userSession) {
    	userSession.setOtpValidateRetries(userSession.getOtpValidateRetries() + 1);
    	boolean limitReached = OtpChallengeUtils.hasReachedValidateLimit(userSession, validateCodeMaxRetries);
    	
    	return limitReached;
    }
    
    private void setOtpAuthenticationResult(UserSession userSession, CustomerInteractionEventType result, HttpServletRequest request) {
    	logEvent(request, result);
        resetOtpChallenge(userSession);
    }
    
    /**
     * Log Customer Event
     * @param request
     * @param e
     */
    private void logEvent(HttpServletRequest request, CustomerInteractionEventType e) {
    	final UserSession userSession = Utils.getUserSession(request); // the user session
    	final String username = userSession.getUsername(); // the username
    	final CustomerInteractionEvent interaction = Utils.generateCustomerInteractionEvent(request, username); // the event
    	
    	userSession.getCustomerService().getInteractionService().logCustomerEvent(interaction, e, InteractionSeverity.INFO);
    }
	
	public boolean isOobModified(UserSession userSession) {
		String profileId = userSession.getCustomerProfile().getProfileId();
		CustomerInteractionService interactionService = userSession.getCustomerService().getInteractionService();
		List<String> oobChangeEvents = Collections.unmodifiableList(Arrays.asList(
				CustomerInteractionEventType.ENROLLMENT_OOB.toString(),
				CustomerInteractionEventType.CHANGE_SMS_PHONE_SUCCESS.toString(),
				CustomerInteractionEventType.REGISTER_SECONDARY_PHONE_OOB.toString()));
		
		return interactionService.eventExists(profileId, oobChangeEvents, limitOobEditedDays);
	}
	
	public boolean isEmailModified(UserSession userSession) {
		String profileId = userSession.getCustomerProfile().getProfileId();
		CustomerInteractionService interactionService = userSession.getCustomerService().getInteractionService();
		List<String> emailChangeEvents = Collections.unmodifiableList(Arrays.asList(
				CustomerInteractionEventType.CHANGE_EMAIL_SUCCESS.toString(),
				CustomerInteractionEventType.EMAIL_CHANGE.toString()));
		
		return interactionService.eventExists(profileId, emailChangeEvents, limitInfoProfileEditedDays);
	}
	
	public void setResendCodeMaxRetries(int resendCodeMaxRetries) {
		this.resendCodeMaxRetries = resendCodeMaxRetries;
	}

	public void setResendCodeExpiredPeriod(int resendCodeExpiredPeriod) {
		this.resendCodeExpiredPeriod = resendCodeExpiredPeriod;
	}

	public void setValidateCodeMaxRetries(int validateCodeMaxRetries) {
		this.validateCodeMaxRetries = validateCodeMaxRetries;
	}

	public void setLimitProfileCreatedDays(Integer limitProfileCreatedDays) {
		this.limitProfileCreatedDays = limitProfileCreatedDays;
	}

	public void setLimitInfoProfileEditedDays(Integer limitInfoProfileEditedDays) {
		this.limitInfoProfileEditedDays = limitInfoProfileEditedDays;
	}

	public void setLimitOobEditedDays(Integer limitOobEditedDays) {
		this.limitOobEditedDays = limitOobEditedDays;
	}
	
	public void setAccountOldMonths(int accountOldMonths) {
		this.accountOldMonths = accountOldMonths;
	}

	public void setUnicaUrl(String unicaUrl) {
		this.unicaUrl = unicaUrl;
	}

	public void setUnicaEncryptionKey(String unicaEncryptionKey) {
		this.unicaEncryptionKey = unicaEncryptionKey;
	}

	public void setUnicaEncryptionInitVector(String unicaEncryptionInitVector) {
		this.unicaEncryptionInitVector = unicaEncryptionInitVector;
	}
	
}
