package evertec.cibp.web.form.mobile;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import bppr.tv.beans.transactions.Transaction;
import evertec.cibp.core.enums.OnOffEnums;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontEndAccountCardPlastic;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.ManageMobileResponses;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.onoff.OnOffCoreModel;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.facade.EmailNotificationFacade;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.core.utils.OnOffUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.utils.Utils;
import evertec.onoff.models.PlasticOnOffModel;
import evertec.onoff.models.PlasticOnOffRequest;
import evertec.onoff.models.PlasticOnOffResponse;
import evertec.onoff.models.ResponseStatus;

public class MobileAthOnOffController extends CancellableFormController {

	private static final String MOBILE_ATH_ONOFF = "mobileAthOnOff";
	private static final String ACTION = "action";
	private static final String ACC_FRONTENDID = "accFrontEndId";
	private static final String CARD_FRONTENDID = "cardFrontEndId";
	private static final String ERROR = "error";
	private static final String USER_SESSION = "userSession";
	private static final String ON = "ON";
	private static final String OFF = "OFF";
	private static final String RESPONSE_STATUS = "responseStatus";

	private CustomerServices customerService;
	private TVFrontendAccount selectedAccount;	

	public MobileAthOnOffController() {
		super();
		setCommandClass(MobileAthOnOffController.class);
		setCommandName(MOBILE_ATH_ONOFF);
		setFormView(MOBILE_ATH_ONOFF);
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object submit,
			BindException error) throws Exception {
		Map<String, Object> model = new HashMap<>();

		String act = request.getParameter(ACTION);
		String cardFrontEndId = request.getParameter(CARD_FRONTENDID);

		if (customerService == null) {
			logger.error("CustomerService is null");
			model.put(ERROR, true);
			return showForm(request, error, MOBILE_ATH_ONOFF, model);
		}

		if (GenericValidator.isBlankOrNull(act) || GenericValidator.isBlankOrNull(cardFrontEndId)) {
			logger.error("Required values are missing in MobileAthOnOffController: OnSubmit method.");
			model.put(ERROR, true);
			return showForm(request, error, MOBILE_ATH_ONOFF, model);
		}

		return this.updatePlastic(request, error);
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();

		String act = request.getParameter(ACTION);

		if (GenericValidator.isBlankOrNull(act))
			model.put("plastics", displayPlastics(request));

		return model;
	}

	private ModelAndView updatePlastic(HttpServletRequest request, BindException error) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION);
		CustomerProfile customerProfile = userSession.getCustomerProfile();

		CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request,
				customerProfile.getUsername());

		String selectedPlasticNumber = "";
		String clientId = OnOffEnums.CIBP_WEB.getValue();
		String action = request.getParameter(ACTION);
		String cardFrontEndId = request.getParameter(CARD_FRONTENDID);

		CardPlasticFrontend frontRecord = Utils.getPlasticRecordByFrontEndId(cardFrontEndId,
				selectedAccount.getOnOffPlasticList().toArray(new CardPlasticFrontend[0]));

		if (frontRecord == null) {
			logger.error("MobileAthOnOffController: Record is null or has an error");
			model.put(RESPONSE_STATUS, ManageMobileResponses.NEW_STATUS_RESPONSE_FAILED.getCode());
			return showForm(request, error, MOBILE_ATH_ONOFF, model);
		}

		CardPlasticAccountRecord record = frontRecord.getCardPlasticAccountRecord();

		selectedPlasticNumber = record.getCardPANId();

		if (interactionEvent != null && !GenericValidator.isBlankOrNull(interactionEvent.getIp())) {
			interactionEvent.resetProperties();
			interactionEvent.putString("ACTION", action);
			interactionEvent.putString("CARD_NUMBER", selectedPlasticNumber);
			interactionEvent.putString("ACCOUNT", record.getAccount().getAccountID());
			interactionEvent.putString("UPDATE_SOURCE", "Mi Banco");
			interactionEvent.putString("UPDATED_BY", userSession.getCustomerProfile().getFullName() + " (" + userSession.getUsername() + ")");
		}
		
		OnOffCoreModel onOffModel = new OnOffCoreModel();
		onOffModel.setCustomerProfile(userSession.getCustomerProfile());
		onOffModel.setIp(request.getRemoteAddr());
		onOffModel.setClientId(clientId);
	
		PlasticOnOffRequest athRequest = OnOffUtils.createOnOffRequest(frontRecord, onOffModel, action);
		return manageResponse(request, error, athRequest, interactionEvent);
	}

	private CardPlasticFrontend[] displayPlastics(HttpServletRequest request) {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION);

		String frontEndId = request.getParameter(ACC_FRONTENDID);
		selectedAccount = Utils.getAccountById(frontEndId, userSession.getAccounts());

		if (selectedAccount == null || selectedAccount.getOnOffPlasticList() == null || selectedAccount.getOnOffPlasticList().isEmpty())
			return new CardPlasticFrontend[0];

		return sortPlasticsByOwner(selectedAccount.getOnOffPlasticList()).toArray(new CardPlasticFrontend[0]);

	}

	private ModelAndView manageResponse(HttpServletRequest request, BindException error, PlasticOnOffRequest athRequest,
			CustomerInteractionEvent interactionEvent) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION);

		PlasticOnOffResponse resp = customerService.getECCResponseByAction(athRequest);
		ResponseStatus status = resp.getResponseStatus();

		if (resp.getResponseStatus() == null || ResponseStatus.ERROR.equals(resp.getResponseStatus())
				|| resp.getCard() == null || status == null
				|| GenericValidator.isBlankOrNull(resp.getCard().getStatus())) {
			logger.error("AthOnOffResponse is null or has an error");
			model.put(ERROR, true);
			
			logOnOffCardActionFailed(athRequest, interactionEvent);
			
			return showForm(request, error, MOBILE_ATH_ONOFF, model);
		}

		if (status.equals(ResponseStatus.ERROR)) {
			model.put("message", resp.getMessage());
			model.put(RESPONSE_STATUS, ManageMobileResponses.NEW_STATUS_RESPONSE_FAILED.getCode());
			return showForm(request, error, MOBILE_ATH_ONOFF, model);
		}

		ManageMobileResponses cardStatus;
		ResponseStatus responseStatus = ResponseStatus.valueOf(resp.getCard().getStatus());

		switch (responseStatus) {
		case OFF:
			cardStatus = ManageMobileResponses.NEW_STATUS_OFF_RESPONSE_SUCCESS;
			customerService.logOnOffCardAction(interactionEvent, ManageMobileResponses.NEW_STATUS_OFF_RESPONSE_SUCCESS);
			break;
		case ON:
			cardStatus = ManageMobileResponses.NEW_STATUS_ON_RESPONSE_SUCCESS;
			customerService.logOnOffCardAction(interactionEvent, ManageMobileResponses.NEW_STATUS_ON_RESPONSE_SUCCESS);
			break;
		default:
			cardStatus = ManageMobileResponses.NEW_STATUS_RESPONSE_FAILED;
			
			interactionEvent.putString("status", responseStatus.toString());
			logOnOffCardActionFailed(athRequest, interactionEvent);
			
			model.put(RESPONSE_STATUS, cardStatus.getCode());
			return showForm(request, error, MOBILE_ATH_ONOFF, model);
		}

		if (!updateAccountsIndicators(userSession, resp.getCard(), selectedAccount)) {
			model.put(RESPONSE_STATUS, ManageMobileResponses.NEW_STATUS_RESPONSE_FAILED.getCode());
			return showForm(request, error, MOBILE_ATH_ONOFF, model);
		}
		
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.ON_OFF_ALERTS)) {
			sendOnOffActionNotifications(userSession, resp);
		}

		PlasticOnOffModel card = resp.getCard();
		model.put("apiAccountKey", selectedAccount.getAccountNumber());
		model.put("card", card);
		model.put(RESPONSE_STATUS, cardStatus.getCode());
		return showForm(request, error, MOBILE_ATH_ONOFF, model);
	}
	
	public void sendOnOffActionNotifications(UserSession userSession, PlasticOnOffResponse onOffResponse) {
 		// TODO: This BIN validation should be removed at some point so that the email sent is Generic for all cards when CCA and ATH Regular are supported.
		if (OnOffUtils.containsCardBin(onOffResponse.getCard().getCardNumber(), userSession.getAllowedOnOffAthBins())
				&& OnOffUtils.isCardOnOffStatusValid(onOffResponse)) {
				
			userSession.getCustomerService().sendOnOffActionNotifications(userSession.getCustomerProfile(), 
					userSession.getPreferredLanguage(), 
					onOffResponse);
		}
	}
	
	private boolean updateAccountsIndicators(UserSession userSession, PlasticOnOffModel card, TVFrontendAccount selectedAccount) {
		
		Map<String, TVFrontEndAccountCardPlastic> onOffCardPlasticMap = userSession.getOnOffAccounts().getTvFrontEndCardPlasticMap();
		
		TVFrontEndAccountCardPlastic frontEndAcc = onOffCardPlasticMap.get(selectedAccount.getAccountNumber());
		List<CardPlasticFrontend> cards = selectedAccount.getOnOffPlasticList();
		
		if (frontEndAcc == null || cards.isEmpty())
			return false;
		
		List<TVFrontendAccount> accounts = frontEndAcc.getAccount();
		
		int counter = 0;
		ResponseStatus responseStatus = ResponseStatus.valueOf(card.getStatus());
		
		switch (responseStatus) {
		case OFF:
			counter += 1;
			break;
		case ON:
			counter -= 1;
			break;
		default:
			break;
		}

		for (TVFrontendAccount acc: accounts)
			acc.setAthOnOffCount(counter);

		for (CardPlasticFrontend c: cards) {
			if (c.getOnOffModel() == null)
				return false;
			
			String cardNumber = c.getOnOffModel().getCardNumber();
			if (!GenericValidator.isBlankOrNull(cardNumber) && cardNumber.equals(card.getCardNumber())) {
				c.getOnOffModel().setStatus(card.getStatus());
				c.getOnOffModel().setLastOffInfo(card.getLastOffInfo());
				c.getOnOffModel().setLastOnInfo(card.getLastOnInfo());
				return true;
			}
		}
		return true;
	}
	
	private void logOnOffCardActionFailed(PlasticOnOffRequest athRequest, CustomerInteractionEvent interactionEvent) {
		
		if (athRequest.getAction().equalsIgnoreCase(ON)) {
			customerService.logOnOffCardAction(interactionEvent, ManageMobileResponses.NEW_STATUS_ON_RESPONSE_FAILED);
		} else if (athRequest.getAction().equalsIgnoreCase(OFF)) {
			customerService.logOnOffCardAction(interactionEvent, ManageMobileResponses.NEW_STATUS_OFF_RESPONSE_FAILED);
		}
	}
	
	/**
	 * By Michael Ortiz
	 * Sorts plastics by Owner first, and then Not-Owned
	 * Then sorts Not-Owned by alphabetical order.
	 * 
	 * @param cards
	 * @return a sorted list
	 */
	
	public static List<CardPlasticFrontend> sortPlasticsByOwner(List<CardPlasticFrontend> cards) {
		
		ArrayList<CardPlasticFrontend> ownedPlastics = new ArrayList<>();
		ArrayList<CardPlasticFrontend> notOwnedPlastics = new ArrayList<>();
		
		for (CardPlasticFrontend cardPlasticFrontend : cards) {
			
			String cardOwnership = null;
			CardPlasticAccountRecord record = cardPlasticFrontend.getCardPlasticAccountRecord();
			
			if (record != null && record.getOwners().length != 0 && record.getOwners()[0].getCardOwnership() != null) {
				
				cardOwnership = record.getOwners()[0].getCardOwnership();
			}
						
			if (cardOwnership != null && cardOwnership.equalsIgnoreCase("Owned")) {	
				ownedPlastics.add(cardPlasticFrontend);
			} else {
				notOwnedPlastics.add(cardPlasticFrontend);
			}
		}
		
		if (!ownedPlastics.isEmpty()) {
			ownedPlastics = sortOwnedPlastics(ownedPlastics);
		}
		
		if (!notOwnedPlastics.isEmpty()) {
			notOwnedPlastics = sortNotOwnedPlastics(notOwnedPlastics);
			ownedPlastics.addAll(notOwnedPlastics);
		}
				
		if (ownedPlastics.isEmpty() && !notOwnedPlastics.isEmpty()) {
			return notOwnedPlastics;
		}
		
		return ownedPlastics; 
	}
	
	private static ArrayList<CardPlasticFrontend> sortOwnedPlastics(ArrayList<CardPlasticFrontend> list) {
		
		list.sort((a, b) -> {
			
			Transaction plastic1 = a.getCardPlasticAccountRecord().getLastCardTransaction();
			Transaction plastic2 = b.getCardPlasticAccountRecord().getLastCardTransaction();
			
			if ((plastic1 == null || plastic1.getEffectiveDate() == null) && (plastic2 == null || plastic2.getEffectiveDate() == null)) {
	            return 0;
	        } else if (plastic1 == null || plastic1.getEffectiveDate() == null) {
	            return 1;
	        } else if (plastic2 == null || plastic2.getEffectiveDate() == null) {
	            return -1;
	        }
			
	        return plastic1.getEffectiveDate().compareTo(plastic2.getEffectiveDate());
        });
		
		return list;
	}
	
	private static ArrayList<CardPlasticFrontend> sortNotOwnedPlastics(ArrayList<CardPlasticFrontend> list) {
		
		list.sort((a, b) -> {
			
			if (a.getCardPlasticAccountRecord().getEmbossedName() == null && b.getCardPlasticAccountRecord().getEmbossedName() == null) {
	            return 0;
	        } else if (a.getCardPlasticAccountRecord().getEmbossedName() == null) {
	            return 1;
	        } else if (b.getCardPlasticAccountRecord().getEmbossedName() == null) {
	            return -1;
	        }

			return a.getCardPlasticAccountRecord().getEmbossedName().compareTo(b.getCardPlasticAccountRecord().getEmbossedName());	
        });
		
		return list;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}
}
