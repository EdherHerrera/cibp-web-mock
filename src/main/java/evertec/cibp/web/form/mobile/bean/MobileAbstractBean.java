package evertec.cibp.web.form.mobile.bean;

public class MobileAbstractBean {
	private String deviceToken;

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
}