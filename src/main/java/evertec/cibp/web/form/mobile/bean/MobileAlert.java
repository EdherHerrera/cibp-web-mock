package evertec.cibp.web.form.mobile.bean;

import java.math.BigDecimal;


import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import bppr.sms.beans.SMSAccount;
import evertec.cibp.core.models.customer.AlertCondition;
import evertec.cibp.core.models.customer.CustomerBalanceAlert;
import evertec.cibp.core.models.customer.CustomerEmailBalanceAlert;
import evertec.cibp.core.models.messaging.MessageChannelType;

public class MobileAlert {
	
	private String type;
	private String amount;
	private Boolean active;
	private MessageChannelType channel;
	private static final String BALANCE_MORE_THAN = "balance-more-than";
	private static final String BALANCE_LESS_THAN = "balance-less-than";
	public static final String PUSH = "PUSH";
	public static final String EMAIL = "EMAIL";
	public static final String SMS = "SMS";
	public static final String GREATER_THAN = "GT";
	public static final String LESS_THAN = "LT";
	public static final String IDA = "IDA";
	public static final String CCA = "CCA";
	public static final String PRM = "PRM";
	public static final String PUSH_DEVICES = "pushTokensDevices";
	public static final String DEVICE_MANAGEMENT_AUTH = "DeviceManagementAuth";
	public static final String DEVICE_MANAGEMENT_VIEW = "pushdeviceremoval";
	
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getAmount() {
		return amount;
	}
	
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public Boolean getActive() {
		return active;
	}
	
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * @return the channel
	 */
	public MessageChannelType getChannel() {
		return channel;
	}

	/**
	 * @param channel the channel to set
	 */
	public void setChannel(MessageChannelType channel) {
		this.channel = channel;
	}
	
	public static MobileAlert getMobileAlert(CustomerEmailBalanceAlert alert) {
		MobileAlert mobileAlert = new MobileAlert();
		mobileAlert.setActive(true);
		mobileAlert.setChannel(MessageChannelType.EMAIL);
		
		if (alert.getAlertCondition() != null) {
			if (StringUtils.equals(alert.getAlertCondition().getOperator(), GREATER_THAN)) {
				final BigDecimal amountHighValue = alert.getAlertCondition().getAmountHigh(); // amount decimal
				final String amountHigh = amountHighValue != null ? amountHighValue.toString() : ""; // amount string
				
				mobileAlert.setAmount(amountHigh);
				mobileAlert.setType(BALANCE_MORE_THAN);
			} else if (StringUtils.equals(alert.getAlertCondition().getOperator(), LESS_THAN)) {
				final BigDecimal amountLowValue = alert.getAlertCondition().getAmountLow(); // amount decimal
				final String amountLow = amountLowValue != null ? amountLowValue.toString() : ""; // amount string
				
				mobileAlert.setAmount(amountLow);
				mobileAlert.setType(BALANCE_LESS_THAN);	
			}

		}
		return mobileAlert;
	}
	
	public static MobileAlert getMobileAlert(SMSAccount alert) {
		MobileAlert mobileAlert = new MobileAlert();
		mobileAlert.setActive(true);
		mobileAlert.setChannel(MessageChannelType.SMS);
		
		if (alert.getAlertCondition() != null) {
			if (StringUtils.equals(alert.getAlertCondition().getOperator(), GREATER_THAN)) {
				final BigDecimal amountHighValue = alert.getAlertCondition().getAmountHigh(); // amount decimal
				final String amountHigh = amountHighValue != null ? amountHighValue.toString() : ""; // amount string
				
				mobileAlert.setAmount(amountHigh);
				mobileAlert.setType(BALANCE_MORE_THAN);
			} else if (StringUtils.equals(alert.getAlertCondition().getOperator(), LESS_THAN)) {
				final BigDecimal amountLowValue = alert.getAlertCondition().getAmountLow(); // amount decimal
				final String amountLow = amountLowValue != null ? amountLowValue.toString() : ""; // amount string
				
				mobileAlert.setAmount(amountLow);
				mobileAlert.setType(BALANCE_LESS_THAN);	
			}
	
		}
		return mobileAlert;
	}
	
	
	public static MobileAlert getMobileAlert(CustomerBalanceAlert alert) {
		MobileAlert mobileAlert = new MobileAlert();
		mobileAlert.setActive(true);
		mobileAlert.setChannel(MessageChannelType.PUSH_NOTIFICATION);
		
		if (alert.getAlertCondition() != null) {
			final BigDecimal amountValue = alert.getAlertCondition().getAmount(); // amount decimal
			final String amount = amountValue != null ? amountValue.toString() : ""; // amount string
			
			mobileAlert.setAmount(amount);
			
			if (StringUtils.equals(alert.getAlertCondition().getOperator(), GREATER_THAN)) {
				mobileAlert.setType(BALANCE_MORE_THAN);
			} else if (StringUtils.equals(alert.getAlertCondition().getOperator(), LESS_THAN)) {
				mobileAlert.setType(BALANCE_LESS_THAN);	
			}


		}
		return mobileAlert;
	}
	
	public static AlertCondition getBalanceAlertCondition(SMSAccount account, String operator) {
		return new AlertCondition(operator, MobileAlert.getAlertAmount(account));
	}
	
	private static BigDecimal getAlertAmount (SMSAccount account) {
		if (StringUtils.equals(account.getAlertCondition().getOperator(), GREATER_THAN)) {
			return account.getAlertCondition().getAmountHigh();
		} else if (StringUtils.equals(account.getAlertCondition().getOperator(), LESS_THAN)) {
			return account.getAlertCondition().getAmountLow();
		} else {
			return new BigDecimal(NumberUtils.INTEGER_ZERO);
		}
		
	}
	
	
	

}
