package evertec.cibp.web.form.mobile;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.devices.CustomerDevice;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.MobileResponseStatus;
import evertec.cibp.web.form.mobile.bean.MobileGetBalanceBean;
import evertec.cibp.web.response.MobileDeviceProfileResponse;
import evertec.cibp.web.utils.MobileUtil;

public class MobileGetBalanceForm extends CancellableFormController {

	protected final Log logger = LogFactory.getLog(getClass());
	private CustomerServices customerService;
	private static final long ONE_MINUTE_IN_MILLIS=60000;//millisecs
	private String refreshMinutes = "10";
	

	public MobileGetBalanceForm() {
		super();
		setCommandClass(MobileGetBalanceBean.class);
		setCommandName("mobileBalanceInfo");
		setFormView("mobileGetBalanceInfo");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object submit,
			BindException error) throws Exception {
		List<TVFrontendAccount> accounts = new LinkedList<>();
		Map<String, Object> model = new HashMap<>();

		MobileGetBalanceBean mobileBalanceBean = (MobileGetBalanceBean) submit;

		if(customerService == null) {
			logger.error("CustomerService is null");
		} if(GenericValidator.isBlankOrNull(mobileBalanceBean.getDeviceToken())) {
			logger.error("Device Token was null and is required");
		} else {
		
			MobileDeviceProfileResponse deviceProfileRespnse = MobileUtil.getProfileByDeviceToken(request, mobileBalanceBean.getDeviceToken(), customerService);
			
			if(deviceProfileRespnse != null && deviceProfileRespnse.getDevice() != null && deviceProfileRespnse.getStatus() == MobileResponseStatus.SUCCESS) {
				
				CustomerDevice device = deviceProfileRespnse.getDevice();

				Date deviceLastUpdate = device.getDeviceLastUsageDate();
				
				boolean sendAccountsBalance = false;
				
				if(deviceLastUpdate != null) {
					long t = deviceLastUpdate.getTime();
					Date tenMinsAfter = new Date(t + (Integer.parseInt(refreshMinutes) * ONE_MINUTE_IN_MILLIS));
					Date currentDateTime = new Date();
					if(tenMinsAfter.before(currentDateTime)){
						sendAccountsBalance = true;
					}
				} else {
					sendAccountsBalance = true;
				}
				
				if(sendAccountsBalance && deviceProfileRespnse.getProfile() != null) {
					BankingSession bankingSession = MobileUtil.getBankingSession(request, deviceProfileRespnse.getProfile(), customerService);
					accounts = MobileUtil.getAccounts(request, bankingSession, deviceProfileRespnse.getProfile(), customerService);
					
					device.setDeviceLastUsageDate(new Date());
					device.setUsername(deviceProfileRespnse.getProfile().getUsername());
					
					customerService.updateCustomerDevice(device);	
					bankingSession = null;
				} else {
					if(deviceProfileRespnse.getProfile() == null)
						logger.error("CustomerProfile was null");
					else 
						logger.warn("Customer "+deviceProfileRespnse.getProfile().getUsername()+" tried to get Balances before time.");
				}
			} else {
				logger.error("There was an error retrieving the device for the customer. Device Token: "+mobileBalanceBean.getDeviceToken());
			}
		}
		model.put("accounts", accounts);
		
		return showForm(request, error, "mobileGetBalanceInfo", model);
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public String getRefreshMinutes() {
		return refreshMinutes;
	}

	public void setRefreshMinutes(String refreshMinutes) {
		this.refreshMinutes = refreshMinutes;
	}
}
