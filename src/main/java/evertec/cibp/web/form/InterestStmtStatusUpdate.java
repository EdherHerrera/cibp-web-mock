package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.form.bean.InterestStmtStatusUpdateBean;
import evertec.cibp.web.session.UserSession;

public class InterestStmtStatusUpdate extends SimpleFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public InterestStmtStatusUpdate() {
		super();
		setCommandClass(InterestStmtStatusUpdateBean.class);
		setCommandName("interestStmtStatusUpdate");
		setFormView("interestStmtStatusUpdate");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		InterestStmtStatusUpdateBean bean = (InterestStmtStatusUpdateBean) command;

		// If the current status is ACTIVE, we need to disable the type selected
		if (bean.getAction() == InterestStmtStatusUpdateBean.ACTION.ACTIVE) {
			customerService.disableInterestStmtEntitlement(bean.getType(), customerProfile);
		}
		// If the current status is DEACTIVE, we need to enable the type
		// selected
		else if (bean.getAction() == InterestStmtStatusUpdateBean.ACTION.DEACTIVE) {
			customerService.enableInterestStmtEntitlement(bean.getType(), customerProfile);
		}

		model.put("completeprocess", true);
		return showForm(request, errors, "interestStmtStatusUpdate", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		InterestStmtStatusUpdateBean bean = (InterestStmtStatusUpdateBean) command;

		String type = (String) request.getParameter("type");
		String action = (String) request.getParameter("action");

		if (action != null && !action.equals(""))
			bean.setAction(InterestStmtStatusUpdateBean.ACTION.valueOf(action));

		if (type != null && !type.equals(""))
			bean.setType(type);

		if (bean.getAction() == InterestStmtStatusUpdateBean.ACTION.ACTIVE) {
			model.put("deactivateDisclosure", true);
		} else {
			model.put("activateDisclosure", true);
		}
		model.put("statusInterestStmt", bean.getAction());
		model.put("type", bean.getType());

		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		InterestStmtStatusUpdateBean infoBean = (InterestStmtStatusUpdateBean) command;

		// Current Status is DEACTIVE so an ACTIVATION is needed. We need to
		// validate the data.
		if (infoBean.getAction() != null && infoBean.getAction() == InterestStmtStatusUpdateBean.ACTION.DEACTIVE) {
			// Validate Ez Stmt Required Info

		}
	}
}
