package evertec.cibp.web.form.dto;

import java.io.Serializable;

import evertec.cibp.vs.beans.payments.RecurringPayment;


public class TVPaymentDto implements Serializable {
	

	private static final long serialVersionUID = 1L;
	
	protected RecurringPayment backendObject;


	/**
	 * Default Constructor to create a TVPayment.
	 */
	public TVPaymentDto() {
	}
	
	/**
	 * Constructor to create a TVPayment.
	 * @param recPayment
	 */
	public TVPaymentDto(RecurringPayment recPayment) {
		backendObject = recPayment;
	}

	/**
	 * @return the backendObject
	 */
	public RecurringPayment getBackendObject() {
		return backendObject;
	}

}
