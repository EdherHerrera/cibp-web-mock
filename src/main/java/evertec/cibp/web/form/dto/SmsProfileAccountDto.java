package evertec.cibp.web.form.dto;

import java.io.Serializable;
import java.util.Date;

public class SmsProfileAccountDto implements Serializable{
	  /** The Constant serialVersionUID. */
private static final long serialVersionUID = 1L;

private String id;
private String accountProductId;
private String accountSubtype;
private String accountSection;
private String accountNumber;
private String shortName;
private String alertposicion;
private Date sentDate;
private String profileId;

private SmsProfileDto smsProfile;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountProductId() {
		return accountProductId;
	}

	public void setAccountProductId(String accountProductId) {
		this.accountProductId = accountProductId;
	}

	public String getAccountSubtype() {
		return accountSubtype;
	}

	public void setAccountSubtype(String accountSubtype) {
		this.accountSubtype = accountSubtype;
	}

	public String getAccountSection() {
		return accountSection;
	}

	public void setAccountSection(String accountSection) {
		this.accountSection = accountSection;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getAlertposicion() {
		return alertposicion;
	}

	public void setAlertposicion(String alertposicion) {
		this.alertposicion = alertposicion;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public SmsProfileDto getSmsProfile() {
		return smsProfile;
	}

	public void setSmsProfile(SmsProfileDto smsProfile) {
		this.smsProfile = smsProfile;
	}


}
