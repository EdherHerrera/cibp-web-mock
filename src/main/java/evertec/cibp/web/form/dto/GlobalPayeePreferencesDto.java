package evertec.cibp.web.form.dto;

import java.io.Serializable;

/**
 * Customer GlobalPayeePreferencesDto
 * @author Joel Contreras Hernandez
 *
 */
public class GlobalPayeePreferencesDto implements Serializable{
	
	 /**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
     * The id
     */
    private int id;

    /**
     * The attributeData
     */
    private String attributeData;


    /**
     * The keyword
     */
    private String keyWords;

    /**
     * constructor
     */
    public GlobalPayeePreferencesDto(){}

    /**
     * Gets the id
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the tool tip id.
     *
     * @param id  the id of the tool tip
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the attributeData
     *
     * @return the attributeData
     */
    public String getAttributeData() {
        return attributeData;
    }

    /**
     * Sets the tool tip attributeData.
     *
     * @param attributeData  the attributeData of the tool tip
     */
    public void setAttributeData(String attributeData) {
        this.attributeData = attributeData;
    }

    /**
     * Gets the keyWords
     *
     * @return the keyWords
     */
    public String getKeyWords() {
        return keyWords;
    }

    /**
     * Sets the tool tip keyWords.
     *
     * @param keyWords  the keyWords of the tool tip
     */
    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }

}
