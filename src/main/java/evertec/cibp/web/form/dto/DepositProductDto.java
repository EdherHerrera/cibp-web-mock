package evertec.cibp.web.form.dto;

import java.io.Serializable;

import evertec.cibp.core.models.account.DepositProduct;

/**
 * Copyright 2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * DepositProductDto class.
 * <p>
 * The class DepositProductDto to wrap DepositProduct.
 *
 * @author Jelvin Ferreras
 * @version 1.0
 * @since 1.0
 */
public class DepositProductDto implements Serializable {

    /**
	 * UID
	 */
	private static final long serialVersionUID = -4163979406043759649L;
	/**
     * Deposit product
     **/
    private DepositProduct depositProduct;

    /**
     * Deposit product constructor
     * */
    public DepositProductDto(DepositProduct depositProduct) {
        this.setDepositProduct(depositProduct);
    }

    /**
     * Gets the data.
     *
     * @return the data
     */
    public DepositProduct getDepositProduct() {
        return depositProduct;
    }

    /**
     * Sets the data.
     *
     * @param depositProduct the new data
     */
    public void setDepositProduct(DepositProduct depositProduct) {
        this.depositProduct = depositProduct;
    }
}
