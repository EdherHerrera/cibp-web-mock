package evertec.cibp.web.form.dto;

import java.io.Serializable;

import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;


/**
 * Copyright 1/6/2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * CustomerEntitlementDto class.
 * <p>
 * The class CustomerEntitlementDto to mapping CustomerEntitlement Objects.
 *
 * @author Jorge L. Morla
 * @version 1.0
 * @since 1.0
 */
public class CustomerEntitlementDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * GlobalEntitlement
     */
    private GlobalEntitlement globalEntitlement;

    /**
     * status
     */
    private EntitlementStatus customerEntitlementStatus = EntitlementStatus.ENABLED;

    /**
     * profile id
     */
    private String profileId;

    /**
     * id
     */
    private String id;

    /**
     * attribute
     */
    private String attributeData;


    /**
     * Gets the globalEntitlement.
     *
     * @return the globalEntitlement
     */
    public GlobalEntitlement getGlobalEntitlement() {
        return globalEntitlement;
    }

    /**
     * Sets the tool tip globalEntitlement.
     *
     * @param globalEntitlement  the globalEntitlement of the tool tip
     */
    public void setGlobalEntitlement(GlobalEntitlement globalEntitlement) {
        this.globalEntitlement = globalEntitlement;
    }

    /**
     * Gets the customerEntitlementStatus
     *
     * @return the customerEntitlementStatus
     */
    public EntitlementStatus getCustomerEntitlementStatus() {
        return customerEntitlementStatus;
    }

    /**
     * Sets the tool tip customerEntitlementStatus.
     *
     * @param customerEntitlementStatus  the customerEntitlementStatus of the tool tip
     */
    public void setCustomerEntitlementStatus(EntitlementStatus customerEntitlementStatus) {
        this.customerEntitlementStatus = customerEntitlementStatus;
    }

    /**
     * Gets the profile id.
     *
     * @return the profileId
     */
    public String getProfileId() {
        return profileId;
    }

    /**
     * Sets the tool tip profileId.
     *
     * @param profileId  the profileId of the tool tip
     */
    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    /**
     * Gets the  id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the tool tip id.
     *
     * @param id  the id of the tool tip
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the attributeData
     *
     * @return the attributeData
     */
    public String getAttributeData() {
        return attributeData;
    }

    /**
     * Sets the tool tip attributeData.
     *
     * @param attributeData  the attributeData of the tool tip
     */
    public void setAttributeData(String attributeData) {
        this.attributeData = attributeData;
    }
}
