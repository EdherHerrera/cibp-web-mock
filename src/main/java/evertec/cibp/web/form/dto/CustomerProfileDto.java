package evertec.cibp.web.form.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import evertec.cibp.core.models.customer.CustomerAuthMethod;
import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.customer.CustomerEntitlement4Hibernate;
import evertec.cibp.core.models.customer.CustomerSignonStats;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.flags.CustomerFlag;
import evertec.cibp.core.models.customer.flags.UserInterface;

/**
 * Copyright 1/6/2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * CustomerProfileDto class.
 * <p>
 * The class CustomerProfileDto to mapping CustomerProfile Objects.
 *
 * @author Joel Contreras
 * @version 1.0
 * @since 1.0
 */
public class CustomerProfileDto  implements Serializable{
	
	
	
	 /**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4285431841593423265L;

	/**
     * profileId
     */
    private String profileId;

    /**
     * permId
     */
    private Long permId;

    /**
     * userName
     */
    private String userName;

    /**
     * status
     */
    private CustomerStatus status;

    /**
     * profileId
     */
    private CustomerEnrollmentSource enrollmentSource;

    /**
     * enrollmentSource
     */
    private CustomerAuthMethod authenticationMethod;

    /**
     * customerFlags
     */
    private Set<CustomerFlag> customerFlags;

    /**
     * creationDate
     */
    private Date creationDate;

    /**
     * activationDate
     */
    private Date activationDate;

    /**
     * birthDate
     */
    private Date birthDate;

    /**
     * TaxId
     */
    private String taxId;

    /**
     * email
     */
    private String email;

    /**
     * passwordHash
     */
    private String passwordHash;

    /**
     * profileId
     */
    private  int passwordSalt;

    /**
     * passwordSalt
     */
    private String enrollmentAccountNumber;

    /**
     * profileId
     */
    private String enrollmentAccountSubtype;

    /**
     * enrollmentAccountSubtype
     */
    private String enrollmentAccountProductId;

    /**
     * firstName
     */
    private String firstName;

    /**
     * lastName
     */
    private String lastName;

    /**
     * maidenName
     */
    private String maidenName;

    /**
     * enrollmentApplicationId
     */
    private String enrollmentApplicationId;

    /**
     * gender
     */
    private String gender;

    /**
     * isTransactional
     */
    private String isTransactional;

    /**
     * profileId
     */
    private UserInterface userInterface;

    /**
     * userInterface
     */
    private CustomerSignonStats signonStats;
    
    /**
     * custEntitlements
     */
    private List<CustomerEntitlement4Hibernate> custEntitlements;

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public Long getPermId() {
		return permId;
	}

	public void setPermId(Long permId) {
		this.permId = permId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public CustomerStatus getStatus() {
		return status;
	}

	public void setStatus(CustomerStatus status) {
		this.status = status;
	}

	public CustomerEnrollmentSource getEnrollmentSource() {
		return enrollmentSource;
	}

	public void setEnrollmentSource(CustomerEnrollmentSource enrollmentSource) {
		this.enrollmentSource = enrollmentSource;
	}

	public CustomerAuthMethod getAuthenticationMethod() {
		return authenticationMethod;
	}

	public void setAuthenticationMethod(CustomerAuthMethod authenticationMethod) {
		this.authenticationMethod = authenticationMethod;
	}

	public Set<CustomerFlag> getCustomerFlags() {
		return customerFlags;
	}

	public void setCustomerFlags(Set<CustomerFlag> customerFlags) {
		this.customerFlags = customerFlags;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getActivationDate() {
		return activationDate;
	}

	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public int getPasswordSalt() {
		return passwordSalt;
	}

	public void setPasswordSalt(int passwordSalt) {
		this.passwordSalt = passwordSalt;
	}

	public String getEnrollmentAccountNumber() {
		return enrollmentAccountNumber;
	}

	public void setEnrollmentAccountNumber(String enrollmentAccountNumber) {
		this.enrollmentAccountNumber = enrollmentAccountNumber;
	}

	public String getEnrollmentAccountSubtype() {
		return enrollmentAccountSubtype;
	}

	public void setEnrollmentAccountSubtype(String enrollmentAccountSubtype) {
		this.enrollmentAccountSubtype = enrollmentAccountSubtype;
	}

	public String getEnrollmentAccountProductId() {
		return enrollmentAccountProductId;
	}

	public void setEnrollmentAccountProductId(String enrollmentAccountProductId) {
		this.enrollmentAccountProductId = enrollmentAccountProductId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMaidenName() {
		return maidenName;
	}

	public void setMaidenName(String maidenName) {
		this.maidenName = maidenName;
	}

	public String getEnrollmentApplicationId() {
		return enrollmentApplicationId;
	}

	public void setEnrollmentApplicationId(String enrollmentApplicationId) {
		this.enrollmentApplicationId = enrollmentApplicationId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getIsTransactional() {
		return isTransactional;
	}

	public void setIsTransactional(String isTransactional) {
		this.isTransactional = isTransactional;
	}

	public UserInterface getUserInterface() {
		return userInterface;
	}

	public void setUserInterface(UserInterface userInterface) {
		this.userInterface = userInterface;
	}

	public CustomerSignonStats getSignonStats() {
		return signonStats;
	}

	public void setSignonStats(CustomerSignonStats signonStats) {
		this.signonStats = signonStats;
	}

	public List<CustomerEntitlement4Hibernate> getCustEntitlements() {
		return custEntitlements;
	}

	public void setCustEntitlements(List<CustomerEntitlement4Hibernate> custEntitlements) {
		this.custEntitlements = custEntitlements;
	}


    
}
