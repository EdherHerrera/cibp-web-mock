package evertec.cibp.web.form.dto;

import java.io.Serializable;

public class SingleBackendActionResultDto  extends BaseBackendActionResultDto implements Serializable{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    	//private Exception exception;
	    private String ifxRequest;
	    private String ifxResponse;
	    private String dataInRequest;

	    public SingleBackendActionResultDto() {
	    }

	   /* public Exception getException() {
	        return this.exception;
	    }

	    public void setException(Exception exception) {
	        this.exception = exception;
	    }*/

	    public String getIfxRequest() {
	        return this.ifxRequest;
	    }

	    public void setIfxRequest(String ifxRequest) {
	        this.ifxRequest = ifxRequest;
	    }

	    public String getIfxResponse() {
	        return this.ifxResponse;
	    }

	    public void setIfxResponse(String ifxResponse) {
	        this.ifxResponse = ifxResponse;
	    }

	    public String getDataInRequest() {
	        return this.dataInRequest;
	    }

	    public void setDataInRequest(String dataInRequest) {
	        this.dataInRequest = dataInRequest;
	    }

	   /* public String generateAuditReport() {
	        String data = "Request Data Received: \n" + this.getDataInRequest() + "\n Request Send: " + this.getIfxRequest() + "\n Response Received: " + this.getIfxResponse();
	        if (this.getException() != null) {
	            data = data + "\nException: \n\t" + this.getException().toString();
	        }

	        data = data + "\n" + this.toString();
	        return data;
	    }*/

	    public String generateErrorReport() {
	        return this.generateAuditReport();
	    }

	    public String toString() {
	        String data = super.toString();
	        return data;
	    }

		@Override
		public String generateAuditReport() {
			// TODO Auto-generated method stub
			return null;
		}
}
