package evertec.cibp.web.form.dto;

import java.io.Serializable;
import java.util.Date;

import evertec.cibp.core.models.documents.CIBPConfigurableTexts;



public class CustomerDocsDto implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
    /** The upload date. */
    private Date uploadDate;
	
	/** The doc info id. */
	private String docInfoId;
	
	/** The hide doc. */
	private String hideDoc;
	
	/** The doc search id. */
	private String docSearchId;
	
	/** The last hidden by. */
	private String lastHiddenBy;
	
	/** The last unhide by. */
	private String lastUnhideBy;
	
	/** The display after. */
	private String displayAfter;
	
	/** The front end text. */
	private String frontEndText;
	
	/** The last hidden date. */
	private Date lastHiddenDate;
	
	/** The staging upload date. */
	private Date stagingUploadDate;
	
	/** The customer documents package. */
	private CustomerDocPackageDto customerDocumentsPackage;
	
	/** The document name. */
	private CIBPConfigurableTexts documentName;

	/**
	 * Gets the upload date.
	 *
	 * @return the upload date
	 */
	public Date getUploadDate() {
		return uploadDate;
	}

	/**
	 * Sets the upload date.
	 *
	 * @param uploadDate the new upload date
	 */
	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	/**
	 * Gets the doc info id.
	 *
	 * @return the doc info id
	 */
	public String getDocInfoId() {
		return docInfoId;
	}

	/**
	 * Sets the doc info id.
	 *
	 * @param docInfoId the new doc info id
	 */
	public void setDocInfoId(String docInfoId) {
		this.docInfoId = docInfoId;
	}

	/**
	 * Gets the hide doc.
	 *
	 * @return the hide doc
	 */
	public String getHideDoc() {
		return hideDoc;
	}

	/**
	 * Sets the hide doc.
	 *
	 * @param hideDoc the new hide doc
	 */
	public void setHideDoc(String hideDoc) {
		this.hideDoc = hideDoc;
	}

	/**
	 * Gets the doc search id.
	 *
	 * @return the doc search id
	 */
	public String getDocSearchId() {
		return docSearchId;
	}

	/**
	 * Sets the doc search id.
	 *
	 * @param docSearchId the new doc search id
	 */
	public void setDocSearchId(String docSearchId) {
		this.docSearchId = docSearchId;
	}

	/**
	 * Gets the last hidden by.
	 *
	 * @return the last hidden by
	 */
	public String getLastHiddenBy() {
		return lastHiddenBy;
	}

	/**
	 * Sets the last hidden by.
	 *
	 * @param lastHiddenBy the new last hidden by
	 */
	public void setLastHiddenBy(String lastHiddenBy) {
		this.lastHiddenBy = lastHiddenBy;
	}

	/**
	 * Gets the last unhide by.
	 *
	 * @return the last unhide by
	 */
	public String getLastUnhideBy() {
		return lastUnhideBy;
	}

	/**
	 * Sets the last unhide by.
	 *
	 * @param lastUnhideBy the new last unhide by
	 */
	public void setLastUnhideBy(String lastUnhideBy) {
		this.lastUnhideBy = lastUnhideBy;
	}

	/**
	 * Gets the display after.
	 *
	 * @return the display after
	 */
	public String getDisplayAfter() {
		return displayAfter;
	}

	/**
	 * Sets the display after.
	 *
	 * @param displayAfter the new display after
	 */
	public void setDisplayAfter(String displayAfter) {
		this.displayAfter = displayAfter;
	}

	/**
	 * Gets the front end text.
	 *
	 * @return the front end text
	 */
	public String getFrontEndText() {
		return frontEndText;
	}

	/**
	 * Sets the front end text.
	 *
	 * @param frontEndText the new front end text
	 */
	public void setFrontEndText(String frontEndText) {
		this.frontEndText = frontEndText;
	}

	/**
	 * Gets the last hidden date.
	 *
	 * @return the last hidden date
	 */
	public Date getLastHiddenDate() {
		return lastHiddenDate;
	}

	/**
	 * Sets the last hidden date.
	 *
	 * @param lastHiddenDate the new last hidden date
	 */
	public void setLastHiddenDate(Date lastHiddenDate) {
		this.lastHiddenDate = lastHiddenDate;
	}

	/**
	 * Gets the staging upload date.
	 *
	 * @return the staging upload date
	 */
	public Date getStagingUploadDate() {
		return stagingUploadDate;
	}

	/**
	 * Sets the staging upload date.
	 *
	 * @param stagingUploadDate the new staging upload date
	 */
	public void setStagingUploadDate(Date stagingUploadDate) {
		this.stagingUploadDate = stagingUploadDate;
	}

	/**
	 * Gets the customer documents package.
	 *
	 * @return the customer documents package
	 */
	public CustomerDocPackageDto getCustomerDocumentsPackage() {
		return customerDocumentsPackage;
	}

	/**
	 * Sets the customer documents package.
	 *
	 * @param customerDocumentsPackage the new customer documents package
	 */
	public void setCustomerDocumentsPackage(CustomerDocPackageDto customerDocumentsPackage) {
		this.customerDocumentsPackage = customerDocumentsPackage;
	}

	/**
	 * Gets the document name.
	 *
	 * @return the document name
	 */
	public CIBPConfigurableTexts getDocumentName() {
		return documentName;
	}

	/**
	 * Sets the document name.
	 *
	 * @param documentName the new document name
	 */
	public void setDocumentName(CIBPConfigurableTexts documentName) {
		this.documentName = documentName;
	}

}
