package evertec.cibp.web.form.dto;

import java.io.Serializable;

import evertec.cibp.vs.models.BackEndStatus;

public abstract class BaseBackendActionResultDto implements Serializable{

	 private int totalSent;
	    private int totalSucessfull;
	    private int totalFailed;
	    private long startTime;
	    private long endTime;
	    private long errorCode;
	    private String errorCodeStr = "";
	    private String errorDescription;
	    protected BackEndStatus status;

	    public BaseBackendActionResultDto() {
	    }

	    public abstract String generateErrorReport();

	    public abstract String generateAuditReport();

	    public void setTotalSent(int totalSent) {
	        this.totalSent = totalSent;
	    }

	    public int getTotalSent() {
	        return this.totalSent;
	    }

	    public long getEndTime() {
	        return this.endTime;
	    }

	    public void setEndTime(long endTime) {
	        this.endTime = endTime;
	    }

	    public long getStartTime() {
	        return this.startTime;
	    }

	    public void setStartTime(long startTime) {
	        this.startTime = startTime;
	    }

	    public BackEndStatus getStatus() {
	        return this.status;
	    }

	    public void setStatus(BackEndStatus status) {
	        this.status = status;
	    }

	    public int getTotalFailed() {
	        return this.totalFailed;
	    }

	    public void setTotalFailed(int totalFailed) {
	        this.totalFailed = totalFailed;
	    }

	    public int getTotalSucessfull() {
	        return this.totalSucessfull;
	    }

	    public void setTotalSucessfull(int totalSucessfull) {
	        this.totalSucessfull = totalSucessfull;
	    }

	    public String getErrorDescription() {
	        return this.errorDescription;
	    }

	    public void setErrorDescription(String errorDescription) {
	        this.errorDescription = errorDescription;
	    }

	    public long getErrorCode() {
	        return this.errorCode;
	    }

	    public void setErrorCode(long errorCode) {
	        this.errorCode = errorCode;
	    }

	    public String toString() {
	        String data = "Base response:\n";
	        data = data + "\t totalSent:  " + this.totalSent + "\n";
	        data = data + "\t totalSucessfull:  " + this.totalSucessfull + "\n";
	        data = data + "\t totalFailed:  " + this.totalFailed + "\n";
	        data = data + "\t startTime:  " + this.startTime + "\n";
	        data = data + "\t endTime:  " + this.endTime + "\n";
	        data = data + "\t errorCode:  " + this.errorCode + "\n";
	        data = data + "\t errorDescription:  " + this.errorDescription;
	        return data;
	    }

	    public void setErrorCodeStr(String errorCodeStr) {
	        this.errorCodeStr = errorCodeStr;
	    }

	    public String getErrorCodeStr() {
	        return this.errorCodeStr;
	    }
}
