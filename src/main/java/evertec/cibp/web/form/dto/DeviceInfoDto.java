package evertec.cibp.web.form.dto;

import com.google.common.base.Optional;

import evertec.cibp.web.form.bean.PasswordBean;
import evertec.cibp.web.models.devices.DeviceInfo;

public class DeviceInfoDto {

    private static final String UNAVAILABLE = "Not Available";

    public DeviceInfo passwordBeanToDeviceInfo (PasswordBean bean) {
        
        DeviceInfo deviceInfo = new DeviceInfo();
        
        if(bean != null) {
            
            deviceInfo.setDeviceModel(Optional.fromNullable(bean.getDeviceModel()).or(UNAVAILABLE));
            deviceInfo.setAppVersion(Optional.fromNullable(bean.getAppVersion()).or(UNAVAILABLE));
            deviceInfo.setNetworkProvider(Optional.fromNullable(bean.getNetworkProvider()).or(UNAVAILABLE));
            deviceInfo.setOsVersion(Optional.fromNullable(bean.getOsVersion()).or(UNAVAILABLE));
        }else {
            
            deviceInfo = new DeviceInfo(true);
            
        }
        return deviceInfo;
    }

}
