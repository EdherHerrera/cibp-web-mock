package evertec.cibp.web.form.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;



public class CustomerDocPackageDto implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
    /** The doc pack id. */
    private String docPackId;
	
	/** The hide. */
	private String hide;
	
	/** The status. */
	private String status;
	
	/** The upload by. */
	private String uploadBy;
	
	/** The profile id. */
	private String profileId;
	
	/** The doc type id. */
	private String docTypeId;
	
	/** The banker email. */
	private String bankerEmail;
	
	/** The pack search id. */
	private String packSearchId;
	
	/** The last hidden by. */
	private String lastHiddenBy;
	
	/** The last unhide by. */
	private String lastUnhideBy;
	
	/** The doc date. */
	private Date docDate;
	
	/** The upload date. */
	private Date uploadDate;
	
	/** The accepted date. */
	private Date acceptedDate;
	
	/** The last hidden date. */
	private Date lastHiddenDate;
	
	/** The signature deadline date. */
	private Date signatureDeadlineDate;
	
	/**  The pending documents. */
	private Set<CustomerDocsDto> documents;

	/**
	 * Gets the doc pack id.
	 *
	 * @return the doc pack id
	 */
	public String getDocPackId() {
		return docPackId;
	}

	/**
	 * Sets the doc pack id.
	 *
	 * @param docPackId the new doc pack id
	 */
	public void setDocPackId(String docPackId) {
		this.docPackId = docPackId;
	}

	/**
	 * Gets the hide.
	 *
	 * @return the hide
	 */
	public String getHide() {
		return hide;
	}

	/**
	 * Sets the hide.
	 *
	 * @param hide the new hide
	 */
	public void setHide(String hide) {
		this.hide = hide;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the upload by.
	 *
	 * @return the upload by
	 */
	public String getUploadBy() {
		return uploadBy;
	}

	/**
	 * Sets the upload by.
	 *
	 * @param uploadBy the new upload by
	 */
	public void setUploadBy(String uploadBy) {
		this.uploadBy = uploadBy;
	}

	/**
	 * Gets the profile id.
	 *
	 * @return the profile id
	 */
	public String getProfileId() {
		return profileId;
	}

	/**
	 * Sets the profile id.
	 *
	 * @param profileId the new profile id
	 */
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	/**
	 * Gets the doc type id.
	 *
	 * @return the doc type id
	 */
	public String getDocTypeId() {
		return docTypeId;
	}

	/**
	 * Sets the doc type id.
	 *
	 * @param docTypeId the new doc type id
	 */
	public void setDocTypeId(String docTypeId) {
		this.docTypeId = docTypeId;
	}

	/**
	 * Gets the banker email.
	 *
	 * @return the banker email
	 */
	public String getBankerEmail() {
		return bankerEmail;
	}

	/**
	 * Sets the banker email.
	 *
	 * @param bankerEmail the new banker email
	 */
	public void setBankerEmail(String bankerEmail) {
		this.bankerEmail = bankerEmail;
	}

	/**
	 * Gets the pack search id.
	 *
	 * @return the pack search id
	 */
	public String getPackSearchId() {
		return packSearchId;
	}

	/**
	 * Sets the pack search id.
	 *
	 * @param packSearchId the new pack search id
	 */
	public void setPackSearchId(String packSearchId) {
		this.packSearchId = packSearchId;
	}

	/**
	 * Gets the last hidden by.
	 *
	 * @return the last hidden by
	 */
	public String getLastHiddenBy() {
		return lastHiddenBy;
	}

	/**
	 * Sets the last hidden by.
	 *
	 * @param lastHiddenBy the new last hidden by
	 */
	public void setLastHiddenBy(String lastHiddenBy) {
		this.lastHiddenBy = lastHiddenBy;
	}

	/**
	 * Gets the last unhide by.
	 *
	 * @return the last unhide by
	 */
	public String getLastUnhideBy() {
		return lastUnhideBy;
	}

	/**
	 * Sets the last unhide by.
	 *
	 * @param lastUnhideBy the new last unhide by
	 */
	public void setLastUnhideBy(String lastUnhideBy) {
		this.lastUnhideBy = lastUnhideBy;
	}

	/**
	 * Gets the doc date.
	 *
	 * @return the doc date
	 */
	public Date getDocDate() {
		return docDate;
	}

	/**
	 * Sets the doc date.
	 *
	 * @param docDate the new doc date
	 */
	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}

	/**
	 * Gets the upload date.
	 *
	 * @return the upload date
	 */
	public Date getUploadDate() {
		return uploadDate;
	}

	/**
	 * Sets the upload date.
	 *
	 * @param uploadDate the new upload date
	 */
	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	/**
	 * Gets the accepted date.
	 *
	 * @return the accepted date
	 */
	public Date getAcceptedDate() {
		return acceptedDate;
	}

	/**
	 * Sets the accepted date.
	 *
	 * @param acceptedDate the new accepted date
	 */
	public void setAcceptedDate(Date acceptedDate) {
		this.acceptedDate = acceptedDate;
	}

	/**
	 * Gets the last hidden date.
	 *
	 * @return the last hidden date
	 */
	public Date getLastHiddenDate() {
		return lastHiddenDate;
	}

	/**
	 * Sets the last hidden date.
	 *
	 * @param lastHiddenDate the new last hidden date
	 */
	public void setLastHiddenDate(Date lastHiddenDate) {
		this.lastHiddenDate = lastHiddenDate;
	}

	/**
	 * Gets the signature deadline date.
	 *
	 * @return the signature deadline date
	 */
	public Date getSignatureDeadlineDate() {
		return signatureDeadlineDate;
	}

	/**
	 * Sets the signature deadline date.
	 *
	 * @param signatureDeadlineDate the new signature deadline date
	 */
	public void setSignatureDeadlineDate(Date signatureDeadlineDate) {
		this.signatureDeadlineDate = signatureDeadlineDate;
	}

	/**
	 * Gets the documents.
	 *
	 * @return the documents
	 */
	public Set<CustomerDocsDto> getDocuments() {
		return documents;
	}

	/**
	 * Sets the documents.
	 *
	 * @param documents the new documents
	 */
	public void setDocuments(Set<CustomerDocsDto> documents) {
		this.documents = documents;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomerDocPackageDto [docPackId=" + docPackId + ", hide=" + hide + ", status=" + status + ", uploadBy="
				+ uploadBy + ", profileId=" + profileId + ", docTypeId=" + docTypeId + ", bankerEmail=" + bankerEmail
				+ ", packSearchId=" + packSearchId + ", lastHiddenBy=" + lastHiddenBy + ", lastUnhideBy=" + lastUnhideBy
				+ ", docDate=" + docDate + ", uploadDate=" + uploadDate + ", acceptedDate=" + acceptedDate
				+ ", lastHiddenDate=" + lastHiddenDate + ", signatureDeadlineDate=" + signatureDeadlineDate
				+ ", documents=" + documents + "]";
	}
	
	

}
