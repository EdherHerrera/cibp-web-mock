package evertec.cibp.web.form.dto;

import java.util.List;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.services.requests.CustomerEmailBalanceAlertAccountsGetterRequest;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.core.services.CustomerServices;


/**
 * CustomerEmailBalanceAlertAccountsReqDto class
 * 
 * @author Jorge De Los Santos
 *
 * @version 1.1
 *
 * @since Dec 13, 2018 11:01:39 AM
 *
 */
public class CustomerEmailBalanceAlertAccountsReqDto {
	
	private CustomerServices customerService;
	private UserSession userSession;
	private String profileId;
	private List<TVFrontendAccount> accounts;
	private CustomerEmailBalanceAlertAccountsGetterRequest emailReq;
	
	/**
	 * Default Constructor
	 */
	public CustomerEmailBalanceAlertAccountsReqDto() {
		super();
	}

	/**
	 * Constructor with parameters
	 */
	public CustomerEmailBalanceAlertAccountsReqDto(CustomerServices customerService, UserSession userSession, String profileId, List<TVFrontendAccount> accounts,
			CustomerEmailBalanceAlertAccountsGetterRequest emailReq) {
		super();
		this.customerService = customerService;
		this.userSession = userSession;
		this.profileId = profileId;
		this.accounts = accounts;
		this.emailReq = emailReq;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public UserSession getUserSession() {
		return userSession;
	}

	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public List<TVFrontendAccount> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<TVFrontendAccount> accounts) {
		this.accounts = accounts;
	}

	public CustomerEmailBalanceAlertAccountsGetterRequest getEmailReq() {
		return emailReq;
	}

	public void setEmailReq(CustomerEmailBalanceAlertAccountsGetterRequest emailReq) {
		this.emailReq = emailReq;
	}
	
	

}
