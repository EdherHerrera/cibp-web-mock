package evertec.cibp.web.form.dto;

import java.io.Serializable;

public class PaymentModifyReponseDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ModifyPaymentActionResultDto modifyPaymentActionResult;


    public PaymentModifyReponseDto(ModifyPaymentActionResultDto modifyPaymentActionResult) {
       this.modifyPaymentActionResult = modifyPaymentActionResult;
    }

    public ModifyPaymentActionResultDto getModifyPaymentActionResult() {
        return modifyPaymentActionResult;
    }

    public void setModifyPaymentActionResult(ModifyPaymentActionResultDto modifyPaymentActionResult) {
        this.modifyPaymentActionResult = modifyPaymentActionResult;
    }
}
