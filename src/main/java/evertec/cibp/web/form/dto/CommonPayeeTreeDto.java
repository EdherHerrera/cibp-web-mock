package evertec.cibp.web.form.dto;

import java.io.Serializable;

public class CommonPayeeTreeDto implements Serializable {
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * The payees
     * */
    private String payees;

    /**
     * Construct
     * */
    public CommonPayeeTreeDto(){
    }

    /**
     * @return the payees
     */
    public String getPayees() {
        return payees;
    }

    /**
     * @param payees the id to set
     */
    public void setPayees(String payees) {
        this.payees = payees;
    }

}
