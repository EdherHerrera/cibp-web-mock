package evertec.cibp.web.form.dto;

import java.io.Serializable;

public class ATHEveryTransAlertDto implements Serializable {
	
	/** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The profileId*/
    private String profileId;

    /** The accountNumber */
    private String accountNumber;

    /** The cardNumber */
    private String cardNumber;
    
    /** Default constructor. */
    public ATHEveryTransAlertDto() {
    	
    }
    
    /** Request constructor */
    public ATHEveryTransAlertDto(String profileId, String accountNumber, String cardNumber) {
    	this.profileId = profileId;
    	this.accountNumber = accountNumber;
    	this.cardNumber = cardNumber;
    }

    /**
     * Gets the profileId.
     *
     * @return the profileId
     */
    public String getProfileId() {
        return profileId;
    }

    /**
     * Sets the profileId.
     *
     * @param profileId    the id to set
     */
    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    /**
     * Gets the accountNumber.
     *
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the accountNumber.
     *
     * @param accountNumber    the id to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * Gets the cardNumber.
     *
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the cardNumber.
     *
     * @param cardNumber    the id to set
     */
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

}
