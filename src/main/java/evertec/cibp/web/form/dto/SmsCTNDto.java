package evertec.cibp.web.form.dto;

import java.io.Serializable;

public class SmsCTNDto implements Serializable{
	  /** The Constant serialVersionUID. */
private static final long serialVersionUID = 1L;


private String id;
private String ctn;
private String provider;
private SmsProfileDto smsProfile;

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCtn() {
		return this.ctn;
	}

	public void setCtn(String number) {
		this.ctn = number;
	}

	public String getProvider() {
		return this.provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public SmsProfileDto getSmsProfile() {
		return smsProfile;
	}

	public void setSmsProfile(SmsProfileDto smsProfile) {
		this.smsProfile = smsProfile;
	}

	



}
