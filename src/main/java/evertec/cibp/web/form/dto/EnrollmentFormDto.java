package evertec.cibp.web.form.dto;

/**
 * @author Saul Castro
 * @since 4/20/2022
 */
public class EnrollmentFormDto  {

    private String nerdsPrefillUrl;
    private String comercialProducts;
    private String paymentvalidationrange;
    private String ntenrollmentaccounts;
    private String wordListDictionary;
    private Integer ageRestriction;
    private String invalidEmailDomains;


    public EnrollmentFormDto() {
    }

    public EnrollmentFormDto(String nerdsPrefillUrl, String comercialProducts,
                             String paymentvalidationrange, String ntenrollmentaccounts,
                             String wordListDictionary, Integer ageRestriction, String invalidEmailDomains) {
        this.nerdsPrefillUrl = nerdsPrefillUrl;
        this.comercialProducts = comercialProducts;
        this.paymentvalidationrange = paymentvalidationrange;
        this.ntenrollmentaccounts = ntenrollmentaccounts;
        this.wordListDictionary = wordListDictionary;
        this.ageRestriction = ageRestriction;
        this.invalidEmailDomains = invalidEmailDomains;
    }

    public String getNerdsPrefillUrl() {
        return nerdsPrefillUrl;
    }

    public void setNerdsPrefillUrl(String nerdsPrefillUrl) {
        this.nerdsPrefillUrl = nerdsPrefillUrl;
    }

    public String getComercialProducts() {
        return comercialProducts;
    }

    public void setComercialProducts(String comercialProducts) {
        this.comercialProducts = comercialProducts;
    }

    public String getPaymentvalidationrange() {
        return paymentvalidationrange;
    }

    public void setPaymentvalidationrange(String paymentvalidationrange) {
        this.paymentvalidationrange = paymentvalidationrange;
    }

    public String getNtenrollmentaccounts() {
        return ntenrollmentaccounts;
    }

    public void setNtenrollmentaccounts(String ntenrollmentaccounts) {
        this.ntenrollmentaccounts = ntenrollmentaccounts;
    }

    public String getWordListDictionary() {
        return wordListDictionary;
    }

    public void setWordListDictionary(String wordListDictionary) {
        this.wordListDictionary = wordListDictionary;
    }

    public Integer getAgeRestriction() {
        return ageRestriction;
    }

    public void setAgeRestriction(Integer ageRestriction) {
        this.ageRestriction = ageRestriction;
    }

    public String getInvalidEmailDomains() {
        return invalidEmailDomains;
    }

    public void setInvalidEmailDomains(String invalidEmailDomains) {
        this.invalidEmailDomains = invalidEmailDomains;
    }
}
