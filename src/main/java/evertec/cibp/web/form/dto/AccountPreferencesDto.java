package evertec.cibp.web.form.dto;

import java.io.Serializable;

/**
 * Copyright 2017 Evertec, Inc. All Rights Reserved.
 * <p>
 * AccountPreferencesDto class.
 * <p>
 * The class AccountPreferencesDto to mapping AccountPreferences Objects.
 *
 * @author Cesar Mata
 * @version 1.0
 * @since 1.0
 */
public class AccountPreferencesDto implements Serializable {

    /**
     * The constant serialVersionUID
     */
    private static final long serialVersionUID = 2084664588582240620L;

    /**
     * The id for an account preference
     */
    private String customerAccountPreferenceId;

    /**
     * The customer profile id
     */
    private String customerProfileId;

    /**
     * The customer account key
     */
    private String customerAccountKey;

    /**
     * The XML format string that contains the account preferences fon an account
     */
    private String accountData;

    /**
     * AccountPreferencesDto constructor (no arguments)
     */
    public AccountPreferencesDto() {
    }

    /**
     * AccountPreferencesDto constructor
     *
     * @param customerAccountPreferenceId the id for an account preference
     * @param customerProfileId           the customer profile id
     * @param customerAccountKey          the customer account key
     * @param accountData                 the XML format string that contains the account
     *                                    preferences from an account
     */
    public AccountPreferencesDto(String customerAccountPreferenceId, String customerProfileId, String customerAccountKey, String accountData) {
        this.customerAccountPreferenceId = customerAccountPreferenceId;
        this.customerProfileId = customerProfileId;
        this.customerAccountKey = customerAccountKey;
        this.accountData = accountData;
    }

    /**
     * Gets the id for an specific account preference
     *
     * @return the id for an specific account preference
     */
    public String getCustomerAccountPreferenceId() {
        return customerAccountPreferenceId;
    }

    /**
     * Sets the id for an specific account preference
     *
     * @param customerAccountPreferenceId the account preference id to set
     */
    public void setCustomerAccountPreferenceId(String customerAccountPreferenceId) {
        this.customerAccountPreferenceId = customerAccountPreferenceId;
    }

    /**
     * Gets the customer profile id
     *
     * @return the customer profile id
     */
    public String getCustomerProfileId() {
        return customerProfileId;
    }

    /**
     * Sets the customer profile id
     *
     * @param customerProfileId the customer profile id to set
     */
    public void setCustomerProfileId(String customerProfileId) {
        this.customerProfileId = customerProfileId;
    }

    /**
     * Gets the customer account key
     *
     * @return the customer account key
     */
    public String getCustomerAccountKey() {
        return customerAccountKey;
    }

    /**
     * Sets the customer account key
     *
     * @param customerAccountKey the customer account key to set
     */
    public void setCustomerAccountKey(String customerAccountKey) {
        this.customerAccountKey = customerAccountKey;
    }

    /**
     * Gets the XML format string that contains the account preferences from an account
     *
     * @return the XML format string that contains the account preferences from an account
     */
    public String getAccountData() {
        return accountData;
    }

    /**
     * Sets the XML format string that contains the account preferences from an account
     *
     * @param accountData the XML format string that contains the account preferences from an account to set
     */
    public void setAccountData(String accountData) {
        this.accountData = accountData;
    }

    /**
     * Gets the string data from the object
     *
     * @return the string data from the object
     */
    @Override
    public String toString() {
        return "AccountPreferencesDto{" +
                "customerAccountPreferenceId='" + customerAccountPreferenceId + '\'' +
                ", customerProfileId='" + customerProfileId + '\'' +
                ", customerAccountKey='" + customerAccountKey + '\'' +
                ", accountData='" + accountData + '\'' +
                '}';
    }

}