package evertec.cibp.web.form.dto;

import java.io.Serializable;
import java.util.Date;

public class PushTokensDto implements Serializable, Comparable<PushTokensDto> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3018752492132907172L;
    
    /** The costumer profile ID. */
    private String profileId;
	
    /** The device token */
	private String token;
	
	/** The device type (example: iOS, Android) */
	private String deviceType;
	
	/** The device type (example: iPhone5,1) */
	private String deviceModel;
	
	/** The device name (example: Customer's iPhone X) */
	private String deviceName;
	
	/** The last sign-on date */
	private Date lastSignonDate;
	
	/** The token status. */
	private String status;
	
	/** The device registration date */
	private Date creationDate;

	/**
	 * @return the profileId
	 */
	public String getProfileId() {
		return profileId;
	}

	/**
	 * @param profileId the profileId to set
	 */
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}

	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	/**
	 * @return the device model
	 */
	public String getDeviceModel() {
		return deviceModel;
	}

	/**
	 * @param deviceModel the device model to set
	 */
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	/**
	 * @return the lastSignonDate
	 */
	public Date getLastSignonDate() {
		return lastSignonDate;
	}

	/**
	 * @param lastSignonDate the lastSignonDate to set
	 */
	public void setLastSignonDate(Date lastSignonDate) {
		this.lastSignonDate = lastSignonDate;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int compareTo(PushTokensDto o) {
		return o.getCreationDate().compareTo(getCreationDate());
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
	

}
