package evertec.cibp.web.form.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class AlertsHistoryDto to mapping AlertsHistory objects.
 *
 * @author Henry Terrero
 * @since 1.0
 * @version 1.0
 */
public class AlertsHistoryDto implements Serializable{

	 /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;


    /** The  id. */
    private String id;

    /** The profile id. */
    private String profileId;

    /** The account number. */
    private String accountNumber;

    /** The card number. */
    private String cardNumber;

    /** The tax id. */
    private String taxId;

    /** The event type. */
    private int eventType;

    /** The message. */
    private String message;

    /** The email. */
    private String email;

    /** The phone. */
    private String phone;

    /** The date. */
    private Date date;

    /** The paxus transaction type. */
    private int paxusTransactionType;

    /** The source. */
    private String source;

    /** The channel. */
    private String channel;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the profile id.
     *
     * @return the profile id
     */
    public String getProfileId() {
        return profileId;
    }

    /**
     * Sets the profile id.
     *
     * @param profileId the profile id to set
     */
    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    /**
     * Gets the account number.
     *
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the account number.
     *
     * @param accountNumber the account number to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * Gets the card number.
     *
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the card number.
     *
     * @param cardNumber the card number to set
     */
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * Gets the tax id.
     *
     * @return the taxId
     */
    public String getTaxId() {
        return taxId;
    }

    /**
     * Sets the tax id.
     *
     * @param taxId the tax id to set
     */
    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    /**
     * Gets the event type.
     *
     * @return the eventType
     */
    public int getEventType() {
        return eventType;
    }

    /**
     * Sets the event type.
     *
     * @param eventType the event type to set
     */
    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message.
     *
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     *
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the phone.
     *
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the phone.
     *
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets the date.
     *
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets the paxus transaction type.
     *
     * @return the paxusTransactionType
     */
    public int getPaxusTransactionType() {
        return paxusTransactionType;
    }

    /**
     * Sets the paxus transaction type.
     *
     * @param paxusTransactionType the paxus transaction type to set
     */
    public void setPaxusTransactionType(int paxusTransactionType) {
        this.paxusTransactionType = paxusTransactionType;
    }

    /**
     * Gets the source.
     *
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the source.
     *
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * Gets the channel.
     *
     * @return the channel
     */
    public String getChannel() {
        return channel;
    }

    /**
     * Sets the channel.
     *
     * @param channel the channel to set
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * Gets the category english.
     *
     * @return the category
     */
    public String getCategoryEnglish() {

        String category = "";
        if (source.equalsIgnoreCase("Mibanco")) {
                switch (eventType) {

                    case 3:
                        category = "Balances";
                        break;
                    case 10: case 11: case 12: case 13: case 14: case 15: case 30: case 31:
                        category = "Payments and Transfers";
                        break;
                    case 16: case 17: case 18: case 19: case 20: case 21: case 22: case 23: case 24: case 25: case 26: case 27: case 29:
                        category = "Changes in Personal Information";
                        break;
                    case 62:
    					category = "ON/OFF Service";
    					break;
                    default:
                        category = "Other";
                        break;

                }
        }

        if (source.equalsIgnoreCase("Paxus")) {

                switch (paxusTransactionType) {

                    case 1:
                    case 2:
                        category = "ATH Payments and Withdrawals";
                        break;
                    case 3:

                        // category = "Transfers Received from 3rd Party";// Not
                        // implemented yet. In a future
                    default:
                        category = "Other";
                        break;

                }
        }

        return category;
    }


    /**
     * Gets the category spanish.
     *
     * @return the category
     */
    public String getCategorySpanish() {

        String category = "";
        if (source.equalsIgnoreCase("Mibanco")) {
                switch (eventType) {

                    case 3:
                        category = "Balances";
                        break;
                    case 10: case 11: case 12: case 13: case 14: case 15: case 30: case 31:
                        category = "Pagos y Transferencias";
                        break;
                    case 16: case 17: case 18: case 19: case 20: case 21: case 22: case 23: case 24: case 25: case 26: case 27: case 29:
                        category = "Cambios en Info Personal";
                        break;
                    case 62:
                        category = "Servicio de ON/OFF";
                        break;
                    default:
                        category = "Otro";
                        break;

                }
        }

        if (source.equalsIgnoreCase("Paxus")) {

                switch (paxusTransactionType) {

                    case 1: case 2:
                        category = "Compras y retiros con mi ATH";
                        break;
                    case 3:

                        // category = "Transferencias recibidos de terceros";// Not
                        // implemented yet. In a future
                    default:
                        category = "Otro";
                        break;

                }
        }

        return category;
    }

}
