package evertec.cibp.web.form.dto;

import java.io.Serializable;
import evertec.otherservices.enums.ResponseStatus;

public class GetDocumentRespDto implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** Document content in base64 encryption*/
	private String documentBase64;
	
	/**Response status*/
	private ResponseStatus responseStatus;
	
	/**Error message if exist*/
	private String errorMsg;
	
	/**
	 * @return the documentBase64
	 */
	public String getDocumentBase64() {
		return documentBase64;
	}
	/**
	 * @param documentBase64 the documentBase64 to set
	 */
	public void setDocumentBase64(String documentBase64) {
		this.documentBase64 = documentBase64;
	}
	/**
	 * @return the responseStatus
	 */
	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}
	/**
	 * @param responseStatus the responseStatus to set
	 */
	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}
	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}
	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GetDocumentResp [documentBase64=" + documentBase64 + ", responseStatus=" + responseStatus
				+ ", errorMsg=" + errorMsg + "]";
	}
}
