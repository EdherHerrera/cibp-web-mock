package evertec.cibp.web.form.dto;

import java.io.Serializable;

import evertec.cibp.core.models.customer.CustomerProfile;

/**
 * <h1>CustomerFlagDto</h1>
 * This class is used for mapping the microservice customer flag operations.
 * <p>
 * Copyright 2018 Evertec, Inc. All Rights Reserved.
 *
 * @author César Mata
 * @version 1.0
 * @since 2018-10-08
 */
public class CustomerFlagDto implements Serializable {

	/**
     * The serial version unique identifier.
     */
	private static final long serialVersionUID = 1L;
	
	/**
	 * This is the customer flag identifier.
	 */
	private String id;
	
	/**
	 * This is the customer profile that the flag belongs to.
	 */
    private CustomerProfile customerProfile;
    
    /**
     * This is the value that describes the flag type.
     */
    private String type;
    
    /**
     * This method returns the customer flag identifier.
     * 
     * @return This returns the customer flag identifier.
     */
	public String getId() {
		return id;
	}
	
	/**
	 * This method sets the customer flag identifier.
	 * 
	 * @param id This is the identifier to be set.
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * This method returns the customer profile that the flag belongs to.
	 * 
	 * @return This returns the customer profile that the flag belongs to.
	 */
	public CustomerProfile getCustomerProfile() {
		return customerProfile;
	}
	
	/**
	 * This method sets the customer profile that the flag belongs to.
	 * 
	 * @param customerProfile This is the customer profile that the flag belongs to.
	 */
	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}
	
	/**
	 * This method returns the flag type.
	 * 
	 * @return This returns the flag type.
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * This method sets the flag type.
	 * 
	 * @param type This is the type to be set.
	 */
	public void setType(String type) {
		this.type = type;
	}

}
