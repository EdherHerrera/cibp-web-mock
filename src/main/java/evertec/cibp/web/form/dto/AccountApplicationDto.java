package evertec.cibp.web.form.dto;

import java.io.Serializable;
import java.util.Date;

import evertec.cibp.core.models.accountopening.AccountApplicationStatus;


/**
 * Copyright 2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * AccountApplicationDto class.
 * <p>
 * The class AccountApplicationDto to mapping AccountApplication Objects.
 *
 * @author Henry Terrero
 * @version 1.0
 * @since 1.0
 */
public class AccountApplicationDto implements Serializable{
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * The application Id for an account application
     */
    private String applicationId;

    /**
     * The username for an account application
     */
    private String username;

    /**
     * The creation Date for an account application
     */
    private Date creationDate;

    /**
     * The validation Type for an account application
     */
    private String validationType;

    /**
     * The signature Card Complete for an account application
     */
    private boolean signatureCardComplete;

    /**
     * The account Number for an account application
     */
    private String accountNumber;

    /**
     * The account Subtype for an account application
     */
    private String accountSubtype;

    /**
     * The account Product Id for an account application
     */
    private String accountProductId;

    /**
     * The account Name for an account application
     */
    private String accountName;

    /**
     * The id for an account application
     */
    private String accountProductDescription;

    /**
     * The status for an account application
     */
    private AccountApplicationStatus status;

    /**
     * The incomplete for an account application
     */
    private boolean incomplete;

    /**
     * The account Available In Backend for an account application
     */
    private boolean accountAvailableInBackend;

    /**
     * The account Has Balance In Backend for an account application
     */
    private boolean accountHasBalanceInBackend;

    /**
     * The waive Overdrafts for an account application
     */
    private boolean waiveOverdrafts;

    /**
     * Gets the application id.
     *
     * @return the applicationId
     */
    public String getApplicationId() {
        return applicationId;
    }

    /**
     * Sets the tool tip applicationId.
     *
     * @param applicationId  the applicationId of the tool tip
     */
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    /**
     * Gets the username
     *
     * @return the userName
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the tool tip username.
     *
     * @param username  the username of the tool tip
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the creation date
     *
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }


    /**
     * Sets the tool tip creationDate.
     *
     * @param creationDate  the creationDate of the tool tip
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Gets the validation type
     *
     * @return the validationType
     */
    public String getValidationType() {
        return validationType;
    }


    /**
     * Sets the tool tip validationType.
     *
     * @param validationType  the validationType of the tool tip
     */
    public void setValidationType(String validationType) {
        this.validationType = validationType;
    }

    /**
     * Gets the signature Card Complete
     *
     * @return the signatureCardComplete
     */
    public boolean isSignatureCardComplete() {
        return signatureCardComplete;
    }

    /**
     * Sets the tool tip signatureCardComplete.
     *
     * @param signatureCardComplete  the signatureCardComplete of the tool tip
     */
    public void setSignatureCardComplete(boolean signatureCardComplete) {
        this.signatureCardComplete = signatureCardComplete;
    }

    /**
     * Gets the account number
     *
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the tool tip accountNumber.
     *
     * @param accountNumber  the accountNumber of the tool tip
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * Gets the Account subtype
     *
     * @return the AccountSubtype
     */
    public String getAccountSubtype() {
        return accountSubtype;
    }

    /**
     * Sets the tool tip accountSubtype.
     *
     * @param accountSubtype  the accountSubtype of the tool tip
     */
    public void setAccountSubtype(String accountSubtype) {
        this.accountSubtype = accountSubtype;
    }

    /**
     * Gets the accountProductId
     *
     * @return the accountProductId
     */
    public String getAccountProductId() {
        return accountProductId;
    }

    /**
     * Sets the tool tip accountProductId.
     *
     * @param accountProductId  the accountProductId of the tool tip
     */
    public void setAccountProductId(String accountProductId) {
        this.accountProductId = accountProductId;
    }

    /**
     * Gets the account name
     *
     * @return the accountName
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Sets the tool tip accountName.
     *
     * @param accountName  the accountName of the tool tip
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * Gets the accountProductDescription
     *
     * @return the accountProductDescription
     */
    public String getAccountProductDescription() {
        return accountProductDescription;
    }

    /**
     * Sets the tool tip accountProductDescription.
     *
     * @param accountProductDescription  the accountProductDescription of the tool tip
     */
    public void setAccountProductDescription(String accountProductDescription) {
        this.accountProductDescription = accountProductDescription;
    }

    /**
     * Gets the status
     *
     * @return the status
     */
    public AccountApplicationStatus getStatus() {
        return status;
    }


    /**
     * Sets the tool tip status.
     *
     * @param status  the status of the tool tip
     */
    public void setStatus(AccountApplicationStatus status) {
        this.status = status;
    }

    /**
     * Gets the account number
     *
     * @return the accountNumber
     */
    public boolean isIncomplete() {
        return incomplete;
    }

    /**
     * Sets the tool tip incomplete.
     *
     * @param incomplete  the incomplete of the tool tip
     */
    public void setIncomplete(boolean incomplete) {
        this.incomplete = incomplete;
    }

    /**
     * Gets the account Available In Backend
     *
     * @return the accountAvailableInBackend
     */
    public boolean isAccountAvailableInBackend() {
        return accountAvailableInBackend;
    }

    /**
     * Sets the tool tip accountAvailableInBackend.
     *
     * @param accountAvailableInBackend  the accountAvailableInBackend of the tool tip
     */
    public void setAccountAvailableInBackend(boolean accountAvailableInBackend) {
        this.accountAvailableInBackend = accountAvailableInBackend;
    }

    /**
     * Gets the account Has Balance In Backend
     *
     * @return the accountHasBalanceInBackend
     */
    public boolean isAccountHasBalanceInBackend() {
        return accountHasBalanceInBackend;
    }

    /**
     * Sets the tool tip accountHasBalanceInBackend.
     *
     * @param accountHasBalanceInBackend  the accountHasBalanceInBackend of the tool tip
     */
    public void setAccountHasBalanceInBackend(boolean accountHasBalanceInBackend) {
        this.accountHasBalanceInBackend = accountHasBalanceInBackend;
    }

    /**
     * Gets the waive Overdrafts
     *
     * @return the waiveOverdrafts
     */
    public boolean isWaiveOverdrafts() {
        return waiveOverdrafts;
    }

    /**
     * Sets the tool tip waiveOverdrafts.
     *
     * @param waiveOverdrafts  the waiveOverdrafts of the tool tip
     */
    public void setWaiveOverdrafts(boolean waiveOverdrafts) {
        this.waiveOverdrafts = waiveOverdrafts;
    }


}
