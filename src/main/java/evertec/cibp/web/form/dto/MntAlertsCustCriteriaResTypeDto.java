package evertec.cibp.web.form.dto;

import java.io.Serializable;

import evertec.otherservices.alertsResponses.MntAlertsCustCriteriaResType;

/**
 * Copyright 2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * MntAlertsCustCriteriaResTypeDto class.
 * <p>
 * The class MntAlertsCustCriteriaResTypeDto to wrap MntAlertsCustCriteriaResType.
 *
 * @author Jelvin Ferreras
 * @version 1.0
 * @since 1.0
 */
public class MntAlertsCustCriteriaResTypeDto implements Serializable {

    /**
     * UID
     */
    private static final long serialVersionUID = -4163979406043759649L;

    /**
     * MntAlertsCustCriteriaResType object
     * */
    private MntAlertsCustCriteriaResType mntAlertsCustCriteriaResType;

    /**
     * MntAlertsCustCriteriaResType constructor
     * */
    public MntAlertsCustCriteriaResTypeDto(MntAlertsCustCriteriaResType mntAlertsCustCriteriaResType) {
        this.mntAlertsCustCriteriaResType = mntAlertsCustCriteriaResType;
    }

    /**
     * Gets the data.
     *
     * @return the data
     */
    public MntAlertsCustCriteriaResType getMntAlertsCustCriteriaResType() {
        return mntAlertsCustCriteriaResType;
    }

    /**
     * Sets the data.
     *
     * @param mntAlertsCustCriteriaResType the new data
     */
    public void setMntAlertsCustCriteriaResType(MntAlertsCustCriteriaResType mntAlertsCustCriteriaResType) {
        this.mntAlertsCustCriteriaResType = mntAlertsCustCriteriaResType;
    }
}

