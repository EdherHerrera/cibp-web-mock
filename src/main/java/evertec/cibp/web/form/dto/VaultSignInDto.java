package evertec.cibp.web.form.dto;

import java.io.Serializable;



public class VaultSignInDto implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
    private String permId;
	private String sessionKey;
	
	/**
	 * @return the permId
	 */
	public String getPermId() {
		return permId;
	}

	/**
	 * @param permId the permId to set
	 */
	public void setPermId(String permId) {
		this.permId = permId;
	}

	/**
	 * @return the sessionKey
	 */
	public String getSessionKey() {
		return sessionKey;
	}

	/**
	 * @param sessionKey the sessionKey to set
	 */
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}


	@Override
	public String toString() {
		return "VaultSignInDto [permId=" + permId + ", sessionKey=" + sessionKey + "]";
	}

}
