package evertec.cibp.web.form.dto;

import java.io.Serializable;

import evertec.cibp.web.response.RecurringPaymentResponse;


public class ModifyPaymentActionResultDto extends SingleBackendActionResultDto implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	private RecurringPaymentResponse recurrPmt;

	public ModifyPaymentActionResultDto() {
	}

	public void setRecurrPmt(RecurringPaymentResponse recurrPmt) {
		this.recurrPmt = recurrPmt;
	}

	public RecurringPaymentResponse getRecurrPmt() {
		return this.recurrPmt;
	}

	public String generateErrorReport() {
		return this.generateAuditReport();
	}


}
