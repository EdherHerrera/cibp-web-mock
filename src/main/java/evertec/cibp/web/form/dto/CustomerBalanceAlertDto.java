package evertec.cibp.web.form.dto;

import java.io.Serializable;
import java.math.BigInteger;

import evertec.cibp.core.models.customer.AlertCondition;
import evertec.cibp.core.models.customer.CustomerBalanceAlert;
import evertec.cibp.core.models.customer.CustomerProfile;

public class CustomerBalanceAlertDto implements Serializable {

    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8908596825229518302L;

    /** The id. */
    private String id;

    /** The customer profile. */
    private CustomerProfile customerProfile;

    /** The perm id. */
    private BigInteger permId;

    /** The nickname. */
    private String nickname;

    /** The account number. */
    private String accountNumber;

    /** The account subtype. */
    private String accountSubtype;

	/** The account section. */
    private String accountSection;

    /** The account product id. */
    private String accountProductId;

    /** The alert condition. */
    private AlertCondition alertCondition;

    /** The Push Channel. */
    private String channelPush;

    /** The SMS Channel. */
    private String channelSMS;

    /** The Email Channel. */
    private String channelEmail;

    /** The alert position. */
    private String alertposicion;

    /** The alert language. */
    private String alertlanguage;

    /** Default constructor. */
    public CustomerBalanceAlertDto() {
    	super();
    }
    
    /** Alert constructor */
    public CustomerBalanceAlertDto(CustomerBalanceAlert alert) {
    	super();
    	this.id = alert.getId();
    	this.customerProfile = new CustomerProfile();
    	this.customerProfile.setProfileId(alert.getCustomerProfile().getProfileId());
    	this.permId = alert.getPermId();
    	this.nickname = alert.getNickname();
    	this.accountNumber = alert.getAccountNumber();
    	this.accountSubtype = alert.getAccountSubtype();
    	this.accountSection = alert.getAccountSection();
    	this.accountProductId = alert.getAccountProductId();
    	this.alertCondition = alert.getAlertCondition();
    	this.channelPush = alert.getChannelPush();
    	this.channelSMS = alert.getChannelSMS();
    	this.channelEmail = alert.getChannelEmail();
    	this.alertposicion = alert.getAlertposicion();
    	this.alertlanguage = alert.getAlertlanguage();
    }


    /**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the customerProfile
	 */
	public CustomerProfile getCustomerProfile() {
		return customerProfile;
	}

	/**
	 * @param customerProfile the customerProfile to set
	 */
	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}

	/**
	 * @return the permId
	 */
	public BigInteger getPermId() {
		return permId;
	}

	/**
	 * @param permId the permId to set
	 */
	public void setPermId(BigInteger permId) {
		this.permId = permId;
	}

	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the accountSubtype
	 */
	public String getAccountSubtype() {
		return accountSubtype;
	}

	/**
	 * @param accountSubtype the accountSubtype to set
	 */
	public void setAccountSubtype(String accountSubtype) {
		this.accountSubtype = accountSubtype;
	}

	/**
	 * @return the accountSection
	 */
	public String getAccountSection() {
		return accountSection;
	}

	/**
	 * @param accountSection the accountSection to set
	 */
	public void setAccountSection(String accountSection) {
		this.accountSection = accountSection;
	}

	/**
	 * @return the accountProductId
	 */
	public String getAccountProductId() {
		return accountProductId;
	}

	/**
	 * @param accountProductId the accountProductId to set
	 */
	public void setAccountProductId(String accountProductId) {
		this.accountProductId = accountProductId;
	}

	/**
	 * @return the alertCondition
	 */
	public AlertCondition getAlertCondition() {
		return alertCondition;
	}

	/**
	 * @param alertCondition the alertCondition to set
	 */
	public void setAlertCondition(AlertCondition alertCondition) {
		this.alertCondition = alertCondition;
	}

	/**
	 * @return the pushChannel
	 */
	public String getChannelPush() {
		return channelPush;
	}

	/**
	 * @param pushChannel the pushChannel to set
	 */
	public void setChannelPush(String channelPush) {
		this.channelPush = channelPush;
	}

	/**
	 * @return the sMSChannel
	 */
	public String getChannelSMS() {
		return channelSMS;
	}

	/**
	 * @param sMSChannel the sMSChannel to set
	 */
	public void setChannelSMS(String channelSMS) {
		this.channelSMS = channelSMS;
	}

	/**
	 * @return the emailChannel
	 */
	public String getChannelEmail() {
		return channelEmail;
	}

	/**
	 * @param emailChannel the emailChannel to set
	 */
	public void setChannelEmail(String channelEmail) {
		this.channelEmail = channelEmail;
	}

	/**
	 * @return the alertposicion
	 */
	public String getAlertposicion() {
		return alertposicion;
	}

	/**
	 * @param alertposicion the alertposicion to set
	 */
	public void setAlertposicion(String alertposicion) {
		this.alertposicion = alertposicion;
	}

	/**
	 * @return the alertlanguage
	 */
	public String getAlertlanguage() {
		return alertlanguage;
	}

	/**
	 * @param alertlanguage the alertlanguage to set
	 */
	public void setAlertlanguage(String alertlanguage) {
		this.alertlanguage = alertlanguage;
	}

}
