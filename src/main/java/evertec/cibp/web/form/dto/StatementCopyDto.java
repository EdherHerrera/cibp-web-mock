package evertec.cibp.web.form.dto;

import java.io.Serializable;

/**
 * The class StatementCopyDto to map statement copy request
 * 
 * @author Jorge L. Morla
 * @version 1.0
 * @since 1.0
 * */
public class StatementCopyDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The accountNumber
	 * */
	private String accountNumber;
	
	/**
	 * The bankId
	 * */
	private String bankId;
	
	
	/**
	 * The startDate
	 * */
	private String startDate;
	
	
	/**
	 * The endDate
	 * */
	private String endDate;
	
	
	/**The default Constructor with no args*/
	public StatementCopyDto() {}
	
	/**
	 * The constructor with args
	 * */
	public StatementCopyDto(String accountNumber, String bankId, String startDate, String endDate) {
		this.accountNumber = accountNumber;
		this.bankId = bankId;
		this.startDate = startDate;
		this.endDate = endDate;
		
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "StatementCopyDto [accountNumber=" + accountNumber + ", bankId=" + bankId + ", startDate=" + startDate
				+ ", endDate=" + endDate + "]";
	}
}
