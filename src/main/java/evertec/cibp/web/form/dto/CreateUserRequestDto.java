package evertec.cibp.web.form.dto;

import java.io.Serializable;

import com.rsa.csd.ws.CreateUserRequest;

import evertec.rsa.RSAConnector.Lang;

/**
 * The createUserRequest DTO
 *
 * @author Henry Terrero
 * @version 1.0
 * @since 1.0
 *
 * */
public class CreateUserRequestDto implements Serializable{
	
	   /**
     * The Language
     *
     * */
    private Lang lang;
    
    /**
     * The CreateUserRequest
     *
     * */
    private CreateUserRequest createUserRequest;

    /**
     * Gets the lang.
     *
     * @return the lang
     */
	public Lang getLang() {
		return lang;
	}

	   /**
     * Sets the lang.
     *
     * @param value the lang to set
     */
	public void setLang(Lang lang) {
		this.lang = lang;
	}

	  /**
     * Gets the create user request.
     *
     * @return the createUserRequest
     */
	public CreateUserRequest getCreateUserRequest() {
		return createUserRequest;
	}

	 /**
     * Sets the create user request.
     *
     * @param createUserRequest the createUserRequest to set
     */
	public void setCreateUserRequest(CreateUserRequest createUserRequest) {
		this.createUserRequest = createUserRequest;
	}
    
    

}
