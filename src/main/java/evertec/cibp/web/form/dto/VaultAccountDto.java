package evertec.cibp.web.form.dto;

import java.io.Serializable;

import evertec.cibp.core.models.account.AccountFeatures;
import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.vault.AcctBalType;
import evertec.cibp.core.models.vault.BankAcctRecType;


public class VaultAccountDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4684158565918518925L;
	private BankAcctRecType bankAcctRecType;
	private String id;
	private String accountType;
	private String statusCode;
	private String statusDescription;
	private String relationshipType;
	private String productId;
	private boolean balance;
	private String restrictionCode;
	private String blockCode;
	private String altBlockCode;
	private String sectionIndicator;
	private String bankId;
	private String checkingsIndicator;
	private String savingsIndicator;
	private String reserveIndicator;
	private AcctBalType balances;
	private AccountFeatures features;
	private TVAccountProductDefinition productDefinition;
	private String delinquencyCode;
	private String odLimit;

	/**
	 * @return the bankAcctRecType
	 */
	public BankAcctRecType getBankAcctRecType() {
		return bankAcctRecType;
	}
	/**
	 * @param bankAcctRecType the bankAcctRecType to set
	 */
	public void setBankAcctRecType(BankAcctRecType bankAcctRecType) {
		this.bankAcctRecType = bankAcctRecType;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the accountType
	 */
	public String getAccountType() {
		return accountType;
	}
	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @return the statusDescription
	 */
	public String getStatusDescription() {
		return statusDescription;
	}
	/**
	 * @param statusDescription the statusDescription to set
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	/**
	 * @return the relationshipType
	 */
	public String getRelationshipType() {
		return relationshipType;
	}
	/**
	 * @param relationshipType the relationshipType to set
	 */
	public void setRelationshipType(String relationshipType) {
		this.relationshipType = relationshipType;
	}
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	/**
	 * @return the balance
	 */
	public boolean isBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(boolean balance) {
		this.balance = balance;
	}
	/**
	 * @return the restrictionCode
	 */
	public String getRestrictionCode() {
		return restrictionCode;
	}
	/**
	 * @param restrictionCode the restrictionCode to set
	 */
	public void setRestrictionCode(String restrictionCode) {
		this.restrictionCode = restrictionCode;
	}
	/**
	 * @return the blockCode
	 */
	public String getBlockCode() {
		return blockCode;
	}
	/**
	 * @param blockCode the blockCode to set
	 */
	public void setBlockCode(String blockCode) {
		this.blockCode = blockCode;
	}
	/**
	 * @return the altBlockCode
	 */
	public String getAltBlockCode() {
		return altBlockCode;
	}
	/**
	 * @param altBlockCode the altBlockCode to set
	 */
	public void setAltBlockCode(String altBlockCode) {
		this.altBlockCode = altBlockCode;
	}
	/**
	 * @return the sectionIndicator
	 */
	public String getSectionIndicator() {
		return sectionIndicator;
	}
	/**
	 * @param sectionIndicator the sectionIndicator to set
	 */
	public void setSectionIndicator(String sectionIndicator) {
		this.sectionIndicator = sectionIndicator;
	}
	/**
	 * @return the balances
	 */
	public AcctBalType getBalances() {
		return balances;
	}
	/**
	 * @param balances the balances to set
	 */
	public void setBalances(AcctBalType balances) {
		this.balances = balances;
	}
	/**
	 * @return the features
	 */
	public AccountFeatures getFeatures() {
		return features;
	}
	/**
	 * @param features the features to set
	 */
	public void setFeatures(AccountFeatures features) {
		this.features = features;
	}
	/**
	 * @return the productDefinition
	 */
	public TVAccountProductDefinition getProductDefinition() {
		return productDefinition;
	}
	/**
	 * @param productDefinition the productDefinition to set
	 */
	public void setProductDefinition(TVAccountProductDefinition productDefinition) {
		this.productDefinition = productDefinition;
	}
	/**
	 * @return the bankId
	 */
	public String getBankId() {
		return bankId;
	}
	/**
	 * @param bankId the bankId to set
	 */
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	/**
	 * @return the checkingsIndicator
	 */
	public String getCheckingsIndicator() {
		return checkingsIndicator;
	}
	/**
	 * @param checkingsIndicator the checkingsIndicator to set
	 */
	public void setCheckingsIndicator(String checkingsIndicator) {
		this.checkingsIndicator = checkingsIndicator;
	}
	/**
	 * @return the savingsIndicator
	 */
	public String getSavingsIndicator() {
		return savingsIndicator;
	}
	/**
	 * @param savingsIndicator the savingsIndicator to set
	 */
	public void setSavingsIndicator(String savingsIndicator) {
		this.savingsIndicator = savingsIndicator;
	}
	/**
	 * @return the reserveIndicator
	 */
	public String getReserveIndicator() {
		return reserveIndicator;
	}
	/**
	 * @param reserveIndicator the reserveIndicator to set
	 */
	public void setReserveIndicator(String reserveIndicator) {
		this.reserveIndicator = reserveIndicator;
	}
	/**
	 * @return the delinquencyCode
	 */
	public String getDelinquencyCode() {
		return delinquencyCode;
	}
	/**
	 * @param delinquencyCode the delinquencyCode to set
	 */
	public void setDelinquencyCode(String delinquencyCode) {
		this.delinquencyCode = delinquencyCode;
	}
	/**
	 * @return the odLimit
	 */
	public String getOdLimit() {
		return odLimit;
	}
	/**
	 * @param odLimit the odLimit to set
	 */
	public void setOdLimit(String odLimit) {
		this.odLimit = odLimit;
	}
}