package evertec.cibp.web.form.dto;

import java.io.Serializable;
import java.util.Date;

import evertec.cibp.core.models.customer.devices.DeviceStatus;
import evertec.cibp.core.models.customer.devices.ProductType;

/**
 * Copyright 8 Evertec, Inc. All Rights Reserved.
 * <p>
 * CustomerDeviceDto class.
 * <p>
 * The class CustomerDeviceDto to mapping CustomerDevice Objects.
 *
 * @author Jorge De Los Santos
 * @version 1.0
 * @since 29/03/2019
 */
public class CustomerDeviceDto implements Serializable {

	private static final long serialVersionUID = 6527738604928326379L;
	
	private String fingerprintId;
	private String profileId;
	private String deviceId;
	private String deviceNickname;
	private DeviceStatus deviceStatus;
	private String deviceMbopId;
	private Date deviceRegDate;
	private Date deviceLastUsageDate;
	private String username;
	private String phone;
	private String ip;
	private ProductType productType;
	
	public CustomerDeviceDto() {
		
	}

	public CustomerDeviceDto(String fingerprintId, String profileId, String deviceId, String deviceNickname,
			DeviceStatus deviceStatus, String deviceMbopId, Date deviceRegDate, Date deviceLastUsageDate,
			String username, String phone, String ip, ProductType productType) {
		super();
		this.fingerprintId = fingerprintId;
		this.profileId = profileId;
		this.deviceId = deviceId;
		this.deviceNickname = deviceNickname;
		this.deviceStatus = deviceStatus;
		this.deviceMbopId = deviceMbopId;
		this.deviceRegDate = deviceRegDate;
		this.deviceLastUsageDate = deviceLastUsageDate;
		this.username = username;
		this.phone = phone;
		this.ip = ip;
		this.productType = productType;
	}

	public String getFingerprintId() {
		return fingerprintId;
	}

	public void setFingerprintId(String fingerprintId) {
		this.fingerprintId = fingerprintId;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceNickname() {
		return deviceNickname;
	}

	public void setDeviceNickname(String deviceNickname) {
		this.deviceNickname = deviceNickname;
	}

	public DeviceStatus getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(DeviceStatus deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	public String getDeviceMbopId() {
		return deviceMbopId;
	}

	public void setDeviceMbopId(String deviceMbopId) {
		this.deviceMbopId = deviceMbopId;
	}

	public Date getDeviceRegDate() {
		return deviceRegDate;
	}

	public void setDeviceRegDate(Date deviceRegDate) {
		this.deviceRegDate = deviceRegDate;
	}

	public Date getDeviceLastUsageDate() {
		return deviceLastUsageDate;
	}

	public void setDeviceLastUsageDate(Date deviceLastUsageDate) {
		this.deviceLastUsageDate = deviceLastUsageDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	@Override
	public String toString() {
		return "CustomerDeviceDto [fingerprintId=" + fingerprintId + ", profileId=" + profileId + ", deviceId="
				+ deviceId + ", deviceNickname=" + deviceNickname + ", deviceStatus=" + deviceStatus + ", deviceMbopId="
				+ deviceMbopId + ", deviceRegDate=" + deviceRegDate + ", deviceLastUsageDate=" + deviceLastUsageDate
				+ ", username=" + username + ", phone=" + phone + ", ip=" + ip + ", productType=" + productType + "]";
	}
	

}
