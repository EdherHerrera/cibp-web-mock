package evertec.cibp.web.form.dto;

import java.io.Serializable;
import java.math.BigInteger;

import evertec.cibp.core.models.customer.CustomerEmailBalanceAlert;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.EmailAlertCondition;

/**
 * This class provides a wrapper for requests related to alerts service
 */
public class CustomerEmailBalanceAlertDto implements Serializable {
	
	/** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2L;

    /** The id. */
    private String id;

    /** The account number. */
    private String accountNumber;

    /** The account subtype. */
    private String accountSubtype;

    /** The account section. */
    private String accountSection;

    /** The account product id. */
    private String accountProductId;

    /** The alert condition. */
    private EmailAlertCondition alertCondition;

    /** The nickname. */
    private String nickname;

    /** The perm id. */
    private BigInteger permId;

    /** The alert posicion. */
    private String alertposicion;

    /** The alert language. */
    private String alertlanguage;

    /** The customer profile. */
    private CustomerProfile customerProfile;

    /** Default constructor. */
    public CustomerEmailBalanceAlertDto() {
    	
    }
    
    /** Alert constructor */
    public CustomerEmailBalanceAlertDto(CustomerEmailBalanceAlert alert) {
    	this.id = alert.getId();
    	this.accountNumber = alert.getAccountNumber();
    	this.accountSubtype = alert.getAccountSubtype();
    	this.accountSection = alert.getAccountSection();
    	this.accountProductId = alert.getAccountProductId();
    	this.alertCondition = alert.getAlertCondition();
    	this.nickname = alert.getNickname();
    	this.permId = alert.getPermId();
    	this.alertposicion = alert.getAlertposicion();
    	this.alertlanguage = alert.getAlertlanguage();
    	this.customerProfile = new CustomerProfile();
    	this.customerProfile.setProfileId(alert.getCustomerProfile().getProfileId());
    }
    
    
    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }


    /**
     * Sets the id.
     *
     * @param id  the id to set
     */
    public void setId(String id) {
        this.id = id;
    }


    /**
     * Gets the accountNumber.
     *
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber.
     *
     * @param accountNumber  the accountNumber to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the accountSubtype.
     *
     * @return the accountSubtype
     */
    public String getAccountSubtype() {
        return accountSubtype;
    }


    /**
     * Sets the accountSubtype.
     *
     * @param accountSubtype  the accountSubtype to set
     */
    public void setAccountSubtype(String accountSubtype) {
        this.accountSubtype = accountSubtype;
    }


    /**
     * Gets the accountSection.
     *
     * @return the accountSection
     */
    public String getAccountSection() {
        return accountSection;
    }


    /**
     * Sets the accountSection.
     *
     * @param accountSection  the accountSection to set
     */
    public void setAccountSection(String accountSection) {
        this.accountSection = accountSection;
    }


    /**
     * Gets the accountProductId.
     *
     * @return the accountProductId
     */
    public String getAccountProductId() {
        return accountProductId;
    }


    /**
     * Sets the accountProductId.
     *
     * @param accountProductId  the accountProductId to set
     */
    public void setAccountProductId(String accountProductId) {
        this.accountProductId = accountProductId;
    }
    

    /**
     * Gets the alertCondition.
     *
     * @return the alertCondition
     */
    public EmailAlertCondition getAlertCondition() {
        return alertCondition;
    }


    /**
     * Sets the alertCondition.
     *
     * @param alertCondition  the alertCondition to set
     */
    public void setAlertCondition(EmailAlertCondition alertCondition) {
        this.alertCondition = alertCondition;
    }


    /**
     * Gets the nickname.
     *
     * @return the nickname
     */
    public String getNickname() {
        return nickname;
    }


    /**
     * Sets the nickname.
     *
     * @param nickname  the nickname to set
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }


    /**
     * Gets the permId.
     *
     * @return the permId
     */
    public BigInteger getPermId() {
        return permId;
    }


    /**
     * Sets the permId.
     *
     * @param permId  the permId to set
     */
    public void setPermId(BigInteger permId) {
        this.permId = permId;
    }


    /**
     * Gets the alertposicion.
     *
     * @return the alertposicion
     */
    public String getAlertposicion() {
        return alertposicion;
    }


    /**
     * Sets the alertposicion.
     *
     * @param alertposicion  the alertposicion to set
     */
    public void setAlertposicion(String alertposicion) {
        this.alertposicion = alertposicion;
    }


    /**
     * Gets the alertlanguage.
     *
     * @return the alertlanguage
     */
    public String getAlertlanguage() {
        return alertlanguage;
    }


    /**
     * Sets the alertlanguage.
     *
     * @param alertlanguage  the alertlanguage to set
     */
    public void setAlertlanguage(String alertlanguage) {
        this.alertlanguage = alertlanguage;
    }


    /**
     * Gets the customerProfile.
     *
     * @return the customerProfile
     */
    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }


    /**
     * Sets the customerProfile.
     *
     * @param customerProfile  the customerProfile to set
     */
    public void setCustomerProfile(CustomerProfile customerProfile) {
        this.customerProfile = customerProfile;
    }

}
