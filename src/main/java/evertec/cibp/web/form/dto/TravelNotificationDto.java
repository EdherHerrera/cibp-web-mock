package evertec.cibp.web.form.dto;

import java.io.Serializable;


/**
 * Copyright 2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * Travel notification Dto class.
 * <p>
 * The class TravelNotificationDto to mapping TravelNotificationResponse Objects.
 *
 * @author Henry Terrero
 * @version 1.0
 * @since 1.0
 */
public class TravelNotificationDto implements Serializable{
	
	 /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /**
     * plasticNumber
     * Type String
     */
    private String plasticNumber;
    
    /**
     * caseNumber
     * Type String
     */
    private String caseNumber;
    
    /**
     * status
     * Type String
     */
    private String status;
    
    /**
     * nextMethod
     * Type String
     */
    private String nextMethod;
    
    /**
     * message
     * Type String
     */
    private String message;
    
    /**
     * success
     * Type String
     */
    private boolean success;
    
    /**
     * Gets the PlasticNumber.
     *
     * @return the plasticNumber
     */
	public String getPlasticNumber() {
		return plasticNumber;
	}
	
	 /**
     * Sets the plasticNumber.
     *
     * @param plasticNumber, the plasticNumber
     */
	public void setPlasticNumber(String plasticNumber) {
		this.plasticNumber = plasticNumber;
	}
	
	 /**
     * Gets the CaseNumber.
     *
     * @return the caseNumber
     */
	public String getCaseNumber() {
		return caseNumber;
	}
	
	 /**
     * Sets the caseNumber.
     *
     * @param caseNumber, the caseNumber
     */
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	
	 /**
     * Gets the Status.
     *
     * @return the status
     */
	public String getStatus() {
		return status;
	}
	
	 /**
     * Sets the status.
     *
     * @param status, the status
     */
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	 /**
     * Gets the NextMethod.
     *
     * @return the nextMethod
     */
	public String getNextMethod() {
		return nextMethod;
	}
	
	 /**
     * Sets the nextMethod.
     *
     * @param nextMethod, the nextMethod
     */
	public void setNextMethod(String nextMethod) {
		this.nextMethod = nextMethod;
	}
	
	 /**
     * Gets the Message.
     *
     * @return the message
     */
	public String getMessage() {
		return message;
	}
	
	 /**
     * Sets the message.
     *
     * @param message, the message
     */
	public void setMessage(String message) {
		this.message = message;
	}
	
	 /**
     * Gets the Success.
     *
     * @return the success
     */
	public boolean isSuccess() {
		return success;
	}
	
	 /**
     * Sets the success.
     *
     * @param success, the success
     */
	public void setSuccess(boolean success) {
		this.success = success;
	}
      

}
