package evertec.cibp.web.form.dto;

import java.io.Serializable;
import java.util.Date;

import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.mbop.MbopPremiaCustomerInfo;
import evertec.cibp.core.models.mbop.MbopStatus;

public class MbopCustomerInfoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

    /**
     * The id
     */
    private String mbopId;

    /**
     *  This is a property for being mapping
     *  profileId
     */
    private String profileId;

    /**
     * This is a property for mapping
     * Enum for status
     * Type String
     */
    private MbopStatus status;

    /**
     *  This is a property for being mapping
     *  firstSignonOk
     */
    private Date firstSignonOk;

    /**
     *  This is a property for being mapping
     *  lastSignonOk
     */
    private Date lastSignonOk;

    /**
     *  This is a property for being mapping
     *  firstSignonFail
     */
    private Date firstSignonFail;

    /**
     *  This is a property for being mapping
     *  lastSignonFail
     */
    private Date lastSignonFail;

    /**
     *  This is a property for being mapping
     *  signonOkQty
     */
    private Long signonOkQty;

    /**
     *  This is a property for being mapping
     *  signonFailQty
     */
    private Long signonFailQty;

    /**
     *  This is a property for being mapping
     *  mbopPremiaInfo
     */
    private MbopPremiaCustomerInfo mbopPremiaInfo;

    /**
     *  This is a property for being mapping
     *  mbopPremiaInfo
     *  Enum for status
     */
    private CustomerEnrollmentSource enrollmentSource;

    /**
     * Gets the MbopId
     *
     * @return the MbopId
     */
    public String getMbopId() {
        return mbopId;
    }

    /**
     * Sets the tool tip mbopId.
     *
     * @param mbopId  the mbopId of the tool tip
     */
    public void setMbopId(String mbopId) {
        this.mbopId = mbopId;
    }

    /**
     * Gets the profile id.
     *
     * @return the profileId
     */
    public String getProfileId() {
        return profileId;
    }


    /**
     * Sets the tool tip profileId.
     *
     * @param profileId  the profileId of the tool tip
     */
    public void setProfileId(String profileId) { this.profileId = profileId; }

    /**
     * Gets the status
     *
     * @return the status
     */
    public MbopStatus getStatus() {
        return status;
    }


    /**
     * Sets the tool tip status.
     *
     * @param status  the status of the tool tip
     */
    public void setStatus(MbopStatus status) {
        this.status = status;
    }

    /**
     * Gets the firstSignonOk
     *
     * @return the firstSignonOk
     */
    public Date getFirstSignonOk() {
        return firstSignonOk;
    }


    /**
     * Sets the tool tip firstSignonOk.
     *
     * @param firstSignonOk  the firstSignonOk of the tool tip
     */
    public void setFirstSignonOk(Date firstSignonOk) {
        this.firstSignonOk = firstSignonOk;
    }

    /**
     * Gets the lastSignonOk
     *
     * @return the lastSignonOk
     */
    public Date getLastSignonOk() {
        return lastSignonOk;
    }


    /**
     * Sets the tool tip lastSignonOk.
     *
     * @param lastSignonOk  the lastSignonOk of the tool tip
     */
    public void setLastSignonOk(Date lastSignonOk) {
        this.lastSignonOk = lastSignonOk;
    }

    /**
     * Gets the firstSignonFail
     *
     * @return the firstSignonFail
     */
    public Date getFirstSignonFail() {
        return firstSignonFail;
    }


    /**
     * Sets the tool tip firstSignonFail.
     *
     * @param firstSignonFail  the enrollmentSource of the tool tip
     */
    public void setFirstSignonFail(Date firstSignonFail) {
        this.firstSignonFail = firstSignonFail;
    }

    /**
     * Gets the lastSignonFail
     *
     * @return the lastSignonFail
     */
    public Date getLastSignonFail() {
        return lastSignonFail;
    }


    /**
     * Sets the tool tip lastSignonFail.
     *
     * @param lastSignonFail  the lastSignonFail of the tool tip
     */
    public void setLastSignonFail(Date lastSignonFail) {
        this.lastSignonFail = lastSignonFail;
    }

    /**
     * Gets the signonOkQty
     *
     * @return the signonOkQty
     */
    public Long getSignonOkQty() {
        return signonOkQty;
    }


    /**
     * Sets the tool tip signonOkQty.
     *
     * @param signonOkQty  the signonOkQty of the tool tip
     */
    public void setSignonOkQty(Long signonOkQty) {
        this.signonOkQty = signonOkQty;
    }

    /**
     * Gets the signonOkQty
     *
     * @return the signonOkQty
     */
    public Long getSignonFailQty() {
        return signonFailQty;
    }


    /**
     * Sets the tool tip signonFailQty.
     *
     * @param signonFailQty  the enrollmentSource of the tool tip
     */
    public void setSignonFailQty(Long signonFailQty) {
        this.signonFailQty = signonFailQty;
    }

    /**
     * Gets the mbopPremiaInfo
     *
     * @return the mbopPremiaInfo
     */
    public MbopPremiaCustomerInfo getMbopPremiaInfo() {
        return mbopPremiaInfo;
    }


    /**
     * Sets the tool tip mbopPremiaInfo.
     *
     * @param mbopPremiaInfo  the mbopPremiaInfo of the tool tip
     */
    public void setMbopPremiaInfo(MbopPremiaCustomerInfo mbopPremiaInfo) {
        this.mbopPremiaInfo = mbopPremiaInfo;
    }

    /**
     * Gets the enrollmentSource
     *
     * @return the enrollmentSource
     */
    public CustomerEnrollmentSource getEnrollmentSource() {
        return enrollmentSource;
    }

    /**
     * Sets the tool tip enrollmentSource.
     *
     * @param enrollmentSource  the enrollmentSource of the tool tip
     */
    public void setEnrollmentSource(CustomerEnrollmentSource enrollmentSource) {
        this.enrollmentSource = enrollmentSource;
    }

}
