package evertec.cibp.web.form.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;

public class SmsProfileDto implements Serializable {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The sms profile id. */
	private String smsProfileId;
	
	/** The user name */
	private String userName;
	
	/** The permId. */
	private BigInteger permId;
	
	/** The acceptCharges. */
    private boolean acceptCharges;
    
  
    private List<SmsProfileAccountDto> accounts;
    private List<SmsEbppAccountDto> ebppaccounts;
    private Set<SmsCTNDto> ctns;



	public String getSmsProfileId() {
		return smsProfileId;
	}

	public void setSmsProfileId(String smsProfileId) {
		this.smsProfileId = smsProfileId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public BigInteger getPermId() {
		return permId;
	}

	public void setPermId(BigInteger permId) {
		this.permId = permId;
	}

	public boolean isAcceptCharges() {
		return acceptCharges;
	}

	public void setAcceptCharges(boolean acceptCharges) {
		this.acceptCharges = acceptCharges;
	}


	public List<SmsProfileAccountDto> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<SmsProfileAccountDto> accounts) {
		this.accounts = accounts;
	}

	public List<SmsEbppAccountDto> getEbppaccounts() {
		return ebppaccounts;
	}

	public void setEbppaccounts(List<SmsEbppAccountDto> ebppaccounts) {
		this.ebppaccounts = ebppaccounts;
	}


	public Set getCtns() {
		return this.ctns;
	}

	public void setCtns(Set numbers) {
		this.ctns = numbers;
	}
	
	
    

}
