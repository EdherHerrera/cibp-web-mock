package evertec.cibp.web.form.dto;

import java.io.Serializable;

import com.rsa.csd.ws.StatusHeader;

import evertec.cibp.core.services.responses.ResultStatus;


/**
 * This class creates a response that includes only the necessary fields
 */
public class ValidateOobChallengeResponseDto implements Serializable {

	/**
	 * The serialVersionUID
	 */
	private static final long serialVersionUID = -2340638875582850338L;

	/**
	 * The type
	 */
	private String type;

    /**
     * The deviceTokenCookie
     */
    private String deviceTokenCookie;

    /**
     * The deviceTokenFSO
     */
    private String deviceTokenFSO;

    /**
     * The resultStatus
     */
    private ResultStatus resultStatus;

    /**
     * Gets the type
     * @return type
     */
    public String getType() {
		return type;
	}

    /**
     * Sets the type
     * @param type
     */
	public void setType(String type) {
		this.type = type;
	}

    /**
     * Gets the deviceTokenCookie
     * @return deviceTokenCookie
     */
    public String getDeviceTokenCookie() {
        return deviceTokenCookie;
    }

    /**
     * Sets the deviceTokenCookie
     * @param deviceTokenCookie
     */
    public void setDeviceTokenCookie(String deviceTokenCookie) {
        this.deviceTokenCookie = deviceTokenCookie;
    }

    /**
     * Gets the deviceTokenFSO
     * @return deviceTokenFSO
     */
    public String getDeviceTokenFSO() {
        return deviceTokenFSO;
    }

    /**
     * Sets the deviceTokenFSO
     * @param deviceTokenFSO
     */
    public void setDeviceTokenFSO(String deviceTokenFSO) {
        this.deviceTokenFSO = deviceTokenFSO;
    }

    /**
     * Gets the resultStatus
     * @return resultStatus
     */
    public ResultStatus getResultStatus() {
        return resultStatus;
    }

    /**
     * Sets the resultStatus
     * @param resultStatus
     */
    public void setResultStatus(ResultStatus resultStatus) {
        this.resultStatus = resultStatus;
    }
}