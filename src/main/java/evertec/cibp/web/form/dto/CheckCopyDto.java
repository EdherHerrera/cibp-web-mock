package evertec.cibp.web.form.dto;

import java.io.Serializable;

import evertec.dp.models.xsd.photocopy.PhotocopyResponse_Type;

/**
 * Copyright 2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * CheckCopyResponse class.
 * <p>
 * The class CheckCopyResponse to return CheckCopy.
 *
 * @author Jelvin Ferreras
 * @version 1.0
 * @since 1.0
 */
public class CheckCopyDto implements Serializable {

    /**
     * Photocopy Response
     * */
    private PhotocopyResponse_Type photocopyResponse_type;

    /** Default constructor*/
    public CheckCopyDto() {

    }
    
    /**
     * Photocopy response constructor
     * */
    public CheckCopyDto(PhotocopyResponse_Type photocopyResponse_type) {
        this.photocopyResponse_type = photocopyResponse_type;
    }

    /**
     * Gets the data.
     *
     * @return the data
     */
    public PhotocopyResponse_Type getPhotocopyResponse_type() {
        return photocopyResponse_type;
    }

    /**
     * Sets the data.
     *
     * @param photocopyResponse_type the new data
     */
    public void setPhotocopyResponse_type(PhotocopyResponse_Type photocopyResponse_type) {
        this.photocopyResponse_type = photocopyResponse_type;
    }
}
