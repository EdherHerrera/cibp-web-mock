package evertec.cibp.web.form.dto;

import java.io.Serializable;

import evertec.cibp.vs.models.results.CustomerPayeeListActionResult;

/**
 * Copyright 2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * CustomerPayeeListActionResultDto class.
 * <p>
 * The class CustomerPayeeListActionResultDto to wrap CustomerPayeeListActionResult.
 *
 * @author Jelvin Ferreras
 * @version 1.0
 * @since 1.0
 */
public class CustomerPayeeListActionResultDto implements Serializable {
	
	 /**
     * UID
     */
    private static final long serialVersionUID = -4163979406043759649L;

    /**
     * CustomerPayeeListActionResult object
     * */
    private CustomerPayeeListActionResult customerPayeeListActionResult;

    /** Default constructor*/
    public CustomerPayeeListActionResultDto() {

    }
    
    /**
     * CustomerPayeeListActionResult constructor
     * */
    public CustomerPayeeListActionResultDto(CustomerPayeeListActionResult customerPayeeListActionResult) {
        this.customerPayeeListActionResult = customerPayeeListActionResult;
    }

    /**
     * Gets the data.
     *
     * @return the data
     */
    public CustomerPayeeListActionResult getCustomerPayeeListActionResult() {
        return customerPayeeListActionResult;
    }

    /**
     * Sets the data.
     *
     * @param customerPayeeListActionResult the new data
     */
    public void setCustomerPayeeListActionResult(CustomerPayeeListActionResult customerPayeeListActionResult) {
        this.customerPayeeListActionResult = customerPayeeListActionResult;
    }

}
