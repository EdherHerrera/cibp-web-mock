package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.entitlement.ATHMovilEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.form.bean.ATHMUpdatePhoneBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AthmUtils;
import evertec.cibp.web.utils.Utils;

public class ATHMUpdatePhoneForm extends AbstractWizardFormController implements MessageSourceAware {
	protected final Log logger = LogFactory.getLog(getClass());

	protected CustomerServices customerService = null;

	private MessageSource messageSource = null;

	private String fiId = null;
	private String fiToken = null;

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getFiId() {
		return fiId;
	}

	public void setFiId(String fiId) {
		this.fiId = fiId;
	}

	public String getFiToken() {
		return fiToken;
	}

	public void setFiToken(String fiToken) {
		this.fiToken = fiToken;
	}

	public ATHMUpdatePhoneForm() {
		super();
		setCommandClass(ATHMUpdatePhoneBean.class);
		setCommandName("athmupdatephone");
		setPages(new String[] { "athmupdatephone", "athmupdatephone", "athmupdatephone", "athmupdatephone" });
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {
		UserSession userSession = new UserSession();//user session
		final String strUserSession = "userSession";//user session string

		if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
			userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
		}

		userSession.setLoadebpp(false);

		Map<String, Object> model = new HashMap<>(); //model result

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		ATHMUpdatePhoneBean athmUpdatePhoneBean = (ATHMUpdatePhoneBean) command;

		String language = "sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "es" : Utils.getRSALanguage(request);

		Locale locale = new Locale(language);

		if (athmUpdatePhoneBean.getCurrentPhone() == null) {
			athmUpdatePhoneBean.setCurrentPhone(request.getParameter("phone"));
		}

		model.put("page", page);

		switch (page) {
		case 0:
			loadPhoneTypesData(request, command, model);
			loadPhoneProviderData(request, command, model);

			break;

		case 2:
			if (athmUpdatePhoneBean.getValidationCount() >= 3) {
				model.put("message", this.messageSource.getMessage("athmovil.enroll.validation.error", null, locale));
			} else {
				page = 3;

				model.put("page", page);
			}

			break;

		default:
			// Do nothing...
			break;
		}

		return model;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		ATHMUpdatePhoneBean athmUpdatePhoneBean = (ATHMUpdatePhoneBean) command;

		ATHMovilEntitlement entitlement = new ATHMovilEntitlement(userSession.getCustomerProfile()
				.getEntitlementByType(EntitlementType.ATHMOVIL));

		switch (page) {
		case 0:
			if (athmUpdatePhoneBean.getPhoneNumber() == null
					|| athmUpdatePhoneBean.getPhoneNumber().trim().length() <= 0) {
				errors.rejectValue("phoneNumber", "athmovil.phone.required", "Phone is required");
			} else if (AthmUtils.getNumericPhoneNumber(athmUpdatePhoneBean.getPhoneNumber()) == null) {
				errors.rejectValue("phoneNumber", "athmovil.phone.invalid", "Invalid phone");
			}

			if (!errors.hasErrors()) {
				// try {
				//
				// IsPhoneActiveResp isPhoneActiveResp =
				// athMobileFiService.isPhoneActive(this.fiId, this.fiToken,
				// AthmUtils.getStringNumericPhoneNumber(athmUpdatePhoneBean.getPhoneNumber()));
				//
				// if (isPhoneActiveResp != null &&
				// isPhoneActiveResp.getStatusCode().equals("00")
				// && isPhoneActiveResp.isIsActive()) {
				// errors.rejectValue("phoneNumber",
				// "athmovil.phone.validation.unavailable",
				// "Telephone is unavailable");
				// } else {
				// PhoneValidationProfileExistsResp
				// phoneValidationProfileExistsResp = athMobileFiService
				// .requestPhoneValidationProfileExists(entitlement.getCustomerToken(),
				// this.fiId,
				// this.fiToken,
				// AthmUtils.getStringNumericPhoneNumber(athmUpdatePhoneBean.getPhoneNumber()),
				// athmUpdatePhoneBean.getPhoneProvider());
				//
				// if (phoneValidationProfileExistsResp == null
				// ||
				// !phoneValidationProfileExistsResp.getStatusCode().equals("00"))
				// {
				// errors.rejectValue("pin", "athmovil.phone.validation.failed",
				// "Unable to validate phone");
				// } else if
				// (phoneValidationProfileExistsResp.getValidationReferenceId()
				// != null) {
				// athmUpdatePhoneBean.setValidationReferenceId(phoneValidationProfileExistsResp
				// .getValidationReferenceId());
				// }
				// }
				// } catch (Exception e) {
				// errors.rejectValue("phoneNumber",
				// "athmovil.phone.validation.failed",
				// "Unable to validate telephone");
				//
				// logger.error("Phone validation failed", e);
				// }
			}

			break;

		case 1:
			// try {
			// athMobileFiService = new ATHMobileFiService(this.wsAddress,
			// this.wsPortName);
			//
			// IsPhoneValidResp isPhoneValidResp =
			// athMobileFiService.isPhoneValid(this.fiId, this.fiToken,
			// athmUpdatePhoneBean.getValidationReferenceId());
			//
			// if (isPhoneValidResp == null ||
			// !isPhoneValidResp.getStatusCode().equals("00")) {
			// errors.reject("athmovil.ivr.validation.failed",
			// "Unable to validate phone");
			// } else if (!isPhoneValidResp.isIsValid()) {
			// athmUpdatePhoneBean.setValidationCount(athmUpdatePhoneBean.getValidationCount()
			// + 1);
			//
			// if (athmUpdatePhoneBean.getValidationCount() < 3) {
			// errors.reject("athmovil.ivr.validation.incorrect",
			// "Incorrect passcode");
			// }
			// }
			//
			// if (!errors.hasErrors() &&
			// athmUpdatePhoneBean.getValidationCount() < 3) {
			// athMobileFiService = new ATHMobileFiService(this.wsAddress,
			// this.wsPortName);
			//
			// ResponseStatus updateUserProfileResponse =
			// athMobileFiService.updateUserProfile(null, null, null,
			// entitlement.getCustomerToken(), this.fiId, null, this.fiToken,
			// null, null, null, AthmUtils
			// .getNumericPhoneNumber(athmUpdatePhoneBean.getPhoneNumber()),
			// ("RES"
			// .equals(athmUpdatePhoneBean.getPhoneType()) ? false : null),
			// null,
			// ("CEL".equals(athmUpdatePhoneBean.getPhoneType()) ?
			// athmUpdatePhoneBean.getPhoneProvider()
			// : null), null);
			//
			// if (updateUserProfileResponse == null ||
			// !updateUserProfileResponse.getStatusCode().equals("00")) {
			// errors.rejectValue("phoneNumber",
			// "athmovil.phone.validation.failed",
			// "Unable to validate phone");
			// }
			// }
			// } catch (Exception e) {
			// errors.reject("athmovil.phone.validation.failed",
			// "Unable to validate phone");
			//
			// logger.error("Phone validation failed", e);
			// }

			break;

		default:
			break;
		}
	}

	@Override
	protected int getTargetPage(HttpServletRequest request, Object command, Errors errors, int currentPage) {
		int target = super.getTargetPage(request, command, errors, currentPage);

		ATHMUpdatePhoneBean athmUpdatePhoneBean = (ATHMUpdatePhoneBean) command;

		if (athmUpdatePhoneBean != null) {
			switch (currentPage) {
			case 1:
				if (athmUpdatePhoneBean.getValidationCount() >= 3) {
					target = 2;
				}

				break;

			default:

				break;
			}
		}

		return target;
	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) {

		return proceessCancelFinishResult(request);
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		return proceessCancelFinishResult(request);
	}

	private void loadPhoneTypesData(HttpServletRequest request, Object command, Map<String, Object> model) {
		Map<String, String> phoneTypesMap = new LinkedHashMap<String, String>();

		String language = "sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "es" : Utils.getRSALanguage(request);

		Locale locale = new Locale(language);

		phoneTypesMap.put("CEL", this.messageSource.getMessage("athmovil.phone.type.cellular", null, locale));
		phoneTypesMap.put("RES", this.messageSource.getMessage("athmovil.phone.type.residential", null, locale));

		model.put("phonetypes", phoneTypesMap);
	}

	private void loadPhoneProviderData(HttpServletRequest request, Object command, Map<String, Object> model) {
		Map<String, String> phoneProvidersMap = new LinkedHashMap<String, String>();

		phoneProvidersMap.put("Claro", "Claro");
		phoneProvidersMap.put("AT&T", "AT&amp;T");
		phoneProvidersMap.put("Centennial", "Centennial");
		phoneProvidersMap.put("Sprint", "Sprint");
		phoneProvidersMap.put("TMobile", "T-Mobile");
		phoneProvidersMap.put("OpenMobile", "Open Mobile");
		phoneProvidersMap.put("Other", "Other");

		model.put("phoneproviders", phoneProvidersMap);
	}

	/**
	 * Process cancel & finish result method.
	 *
	 * @param request value
	 * @return ModelAndView result
	 * @author Modified by ET58344
	 * @since 08-31-2020
	 */
	private ModelAndView proceessCancelFinishResult (HttpServletRequest request) {
		Map<String, Object> model = new HashMap<>();//model result
		final String strUserSession = "userSession";//user session string
		final String strAthmUpdatePhone = "athmupdatephone";//athm string
		final String strPage = "page";//page string
		final int pageNumber = 3;//page number
		UserSession userSession = new UserSession();//user session

		if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
			userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
		}

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		model.put(strPage, pageNumber);

		return new ModelAndView(strAthmUpdatePhone, model);
	}
}
