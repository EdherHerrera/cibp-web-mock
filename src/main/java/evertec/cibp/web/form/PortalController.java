package evertec.cibp.web.form;

import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceRequest;
import edu.emory.mathcs.backport.java.util.Arrays;
import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.models.account.DelinquencyType;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.accountopening.AccountApplication;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.campaign.ActiveCampaignCustomerView;
import evertec.cibp.core.models.campaign.ActiveCampaignView;
import evertec.cibp.core.models.campaign.CampaignGenericBean;
import evertec.cibp.core.models.campaign.CampaignGuidedSetupType;
import evertec.cibp.core.models.campaign.CampaignStatus;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.campaign.GuidedSetupInfo;
import evertec.cibp.core.models.campaign.MyMessagesCampaign;
import evertec.cibp.core.models.campaign.ViewCampaign;
import evertec.cibp.core.models.customer.CustomerDevices;
import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.documents.CustomerDocumentsPackage;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.CookieEntitlement;
import evertec.cibp.core.models.entitlement.EZStatementEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.PFMEntitlement;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.postinitialfunding.PIFAccountStatusTypes;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.signup.SignupCampInfo;
import evertec.cibp.core.models.signup.SignupCampaign;
import evertec.cibp.core.models.transfer.CustomerExternalTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.adaptiveauth.RsaChallengeRequest;
import evertec.cibp.core.services.responses.AddPayeeResponse;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.RandomUtils;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.CampaignConcurrencyBean;
import evertec.cibp.web.form.interruptions.OOBInterruptionForm;
import evertec.cibp.web.models.customer.CustomerMessageContent;
import evertec.cibp.web.models.customer.CustomerMessageContentType;
import evertec.cibp.web.premia.control.PremiaUtil;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.CookieUtils;
import evertec.cibp.web.utils.DateUtil;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.PFMUtil;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static evertec.cibp.web.form.SessionInitializationController.NON_PREMIA_STATE_FLAG;

public class PortalController extends MultiActionController implements MessageSourceAware {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private CampaignConcurrencyBean concurrencyBean;
	private MessageSource messageSource;
	private CampaignGenericBean campaignGenericBean;
	private String wealthCodes;
	private String premiumBankingCodes; // customer codes for premium banking 
	private String preferredCodes;
	private boolean pfmAutomaticEntitlementCreation;
	private String demoUrl_en;
	private String demoUrl_es;
	private String bypassAccounts;
	private String iraProductIds;
	private String caeProductIds;
	private String insuranceRemoved;
	private String nonTransEnabledEstmts;
	private String nonTransEnabledInterestLetters;

	// FFEIC
	private CustomerServices customerService;

	// FIN FFEIC

	//MBFIS-83 - ET57991
	private String pingfederateSsoUrl; 
	private String pingfederateSsoEndpoint;
	private String pingfederateSsoPartnerUrl;
	//FIN
	
	private String[] marketplaceCountries;
	private final String strPortalView = "portal"; //string portal view
    
	public ModelAndView portal(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		Cookie versionCookie = CookieUtils.getCookie(request, "client-version");

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99
		
		if (!Objects.isNull(userSession.getNonPremiaState())) {
			model.put(NON_PREMIA_STATE_FLAG, userSession.getNonPremiaState());
		}

		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
		
		final int capacity = 2; // set capacity
		final Set<String> accountTypes = new HashSet<String>(capacity); // account types

		String lang = Utils.getRSALanguage(request);
		String language = "sp".equalsIgnoreCase(lang) ? "es" : lang;
		Locale locale = new Locale(language);
		@SuppressWarnings("unchecked")
		List<String> ntStatementsEnabledArray = new ArrayList<>(Arrays.asList(getNonTransEnabledEstmts().split(",")));
		@SuppressWarnings("unchecked")
		List<String> ntInterestLettersEnabledArray = new ArrayList<>(Arrays.asList(getNonTransEnabledInterestLetters().split(",")));
		
		if (this.getDowntimeMessageSourceStatus()) {
			return new ModelAndView("redirect:signondowntime");
		}
		
		if (userSession.getMessagesList() == null) {
			userSession.setMessagesList(new LinkedList<CustomerMessageContent>());
		}
		
		if (userSession.getCompletedApplications() == null) {
			userSession.setCompletedApplications(new LinkedList<AccountApplication>());
		}
		
		Object deepLink = WebUtils.getSessionAttribute(request, Utils.DEEP_LINK);
		if (deepLink != null) {
			try {
				String redirect = (String)deepLink;
				WebUtils.setSessionAttribute(request, Utils.DEEP_LINK, null);
				return new ModelAndView("redirect:" + redirect);
			}catch (Exception e) {
				logger.error("DeepLink received. Not a string parseable URL.");
			}
			
		}
		

		CustomerProfile profile = userSession.getCustomerProfile();
		if (profile == null && userSession.getUsernameReservation() == null) {
			throw new ProgrammingError("Customer profile and username reservation null");
		} else if (profile == null && userSession.getUsernameReservation() != null) {
			model.put("noaccounts", true);
			model.put("applicationCancelationTriggersErase", userSession.getIncompleteApplicationCount() == 1);
			// Username is reserve, redirigirlo a la pagina de pending
			// applications = fake portal
			return new ModelAndView(strPortalView, model);
		}
		boolean realTimeNotificationsClaro=StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO);
		model.put("realTimeNotificationsClaro",realTimeNotificationsClaro);
        model.put("athmSso", StaticMessageSource.isFlagEnabled(StaticMessageSource.ATHM_SSO));
		model.put("onOff", StaticMessageSource.isFlagEnabled(StaticMessageSource.ATH_ONOFF));
		model.put("pushNotifications", Utils.isPushEnabled());
		model.put("limitsPerSegment", Utils.isLimitsPerSegmentEnabled());
		model.put("ebillsflag", StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS));
		model.put("customerEmail", userSession.getCustomerProfile().getEmail());
		
		//MBSE-2513 Flag to Hide or Show the Mortgage Transactions Sections
		final String flagMBSE2513 = "flagMBSE2513"; // model key flag hide mortgage transactions
		
		model.put(flagMBSE2513, StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513));

		if (profile != null
				&& profile.hasEntitlement(EntitlementType.BILLPAY)
				&& customerService.getGlobalEntitlement(EntitlementType.BILLPAY).getStatus() == EntitlementStatus.ENABLED)
			model.put("billpayEnable", true);

		if (profile.getEntitlementByType(EntitlementType.BANKING) == null)
			throw new ProgrammingError("Customer profile banking entitlement null");

		String athm = request.getParameter("athm");

		BankingSession bankingSession = userSession.getBankingSession();
		if (bankingSession == null) {
			
			// Aqui ya se valido al usario, el exite pero el login no paso
			// Se redirige a la pagina de pending applications, sin cuentas
			
			userSession.setLoginCompleteSuccess(true);
			return new ModelAndView(strPortalView, model);
		}

		List<TVFrontendAccount> accounts = userSession.getAccounts();
		if (accounts == null)
			throw new ProgrammingError("Unable to get user accounts");
		
		// MBCA-1311
		accountTypes.add(Utils.RVA);
		accountTypes.add(Utils.LEA);		
		userSession.setRemoveAccountStatement(Utils.onlyHasAccountType(accountTypes, accounts));
		
		/*
		 * mpicado, muevo un poco el codigo que redirige a womancare para que
		 * pase por algunas validaciones, por lo tanto implica que tengo que
		 * mover tambien el codigo que redirige a SignupCampaign para que esto
		 * no ocurra antes que el redirect a womancare
		 */
		/* cibp-1867 */
		if (!GenericValidator.isBlankOrNull(userSession.getPromotion())
				&& userSession.getPromotion().matches(
						messageSource.getMessage("promo.regexp.condition", null, "", Locale.ENGLISH))) {
			String promo = userSession.getPromotion();
			userSession.setPromotion("");
			return new ModelAndView("redirect:" + promo);
		}
		
		/**
		 * banner accounts pending confirmation for MB
		 * @return boolean 
		 */		
		if (hasPendingPifExternalAccounts(userSession)) {
			model.put("hasPendingPifExternalAccounts", true);
		}


		// Not in mobile version
		if (userSession.getUserAgentType() == UserAgentType.WEB || userSession.getUserAgentType() == UserAgentType.IPAD) {

			if (profile.hasFlag(CustomerFlagType.DOCUMENTS_AVAILABLE)) {
				if (userSession.getDisbursementList() == null) {
					List<CustomerDocumentsPackage> packList = userSession.getDisbursementsList(false);
					if (packList != null && packList.isEmpty()) {
						CustomerFacade.getInstance().removeCustomerFlag(userSession.getCustomerProfile(), CustomerFlagType.DOCUMENTS_AVAILABLE, userSession);
					}
				}
				model.put("displayMyDocuments", userSession.getDisbursementList().size() > 0);
			}

			SignupCampInfo campaign = userSession.getCustomerService().getEnableSignupCampInfo(CampaignType.ESTMT);
			if (campaign.getStatus() == CampaignStatus.ENABLE
					&& DateUtil.isBetweenDate(campaign.getStartDate(), campaign.getEndDate(), new Date())) {
				List<SignupCampaign> signupCampaignInfoByProfileIdA = userSession.getCustomerSignupCampaigns();
				SignupCampaign estmtCampaign = null;
				for (Iterator<SignupCampaign> iter = signupCampaignInfoByProfileIdA.iterator(); iter.hasNext();) {
					SignupCampaign camp = (SignupCampaign) iter.next();
					if (camp.getCampaignType().equals(CampaignType.ESTMT.toString())) {
						estmtCampaign = camp;
						break;
					}
				}

				// Verify if the user needs to view the e-Statement campaign
				// page
				if (estmtCampaign != null && estmtCampaign.getCampaignResult() == 'X'
						&& estmtCampaign.getCampaignType().equals("ESTMT")) {
					if (versionCookie == null
							|| (versionCookie != null && !"mobile".equalsIgnoreCase(versionCookie.getValue())))
						return new ModelAndView("redirect:estatementcampaign");
				}
			}
		}

		if (accounts != null && accounts.size() > 0) {
			List<TVFrontendAccount> portalAccounts = new LinkedList<>();
			List<TVFrontendAccount> secins = new LinkedList<>();//security and insurance accounts
			List<TVFrontendAccount> programs = new LinkedList<>();//premia rewards
			List<TVFrontendAccount> guardianRepPayeeAccts = new LinkedList<>();
			List<TVFrontendAccount> ghostAccounts = new LinkedList<>(); // List of ghost accounts.
			Map<String, Boolean> map = new HashMap<>();
          
			for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount name = (TVFrontendAccount) iterator.next();
				if (name.getSubtype().equalsIgnoreCase("RVA")) {
					userSession.setHasAuto(true);
				}
				if (name.getSubtype().equals("CCA")) {
					userSession.setHasCCA(true);
					if (!name.getHide())
						userSession.setHasVisibleCCA(true);
						//If the user has one or many CashRewards, this will verify if its a Primary or a Secondary Customer 
						//to see the balance information
						model.put("cashRewardsFlag", StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS));
						
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS) && name.isPrimaryAccount()) {
							map.put(name.getFrontEndId(), true);	
						} else {
							map.put(name.getFrontEndId(), false);
						}

						model.put("isPrimaryMap", map);

				}

				if (name.getSubtype().equals("IDA")) {
					if (name.getAccountProductId().equals("081")) {
						userSession.setHasEaccount(true);
					}
					userSession.setHasDDA(true);
					if (!name.getHide())
						userSession.setHasVisibleDDA(true);
				}
				if (name.getSubtype().equals("MLA")) {
					userSession.setHasMortgage(true);
				}
				if (name.getSubtype().equals("PRM")) {
					userSession.setHasPremia(true);
					if (!name.getHide())
						userSession.setHasVisiblePremia(true);
				}
				if (name.getSubtype().equals("CDA")) {
					if (this.iraProductIds.contains(name.getAccountProductId())) {
						userSession.setHasIRA(true);
					} else {
						userSession.setHasCD(true);
					}
				}
				if (name.getSubtype().equals("INS") || name.getSubtype().equals("SEC")) {
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE675)) {
						if (!this.insuranceRemoved.contains(name.getProductDefinition().getProductId())){
							userSession.setHasInsurance(true);
						}
					}
					else{
						userSession.setHasInsurance(true);
					}
				}
				if (name.getSubtype().equals("LEA")) {
					userSession.setHasLeasing(true);
				}
				if (name.getSubtype().equals("SEC")) {
					userSession.setHasSecurities(true);
					
					//MBFIS-83 - ET57991
					// This is for the SSO agreement for Popular Securities. Copy in
					// referenceData() for null account parameter
					String accepted = userSession.getCustomerService().getSSOAccepted(userSession.getCustomerProfile().getProfileId());
					if ("1".equalsIgnoreCase(accepted) || "0".equalsIgnoreCase(accepted))
						model.put("SSOaccepted", accepted);
					else if (accepted == null)
						model.put("SSOaccepted", "nouser");
					model.put("pingfederateSsoUrl", pingfederateSsoUrl);
					model.put("pingfederateSsoEndpoint", pingfederateSsoEndpoint);
					model.put("pingfederateSsoPartnerUrl", pingfederateSsoPartnerUrl);
				}
				if (name.getSubtype().equals("ILA")) {
					userSession.setHasPersonalLoans(true);
				}
				if (!name.getHide()) {
					if (name.getSubtype().equals("INS") || name.getSubtype().equals("SEC")){
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE675)) {
							if (!this.insuranceRemoved.contains(name.getProductDefinition().getProductId())){
								secins.add(name);
							}
						}
						else{
							secins.add(name);
						}
					}
					else if (name.getSubtype().equals("PRM"))
						programs.add(name);
					else if (name.getIsGuardianOrRepPayeeAcc())
						guardianRepPayeeAccts.add(name);
					else if ( "CCA".equals(name.getSubtype())  
							&& name.getProductDefinition() != null 
							&& name.getProductDefinition().getProductId().equals("PLNOC")) {
						ghostAccounts.add(name);
					}
					else
						portalAccounts.add(name);
				}
			}

			if (portalAccounts != null && portalAccounts.size() > 0)
				model.put("accounts", portalAccounts);
			if (secins != null && secins.size() > 0)
				model.put("secins", secins);
			if (guardianRepPayeeAccts != null && guardianRepPayeeAccts.size() > 0)
				model.put("guardianRepPayeeAccts", guardianRepPayeeAccts); // CIBP-2542
			if (userSession.getFisFilterAccounts() != null && userSession.getFisFilterAccounts().size() > 0)
				model.put("rdcAccounts", userSession.getFisFilterAccounts());
			if (!ghostAccounts.isEmpty()) {
				model.put("ghostAccounts", ghostAccounts);
			}
			

			final boolean isPremEnabled = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559);
			if (isPremEnabled) {
				List<TVFrontendAccount> frontendAccounts = userSession.getPremiaAccountList()
						.stream().filter(tvFrontendAccount -> tvFrontendAccount.isPrimaryAccount())
						.collect(Collectors.toList());

				if (frontendAccounts.size() > 0 && userSession.isHasPremia()) {
					model.put("premiaProgramText" , messageSource.getMessage("premia.program.name" , null , locale));
					model.put("premiaMobileSubtype" , PremiaUtil.PREMIA_MOBILE_SUBTYPE);
					model.put("premiaMobileProdId" , PremiaUtil.PREMIA_MOBILE_PRODID);
					final TVFrontendAccount premiaPriority = userSession.getCustomerService().getPriorityPremiaAccount(frontendAccounts);//
					List<TVFrontendAccount> programstmp = new LinkedList<TVFrontendAccount>();
					programstmp.add(premiaPriority);
					model.put("programs" , programstmp);

				}
			} else if (programs.size() > 0) {
				model.put("programs" , programs);model.put("cashRewardsFlag", StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS));
			}

			model.put("premiaFlag" , isPremEnabled);

			/**
			 * @author jortiz
			 * @mbfis462
			 * Get the retirement plan of the user if the user have one retirement plan.
			 */
			 boolean globalRetirementPlanEnable = false; //GlobalEntitlement RETIREMENT_PLAN
			 
			 final String flagMBFIS581 = "flagMBFIS581";//flag retirement plan for devices
			 
			 final String flagMBFIS581CIBP = "MBFIS581_CIBP";//flag retirement plan for devices
			 
			 final String retplanDowntimeMessage = "retplanDowntimeMessage"; //Retirement plan downtime message
			 
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_CIBP)) {
				
				globalRetirementPlanEnable = CustomerFacade.getInstance().getGlobalEntitlement(userSession, EntitlementType.RETIREMENT_PLAN).getStatus() == EntitlementStatus.ENABLED; // check if global entitlement retirement plan is enabled
				
				model.put(flagMBFIS581CIBP, true);
				
				if (userSession.isHasRetirementPlan()) {

	    			final String retPlanInfo = "retplan";//var to put in model

	    			model.put(retPlanInfo, userSession.getRetirementPlan());

	    		}
				
				if (userSession.isHasRetirementPlan() && globalRetirementPlanEnable) 
		    	{
		    		final String rpBadge = "retirementPlanAccountNewBadge"; //Retirement plan new badge key

		    		model.put(rpBadge, userSession.hasRetirementPlanBudge());

		    	}
				
			}else {
				
				model.put(flagMBFIS581CIBP, false);
				
			}
			
			/**
			 * MBFIS-809 Show a message when CSR Downtime is
			 **/
			if (globalRetirementPlanEnable) {
				model.put(retplanDowntimeMessage, Boolean.TRUE);
			}else {
				model.put(retplanDowntimeMessage, Boolean.FALSE);
			}
			

			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_MOBILE)) {
				model.put(flagMBFIS581, true);
			}else{
				model.put(flagMBFIS581, false);
			}
			
			final String customerEnt = "custEntitlementRetPlan"; //Retirement plan customer Entitlement
			
			model.put(customerEnt,profile.hasEntitlement(EntitlementType.RETIREMENT_PLAN));
			
			final String retPlanCallBack = "retPlanCallBack"; //Retirement plan call flag
			
			model.put(retPlanCallBack,userSession.getRetPlanCallback());

			
			model.put("customername", profile.getFirstName());

			addDelinquencyFlags(model, userSession);
			
			//Deliquency Messages saved in session
			if (userSession.getNewNotifications() == null) {
				if (model.containsKey("skip")) {
					CustomerMessageContent message = 
							new CustomerMessageContent(Utils.CUSTOMER_MESSAGE_DELICUENCY_SKIP + CustomerMessageContentType.WARNING,
													   CustomerMessageContentType.WARNING, 
													   messageSource.getMessage("notification.center.delinquency.skip", null, locale), 
													   Utils.CUSTOMER_MESSAGE_WARNING_ICON);
					if (!userSession.getMessagesList().contains(message))
						userSession.getMessagesList().add(message);
				}
				if (model.containsKey("pastdue3")) {
					CustomerMessageContent message = 
							new CustomerMessageContent(Utils.CUSTOMER_MESSAGE_DELICUENCY_PASTDUE3 + CustomerMessageContentType.WARNING,
													   CustomerMessageContentType.WARNING, 
													   messageSource.getMessage("notification.center.delinquency.pastdue3", null, locale), 
													   Utils.CUSTOMER_MESSAGE_WARNING_ICON);
					if (!userSession.getMessagesList().contains(message))
						userSession.getMessagesList().add(message);
				}
				if(model.containsKey("chargeoff")) {
					CustomerMessageContent message = 
							new CustomerMessageContent(Utils.CUSTOMER_MESSAGE_DELICUENCY_CHARGEOFF + CustomerMessageContentType.WARNING,
													   CustomerMessageContentType.WARNING, 
													   messageSource.getMessage("notification.center.delinquency.chargeoff", null, locale), 
													   Utils.CUSTOMER_MESSAGE_WARNING_ICON);

					if (!userSession.getMessagesList().contains(message))
						userSession.getMessagesList().add(message);
				}
			}				

			// CIBP-2769 Enable PFM for new users of OAO
			boolean hasNewOaoAccount = false;
			if (profile.getEnrollmentSource().equals(CustomerEnrollmentSource.ONLINE_AO)) {
				TVFrontendAccount acct = null;
				for (Iterator<TVFrontendAccount> iterator = portalAccounts.iterator(); iterator.hasNext();) {
					acct = (TVFrontendAccount) iterator.next();
					if (acct.isNewAccount() && acct.getAccountProductId().equals("081")
							&& profile.getEnrollmentAccountNumber().equals(acct.getAccountNumber())) {
						hasNewOaoAccount = true;
						break;
					}
				}
			}
			// CIBP-2769 End

			//Birthday Verification
			Boolean isBirthday = false;
			Calendar birthday = Calendar.getInstance();
			Calendar today = Calendar.getInstance();
			birthday.setTime(profile.getBirthDate());
			if (birthday.get(Calendar.MONTH) == today.get(Calendar.MONTH)
					&& birthday.get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH)) {
				isBirthday = true;
			}
			
			// CIBP-2700 No PFM for mobile version
			if (userSession.getUserAgentType() == UserAgentType.WEB
					|| userSession.getUserAgentType() == UserAgentType.IPAD) {

				if (isBirthday)
					model.put("birthday", isBirthday);

				// PFM - Get the PFM entitlement
				PFMUtil pfmUtil = new PFMUtil();
				boolean showUpdateInProcess = false;
				CustomerEntitlement entitlementByType = null;
				PFMEntitlement pfmentitlement = null;

				if (!profile.hasEntitlement(EntitlementType.PFM)) {
					if (pfmAutomaticEntitlementCreation
							|| (!pfmAutomaticEntitlementCreation && userSession
									.isNewUserForAutomaticPFMEntitlementCreation()) || hasNewOaoAccount) {
						pfmUtil.createPfmEntitlement(userSession.getCustomerService(), profile);
						entitlementByType = profile.getEntitlementByType(EntitlementType.PFM);
						pfmentitlement = new PFMEntitlement(entitlementByType);
					}
					// First time in PFM
					if (userSession.getPfmFirstTime() == null)
						userSession.setPfmFirstTime("Y");
				} else {

					// If the PFM user id in the entitlement does not have a
					// value, set it to blank
					entitlementByType = profile.getEntitlementByType(EntitlementType.PFM);

					pfmentitlement = new PFMEntitlement(entitlementByType);
					if (entitlementByType.getString("PFM_USER_ID") == null
							|| entitlementByType.getString("PFM_USER_ID").equals(""))
						pfmentitlement.setPFM_USER_ID("");

					if (userSession.getPfmFirstTime() == null)
						userSession.setPfmFirstTime("N");
				}

				model.put("pfmFirstTime", userSession.getPfmFirstTime().equals("Y"));

				// ATH Movil Entitlement
				if (profile != null
						&& customerService.getGlobalEntitlement(EntitlementType.ATHMOVIL) != null
						&& customerService.getGlobalEntitlement(EntitlementType.ATHMOVIL).getStatus() == EntitlementStatus.ENABLED) {
					model.put("athmovilEnable", true);

					// Check if user is registered
					if (profile.hasEntitlement(EntitlementType.ATHMOVIL)) {
						model.put("athmsendEnable", true);
						model.put("athmsend", athm);
					} else {
						if (userSession.isAthmUnregistered()) {
							userSession.setStatusmessage(messageSource.getMessage(
									"athmobile.portal.message.unregistered", null, locale));
							userSession.setAthmUnregistered(false); // reset so
							// the
							// message
							// is
							// displayed
							// only once
						}
						model.put("athmenrollEnable", true);
						model.put("athmenroll", athm);
					}
				}
				// END ATH Movil

				// Automatically create the EZSTMT entitlement if the client
				// does not have one
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.ESTATEMENTS_ENROLLMENT))
				{
					if (!profile.hasEntitlement(EntitlementType.EZSTMT))
					{
						CustomerEntitlement entitlement = new CustomerEntitlement();
						entitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
						entitlement.setCustomerProfile(profile);
						entitlement.setGlobalEntitlement(customerService.getGlobalEntitlement(EntitlementType.EZSTMT));
						entitlement.setType(EntitlementType.EZSTMT);
						EZStatementEntitlement ezStatementEntitlement = new EZStatementEntitlement(entitlement);
						ezStatementEntitlement.setEmptyEntitlement();

						// Enable eStatements for Non Transactional Accounts Automatically 
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA176) 
								&& AccountUtils.getIsTransactional(profile).equals("N")) {

							DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
							Date date = new Date();     	
							StringBuilder accountsStatus = new StringBuilder();

							for(int i = 0; i < userSession.getAccounts().size(); i++) {
								if(ntStatementsEnabledArray.contains(userSession.getAccounts().get(i).getSubtype())) {
									if(i >= 0 && i < userSession.getAccounts().size()-1) {
										accountsStatus.append(userSession.getAccounts().get(i).getAccountNumber() + "-" + dateFormat.format(date) + "-Y" + ",");
									}
									else {
										accountsStatus.append(userSession.getAccounts().get(i).getAccountNumber() + "-" + dateFormat.format(date) + "-Y"); 
									}
								}
							}			     	
							ezStatementEntitlement.setMlaEstmt(accountsStatus.toString());
						}

						//MBCA178
						//eSign Enforcement_Mortgage/Auto Interest letters for non-Transactional Accounts Automatically
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA178) 
								&& ntInterestLettersEnabledArray.contains(profile.getEnrollmentAccountSubtype())) {

							String dateString = (((today.get(Calendar.MONTH) + 1) < 10) ? "0" : "") + (today.get(Calendar.MONTH) + 1) + "/"
									+ ((today.get(Calendar.DATE) < 10) ? "0" : "") + (today.get(Calendar.DATE)) + "/"
									+ (today.get(Calendar.YEAR));

							ezStatementEntitlement.setResStatus(String.valueOf(EntitlementStatus.ENABLED));
							ezStatementEntitlement.setRvaStatus(String.valueOf(EntitlementStatus.ENABLED));
							ezStatementEntitlement.setLvaStatus(String.valueOf(EntitlementStatus.ENABLED));
							ezStatementEntitlement.setResActiveDate(dateString);
							ezStatementEntitlement.setRvaActiveDate(dateString);
							ezStatementEntitlement.setLvaActiveDate(dateString);				
						}

						customerService.addEntitlement(profile, entitlement);
					}
				}
				else 
				{
					if (!profile.hasEntitlement(EntitlementType.EZSTMT)) {
						CustomerEntitlement entitlement = new CustomerEntitlement();
						entitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
						entitlement.setCustomerProfile(profile);
						entitlement.setGlobalEntitlement(customerService.getGlobalEntitlement(EntitlementType.EZSTMT));
						entitlement.setType(EntitlementType.EZSTMT);
						EZStatementEntitlement ezStatementEntitlement = new EZStatementEntitlement(entitlement);
						ezStatementEntitlement.setEmptyEntitlement();
						customerService.addEntitlement(profile, entitlement);
					}
				}

				// PFM - is the PFM system up?
				if (customerService.getGlobalEntitlement(EntitlementType.PFM)
						.getStatus() == EntitlementStatus.ENABLED) {

					if (entitlementByType != null) {
						// If the system is in update AND the pfm user id does
						// not exist, then we present the update image
						if (entitlementByType.getString("PFM_USER_ID").equals("")
								&& userSession.isInPfmUpdatedLookedUp() && userSession.isInPfmUpdate()) {
							model.put("viewPfmPortal", "N");
							model.put("viewPfm", "N");
							model.put("pfmEnabled", "N");
							showUpdateInProcess = true;
						} else {
							if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA511))
							{
								if(AccountUtils.getIsTransactional(profile).equalsIgnoreCase("Y"))
								{	
									model.put("viewPfmPortal", entitlementByType.getString("VIEW_PORTAL"));
									model.put("viewPfm", entitlementByType.getString("VIEW_PFM"));
									model.put("pfmEnabled", "Y");

									// PFM - Get the past 24 months
									Date[] past24Months = new Date[24];
									Calendar cal = Calendar.getInstance();
									past24Months[0] = cal.getTime();
									model.put("past24Months_0", past24Months[0]);

									for (int i = 1; i < 24; i++) {
										cal.add(Calendar.MONTH, -1);
										past24Months[i] = cal.getTime();
										model.put("past24Months_" + i, past24Months[i]);
									}

									// PFM - Get the language
									boolean isEnglish = userSession.getLanguage().equalsIgnoreCase("en");
									model.put("isenglish", isEnglish);

								} else {
									model.put("viewPfmPortal", "N");
									model.put("viewPfm", "N");
									model.put("pfmEnabled", "N");
								}
							} else{
								model.put("viewPfmPortal", entitlementByType.getString("VIEW_PORTAL"));
								model.put("viewPfm", entitlementByType.getString("VIEW_PFM"));
								model.put("pfmEnabled", "Y");

								// PFM - Get the past 24 months
								Date[] past24Months = new Date[24];
								Calendar cal = Calendar.getInstance();
								past24Months[0] = cal.getTime();
								model.put("past24Months_0", past24Months[0]);

								for (int i = 1; i < 24; i++) {
									cal.add(Calendar.MONTH, -1);
									past24Months[i] = cal.getTime();
									model.put("past24Months_" + i, past24Months[i]);
								}

								// PFM - Get the language
								boolean isEnglish = userSession.getLanguage().equalsIgnoreCase("en");
								model.put("isenglish", isEnglish);

							} 
						}
					}
					else {
						model.put("viewPfmPortal", "N");
						model.put("viewPfm", "N");
						model.put("pfmEnabled", "N");
					}
				} else {
					model.put("viewPfmPortal", "N");
					model.put("viewPfm", "N");
					model.put("pfmEnabled", "N");
				}
				model.put("showUpdateInProcess", showUpdateInProcess);


			} else {
				model.put("viewPfmPortal", "N");
				model.put("viewPfm", "N");
				model.put("pfmEnabled", "N");
				model.put("showUpdateInProcess", false);
			}

			if (isBirthday && userSession.getNewNotifications() == null) {
				//Birthday message saved in session
				CustomerMessageContent message = 
						new CustomerMessageContent(Utils.CUSTOMER_MESSAGE_BIRTHDAY,
												   CustomerMessageContentType.EVENT, 
												   messageSource.getMessage("portal.label.birthday", null, locale), 
												   Utils.CUSTOMER_MESSAGE_EVENT_ICON);
				if (!userSession.getMessagesList().contains(message))
					userSession.getMessagesList().add(message);
			}
				
			boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);
			String rsaCookieValue = userSession.getDeviceRequest() == null ? ""
					: userSession.getDeviceRequest().getDeviceTokenCookie();
			
			if (oobFlag && !userSession.getCheckedOobEnroll()) {
				/**
				 * Codigo para ayudar a la verificación y de los estados de OOB
				 */
				userSession.setCheckedOobEnroll(true);
				// RSA Entitlement
				if (!profile.hasEntitlement(EntitlementType.RSA)) {
					RSAUtils.createRSAEntitlement(customerService, userSession.getCustomerProfile());
					userSession.setOobEnroll(false);
				} else {
					final String mobileSdkData = userSession.getMobileSdkData();
					
					final boolean isSDKMobileRequest = !GenericValidator.isBlankOrNull(mobileSdkData);
					DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), 
							mobileSdkData, request, isSDKMobileRequest);
					/**
					 * MBNA-1985
					 * Method getRsaChallengeMethod : CIBP-WEB microservice integration
					 * 
					 */
					RsaChallengeRequest rsaChallengeRequest = new RsaChallengeRequest(profile.getUsername(),deviceRequest);
					
					CredentialType credentialType = customerService.rsaGetChallengeMethod(rsaChallengeRequest);
					userSession.setDeviceRequest(deviceRequest);

					if (deviceRequest.getDeviceTokenCookie() != null) {
						Cookie cookie = new Cookie("PMData", deviceRequest.getDeviceTokenCookie());
						cookie.setPath("/");
						cookie.setMaxAge(31536000);
						ESAPI.httpUtilities().addCookie(response, cookie);
						rsaCookieValue = deviceRequest.getDeviceTokenCookie();
					}
					if (credentialType != null && (credentialType.equals(CredentialType.OOBPHONE)
							|| credentialType.equals(CredentialType.USER_DEFINED)))
						userSession.setOobEnroll(true);
					else
						userSession.setOobEnroll(false);
				}

			}

			// Automatically create the MOBILE CASH entitlement if the client
			// does not have one
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MOBILE_CASH_ENROLLMENT)
					&& !profile.hasEntitlement(EntitlementType.MOBILE_CASH)) {
				this.createMobileCashEntitlement(userSession);
			}

			// Demo URL
			model.put("demoUrl", userSession.getLanguage().equalsIgnoreCase("en") ? demoUrl_en : demoUrl_es);
			model.put("rsa_cookie", rsaCookieValue);
		}
		// if (profile.isPendingAccountApplication() || profile == null) {
		// addApplications(model, userSession);
		// } else {
		// model.put("canOpenAdditionalAccounts",
		// userSession.isCanOpenAdditionalAccounts());
		// model.put("canCompleteAdditionalAccounts",
		// userSession.isCanCompleteAdditionalAccounts());
		//
		// }
		
		//Pending External Accounts messages
		AccountMaskUtil maskutil = new AccountMaskUtil();
		if (profile.isPendingExternalAccountValidation()) {
			List<CustomerExternalTransferSource> pendingExternalAccounts = userSession.getPendingExternalAccountsValidation();
			if (!pendingExternalAccounts.isEmpty() && userSession.getNewNotifications() == null) {
				String accountMessage = messageSource.getMessage("portal.externalVerification.label", null, locale) + ":";
				String messageId = Utils.CUSTOMER_MESSAGE_PENDING_ACCOUNTS;
				for (Iterator iterator = pendingExternalAccounts.iterator(); iterator.hasNext();) {
					CustomerExternalTransferSource externalAccount= (CustomerExternalTransferSource) iterator
							.next();
					accountMessage += (!externalAccount.equals(pendingExternalAccounts.get(0)))? ", " : " ";
					accountMessage += externalAccount.getBankName() + " - " + maskutil.mask(externalAccount.getAccountNumber());
					messageId += "-" + externalAccount.getAccountNumber();
				}
				CustomerMessageContent message = 
						new CustomerMessageContent(messageId,
												   CustomerMessageContentType.INFORMATION, 
												   accountMessage, 
												   Utils.CUSTOMER_MESSAGE_INFORMATION_ICON);
				if (!userSession.getMessagesList().contains(message))
					userSession.getMessagesList().add(message);
			}
			model.put("pendingExternalAccounts", pendingExternalAccounts);
		}
		
		//Completed Applications messages
		if (!userSession.getCompletedApplications().isEmpty() && userSession.getNewNotifications() == null) {
			List<AccountApplication> completedApplicationList = userSession.getCompletedApplications();
			String applicationMessage = messageSource.getMessage("portal.completed.label", null, locale) + ":";
			String messageId = Utils.CUSTOMER_MESSAGE_COMPLETED_ACCOUNTS;
			for (Iterator iterator = completedApplicationList.iterator(); iterator.hasNext();) {
				AccountApplication accountApplication = (AccountApplication) iterator.next();
				if (!accountApplication.isSignatureCardComplete()) {
					applicationMessage += (!accountApplication.equals(completedApplicationList.get(0)))? ", " : " ";
					applicationMessage += accountApplication.getApplicationId();
					messageId += "-" + accountApplication.getAccountNumber();
				}
			}
			CustomerMessageContent message = 
					new CustomerMessageContent(messageId,
											   CustomerMessageContentType.INFORMATION, 
											   applicationMessage, 
											   Utils.CUSTOMER_MESSAGE_INFORMATION_ICON);
			message.setId(messageId);
			if (!userSession.getMessagesList().contains(message))
				userSession.getMessagesList().add(message);
		}
		
		//Show Alert Text message
		if (userSession.isShowAlertText()  && userSession.getNewNotifications() == null) {
			CustomerMessageContent message = 
					new CustomerMessageContent(Utils.CUSTOMER_MESSAGE_SHOW_ALERT,
											   CustomerMessageContentType.INFORMATION, 
											   messageSource.getMessage("portal.processing.label", null, locale), 
											   Utils.CUSTOMER_MESSAGE_INFORMATION_ICON);
			if (!userSession.getMessagesList().contains(message))
				userSession.getMessagesList().add(message);
		}

		// if
		// (userSession.getUserSessionType().equals(UserSessionType.USERNAME_RESERVATION))
		// {
		// addApplications(model, userSession);
		// model.put("applicationCancelationTriggersErase",
		// userSession.getIncompleteApplicationCount() == 1);
		// }
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_RENEWPERMID)) {
			/**
			 * Boolean to validate if the users PermId needs to be updated
			 */
			if (userSession.getRenewPermId()) {
				model.put("renewPermId", true);
			} else {
				model.put("renewPermId", false);
			}
		}
		if (userSession.hasVirginIslandAccount())
			model.put("hasVIAccount", true);

		if (accounts.size() == 0)
			model.put("noaccounts", true);

		//Save campaigns messages on session
		Map<String, List<MyMessagesCampaign>> campaignsMap = Utils.messageCampaign(userSession, wealthCodes, preferredCodes);
		
		if(userSession.getNewNotifications() == null) {
			getMessageContentFromCampaigns(campaignsMap.get(Utils.ALERT), userSession, Utils.CUSTOMER_MESSAGE_WARNING_ICON);
			getMessageContentFromCampaigns(campaignsMap.get(Utils.INFO), userSession, Utils.CUSTOMER_MESSAGE_INFORMATION_ICON);
			getMessageContentFromCampaigns(campaignsMap.get(Utils.UPDATE), userSession, Utils.CUSTOMER_MESSAGE_PUSH_ICON);
			getMessageContentFromCampaigns(campaignsMap.get(Utils.EVENTS), userSession, Utils.CUSTOMER_MESSAGE_CAMPAIGN_ICON);
		}

		final String popupCookieFlag = "MBSD3806"; //ENHA.20191018.popup.cookie.track.message

		//MBFM-62
		if (userSession.getUserAgentType() != UserAgentType.JSON) {

			boolean notExistsCookieEntitlement = notExistsCookieEntitlement(userSession);
			boolean existsCookieBpprDmp = existsCookieBpprDmp(request);

			if (notExistsCookieEntitlement && existsCookieBpprDmp) {
				createCookieEntitlement(userSession);
			}

			if (!existsCookieBpprDmp && notExistsCookieEntitlement) {
				model.put(popupCookieFlag, StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3806).toString());
				return new ModelAndView(strPortalView , model);
			}
        } else{
			model.put(popupCookieFlag, StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3806).toString());
		}
		//END MBFM-62

		//MBFIS-11
		String showOOBSEC =  (String) WebUtils.getSessionAttribute(request, "showOOBSEC");
		if (showOOBSEC != null) {
			model.put ("showOOBSec",Boolean.TRUE);
			WebUtils.setSessionAttribute(request, "showOOBSEC", null);
		}else {
			model.put ("showOOBSec",Boolean.FALSE);
		}
		// END MBFIS-11
		
		// CIBP-2700 No PFM for mobile version
		if (userSession.getUserAgentType() == UserAgentType.WEB
				|| userSession.getUserAgentType() == UserAgentType.IPAD) {
			List<String> portalMessages = customerService.getPortalMessages(userSession.getCustomerProfile().getTaxId(),
					Utils.getRSALanguage(request));
			if (portalMessages.size() > 0) {
				String msgString = "";

				for (int i = 0; i < portalMessages.size(); i++)
					msgString += portalMessages.get(i) + "<br><br>";

				model.put("portalmessages", msgString);
				if(userSession.getNewNotifications() == null) {
					//Portal messages saved in session
					CustomerMessageContent message = 
							new CustomerMessageContent(Utils.CUSTOMER_MESSAGE_SHOW_ALERT,
													   CustomerMessageContentType.INFORMATION, 
													   msgString, 
													   Utils.CUSTOMER_MESSAGE_INFORMATION_ICON);
					if (!userSession.getMessagesList().contains(message))
						userSession.getMessagesList().add(message);
				}
			}

			// Busca las campanas de mensajes para mostrarlas en la cajita
			if (userSession.getCustomerService().getGlobalEntitlement(EntitlementType.CAMPAIGNS)
					.getStatus() == EntitlementStatus.ENABLED) {
				
				boolean isEnglish = userSession.getLanguage().equalsIgnoreCase("en");
				model.put("alertCampaign", campaignsMap.get(Utils.ALERT));
				model.put("infoCampaign", campaignsMap.get(Utils.INFO));
				model.put("updateCampaign", campaignsMap.get(Utils.UPDATE));
				model.put("eventCampaign", campaignsMap.get(Utils.EVENTS));
				model.put("isenglish", isEnglish);

				// campanna wb agree cibp-2360
				SignupCampInfo campaignWBA = userSession.getCustomerService().getEnableSignupCampInfo(CampaignType.WBA);
				if (campaignWBA.getStatus() == CampaignStatus.ENABLE
						&& DateUtil.isBetweenDate(campaignWBA.getStartDate(), campaignWBA.getEndDate(), new Date())) {
					// no presentar campanna wba a iphone version.
					Boolean skipwba = false;
					if (WebUtils.getSessionAttribute(request, "skipwba") != null) {
						if (versionCookie == null
								|| (versionCookie != null && !"mobile".equalsIgnoreCase(versionCookie.getValue()))) {
							skipwba = (Boolean) WebUtils.getSessionAttribute(request, "skipwba");
						}
					} else if (versionCookie != null && "mobile".equalsIgnoreCase(versionCookie.getValue())) {
						skipwba = true;
					}

					if (!skipwba) {
						if (concurrencyBean.isConcurrencyAvailable()) {
							concurrencyBean.addConcurrency();
							List<SignupCampaign> listaCampannas = userSession.getCustomerSignupCampaigns();
							for (SignupCampaign signupCampaign : listaCampannas) {
								if (signupCampaign.getCampaignType().equals(CampaignType.WBA.toString())
										&& signupCampaign.getCampaignResult() == SignupCampaign.CAMPANNA_NO_VISTA) {
									return new ModelAndView("redirect:wbaStart");
								}
							}
						}
					}
				}

				SignupCampInfo campaignREGE = userSession.getCustomerService()
						.getEnableSignupCampInfo(CampaignType.REGE);
				if (campaignREGE.getStatus() == CampaignStatus.ENABLE
						&& DateUtil.isBetweenDate(campaignREGE.getStartDate(), campaignREGE.getEndDate(), new Date())) {
					Boolean skiprege = false;
					// no presentar campanna regE a iphone version.
					if (WebUtils.getSessionAttribute(request, "skiprege") != null) {
						if (versionCookie == null
								|| (versionCookie != null && !"mobile".equalsIgnoreCase(versionCookie.getValue()))) {
							skiprege = (Boolean) WebUtils.getSessionAttribute(request, "skiprege");
						}
					} else if (versionCookie != null && "mobile".equalsIgnoreCase(versionCookie.getValue())) {
						skiprege = true;
					}

					if (!skiprege) {
						SignupCampaign regeCampaign = null;
						List<SignupCampaign> signupCampaignInfoByProfileIdA = userSession.getCustomerSignupCampaigns();
						for (Iterator<SignupCampaign> iter = signupCampaignInfoByProfileIdA.iterator(); iter
								.hasNext();) {
							SignupCampaign camp = (SignupCampaign) iter.next();
							if (camp.getCampaignType().equals(CampaignType.REGE.toString())) {
								regeCampaign = camp;
								break;
							}
						}

						if (regeCampaign == null) {
							if (concurrencyBean.isConcurrencyAvailable()) {
								concurrencyBean.addConcurrency();
								if (haveRegEAccounts(accounts)) {
									return new ModelAndView("redirect:regestart");
								}
							}
						}
					}
				}

				boolean GuidedSetupStatus = userSession.getCustomerService()
						.getGlobalEntitlement(EntitlementType.GUIDED_SETUP).getStatus() == EntitlementStatus.ENABLED;

				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA242) && userSession.getCustomerProfile().getIsTransactional() != null
						&& userSession.getCustomerProfile().getIsTransactional().equalsIgnoreCase("N"))
				{
					GuidedSetupStatus = false;
				}

				if (GuidedSetupStatus) {
					// dsalas: deberia ingresar a este if solo si ya se ha
					// ingresado
					// en las partes de abajo, donde se verifica de que forma se
					// le
					// debe mostrar la camapania
					if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
						customerService.creationInicalPayeesGuidedSetud(userSession.getCustomerProfile(),
								bankingSession);// crea
						// los
						// comercios
						boolean canPerformPif;
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD792))
							canPerformPif = userSession.getInterbankFundingAllowed();
						else
							canPerformPif = customerService.canPerformPostInitialFunding(userSession.getAccounts());

						userSession.getGuidedSetupSession().setHasPIFguidedSetup(canPerformPif && customerService
								.pifAccountLimitReached(userSession.getCustomerProfile().getTaxId()) == false);
						boolean ebppStatus = userSession.getCustomerService()
								.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED
								&& this.getEBPPMessageSourceStatus();
						boolean paymentsStatus = userSession.getCustomerService()
								.getGlobalEntitlement(EntitlementType.BILLPAY).getStatus() == EntitlementStatus.ENABLED;
						userSession.getGuidedSetupSession().setHasPaymentsGuidedSetup(paymentsStatus);
						// Forzar la baja de ezstament para esconder la seccion de
						userSession.getGuidedSetupSession().setHasEBPPGuidedSetup(ebppStatus);
						// dsalas: si la campania es individual no sobreescriba
						// estos valores

						if (!userSession.getGuidedSetupSession().isIndividualguidedSetup()) {
							userSession.getGuidedSetupSession().setIndividualguidedSetup(false);
							userSession.getGuidedSetupSession()
							.setIndividualguidedSetupCampaignId(GuidedSetupInfo.ID_GUIDED_SETUP);
						} else {
							return goToIndividualCampaign(paymentsStatus, ebppStatus, model, userSession);
						}

					} else {
						// mpicado, se debe verificar si ya se paso o no por
						// esta
						// logica dentro de esta misma sesion
						if (!userSession.getGuidedSetupSession().isGuidedSetupMostrado()) {
							/*
							 * mpicado, esto es para saber el estado de EBPP en base de datos y en el
							 * archivo downtime, si uno de los dos es false no deberia estar habilitado EBPP
							 */
							boolean ebppStatus = userSession.getCustomerService()
									.getGlobalEntitlement(EntitlementType.EBILLS)
									.getStatus() == EntitlementStatus.ENABLED && this.getEBPPMessageSourceStatus();

							boolean paymentsStatus = userSession.getCustomerService()
									.getGlobalEntitlement(EntitlementType.BILLPAY)
									.getStatus() == EntitlementStatus.ENABLED;
							/*
							 * mpicado, cibp-2218, logica para verificar si hay que volver a mostrar el
							 * guided setup a clientes nuevos que la omitieron, le dieron ver luego o se les
							 * vencio la sesion
							 */
							// clientes con campania de clientes nuevos tienen
							// prioridad, aunque tengan configurada una
							// individual
							ViewCampaign vc = customerService.getViewCampaign(userSession.getCustomerProfile(),
									GuidedSetupInfo.ID_GUIDED_SETUP);
							if (vc != null) {
								if (vc.getStatus().equals(ViewCampaign.OMITIDA)
										|| vc.getStatus().equals(ViewCampaign.VER_LUEGO)) {
									userSession.getGuidedSetupSession().setGuidedSetupMode(true);
									userSession.getGuidedSetupSession().setHasPIFguidedSetup(
											customerService.canPerformPostInitialFunding(userSession.getAccounts())
											&& customerService.pifAccountLimitReached(
													userSession.getCustomerProfile().getTaxId()) == false);
									userSession.getGuidedSetupSession().setHasEBPPGuidedSetup(ebppStatus);
									userSession.getGuidedSetupSession().setHasPaymentsGuidedSetup(paymentsStatus);
									userSession.getGuidedSetupSession().setIndividualguidedSetup(false);
									userSession.getGuidedSetupSession()
									.setIndividualguidedSetupCampaignId(GuidedSetupInfo.ID_GUIDED_SETUP);
									CookieUtils.addCookie(response, -1, "sessionError", "sessionError");
									return new ModelAndView(strPortalView, model);
								}
							}
							List<ActiveCampaignView> l = customerService
									.getActiveCampaignByType(CampaignType.GUIDED_SETUP);
							List<ActiveCampaignCustomerView> campaIndivFile = null;

							final ModelAndView modelAndView = new ModelAndView(strPortalView, model);//model & view result

							/*
							Valida campanias completas, dsalas: tienen segunda
							prioridad, por encima de las individuales no completas.
							*/
							for (Iterator<ActiveCampaignView> iter = l.iterator(); iter.hasNext();) {
								ActiveCampaignView c = (ActiveCampaignView) iter.next();
								String t = c.getString("type");
								if (t != null && t.equals(CampaignGuidedSetupType.COMPLETA.toString())) {
									ViewCampaign vcc = customerService.getViewCampaign(userSession.getCustomerProfile(),
											c.getId());
									boolean mostrarCompleta = false;
									if (c.getFileCampaign() == null || c.getCampaignName().trim().length() == 0) {// la
										// camapania
										// no
										// esta
										// dirigida
										// a
										// usuarios
										// especificos
										mostrarCompleta = true;
									} else {// la camapania esta dirigida a
										// usuarios
										// especificos
										if (campaIndivFile == null)
											campaIndivFile = userSession.getCustomerService()
											.getActiveCustCampaign(userSession.getCustomerProfile().getTaxId());

										for (Iterator<ActiveCampaignCustomerView> iterc = campaIndivFile
												.iterator(); iterc.hasNext();) {
											ActiveCampaignCustomerView cc = (ActiveCampaignCustomerView) iterc.next();
											if (cc.getId().getCampaignId().equals(c.getId())) {
												mostrarCompleta = true;
												break;
											}
										}
									}
									if (mostrarCompleta && (vcc == null || (vcc.getStatus().equals(ViewCampaign.OMITIDA)
											|| vcc.getStatus().equals(ViewCampaign.VER_LUEGO)))) {
										userSession.getGuidedSetupSession().setGuidedSetupMode(true);
										userSession.getGuidedSetupSession().setHasPIFguidedSetup(
												customerService.canPerformPostInitialFunding(userSession.getAccounts())
												&& customerService.pifAccountLimitReached(
														userSession.getCustomerProfile().getTaxId()) == false);
										userSession.getGuidedSetupSession().setHasEBPPGuidedSetup(ebppStatus);
										userSession.getGuidedSetupSession().setHasPaymentsGuidedSetup(paymentsStatus);
										userSession.getGuidedSetupSession().setIndividualguidedSetup(false);
										userSession.getGuidedSetupSession()
										.setIndividualguidedSetupCampaignId(c.getId());
										CookieUtils.addCookie(response, -1, "sessionError", "sessionError");

										return modelAndView;
									}

								}
							}
							// bgarcia, CIBP-2218
							// se muestra si no tiene configurada una campania
							// de
							// clientes nuevos o individual-completa
							boolean Visito = false;

							for (Iterator<ActiveCampaignView> iter = l.iterator(); iter.hasNext();) {
								ActiveCampaignView c = (ActiveCampaignView) iter.next();
								if (!customerService.isViewedCampaign(userSession.getCustomerProfile(), c)
										&& !c.getId().equals(GuidedSetupInfo.ID_GUIDED_SETUP)) {
									String type = c.getString("type");
									if (c.getFileCampaign() == null || c.getFileCampaign().trim().length() == 0)// para
										// todos
										// los
										// clientes
										Visito = true;
									else {// campania de archivo
										if (campaIndivFile == null)
											campaIndivFile = userSession.getCustomerService()
											.getActiveCustCampaign(userSession.getCustomerProfile().getTaxId());

										for (Iterator<ActiveCampaignCustomerView> iterc = campaIndivFile
												.iterator(); iterc.hasNext();) {
											ActiveCampaignCustomerView cc = (ActiveCampaignCustomerView) iterc.next();
											if (cc.getId().getCampaignId().equals(c.getId())) {
												Visito = true;
												break;
											}
										}
									}
									if (Visito) {
										userSession.getGuidedSetupSession()
										.setIndividualguidedSetupCampaignId(c.getId());
										userSession.getGuidedSetupSession().setIndividualguidedSetup(true);
										userSession.getGuidedSetupSession().setGuidedSetupMode(true);
										userSession.getGuidedSetupSession().setIndividualGuidedSetupCampaignType(type);

										return goToIndividualCampaign(paymentsStatus, ebppStatus, model, userSession);
									}
								}
							}

							// fin del for
						} // fin del if de isGuidedSetupMostrado
					}
				} else {
					// bgarcia esto nos garantizamos que si estando trabajando
					// en MB
					// se habilita la opcion de GuidedSetup , no la muestre
					// hasta el
					// proximo login exitoso
					userSession.getGuidedSetupSession().setGuidedSetupMostrado(true);
					userSession.getGuidedSetupSession().setGuidedSetupMode(false);

				}
			}
		}

		// FFEIC
		String header = request.getHeader("user-agent");
		CustomerDevices cd = null;

		if (!userSession.isDeviceChecked()) {
			boolean hasalert = false;
			AlertsEntitlement ent = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));
			if (ent != null && ent.getCustomerEntitlement() != null) {
				Set<AlertType> alerts = ent.getAlerts();
				if (alerts.contains(AlertType.NEW_DEVICE_EMAIL) || alerts.contains(AlertType.NEW_DEVICE_SMS)
						|| alerts.contains(AlertType.NEW_DEVICE_PUSH)) {
					hasalert = true;
				}
			}
			cd = DeviceUtils.getCustomerDevice(header);
			if (hasalert && (!cd.equals(CustomerDevices.PERSONAL) && !cd.equals(CustomerDevices.IPAD))
					|| ((cd.equals(CustomerDevices.PERSONAL) || cd.equals(CustomerDevices.IPAD))
							&& versionCookie != null && "mobile".equalsIgnoreCase(versionCookie.getValue()))) {

				Cookie deviceCookie = null;
				Cookie[] cookies = request.getCookies();
				if (cookies != null) {
					for (int i = 0; i < cookies.length; i++) {
						if (cookies[i].getName().equalsIgnoreCase(DeviceUtils.DEVICE_TOKEN_COOKIE)) {
							deviceCookie = cookies[i];
							break;
						}
					}
				}

				String userAgent = null;
				try {
					userAgent = profile.getSignonStats().getString(deviceCookie.getValue());
					if (!userAgent.equals(header)) {
						throw new Exception("NEW USER-AGENT");
					}
				} catch (Exception e) {

					this.customerService.sendNewDeviceConfirmation(profile,
							userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
							userSession.getPreferredLanguage());

					if (profile.getSignonStats().getAttributeData() == null) {
						profile.getSignonStats().resetProperties();
					}
					String token;
					if (deviceCookie != null && deviceCookie.getValue() != null) {
						token = deviceCookie.getValue();
					} else {
						token = RandomUtils.generateRandomUniqueString();
						CookieUtils.addCookie(response, 60 * 60 * 24 * 365, DeviceUtils.DEVICE_TOKEN_COOKIE, token);
					}
					if (userAgent != null) {
						profile.getSignonStats().removeProperty(token);
					}
					profile.getSignonStats().putString(token, header);
					CustomerFacade.getInstance().updateCustomerSignonStats(profile.getSignonStats(), userSession);

				}
				userSession.setDeviceChecked(true);
			}
		}
		// FIN FFEIC


		if (userSession.getUserAgentType() == UserAgentType.JSON) {
			model.put("isComercialCustomer", userSession.isComercialCustomer());
			model.put("isWealth", userSession.isWealth(this.wealthCodes, this.preferredCodes));
			model.put("flagMBCPremiumBanking", false);
			model.put("showMarketplace", showMarketplace(userSession));
			
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_PREMIUM_BANKING_LIMITS_PER_SEGMENT)) {
				model.put("flagMBCPremiumBanking", true);
				model.put("isPremiumBanking", userSession.isPremiumBanking(premiumBankingCodes, wealthCodes, preferredCodes));
			}
			
			model.put("flagMBCA201", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA201));
			
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA201)) {
				model.put("isTransactional", AccountUtils.getIsTransactional(profile).equalsIgnoreCase("Y") ? true : false);
			}
			
			if (Utils.isPushPhase2Enabled()) {
				List<ActiveCampaignView> campaigns = customerService.getActiveCampaignByType(CampaignType.PUSH_ALERTS);
				for (Iterator<ActiveCampaignView> iter = campaigns.iterator(); iter.hasNext();) {
					ActiveCampaignView c = iter.next();
					ViewCampaign vc = customerService.getViewCampaign(profile, c.getId());
					if (vc == null || (vc != null && vc.getStatus().equals(ViewCampaign.OMITIDA))) {
						model.put("showAlertsSplash", true);
						break;
					}
				}
			}
		}		
		
		OOBInterruptionForm.applyForInterruptionPageMobileVersion(userSession, model);

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OUTREACH)) {
			if (userSession.isElegibleToOutreach() && !userSession.isAlreadyShownOutreach()) {
				if (userSession.getUserAgentType() == UserAgentType.JSON) {
					model.put("outreach", true);
					return new ModelAndView(strPortalView, model);
				}
			}else if(userSession.isElegibleToOutreach() && userSession.isAlreadyShownOutreach() && (!userSession.getOutReachConfirmation().equals(""))){
				userSession.setStatusmessage(this.messageSource.getMessage(userSession.getOutReachConfirmation(),null, new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang )));
				userSession.setOutReachConfirmation("");
			}
		}
		CookieUtils.addCookie(response, -1, "sessionError", "sessionError");
		model.put("massCompromise", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE665));
		model.put("showMenuRSAQuestions", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3274));
		model.put("isOOBEnrolled", userSession.getOobEnroll());

		return new ModelAndView(strPortalView, model);

	}

	private ModelAndView goToIndividualCampaign(boolean paymentsStatus, boolean ebppStatus, Map<String, Object> model,
			UserSession userSession) {

		String type = userSession.getGuidedSetupSession().getIndividualGuidedSetupCampaignType();
		CustomerServices customerService = userSession.getCustomerService();
		String campania = "";

		// WebUtils.setSessionAttribute(request, "userSession", userSession);
		if (type.equals(CampaignGuidedSetupType.PIF.toString())) {
			// CCA CLIENTS CAN PERFORM PIF
			boolean hasCCA = userSession.hasCCA();
			model.put("addPifAccountsCCAEnabled", hasCCA);

			boolean canPerformPif;
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD792))
				canPerformPif = userSession.getInterbankFundingAllowed();
			else
				canPerformPif = customerService.canPerformPostInitialFunding(userSession.getAccounts());
			boolean limitReached = customerService.pifAccountLimitReached(userSession.getCustomerProfile().getTaxId());
			boolean hasPiffBypass = userSession.hasPifBypassAccount(this.bypassAccounts);
			if (limitReached) {
				userSession.getGuidedSetupSession().setHasPIFguidedSetup(false);
			} else {
				if (hasCCA || canPerformPif || hasPiffBypass) {
					userSession.getGuidedSetupSession().setHasPIFguidedSetup(true);
				} else {
					userSession.getGuidedSetupSession().setHasPIFguidedSetup(false);
				}
			}
			if (userSession.getGuidedSetupSession().isHasPIFguidedSetup()) {
				model.put("gsHasPIF", userSession.getGuidedSetupSession().isHasPIFguidedSetup());
				model.put("gsPage", "gsPIF");
				campania = "gsPIF";
			} else {
				userSession.getGuidedSetupSession().setIndividualguidedSetupCampaignId("");
				userSession.getGuidedSetupSession().setIndividualguidedSetup(false);
				userSession.getGuidedSetupSession().setGuidedSetupMode(false);
			}
		}
		userSession.getGuidedSetupSession().setHasEBPPGuidedSetup(ebppStatus);
		userSession.getGuidedSetupSession().setHasPaymentsGuidedSetup(paymentsStatus);
		model.put("gsHasEBPP", userSession.getGuidedSetupSession().isHasEBPPGuidedSetup());
		model.put("gsHasPayments", userSession.getGuidedSetupSession().isHasPaymentsGuidedSetup());

		if (type.equals(CampaignGuidedSetupType.ALERTA.toString())) {
			model.put("gsPage", "gsSMS");
			campania = "gsSMS";
		}
		if (type.equals(CampaignGuidedSetupType.ESTATEMENT_EBILL.toString())) {
			if (ebppStatus) {
				model.put("gsPage", "gsEbills");

				campania = "gsEbills";
			} else {
				userSession.getGuidedSetupSession().setIndividualguidedSetupCampaignId("");
				userSession.getGuidedSetupSession().setIndividualguidedSetup(false);
				userSession.getGuidedSetupSession().setGuidedSetupMode(false);
			}
		}
		if (type.equals(CampaignGuidedSetupType.PAGO.toString())) {
			if (paymentsStatus) {
				model.put("gsPage", "gsPayments");

				campania = "gsPayments";
			} else {
				userSession.getGuidedSetupSession().setIndividualguidedSetupCampaignId("");
				userSession.getGuidedSetupSession().setIndividualguidedSetup(false);
				userSession.getGuidedSetupSession().setGuidedSetupMode(false);
			}
		}
		// IF DOWNTIME OR PIF VALIDATION THEN CAMPAIGNS ARE NOT SHOWN
		// APP GOES TO MAIN PAGE INSTEAD
		if (campania.equals("")) {
			userSession.getGuidedSetupSession().setGuidedSetupMostrado(true);
			userSession.getGuidedSetupSession().setGuidedSetupMode(false);
			// return new ModelAndView("redirect:personalinformation");
			return new ModelAndView(strPortalView, model);
		} else {
			// CIBP-2353 Statistics
			model.put("guidedSetupId", userSession.getGuidedSetupSession().getIndividualguidedSetupCampaignId());
			return new ModelAndView(campania, model);
		}
	}
	

	
	private boolean haveRegEAccounts(List<TVFrontendAccount> accounts) {
		if (accounts != null && accounts.size() > 0) {
			for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount name = (TVFrontendAccount) iterator.next();
				String accountNumberSuffix = name.getAccountNumberSuffix();
				if (!name.getHide()
						&& getCampaignGenericBean().getCampaignProductsId().contains(name.getAccountProductId())
						&& !getCampaignGenericBean().getInvalidSufix().contains(accountNumberSuffix)) {
					if (name.getSubtype().equals("INS") || name.getSubtype().equals("SEC")) {
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE675)) {
							if(!this.insuranceRemoved.contains(name.getProductDefinition().getProductId())){
								continue;
							}
						}
						else{
							continue;
						}					
					} else if (name.getSubtype().equals("PRM")) {
						continue;
					} else {
						if (!name.getOdLimit().equals(CampaignGenericBean.ACTIVE_ODLIMIT))
							return true;
					}
				}
			}
		}
		return false;
	}

	protected void addAdditionalModels(HttpServletRequest request, Map<String, Object> model) throws Exception {

		if (WebUtils.getSessionAttribute(request, "userSession") instanceof UserSession) {
			UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

			// TODO poner un if para cuando solo queramos actulaizar las cuentas
			// if
			// (userSession.getUserSessionType().equals(UserSessionType.USERNAME_RESERVATION))
			// {
			// addApplications(model, userSession);
			// model.put("cannotOpenAdditionalAccounts", true);
			// return;
			// }

			CustomerProfile profile = userSession.getCustomerProfile();
			//BankingSession bankingSession = userSession.getBankingSession();
			// if (bankingSession == null) {
			// // Este cliente pudo loguearse sucessfully pero no tiene un banking
			// // session valido
			// // (La cuenta no existe, se cerro etc. Le presentamos lo mismo que
			// // un username reservation.
			// addApplications(model, userSession);
			// return;
			// }

			List<TVFrontendAccount> accounts = userSession.getAccounts();
			if (accounts == null)
				throw new ProgrammingError("Unable to get user accounts");

			if (accounts != null && accounts.size() > 0) {
				List<TVFrontendAccount> portalAccounts = new LinkedList<TVFrontendAccount>();
				List<TVFrontendAccount> secins = new LinkedList<TVFrontendAccount>();
				List<TVFrontendAccount> programs = new LinkedList<TVFrontendAccount>();
				for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
					TVFrontendAccount name = iterator.next();
					if (!name.getHide()) {
						if (name.getSubtype().equals("INS") || name.getSubtype().equals("SEC")){
							if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE675)) {
								if(!this.insuranceRemoved.contains(name.getProductDefinition().getProductId())){
									secins.add(name);
								}
							}
							else{
								secins.add(name);
							}
						}
						else if (name.getSubtype().equals("PRM"))
							programs.add(name);
						else
							portalAccounts.add(name);
					}
				}

				if (portalAccounts != null && portalAccounts.size() > 0)
					model.put("accounts", portalAccounts);
				if (secins != null && secins.size() > 0)
					model.put("secins", secins);
				if (programs != null && programs.size() > 0)
					model.put("programs", programs);
				model.put("customername", profile.getFirstName());

				Calendar birthday = Calendar.getInstance();
				Calendar today = Calendar.getInstance();
				birthday.setTime(profile.getBirthDate());
				if (birthday.get(Calendar.MONTH) == today.get(Calendar.MONTH)
						&& birthday.get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH)) {
					model.put("birthday", true);
				}

				addDelinquencyFlags(model, userSession);

			}

			// if (profile.isPendingAccountApplication()) {
			// addApplications(model, userSession);
			// } else {
			// model.put("canOpenAdditionalAccounts", true);
			// model.put("canCompleteAdditionalAccounts", true);
			// }

			if (profile.isPendingExternalAccountValidation()) {

				model.put("pendingExternalAccounts", userSession.getPendingExternalAccountsValidation());
				for (Iterator<CustomerExternalTransferSource> iterator = userSession.getPendingExternalAccountsValidation()
						.iterator(); iterator.hasNext();) {
					CustomerExternalTransferSource act = iterator.next();

				}
			}

			return;
		}
	}

	// private void addApplications(Map<String, Object> model, UserSession
	// userSession) {
	// AccountApplications accountApplications =
	// userSession.getAccountApplications(true);
	// List<AccountApplication> completedApplications2 =
	// accountApplications.getCompletedApplications();
	//
	// for (AccountApplication accountApplication : completedApplications2) {
	// if (!accountApplication.isAccountAvailableInBackend()) {
	// model.put("completedNotAvailableApplications", true);
	// break;
	// }
	//
	// }
	//
	// model.put("canOpenAdditionalAccounts",
	// userSession.isCanOpenAdditionalAccounts());
	// model.put("canCompleteAdditionalAccounts",
	// userSession.isCanCompleteAdditionalAccounts());
	//
	// model.put("completedApplications", completedApplications2);
	// List<AccountApplication> aps =
	// accountApplications.getPendingApplications();
	// int pending = 0, others = 0;
	// for (Iterator<AccountApplication> iterator = aps.iterator();
	// iterator.hasNext();) {
	// AccountApplication accountApplication2 = (AccountApplication)
	// iterator.next();
	//
	// if (!accountApplication2.isIncomplete()) {
	// model.put("showAlertText", true);
	// model.put("showNotice", true);
	// }
	//
	// }
	//
	// model.put("pendingApplications",
	// accountApplications.getPendingApplications());
	// }

	private void addDelinquencyFlags(Map<String, Object> model, UserSession userSession) {

		if (userSession.getAccounts() != null) {
			for (Iterator<TVFrontendAccount> iterator = userSession.getAccounts().iterator(); iterator.hasNext();) {
				TVFrontendAccount type = (TVFrontendAccount) iterator.next();
				if (type.isDelinquency()) {
					if (type.getDelinquencyType() == DelinquencyType.PROMISE)
						model.put("promise", true);
					if (type.getDelinquencyType() == DelinquencyType.SKIP)
						model.put("skip", true);
					if (type.getDelinquencyType() == DelinquencyType.DPD_1_29)
						model.put("pastdue1", true);
					if (type.getDelinquencyType() == DelinquencyType.DPD_30_59)
						model.put("pastdue2", true);
					if (type.getDelinquencyType() == DelinquencyType.DPD_60_PLUS)
						model.put("pastdue3", true);
					if (type.getDelinquencyType() == DelinquencyType.CHARGE_OFF)
						model.put("chargeoff", true);
					if (type.getDelinquencyType() == DelinquencyType.OVERLIMIT)
						model.put("overlimit", true);

				}
			}
		}
	}
	
	private void getMessageContentFromCampaigns(List<MyMessagesCampaign> list, UserSession userSession, String iconUrl ) {
		for (MyMessagesCampaign notification : list) {
			String campaignMessage = (userSession.getLanguage().equals("en"))? notification.getEnglishBody() : notification.getSpanishBody();
			campaignMessage += "</br>";
			if (notification.getLinkable()) {
				campaignMessage += (userSession.getLanguage().equals("en"))? "<a class='message_link' href='" + notification.getEnglishLink() + "' target='_blank'>" + notification.getEnglishAction() + "</a>" : "<a class='message_link' href='" + notification.getSpanishLink() + "' target='_blank'>" + notification.getSpanishAction() + "</a>";
			} else {
				campaignMessage += (userSession.getLanguage().equals("en"))? notification.getEnglishAction() : notification.getSpanishAction();
			}
			
			//TODO: veRIFICAR LA FECHA
			CustomerMessageContent message = 
					new CustomerMessageContent(notification.getCampaign().getId(),
											   CustomerMessageContentType.CAMPAIGN, 
											   campaignMessage, iconUrl,
											   notification.getCampaign().getApprovalDate());
			message.setId(notification.getCampaign().getId());
			if (!userSession.getMessagesList().contains(message))
				userSession.getMessagesList().add(message);
		}
	}
	
	/**
	 * This method shows a banner at the top of the MB home screen
	 * If the client has accounts of other banks pending confirmation, the confirmation is by initial deposit.
	 * If the customer confirms the Other Bank Account or Eliminates the Pending Confirmation the message disappears
	 * @param userSession The User Session.
	 * @return boolean true if exist other bank account
	 */
	public boolean hasPendingPifExternalAccounts(UserSession userSession) {
		
		// result, of pending accounts.
		boolean hasPendingAccounts = false;
		
		if (userSession.getCustomerProfile() != null
			&& !GenericValidator.isBlankOrNull(userSession.getCustomerProfile().getTaxId())) {
			
			// External accounts.
			final List<PIFExternalAccount> pifExternalAccounts
			    = userSession.getCustomerService().getPIFExternalAccounts(userSession.getCustomerProfile().getTaxId());
			
			for (PIFExternalAccount a : pifExternalAccounts) {
				if (a.getAccountStatus().equals(PIFAccountStatusTypes.PENDING)) {
					
					// Has pending accounts.
					hasPendingAccounts = true;
					break;
				}
			}
		}
		
		// No pending accounts.
		return hasPendingAccounts;
	}


	// Fixed downtime from downtime.properties while you are in MiBanco
	/**
	 * CIBP-1767
	 * 
	 * @return true si debe presentar el downtime de MiBanco
	 */
	private boolean getDowntimeMessageSourceStatus() {
		String isdown = messageSource.getMessage("downPage", null, "UP", Locale.ENGLISH);
		if (isdown != null && isdown.equalsIgnoreCase("DOWN"))
			return true;
		else
			return false;
	}

	// Fixed downtime from downtime.properties while you are in MiBanco
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	// Fixed downtime from downtime.properties while you are in MiBanco
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @return the concurrencyBean
	 */
	public CampaignConcurrencyBean getConcurrencyBean() {
		return concurrencyBean;
	}

	/**
	 * @param concurrencyBean
	 *            the concurrencyBean to set
	 */
	public void setConcurrencyBean(CampaignConcurrencyBean concurrencyBean) {
		this.concurrencyBean = concurrencyBean;
	}

	/**
	 * @return the campaignGenericBean
	 */
	public CampaignGenericBean getCampaignGenericBean() {
		return campaignGenericBean;
	}

	/**
	 * @param campaignGenericBean
	 *            the campaignGenericBean to set
	 */
	public void setCampaignGenericBean(CampaignGenericBean campaignGenericBean) {
		this.campaignGenericBean = campaignGenericBean;
	}

	public void setWealthCodes(String wealthCodes) {
		this.wealthCodes = wealthCodes;
	}

	public String getWealthCodes() {
		return wealthCodes;
	}
	
	public void setPremiumBankingCodes(String premiumBankingCodes) {
		this.premiumBankingCodes = premiumBankingCodes;
	}

	public String getPremiumBankingCodes() {
		return premiumBankingCodes;
	}

	public void setPreferredCodes(String preferredCodes) {
		this.preferredCodes = preferredCodes;
	}

	public String getPreferredCodes() {
		return preferredCodes;
	}

	public boolean CrearComercios(UserSession userSession, String id, String billingAct, String nickname,
			HttpServletRequest request) {

		GlobalPayee global = PaymentsFacade.getInstance().getGlobalPayeeById(new Integer(id), userSession);
		if (global == null)
			return false;

		PaymentsFacade.getInstance().getGlobalPayeePreference(global, userSession);

		boolean isHighRiskPayee = false;
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1701) && global.getPreference() != null) {
			isHighRiskPayee = global.getPreference().getHighRisk();
		}

		// CIBP-1985 nulls al final relacionado a los campos de employee name,
		// employee id, routing number y employee acct# de telenomina
		AddPayeeResponse res = userSession.getCustomerService().addCustomerPayeeWithoutEbillWithErrorDescription(
				userSession.getCustomerProfile(), userSession.getBankingSession(), global, billingAct, nickname,
				Utils.generateCustomerInteractionEvent(request), null, null, null, null, false, isHighRiskPayee);

		if (res.getErrorCode() != 0)
			return false;

		return true;

	}

	private void createMobileCashEntitlement(UserSession userSession) {
		CustomerEntitlement entitlement = new CustomerEntitlement();
		entitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		entitlement.setGlobalEntitlement(customerService.getGlobalEntitlement(EntitlementType.MOBILE_CASH));
		entitlement.setType(EntitlementType.MOBILE_CASH);
		entitlement.setCustomerProfile(userSession.getCustomerProfile());
		entitlement.putString("STATUS", EntitlementStatus.ENABLED.toString());
		customerService.addEntitlement(userSession.getCustomerProfile(), entitlement);
	}

	/*
	 * -------------------------------------Como identificar tipo de
	 * CCA---------------------------------------------------- VISA �comienza
	 * con 4549 AMEX �comienza con 0377, 3773, 3778, 03778 Master Card �comienza
	 * con 5310 -----------------------------------------Account Class que bajan
	 * en boveda------------------------------------------------ CARD = Tarjeta
	 * DEP = Deposito CDA = CD/IRA INS = Insurance LOAN = Prestamo PFC = Payroll
	 * Card REW = ? SEC = Securities
	 * -------------------------------------------Account Type por Account Class
	 * ------------------------------------------------ Account Class Account
	 * Type CARD CCA (Credit CDA DEP (Deposit) DEP INT DEP ATH (ATH Card) DEP
	 * CDA (CD/IRA) DEP IDA (Deposit) DEP PFC (Payroll Card) INS INS LOAN CLA
	 * (Credit Line) LOAN LEA (Leasing) LOAN ILA LOAN IPF LOAN RVA (Revolving
	 * Line) LOAN MLA (Mortgage) PFC DEP REW PRM SEC SEC
	 * ------------------------
	 * ---------------------------------------------------------------------
	 */

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}
	
	/**
	 * This method checks if user has Commercial / Retail Tortola (BVI) or Non-Transactional account
	 * to show or hide Credit Card Market Place Menu Section on Mobile
	 * @param userSession the user session
	 * @return boolean true if customer must have access to the marketplace
	 */
	private boolean showMarketplace(UserSession userSession) {
		
		boolean showMarketplace = false;
		
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)
				&& userSession != null
				&& userSession.getCustomerProfile() != null
				&& !"N".equalsIgnoreCase(AccountUtils.getIsTransactional(userSession.getCustomerProfile()))
				&& !userSession.isComercialCustomer()
				&& !userSession.hasBritishVirginIslandAccount()
				&& ArrayUtils.contains(marketplaceCountries, userSession.getCountry())) {
			showMarketplace = true;
		}
		
		return showMarketplace;
		
	}

	public boolean isPfmAutomaticEntitlementCreation() {
		return pfmAutomaticEntitlementCreation;
	}

	public void setPfmAutomaticEntitlementCreation(boolean pfmAutomaticEntitlementCreation) {
		this.pfmAutomaticEntitlementCreation = pfmAutomaticEntitlementCreation;
	}

	public String getDemoUrl_en() {
		return demoUrl_en;
	}

	public void setDemoUrl_en(String demoUrl_en) {
		this.demoUrl_en = demoUrl_en;
	}

	public String getDemoUrl_es() {
		return demoUrl_es;
	}

	public void setDemoUrl_es(String demoUrl_es) {
		this.demoUrl_es = demoUrl_es;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public String getBypassAccounts() {
		return bypassAccounts;
	}

	public void setBypassAccounts(String bypassAccounts) {
		this.bypassAccounts = bypassAccounts;
	}

	public String getIraProductIds() {
		return iraProductIds;
	}

	public void setIraProductIds(String iraProductIds) {
		this.iraProductIds = iraProductIds;
	}

	public String getCaeProductIds() {
		return caeProductIds;
	}

	public void setCaeProductIds(String caeProductIds) {
		this.caeProductIds = caeProductIds;
	}

	public String getInsuranceRemoved() {
		return insuranceRemoved;
	}

	public void setInsuranceRemoved(String insuranceRemoved) {
		this.insuranceRemoved = insuranceRemoved;
	}

	public String getNonTransEnabledEstmts() {
		return nonTransEnabledEstmts;
	}

	public void setNonTransEnabledEstmts(String nonTransEnabledEstmts) {
		this.nonTransEnabledEstmts = nonTransEnabledEstmts;
	}
	
	public String getNonTransEnabledInterestLetters() {
		return nonTransEnabledInterestLetters;
	}

	public void setNonTransEnabledInterestLetters(String nonTransEnabledInterestLetters) {
		this.nonTransEnabledInterestLetters = nonTransEnabledInterestLetters;
	}
	
	public String getPingfederateSsoUrl() {
		return pingfederateSsoUrl;
	}

	public void setPingfederateSsoUrl(String pingfederateSsoUrl) {
		this.pingfederateSsoUrl = pingfederateSsoUrl;
	}

	public String getPingfederateSsoEndpoint() {
		return pingfederateSsoEndpoint;
	}

	public void setPingfederateSsoEndpoint(String pingfederateSsoEndpoint) {
		this.pingfederateSsoEndpoint = pingfederateSsoEndpoint;
	}

	public String getPingfederateSsoPartnerUrl() {
		return pingfederateSsoPartnerUrl;
	}

	public void setPingfederateSsoPartnerUrl(String pingfederateSsoPartnerUrl) {
		this.pingfederateSsoPartnerUrl = pingfederateSsoPartnerUrl;
	}

	/**
     * @param countries the allowed countries to use the Marketplace
     */
    public void setMarketplaceCountries(String[] countries) {
        this.marketplaceCountries = countries;
    }
	
	public ModelAndView removeNonPremiaSession(HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> model = new HashMap<>();

		if (WebUtils.getSessionAttribute(request, "userSession") instanceof UserSession) {
			UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			userSession.setNonPremiaState(null);
		}

		return new ModelAndView("text", model);
	}

	/**
	 * This method checks if user the cookie bppr_dmp exist
	 *
	 * @param request
	 * @return boolean
	 */
	public boolean existsCookieBpprDmp(HttpServletRequest request) {
		final String cookieBanner = "bppr_dmp"; //string cookie banner
		final Cookie cookieBannerCookie = CookieUtils.getCookie(request , cookieBanner); //cookie banner value
		return Objects.nonNull(cookieBannerCookie) && StringUtils.isNotBlank(cookieBannerCookie.getValue());
	}

	/**
	 * This method checks if user has entitlement type cookie
	 *
	 * @param userSession
	 * @return boolean
	 */
	public boolean notExistsCookieEntitlement(UserSession userSession) {
		CustomerEntitlement ent = userSession.getCustomerProfile().getEntitlementByType(EntitlementType.COOKIES);
		return Objects.isNull(ent);
	}

	/**
	 * This method create entitlement cookie
	 *
	 * @param userSession
	 */
	public void createCookieEntitlement(UserSession userSession) {
		CookieEntitlement cookieEntitlement = new CookieEntitlement(Boolean.TRUE.toString());
		userSession.getCustomerService().getCustomerProfileService()
				.addEntitlement(userSession.getCustomerProfile() , cookieEntitlement.getCustomerEntitlement());
	}
}
