package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.services.requests.adaptiveauth.RsaChallengeRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceRequest;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.oob.CustomerOOBChallengeType;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.web.form.bean.ChangeOOBBean;
import evertec.cibp.web.form.bean.ChangeOOBBean.ACTION;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.NavigationSequenceUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

// Add AbstractWizardFormController capabilities 
public class ChangeOOBInfoForm extends AbstractWizardFormController {
	/** Logger */
	protected final Log log = LogFactory.getLog(getClass());
	static final String USERSESSION = "userSession";

	// init finish var to force isFinish() method
	private boolean finish = false;

	// FIN FFEIC

	public ChangeOOBInfoForm() {
		super();
		setCommandClass(ChangeOOBBean.class);
		setCommandName("changeOOB");
		setPages(new String[] { "changeOOB" });

	}

	@Override
	/**
	 * This method validate the next functionality
	 */
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		ChangeOOBBean bean = (ChangeOOBBean) command;
		ChangeOOBBean.ACTION action = bean.getAction();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);

		finish = false;

		List<ASK_FOR_OOB> listNavOOB = new ArrayList<>();

		// add the navigation to CHANGE_OOB in cancel case
		listNavOOB.add(0, ASK_FOR_OOB.CHANGE_OOB);

		if (action != null) {

			// select the next steep after OOB Challenge
			listNavOOB = selectNavOOB(listNavOOB, action);

			// add the challenge OOB steep navigation is optional
			if (!userSession.isOobAuthenticated()) {
				listNavOOB.add(0, ASK_FOR_OOB.OOB_CHALLENGE);
			}

			finish = true;

		}

		userSession.setTasksListOOB(listNavOOB);

		WebUtils.setSessionAttribute(request, USERSESSION, userSession);
	}

	/*
	 * This method select and fill the next steep to navigate after optional OOB
	 * challenge
	 */
	private List<ASK_FOR_OOB> selectNavOOB(List<ASK_FOR_OOB> listNavOOB, ACTION action) {

		switch (action) {

		case EDIT_PRIMARY_PHONE:
			listNavOOB.add(0, ASK_FOR_OOB.EDIT_PRIMARY_PHONE);
			break;
		case EDIT_BACKUP_PHONE:
			listNavOOB.add(0, ASK_FOR_OOB.EDIT_BACKUP_PHONE);
			break;
		case EDIT_CHALLENGE_TYPE:
			listNavOOB.add(0, ASK_FOR_OOB.EDIT_CHALLENGE_TYPE);
			break;
		case EDIT_CHALLENGE_FREQUENCY:
			listNavOOB.add(0, ASK_FOR_OOB.EDIT_CHALLENGE_FREQUENCY);
			break;
		case GENERATE_RECOVERY_CODE:
			listNavOOB.add(0, ASK_FOR_OOB.GENERATE_RECOVERY_CODE);
			break;
		case DEACTIVATE_2_STEP:
			listNavOOB.add(0, ASK_FOR_OOB.DEACTIVATE_2_STEP);
			break;
		}

		return listNavOOB;
	}

	@Override
	// add page parameter
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		// Is enrolled in Mobile_Banking
		String enrollSMS = "EnrollSMS";
		if (userSession.getCustomerProfile() != null) {
			if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
				SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
				if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
						&& !smsprofile.getCtns().isEmpty()) {
					model.put(enrollSMS, true);
					String ctn = ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn();
					model.put("smsPhone", RSAUtils.setPhoneMask(ctn));
				} else
					model.put(enrollSMS, false);
			} else
				model.put(enrollSMS, false);
		}

		if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.RSA) != null) {
			CustomerEntitlement rsaEntitlement = userSession.getCustomerProfile()
					.getEntitlementByType(EntitlementType.RSA);
			if (rsaEntitlement != null) {
				// Get the ALTERNATE_PHONE
				String altPhone = rsaEntitlement.getString("ALTERNATE_PHONE");
				if (!(altPhone == null || altPhone.equals("") || altPhone.equals("N"))) {
					model.put("altPhone", RSAUtils.setPhoneMask(altPhone));
				}

				String altProvider = rsaEntitlement.getString("ALTERNATE_PROVIDER");
				if (!(altProvider == null || altProvider.equals("") || altProvider.equals("N"))) {
					model.put("altProvider", altProvider);
				}

				String frecuency = rsaEntitlement.getString("ALWAYSCHALLENGE");
				if (!(frecuency == null || frecuency.equals(""))) {
					model.put("frecuencyOOB", frecuency);
				} else {
					model.put("frecuencyOOB", "FALSE");
				}

				DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
						userSession.getDeviceRequest().getDeviceTokenCookie(), request);
				RsaChallengeRequest rsaChallengeRequest = new RsaChallengeRequest(userSession.getUsername(),deviceRequest);
				CredentialType challengeType = userSession.getCustomerService()
						.rsaGetChallengeMethod(rsaChallengeRequest);
				userSession.setDeviceRequest(deviceRequest);
				if (deviceRequest.getDeviceTokenCookie() != null) {
					model.put("devicetoken", deviceRequest.getDeviceTokenCookie());
				}
				if (challengeType != null && challengeType.equals(CredentialType.OOBPHONE))
					model.put("challengeType", CustomerOOBChallengeType.OOBPHONE.toString());
				else
					model.put("challengeType", CustomerOOBChallengeType.OOBSMS.toString());
			}
		} else {
			errors.rejectValue("smsError", "enrollOOB.register.sms.error", "No inscrito en mi banca movil.");
		}

		return model;
	}

	/**
	 * This method is executed at last, evaluated the navigation
	 */
	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object arg2,
			BindException arg3) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		String view = NavigationSequenceUtils.getNextView(userSession.getTasksListOOB());

		if (userSession.getDeviceRequest() != null && userSession.getDeviceRequest().getDeviceTokenCookie() != null) {
			Cookie cookie = new Cookie("PMData", userSession.getDeviceRequest().getDeviceTokenCookie());
			cookie.setPath("/");
			cookie.setMaxAge(31536000);
			ESAPI.httpUtilities().addCookie(response, cookie);

		}

		return new ModelAndView(view);
	}

	@Override
	protected boolean isFinishRequest(HttpServletRequest request) {
		return finish;
	}

}
