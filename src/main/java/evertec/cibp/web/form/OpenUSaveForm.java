package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.factory.TVAccountFactory;
import bppr.tv.beans.accounts.types.DepositAccount;
import bppr.tv.beans.holds.Address;
import bppr.tv.beans.payments.PaymentFrequency;

import com.evertecinc.services.seagull.accountopening.deposit.beans.AccountOpeningResult;
import com.evertecinc.services.seagull.accountopening.deposit.beans.AccountOwner;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.accountopening.SavingsAccountOpeningInfo;
import evertec.cibp.core.models.accountopening.dci.DCICustomer;
import evertec.cibp.core.models.branch.BranchZipcodeAssignment;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.documents.CIBPConfigurableTexts;
import evertec.cibp.core.models.documents.CIBPConfigurableTextsTypes;
import evertec.cibp.core.models.documents.ConfigurableTextSubSections;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.holiday.Holiday;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.GlobalPayeePaymentType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.BankingDaysUtils;
import evertec.cibp.core.utils.CibpValidationUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingAdminSession;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.facade.AccountsFacade;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.OpenUSaveBean;
import evertec.cibp.web.request.ModifyAccountPreferenceRequest;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class OpenUSaveForm extends AbstractWizardFormController implements GlobalEntitlementAwareController {

	protected final Log logger = LogFactory.getLog(getClass());
	private CustomerServices customerService;
	private TVFrontendAccount account;

	public OpenUSaveForm() {
		super();
		setCommandClass(OpenUSaveBean.class);
		setCommandName("usave");
		setPages(new String[] { "openusave", "openusave", "openusave", "openusave", "openusave", "openusave" });
	}

	protected int getTargetPage(HttpServletRequest request, int currentPage) {
		return super.getTargetPage(request, currentPage);
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new HashMap<>();
		model.put("page", "5");

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		CustomerProfile profile = userSession.getCustomerProfile();
		
		if(profile == null) {
			logger.error("Profile was null");
			model.put("openSuccess", false);
			return showForm(request, error, "openusave", model);			
		}
		
		BankingSession bankSession = userSession.getBankingSession();

		CustomerServices customerservice = userSession.getCustomerService();
		OpenUSaveBean usave = (OpenUSaveBean) command;

		// Aqui es donde iria la logica de la apertura...\\\\

		SavingsAccountOpeningInfo info = new SavingsAccountOpeningInfo();
		info.setProductId("090");// U-Save
		if (GenericValidator.isBlankOrNull(usave.getNickname()))
			info.setAccountNickname("U Save");
		else
			info.setAccountNickname(usave.getNickname());

		Address res = new Address();
		res.setAddr1(usave.getPhysicalAddress1());
		res.setAddr2(usave.getPhysicalAddress2());
		res.setCity(usave.getPhysicalCity());
		res.setStateProv(usave.getPhysicalState());
		String zip = usave.getPhysicalZipCode1();
		if (!GenericValidator.isBlankOrNull(usave.getPhysicalZipCode2()))
			zip = zip + "-" + usave.getPhysicalZipCode2();
		res.setPostalCode(zip);

		Address pos = new Address();
		if (!usave.getSamePhysicalPostalAddress()) {
			pos.setAddr1(usave.getPostalAddress1());
			pos.setAddr2(usave.getPostalAddress2());
			pos.setCity(usave.getPostalCity());
			pos.setStateProv(usave.getPostalState());
			zip = usave.getPostalZipCode1();
			if (!GenericValidator.isBlankOrNull(usave.getPostalZipCode2()))
				zip = zip + "-" + usave.getPostalZipCode2();
			pos.setPostalCode(zip);
		} else
			pos = res;

		info.setPortalAddress(pos);
		info.setResidentialAddress(res);
		if (profile != null) {
			if (profile.getTaxId() != null)
				info.setTaxId(profile.getTaxId());
			if (bankSession.getPermId() != null)
				info.setCustomerPermId(bankSession.getPermId().toString());
			if (profile.getProfileId() != null)
				info.setCustomerProfileId(profile.getProfileId());
		}
		// Credit account
		info.setAccountToCredit(Utils.getAccountById(usave.getWithdrawalAccount(), userSession.getAccounts()));
		info.setReceivedAmountDate(usave.getWithdrawalDate());
		if (GenericValidator.isBlankOrNull(usave.getWithdrawalOption())) {
			info.setReceivedAmount("N"); // No specific date
		}
		info.setReceivedAmount(usave.getWithdrawalOption());

		// Initial funding
		info.setInitialFundingAccount(Utils.getAccountById(usave.getDebitFromAccount(), userSession.getAccounts()));
		info.setInitialFundingAmount(usave.getInitialDepositAmount());

		// Transfer recurring
		if (usave.getRecurringDebit()) {

			info.setFrequency(usave.getRecurringDebitFrequency());
			info.setTransferSourceAccount(Utils.getAccountById(usave.getRecurringDebitAccount(),
					userSession.getAccounts()));
			info.setTransferAmount(usave.getRecurringDebitAmount());
			Calendar startDate = Calendar.getInstance();
			startDate.setTime(usave.getStartRecurringDebitDate());
			info.setTransferDate(startDate.getTime());

			if (GenericValidator.isBlankOrNull(usave.getWithdrawalOption())
					|| usave.getWithdrawalOption().equalsIgnoreCase("N")) {
				info.setRecurringInstances(999);
				info.setEndTransferDate(null);
				info.setReceivedAmount("N"); // No Date
				info.setReceivedAmountDate(null);
			} else if (usave.getWithdrawalOption().equalsIgnoreCase("A")) {
				info.setRecurringInstances(999);
				info.setEndTransferDate(null);
				info.setReceivedAmount("A"); // Annual
				Calendar c2 = Calendar.getInstance();
				c2.setTime(usave.getWithdrawalDate());
				info.setEndTransferDate(usave.getWithdrawalDate());
			} else if (usave.getWithdrawalOption().equalsIgnoreCase("D")) {
				Calendar c2 = Calendar.getInstance();
				c2.setTime(usave.getWithdrawalDate());
				info.setRecurringInstances(calculateNumberOfInstances(startDate, c2, usave.getRecurringDebitFrequency()
						.name()));
				info.setEndTransferDate(usave.getWithdrawalDate());
				info.setReceivedAmount("D");// Pay out Future
			}

		}

		// ==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>//

		// Llamados a Core - Backend para procesar los request
		BankingAdminSession bankAdminSes = customerservice.getBankingAdminSession();
		if (bankAdminSes == null) {
			logger.error("BankingAdminSession was null.");
			model.put("openSuccess", false);
			return showForm(request, error, "openusave", model);
		}
		AccountOwner owner = null;
		if (!GenericValidator.isBlankOrNull(info.getCustomerPermId())) {
			owner = customerservice.getCustomerbyPermId(bankAdminSes, info.getCustomerPermId(), profile,
					info.getProductId());
			if (owner == null) {
				if(GenericValidator.isBlankOrNull(profile.getUsername())) {
					logger.error("Owner with Banking Session PermId: "+info.getCustomerPermId()+" was null.");										
				} else {
					logger.error("Owner with Banking Session PermId: "+info.getCustomerPermId()+" was null. Username was: "+profile.getUsername());					
				}
				model.put("openSuccess", false);
				return showForm(request, error, "openusave", model);
			}
			owner.setAddress(res);

		}
		// Obtener el bank code y branch
		// lguzman/ CIBP-1859 aperturas usave asignadas a la sucursal
		// incorrecta/ se restringe la busqueda de Branch a los 5 primeros
		// valores del PostalCode si es de PR/USVI; 6 si es de BVI
		BranchZipcodeAssignment branchCostCenterForZipCode = customerservice.getBranchCostCenterForZipCode(info
				.getPortalAddress().getStateProv().equals("BVI") ? info.getPortalAddress().getPostalCode()
				.substring(0, 6) : info.getPortalAddress().getPostalCode().substring(0, 5));

		if (branchCostCenterForZipCode == null) {
			// return an error pq sin esta informacion la cuenta no se puede
			// abrir, puede ser algun zipcode incorrecto
			if(GenericValidator.isBlankOrNull(profile.getUsername())) {
				logger.error("BranchCostCenterForZipCode entered was null.");
			} else {
				logger.error("BranchCostCenterForZipCode entered by username: "+profile.getUsername()+" was null.");
			}
			
			model.put("openSuccess", false);
			return showForm(request, error, "openusave", model);
		}
		AccountOpeningResult openingResult = customerservice.openUSaveAccount(profile, owner, bankAdminSes,
				info.getResidentialAddress(), info.getPortalAddress(), branchCostCenterForZipCode, info.getProductId());
		if (openingResult == null || GenericValidator.isBlankOrNull(openingResult.getNewAccountNumber())) {
			if(GenericValidator.isBlankOrNull(profile.getUsername())) {
				logger.error("Could not Open Account.");
			} else {
				logger.error("Could not Open Account for user: "+profile.getUsername());
			}
			if(openingResult != null) {
				logger.error("      Code:"+openingResult.getReturnCode()+" Description:"+openingResult.getReturnDescription());
			}
			model.put("openSuccess", false);
			return showForm(request, error, "openusave", model);
		}
		model.put("openSuccess", true);
		
		// DCI
		try {
			DCICustomer dciCustomer = userSession.getDciCustomer();
			dciCustomer.setAccounOpenedDate(Calendar.getInstance().getTime());
			dciCustomer.setAccount(openingResult.getNewAccountNumber());
			dciCustomer.setAccounType(openingResult.getNewAccountProductId());
			dciCustomer.setCompleted(Calendar.getInstance().getTime());
			if (Utils.getRSALanguage(request).equals("en")) {
				dciCustomer.setAnswerLang(DCICustomer.LANG_EN);
			} else {
				dciCustomer.setAnswerLang(DCICustomer.LANG_SP);
			}
			userSession.getCustomerService().addDCICustomer(dciCustomer,
					Utils.generateCustomerInteractionEvent(request));
			userSession.getCustomerService().updateDCICustomerAnswers(userSession.getDciCustomerAnswers(),
					Utils.generateCustomerInteractionEvent(request));
			userSession.setDciCustomer(null);
			userSession.setDciCustomerAnswers(null);
		} catch (Exception e) {
			if(GenericValidator.isBlankOrNull(profile.getUsername())) {
				logger.error("Could not create DCI.");				
			} else {
				logger.error("Could not create DCI info for customer " + profile.getUsername());				
			}
		}

		TVAccountFactory fact = new TVAccountFactory();
		DepositAccount depact = fact.createDepositAccount(openingResult.getNewAccountNumber(), "S");
		TVFrontendAccount newaccount = new TVFrontendAccount(depact);

		boolean updateIDA = customerservice.updateAccountCreditInfo(profile, info, newaccount,
				branchCostCenterForZipCode);
		if (!updateIDA) {
			model.put("maintainaceFail", true);
		} else
			model.put("maintainaceFail", false);

		if (!GenericValidator.isBlankOrNull(info.getAccountNickname())) {
			// poner nickname a la cuenta
			CustomerAccountPreferences pref = new CustomerAccountPreferences();
			pref.setHide(false);
			pref.setNickname(info.getAccountNickname());

			// Accounts facade implementation
			ModifyAccountPreferenceRequest modifyAccountPreferenceRequest = new ModifyAccountPreferenceRequest();
			modifyAccountPreferenceRequest.setAccount(newaccount);
			modifyAccountPreferenceRequest.setCustomerProfile(profile);
			modifyAccountPreferenceRequest.setUserSession(userSession);
			modifyAccountPreferenceRequest.setNickname(info.getAccountNickname());
			
			if (newaccount.getCustomerAccountPreferences() != null) {
				modifyAccountPreferenceRequest.setHide(newaccount.getCustomerAccountPreferences().getHide());
			} else {
				modifyAccountPreferenceRequest.setHide(false);
			}
			
			AccountsFacade.getInstance().modifyAccountNickname(modifyAccountPreferenceRequest);
		}
		Integer payeeId = new Integer("17748");
		if (branchCostCenterForZipCode != null && branchCostCenterForZipCode.getBankCode().equalsIgnoreCase("002"))// VI
		{
			payeeId = new Integer("17751");
		}

		GlobalPayee globalPayee = PaymentsFacade.getInstance().getGlobalPayeeById(payeeId, userSession);
		if (globalPayee == null) {
			if (info.getInitialFundingAccount() != null
					&& info.getInitialFundingAmount().compareTo(BigDecimal.ZERO) > 0)
				model.put("initialFail", true);

			model.put("transferFail", true);
			return showForm(request, error, "openusave", model);
		}
		boolean isHighRiskPayee = false;
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1701)) {
			
			PaymentsFacade.getInstance().getGlobalPayeePreference(globalPayee, userSession);

			if(globalPayee.getPreference() != null)
				isHighRiskPayee = globalPayee.getPreference().getHighRisk();
		}
		TVFrontEndPayee usavePayee = customerservice.addUsavePayee(profile, bankSession, globalPayee, info, newaccount,
				Utils.generateCustomerInteractionEvent(request), isHighRiskPayee);
		if (usavePayee == null) {
			if (info.getInitialFundingAccount() != null
					&& info.getInitialFundingAmount().compareTo(BigDecimal.ZERO) > 0)
				model.put("initialFail", true);

			model.put("transferFail", true);
			return showForm(request, error, "openusave", model);
		}

		// Processing Initial Funding
		if (info.getInitialFundingAccount() != null && info.getInitialFundingAmount().compareTo(BigDecimal.ZERO) > 0) {
			TVAccountTransferSource source = new TVAccountTransferSource(info.getInitialFundingAccount());
			TVPayment payment = new TVPayment(info.getInitialFundingAmount(), Frequency.MANUALLY, 1,
					Calendar.getInstance(), source, usavePayee);
			payment.getPayee().setTelenomina(false);
			PaymentResponse initialResponse = customerservice.setInitialFunding(bankSession, payment,
					info.getProductId(), profile);
			if (initialResponse.getStatus() != ResultStatus.SUCCESS) {
				model.put("initialFail", true);
			} else
				model.put("initialFail", false);
		}

		if (usave.getRecurringDebit()) {
			// Recurring transfer
			TVAccountTransferSource sourceRecurring = new TVAccountTransferSource(info.getTransferSourceAccount());
			Calendar startDate = Calendar.getInstance();
			startDate.setTime(info.getTransferDate());
			Calendar endDate = Calendar.getInstance();
			if (info.getReceivedAmountDate() != null)
				endDate.setTime(info.getReceivedAmountDate());
			// int instances = calculateNumberOfInstances(startDate, endDate,
			// info.getFrequency().name());
			TVPayment transferrecurring = new TVPayment(info.getTransferAmount(), info.getFrequency(),
					info.getRecurringInstances(), startDate, sourceRecurring, usavePayee);
			transferrecurring.setPayee(usavePayee);
			transferrecurring.getPayee().setTelenomina(false);
			PaymentResponse recurrinfResponse = customerservice.setRecurringTransfer(bankSession, transferrecurring,
					info.getProductId(), profile);
			if (recurrinfResponse.getStatus() != ResultStatus.SUCCESS) {
				model.put("transferFail", true);
			} else
				model.put("transferFail", false);
		}
		// ==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>==>//

		// userSession.resetAccounts();
		userSession.resetPayees();
		// userSession.resetTransfers();
		return showForm(request, error, "openusave", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		Map<String, Object> model = new HashMap<>();

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		model.put("page", page);
		AccountMaskUtil mask = new AccountMaskUtil();
		NumberTool numberTool = new NumberTool();

		// Set holidays
		Map<String, Object> h = new LinkedHashMap<String, Object>();
		Map<String, Object> ho = new LinkedHashMap<String, Object>();

		List<Holiday> holidays = customerService.getHolidays();
		if (holidays != null)
			for (Holiday holiday : holidays) {
				h.put(holiday.getId(),
						new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(holiday.getDate().getTime()));
			}
		model.put("holidays", h);

		// CIBP-2233 U-Save - Fecha de desembolso
		List<Holiday> holidaysAll = customerService.getAllHolidays();
		if (holidaysAll != null)
			for (Holiday holiday : holidaysAll) {
				ho.put(holiday.getId(),
						new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(holiday.getDate().getTime()));
			}
		model.put("holidaysAll", ho);

		List<CIBPConfigurableTexts> textlist = userSession.getDBConfiguredTextList(CIBPConfigurableTextsTypes.UNICA,
				ConfigurableTextSubSections.CREDIT_PRODUCTS_PR);
		Map<String, String> creditProducts = new LinkedHashMap<String, String>();

		for (CIBPConfigurableTexts text : textlist) {
			if (userSession.getPreferredLanguage() == PreferredLanguage.ENGLISH) {
				creditProducts.put(text.getTextCode(), text.getEnglishDescription());
			} else {
				creditProducts.put(text.getTextCode(), text.getSpanishDescription());
			}
		}
		if (userSession.hasVirginIslandAccount())
			model.put("profileCountry", "vi");
		else if (userSession.hasBritishVirginIslandAccount())
			model.put("profileCountry", "bvi");
		else
			model.put("profileCountry", "pr");

		model.put("creditProducts", creditProducts);
		model.put("textlist", textlist);

		if (page == 0 || page == 1) {

			List<TVFrontendAccount> withdrawalToAccounts = Utils.getAllDepositAccounts(userSession.getAccounts());
			Map<String, String> accountsToMap = new LinkedHashMap<String, String>();

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				accountsToMap.put("none", "Select an account");
			else
				accountsToMap.put("none", "Selecciona una cuenta");

			for (Iterator<TVFrontendAccount> iterator = withdrawalToAccounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount account = iterator.next();
				if (account != null) {
					if (!account.getHide() && !account.getAccountSection().equals("R")) {
						if (account.getPortalBalance() != null)
							accountsToMap.put(
									account.getFrontEndId(),
									StringUtils.abbreviate(account.getNickname(), 15)
											+ " "
											+ mask.mask(account.getAccountNumber())
											+ ((account.getAccountNumberSuffix() != null && !account
													.getAccountNumberSuffix().equals("")) ? " "
													+ account.getAccountNumberSuffix() : "") + " "
											+ numberTool.format("currency", account.getPortalBalance()));
						else
							accountsToMap.put(
									account.getFrontEndId(),
									StringUtils.abbreviate(account.getNickname(), 15)
											+ " "
											+ mask.mask(account.getAccountNumber())
											+ ((account.getAccountNumberSuffix() != null && !account
													.getAccountNumberSuffix().equals("")) ? " "
													+ account.getAccountNumberSuffix() : ""));
					}
				}
			}

			model.put("toaccounts", accountsToMap);

			Calendar cal = Calendar.getInstance();
			String processDate = Utils.getEstimateDate(customerService.getHolidays(), cal,
					GlobalPayeePaymentType.INTERNO);
			model.put("processDate", processDate);

			if (((OpenUSaveBean) command).getInitialDepositAmount() != null) {
				if (BigDecimal.ZERO.compareTo(((OpenUSaveBean) command).getInitialDepositAmount()) == 0) {
					((OpenUSaveBean) command).setInitialDepositAmount(null);
				}
			}

		}

		if (page == 0 || page == 1) {

			// Set up today's date
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			Calendar n = Calendar.getInstance();

			// Weekend fix
			if (Calendar.SATURDAY == n.get(Calendar.DAY_OF_WEEK))
				n.add(Calendar.DAY_OF_WEEK, 2);
			else if (Calendar.SUNDAY == n.get(Calendar.DAY_OF_WEEK))
				n.add(Calendar.DAY_OF_WEEK, 1);

			// Holiday fix
			if (holidays != null) {
				Calendar thisHoliday = Calendar.getInstance();
				for (Holiday holiday : holidays) {
					thisHoliday = holiday.getDate();
					if (thisHoliday.get(Calendar.DATE) == n.get(Calendar.DATE)
							&& thisHoliday.get(Calendar.MONTH) == n.get(Calendar.MONTH)
							&& thisHoliday.get(Calendar.YEAR) == n.get(Calendar.YEAR)) {
						n.add(Calendar.DAY_OF_WEEK, 1);
					}
				}
			}

			model.put("todayDate", df.format(n.getTime()));
			df = new SimpleDateFormat("MM");
			model.put("month", df.format(n.getTime()));
			df = new SimpleDateFormat("dd");
			model.put("day", df.format(n.getTime()));

			df = new SimpleDateFormat("MM/dd/yyyy");
			n = Calendar.getInstance();
			n.add(Calendar.DAY_OF_YEAR, 1);

			// Weekend fix
			if (Calendar.SATURDAY == n.get(Calendar.DAY_OF_WEEK))
				n.add(Calendar.DAY_OF_WEEK, 2);
			else if (Calendar.SUNDAY == n.get(Calendar.DAY_OF_WEEK))
				n.add(Calendar.DAY_OF_WEEK, 1);

			// Holiday fix
			if (holidays != null) {
				Calendar thisHoliday = Calendar.getInstance();
				for (Holiday holiday : holidaysAll) {
					thisHoliday = holiday.getDate();
					if (thisHoliday.get(Calendar.DATE) == n.get(Calendar.DATE)
							&& thisHoliday.get(Calendar.MONTH) == n.get(Calendar.MONTH)
							&& thisHoliday.get(Calendar.YEAR) == n.get(Calendar.YEAR)) {
						n.add(Calendar.DAY_OF_WEEK, 1);
					}
				}
			}

			model.put("nextDate", df.format(n.getTime()));
			df = new SimpleDateFormat("MM");
			model.put("nextmonth", df.format(n.getTime()));
			df = new SimpleDateFormat("dd");
			model.put("nextday", df.format(n.getTime()));

			df = new SimpleDateFormat("MM/dd/yyyy");
			Calendar thresholdDate = Calendar.getInstance();
			thresholdDate.add(Calendar.YEAR, 40);
			model.put("thresholddate", df.format(thresholdDate.getTime()));

		} else if (page == 2) {

			Map<Object, String> states = new LinkedHashMap<Object, String>();
			states.put("PR", "PR");
			states.put("VI", "USVI");
			states.put("BVI", "BVI");
			model.put("states", states);
		} else if (page == 4) {

			OpenUSaveBean usave = (OpenUSaveBean) command;

			if (GenericValidator.isBlankOrNull(usave.getWithdrawalOption())
					|| usave.getWithdrawalOption().equalsIgnoreCase("N"))
				model.put("receivemoney", "later");
			else if (usave.getWithdrawalOption().equalsIgnoreCase("A")) {
				model.put("receivemoney", "year");
				Calendar withdrawalDate = Calendar.getInstance();
				withdrawalDate.add(Calendar.YEAR, 1);
				model.put("wdate", withdrawalDate);
			} else if (usave.getWithdrawalOption().equalsIgnoreCase("D"))
				model.put("receivemoney", "specific");

			TVFrontendAccount depositIntoAccountNumber = Utils.getAccountById(usave.getWithdrawalAccount(),
					Utils.getAllDepositAccounts(userSession.getAccounts()));
			TVFrontendAccount debitFromAccountNumber = Utils.getAccountById(usave.getDebitFromAccount(),
					Utils.getAllDepositAccounts(userSession.getAccounts()));
			TVFrontendAccount trasnferdebitFromAccountNumber = Utils.getAccountById(usave.getRecurringDebitAccount(),
					Utils.getAllDepositAccounts(userSession.getAccounts()));
			if (depositIntoAccountNumber != null)
				model.put("depositIntoAccountNumber", depositIntoAccountNumber.getAccountNumber());
			if (debitFromAccountNumber != null)
				model.put("debitFromAccountNumber", debitFromAccountNumber.getAccountNumber());
			if (trasnferdebitFromAccountNumber != null)
				model.put("trasnferdebitFromAccountNumber", trasnferdebitFromAccountNumber.getAccountNumber());

			// We cannot withdraw on a weekend
			Calendar cal = Calendar.getInstance();
			cal.setTime(usave.getWithdrawalDate());

			if (Calendar.SATURDAY == cal.get(Calendar.DAY_OF_WEEK) || Calendar.SUNDAY == cal.get(Calendar.DAY_OF_WEEK)) {
				cal = calculateEffDate(cal);
				usave.setWithdrawalDate(cal.getTime());
			}
			// We cannot withdraw on a holiday
			else {
				if (holidays != null) {
					Calendar thisHoliday = Calendar.getInstance();
					for (Holiday holiday : holidays) {
						thisHoliday = holiday.getDate();
						if (thisHoliday.get(Calendar.DATE) == cal.get(Calendar.DATE)
								&& thisHoliday.get(Calendar.MONTH) == cal.get(Calendar.MONTH)
								&& thisHoliday.get(Calendar.YEAR) == cal.get(Calendar.YEAR)) {
							cal = calculateEffDate(cal);
							usave.setWithdrawalDate(cal.getTime());
							break;
						}
					}
				}
			}
		}

		return model;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		boolean err = false;
		OpenUSaveBean usave = (OpenUSaveBean) command;
		usave.setShowDateInWithdrawalDateError(false);

		if (page == 0) {

			String nickname = usave.getNickname();

			if (!GenericValidator.isBlankOrNull(nickname)) {
				if (!CibpValidationUtils.isValidPayeeNickname(nickname)) {
					errors.rejectValue("nickname", "productselection.usave.nickname.invalid",
							"Please enter a valid account name");
					err = true;
				}
			}

			if (usave.getWithdrawalOption() == null || usave.getWithdrawalOption().equals("")) {
				errors.rejectValue("withdrawalOption", "productselection.usave.withdrawaloption.invalid",
						"Select when to receive the money.");
				err = true;
			}

			if (usave.getWithdrawalAccount() == null || usave.getWithdrawalAccount().equals("none")) {
				errors.rejectValue("withdrawalAccount", "productselection.usave.withdrawalaccount.invalid",
						"Please select an account.");
				err = true;
			}

			// The date only matters if a specific date is chosen
			if (usave.getWithdrawalOption() != null
					&& (usave.getWithdrawalOption().equals("D") || usave.getWithdrawalOption().equals("A"))) {

				Date effectiveDate = usave.getWithdrawalDate();
				Calendar today = Calendar.getInstance();

				// Set the max date (forty years in future)
				Calendar thresholdDate = Calendar.getInstance();
				thresholdDate.add(Calendar.YEAR, 40);

				if (effectiveDate != null) {
					// Validate no future dates
					Calendar n = Calendar.getInstance();
					n.setTime(effectiveDate);

					if ((n.get(Calendar.YEAR) < today.get(Calendar.YEAR))
							|| n.get(Calendar.YEAR) == today.get(Calendar.YEAR)
							&& n.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR)) {
						errors.rejectValue("withdrawalDate", "productselection.usave.withdrawaldate.past",
								"You must select a future date");
						err = true;
					} else if (n.after(thresholdDate)) {
						errors.rejectValue("withdrawalDate", "productselection.usave.withdrawaldate.oneyear",
								"You must select a date less than the limit date");
						err = true;
						usave.setShowDateInWithdrawalDateError(true);
					}

				} else {
					errors.rejectValue("withdrawalDate", "productselection.usave.withdrawaldate.select",
							"Select the date");
					err = true;
				}
			}
		} else if (page == 1) {

			BigDecimal maxpayamount = new BigDecimal(999999.99);

			if (usave.getInitialDepositAmount() == null) {
				usave.setInitialDepositAmount(BigDecimal.ZERO);
			}

			if (usave.getInitialDepositAmount().compareTo(maxpayamount) > 0) {
				errors.rejectValue("initialDepositAmount", "productselection.usave.amount.over",
						"The maximum amount to transfer is $1000000.00");
				err = true;
			}
			if (usave.getInitialDepositAmount().compareTo(BigDecimal.ZERO) > 0) {
				if (usave.getDebitFromAccount() == null || "none".equalsIgnoreCase(usave.getDebitFromAccount())) {
					errors.rejectValue("debitFromAccount", "productselection.usave.debitfromaccount.required",
							"Select the account");
					err = true;
				} else {
					UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
					List<TVFrontendAccount> accounts = userSession.getAccounts();

					TVFrontendAccount account = Utils.getAccountById(usave.getDebitFromAccount(), accounts);
					if (usave.getInitialDepositAmount().compareTo(account.getPortalBalance()) > 0) {
						errors.rejectValue("initialDepositAmount",
								"productselection.usave.debitfromaccount.greaterthanbalance", "Select the account");
						err = true;
					}
				}

			} else if (usave.getInitialDepositAmount().doubleValue() < new Double(0d)) {
				errors.rejectValue("initialDepositAmount", "productselection.usave.initial.amount.cannotbenegative",
						"The initial amount cannot be negative.");
				err = true;
			}

			if (usave.getRecurringDebit()) {

				if (usave.getRecurringDebitAmount() == null) {
					errors.rejectValue("recurringDebitAmount", "productselection.usave.withdrawal.amount.under",
							"The minimum amount to transfer is $0.01");
					err = true;
				} else {
					if (usave.getRecurringDebitAmount().compareTo(maxpayamount) > 0) {
						errors.rejectValue("recurringDebitAmount", "productselection.usave.withdrawal.amount.over",
								"The maximum amount to transfer is $10000.00");
						err = true;
					} else if (usave.getRecurringDebitAmount().doubleValue() < new Double(0.01d)) {
						errors.rejectValue("recurringDebitAmount", "productselection.usave.withdrawal.amount.under",
								"The minimum amount to transfer is $0.01");
						err = true;
					}
				}

				if (usave.getRecurringDebitFrequency() == null
						|| usave.getRecurringDebitFrequency().equals(Frequency.MANUALLY)) {
					errors.rejectValue("recurringDebitFrequency", "productselection.usave.frequency.required",
							"Select the frequency");
					err = true;
				}

				if (usave.getRecurringDebitAccount() == null
						|| "none".equalsIgnoreCase(usave.getRecurringDebitAccount())) {
					errors.rejectValue("recurringDebitAccount", "productselection.usave.debitfromaccount.required",
							"Select the account");
					err = true;
				}

				Date effectiveDate = usave.getStartRecurringDebitDate();
				Calendar today = Calendar.getInstance();

				// Set the max date (forty years in future)
				Calendar thresholdDate = Calendar.getInstance();
				thresholdDate.add(Calendar.YEAR, 40);

				if (effectiveDate != null) {
					// Validate no future dates
					Calendar n = Calendar.getInstance();
					n.setTime(effectiveDate);

					if ((n.get(Calendar.YEAR) < today.get(Calendar.YEAR))
							|| n.get(Calendar.YEAR) == today.get(Calendar.YEAR)
							&& n.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR)) {
						errors.rejectValue("startRecurringDebitDate", "productselection.usave.withdrawaldate.past",
								"You must select a future date");
						err = true;
					} else if (n.after(thresholdDate)) {
						errors.rejectValue("startRecurringDebitDate", "productselection.usave.withdrawaldate.oneyear",
								"You must select a date less than the limit date");
						err = true;
					}
				} else {
					errors.rejectValue("startRecurringDebitDate", "productselection.usave.withdrawaldate.select",
							"Select the date");
					err = true;
				}
			}
		} else if (page == 2) {

			String physicalAddress1 = usave.getPhysicalAddress1();
			String physicalAddress2 = usave.getPhysicalAddress2();
			String physicalCity = usave.getPhysicalCity();
			String physicalState = usave.getPhysicalState();
			String physicalZipCode1 = usave.getPhysicalZipCode1();
			String physicalZipCode2 = usave.getPhysicalZipCode2();

			boolean samePhysicalResidentialAddress = usave.getSamePhysicalPostalAddress();
			String postalAddress1 = usave.getPostalAddress1();
			String postalAddress2 = usave.getPostalAddress2();
			String postalCity = usave.getPostalCity();
			String postalState = usave.getPostalState();
			String postalZipCode1 = usave.getPostalZipCode1();
			String postalZipCode2 = usave.getPostalZipCode2();

			if (GenericValidator.isBlankOrNull(physicalAddress1)) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					errors.rejectValue("physicalAddress1", "", "Please enter a valid address.");
					err = true;
				} else {
					errors.rejectValue("physicalAddress1", "", "Favor entrar una direcci&oacute;n valida.");
					err = true;
				}
			} else if (!CibpValidationUtils.isValidUSaveAddress(physicalAddress1)) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					errors.rejectValue("physicalAddress1", "", "Please include letters and numbers only.");
					err = true;
				} else {
					errors.rejectValue("physicalAddress1", "",
							"La direcci&oacute;n solo puede incluir letras y n&uacute;meros.");
					err = true;
				}
			}
			if (!GenericValidator.isBlankOrNull(physicalAddress2)
					&& !CibpValidationUtils.isValidUSaveAddress(physicalAddress2)) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					errors.rejectValue("physicalAddress2", "", "Please include letters and numbers only.");
					err = true;
				} else {
					errors.rejectValue("physicalAddress2", "",
							"La direcci&oacute;n solo puede incluir letras y n&uacute;meros.");
					err = true;
				}
			}
			if (GenericValidator.isBlankOrNull(physicalCity)
					|| !GenericValidator.matchRegexp(physicalState, "^[A-Za-z ]+$")) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					errors.rejectValue("physicalCity", "", "Please enter a valid city.");
					err = true;
				} else {
					errors.rejectValue("physicalCity", "", "Favor entrar una ciudad valida.");
					err = true;
				}
			}
			if (physicalState.equals("BVI")) {
				if (GenericValidator.isBlankOrNull(physicalZipCode1)
						|| physicalZipCode1.length() != 6
						|| !physicalZipCode1.toLowerCase().startsWith("vg11")
						|| !(physicalZipCode1.endsWith("10") || physicalZipCode1.endsWith("20")
								|| physicalZipCode1.endsWith("30") || physicalZipCode1.endsWith("40")
								|| physicalZipCode1.endsWith("50") || physicalZipCode1.endsWith("60"))) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("physicalZipCode2", "", "Invalid zipcode.");
						err = true;
					} else {
						errors.rejectValue("physicalZipCode2", "", "C&oacute;digo postal inv&aacute;lido.");
						err = true;
					}
				}
			} else {
				if (GenericValidator.isBlankOrNull(physicalZipCode1)
						|| !GenericValidator.matchRegexp(physicalZipCode1, "^\\d{5}$")) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("physicalZipCode2", "", "Please enter a valid zipcode.");
						err = true;
					} else {
						errors.rejectValue("physicalZipCode2", "", "Favor entrar un c&oacute;digo postal valido.");
						err = true;
					}
				} else if (!GenericValidator.isBlankOrNull(physicalZipCode2)
						&& !GenericValidator.matchRegexp(physicalZipCode2, "^\\d{4}$")) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("physicalZipCode2", "", "Please enter a valid zipcode.");
						err = true;
					} else {
						errors.rejectValue("physicalZipCode2", "", "Favor entrar un c&oacute;digo postal valido.");
						err = true;
					}
				}
			}

			if (!samePhysicalResidentialAddress) {

				if (GenericValidator.isBlankOrNull(postalAddress1)) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("postalAddress1", "", "Please enter a valid address.");
						err = true;
					} else {
						errors.rejectValue("postalAddress1", "", "Favor entrar una direcci&oacute;n valida.");
						err = true;
					}
				} else if (!CibpValidationUtils.isValidUSaveAddress(postalAddress1)) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("postalAddress1", "", "Please include letters and numbers only.");
						err = true;
					} else {
						errors.rejectValue("postalAddress1", "",
								"La direcci&oacute;n solo puede incluir letras y n&uacute;meros.");
						err = true;
					}
				}
				if (!GenericValidator.isBlankOrNull(postalAddress2)
						&& !CibpValidationUtils.isValidUSaveAddress(postalAddress2)) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("postalAddress2", "", "Please include letters and numbers only.");
						err = true;
					} else {
						errors.rejectValue("postalAddress2", "",
								"La direcci&oacute;n solo puede incluir letras y n&uacute;meros.");
						err = true;
					}
				}
				if (GenericValidator.isBlankOrNull(postalCity)
						|| !GenericValidator.matchRegexp(postalState, "^[A-Za-z ]+$")) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("postalCity", "", "Please enter a valid city.");
						err = true;
					} else {
						errors.rejectValue("postalCity", "", "Favor entrar una ciudad valida.");
						err = true;
					}
				}
				if (GenericValidator.isBlankOrNull(postalState)
						|| !GenericValidator.matchRegexp(postalState, "^[A-Za-z ]+$")) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("postalState", "", "Please enter a valid state.");
						err = true;
					} else {
						errors.rejectValue("postalState", "", "Favor entrar un estado valido.");
						err = true;
					}
				}
				if (postalState.equals("BVI")) {
					if (GenericValidator.isBlankOrNull(postalZipCode1)
							|| postalZipCode1.length() != 6
							|| !postalZipCode1.toLowerCase().startsWith("vg11")
							|| !(postalZipCode1.endsWith("10") || postalZipCode1.endsWith("20")
									|| postalZipCode1.endsWith("30") || postalZipCode1.endsWith("40")
									|| postalZipCode1.endsWith("50") || postalZipCode1.endsWith("60"))) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							errors.rejectValue("postalZipCode2", "", "Invalid zipcode.");
							err = true;
						} else {
							errors.rejectValue("postalZipCode2", "", "C&oacute;digo postal inv&aacute;lido.");
							err = true;
						}
					}
				} else {
					if (GenericValidator.isBlankOrNull(postalZipCode1)
							|| !GenericValidator.matchRegexp(postalZipCode1, "^\\d{5}$")) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							errors.rejectValue("postalZipCode2", "", "Please enter a valid zipcode.");
							err = true;
						} else {
							errors.rejectValue("postalZipCode2", "", "Favor entrar un c&oacute;digo postal valido.");
							err = true;
						}
					} else if (!GenericValidator.isBlankOrNull(postalZipCode2)
							&& !GenericValidator.matchRegexp(postalZipCode2, "^\\d{4}$")) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							errors.rejectValue("postalZipCode2", "", "Please enter a valid zipcode.");
							err = true;
						} else {
							errors.rejectValue("postalZipCode2", "", "Favor entrar un c&oacute;digo postal valido.");
							err = true;
						}
					}
				}
			}
		} else if (page == 3) {
			if (!usave.getAgreeToContract1()) {
				errors.rejectValue("agreeToContract1", "productselection.usave.agree.error",
						"You must read and accept the terms and conditions and this addendum.");
				err = true;
			}
			if (!usave.getAgreeToContract2()) {
				errors.rejectValue("agreeToContract2", "productselection.usave.agree.error",
						"You must read and accept the terms and conditions and this addendum.");
				err = true;
			}
		}
		if (err) {
			UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
				session.setStatusmessage("Please verify information entered");
			else
				session.setStatusmessage("Verifica la informaci&oacute;n entrada");
		}
	}

	public Calendar calculateEffDate(Calendar cal) {
		List<Holiday> holidays = customerService.getHolidays();
		BankingDaysUtils bankingDaysUtils = new BankingDaysUtils(holidays);
		return bankingDaysUtils.calculatePayDaysWithCutoff(cal, 1, 17, 0);
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerServices) {
		this.customerService = customerServices;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.TRANSFERS;
	}

	private Calendar getEndDate(Calendar startDate, int inst, String freq) {

		Calendar gregoriancalendar = startDate;// new
												// GregorianCalendar(startDate.get(Calendar.YEAR),
												// startDate.get(Calendar.MONTH),
												// startDate.get(Calendar.DAY_OF_MONTH));
		if (freq.equals("WW") || freq.equals(Frequency.WEEKLY.getVaultCode()))
			gregoriancalendar.add(Calendar.DAY_OF_MONTH, inst * 7);
		else if (freq.equals("BW") || freq.equals(Frequency.BIWEEKLY.getVaultCode()))
			gregoriancalendar.add(Calendar.DAY_OF_MONTH, inst * 14);
		else if (freq.equals("MM") || freq.equals(Frequency.MONTHLY.getVaultCode()))
			gregoriancalendar.add(Calendar.MONTH, inst);
		else if (freq.equals("QQ") || freq.equals(Frequency.QUARTERLY.getVaultCode()))
			gregoriancalendar.add(Calendar.MONTH, inst * 3);
		else if (freq.equals("SA") || freq.equals(Frequency.SEMI_ANNUALLY.getVaultCode()))
			gregoriancalendar.add(Calendar.MONTH, inst * 6);
		else if (freq.equals("AA") || freq.equals(Frequency.ANNUALLY.getVaultCode()))
			gregoriancalendar.add(Calendar.YEAR, inst);
		return gregoriancalendar;
	}

	private int calculateNumberOfInstances(Calendar startDate, Calendar endDate2, String frequency) {

		Calendar start = (Calendar) startDate.clone();
		Calendar end = (Calendar) endDate2.clone();

		if (end.before(startDate))
			return -1;

		int i = 0;
		if (frequency.equals(PaymentFrequency.WEEKLY) || frequency.equals(PaymentFrequency.WEEKLY_COMBINED)
				|| frequency.equals("WEEKLY")) {
			while (!start.equals(end)) {
				start.add(Calendar.DAY_OF_MONTH, 7);
				if (end.before(start)) {
					break;
				}
				i++;
			}
		} else if (frequency.equals(PaymentFrequency.BIWEEKLY) || frequency.equals(PaymentFrequency.BIWEEKLY_COMBINED)
				|| frequency.equals("BIWEEKLY")) {
			while (start.before(end) || !start.equals(end)) {
				start.add(Calendar.DAY_OF_MONTH, 14);
				if (end.before(start)) {
					break;
				}
				i++;
			}
		} else if (frequency.equals(PaymentFrequency.MONTHLY) || frequency.equals(PaymentFrequency.MONTHLY_COMBINED)
				|| frequency.equals("MONTHLY")) {
			while (!startDate.after(end) && !startDate.equals(end)) {
				startDate.add(Calendar.MONTH, 1);
				if (end.before(startDate)) {
					break;
				}
				i++;
			}
		} else if (frequency.equals(PaymentFrequency.QUARTERLY)
				|| frequency.equals(PaymentFrequency.QUARTERLY_COMBINED) || frequency.equals("QUARTERLY")) {
			while (!startDate.after(end) && !startDate.equals(end)) {
				startDate.add(Calendar.MONTH, 3);
				if (end.before(startDate)) {
					break;
				}
				i++;
			}
		} else if (frequency.equals(PaymentFrequency.SEMI_ANNUALLY)
				|| frequency.equals(PaymentFrequency.SEMI_ANNUALLY_COMBINED) || frequency.equals("SEMI_ANNUALLY")) {
			while (!startDate.after(end) && !startDate.equals(end)) {
				startDate.add(Calendar.MONTH, 6);
				if (end.before(startDate)) {
					break;
				}
				i++;
			}
		} else if (frequency.equals(PaymentFrequency.ANNUALLY) || frequency.equals(PaymentFrequency.ANNUALLY_COMBINED)
				|| frequency.equals("ANNUALLY")) {
			while (!start.equals(end)) {
				start.add(Calendar.YEAR, 1);
				if (end.before(start)) {
					break;
				}
				i++;
			}
		}
		return i;
	}
}
