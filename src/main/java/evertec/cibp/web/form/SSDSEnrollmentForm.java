package evertec.cibp.web.form;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.DeviceRequest;

import evertec.cibp.core.models.customer.CustomerAuthMethod;
import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.enrollment.EnrollableAccount;
import evertec.cibp.core.models.enrollment.EnrollmentInfo;
import evertec.cibp.core.models.enrollment.EnrollmentStatus;
import evertec.cibp.core.models.enrollment.EnrollmentType;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.CardPINAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.ConnectionInfoValidationRequest;
import evertec.cibp.core.services.requests.DepositAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.EnrollmentInformationRequest;
import evertec.cibp.core.services.requests.PayrollCardAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.PersonalInformationValidationRequest;
import evertec.cibp.core.services.responses.ConnectionInfoValidationResponse;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse.EnrollmentValidationStatus;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse.AccountValidationStatus;
import evertec.cibp.core.utils.InternationalCharacterUtils;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.form.bean.SSDSEnrollmentBean;
import evertec.cibp.web.form.validator.SSDSEnrollmentFormPageValidator;
import evertec.cibp.web.session.EnrollmentSession;
import evertec.cibp.web.utils.Utils;

public class SSDSEnrollmentForm extends AbstractWizardFormController implements GlobalEntitlementAwareController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private String cancelView;
	private CustomerServices customerService;

	public SSDSEnrollmentForm() {
		super();
		setCommandClass(SSDSEnrollmentBean.class);
		setCommandName("ssdsenrollment");
		setPages(new String[] { "ssdsenrollment", "ssdsenrollment" });
	}

	// This overrides the default validator and uses a multiple page aware
	// validator.
	@Override
	protected void validatePage(Object command, Errors errors, int page, boolean finish) {

		new SSDSEnrollmentFormPageValidator(page).validate(command, errors);

	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		EnrollmentSession eS = (EnrollmentSession) WebUtils.getSessionAttribute(request, "enrollmentSession");
		Map<String, Object> model = new HashMap<>();
		SSDSEnrollmentBean e = (SSDSEnrollmentBean) command;

		EnrollmentInfo ei = new EnrollmentInfo();
		ei.setPermId(e.getPermId());
		ei.setTaxId(eS.getValidatedTaxId());
		ei.setCreationDate(new Date());
		ei.setActivationDate(new Date());
		ei.setBirthDate(eS.getSubmitedDob().getTime());
		ei.setEmail(e.getEmail());
		ei.setFirstName(e.getFirstname());
		ei.setLastName(e.getLastname());
		ei.setUsername(e.getUsername().toLowerCase());
		int genereatedRandomSalt = PasswordHashUtil.genereateRandomSalt();

		String pass = "Bppr" + ei.getTaxId().substring(ei.getTaxId().length() - 4, ei.getTaxId().length());
		ei.setPasswordHash(PasswordHashUtil.generateSaltedHash(pass, genereatedRandomSalt));
		ei.setPasswordSalt(genereatedRandomSalt);
		if (eS.isFailed() || eS.getDobCounter() >= 3 || eS.getActCounter() >= 3 || eS.getActInfCounter() >= 3) {

			if (eS.getDobCounter() >= 3)
				ei.setDobFail(eS.getSubmitedDob().getTime());
			else if (eS.getActCounter() >= 3 || eS.getActInfCounter() >= 3) {
				ei.setAccountTypeFail(eS.getValidatedAcctType());
				ei.setAccountNumberFail(e.getAccountnumber());
				if (e.getAccountInfoType().equals("IDA"))
					ei.setAthNumberFail(e.getAthnumber());
			}

			ei.setInitialCustomerStatus(CustomerStatus.PENDING_CSR_ACTIVATION);
			e.setValidated(false);
			error.reject("enrollment.error.notexist", "Customer Information not found in our records.");
			return new ModelAndView("ssdsenrollment", model);

		} else {
			ei.setInitialCustomerStatus(CustomerStatus.ENROLLED);
		}
		ei.setEnrollmentSource(CustomerEnrollmentSource.SSDS);
		ei.setInitialAuthenticationMethod(CustomerAuthMethod.PASSWD_RSA);
		ei.setEnrollmentStatus(EnrollmentStatus.COMPLETE);
		ei.setEnrollmentAccountNumber(eS.getValidatedAcct());
		ei.setEnrollmentAccountSubType(eS.getValidatedAcctType());
		ei.setEnrollmentAccountProductId(eS.getValidatedAcctProductId());
		if (e.getAccountInfoType().equalsIgnoreCase("IDA"))
			ei.setEnrollmentPlasticNumber(e.getAthnumber());
		else
			ei.setEnrollmentPlasticNumber(e.getAthnumber());

		List<CustomerFlagType> flags = new LinkedList<CustomerFlagType>();
		flags.add(CustomerFlagType.ESIGN_ENFORCEMENT);
		ei.setCustomerFlagTypes(flags);

		PreferredLanguage lang;
		if ("en".equals(Utils.getRSALanguage(request)))
			lang = PreferredLanguage.ENGLISH;
		else
			lang = PreferredLanguage.SPANISH;

		EnrollmentInformationRequest enrollReq = new EnrollmentInformationRequest(null, false, ei,
				EnrollmentType.ACTIVE, null, null, null, null, null, null, null, false, true);

		Cookie cookie = WebUtils.getCookie(request, "PMData") != null ? WebUtils.getCookie(request, "PMData")
				: new Cookie("PMData", "");
		cookie.setMaxAge(31536000);
		cookie.setPath("/");

		DeviceRequest deviceRequest = new DeviceRequest();
		deviceRequest.setDevicePrint(null);
		deviceRequest.setIpAddress(request.getRemoteAddr());
		deviceRequest.setDeviceTokenCookie(cookie.getValue());
		deviceRequest.setDeviceTokenFSO(cookie.getValue());

		deviceRequest.setHttpAccept(request.getHeader("Accept"));
		deviceRequest.setHttpAcceptChars(request.getHeader("Accept-Charset"));
		deviceRequest.setHttpAcceptEncoding(request.getHeader("Accept-Encoding"));
		deviceRequest.setHttpAcceptLanguage(request.getHeader("Accept-Language"));
		deviceRequest.setHttpReferrer(request.getHeader("Referer"));
		deviceRequest.setUserAgent(request.getHeader("User-Agent"));

		enrollReq.setRsaLang(Utils.getRSALanguage(request));
		enrollReq.setDeviceRequest(deviceRequest);
		CustomerProfile profile = customerService.enrollUserNoRSA(enrollReq, lang);
		if (profile.hasFlag(CustomerFlagType.ESIGN_ENFORCEMENT)) {
			customerService.logEsignFlagAdded(Utils.generateCustomerInteractionEvent(request, profile.getUsername()));
		}

		cookie.setValue(deviceRequest.getDeviceTokenCookie());			
		ESAPI.httpUtilities().addCookie(response, cookie);

		model.put("confirm", "confirm");
		return new ModelAndView("ssdsenrollment", model);
	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		return new ModelAndView(cancelView);
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		EnrollmentSession enrollmentSession = (EnrollmentSession) WebUtils.getSessionAttribute(request,
				"enrollmentSession");
		SSDSEnrollmentBean enrollment = (SSDSEnrollmentBean) command;

		if (page == 0) {

			String taxId = enrollment.getTaxId();
			int year = enrollment.getDobyear();
			int month = enrollment.getDobmonth();
			int day = enrollment.getDobday();

			String accountnumber = enrollment.getAccountnumber();
			String athnumber = enrollment.getAthnumber();
			String accountType = enrollment.getAccountInfoType();
			ValidateAccountStatusResponse validateAccountInfo = null;

			boolean error = false;

			if (GenericValidator.isBlankOrNull(taxId) || !GenericValidator.matchRegexp(taxId, "\\d{9}")) {
				errors.rejectValue("taxId", "enrollment.taxid.invalid", "Invalid SSN Number");
				error = true;
			}

			if (accountType.equalsIgnoreCase("IDA")) {
				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{9}")) {
					errors.rejectValue("accountnumber", "enrollment.accountnumber.invalid", "Invalid account Number");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(athnumber) || !GenericValidator.matchRegexp(athnumber, "\\d{16}")) {
					errors.rejectValue("athnumber", "enrollment.athnumber.invalid", "Invalid ATH Number");
					error = true;
				}
			} else if (accountType.equalsIgnoreCase("CCA") || accountType.equalsIgnoreCase("AMEX")) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d+")) {
					errors.rejectValue("accountnumber", "enrollment.accountnumber.invalid", "Invalid account Number");
					error = true;
				}
			} else if (accountType.equalsIgnoreCase("PFC")) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{16}")) {
					errors.rejectValue("accountNumber", "enrollment.accountnumber.invalid", "Invalid Account");
					error = true;
				}
			}
			String rePwd1 = "^[a-zA-Z0-9]+$";
			String username1 = enrollment.getUsername();

			if (username1 == null || username1.equals("") || !username1.matches(rePwd1)
					|| (username1.length() < 6 || username1.length() > 20)) {
				errors.rejectValue("username", "enrollment.username.invalid", "Invalid Username");
				error = true;
			}

			if (error)
				return;

			GregorianCalendar dob = new GregorianCalendar(year, month, day);
			enrollmentSession.setSubmitedDob(dob);
			enrollmentSession.setValidatedTaxId(taxId);

			PersonalInformationValidationRequest personalRequest = new PersonalInformationValidationRequest(null,
					taxId, dob, enrollmentSession.isFailed(), false);
			PersonalInformationValidationResponse customerEnrollment = customerService
					.isEnrollableCustomer(personalRequest);
			EnrollmentValidationStatus status = customerEnrollment.getStatus();

			boolean isenroll = false;
			if (customerEnrollment.getExistingAccounts() != null)
				for (Iterator<EnrollableAccount> iterator = customerEnrollment.getExistingAccounts().iterator(); iterator
						.hasNext();) {
					EnrollableAccount type = (EnrollableAccount) iterator.next();
					if (type.getAccountId().equals(accountnumber)) {
						isenroll = true;
					}

				}

			// end enrollment. Customer exists on the database or counter
			// reached blocked count limit
			if (status.equals(PersonalInformationValidationResponse.EnrollmentValidationStatus.ALREADY_ENROLL)
					|| isenroll) {// customerEnrollment.isHasExistingOnlineAccount())
									// {
				errors.reject("enrollment.error.alreadyenrolled", "Already enrolled, please login");
				return;
			}
			else{
				enrollment.setAccounts(customerEnrollment.getEnrollableAccounts());
				enrollmentSession.setSubmitedDob(dob);
				enrollmentSession.setValidatedTaxId(taxId);
			}

			if (accountType.equalsIgnoreCase("IDA")) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{9}")) {
					errors.rejectValue("accountnumber", "enrollment.accountnumber.invalid", "Invalid account Number");
					error = true;
				}
				if (!error) {
					DepositAccountInformationValidationRequest depReq = new DepositAccountInformationValidationRequest(
							null, accountnumber, athnumber, taxId, null, false, true);
					validateAccountInfo = customerService.validateDepositAccountInfo(depReq);
				}

			} else if (accountType.equalsIgnoreCase("CCA") || accountType.equalsIgnoreCase("AMEX")) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d+")) {
					errors.rejectValue("accountnumber", "enrollment.accountnumber.invalid", "Invalid account Number");
					error = true;
				}

				if (!error) {
					CardPINAccountInformationValidationRequest cardPinReq = new CardPINAccountInformationValidationRequest(
							null, accountnumber, taxId, "", "", "", "", false, false, false);
					validateAccountInfo = customerService.validateCrediCardPINAccountInfo(cardPinReq);
				}

			} else if (accountType.equalsIgnoreCase("PFC")) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{16}")) {
					errors.rejectValue("accountNumber", "enrollment.accountnumber.invalid", "Invalid Account");
					error = true;
				}
				if (!error) {
					PayrollCardAccountInformationValidationRequest payrollReq = new PayrollCardAccountInformationValidationRequest(
							null, accountnumber, taxId, "", false);
					validateAccountInfo = customerService.validatePayrollCardAccountInfo(payrollReq);
				}
			}

			if (!error) {
				if (validateAccountInfo.getStatus() == AccountValidationStatus.VALID) {
					enrollmentSession.setValidatedAcct(accountnumber);
					enrollmentSession.setValidatedAcctType(accountType);
					enrollment.setPermId(validateAccountInfo.getPermId());
					enrollmentSession.setValidatedAcctProductId(validateAccountInfo.getAccountProductId());
				} else {
					enrollmentSession.setValidatedAcct(accountnumber);
					enrollmentSession.setValidatedAcctType(accountType);
				}

			} else {
				enrollmentSession.setValidatedAcct(accountnumber);
				enrollmentSession.setValidatedAcctType(accountType);
			}

			ConnectionInfoValidationRequest conectionRequest = new ConnectionInfoValidationRequest(null, username1,
					enrollment.getTaxId(), null, Utils.getRSALanguage(request), false);
			conectionRequest.setValidateInRsa(false);
			ConnectionInfoValidationResponse validateConnectionInformation = customerService
					.validateConnectionInformation(conectionRequest);
			if (!validateConnectionInformation.isUserNameValid()) {
				errors.rejectValue("username", "enrollment.username.invalid", "Invalid Username");
			}

		} else if (page == 1) {// aggrement session
			{

				if (GenericValidator.isBlankOrNull(enrollment.getAgreement())
						|| !enrollment.getAgreement().equalsIgnoreCase("agree")) {
					errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
				}

			}

		}

	}

	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {

		SSDSEnrollmentBean enrollment = new SSDSEnrollmentBean();

		String name = InternationalCharacterUtils.sanatizeInputs(request.getParameter("NAME"), true);
		String ssn = InternationalCharacterUtils.sanatizeInputs(request.getParameter("SSN"), true);
		String birth = GenericValidator.isDate(request.getParameter("BIRTHDATE"), "MM/dd/yyyy", false) ? request
				.getParameter("BIRTHDATE") : null;
		String account = InternationalCharacterUtils.sanatizeInputs(request.getParameter("ACCTNUM"), true);
		String ath = InternationalCharacterUtils.sanatizeInputs(request.getParameter("CARDNUM"), true);
		String lastname = "";

		if (ssn == null || birth == null)
			enrollment.setValidated(false);
		else
			enrollment.setValidated(true);

		if (ssn != null)
			enrollment.setTaxId(ssn);
		else
			ssn = enrollment.getTaxId();
		if (account != null)
			enrollment.setAccountnumber(account);
		else
			account = enrollment.getAccountnumber();
		if (ath != null)
			enrollment.setAthnumber(ath);
		else
			ath = enrollment.getAthnumber();

		String type = "IDA";
		if (account != null && account.length() > 15) {
			if (account.startsWith("0377")) {
				type = "AMEX";
			} else if (account.startsWith("6391") || account.startsWith("5045"))
				type = "PFC";
			else
				type = "CCA";
		}
		enrollment.setAccountInfoType(type);

		if (name != null && name.indexOf(" ") != -1) {
			enrollment.setFirstname(name.substring(0, name.indexOf(" ")).trim());
			lastname = name.substring(name.indexOf(" "), name.length()).trim();
			if (lastname.indexOf("-") != -1) {
				enrollment.setLastname(lastname.substring(0, lastname.indexOf("-")).trim());
				enrollment.setMaidenName(lastname.substring(lastname.indexOf("-") + 1, lastname.length()).trim());
			} else
				enrollment.setLastname(lastname.trim());

		} else if (name != null)
			enrollment.setFirstname(name);

		if (birth != null) {
			String[] birthdate = birth.split("/");
			if (birthdate.length == 3) {
				enrollment.setDobmonth(Integer.parseInt(birthdate[0]) - 1);
				enrollment.setDobday(Integer.parseInt(birthdate[1]));
				if (birthdate[2].length() <= 2)
					birthdate[2] = "19" + birthdate[2];
				enrollment.setDobyear(Integer.parseInt(birthdate[2]));
			}
		}

		return enrollment;
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		EnrollmentSession enrollmentSession = (EnrollmentSession) WebUtils.getSessionAttribute(request,
				"enrollmentSession");
		if (page == 0 && enrollmentSession == null) {
			enrollmentSession = new EnrollmentSession();
		} else {
			enrollmentSession = (EnrollmentSession) WebUtils.getSessionAttribute(request, "enrollmentSession");
			if (enrollmentSession == null) {
				errors.reject("", "Enrollment Session is null.  Please retry enrollment");
				return null;
			}
		}

		SSDSEnrollmentBean enrollment = (SSDSEnrollmentBean) command;
		Map<String, Object> model = new HashMap<>();

		if (enrollmentSession.isBlocked()) {
			model.put("blocked", enrollmentSession.isBlocked());
		} else if (!enrollment.isValidated()) {
			model.put("notvalidated", true);
		}

		WebUtils.setSessionAttribute(request, "enrollmentSession", enrollmentSession);

		// months
		Map<Object, String> months = new LinkedHashMap<Object, String>();
		SimpleDateFormat df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.MONTH, 0);
		for (int i = 0; i < 12; i++) {
			String month = df.format(cal.getTime());
			months.put(i, month);
			cal.add(Calendar.MONTH, 1);
		}
		model.put("months", months);

		// days
		Map<Object, String> days = new LinkedHashMap<Object, String>();
		df = new SimpleDateFormat("d");
		cal.set(Calendar.DAY_OF_MONTH, 1);
		for (int i = 1; i <= 31; i++) {
			String day = df.format(cal.getTime());
			days.put(i, day);
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		model.put("days", days);

		// years
		Map<Object, String> years = new LinkedHashMap<Object, String>();
		df = new SimpleDateFormat("yyyy");
		cal = GregorianCalendar.getInstance();
		for (int i = 0; i <= 100; i++) {
			String year = df.format(cal.getTime());
			years.put(Integer.parseInt(year), year);
			cal.add(Calendar.YEAR, -1);
		}
		model.put("years", years);

		model.put("page", Integer.toString(page));
		Map<String, String> agree = new HashMap<String, String>();
		agree.put("agree", "");
		model.put("agree", agree);

		String accountInfoType = enrollment.getAccountInfoType();
		if (GenericValidator.isBlankOrNull(accountInfoType))
			accountInfoType = "IDA";
		model.put("accountInfoType", accountInfoType);

		return model;
	}

	public String getCancelView() {
		return cancelView;
	}

	public void setCancelView(String cancelView) {
		this.cancelView = cancelView;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.ENROLLMENT;
	}
}
