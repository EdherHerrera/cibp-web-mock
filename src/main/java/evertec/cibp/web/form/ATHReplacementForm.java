package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;

import evertec.cibp.web.form.bean.AccountServiceBean;

public class ATHReplacementForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public ATHReplacementForm() {
		super();
		setCommandClass(AccountServiceBean.class);
		setCommandName("accountservices");
		setFormView("replaceath");
		setBindOnNewForm(true);
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		model.put("url", true);

		return showForm(request, error, "replaceath", model);

	}

	protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		Map<String, String> agree = new HashMap<String, String>();
		agree.put("agree", "");
		model.put("agree", agree);

		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		AccountServiceBean searchBean = (AccountServiceBean) command;

		if (GenericValidator.isBlankOrNull(searchBean.getAgreement())
				|| !searchBean.getAgreement().equalsIgnoreCase("agree")) {
			errors.rejectValue("agreement", "accountservices.agreement.error", "Please agree.");
		}
	}

}
