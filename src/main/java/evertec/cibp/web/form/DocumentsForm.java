package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.WordUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.DeviceRequest;

import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.documents.CustomerDocuments;
import evertec.cibp.core.models.documents.CustomerDocumentsPackage;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.CISCustomerInformation;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.facade.DocumentsFacade;
import evertec.cibp.web.facade.EmailNotificationFacade;
import evertec.cibp.web.form.bean.DocumentsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.requests.GetDocumentRequest;
import evertec.otherservices.responses.GetDocumentResp;

public class DocumentsForm extends AbstractWizardFormController implements MessageSourceAware {

	private int index = 0;
	private String graceMinutes;
	private MessageSource messageSource = null;
	protected CustomerServices customerService = null;
	protected final Log logger = LogFactory.getLog(getClass());
	private static String USERSESSION = "userSession";
	
	/** Id used to access CIS */
	private String seagullUserId;

	public DocumentsForm() {
		super();
		setCommandClass(DocumentsBean.class);
		setCommandName("mydocuments");
		setPages(new String[] { "mydocuments" });
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {
		
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request);
		
		if (userSession.isInformationChallenge()) {
			boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);
			if (oobFlag && userSession.getOobEnroll()) {

				if (userSession.getOobEnroll() && userSession.isOobAuthenticated()) {
					myDocuments(command, model, userSession);
					return model;
				} else {
					model.put("oobChallengeOpen", true);
					model.put("text", "direct2-");
				}
				userSession.setDeviceRequest(deviceRequest);
				List<ASK_FOR_OOB> listOOB = new ArrayList<>();
				listOOB.add(ASK_FOR_OOB.OOB_CHALLENGE);
				listOOB.add(ASK_FOR_OOB.MY_DOCUMENTS);
				userSession.setTasksListOOB(listOOB);
			}
			else{
				myDocuments(command, model, userSession);
			}
		}
		return model;
	}

	private void myDocuments(Object command, Map<String, Object> model, UserSession userSession) {
		DocumentsBean documentsBean = (DocumentsBean) command;
		
		List<CustomerDocumentsPackage> disbursementList = userSession.getDisbursementsList(false);
		
		// Orders the list to the most recent configuration set in CSR Tool
		for (CustomerDocumentsPackage disb : userSession.getDisbursementsList(false)) {
			disb.setCustomerDocuments(userSession.getOrderedDocsList(disb));
			
			//Verify if documents are available in the backend. Only verifies if
			//the flag is set to false
			if(disb.getCustomerDocuments()!= null 
					&& !disb.getCustomerDocuments().isEmpty()
					&& !disb.isDocumentsAvailable()){
				CustomerDocuments document = disb.getCustomerDocuments().get(0);
				GetDocumentRequest documentumRequest = new GetDocumentRequest();
				documentumRequest.setDocumentId(document.getDocSearchId());
				GetDocumentResp documentumResponse = DocumentsFacade.getInstance().getDocumentFile(documentumRequest,userSession);
				disb.setDocumentsAvailable(documentumResponse != null 
						&& documentumResponse.getDocumentByteArray() != null 
						&& documentumResponse.getDocumentByteArray().length >0);
				documentumResponse = null; //cleanup object
			}
		}

		userSession.setDisbursementsList(disbursementList);
		model.put("disbursementList", disbursementList);
		documentsBean.setDisbursementList(disbursementList);
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		DocumentsBean documentsBean = (DocumentsBean) command;

		String disbursementId = request.getParameter("dId");
		List<CustomerDocumentsPackage> list = userSession.getDisbursementsList(false);
		CustomerDocumentsPackage chosenDisbursement = this.getDisbursement(list, disbursementId);

		if (chosenDisbursement != null && chosenDisbursement.getCustomerDocuments() != null
				&& !chosenDisbursement.getCustomerDocuments().isEmpty()) {
			List<CustomerDocuments> oldChosenDocs = chosenDisbursement.getCustomerDocuments();

			if (documentsBean.getDocumentsAccepted() == null
					|| GenericValidator.isBlankOrNull(documentsBean.getDocumentsAccepted())
					|| documentsBean.getDocumentsAccepted().split(",").length != chosenDisbursement
							.getCustomerDocuments().size()) {

				errors.rejectValue("disbursementList[" + index + "].docPackId", "documents.accept.error",
						"You did not mark all of the documents.");
				documentsBean.setDocumentsAccepted("");
			} else {

				list = userSession.getDisbursementsList(true);
				chosenDisbursement = this.getDisbursement(list, disbursementId);
				if (chosenDisbursement != null && chosenDisbursement.getCustomerDocuments() != null
						&& !chosenDisbursement.getCustomerDocuments().isEmpty()) {

					// Verify if same documents as before
					if (oldChosenDocs.size() == chosenDisbursement.getCustomerDocuments().size()) {
						for (int i = 0; i < oldChosenDocs.size(); i++) {
							if (!(oldChosenDocs.get(i).getDocSearchId().equals(chosenDisbursement
									.getCustomerDocuments().get(i).getDocSearchId()))) {
								errors.rejectValue("disbursementList[" + index + "].docPackId",
										"documents.edited.error",
										"The documents changed. Please revise them again before accepting.");
								break;
							}
						}
					} else {
						errors.rejectValue("disbursementList[" + index + "].docPackId", "documents.edited.error",
								"The documents changed. Please revise them again before accepting.");
					}
				} else if (chosenDisbursement == null) {
					errors.rejectValue("error", "documents.deadline.error", "The deadline has been reached.");
				} else {
					errors.rejectValue("error", "documents.system.error", "An error has occured. Try again later.");
				}
			}
		} else {
			errors.rejectValue("error", "documents.system.error", "An error has occured. Try again later.");
		}
	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		String disbursementId = request.getParameter("dId");
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);

		CustomerDocumentsPackage disbursement = this.getDisbursement(userSession.getDisbursementsList(false),
				disbursementId);

		if(disbursement != null){
			boolean isSignatureSuccess = false;
			
			CISCustomerInformation custInfo = null;
			
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA818) || StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1801)) {
				custInfo = customerService.getCISCustomerInformation(userSession.getCustomerProfile().getPermId().toString(), this.seagullUserId);
			}
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1801)) {
				if(custInfo != null) {
					disbursement.setCustomerName(WordUtils.capitalizeFully(custInfo.getFullName().replace("-", " ")));
				}
			}
			
			//MBCA-818 Alexis G
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA818)) {
				if(custInfo != null) {					
					isSignatureSuccess = DocumentsFacade.getInstance().signDocuments(WordUtils.capitalizeFully(custInfo.getFullName().replace("-", " ")),disbursement, userSession);
				}else {
					logger.error("Flag MBCA818 was enabled but custInfo object was null use the old code.");
					isSignatureSuccess = DocumentsFacade.getInstance().signDocuments(userSession.getCustomerProfile().getFullName(),disbursement, userSession);
				}
			}else {
					isSignatureSuccess = DocumentsFacade.getInstance().signDocuments(userSession.getCustomerProfile().getFullName(),disbursement, userSession);
			}

			if (isSignatureSuccess) {
				Locale locale = "en".equalsIgnoreCase(Utils.getRSALanguage(request)) ? Locale.ENGLISH
						: new Locale("es");
				sendDocumentAcceptanceNotifications(userSession, disbursement);
				customerService.logAcceptDisbursementDocs(Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));
				userSession.setStatusmessage(messageSource.getMessage("documents.accept.success", null, "", locale));

				List<CustomerDocumentsPackage> updatedPackage = userSession.getDisbursementsList(true);
				if (updatedPackage != null && updatedPackage.isEmpty()) {
					CustomerFacade.getInstance().removeCustomerFlag(userSession.getCustomerProfile(), CustomerFlagType.DOCUMENTS_AVAILABLE, userSession);
					return new ModelAndView("redirect:portal");
				}
			}
		}
		return new ModelAndView("redirect:mydocuments");
	}

	private CustomerDocumentsPackage getDisbursement(List<CustomerDocumentsPackage> list, String disbursementId) {
		CustomerDocumentsPackage chosenDisbursement = null;

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getDocPackId().equals(disbursementId)) {
				chosenDisbursement = list.get(i);
				index = i;
				break;
			}
		}
		return chosenDisbursement;
	}

	/**
	 * Send Documents Acceptance notifications
	 * @param UsserSession
	 * @param CustomerDocumentsPackage
	 */
    public void sendDocumentAcceptanceNotifications(UserSession userSession, CustomerDocumentsPackage disbursement) {
		
    	EmailNotificationFacade alerts = EmailNotificationFacade.getInstance();
		
    	alerts.sendDocumentAcceptanceNotifications(userSession, disbursement);
	}
    
	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * @return the graceMinutes
	 */
	public String getGraceMinutes() {
		return graceMinutes;
	}

	/**
	 * @param graceMinutes
	 *            the graceMinutes to set
	 */
	public void setGraceMinutes(String graceMinutes) {
		this.graceMinutes = graceMinutes;
	}

	public String getSeagullUserId() {
		return seagullUserId;
	}

	public void setSeagullUserId(String seagullUserId) {
		this.seagullUserId = seagullUserId;
	}
}
