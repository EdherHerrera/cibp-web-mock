package evertec.cibp.web.form;

import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.factory.TVAccountFactory;
import bppr.tv.beans.accounts.types.DepositAccount;
import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.VendorReceiptHistory;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.customer.oob.CustomerOOBChallengeType;
import evertec.cibp.core.models.entitlement.BillPayEntitlement;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.holiday.Holiday;
import evertec.cibp.core.models.payee.GlobalPayeePaymentType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.FavoritePaymentComparator;
import evertec.cibp.core.models.payment.TVAccountFavoritePayment;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.adaptiveauth.RsaChallengeRequest;
import evertec.cibp.core.services.requests.adaptiveauth.ValidateOobChallengeRequest;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.BankingDaysUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.form.bean.EvendorBean;
import evertec.cibp.web.form.bean.MultiplePaymentBean;
import evertec.cibp.web.form.bean.PaymentBean;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.PaymentSortbyAccount;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.UserAccount;
import evertec.rsa.RSAConnector.Lang;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class MakeVendorForm extends AbstractWizardFormController
		implements GlobalEntitlementAwareController, MessageSourceAware {

	/**
	  * cancelView field
	  */
	private String cancelView;
	protected final Log logger = LogFactory.getLog(getClass());
	protected CustomerServices customerService;
	
	/**
	  * messageSource field
	  */
	private MessageSource messageSource;
	
	/**
	  * productId field
	  */
	private String[] productId;
	
	/**
	  * telenominaID field
	  */
	private String telenominaID;
	
	/**
	  * evendorId field
	  */
	private String evendorId;

	public MakeVendorForm() {
		super();
		setCommandClass(EvendorBean.class);
		setCommandName("vendorList");
		setPages(new String[] { "makeVendor", "makeVendor", "makeVendor" });
	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object vendorList,
			BindException error) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		Map<String, Object> model = new HashMap<>();
		List<TVPayment> confirmPayments = new LinkedList<>();
		List<TVPayment> errorPayments = new LinkedList<>();

		if (userSession.isOobRedirectToLogin()) {
	
			return new ModelAndView("redirect:login");

		} else if (userSession.isOobRedirectToBlock()) {
			
			return new ModelAndView("redirect:login?username=" + userSession.getUsername());
		}

		userSession.setOobAuthenticated(true);
		request.getSession().removeAttribute("oobCode");
		///// OOB

		List<TVFrontEndPayee> payees = userSession.getPayessBasicEbpp(true);
		BigDecimal totalcomplete = BigDecimal.valueOf(0.0d);
		BigDecimal totalfail = BigDecimal.valueOf(0.0d);

		EvendorBean vendorListAux = (EvendorBean) vendorList;
		PaymentSortbyAccount paymentsort = new PaymentSortbyAccount();
		boolean paymentsDone = false;
		Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsPaymentFrom();
		model.put("statusPayrollApp", userSession.getPayrollAppStatus());
		model.put("OobEnroll", userSession.getOobEnroll());

		if (vendorListAux.getVendors() != null) {
			for (Iterator<PaymentBean> iterator = vendorListAux.getVendors().iterator(); iterator.hasNext();) {
				PaymentBean payAux = iterator.next();
				if (payAux.getAmount() == null)
					continue;

				String accountFrom = "";
				if (!vendorListAux.getSingleAccountVendorFrom().equalsIgnoreCase("none")
						&& !vendorListAux.getSingleAccountVendorFrom().equalsIgnoreCase("multiple")) {
					accountFrom = vendorListAux.getSingleAccountVendorFrom();
				} else {
					accountFrom = payAux.getAccountFrom();
				}

				TVAccountTransferSource source = null;

				if (accountFrom.startsWith("PIF")) {
					String externalAccountId = accountFrom.substring(3);
					PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
					if (externalAccount != null) {
						BankInformation bankInformation = new BankInformation();
						bankInformation.setBankId(externalAccount.getRoutingNumber());
						bankInformation.setBankName(externalAccount.getBankName());
						CustomerAccountPreferences pref = new CustomerAccountPreferences();
						TVAccountFactory accountFactory = new TVAccountFactory();
						Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
								externalAccount.getAccountNumber());
						sourceAccount.setAccountID(externalAccount.getAccountNumber());
						sourceAccount.setSubType(Character.toString(externalAccount.getAccountType()));
						((DepositAccount) sourceAccount)
								.setSectionIndicator(Character.toString(externalAccount.getAccountType()));
						TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
						fromAccount.setCustomerAccountPreferences(pref);
						fromAccount.setNickname(externalAccount.getNickname());
						fromAccount.setHide(externalAccount.getShow() == '0');
						fromAccount.getTVAccount().setBankInformation(bankInformation);
						source = new TVAccountTransferSource(fromAccount);
					}
				} else {
					source = Utils.getAccountSource(accountFrom, accountsFrom);
				}

				TVFrontEndPayee target = Utils.getPayeebyId(payAux.getPayeeid(), payees);

				if (source == null || target == null)
					continue;

				BigDecimal amount = payAux.getAmount();

				Calendar cal = Calendar.getInstance();
				cal.setTime(payAux.getEffectiveDate());

				TVPayment payment = null;

				String recurrent = payAux.getRecurrent();
				String infinite = payAux.getInfinite();

				if (recurrent != null && recurrent.equalsIgnoreCase("recurrent")) {
					Integer inst = payAux.getInstances();

					if (infinite != null && infinite.equalsIgnoreCase("infinite")) {
						inst = 999;
					}
					// else infinite == byinstances
					payment = new TVPayment(amount, payAux.getFrequency(), inst, cal, source, target);
				} else
					payment = new TVPayment(amount, Frequency.MANUALLY, 0, cal, source, target);

				// CIBP-2548 JOCAMPO
				if (payment.getPayee().getGlobalPayeeIdString() != null
						&& payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
					payment.getPayee().setTelenomina(true);
				} else {
					payment.getPayee().setTelenomina(false);
				}
				// mpicado, CIBP-1799
				PaymentResponse res = PaymentsServiceModule.getInstance().sendPayments(customerProfile, userSession,
						payment, Utils.generateCustomerInteractionEvent(request), customerService);
				payment = res.getPayment();
				payment.getTVPayment().setEffectiveDate(cal);
				payment.setVendorSubject(payAux.getSubject());
				payment.setEmail(payAux.getEmail());
				payment.setComments(payAux.getComments());
				payment.setInvoiceNumber(payAux.getInvoiceNumber());
				payment.setEmployeeName(target.getEmployeeName());
				payment.setReceivingDate(Utils.getEstimateDate(customerService.getHolidays(), payment.getTVPayment().getEffectiveDate(), target.getGlobalPayee().getPaymentType()));

				if (target.getGlobalPayee() != null && target.getGlobalPayee().getPaymentType() != null) {
					if (target.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO
							|| target.getGlobalPayee()
									.getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PUBLICO)
						payment.setEstimateDate("CHECK");
					else
						payment.setEstimateDate(Utils.getEstimateDate(customerService.getHolidays(), cal,
								target.getGlobalPayee().getPaymentType()));
				} else
					payment.setEstimateDate(
							Utils.getEstimateDate(customerService.getHolidays(), cal, GlobalPayeePaymentType.INTERNO));

				if (res.getStatus() == ResultStatus.SUCCESS) {

					if (res.getPayment().getPayee().isHasNewEbill()) {
						customerService.updateAccountPayment(userSession.getEbpp2UserId(),
								res.getPayment().getPayee().getEbpp2UserAccount().getBillerAccountID());
						// Alizano hide new ebill message when payment is
						// success Fix Incident 1537231
						res.getPayment().getPayee().setHasNewEbill(false);
						if (res.getPayment().getPayee() != null
								&& res.getPayment().getPayee().getEbpp2UserAccount() != null) {
							res.getPayment().getPayee().getEbpp2UserAccount().setLastEbillPaid(true);
						}
						// End Fix
					}
					confirmPayments.add(payment);
					totalcomplete = totalcomplete.add(payment.getAmount());
					/*
					 * mpicado, CIBP-1799, esto se debe validar porque esto va a
					 * la base de datos de EBPP, y sin la condicion la
					 * aplicacion se va a caer si no tiene acceso a EBPP
					 */
					if (this.getEBPPMessageSourceStatus() && customerService
							.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {

						// The EBPP2 user id is not yet in session; add it
						if (!userSession.isEbpp2UserIdLookedUp()) {
							EbppEntitlement ent = null;
							if (customerProfile.getEntitlementByType(EntitlementType.EBILLS) != null) {
								ent = new EbppEntitlement(customerProfile.getEntitlementByType(EntitlementType.EBILLS));

								// If the username is blank, we need to look in
								// the current EBPP accounts ONLY
								if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
									userSession.setEbpp2UserId("");
								} else {
									userSession.setEbpp2UserId(ent.getUsername());
								}
								userSession.setEbpp2UserIdLookedUp(true);
							}
						}

						// The user has an EBPP2 ID
						if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

							// Find the account in the user accounts list
							List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

							// Loop through all the accounts and send the update
							// payment message if necessary
							for (Iterator<UserAccount> iterator2 = ebpp2UserAccounts.iterator(); iterator2.hasNext();) {
								UserAccount userAccount = iterator2.next();

								if (userAccount.getAccountNumber().replaceFirst("^0+(?!$)", "")
										.equals(target.getBillingAccount().replaceFirst("^0+(?!$)", ""))) {

									if (!userAccount.isLastEbillPaid()) {
										customerService.updateAccountPayment(userSession.getEbpp2UserId(),
												userAccount.getBillerAccountID());
										// Alizano hide new ebill message when
										// payment is success Fix Incident
										// 1537231
										res.getPayment().getPayee().setHasNewEbill(false);
										if (res.getPayment().getPayee() != null
												&& res.getPayment().getPayee().getEbpp2UserAccount() != null) {
											res.getPayment().getPayee().getEbpp2UserAccount().setLastEbillPaid(true);
										}
										// End Fix
									}
									break;
								}
							}
						}
					}
					
					if(payAux.getCheckSendEmail() != null && payAux.getCheckSendEmail().equalsIgnoreCase("on")){
						
							
						//Save Vendor Receipt
						VendorReceiptHistory vendorReceipt = new VendorReceiptHistory();
						vendorReceipt.setAccountNumber(payment.getSourceNickname()+' '+customerProfile.getEnrollmentAccountNumber().substring(customerProfile.getEnrollmentAccountNumber().length() - 4,
								customerProfile.getEnrollmentAccountNumber().length()));
						vendorReceipt.setAmount(payment.getAmount());
						vendorReceipt.setEmail(payment.getEmail());
						vendorReceipt.setComments(payment.getComments());
						vendorReceipt.setEffectiveDate(payment.getEstimateDate());
						vendorReceipt.setInvoiceNumber(payment.getInvoiceNumber());
						vendorReceipt.setConcept(payment.getVendorSubject());
						vendorReceipt.setReferenceNumber(payment.getReferenceNumber());
						vendorReceipt.setTaxId(customerProfile.getTaxId());
						vendorReceipt.setProfileId(customerProfile.getProfileId());
						vendorReceipt.setDate(payment.getEffectiveDate());
						vendorReceipt.setCustomerName(customerProfile.getFirstName());
									
						if (!GenericValidator.isBlankOrNull(payment.getFrequency())) {	
							vendorReceipt.setFrequency(Utils.getEmailFrequency(payment.getRemainingInstances(), payment.getFrequency(), messageSource, request));
						}else{
							vendorReceipt.setFrequency(messageSource.getMessage("freq.one.Payment", null, new Locale(Utils.getRSALanguage(request))));
						}
							
						
						// Send Vendor Email Payment to recipient 
						customerService.sendVendorPaymentRecipientConf(customerProfile, vendorReceipt, userSession.getPreferredLanguage()); 
						customerService.saveVendorReceiptHistory(vendorReceipt, customerProfile, userSession.getPreferredLanguage());
					}
				} else {
					/*
					 * CIBP-1680 bgarcia Se agrega la logica para poner el
					 * detalle del mensaje
					 */
					String er = getTelepagoErrorDescriptor(res.getErrorEBA(),
							Utils.getRSALanguage(request).toLowerCase());
					if (er == null || er.equals("")) {
						payment.setPaymentMessage(res.getErrorMessage());
					} else {
						payment.setPaymentMessage(er + " - " + res.getErrorEBA());
					}
					errorPayments.add(payment);
					totalfail = totalfail.add(payment.getAmount());
				}
				

			} // Fin del while del Iterator

			for (Iterator<TVPayment> iterator = confirmPayments.iterator(); iterator.hasNext();) {
				TVPayment payment = iterator.next();
				paymentsort.addPayment(payment);

			}
			for (Iterator<TVPayment> iterator = errorPayments.iterator(); iterator.hasNext();) {
				TVPayment payment = iterator.next();
				paymentsort.addPayment(payment);
			}

			boolean errorpay = false;
			model.put("paymentsort", paymentsort.getElementMap());

			if (confirmPayments != null && !confirmPayments.isEmpty()) {
				model.put("confirmPayments", confirmPayments);
				model.put("totalConfirm", totalcomplete);
			}
			if (errorPayments != null && !errorPayments.isEmpty()) {
				model.put("errorPayments", errorPayments);
				model.put("totalFail", totalfail);
				errorpay = true;
			}
			if (!errorpay) {
				if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request))) {
					paymentsDone = true;
					userSession.setStatusmessage(messageSource.getMessage("commercial.business.vendor.confirmationpayment", null, new Locale("EN")));
				} else {
					paymentsDone = true;
					userSession.setStatusmessage(messageSource.getMessage("commercial.business.vendor.confirmationpayment", null, new Locale("ES")));
				}
			} else {
				if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request))) {
					paymentsDone = true;
					userSession.setStatusmessage("Please verify errors in payments");
				} else {
					userSession.setStatusmessage("Tienes errores en pagos");
					paymentsDone = true;
				}
			}
		}

		// Send Email Payment Vendor Confirmation
		customerService.sendVendorPaymentConfirmation(customerProfile, confirmPayments, errorPayments,
				userSession.getPreferredLanguage(), userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)));
		
		
		model.put("cantEvendors", confirmPayments.size() + errorPayments.size());
		model.put("statusmessage", userSession.getStatusmessage());
		userSession.setStatusmessage("");
		model.put("paymentsDone", paymentsDone);
		model.put("page", "2");
		// Set RSA cookie
		Cookie cookie = WebUtils.getCookie(request, "PMData") != null ? WebUtils.getCookie(request, "PMData")
				: new Cookie("PMData", "");
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		ESAPI.httpUtilities().addCookie(response, cookie);

		return new ModelAndView("makeVendor", model);
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		Map<String, Object> model = new HashMap<>();
		EvendorBean vendorList = (EvendorBean) command;
		Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsPaymentFrom();
		List<TVFrontEndPayee> payees = userSession.getPayessBasicEbpp(true);
		List<TVAccountFavoritePayment> vendors = userSession.getVendorPayments();
		model.put("MBSFE_138", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138));
		model.put("telenominaID", telenominaID);
		model.put("evendorId", evendorId);
		model.put("page", page);
		int cantVendors = 0;
		if (page == 0 || page == 1) {
			// Set Holidays
			
			if(!GenericValidator.isBlankOrNull(userSession.getStatusmessage())){
				model.put("showStatusMessage", true);
				model.put("statusmessage", userSession.getStatusmessage());
				userSession.setStatusmessage("");
			}
			
			Map<String, String> accountsFromVendorMap = new LinkedHashMap<>();
			Map<String, Object> h = new LinkedHashMap<>();
			List<Holiday> holidays = customerService.getHolidays();
			if (holidays != null)
				for (Holiday holiday : holidays) {
					h.put(holiday.getId(),
							new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(holiday.getDate().getTime()));
				}
			model.put("holidays", h);

			AccountMaskUtil mask = new AccountMaskUtil();
			NumberTool numberTool = new NumberTool();
			if (userSession.getCustomerProfile().getUserInterface() != null) {
				if (userSession.getCustomerProfile().getUserInterface().equals(UserInterface.COMMERCIAL)) {
					Set<TVAccountTransferSource> accountsVendorFrom = userSession.getAccountsPaymentFrom();
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
						accountsFromVendorMap.put("none", "Select an account");
					else
						accountsFromVendorMap.put("none", "Selecciona una cuenta");

					for (Iterator<TVAccountTransferSource> iterator = accountsVendorFrom.iterator(); iterator
							.hasNext();) {
						TVAccountTransferSource acctfrom = iterator.next();
						TVFrontendAccount accfront = acctfrom.getAccount();
						if (!accfront.getHide()) {
							if (accfront != null && accfront.getPortalBalance() != null && accfront.getSubtype() != null
									&& isCommercialAccount(accfront)) {
								/**
								 * TODO pasar este valor a un properties 021
								 **/
								accountsFromVendorMap.put(acctfrom.getFrontEndId(),
										StringUtils.abbreviate(accfront.getNickname(), 15) + " "
												+ mask.mask(accfront.getAccountNumber()) + " "
												+ numberTool.format("currency", accfront.getPortalBalance())
												+ ((accfront.getAccountNumberSuffix() != null
														&& !accfront.getAccountNumberSuffix().equals(""))
																? " " + accfront.getAccountNumberSuffix() : ""));
							}
						}
					}
					model.put("singleAccountVendorFrom", accountsFromVendorMap);
					if (accountsFromVendorMap.size() > 2) {
						model.put("hasFavoriteAccount", true);
					} else {
						model.put("hasFavoriteAccount", false);
					}
				}
			}

			Map<String, String> payeeMap = new LinkedHashMap<>();
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				payeeMap.put("none", "Select a payee");
			} else {
				payeeMap.put("none", "Selecciona un comercio");
			}
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				payeeMap.put("add", "Add a new payee");
			} else {
				payeeMap.put("add", "A&ntilde;adir un comercio");
			}
			Map<String, String> frequencies = new LinkedHashMap<>();
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				frequencies.put("variable", "one payment");
				frequencies.put("recurrent", "recurrent payment");
			} else {
				frequencies.put("variable", "una vez");
				frequencies.put("recurrent", "recurrente");
			}

			if (page == 0) {

				model.put("epayroll_ach_efectivedate",
						StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLLENHA));
				model.put("payeesMap", payeeMap);
				if (vendors != null && !vendors.isEmpty())
					Collections.sort(vendors, new FavoritePaymentComparator());
				model.put("favoriteVendors", vendors);

				if (vendors != null)
					cantVendors = vendors.size();
				model.put("cantEvendors", cantVendors);
				model.put("frequencymap", frequencies);

				// Set up today's date
				SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				Calendar n = calculateEffDate(Calendar.getInstance());

				model.put("todayDate", df.format(n.getTime()));
				df = new SimpleDateFormat("MM");
				model.put("month", df.format(n.getTime()));
				df = new SimpleDateFormat("dd");
				model.put("day", df.format(n.getTime()));

				if (n.get(Calendar.YEAR) - Calendar.getInstance().get(Calendar.YEAR) >= 1) {
					model.put("dateMismatch", true);
				}

				// TODO verificar si existe una cuenta defatul para pagos y
				// ponerla como valor del singleaccountfrom
				if ((vendorList.getSingleAccountVendorFrom() == null
						|| vendorList.getSingleAccountVendorFrom().equals(""))
						&& customerProfile.hasEntitlement(EntitlementType.BILLPAY)) {
					BillPayEntitlement ent = new BillPayEntitlement(
							customerProfile.getEntitlementByType(EntitlementType.BILLPAY));
					if (ent.getDetailView())
						model.put("showDetail", true);
					else
						model.put("showDetail", false);

					if (ent.getAccountKey() != null && !ent.getAccountKey().equals("")) {
						String defaultaccountkey = ent.getAccountKey();
						if (defaultaccountkey != null && defaultaccountkey.equalsIgnoreCase("multiple"))
							vendorList.setSingleAccountVendorFrom("multiple");
						else {
							TVAccountTransferSource defaultAccount = Utils.getAccountSourceByActKey(defaultaccountkey,
									accountsFrom);
							if (defaultAccount != null)
								vendorList.setSingleAccountVendorFrom(defaultAccount.getFrontEndId());
						}
					}
				}
				if (page == 1) {
					model.put("confirm", "true");
					model.put("fromaccounts", accountsFromVendorMap);
					model.put("payeesMap", payeeMap);
					model.put("favoriteVendors", vendors);
					model.put("frequencymap", frequencies);

					int size = 3;
					model.put("listSize", size);
					model.put("todayDate", "");

				}

			}
			if (page == 1) {
				/// OOB
				DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
						userSession.getDeviceRequest().getDeviceTokenCookie(), request);
				RsaChallengeRequest rsaChallengeRequest = new RsaChallengeRequest(userSession.getUsername(), deviceRequest);
				CredentialType credentialType = customerService.rsaGetChallengeMethod(rsaChallengeRequest);
				userSession.setDeviceRequest(deviceRequest);
	
				if (credentialType.equals(CredentialType.OOBPHONE)) {
					model.put("challengeType", CustomerOOBChallengeType.OOBPHONE.toString());
				} else {
					model.put("challengeType", CustomerOOBChallengeType.OOBSMS.toString());
				}

			
				Map<String, Object> modelReturn = Utils.oobCommercialChallenge(request, command, errors, page);
				model.put("phone", modelReturn.get("phone"));
				model.put("devicetoken", modelReturn.get("devicetoken"));
				model.put("challengeType", modelReturn.get("challengeType"));
				model.put("from", modelReturn.get("from"));
				model.put("hasAltPhone", modelReturn.get("hasAltPhone"));
				model.put("altphone", modelReturn.get("altphone"));
				model.put("codeSent", modelReturn.get("codeSent"));
				model.put("code", modelReturn.get("code"));
				model.put("callMade", modelReturn.get("callMade"));
				model.put("redirectToBlock", modelReturn.get("redirectToBlock"));
				model.put("username", userSession.getUsername());
			}
			/// OOB

		} // End Page 0 || 1

		if (page == 1) {
			cantVendors = 0;
			PaymentSortbyAccount paymentsort = new PaymentSortbyAccount();

			List<MultiplePaymentBean> multipleaccounts = new LinkedList<MultiplePaymentBean>();
			if (vendorList.getSingleAccountVendorFrom().equalsIgnoreCase("multiple")) {
				model.put("hasmultipleaccounts", "true");
			}

			BigDecimal totalverify = BigDecimal.valueOf(0.0);

			for (Iterator<PaymentBean> iterator = vendorList.getVendors().iterator(); iterator.hasNext();) {
				PaymentBean payment = iterator.next();
				if (payment.getAmount() == null) {
					continue;
				}
				String accountFrom = "";

				if (!vendorList.getSingleAccountVendorFrom().equalsIgnoreCase("none")
						&& !vendorList.getSingleAccountVendorFrom().equalsIgnoreCase("multiple")) {
					accountFrom = vendorList.getSingleAccountVendorFrom();
				} else {
					accountFrom = payment.getAccountFrom();
				}
				TVAccountTransferSource source = null;

				if (accountFrom.startsWith("PIF")) {
					String externalAccountId = accountFrom.substring(3);
					PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
					if (externalAccount != null) {
						BankInformation bankInformation = new BankInformation();
						bankInformation.setBankId(externalAccount.getRoutingNumber());
						bankInformation.setBankName(externalAccount.getBankName());
						CustomerAccountPreferences pref = new CustomerAccountPreferences();
						TVAccountFactory accountFactory = new TVAccountFactory();
						Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
								externalAccount.getAccountNumber());
						sourceAccount.setAccountID(externalAccount.getAccountNumber());
						sourceAccount.setSubType(Character.toString(externalAccount.getAccountType()));
						((DepositAccount) sourceAccount)
								.setSectionIndicator(Character.toString(externalAccount.getAccountType()));
						TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
						fromAccount.setCustomerAccountPreferences(pref);
						fromAccount.setNickname(externalAccount.getNickname());
						fromAccount.setHide(externalAccount.getShow() == '0');
						fromAccount.getTVAccount().setBankInformation(bankInformation);
						source = new TVAccountTransferSource(fromAccount);
					}
				} else {
					source = Utils.getAccountSource(accountFrom, accountsFrom);
				}

				TVFrontEndPayee target = Utils.getPayeebyId(payment.getPayeeid().trim(), payees);
				if (source == null || target == null)
					continue;

				BigDecimal amount = payment.getAmount();

				Calendar cal = Calendar.getInstance();
				cal.setTime(payment.getEffectiveDate());
				cal = calculateEffDate(cal);
				payment.setEffectiveDate(cal.getTime());

				TVPayment paymentAux = null;
				String recurrent = payment.getRecurrent();
				String infinite = payment.getInfinite();

				if (recurrent != null && recurrent.equalsIgnoreCase("recurrent")) {

					Integer inst = payment.getInstances();

					if (inst == null)
						inst = Integer.valueOf(0);

					if (infinite != null && infinite.equalsIgnoreCase("infinite")) {
						inst = 999;
					}

					paymentAux = new TVPayment(amount, payment.getFrequency(), inst, cal, source, target);
				} else {
					paymentAux = new TVPayment(amount, Frequency.MANUALLY, Integer.valueOf(0), cal, source, target);
				}

				if (target.getGlobalPayee() != null && target.getGlobalPayee().getPaymentType() != null) {
					if (target.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO
							|| target.getGlobalPayee()
									.getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PUBLICO)
						paymentAux.setEstimateDate("CHECK");
					else
						paymentAux.setEstimateDate(Utils.getEstimateDate(customerService.getHolidays(), cal,
								target.getGlobalPayee().getPaymentType()));
				} else {
					paymentAux.setEstimateDate(
							Utils.getEstimateDate(customerService.getHolidays(), cal, GlobalPayeePaymentType.INTERNO));
				}

				totalverify = totalverify.add(paymentAux.getAmount());
				paymentsort.addPayment(paymentAux);
				cantVendors++;

			}

			model.put("paymentsort", paymentsort.getElementMap());
			model.put("totalAmount", totalverify);
			model.put("multipleaccounts", multipleaccounts);
			model.put("cantEvendors", cantVendors);
		}

		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		if (page == 0 || page == 1) {
			EvendorBean vendorList = (EvendorBean) command;

			if (vendorList.getSingleAccountVendorFrom() != null) {
				if (vendorList.getSingleAccountVendorFrom().equalsIgnoreCase("none")) {
					errors.rejectValue("singleAccountVendorFrom", "commercial.business.select.required",
							"Please select the account registered in Business Payments.");
				}
			}

			int i = 0;
			int m = 0;
			int noamount = 0;

			for (Iterator<PaymentBean> iter = vendorList.getVendors().iterator(); iter.hasNext();) {
				PaymentBean element = (PaymentBean) iter.next();
				if (element.getAmount() == null) {
					m++;
					noamount++;
					continue;
				}

				BigDecimal maxpayamount = BigDecimal.valueOf(999999.99);
				if (element.getAmount().compareTo(maxpayamount) > 0) {
					errors.rejectValue("vendors[" + m + "].amount", "payments.amount.largermax",
							"Amount larger than max");

				} else if (element.getAmount().doubleValue() < new Double(0.01d)) {
					errors.rejectValue("vendors[" + m + "].amount", "payments.amt.invalid", "Invalid");

				}

				if ("none".equalsIgnoreCase(vendorList.getSingleAccountVendorFrom())
						|| "multiple".equalsIgnoreCase(vendorList.getSingleAccountVendorFrom())) {
					if ("none".equalsIgnoreCase(element.getAccountFrom())) {
						errors.rejectValue("vendors[" + m + "].accountFrom", "payments.acctfrom.required",
								"Account From is required");

					}
				}

				if ("none".equalsIgnoreCase(element.getPayeeid())) {
					errors.rejectValue("vendors[" + m + "].payeeid", "payments.payee.invalid", "Payee is required");

				}

				if (element.getRecurrent() == null || "variable".equalsIgnoreCase(element.getRecurrent()))
					element.setFrequency(Frequency.MANUALLY);

				if (element.getCheckSendEmail() != null && element.getCheckSendEmail().equalsIgnoreCase("on")) {
					if (GenericValidator.isBlankOrNull(element.getEmail())) {
						errors.rejectValue("vendors[" + m + "].email", "", "Email Requerido");
					}
					if (GenericValidator.isBlankOrNull(element.getInvoiceNumber())) {
						errors.rejectValue("vendors[" + m + "].invoiceNumber", "", "Invoice Number Requerido");
					}
				}

				Date effectiveDate = element.getEffectiveDate();
				Calendar today = Calendar.getInstance();

				// Set the max date (one year in future)
				Calendar thresholdDate = Calendar.getInstance();
				thresholdDate.add(Calendar.YEAR, 1);

				if (effectiveDate != null) {
					// Validate no future dates
					Calendar n = Calendar.getInstance();
					n.setTime(effectiveDate);

					if ((n.get(Calendar.YEAR) < today.get(Calendar.YEAR))
							|| n.get(Calendar.YEAR) == today.get(Calendar.YEAR)
									&& n.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR)) {
						errors.rejectValue("vendors[" + m + "].effectiveDate", "payments.date.past",
								"Invalid Date.<br>(Day in the past)");
					} else if (n.after(thresholdDate)) {
						errors.rejectValue("vendors[" + m + "].effectiveDate", "payments.date.future",
								"Invalid Date.<br>(Date more than one year in future)");
					}

				} else {
					errors.rejectValue("vendors[" + m + "].effectiveDate", "payments.date.required", "Required");

				}

				if ("recurrent".equalsIgnoreCase(element.getRecurrent())) {
					// Make sure that a frequency is selected
					if (element.getFrequency() == null) {
						errors.rejectValue("vendors[" + m + "].frequency", "payments.frequency.required",
								"Frequency is required");

					}
					// Make sure that a frequency type is required
					if (GenericValidator.isBlankOrNull(element.getInfinite())) {
						errors.rejectValue("vendors[" + m + "].infinite", "payments.freqtype.required",
								"Frequency type is required");

					} else if (!element.getInfinite().equalsIgnoreCase("infinite")) {
						if (element.getInstances() == null) {
							errors.rejectValue("vendors[" + m + "].instances", "payments.instances.required",
									"Instances required");

						} else if (element.getInstances() < 2) {
							errors.rejectValue("vendors[" + m + "].instances", "payments.amtinstances.required",
									"Instances must be 2 or more");

						}

					}
				}
				m++;
			}

			if ((m - noamount + i) == 0)
				errors.rejectValue("singleAccountVendorFrom", "payments.pmtamount.required",
						"Please select a payment and amount");

			if (errors.hasErrors()) {
				UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
					session.setStatusmessage("Please verify the information below");
				else
					session.setStatusmessage("Verifica la informaci&oacute;n entrada");
			}

		}

		/////////////////// OOB
		EvendorBean bean = (EvendorBean) command;
		EvendorBean.ACTION action = bean.getAction();

		bean.setTimeout(false);
		bean.setValidationError(false);

		if (userSession.isOobRedirectToBlock()) {
			return;
		}

		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);

		// Validate SMS & voice call challenges

		if ((page == 1) && (action == EvendorBean.ACTION.VALIDATE_SMSCODE
				|| action == EvendorBean.ACTION.VALIDATE_CALLCODE || action == EvendorBean.ACTION.VALIDATE_ALTSMSCODE
				|| action == EvendorBean.ACTION.VALIDATE_ALTCALLCODE)) {

			ValidateOobChallengeRequest validateRequest = new ValidateOobChallengeRequest();
			validateRequest.setUsername(userSession.getUsername());
			validateRequest.setDeviceRequest(deviceRequest);
			validateRequest.setRsaSessionId(userSession.getRsaSessionId());
			validateRequest.setRsaTransactionId(userSession.getRsaTransactionId());
			validateRequest.setDeviceTokenCookie(deviceRequest.getDeviceTokenCookie());
			validateRequest.setAnswer(bean.getCode());

			if (action == EvendorBean.ACTION.VALIDATE_SMSCODE || action == EvendorBean.ACTION.VALIDATE_ALTSMSCODE) {
				validateRequest.setCredentialType(CredentialType.USER_DEFINED);
				validateRequest.setBindDevice(true);
			} else {
				validateRequest.setCredentialType(CredentialType.OOBPHONE);
				validateRequest.setBindDevice(false);
				validateRequest.setLanguage("EN".equalsIgnoreCase(Utils.getRSALanguage(request)) ? Lang.en : Lang.es);
			}

			ResultStatus statusResult = customerService.rsaValidateOobChallenge(validateRequest);

			if (statusResult != null) {
				deviceRequest.setDeviceTokenCookie(validateRequest.getDeviceTokenCookie());
				deviceRequest.setDeviceTokenFSO(validateRequest.getDeviceTokenCookie());
				userSession.setDeviceRequest(deviceRequest);
				userSession.setRsaSessionId(validateRequest.getRsaSessionId());
				userSession.setRsaTransactionId(validateRequest.getRsaTransactionId());

				if (ResultStatus.RSA_UNABLE.equals(statusResult)) {
					errors.rejectValue("code", "login.oob.codevalidation.error",
							"Unable to validate code at this time.");
					bean.setValidationError(true);
				} else if (ResultStatus.RSA_EXPIRED.equals(statusResult)) {
					errors.rejectValue("code", "login.oob.codeexpired.error", "Code has expired.");
					bean.setTimeout(true);
				} else if (ResultStatus.RSA_BLOCKED.equals(statusResult)) {
					userSession.setOobRedirectToBlock(true);
					logger.error("RSA User " + userSession.getUsername() + " blocked by OOB challenge tries");
				} else if (ResultStatus.RSA_FAILED.equals(statusResult)) {
					if (action == EvendorBean.ACTION.VALIDATE_SMSCODE
							|| action == EvendorBean.ACTION.VALIDATE_ALTSMSCODE) {
						errors.rejectValue("code", "sms.code.invalidRSA", "Incorrect code");
					} else
						errors.rejectValue("code", "login.oob.call.error",
								"Input the code on your phone, before clicking continue.");
				}
			}
		}
	}

	@Override
	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		return new ModelAndView(cancelView);
	}

	private Calendar calculateEffDate(Calendar cal) {
		List<Holiday> holidays = customerService.getHolidays();
		BankingDaysUtils bankingDaysUtils = new BankingDaysUtils(holidays);
		return bankingDaysUtils.calculatePayDaysWithCutoff(cal, 1, 17, 0);
	}

	private boolean isCommercialAccount(TVFrontendAccount account) {
		for (int i = 0; i < productId.length; i++) {
			if (account.getProductDefinition().getProductId().equals(productId[i])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

	/*
	 * CIBP-1680 bgarcia Obtiene el texto de error actualizado
	 */
	private String getTelepagoErrorDescriptor(String codigo, String len) {
		String status = "";
		
		try {
			if (!len.equals("en"))
				len = "es";
			status = this.messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
		} catch (Exception e) {
			status = "";
		}

		return status;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getCancelView() {
		return cancelView;
	}

	public void setCancelView(String cancelView) {
		this.cancelView = cancelView;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	@Override
	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.BILLPAY;
	}

	public String[] getProductId() {
		return productId;
	}

	public void setProductId(String[] productId) {
		this.productId = productId;
	}

	public String getTelenominaID() {
		return telenominaID;
	}

	public void setTelenominaID(String telenominaID) {
		this.telenominaID = telenominaID;
	}

	public String getEvendorId() {
		return evendorId;
	}

	public void setEvendorId(String evendorId) {
		this.evendorId = evendorId;
	}
}
