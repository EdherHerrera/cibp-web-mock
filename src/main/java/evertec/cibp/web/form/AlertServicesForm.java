package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.PushNotificationInteraction;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyCustomerAlertsRequest;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.AlertServicesBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * 
 */
public class AlertServicesForm extends CancellableFormController {
	private static final Logger logger = Logger.getLogger(AlertServicesForm.class);
	private static final String USERSESSION = "userSession";
	
	public AlertServicesForm() {
		super();
		setCommandClass(AlertServicesBean.class);
		setCommandName("alertservices");
		setFormView("alertservices");
	}
	
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request, userSession.getUsername());
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();
		AlertsEntitlement entitlement = null;
		AlertType pushCategory = null;
		boolean pushAdded = false;
		
		boolean isEntitlementChanged = false;
		String viewName = !userSession.getUserAgentType().equals(UserAgentType.IPHONE) && !userSession.getUserAgentType().equals(UserAgentType.ANDROID) ? "text" : "mobileAlertsJson";
		
	
		if (!profile.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement alert = new AlertsEntitlement();
			customerService.getCustomerProfileService().addEntitlement(profile, alert.getCustomerEntitlement());
		}
		
		entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		Set<AlertType> customerAlerts = new HashSet<>();
		if (entitlement.getAlerts() != null)
			customerAlerts = entitlement.getAlerts();

		ModifyCustomerAlertsRequest req = new ModifyCustomerAlertsRequest();
		req.setCustomerProfile(userSession.getCustomerProfile());
		req.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));

		AlertServicesBean bean = (AlertServicesBean) command;
		
		if (bean.isOnOffDebitServicePush()) {
			pushAdded = bean.getAction().equalsIgnoreCase("true");
			pushCategory = AlertType.ONOFF_DEBIT_SERVICE_PUSH;
			if (pushAdded) {
				customerAlerts.add(AlertType.ONOFF_DEBIT_SERVICE_PUSH);
			} else {
				if (customerAlerts.contains(AlertType.ONOFF_DEBIT_SERVICE_PUSH))
					customerAlerts.remove(AlertType.ONOFF_DEBIT_SERVICE_PUSH);
			}
			
			isEntitlementChanged = true;
		}
		
		if (isEntitlementChanged) {
			req.setAlerts(customerAlerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", bean.getAction());
				model.put("success", true);
				logPushNotificationEvent(pushCategory, pushAdded, true, customerService, interactionEvent);
				return new ModelAndView(viewName, model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e);
				model.put("text", false);
				logPushNotificationEvent(pushCategory, pushAdded, false, customerService, interactionEvent);
				return new ModelAndView(viewName, model);

			}
		}
		
		model.put("text", false);
		
		return new ModelAndView(viewName, model);

	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		CustomerProfile profile = userSession.getCustomerProfile();

		AlertsEntitlement entitlement = null;
		if (profile.hasEntitlement(EntitlementType.ALERTS))
			entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		Set<AlertType> customeralerts = new HashSet<>();
		if (entitlement != null && entitlement.getAlerts() != null) {
			customeralerts = entitlement.getAlerts();
		}

		model.put("onOffDebitServicePush", customeralerts.contains(AlertType.ONOFF_DEBIT_SERVICE_PUSH));
		
		return model;
	}
	
	private void logPushNotificationEvent(AlertType pushCategory, Boolean pushAdded, Boolean success, CustomerServices service, CustomerInteractionEvent event) {
		if (pushCategory != null) {
			event.setCustomStringData(PushNotificationInteraction.CATEGORY, pushCategory.toString());
			if (pushAdded)
				service.logPushNotificationAddEvent(event, success);
			else 
				service.logPushNotificationDeleteEvent(event, success);
		}
	}
}
