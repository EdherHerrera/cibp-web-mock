package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.Owner;
import bppr.tv.beans.accounts.AccountRecord;
import bppr.tv.beans.accounts.types.DepositAccount;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.applications.EstatementApplication;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.EpayrollEmailStatementBean;
import evertec.cibp.web.form.bean.EpayrollStatementBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class EpayrollStatementForm extends CancellableFormController {

	private MessageSource messageSource;
	private String[] productId;
	private String[] relationshipType;
	private String maxEmailAllowed;

	public EpayrollStatementForm() {
		super();
		setCommandClass(EpayrollStatementBean.class);
		setCommandName("estatementservice");
		setFormView("estatementservice");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		String lang = Utils.getRSALanguage(request);
		CustomerServices customerService = userSession.getCustomerService();

		EpayrollStatementBean bean = (EpayrollStatementBean) command;
		String name = bean.getName();
		String email = customer.getEmail();
		String ssn = customer.getTaxId();
		String enrollAcct = customer.getEnrollmentAccountNumber();
		String phone1 = bean.getPhone1();
		String phone2 = bean.getPhone2();
		String phone3 = bean.getPhone3();

		String businessName = bean.getBusinessName();
		String businessPhone1 = bean.getBusinessPhone1();
		String businessPhone2 = bean.getBusinessPhone2();
		String businessPhone3 = bean.getBusinessPhone3();

		String phone = phone1 + phone2 + phone3;
		String businessPhone = businessPhone1 + businessPhone2 + businessPhone3;

		List<EpayrollEmailStatementBean> emailList = bean.getEmailList();

		boolean saveApplication = false;

		EstatementApplication application = new EstatementApplication();

		application.setName(name);
		application.setPhone(phone);
		application.setSSN(ssn);
		application.setEnrollAcct(enrollAcct);
		application.setBusinessName(businessName);
		application.setBusinessPhone(businessPhone);
		application.setEmail(email);
		int index = 0;
		for (int i = 0; i < emailList.size(); i++) {
			if (emailList.get(i).getEmailList() != null && !emailList.get(i).getEmailList().equalsIgnoreCase("")) {
				index++;
				TVFrontendAccount account = Utils.getAccountById(emailList.get(i).getAccountNumber(),
						userSession.getAccounts());
				application.setAccountNumber(account.getAccountNumber(), index);
				application.setAccountType(emailList.get(i).getAccountType(), index);
				application.setAccountListEmail(emailList.get(i).getEmailList(), index);
				application.setRelationship(emailList.get(i).getRelationship(), index);
				application.setAccountProductType(emailList.get(i).getAccountProductType(), index);
			}
		}
		application.setAmountAccountRequest(String.valueOf(index));
		try {
			CustomerFacade.getInstance().saveEstatementApplication(application, customer, request);
			saveApplication = true;
		} catch (RuntimeException e) {
			saveApplication = false;
		}

		if (!saveApplication) {
			userSession.setStatusmessage(this.messageSource.getMessage("commercialservice.enrollment.fail", null,
					new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang)));
		} else {

			userSession.setStatusmessage(this.messageSource.getMessage("commercialservice.enrollment.success", null,
					new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang)));
		}

		model.put("completed", true);
		model.put("saveApplication", saveApplication);
		return showForm(request, errors, "estatementservice", model);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {

		EpayrollStatementBean bean = (EpayrollStatementBean) command;

		String businessName = bean.getBusinessName();

		String phone1 = bean.getPhone1();
		String phone2 = bean.getPhone2();
		String phone3 = bean.getPhone3();

		String businessPhone1 = bean.getBusinessPhone1();
		String businessPhone2 = bean.getBusinessPhone2();
		String businessPhone3 = bean.getBusinessPhone3();

		List<EpayrollEmailStatementBean> emailList = bean.getEmailList();

		String terms = bean.getTermsConfirmation();
		int emails = 0;
		if (GenericValidator.isBlankOrNull(businessName)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("businessName", "", "Please enter a valid business name.");
			else
				errors.rejectValue("businessName", "", "Por favor entre un nombre del negocio v&aacute;lido.");
		}

		if (GenericValidator.isBlankOrNull(phone1) || GenericValidator.isBlankOrNull(phone2)
				|| GenericValidator.isBlankOrNull(phone3))
			if (GenericValidator.isBlankOrNull(phone1) || !GenericValidator.matchRegexp(phone1, "^\\d{3}$")
					|| GenericValidator.isBlankOrNull(phone2) || !GenericValidator.matchRegexp(phone2, "^\\d{3}$")
					|| GenericValidator.isBlankOrNull(phone3) || !GenericValidator.matchRegexp(phone3, "^\\d{4}$")) {

				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("phone3", "", "Please enter a home phone number.");
				else
					errors.rejectValue("phone3", "", "Por favor entre un tel&eacute;fono residencial.");
			}

		if (GenericValidator.isBlankOrNull(businessPhone1) || GenericValidator.isBlankOrNull(businessPhone2)
				|| GenericValidator.isBlankOrNull(businessPhone3))
			if (GenericValidator.isBlankOrNull(businessPhone1)
					|| !GenericValidator.matchRegexp(businessPhone1, "^\\d{3}$")
					|| GenericValidator.isBlankOrNull(businessPhone2)
					|| !GenericValidator.matchRegexp(businessPhone2, "^\\d{3}$")
					|| GenericValidator.isBlankOrNull(businessPhone3)
					|| !GenericValidator.matchRegexp(businessPhone3, "^\\d{4}$")) {

				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("businessPhone3", "", "Please enter a Business phone number.");
				else
					errors.rejectValue("businessPhone3", "", "Por favor entre un tel&eacute;fono del Negocio.");
			}

		if (GenericValidator.isBlankOrNull(terms) || !terms.equalsIgnoreCase("on")) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("termsConfirmation", "", "Please agree.");
			else
				errors.rejectValue("termsConfirmation", "", "Por favor acepte los t&eacute;rminos y condiciones.");
		}

		for (int i = 0; i < emailList.size(); i++) {
			if (GenericValidator.isBlankOrNull(emailList.get(i).getEmailList())) {
				emails++;
			}
		}

		if (emails == bean.getEmailList().size()) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("isValid", "", "Please add at least one email");
			else
				errors.rejectValue("isValid", "", "Por favor ingrese al menos un correo electr&oacute;nico");
		}

		super.onBindAndValidate(request, command, errors);
	}

	private Map<String, Object> loadPersonalInformation(HttpServletRequest request, Map<String, Object> model,
			UserSession session) {
		model.put("customer", session.getCustomerProfile());
		return model;
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		model = this.loadPersonalInformation(request, model, userSession);

		AccountMaskUtil mask = new AccountMaskUtil();
		NumberTool numberTool = new NumberTool();

		List<EpayrollEmailStatementBean> listEmail = new ArrayList<>();

		EpayrollStatementBean bean = (EpayrollStatementBean) command;
		String name = bean.getName();
		if (bean.getName() == null) {
			bean.setName(userSession.getCustomerProfile().getFullName());
		}

		if (GenericValidator.isBlankOrNull(name) || !GenericValidator.matchRegexp(name, "^\\d+$")) {
			name = (String) request.getParameter("name");
		}

		Set<TVAccountTransferSource> accountsPayrollFrom = userSession.getAccountsPaymentFrom();
		Map<String, String> accountsFromPayrollMap = new LinkedHashMap<>();
		if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
			accountsFromPayrollMap.put("none", "Select an account");
		else
			accountsFromPayrollMap.put("none", "Selecciona una cuenta");

		for (Iterator<TVAccountTransferSource> iterator = accountsPayrollFrom.iterator(); iterator.hasNext();) {
			TVAccountTransferSource acctfrom = iterator.next();

			TVFrontendAccount accfront = Utils.getAccountById(acctfrom.getFrontEndId(), userSession.getAccounts());

			
			if (!accfront.getHide()) {
				if (accfront != null && accfront.getPortalBalance() != null && accfront.getSubtype() != null && Utils.isCommercialAccount(accfront, productId)) {
					String relationship = Utils.getRelationshipTypeByAcct(userSession, accfront, relationshipType);
					if (!relationship.equalsIgnoreCase("")) {
						EpayrollEmailStatementBean detailEmail = new EpayrollEmailStatementBean();
						detailEmail.setAccountNumber(acctfrom.getFrontEndId());
						detailEmail.setRealAccountNumber(acctfrom.getAccount().getAccountNumber());
						detailEmail.setAccountType("");
						detailEmail.setEmailList("");
						detailEmail.setRelationship(relationship);
						listEmail.add(detailEmail);
						accountsFromPayrollMap.put(acctfrom.getFrontEndId(),
								StringUtils.abbreviate(accfront.getNickname(), 15) + " "
										+ mask.mask(accfront.getAccountNumber()) + " "
										+ numberTool.format("currency", accfront.getPortalBalance())
										+ ((accfront.getAccountNumberSuffix() != null
												&& !accfront.getAccountNumberSuffix().equals(""))
														? " " + accfront.getAccountNumberSuffix() : ""));

					}
				}
			}
		}
		for (int i = 0; i < bean.getEmailList().size(); i++) {
			if (!bean.getEmailList().get(i).getEmailList().equalsIgnoreCase("")) {
				listEmail = bean.getEmailList();
			}
		}
		model.put("amountAccount", listEmail.size());
		model.put("frompayrollaccounts", accountsFromPayrollMap);
		model.put("maxEmailAllowed", maxEmailAllowed);
		// Se hace la resta de 1 por el texto "Select an account / Seleccione
		// una cuenta"
		model.put("accountDetails", listEmail);
		return model;
	}


	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String[] getProductId() {
		return productId;
	}

	public void setProductId(String[] productId) {
		this.productId = productId;
	}

	public String[] getRelationshipType() {
		return relationshipType;
	}

	public void setRelationshipType(String[] relationshipType) {
		this.relationshipType = relationshipType;
	}

	public String getMaxEmailAllowed() {
		return maxEmailAllowed;
	}

	public void setMaxEmailAllowed(String maxEmailAllowed) {
		this.maxEmailAllowed = maxEmailAllowed;
	}
}
