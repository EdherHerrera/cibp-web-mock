package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import com.evertecinc.services.seagull.pinnumbervalidator.beans.PinValidationRequest;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.ActivationCardAccountResponse;
import evertec.cibp.core.services.responses.ActivationCardAccountResponse.Status;
import evertec.cibp.encryptedPin.PinEncryptionTool;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.form.bean.ModifyAthPinBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.utils.AccountMaskUtil;

public class ModifyAthPinForm extends CancellableFormController{

	protected final Log logger = LogFactory.getLog(getClass());
	private PinEncryptionTool pinTool;
	
	private String accountSubtypes;
	private String accountProductIds;

	private static final String VIEW = "modifyAthPin";
	private static final String MESSAGE = "message";
	private static final String VALIDATED = "validated";
	private static final String VALIDATION_FAIL = "validationFail";

	public ModifyAthPinForm(){
		super();
		setCommandClass(ModifyAthPinBean.class);
		setCommandName(VIEW);
		setFormView(VIEW);
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		ModifyAthPinBean bean = (ModifyAthPinBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();

		BankingSession bankingSession = userSession.getBankingSession();
		CardPlasticAccountRecord plastic = userSession.getCardPlastic(bean.getAccount(), bean.getCardPlasticNumber());
		
		boolean statusError = false;
		
		if(plastic != null){
			String cardPlasticNumber = plastic.getCardPANId();
			String oldPin = bean.getOldPin();
			String oldPinBlock = pinTool.encrypt(oldPin);
			String newPin = bean.getPin();
			String newPinBlock = pinTool.encrypt(newPin);
			String csrId = "PR00032";
			CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request);

			// Old pin verification
			ActivationCardAccountResponse verificationResponse = customerService.changeOrVerifyAthPin(customerProfile, bankingSession, cardPlasticNumber, oldPinBlock, csrId, PinValidationRequest.PinTransaction.VERIFYPIN, event);

			if(verificationResponse.getResponseStatus() == Status.VALIDATED){
				// Pin modification
				ActivationCardAccountResponse modificationResponse = customerService.changeOrVerifyAthPin(customerProfile, bankingSession, cardPlasticNumber, newPinBlock, csrId, PinValidationRequest.PinTransaction.REVALIDATE, event);
				
				if(modificationResponse.getResponseStatus() == Status.VALIDATED){
					model.put(VALIDATED, true);
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						model.put(MESSAGE, "Your personal identification number (PIN) was successfully changed.");
					} else {
						model.put(MESSAGE, "Tu número de identificación personal (PIN) fue cambiado exitosamente.");
					}
				} else {
					model.put(VALIDATION_FAIL, true);
					statusError = true;
				}
			} else {
				model.put(VALIDATION_FAIL, true);
				if(verificationResponse.getResponseStatus() == Status.CARD_NOT_FOUND){
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						model.put(MESSAGE, "Your request to assign a personal identification number (PIN) was not authorized.");
					} else {
						model.put(MESSAGE, "Tu solicitud para cambiar el número de identificación personal (ATH) no fue autorizada.");
					}
				} else {
					if(verificationResponse.getResponseStatus() == Status.PIN_VALIDATION_FAIL) {
						if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
							model.put(MESSAGE, "The actual personal identification number (PIN) entered is incorrect.  If you forgot your PIN, visit any of our branches.");
						} else {
							model.put(MESSAGE, "El número de identificación personal (PIN) actual entrado es incorrecto. Si olvidaste el PIN, visita cualquiera de nuestras sucursales.");
						}
					} else {
						statusError = true;
					}
				}
			}
		}
		else{
			statusError = true;
		}
		
		if(statusError) {
			model.put(VALIDATION_FAIL, true);
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				model.put(MESSAGE, "Your request to change a personal identification number (PIN) cannot be processed at this moment.");
			} else {
				model.put(MESSAGE, "Tu solicitud para cambiar el número de identificación personal (PIN) no pudo ser procesada en este momento.");
			}
		}

		return showForm(request, error, VIEW, model);

	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		ModifyAthPinBean bean = (ModifyAthPinBean) command;

		if (bean.getAccount().equals("none") || bean.getCardPlasticNumber().equals("none")){
			errors.rejectValue("cardPlasticNumber", "modifyath.account.required.message", "Please select an account and card number");
			bean.setFormError(true);
		}
		if (!GenericValidator.matchRegexp(bean.getOldPin(), "\\d{4}")) {
			errors.rejectValue("oldPin", "modifyath.pin.invalid", "Please enter a valid PIN");
			bean.setFormError(true);
		}
		if (!GenericValidator.matchRegexp(bean.getPin(), "\\d{4}")) {
			errors.rejectValue("pin", "modifyath.pin.invalid", "Please enter a valid PIN");
			bean.setFormError(true);
		}
		if (GenericValidator.isBlankOrNull(bean.getPinconfirm()) || !bean.getPin().equals(bean.getPinconfirm())) {
			errors.rejectValue("pinconfirm", "modifyath.pinconfirmation.invalid",
					"Please enter a valid PIN confirmation");
			bean.setFormError(true);
		}
		
		if(bean.getFormError()){
			WebUtils.setSessionAttribute(request, "hasError", "true");
		}

	}
	
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		AccountMaskUtil maskutil = new AccountMaskUtil();
		Map<Object, Object> accountsMap = new LinkedHashMap<>();
		
		//Customer accounts list for dropdown
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			accountsMap.put("none", "Select an account");
		}
		else {
			accountsMap.put("none", "Seleccione una cuenta");
		}
		
		List<TVFrontendAccount> accounts = userSession.getAccounts();

		for (TVFrontendAccount account : accounts) {
			String suffix;
			if (account != null && account.getAccountNumberSuffix() != null)
				suffix = account.getAccountNumberSuffix();
			else
				suffix = "";
			if ((!suffix.equalsIgnoreCase("")) && suffix.equalsIgnoreCase("RES") || suffix.equalsIgnoreCase("SAV"))
				continue;
			
			boolean allowSubtype = Utils.haveType(this.accountSubtypes, account.getSubtype());
			boolean allowProductId = Utils.haveType(this.accountProductIds, account.getAccountProductId());

			if(account != null && !account.getHide() && allowSubtype && allowProductId) {
				accountsMap.put(account.getFrontEndId(),
						account.getNickname() + " " + maskutil.mask(account.getAccountNumber()));
			}
		}
		
		model.put("accounts", accountsMap);

		String hasError = WebUtils.getSessionAttribute(request, "hasError") != null ? WebUtils.getSessionAttribute(request, "hasError").toString() : "false";
		request.getSession().removeAttribute("hasError");
		model.put("hasError", hasError);
		
		return model;
	}

	public PinEncryptionTool getPinTool() {
		return pinTool;
	}

	public void setPinTool(PinEncryptionTool pinTool) {
		this.pinTool = pinTool;
	}

	public void setAccountSubtypes(String accountSubtypes) {
		this.accountSubtypes = accountSubtypes;
	}

	public void setAccountProductIds(String accountProductIds) {
		this.accountProductIds = accountProductIds;
	}

}
