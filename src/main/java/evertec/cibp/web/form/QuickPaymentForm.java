package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.factory.TVAccountFactory;
import bppr.tv.beans.accounts.types.DepositAccount;

import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.base.AbstractStatement;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerPayeePreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.holiday.Holiday;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVAccountFavoritePayment;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.BankingDaysUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.PaymentBean;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.UserAccount;

/**
 * QuickPaymentForm
 * @author Evertec
 * @see QuickPaymentForm
 * @version 1.0
 */
public class QuickPaymentForm extends AbstractWizardFormController implements MessageSourceAware {

	/**
	 * cancelView
	 */
	private String cancelView; // cancelView
	/**
	 * Logger instance
	 */
	protected final Log logger = LogFactory.getLog(getClass()); // Logger instance
	/**
	 * customer Service
	 */
	protected CustomerServices customerService; // customer Service
	/**
	 * overpayCount
	 */
	private int overpayCount = 0; // overpayCount
	
	private String balanceDelay = "";
	private MessageSource messageSource;
	private String telenominaID;
	private String evendorId;
	private static final String PIF_PREFIX = "PIF";

	public String getEvendorId() {
		return evendorId;
	}
	
	public void setEvendorId(String evendorId) {
		this.evendorId = evendorId;
	}
	
	/**
	 * getTelenominaID
	 * @return String
	 */
	public String getTelenominaID() {
		return telenominaID;
	}

	public void setTelenominaID(String telenominaID) {
		this.telenominaID = telenominaID;
	}

	public QuickPaymentForm() {
		super();
		setCommandClass(PaymentBean.class);
		setCommandName("quickpayment");
		setPages(new String[] { "quickpayment", "quickpayment", "quickpayment" });
	}

	/**
	 * processFinish
	 * @param request user request
	 * @param response response
	 * @param command command
	 * @param error error description
	 * @return ModelAndView
	 */
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		
		final UserSession userSession = Utils.getUserSession(request); //usersession
		final CustomerProfile customer = userSession.getCustomerProfile();
		final CustomerServices customerService = userSession.getCustomerService();
		final boolean MBMT_511 = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT511);
		final boolean MBSFE797 = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE797);
		final PaymentBean paymentBean = (PaymentBean) command; // paymentBean
		Map<String, Object> model = new HashMap<String, Object>(0); // model view return

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		model.put("telenominaID", telenominaID);
		List<TVPayment> confirmPayments = null; // confirm Payments
		List<TVPayment> errorPayments = null; // error Payments
		overpayCount = 0;

		if (paymentBean != null) {
			String accountfrom = "";
			if (!paymentBean.getAccountFrom().equalsIgnoreCase("none")) {
				accountfrom = paymentBean.getAccountFrom();
			} else {
				accountfrom = paymentBean.getAccountFrom();
			}

			TVFrontendAccount sou = null;

			// If it is a PIF account, create it the TVFrontendAccount Object.
			if (accountfrom.startsWith("PIF")) {
				String externalAccountId = accountfrom.substring(3);
				PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
				if (externalAccount != null) {
					BankInformation bankInformation = new BankInformation();
					bankInformation.setBankId(externalAccount.getRoutingNumber());
					bankInformation.setBankType("ABA");
					bankInformation.setBankName(externalAccount.getBankName());
					CustomerAccountPreferences pref = new CustomerAccountPreferences();
					TVAccountFactory accountFactory = new TVAccountFactory();
					Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
							externalAccount.getAccountNumber());
					sourceAccount.setAccountID(externalAccount.getAccountNumber());
					sourceAccount.setSubType("IDA");
					((DepositAccount) sourceAccount).setSectionIndicator(Character.toString(externalAccount
							.getAccountType()));
					TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
					fromAccount.setCustomerAccountPreferences(pref);
					fromAccount.setNickname(externalAccount.getNickname());
					fromAccount.setHide(externalAccount.getShow() == '0');
					fromAccount.getTVAccount().setBankInformation(bankInformation);
					sou = fromAccount;
				}
			} else {
				sou = Utils.getAccountById(accountfrom, userSession.getAccounts());
			}

			TVFrontEndPayee tar = Utils.getPayeebyId(paymentBean.getPayeeid(), userSession.getPayees(false));
			BigDecimal amount = paymentBean.getAmount();

			Calendar cal = Calendar.getInstance();
			if (paymentBean != null && paymentBean.getEffectiveDate() != null)
				cal.setTime(paymentBean.getEffectiveDate());

			TVPayment payment = null;
			payment = new TVPayment(amount, Frequency.MANUALLY, 0, cal, new TVAccountTransferSource(sou), tar);

			// CIBP-2548 JOCAMPO
			if (payment.getPayee().getGlobalPayeeIdString() != null
					&& payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
				payment.getPayee().setTelenomina(true);
			} else {
				payment.getPayee().setTelenomina(false);
			}

			PaymentResponse res = PaymentsServiceModule.sendPayments(customer, userSession, payment,
					Utils.generateCustomerInteractionEvent(request), customerService);
			payment.getTVPayment().setEffectiveDate(cal);

			String estimatedDate=Utils.getEstimateDateAccordingToPaymentType(tar,customerService,cal,"",Utils.getRSALanguage(request));
			payment.setEstimateDate(estimatedDate);

			confirmPayments = new LinkedList<TVPayment>();
			errorPayments = new LinkedList<TVPayment>();

			if (res.getStatus() == ResultStatus.SUCCESS) {

				userSession.setMobileCashAccounts(null);

				// Incidente de actualizar balance en tiempo real
				if (MBMT_511) {
					TVFrontendAccount frontendAccountToUpdate = Utils
							.getAccountByAccountNumber(payment.getSourceAccountNumber(), userSession.getAccounts());
					if (MBSFE797){
						if (frontendAccountToUpdate != null){
							if (frontendAccountToUpdate.getSubtype() != null
									&& frontendAccountToUpdate.getSubtype().equals("IDA")) {
								List<TVFrontendAccount> frontEndAccounts2Update = new LinkedList<TVFrontendAccount>();
								frontEndAccounts2Update.add(frontendAccountToUpdate);
								userSession.updateAccountBalance(frontEndAccounts2Update, balanceDelay);
							}
						}
					}else{
						if (frontendAccountToUpdate.getSubtype() != null
								&& frontendAccountToUpdate.getSubtype().equals("IDA")) {
							List<TVFrontendAccount> frontEndAccounts2Update = new LinkedList<TVFrontendAccount>();
							frontEndAccounts2Update.add(frontendAccountToUpdate);
							userSession.updateAccountBalance(frontEndAccounts2Update, balanceDelay);
						}
					}
					
				}

				// The EBPP2 user id is not yet in session; add it
				if (!userSession.isEbpp2UserIdLookedUp()) {
					EbppEntitlement ent = null;
					if (customer.getEntitlementByType(EntitlementType.EBILLS) != null) {
						ent = new EbppEntitlement(customer.getEntitlementByType(EntitlementType.EBILLS));

						// If the username is blank, we need to look in the
						// current EBPP accounts ONLY
						if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
							userSession.setEbpp2UserId("");
						} else {
							userSession.setEbpp2UserId(ent.getUsername());
						}
						userSession.setEbpp2UserIdLookedUp(true);
					}
				}

				// The user has an EBPP2 ID
				if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

					// Find the account in the user accounts list
					List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

					// Loop through all the accounts and send the update payment
					// message if necessary
					for (Iterator<UserAccount> iterator2 = ebpp2UserAccounts.iterator(); iterator2.hasNext();) {
						UserAccount userAccount = (UserAccount) iterator2.next();

						if (userAccount.getAccountNumber().replaceFirst("^0+(?!$)", "")
								.equals(tar.getBillingAccount().replaceFirst("^0+(?!$)", ""))) {

							if (!userAccount.isLastEbillPaid()) {
								customerService.updateAccountPayment(userSession.getEbpp2UserId(),
										userAccount.getBillerAccountID());
								// Alizano hide new ebill message when payment
								// is success Fix Incident 1537231
								res.getPayment().getPayee().setHasNewEbill(false);
								userAccount.setLastEbillPaid(true);
								// End Fix
							}
							break;
						}
					}
				}
				model.put("PAYMENTSEND", true);
				model.put("payment", payment);
				confirmPayments.add(payment);
				model.put("quickpayment", paymentBean);
				
				// Mark is the Ebills PDF is not view
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
					if (tar.getBillingAccount() != null) {
						TVFrontendAccount cca = Utils.getAccountByAccountNumber(tar.getBillingAccount(),
								userSession.getAccounts());
						if(cca != null){
							List<AbstractStatement> ebills = Utils.statementsByAccount(cca, userSession, request);
							if (ebills != null && ebills.size() > 1) {
								Utils.markPdfViewed(ebills.get(1), cca, userSession);
								int pid = tar.getGlobalPayeeId();
								
								if(pid == 359 || pid == 517 || pid == 518 || pid == 1983 || pid == 223){
								
									CustomerPayeePreferences customerPayeePreferences = tar.getCustomerPayeePreferences();
																		
									SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
									customerPayeePreferences.setLastPaymentDate(format.format(ebills.get(1).getEndDate()));
									
									tar.setCustomerPayeePreferences(customerPayeePreferences);
									customerService.modifyPayeeFrequentPreference(userSession.getCustomerProfile(), userSession.getBankingSession(), tar, tar.getFavorite());
							
								}
							}
						}
					}
				} // End EBILLS_STATEMENTS
			} else {

				String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request).toLowerCase());
				if (er == null || er.equals("")) {
					payment.setPaymentMessage(res.getErrorMessage());
				} else {
					payment.setPaymentMessage(er + " - " + res.getErrorEBA());
				}
				model.put("PAYMENTSEND", false);
				model.put("payment", payment);
				errorPayments.add(payment);
				model.put("quickpayment", paymentBean);

			}
			model.put("payment", payment);
			model.put("quickpayment", paymentBean);

		}

		boolean hasalert = false;
		if (customer.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
			if (ent.getAlerts().contains(AlertType.PAYMENTS_EMAIL) || ent.getAlerts().contains(AlertType.PAYMENTS_SMS)
					|| ent.getAlerts().contains(AlertType.PAYMENTS_PUSH)) {
				hasalert = true;
			}

		}
		if (hasalert) {
			customerService.sendPaymentConfirmation(customer, confirmPayments, errorPayments,
					userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
					userSession.getPreferredLanguage());
		}

		model.put("page", "2");

		loadData(request, command, model);
		return new ModelAndView("quickpayment", model);
	}

	/**
	 * referenceData
	 * @param request user request type
	 * @param command user coomand
	 * @param errors erros result
	 * @param page page form
	 * @return Map<String, Object>
	 */
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		userSession.setLoadebpp(false);
		Map<String, Object> model = new HashMap<>();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99
				
		model.put("page", page);
		model.put("telenominaID", telenominaID);
		boolean realTimeNotificationsClaro=StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO);
		model.put("realTimeNotificationsClaro",realTimeNotificationsClaro);
		model.put("favorites", userSession.getFavoritePayments()); // Fix for
																	// CIBP-2744
		GlobalEntitlement ent = customerService.getGlobalEntitlement(EntitlementType.BILLPAY);

		if (ent == null || ent.getStatus() != EntitlementStatus.ENABLED) {
			model.put("down", "true");
			return model;
		}

		loadData(request, command, model);

		if (page == 1) {

			PaymentBean paymentBean = (PaymentBean) command;

			if (paymentBean != null) {
				String accountfrom = "";
				if (!paymentBean.getAccountFrom().equalsIgnoreCase("none")) {
					accountfrom = paymentBean.getAccountFrom();
				} else {
					accountfrom = paymentBean.getAccountFrom();
				}

				// TVFrontendAccount sou = Utils.getAccountById(accountfrom,
				// userSession.getAccounts());

				TVFrontendAccount sou = null;

				// If it is a PIF account, create it the TVFrontendAccount
				// Object.
				if (accountfrom.startsWith("PIF")) {
					String externalAccountId = accountfrom.substring(3);
					PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
					if (externalAccount != null) {
						BankInformation bankInformation = new BankInformation();
						bankInformation.setBankId(externalAccount.getRoutingNumber());
						bankInformation.setBankType("ABA");
						bankInformation.setBankName(externalAccount.getBankName());
						CustomerAccountPreferences pref = new CustomerAccountPreferences();
						TVAccountFactory accountFactory = new TVAccountFactory();
						Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
								externalAccount.getAccountNumber());
						sourceAccount.setAccountID(externalAccount.getAccountNumber());
						sourceAccount.setSubType("IDA");
						((DepositAccount) sourceAccount).setSectionIndicator(Character.toString(externalAccount
								.getAccountType()));
						TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
						fromAccount.setCustomerAccountPreferences(pref);
						fromAccount.setNickname(externalAccount.getNickname());
						fromAccount.setHide(externalAccount.getShow() == '0');
						fromAccount.getTVAccount().setBankInformation(bankInformation);
						sou = fromAccount;
					}
				} else {
					sou = Utils.getAccountById(accountfrom, userSession.getAccounts());
				}

				TVFrontEndPayee tar = Utils.getPayeebyId(paymentBean.getPayeeid(), userSession.getPayees(false));
				BigDecimal amount = paymentBean.getAmount();

				Calendar cal = Calendar.getInstance();
				if (paymentBean != null && paymentBean.getEffectiveDate() != null)
					cal.setTime(paymentBean.getEffectiveDate());

				TVPayment payment = null;
				payment = new TVPayment(amount, Frequency.MANUALLY, 0, cal, new TVAccountTransferSource(sou), tar);

				model.put("payment", payment);
			}
		}

		return model;
	}

	/**
	 * Separate fields into a map for use by various client views.
	 * 
	 * @param account
	 * @return
	 */
	private Map<String, String> accountDetailForAccount(TVFrontendAccount account) {
		Map<String, String> accountDetail = new LinkedHashMap<String, String>();
		if (account == null) {
			accountDetail.put("accountName", "");
			accountDetail.put("acctLast4Num", "");
			accountDetail.put("accountBalance", "");
			accountDetail.put("accountNumber", "");
			accountDetail.put("accountSection", "");
			accountDetail.put("accountSubtype", "");
			accountDetail.put("text", "");

		} else {
			AccountMaskUtil mask = new AccountMaskUtil();
			NumberTool numberTool = new NumberTool();

			StringBuilder sb = new StringBuilder();

			accountDetail.put("accountName", account.getNickname());
			accountDetail.put("accountLast4Num", mask.mask(account.getAccountNumber()));
			accountDetail.put("accountNumber", account.getAccountNumber());
			accountDetail.put("accountSubtype", account.getSubtype());
			accountDetail.put("accountSection", account.getAccountSection());
			if (account != null && account.getSubtype().equals("CCA") && account.getAvailableBalance() != null) {
				accountDetail.put("accountBalance", numberTool.format("currency", account.getAvailableBalance()));
				accountDetail.put("accountNumberSuffix", ((account.getAccountNumberSuffix() != null && !account
						.getAccountNumberSuffix().equals("")) ? " " + account.getAccountNumberSuffix() : ""));
			} else if (account != null && account.getPortalBalance() != null) {
				accountDetail.put("accountBalance", numberTool.format("currency", account.getPortalBalance()));
				accountDetail.put("accountNumberSuffix", ((account.getAccountNumberSuffix() != null && !account
						.getAccountNumberSuffix().equals("")) ? " " + account.getAccountNumberSuffix() : ""));
			} else {
				accountDetail.put("accountBalance", "");
				accountDetail.put("accountNumberSuffix", ((account.getAccountNumberSuffix() != null && !account
						.getAccountNumberSuffix().equals("")) ? " " + account.getAccountNumberSuffix() : ""));
			}

			sb.append(StringUtils.left(accountDetail.get("accountName"), 15));
			sb.append(" ");
			sb.append(accountDetail.get("accountLast4Num"));
			if (!accountDetail.get("accountBalance").equals("")) {
				sb.append(" ");
				sb.append(accountDetail.get("accountBalance"));
			}
			if (!accountDetail.get("accountNumberSuffix").equals("")) {
				sb.append(" ");
				sb.append(accountDetail.get("accountNumberSuffix"));
			}

			accountDetail.put("text", sb.toString());
		}
		return accountDetail;
	}

	/**
	 * Separate fields into a map for use by various client views.
	 * 
	 * @param payee
	 * @return
	 */
	private Map<String, String> payeeDetailForPayee(TVFrontEndPayee payee, UserSession userSession) {

		
		Map<String, String> payeeDetail = new LinkedHashMap<>(); // payee Detail
		final String rtNotification = "rtNotification"; // rtNotification key
		final String rtHasPaymentHistory = "rtHasPaymentHistory"; // rtHasPaymentHistory key
		
		if (payee == null) {
			payeeDetail.put("accountName", "");
			payeeDetail.put("acctLast4Num", "");
			payeeDetail.put("globalPayeeId", "");
			payeeDetail.put("text", "");
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)) {
				payeeDetail.put(rtNotification, StringUtils.EMPTY);
			}
		} else {

			AccountMaskUtil maskUtil = new AccountMaskUtil();
			
			payeeDetail.put("accountName", payee.getNickname());
			payeeDetail.put("acctLast4Num", maskUtil.mask(payee.getBillingAccount()));
			payeeDetail.put("globalPayeeId", payee.getGlobalPayee().getIdString());
			payeeDetail.put("text",
					StringUtils.left(payeeDetail.get("accountName"), 15) + " " + payeeDetail.get("acctLast4Num"));
			
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)) {
				final String nValue = "N"; // false value
				final String yValue = "Y"; // true value
				String realTimePayee = nValue; // payee real Time
				final String rtNotificationValue = payee.getGlobalPayee().getRtNotification(); // payee rtNotificationValue
				final String rtIndicatorValue = payee.getGlobalPayee().getRtIndicator(); // payee rtIndicatorValue
				
				if (rtNotificationValue != null && rtNotificationValue.equalsIgnoreCase(yValue) && 
						rtIndicatorValue != null && rtIndicatorValue.equalsIgnoreCase(yValue)) {
					realTimePayee = yValue;
				}
				payeeDetail.put(rtNotification, realTimePayee);
				payeeDetail.put(rtHasPaymentHistory, Utils.containsPayments(payee.getFrontEndId(), userSession) ? "true" : "false");
			}

		}
		return payeeDetail;
	}

	/**
	 * loadData
	 * @param request
	 * @param command
	 * @param model
	 */
	private void loadData(HttpServletRequest request, Object command, Map<String, Object> model) {
		final UserSession userSession = Utils.getUserSession(request); //usersession
		List<TVFrontEndPayee> payees = userSession.getPayees(false);
		List<TVAccountFavoritePayment> allPayments = new LinkedList<>();
		
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT477)
				&& userSession.getUserAgentType() == UserAgentType.JSON && payees != null) {

			for (TVFrontEndPayee payee : payees) {

				TVAccountFavoritePayment favpayment = new TVAccountFavoritePayment(payee);
				favpayment.setFrontEndId(payee.getFrontEndId());
				allPayments.add(favpayment); 
			}
			
			if (!allPayments.isEmpty()) {
				userSession.setFavoritePayments(allPayments);
			}
		}

		userSession.getPayeesPaymentHistoryInfo();
		boolean realTimeNotificationsClaro=StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO);
		model.put("realTimeNotificationsClaro",realTimeNotificationsClaro);
        boolean epayrollActivated = StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL);
		boolean evendorActivated = StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR);
		Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsPaymentFrom();
		AccountMaskUtil mask = new AccountMaskUtil();

		Map<String, String> accountsFromMap = new LinkedHashMap<String, String>();
		Map<String, Map<String, String>> detailedAccountsFromMap = new LinkedHashMap<String, Map<String, String>>(); // TK
																														// 04/29/11
																														// Hector
																														// iPhone
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			accountsFromMap.put("none", "Account");
			detailedAccountsFromMap.put("none", accountDetailForAccount(null));
		} else {
			accountsFromMap.put("none", "Cuenta");
			detailedAccountsFromMap.put("none", accountDetailForAccount(null));
		}
		for (Iterator<TVAccountTransferSource> iterator = accountsFrom.iterator(); iterator.hasNext();) {
			TVAccountTransferSource from = (TVAccountTransferSource) iterator.next();
			TVFrontendAccount frontFrom = from.getAccount();
			if (!frontFrom.getHide()) {
			
				 // TK 04/29/11 Hector iPhone - The generic accountsFromMap only
				 // contains a preformatted string ready for display, however the
				 // detailedAccountsFromMap includes separates these values so
				 // that the view may independently construct the display. e.g.
				 // iPhone can display these values as separate strings.
				 
				Map<String, String> accountDetail = accountDetailForAccount(frontFrom);
				if (accountDetail != null) {
					accountsFromMap.put(from.getFrontEndId(), accountDetail.get("text"));
					detailedAccountsFromMap.put(from.getFrontEndId(), accountDetail);
				} else {
					accountsFromMap.put(from.getFrontEndId(), "");
				}
			}
		}

		List<PIFExternalAccount> pifExternalAccounts = customerService.getConfirmedPIFExternalAccounts(userSession
				.getCustomerProfile().getTaxId());
		if (pifExternalAccounts != null)
			for (Iterator<PIFExternalAccount> iterator = pifExternalAccounts.iterator(); iterator.hasNext();) {
				PIFExternalAccount from = (PIFExternalAccount) iterator.next();
				accountsFromMap.put("PIF" + from.getAccountId(), StringUtils.abbreviate(from.getNickname(), 15) + " "
						+ mask.mask(from.getAccountNumber()));
				detailedAccountsFromMap.put(PIF_PREFIX + from.getAccountId(), Utils.detailedAccountFromPIF(from));

			}
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)){
			if(userSession.getInterbankFundingAllowed()){
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					accountsFromMap.put("add", "Add other bank account");
				else
					accountsFromMap.put("add", "A&ntilde;ade una cuenta de otro Banco");
			}
		}
		else{
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				accountsFromMap.put("add", "Add other bank account");
			else
				accountsFromMap.put("add", "A&ntilde;ade una cuenta de otro Banco");
		}

		model.put("fromaccounts", accountsFromMap);
		model.put("detailedfromaccounts", detailedAccountsFromMap);

		// Set up today's date
		SimpleDateFormat df = KiuwanUtils.getDateFormat("MM/dd/yyyy"); // date format
		Calendar n = calculateEffDate(Calendar.getInstance());
		model.put("todayDate", df.format(n.getTime()));
		df = new SimpleDateFormat("MM");
		model.put("month", df.format(n.getTime()));
		df = new SimpleDateFormat("dd");
		model.put("day", df.format(n.getTime()));

		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)){
			Calendar today=Calendar.getInstance();
			SimpleDateFormat todayformat = new SimpleDateFormat("MM/dd/yyyy");
			model.put("todayDateRealTime",todayformat.format(today.getTime()));
        }

        Map<String, String> payeeMap = new LinkedHashMap<String, String>();
		Map<String, Map<String, String>> detailedPayeeMap = new LinkedHashMap<String, Map<String, String>>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			payeeMap.put("none", "Payee");
			detailedPayeeMap.put("none", payeeDetailForPayee(null, userSession));
		} else {
			payeeMap.put("none", "Comercio");
			detailedPayeeMap.put("none", payeeDetailForPayee(null, userSession));
		}
		if (payees != null) {

			

			for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
				TVFrontEndPayee pay = (TVFrontEndPayee) iterator.next();
				if (epayrollActivated) {
					if (userSession.getUserAgentType() == UserAgentType.WEB) {
						if (evendorActivated) {
							if (pay.getGlobalPayeeId() != Integer.valueOf(telenominaID)
									&& pay.getGlobalPayeeId() != Integer.valueOf(evendorId)) {
								Map<String, String> payeeDetail = payeeDetailForPayee(pay, userSession);
								payeeMap.put(pay.getFrontEndId(), payeeDetail.get("text"));
								detailedPayeeMap.put(pay.getFrontEndId(), payeeDetail);
							}
						} else {
							if (pay.getGlobalPayeeId() != Integer.valueOf(telenominaID)) {
								Map<String, String> payeeDetail = payeeDetailForPayee(pay, userSession);
								payeeMap.put(pay.getFrontEndId(), payeeDetail.get("text"));
								detailedPayeeMap.put(pay.getFrontEndId(), payeeDetail);
							}
						}
					} else {
						final Map<String, String> payeeDetail = payeeDetailForPayee(pay, userSession);
						final List<TVAccountFavoritePayment> lastPayments = userSession.getFavoritePayments();

						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT477) 
								&& lastPayments != null) {

							for (TVAccountFavoritePayment payment : lastPayments) {

								if (payment.getLastPayment() != null) {
									TVPayment lastPayment = (TVPayment) payment.getLastPayment();

									if (pay.getFrontEndId() == payment.getPayee().getFrontEndId()) {

										payeeDetail.put("lastPayment", lastPayment.getAmount().toString());
										DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
										payeeDetail.put("lastPaymentDate", dateFormat.format(lastPayment.getEffectiveDate()));

									}
								}
							}
						}

						payeeMap.put(pay.getFrontEndId(), payeeDetail.get("text"));
						detailedPayeeMap.put(pay.getFrontEndId(), payeeDetail);

					}

				}

				else {
					Map<String, String> payeeDetail = payeeDetailForPayee(pay, userSession);
					payeeMap.put(pay.getFrontEndId(), payeeDetail.get("text"));
					detailedPayeeMap.put(pay.getFrontEndId(), payeeDetail);
				}

			}
		}

		final String isActiveRealTimeNotificationKey = "isActiveRealTimeNotification"; // active real time notification key
		
		model.put(isActiveRealTimeNotificationKey, StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO));
		model.put("payeesMap", payeeMap);
		model.put("detailedPayeesMap", detailedPayeeMap);

	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		return new ModelAndView(cancelView);
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		PaymentBean paymentBean = (PaymentBean) command;

		if (page == 0) {

			if (paymentBean.getAmount() == null) {
				errors.rejectValue("amount", "paymentshistory.amount.required", "Amount is required");
			}
			if (paymentBean.getAmount() != null) {
				
				BigDecimal maxpayamount = null;
				maxpayamount = new BigDecimal(999999999.99);
				
				if (paymentBean.getAmount().compareTo(maxpayamount) > 0) {
					errors.rejectValue("amount", "payments.amount.largermax", "Exceeds max amount");
				} else if (paymentBean.getAmount().compareTo(new BigDecimal(0)) <= 0) {
					errors.rejectValue("amount", "payments.amt.invalid", "Invalid");
				}
			}
			if ("none".equalsIgnoreCase(paymentBean.getAccountFrom())) {
				errors.rejectValue("accountFrom", "payments.acctfrom.required", "Account From is required");
			}
			if ("none".equalsIgnoreCase(paymentBean.getPayeeid())) {
				errors.rejectValue("payeeid", "payments.payee.invalid", "Payee is required");
			}
			paymentBean.setFrequency(Frequency.MANUALLY);

			Date effectiveDate = paymentBean.getEffectiveDate();
			Calendar today = Calendar.getInstance();

			// Set the max date (one year in future)
			Calendar thresholdDate = Calendar.getInstance();
			thresholdDate.add(Calendar.YEAR, 1);

			if (effectiveDate != null) {
				// Validate no future dates
				Calendar n = Calendar.getInstance();
				n.setTime(effectiveDate);

				if ((n.get(Calendar.YEAR) < today.get(Calendar.YEAR))
						|| n.get(Calendar.YEAR) == today.get(Calendar.YEAR)
						&& n.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR)) {
					errors.rejectValue("effectiveDate", "makepayments.effectivedate.invaliddate.past",
							"Day in the past");

				} else if (n.after(thresholdDate)) {
					errors.rejectValue("effectiveDate", "makepayments.effectivedate.invaliddate.future",
							"Date more than one year in future");

				}

			} else {
				errors.rejectValue("effectiveDate", "paymenthistory.date.required", "Date is required");
			}

			if (!"none".equalsIgnoreCase(paymentBean.getAccountFrom())
					&& !"none".equalsIgnoreCase(paymentBean.getPayeeid())) {
				TVFrontendAccount acc = Utils.getAccountById(paymentBean.getAccountFrom(), userSession.getAccounts());

				if (acc != null) {
					BigDecimal balance = new BigDecimal(0.00);
					BigDecimal bdAmount = (paymentBean.getAmount() == null ? (new BigDecimal(0)) : paymentBean
							.getAmount());
					if (acc.getSubtype().equals("CCA") && acc.getAvailableBalance() != null) {
						balance = acc.getAvailableBalance();
						if (balance.compareTo(bdAmount) < 0) {
							overpayCount++;
							logger.info("QP1 OverpayCount: " + overpayCount);
							if (overpayCount == 1 && balance.compareTo(bdAmount) < 0) {
								logger.info(Utils.neutralizeMessage("QP1 Rejection Balance: " + balance + "  Payment Amount: " + bdAmount));
								errors.rejectValue("amount", "payments.amt.overpay", "Invalid");
							}
						}
					} else {
						balance = acc.getPortalBalance();

						if (balance != null) {
							if (balance.compareTo(bdAmount) < 0) {
								overpayCount++;
								logger.info("QP2 OverpayCount: " + overpayCount);
								if (overpayCount == 1 && balance.compareTo(bdAmount) < 0) {
									logger.info(Utils.neutralizeMessage("QP2 Rejection Balance: " + balance + "  Payment Amount: " + bdAmount));
									errors.rejectValue("amount", "payments.amt.overpay", "Invalid");
								}
							}
						}
					}
				}
			}
		}
	}

	public Calendar calculateEffDate(Calendar cal) {
		List<Holiday> holidays = customerService.getHolidays();
		BankingDaysUtils bankingDaysUtils = new BankingDaysUtils(holidays);
		return bankingDaysUtils.calculatePayDaysWithCutoff(cal, 1, 17, 0);
	}

	public String getCancelView() {
		return cancelView;
	}

	public void setCancelView(String cancelView) {
		this.cancelView = cancelView;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	/**
	 * getTelepagoErrorDescriptor
	 * @param codigo
	 * @param len
	 * @return String
	 */
	private String getTelepagoErrorDescriptor(String codigo, String len) {
		String status = ""; // status description
		try {
			if (!len.equalsIgnoreCase("en"))
				len = "es";
			status = this.messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
		} catch (Exception e) {
			status = "";
		}

		return status;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}


	public String getBalanceDelay() {
		return balanceDelay;
	}

	public void setBalanceDelay(String balanceDelay) {
		this.balanceDelay = balanceDelay;
	}
}
