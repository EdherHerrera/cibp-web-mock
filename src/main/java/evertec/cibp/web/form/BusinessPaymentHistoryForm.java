package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.bouncycastle.asn1.ocsp.Request;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.VendorReceiptHistory;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.CancelPayeeResponse;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.FrequencyUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.vs.beans.payments.PaymentModify;
import evertec.cibp.vs.beans.payments.RecurringPayment;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.form.bean.EpayrollSearchBean;
import evertec.cibp.web.form.bean.ModPaymentBean;
import evertec.cibp.web.request.ModifyPaymentRequest;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.PaymentSort;
import evertec.cibp.web.utils.Utils;

// CIBP-1680 se agrega  implements MessageSourceAware para acceso a los archivos
public class BusinessPaymentHistoryForm extends CancellableFormController implements GlobalEntitlementAwareController,
        MessageSourceAware {
    /**
     * Logger
     */
    protected final Log logger = LogFactory.getLog(getClass());
    private int maxPayments = 15;

    private MessageSource messageSource;

    private String maxReverseableDays;
    private static String telenominaID;
    private static String vendorID;
    private static String businessPayments;
    protected CustomerServices customerService;

    public String getTelenominaID() {
        return telenominaID;
    }

    public void setTelenominaID(String telenominaID) {
        this.telenominaID = telenominaID;
    }

    public String getVendorID() {
        return vendorID;
    }

    public void setVendorID(String vendorID) {
        this.vendorID = vendorID;
    }

    public String getBusinessPayments() {
        return businessPayments;
    }

    public void setBusinessPayments(String businessPayments) {
        this.businessPayments = businessPayments;
    }

    public BusinessPaymentHistoryForm() {
        super();
        setCommandClass(EpayrollSearchBean.class);
        setCommandName("businesspaymenthistory");
        setFormView("businesspaymenthistory");
        setBindOnNewForm(true);
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
                                    BindException error) throws Exception {

        EpayrollSearchBean searchBean = (EpayrollSearchBean) command;
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerProfile customer = userSession.getCustomerProfile();

        int action = searchBean.getAction() == 0 ? 1 : searchBean.getAction();
        Map<String, Object> model = new LinkedHashMap<>();
        model.put("telenominaID", telenominaID);
        model.put("vendorID", vendorID);
        if (action == EpayrollSearchBean.SEARCH_BY_DATE && searchBean.getDate().equalsIgnoreCase("LAST_15"))
            action = EpayrollSearchBean.SEARCH_LAST15;
        if (action == EpayrollSearchBean.SEARCH_BY_PAYEE && searchBean.getPayeeId().equalsIgnoreCase("LAST_15"))
            action = EpayrollSearchBean.SEARCH_LAST15;

        if (action == EpayrollSearchBean.MOD_PAYMENT) {
            boolean hasalert = false;
            if (customer.hasEntitlement(EntitlementType.ALERTS)) {
                AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
                if (ent.getAlerts().contains(AlertType.EDITPAYROLL_SMS) || ent.getAlerts().contains(AlertType.EDITPAYROLL_PUSH)) {
                    hasalert = true;
                }
            }

            searchBean.setAction(1);
            PaymentResponse res = null;

            String tr = searchBean.getModPayment();
            TVPayment payment = null;
            ModPaymentBean modbean = null;
            for (Iterator<ModPaymentBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext(); ) {
                ModPaymentBean name = (ModPaymentBean) iterator.next();
                if (name != null)
                    if (tr.equalsIgnoreCase(name.getModTransferId())) {
                        payment = Utils.getPaymentById(tr, userSession.getPendingPayments());
                        modbean = name;
                        break;
                    }
            }

            // CIBP-1104 se utiliza para presentar en la alerta de correo el
            // pago original previo a la modificacion

            TVPayment paymentOriginal = new TVPayment(new RecurringPayment());
            paymentOriginal.setEstimateDate(payment.getEstimateDate());
            paymentOriginal.setPayee(payment.getPayee());
            paymentOriginal.getTVPayment().setEffectiveDate(payment.getTVPayment().getEffectiveDate());
            paymentOriginal.getTVPayment().setAmount(payment.getAmount());
            paymentOriginal.setSource(payment.getSource());
            paymentOriginal.getTVPayment().setFrequency(payment.getFrequency());
            paymentOriginal.getTVPayment().setRemainingInstances(payment.getTVPayment().getRemainingInstances());
            paymentOriginal.getTVPayment().setReferenceNumber(payment.getTVPayment().getReferenceNumber());

            // IS A RECURRING PAYMENT
            String freq = modbean.getModFreq();
            String inst = modbean.getModInstances();
            String modInstancesType = modbean.getModInstancesType();
            Date modDate = modbean.getModDate();
            Calendar calModDate = Calendar.getInstance();
            int favId = searchBean.getFavId();
            boolean err = false;

            if (GenericValidator.isBlankOrNull(payment.getFrequency())
                    || GenericValidator.isBlankOrNull(freq)
                    || GenericValidator.isBlankOrNull(modInstancesType)
                    || (!"infinite".equalsIgnoreCase(modInstancesType) && (!GenericValidator.matchRegexp(inst,
                    "^([1-9]|0[1-9]|[1-9][0-9]|[1-9][0-9]{1,2}|[0-9]{1,2}[0-9])$"))) || modDate == null
                // ||GenericValidator.isBlankOrNull(modDate)
                // ||!GenericValidator.matchRegexp(modDate,
                // "\\d{2}\\/\\d{2}\\/\\d{4}")
            ) {

                err = true;

                if (GenericValidator.isBlankOrNull(payment.getFrequency()) || GenericValidator.isBlankOrNull(freq))
                    error.rejectValue("favorites[" + favId + "].frequency", "", "Frequency is required");
                if (GenericValidator.isBlankOrNull(modInstancesType)
                        || (!"infinite".equalsIgnoreCase(modInstancesType) && (!GenericValidator.matchRegexp(inst,
                        "^([1-9]|0[1-9]|[1-9][0-9]|[1-9][0-9]{1,2}|[0-9]{1,2}[0-9])$"))))
                    error.rejectValue("favorites[" + favId + "].modInstances", "paymenthistory.instances.required",
                            "Instances must be filled");
                if (modDate == null)
                    error.rejectValue("favorites[" + favId + "].frequency", "paymenthistory.date.required",
                            "Date is required");
                else {
                    calModDate.setTime(modDate);
                    calModDate.set(Calendar.HOUR, 0);
                    calModDate.set(Calendar.MINUTE, 0);
                    calModDate.set(Calendar.SECOND, 0);
                    calModDate.set(Calendar.MILLISECOND, 0);

                    Calendar now = Calendar.getInstance();
                    now.set(Calendar.HOUR, 0);
                    now.set(Calendar.MINUTE, 0);
                    now.set(Calendar.SECOND, 0);
                    now.set(Calendar.MILLISECOND, 0);

                    if (!(calModDate.get(Calendar.MONTH) == now.get(Calendar.MONTH)
                            && calModDate.get(Calendar.YEAR) == now.get(Calendar.YEAR) && calModDate
                            .get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH))) {
                        err = calModDate.before(now);
                        if (err)
                            error.rejectValue("favorites[" + favId + "].modDate", "", "Date must be after today.");
                    }

                    now.add(Calendar.YEAR, 1);
                    err = calModDate.after(now);
                    if (err)
                        error.rejectValue("favorites[" + favId + "].modDate", "", "Date must be before a year.");
                }

                model.put("modPaymentErr", true);
                model.put("modPaymentErrId", favId);

            } else {
                calModDate.setTime(modDate);
            }

            if (!err) {
                // CIBP-2548 JOCAMPO
                if (payment.getPayee().getGlobalPayeeIdString() != null
                        && payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
                    payment.getPayee().setTelenomina(true);
                } else {
                    payment.getPayee().setTelenomina(false);
                }
                int instances = 1;

                if ("infinite".equalsIgnoreCase(modInstancesType)
                        || (inst.matches("\\d+") && Integer.parseInt(inst) > 750))
                    instances = 999;
                else if (!GenericValidator.isBlankOrNull(inst) && inst.matches("\\d+"))
                    instances = Integer.parseInt(inst);

				String taxId = customer.getEntitlementByType(EntitlementType.BANKING).getString("SSN");
				
				PaymentModify pmtMod = new PaymentModify();
				pmtMod.setNewFrequency(Frequency.valueOf(freq).getVaultCode());
				pmtMod.setNewAmount(payment.getAmount());
				pmtMod.setNewEffectiveDate(calModDate);
				
				ModifyPaymentRequest modifyPaymentRequest = new ModifyPaymentRequest(taxId,payment,pmtMod);
				modifyPaymentRequest.setInitialAmount(payment.getAmount());
				modifyPaymentRequest.setCustomerService(customerService);
				modifyPaymentRequest.setInstances(instances);
				modifyPaymentRequest.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
				modifyPaymentRequest.setUserSession(userSession);
				
				res = PaymentsServiceModule.getInstance().modifyPayment(modifyPaymentRequest);
            }
            List<TVPayment> listp = new LinkedList<>();
            listp.add(paymentOriginal);// CIBP-1104
            listp.add(res.getPayment());
            if (res != null && res.getStatus() == ResultStatus.SUCCESS) {
                userSession.resetPayees();
                userSession.setLoadedDelinquencyPmtInfo(false);
                if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
                    userSession.setStatusmessage("Payment was modified");
                else
                    userSession.setStatusmessage("Pago ha sido modificado");
                // CIBP-1104 se activa envio alerta de modificacion de pago
                if (hasalert) {
                    customerService.sendPaymentModifyConfirmation(customer, listp, null,
                            userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
                            userSession.getPreferredLanguage());
                }
            } else if (res != null) {

                String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request).toLowerCase());
                String str_error = "";
                if (er == null || er.equals("")) {
                    userSession.setErrorDetail(res.getErrorMessage());
                    if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
                        str_error = "Unable to modify payment" + "<a href='javascript:Show()' > Learn More</a>";
                        userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
                    } else {
                        str_error = "Error modificando el pago" + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
                        userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
                    }
                } else {
                    userSession.setErrorDetail(er);
                    if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
                        str_error = "Unable to modify payment" + "<a href='javascript:Show()' > Learn More</a>";
                        userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());

                    } else {
                        userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
                        str_error = "Error modificando el pago" + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
                    }
                }
                userSession.setStatusmessage(str_error);


            } else {
                if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                    userSession.setStatusmessage("Unable to modify payment");
                else
                    userSession.setStatusmessage("Error modificando el pago");
            }

        } else if (action == EpayrollSearchBean.MOD_AMT) {
            boolean hasalert = false;
            if (customer.hasEntitlement(EntitlementType.ALERTS)) {
                AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
                if (ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL)
                        || ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_SMS)
                        || ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH)) {
                    hasalert = true;
                }
            }

            searchBean.setAction(1);
            PaymentResponse res = null;

            String modPayment = searchBean.getModPayment();
            TVPayment payment = null;
            ModPaymentBean modbean = null;
            for (Iterator<ModPaymentBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext(); ) {
                ModPaymentBean name = (ModPaymentBean) iterator.next();
                if (name != null)
                    if (modPayment.equalsIgnoreCase(name.getModTransferId())) {
                        payment = Utils.getPaymentById(modPayment, userSession.getPendingPayments());
                        modbean = name;
                        break;
                    }
            }

            TVPayment paymentOriginal = new TVPayment(new RecurringPayment());
            paymentOriginal.setEstimateDate(payment.getEstimateDate());
            paymentOriginal.setPayee(payment.getPayee());
            paymentOriginal.getTVPayment().setEffectiveDate(payment.getTVPayment().getEffectiveDate());
            paymentOriginal.getTVPayment().setAmount(payment.getAmount());
            paymentOriginal.setSource(payment.getSource());
            paymentOriginal.getTVPayment().setFrequency(payment.getFrequency());
            paymentOriginal.getTVPayment().setRemainingInstances(payment.getTVPayment().getRemainingInstances());
            paymentOriginal.getTVPayment().setReferenceNumber(payment.getTVPayment().getReferenceNumber());

            // IS A SINGLE PAYMENT
            BigDecimal amt = modbean.getModAmount();

            if (amt != null && amt.doubleValue() >= new Double(0.01d)) {
                payment.getTVPayment().setProcessingDate(payment.getTVPayment().getEffectiveDate());
                String freq = payment.getFrequency();
                freq = FrequencyUtils.getStringFrequency(freq);
                // payment.getTVPayment().setAmount(amt);
                Calendar eff = Calendar.getInstance();
                eff.setTime(payment.getEffectiveDate());
                // CIBP-2548 JOCAMPO
                if (payment.getPayee().getGlobalPayeeIdString() != null
                        && payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
                    payment.getPayee().setTelenomina(true);
                } else {
                    payment.getPayee().setTelenomina(false);
                }
                
                String taxId = customer.getEntitlementByType(EntitlementType.BANKING).getString("SSN");
				
				PaymentModify pmtMod = new PaymentModify();
				pmtMod.setNewFrequency(Frequency.valueOf(freq).getVaultCode());
				pmtMod.setNewAmount(amt);
				pmtMod.setNewEffectiveDate(eff);
				
				ModifyPaymentRequest modifyPaymentRequest = new ModifyPaymentRequest(taxId,payment,pmtMod);
				modifyPaymentRequest.setInitialAmount(payment.getAmount());
				modifyPaymentRequest.setCustomerService(customerService);
				modifyPaymentRequest.setInstances((int) payment.getTVPayment().getRemainingInstances());
				modifyPaymentRequest.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
				modifyPaymentRequest.setUserSession(userSession);
				
				res = PaymentsServiceModule.getInstance().modifyPayment(modifyPaymentRequest);

            }
            List<TVPayment> listp = new LinkedList<>();
            listp.add(paymentOriginal);// CIBP-1104
            listp.add(res.getPayment());
            if (res != null && res.getStatus() == ResultStatus.SUCCESS) {
                userSession.resetPayees();
                userSession.setLoadedDelinquencyPmtInfo(false);
                if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
                    userSession.setStatusmessage("Payment was modified");
                else
                    userSession.setStatusmessage("Pago ha sido modificado");

                if (hasalert) {
                    customerService.sendPaymentModifyConfirmation(customer, listp, null,
                            userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
                            userSession.getPreferredLanguage());
                }
            } else if (res != null) {

                String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request).toLowerCase());
                String str_error = "";
                if (er == null || "".equals(er)) {
                    userSession.setErrorDetail(res.getErrorMessage());
                    if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
                        str_error = "Unable to modify payment" + "<a href='javascript:Show()'> Learn More</a>";
                        userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
                    } else {
                        str_error = "Error modificando el pago" + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
                        userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
                    }
                } else {
                    userSession.setErrorDetail(er);
                    if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
                        str_error = "Unable to modify payment" + "<a href='javascript:Show()'> Learn More</a>";
                        userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());

                    } else {
                        userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
                        str_error = "Error modificando el pago" + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
                    }
                }
                userSession.setStatusmessage(str_error);

            } else {
                if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                    userSession.setStatusmessage("Unable to modify payment");
                else
                    userSession.setStatusmessage("Error modificando el pago");
            }
        } else if (action == EpayrollSearchBean.MOD_AMT_DATE) {
            boolean hasalert = false;
            if (customer.hasEntitlement(EntitlementType.ALERTS)) {
                AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
                if (ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL)
                        || ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_SMS)
                        || ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH)) {
                    hasalert = true;
                }
            }

            searchBean.setAction(1);
            // IS A SINGLE PAYMENT

            PaymentResponse res = null;
            String modPayment = searchBean.getModPayment();
            TVPayment payment = null;
            ModPaymentBean modbean = null;
            for (Iterator<ModPaymentBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext(); ) {
                ModPaymentBean name = (ModPaymentBean) iterator.next();
                if (name != null)
                    if (modPayment.equalsIgnoreCase(name.getModTransferId())) {
                        payment = Utils.getPaymentById(modPayment, userSession.getPendingPayments());
                        modbean = name;
                        break;
                    }
            }

            TVPayment paymentOriginal = new TVPayment(new RecurringPayment());
            paymentOriginal.setEstimateDate(payment.getEstimateDate());
            paymentOriginal.setPayee(payment.getPayee());
            paymentOriginal.getTVPayment().setEffectiveDate(payment.getTVPayment().getEffectiveDate());
            paymentOriginal.getTVPayment().setAmount(payment.getAmount());
            paymentOriginal.setSource(payment.getSource());
            paymentOriginal.getTVPayment().setFrequency(payment.getFrequency());
            paymentOriginal.getTVPayment().setRemainingInstances(payment.getTVPayment().getRemainingInstances());
            paymentOriginal.getTVPayment().setReferenceNumber(payment.getTVPayment().getReferenceNumber());

            BigDecimal amt = modbean.getModAmount();

            if (amt != null && amt.doubleValue() >= new Double(0.01d)) {
                payment.getTVPayment().setProcessingDate(payment.getTVPayment().getEffectiveDate());
                String freq = payment.getFrequency();
                freq = FrequencyUtils.getStringFrequency(freq);
                Date modDate = modbean.getModDate();
                Calendar calModDate = Calendar.getInstance();
                boolean err = false;
                err = (modDate == null);
                if (!err) {

                    calModDate.setTime(modDate);

                    calModDate.set(Calendar.HOUR, 0);
                    calModDate.set(Calendar.MINUTE, 0);
                    calModDate.set(Calendar.SECOND, 0);
                    calModDate.set(Calendar.MILLISECOND, 0);

                    Calendar now = Calendar.getInstance();
                    now.set(Calendar.HOUR, 0);
                    now.set(Calendar.MINUTE, 0);
                    now.set(Calendar.SECOND, 0);
                    now.set(Calendar.MILLISECOND, 0);

                    if (!(calModDate.get(Calendar.MONTH) == now.get(Calendar.MONTH)
                            && calModDate.get(Calendar.YEAR) == now.get(Calendar.YEAR) && calModDate
                            .get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH)))
                        err = calModDate.before(now);
                    now.add(Calendar.YEAR, 1);
                    err = calModDate.after(now);
                }
                // Se valida si no hay error en la fecha
                if (!err) {
                    // CIBP-2548 JOCAMPO
                    if (payment.getPayee().getGlobalPayeeIdString() != null
                            && payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
                        payment.getPayee().setTelenomina(true);
                    } else {
                        payment.getPayee().setTelenomina(false);
                    }
                    
                    String taxId = customer.getEntitlementByType(EntitlementType.BANKING).getString("SSN");
    				
    				PaymentModify pmtMod = new PaymentModify();
    				pmtMod.setNewFrequency(Frequency.valueOf(freq).getVaultCode());
    				pmtMod.setNewAmount(amt);
    				pmtMod.setNewEffectiveDate(calModDate);
    				
    				ModifyPaymentRequest modifyPaymentRequest = new ModifyPaymentRequest(taxId,payment,pmtMod);
    				modifyPaymentRequest.setInitialAmount(payment.getAmount());
    				modifyPaymentRequest.setCustomerService(customerService);
    				modifyPaymentRequest.setInstances(1);
    				modifyPaymentRequest.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
    				modifyPaymentRequest.setUserSession(userSession);
    				
    				res = PaymentsServiceModule.getInstance().modifyPayment(modifyPaymentRequest);
                }
            }
            List<TVPayment> listp = new LinkedList<>();
            listp.add(paymentOriginal);// CIBP-1104
            listp.add(res.getPayment());
            if (res != null && res.getStatus() == ResultStatus.SUCCESS) {
                userSession.resetPayees();
                userSession.setLoadedDelinquencyPmtInfo(false);
                if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
                    userSession.setStatusmessage("Payment was modified");
                else
                    userSession.setStatusmessage("Pago ha sido modificado");

                if (hasalert) {
                    customerService.sendPaymentModifyConfirmation(customer, listp, null,
                            userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
                            userSession.getPreferredLanguage());
                }
            } else if (res != null) {

                String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request).toLowerCase());
                String str_error = "";
                if (er == null || "".equals(er)) {
                    userSession.setErrorDetail(res.getErrorMessage());
                    if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
                        str_error = "Unable to modify payment" + "<a href='javascript:Show()' > Learn More</a>";
                        userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
                    } else {
                        str_error = "Error modificando el pago" + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
                        userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
                    }
                } else {
                    userSession.setErrorDetail(er);
                    if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
                        str_error = "Unable to modify payment" + "<a href='javascript:Show()' > Learn More</a>";
                        userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
                    } else {
                        userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
                        str_error = "Error modificando el pago" + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
                    }
                }
                userSession.setStatusmessage(str_error);
            }

        } else if (action == EpayrollSearchBean.CAN_PAYMENT) {
            boolean hasalert = false;
            if (customer.hasEntitlement(EntitlementType.ALERTS)) {
                AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));                if (ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL)
                        || ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_SMS)
                        || ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH)
                        || ent.getAlerts().contains(AlertType.EDITPAYROLL_SMS)
                        || ent.getAlerts().contains(AlertType.EDITPAYROLL_PUSH)
                        || ent.getAlerts().contains(AlertType.VENDOR_EDIT_DELETE_SMS)
                        || ent.getAlerts().contains(AlertType.VENDOR_EDIT_DELETE_PUSH)) {
                    hasalert = true;
                }
            }

            searchBean.setAction(1);
            String tr = searchBean.getModPayment();

            TVPayment payment = null;
            if (tr != null && !tr.equalsIgnoreCase("") && userSession.getPendingPayments() != null)
                payment = Utils.getPaymentById(tr, userSession.getPendingPayments());

            CancelPayeeResponse can;
            if (payment != null) {
                // CIBP-2548 JOCAMPO
                if (payment.getPayee().getGlobalPayeeIdString() != null
                        && payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
                    payment.getPayee().setTelenomina(true);
                } else {
                    payment.getPayee().setTelenomina(false);
                }
                can = customerService.cancelPaymentWithErrorDescription(customer, userSession.getBankingSession(),
                        payment, Utils.generateCustomerInteractionEvent(request));

                List<TVPayment> listp = new LinkedList<>();
                Boolean hasReceipt = customerService.hasVendorReceipt(payment.getTVPayment().getReferenceNumber());


                if (hasReceipt) {
                    VendorReceiptHistory vendorReceipt = customerService.getVendorReceiptHistory(payment.getTVPayment().getReferenceNumber());
                    payment.setVendorSubject(vendorReceipt.getConcept());
                    payment.setComments(vendorReceipt.getComments());
                    payment.setInvoiceNumber(vendorReceipt.getInvoiceNumber());
                    payment.setEmail(vendorReceipt.getEmail());
                }
                listp.add(payment);
                if (can.getResult().equals(CancelPayeeResponse.Result.SUCCESS)) {


                    userSession.setLoadedDelinquencyPmtInfo(false);
                    if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
                        userSession.setStatusmessage("Payment was deleted");
                    else
                        userSession.setStatusmessage("Pago ha sido borrado");

                    customerService.sendBusinessPaymentsCancelConfirmation(customer, model, listp, null,
                            userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)), userSession.getPreferredLanguage(), hasalert);
                    
                    //Tiene Recibo de Vendor
                    if (hasReceipt) {
                        ///Se envia el correo de la cancelacion del Vendor Receipt
                        customerService.sendVendorPaymentCancelRecipConf(customer, listp, userSession.getPreferredLanguage());
                        //Eliminamos el Recibo de la tabla
                        customerService.deleteVendorReceipt(payment.getTVPayment().getReferenceNumber());
                    }


                } else {

                    String er = getTelepagoErrorDescriptor(can.getErrorEBA(), Utils.getRSALanguage(request)
                            .toLowerCase());
                    String str_error = "";
                    if (er == null || "".equals(er)) {
                        userSession.setErrorDetail(can.getErrorString());
                        if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
                            str_error = "Unable to delete payment" + "<a href='javascript:Show()' > Learn More</a>";
                            userSession.setErrorDetailCod("Error Code: " + can.getErrorEBA());
                        } else {
                            str_error = "Error borrando  el pago"
                                    + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
                            userSession.setErrorDetailCod("C&oacute;digo de error: " + can.getErrorEBA());
                        }
                    } else {
                        userSession.setErrorDetail(er);
                        if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
                            str_error = "Unable to delete payment" + "<a href='javascript:Show()' > Learn More</a>";
                            userSession.setErrorDetailCod("Error Code: " + can.getErrorEBA());

                        } else {
                            userSession.setErrorDetailCod("C&oacute;digo de error: " + can.getErrorEBA());
                            str_error = "Error borrando  el pago"
                                    + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
                        }
                    }
                    userSession.setStatusmessage(str_error);

                }
            }
            userSession.resetPayees();
        } else if (action == EpayrollSearchBean.REVERSE_PAYMENT) {
            searchBean.setAction(1);

            String tr = searchBean.getModPayment();
            String payeeId = searchBean.getPayeeId();
            String referenceNum = searchBean.getReferenceNum();

            TVPayment payment = null;

            if (tr != null && payeeId != null && !tr.equalsIgnoreCase("") && !payeeId.equalsIgnoreCase("")
                    && userSession.getCustomerPayments() != null)
                payment = Utils.getProcessedPaymentById(tr, payeeId, referenceNum, userSession.getCustomerPayments());
            PaymentResponse res;
            if (payment != null) {
                // CIBP-2548 JOCAMPO
                if (payment.getPayee().getGlobalPayeeIdString() != null
                        && payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
                    payment.getPayee().setTelenomina(true);
                } else {
                    payment.getPayee().setTelenomina(false);
                }
                res = PaymentsServiceModule.getInstance().reversePayment(customer, userSession.getBankingSession(), payment,
                        Utils.generateCustomerInteractionEvent(request), customerService);

                if (res != null && res.getStatus() == ResultStatus.SUCCESS) {
                    userSession.setLoadedDelinquencyPmtInfo(false);
                    if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
                        userSession.setStatusmessage("Payment was reversed");
                    else
                        userSession.setStatusmessage("Pago ha sido revertido");

                } else {

                    String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request)
                            .toLowerCase());
                    String str_error = "";
                    if (er == null || "".equals(er)) {
                        userSession.setErrorDetail(res.getErrorString());
                        if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
                            str_error = "Unable to reverse payment" + "<a href='javascript:Show()' > Learn More</a>";
                            userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
                        } else {
                            str_error = "Error revirtiendo  el pago"
                                    + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
                            userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
                        }
                    } else {
                        userSession.setErrorDetail(er);
                        if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
                            str_error = "Unable to reverse payment" + "<a href='javascript:Show()' > Learn More</a>";
                            userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());

                        } else {
                            userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
                            str_error = "Error revirtiendo  el pago"
                                    + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
                        }
                    }
                    userSession.setStatusmessage(str_error);
                    // Por si en el futuro se desea alertas para payment
                    // reverse, hay
                    // que crear metodo sendPaymentReverseConfirmation en
                    // customerService
                    // if(hasalert) {
                    // customerService.sendPaymentModifyConfirmation(customer,
                    // null, listp,
                    // userSession.getSmsProfile(),userSession.getPreferredLanguage());
                    // }
                }
            }
            userSession.resetPayees();
        }

        return showForm(request, response, error, model);
    }

    @Override
    protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
            throws Exception {

        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        EpayrollSearchBean searchBean = (EpayrollSearchBean) command;
        final String realTimeNotificationsClaroKey = "realTimeNotificationsClaro"; // rt flag


        List<TVFrontEndPayee> payees = userSession.getPayees(true);
        String paymentTypeS = searchBean.getPaymentType();

        Map<String, Object> model = new HashMap<>();
        Map<String, Object> paymentMap = new LinkedHashMap<>();
        model.put("isEvendorActivated", StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR));
        final boolean rtFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO); // flag
        model.put(realTimeNotificationsClaroKey, rtFlag);
        model.put("telenominaID", telenominaID);
        String lblAll; // lblAll
        String lblDate; // lblDate
        String lblPayroll; // lblPayroll
        String lblVendor;

        if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
            lblAll = "All";
            lblDate = "Recent Payments";
            lblPayroll = "Payroll";
            lblVendor = "Vendor";
        } else {
            lblAll = "Todos";
            lblDate = "Pagos Recientes";
            lblPayroll = "N&oacute;mina";
            lblVendor = "Suplidores";
        }
        paymentMap.put("All", lblAll);
        paymentMap.put(telenominaID, lblPayroll);
        paymentMap.put(vendorID, lblVendor);

        String[] businessPayee = {telenominaID, vendorID};

        model.put("payeeMap", this.getDropDownPayeeBP(paymentTypeS, businessPayee, request));
        model.put("favorites", userSession.getFavoritePayments());

        Map<String, Object> datemap = new LinkedHashMap<>();
        List<Date> allDates = new LinkedList<>();
        // 12 meses
        Calendar now = Calendar.getInstance();
        SimpleDateFormat simp = new SimpleDateFormat("yyyyMM");
        SimpleDateFormat month = new SimpleDateFormat("MMMMM");
        datemap.put("LAST_15", lblDate);

        for(int i = 1;i < 11; i++) {
        	
            now.add(Calendar.MONTH, -1);
            datemap.put(simp.format(now.getTime()), month.format(now.getTime()) + " " + now.get(Calendar.YEAR));
            allDates.add(new Date(now.getTimeInMillis()));            
        }
        
        model.put("paymentMap", paymentMap);
        model.put("alldates", allDates);
        model.put("allpayees", payees);
        model.put("dateMap", datemap);


        Map<String, Object> objParam = new HashMap<>();
        objParam.put("businessPayee", businessPayee);
        objParam.put("maxPayments", maxPayments);
        objParam.put("paymentTypeS", paymentTypeS);
        objParam.put("maxReverseableDays", maxReverseableDays);

        Map<String, Object> businessPayment = Utils.getBusinessPaymentList(request, searchBean, objParam);

        List<TVPayment> customerPayments;
        List<TVPayment> customerPendingPayments;
        customerPayments = (List<TVPayment>) businessPayment.get("processedPayments");
        customerPendingPayments = (List<TVPayment>) businessPayment.get("pendingPayments");

        model.put("totalInProcess", businessPayment.get("totalInProcess"));
        Collections.sort(customerPayments, new PaymentSort());
        model.put("processedPayments", customerPayments);
        Collections.sort(customerPendingPayments, new PaymentSort());
        model.put("pendingPayments", customerPendingPayments);
        userSession.setPendingPayments(customerPendingPayments);
        userSession.setCustomerPayments(customerPayments);

        return model;

    }

    public int getMaxPayments() {
        return maxPayments;
    }

    public void setMaxPayments(int maxPayments) {
        this.maxPayments = maxPayments;
    }

    @Override
    protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        EpayrollSearchBean searchBean = (EpayrollSearchBean) command;
        int action = searchBean.getAction() == 0 ? 1 : searchBean.getAction();

        if (action == EpayrollSearchBean.MOD_PAYMENT) {
            PaymentResponse res = null;
            String tr = searchBean.getModPayment();
            TVPayment payment = null;
            ModPaymentBean modbean = null;
            for (Iterator<ModPaymentBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext(); ) {
                ModPaymentBean name = (ModPaymentBean) iterator.next();
                if (name != null)
                    if (tr.equalsIgnoreCase(name.getModTransferId())) {
                        payment = Utils.getPaymentById(tr, userSession.getPendingPayments());
                        modbean = name;
                        break;
                    }
            }

            int favId = searchBean.getFavId();
            if (payment == null) {
                errors.rejectValue("favorites[" + favId + "].modFreq", "", "Frequency is required");
                return;
            }

            // IS A RECURRING PAYMENT
            String freq = modbean.getModFreq();
            String inst = modbean.getModInstances();
            String modInstancesType = modbean.getModInstancesType();
            Date modDate = modbean.getModDate();
            Calendar calModDate = Calendar.getInstance();

            boolean err = false;

            if (GenericValidator.isBlankOrNull(payment.getFrequency())
                    || GenericValidator.isBlankOrNull(freq)
                    || GenericValidator.isBlankOrNull(modInstancesType)
                    || (!"infinite".equalsIgnoreCase(modInstancesType) && (!GenericValidator.matchRegexp(inst,
                    "^([1-9]|0[1-9]|[1-9][0-9]|[1-9][0-9]{1,2}|[0-9]{1,2}[0-9])$"))) || modDate == null) {

                err = true;

                if (GenericValidator.isBlankOrNull(payment.getFrequency()) || GenericValidator.isBlankOrNull(freq))
                    errors.rejectValue("favorites[" + favId + "].modFreq", "", "Frequency is required");
                if (GenericValidator.isBlankOrNull(modInstancesType)
                        || (!"infinite".equalsIgnoreCase(modInstancesType) && (!GenericValidator.matchRegexp(inst,
                        "^([1-9]|0[1-9]|[1-9][0-9]|[1-9][0-9]{1,2}|[0-9]{1,2}[0-9])$"))))
                    errors.rejectValue("favorites[" + favId + "].modInstances", "paymenthistory.instances.required",
                            "Instances must be filled");
                if (modDate == null) {
                    modbean.setPendingDate(modDate);
                    errors.rejectValue("favorites[" + favId + "].modDate", "paymenthistory.date.required",
                            "Date is required");

                } else {
                    calModDate.setTime(modDate);
                    calModDate.set(Calendar.HOUR, 0);
                    calModDate.set(Calendar.MINUTE, 0);
                    calModDate.set(Calendar.SECOND, 0);
                    calModDate.set(Calendar.MILLISECOND, 0);
                    Calendar now = Calendar.getInstance();
                    now.set(Calendar.HOUR, 0);
                    now.set(Calendar.MINUTE, 0);
                    now.set(Calendar.SECOND, 0);
                    now.set(Calendar.MILLISECOND, 0);
                    if (!(calModDate.get(Calendar.MONTH) == now.get(Calendar.MONTH)
                            && calModDate.get(Calendar.YEAR) == now.get(Calendar.YEAR) && calModDate
                            .get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH))) {
                        err = calModDate.before(now);
                        errors.rejectValue("favorites[" + favId + "].modDate", "", "Date must be after today.");
                    } else if (err) {
                        modbean.setPendingDate(modDate);
                    } else {
                        now.add(Calendar.YEAR, 1);
                        err = calModDate.after(now);
                        if (err) {
                            modbean.setPendingDate(modDate);
                            errors.rejectValue("favorites[" + favId + "].modDate", "", "Date must be after today.");
                        }
                    }
                }
            }
        } else if (action == EpayrollSearchBean.MOD_AMT) {
            String modPayment = searchBean.getModPayment();
            ModPaymentBean modbean = null;
            int favId = searchBean.getFavId();

            for (Iterator<ModPaymentBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext(); ) {
                ModPaymentBean name = (ModPaymentBean) iterator.next();
                if (name != null)
                    if (modPayment.equalsIgnoreCase(name.getModTransferId())) {
                        modbean = name;
                        break;
                    }
            }
            if (modbean != null) {
                // IS A SINGLE PAYMENT
                modbean.setPendingAmount(modbean.getModAmount());
                BigDecimal amt = modbean.getModAmount();
                if (amt != null && amt.doubleValue() >= new Double(0.01d)) {
                    modbean.setPendingAmount(modbean.getModAmount());
                } else
                    errors.rejectValue("favorites[" + favId + "].modAmount", "paymentshistory.amount.required",
                            "Amount is required");
            } else {
                errors.rejectValue("favorites[" + favId + "].modAmount", "paymentshistory.amount.required",
                        "Amount is required");
            }

        } else if (action == EpayrollSearchBean.MOD_AMT_DATE) {
            // IS A SINGLE PAYMENT

            PaymentResponse res = null;
            int favId = searchBean.getFavId();

            boolean err = false;
            String modPayment = searchBean.getModPayment();
            TVPayment payment = null;
            ModPaymentBean modbean = null;
            for (Iterator<ModPaymentBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext(); ) {
                ModPaymentBean name = (ModPaymentBean) iterator.next();
                if (name != null)
                    if (modPayment.equalsIgnoreCase(name.getModTransferId())) {
                        payment = Utils.getPaymentById(modPayment, userSession.getPendingPayments());
                        modbean = name;
                        break;
                    }
            }

            if (modbean != null) {
                modbean.setPendingAmount(modbean.getModAmount());
                Date modDate = modbean.getModDate();
                Calendar calModDate = Calendar.getInstance();

                BigDecimal amt = modbean.getModAmount();
                if (amt == null || amt.doubleValue() < new Double(0.01d)) {
                    errors.rejectValue("favorites[" + favId + "].modAmount", "paymentshistory.amount.required",
                            "Amount is required");
                }

                if (modDate == null)
                    errors.rejectValue("favorites[" + favId + "].modDate", "paymenthistory.date.required",
                            "Date is required");
                else {
                    calModDate.setTime(modDate);
                    calModDate.setTime(modDate);
                    calModDate.set(Calendar.HOUR, 0);
                    calModDate.set(Calendar.MINUTE, 0);
                    calModDate.set(Calendar.SECOND, 0);
                    calModDate.set(Calendar.MILLISECOND, 0);

                    Calendar now = Calendar.getInstance();
                    now.set(Calendar.HOUR, 0);
                    now.set(Calendar.MINUTE, 0);
                    now.set(Calendar.SECOND, 0);
                    now.set(Calendar.MILLISECOND, 0);

                    if (!(calModDate.get(Calendar.MONTH) == now.get(Calendar.MONTH)
                            && calModDate.get(Calendar.YEAR) == now.get(Calendar.YEAR) && calModDate
                            .get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH)))
                        err = calModDate.before(now);
                    if (err) {
                        modbean.setPendingDate(modDate);
                        errors.rejectValue("favorites[" + favId + "].modDate", "", "Date must be after today.");
                    } else {
                        now.add(Calendar.YEAR, 1);
                        err = calModDate.after(now);
                        if (err) {
                            modbean.setPendingDate(modDate);
                            errors.rejectValue("favorites[" + favId + "].modDate", "", "Date must be after today.");
                        }
                    }
                }
            } else {
                errors.rejectValue("favorites[" + favId + "].modAmount", "paymentshistory.amount.required",
                        "Amount is required");
            }
        }
    }

    private String getTelepagoErrorDescriptor(String codigo, String len) {
        String status = "";
        try {
            if (!"en".equals(len))
                len = "es";
            status = this.messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
        } catch (Exception e) {
            status = "";
        }

        return status;
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public EntitlementType getGlobalEntitlement() {
        return EntitlementType.BILLPAY;
    }

    public String getMaxReverseableDays() {
        return maxReverseableDays;
    }

    public void setMaxReverseableDays(String maxReverseableDays) {
        this.maxReverseableDays = maxReverseableDays;
    }


    private Map<String, String> getDropDownPayeeBP(String paymentTypeS, String[] businessPayee, HttpServletRequest request) {

        Map<String, String> payeeMap = new LinkedHashMap<>();
        AccountMaskUtil mask = new AccountMaskUtil();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        List<TVFrontEndPayee> payees = userSession.getPayees(true);

        String lblAll;
        String lblPayroll;
        String lblVendor;
        if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
            lblAll = "All Business Payments";
            lblPayroll = "All Employee payees";
            lblVendor = "All Vendors payees";
        } else {
            lblAll = "Todos los Comercios";
            lblPayroll = "Todos los pagos de N&oacute;mina";
            lblVendor = "Todos los pagos de Suplidores";
        }

        if (paymentTypeS.equalsIgnoreCase("All")) {
            payeeMap.put("LAST_15", lblAll);
        } else if (paymentTypeS.equalsIgnoreCase(telenominaID)) {
            payeeMap.put("LAST_15", lblPayroll);
        } else if (paymentTypeS.equalsIgnoreCase(vendorID)) {
            payeeMap.put("LAST_15", lblVendor);
        }


        if (payees != null)
            for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext(); ) {
                TVFrontEndPayee pay = iterator.next();
                boolean isBusinessPayment = ArrayUtils.contains(businessPayee, pay.getGlobalPayeeIdString());
                if (pay.getGlobalPayeeIdString() != null && pay.getGlobalPayeeIdString().equalsIgnoreCase(paymentTypeS)) {
                    payeeMap.put(pay.getFrontEndId(), pay.getNickname() + " " + mask.mask(pay.getBillingAccount()));
                } else if (paymentTypeS.equalsIgnoreCase("All") && isBusinessPayment) {
                    payeeMap.put(pay.getFrontEndId(), pay.getNickname() + " " + mask.mask(pay.getBillingAccount()));
                }
            }

        return payeeMap;
    }


    public CustomerServices getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerServices customerService) {
        this.customerService = customerService;
    }
}
