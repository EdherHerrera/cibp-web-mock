package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.base.AbstractStatement;
import evertec.cibp.core.models.customer.CustomerPayeePreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.holiday.Holiday;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.BankingDaysUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.QuickPortalPaymentBean;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class QuickPaymentPortalForm extends AbstractWizardFormController {

	private String cancelView;
	protected final Log logger = LogFactory.getLog(getClass());
	protected CustomerServices customerService;
	private int overpayCount = 0;
	private String telenominaID;
	private MessageSource messageSource;
	private String balanceDelay = "";

	public String getTelenominaID() {
		return telenominaID;
	}

	public void setTelenominaID(String telenominaID) {
		this.telenominaID = telenominaID;
	}

	public QuickPaymentPortalForm() {
		super();
		setCommandClass(QuickPortalPaymentBean.class);
		setCommandName("quickpaymentportal");
		setPages(new String[] { "quickpaymentportal", "quickpaymentportal" });
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		boolean MBMT_511 = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT511);

		QuickPortalPaymentBean paymentBean = (QuickPortalPaymentBean) command;
		Map<String, Object> model = new HashMap<>();
		model.put("telenominaID", telenominaID);
		List<TVPayment> confirmPayments = null;
		List<TVPayment> errorPayments = null;
		overpayCount = 0;

		if (paymentBean != null) {
			String accountfrom = "";
			if (!paymentBean.getAccountFrom().equalsIgnoreCase("none")) {
				accountfrom = paymentBean.getAccountFrom();
			} else {
				accountfrom = paymentBean.getAccountFrom();
			}

			TVFrontendAccount sou = Utils.getAccountById(accountfrom, userSession.getAccounts());
			TVFrontEndPayee tar = Utils.getPayeebyId(paymentBean.getPayeeid(), userSession.getPayees(false));
			BigDecimal amount = paymentBean.getAmount();

			Calendar cal = Calendar.getInstance();
			if (paymentBean != null && paymentBean.getEffectiveDate() != null)
				cal.setTime(paymentBean.getEffectiveDate());

			TVPayment payment = null;
			payment = new TVPayment(amount, Frequency.MANUALLY, 0, cal, new TVAccountTransferSource(sou), tar);

			// CIBP-2548 JOCAMPO
			if (payment.getPayee().getGlobalPayeeIdString() != null
					&& payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
				payment.getPayee().setTelenomina(true);
			} else {
				payment.getPayee().setTelenomina(false);
			}
			PaymentResponse res = PaymentsServiceModule.sendPayments(customer, userSession, payment,
					Utils.generateCustomerInteractionEvent(request), customerService);
			payment = res.getPayment();
			payment.getTVPayment().setEffectiveDate(cal);
			if (res.getStatus() == ResultStatus.SUCCESS) {
				
				// Incidente de actualizar balance en tiempo real
				if (MBMT_511) {
					TVFrontendAccount frontendAccountToUpdate = Utils
							.getAccountByAccountNumber(payment.getSourceAccountNumber(), userSession.getAccounts());
					if (frontendAccountToUpdate.getSubtype() != null
							&& frontendAccountToUpdate.getSubtype().equals("IDA")) {
						List<TVFrontendAccount> frontEndAccounts2Update = new LinkedList<TVFrontendAccount>();
						frontEndAccounts2Update.add(frontendAccountToUpdate);
						userSession.updateAccountBalance(frontEndAccounts2Update, balanceDelay);
					}
				}

				customerService.logOccPaymentAdd(customer, Utils.generateCustomerInteractionEvent(request),
						InteractionSeverity.INFO, amount);
				// Alizano hide new ebill message when payment is success Fix
				// Incident 1537231
				res.getPayment().getPayee().setHasNewEbill(false);
				if (res.getPayment().getPayee().getEbpp2UserAccount() != null)
					res.getPayment().getPayee().getEbpp2UserAccount().setLastEbillPaid(true);
				// End Fix
				model.put("PAYMENTSEND", true);
				model.put("payment", payment);
				confirmPayments = new LinkedList<TVPayment>();
				confirmPayments.add(payment);
				model.put("quickpayment", paymentBean);

				// Mark is the Ebills PDF is not view
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
					if (tar.getBillingAccount() != null) {
						TVFrontendAccount cca = Utils.getAccountByAccountNumber(tar.getBillingAccount(),
								userSession.getAccounts());
						List<AbstractStatement> ebills = Utils.statementsByAccount(cca, userSession, request);
						if (ebills != null && ebills.size() > 1) {

							Utils.markPdfViewed(ebills.get(1), cca, userSession);
							int pid = tar.getGlobalPayeeId();
							
							if(pid == 359 || pid == 517 || pid == 518 || pid == 1983 || pid == 223){
							
								CustomerPayeePreferences customerPayeePreferences = tar.getCustomerPayeePreferences();
																	
								SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
								customerPayeePreferences.setLastPaymentDate(format.format(ebills.get(1).getEndDate()));
								
								tar.setCustomerPayeePreferences(customerPayeePreferences);
								customerService.modifyPayeeFrequentPreference(userSession.getCustomerProfile(), userSession.getBankingSession(), tar, tar.getFavorite());
						
							}
							
						}
					}
				} // End EBILLS_STATEMENTS

				
			} else {
				payment.setPaymentMessage("ERROR REASON: " + res.getErrorMessage());
				model.put("PAYMENTSEND", false);
				model.put("payment", payment);
				errorPayments = new LinkedList<TVPayment>();
				errorPayments.add(payment);
				model.put("quickpayment", paymentBean);

			}
			
			model.put("payment", payment);
			model.put("quickpayment", paymentBean);

		}

		boolean hasalert = false;
		if (customer.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
			if (ent.getAlerts().contains(AlertType.PAYMENTS_EMAIL) || ent.getAlerts().contains(AlertType.PAYMENTS_SMS)
					|| ent.getAlerts().contains(AlertType.PAYMENTS_PUSH)) {
				hasalert = true;
			}

		}
		if (hasalert) {
			customerService.sendPaymentConfirmation(customer, confirmPayments, errorPayments,
					userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
					userSession.getPreferredLanguage());
		}

		model.put("page", "1");

		loadData(request, command, model);
		return new ModelAndView("quickpaymentportal", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		userSession.setLoadebpp(false);

		Map<String, Object> model = new HashMap<>();
		model.put("page", page);
		model.put("telenominaID", telenominaID);
		GlobalEntitlement ent = customerService.getGlobalEntitlement(EntitlementType.BILLPAY);

		if (ent == null || ent.getStatus() != EntitlementStatus.ENABLED) {
			model.put("down", "true");
			return model;
		}

		loadData(request, command, model);

		return model;
	}

	private void loadData(HttpServletRequest request, Object command, Map<String, Object> model) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		QuickPortalPaymentBean bean = (QuickPortalPaymentBean) command;
		String accountfrontEndId = null;
		TVFrontendAccount account = null;

		String ac = (String) request.getParameter("referenceAccountId");
		if (ac != null) {
			accountfrontEndId = ac;
			bean.setReferenceAccountId(accountfrontEndId);
			account = Utils.getAccountById(accountfrontEndId, userSession.getAccounts());
		} else if (bean.getReferenceAccountId() != null) {
			accountfrontEndId = bean.getReferenceAccountId();
			bean.setReferenceAccountId(accountfrontEndId);
			account = Utils.getAccountById(accountfrontEndId, userSession.getAccounts());
		}

		if (account != null)
			model.put("act", account);

		Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsPaymentFrom();
		AccountMaskUtil mask = new AccountMaskUtil();
		NumberTool numberTool = new NumberTool();
		Map<String, String> accountsFromMap = new LinkedHashMap<String, String>();
		// Used by JSON View for apps
		Map<String, TVAccountTransferSource> accountFromReferenceMap = new LinkedHashMap<String, TVAccountTransferSource>();

		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			accountsFromMap.put("none", "Account");
		else
			accountsFromMap.put("none", "Cuenta");
		for (Iterator<TVAccountTransferSource> iterator = accountsFrom.iterator(); iterator.hasNext();) {
			TVAccountTransferSource from = (TVAccountTransferSource) iterator.next();
			TVFrontendAccount frontFrom = from.getAccount();
			if (!from.getFrontEndId().equals(accountfrontEndId) && frontFrom != null && !frontFrom.getHide()) {
				if (frontFrom.getSubtype().equals("CCA") && frontFrom.getAvailableBalance() != null)
					accountsFromMap.put(
							from.getFrontEndId(),
							StringUtils.abbreviate(frontFrom.getNickname(), 15)
									+ " "
									+ mask.mask(frontFrom.getAccountNumber())
									+ " "
									+ numberTool.format("currency", frontFrom.getAvailableBalance())
									+ ((frontFrom.getAccountNumberSuffix() != null && !frontFrom
											.getAccountNumberSuffix().equals("")) ? " "
											+ frontFrom.getAccountNumberSuffix() : ""));
				else if (frontFrom != null && frontFrom.getPortalBalance() != null)
					accountsFromMap.put(
							from.getFrontEndId(),
							StringUtils.abbreviate(frontFrom.getNickname(), 15)
									+ " "
									+ mask.mask(frontFrom.getAccountNumber())
									+ " "
									+ numberTool.format("currency", frontFrom.getPortalBalance())
									+ ((frontFrom.getAccountNumberSuffix() != null && !frontFrom
											.getAccountNumberSuffix().equals("")) ? " "
											+ frontFrom.getAccountNumberSuffix() : ""));
				else
					accountsFromMap.put(from.getFrontEndId(), StringUtils.abbreviate(frontFrom.getNickname(), 15)
							+ " "
							+ mask.mask(frontFrom.getAccountNumber())
							+ ((frontFrom.getAccountNumberSuffix() != null && !frontFrom.getAccountNumberSuffix()
									.equals("")) ? " " + frontFrom.getAccountNumberSuffix() : ""));

				accountFromReferenceMap.put(from.getFrontEndId(), from);
			}
		}
		model.put("fromaccounts", accountsFromMap);
		model.put("fromaccountsreference", accountFromReferenceMap);

		// Set up today's date
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Calendar n = calculateEffDate(Calendar.getInstance());

		model.put("todayDate", df.format(n.getTime()));
		df = new SimpleDateFormat("MM");
		model.put("month", df.format(n.getTime()));
		df = new SimpleDateFormat("dd");
		model.put("day", df.format(n.getTime()));

		List<TVFrontEndPayee> payees = userSession.getPayees(false);
		if (payees == null || payees.size() == 0) {
			model.put("availablePayees", false);
			// return;
		}

		TVFrontEndPayee selectedPayee = null;
		if (account != null && payees != null && payees.size() > 0) {
			bean.setAmount(account.getDelinquencyInfo().getAmount());
			if (account.getPayeeFrontEndId() != null)
				selectedPayee = Utils.getPayeebyId(account.getPayeeFrontEndId(), payees);
			else if (selectedPayee == null)
				selectedPayee = Utils.getPayeebyAccountNumber(account.getAccountNumber(), payees);

		}
		if (bean.getReferencePayeeId() != null && selectedPayee == null && payees != null && payees.size() > 0) {
			selectedPayee = Utils.getPayeebyId(bean.getReferencePayeeId(), payees);
		}

		if (selectedPayee != null) {
			model.put("selectedPayee", selectedPayee);
			bean.setPayeeid(selectedPayee.getFrontEndId());
			bean.setReferencePayeeId(selectedPayee.getFrontEndId());
			if (account != null)
				account.setPayeeFrontEndId(selectedPayee.getFrontEndId());
		} else {
			// We try to add the account as payee to make a payment

			boolean payeeadded = false;
			GlobalPayee global = null;
			if (account != null && account.getSubtype().equalsIgnoreCase("IDA")) {

				// REDI-EQUITY(082,083) REDICASH(084,085) HELOC(44)
				if (account.getProductDefinition().getProductId().equalsIgnoreCase("082")
						|| account.getProductDefinition().getProductId().equalsIgnoreCase("083"))
					global = PaymentsFacade.getInstance().getGlobalPayeeById(12453, userSession);
				else if (account.getProductDefinition().getProductId().equalsIgnoreCase("084")
						|| account.getProductDefinition().getProductId().equalsIgnoreCase("085"))
					global = PaymentsFacade.getInstance().getGlobalPayeeById(12682, userSession);
				else if (account.getBankId().equals("001")) {
					// banco puerto rico
					// AccesoPopular 009, Cuenta Popular 015, Multicuenta Staff
					// 023, Multicuenta reg 025
					global = PaymentsFacade.getInstance().getGlobalPayeeById(288, userSession);
				}

				// We try to add a line of credit ( Una Reserva)
				/*
				 * ID=288 Reserva BPPR ID=4144 Reserva BPNA ID=289 Reserva BVI
				 * Tortola ID=290 Reserva USVI ID=12682 Redi Cash Línea de
				 * Crédito BPPR ID=12453 Redi Equity Línea de Crédito BPPR (9
				 * dígitos) ID=14346 Redi Equity Línea de Crédito BPPR / HELOC
				 * (17 dígitos, comenzando con 101001)
				 */

			} else if (account != null && account.getSubtype().equals("CCA")) {
				// Tarjetas de credito (visa, amex, etc)
				if (account.getAccountNumber().startsWith("03"))// means amex
																// 03778
					global = PaymentsFacade.getInstance().getGlobalPayeeById(1983, userSession);
				else if (account.getAccountNumber().startsWith("4")) // visa
																		// 4549.....
					global = PaymentsFacade.getInstance().getGlobalPayeeById(359, userSession);
				else if (account.getAccountNumber().startsWith("5") && !account.getAccountNumber().startsWith("50"))// mastercard
																													// 5310
																													// 53100585
																													// 546620
					global = PaymentsFacade.getInstance().getGlobalPayeeById(223, userSession);

				/*
				 * VISA BPPR = globalPayeeId = 359 American Express BPPR
				 * (e-Bill) = 1983 Master Card BPPR (e-Bill) = 223
				 */

			} else if (account != null && account.getSubtype().equalsIgnoreCase("ILA")) {
				if (account.getAccountNumber().startsWith("106001"))
					global = PaymentsFacade.getInstance().getGlobalPayeeById(5301, userSession);
				else if (account.getAccountNumber().startsWith("107001"))
					global = PaymentsFacade.getInstance().getGlobalPayeeById(187, userSession);
				else if (account.getAccountNumber().startsWith("104001"))
					global = PaymentsFacade.getInstance().getGlobalPayeeById(189, userSession);
				else if (account.getAccountNumber().startsWith("140001"))// prestamso
																			// pop
																			// finance
					global = PaymentsFacade.getInstance().getGlobalPayeeById(63, userSession);
				else if (account.getAccountNumber().startsWith("04101001"))// prestamos
																			// pop
																			// finance
					global = PaymentsFacade.getInstance().getGlobalPayeeById(1368, userSession);
				else if (account.getAccountNumber().startsWith("101001")) {
					if (account.getProductDefinition().getProductId().equals("044")
							|| account.getProductDefinition().getProductId().equals("043"))// Cuentas
																							// HELOC
						global = PaymentsFacade.getInstance().getGlobalPayeeById(14346, userSession);
					else
						global = PaymentsFacade.getInstance().getGlobalPayeeById(185, userSession);
				}
				/*
				 * Prestamos Personales ID=5301 Préstamo BPNA (Comenzando con
				 * 106001) ID=187 Préstamo BVI (Comenzando con 107001) ID=189
				 * Préstamo USVI (Comenzando con 104001) ID=185 Préstamo BPPR
				 * (Comenzando con 101001)
				 * 
				 * Prestamos pop finance - no se donde van ID=63 Préstamos
				 * (Comenzando con 140001)< ID=1368 Préstamos con Garantía
				 * Hipoptecaria (Comenzando con 04101001)
				 * 
				 * HELOC (17 dígitos, comenzando con 101001) HELOC(product id 44
				 * o 43)
				 */
			} else if (account != null && account.getSubtype().equalsIgnoreCase("LEA")) {
				// Arrendamiento de Auto (Leasing) 279
				global = PaymentsFacade.getInstance().getGlobalPayeeById(279, userSession);

			} else if (account != null && account.getSubtype().equalsIgnoreCase("RVA")) {
				// ID=13330 Préstamos(Comenzando con 822001 ó 101001) // son de
				// pop auto
				global = PaymentsFacade.getInstance().getGlobalPayeeById(13330, userSession);

			} else if (account != null && account.getSubtype().equals("CLA")) {
				// comercial loans no idea - aqui no se muestran en MB
			}

			if (global != null) {
				boolean isHighRiskPayee = false;
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1701)) {
					PaymentsFacade.getInstance().getGlobalPayeePreference(global, userSession);

					if(global.getPreference() != null)
						 isHighRiskPayee = global.getPreference().getHighRisk();
				}
				// nulls al final relacionado a los campos de employee name,
				// employee id, routing number y employee acct# de telenomina, y
				// boolean telenomina
				payeeadded = customerService.addCustomerPayeeWithoutEbill(userSession.getCustomerProfile(),
						userSession.getBankingSession(), global, account.getAccountNumber(), global.getName(),
						Utils.generateCustomerInteractionEvent(request), null, null, null, null, false, isHighRiskPayee);
			}
			if (payeeadded) {
				customerService.logOccPayeeAdd(userSession.getCustomerProfile(),
						Utils.generateCustomerInteractionEvent(request), InteractionSeverity.INFO);
				userSession.resetPayees();
				payees = userSession.getPayees(false);// se vuelven y se buscan
														// a ver si se pudo
														// anadir el comercio

				selectedPayee = Utils.getPayeebyId(account.getPayeeFrontEndId(), payees);
				if (selectedPayee == null)
					selectedPayee = Utils.getPayeebyAccountNumber(account.getAccountNumber(), payees);

				if (selectedPayee != null) {
					model.put("selectedPayee", selectedPayee);
					bean.setPayeeid(selectedPayee.getFrontEndId());
					bean.setReferencePayeeId(selectedPayee.getFrontEndId());
					if (account != null)
						account.setPayeeFrontEndId(selectedPayee.getFrontEndId());
				} else {
					model.put("errorAddingPayee", true);
				}
			} else {
				model.put("errorAddingPayee", true);
			}

		}// final else no comercio registrado

	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		return new ModelAndView(cancelView);
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		QuickPortalPaymentBean paymentBean = (QuickPortalPaymentBean) command;

		if (page == 0) {

			if (paymentBean.getAmount() == null) {
				errors.rejectValue("amount", "", "Amount is required");
			}
			if (paymentBean.getAmount() != null) {
				BigDecimal maxpayamount = new BigDecimal(10000.00);
				if (paymentBean.getAmount().compareTo(maxpayamount) > 0) {
					errors.rejectValue("amount", "", "Exceeds max amount");
				} else if (paymentBean.getAmount().compareTo(new BigDecimal(0)) <= 0) {
					errors.rejectValue("amount", "", "Invalid");
				}
			}
			if ("none".equalsIgnoreCase(paymentBean.getAccountFrom())) {
				errors.rejectValue("accountFrom", "", "Account From is required");
			}
			if ("none".equalsIgnoreCase(paymentBean.getPayeeid())) {
				errors.rejectValue("payeeid", "", "Payee is required");
			}
			paymentBean.setFrequency(Frequency.MANUALLY);

			Date effectiveDate = paymentBean.getEffectiveDate();
			Calendar today = Calendar.getInstance();

			// Set the max date (one year in future)
			Calendar thresholdDate = Calendar.getInstance();
			thresholdDate.add(Calendar.YEAR, 1);

			if (effectiveDate != null) {
				// Validate no future dates
				Calendar n = Calendar.getInstance();
				n.setTime(effectiveDate);

				if ((n.get(Calendar.YEAR) < today.get(Calendar.YEAR))
						|| n.get(Calendar.YEAR) == today.get(Calendar.YEAR)
						&& n.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR)) {
					errors.rejectValue("effectiveDate", "", "Day in the past");

				} else if (n.after(thresholdDate)) {
					errors.rejectValue("effectiveDate", "", "Date more than one year in future");

				}

			} else {
				errors.rejectValue("effectiveDate", "", "Date is required");
			}

			if (!"none".equalsIgnoreCase(paymentBean.getAccountFrom())
					&& !"none".equalsIgnoreCase(paymentBean.getPayeeid())) {
				TVFrontendAccount acc = Utils.getAccountById(paymentBean.getAccountFrom(), userSession.getAccounts());

				if (acc != null) {
					BigDecimal balance = new BigDecimal(0.00);
					if (acc.getSubtype().equals("CCA") && acc.getAvailableBalance() != null) {
						balance = acc.getAvailableBalance();
						if (balance.compareTo(paymentBean.getAmount()) < 0) {
							overpayCount++;
							logger.info("QP3 OverpayCount: " + overpayCount);
							if (overpayCount == 1 && balance.compareTo(paymentBean.getAmount()) < 0) {
								logger.info("QP3 Rejection Balance: " + balance + "  Payment Amount: "
										+ paymentBean.getAmount());
								errors.rejectValue("amount", "payments.amt.overpay", "Invalid");
							}
						}
					} else {
						balance = acc.getPortalBalance();

						if (balance != null) {
							if (balance.compareTo(paymentBean.getAmount()) < 0) {
								overpayCount++;
								logger.info("QP4 OverpayCount: " + overpayCount);
								if (overpayCount == 1 && balance.compareTo(paymentBean.getAmount()) < 0) {
									logger.info("QP4 Rejection Balance: " + balance + "  Payment Amount: "
											+ paymentBean.getAmount());
									errors.rejectValue("amount", "payments.amt.overpay", "Invalid");
								}
							}
						}
					}
				}
			}
		}
	}

	public Calendar calculateEffDate(Calendar cal) {
		List<Holiday> holidays = customerService.getHolidays();
		BankingDaysUtils bankingDaysUtils = new BankingDaysUtils(holidays);
		return bankingDaysUtils.calculatePayDaysWithCutoff(cal, 1, 17, 0);
	}

	public String getCancelView() {
		return cancelView;
	}

	public void setCancelView(String cancelView) {
		this.cancelView = cancelView;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	/*private boolean isEbppAvailable() {
		boolean ebppStatus = false;

		if (this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
				.equals(EntitlementStatus.ENABLED.toString())) {
			ebppStatus = true;
		} else {
			ebppStatus = false;
		}

		return ebppStatus;
	}*/

	public String getBalanceDelay() {
		return balanceDelay;
	}

	public void setBalanceDelay(String balanceDelay) {
		this.balanceDelay = balanceDelay;
	}
}
