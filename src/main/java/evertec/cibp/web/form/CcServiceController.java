package evertec.cibp.web.form;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.cc.portal.QueueInsurance;
import evertec.cibp.core.models.cc.portal.QueuePin;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.CryptoUtils;
import evertec.cibp.web.form.bean.CCPlasticsCustInfo;
import evertec.cibp.web.form.bean.CCPortalServices;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.prophit.service.DefaultProphITService;
import evertec.prophit.service.bean.AddAdditionalCardBean;
import evertec.prophit.service.bean.AddAdditionalCardResponse;
import evertec.prophit.service.bean.CardActivationResponse;
import evertec.prophit.service.bean.CustomerInfoBean;
import evertec.prophit.service.bean.CustomerInquiryBean;
import evertec.prophit.service.bean.LostStolenResponse;
import evertec.prophit.service.bean.PinRequestBean;
import evertec.prophit.service.bean.PinRequestResponse;
import evertec.prophit.service.bean.RequestCardBean;
import evertec.prophit.service.bean.RequestCardResponse;
import evertec.prophit.service.bean.RetrieveCorpAcctBean;

public class CcServiceController extends SimpleFormController implements MessageSourceAware {
	/** Logger */
	protected final Log log = LogFactory.getLog(getClass());
	private MessageSource messageSource;
	private String ccInsuExcluded;

	private static final String INQUIRYNOTSET = "ProphIT Customer Inquiry has not been set in the user session";
	private static final String ENABLED = "ENABLED";
	private static final String VBVBR = "VBVBR";
	private static final String SCREEN = "screen";
	private static final String NICKNAME = "nickname";
	private static final String INSURANCE = "insurance";
	private static final String ERROR = "error";
	private static final String SUCCESS = "SUCCESS";
	private static final String REPLACEMENT = "replacement";
	private static final String MSGSTATUS = "MSGSTATUS";
	private static final String ADDADDITIONALPROCESS = "ADDADDITIONALPROCESS";
	private static final String PLASTICQTY = "plasticQty";
	private static final String CARDNUMBER = "cardNumber";
	private static final String ADDITIONAL = "additional";
	private static final String CREDITCARDTAB = "credit.card.portal.tab.cc.services.pin.invalid";
	private static final String INVALIDPIN = "Invalid PIN";
	private static final String REWARDS = "rewards";
	private static final String MINIMUM_AMOUNT = "minimumAmount";

	public CcServiceController() {
		super();
		setCommandClass(CCPortalServices.class);
		setCommandName("ccServices");
		setFormView("ccServices");
	}

	private List<CCPlasticsCustInfo> getCorpAcctNumbers(List<String> strCorpAcct, String acctNumber,
			UserSession userSession, HttpServletRequest request) {

		CryptoUtils crypto = new CryptoUtils();
		List<CCPlasticsCustInfo> listaRetorno = new LinkedList<>();
		LinkedList<CustomerInfoBean> listInfoBean = new LinkedList<>();

		for (String acct : strCorpAcct) {
			if (!acct.equals(acctNumber)) {
				// se debe hacer customer inquiry para cada tarjeta
				DefaultProphITService prophit = (DefaultProphITService) userSession.getCustomerService()
						.prophItService();
				CustomerInquiryBean qiBean;

				try {
					qiBean = prophit.makeCustomerInquiry(acct);
					CCPlasticsCustInfo plasticCustInfo = new CCPlasticsCustInfo();
					List<CustomerInfoBean> infobean = qiBean.getCustomerInfoGroupAsList();

					for (CustomerInfoBean customerInfoBean : infobean) {
						listInfoBean.add(customerInfoBean);
						String cardEncrypted = crypto.encrypt(customerInfoBean.getPlasticNumber(),
								userSession.getCryptoKey());

						plasticCustInfo.setCrvFlag(customerInfoBean.getCrvFlag());
						plasticCustInfo.setCustName(customerInfoBean.getCustomerName());
						plasticCustInfo.setCustType(customerInfoBean.getCustType());
						plasticCustInfo.setLast4PlasticNumber("x" + customerInfoBean.getPlasticNumber().substring(
								customerInfoBean.getPlasticNumber().length() - 4,
								customerInfoBean.getPlasticNumber().length()));
						plasticCustInfo.setPlasticNum(cardEncrypted);
						plasticCustInfo.setToAcctNumber(customerInfoBean.getToAcctNumber());

						if (!GenericValidator.isBlankOrNull(customerInfoBean.getLastCardRequestDate())
								&& prophit.isOverMaxDaysAllowed(customerInfoBean.getLastCardRequestDate(), 7)) {
							plasticCustInfo.setOlderThanLastReqDate(true);
						} else {
							plasticCustInfo.setOlderThanLastReqDate(false);
						}
						listaRetorno.add(plasticCustInfo);
					}

				} catch (Exception e) {
					log.error(e.getMessage());
				}

			}

		}
		WebUtils.setSessionAttribute(request, "ListInfoBean", listInfoBean);
		return listaRetorno;
	}

	private List<CCPlasticsCustInfo> getCardPlasticNumbers(UserSession userSession, boolean filterCrvFlag) {

		if (userSession.getProphItCustomerInfo() == null) {
			log.error(INQUIRYNOTSET);
			return new LinkedList<>();
		}

		DefaultProphITService prophit = (DefaultProphITService) userSession.getCustomerService().prophItService();

		List<CustomerInfoBean> customerInfoLst = userSession.getProphItCustomerInfo().getCustomerInfoGroupAsList();
		CryptoUtils crypto = new CryptoUtils();

		List<CCPlasticsCustInfo> associatedCards = new LinkedList<>();
		for (Iterator<CustomerInfoBean> iterator = customerInfoLst.iterator(); iterator.hasNext();) {
			try {
				CustomerInfoBean custInfo = iterator.next();

				// if the crv flag filter is on and the custInfo.crvFlag = N
				// then it jumps to the next plastic.
				if (!filterCrvFlag || (filterCrvFlag
						&& (custInfo.getCrvFlag() == null || custInfo.getCrvFlag().equalsIgnoreCase("")))) {
					String cardEncrypted = crypto.encrypt(custInfo.getPlasticNumber(), userSession.getCryptoKey());

					CCPlasticsCustInfo plasticInfo = new CCPlasticsCustInfo();
					plasticInfo.setLast4PlasticNumber("x" + custInfo.getPlasticNumber()
							.substring(custInfo.getPlasticNumber().length() - 4, custInfo.getPlasticNumber().length()));
					plasticInfo.setCrvFlag(custInfo.getCrvFlag());
					plasticInfo.setCustName(custInfo.getCustomerName());
					plasticInfo.setCustType(custInfo.getCustType());
					plasticInfo.setToAcctNumber(custInfo.getToAcctNumber());
					plasticInfo.setPlasticNum(cardEncrypted);
					if (!GenericValidator.isBlankOrNull(custInfo.getLastCardRequestDate())
							&& prophit.isOverMaxDaysAllowed(custInfo.getLastCardRequestDate(), 7)) {
						plasticInfo.setOlderThanLastReqDate(true);
					} else {
						plasticInfo.setOlderThanLastReqDate(false);
					}
					if (this.getPinRequestStatus().equalsIgnoreCase(ENABLED)) {
						if (custInfo.getReturnCode().equalsIgnoreCase("C")
								|| custInfo.getReturnCode().equalsIgnoreCase("G")) {
							if (custInfo.getVerificationId() != null && !custInfo.getVerificationId().isEmpty()) {
								plasticInfo.setPinValidation(true);
							} else {
								plasticInfo.setPinAllow(true);
							}
						} else if (custInfo.getReturnCode().equalsIgnoreCase("F")) {
							plasticInfo.setPinEmail(true);
						} else {
							plasticInfo.setPinBranch(true);
						}
					}

					associatedCards.add(plasticInfo);
				}

			} catch (Exception e) {
				log.error("Error encrypting Credit Card Number");
				log.error(e);
			}
		}

		return associatedCards;
	}

	private String getCardPlasticNumberDecrypted(String encryptedNumber, UserSession userSession) {
		CryptoUtils crypto = new CryptoUtils();
		String num = "";

		try {
			if (userSession.getCryptoKey() == null || userSession.getCryptoKey().equals("")) {
				log.error("crypto key not set in usersession");
			} else {
				num = crypto.decrypt(encryptedNumber, userSession.getCryptoKey());
			}
		} catch (Exception e) {
			log.error("Error decrypting Credit Card Number");
			log.error(e);
		}
		return num;
	}

	private Map<String, Object> getInsuranceWorkMap(UserSession userSession) {
		Map<String, Object> workMap = new LinkedHashMap<>();

		workMap.put("Y", messageSource.getMessage("credit.card.portal.tab.cc.services.insurance.answer.yes", null,
				new Locale(userSession.getLanguage())));
		workMap.put("N", messageSource.getMessage("credit.card.portal.tab.cc.services.insurance.answer.no", null,
				new Locale(userSession.getLanguage())));

		return workMap;
	}

	private Map<String, Object> getLostStolenYesNoMap(UserSession userSession) {
		Map<String, Object> yesNoMap = new LinkedHashMap<>();

		yesNoMap.put("Y", messageSource.getMessage("credit.card.portal.tab.cc.services.lost.yes", null,
				new Locale(userSession.getLanguage())));
		yesNoMap.put("N", messageSource.getMessage("credit.card.portal.tab.cc.services.lost.no", null,
				new Locale(userSession.getLanguage())));

		return yesNoMap;
	}

	private Map<String, Object> getLostStolenTypeMap(UserSession userSession) {
		Map<String, Object> lostStolenMap = new LinkedHashMap<>();

		lostStolenMap.put("LS", messageSource.getMessage("credit.card.portal.tab.cc.services.lost.lost", null,
				new Locale(userSession.getLanguage())));
		lostStolenMap.put("ST", messageSource.getMessage("credit.card.portal.tab.cc.services.lost.stolen", null,
				new Locale(userSession.getLanguage())));

		return lostStolenMap;
	}

	@SuppressWarnings("unchecked")
	private CustomerInfoBean getPlasticCustomerInfo(String plasticNumber, UserSession userSession,
			TVFrontendAccount account, HttpServletRequest request) {
		CustomerInfoBean custInfo = new CustomerInfoBean();
		List<CustomerInfoBean> customerInfoLst;

		if (VBVBR.equals(account.getProductDefinition().getProductId())) {
			customerInfoLst = (List<CustomerInfoBean>) WebUtils.getSessionAttribute(request, "ListInfoBean");
		} else {
			customerInfoLst = userSession.getProphItCustomerInfo().getCustomerInfoGroupAsList();
		}

		for (Iterator<CustomerInfoBean> iterator = customerInfoLst.iterator(); iterator.hasNext();) {
			CustomerInfoBean info = iterator.next();
			if (plasticNumber.indexOf(info.getPlasticNumber()) >= 0) {
				custInfo = info;
				break;
			}
		}

		return custInfo;
	}

	private Map<String, Object> getInsuranceCountryMap(UserSession userSession) {
		Map<String, Object> workMap = new LinkedHashMap<>();

		workMap.put("PR", messageSource.getMessage("credit.card.portal.tab.cc.services.insurance.country.pr", null,
				new Locale(userSession.getLanguage())));
		//Remove for history MBSD-2958
		/*workMap.put("USVI", messageSource.getMessage("credit.card.portal.tab.cc.services.insurance.country.usvi", null,
				new Locale(userSession.getLanguage())));*/

		return workMap;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		CCPortalServices ccServices = (CCPortalServices) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		TVFrontendAccount account = Utils.getAccountById(ccServices.getId(), userSession.getAccounts());

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		DefaultProphITService prophit = (DefaultProphITService) userSession.getCustomerService().prophItService();

		String screen = request.getParameter(SCREEN);
		String nick = request.getParameter(NICKNAME);
		String id = request.getParameter("id");
		String plasticNumEncrypted = request.getParameter("info");

		model.put("pinrequeststatus", this.getPinRequestStatus());

		if (screen != null)
			ccServices.setScreen(screen);

		if (nick != null)
			ccServices.setNickname(nick);

		if (id != null && !id.equalsIgnoreCase(""))
			ccServices.setId(id);

		if ((plasticNumEncrypted == null || plasticNumEncrypted.equalsIgnoreCase(""))
				&& !screen.equalsIgnoreCase(INSURANCE)) {
			ccServices.setLostStolenAnswerMap(this.getLostStolenYesNoMap(userSession));
			ccServices.setLostStolenTypeMap(this.getLostStolenTypeMap(userSession));
			ccServices.setAccountPlasticNumbers(this.getCardPlasticNumbers(userSession, false));

			log.error("PlasticNumber Received is empty");
			return showForm(request, errors, this.getFormView(), model);
		}

		if (screen != null && screen.equalsIgnoreCase(INSURANCE)) {
			model.put("INSUPROCESS", true);

			if (ccServices.getInsuranceSelfEmployeed().equalsIgnoreCase("Y")
					|| ccServices.getInsuranceWorkTime().equalsIgnoreCase("Y")
					|| ccServices.getInsuranceAge().equalsIgnoreCase("N")
					|| ccServices.getInsuranceEmployer().equalsIgnoreCase("N")) {
				model.put("QUALIFY", "N");
			} else {
				QueueInsurance insuranceInfo = new QueueInsurance();
				insuranceInfo.setProfileId(userSession.getCustomerProfile().getProfileId());
				insuranceInfo.setCcAccount(account.getAccountNumber());
				insuranceInfo.setStatus("P");
				// MBSD-2229
				insuranceInfo.setRegion(ccServices.getInsuranceCountry());
				customerService.insertCcQueueInsurance(insuranceInfo);
				model.put("QUALIFY", "Y");
			}
		} else if (screen.equalsIgnoreCase("pin")) {
			if (this.getPinRequestStatus().equals(ENABLED)) {
				ccServices.setPage(Integer.valueOf(2));
				String cardNumber = this.getCardPlasticNumberDecrypted(plasticNumEncrypted, userSession);
				CustomerInfoBean plasticInfo = this.getPlasticCustomerInfo(cardNumber, userSession, account, request);

				PinRequestBean pinRequest = new PinRequestBean(userSession.getProphItCustomerInfo().getAccountNumber(),
						userSession.getProphItCustomerInfo().getClientProductCode(), plasticInfo.getCustId(),
						ccServices.getPin(), plasticInfo.getReturnCode(),
						userSession.getProphItCustomerInfo().getOrgLevel(),
						userSession.getProphItCustomerInfo().getOrgId(),
						userSession.getProphItCustomerInfo().getCaseNumber(),
						userSession.getProphItCustomerInfo().getNewContact(),
						userSession.getProphItCustomerInfo().getKeepCaseOpen(),
						userSession.getProphItCustomerInfo().getChannelId(),
						userSession.getProphItCustomerInfo().getImpersonateId());

				PinRequestResponse pinResponse = customerService.prophItPinRequest(pinRequest);

				if (pinResponse != null && pinResponse.getStatus().equals("Processed")) {
					model.put(SUCCESS, true);
				} else {
					model.put(SUCCESS, false);
				}
			} else {
				model.put("PINPROCESS", true);

				String cardNumber = this.getCardPlasticNumberDecrypted(plasticNumEncrypted, userSession);
				if (cardNumber != null && !cardNumber.equalsIgnoreCase("")) {
					ccServices.setPlasticLast4Digits(
							"x" + cardNumber.substring(cardNumber.length() - 4, cardNumber.length()));

					QueuePin pinInfo = new QueuePin();
					pinInfo.setProfileId(userSession.getCustomerProfile().getProfileId());
					pinInfo.setCcAccount(cardNumber);
					pinInfo.setStatus("P");

					customerService.insertCcQueuePin(pinInfo);
					model.put(SUCCESS, true);
				} else
					model.put(SUCCESS, false);
			}
		} else if (screen.equalsIgnoreCase("activate")) {
			model.put("ACTIVATEPROCESS", true);

			String cardNumber = this.getCardPlasticNumberDecrypted(plasticNumEncrypted, userSession);
			if (cardNumber != null && !cardNumber.equalsIgnoreCase("")) {
				if (userSession.getProphItCustomerInfo() != null) {

					String lastCardReqDate = userSession.getProphItCustomerInfo()
							.getCustomerInfoByCardNumber(cardNumber).getLastCardRequestDate();
					if (!GenericValidator.isBlankOrNull(lastCardReqDate)
							&& !prophit.isOverMaxDaysAllowed(lastCardReqDate, 7)) {
						model.put("lastCardReq", true);
						errors.rejectValue(ERROR, "credit.card.portal.tab.cc.services.activate.error.lastcardrequest",
								new Object[] { lastCardReqDate }, "");
						model.put(SUCCESS, false);
					}

					else {
						List<String> plasticNumbers = new LinkedList<>();
						plasticNumbers.add(cardNumber);

						String custName = userSession.getCustomerProfile().getFullName();
						if (userSession.getCustomerProfile().getFullName().length() > 25)
							custName = userSession.getCustomerProfile().getFullName().substring(0, 25);

						List<CardActivationResponse> actResponse = customerService.prophItCardActivation(plasticNumbers,
								custName, userSession.getProphItCustomerInfo());

						if (actResponse != null) {
							model.put(SUCCESS, true);
							userSession.setProphItCustomerInfo(account.getAccountNumber());
							userSession.setPlastics(this.getCardPlasticNumbers(userSession, false));
						} else {
							model.put(SUCCESS, false);
						}
					}
				} else {
					log.error(INQUIRYNOTSET);
					model.put(SUCCESS, false);
				}
			} else
				model.put(SUCCESS, false);

		} else if (screen.equalsIgnoreCase("lost")) {
			model.put("LOSTPROCESS", true);
			String cardNumber = this.getCardPlasticNumberDecrypted(plasticNumEncrypted, userSession);
			if (cardNumber != null && !cardNumber.equalsIgnoreCase("")) {

				if (userSession.getProphItCustomerInfo() != null) {
					CustomerInfoBean plasticInfo = getPlasticCustomerInfo(cardNumber, userSession, account, request);

					ccServices.getLostStolenInfo().setCustInq(userSession.getProphItCustomerInfo());
					ccServices.getLostStolenInfo().setPlasticNumber(cardNumber);
					ccServices.getLostStolenInfo().setCustId(plasticInfo.getCustId());
					ccServices.getLostStolenInfo().setCustType(plasticInfo.getCustType());
					ccServices.getLostStolenInfo().setCardMissing("Y");

					if (ccServices.getLostStolenInfo().getPoliceCaseNumber() == null)
						ccServices.getLostStolenInfo().setPoliceCaseNumber("NoCaseNum");

					if (ccServices.getLostStolenInfo().getIncidentLocation() == null
							|| ccServices.getLostStolenInfo().getIncidentLocation().trim().equals(""))
						ccServices.getLostStolenInfo().setIncidentLocation("No recuerda");

					ccServices.getLostStolenInfo()
							.setIncidentDate(ccServices.getLostStolenInfo().getIncidentDate().substring(4, 8) + "-"
									+ ccServices.getLostStolenInfo().getIncidentDate().substring(0, 2) + "-"
									+ ccServices.getLostStolenInfo().getIncidentDate().substring(2, 4));
					ccServices.getLostStolenInfo()
							.setLastUsedDate(ccServices.getLostStolenInfo().getLastUsedDate().substring(4, 8) + "-"
									+ ccServices.getLostStolenInfo().getLastUsedDate().substring(0, 2) + "-"
									+ ccServices.getLostStolenInfo().getLastUsedDate().substring(2, 4));
					ccServices.getLostStolenInfo().setReportedBy(userSession.getCustomerProfile().getFullName());

					LostStolenResponse lostResponse = customerService
							.prophItLostStolenNotification(ccServices.getLostStolenInfo());

					if (lostResponse == null) {
						log.error("ProphIT Lost Response is null.");

					} else {
						model.put(MSGSTATUS, lostResponse.getMsgStatus());
						model.put(SUCCESS, true);
					}
				} else {
					log.error(INQUIRYNOTSET);
					model.put(SUCCESS, false);
				}
			} else
				model.put(SUCCESS, false);
		} else if (screen.equalsIgnoreCase(REPLACEMENT)) {

			String cardNumber = this.getCardPlasticNumberDecrypted(plasticNumEncrypted, userSession);
			cardNumber = this.getPlasticCustomerInfo(cardNumber, userSession, account, request).getPlasticNumber();
			String lastCardReqDate = userSession.getProphItCustomerInfo().getCustomerInfoByCardNumber(cardNumber)
					.getLastCardRequestDate();
			if (!GenericValidator.isBlankOrNull(lastCardReqDate)
					&& !prophit.isOverMaxDaysAllowed(lastCardReqDate, 180)) {
				model.put("lastCardReq", true);
				errors.rejectValue(ERROR, "credit.card.portal.tab.cc.services.replacement.error.lastrequestdate",
						new Object[] { lastCardReqDate }, "");
			} else if (cardNumber != null && !cardNumber.equalsIgnoreCase("")) {

				if (userSession.getProphItCustomerInfo() != null) {

					CustomerInfoBean plasticInfo = getPlasticCustomerInfo(cardNumber, userSession, account, request);

					ccServices.getRequestCardInfo().setCustomerInfoBean(userSession.getProphItCustomerInfo());
					ccServices.getRequestCardInfo().setCardholderName(plasticInfo.getCustomerName());
					ccServices.getRequestCardInfo().setBlueBarAccountStatus(plasticInfo.getReqStatus());
					ccServices.getRequestCardInfo().getCustomerInfoBean().setOrgLevel(RequestCardBean.ORG_LEVEL);
					ccServices.getRequestCardInfo().getCustomerInfoBean().setCustInfoGroup1(plasticInfo.getCustId());
					ccServices.getRequestCardInfo().setCardNumber(cardNumber);
					RequestCardResponse requestCardResponse = customerService
							.prophiItRequestCard(ccServices.getRequestCardInfo());

					if (requestCardResponse == null) {
						log.error("ProphIT Request Card is null.");
						model.put(SUCCESS, false);
						model.put("REPLACEMENTPROCESS", true);
					} else {
						model.put(MSGSTATUS, requestCardResponse.getMessageID());
						model.put(SUCCESS, true);
						model.put("REPLACEMENTPROCESS", true);
						errors.rejectValue(ERROR, "credit.card.portal.tab.cc.services.replacement.success",
								new Object[] { "x" + cardNumber.substring(cardNumber.length() - 4) }, "");
					}
				} else {
					log.error(INQUIRYNOTSET);
					model.put(SUCCESS, false);
				}
			} else
				model.put(SUCCESS, false);
			// /cibp-2277 dsalas
		} else if (screen.equalsIgnoreCase(ADDITIONAL)) {

			if (userSession.getProphItCustomerInfo() != null) {
				ccServices.getAddAdditionalInfo().setCustomerInfo(userSession.getProphItCustomerInfo());

				String tarjeta = ccServices.getAddAdditionalInfo().getCustomerInfo().getAccountNumber();
				CustomerInfoBean plasticInfo = getPlasticCustomerInfo(tarjeta, userSession, account, request);

				ccServices.getAddAdditionalInfo()
						.setEmbossName(ccServices.getAddAdditionalInfo().getEmbossName().toUpperCase());

				ccServices.getAddAdditionalInfo().getCustomerInfo().setPrimaryCardID(plasticInfo.getPrimId());
				ccServices.getAddAdditionalInfo().getCustomerInfo().setPrimaryMailerID(plasticInfo.getPrimMailerId());
				ccServices.getAddAdditionalInfo().getCustomerInfo().setOrgLevel(AddAdditionalCardBean.ORG_LEVEL);
				ccServices.getAddAdditionalInfo().setClientID(AddAdditionalCardBean.ORG_ID);
				AddAdditionalCardResponse addResponse = customerService
						.prophItAddAdditionalCard(ccServices.getAddAdditionalInfo());
				if (addResponse == null) {
					log.error("Prophit Add Additional Card Response is null");
					model.put(SUCCESS, false);
					model.put(ADDADDITIONALPROCESS, true);
				} else {
					model.put(MSGSTATUS, addResponse.getMessageText());
					model.put(SUCCESS, true);
					model.put(ADDADDITIONALPROCESS, true);
					errors.rejectValue(ERROR, "credit.card.portal.tab.cc.services.additional.success",
							new Object[] { "x" + tarjeta.substring(tarjeta.length() - 4) }, "");
				}

			} else {
				log.error(INQUIRYNOTSET);
				model.put(SUCCESS, false);
				model.put(ADDADDITIONALPROCESS, true);
			}

		}

		if (!screen.equalsIgnoreCase(INSURANCE) && !screen.equalsIgnoreCase(ADDITIONAL)
				&& !screen.equalsIgnoreCase(REPLACEMENT) && ccServices.getAccountPlasticNumbers() != null)
			model.put(PLASTICQTY, ccServices.getAccountPlasticNumbers().size());

		userSession.setProphItCustomerInfo(account.getAccountNumber());

		return showForm(request, errors, this.getFormView(), model);
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		CCPortalServices ccServices = (CCPortalServices) command;

		model.put("pinrequeststatus", this.getPinRequestStatus());

		String screen = request.getParameter(SCREEN);
		String nick = request.getParameter(NICKNAME);
		String id = request.getParameter("id");

		if (screen != null)
			ccServices.setScreen(screen);

		if (nick != null)
			ccServices.setNickname(nick);

		if (id != null && !id.equalsIgnoreCase(""))
			ccServices.setId(id);

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		DefaultProphITService prophit = (DefaultProphITService) userSession.getCustomerService().prophItService();
		// set the encryption key in the usersession so it can be used in the
		// onsubmit method.
		if (userSession.getCryptoKey() == null) {
			CryptoUtils crypto = new CryptoUtils();
			userSession.setCryptoKey(crypto.generateKey());
		}

		TVFrontendAccount account = Utils.getAccountById(ccServices.getId(), userSession.getAccounts());
		if (screen != null) {
			if (screen.equalsIgnoreCase(INSURANCE)) {
				ccServices.setInsuranceAnswerMap(this.getInsuranceWorkMap(userSession));
				ccServices.setInsuranceCountryMap(this.getInsuranceCountryMap(userSession));
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD2229)) {
					if (GenericValidator.isBlankOrNull(ccServices.getInsuranceCountry()))
						ccServices.setInsuranceCountry("PR");
					model.put("MBSD2229", true);
				}

				TVAccountProductDefinition productDef = account.getProductDefinition();
				if (ccInsuExcluded.indexOf(productDef.getProductId()) > -1) {
					model.put("ICONNOTQUALIFY", true);
				}
			} // ///cibp-2277 dsalas
			else if (screen.equalsIgnoreCase("pin") || screen.equalsIgnoreCase("activate")) {
				if (userSession.getProphItCustomerInfo() == null || !userSession.getProphItCustomerInfo()
						.getAccountNumber().equals(account.getAccountNumber())) {
					userSession.setProphItCustomerInfo(account.getAccountNumber());
					if (VBVBR.equals(account.getProductDefinition().getProductId())) {
						CustomerInquiryBean custinqbean = userSession.getProphItCustomerInfo();
						custinqbean.setAccountNumber(account.getAccountNumber());
						List<CCPlasticsCustInfo> tarjetas = this.getCorpAcctNumbers(
								customerService.prophItRetrieveCorpAcct(
										new RetrieveCorpAcctBean().setCustomerInfoBean(custinqbean)),
								account.getAccountNumber(), userSession, request);
						ccServices.setAccountPlasticNumbers(tarjetas);
					} else {
						ccServices.setAccountPlasticNumbers(this.getCardPlasticNumbers(userSession, false));
					}
					userSession.setPlastics(ccServices.getAccountPlasticNumbers());
				} else {
					ccServices.setAccountPlasticNumbers(userSession.getPlastics());
				}
				if (this.getPinRequestStatus().equals(ENABLED) && screen.equalsIgnoreCase("pin")) {
					Integer page = ccServices.getPage();
					if (page == null) {
						ccServices.setPage(Integer.valueOf(0));
					}
					String plasticNumEncrypted = (String) WebUtils.getSessionAttribute(request, CARDNUMBER);
					if (plasticNumEncrypted != null || page == Integer.valueOf(1)) {
						if (plasticNumEncrypted != null) {
							model.put("cardEncrypt", plasticNumEncrypted);
						} else {
							plasticNumEncrypted = request.getParameter("info");
							model.put("cardEncrypt", plasticNumEncrypted);
						}
						String cardNumber = this.getCardPlasticNumberDecrypted(plasticNumEncrypted, userSession);
						if (userSession.getPinRequestAttempts() != null
								&& userSession.getPinRequestAttempts().get(cardNumber) != null
								&& userSession.getPinRequestAttempts().get(cardNumber).equals(Integer.valueOf(2))) {
							userSession.getPinRequestAttempts().put(cardNumber, Integer.valueOf(0));
							ccServices.setPage(Integer.valueOf(2));
							model.put(SUCCESS, false);
							WebUtils.setSessionAttribute(request, CARDNUMBER, null);
							// SENDING BACK CODE N TO TSYS
							CustomerInfoBean plasticInfo = this.getPlasticCustomerInfo(cardNumber, userSession, account,
									request);
							PinRequestBean pinRequest = new PinRequestBean(
									userSession.getProphItCustomerInfo().getAccountNumber(),
									userSession.getProphItCustomerInfo().getClientProductCode(),
									plasticInfo.getCustId(), "", "N",
									userSession.getProphItCustomerInfo().getOrgLevel(),
									userSession.getProphItCustomerInfo().getOrgId(),
									userSession.getProphItCustomerInfo().getCaseNumber(),
									userSession.getProphItCustomerInfo().getNewContact(),
									userSession.getProphItCustomerInfo().getKeepCaseOpen(),
									userSession.getProphItCustomerInfo().getChannelId(),
									userSession.getProphItCustomerInfo().getImpersonateId());

							customerService.prophItPinRequest(pinRequest);
						} else {
							ccServices.setPage(Integer.valueOf(1));
							WebUtils.setSessionAttribute(request, CARDNUMBER, null);
							for (int i = 0; i < ccServices.getAccountPlasticNumbers().size(); i++) {
								if (ccServices.getAccountPlasticNumbers().get(i).getPlasticNum()
										.equals(plasticNumEncrypted)) {
									model.put("last4PlasticNumber",
											ccServices.getAccountPlasticNumbers().get(i).getLast4PlasticNumber());
									model.put("custName", ccServices.getAccountPlasticNumbers().get(i).getCustName());
									break;
								}
							}
						}
					} else {
						plasticNumEncrypted = request.getParameter("info");
						for (int i = 0; i < userSession.getPlastics().size(); i++) {
							if (userSession.getPlastics().get(i).getPlasticNum().equals(plasticNumEncrypted)) {
								model.put("last4PlasticNumber",
										userSession.getPlastics().get(i).getLast4PlasticNumber());
								model.put("custName", userSession.getPlastics().get(i).getCustName());
								break;
							}
						}
					}

				}
			} else if (screen.equalsIgnoreCase("lost")) {
				if (userSession.getProphItCustomerInfo() == null || !userSession.getProphItCustomerInfo()
						.getAccountNumber().equals(account.getAccountNumber())) {
					userSession.setProphItCustomerInfo(account.getAccountNumber());
					if (VBVBR.equals(account.getProductDefinition().getProductId())) {

						CustomerInquiryBean custinqbean = userSession.getProphItCustomerInfo();
						if (custinqbean != null) {
							custinqbean.setAccountNumber(account.getAccountNumber());
							List<CCPlasticsCustInfo> tarjetas = this.getCorpAcctNumbers(
									customerService.prophItRetrieveCorpAcct(
											new RetrieveCorpAcctBean().setCustomerInfoBean(custinqbean)),
									account.getAccountNumber(), userSession, request);
							ccServices.setAccountPlasticNumbers(tarjetas);

							String lastCardReqDate = custinqbean.getCustomerInfoByCardNumber(account.getAccountNumber())
									.getLastCardRequestDate();

							if (!GenericValidator.isBlankOrNull(lastCardReqDate)
									&& !prophit.isOverMaxDaysAllowed(lastCardReqDate, 7)) {
								errors.rejectValue(ERROR,
										"credit.card.portal.tab.cc.services.lost.error.lastcardrequest",
										new Object[] { lastCardReqDate }, "");
							}

						}
						ccServices.setLostStolenAnswerMap(this.getLostStolenYesNoMap(userSession));
						ccServices.setLostStolenTypeMap(this.getLostStolenTypeMap(userSession));

					} else {

						CustomerInquiryBean custinqbean = userSession.getProphItCustomerInfo();
						if (custinqbean != null) {

							String lastCardReqDate = custinqbean.getCustomerInfoByCardNumber(account.getAccountNumber())
									.getLastCardRequestDate();

							if (!GenericValidator.isBlankOrNull(lastCardReqDate)
									&& !prophit.isOverMaxDaysAllowed(lastCardReqDate, 7)) {
								errors.rejectValue(ERROR,
										"credit.card.portal.tab.cc.services.lost.error.lastcardrequest",
										new Object[] { lastCardReqDate }, "");
							}

						}
						ccServices.setLostStolenAnswerMap(this.getLostStolenYesNoMap(userSession));
						ccServices.setLostStolenTypeMap(this.getLostStolenTypeMap(userSession));

						ccServices.setAccountPlasticNumbers(this.getCardPlasticNumbers(userSession, false));
					}
					userSession.setPlastics(ccServices.getAccountPlasticNumbers());
				} else {
					ccServices.setLostStolenAnswerMap(this.getLostStolenYesNoMap(userSession));
					ccServices.setLostStolenTypeMap(this.getLostStolenTypeMap(userSession));

					ccServices.setAccountPlasticNumbers(userSession.getPlastics());
				}
			} else if (screen.equalsIgnoreCase(REPLACEMENT)) {
				if (userSession.getProphItCustomerInfo() == null || !userSession.getProphItCustomerInfo()
						.getAccountNumber().equals(account.getAccountNumber())) {
					userSession.setProphItCustomerInfo(account.getAccountNumber());
					if (VBVBR.equals(account.getProductDefinition().getProductId())) {
						CustomerInquiryBean custinqbean = userSession.getProphItCustomerInfo();
						custinqbean.setAccountNumber(account.getAccountNumber());
						List<CCPlasticsCustInfo> tarjetas = this.getCorpAcctNumbers(
								customerService.prophItRetrieveCorpAcct(
										new RetrieveCorpAcctBean().setCustomerInfoBean(custinqbean)),
								account.getAccountNumber(), userSession, request);
						ccServices.setAccountPlasticNumbers(tarjetas);
					} else {
						try {
							List<CCPlasticsCustInfo> tarjetas = this.getCardPlasticNumbers(userSession, false);
							ccServices.setAccountPlasticNumbers(tarjetas);
						} catch (NullPointerException e) {
							model.put(PLASTICQTY, 0);
						}
					}
					userSession.setPlastics(ccServices.getAccountPlasticNumbers());
				} else {
					ccServices.setAccountPlasticNumbers(userSession.getPlastics());
				}
			} else if (screen.equalsIgnoreCase(ADDITIONAL)) {
				if (userSession.getProphItCustomerInfo() == null || !userSession.getProphItCustomerInfo()
						.getAccountNumber().equals(account.getAccountNumber())) {
					userSession.setProphItCustomerInfo(account.getAccountNumber());
				}

				String cost = prophit.getAdditionalCardCost(account.getProductDefinition().getProductId());

				if (!cost.equals("0")) {
					errors.rejectValue(ERROR, "credit.card.portal.tab.cc.services.additional.cost", "");
				}

				try {
					TVAccountProductDefinition productDef = account.getProductDefinition();
					if (account.getBankId().indexOf(prophit.getBankIdCommercial()) > -1
							|| prophit.isAvantageBusinessProduct(productDef.getProductId())) {
						model.put("commercialAccount", true);
					}
					if (prophit.getIconAccounts().indexOf(productDef.getProductId()) > -1) {
						model.put("iconAccount", true);
					}

					int cantTarjetas = userSession.getProphItCustomerInfo().getCustomerInfoGroupAsList().size();
					if (cantTarjetas >= prophit.allowedCardCant(productDef.getProductId())) {
						model.put("cantTarjetasExedido", true);
					}

					cargarCalendario(request, model);
					model.put("estados", loadStates());
				} catch (NullPointerException e) {
					model.put(PLASTICQTY, 0);
				}

			} else if (screen.equalsIgnoreCase(REWARDS)) {
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)
						&& account.getTsysLoyaltyRewardsInfo() != null
						&& account.getTsysLoyaltyRewardsInfo().getCanRedeemRewards()) {
					if (account.getTsysLoyaltyRewardsInfo().getAvailableRewardsBalance() != null
							&& !account.getTsysLoyaltyRewardsInfo().getAvailableRewardsBalance().isEmpty()) {
						if (account.getTsysLoyaltyRewardsInfo()
								.getAvailableBalanceDouble() >= account.getTsysLoyaltyRewardsInfo()
										.getMinimumRewardsBalance()) {
							model.put(SUCCESS, true);
						} else {
							errors.rejectValue(ERROR,
									"tsys.loyalty.rewards.minimum.redemption.amount.error.message", "");
							model.put(SUCCESS, false);
							model.put(MINIMUM_AMOUNT, true);
						}
					} else {
						errors.rejectValue(ERROR, "tsys.loyalty.rewards.can.redeem.false.error.meassge", "");
						model.put(SUCCESS, false);
					}
				} else {
					errors.rejectValue(ERROR, "tsys.loyalty.rewards.can.redeem.false.error.meassge", "");
					model.put(SUCCESS, false);
				}
			}

			if (!screen.equalsIgnoreCase(INSURANCE) && ccServices.getAccountPlasticNumbers() != null)
				model.put(PLASTICQTY, ccServices.getAccountPlasticNumbers().size());
		}
		return model;

	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {

		CCPortalServices ccServices = (CCPortalServices) command;

		String screen = request.getParameter(SCREEN);
		String nick = request.getParameter(NICKNAME);
		String id = request.getParameter("id");
		if (screen != null)
			ccServices.setScreen(screen);

		if (nick != null)
			ccServices.setNickname(nick);

		if (id != null && !id.equalsIgnoreCase(""))
			ccServices.setId(id);

		if (screen != null && screen.equalsIgnoreCase(INSURANCE)
				&& (ccServices.getInsuranceWorkTime() == null || ccServices.getInsuranceSelfEmployeed() == null
						|| ccServices.getInsuranceAge() == null || ccServices.getInsuranceEmployer() == null
						|| id == null || ccServices.getInsuranceWorkTime().trim().equals("")
						|| ccServices.getInsuranceSelfEmployeed().trim().equals("")
						|| ccServices.getInsuranceAge().trim().equals("")
						|| ccServices.getInsuranceEmployer().trim().equals("") || id.trim().equals(""))) {
			errors.rejectValue(ERROR, "credit.card.portal.tab.cc.services.fields.required", "All Fields Are Required");
		}
		if (screen != null && screen.equalsIgnoreCase("lost")) {

			String plasticNumEncrypted = request.getParameter("info");
			if (ccServices.getLostStolenInfo().getLastUsedDate() == null
					&& (ccServices.getLostStolenInfo().getIncidentDate() == null || id == null
					|| ccServices.getLostStolenInfo().getLastUsedDate().trim().equals("")
					|| ccServices.getLostStolenInfo().getIncidentDate().trim().equals("") || plasticNumEncrypted == null
					|| plasticNumEncrypted.trim().equalsIgnoreCase("") || id.trim().equals("")))
				errors.rejectValue(ERROR, "credit.card.portal.tab.cc.services.fields.required",
						"All Fields Are Required");
		}
		// cibp-2277 dsalas
		if (screen != null && screen.equalsIgnoreCase(ADDITIONAL)) {
			if (GenericValidator.isBlankOrNull(ccServices.getAddAdditionalInfo().getFirstName())) {
				errors.rejectValue("addAdditionalInfo.firstName",
						"credit.card.portal.tab.cc.services.fields.additional.firstname.required",
						"First name is required");
			}
			if (GenericValidator.isBlankOrNull(ccServices.getAddAdditionalInfo().getLastName())) {
				errors.rejectValue("addAdditionalInfo.lastName",
						"credit.card.portal.tab.cc.services.fields.additional.lastname.required",
						"Last name is required");
			}
			if (containsSpecialCharacters(ccServices.getAddAdditionalInfo().getFirstName())) {
				errors.rejectValue("addAdditionalInfo.firstName",
						"credit.card.portal.tab.cc.services.fields.additional.firstname.invalid",
						"First name is invalid");
			}
			if (containsSpecialCharacters(ccServices.getAddAdditionalInfo().getLastName())) {
				errors.rejectValue("addAdditionalInfo.lastName",
						"credit.card.portal.tab.cc.services.fields.additional.lastname.invalid",
						"Last name is invalid");
			}
			if (containsSpecialCharacters(ccServices.getAddAdditionalInfo().getMi())) {
				errors.rejectValue("addAdditionalInfo.mi",
						"credit.card.portal.tab.cc.services.fields.additional.mi.invalid",
						"Middle initial name is invalid");
			}
			if (ccServices.getAddAdditionalInfo().getDayBirth().equals("0")
					|| ccServices.getAddAdditionalInfo().getMonthBirth().equals("")
					|| ccServices.getAddAdditionalInfo().getYearBirth().equals("0")) {
				if (ccServices.getAddAdditionalInfo().getDayBirth().equals("0")) {
					errors.rejectValue("addAdditionalInfo.yearBirth",
							"credit.card.portal.tab.cc.services.fields.additional.birthdate.invalid",
							"Birth date is invalid");
				} else if (ccServices.getAddAdditionalInfo().getMonthBirth().equals("")) {
					errors.rejectValue("addAdditionalInfo.yearBirth",
							"credit.card.portal.tab.cc.services.fields.additional.birthdate.invalid",
							"Birth date is invalid");
				} else if (ccServices.getAddAdditionalInfo().getYearBirth().equals("0")) {
					errors.rejectValue("addAdditionalInfo.yearBirth",
							"credit.card.portal.tab.cc.services.fields.additional.birthdate.invalid",
							"Birth date is invalid");
				}
			} else {
				// additional user must have 18+ years old
				Integer bYear = Integer.valueOf(ccServices.getAddAdditionalInfo().getYearBirth());
				Integer bMonth = Integer.valueOf(ccServices.getAddAdditionalInfo().getMonthBirth()) - 1;
				Integer bDay = Integer.valueOf(ccServices.getAddAdditionalInfo().getDayBirth());
				Calendar cal = Calendar.getInstance();
				if (bYear.equals(cal.get(Calendar.YEAR) - 18)) {
					if (bMonth.equals(cal.get(Calendar.MONTH)) && bDay > cal.get(Calendar.DAY_OF_MONTH)) {
						errors.rejectValue("addAdditionalInfo.yearBirth",
								"credit.card.portal.tab.cc.services.fields.additional.birthdate.invalid",
								"Birth date is invalid");
					} else if (bMonth > cal.get(Calendar.MONTH)) {
						errors.rejectValue("addAdditionalInfo.yearBirth",
								"credit.card.portal.tab.cc.services.fields.additional.birthdate.invalid",
								"Birth date is invalid");
					}

				}
			}
			if (GenericValidator.isBlankOrNull(ccServices.getAddAdditionalInfo().getPhnNbr())) {
				errors.rejectValue("addAdditionalInfo.phnNbr",
						"credit.card.portal.tab.cc.services.fields.additional.phone.required",
						"Phone number is required");
			}
			if (containsSpecialCharacters(ccServices.getAddAdditionalInfo().getPhnNbr())) {
				errors.rejectValue("addAdditionalInfo.phnNbr",
						"credit.card.portal.tab.cc.services.fields.additional.field.invalid", "Invalid value");
			}
			if (!GenericValidator.isBlankOrNull(ccServices.getAddAdditionalInfo().getDriversLicenseNbr())) {
				if (!GenericValidator.maxLength(ccServices.getAddAdditionalInfo().getDriversLicenseNbr(), 15)
						|| !GenericValidator.isLong(ccServices.getAddAdditionalInfo().getDriversLicenseNbr())) {
					errors.rejectValue("addAdditionalInfo.driversLicenseNbr",
							"credit.card.portal.tab.cc.services.fields.additional.field.invalid", "Invalid value");
				}
			}
			if (!GenericValidator.isBlankOrNull(ccServices.getAddAdditionalInfo().getSsn())) {
				if (!GenericValidator.isInt(ccServices.getAddAdditionalInfo().getSsn())) {
					errors.rejectValue("addAdditionalInfo.ssn",
							"credit.card.portal.tab.cc.services.fields.additional.field.invalid", "Invalid value");
				}
				if (ccServices.getAddAdditionalInfo().getSsn().length() < 9) {
					errors.rejectValue("addAdditionalInfo.ssn",
							"credit.card.portal.tab.cc.services.fields.additional.field.ssn.length",
							"Must be 9 digits");
				}
			}
			if (GenericValidator.isBlankOrNull(ccServices.getAddAdditionalInfo().getEmbossName())) {
				errors.rejectValue("addAdditionalInfo.embossName",
						"credit.card.portal.tab.cc.services.fields.additional.field.required",
						"This field is required");
			}
			if (containsSpecialCharacters(ccServices.getAddAdditionalInfo().getEmbossName())) {
				errors.rejectValue("addAdditionalInfo.embossName",
						"credit.card.portal.tab.cc.services.fields.additional.field.invalid", "Invalid value");
			}

		}
		if (screen != null && screen.equalsIgnoreCase(REPLACEMENT)) {
			String plasticNumEncrypted = request.getParameter("info");
			if (GenericValidator.isBlankOrNull(plasticNumEncrypted)) {
				errors.rejectValue(ERROR, "credit.card.portal.tab.cc.services.fields.additional.card.notselected",
						"Debes seleccionar una tarjeta");
			}
		}
		if (this.getPinRequestStatus().equals(ENABLED)) {
			if (screen.equalsIgnoreCase("pin")) {
				if (ccServices.getPage().equals(Integer.valueOf(1))) {
					boolean error = false;
					if (ccServices.getPin() == null || ccServices.getPin().trim().equals("")
							|| ccServices.getPin().length() != 4) {
						errors.rejectValue("pin", CREDITCARDTAB, INVALIDPIN);
						error = true;
					} else if (ccServices.getConfirmPin() == null || ccServices.getConfirmPin().trim().equals("")
							|| ccServices.getConfirmPin().length() != 4) {
						errors.rejectValue("confirmPin", CREDITCARDTAB, INVALIDPIN);
						error = true;
					}
					if (!error) {
						if (!ccServices.getPin().equals(ccServices.getConfirmPin())) {
							errors.rejectValue("confirmPin", "credit.card.portal.tab.cc.services.pin.nomatch",
									"PIN and confirmation do not match");
						} else {
							if (!ccServices.isPinNumber()) {
								errors.rejectValue("pin", CREDITCARDTAB, INVALIDPIN);
							}
						}
					}
				}
				if (ccServices.getPage().equals(Integer.valueOf(0))) {
					ccServices.setPage(Integer.valueOf(1));
					errors.rejectValue(ERROR, "", "");
				}
			}
		}
	}

	/**
	 * @param messageSource
	 *            the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param ccInsuExcluded
	 *            the ccInsuExcluded to set
	 */
	public void setCcInsuExcluded(String ccInsuExcluded) {
		this.ccInsuExcluded = ccInsuExcluded;
	}

	/**
	 * @return the ccInsuExcluded
	 */
	public String getCcInsuExcluded() {
		return ccInsuExcluded;
	}

	private void cargarCalendario(HttpServletRequest request, Map<String, Object> model) {
		// months
		Map<Object, String> months = new LinkedHashMap<>();
		SimpleDateFormat df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.MONTH, 0);
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			months.put("", "Mes");
		else
			months.put("", "Month");

		for (int i = 0; i <= 11; i++) {
			String month = df.format(cal.getTime());
			months.put(i + 1, month);
			cal.add(Calendar.MONTH, 1);
		}
		model.put("months", months);

		// days
		Map<Object, String> days = new LinkedHashMap<>();
		df = new SimpleDateFormat("d");
		cal.set(Calendar.DAY_OF_MONTH, 1);
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			days.put(0, "D&iacute;a");
		else
			days.put(0, "Day");

		for (int i = 1; i <= 31; i++) {
			String day = df.format(cal.getTime());
			days.put(i, day);
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		model.put("days", days);

		// years
		Map<Object, String> years = new LinkedHashMap<>();
		df = new SimpleDateFormat("yyyy");
		cal = GregorianCalendar.getInstance();
		cal.add(Calendar.YEAR, -18);
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			years.put(0, "A&ntilde;o");
		else
			years.put(0, "Year");

		for (int i = 0; i <= 100; i++) {
			String year = df.format(cal.getTime());
			years.put(Integer.parseInt(year), year);
			cal.add(Calendar.YEAR, -1);
		}
		model.put("years", years);

	}

	private Map<String, String> loadStates() {
		Map<String, String> estados = new LinkedHashMap<>();

		estados.put("PR", "PR	 - 	PUERTO RICO");
		estados.put("AL", "AL	 - 	ALABAMA");
		estados.put("AK", "AK	 - 	ALASKA");
		estados.put("AZ", "AZ	 - 	ARIZONA");
		estados.put("AR", "AR	 - 	ARKANSAS");
		estados.put("CA", "CA	 - 	CALIFORNIA");
		estados.put("CO", "CO	 - 	COLORADO");
		estados.put("CT", "CT	 - 	CONNECTICUT");
		estados.put("DE", "DE	 - 	DELAWARE");
		estados.put("DC", "DC	 - 	DISTRICT OF COLUMBIA");
		estados.put("FL", "FL	 - 	FLORIDA");
		estados.put("GA", "GA	 - 	GEORGIA");
		estados.put("HI", "HI	 - 	HAWAII");
		estados.put("ID", "ID	 - 	IDAHO");
		estados.put("IL", "IL	 - 	ILLINOIS");
		estados.put("IN", "IN	 - 	INDIANA");
		estados.put("IA", "IA	 - 	IOWA");
		estados.put("KS", "KS	 - 	KANSAS");
		estados.put("KY", "KY	 - 	KENTUCKY");
		estados.put("LA", "LA	 - 	LOUISIANA");
		estados.put("ME", "ME	 - 	MAINE");
		estados.put("MD", "MD	 - 	MARYLAND");
		estados.put("MA", "MA	 - 	MASSACHUSETTS");
		estados.put("MI", "MI	 - 	MICHIGAN");
		estados.put("MN", "MN	 - 	MINNESOTA");
		estados.put("MS", "MS	 - 	MISSISSIPPI");
		estados.put("MO", "MO	 - 	MISSOURI");
		estados.put("MT", "MT	 - 	MONTANA");
		estados.put("SD", "SD	 - 	SOUTH DAKOTA");
		estados.put("TN", "TN	 - 	TENNESSEE");
		estados.put("TX", "TX	 - 	TEXAS");
		estados.put("UT", "UT	 - 	UTAH");
		estados.put("VT", "VT	 - 	VERMONT");
		estados.put("VA", "VA	 - 	VIRGINIA");
		estados.put("WA", "WA	 - 	WASHINGTON");
		estados.put("WV", "WV	 - 	WEST VIRGINIA");
		estados.put("WI", "WI	 - 	WISCONSIN");
		estados.put("WY", "WY	 - 	WYOMING");
		estados.put("AA", "AA	 - 	APO AMERICAS");
		estados.put("AE", "AE	 - 	APO EAST");
		estados.put("AP", "AP	 - 	APO PACIFIC");
		estados.put("AS", "AS	 - 	AMERICAN SAMOA");
		estados.put("GU", "GU	 - 	GUAM");
		estados.put("VI", "VI	 - 	U.S. VIRGIN ISLANDS");
		estados.put("AB", "AB	 - 	ALBERTA");
		estados.put("BC", "BC	 - 	BRITISH COLUMBIA");
		estados.put("MB", "MB	 - 	MANITOBA");
		estados.put("NT", "NT	 - 	NORTH WEST TERRITORIES");
		estados.put("NS", "NS	 - 	NOVA SCOTIA");
		estados.put("NB", "NB	 - 	NEW BRUNSWICK");
		estados.put("NF", "NF	 - 	NEWFOUNDLAND");
		estados.put("NL", "NL	 - 	NEWFOUNDLAND AND LABRADOR");
		estados.put("NU", "NU	 - 	NINUVAT");
		estados.put("ON", "ON	 - 	ONTARIO");
		estados.put("NE", "NE	 - 	NEBRASKA");
		estados.put("NV", "NV	 - 	NEVADA");
		estados.put("NH", "NH	 - 	NEW HAMPSHIRE");
		estados.put("NJ", "NJ	 - 	NEW JERSEY");
		estados.put("NM", "NM	 - 	NEW MEXICO");
		estados.put("NY", "NY	 - 	NEW YORK");
		estados.put("NC", "NC	 - 	NORTH CAROLINA");
		estados.put("ND", "ND	 - 	NORTH DAKOTA");
		estados.put("OH", "OH	 - 	OHIO");
		estados.put("OK", "OK	 - 	OKLAHOMA");
		estados.put("OR", "OR	 - 	OREGON");
		estados.put("PA", "PA	 - 	PENNSYLVANIA");
		estados.put("RI", "RI	 - 	RHODE ISLAND");
		estados.put("SC", "SC	 - 	SOUTH CAROLINA");
		estados.put("PE", "PE	 - 	PRINCE EDWARD");
		estados.put("QC", "QC	 - 	QUEBEC");
		estados.put("SK", "SK	 - 	SASKATCHEWAN");
		estados.put("YT", "YT	 - 	YUKON TERRITORY");
		estados.put("TN", "TN	 - 	TENNESSEE");
		estados.put("TX", "TX	 - 	TEXAS");
		estados.put("UT", "UT	 - 	UTAH");
		estados.put("VT", "VT	 - 	VERMONT");
		estados.put("VA", "VA	 - 	VIRGINIA");
		estados.put("WA", "WA	 - 	WASHINGTON");
		estados.put("WV", "WV	 - 	WEST VIRGINIA");
		estados.put("WI", "WI	 - 	WISCONSIN");
		estados.put("WY", "WY	 - 	WYOMING");
		estados.put("FM", "FM	 - 	Federated States of Micronesia");
		estados.put("MH", "MH	 - 	Marshall Islands");
		estados.put("MP", "MP	 - 	Northern Mariana Islands");
		estados.put("PW", "PW	 - 	Palau");
		estados.put("BVI", "BVI	 - 	Tortola ");
		return estados;
	}

	private boolean containsSpecialCharacters(String input) {
		Pattern p = Pattern.compile("[^A-Za-z0-9\\w ]+");
		Matcher m = p.matcher(input);

		return m.find();
	}

	private String getPinRequestStatus() {
		String pinRequestStatus = messageSource.getMessage("ccservices.pinrequest", null, Locale.ENGLISH);
		if (pinRequestStatus == null || pinRequestStatus.equals("")) {
			pinRequestStatus = "DISABLED";
		}
		return pinRequestStatus;
	}
}
