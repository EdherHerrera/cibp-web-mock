package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.context.MessageSource;
import org.springframework.web.servlet.mvc.CancellableFormController;

import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.constants.RemoteDepositConstants;
import evertec.cibp.web.request.RDCEnrollmentInfoRequest;
import evertec.cibp.web.session.UserSession;

/**
 * BaseRemoteDepositForm
 * @author Evertec CIBP
 * @version 1.0
 */
public class BaseRemoteDepositForm extends CancellableFormController {
	
	protected CustomerServices customerService;
	
	protected String rdcBid;
	protected String settlementGroupIdChk;
	protected String settlementGroupIdSav;
	
	protected BigDecimal regularDailyAmount;
	protected BigDecimal wealthDailyAmount;
	protected int regularDailyItem;
	protected int wealthDailyItem;
	protected BigDecimal regularMultidayAmount;
	protected BigDecimal wealthMultidayAmount;
	protected int regularMultidayItem;
	protected int wealthMultidayItem;
	
	protected String wealthCodes; // customer codes for wealth 
	protected String premiumBankingCodes; // customer codes for premium banking 
	protected String preferredCodes; // customer codes for preferred waalth 
	protected MessageSource messageSource; // MessageSource instance
	protected Map<String, String> confParameterLimits; // result from database of limits
	
	protected BigDecimal mDailyAmount; // daily limit amount
	protected int mDailyItem; // daily limit item count
	protected BigDecimal mMultidayAmount; // multiday limit amount
	protected int mMultidayItem; // multiday limit item count
	
	/**
	 * generateEnrollmentInfoRequest
	 * @param userSession
	 * @return RDCEnrollmentInfoRequest
	 */
	protected RDCEnrollmentInfoRequest generateEnrollmentInfoRequest(UserSession userSession) {
		RDCEnrollmentInfoRequest rdcEnrollmentReq = new RDCEnrollmentInfoRequest();
		setRDCLimitFieldsFromDB(userSession);
		rdcEnrollmentReq.setRdcBid(rdcBid);
		rdcEnrollmentReq.setSettlementGroupIdChk(settlementGroupIdChk);
		rdcEnrollmentReq.setSettlementGroupIdSav(settlementGroupIdSav);
		
		if (!StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)) {
			if (userSession.isWealth(wealthCodes, preferredCodes)) {
				mDailyAmount = wealthDailyAmount;
				mDailyItem = wealthDailyItem;
				mMultidayAmount = wealthMultidayAmount;
				mMultidayItem = wealthMultidayItem;
			}  else {
				mDailyAmount = regularDailyAmount;
				mDailyItem = regularDailyItem;
				mMultidayAmount = regularMultidayAmount;
				mMultidayItem = regularMultidayItem;
			}
		}
		
		rdcEnrollmentReq.setUserDailyAmount(mDailyAmount);
		rdcEnrollmentReq.setUserDailyItem(mDailyItem);
		rdcEnrollmentReq.setUserMultidayAmount(mMultidayAmount);
		rdcEnrollmentReq.setUserMultidayItem(mMultidayItem);
		
		return rdcEnrollmentReq;
	}
    
	/**
	 * setRDCLimitFieldsFromDB
	 * @param userSession
	 */
	protected void setRDCLimitFieldsFromDB (UserSession userSession) {
		final String rdcConstant = "RDC"; // Constant property filter of registers BD
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)) {
            confParameterLimits = customerService.getConfParametersByApp(rdcConstant).stream()
                    .collect(Collectors.toMap(m -> m.getShortName(), m -> m.getValue()));
            
            String dailyAmountValue = ""; // dailyAmountValue limits Segment
            String dailyItemCountValue = ""; // dailyItemCountValue limits Segment
            String multidayAmountValue = ""; // multidayAmountValue limits Segment
            String multidayItemCountValue = ""; // multidayItemCountValue limits Segment
            
            if (getConfParameterLimits() != null && !getConfParameterLimits().isEmpty()) {
                if (userSession.isComercialCustomer()) {
                	dailyAmountValue = RemoteDepositConstants.COMMERCIAL_DAILY_AMOUNT;
                	dailyItemCountValue = RemoteDepositConstants.COMMERCIAL_DAILY_ITEM_COUNT;
                	multidayAmountValue = RemoteDepositConstants.COMMERCIAL_MULTIDAY_AMOUNT;
                	multidayItemCountValue = RemoteDepositConstants.COMMERCIAL_MULTIDAY_ITEM_COUNT;
				} else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_PREMIUM_BANKING_LIMITS_PER_SEGMENT) && 
						userSession.isPremiumBanking(premiumBankingCodes, wealthCodes, preferredCodes)) {
					dailyAmountValue = RemoteDepositConstants.PREMIUM_BANKING_DAILY_AMOUNT;
					dailyItemCountValue = RemoteDepositConstants.PREMIUM_BANKING_DAILY_ITEM_COUNT;
					multidayAmountValue = RemoteDepositConstants.PREMIUM_BANKING_MULTIDAY_AMOUNT;
					multidayItemCountValue = RemoteDepositConstants.PREMIUM_BANKING_MULTIDAY_ITEM_COUNT;
				} else if (userSession.isWealth(wealthCodes, preferredCodes)) {
					dailyAmountValue = RemoteDepositConstants.WEALTH_DAILY_AMOUNT;
					dailyItemCountValue = RemoteDepositConstants.WEALTH_DAILY_ITEM_COUNT;
					multidayAmountValue = RemoteDepositConstants.WEALTH_MULTIDAY_AMOUNT;
					multidayItemCountValue = RemoteDepositConstants.WEALTH_MULTIDAY_ITEM_COUNT;
				}  else {
					dailyAmountValue = RemoteDepositConstants.REGULAR_DAILY_AMOUNT;
					dailyItemCountValue = RemoteDepositConstants.REGULAR_DAILY_ITEM_COUNT;
					multidayAmountValue = RemoteDepositConstants.REGULAR_MULTIDAY_AMOUNT;
					multidayItemCountValue = RemoteDepositConstants.REGULAR_MULTIDAY_ITEM_COUNT;
				}
                mDailyAmount = new BigDecimal(getValue(dailyAmountValue));
				mDailyItem = Integer.parseInt(getValue(dailyItemCountValue));
				mMultidayAmount = new BigDecimal(getValue(multidayAmountValue));
				mMultidayItem = Integer.parseInt(getValue(multidayItemCountValue));
            }
        }
    }
	
	/**
	 * getValue
	 * @param confParameterLimits
	 * @param strKey
	 * @return
	 */
	private String getValue(String strKey) {
		String value = "0";
		String parameterLimit = getConfParameterLimits().get(strKey);
		if (parameterLimit != null) {
			value = parameterLimit;
		}
		return value;
	}

	/**
	 * getWealthCodes
	 * @return String
	 */
	public String getWealthCodes() {
		return wealthCodes;
	}

	/**
	 * setWealthCodes
	 * @param strWealthCodes
	 */
	public void setWealthCodes(String strWealthCodes) {
		this.wealthCodes = strWealthCodes;
	}

	/**
	 * getCustomerService
	 * @return CustomerServices
	 */
	public CustomerServices getCustomerService() {
		return customerService;
	}

	/**
	 * setCustomerService
	 * @param customerService
	 */
	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	/**
	 * getPremiumBankingCodes
	 * @return String
	 */
	public String getPremiumBankingCodes() {
		return premiumBankingCodes;
	}

	/**
	 * setPremiumBankingCodes
	 * @param strPremiumBankingCodes
	 */
	public void setPremiumBankingCodes(String strPremiumBankingCodes) {
		this.premiumBankingCodes = strPremiumBankingCodes;
	}

	/**
	 * getMessageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * setMessageSource
	 * @param messageSource
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * getPreferredCodes
	 * @return String
	 */
	public String getPreferredCodes() {
		return preferredCodes;
	}

	/**
	 * setPreferredCodes
	 * @param strPreferredCodes
	 */
	public void setPreferredCodes(String strPreferredCodes) {
		this.preferredCodes = strPreferredCodes;
	}
	
	////// Others variables
	
	/**
	 * getRdcBid
	 * @return String
	 */
	public String getRdcBid() {
		return rdcBid;
	}

	/**
	 * setRdcBid
	 * @param strRdcBid
	 */
	public void setRdcBid(String strRdcBid) {
		this.rdcBid = strRdcBid;
	}

	/**
	 * getRegularDailyAmount
	 * @return BigDecimal
	 */
	public BigDecimal getRegularDailyAmount() {
		return regularDailyAmount;
	}

	/**
	 * 
	 * @param regularDailyAmount
	 */
	public void setRegularDailyAmount(BigDecimal regularDailyAmount) {
		this.regularDailyAmount = regularDailyAmount;
	}

	/**
	 * getWealthDailyAmount
	 * @return
	 */
	public BigDecimal getWealthDailyAmount() {
		return wealthDailyAmount;
	}

	/**
	 * wealthDailyAmount
	 * @param wealthDailyAmount
	 */
	public void setWealthDailyAmount(BigDecimal wealthDailyAmount) {
		this.wealthDailyAmount = wealthDailyAmount;
	}

	/**
	 * getRegularDailyItem
	 * @return
	 */
	public int getRegularDailyItem() {
		return regularDailyItem;
	}

	/**
	 * setRegularDailyItem
	 * @param intRegularDailyItem
	 */
	public void setRegularDailyItem(int intRegularDailyItem) {
		this.regularDailyItem = intRegularDailyItem;
	}

	/**
	 * getWealthDailyItem
	 * @return
	 */
	public int getWealthDailyItem() {
		return wealthDailyItem;
	}

	/**
	 * setWealthDailyItem
	 * @param intWealthDailyItem
	 */
	public void setWealthDailyItem(int intWealthDailyItem) {
		this.wealthDailyItem = intWealthDailyItem;
	}

	/**
	 * getRegularMultidayAmount
	 * @return
	 */
	public BigDecimal getRegularMultidayAmount() {
		return regularMultidayAmount;
	}

	/**
	 * setRegularMultidayAmount
	 * @param regularMultidayAmount
	 */
	public void setRegularMultidayAmount(BigDecimal regularMultidayAmount) {
		this.regularMultidayAmount = regularMultidayAmount;
	}

	/**
	 * getWealthMultidayAmount
	 * @return
	 */
	public BigDecimal getWealthMultidayAmount() {
		return wealthMultidayAmount;
	}

	/**
	 * setWealthMultidayAmount
	 * @param wealthMultidayAmount
	 */
	public void setWealthMultidayAmount(BigDecimal wealthMultidayAmount) {
		this.wealthMultidayAmount = wealthMultidayAmount;
	}

	/**
	 * getRegularMultidayItem
	 * @return
	 */
	public int getRegularMultidayItem() {
		return regularMultidayItem;
	}

	/**
	 * setRegularMultidayItem
	 * @param intRegularMultidayItem
	 */
	public void setRegularMultidayItem(int intRegularMultidayItem) {
		this.regularMultidayItem = intRegularMultidayItem;
	}

	/**
	 * getWealthMultidayItem
	 * @return
	 */
	public int getWealthMultidayItem() {
		return wealthMultidayItem;
	}

	/**
	 * setWealthMultidayItem
	 * @param intWealthMultidayItem
	 */
	public void setWealthMultidayItem(int intWealthMultidayItem) {
		this.wealthMultidayItem = intWealthMultidayItem;
	}

	/**
	 * getSettlementGroupIdChk
	 * @return
	 */
	public String getSettlementGroupIdChk() {
		return settlementGroupIdChk;
	}

	/**
	 * setSettlementGroupIdChk
	 * @param strSettlementGroupIdChk
	 */
	public void setSettlementGroupIdChk(String strSettlementGroupIdChk) {
		this.settlementGroupIdChk = strSettlementGroupIdChk;
	}

	/**
	 * getSettlementGroupIdSav
	 * @return
	 */
	public String getSettlementGroupIdSav() {
		return settlementGroupIdSav;
	}

	/**
	 * setSettlementGroupIdSav
	 * @param strSettlementGroupIdSav
	 */
	public void setSettlementGroupIdSav(String strSettlementGroupIdSav) {
		this.settlementGroupIdSav = strSettlementGroupIdSav;
	}
	
	/**
	 * confParameterLimits
	 * @return Map<String, String>
	 */
	public Map<String, String> getConfParameterLimits() {
		return confParameterLimits;
	}
	
}
