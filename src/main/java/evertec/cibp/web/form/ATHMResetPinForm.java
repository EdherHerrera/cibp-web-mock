package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.entitlement.ATHMovilEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.form.bean.ATHMResetBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AthmUtils;
//import ath.fi.ws.client.ResponseStatus;
//import ath.fi.ws.client.api.ATHMobileFiService;

public class ATHMResetPinForm extends AbstractWizardFormController implements MessageSourceAware {
	protected final Log logger = LogFactory.getLog(getClass());

	protected CustomerServices customerService = null;

	private MessageSource messageSource = null;

	private String fiId = null;
	private String fiToken = null;

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getFiId() {
		return fiId;
	}

	public void setFiId(String fiId) {
		this.fiId = fiId;
	}

	public String getFiToken() {
		return fiToken;
	}

	public void setFiToken(String fiToken) {
		this.fiToken = fiToken;
	}

	public ATHMResetPinForm() {
		super();
		setCommandClass(ATHMResetBean.class);
		setCommandName("athmresetpin");
		setPages(new String[] { "athmresetpin", "athmresetpin", "athmresetpin" });
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		userSession.setLoadebpp(false);

		Map<String, Object> model = new HashMap<String, Object>();

		GlobalEntitlement entitlement = customerService.getGlobalEntitlement(EntitlementType.ATHMOVIL);

		model.put("page", page);

		if (entitlement == null || entitlement.getStatus() != EntitlementStatus.ENABLED) {
			model.put("down", "true");

			return model;
		}

		return model;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		ATHMResetBean athmResetBean = (ATHMResetBean) command;

		switch (page) {
		case 0:
			boolean isValidPin = true;

			if (athmResetBean.getPin() == null || athmResetBean.getPin().trim().length() <= 0) {
				isValidPin = false;
				errors.rejectValue("pin", "athmovil.pin.required", "PIN is required");
			} else if (AthmUtils.getNumericPin(athmResetBean.getPin()) == null
					|| "123456".equals(athmResetBean.getPin())) {
				isValidPin = false;
				errors.rejectValue("pin", "athmovil.pin.invalid", "Invalid PIN");
			}

			if (athmResetBean.getPinConfirm() == null
					|| athmResetBean.getPinConfirm().trim().length() <= 0) {
				isValidPin = false;
				errors.rejectValue("pinConfirm", "athmovil.pin.confirm.required", "PIN confirmation is required");
			} else if (athmResetBean.getPin() != null
					&& !athmResetBean.getPin().equals(athmResetBean.getPinConfirm())) {
				isValidPin = false;
				errors.rejectValue("pinConfirm", "athmovil.pin.confirm.nomatch",
						"PIN and PIN confirmation does not match");
			}

			if (isValidPin) {
				try {
					ATHMovilEntitlement entitlement = new ATHMovilEntitlement(userSession.getCustomerProfile()
							.getEntitlementByType(EntitlementType.ATHMOVIL));

					// ATHMobileFiService athMobileFiService = new
					// ATHMobileFiService(this.wsAddress, this.wsPortName);
					//
					// ResponseStatus status =
					// athMobileFiService.resetPin(entitlement.getCustomerToken(),
					// this.fiId,
					// this.fiToken, athmResetBean.getPin());
					//
					// if (status == null) {
					// errors.rejectValue("pin", "athmovil.reset.failed",
					// "Unable to complete PIN reset");
					// } else if (!status.getStatusCode().equals("00")) //
					// Success
					// {
					// errors.rejectValue("pin", "athmovil.reset.failed",
					// "Unable to complete PIN reset");
					// }
				} catch (Exception e) {
					errors.rejectValue("pin", "athmovil.reset.failed", "Unable to complete PIN reset");

					logger.error("PIN reset failed", e);
				}
			}

			break;

		default:
			break;
		}
	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();

		model.put("page", 2);

		return new ModelAndView("athmsendmoney", model);
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();

		model.put("page", 2);

		return new ModelAndView("athmsendmoney", model);
	}
}
