package evertec.cibp.web.form;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.chemistry.opencmis.commons.impl.json.JSONArray;
import org.apache.chemistry.opencmis.commons.impl.json.JSONObject;
import org.apache.chemistry.opencmis.commons.impl.json.parser.JSONParser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.AddPayeeBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * AddPayeeForm
 * @author et55596
 * @since 1.2
 * @version 1.0 
 * **/
public class AddPayeeForm extends CancellableFormController implements GlobalEntitlementAwareController,
		MessageSourceAware {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass()); // logger instance
	
	/**mpicado, atributo que se agrega para poder leer el archivo downtime,
	ocupa el implements del MessageSourceAware**/
	private MessageSource messageSource;
	private String findPayeeBox;

	public AddPayeeForm() {
		super();
		setCommandClass(AddPayeeBean.class);
		setCommandName("addpayee");
		setFormView("addpayee");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		return new ModelAndView("addpayee");
	}

	/**
	 * referenceData
	 * @param request request 
	 * @param command command
	 * @param errors errors forms
	 * @return Map
	 */
	protected Map<Object, Object> referenceData (HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		
		final UserSession userSession = KiuwanUtils.cast(UserSession.class, WebUtils.getSessionAttribute(request, "userSession")); // user
		CustomerServices customerService = userSession.getCustomerService();
		Map<Object, Object> model = new HashMap<>();
		if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			if (request.getSession().getAttribute("payeebean") != null) {
				request.getSession().removeAttribute("payeebean");
				model.put("submitgs", true);
			}
		}
		
		final boolean rtPayeeFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO); // flag
		final String rtPayeeKey = "realTimeNotificationsClaro"; // model key real time flag
		
		model.put(rtPayeeKey, rtPayeeFlag);

		//MBSFE-111 remove triple-s salud from common payees
		model.put("MBSFE111", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE111).toString());
		//MBSFE-7 hide ebill AEE payee
		model.put("MBSFE7", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE7).toString());
		model.put("MBSFE_138", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138));	
		
		//MBSFE-646 Common payees dynamic
		model.put("MBSFE646", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE646).toString());
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE646)) {
			String commonPayeeTreeRaw = "";
			
			// Load/Store common payee tree (first time retrieved from DB then from userSession)
			if (userSession.getCommonGlobalPayeesTree() == null) {
				commonPayeeTreeRaw = customerService.getCommonPayeeTree();
				userSession.setCommonGlobalPayeesTree(commonPayeeTreeRaw);
			} else {
				commonPayeeTreeRaw = userSession.getCommonGlobalPayeesTree();
			}
			
			if (!commonPayeeTreeRaw.equals("")) {				
				if ((userSession.getCommonGlobalPaymentTypeMap() == null || userSession.getRtMap() == null)) {
					JSONParser jsonParser = new JSONParser();
					JSONArray commonPayeesTreeJSON = (JSONArray) jsonParser.parse(commonPayeeTreeRaw);
					
					if (commonPayeesTreeJSON != null) {
						List<Integer> commonPayeeIds = getCommonPayeeIds(commonPayeesTreeJSON);
						Map<Integer, GlobalPayee> ps = PaymentsFacade.getInstance().getGlobalPayeeById(commonPayeeIds, userSession);
						Map<String, String> globalMap = new HashMap<String, String>();

						// RT Payment Logo
						Map<String, String> rtMap = new HashMap<String, String>();

						Set<Entry<Integer, GlobalPayee>> entrymap = ps.entrySet();
						for (Entry<Integer, GlobalPayee> entry : entrymap) {
							if (entry != null && entry.getKey() != null && entry.getValue() != null
									&& entry.getValue().getPaymentType() != null) {
								globalMap.put(String.valueOf(entry.getKey()), entry.getValue().getPaymentType().name());
								rtMap.put(String.valueOf(entry.getKey()), entry.getValue().getRtIndicator());
							}
						}

						userSession.setCommonGlobalpayees(ps);
						userSession.setCommonGlobalPaymentTypeMap(globalMap);
						userSession.setRtMap(rtMap);
						model.put("globalMap", globalMap);
						model.put("rtMap", rtMap);
					}
				} else {
					model.put("globalMap", userSession.getCommonGlobalPaymentTypeMap());
					model.put("rtMap", userSession.getRtMap());
				}
				
				model.put("language", Utils.getRSALanguage(request));
				model.put("commonPayeeTree", commonPayeeTreeRaw);
			} else {
				model.put("MBSFE646", "false");
				System.out.println("evertec.cibp.web.form.AddPayeeForm: Error with JSONObject retrieved from database.");
			}
		} else {
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138)|(userSession.getCommonGlobalPaymentTypeMap() == null || userSession.getRtMap() == null)) {

				List<Integer> utilities = Arrays.asList(new Integer[] { 36, 37, 331, 11492, 2334, 11562 });
				List<Integer> cable = Arrays.asList(new Integer[] { 69, 66, 64, 328, 68, 8184, 67, 228, 332, 1620, 4251,
						8739 });
				List<Integer> cells = Arrays.asList(new Integer[] { 405, 80, 530, 1152, 8471, 14226, 8469, 8470, 4688,
						10912 });
				// *************
				// Angel says: Seria bueno saber cual es la diferencia entre los
				// arreglos de cards, cards2 y cards3. Gracias.
				// *************
				List<Integer> cards = Arrays.asList(new Integer[] { 359, 517, 518, 1983, 223, 288, 4144, 289, 290, 12682,
						12453, 14346, 27, 8769, 8765, 8766, 9275, 8770, 8493, 8491, 8486, 8492, 8488 });
				List<Integer> cards2 = Arrays.asList(new Integer[] { 8490, 8485, 9268, 9428, 9302, 9303, 9308, 14178,
						14241, 11479, 11448, 11611, 11824, 14291, 10705, 8476, 8477, 8474, 8472, 8475, 8473, 14251 });
				List<Integer> cards3 = Arrays.asList(new Integer[] { 11515, 8467, 8461, 8460, 8464, 9078, 8465, 8462, 8671,
						8672, 11561, 11321, 11877, 11878, 8684 });
				List<Integer> loans = Arrays.asList(new Integer[] { 239, 240, 241, 185, 5301, 187, 189, 13330, 279, 63,
						1368, 1546, 8697, 8696 });
				List<Integer> gov = Arrays.asList(new Integer[] { 23, 148, 2173 });
				List<Integer> pmed = Arrays.asList(new Integer[] { 18509, 350, 7718, 11045, 319, 735, 22108 });// CIBP-2243
				List<Integer> ids = new LinkedList<Integer>();
				ids.addAll(utilities);
				ids.addAll(cable);
				ids.addAll(cable);
				ids.addAll(cells);
				ids.addAll(cards);
				ids.addAll(cards2);
				ids.addAll(cards3);
				ids.addAll(loans);
				ids.addAll(gov);
				ids.addAll(pmed);// CIBP-2243

				Map<Integer, GlobalPayee> ps = PaymentsFacade.getInstance().getGlobalPayeeById(ids, userSession);
				Map<String, String> globalMap = new HashMap<String, String>();

				// RT Payment Logo
				Map<String, String> rtMap = new HashMap<String, String>();

				Set<Entry<Integer, GlobalPayee>> entrymap = ps.entrySet();
				for (Entry<Integer, GlobalPayee> entry : entrymap) {
					if (entry != null && entry.getKey() != null && entry.getValue() != null
							&& entry.getValue().getPaymentType() != null) {
						globalMap.put(String.valueOf(entry.getKey()), entry.getValue().getPaymentType().name());
						rtMap.put(String.valueOf(entry.getKey()), entry.getValue().getRtIndicator());
					}
				}

				userSession.setCommonGlobalpayees(ps);
				userSession.setCommonGlobalPaymentTypeMap(globalMap);
				userSession.setRtMap(rtMap);
				model.put("globalMap", globalMap);
				model.put("rtMap", rtMap);
			} else {
				model.put("globalMap", userSession.getCommonGlobalPaymentTypeMap());
				model.put("rtMap", userSession.getRtMap());
			}
		}
		//MBSFE-646 Common payees dynamic

		// mpicado, cibp-2218, se agrega logica para poder incluir la seccion
		// extra del guided setup

		if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			/*
			 * aqui establezco el valor de el MessageSource para que lo pueda
			 * usar el bean del core, me parece que podria hacerse accesando el
			 * context pero tuve algunos problemas al hacerlo.. mpicado
			 * CIBP-1799
			 */
			customerService.setEBPPStatus(this.getEBPPMessageSourceStatus());

			// List<TVFrontEndPayee> payees = userSession.getPayessFullEbpp();
			// mpicado CIBP-1799 se cambia el llamado por el metodo sobrecargado
			// que se creo, para poder enviarle el status de EBPP
			List<TVFrontEndPayee> payees = userSession.getPayees(true);

			if (payees != null && payees.size() > 0) {
				for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
					TVFrontEndPayee name = (TVFrontEndPayee) iterator.next();
					// This are globalPayee IDs for BPPR reserve accounts
					int tp = name.getGlobalPayeeId();

					/*
					 * mpicado, me parece que esto no deberia estar hardcoded,
					 * podria hacerse una propiedad para esta condicion, como no
					 * se la logica exacta para lo que se usa entonces lo dejo
					 * tal y como lo encontre
					 */
					if (tp == 288 || tp == 289 || tp == 4144 || tp == 290 || tp == 12682 || tp == 12453) {
						TVFrontendAccount reserveAct = Utils.getAccountByKey(
								name.getBillingAccount().trim() + "|IDA|R", userSession.getAccounts());
						if (reserveAct != null && reserveAct.getPortalBalance() != null) {
							name.setCurrentBalance(reserveAct.getPortalBalance());
						}
					}
				}
				model.put("payeesGS", payees);
			}

			if (userSession.getGuidedSetupSession().isResubmitParent()) {
				model.put("submitgs", true);
				userSession.getGuidedSetupSession().setResubmitParent(false);
			}

		}
		
		//MBSFE 1143
		model.put("findPayeeBox", getFindPayeeBox());

		return model;

	}

	/**
	 * Metodo getCommonPayeeIds Metodo recursivo para obtener los ids de los pagos comunes 
	 * obtenidos de la base de datos
	 * 
	 * @return lista con los ids de los pagos comunes
	 */
	private List<Integer> getCommonPayeeIds(JSONArray commonPayees) {
		List<Integer> ids = new LinkedList<Integer>();
		
		for (int i = 0; i < commonPayees.size(); i++) {
		    JSONObject node = (JSONObject) commonPayees.get(i);
		    
		    if (node.containsKey("children")) {
		    	ids.addAll(getCommonPayeeIds((JSONArray) node.get("children")));
		    } else {
		    	if (node.containsKey("id")) {
		    		ids.add(new Integer(node.get("id").toString()));
		    	}
		    }
		  }
		
		return ids;
	}
	
	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.BILLPAY;
	}

	/** @return regresa el messageSource */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource
	 *            el messageSource a establecer
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	//MBSFE 1143
	public String getFindPayeeBox() {
		return findPayeeBox;
	}

	public void setFindPayeeBox(String findPayeeBox) {
		this.findPayeeBox = findPayeeBox;
	}
	//END MBSFE 1143
	
	

}
