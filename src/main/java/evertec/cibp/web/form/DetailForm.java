package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.ifxforum.x15.CurrencyAmount;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.AccountSubtypes;
import bppr.tv.beans.accounts.records.InstallmentLoanAccountRecord;
import bppr.tv.beans.accounts.types.LoanAccount;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TVFrontendCreditAccount;
import evertec.cibp.core.models.account.TVFrontendRewardsAccount;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.base.AbstractStatement;
import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.documents.CustomerDocuments;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.statements.AccountPDFStmtInfo;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.StatementRequest;
import evertec.cibp.core.services.requests.TransactionRequest;
import evertec.cibp.core.services.responses.PremiaAgregatedTransactionResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.form.bean.DetailBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class DetailForm extends CancellableFormController implements MessageSourceAware {
	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;
	private int maxStatements;
	private int maxTransactions;
	private String unversalProducts;
	private String insuranceRemoved;
	private String pingfederateSsoUrl; 
	private String pingfederateSsoEndpoint;
	private String pingfederateSsoPartnerUrl;

	public DetailForm() {
		super();
		setCommandClass(DetailBean.class);
		setCommandName("detail");
		setFormView("detail");
		setBindOnNewForm(true);
	}

	protected boolean isFormSubmission(HttpServletRequest request) {
		return (request.getParameter("account") != null);
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customerProfile = session.getCustomerProfile();
		CustomerServices customerService = session.getCustomerService();

		DetailBean d = (DetailBean) command;
		TVFrontendAccount account = Utils.getAccountById(d.getAccount(), session.getAccounts());

		// CIBP-1767 - redirect to the new portal.
		if (account != null && !GenericValidator.isBlankOrNull(account.getSubtype())
				&& (account.getSubtype().equalsIgnoreCase("IDA") || account.getSubtype().equalsIgnoreCase("CCA"))) {
			return new ModelAndView("redirect:transaction?account=" + account.getFrontEndId() + "&tabNum=0&trxTabNum=0");
		}
		// end CIBP-1767

		AbstractAccount accountDetails = session.getCustomerService().getAccountDetails(session.getCustomerProfile(),
				session.getBankingSession(), account, true);
		Map<String, Object> model = new HashMap<>();
		model.put("accountDetails", accountDetails);
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			model.put("Lang", "sp");
		else
			model.put("Lang", "en");

		if (account != null && (account.getAccountProductId().equalsIgnoreCase("009")// Acceso
																						// Popular
				|| account.getAccountProductId().equalsIgnoreCase("020")// Avance
																		// 50
				|| account.getAccountProductId().equalsIgnoreCase("023")// Multicuenta
																		// Staff
				|| account.getAccountProductId().equalsIgnoreCase("024")// Popular
																		// Plus
				|| account.getAccountProductId().equalsIgnoreCase("025")// Multicuenta
																		// Popular
				|| account.getAccountProductId().equalsIgnoreCase("026")// Ideal
				|| account.getAccountProductId().equalsIgnoreCase("028")// MaxiPremium
				|| account.getAccountProductId().equalsIgnoreCase("029")// Private
																		// Management
																		// Account
				|| account.getAccountProductId().equalsIgnoreCase("049")// Ahorro
																		// a
																		// Toda
																		// Hora
																		// Staff
				|| account.getAccountProductId().equalsIgnoreCase("051")// Ahorro
																		// 24
				|| account.getAccountProductId().equalsIgnoreCase("052")// Ahorro
																		// a
																		// Toda
																		// Hora
				|| account.getAccountProductId().equalsIgnoreCase("069")// Multicuenta
																		// Subsidiary
																		// Staff
				|| account.getAccountProductId().equalsIgnoreCase("072")// Ahorro
																		// a
																		// Toda
																		// Hora
																		// Subsidiary
																		// Staff
				|| account.getAccountProductId().equalsIgnoreCase("081")// e-account
		|| account.getAccountProductId().equalsIgnoreCase("087")))// CIMA
		{
			model.put("showOdLimitLink", true);
		} else
			model.put("showOdLimitLink", false);

		// Interest Code
		if (account != null
				&& !GenericValidator.isBlankOrNull(account.getSubtype())
				&& (account.getSubtype().equals("MLA") || account.getSubtype().equals("RVA")
						|| account.getSubtype().equals("LEA") || account.getSubtype().equals("CDA"))) {

			// mpicado, cibp-1799, se agrego la condicion de estado de ebpp en
			// el archvio (this.getEBPPMessageSourceStatus)
			if (customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED
					&& this.getEBPPMessageSourceStatus()) {
				AccountPDFStmtInfo infoData = null;

				List<AccountPDFStmtInfo> intStmtInfoList = session.getInterestStatementsLastStmt();

				if (intStmtInfoList != null) {
					for (Iterator<AccountPDFStmtInfo> iterator = intStmtInfoList.iterator(); iterator.hasNext();) {
						AccountPDFStmtInfo info = iterator.next();

						if (account.getSubtype().equals("CDA")) {
							if (account.getAccountNumber().startsWith(info.getAccountNumber().trim())) {
								infoData = info;
								break;
							}
						} else {
							if (info.getAccountNumber().equalsIgnoreCase(account.getAccountNumber())) {
								infoData = info;
								break;
							}
						}
					}

					if (infoData != null) {
						model.put("AccountPDFStmtInfo", infoData);
					}
				}
			}
		}
		// End Interest Code
		if (account != null && account.getSubtype().equalsIgnoreCase("CCA")) {
			TVFrontendCreditAccount frontendCreditAccount = (TVFrontendCreditAccount) accountDetails;
			String activity = d.getActivity();
			model.put("activity", activity);

			LinkedHashMap<String, String> activityMap = new LinkedHashMap<String, String>();
			if ((unversalProducts.indexOf(account.getAccountProductId()) != -1)
					&& "sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				activityMap.put("All", "Todas");
				activityMap.put("R", "Transacciones a cr&eacute;dito");
				activityMap.put("C", "Transacciones a cargo");
			}
			if ((unversalProducts.indexOf(account.getAccountProductId()) != -1)
					&& !("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))) {
				activityMap.put("All", "All");
				activityMap.put("R", "Credit Transactions");
				activityMap.put("C", "Charge Transactions");
			}
			if ((unversalProducts.indexOf(account.getAccountProductId()) != -1)) {
				model.put("activityMap", activityMap);
				model.put("universalProduct", true);
			} else
				model.put("universalProduct", false);

			List<AbstractStatement> availableStatements = null;
			if (account.getStatements() == null) {
				BankingSession bankingSession = session.getBankingSession();
				StatementRequest stmtrq = new StatementRequest(Utils.generateCustomerInteractionEvent(request),
						customerProfile, bankingSession, account, maxStatements);
				availableStatements = customerService.getAvailableStatements(stmtrq).getStatementList();
				account.setStatements(availableStatements);
			} else
				availableStatements = account.getStatements();
			AbstractStatement stm = Utils.getStatementById("2", availableStatements);
			if (stm != null)
				model.put("noStatement", false);
			else if (stm == null)
				model.put("noStatement", true);
			if (stm != null && (unversalProducts.indexOf(account.getAccountProductId()) != -1)) {
				if (stm.getChargeCardBalance() != null && stm.getEndBalance() != null) {
					frontendCreditAccount.setLastChargeCardBalance(stm.getChargeCardBalance());
					frontendCreditAccount.setLastRevolvingCardBalance(stm.getEndBalance().subtract(
							frontendCreditAccount.getLastChargeCardBalance()));
					model.put("frontendCreditAccount", frontendCreditAccount);

				} else {
					logger.error("Error: Account Summary CCA -- chargecard balance and/or end balance is null");
				}
			}
			// model.put("chargeCardBalance", chargeCardBalance.toString());
		} else if (account.getSubtype().equalsIgnoreCase("MLA")) {
			Calendar startDate = Calendar.getInstance();
			startDate.add(Calendar.MONTH, -16);
			startDate.set(Calendar.DAY_OF_MONTH, 1);

			TransactionRequest reqes = new TransactionRequest(Utils.generateCustomerInteractionEvent(request), null,
					customerProfile, session.getBankingSession(), account, maxTransactions, 1, false, startDate, null,
					null);
			TransactionsResponse transactionResponse = session.getCustomerService()
					.getMortgageEscrowTransactions(reqes);

			if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
				return showForm(request, response, errors, model);

			List<AbstractTransaction> policyTaxes = transactionResponse.getTransactions();
			if (policyTaxes != null && policyTaxes.size() > 0)
				model.put("policyTaxes", policyTaxes);
		} else if (account.getSubtype().equalsIgnoreCase("PRM")) {
			if (d.getPremiaCycle() != null)
				model.put("premiacycle", d.getPremiaCycle());
			else
				model.put("premiacycle", "1");

			List<AbstractStatement> availableStatements = account.getStatements();

			// Get premia detail to obtain points buckets
			TVFrontendRewardsAccount prmAcct = (TVFrontendRewardsAccount) accountDetails;
			if (prmAcct != null && prmAcct.getExpirationPoints() != null && prmAcct.getExpirationPoints().size() > 0)
				model.put("expirationPoints", prmAcct.getExpirationPoints());

			if (account.getStatements() == null) {
				StatementRequest stmtrq = new StatementRequest(Utils.generateCustomerInteractionEvent(request),
						customerProfile, session.getBankingSession(), account, maxStatements);
				availableStatements = session.getCustomerService().getAvailableStatements(stmtrq).getStatementList();
				account.setStatements(availableStatements);
			}

			if (d.getPremiaCycle() != null && !d.getPremiaCycle().equals("1")) // Means
																				// Current
																				// Transactions
			{
				// We assume to get the agregated transactions
				AbstractStatement stm = Utils.getStatementById(d.getPremiaCycle(), availableStatements);
				model.put("endbalance", stm.getEndBalance());
				TransactionRequest prmrq = new TransactionRequest(Utils.generateCustomerInteractionEvent(request), stm,
						customerProfile, session.getBankingSession(), account, maxTransactions, 1, false, null, null,
						null);
				prmrq.setCategories(null);
				PremiaAgregatedTransactionResponse prmResponse = session.getCustomerService()
						.getPremiaAgregatedTransactions(prmrq);

				if (prmResponse.getStatusResult() != ResultStatus.SUCCESS)
					return showForm(request, response, errors, model);

				processAgregatedResponse(model, prmResponse);
			} else {

				// Buscando transacciones agregadas:
				TransactionRequest prmreq = new TransactionRequest(Utils.generateCustomerInteractionEvent(request),
						null, customerProfile, session.getBankingSession(), account, maxTransactions, 1, false, null,
						null, null);
				PremiaAgregatedTransactionResponse prmResponse = session.getCustomerService()
						.getPremiaAgregatedTransactions(prmreq);

				if (prmResponse.getStatusResult() != ResultStatus.SUCCESS)
					return showForm(request, response, errors, model);

				processAgregatedResponse(model, prmResponse);
			}
		} else if ("SEC".equalsIgnoreCase(account.getSubtype())) {
			// This is for the SSO agreement for Popular Securities. Copy in
			// referenceData() for null account parameter
			String accepted = session.getCustomerService().getSSOAccepted(session.getCustomerProfile().getProfileId());
			if ("1".equalsIgnoreCase(accepted) || "0".equalsIgnoreCase(accepted))
				model.put("SSOaccepted", accepted);
			else if (accepted == null)
				model.put("SSOaccepted", "nouser");
			model.put("pingfederateSsoUrl", pingfederateSsoUrl);
			model.put("pingfederateSsoEndpoint", pingfederateSsoEndpoint);
			model.put("pingfederateSsoPartnerUrl", pingfederateSsoPartnerUrl);
		} else if(account.getSubtype().equalsIgnoreCase("LEA")){
			model.put("flagAuto", StaticMessageSource.isFlagEnabled(StaticMessageSource.MORTGAGE_AUTO_ACH));
		} else if (account.getSubtype().equalsIgnoreCase("RVA")){
			model.put("flagAuto", StaticMessageSource.isFlagEnabled(StaticMessageSource.MORTGAGE_AUTO_ACH));
		}

		return showForm(request, response, errors, model);
	}

	private void processAgregatedResponse(Map<String, Object> model, PremiaAgregatedTransactionResponse prmResponse) {
		model.put("cardsTrans", prmResponse.getCardTransactions());
		model.put("acctsTrans", prmResponse.getAccountsTransactions());
		model.put("electTrans", prmResponse.getElectronicsTransactions());
		model.put("loansTrans", prmResponse.getLoansTransactions());

		model.put("totalAcumulated", prmResponse.getTotalAcumulated());

		model.put("promoAmt", prmResponse.getTotalPromotions());
		model.put("redemAmt", prmResponse.getTotalRedemption());
		model.put("adjstAmt", prmResponse.getTotalAdjustment());
		model.put("expAmt", prmResponse.getTotalExpiration());
		model.put(
				"totalRedemptAmt",
				prmResponse.getTotalPromotions().add(
						prmResponse.getTotalRedemption().add(
								prmResponse.getTotalAdjustment().add(prmResponse.getTotalExpiration()))));
	}

	protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String firstAccount = "";

		Map<String, Object> model = new HashMap<>();
		List<TVFrontendAccount> userAccounts = userSession.getAccounts();
		AccountMaskUtil mask = new AccountMaskUtil();
		Map<String, String> detailsAccounts = new LinkedHashMap<String, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			model.put("Lang", "sp");
		else
			model.put("Lang", "en");
		
		// MBSFE-1784 Display residual value for auto loan
		String residualIndRVA = "I";
		BigDecimal residualValueRVA = new BigDecimal(0.0);
		// END MBSFE-1784

		String accountKey = request.getParameter("account");
		if (!GenericValidator.isBlankOrNull(accountKey)) {
			TVFrontendAccount account = Utils.getAccountById(accountKey, userSession.getAccounts());
			
			// MBSFE-1784 Display residual value for auto loan
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1784)) {
				if (AccountSubtypes.INSTALLMENT_LOAN.equals(account.getSubtype())
						|| "RVA".equals(account.getSubtype())) {
					AbstractAccount accountDetails = userSession.getCustomerService().getAccountDetails(
							userSession.getCustomerProfile(), userSession.getBankingSession(), account, true);
					LoanAccount loanAct = (LoanAccount) accountDetails.getBackendObject();
					InstallmentLoanAccountRecord rec = (InstallmentLoanAccountRecord) loanAct.getRecords();

					if (rec.getResidualInd() != null && rec.getResidualValue().getAmt() != null) {
						residualIndRVA = rec.getResidualInd();
						residualValueRVA = rec.getResidualValue().getAmt();
					}
				}
			}
			// END MBSFE-1784
			
			List<CustomerDocuments> docs = userSession.getDocumentsAfterAcceptance(account.getAccountNumber());
			if (docs != null && docs.size() > 0) {
				model.put("documents", docs);
			}
		}

		/*
		 * if(Utils.getRSALanguage(request).equals("sp"))
		 * detailsAccounts.put("none", "Seleccione una cuenta"); else
		 * detailsAccounts.put("none", "Select an account");
		 */
		int odLimitAccounts = 0;
		for (Iterator<TVFrontendAccount> iter = userAccounts.iterator(); iter.hasNext();) {
			TVFrontendAccount element = (TVFrontendAccount) iter.next();
			String suffix;
			if (element.getAccountNumberSuffix() != null)
				suffix = element.getAccountNumberSuffix();
			else
				suffix = "";
			if ((!suffix.equalsIgnoreCase("")) && suffix.equalsIgnoreCase("RES") || suffix.equalsIgnoreCase("SAV"))
				continue;
			String productId = element.getAccountProductId();
			if (productId.equalsIgnoreCase("009")// Acceso Popular
					|| productId.equalsIgnoreCase("020")// Avance 50
					|| productId.equalsIgnoreCase("023")// Multicuenta Staff
					|| productId.equalsIgnoreCase("024")// Popular Plus
					|| productId.equalsIgnoreCase("025")// Multicuenta Popular
					|| productId.equalsIgnoreCase("026")// Ideal
					|| productId.equalsIgnoreCase("028")// MaxiPremium
					|| productId.equalsIgnoreCase("029")// Private Management
														// Account
					|| productId.equalsIgnoreCase("049")// Ahorro a Toda Hora
														// Staff
					|| productId.equalsIgnoreCase("051")// Ahorro 24
					|| productId.equalsIgnoreCase("052")// Ahorro a Toda Hora
					|| productId.equalsIgnoreCase("069")// Multicuenta
														// Subsidiary Staff
					|| productId.equalsIgnoreCase("072")// Ahorro a Toda Hora
														// Subsidiary Staff
					|| productId.equalsIgnoreCase("081")// e-account
					|| productId.equalsIgnoreCase("087"))// CIMA
			{
				odLimitAccounts++;
			}
		}
		model.put("odLimitAccounts", odLimitAccounts);
		Map<String, TVFrontendAccount> detailsAccountsMap = new LinkedHashMap<String, TVFrontendAccount>(); // detailsAccountsMap
		for (Iterator<TVFrontendAccount> iter = userAccounts.iterator(); iter.hasNext();) {
			TVFrontendAccount element = (TVFrontendAccount) iter.next();
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE675)) {
				if (element.getSubtype().equals("INS") || element.getSubtype().equals("SEC")){			
					if(!this.insuranceRemoved.contains(element.getProductDefinition().getProductId())){
						firstAccount = listAccounts(element,  mask,  detailsAccounts, firstAccount,  userSession,  request,  model);
					}
				}
				else{
					firstAccount = listAccounts(element,  mask,  detailsAccounts, firstAccount,  userSession,  request,  model);
				}
			}
			else{
				firstAccount = listAccounts(element,  mask,  detailsAccounts, firstAccount,  userSession,  request,  model);
			}
			
			detailsAccountsMap.put(String.valueOf(element.getFrontEndId()), element);
		}

		model.put("detailAccounts", detailsAccounts);
		model.put("detailAccountsMap", detailsAccountsMap);

		// If there is no account selected, select the first one and add its
		// details
		if (request.getParameter("account") == null && !("".equals(firstAccount))) {

			TVFrontendAccount account = Utils.getAccountById(firstAccount, userSession.getAccounts());

			// CIBP-1767 - redirect to the new portal.
			if (account.getSubtype().equalsIgnoreCase("IDA") || account.getSubtype().equalsIgnoreCase("CCA")) {
				throw new ModelAndViewDefiningException(
						new ModelAndView(new RedirectView("transaction?tabNum=0", true)));
			}
			// end CIBP-1767

			AbstractAccount accountDetails = userSession.getCustomerService().getAccountDetails(
					userSession.getCustomerProfile(), userSession.getBankingSession(), account, true);
			model.put("firstaccount", firstAccount);
			model.put("accountDetails", accountDetails);

			// This is for the SSO agreement for Popular Securities. Copy on
			// onSubmit() method.
			if ("SEC".equalsIgnoreCase(account.getSubtype())) {
				String accepted = userSession.getCustomerService().getSSOAccepted(
						userSession.getCustomerProfile().getProfileId());
				if ("1".equalsIgnoreCase(accepted))
					model.put("SSOaccepted", accepted);
				model.put("pingfederateSsoUrl", pingfederateSsoUrl);
				model.put("pingfederateSsoEndpoint", pingfederateSsoEndpoint);
				model.put("pingfederateSsoPartnerUrl", pingfederateSsoPartnerUrl);
			}
		}
		
		model.put("isTransactional", AccountUtils.getIsTransactional(userSession.getCustomerProfile()).equalsIgnoreCase("Y"));
		
		model.put("ACH_PAYMENT_NT", StaticMessageSource.isFlagEnabled(StaticMessageSource.ACH_PAYMENT_NT));
		
		model.put("flagMortgage", StaticMessageSource.isFlagEnabled(StaticMessageSource.MORTGAGE_AUTO_ACH));
		String isTab = request.getParameter("tab");
		model.put("isTab", !GenericValidator.isBlankOrNull(isTab) && isTab.equals("1"));
		
		// MBSFE-1784 Display residual value for auto loan
		model.put("residualIndRVA", residualIndRVA);
		model.put("residualValueRVA", residualValueRVA);
		model.put("MBSFE1784", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1784));
		// END MBSFE-1784
		
		// MBFIS-231
		model.put("isSecurities", CustomerUtils.isSecurities(userSession.getCustomerProfile()));
		//END
		
		//MBSE-2513 Hide or Show Mortgage Transactions tab
		final String flagMBSE2513 = "flagMBSE2513"; // model key flag hide mortgage transactions
		
		model.put(flagMBSE2513, StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513));
		//End
		return model;
	}
	
	
	private String listAccounts(TVFrontendAccount element, AccountMaskUtil mask, Map<String, String> detailsAccounts, 
			String firstAccount, UserSession userSession, HttpServletRequest request, Map<String, Object> model){
		if (!element.getHide()) {
			String key = "";
			
			//MBFIS-231
			if (CustomerUtils.isSecurities(userSession.getCustomerProfile())) {
				key = element.getNickname() + " " + mask.mask(element.getAccountNumber()) + " "  + Utils.formatCurrency(element.getNetworthBalance());
			} else {
				key = StringUtils.abbreviate(element.getNickname(), 15)
					+ " "
						+ mask.mask(element.getAccountNumber())
					+ ((element.getAccountNumberSuffix() != null && !element.getAccountNumberSuffix().equals("")) ? " "
							+ element.getAccountNumberSuffix()
							: "");
			}
			//END MBFIS-231
			
			detailsAccounts.put(String.valueOf(element.getFrontEndId()), key);
			if ("".equals(firstAccount))
				firstAccount = String.valueOf(element.getFrontEndId());
			if (element.getSubtype().equalsIgnoreCase("PRM")) {
				// Solo poner en reference data las fechas de statements
				List<AbstractStatement> availableStatements = null;
				if (element.getStatements() == null) {

					CustomerInteractionEvent customerInteractionEvent = Utils
							.generateCustomerInteractionEvent(request);
					StatementRequest stmtrq = new StatementRequest(customerInteractionEvent,
							userSession.getCustomerProfile(), userSession.getBankingSession(), element,
							maxStatements);
					availableStatements = userSession.getCustomerService().getAvailableStatements(stmtrq)
							.getStatementList();
					element.setStatements(availableStatements);
				} else
					availableStatements = element.getStatements();

				LinkedHashMap<String, AbstractStatement> accountStatementMap = new LinkedHashMap<String, AbstractStatement>();
				LinkedHashMap<String, String> statementsMap = new LinkedHashMap<String, String>();
				statementsMap = Utils.getStatements(availableStatements, accountStatementMap,
						Utils.getRSALanguage(request), element.getSubtype().equals("PRM"));
				model.put("statementsMap", statementsMap);
			
			}
		}
		return firstAccount;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		// TODO Auto-generated method stub

		DetailBean d = (DetailBean) command;
		if (GenericValidator.isBlankOrNull(d.getAccount()) || "none".equals(d.getAccount()))
			errors.rejectValue("account", "", "Please select an account");

	}

	public int getMaxStatements() {
		return maxStatements;
	}

	public void setMaxStatements(int maxStatements) {
		this.maxStatements = maxStatements;
	}

	public int getMaxTransactions() {
		return maxTransactions;
	}

	public void setMaxTransactions(int maxTransactions) {
		this.maxTransactions = maxTransactions;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

	public String getUnversalProducts() {
		return unversalProducts;
	}

	public void setUnversalProducts(String unversalProducts) {
		this.unversalProducts = unversalProducts;
	}

	public String getInsuranceRemoved() {
		return insuranceRemoved;
	}

	public void setInsuranceRemoved(String insuranceRemoved) {
		this.insuranceRemoved = insuranceRemoved;
	}

	public String getPingfederateSsoUrl() {
		return pingfederateSsoUrl;
	}

	public void setPingfederateSsoUrl(String pingfederateSsoUrl) {
		this.pingfederateSsoUrl = pingfederateSsoUrl;
	}

	public String getPingfederateSsoEndpoint() {
		return pingfederateSsoEndpoint;
	}

	public void setPingfederateSsoEndpoint(String pingfederateSsoEndpoint) {
		this.pingfederateSsoEndpoint = pingfederateSsoEndpoint;
	}

	public String getPingfederateSsoPartnerUrl() {
		return pingfederateSsoPartnerUrl;
	}

	public void setPingfederateSsoPartnerUrl(String pingfederateSsoPartnerUrl) {
		this.pingfederateSsoPartnerUrl = pingfederateSsoPartnerUrl;
	}
	
	

}
