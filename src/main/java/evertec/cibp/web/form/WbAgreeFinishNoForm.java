package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.signup.SignupCampaign;
import evertec.cibp.web.session.UserSession;

public class WbAgreeFinishNoForm extends CancellableFormController {

	public WbAgreeFinishNoForm() {
		super();
		setCommandClass(SignupCampaign.class);
		setCommandName("wbaFinishno");
		setFormView("wbaFinishno");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		return new ModelAndView("redirect:portal");

	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		// UserSession userSession = (UserSession)
		// WebUtils.getSessionAttribute(request, "userSession");
		// String hidden = ServletRequestUtils.getStringParameter(request,
		// "actiontype");
		// SignupCampaign signupCampaign = new SignupCampaign();
		// signupCampaign.setCampaignDate(new Date());
		// signupCampaign.setCampaignResult('N');
		// signupCampaign.setProfileId(userSession.getCustomerProfile().getProfileId());
		// signupCampaign.setSsn(userSession.getCustomerProfile().getTaxId());
		// signupCampaign.setCampaignType(CampaignType.REGE.toString());
		// signupCampaign.setCan(userSession.getCustomerProfile().getEnrollmentAccountNumber());
		// userSession.getCustomerProfile().setSignupCampaign(signupCampaign);
		// userSession.getCustomerService().saveSignupCampInfo(userSession.getCustomerProfile());
		return model;
	}

}
