package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import com.evertecinc.www.PremiaPointsActivation.PointsCostCenter;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TVFrontendCreditAccount;
import evertec.cibp.core.models.account.TVFrontendDepositAccount;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.EStatementInteraction;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.ebill.Ebpp2BillerProductIdResponse;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyAllCustomerEmailsRequest;
import evertec.cibp.core.services.requests.PremiaPointsRequest;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.InternationalCharacterUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.EbppStatusUpdateBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.UserAccount;
import evertec.otherservices.requests.RegisterUserAccountReq;
import evertec.otherservices.requests.UpdateUserAccountReq;
import evertec.otherservices.responses.RegisterUserAccountResp;
import evertec.otherservices.responses.UpdateUserAccountResp;
import evertec.prophit.service.bean.CustomerInquiryBean;
import evertec.prophit.service.bean.EbillIndType;
import evertec.prophit.service.bean.MaintenceEbillBean;
import evertec.prophit.service.bean.MaintenceEbillResponse;

public class EbppStatusUpdate extends SimpleFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;
	
	// MBSFE-691
	private String invalidEmailDomains;
	// END MBSFE-691

	private CustomerInteractionEvent customerInteractionEvent;

	public EbppStatusUpdate() {
		super();
		setCommandClass(EbppStatusUpdateBean.class);
		setCommandName("ebppStatusUpdate");
		setFormView("ebppStatusUpdate");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		Map<String, Object> model = new HashMap<>(); //model result
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService(); //customer service
		if (this.messageSource != null && this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
				.equals(EntitlementStatus.ENABLED.toString())) {
			customerService.setEBPPStatus(true);
		}
		else {
			customerService.setEBPPStatus(false);
		}
			
		CustomerProfile profile = userSession.getCustomerProfile();
		AccountMaskUtil mask = new AccountMaskUtil();
		EbppStatusUpdateBean payeeBean = (EbppStatusUpdateBean) command;
		BankingSession bankingSession = userSession.getBankingSession();
		String insidePortal = (String) request.getParameter("showInsidePortal");
		model.put("showInsidePortal", insidePortal);
		String originalId = (String) request.getParameter("origId");
		model.put("origId", originalId);
		
		// The EBPP2 user id is not yet in session; add it
		if (!userSession.isEbpp2UserIdLookedUp()) {
			EbppEntitlement ent = null;
			if (profile.getEntitlementByType(EntitlementType.EBILLS) != null) {
				ent = new EbppEntitlement(profile.getEntitlementByType(EntitlementType.EBILLS));

				// If the username is blank, we need to look in the current EBPP
				// accounts ONLY
				if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
					userSession.setEbpp2UserId("");
				} else {
					userSession.setEbpp2UserId(ent.getUsername());
				}
				userSession.setEbpp2UserIdLookedUp(true);
			}
		}

		// The user has an EBPP2 ID
		if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

			if (payeeBean.getAction() != null
					&& payeeBean.getAction() == EbppStatusUpdateBean.ACTION.UPDATE_EBILL_STATUS) {//
				String p = payeeBean.getId();
				TVFrontEndPayee tvp = Utils.getPayeebyId(p, userSession.getPayees(true));
				
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS) && tvp != null
						&& Utils.getPayeeisCCA(tvp.getBillingAccount(), userSession)) {

					model.put("act", tvp);
					model.put("description", tvp.getNickname() + " " + tvp.getBillingAccount());
					model.put("nickname", tvp.getNickname());
					TVFrontendAccount tfa = Utils.getAccountByAccountNumber(tvp.getBillingAccount(), userSession.getAccounts());
					String acctNum = tfa.getAccountNumber();	
					
					// Update the customer email if necessary
					if (payeeBean.getEmail() != null && !payeeBean.getEmail().equals("")
							&& GenericValidator.isEmail(payeeBean.getEmail())
							&& !payeeBean.getEmail().equals(profile.getEmail())
							&& payeeBean.getEmail().equals(payeeBean.getEmailConfirm())) {

						ModifyAllCustomerEmailsRequest macer = new ModifyAllCustomerEmailsRequest(
								Utils.generateCustomerInteractionEvent(request), profile, 
								customerService, userSession.getAccounts(), bankingSession, 
								userSession.getEbpp2UserId(), userSession.getPreferredLanguage(), 
								payeeBean.getEmail()); 
						profile = CustomerUtils.modifyAllEmails(macer);
						userSession.setCustomerProfile(profile);
					}
					try {
						CustomerInquiryBean responseTsys = customerService.prophItService().makeCustomerInquiry(acctNum);
						if (responseTsys != null) {
							String ebillInd = "";
							if(responseTsys.getEbillInd().equals(EbillIndType.ACTIVE.toString())){
								ebillInd  = EbillIndType.DEACTIVE.toString();
							}else if(responseTsys.getEbillInd().equals(EbillIndType.ELIGIBLE.toString())){
								ebillInd = EbillIndType.ENROLL.toString();
							}else{
								//no eligible
							}
							MaintenceEbillBean bean = new MaintenceEbillBean();
							bean.setAcctNum(acctNum);
							bean.setCaseNumber(responseTsys.getCaseNumber());
							bean.setChannelId(responseTsys.getChannelId());
							bean.setEbillInd(ebillInd);
							bean.setImpersonateId(responseTsys.getImpersonateId());
							bean.setKeepCaseOpen(responseTsys.getKeepCaseOpen());
							bean.setNewContact(responseTsys.getNewContact());
							bean.setOrgId(responseTsys.getOrgId());
							bean.setOrgLevel(responseTsys.getOrgLevel());
							MaintenceEbillResponse responseMant = customerService.prophItService().maintenceEBill(bean);
							if(responseMant != null){
								if(responseMant.getMessage().indexOf("Successfull") != -1){
									if(ebillInd.equals(EbillIndType.DEACTIVE.toString())){
										if (Utils.getRSALanguage(request).equalsIgnoreCase("en")){
											userSession.setStatusmessage("Your e-Bills " + tvp.getNickname()
											+ " was deactivated.");
										}else{
											userSession.setStatusmessage("Has desactivado la opci&oacute;n de recibir e-Bills de " + tvp.getNickname());
										}
										customerService.sendEbillsCCAEmailDeactivationSuccess(
												userSession.getCustomerProfile(), PreferredLanguage.SPANISH,
												tfa.getNickname());
										// Premia Report
										PremiaPointsRequest premiaRequest = new PremiaPointsRequest(null,
												String.valueOf(profile.getPermId()), acctNum, profile.getTaxId(),
												"Eliminar Factura Electronica", "098", PointsCostCenter.INSFE,
												profile.getUsername(), "MiBanco", false);
										customerService.premiaPointsActivation(premiaRequest);
										customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"Account: " + tfa.getNickname() + " deactivated");
										customerService.logUpdateStatementStatus(customerInteractionEvent, InteractionSeverity.INFO);
										Utils.registerStatusEbillHistoryLog(request, acctNum, userSession, customerService,
												EStatementInteraction.DEACTIVATE_STATUS); //MBSE-3142
									}else{
										if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
											userSession
											.setStatusmessage("The option of receiving e-Bills has been activated for "
													+ tvp.getNickname());
										else
											userSession.setStatusmessage("Has activado la opci&oacute;n de recibir e-Bills de "
													+ tvp.getNickname());
										customerService.sendEbillsCCAEmailActivationSuccess(
												userSession.getCustomerProfile(), PreferredLanguage.SPANISH,
												tfa.getNickname());
										//Premia Report
										PremiaPointsRequest premiaRequest = new PremiaPointsRequest(null,
												String.valueOf(profile.getPermId()),acctNum, profile.getTaxId(),
												"Anadir Factura Electronica", "098", PointsCostCenter.INSFE, profile.getUsername(),"MiBanco", true);	
										customerService.premiaPointsActivation(premiaRequest);
										customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"Account: " + tfa.getNickname() + " activate.");
										customerService.logUpdateStatementStatus(customerInteractionEvent,
												InteractionSeverity.INFO);
										Utils.registerStatusEbillHistoryLog(request, acctNum, userSession, customerService,
												EStatementInteraction.ACTIVATE_STATUS); //MBSE-3142
									}									
								}else{
									if(ebillInd.equals(EbillIndType.DEACTIVE.toString())){
										if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
											userSession.setStatusmessage("Unable to deactivate your  e-Bill + "
													+ tvp.getNickname() + " at this time.");
										else
											userSession.setStatusmessage("Error desactivando tu e-Bill "
													+ tvp.getNickname() + " en este momento.");
										customerService.sendEbillsCCAEmailDeactivationFailure(
												userSession.getCustomerProfile(), PreferredLanguage.SPANISH,
												tfa.getNickname());
										customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"unable to deactivate, reason: " + responseMant.getMessage());
										customerService.logUpdateStatementStatus(customerInteractionEvent,
												InteractionSeverity.INFO);

									}else{
										if (Utils.getRSALanguage(request).equalsIgnoreCase("en")){
											userSession.setStatusmessage("Unable to activate your e-Bills"
													+ tvp.getNickname() + " at this time.");
										}else{
											userSession.setStatusmessage("Error activando tu e-Bills " + tvp.getNickname()
											+ " en este momento.");
										}
										customerService.sendEbillsCCAEmailActivationFailure(
												userSession.getCustomerProfile(), PreferredLanguage.SPANISH,
												tfa.getNickname());
										customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"unable to activate, reason: " + responseMant.getMessage());
										customerService.logUpdateStatementStatus(customerInteractionEvent,
												InteractionSeverity.INFO);

									}
								}	
							}
						}
					} catch (Exception e) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")){
							userSession.setStatusmessage("Unable to activate your eBill"
									+ tvp.getNickname() + " at this time.");
						}else{
							userSession.setStatusmessage("Error activando tu eBill " + tvp.getNickname()
							+ " en este momento.");
						}
					}
					
					model.put("completeprocess", true);
					model.put("act", tvp);
					return showForm(request, response, errors, model);
					
				} else			
				if (tvp != null) {
					model.put("act", tvp);
					model.put("description", tvp.getNickname() + " " + tvp.getBillingAccount());
					model.put("nickname", tvp.getNickname());

					// Get the list of user accounts
					List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

					// Loop through all the accounts and match them with the
					// proper userAccount
					if (ebpp2UserAccounts != null && ebpp2UserAccounts.size() > 0) {
						tvp.setEbpp2UserAccount(Utils.getEbpp2UserAccountsByAccount(tvp.getBillingAccount(),
								userSession));
					}

					// Active - Deactivate the account
					if (tvp != null && tvp.getEbpp2UserAccount() != null && tvp.getEbpp2UserAccount().isActive()
							&& tvp.getEbpp2UserAccount().getIsRegistered() == 1) {

						UpdateUserAccountReq req = new UpdateUserAccountReq();
						req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
						req.setBillerAccountID(tvp.getEbpp2UserAccount().getBillerAccountID());
						req.setLabel(tvp.getEbpp2UserAccount().getAccountLabel());
						req.setActive(false);
						req.setEbillNotif(tvp.getEbpp2UserAccount().isReceiveEbillNotification());
						req.setEstmNotif(tvp.getEbpp2UserAccount().isReceiveEstatementNotification());
						UpdateUserAccountResp resp = customerService.updateEbpp2UserAccount(req);

						if (resp.getUpdateResponse().equals("0")) {

							customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
									"Account: " + tvp.getEbpp2UserAccount().getAccountLabel() + "Deactivate");
							customerService.logUpdateEbillsStatus(customerInteractionEvent, InteractionSeverity.INFO);

							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								userSession.setStatusmessage("Your e-Bill " + tvp.getNickname() + " was deactivated.");
							else
								userSession.setStatusmessage("Has desactivado la opci&oacute;n de recibir e-Bills de "
										+ tvp.getNickname());

							// Reset the user account and user account summary
							// session variables
							userSession.resetEbpp2Accounts();
						}
						// FAILURE
						else {
							customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
							customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
									"Unable to desactivated, reason: " + resp.getResponseDescription());
							customerService.logUpdateEbillsStatus(customerInteractionEvent, InteractionSeverity.INFO);


							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								userSession.setStatusmessage("Unable to deactivate your e-Bill " + tvp.getNickname()
								+ " at this time.");
							else
								userSession.setStatusmessage("Error desactivando tu e-Bill " + tvp.getNickname()
								+ " en este momento.");
						}

						model.put("completeprocess", true);

						userSession.resetAccounts();
						userSession.resetPayees();
						return showForm(request, response, errors, model);
					}
					// Deactivated or not registered; activate the account
					else if (tvp != null
							&& (tvp.getEbpp2UserAccount() != null && !tvp.getEbpp2UserAccount().isActive())
							|| tvp.getEbpp2UserAccount() == null) {

						// Update the customer email if necessary
						if (payeeBean.getEmail() != null && !payeeBean.getEmail().equals("")
								&& GenericValidator.isEmail(payeeBean.getEmail())
								&& !payeeBean.getEmail().equals(profile.getEmail())
								&& payeeBean.getEmail().equals(payeeBean.getEmailConfirm())) {

							ModifyAllCustomerEmailsRequest macer = new ModifyAllCustomerEmailsRequest(
									Utils.generateCustomerInteractionEvent(request), profile, 
									customerService, userSession.getAccounts(), bankingSession, 
									userSession.getEbpp2UserId(), userSession.getPreferredLanguage(), 
									payeeBean.getEmail()); 
							profile = CustomerUtils.modifyAllEmails(macer);
							userSession.setCustomerProfile(profile);
						}

						// If the account already exists and it is not active,
						// activate it
						if (tvp.getEbpp2UserAccount() != null && !tvp.getEbpp2UserAccount().isActive()) {

							UpdateUserAccountReq req = new UpdateUserAccountReq();
							req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
							req.setBillerAccountID(tvp.getEbpp2UserAccount().getBillerAccountID());
							req.setLabel(tvp.getEbpp2UserAccount().getAccountLabel());
							req.setActive(true);
							req.setEbillNotif(tvp.getEbpp2UserAccount().isReceiveEbillNotification());
							req.setEstmNotif(tvp.getEbpp2UserAccount().isReceiveEstatementNotification());
							UpdateUserAccountResp resp = customerService.updateEbpp2UserAccount(req);




							if (resp.getUpdateResponse().equals("0")) {

								customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
								customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
										"Account: " + tvp.getEbpp2UserAccount().getAccountLabel() + " activate.");
								customerService.logUpdateEbillsStatus(customerInteractionEvent,
										InteractionSeverity.INFO);

								if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
									userSession
									.setStatusmessage("The option of receiving e-Bills has been activated for "
											+ tvp.getNickname());
								else
									userSession.setStatusmessage("Has activado la opci&oacute;n de recibir e-Bills de "
											+ tvp.getNickname());

								// Reset the user account and user account
								// summary session variables
								userSession.resetEbpp2Accounts();
							}
							// FAILURE
							else {
								customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
								customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
										"Unable to activate, reason: " + resp.getResponseDescription());
								customerService.logUpdateEbillsStatus(customerInteractionEvent,
										InteractionSeverity.INFO);

								if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
									userSession.setStatusmessage("Unable to activate your e-Bill " + tvp.getNickname()
									+ " at this time.");
								else
									userSession.setStatusmessage("Error activando tu e-Bill " + tvp.getNickname()
									+ " en este momento.");
							}
						}
						// We are not registered; add the account
						else {

							RegisterUserAccountReq req = new RegisterUserAccountReq();
							int billerProductId = 0;
							int billerId = 0;
							Ebpp2BillerProductIdResponse ebpp2BillerProductIdResponse = null;

							// EBPP2 FIX
							// If the account is a Visa card, try to find it in
							// the user's accounts
							if (tvp.getGlobalPayeeId() == 359) {

								ebpp2BillerProductIdResponse = new Ebpp2BillerProductIdResponse();
								ebpp2BillerProductIdResponse.setBillerId(1);
								TVFrontendAccount tva = Utils.getAccountByAccountNumber(tvp.getBillingAccount(),
										userSession.getAccounts());

								// The account exists - set up its payee id
								if (tva != null && tva.getSubtype().equals("CCA")) {

									// Corporate cards start with 454903
									if (tva.getAccountNumber().startsWith("454903"))
										ebpp2BillerProductIdResponse.setBillerProductId(59);
									// Business cards start with 454925
									else if (tva.getAccountNumber().startsWith("454925"))
										ebpp2BillerProductIdResponse.setBillerProductId(60);
									// VISA Advantage Business = 61
									else if (tva.getProductDefinition().getProductId().equals("VBABS"))
										ebpp2BillerProductIdResponse.setBillerProductId(61);
									// All others are individual cards
									else
										ebpp2BillerProductIdResponse.setBillerProductId(55);
								}
								// The account does not exist - go with the
								// generic validation
								else {
									if (tvp.getBillingAccount().startsWith("454903"))
										ebpp2BillerProductIdResponse.setBillerProductId(59);
									// Business cards start with 454925
									else if (tvp.getBillingAccount().startsWith("454925"))
										ebpp2BillerProductIdResponse.setBillerProductId(60);
									else
										ebpp2BillerProductIdResponse.setBillerProductId(55);
								}
							} else {
								ebpp2BillerProductIdResponse = customerService
										.getEbpp2BillerProductIdInformation(Integer.toString(tvp.getGlobalPayeeId()));
							}

							if (ebpp2BillerProductIdResponse != null) {

								billerProductId = ebpp2BillerProductIdResponse.getBillerProductId();
								billerId = ebpp2BillerProductIdResponse.getBillerId();

								// Do we have a valid biller product ID?
								if (billerProductId > 0 && billerId > 0) {

									req.setAccountNumber(tvp.getBillingAccount());
									req.setBillerId(billerId);
									req.setBillerProductId(billerProductId);
									req.setLabel(tvp.getNickname());
									req.setNss(profile.getTaxId());
									req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
									req.setZip(payeeBean.getZipCode() == null ? "00000" : payeeBean.getZipCode());

									// Register the account
									RegisterUserAccountResp resp = customerService.registerEbpp2UserAccount(req);

									// SUCCESS
									if (resp.getRegisterResponse() != null && resp.getRegisterResponse().equals("0")) {

										// Set this as a frequent payee
										if (!tvp.getFavorite()) {
											customerService.modifyPayeeFrequentPreference(profile, bankingSession, tvp,
													true);
											tvp.getCustomerPayeePreferences().setFavorite(true);
											userSession.resetFavoritePayments();
										}

										customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"Account: " + tvp.getNickname() + " activate.");
										customerService.logUpdateEbillsStatus(customerInteractionEvent,
												InteractionSeverity.INFO);

										if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
											userSession
											.setStatusmessage("The option of receiving e-Bills has been activated for "
													+ tvp.getNickname());
										else
											userSession
											.setStatusmessage("Has activado la opci&oacute;n de recibir e-Bills de "
													+ tvp.getNickname());

										// Reset the user account and user
										// account summary session variabled
										userSession.resetEbpp2Accounts();
									}
									// FAILURE
									else {

										customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"Unable to activate, reason: " + resp.getResponseDescription());
										customerService.logUpdateEbillsStatus(customerInteractionEvent,
												InteractionSeverity.INFO);

										if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
											userSession.setStatusmessage("Unable to activate your e-Bill "
													+ tvp.getNickname() + " at this time.");
										else
											userSession.setStatusmessage("Error activando tu e-Bill "
													+ tvp.getNickname() + " en este momento.");
									}
								}
								// FAILURE
								else {
									customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
									customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
											"Unable to activate, reason: We don't have a valid biller product ID. (billerProductId, billerId)");
									customerService.logUpdateEbillsStatus(customerInteractionEvent,
											InteractionSeverity.INFO);

									if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
										userSession.setStatusmessage("Unable to activate your e-Bill "
												+ tvp.getNickname() + " at this time.");
									else
										userSession.setStatusmessage("Error activando tu e-Bill " + tvp.getNickname()
										+ " en este momento.");
								}
							}
						}

						model.put("completeprocess", true);
						userSession.resetAccounts();
						userSession.resetPayees();
						return showForm(request, response, errors, model);

					}
				}//End
			} else if (payeeBean.getAction() != null
					&& payeeBean.getAction() == EbppStatusUpdateBean.ACTION.UPDATE_ESTMT_STATUS) {
				String p = payeeBean.getId();

				TVFrontendAccount tvp = Utils.getAccountById(p, userSession.getAccounts());
				if (tvp != null) {
					model.put("act", tvp);
					model.put("description", tvp.getNickname() + " " + tvp.getAccountNumber());
					model.put("nickname", tvp.getNickname() + " " + mask.mask(tvp.getAccountNumber()));
				}

				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS) && tvp.getSubtype() != null
						&& tvp.getSubtype().equals("IDA")) {
					boolean IDAStatusChange = false;
					TVFrontendDepositAccount idaAccount = Utils.getIdaAccountById(p, userSession.getIdaAccounts());

					//account is active - deactivate it
					if(idaAccount.getStmtDisposition().equalsIgnoreCase("E") || idaAccount.getStmtDisposition().equalsIgnoreCase("V")){

						IDAStatusChange = customerService.deactivateEstatementIDAeBills(bankingSession,idaAccount.getAccountNumber(),idaAccount.getIdaAcctInq());

						if (IDAStatusChange){
							customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
							customerInteractionEvent.setCustomStringData("IDA accountNumber: ",
									idaAccount.getAccountNumber());
							customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
									"Account: " + idaAccount.getAccountNumber() + " deactivated");
							customerService.logUpdateStatementStatus(customerInteractionEvent,
									InteractionSeverity.INFO);

							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								userSession.setStatusmessage("Your eStatements " + tvp.getNickname()
								+ " was deactivated.");
							else
								userSession.setStatusmessage("Has desactivado la opci&oacute;n de recibir eStatements de " + tvp.getNickname());
							//do I have to reset the accounts?		
							//Premia Report
							PremiaPointsRequest premiaRequest = new PremiaPointsRequest(null,
									String.valueOf(profile.getPermId()),idaAccount.getAccountNumber(), profile.getTaxId(),
									"Desactivacion de estatement", "098", PointsCostCenter.DESTMT, profile.getUsername(),"MiBanco", false);	
							customerService.premiaPointsActivation(premiaRequest);
						}
						else{
							customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
							customerInteractionEvent.setCustomStringData("IDA accountNumber: ",
									idaAccount.getAccountNumber());
							customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
									"Unable to deactivate");
							customerService.logUpdateStatementStatus(customerInteractionEvent,
									InteractionSeverity.INFO);

							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								userSession.setStatusmessage("Unable to deactivate your eStatement " + tvp.getNickname() + " at this time.");
							else
								userSession.setStatusmessage("Error desactivando tu eStatement "
										+ tvp.getNickname() + " en este momento.");
						}

					model.put("completeprocess", true);
					userSession.resetAccounts();
					userSession.resetPayees();
					userSession.resetIdaAccount();
					tvp = Utils.getAccountByAccountNumber(tvp.getAccountNumber(), userSession.getAccounts());
					idaAccount = Utils.getIdaAccountById(p, userSession.getIdaAccounts());
					model.put("act", tvp);
					model.put("idaAccount", idaAccount);
					return showForm(request, response, errors, model);

					}
					//Account isn't active - activate it
					else if(idaAccount.getStmtDisposition().equalsIgnoreCase("M") || idaAccount.getStmtDisposition().equalsIgnoreCase("S")){

						// Update the customer email if necessary
						if (payeeBean.getEmail() != null && !payeeBean.getEmail().equals("")
								&& GenericValidator.isEmail(payeeBean.getEmail())
								&& !payeeBean.getEmail().equals(profile.getEmail())
								&& payeeBean.getEmail().equals(payeeBean.getEmailConfirm())) {

							ModifyAllCustomerEmailsRequest macer = new ModifyAllCustomerEmailsRequest(
									Utils.generateCustomerInteractionEvent(request), profile, 
									customerService, userSession.getAccounts(), bankingSession, 
									userSession.getEbpp2UserId(), userSession.getPreferredLanguage(), 
									payeeBean.getEmail()); 
							profile = CustomerUtils.modifyAllEmails(macer);
							userSession.setCustomerProfile(profile);
						}

						IDAStatusChange = customerService.activateEstatementIDAeBills(bankingSession,idaAccount.getAccountNumber(),idaAccount.getIdaAcctInq());

						if(IDAStatusChange){
							customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
							customerInteractionEvent.setCustomStringData("IDA accountNumber: ",
									idaAccount.getAccountNumber());
							customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
									"Account: " + idaAccount.getAccountNumber() + " activate.");
							customerService.logUpdateStatementStatus(customerInteractionEvent,
									InteractionSeverity.INFO);

							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								userSession.setStatusmessage("The option of receiving eStatements has been activated for " + tvp.getNickname());
							else
								userSession.setStatusmessage("Has activado la opci&oacute;n de recibir eStatements de " + tvp.getNickname());
							
							PremiaPointsRequest premiaRequest = new PremiaPointsRequest(null,
									String.valueOf(profile.getPermId()),idaAccount.getAccountNumber(), profile.getTaxId(),
									"Activacion de estatement", "098", PointsCostCenter.DESTMT, profile.getUsername(),"MiBanco", true);	
							customerService.premiaPointsActivation(premiaRequest);
						}
						else{
							customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
							customerInteractionEvent.setCustomStringData("IDA accountNumber: ",
									idaAccount.getAccountNumber());
							// validate if the inquiry carries the STATUSof the
							// activation, message status
							customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
									"Unable to activate account:" + idaAccount.getAccountNumber());
							customerService.logUpdateStatementStatus(customerInteractionEvent,
									InteractionSeverity.INFO);

							String lang = Utils.getRSALanguage(request);
							String language = "sp".equalsIgnoreCase(lang) ? "es" : lang;
							Locale locale = new Locale(language);

							final String errorMessageEstamentpartOne = messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.one", null, locale);
							final String errorMessageEstamentpartTwo = messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.two", null, locale);

							userSession.setStatusmessage(errorMessageEstamentpartOne + tvp.getNickname() + errorMessageEstamentpartTwo);

						}
					model.put("completeprocess", true);
					userSession.resetAccounts();
					userSession.resetPayees();
					userSession.resetIdaAccount();
					tvp = Utils.getAccountByAccountNumber(tvp.getAccountNumber(), userSession.getAccounts());
					model.put("act", tvp);
					return showForm(request, response, errors, model);
					}
				}else if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS) && tvp.getSubtype().equals("CCA") ){
					
					String acctNum = tvp.getAccountNumber();
					try {
						CustomerInquiryBean responseTsys = customerService.prophItService()
								.makeCustomerInquiry(acctNum); //call to tsys customer inquiry to get ebill ind
						if (responseTsys != null) {
							String ebillInd = ""; //ebill ind
							if(responseTsys.getEbillInd().equals(EbillIndType.ACTIVE.toString())){
								ebillInd  = EbillIndType.DEACTIVE.toString();
							}else if(responseTsys.getEbillInd().equals(EbillIndType.ELIGIBLE.toString())){
								ebillInd = EbillIndType.ENROLL.toString();
							}else{
								//no eligible
							}
							MaintenceEbillBean bean = new MaintenceEbillBean();
							bean.setAcctNum(acctNum);
							bean.setCaseNumber(responseTsys.getCaseNumber());
							bean.setChannelId(responseTsys.getChannelId());
							bean.setEbillInd(ebillInd);
							bean.setImpersonateId(responseTsys.getImpersonateId());
							bean.setKeepCaseOpen(responseTsys.getKeepCaseOpen());
							bean.setNewContact(responseTsys.getNewContact());
							bean.setOrgId(responseTsys.getOrgId());
							bean.setOrgLevel(responseTsys.getOrgLevel());
							MaintenceEbillResponse responseMant = customerService.prophItService().maintenceEBill(bean);
							if(responseMant != null){
								if(responseMant.getMessage().indexOf("Successfull") != -1){

									if(ebillInd.equals(EbillIndType.DEACTIVE.toString())){
										if (Utils.getRSALanguage(request).equalsIgnoreCase("en")){
											userSession.setStatusmessage("Your eBill " + tvp.getNickname()
											+ " was deactivated.");
										}else{
											userSession.setStatusmessage("Has desactivado la opci&oacute;n de recibir eBills de " + tvp.getNickname());
										}
										customerService.sendEbillsCCAEmailDeactivationSuccess(
												userSession.getCustomerProfile(), PreferredLanguage.SPANISH,
												tvp.getNickname());
										//Premia Report
										PremiaPointsRequest premiaRequest = new PremiaPointsRequest(null,
												String.valueOf(profile.getPermId()),acctNum, profile.getTaxId(),
												"Eliminar Factura Electronica", "098", PointsCostCenter.INSFE, profile.getUsername(),"MiBanco", false);	
										customerService.premiaPointsActivation(premiaRequest);
										customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"Account: " + tvp.getNickname() + " deactivated");
										customerService.logUpdateStatementStatus(customerInteractionEvent,
												InteractionSeverity.INFO);
										Utils.registerStatusEbillHistoryLog(request, acctNum, userSession, customerService,
												EStatementInteraction.DEACTIVATE_STATUS); //MBSE-3142

									}else{
										if (Utils.getRSALanguage(request).equalsIgnoreCase("en")){
											userSession
											.setStatusmessage("The option of receiving e-Bills has been activated for "
													+ tvp.getNickname());
										}else{
											userSession.setStatusmessage("Has activado la opci&oacute;n de recibir e-Bills de "
													+ tvp.getNickname());
										}
										
										// Update the customer email if necessary
										if (payeeBean.getEmail() != null && !payeeBean.getEmail().equals("")
												&& GenericValidator.isEmail(payeeBean.getEmail())
												&& !payeeBean.getEmail().equals(profile.getEmail())
												&& payeeBean.getEmail().equals(payeeBean.getEmailConfirm())) {

											ModifyAllCustomerEmailsRequest macer = new ModifyAllCustomerEmailsRequest(
													Utils.generateCustomerInteractionEvent(request), profile, 
													customerService, userSession.getAccounts(), bankingSession, 
													userSession.getEbpp2UserId(), userSession.getPreferredLanguage(), 
													payeeBean.getEmail()); 
											profile = CustomerUtils.modifyAllEmails(macer);
											userSession.setCustomerProfile(profile);
										}
										
										customerService.sendEbillsCCAEmailActivationSuccess(
												userSession.getCustomerProfile(), PreferredLanguage.SPANISH,
												tvp.getNickname());
										//Premia Report
										PremiaPointsRequest premiaRequest = new PremiaPointsRequest(null,
												String.valueOf(profile.getPermId()), acctNum, profile.getTaxId(),
												"Anadir factura electronica", "098", PointsCostCenter.INSFE,
												profile.getUsername(), "MiBanco", true);
										customerService.premiaPointsActivation(premiaRequest);
										customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"Account: " + tvp.getNickname() + " activate.");
										customerService.logUpdateStatementStatus(customerInteractionEvent,
												InteractionSeverity.INFO);
										Utils.registerStatusEbillHistoryLog(request, acctNum, userSession, customerService,
												EStatementInteraction.ACTIVATE_STATUS); //MBSE-3142
									}									
								}else{
									if(ebillInd.equals(EbillIndType.DEACTIVE.toString())){
										if (Utils.getRSALanguage(request).equalsIgnoreCase("en")){
											userSession.setStatusmessage("Unable to deactivate your e-Bill "
													+ tvp.getNickname() + " at this time.");
										}else{
											userSession.setStatusmessage("Error desactivando tu e-Bill "
													+ tvp.getNickname() + " en este momento.");
										}
										customerService.sendEbillsCCAEmailDeactivationFailure(
												userSession.getCustomerProfile(), PreferredLanguage.SPANISH,
												tvp.getNickname());
										customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"unable to deactivate, reason: " + responseMant.getMessage());
										customerService.logUpdateStatementStatus(customerInteractionEvent,
												InteractionSeverity.INFO);

									}else{
										if (Utils.getRSALanguage(request).equalsIgnoreCase("en")){
											userSession.setStatusmessage("Unable to activate your eStatements"
													+ tvp.getNickname() + " at this time.");
										}else{
											userSession.setStatusmessage("Error activando tu e-Statement " + tvp.getNickname()
											+ " en este momento.");
										}
										customerService.sendEbillsCCAEmailActivationFailure(
												userSession.getCustomerProfile(), PreferredLanguage.SPANISH,
												tvp.getNickname());
										customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"unable to activate, reason: " + responseMant.getMessage());
										customerService.logUpdateStatementStatus(customerInteractionEvent,
												InteractionSeverity.INFO);

									}									
								}	
							}
						}
					} catch (Exception e) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")){
							userSession.setStatusmessage("Unable to activate your eBill"
									+ tvp.getNickname() + " at this time.");
						}else{
							userSession.setStatusmessage("Error activando tu eBill " + tvp.getNickname()
							+ " en este momento.");
						}
					}
					
					model.put("completeprocess", true);
					model.put("act", tvp);
					return showForm(request, response, errors, model);
				}
				
				else{

					// Get the list of user accounts
					List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

					// Loop through all the accounts and match them with the proper
					// userAccount
					if (ebpp2UserAccounts != null && ebpp2UserAccounts.size() > 0) {
						tvp.setEbpp2UserAccount(Utils.getEbpp2UserAccountsByAccount(tvp.getAccountNumber(), userSession));
					}

					// Active - Deactivate the account
					if (tvp != null && tvp.getEbpp2UserAccount() != null && tvp.getEbpp2UserAccount().isActive()
							&& tvp.getEbpp2UserAccount().getIsRegistered() == 1) {

						// If this is an e-Statement, we need to deactivate it in
						// IDA
						boolean eStatement = !tvp.getSubtype().equals("CCA");
						String option = (tvp.getSubtype().equals("CCA")) ? "e-Bill" : "e-Statement";
						boolean IDADeactivationSuccess = true;

						if (eStatement) {
							IDADeactivationSuccess = customerService.deactivateEbpp2AccountInIDA(bankingSession, tvp);						
						}

						if (IDADeactivationSuccess) {

							UpdateUserAccountReq req = new UpdateUserAccountReq();
							req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
							req.setBillerAccountID(tvp.getEbpp2UserAccount().getBillerAccountID());
							req.setLabel(tvp.getEbpp2UserAccount().getAccountLabel());
							req.setActive(false);
							req.setEbillNotif(tvp.getEbpp2UserAccount().isReceiveEbillNotification());
							req.setEstmNotif(tvp.getEbpp2UserAccount().isReceiveEstatementNotification());
							UpdateUserAccountResp resp = customerService.updateEbpp2UserAccount(req);

							if (resp.getUpdateResponse().equals("0")) {

								customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
								customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
										"Account: " + tvp.getEbpp2UserAccount().getAccountLabel() + " deactivated");
								customerService.logUpdateStatementStatus(customerInteractionEvent,
										InteractionSeverity.INFO);

								if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
									userSession.setStatusmessage("Your " + option + " " + tvp.getNickname()
									+ " was deactivated.");
								else
									userSession.setStatusmessage("Has desactivado la opci&oacute;n de recibir " + option
											+ "s de " + tvp.getNickname());

								// Reset the user account and user account summary
								// session variabled
								userSession.resetEbpp2Accounts();
							}
							// FAILURE
							else {

								customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
								customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
										"Unable to deactivate, reason: " + resp.getResponseDescription());
								customerService.logUpdateStatementStatus(customerInteractionEvent,
										InteractionSeverity.INFO);

								if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
									userSession.setStatusmessage("Unable to deactivate your " + option + " "
											+ tvp.getNickname() + " at this time.");
								else
									userSession.setStatusmessage("Error desactivando tu " + option + " "
											+ tvp.getNickname() + " en este momento.");
							}
						}
						// FAILURE - We need to have IDA and EBPP2 synchronized
						else {

							customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
							customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
									"Unable to desactivate, note: We need to have IDA and EBPP2 synchronized");
							customerService.logUpdateStatementStatus(customerInteractionEvent,
									InteractionSeverity.INFO);

							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								userSession.setStatusmessage("Unable to deactivate your " + option + " "
										+ tvp.getNickname() + " at this time.");
							else
								userSession.setStatusmessage("Error desactivando tu " + option + " " + tvp.getNickname()
								+ " en este momento.");
						}

						model.put("completeprocess", true);
						userSession.resetAccounts();
						userSession.resetPayees();
						tvp = Utils.getAccountByAccountNumber(tvp.getAccountNumber(), userSession.getAccounts());
						model.put("act", tvp);
						return showForm(request, response, errors, model);

					}
					// Deactivated or not registered; activate the account
					else if (tvp != null && (tvp.getEbpp2UserAccount() != null && !tvp.getEbpp2UserAccount().isActive())
							|| tvp.getEbpp2UserAccount() == null) {

						// Update the customer email if necessary
						if (payeeBean.getEmail() != null && !payeeBean.getEmail().equals("")
								&& GenericValidator.isEmail(payeeBean.getEmail())
								&& !payeeBean.getEmail().equals(profile.getEmail())
								&& payeeBean.getEmail().equals(payeeBean.getEmailConfirm())) {

							ModifyAllCustomerEmailsRequest macer = new ModifyAllCustomerEmailsRequest(
									Utils.generateCustomerInteractionEvent(request), profile, 
									customerService, userSession.getAccounts(), bankingSession, 
									userSession.getEbpp2UserId(), userSession.getPreferredLanguage(), 
									payeeBean.getEmail()); 
							profile = CustomerUtils.modifyAllEmails(macer);
							userSession.setCustomerProfile(profile);
						}

						// Show the correct message
						String option = (tvp.getSubtype().equals("CCA")) ? "e-Bill" : "e-Statement";

						// If the account already exists and it is not active,
						// activate it
						if (tvp.getEbpp2UserAccount() != null && !tvp.getEbpp2UserAccount().isActive()) {

							boolean eStatement = !tvp.getSubtype().equals("CCA");
							boolean continueProcess = true;

							if (eStatement) {
								continueProcess = customerService.activateEbpp2AccountInIDA(bankingSession, tvp);
							}

							if (continueProcess) {
								UpdateUserAccountReq req = new UpdateUserAccountReq();
								req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
								req.setBillerAccountID(tvp.getEbpp2UserAccount().getBillerAccountID());
								req.setLabel(tvp.getEbpp2UserAccount().getAccountLabel());
								req.setActive(true);
								req.setEbillNotif(tvp.getEbpp2UserAccount().isReceiveEbillNotification());
								req.setEstmNotif(tvp.getEbpp2UserAccount().isReceiveEstatementNotification());
								UpdateUserAccountResp resp = customerService.updateEbpp2UserAccount(req);

								if (resp.getUpdateResponse().equals("0")) {

									customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
									customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
											"Account: " + tvp.getEbpp2UserAccount().getAccountLabel() + " activate.");
									customerService.logUpdateStatementStatus(customerInteractionEvent,
											InteractionSeverity.INFO);

									if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
										userSession.setStatusmessage("The option of receiving " + option
												+ "s has been activated for " + tvp.getNickname());
									else
										userSession.setStatusmessage("Has activado la opci&oacute;n de recibir " + option
												+ "s de " + tvp.getNickname());

									// Reset the user account and user account
									// summary
									// session variables
									userSession.resetEbpp2Accounts();
								}
								// FAILURE
								else {
									customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
									customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
											"Unable to activate, reason: " + resp.getResponseDescription());
									customerService.logUpdateStatementStatus(customerInteractionEvent,
											InteractionSeverity.INFO);

									if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
										userSession.setStatusmessage("Unable to activate your " + option + " "
												+ tvp.getNickname() + " at this time.");
									else
										userSession.setStatusmessage("Error activando tu e-Statement " + tvp.getNickname()
										+ " en este momento.");
								}
							}
						}
						// We are not registered; add the account
						else {

							boolean eStatement = false;
							RegisterUserAccountReq req = new RegisterUserAccountReq();
							int billerProductId = 0;

							// Credit cards
							if (tvp.getSubtype().equals("CCA")) {

								AbstractAccount accountDetails = customerService.getAccountDetails(profile, bankingSession,
										tvp, true);
								TVFrontendCreditAccount frontendCreditAccount = (TVFrontendCreditAccount) accountDetails;
								if (frontendCreditAccount.getProductIdBCO() != null
										&& !frontendCreditAccount.getProductIdBCO().equals(""))
									billerProductId = Integer.parseInt(frontendCreditAccount.getProductIdBCO());
								else {
									// Corporate cards start with 454903
									if (tvp.getAccountNumber().startsWith("454903"))
										billerProductId = 59;
									// Business cards start with 454925
									else if (tvp.getAccountNumber().startsWith("454925"))
										billerProductId = 60;
									// VISA Advantage Business = 61
									else if (tvp.getProductDefinition().getProductId().equals("VBABS"))
										billerProductId = 61;
									// All others are individual cards
									else
										billerProductId = 55;
								}

							}
							// Deposit accounts
							else {

								eStatement = true;
								if (tvp.getAccountProductId() != null) {
									Ebpp2BillerProductIdResponse ebpp2BillerProductIdResponse = customerService
											.getEbpp2BillerProductIdInformationDepositAccount(tvp);

									if (ebpp2BillerProductIdResponse != null)
										billerProductId = ebpp2BillerProductIdResponse.getBillerProductId();
								}
							}
							// Do we have a valid biller product ID?
							if (billerProductId > 0) {

								// If this is an estatement, activate it in IDA
								boolean continueProcess = true;
								if (eStatement) {
									continueProcess = customerService.activateEbpp2AccountInIDA(bankingSession, tvp);
								}

								if (continueProcess) {
									req.setAccountNumber(tvp.getAccountNumber());
									req.setBillerId(1);
									req.setBillerProductId(billerProductId);
									req.setLabel(tvp.getNickname());
									req.setNss(profile.getTaxId());
									req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
									req.setZip("00000");

									// Register the account
									RegisterUserAccountResp resp = customerService.registerEbpp2UserAccount(req);

									// SUCCESS
									if (resp.getRegisterResponse() != null && resp.getRegisterResponse().equals("0")) {
										customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"Account: " + tvp.getNickname() + " activate.");
										customerService.logUpdateStatementStatus(customerInteractionEvent,
												InteractionSeverity.INFO);

										if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
											userSession.setStatusmessage("The option of receiving " + option
													+ "s has been activated for " + tvp.getNickname());
										else
											userSession.setStatusmessage("Has activado la opci&oacute;n de recibir "
													+ option + "s de " + tvp.getNickname());

										// Reset the user account and user account
										// summary session variabled
										userSession.resetEbpp2Accounts();
									}
									// FAILURE
									else {

										customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"Unable to activate, reason: " + resp.getResponseDescription());
										customerService.logUpdateStatementStatus(customerInteractionEvent,
												InteractionSeverity.INFO);

										if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
											userSession.setStatusmessage("Unable to activate your " + option + " "
													+ tvp.getNickname() + " at this time.");
										else
											userSession.setStatusmessage("Error activando tu " + option + " "
													+ tvp.getNickname() + " en este momento.");
									}
								}
								// FAILURE - We need to have IDA and EBPP2
								// synchronized
								else {
									customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
									customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
											"unable to activate, note: We need to have IDA and EBPP2 synchronized");
									customerService.logUpdateStatementStatus(customerInteractionEvent,
											InteractionSeverity.INFO);

									if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
										userSession.setStatusmessage("Unable to activate your e-Statement "
												+ tvp.getNickname() + " at this time.");
									else
										userSession.setStatusmessage("Error activando tu e-Statement " + tvp.getNickname()
										+ " en este momento.");
								}
							}
						}
						// CIBP-2771 register the account as payee only if is a cca
						if (tvp.getSubtype().equals("CCA")) {
							GlobalPayee global = null;
							if (tvp.getAccountNumber().startsWith("03"))// means
								// amex
								// 03778
								global = PaymentsFacade.getInstance().getGlobalPayeeById(1983, userSession);
							else if (tvp.getAccountNumber().startsWith("4")) // visa
								// 4549.....
								global = PaymentsFacade.getInstance().getGlobalPayeeById(359, userSession);
							else if (tvp.getAccountNumber().startsWith("5") && !tvp.getAccountNumber().startsWith("50"))// mastercard
								// 5310
								// 53100585
								// 546620
								global = PaymentsFacade.getInstance().getGlobalPayeeById(223, userSession);

							if (global != null) {
								boolean isHighRiskPayee = false;
								if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1701)) {
									PaymentsFacade.getInstance().getGlobalPayeePreference(global, userSession);

									if(global.getPreference() != null)
										 isHighRiskPayee = global.getPreference().getHighRisk();
								}
								boolean resPayee = customerService.addCustomerPayeeWithoutEbill(profile, bankingSession,
										global, tvp.getAccountNumber(), tvp.getNickname(),
										Utils.generateCustomerInteractionEvent(request), null, null, null, null, false, isHighRiskPayee);
								if (resPayee) {
									System.out.println("added payee");
								}
							}
						}
						// End CIBP-2771
						model.put("completeprocess", true);
						userSession.resetAccounts();
						userSession.resetPayees();
						tvp = Utils.getAccountByAccountNumber(tvp.getAccountNumber(), userSession.getAccounts());
						model.put("act", tvp);
						return showForm(request, response, errors, model);
					}
				}
			} else if (payeeBean.getAction() != null
					&& payeeBean.getAction() == EbppStatusUpdateBean.ACTION.ALGARO_UPDATE) {
				String p = payeeBean.getId();

				// Find this e-Bill in the list
				List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();
				UserAccount algaroAccount = null;

				// Loop through all the accounts and match them with the proper
				// userAccount
				if (ebpp2UserAccounts != null && ebpp2UserAccounts.size() > 0) {
					for (Iterator<UserAccount> iterator2 = ebpp2UserAccounts.iterator(); iterator2.hasNext();) {
						UserAccount userAccount = (UserAccount) iterator2.next();

						if (userAccount.getAccountNumber().replaceFirst("^0+(?!$)", "")
								.equals(p.replaceFirst("^0+(?!$)", ""))) {

							algaroAccount = userAccount;
							break;
						}
					}
				}

				if (algaroAccount != null) {
					// Active - Deactivate the account
					if (algaroAccount.isActive() && algaroAccount.getIsRegistered() == 1) {

						UpdateUserAccountReq req = new UpdateUserAccountReq();
						req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
						req.setBillerAccountID(algaroAccount.getBillerAccountID());
						req.setLabel(algaroAccount.getAccountLabel());
						req.setActive(false);
						req.setEbillNotif(algaroAccount.isReceiveEbillNotification());
						req.setEstmNotif(algaroAccount.isReceiveEstatementNotification());
						UpdateUserAccountResp resp = customerService.updateEbpp2UserAccount(req);

						if (resp.getUpdateResponse().equals("0")) {
							customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
							customerInteractionEvent.setCustomStringData("algaro", "Algaro account");
							customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
									"ebill deactivated");
							customerService.logUpdateStatementStatus(customerInteractionEvent,
									InteractionSeverity.INFO);

							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								userSession.setStatusmessage("Your e-Bill " + algaroAccount.getAccountLabel()
								+ " was deactivated.");
							else
								userSession.setStatusmessage("Has desactivado la opci&oacute;n de recibir e-Bills de "
										+ algaroAccount.getAccountLabel());

							// Reset the user account and user account summary
							// session variables
							userSession.resetEbpp2Accounts();
						}
						// FAILURE
						else {
							customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
							customerInteractionEvent.setCustomStringData("algaro", "Algaro account");
							customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
									"unable to deactivate ebill");
							customerService.logUpdateStatementStatus(customerInteractionEvent,
									InteractionSeverity.INFO);

							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								userSession.setStatusmessage("Unable to deactivate your e-Bill "
										+ algaroAccount.getAccountLabel() + " at this time.");
							else
								userSession.setStatusmessage("Error desactivando tu e-Bill "
										+ algaroAccount.getAccountLabel() + " en este momento.");
						}
					} else {
						customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
						customerInteractionEvent.setCustomStringData("algaro", "Algaro account");
						customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
								"unable to deactivate ebill");
						customerService.logUpdateStatementStatus(customerInteractionEvent, InteractionSeverity.INFO);

						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession.setStatusmessage("Unable to deactivate your e-Bill at this time.");
						else
							userSession.setStatusmessage("Error desactivando tu e-Bill en este momento.");
					}
				} else {
					customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
					customerInteractionEvent.setCustomStringData("algaro", "Algaro account");
					customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
							"unable to deactivate ebill");
					customerService.logUpdateStatementStatus(customerInteractionEvent, InteractionSeverity.INFO);

					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Unable to deactivate your e-Bill at this time.");
					else
						userSession.setStatusmessage("Error desactivando tu e-Bill en este momento.");
				}
			}

			model.put("completeprocess", true);
			userSession.resetAccounts();
			userSession.resetPayees();
			return showForm(request, response, errors, model);
		}

		return showForm(request, errors, "ebppStatusUpdate", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		EbppStatusUpdateBean payeeBean = (EbppStatusUpdateBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		if (this.messageSource != null && this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
				.equals(EntitlementStatus.ENABLED.toString()))
			userSession.getCustomerService().setEBPPStatus(true);
		else
			userSession.getCustomerService().setEBPPStatus(false);

		AccountMaskUtil mask = new AccountMaskUtil();
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		String id = (String) request.getParameter("id");

		String insidePortal = (String) request.getParameter("showInsidePortal");
		model.put("showInsidePortal", insidePortal);
		String originalId = (String) request.getParameter("origId");

		id = InternationalCharacterUtils.sanatizeInputs(id, false);

		String action = (String) request.getParameter("action");

		if (id != null && !id.equals("")) {
			payeeBean.setId(id);
			if (originalId == null || originalId.equals(""))
				model.put("origId", id);
			else
				model.put("origId", originalId);
		}

		if (action != null && !action.equals(""))
			payeeBean.setAction(EbppStatusUpdateBean.ACTION.valueOf(action));

		// The EBPP2 user id is not yet in session; add it
		if (!userSession.isEbpp2UserIdLookedUp()) {
			EbppEntitlement ent = null;
			if (customerProfile.getEntitlementByType(EntitlementType.EBILLS) != null) {
				ent = new EbppEntitlement(customerProfile.getEntitlementByType(EntitlementType.EBILLS));

				// If the username is blank, we need to look in the current EBPP
				// accounts ONLY
				if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
					userSession.setEbpp2UserId("");
				} else {
					userSession.setEbpp2UserId(ent.getUsername());
				}
				userSession.setEbpp2UserIdLookedUp(true);
			}
		}

		if (payeeBean.getAction() != null && payeeBean.getAction() == EbppStatusUpdateBean.ACTION.UPDATE_EBILL_STATUS) {
			String p = payeeBean.getId();
			TVFrontEndPayee tvp = Utils.getPayeebyId(p, userSession.getPayees(true));

			if (tvp != null) {
				model.put("nickname", tvp.getNickname());

				// The user has an EBPP2 ID
				if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

					if (tvp.getEbpp2UserAccount() != null && tvp.getEbpp2UserAccount().isActive()
							&& tvp.getEbpp2UserAccount().getIsRegistered() == 1) {

						model.put("deactivateEbill", true);
						model.put("act", tvp);
					} else if (tvp.getEbpp2UserAccount() != null && !tvp.getEbpp2UserAccount().isActive()
							|| tvp.getEbpp2UserAccount() == null) {
						// 331=PRTC 4688=MOVISTAR 4251=DIRECTV THOSE
						// REQUIRED TO ASK FOR ZIPCODE
						if (tvp.getGlobalPayeeId() == 331 || tvp.getGlobalPayeeId() == 4688
								|| tvp.getGlobalPayeeId() == 4251) {
							model.put("askZipCode", true);
						}

						switch (tvp.getGlobalPayeeId()) {
						case 36:
							model.put("ebilltype", "aaa");
							break;
						case 37:
							model.put("ebilltype", "aee");
							break;
						case 223:
							model.put("ebilltype", "mastercard");
							break;
						case 331:
							model.put("ebilltype", "prtc");
							break;
						case 359:
						case 517:
						case 518:
							model.put("ebilltype", "visa");
							break;
						case 405:
							model.put("ebilltype", "centenial");
							break;
						case 1983:
							model.put("ebilltype", "amex");
							break;
						case 4251:
							model.put("ebilltype", "directv");
							break;
						case 4688:
							model.put("ebilltype", "openmobile");
							break;

						default:
							model.put("ebilltype", "default");
							break;
						}

						model.put("activateEbill", true);
						model.put("act", tvp);
						model.put("customerEmail", customerProfile.getEmail());
						
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
							TVFrontendAccount acc = Utils.getAccountByAccountNumber(tvp.getBillingAccount(),
									userSession.getAccounts());
							if (acc != null && acc.getSubtype() != null && acc.getSubtype().equals("CCA")) {
								try {
									CustomerServices customerService = userSession.getCustomerService();
									CustomerInquiryBean responseTsys = customerService.prophItService()
											.makeCustomerInquiry(acc.getAccountNumber());
									if (responseTsys != null) {
										if (responseTsys.getEbillInd().equals(EbillIndType.ACTIVE.toString())) {
											model.put("deactivateEbill", true);
											model.put("act", tvp);
											model.put("customerEmail", customerProfile.getEmail());
										} else if (responseTsys.getEbillInd()
												.equals(EbillIndType.ELIGIBLE.toString())) {
											model.put("activateEbill", true);
											model.put("act", tvp);
											model.put("customerEmail", customerProfile.getEmail());
										} else {
											// no eligible
										}
									}
								} catch (Exception e) {
									logger.error(e.getMessage());
								}
							}
						}
					}
				}
			}
		} else if (payeeBean.getAction() != null
				&& payeeBean.getAction() == EbppStatusUpdateBean.ACTION.UPDATE_ESTMT_STATUS) {
			String p = payeeBean.getId();
			TVFrontendAccount tvp = Utils.getAccountById(p, userSession.getAccounts());

			if (tvp == null)
				return model;

			// Is this an IDA account or a credit card?
			boolean isCreditCard = tvp.getSubtype().equals("CCA");
			if (isCreditCard) {
				String ebillType = "visa";
				if (tvp.getAccountNumber().startsWith("5"))
					ebillType = "mastercard";
				else if (tvp.getAccountNumber().startsWith("03") || tvp.getAccountNumber().startsWith("3"))
					ebillType = "amex";
				model.put("ebilltype", ebillType);
				model.put("isCreditCard", "true");
			}

			model.put("nickname", tvp.getNickname() + " " + mask.mask(tvp.getAccountNumber()));

			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)){
				if(isCreditCard){
					try {
						CustomerServices customerService = userSession.getCustomerService();
						CustomerInquiryBean responseTsys = customerService.prophItService().makeCustomerInquiry(tvp.getAccountNumber());
						if (responseTsys != null) {
							if(responseTsys.getEbillInd().equals(EbillIndType.ACTIVE.toString())){
								model.put("deactivateEstatement", true);
								model.put("act", tvp);
								model.put("customerEmail", customerProfile.getEmail());
							}else if(responseTsys.getEbillInd().equals(EbillIndType.ELIGIBLE.toString())){
								model.put("activateEstatement", true);
								model.put("act", tvp);
								model.put("customerEmail", customerProfile.getEmail());
							}else{
								//no eligible
							}
						}
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
				}else{
					TVFrontendDepositAccount tvpIda = Utils.getIdaAccountById(p, userSession.getIdaAccounts());
					if (tvpIda != null && tvpIda.getStmtDisposition() != null) {
						if(tvpIda.getStmtDisposition().equalsIgnoreCase("E") || tvpIda.getStmtDisposition().equalsIgnoreCase("V")){
							model.put("deactivateEstatement", true);
							model.put("act", tvp);
							model.put("customerEmail", customerProfile.getEmail());
						}
						else if(tvpIda.getStmtDisposition().equalsIgnoreCase("M") || tvpIda.getStmtDisposition().equalsIgnoreCase("S")){
							model.put("activateEstatement", true);
							model.put("act", tvp);
							model.put("customerEmail", customerProfile.getEmail());
						}
					}
				}			
			}else{
				// The user has an EBPP2 ID
				if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

					// Get the list of user accounts
					List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

					// Loop through all the accounts and match them with the proper
					// userAccount
					if (ebpp2UserAccounts != null && ebpp2UserAccounts.size() > 0) {
						tvp.setEbpp2UserAccount(Utils.getEbpp2UserAccountsByAccount(tvp.getAccountNumber(), userSession));
					}

					if (tvp != null && tvp.getEbpp2UserAccount() != null && tvp.getEbpp2UserAccount().isActive()
							&& tvp.getEbpp2UserAccount().getIsRegistered() == 1) {

						model.put("deactivateEstatement", true);
						model.put("act", tvp);
						model.put("customerEmail", customerProfile.getEmail());
					} else if (tvp != null && (tvp.getEbpp2UserAccount() != null && !tvp.getEbpp2UserAccount().isActive())
							|| tvp.getEbpp2UserAccount() == null) {

						model.put("activateEstatement", true);
						model.put("act", tvp);
						model.put("customerEmail", customerProfile.getEmail());
					}
				}
			}
		}
		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		EbppStatusUpdateBean payeeBean = (EbppStatusUpdateBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		if (payeeBean.getAction() != null && payeeBean.getAction() == EbppStatusUpdateBean.ACTION.UPDATE_EBILL_STATUS) {
			String p = payeeBean.getId();
			if (p == null || p.equals(""))
				errors.rejectValue("id", "", "Payee id is required");

			TVFrontEndPayee tvp = Utils.getPayeebyId(p, userSession.getPayees(true));
			if (tvp == null || tvp.equals(""))
				errors.rejectValue("id", "", "Payee selection is required");

			if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {
				if (tvp != null
						&& ((tvp.getEbpp2UserAccount() != null && !tvp.getEbpp2UserAccount().isActive()) || tvp
								.getEbpp2UserAccount() == null)) {
					// 331=PRTC 4688=MOVISTAR 4251=DIRECTV THOSE
					// REQUIRED TO ASK FOR ZIPCODE
					if (tvp.getGlobalPayeeId() == 331 || tvp.getGlobalPayeeId() == 4688
							|| tvp.getGlobalPayeeId() == 4251) {
						if (payeeBean.getZipCode() == null || payeeBean.getZipCode().equals("")
								|| !payeeBean.getZipCode().matches("\\d{5}") || payeeBean.getZipCode().equals("00000"))
							errors.rejectValue("zipCode", "ebppStatusUpdate.zipcode.required", "Zipcode is required");
					}
				}
			}
		} else if (payeeBean.getAction() != null
				&& payeeBean.getAction() == EbppStatusUpdateBean.ACTION.UPDATE_ESTMT_STATUS) {
			String p = payeeBean.getId();
			if (p == null || p.equals(""))
				errors.rejectValue("id", "", "Account Id is required");
		} else if (payeeBean.getAction() != null && payeeBean.getAction() == EbppStatusUpdateBean.ACTION.ALGARO_UPDATE) {
			String p = payeeBean.getId();
			if (p == null || p.equals(""))
				errors.rejectValue("id", "", "Account Id is required");
		}
		
		boolean emailError = false;
		if (payeeBean.getEmail() != null && !payeeBean.getEmail().equals("")
				&& !GenericValidator.isEmail(payeeBean.getEmail())) {
			errors.rejectValue("email", "", "Please verify your email");
			emailError = true;
		} else if (payeeBean.getEmail() != null && !payeeBean.getEmail().equals("")
				&& GenericValidator.isEmail(payeeBean.getEmail())
				&& !payeeBean.getEmail().equalsIgnoreCase(payeeBean.getEmailConfirm())) {
			errors.rejectValue("emailConfirm", "", "Your email must be equal");
			emailError = true;
		}
		// MBSFE-691
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE691) && payeeBean.getEmail() != null && !emailError){
			String[] domain = payeeBean.getEmail().split("@");
			if(domain.length > 1 && Utils.haveType(this.invalidEmailDomains, domain[1])) {
				errors.rejectValue("emailConfirm", "email.invaliddomain", "Your e-mail address is incorrect. Please try again.");
			}
		}
		// END MBSFE-691
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}
	
	// MBSFE-691
	public String getInvalidEmailDomains() {
		return invalidEmailDomains;
	}

	public void setInvalidEmailDomains(String invalidEmailDomains) {
		this.invalidEmailDomains = invalidEmailDomains;
	}
	// END MBSFE-691
}
