package evertec.cibp.web.form;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.signup.SignupCampaign;
import evertec.cibp.web.form.bean.CampaignConcurrencyBean;
import evertec.cibp.web.session.UserSession;

public class RegestartForm extends CancellableFormController {

	private CampaignConcurrencyBean concurrencyBean;

	public RegestartForm() {
		super();
		setCommandClass(SignupCampaign.class);
		setCommandName("regestart");
		setFormView("regestart");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String hidden = ServletRequestUtils.getStringParameter(request, "actiontype");
		if (hidden.equalsIgnoreCase("yes")) {
			return new ModelAndView("redirect:campaigns");
		} else if (hidden.equalsIgnoreCase("no")) {
			SignupCampaign signupCampaign = new SignupCampaign();
			signupCampaign.setCampaignDate(new Date());
			signupCampaign.setCampaignResult('N');
			signupCampaign.setProfileId(userSession.getCustomerProfile().getProfileId());
			signupCampaign.setSsn(userSession.getCustomerProfile().getTaxId());
			signupCampaign.setCampaignType(CampaignType.REGE.toString());
			signupCampaign.setCan(userSession.getCustomerProfile().getEnrollmentAccountNumber());

			List<SignupCampaign> signupCampaignInfoByProfileIdA = userSession.getCustomerSignupCampaigns();
			boolean campaignFound = false;
			for (Iterator<SignupCampaign> iter = signupCampaignInfoByProfileIdA.iterator(); iter.hasNext();) {
				SignupCampaign camp = (SignupCampaign) iter.next();
				if (camp.getCampaignType().equals(CampaignType.REGE.toString())) {
					campaignFound = true;
					break;
				}
			}
			if (!campaignFound) {
				userSession.getCustomerProfile().getSignupCampaign().add(signupCampaign);
				userSession.getCustomerService().saveSignupCampInfo(signupCampaign);
			}
			concurrencyBean.subConcurrency();
			return new ModelAndView("redirect:portal");
		} else if (hidden.equalsIgnoreCase("skip")) {
			WebUtils.setSessionAttribute(request, "skiprege", new Boolean(true));
			return new ModelAndView("redirect:portal");
		}

		return new ModelAndView("redirect:portal");

	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		// concurrencyBean.subConcurrency();
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		return model;
	}

	/**
	 * @return the concurrencyBean
	 */
	public CampaignConcurrencyBean getConcurrencyBean() {
		return concurrencyBean;
	}

	/**
	 * @param concurrencyBean
	 *            the concurrencyBean to set
	 */
	public void setConcurrencyBean(CampaignConcurrencyBean concurrencyBean) {
		this.concurrencyBean = concurrencyBean;
	}
}
