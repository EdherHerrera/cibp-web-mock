package evertec.cibp.web.form;

/**Clase evertec.cibp.web.form.GuidedSetupAddEbillPayee
 * Sistema: Mi Banco
 * Descripcion: Clase que controla las acciones de la capa de presentacion
 * para agregar ebills y comercios en la campana guided setup
 * @author Ing. Blas Alonso Garcia Ruiz 
 * Creada el Mayo 02, 
 * cibp-2218 
 */

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.form.bean.AddPayeeBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

public class GuidedSetupAddEbillPayee extends CancellableFormController implements GlobalEntitlementAwareController,
		MessageSourceAware {

	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	// mpicado, atributo que se agrega para poder leer el archivo downtime,
	// ocupa el implements del MessageSourceAware
	private MessageSource messageSource;

	public GuidedSetupAddEbillPayee() {
		super();
		setCommandClass(AddPayeeBean.class);
		setCommandName("gsAddEbillPayee");
		setFormView("gsAddEbillPayee");

	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		return new ModelAndView("gsAddEbillPayee");
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		Map<String, Object> model = new HashMap<>();

		// mpicado, cibp-2218, se agrega logica para poder incluir la seccion
		// extra del guided setup
		if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			/*
			 * aqui establezco el valor de el MessageSource para que lo pueda
			 * usar el bean del core, me parece que podria hacerse accesando el
			 * context pero tuve algunos problemas al hacerlo.. mpicado
			 * CIBP-1799
			 */
			customerService.setEBPPStatus(this.getEBPPMessageSourceStatus());

			// List<TVFrontEndPayee> payees = userSession.getPayessFullEbpp();
			// mpicado CIBP-1799 se cambia el llamado por el metodo sobrecargado
			// que se creo, para poder enviarle el status de EBPP
			List<TVFrontEndPayee> payees = userSession.getPayees(true);

			if (payees != null && payees.size() > 0) {
				for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
					TVFrontEndPayee name = (TVFrontEndPayee) iterator.next();
					// This are globalPayee IDs for BPPR reserve accounts
					int tp = name.getGlobalPayeeId();
					/*
					 * mpicado, me parece que esto no deberia estar hardcoded,
					 * podria hacerse una propiedad para esta condicion, como no
					 * se la logica exacta para lo que se usa entonces lo dejo
					 * tal y como lo encontre
					 */
					if (tp == 288 || tp == 289 || tp == 4144 || tp == 290 || tp == 12682 || tp == 12453) {
						if (name.getBillerNumberDescription() != null) {
							TVFrontendAccount reserveAct = Utils.getAccountByKey(name.getBillerNumberDescription()
									.trim() + "|IDA|R", userSession.getAccounts());
							if (reserveAct != null && reserveAct.getPortalBalance() != null) {
								name.setCurrentBalance(reserveAct.getPortalBalance());
							}
						}
					}
				}
				model.put("payeesGS", payees);
			}

			if (userSession.getGuidedSetupSession().isResubmitParent()) {
				model.put("submitgs", true);
				userSession.getGuidedSetupSession().setResubmitParent(false);
			}

		}

		return model;

	}

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.BILLPAY;
	}

	/** @return regresa el messageSource */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource
	 *            el messageSource a establecer
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

}
