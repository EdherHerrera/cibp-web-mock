package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.utils.CibpValidationUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.form.bean.AddPayeeBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

public class AddCheckPayeeForm extends CancellableFormController implements GlobalEntitlementAwareController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;

	public AddCheckPayeeForm() {
		super();
		setCommandClass(AddPayeeBean.class);
		setCommandName("cp");
		setFormView("addcheckpayee");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		AddPayeeBean payeeBean = (AddPayeeBean) command;

		String address1 = payeeBean.getCkladdress1();
		String address2 = payeeBean.getCkladdress2();
		String city = payeeBean.getCklcity();
		String state = payeeBean.getCklstate();
		String postalCode = payeeBean.getCklpostalCode();
		String postalCode2 = payeeBean.getCklpostalCode2();
		String phone = payeeBean.getCklphone();
		String phone2 = payeeBean.getCklphone2();
		String phone3 = payeeBean.getCklphone3();

		if (!GenericValidator.isBlankOrNull(postalCode2))
			postalCode = postalCode + "-" + postalCode2;

		phone = phone + phone2 + phone3;

		String billinAct = payeeBean.getBillingNumber();
		String nick = payeeBean.getNickname();

		boolean isHighRiskPayee = false;
		boolean add = userSession.addCheckPayee(profile, userSession.getBankingSession(),
				Utils.generateCustomerInteractionEvent(request), address1, address2, city, postalCode, state, phone,
				billinAct, nick, this.isEbppAvailable(), isHighRiskPayee);

		if (add) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				// userSession.setStatusmessage("Your payee " + nick + " - " +
				// billinAct + " was added.");
				userSession.setStatusmessage("Payee sent for approval.");
			} else {
				// userSession.setStatusmessage("Su comercio " + nick + " - " +
				// billinAct + " fue a&ntilde;adido existosamente.");
				userSession.setStatusmessage("Comercio enviado para aprobaci&oacute;n.");
			}
		} else {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				// userSession.setStatusmessage("Unable to add your payee " +
				// nick + " - " + billinAct + " at this time.");
				userSession.setStatusmessage("Payee could not be sent for approval.");
			} else {
				// userSession.setStatusmessage("Error a&ntilde;adiendo su comercio "
				// + nick + " - " + billinAct + " en este momento.");
				userSession.setStatusmessage("Comercio no pudo ser enviado para aprobac&oacute;n.");
			}
		}
		return new ModelAndView("redirect:editpayee");
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();

		// Get the states
		Map<Object, String> states = new LinkedHashMap<Object, String>();
		states.put("PR", "PR");
		states.put("VI", "VI");
		states.put("BVI", "BVI");

		states.put("AL", "AL");
		states.put("AK", "AK");
		states.put("AZ", "AZ");
		states.put("AR", "AR");
		states.put("CA", "CA");
		states.put("CO", "CO");
		states.put("CT", "CT");
		states.put("DE", "DE");
		states.put("DC", "DC");
		states.put("FL", "FL");

		states.put("GA", "GA");
		states.put("HI", "HI");
		states.put("ID", "ID");
		states.put("IL", "IL");
		states.put("IN", "IN");
		states.put("IA", "IA");
		states.put("KS", "KS");
		states.put("KY", "KY");
		states.put("LA", "LA");
		states.put("ME", "ME");

		states.put("MD", "MD");
		states.put("MA", "MA");
		states.put("MI", "MI");
		states.put("MN", "MN");
		states.put("MS", "MS");
		states.put("MO", "MO");
		states.put("MI", "MI");
		states.put("MT", "MT");
		states.put("NE", "NE");
		states.put("NV", "NV");
		states.put("NH", "NH");

		states.put("NJ", "NJ");
		states.put("NM", "NM");
		states.put("NY", "NY");
		states.put("NC", "NC");
		states.put("ND", "ND");
		states.put("OH", "OH");
		states.put("OK", "OK");
		states.put("OR", "OR");
		states.put("PA", "PA");
		states.put("RI", "RI");

		states.put("SC", "SC");
		states.put("SD", "SD");
		states.put("TN", "TN");
		states.put("TX", "TX");
		states.put("UT", "UT");
		states.put("VT", "VT");
		states.put("VA", "VA");
		states.put("WA", "WA");
		states.put("WV", "WV");
		states.put("WI", "WI");
		states.put("WY", "WY");

		model.put("states", states);

		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		AddPayeeBean payeeBean = (AddPayeeBean) command;

		String address1 = payeeBean.getCkladdress1();
		String address2 = payeeBean.getCkladdress2();
		String zipCode = payeeBean.getCklpostalCode();
		String zipCode2 = payeeBean.getCklpostalCode2();
		String phoneNumber = payeeBean.getCklphone();
		String phoneNumber2 = payeeBean.getCklphone2();
		String phoneNumber3 = payeeBean.getCklphone3();
		String accountNumber = payeeBean.getBillingNumber();
		boolean hasError = false;

		if (GenericValidator.isBlankOrNull(accountNumber)
				|| !CibpValidationUtils.isValidPayeeAccountNumber(accountNumber)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("billingNumber", "", "Account number is required");
			else
				errors.rejectValue("billingNumber", "", "Entra un n&uacute;mero de cuenta v&aacute;lido");
			hasError = true;
		}

		if (GenericValidator.isBlankOrNull(payeeBean.getBillingNumberConf())) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("billingNumberConf", "", "Please enter your account number in this field");
			else
				errors.rejectValue("billingNumberConf", "", "Confirma el n&uacute;mero de cuenta");
			hasError = true;
		} else {
			if (!GenericValidator.isBlankOrNull(payeeBean.getBillingNumber())
					&& !GenericValidator.isBlankOrNull(payeeBean.getBillingNumberConf())
					&& !payeeBean.getBillingNumber().equals(payeeBean.getBillingNumberConf())) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("billingNumberConf", "", "Your account number and confirmation must be equal");
				else
					errors.rejectValue("billingNumberConf", "",
							"Su n&uacute;mero de cuenta y confirmaci&oacute;n deben ser igual");
				hasError = true;
			}
		}

		if (GenericValidator.isBlankOrNull(payeeBean.getNickname())) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("nickname", "", "Please enter a payee name");
			else
				errors.rejectValue("nickname", "", "Entra el nombre del comercio");
			hasError = true;
		}

		if (GenericValidator.isBlankOrNull(address1) || !CibpValidationUtils.isValidAddress(address1)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("ckladdress1", "", "Please enter a valid payee address");
			else
				errors.rejectValue("ckladdress1", "", "Entra una direcci&oacute;n v&aacute;lida");
			hasError = true;
		}

		if (!GenericValidator.isBlankOrNull(address2) && !CibpValidationUtils.isValidAddress(address2)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("ckladdress2", "", "Please enter a valid payee address");
			else
				errors.rejectValue("ckladdress2", "", "Entra una direcci&oacute;n v&aacute;lida");
			hasError = true;
		}

		if (GenericValidator.isBlankOrNull(payeeBean.getCklcity())) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("cklcity", "", "Please enter the city of payee");
			else
				errors.rejectValue("cklcity", "", "Entra la ciudad");
			hasError = true;
		}

		if (GenericValidator.isBlankOrNull(payeeBean.getCklpostalCode())) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("cklpostalCode", "", "Please enter the zipcode");
			else
				errors.rejectValue("cklpostalCode", "", "Entra un c&oacute;digo postal v&aacute;lido");
			hasError = true;
		}

		if (GenericValidator.isBlankOrNull(payeeBean.getCklstate())) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("cklstate", "", "Please enter a state");
			else
				errors.rejectValue("cklstate", "", "Entra un estado");
			hasError = true;
		}

		if (GenericValidator.isBlankOrNull(zipCode) || !CibpValidationUtils.isValidZipcode(zipCode)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("cklpostalCode2", "", "Please enter a valid zipcode");
			else
				errors.rejectValue("cklpostalCode2", "", "Entra un c&oacute;digo postal v&aacute;lido");
			hasError = true;
		} else {
			if (!GenericValidator.isBlankOrNull(zipCode2)) {
				if (!GenericValidator.matchRegexp(zipCode2, "\\d{4}")) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("cklpostalCode2", "", "Please enter a valid zipcode");
					else
						errors.rejectValue("cklpostalCode2", "", "Entra un c&oacute;digo postal v&aacute;lido");
					hasError = true;
				}
			}
		}

		if (GenericValidator.isBlankOrNull(phoneNumber) || !GenericValidator.matchRegexp(phoneNumber, "\\d{3}")) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("cklphone3", "", "Please enter a valid telephone number");
			else
				errors.rejectValue("cklphone3", "", "Entra un tel&eacute;fono v&aacute;lido");
			hasError = true;
		} else {
			if (GenericValidator.isBlankOrNull(phoneNumber2) || !GenericValidator.matchRegexp(phoneNumber2, "\\d{3}")) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("cklphone3", "", "Please enter a valid telephone number");
				else
					errors.rejectValue("cklphone3", "", "Entra un tel&eacute;fono v&aacute;lido");
				hasError = true;
			} else {
				if (GenericValidator.isBlankOrNull(phoneNumber3)
						|| !GenericValidator.matchRegexp(phoneNumber3, "\\d{4}")) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("cklphone3", "", "Please enter a valid telephone number");
					else
						errors.rejectValue("cklphone3", "", "Entra un tel&eacute;fono v&aacute;lido");
					hasError = true;
				}
			}
		}
		if (hasError) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Please verify for errors.");
			else
				userSession.setStatusmessage("Verifica la informaci&oacute;n entrada.");
		} else
			userSession.setStatusmessage("");
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.BILLPAY;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	private boolean isEbppAvailable() {
		boolean ebppStatus = false;

		if (this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
				.equals(EntitlementStatus.ENABLED.toString())) {
			ebppStatus = true;
		} else {
			ebppStatus = false;
		}

		return ebppStatus;
	}

}
