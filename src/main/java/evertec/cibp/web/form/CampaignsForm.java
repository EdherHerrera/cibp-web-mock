package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.types.DepositAccount;

import com.evertecinc.seagull.SeagullService;

import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.campaign.CampaignGenericBean;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.signup.SignupCampaign;
import evertec.cibp.web.form.bean.CampaignConcurrencyBean;
import evertec.cibp.web.form.bean.RegEAccountBean;
import evertec.cibp.web.session.UserSession;

public class CampaignsForm extends CancellableFormController {

	private CampaignGenericBean campaignGenericBean;
	private SeagullService seagullService;
	private CampaignConcurrencyBean concurrencyBean;

	public CampaignsForm() {
		super();
		setCommandClass(SignupCampaign.class);
		setCommandName("campaigns");
		setFormView("campaigns");
	}

	// @Override
	// protected void onBindAndValidate(HttpServletRequest request, Object
	// command, BindException errors) throws Exception {
	// String hidden = ServletRequestUtils.getStringParameter(request,
	// "actiontype");
	// SignupCampaign sc = (SignupCampaign) command;
	// if (hidden.equalsIgnoreCase("yes") && sc.getAgree() == null) {
	// errors.rejectValue("agree", "rege.error.agreement",
	// "<span style='color:red;font-weight:bold;'>Agree selection required.</span>");
	// }
	// super.onBindAndValidate(request, command, errors);
	// }
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		Collection<RegEAccountBean> odAcct = new ArrayList<RegEAccountBean>();
		String hidden = ServletRequestUtils.getStringParameter(request, "actiontype");
		SignupCampaign sc = (SignupCampaign) command;

		boolean acctFound = false;
		boolean addSignupCampaign = true;

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		if (hidden.equalsIgnoreCase("yes") && sc.getAgree() == null) {
			error.rejectValue("agree", "rege.error.agreement",
					"<span style='color:red;font-weight:bold;'>Agree selection required.</span>");
			return showForm(request, response, error);

		}

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		getConcurrencyBean().subConcurrency();
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		if (accounts == null) {
			throw new ProgrammingError("Unable to get user accounts");
		}
		List<RegEAccountBean> portalAccounts = new LinkedList<RegEAccountBean>();
		if (hidden.equalsIgnoreCase("yes")) {
			boolean errorFound = false;
			for (TVFrontendAccount account : accounts) {
				String frontEndId = account.getFrontEndId();
				String stringParameter = ServletRequestUtils.getStringParameter(request, frontEndId);
				if (stringParameter != null && StringUtils.isNotEmpty(stringParameter.trim())) {
					DepositAccount dda = (DepositAccount) account.getBackendObject();
					boolean completePendingRegE = userSession.getCustomerService().completePendingRegE(account, "A");
					RegEAccountBean regEAccountBean = new RegEAccountBean(account);
					if (completePendingRegE) {
						account.setOdLimit(CampaignGenericBean.ACTIVE_ODLIMIT);
						regEAccountBean.setActivationStatus("A");
					} else {
						// en vez de enviar a una pantalla de error que no
						// existe, no se a�adir� a la tabla de
						// signupCampaignCustomer para que le vuelva a salir la
						// campa�a.
						// return new ModelAndView("forward:regefail");
						regEAccountBean.setActivationStatus("F");
						request.setAttribute("accountFail", "true");
						errorFound = true;
					}
					odAcct.add(regEAccountBean);
					acctFound = true;
				}
			}
			if (!errorFound && acctFound) {
				SignupCampaign signupCampaign = new SignupCampaign();
				signupCampaign.setCampaignDate(new Date());
				signupCampaign.setCampaignResult('Y');
				signupCampaign.setProfileId(userSession.getCustomerProfile().getProfileId());
				signupCampaign.setSsn(userSession.getCustomerProfile().getTaxId());
				signupCampaign.setCampaignType(CampaignType.REGE.toString());
				signupCampaign.setCan(userSession.getCustomerProfile().getEnrollmentAccountNumber());
				if (userSession.getCustomerProfile().getSignupCampaign() == null
						|| userSession.getCustomerProfile().getSignupCampaign().size() == 0) {
					List<SignupCampaign> lstCampaigns = new LinkedList<SignupCampaign>();
					lstCampaigns.add(signupCampaign);
					userSession.getCustomerProfile().setSignupCampaign(lstCampaigns);
				} else {
					List<SignupCampaign> signupCampaignInfoByProfileIdA = userSession.getCustomerSignupCampaigns();
					boolean campaignFound = false;
					for (Iterator<SignupCampaign> iter = signupCampaignInfoByProfileIdA.iterator(); iter.hasNext();) {
						SignupCampaign camp = (SignupCampaign) iter.next();
						if (camp.getCampaignType().equals(CampaignType.REGE.toString())) {
							campaignFound = true;
							addSignupCampaign = false;
							break;
						}
					}
					if (!campaignFound)
						userSession.getCustomerProfile().getSignupCampaign().add(signupCampaign);
				}
				if (addSignupCampaign) {
					userSession.getCustomerService().saveSignupCampInfo(signupCampaign);
				}
			}
		} else if (hidden.equalsIgnoreCase("no")) {
			SignupCampaign signupCampaign = new SignupCampaign();
			signupCampaign.setCampaignDate(new Date());
			signupCampaign.setCampaignResult('N');
			signupCampaign.setProfileId(userSession.getCustomerProfile().getProfileId());
			signupCampaign.setSsn(userSession.getCustomerProfile().getTaxId());
			signupCampaign.setCampaignType(CampaignType.REGE.toString());
			signupCampaign.setCan(userSession.getCustomerProfile().getEnrollmentAccountNumber());
			if (userSession.getCustomerProfile().getSignupCampaign() == null
					|| userSession.getCustomerProfile().getSignupCampaign().size() == 0) {
				List<SignupCampaign> lstCampaigns = new LinkedList<SignupCampaign>();
				lstCampaigns.add(signupCampaign);
				userSession.getCustomerProfile().setSignupCampaign(lstCampaigns);
			} else {
				List<SignupCampaign> signupCampaignInfoByProfileIdA = userSession.getCustomerSignupCampaigns();
				boolean campaignFound = false;
				for (Iterator<SignupCampaign> iter = signupCampaignInfoByProfileIdA.iterator(); iter.hasNext();) {
					SignupCampaign camp = (SignupCampaign) iter.next();
					if (camp.getCampaignType().equals(CampaignType.REGE.toString())) {
						campaignFound = true;
						addSignupCampaign = false;
						break;
					}
				}
				if (!campaignFound)
					userSession.getCustomerProfile().getSignupCampaign().add(signupCampaign);
			}
			if (addSignupCampaign) {
				userSession.getCustomerService().saveSignupCampInfo(signupCampaign);
			}

			request.setAttribute("displayNo", "block");
			return showForm(request, response, error);
			// return new ModelAndView("redirect:portal");
		} else if (hidden.equalsIgnoreCase("skip")) {
			WebUtils.setSessionAttribute(request, "skiprege", new Boolean(true));
			return new ModelAndView("redirect:portal");
		}
		if (!acctFound) {
			error.rejectValue("agree", "rege.error.acctselection",
					"<span style='color:red;font-weight:bold;'>Account selection required.</span>");
			return showForm(request, response, error);
		}
		request.setAttribute("odAcct", odAcct);
		return showForm(request, response, error);

	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		SignupCampaign sc = (SignupCampaign) command;
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		if (accounts == null) {
			throw new ProgrammingError("Unable to get user accounts");
		}

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		CustomerProfile profile = userSession.getCustomerProfile();
		if (request.getAttribute("accounts") == null) {
			if (accounts != null && accounts.size() > 0) {
				List<RegEAccountBean> portalAccounts = new LinkedList<RegEAccountBean>();
				for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
					TVFrontendAccount name = (TVFrontendAccount) iterator.next();
					String accountNumberSuffix = name.getAccountNumberSuffix();
					if (!name.getHide()
							&& getCampaignGenericBean().getCampaignProductsId().contains(name.getAccountProductId())
							&& !getCampaignGenericBean().getInvalidSufix().contains(accountNumberSuffix)) {
						if (name.getSubtype().equals("INS") || name.getSubtype().equals("SEC")) {
							continue;
						} else if (name.getSubtype().equals("PRM")) {
							continue;
						} else {
							String frontEndId = name.getFrontEndId();
							String selectedAcct = ServletRequestUtils.getStringParameter(request, frontEndId);
							RegEAccountBean regEAccountBean = new RegEAccountBean(name);
							if (selectedAcct != null) {
								regEAccountBean.setSelected("checked");
							}
							portalAccounts.add(regEAccountBean);
						}
					}
				}
				if (portalAccounts != null && portalAccounts.size() > 0) {
					model.put("accounts", portalAccounts);
				}
				model.put("customername", profile.getFirstName());
				Map<String, String> agree = new HashMap<String, String>();
				agree.put("agree", "");
				model.put("agree", agree);
				model.put("agreeChecked", "");
				if (sc.getAgree() != null && sc.getAgree().equals("agree")) {
					model.put("agreeChecked", "checked");
				}
				Collection<TVFrontendAccount> odAcct = (Collection<TVFrontendAccount>) request.getAttribute("odAcct");
				String displayNo = (String) request.getAttribute("displayNo");
				String accountFail = (String) request.getAttribute("accountFail");
				model.put("accountFail", accountFail);
				model.put("displayYes", "none");
				model.put("displayNo", "none");
				if (odAcct != null) {
					model.put("displayYes", "block");
					model.put("accounts", odAcct);
				}
				if (displayNo != null) {
					model.put("displayNo", "none");
				}
			}
		}

		return model;
	}

	/**
	 * @return the seagullService
	 */
	public SeagullService getSeagullService() {
		return seagullService;
	}

	/**
	 * @param seagullService
	 *            the seagullService to set
	 */
	public void setSeagullService(SeagullService seagullService) {
		this.seagullService = seagullService;
	}

	/**
	 * @return the concurrencyBean
	 */
	public CampaignConcurrencyBean getConcurrencyBean() {
		return concurrencyBean;
	}

	/**
	 * @param concurrencyBean
	 *            the concurrencyBean to set
	 */
	public void setConcurrencyBean(CampaignConcurrencyBean concurrencyBean) {
		this.concurrencyBean = concurrencyBean;
	}

	/**
	 * @return the campaignGenericBean
	 */
	public CampaignGenericBean getCampaignGenericBean() {
		return campaignGenericBean;
	}

	/**
	 * @param campaignGenericBean
	 *            the campaignGenericBean to set
	 */
	public void setCampaignGenericBean(CampaignGenericBean campaignGenericBean) {
		this.campaignGenericBean = campaignGenericBean;
	}
}
