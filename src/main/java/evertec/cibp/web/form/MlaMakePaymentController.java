package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.factory.TVAccountFactory;
import bppr.tv.beans.accounts.records.MortgageAccountRecord;
import bppr.tv.beans.accounts.types.DepositAccount;
import bppr.tv.beans.accounts.types.LoanAccount;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.BillPayEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.holiday.Holiday;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.PaymentServicesEnrollmentRequest;
import evertec.cibp.core.services.responses.AddPayeeResponse;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.TelepagoResponse;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.CCPortalPayments;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class MlaMakePaymentController extends CancellableFormController implements MessageSourceAware {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	private evertec.cibp.vs.services.TransactionVaultServices vaultService;

	private MessageSource messageSource;

	private String jsonPayees, jsonPayeesOtherBank;

	public MlaMakePaymentController() {
		super();
		setCommandClass(CCPortalPayments.class);
		setCommandName("mlaMakePayment");
		setFormView("mlaMakePayment");
	}

	// Added for CIBP-1767 Portal de Tarjetas de Credito
	private Map<String, String> getPaymentDays(TVFrontendAccount account) {

		Calendar pmtDueDate = Calendar.getInstance();
		Calendar pmtStartDate = Calendar.getInstance();

		// process to know which day the user can make the payment
		Map<String, String> paymentDays = new LinkedHashMap<String, String>();
		LoanAccount loanAccount = null;
		Account accountTemp = (Account) account.getBackendObject();

		if (accountTemp instanceof LoanAccount) {
			loanAccount = (LoanAccount) account.getBackendObject();
			MortgageAccountRecord mar = (MortgageAccountRecord) loanAccount.getRecords();
			if (mar.getMaturityDate() != null)
				pmtDueDate = mar.getMaturityDate();
		}

		if (pmtDueDate != null) {
			pmtStartDate.setTime(pmtDueDate.getTime());
			// dsalas cibp-2644 change from 7 to 13 days
			pmtStartDate.add(Calendar.DAY_OF_MONTH, -17);

			int startDay = pmtStartDate.get(Calendar.DAY_OF_MONTH);
			int endDay = pmtDueDate.get(Calendar.DAY_OF_MONTH);
			if (pmtDueDate.get(Calendar.MONTH) != pmtStartDate.get(Calendar.MONTH)) {
				int monthEndDay = pmtStartDate.getActualMaximum(Calendar.DAY_OF_MONTH);

				for (int e = 1; e <= endDay; e++) {
					paymentDays.put(String.valueOf(e), String.valueOf(e));
				}
				for (int e = startDay; e <= monthEndDay; e++) {
					paymentDays.put(String.valueOf(e), String.valueOf(e));
				}
			} else {
				for (int e = startDay; e <= endDay; e++) {
					paymentDays.put(String.valueOf(e), String.valueOf(e));
				}
			}
			return paymentDays;
		}
		return null;
	}

	// Added for CIBP-1767
	private Map<String, String> getAccountFromPaymentMap(HttpServletRequest request, UserSession userSession,
			boolean includePIFAccounts) {

		Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsPaymentFrom();
		CustomerServices customerService = userSession.getCustomerService();
		AccountMaskUtil mask = new AccountMaskUtil();
		NumberTool numberTool = new NumberTool();

		Map<String, String> accountsFromMap = new LinkedHashMap<String, String>();
		if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
			accountsFromMap.put("none", "Select an account");
		else
			accountsFromMap.put("none", "Selecciona una cuenta");

		for (Iterator<TVAccountTransferSource> iterator = accountsFrom.iterator(); iterator.hasNext();) {
			TVAccountTransferSource from = (TVAccountTransferSource) iterator.next();
			TVFrontendAccount frontFrom = from.getAccount();
			if (frontFrom != null && !frontFrom.getHide()) {
				if (!frontFrom.getSubtype().equals("CCA") && frontFrom.getPortalBalance() != null)
					accountsFromMap.put(from.getFrontEndId(),
							StringUtils.abbreviate(frontFrom.getNickname(), 15) + " "
									+ mask.mask(frontFrom.getAccountNumber()) + " "
									+ numberTool.format("currency", frontFrom.getPortalBalance())
									+ ((frontFrom.getAccountNumberSuffix() != null
											&& !frontFrom.getAccountNumberSuffix().equals(""))
													? " " + frontFrom.getAccountNumberSuffix() : ""));
				else if (!frontFrom.getSubtype().equals("CCA"))
					accountsFromMap.put(from.getFrontEndId(),
							StringUtils.abbreviate(frontFrom.getNickname(), 15) + " "
									+ mask.mask(frontFrom.getAccountNumber())
									+ ((frontFrom.getAccountNumberSuffix() != null
											&& !frontFrom.getAccountNumberSuffix().equals(""))
													? " " + frontFrom.getAccountNumberSuffix() : ""));
			}
		}

		if (includePIFAccounts) {
			List<PIFExternalAccount> pifExternalAccounts = customerService
					.getConfirmedPIFExternalAccounts(userSession.getCustomerProfile().getTaxId());
			if (pifExternalAccounts != null)
				for (Iterator<PIFExternalAccount> iterator = pifExternalAccounts.iterator(); iterator.hasNext();) {
					PIFExternalAccount from = (PIFExternalAccount) iterator.next();
					accountsFromMap.put("PIF" + from.getAccountId(),
							StringUtils.abbreviate(from.getNickname(), 15) + " " + mask.mask(from.getAccountNumber()));
				}
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)){
				if(userSession.getInterbankFundingAllowed()){
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						accountsFromMap.put("add", "Add other bank account");
					else
						accountsFromMap.put("add", "A&ntilde;ade una cuenta de otro Banco");
				}
			}
			else{
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					accountsFromMap.put("add", "Add other bank account");
				else
					accountsFromMap.put("add", "A&ntilde;ade una cuenta de otro Banco");
			}
		}

		return accountsFromMap;
	}

	private String getAccountFromPaymentInfo(HttpServletRequest request, UserSession userSession, String accountId) {
		String acct = "";

		Map<String, String> accounts = this.getAccountFromPaymentMap(request, userSession, true);
		acct = accounts.get(accountId);

		return acct;
	}

	// Added for CIBP-1767 - Used in Payment Tab
	private Map<String, Object> makeSinglePayment(CCPortalPayments ccPayment, UserSession userSession,
			CustomerProfile customerProfile, CustomerServices customerService, HttpServletRequest request,
			boolean hasAlert) throws RemoteException {
		Map<String, Object> model = new HashMap<>();

		TVFrontendAccount sou = null;

		// If it is a PIF account, create it the TVFrontendAccount Object.
		if (ccPayment.getSinglePayAccountFrom().startsWith("PIF")) {
			String externalAccountId = ccPayment.getSinglePayAccountFrom().substring(3);
			PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
			if (externalAccount != null) {
				BankInformation bankInformation = new BankInformation();
				bankInformation.setBankId(externalAccount.getRoutingNumber());
				bankInformation.setBankType("ABA");
				bankInformation.setBankName(externalAccount.getBankName());
				CustomerAccountPreferences pref = new CustomerAccountPreferences();
				TVAccountFactory accountFactory = new TVAccountFactory();
				Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
						externalAccount.getAccountNumber());
				sourceAccount.setAccountID(externalAccount.getAccountNumber());
				sourceAccount.setSubType("IDA");
				((DepositAccount) sourceAccount)
						.setSectionIndicator(Character.toString(externalAccount.getAccountType()));
				TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
				fromAccount.setCustomerAccountPreferences(pref);
				fromAccount.setNickname(externalAccount.getNickname());
				fromAccount.setHide(externalAccount.getShow() == '0');
				fromAccount.getTVAccount().setBankInformation(bankInformation);
				sou = fromAccount;
			}
		} else {
			sou = Utils.getAccountById(ccPayment.getSinglePayAccountFrom(), userSession.getAccounts());
		}
		TVFrontEndPayee tar = Utils.getPayeebyId(ccPayment.getPayeeInfo().getFrontEndId(), userSession.getPayees(true));
		model.put("PAYMENTINPROCESS", true);
		if (tar != null) {

			BigDecimal amount = ccPayment.getSinglePayAmount();

			model.put("PAYMENTFROM",
					this.getAccountFromPaymentInfo(request, userSession, ccPayment.getSinglePayAccountFrom()));

			Calendar cal = Calendar.getInstance();
			if (ccPayment.getSinglePayDate() != null)
				cal.setTime(ccPayment.getSinglePayDate());

			TVPayment payment = null;
			payment = new TVPayment(amount, Frequency.MANUALLY, 0, cal, new TVAccountTransferSource(sou), tar);
			payment.getTVPayment().setEffectiveDate(cal);
			payment.getPayee().setTelenomina(false);
			PaymentResponse res = PaymentsServiceModule.getInstance().sendPayments(customerProfile, userSession,
					payment, Utils.generateCustomerInteractionEvent(request), customerService);
			List<TVPayment> successfullPayment = new LinkedList<TVPayment>();
			List<TVPayment> failPayment = new LinkedList<TVPayment>();

			if (res.getStatus() == ResultStatus.SUCCESS) {
				if (res.getPayment().getPayee().getEbpp2UserAccount() != null
						&& res.getPayment().getPayee().isHasNewEbill()) {
					customerService.updateAccountPayment(userSession.getEbpp2UserId(),
							res.getPayment().getPayee().getEbpp2UserAccount().getBillerAccountID());
					// Alizano hide new ebill message when payment is success
					// Fix Incident 1537231
					res.getPayment().getPayee().setHasNewEbill(false);
					res.getPayment().getPayee().getEbpp2UserAccount().setLastEbillPaid(true);
					// End Fix
				}
				model.put("PAYMENTSEND", true);
				model.put("REFNUM", payment.getReferenceNumber());
				successfullPayment.add(payment);
			} else {
				model.put("PAYMENTSEND", false);
				model.put("PAYMENTERROR", "ERROR REASON: " + res.getErrorMessage());
				payment.setPaymentMessage(res.getErrorMessage());
				failPayment.add(payment);
			}

			if (hasAlert) {
				customerService.sendPaymentConfirmation(customerProfile, successfullPayment, failPayment,
						userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
						userSession.getPreferredLanguage());
			}
		}

		return model;

	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		CCPortalPayments mlaMakePayment = (CCPortalPayments) command;

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile customerProfile = userSession.getCustomerProfile();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		String id = "";
		if (mlaMakePayment.getAccountId() != null) {
			id = mlaMakePayment.getAccountId();
		} else {
			id = request.getParameter("id");
			mlaMakePayment.setAccountId(id);
		}

		TVFrontendAccount account = null;
		if (mlaMakePayment.getAccountInfo() == null) {
			account = Utils.getAccountById(id, userSession.getAccounts());
			mlaMakePayment.setAccountInfo(account);
		} else {
			account = mlaMakePayment.getAccountInfo();
		}

		if (account != null) {
			model.put("accountSubtype", account.getSubtype());
        	
        	getAmountDueInfo(mlaMakePayment, account, userSession);
        	
			mlaMakePayment.setPayNickname(account.getNickname() + " x"
					+ account.getAccountNumber().substring(account.getAccountNumber().length() - 4));
			mlaMakePayment.setPaymentDaysMap(this.getPaymentDays(account));

			TVFrontEndPayee payee = Utils.getPayeebyAccountNumber(account.getAccountNumber(),
					userSession.getPayees(true));

			if (payee != null) {
				mlaMakePayment.setPayeeInfo(payee);
			}

			GlobalEntitlement entEbill = customerService.getGlobalEntitlement(EntitlementType.EBILLS);

			if (entEbill == null || entEbill.getStatus() != EntitlementStatus.ENABLED) {
				mlaMakePayment.setEbillUp(false);
			} else {
				mlaMakePayment.setEbillUp(true);
			}
		}

		mlaMakePayment.setAccountFromPaymentMapRegular(this.getAccountFromPaymentMap(request, userSession, true));

		String ccAction = request.getParameter("formAction");

		boolean hasalert = false;
		if (customerProfile.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement ent = new AlertsEntitlement(customerProfile.getEntitlementByType(EntitlementType.ALERTS));
			if (ent.getAlerts().contains(AlertType.PAYMENTS_EMAIL)
					|| ent.getAlerts().contains(AlertType.PAYMENTS_SMS)
					|| ent.getAlerts().contains(AlertType.PAYMENTS_PUSH)) {
				hasalert = true;
			}

		}

		if (ccAction.equalsIgnoreCase("singlepay")) {
			Map<String, Object> modelSinglePayment = this.makeSinglePayment(mlaMakePayment, userSession,
					customerProfile, customerService, request, hasalert);
			model.putAll(modelSinglePayment);
		}

		return showForm(request, errors, "mlaMakePayment", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		CCPortalPayments mlaMakePayment = (CCPortalPayments) command;

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99
		
		mlaMakePayment.setAccountFromPaymentMapRegular(this.getAccountFromPaymentMap(request, userSession, true));
		
		String id = "";
		if (mlaMakePayment.getAccountId() != null) {
			id = mlaMakePayment.getAccountId();
		} else {
			id = request.getParameter("id");
			mlaMakePayment.setAccountId(id);
		}
		
		if (id != null && !id.equalsIgnoreCase(""))
			model.put("accountId", id);

		TVFrontendAccount account = null;
		if (mlaMakePayment.getAccountInfo() == null) {
			account = Utils.getAccountById(id, userSession.getAccounts());
			mlaMakePayment.setAccountInfo(account);
		} else {
			account = mlaMakePayment.getAccountInfo();
		}
		
		if (account!=null) {
        	model.put("accountSubtype", account.getSubtype());
        	getAmountDueInfo(mlaMakePayment, account, userSession);
        }
		
		GlobalEntitlement ent = customerService.getGlobalEntitlement(EntitlementType.BILLPAY);
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.ACH_PAYMENT_NT) 
				&& AccountUtils.getIsTransactional(profile).equalsIgnoreCase("N")) {
			List<PIFExternalAccount> pifExternalAccounts = customerService
					.getConfirmedPIFExternalAccounts(profile.getTaxId());
			
			if(pifExternalAccounts == null || pifExternalAccounts.isEmpty()) {
				model.put("disablePaymentsForm", true);
				String message = messageSource.getMessage(
						"makepayments.non.transactional.add.other.bank.account", null, 
						"Other bank account is required to use this service.", 
						new Locale(userSession.getPreferredLanguage().getCode()));
				model.put("ERROR", message);
				return model;
			}
			else {
				
				PaymentServicesEnrollmentRequest paymentServiceEnrollmentRequest = 
						new PaymentServicesEnrollmentRequest();
				
				paymentServiceEnrollmentRequest.setCustomerProfile(profile);
				paymentServiceEnrollmentRequest.setEnrollmentAccount(account);
				
				//We use the first external account to enroll the customer
				paymentServiceEnrollmentRequest.setExternalEnrollmentAccount(
						pifExternalAccounts.get(0));
				
				//The interaction for logging in audit log table.
        		CustomerInteractionEvent interaction = Utils.generateCustomerInteractionEvent(request, profile.getUsername());
				
				paymentServiceEnrollmentRequest.setEnrollmentInteraction(interaction);
				
				TelepagoResponse telepagoResponse = customerService
						.enrollInPaymentServicesWithExternalBankAccount(
								paymentServiceEnrollmentRequest);
				
				//If the enum contains success it means the customer was enrolled successfully 
				//or that the customer is already enrolled.
				if(telepagoResponse.getResultStatusEnum() == ResultStatus.ERROR){
					model.put("disablePaymentsForm", true);
					
					String message = messageSource.getMessage(
							"makepayments.non.transactional.enrollment.error.message", null, 
							"At this time your transaction cannot be processed, please call Customer Service at 787-724-3655.", 
							new Locale(userSession.getPreferredLanguage().getCode()));
					
					//Error displayed at the top of the form.
					model.put("ERROR", message);
					return model;
				}
				else {
					//Add payment service entitlement.
					if(ent == null) {
						customerService.addEntitlement(profile, new BillPayEntitlement(false,
								EntitlementStatus.ENABLED).getCustomerEntitlement());
					}
				}
			}
		}

		if (ent == null || ent.getStatus() != EntitlementStatus.ENABLED) {
			model.put("DOWNPAYMENT", true);
		}
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBIM76)) {
			// Set holidays
			Map<String, Object> h = new LinkedHashMap<String, Object>();
			List<Holiday> holidays = customerService.getHolidays();
			if (holidays != null)
				for (Holiday holiday : holidays) {
					h.put(holiday.getId(),
							new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(holiday.getDate().getTime()));
				}
			model.put("holidays", h);
		}

		if (account != null) {
			
			addNewPayee(request, model, userSession, customerService, profile, account);

			updatePayees(request);
			
			mlaMakePayment.setPayNickname(account.getNickname() + " x"
					+ account.getAccountNumber().substring(account.getAccountNumber().length() - 4));
			mlaMakePayment.setPaymentDaysMap(this.getPaymentDays(account));
			
			userSession.getCustomerService().setEBPPStatus(this.getEBPPMessageSourceStatus());
			userSession.getPayees(true);
			
			TVFrontEndPayee payee = Utils.getPayeebyAccountNumber(account.getAccountNumber(),
					userSession.getPayessBasicEbpp(true));

			if (payee != null) {
				TVFrontendAccount reserveAct = Utils.getAccountByAccountNumber(payee.getBillingAccount().trim(),
						userSession.getAccounts());
				if (reserveAct != null && reserveAct.getPortalBalance() != null) {
					payee.setCurrentBalance(reserveAct.getPortalBalance());
				}
				mlaMakePayment.setPayeeInfo(payee);
			}

		}

		return model;
	}
	
	/**
	 * @param mlaMakePayment
	 * @param account
	 * @param userSession
	 */
	private void getAmountDueInfo(CCPortalPayments mlaMakePayment, TVFrontendAccount account, UserSession userSession) {
		
		final String mla = "MLA"; //string to avoid using literals
		
		LoanAccount loanAccount = null; //backend account info
		
		MortgageAccountRecord mortgageActRecord = null; // MLA Record
		
		if (account.getSubtype().equals(mla) && account.getBackendObject() instanceof LoanAccount ) {
			
			 loanAccount = (LoanAccount) account.getBackendObject();
			 
			if (loanAccount.getRecords() instanceof MortgageAccountRecord) {
				mortgageActRecord = (MortgageAccountRecord) loanAccount.getRecords();
			
				if (mortgageActRecord.getCurrentAmount()==null) {
					mlaMakePayment.setCurrentAmountDue(getAmountDueFromAccountDetails(account, userSession));
				}else {
					mlaMakePayment.setCurrentAmountDue(mortgageActRecord.getCurrentAmount());
				}
								
			}
			 
		}
            
	}

	/**
	 * @param account
	 * @param userSession
	 * @return
	 */
	private BigDecimal getAmountDueFromAccountDetails(TVFrontendAccount account, UserSession userSession) {
		
		LoanAccount loanAccount = null; //backend account info
		
		MortgageAccountRecord mortgageActRecord  = null; // MLA Record
		
		AbstractAccount accountDetails = null; // Account Details
		
		BigDecimal amountDue = new BigDecimal(0); //Amount Due
		
		accountDetails = userSession.getCustomerService().getAccountDetails(
				userSession.getCustomerProfile(), userSession.getBankingSession(), account, true);
		if (accountDetails.getBackendObject() instanceof LoanAccount) {
			loanAccount = (LoanAccount) accountDetails.getBackendObject();
			
			if (loanAccount.getRecords() instanceof MortgageAccountRecord) {
				mortgageActRecord = (MortgageAccountRecord) loanAccount.getRecords();
				amountDue = mortgageActRecord.getCurrentAmount();
			}
		}
		
		return amountDue;
	}

	/**
	 * @param request
	 * @param model
	 * @param userSession
	 * @param customerService
	 * @param profile
	 * @param account
	 */
	private void addNewPayee(HttpServletRequest request, Map<String, Object> model, UserSession userSession,
			CustomerServices customerService, CustomerProfile profile, TVFrontendAccount account) {
		String payeeMortgage = null;
		String payeeMortgageOtherBank = null;
		payeeMortgage = Utils.getPayeeJsonValue(account.getBankId(), account.getSubtype(), jsonPayees);
		payeeMortgageOtherBank = Utils.getPayeeJsonValue(account.getBankId(), account.getSubtype(), jsonPayeesOtherBank);
		
		if (payeeMortgage != null && AccountUtils.getIsTransactional(profile).equalsIgnoreCase("Y")) {
			addNewPayee(request, model, userSession, customerService, profile, account, payeeMortgage);
		}
		
		if (payeeMortgageOtherBank != null) {
			addNewPayee(request, model, userSession, customerService, profile, account, payeeMortgageOtherBank);
		}
	}

	private void addNewPayee(HttpServletRequest request, Map<String, Object> model, UserSession userSession,
			CustomerServices customerService, CustomerProfile profile, TVFrontendAccount account,
			String payeeMortgage) {
		AddPayeeResponse res = null;
		String billingAct;
		String nick;

		billingAct = account.getAccountNumber();
		nick = account.getNickname();
		
		GlobalPayee global = PaymentsFacade.getInstance().getGlobalPayeeById(new Integer(payeeMortgage), userSession);
		
		boolean isHighRiskPayee = false;
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1701)) {
			PaymentsFacade.getInstance().getGlobalPayeePreference(global, userSession);

			if (global.getPreference() != null)
			isHighRiskPayee = global.getPreference().getHighRisk();
		}
		res = customerService.addCustomerPayeeWithoutEbillWithErrorDescription(profile,
				userSession.getBankingSession(), global, billingAct, nick,
				Utils.generateCustomerInteractionEvent(request), null, null, null, null, false, isHighRiskPayee);
		if (res.getErrorEBA() != null && !res.getErrorEBA().equals("EBP066E")) {
			if (res.getErrorCode() != 0) {
				String er = getTelepagoErrorDescriptor(res.getErrorEBA(),
						Utils.getRSALanguage(request).toLowerCase(), res.getErrorString());
				model.put("ERROR", er);
			}
		}
	}
	
	private void updatePayees(HttpServletRequest request) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		if (!userSession.isLoadebpp())
			userSession.resetPayees();

		userSession.getCustomerService().setEBPPStatus(this.getEBPPMessageSourceStatus());
		userSession.getPayees(true);
		userSession.getEbpp2UserAccounts();

	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {

		CCPortalPayments mlaMakePayment = (CCPortalPayments) command;
		String ccAction = request.getParameter("formAction");
		boolean allFieldsReqSingle = false;
		if (ccAction.equalsIgnoreCase("singlepay")) {
			if (mlaMakePayment.getSinglePayAccountFrom().equalsIgnoreCase("none") && !allFieldsReqSingle) {
				allFieldsReqSingle = true;
				errors.rejectValue("errorSinglePayment", "credit.card.portal.tab.cc.services.fields.required",
						"All Fields Are Required.");
			}
			if (mlaMakePayment.getSinglePayDate() != null) {
				Calendar today = Calendar.getInstance();

				// Set the max date (one year in future)
				Calendar thresholdDate = Calendar.getInstance();
				thresholdDate.add(Calendar.YEAR, 1);

				// Validate no future dates
				Calendar n = Calendar.getInstance();
				n.setTime(mlaMakePayment.getSinglePayDate());

				if ((n.get(Calendar.YEAR) < today.get(Calendar.YEAR))
						|| (n.get(Calendar.YEAR) == today.get(Calendar.YEAR))
								&& (n.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR))) {
					errors.rejectValue("errorSinglePayment", "credit.card.portal.tab.make.payment.single.past",
							"Invalid Date.<br>(Day in the past)");
				} else if (n.after(thresholdDate)) {
					errors.rejectValue("errorSinglePayment", "credit.card.portal.tab.make.payment.single.future",
							"Invalid Date.<br>(Date more than one year in future)");
				}
			} else if (!allFieldsReqSingle) {
				allFieldsReqSingle = true;
				errors.rejectValue("errorSinglePayment", "credit.card.portal.tab.cc.services.fields.required",
						"All Fields Are Required.");
			}
			if (mlaMakePayment.getSinglePayAmount() != null) {
				if (mlaMakePayment.getSinglePayAmount().compareTo(new BigDecimal(0)) <= 0) {
					errors.rejectValue("errorSinglePayment", "payments.amt.invalid", "Amount Invalid");
				}
			} else if (!allFieldsReqSingle) {
				allFieldsReqSingle = true;
				errors.rejectValue("errorSinglePayment", "credit.card.portal.tab.cc.services.fields.required",
						"All Fields Are Required.");
			}
		}

	}

	/**
	 * @param messageSource
	 *            the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	public evertec.cibp.vs.services.TransactionVaultServices getVaultService() {
		return vaultService;
	}

	public void setVaultService(evertec.cibp.vs.services.TransactionVaultServices vaultService) {
		this.vaultService = vaultService;
	}
	
	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

	/*
	 * CIBP-1680 bgarcia Obtiene el texto de error actualizado de los archivo
	 */
	private String getTelepagoErrorDescriptor(String codigo, String len, String errorCode) {
		String status = "";
		try {
			if (!"en".equals(len))
				len = "es";

			status = this.messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
		} catch (Exception e) {
			status = errorCode;
		}

		return status;
	}

	public String getJsonPayees() {
		return jsonPayees;
	}

	public void setJsonPayees(String jsonPayees) {
		this.jsonPayees = jsonPayees;
	}

	public String getJsonPayeesOtherBank() {
		return jsonPayeesOtherBank;
	}

	public void setJsonPayeesOtherBank(String jsonPayeesOtherBank) {
		this.jsonPayeesOtherBank = jsonPayeesOtherBank;
	}
	
	
}
