package evertec.cibp.web.form.bean;

import java.math.BigDecimal;
import java.util.Date;

public class ModTransferBean {

	private BigDecimal modAmount;
	private Date modDate;
	private String modFreq;
	private String modInstances;
	private String modTransferId;
	private String modInstancesType;
	private Date pendingDate;

	public Date getPendingDate() {
		return pendingDate;
	}

	public void setPendingDate(Date pendingDate) {
		this.pendingDate = pendingDate;
	}

	public String getModInstances() {
		return modInstances;
	}

	public void setModInstances(String modInstances) {
		this.modInstances = modInstances;
	}

	public BigDecimal getModAmount() {
		return modAmount;
	}

	public void setModAmount(BigDecimal modAmount) {
		this.modAmount = modAmount;
	}

	public String getModFreq() {
		return modFreq;
	}

	public void setModFreq(String modFreq) {
		this.modFreq = modFreq;
	}

	public String getModTransferId() {
		return modTransferId;
	}

	public void setModTransferId(String modTransferId) {
		this.modTransferId = modTransferId;
	}

	public String getModInstancesType() {
		return modInstancesType;
	}

	public void setModInstancesType(String modInstancesType) {
		this.modInstancesType = modInstancesType;
	}

	public Date getModDate() {
		return modDate;
	}

	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}

}
