package evertec.cibp.web.form.bean;

public class PaymentDisplayHistoryBean {
	private String id;
	private String favid;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFavid() {
		return favid;
	}

	public void setFavid(String favid) {
		this.favid = favid;
	}

}