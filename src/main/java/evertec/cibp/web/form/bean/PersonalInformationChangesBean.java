package evertec.cibp.web.form.bean;

public class PersonalInformationChangesBean {
	private boolean passwordChangeEmail;
	private boolean passwordChangeMobile;

	private boolean blockedPasswordEmail;
	private boolean blockedPasswordMobile;

	private boolean blockedSecretQuestionsEmail;
	private boolean blockedSecretQuestionsMobile;

	private boolean secretQuestionsUpdatedEmail;
	private boolean secretQuestionsUpdatedMobile;

	private boolean emailUpdatedEmail;
	private boolean emailUpdatedMobile;

	private boolean phoneNumberUpdatedEmail;
	private boolean phoneNumberUpdatedMobile;

	private boolean addressUpdatedEmail;
	private boolean addressUpdatedMobile;

	private boolean mobileBankingPhoneNumberUpdatedEmail;
	private boolean mobileBankingPhoneNumberUpdatedMobile;
	private String action;

	private boolean newDeviceEmail;
	private boolean newDeviceMobile;
	
	private boolean newFingerprintEmail;
	private boolean newFingerprintMobile;

	//Push Actions
	private boolean passwordChangePush;
	private boolean blockedPasswordPush;
	private boolean blockedSecretQuestionsPush;
	private boolean secretQuestionsUpdatedPush;
	private boolean emailUpdatedPush;
	private boolean phoneNumberUpdatedPush;
	private boolean addressUpdatedPush;
	private boolean mobileBankingPhoneNumberUpdatedPush;
	private boolean newDevicePush;
	private boolean newFingerprintPush;
	
	
	public boolean isPasswordChangeEmail() {
		return passwordChangeEmail;
	}

	public void setPasswordChangeEmail(boolean passwordChangeEmail) {
		this.passwordChangeEmail = passwordChangeEmail;
	}

	public boolean isPasswordChangeMobile() {
		return passwordChangeMobile;
	}

	public void setPasswordChangeMobile(boolean passwordChangeMobile) {
		this.passwordChangeMobile = passwordChangeMobile;
	}

	public boolean isBlockedPasswordEmail() {
		return blockedPasswordEmail;
	}

	public void setBlockedPasswordEmail(boolean blockedPasswordEmail) {
		this.blockedPasswordEmail = blockedPasswordEmail;
	}

	public boolean isBlockedPasswordMobile() {
		return blockedPasswordMobile;
	}

	public void setBlockedPasswordMobile(boolean blockedPasswordMobile) {
		this.blockedPasswordMobile = blockedPasswordMobile;
	}

	public boolean isBlockedSecretQuestionsEmail() {
		return blockedSecretQuestionsEmail;
	}

	public void setBlockedSecretQuestionsEmail(boolean blockedSecretQuestionsEmail) {
		this.blockedSecretQuestionsEmail = blockedSecretQuestionsEmail;
	}

	public boolean isBlockedSecretQuestionsMobile() {
		return blockedSecretQuestionsMobile;
	}

	public void setBlockedSecretQuestionsMobile(boolean blockedSecretQuestionsMobile) {
		this.blockedSecretQuestionsMobile = blockedSecretQuestionsMobile;
	}

	public boolean isSecretQuestionsUpdatedEmail() {
		return secretQuestionsUpdatedEmail;
	}

	public void setSecretQuestionsUpdatedEmail(boolean secretQuestionsUpdatedEmail) {
		this.secretQuestionsUpdatedEmail = secretQuestionsUpdatedEmail;
	}

	public boolean isSecretQuestionsUpdatedMobile() {
		return secretQuestionsUpdatedMobile;
	}

	public void setSecretQuestionsUpdatedMobile(boolean secretQuestionsUpdatedMobile) {
		this.secretQuestionsUpdatedMobile = secretQuestionsUpdatedMobile;
	}

	public boolean isEmailUpdatedEmail() {
		return emailUpdatedEmail;
	}

	public void setEmailUpdatedEmail(boolean emailUpdatedEmail) {
		this.emailUpdatedEmail = emailUpdatedEmail;
	}

	public boolean isEmailUpdatedMobile() {
		return emailUpdatedMobile;
	}

	public void setEmailUpdatedMobile(boolean emailUpdatedMobile) {
		this.emailUpdatedMobile = emailUpdatedMobile;
	}

	public boolean isPhoneNumberUpdatedEmail() {
		return phoneNumberUpdatedEmail;
	}

	public void setPhoneNumberUpdatedEmail(boolean phoneNumberUpdatedEmail) {
		this.phoneNumberUpdatedEmail = phoneNumberUpdatedEmail;
	}

	public boolean isPhoneNumberUpdatedMobile() {
		return phoneNumberUpdatedMobile;
	}

	public void setPhoneNumberUpdatedMobile(boolean phoneNumberUpdatedMobile) {
		this.phoneNumberUpdatedMobile = phoneNumberUpdatedMobile;
	}

	public boolean isAddressUpdatedEmail() {
		return addressUpdatedEmail;
	}

	public void setAddressUpdatedEmail(boolean addressUpdatedEmail) {
		this.addressUpdatedEmail = addressUpdatedEmail;
	}

	public boolean isAddressUpdatedMobile() {
		return addressUpdatedMobile;
	}

	public void setAddressUpdatedMobile(boolean addressUpdatedMobile) {
		this.addressUpdatedMobile = addressUpdatedMobile;
	}

	public boolean isMobileBankingPhoneNumberUpdatedEmail() {
		return mobileBankingPhoneNumberUpdatedEmail;
	}

	public void setMobileBankingPhoneNumberUpdatedEmail(boolean mobileBankingPhoneNumberUpdatedEmail) {
		this.mobileBankingPhoneNumberUpdatedEmail = mobileBankingPhoneNumberUpdatedEmail;
	}

	public boolean isMobileBankingPhoneNumberUpdatedMobile() {
		return mobileBankingPhoneNumberUpdatedMobile;
	}

	public void setMobileBankingPhoneNumberUpdatedMobile(boolean mobileBankingPhoneNumberUpdatedMobile) {
		this.mobileBankingPhoneNumberUpdatedMobile = mobileBankingPhoneNumberUpdatedMobile;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public boolean isNewDeviceEmail() {
		return newDeviceEmail;
	}

	public void setNewDeviceEmail(boolean newDeviceEmail) {
		this.newDeviceEmail = newDeviceEmail;
	}

	public boolean isNewDeviceMobile() {
		return newDeviceMobile;
	}

	public void setNewDeviceMobile(boolean newDeviceMobile) {
		this.newDeviceMobile = newDeviceMobile;
	}

	public boolean isNewFingerprintEmail() {
		return newFingerprintEmail;
	}

	public void setNewFingerprintEmail(boolean newFingerprintEmail) {
		this.newFingerprintEmail = newFingerprintEmail;
	}

	public boolean isNewFingerprintMobile() {
		return newFingerprintMobile;
	}

	public void setNewFingerprintMobile(boolean newFingerprintMobile) {
		this.newFingerprintMobile = newFingerprintMobile;
	}

	public boolean isPasswordChangePush() {
		return passwordChangePush;
	}

	public void setPasswordChangePush(boolean passwordChangePush) {
		this.passwordChangePush = passwordChangePush;
	}

	public boolean isBlockedPasswordPush() {
		return blockedPasswordPush;
	}

	public void setBlockedPasswordPush(boolean blockedPasswordPush) {
		this.blockedPasswordPush = blockedPasswordPush;
	}

	public boolean isBlockedSecretQuestionsPush() {
		return blockedSecretQuestionsPush;
	}

	public void setBlockedSecretQuestionsPush(boolean blockedSecretQuestionsPush) {
		this.blockedSecretQuestionsPush = blockedSecretQuestionsPush;
	}

	public boolean isSecretQuestionsUpdatedPush() {
		return secretQuestionsUpdatedPush;
	}

	public void setSecretQuestionsUpdatedPush(boolean secretQuestionsUpdatedPush) {
		this.secretQuestionsUpdatedPush = secretQuestionsUpdatedPush;
	}

	public boolean isEmailUpdatedPush() {
		return emailUpdatedPush;
	}

	public void setEmailUpdatedPush(boolean emailUpdatedPush) {
		this.emailUpdatedPush = emailUpdatedPush;
	}

	public boolean isPhoneNumberUpdatedPush() {
		return phoneNumberUpdatedPush;
	}

	public void setPhoneNumberUpdatedPush(boolean phoneNumberUpdatedPush) {
		this.phoneNumberUpdatedPush = phoneNumberUpdatedPush;
	}

	public boolean isAddressUpdatedPush() {
		return addressUpdatedPush;
	}

	public void setAddressUpdatedPush(boolean addressUpdatedPush) {
		this.addressUpdatedPush = addressUpdatedPush;
	}

	public boolean isMobileBankingPhoneNumberUpdatedPush() {
		return mobileBankingPhoneNumberUpdatedPush;
	}

	public void setMobileBankingPhoneNumberUpdatedPush(boolean mobileBankingPhoneNumberUpdatedPush) {
		this.mobileBankingPhoneNumberUpdatedPush = mobileBankingPhoneNumberUpdatedPush;
	}

	public boolean isNewDevicePush() {
		return newDevicePush;
	}

	public void setNewDevicePush(boolean newDevicePush) {
		this.newDevicePush = newDevicePush;
	}

	public boolean isNewFingerprintPush() {
		return newFingerprintPush;
	}

	public void setNewFingerprintPush(boolean newFingerprintPush) {
		this.newFingerprintPush = newFingerprintPush;
	}
	
}
