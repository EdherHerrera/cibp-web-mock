package evertec.cibp.web.form.bean;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.list.LazyList;

public class TransfersListBean {
	private List<Object> transfers;
	private List<Object> favorites;

	@SuppressWarnings("unchecked")
	public TransfersListBean() {
		transfers = LazyList.decorate(new ArrayList(), new Factory() {
			public Object create() {
				return new TransferBean();
			}
		});
		favorites = LazyList.decorate(new ArrayList(), new Factory() {
			public Object create() {
				return new TransferBean();
			}
		});
	}

	public List<Object> getTransfers() {
		return transfers;
	}

	public void setTransfers(List<Object> list) {
		this.transfers = list;
	}

	public List<Object> getFavorites() {
		return favorites;
	}

	public void setFavorites(List<Object> favorites) {
		this.favorites = favorites;
	}

}