package evertec.cibp.web.form.bean;

public class EnrollmentComercialBean {
	
	private String agreement;
	
	public String getAgreement() {
		return agreement;
	}

	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}

}
