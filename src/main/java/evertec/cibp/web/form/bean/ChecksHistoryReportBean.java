package evertec.cibp.web.form.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import evertec.cibp.core.models.fraud.prevention.IncomingChecks;
import evertec.cibp.core.models.fraud.prevention.IncomingChecksDecision;

public class ChecksHistoryReportBean {
	
	private Date dateTo;
	private Date dateFrom;
	private String account;
	private String nickName;
	private String accountMask;
	private List<IncomingChecks> checksList;
	private int checkSize;
	private List<IncomingChecksDecision> decisionList;
	private List<String> checkNumberList;
	private List<BigDecimal> amount;
	private List<String> issueDate;
	private List<IncomingChecksDecision> decision;
	private List<Boolean> isDefault;
	private int cantDec;
	private List<String> incoCheckIdList;
	private boolean showLoadMore;
	
	public List<Boolean> getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(List<Boolean> isDefault) {
		this.isDefault = isDefault;
	}
	public Date getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Date getDateTo() {
		return dateTo;
	}
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getAccountMask() {
		return accountMask;
	}
	public void setAccountMask(String accountMask) {
		this.accountMask = accountMask;
	}
	public List<IncomingChecks> getChecksList() {
		return checksList;
	}
	public void setChecksList(List<IncomingChecks> checksList) {
		this.checksList = checksList;
	}
	public int getCheckSize() {
		return checkSize;
	}
	public void setCheckSize(int checkSize) {
		this.checkSize = checkSize;
	}
	public List<IncomingChecksDecision> getDecisionList() {
		return decisionList;
	}
	public void setDecisionList(List<IncomingChecksDecision> decisionList) {
		this.decisionList = decisionList;
	}
	
	public List<String> getCheckNumberList() {
		return checkNumberList;
	}
	
	public void setCheckNumberList(List<String> checkNumberList) {
		this.checkNumberList = checkNumberList;
	}
	public List<BigDecimal> getAmount() {
		return amount;
	}
	public void setAmount(List<BigDecimal> amount) {
		this.amount = amount;
	}
	public List<String> getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(List<String> issueDate) {
		this.issueDate = issueDate;
	}
	public List<IncomingChecksDecision> getDecision() {
		return decision;
	}
	public void setDecision(List<IncomingChecksDecision> decision) {
		this.decision = decision;
	}
	public int getCantDec() {
		return cantDec;
	}
	public void setCantDec(int cantDec) {
		this.cantDec = cantDec;
	}
	public List<String> getIncoCheckIdList() {
		return incoCheckIdList;
	}
	public void setIncoCheckIdList(List<String> incoCheckIdList) {
		this.incoCheckIdList = incoCheckIdList;
	}
	public boolean isShowLoadMore() {
		return showLoadMore;
	}
	public void setShowLoadMore(boolean showLoadMore) {
		this.showLoadMore = showLoadMore;
	}

}
