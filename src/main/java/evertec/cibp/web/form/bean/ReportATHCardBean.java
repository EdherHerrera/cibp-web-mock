package evertec.cibp.web.form.bean;

import java.util.Date;

public class ReportATHCardBean {
	
	private String account;
	private String cardNumber;
	private String reason;
	
	private boolean policeNotified;
	private String disputeNumber;
	private String placeOfNotification;
	private Date dateOfNotification;
	
	private Date lastPurchaseDate;
	private String lastPurchaseLocation;
	private String lastPurchaseCity;
		
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public boolean isPoliceNotified() {
		return policeNotified;
	}
	public void setPoliceNotified(boolean policeNotified) {
		this.policeNotified = policeNotified;
	}
	public String getDisputeNumber() {
		return disputeNumber;
	}
	public void setDisputeNumber(String disputeNumber) {
		this.disputeNumber = disputeNumber;
	}
	public String getPlaceOfNotification() {
		return placeOfNotification;
	}
	public void setPlaceOfNotification(String placeOfNotification) {
		this.placeOfNotification = placeOfNotification;
	}
	public Date getDateOfNotification() {
		return dateOfNotification;
	}
	public void setDateOfNotification(Date dateOfNotification) {
		this.dateOfNotification = dateOfNotification;
	}
	public Date getLastPurchaseDate() {
		return lastPurchaseDate;
	}
	public void setLastPurchaseDate(Date lastPurchaseDate) {
		this.lastPurchaseDate = lastPurchaseDate;
	}
	public String getLastPurchaseLocation() {
		return lastPurchaseLocation;
	}
	public void setLastPurchaseLocation(String lastPurchaseLocation) {
		this.lastPurchaseLocation = lastPurchaseLocation;
	}
	public String getLastPurchaseCity() {
		return lastPurchaseCity;
	}
	public void setLastPurchaseCity(String lastPurchaseCity) {
		this.lastPurchaseCity = lastPurchaseCity;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

}
