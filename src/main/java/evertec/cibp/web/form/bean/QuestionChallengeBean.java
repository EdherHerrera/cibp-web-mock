package evertec.cibp.web.form.bean;

public class QuestionChallengeBean {
	private String answer, pm_fp;
	private boolean remember;

	/**
	 * New implementation RSA SDK Mobile
	 */
	private String device_info_rsa; //
	
	private String rsa_cookie;

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getPm_fp() {
		return pm_fp;
	}

	public void setPm_fp(String pm_fp) {
		this.pm_fp = pm_fp;
	}

	public boolean isRemember() {
		return remember;
	}

	public void setRemember(boolean remember) {
		this.remember = remember;
	}
	
	/**
	 * getDeviceInfoRsa
	 * @return String
	 */
	public String getDevice_info_rsa () {
		return device_info_rsa;
	}
	
	/**
	 * setDevideInfoRsa
	 * @param strDeviceInfoRsa
	 */
	public void setDevice_info_rsa (String strDeviceInfoRsa) {
		this.device_info_rsa = strDeviceInfoRsa;
	}
	
	/**
	 * @return the rewardPoints
	 */
	public String getRsa_cookie() {
		return rsa_cookie;
	}

	/**
	 * @param rewardPoints the rewardPoints to set
	 */
	public void setRsa_cookie(String strRsaCookie) {
		this.rsa_cookie = strRsaCookie;
	}

}