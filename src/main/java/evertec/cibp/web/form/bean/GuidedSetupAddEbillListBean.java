/**
 * 
 */
package evertec.cibp.web.form.bean;

import java.util.List;

/**
 * Clase|Interfaz evertec.cibp.web.form.bean.GuidedSetupAddEbillListBean
 * Sistema: TODO Descripcion: TODO
 * 
 * @author Ing. Michael Picado Fuentes Creada el Mar 15, 2011 3:23:20 PM
 */

public class GuidedSetupAddEbillListBean {

	private List<GuidedSetupAddEbillBean> listAddEbillBeans;

	/* Constructor de la clase GuidedSetupAddEbillListBean */
	public GuidedSetupAddEbillListBean() {
		// TODO Auto-generated constructor stub
	}

	/** @return regresa el listAddEbillBeans */
	public List<GuidedSetupAddEbillBean> getListAddEbillBeans() {
		return listAddEbillBeans;
	}

	/**
	 * @param listAddEbillBeans
	 *            el listAddEbillBeans a establecer
	 */
	public void setListAddEbillBeans(List<GuidedSetupAddEbillBean> listAddEbillBeans) {
		this.listAddEbillBeans = listAddEbillBeans;
	}

}
