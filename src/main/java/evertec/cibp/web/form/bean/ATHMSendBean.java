package evertec.cibp.web.form.bean;

public class ATHMSendBean {
	private String _account = null;
	private String _displayAccount = null;
	private String _friend = null;
	private String _phoneNumber = null;
	private String _message = null;
	private String _referenceNumber = null;

	private String _amount = null;

	private Boolean _portal = null;

	private String error;
	private boolean errorFound = false;
  
	private String mobileAppType;
	private boolean mobileApp;

	public String getAccount() {
		return _account;
	}

	public void setAccount(String account) {
		_account = account;
	}

	public String getDisplayAccount() {
		return _displayAccount;
	}

	public void setDisplayAccount(String displayAccount) {
		_displayAccount = displayAccount;
	}

	public String getFriend() {
		return _friend;
	}

	public void setFriend(String friend) {
		_friend = friend;
	}

	public String getPhoneNumber() {
		return _phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		_phoneNumber = phoneNumber;
	}

	public String getMessage() {
		return _message;
	}

	public void setMessage(String message) {
		_message = message;
	}

	public String getReferenceNumber() {
		return _referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		_referenceNumber = referenceNumber;
	}

	public String getAmount() {
		return _amount;
	}

	public void setAmount(String amount) {
		_amount = amount;
	}

	public Boolean getPortal() {
		return _portal;
	}

	public void setPortal(Boolean portal) {
		_portal = portal;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
  
  public String getMobileAppType() {
		return mobileAppType;
	}

	public void setMobileAppType(String mobileAppType) {
		this.mobileAppType = mobileAppType;
	}

	public boolean isMobileApp() {
		return mobileApp;
	}

	public void setMobileApp(boolean mobileApp) {
		this.mobileApp = mobileApp;
	}

	public boolean isErrorFound() {
		return errorFound;
	}

	public void setErrorFound(boolean errorFound) {
		this.errorFound = errorFound;
	}

}
