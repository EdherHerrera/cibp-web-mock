package evertec.cibp.web.form.bean;

import evertec.cibp.core.models.customer.applications.AddressApplication.AppliedToAccount;

public class ModifyAddressBean {
	
//	This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//	{
	private String residentialAddress1;
	private String residentialAddress2;
	private String residentialCity;
	private String residentialState;
	private String residentialZipCode1;
	private String residentialZipCode2;

	private String oldResidentialAddress1;
	private String oldResidentialAddress2;
	private String oldResidentialCity;
	private String oldResidentialState;
	private String oldResidentialZipCode1;
	private String oldResidentialZipCode2;
	
	private String addressChangeType;
//	}

	private String newPostalAddress1;
	private String newPostalAddress2;
	private String newPostalCity;
	private String newPostalState;
	private String newPostalZipCode1;
	private String newPostalZipCode2;

	private String oldPostalAddress1;
	private String oldPostalAddress2;
	private String oldPostalCity;
	private String oldPostalState;
	private String oldPostalZipCode1;
	private String oldPostalZipCode2;

	private AppliedToAccount appliedTo;
	private String[] accounts;

	public String getNewPostalAddress1() {
		return newPostalAddress1;
	}

	public void setNewPostalAddress1(String newPostalAddress1) {
		this.newPostalAddress1 = newPostalAddress1;
	}

	public String getNewPostalAddress2() {
		return newPostalAddress2;
	}

	public void setNewPostalAddress2(String newPostalAddress2) {
		this.newPostalAddress2 = newPostalAddress2;
	}

	public String getNewPostalCity() {
		return newPostalCity;
	}

	public void setNewPostalCity(String newPostalCity) {
		this.newPostalCity = newPostalCity;
	}

	public String getNewPostalState() {
		return newPostalState;
	}

	public void setNewPostalState(String newPostalState) {
		this.newPostalState = newPostalState;
	}

	public String getNewPostalZipCode1() {
		return newPostalZipCode1;
	}

	public void setNewPostalZipCode1(String newPostalZipCode1) {
		this.newPostalZipCode1 = newPostalZipCode1;
	}

	public String getNewPostalZipCode2() {
		return newPostalZipCode2;
	}

	public void setNewPostalZipCode2(String newPostalZipCode2) {
		this.newPostalZipCode2 = newPostalZipCode2;
	}

	public String getOldPostalAddress1() {
		return oldPostalAddress1;
	}

	public void setOldPostalAddress1(String oldPostalAddress1) {
		this.oldPostalAddress1 = oldPostalAddress1;
	}

	public String getOldPostalAddress2() {
		return oldPostalAddress2;
	}

	public void setOldPostalAddress2(String oldPostalAddress2) {
		this.oldPostalAddress2 = oldPostalAddress2;
	}

	public String getOldPostalCity() {
		return oldPostalCity;
	}

	public void setOldPostalCity(String oldPostalCity) {
		this.oldPostalCity = oldPostalCity;
	}

	public String getOldPostalState() {
		return oldPostalState;
	}

	public void setOldPostalState(String oldPostalState) {
		this.oldPostalState = oldPostalState;
	}

	public String getOldPostalZipCode1() {
		return oldPostalZipCode1;
	}

	public void setOldPostalZipCode1(String oldPostalZipCode1) {
		this.oldPostalZipCode1 = oldPostalZipCode1;
	}

	public String getOldPostalZipCode2() {
		return oldPostalZipCode2;
	}

	public void setOldPostalZipCode2(String oldPostalZipCode2) {
		this.oldPostalZipCode2 = oldPostalZipCode2;
	}

	public AppliedToAccount getAppliedTo() {
		return appliedTo;
	}

	public void setAppliedTo(AppliedToAccount appliedTo) {
		this.appliedTo = appliedTo;
	}

	public String[] getAccounts() {
		return accounts;
	}

	public void setAccounts(String[] accounts) {
		this.accounts = accounts;
	}
	
//	This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//	{
	public String getResidentialAddress1() {
		return residentialAddress1;
	}

	public void setResidentialAddress1(String residentialAddress1) {
		this.residentialAddress1 = residentialAddress1;
	}

	public String getResidentialAddress2() {
		return residentialAddress2;
	}

	public void setResidentialAddress2(String residentialAddress2) {
		this.residentialAddress2 = residentialAddress2;
	}

	public String getResidentialCity() {
		return residentialCity;
	}

	public void setResidentialCity(String residentialCity) {
		this.residentialCity = residentialCity;
	}

	public String getResidentialState() {
		return residentialState;
	}

	public void setResidentialState(String residentialState) {
		this.residentialState = residentialState;
	}

	public String getResidentialZipCode1() {
		return residentialZipCode1;
	}

	public void setResidentialZipCode1(String residentialZipCode1) {
		this.residentialZipCode1 = residentialZipCode1;
	}

	public String getResidentialZipCode2() {
		return residentialZipCode2;
	}

	public void setResidentialZipCode2(String residentialZipCode2) {
		this.residentialZipCode2 = residentialZipCode2;
	}

	public String getOldResidentialAddress1() {
		return oldResidentialAddress1;
	}

	public void setOldResidentialAddress1(String oldResidentialAddress1) {
		this.oldResidentialAddress1 = oldResidentialAddress1;
	}

	public String getOldResidentialAddress2() {
		return oldResidentialAddress2;
	}

	public void setOldResidentialAddress2(String oldResidentialAddress2) {
		this.oldResidentialAddress2 = oldResidentialAddress2;
	}

	public String getOldResidentialCity() {
		return oldResidentialCity;
	}

	public void setOldResidentialCity(String oldResidentialCity) {
		this.oldResidentialCity = oldResidentialCity;
	}

	public String getOldResidentialState() {
		return oldResidentialState;
	}

	public void setOldResidentialState(String oldResidentialState) {
		this.oldResidentialState = oldResidentialState;
	}

	public String getOldResidentialZipCode1() {
		return oldResidentialZipCode1;
	}

	public void setOldResidentialZipCode1(String oldResidentialZipCode1) {
		this.oldResidentialZipCode1 = oldResidentialZipCode1;
	}

	public String getOldResidentialZipCode2() {
		return oldResidentialZipCode2;
	}

	public void setOldResidentialZipCode2(String oldResidentialZipCode2) {
		this.oldResidentialZipCode2 = oldResidentialZipCode2;
	}

	public String getAddressChangeType() {
		return addressChangeType;
	}

	public void setAddressChangeType(String addressChangeType) {
		this.addressChangeType = addressChangeType;
	}
//	}

}
