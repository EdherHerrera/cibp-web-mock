package evertec.cibp.web.form.bean;

import java.math.BigDecimal;
import java.util.Date;

public class AccountServiceBean {

	public enum ACTION {
		CHECKCOPY, STATEMENTCOPY, STOPCHECK, ORDERCHECK
	};

	private ACTION action;
	private String account;
	private String checkNumber;
	private Date checkDate;
	private BigDecimal checkAmount;
	private boolean acceptCharges;
	private String agreement;

	private int stmtStartday;
	private int stmtStartmonth;
	private int stmtStartyear;
	private int stmtEndday;
	private int stmtEndmonth;
	private int stmtEndyear;
	private String statementId;
	private String rangeBy;

	private String stopCheckNumber;

	private String athType;
	private String athNumber;
	private String expdate;
	private boolean formError = false;
	
	//MBC-382
	private String bsmartAccount;

	public void setBsmartAccount(String bsmartAccount) {
		this.bsmartAccount = bsmartAccount;
	}
	
	public String getBsmartAccount() {
		return bsmartAccount;
	}
	
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public BigDecimal getCheckAmount() {
		return checkAmount;
	}

	public void setCheckAmount(BigDecimal checkAmount) {
		this.checkAmount = checkAmount;
	}

	public boolean isAcceptCharges() {
		return acceptCharges;
	}

	public void setAcceptCharges(boolean acceptCharges) {
		this.acceptCharges = acceptCharges;
	}

	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}

	public int getStmtStartday() {
		return stmtStartday;
	}

	public void setStmtStartday(int stmtStartday) {
		this.stmtStartday = stmtStartday;
	}

	public int getStmtStartmonth() {
		return stmtStartmonth;
	}

	public void setStmtStartmonth(int stmtStartmonth) {
		this.stmtStartmonth = stmtStartmonth;
	}

	public int getStmtStartyear() {
		return stmtStartyear;
	}

	public void setStmtStartyear(int stmtStartyear) {
		this.stmtStartyear = stmtStartyear;
	}

	public int getStmtEndday() {
		return stmtEndday;
	}

	public void setStmtEndday(int stmtEndday) {
		this.stmtEndday = stmtEndday;
	}

	public int getStmtEndmonth() {
		return stmtEndmonth;
	}

	public void setStmtEndmonth(int stmtEndmonth) {
		this.stmtEndmonth = stmtEndmonth;
	}

	public int getStmtEndyear() {
		return stmtEndyear;
	}

	public void setStmtEndyear(int stmtEndyear) {
		this.stmtEndyear = stmtEndyear;
	}

	public String getStatementId() {
		return statementId;
	}

	public void setStatementId(String statementId) {
		this.statementId = statementId;
	}

	public String getStopCheckNumber() {
		return stopCheckNumber;
	}

	public void setStopCheckNumber(String stopCheckNumber) {
		this.stopCheckNumber = stopCheckNumber;
	}

	public String getAgreement() {
		return agreement;
	}

	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}

	public String getAthType() {
		return athType;
	}

	public void setAthType(String athType) {
		this.athType = athType;
	}

	public String getAthNumber() {
		return athNumber;
	}

	public void setAthNumber(String athNumber) {
		this.athNumber = athNumber;
	}

	public String getExpdate() {
		return expdate;
	}

	public void setExpdate(String expdate) {
		this.expdate = expdate;
	}

	public String getRangeBy() {
		return rangeBy;
	}

	public void setRangeBy(String rangeBy) {
		this.rangeBy = rangeBy;
	}

	/**
	 * @param formError
	 *            the formError to set
	 */
	public void setFormError(boolean formError) {
		this.formError = formError;
	}

	/**
	 * @return the formError
	 */
	public boolean getFormError() {
		return formError;
	}

}
