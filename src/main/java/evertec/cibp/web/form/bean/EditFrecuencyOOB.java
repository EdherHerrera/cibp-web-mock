package evertec.cibp.web.form.bean;

public class EditFrecuencyOOB {
	private String frecuency;

	public String getFrecuency() {
		return frecuency;
	}

	public void setFrecuency(String frecuency) {
		this.frecuency = frecuency;
	}
}
