/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evertec.cibp.web.form.bean;

import evertec.cibp.core.models.account.TVFrontendAccount;

/**
 * 
 * @author et23558
 */
public class RegEAccountBean {

	private TVFrontendAccount frontendAccount;
	private String selected = "";
	private String activationStatus;

	public RegEAccountBean(TVFrontendAccount frontendAccount) {
		this.frontendAccount = frontendAccount;
	}

	/**
	 * @return the frontendAccount
	 */
	public TVFrontendAccount getFrontendAccount() {
		return frontendAccount;
	}

	/**
	 * @param frontendAccount
	 *            the frontendAccount to set
	 */
	public void setFrontendAccount(TVFrontendAccount frontendAccount) {
		this.frontendAccount = frontendAccount;
	}

	/**
	 * @return the selected
	 */
	public String getSelected() {
		return selected;
	}

	/**
	 * @param selected
	 *            the selected to set
	 */
	public void setSelected(String selected) {
		this.selected = selected;
	}

	/**
	 * @return the activationStatus
	 */
	public String getActivationStatus() {
		return activationStatus;
	}

	/**
	 * @param activationStatus
	 *            the activationStatus to set
	 */
	public void setActivationStatus(String activationStatus) {
		this.activationStatus = activationStatus;
	}
}
