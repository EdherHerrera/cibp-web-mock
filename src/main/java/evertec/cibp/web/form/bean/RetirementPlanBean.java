package evertec.cibp.web.form.bean;

import evertec.relius.retirementplan.ws.wsdl.RetirementPlanInfo;

/**
 * RetirementPlanBean
 * @author s681718
 * @version 1.0 
 */
public class RetirementPlanBean extends RetirementPlanInfo {
	
	/** 
	 * 4 last numbers from account
	 */
	private String accountLast4Num;
	
	public String getAccountLast4Num() {
		return accountLast4Num;
	}
	public void setAccountLast4Num(String accountLast4Num) {
		this.accountLast4Num = accountLast4Num;
	}
    
    
}
