package evertec.cibp.web.form.bean;

public class ATHMResetBean {

	private String _pin = null;
	private String _pinConfirm = null;

	public String getPin() {
		return _pin;
	}

	public void setPin(String pin) {
		_pin = pin;
	}

	public String getPinConfirm() {
		return _pinConfirm;
	}

	public void setPinConfirm(String pinConfirm) {
		_pinConfirm = pinConfirm;
	}
}
