package evertec.cibp.web.form.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import evertec.cibp.vs.beans.Frequency;

public class OpenUSaveBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5362060156626355348L;
	// public enum WITHDRAWALOPTION {SELECT, ONEYEAR, SPECIFICDATE, LATER};
	/*
	 * LATER N //No specific date ONEYEAR A //Annual SPECIFICDATE D //Pay out
	 * Future
	 */
	private String nickname;
	private String withdrawalOption = "";
	private Date withdrawalDate;
	private String withdrawalAccount;
	private BigDecimal initialDepositAmount;
	private String debitFromAccount;
	private boolean recurringDebit = true;
	private BigDecimal recurringDebitAmount;
	private Frequency recurringDebitFrequency;
	private String recurringDebitAccount;
	private Date startRecurringDebitDate;

	private String physicalAddress1;
	private String physicalAddress2;
	private String physicalCity;
	private String physicalState;
	private String physicalZipCode1;
	private String physicalZipCode2;

	private boolean samePhysicalPostalAddress = true;
	private String postalAddress1;
	private String postalAddress2;
	private String postalCity;
	private String postalState;
	private String postalZipCode1;
	private String postalZipCode2;

	private boolean agreeToContract1;
	private boolean agreeToContract2;
	private boolean showDateInWithdrawalDateError = false;

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getWithdrawalOption() {
		return withdrawalOption;
	}

	public void setWithdrawalOption(String withdrawalOption) {
		this.withdrawalOption = withdrawalOption;
	}

	public Date getWithdrawalDate() {
		return withdrawalDate;
	}

	public void setWithdrawalDate(Date withdrawalDate) {
		this.withdrawalDate = withdrawalDate;
	}

	public String getWithdrawalAccount() {
		return withdrawalAccount;
	}

	public void setWithdrawalAccount(String withdrawalAccount) {
		this.withdrawalAccount = withdrawalAccount;
	}

	public BigDecimal getInitialDepositAmount() {
		return initialDepositAmount;
	}

	public void setInitialDepositAmount(BigDecimal initialDepositAmount) {
		this.initialDepositAmount = initialDepositAmount;
	}

	public String getDebitFromAccount() {
		return debitFromAccount;
	}

	public void setDebitFromAccount(String debitFromAccount) {
		this.debitFromAccount = debitFromAccount;
	}

	public boolean getRecurringDebit() {
		return recurringDebit;
	}

	public void setRecurringDebit(boolean recurringDebit) {
		this.recurringDebit = recurringDebit;
	}

	public BigDecimal getRecurringDebitAmount() {
		return recurringDebitAmount;
	}

	public void setRecurringDebitAmount(BigDecimal recurringDebitAmount) {
		this.recurringDebitAmount = recurringDebitAmount;
	}

	public Frequency getRecurringDebitFrequency() {
		return recurringDebitFrequency;
	}

	public void setRecurringDebitFrequency(Frequency recurringDebitFrequency) {
		this.recurringDebitFrequency = recurringDebitFrequency;
	}

	public String getRecurringDebitAccount() {
		return recurringDebitAccount;
	}

	public void setRecurringDebitAccount(String recurringDebitAccount) {
		this.recurringDebitAccount = recurringDebitAccount;
	}

	public Date getStartRecurringDebitDate() {
		return startRecurringDebitDate;
	}

	public void setStartRecurringDebitDate(Date startRecurringDebitDate) {
		this.startRecurringDebitDate = startRecurringDebitDate;
	}

	public String getPhysicalAddress1() {
		return physicalAddress1;
	}

	public void setPhysicalAddress1(String physicalAddress1) {
		this.physicalAddress1 = physicalAddress1;
	}

	public String getPhysicalAddress2() {
		return physicalAddress2;
	}

	public void setPhysicalAddress2(String physicalAddress2) {
		this.physicalAddress2 = physicalAddress2;
	}

	public String getPhysicalCity() {
		return physicalCity;
	}

	public void setPhysicalCity(String physicalCity) {
		this.physicalCity = physicalCity;
	}

	public String getPhysicalState() {
		return physicalState;
	}

	public void setPhysicalState(String physicalState) {
		this.physicalState = physicalState;
	}

	public String getPhysicalZipCode1() {
		return physicalZipCode1;
	}

	public void setPhysicalZipCode1(String physicalZipCode1) {
		this.physicalZipCode1 = physicalZipCode1;
	}

	public String getPhysicalZipCode2() {
		return physicalZipCode2;
	}

	public void setPhysicalZipCode2(String physicalZipCode2) {
		this.physicalZipCode2 = physicalZipCode2;
	}

	public boolean getSamePhysicalPostalAddress() {
		return samePhysicalPostalAddress;
	}

	public void setSamePhysicalPostalAddress(boolean samePhysicalPostalAddress) {
		this.samePhysicalPostalAddress = samePhysicalPostalAddress;
	}

	public String getPostalAddress1() {
		return postalAddress1;
	}

	public void setPostalAddress1(String postalAddress1) {
		this.postalAddress1 = postalAddress1;
	}

	public String getPostalAddress2() {
		return postalAddress2;
	}

	public void setPostalAddress2(String postalAddress2) {
		this.postalAddress2 = postalAddress2;
	}

	public String getPostalCity() {
		return postalCity;
	}

	public void setPostalCity(String postalCity) {
		this.postalCity = postalCity;
	}

	public String getPostalState() {
		return postalState;
	}

	public void setPostalState(String postalState) {
		this.postalState = postalState;
	}

	public String getPostalZipCode1() {
		return postalZipCode1;
	}

	public void setPostalZipCode1(String postalZipCode1) {
		this.postalZipCode1 = postalZipCode1;
	}

	public String getPostalZipCode2() {
		return postalZipCode2;
	}

	public void setPostalZipCode2(String postalZipCode2) {
		this.postalZipCode2 = postalZipCode2;
	}

	public boolean getAgreeToContract1() {
		return agreeToContract1;
	}

	public void setAgreeToContract1(boolean agreeToContract1) {
		this.agreeToContract1 = agreeToContract1;
	}

	public boolean getAgreeToContract2() {
		return agreeToContract2;
	}

	public void setAgreeToContract2(boolean agreeToContract2) {
		this.agreeToContract2 = agreeToContract2;
	}

	public boolean isShowDateInWithdrawalDateError() {
		return showDateInWithdrawalDateError;
	}

	public void setShowDateInWithdrawalDateError(boolean showDateInWithdrawalDateError) {
		this.showDateInWithdrawalDateError = showDateInWithdrawalDateError;
	}
}
