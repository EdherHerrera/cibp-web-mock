package evertec.cibp.web.form.bean;

public class ModifyAthPinBean {

	private String account, cardPlasticNumber, pin, pinconfirm, oldPin;
	private boolean formError = false;

	public String getOldPin() {
		return oldPin;
	}

	public void setOldPin(String oldPin) {
		this.oldPin = oldPin;
	}

	public String getPinconfirm() {
		return pinconfirm;
	}

	public void setPinconfirm(String pinconfirm) {
		this.pinconfirm = pinconfirm;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getCardPlasticNumber() {
		return cardPlasticNumber;
	}

	public void setCardPlasticNumber(String cardPlasticNumber) {
		this.cardPlasticNumber = cardPlasticNumber;
	}
	
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @param formError
	 *            the formError to set
	 */
	public void setFormError(boolean formError) {
		this.formError = formError;
	}

	/**
	 * @return the formError
	 */
	public boolean getFormError() {
		return formError;
	}

}
