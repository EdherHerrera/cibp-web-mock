/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evertec.cibp.web.form.bean;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * @author ET23558
 */
public class CampaignConcurrencyBean {

	protected final Log logger = LogFactory.getLog(this.getClass());
	private static Date lastUpdate = new Date();
	private static int resetTimeLimit = 5;
	private static int concurrencyCounter = 0;
	private static int concurrencyLimit = 0;

	/**
	 * @return the concurrencyCounter
	 */
	public int getConcurrencyCounter() {
		return concurrencyCounter;
	}

	/**
	 * @param aConcurrencyCounter
	 *            the concurrencyCounter to set
	 */
	public void setConcurrencyCounter(int aConcurrencyCounter) {
		concurrencyCounter = aConcurrencyCounter;
	}

	/**
	 * @return the concurrencyLimit
	 */
	public int getConcurrencyLimit() {
		return concurrencyLimit;
	}

	/**
	 * @param aConcurrencyLimit
	 *            the concurrencyLimit to set
	 */
	public void setConcurrencyLimit(int aConcurrencyLimit) {
		concurrencyLimit = aConcurrencyLimit;
	}

	public void addConcurrency() {
		setConcurrencyCounter(getConcurrencyCounter() + 1);
		setLastUpdate(new Date());
		logger.info("Concurrency addition:" + getConcurrencyCounter());
	}

	public void subConcurrency() {
		setConcurrencyCounter(getConcurrencyCounter() - 1);
		setLastUpdate(new Date());
		logger.info("Concurrency subtract:" + getConcurrencyCounter());
	}

	public boolean isConcurrencyAvailable() {
		long time = getResetTimeLimit() * 60 * 1000;
		Date now = new Date();
		long subs = now.getTime() - getLastUpdate().getTime();
		if (subs > time) {
			setConcurrencyCounter(0);
		}
		setLastUpdate(new Date());
		return getConcurrencyCounter() < getConcurrencyLimit();
	}

	/**
	 * @return the lastUpdate
	 */
	public Date getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @param aLastUpdate
	 *            the lastUpdate to set
	 */
	public void setLastUpdate(Date aLastUpdate) {
		lastUpdate = aLastUpdate;
	}

	/**
	 * @return the resetTimeLimit
	 */
	public int getResetTimeLimit() {
		return resetTimeLimit;
	}

	/**
	 * @param aResetTimeLimit
	 *            the resetTimeLimit to set
	 */
	public void setResetTimeLimit(int aResetTimeLimit) {
		resetTimeLimit = aResetTimeLimit;
	}
}
