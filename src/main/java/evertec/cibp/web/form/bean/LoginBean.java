package evertec.cibp.web.form.bean;

public class LoginBean {
	private String username, pm_fp, promo;
	private boolean fromvi;
	
	/**
	 * rewardPoints
	 */
	private String rewardPoints;//
	
	/**
	 * New implementation RSA SDK Mobile
	 */
	private String device_info_rsa; //

	private String rsa_cookie;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		if(username != null)
			this.username = username.toLowerCase();
		else
			this.username = username;
	}

	public String getPm_fp() {
		return pm_fp;
	}

	public void setPm_fp(String pm_fp) {
		this.pm_fp = pm_fp;
	}

	public boolean isFromvi() {
		return fromvi;
	}

	public void setFromvi(boolean fromvi) {
		this.fromvi = fromvi;
	}

	public String getPromo() {
		return promo;
	}

	public void setPromo(String promo) {
		this.promo = promo;
	}
	
	/**
	 * @return the rewardPoints
	 */
	public String getRewardPoints() {
		return rewardPoints;
	}

	/**
	 * @param rewardPoints the rewardPoints to set
	 */
	public void setRewardPoints(String rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	
	/**
	 * getDeviceInfoRsa
	 * @return String
	 */
	public String getDevice_info_rsa () {
		return device_info_rsa;
	}
	
	/**
	 * setDevideInfoRsa
	 * @param strDeviceInfoRsa
	 */
	public void setDevice_info_rsa (String strDeviceInfoRsa) {
		this.device_info_rsa = strDeviceInfoRsa;
	}

	
	/**
	 * @return the rewardPoints
	 */
	public String getRsa_cookie() {
		return rsa_cookie;
	}

	/**
	 * @param rewardPoints the rewardPoints to set
	 */
	public void setRsa_cookie(String strRsaCookie) {
		this.rsa_cookie = strRsaCookie;
	}

}