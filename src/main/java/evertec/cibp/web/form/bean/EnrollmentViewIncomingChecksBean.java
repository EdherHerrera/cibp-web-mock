package evertec.cibp.web.form.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.list.LazyList;
import org.apache.commons.lang.StringUtils;

import evertec.cibp.core.models.customer.applications.ViewIncomingChecksApplication;

public class EnrollmentViewIncomingChecksBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private String phone1;
	private String phone2;
	private String phone3;
	private String businessName;
	private String businessPhone1;
	private String businessPhone2;
	private String businessPhone3;
	private AddressBean residential;
	private AddressBean mailing;
	private String account;
	private String termsConfirmation;

	private String isValid;

	private List<String> addedAccounts;
	private List<EnrollmentViewIncomingCheksAccountListBean> listAccounts;

	@SuppressWarnings("unchecked")
	public EnrollmentViewIncomingChecksBean() {
		this.residential = new AddressBean();
		this.mailing = new AddressBean();
		this.addedAccounts = new ArrayList<>();

		listAccounts = LazyList.decorate(new ArrayList<EnrollmentViewIncomingCheksAccountListBean>(), new Factory() {
			public Object create() {
				return new EnrollmentViewIncomingCheksAccountListBean();
			}
		});
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getBusinessPhone1() {
		return businessPhone1;
	}

	public void setBusinessPhone1(String businessPhone1) {
		this.businessPhone1 = businessPhone1;
	}

	public String getBusinessPhone2() {
		return businessPhone2;
	}

	public void setBusinessPhone2(String businessPhone2) {
		this.businessPhone2 = businessPhone2;
	}

	public AddressBean getResidential() {
		return residential;
	}

	public void setResidential(AddressBean residential) {
		this.residential = residential;
	}

	public AddressBean getMailing() {
		return mailing;
	}

	public void setMailing(AddressBean mailing) {
		this.mailing = mailing;
	}

	public String getTermsConfirmation() {
		return termsConfirmation;
	}

	public void setTermsConfirmation(String termsConfirmation) {
		this.termsConfirmation = termsConfirmation;
	}

	public String getBusinessPhone3() {
		return businessPhone3;
	}

	public void setBusinessPhone3(String businessPhone3) {
		this.businessPhone3 = businessPhone3;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getIsValid() {
		return isValid;
	}

	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}

	public List<EnrollmentViewIncomingCheksAccountListBean> getListAccounts() {
		return listAccounts;
	}

	public void setListAccount(List<EnrollmentViewIncomingCheksAccountListBean> listAccounts) {
		this.listAccounts = listAccounts;
	}

	public void fillFrom(ViewIncomingChecksApplication view) {
		if (view != null) {
			this.setBusinessName(view.getBusinessName());

			this.setPhone1(StringUtils.substring(view.getPhone(), 0, 3));
			this.setPhone2(StringUtils.substring(view.getPhone(), 3, 6));
			this.setPhone3(StringUtils.substring(view.getPhone(), 6, 10));

			this.setBusinessPhone1(StringUtils.substring(view.getBusinessPhone(), 0, 3));
			this.setBusinessPhone2(StringUtils.substring(view.getBusinessPhone(), 3, 6));
			this.setBusinessPhone3(StringUtils.substring(view.getBusinessPhone(), 6, 10));

			this.residential.setAddress1(view.getPhysicalAddress1());
			this.residential.setAddress2(view.getPhysicalAddress2());
			this.residential.setCity(view.getPhysicalAddressCity());
			this.residential.setState(view.getPhysicalAddressState());
			this.residential.setZipCode1(StringUtils.substring(view.getPhysicalAddressZip(), 0, 5));
			this.residential.setZipCode2(StringUtils.substring(view.getPhysicalAddressZip(), 5, 9));

			this.mailing.setAddress1(view.getMailingAddress1());
			this.mailing.setAddress2(view.getMailingAddress2());
			this.mailing.setCity(view.getMailingAddressCity());
			this.mailing.setState(view.getMailingAddressState());
			this.mailing.setZipCode1(StringUtils.substring(view.getMailingAddressZip(), 0, 5));
			this.mailing.setZipCode2(StringUtils.substring(view.getMailingAddressZip(), 5, 9));
		}

	}

	public List<String> getAddedAccounts() {
		return addedAccounts;
	}

	public void setAddedAccounts(List<String> addedAccounts) {
		this.addedAccounts = addedAccounts;
	}

}
