package evertec.cibp.web.form.bean;

public class EditBusinessPayeeBean {

	public enum ACTION {
		DELETE, MODIFY, FREQUENT, DELETE_EBILL, FREQUENT_PAYMENT_CONFIRMATION
	};

	private String payee;
	private ACTION action;
	private String nickname;
	private String frequent;

	public String getPayee() {
		return payee;
	}

	public void setPayee(String payee) {
		this.payee = payee;
	}

	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getFrequent() {
		return frequent;
	}

	public void setFrequent(String frequent) {
		this.frequent = frequent;
	}

}