package evertec.cibp.web.form.bean;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.TVFrontEndPayee;

public class GuidedSetupAddPayeeBean {

	private TVFrontendAccount tvfeAccount;
	private TVFrontEndPayee tvfePayee;
	private boolean checked;
	private boolean hasEbiil;
	private GlobalPayee globalPayee;
	private String subType;
	// cibp-2218 para saber si se agrego el payee 0=no se ha intentado agregar,
	// -1=fallo,1=se agreg�
	private int success = 0;

	public int getSuccess() {
		return success;
	}

	public void setSuccess(int succes) {
		this.success = succes;
	}

	/**
	 * @return the subType
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * @param subType
	 *            the subType to set
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * @return the globalPayee
	 */
	public GlobalPayee getGlobalPayee() {
		return globalPayee;
	}

	/**
	 * @param globalPayee
	 *            the globalPayee to set
	 */
	public void setGlobalPayee(GlobalPayee globalPayee) {
		this.globalPayee = globalPayee;
	}

	/**
	 * @return the tvfeAccount
	 */
	public TVFrontendAccount getTvfeAccount() {
		return tvfeAccount;
	}

	/**
	 * @param tvfeAccount
	 *            the tvfeAccount to set
	 */
	public void setTvfeAccount(TVFrontendAccount tvfeAccount) {
		this.tvfeAccount = tvfeAccount;
	}

	/**
	 * @return the tvfePayee
	 */
	public TVFrontEndPayee getTvfePayee() {
		return tvfePayee;
	}

	/**
	 * @param tvfePayee
	 *            the tvfePayee to set
	 */
	public void setTvfePayee(TVFrontEndPayee tvfePayee) {
		this.tvfePayee = tvfePayee;
	}

	/**
	 * @return the checked
	 */
	public boolean isChecked() {
		return checked;
	}

	/**
	 * @param checked
	 *            the checked to set
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	/**
	 * @return the hasEbiil
	 */
	public boolean isHasEbiil() {
		return hasEbiil;
	}

	/**
	 * @param hasEbiil
	 *            the hasEbiil to set
	 */
	public void setHasEbiil(boolean hasEbiil) {
		this.hasEbiil = hasEbiil;
	}

}
