package evertec.cibp.web.form.bean;

public class EbppViewBillerBean {

	public enum ACTION {
		ACTIVATE_EBILL, ACTIVATE_ESTMT, VIEW_EBILL, VIEW_ESTMT
	};

	private String id;
	private ACTION action;
	private String email;
	private String emailConfirm;
	private String zipCode;
	private String cycle;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailConfirm() {
		return emailConfirm;
	}

	public void setEmailConfirm(String emailConfirm) {
		this.emailConfirm = emailConfirm;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

}