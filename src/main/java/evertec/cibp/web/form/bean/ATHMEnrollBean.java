package evertec.cibp.web.form.bean;

public class ATHMEnrollBean {

	private String _phoneNumber = null;
	private String _phoneType = null;
	private String _phoneProvider = null;
	private String _pin = null;
	private String _pinConfirm = null;
	private String _validationType = null;
	private String _smsCodeSent = null;
	private String _smsCode = null;
	private String _validationReferenceId = null;
	private String _account = null;
	private String _termsConfirmation = null;
	private String _usernameEnroll = null;
	private String _passwordEnroll = null;
	private String _passwordConfirm = null;
	private String _usernameLogin = null;
	private String _passwordLogin = null;

	private String errorMessage;
	private String action = null;
	private String error;
	private String errorLogin;

	private String plasticAccountNumber = null;
	private String plasticNumber = null;
	private String plasticNum = null;
	private String plasticExpDate = null;
	private String plasticAcctType = null;
	private String plasticExpDateMonth = null;
	private String plasticExpDateYear = null;
	private String mobileCancelForm = "";

	private int _step = 0;
	private int _pinCount = 0;
	private int _validationCount = 0;

	private boolean _phoneInUse = false;
	private boolean _profileExists = false;
	private boolean enrollmentLoginError = false;
	private boolean errorFound = false;
	private boolean waitCall = false;
	private boolean validationCallError = false;
	private boolean serviceDown = false;
	private boolean sectionAutoBlock = false;
	private String mobileAppType;
	private boolean mobileApp;

	public boolean isServiceDown() {
		return serviceDown;
	}

	public void setServiceDown(boolean serviceDown) {
		this.serviceDown = serviceDown;
	}

	private String randomCode;
	private Long phoneValidationId;

	public boolean isWaitCall() {
		return waitCall;
	}

	public void setWaitCall(boolean waitCall) {
		this.waitCall = waitCall;
	}

	public boolean isValidationCallError() {
		return validationCallError;
	}

	public void setValidationCallError(boolean validationCallError) {
		this.validationCallError = validationCallError;
	}

	public String getPhoneNumber() {
		return _phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		_phoneNumber = phoneNumber;
	}

	public String getPhoneType() {
		return _phoneType;
	}

	public void setPhoneType(String phoneType) {
		_phoneType = phoneType;
	}

	public String getPhoneProvider() {
		return _phoneProvider;
	}

	public void setPhoneProvider(String phoneProvider) {
		_phoneProvider = phoneProvider;
	}

	public String getPin() {
		return _pin;
	}

	public void setPin(String pin) {
		_pin = pin;
	}

	public String getPinConfirm() {
		return _pinConfirm;
	}

	public void setPinConfirm(String pinConfirm) {
		_pinConfirm = pinConfirm;
	}

	public String getValidationType() {
		return _validationType;
	}

	public void setValidationType(String validationType) {
		_validationType = validationType;
	}

	public String getSmsCodeSent() {
		return _smsCodeSent;
	}

	public void setSmsCodeSent(String smsCodeSent) {
		_smsCodeSent = smsCodeSent;
	}

	public String getSmsCode() {
		return _smsCode;
	}

	public void setSmsCode(String smsCode) {
		_smsCode = smsCode;
	}

	public String getValidationReferenceId() {
		return _validationReferenceId;
	}

	public void setValidationReferenceId(String validationReferenceId) {
		_validationReferenceId = validationReferenceId;
	}

	public String getAccount() {
		return _account;
	}

	public void setAccount(String account) {
		_account = account;
	}

	public String getTermsConfirmation() {
		return _termsConfirmation;
	}

	public void setTermsConfirmation(String termsConfirmation) {
		_termsConfirmation = termsConfirmation;
	}

	public int getStep() {
		return _step;
	}

	public void setStep(int step) {
		_step = step;
	}

	public int getPinCount() {
		return _pinCount;
	}

	public void setPinCount(int pinCount) {
		_pinCount = pinCount;
	}

	public int getValidationCount() {
		return _validationCount;
	}

	public void setValidationCount(int validationCount) {
		_validationCount = validationCount;
	}

	public boolean isPhoneInUse() {
		return _phoneInUse;
	}

	public void setPhoneInUse(boolean phoneInUse) {
		_phoneInUse = phoneInUse;
	}

	public boolean isProfileExists() {
		return _profileExists;
	}

	public void setProfileExists(boolean profileExists) {
		this._profileExists = profileExists;
	}

	public String getUsernameEnroll() {
		return _usernameEnroll;
	}

	public void setUsernameEnroll(String usernameEnroll) {
		this._usernameEnroll = usernameEnroll;
	}

	public String getPasswordEnroll() {
		return _passwordEnroll;
	}

	public void setPasswordEnroll(String passwordEnroll) {
		this._passwordEnroll = passwordEnroll;
	}

	public String getPasswordConfirm() {
		return _passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this._passwordConfirm = passwordConfirm;
	}

	public String getUsernameLogin() {
		return _usernameLogin;
	}

	public void setUsernameLogin(String usernameLogin) {
		this._usernameLogin = usernameLogin;
	}

	public String getPasswordLogin() {
		return _passwordLogin;
	}

	public void setPasswordLogin(String passwordLogin) {
		this._passwordLogin = passwordLogin;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public boolean isErrorFound() {
		return errorFound;
	}

	public void setErrorFound(boolean errorFound) {
		this.errorFound = errorFound;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getErrorLogin() {
		return errorLogin;
	}

	public void setErrorLogin(String errorLogin) {
		this.errorLogin = errorLogin;
	}

	public String getRandomCode() {
		return randomCode;
	}

	public void setRandomCode(String randomCode) {
		this.randomCode = randomCode;
	}

	public Long getPhoneValidationId() {
		return phoneValidationId;
	}

	public void setPhoneValidationId(Long phoneValidationId) {
		this.phoneValidationId = phoneValidationId;
	}

	public boolean isEnrollmentLoginError() {
		return enrollmentLoginError;
	}

	public void setEnrollmentLoginError(boolean enrollmentLoginError) {
		this.enrollmentLoginError = enrollmentLoginError;
	}

	public String getPlasticNumber() {
		return plasticNumber;
	}

	public void setPlasticNumber(String plasticNumber) {
		this.plasticNumber = plasticNumber;
	}

	public String getPlasticExpDate() {
		return plasticExpDate;
	}

	public void setPlasticExpDate(String plasticExpDate) {
		this.plasticExpDate = plasticExpDate;
	}

	public String getPlasticAcctType() {
		return plasticAcctType;
	}

	public void setPlasticAcctType(String plasticAcctType) {
		this.plasticAcctType = plasticAcctType;
	}

	public String getPlasticAccountNumber() {
		return plasticAccountNumber;
	}

	public void setPlasticAccountNumber(String plasticAccountNumber) {
		this.plasticAccountNumber = plasticAccountNumber;
	}

	/**
	 * @return the sectionAutoBlock
	 */
	public boolean isSectionAutoBlock() {
		return sectionAutoBlock;
	}

	/**
	 * @param sectionAutoBlock
	 *            the sectionAutoBlock to set
	 */
	public void setSectionAutoBlock(boolean sectionAutoBlock) {
		this.sectionAutoBlock = sectionAutoBlock;
	}

	/**
	 * @return the plasticExpDateMonth
	 */
	public String getPlasticExpDateMonth() {
		return plasticExpDateMonth;
	}

	/**
	 * @param plasticExpDateMonth
	 *            the plasticExpDateMonth to set
	 */
	public void setPlasticExpDateMonth(String plasticExpDateMonth) {
		this.plasticExpDateMonth = plasticExpDateMonth;
	}

	/**
	 * @return the plasticExpDateYear
	 */
	public String getPlasticExpDateYear() {
		return plasticExpDateYear;
	}

	/**
	 * @param plasticExpDateYear
	 *            the plasticExpDateYear to set
	 */
	public void setPlasticExpDateYear(String plasticExpDateYear) {
		this.plasticExpDateYear = plasticExpDateYear;
	}

	/**
	 * @return the plasticNum
	 */
	public String getPlasticNum() {
		return plasticNum;
	}

	/**
	 * @param plasticNum
	 *            the plasticNum to set
	 */
	public void setPlasticNum(String plasticNum) {
		this.plasticNum = plasticNum;
	}
  
  public String getMobileAppType() {
		return mobileAppType;
	}

	public void setMobileAppType(String mobileAppType) {
		this.mobileAppType = mobileAppType;
	}

	public boolean isMobileApp() {
		return mobileApp;
	}

	public void setMobileApp(boolean mobileApp) {
		this.mobileApp = mobileApp;
	}

	public String getMobileCancelForm() {
		return mobileCancelForm;
	}

	public void setMobileCancelForm(String mobileCancelForm) {
		this.mobileCancelForm = mobileCancelForm;
	}
}
