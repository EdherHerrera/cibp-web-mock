package evertec.cibp.web.form.bean;

import java.math.BigDecimal;
import java.util.Date;

import bppr.tv.beans.transfer.Transfer;
import evertec.cibp.vs.beans.Frequency;

public class TransferBean {

	private String accountFrom;
	private String accountTo;
	private String accountToPifFilter;
	private BigDecimal amount;
	private Date effectiveDate;
	private Frequency frequency;
	private Integer instances;
	private String recurrent;
	private String memo;
	private Transfer tvTransfer;
	private String infinite;

	public String getAccountFrom() {
		return accountFrom;
	}

	public void setAccountFrom(String accountFrom) {
		this.accountFrom = accountFrom;
	}

	public String getAccountTo() {
		return accountTo;
	}

	public void setAccountTo(String accountTo) {
		this.accountTo = accountTo;
	}

	public String getAccountToPifFilter() {
		return accountToPifFilter;
	}

	public void setAccountToPifFilter(String accountToPifFilter) {
		this.accountToPifFilter = accountToPifFilter;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Transfer getTvTransfer() {
		return tvTransfer;
	}

	public void setTvTransfer(Transfer tvTransfer) {
		this.tvTransfer = tvTransfer;
	}

	public Integer getInstances() {
		return instances;
	}

	public void setInstances(Integer instances) {
		this.instances = instances;
	}

	public String getRecurrent() {
		return recurrent;
	}

	public void setRecurrent(String recurrent) {
		this.recurrent = recurrent;
	}

	public String getInfinite() {
		return infinite;
	}

	public void setInfinite(String infinite) {
		this.infinite = infinite;
	}
}
