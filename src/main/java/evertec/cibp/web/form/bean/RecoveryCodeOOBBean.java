package evertec.cibp.web.form.bean;

public class RecoveryCodeOOBBean {
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
