package evertec.cibp.web.form.bean;

public class ChangePasswordBean {
	private String origpassword;
	private String password;
	private String passwordconfirm;

	public String getOrigpassword() {
		return origpassword;
	}

	public void setOrigpassword(String origpassword) {
		this.origpassword = origpassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordconfirm() {
		return passwordconfirm;
	}

	public void setPasswordconfirm(String passwordconfirm) {
		this.passwordconfirm = passwordconfirm;
	}

}
