package evertec.cibp.web.form.bean;

public class EnrollOOBBean {
	private String answer, pm_fp;
	private String provider;
	private String phone1;
	private String phone2;
	private String phone3;
	private String smscode;
	private String callType;
	private Boolean miBancoTextEnroll;
	private Boolean alternatyEnroll;

	private String recoveryCode;
	private String frecuency;
	private boolean altPhoneRegistered;
	
	private String smsError;
	
	public enum ACTION {
		DELETE_ALT, ALT_SKIP, EDIT_PRIMARY_PHONE, CHOOSE_PREFERENCES
	};

	private ACTION action;
	
	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getPm_fp() {
		return pm_fp;
	}

	public void setPm_fp(String pm_fp) {
		this.pm_fp = pm_fp;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public String getSmscode() {
		return smscode;
	}

	public void setSmscode(String smscode) {
		this.smscode = smscode;
	}

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}

	public String getRecoveryCode() {
		return recoveryCode;
	}

	public void setRecoveryCode(String recoveryCode) {
		this.recoveryCode = recoveryCode;
	}
	
	public String getFrecuency() {
		return frecuency;
	}

	public void setFrecuency(String frecuency) {
		this.frecuency = frecuency;
	}
	
	public Boolean getMiBancoTextEnroll() {
		return miBancoTextEnroll;
	}

	public void setMiBancoTextEnroll(Boolean miBancoTextEnroll) {
		this.miBancoTextEnroll = miBancoTextEnroll;
	}

	public Boolean getAlternatyEnroll() {
		return alternatyEnroll;
	}

	public void setAlternatyEnroll(Boolean alternatyEnroll) {
		this.alternatyEnroll = alternatyEnroll;
	}

	public boolean isAltPhoneRegistered() {
		return altPhoneRegistered;
	}

	public void setAltPhoneRegistered(boolean altPhoneRegistered) {
		this.altPhoneRegistered = altPhoneRegistered;
	}

	public String getSmsError() {
		return smsError;
	}

	public void setSmsError(String smsError) {
		this.smsError = smsError;
	}
}
