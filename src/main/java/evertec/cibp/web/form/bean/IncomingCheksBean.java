package evertec.cibp.web.form.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import evertec.cibp.core.models.fraud.prevention.IncomingChecksDecision;

public class IncomingCheksBean {
	
	private IncomingChecksDecision decision = IncomingChecksDecision.PAY;
	private String checkId;
	private String accountNumber;
	private BigDecimal amount;
	private Boolean parent;
	private long issueDate;
	private String accountNickName;
	private String incomingCheckId;
	private String imageKey;
	private String frontEndImageKey;
	private boolean hasImage;
	private String frontEndId;
	private String traceId;
	
	public String getAccountNickName() {
		return accountNickName;
	}
	
	public void setAccountNickName(String accountNickName) {
		this.accountNickName = accountNickName;
	}
	
	public Boolean getParent() {
		return parent;
	}
	
	public void setParent(Boolean parent) {
		this.parent = parent;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	
	public void setAmount(BigDecimal bigDecimal) {
		this.amount = bigDecimal;
	}
	
	public IncomingChecksDecision getDecision() {
		return decision;
	}
	
	public void setDecision(IncomingChecksDecision decision) {
		this.decision = decision;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getCheckId() {
		return checkId;
	}

	public void setCheckId(String checkId) {
		this.checkId = checkId;
	}
	
	public void setIssueDate(Date issueDate) {
		if (issueDate == null)
			this.issueDate = 0;
		else
			this.issueDate = issueDate.getTime();
	}

	public Date getIssueDate() {
		if (issueDate == 0)
			return null;
		return new Date(this.issueDate);
	}
	public String getIncomingCheckId() {
		return incomingCheckId;
	}
	public void setIncomingCheckId(String incomingCheckId) {
		this.incomingCheckId = incomingCheckId;
	}
	
	public String getFrontEndId() {
		return frontEndId;
	}

	public void setFrontEndId(String frontEndId) {
		this.frontEndId = frontEndId;
	}

	public boolean isHasImage() {
		return hasImage;
	}
	
	public void setHasImage(boolean hasImage) {
		this.hasImage = hasImage;
	}
	
	public String getImageKey() {
		return imageKey;
	}
	
	public void setImageKey(String imageKey) {
		this.imageKey = imageKey;
	}
	
	public String getFrontEndImageKey() {
		return frontEndImageKey;
	}
	
	public void setFrontEndImageKey(String frontEndImageKey) {
		this.frontEndImageKey = frontEndImageKey;
	}
	
	public String getTraceId() {
		return traceId;
	}
	
	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}
}
