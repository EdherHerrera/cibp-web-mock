package evertec.cibp.web.form.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVPayment;

public class CCPortalPayments {

	private String accountToPay;

	private String autoPayAccountFrom = null;
	private String autoPayProcessType = null; // COMPLETE= Balance Total /
												// MINIMUM= Pago minimo
	private BigDecimal autoPayAmountMax = null; // used by Pay Process Type
												// COMPLETE (Balance Total)
	private Date autoPayNextProcessDate = null;

	private Date paymentLastProcessDate = null;
	private String paymentLastProcessStatus = null;

	private String autoPayDay = null;
	private String singlePayAccountFrom = null;
	private Date singlePayDate = null;
	private BigDecimal singlePayAmount = null;
	private String paymentMessage = null;
	private String singlePayRefNumber = null;
	private Map<String, String> accountFromPaymentMapAutoPay = null;
	private Map<String, String> accountFromPaymentMapRegular = null;
	private Map<String, String> paymentDaysMap = null;
	private Map<String, String> autoPayPaymentProcessMap = null;
	private TVFrontEndPayee payeeInfo = null;
	private TVPayment paymentInfo = null;
	private String payNickname = null;
	private String currentBalance = null;
	private boolean ebillUp = false;
	private TVFrontendAccount accountInfo = null;
	private String accountId = null;
	private String errorAutoPay = null;
	private String errorSinglePayment = null;
	
    /**currentAmount - AmountDue**/
    private BigDecimal currentAmountDue = null;

	public void setAccountToPay(String accountToPay) {
		this.accountToPay = accountToPay;
	}

	public String getAccountToPay() {
		return accountToPay;
	}

	public void setAutoPayAccountFrom(String autoPayAccountFrom) {
		this.autoPayAccountFrom = autoPayAccountFrom;
	}

	public String getAutoPayAccountFrom() {
		return autoPayAccountFrom;
	}

	public void setAutoPayProcessType(String autoPayProcessType) {
		this.autoPayProcessType = autoPayProcessType;
	}

	public String getAutoPayProcessType() {
		return autoPayProcessType;
	}

	public void setAutoPayDay(String autoPayDay) {
		this.autoPayDay = autoPayDay;
	}

	public String getAutoPayDay() {
		return autoPayDay;
	}

	public void setSinglePayAccountFrom(String singlePayAccountFrom) {
		this.singlePayAccountFrom = singlePayAccountFrom;
	}

	public String getSinglePayAccountFrom() {
		return singlePayAccountFrom;
	}

	public void setSinglePayDate(Date singlePayDate) {
		this.singlePayDate = singlePayDate;
	}

	public Date getSinglePayDate() {
		return singlePayDate;
	}

	public void setSinglePayAmount(BigDecimal singlePayAmount) {
		this.singlePayAmount = singlePayAmount;
	}

	public BigDecimal getSinglePayAmount() {
		return singlePayAmount;
	}

	public void setAutoPayAmountMax(BigDecimal autoPayAmountMax) {
		this.autoPayAmountMax = autoPayAmountMax;
	}

	public BigDecimal getAutoPayAmountMax() {
		return autoPayAmountMax;
	}

	public void setPaymentMessage(String paymentMessage) {
		this.paymentMessage = paymentMessage;
	}

	public String getPaymentMessage() {
		return paymentMessage;
	}

	public void setSinglePayRefNumber(String singlePayRefNumber) {
		this.singlePayRefNumber = singlePayRefNumber;
	}

	public String getSinglePayRefNumber() {
		return singlePayRefNumber;
	}

	/**
	 * @param paymentDaysMap
	 *            the paymentDaysMap to set
	 */
	public void setPaymentDaysMap(Map<String, String> paymentDaysMap) {
		this.paymentDaysMap = paymentDaysMap;
	}

	/**
	 * @return the paymentDaysMap
	 */
	public Map<String, String> getPaymentDaysMap() {
		return paymentDaysMap;
	}

	/**
	 * @param autoPayPaymentProcessMap
	 *            the autoPayPaymentProcessMap to set
	 */
	public void setAutoPayPaymentProcessMap(Map<String, String> autoPayPaymentProcessMap) {
		this.autoPayPaymentProcessMap = autoPayPaymentProcessMap;
	}

	/**
	 * @return the autoPayPaymentProcessMap
	 */
	public Map<String, String> getAutoPayPaymentProcessMap() {
		return autoPayPaymentProcessMap;
	}

	/**
	 * @param payeeInfo
	 *            the payeeInfo to set
	 */
	public void setPayeeInfo(TVFrontEndPayee payeeInfo) {
		this.payeeInfo = payeeInfo;
	}

	/**
	 * @return the payeeInfo
	 */
	public TVFrontEndPayee getPayeeInfo() {
		return payeeInfo;
	}

	/**
	 * @param payNickname
	 *            the payNickname to set
	 */
	public void setPayNickname(String payNickname) {
		this.payNickname = payNickname;
	}

	/**
	 * @return the payNickname
	 */
	public String getPayNickname() {
		return payNickname;
	}

	/**
	 * @param currentBalance
	 *            the currentBalance to set
	 */
	public void setCurrentBalance(String currentBalance) {
		this.currentBalance = currentBalance;
	}

	/**
	 * @return the currentBalance
	 */
	public String getCurrentBalance() {
		return currentBalance;
	}

	/**
	 * @param ebillUp
	 *            the ebillUp to set
	 */
	public void setEbillUp(boolean ebillUp) {
		this.ebillUp = ebillUp;
	}

	/**
	 * @return the ebillUp
	 */
	public boolean isEbillUp() {
		return ebillUp;
	}

	/**
	 * @param accountInfo
	 *            the accountInfo to set
	 */
	public void setAccountInfo(TVFrontendAccount accountInfo) {
		this.accountInfo = accountInfo;
	}

	/**
	 * @return the accountInfo
	 */
	public TVFrontendAccount getAccountInfo() {
		return accountInfo;
	}

	/**
	 * @param accountId
	 *            the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	public void setErrorAutoPay(String errorAutoPay) {
		this.errorAutoPay = errorAutoPay;
	}

	public String getErrorAutoPay() {
		return errorAutoPay;
	}

	public void setErrorSinglePayment(String errorSinglePayment) {
		this.errorSinglePayment = errorSinglePayment;
	}

	public String getErrorSinglePayment() {
		return errorSinglePayment;
	}

	/**
	 * @param autoPayNextProcessDate
	 *            the autoPayNextProcessDate to set
	 */
	public void setAutoPayNextProcessDate(Date autoPayNextProcessDate) {
		this.autoPayNextProcessDate = autoPayNextProcessDate;
	}

	/**
	 * @return the autoPayNextProcessDate
	 */
	public Date getAutoPayNextProcessDate() {
		return autoPayNextProcessDate;
	}

	/**
	 * @param paymentInfo
	 *            the paymentInfo to set
	 */
	public void setPaymentInfo(TVPayment paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	/**
	 * @return the paymentInfo
	 */
	public TVPayment getPaymentInfo() {
		return paymentInfo;
	}

	public void setAccountFromPaymentMapAutoPay(Map<String, String> accountFromPaymentMapAutoPay) {
		this.accountFromPaymentMapAutoPay = accountFromPaymentMapAutoPay;
	}

	public Map<String, String> getAccountFromPaymentMapAutoPay() {
		return accountFromPaymentMapAutoPay;
	}

	public void setAccountFromPaymentMapRegular(Map<String, String> accountFromPaymentMapRegular) {
		this.accountFromPaymentMapRegular = accountFromPaymentMapRegular;
	}

	public Map<String, String> getAccountFromPaymentMapRegular() {
		return accountFromPaymentMapRegular;
	}

	public void setPaymentLastProcessDate(Date paymentLastProcessDate) {
		this.paymentLastProcessDate = paymentLastProcessDate;
	}

	public Date getPaymentLastProcessDate() {
		return paymentLastProcessDate;
	}

	public void setPaymentLastProcessStatus(String paymentLastProcessStatus) {
		this.paymentLastProcessStatus = paymentLastProcessStatus;
	}

	public String getPaymentLastProcessStatus() {
		return paymentLastProcessStatus;
	}
	
	/**
	 * @return the currentAmountDue
	 */
	public BigDecimal getCurrentAmountDue() {
		return currentAmountDue;
	}

	/**
	 * @param currentAmountDue the currentAmountDue to set
	 */
	public void setCurrentAmountDue(BigDecimal currentAmountDue) {
		this.currentAmountDue = currentAmountDue;
	}
}
