package evertec.cibp.web.form.bean;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ValidateInfoBean {

	public enum ACTION {
		EMAIL, DOB, PIN, QUESTIONS, USERNAME, PASSWORD, CONTRACT, EXPDATE, ENROLLACT, NONTRANSACT
	};

	public List<ACTION> actionsProcessed = new LinkedList<ACTION>();
	private String email;
	private String emailconfirm;
	private String username;
	private String usernamealternativo; // CIBP-1947
	private String password;
	private String passwordconfirm;
	private String accountnumber;
	private String athnumber;
	private String pin;
	private String question1, question2, question3;
	private String answer1, answer2, answer3;
	private String answer1confirm, answer2confirm, answer3confirm;
	private ACTION action;
	private boolean saveDevice;
	private int dobmonth;
	private int dobyear;
	private int dobday;
	private String agreement;
	private String computerType;
	private int dobCounter;
	private int pinCounter;
	private String cvv;
	private String expMonth;
	private String expYear;
	private String zipCode;
	private String accountType;
	private int actInfCounter;
	private boolean checkAvaliability;
	private String productDescription;
	private String pm_fp;
	private boolean mobileApp;
	private String mobileAppType;
	
	//MBCA-228 And MBCA-459
	private Integer originationYear;
	private BigDecimal paymentAmount;
	
	//MBCA-195
	/**
	 * This field is to control the area in which the error message appears.
	 */
	private String error;

	//2881
	private String birthday;
	
	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	/**
	 * @return the checkAvaliability
	 */
	public boolean isCheckAvaliability() {
		return checkAvaliability;
	}

	/**
	 * @param checkAvaliability
	 *            the checkAvaliability to set
	 */
	public void setCheckAvaliability(boolean checkAvaliability) {
		this.checkAvaliability = checkAvaliability;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getExpMonth() {
		return expMonth;
	}

	public String getExpYear() {
		return expYear;
	}

	public int getDobCounter() {
		return dobCounter;
	}

	public void setDobCounter(int dobCounter) {
		this.dobCounter = dobCounter;
	}

	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailconfirm() {
		return emailconfirm;
	}

	public void setEmailconfirm(String emailconfirm) {
		this.emailconfirm = emailconfirm;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		if (username != null)
			this.username = username.toLowerCase();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordconfirm() {
		return passwordconfirm;
	}

	public void setPasswordconfirm(String passwordconfirm) {
		this.passwordconfirm = passwordconfirm;
	}

	public String getAccountnumber() {
		return accountnumber;
	}

	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}

	public String getQuestion1() {
		return question1;
	}

	public void setQuestion1(String question1) {
		this.question1 = question1;
	}

	public String getQuestion2() {
		return question2;
	}

	public void setQuestion2(String question2) {
		this.question2 = question2;
	}

	public String getQuestion3() {
		return question3;
	}

	public void setQuestion3(String question3) {
		this.question3 = question3;
	}

	public String getAnswer1() {
		return answer1;
	}

	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	public String getAnswer2() {
		return answer2;
	}

	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	public String getAnswer3() {
		return answer3;
	}

	public void setAnswer3(String answer3) {
		this.answer3 = answer3;
	}

	public String getAthnumber() {
		return athnumber;
	}

	public void setAthnumber(String athnumber) {
		this.athnumber = athnumber;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public boolean isSaveDevice() {
		return saveDevice;
	}

	public void setSaveDevice(boolean saveDevice) {
		this.saveDevice = saveDevice;
	}

	public int getDobmonth() {
		return dobmonth;
	}

	public void setDobmonth(int dobmonth) {
		this.dobmonth = dobmonth;
	}

	public int getDobyear() {
		return dobyear;
	}

	public void setDobyear(int dobyear) {
		this.dobyear = dobyear;
	}

	public int getDobday() {
		return dobday;
	}

	public void setDobday(int dobday) {
		this.dobday = dobday;
	}

	public String getAgreement() {
		return agreement;
	}

	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}

	public String getAnswer1confirm() {
		return answer1confirm;
	}

	public void setAnswer1confirm(String answer1confirm) {
		this.answer1confirm = answer1confirm;
	}

	public String getAnswer2confirm() {
		return answer2confirm;
	}

	public void setAnswer2confirm(String answer2confirm) {
		this.answer2confirm = answer2confirm;
	}

	public String getAnswer3confirm() {
		return answer3confirm;
	}

	public void setAnswer3confirm(String answer3confirm) {
		this.answer3confirm = answer3confirm;
	}

	public String getComputerType() {
		return computerType;
	}

	public void setComputerType(String computerType) {
		this.computerType = computerType;
	}

	public List<ACTION> getActionsProcessed() {
		return actionsProcessed;
	}

	public void setActionsProcessed(List<ACTION> actionsProcessed) {
		this.actionsProcessed = actionsProcessed;
	}

	public boolean isActionProccesedOK(ACTION act) {
		for (Iterator<ACTION> iterator = actionsProcessed.iterator(); iterator.hasNext();) {
			ACTION actt = (ACTION) iterator.next();
			if (actt == act)
				return true;
		}
		return false;
	}

	public int getPinCounter() {
		return pinCounter;
	}

	public void setPinCounter(int pinCounter) {
		this.pinCounter = pinCounter;
	}

	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}

	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}

	public int getActInfCounter() {
		return actInfCounter;
	}

	public void setActInfCounter(int actInfCounter) {
		this.actInfCounter = actInfCounter;
	}

	public String getUsernamealternativo() {
		return usernamealternativo;
	}

	public void setUsernamealternativo(String usernamealternativo) {
		if (usernamealternativo != null) {
			this.usernamealternativo = usernamealternativo.toLowerCase();
			this.username = usernamealternativo.toLowerCase();
		}
	}

	public String getPm_fp() {
		return pm_fp;
	}

	public void setPm_fp(String pm_fp) {
		this.pm_fp = pm_fp;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public boolean isMobileApp() {
		return mobileApp;
	}

	public void setMobileApp(boolean mobileApp) {
		this.mobileApp = mobileApp;
	}

	public String getMobileAppType() {
		return mobileAppType;
	}

	public void setMobileAppType(String mobileAppType) {
		this.mobileAppType = mobileAppType;
	}
	
	public Integer getOriginationYear() {
		return originationYear;
	}

	public void setOriginationYear(Integer originationYear) {
		this.originationYear = originationYear;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
