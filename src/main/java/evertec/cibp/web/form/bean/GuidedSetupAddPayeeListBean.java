package evertec.cibp.web.form.bean;

import java.util.List;

public class GuidedSetupAddPayeeListBean {

	private List<GuidedSetupAddPayeeBean> listAddPayeeBeans;

	public GuidedSetupAddPayeeListBean() {
	}

	/**
	 * @return the listAddPayeeBeans
	 */
	public List<GuidedSetupAddPayeeBean> getListAddPayeeBeans() {
		return listAddPayeeBeans;
	}

	/**
	 * @param listAddPayeeBeans
	 *            the listAddPayeeBeans to set
	 */
	public void setListAddPayeeBeans(List<GuidedSetupAddPayeeBean> listAddPayeeBeans) {
		this.listAddPayeeBeans = listAddPayeeBeans;
	}

}
