package evertec.cibp.web.form.bean;

public class AddressBean {
	
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zipCode1;
	private String zipCode2;
	
	public String getAddress1() {
		return address1;
	}
	
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	public String getAddress2() {
		return address2;
	}
	
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode1() {
		return zipCode1;
	}
	
	public void setZipCode1(String zipCode1) {
		this.zipCode1 = zipCode1;
	}
	
	public String getZipCode2() {
		return zipCode2;
	}
	
	public void setZipCode2(String zipCode2) {
		this.zipCode2 = zipCode2;
	}
}
