package evertec.cibp.web.form.bean;

import java.util.Date;

public class ExportFormBean {

	private String statementId;
	private String account;
	private String expcycle = "1"; // Cycle 1 = current
	private Date expStartDate;
	private String exportTypeFormat;
	private String searchType;
	private Date expEndDate;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getStatementId() {
		return statementId;
	}

	public void setStatementId(String statementId) {
		this.statementId = statementId;
	}

	public String getExportTypeFormat() {
		return exportTypeFormat;
	}

	public void setExportTypeFormat(String exportTypeFormat) {
		this.exportTypeFormat = exportTypeFormat;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getExpcycle() {
		return expcycle;
	}

	public void setExpcycle(String expcycle) {
		this.expcycle = expcycle;
	}

	public Date getExpStartDate() {
		return expStartDate;
	}

	public void setExpStartDate(Date expStartDate) {
		this.expStartDate = expStartDate;
	}

	public Date getExpEndDate() {
		return expEndDate;
	}

	public void setExpEndDate(Date expEndDate) {
		this.expEndDate = expEndDate;
	}

}
