package evertec.cibp.web.form.bean;

import java.io.Serializable;

public class CaptchaSettings implements Serializable {

	private static final long serialVersionUID = 1L;
	boolean disabled = false;
	double distortionFactor = 1.4;

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public double getDistortionFactor() {
		return distortionFactor;
	}

	public void setDistortionFactor(double distortionFactor) {
		this.distortionFactor = distortionFactor;
	}

}
