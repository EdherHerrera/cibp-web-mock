package evertec.cibp.web.form.bean;

import java.io.Serializable;


public class ChallengeTypeOOBBean implements Serializable {

	private static final long serialVersionUID = 3773091014422235669L;
	
	private String error;
	
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
