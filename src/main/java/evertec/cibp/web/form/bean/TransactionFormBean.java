package evertec.cibp.web.form.bean;

import evertec.cibp.core.models.account.TVFrontendAccount;

public class TransactionFormBean {

	private String statementId;
	private String account;
	private int pageReq = 1;
	private int totalPages = 1;
	private String cycle = "1"; // Cycle 1 = current
	private String category = "";
	private String imgPath = null;
	private TVFrontendAccount accountInfo = null;
	private String tabNum = "1";
	private String trxTabNum = "1";
	private String subTab = "lost";
	private String activity = "All";

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public int getPageReq() {
		return pageReq;
	}

	public void setPageReq(int pagereq) {
		this.pageReq = pagereq;
	}

	public String getStatementId() {
		return statementId;
	}

	public void setStatementId(String statementId) {
		this.statementId = statementId;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	/**
	 * @param imgPath
	 *            the imgPath to set
	 */
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	/**
	 * @return the imgPath
	 */
	public String getImgPath() {
		return imgPath;
	}

	/**
	 * @param accountInfo
	 *            the accountInfo to set
	 */
	public void setAccountInfo(TVFrontendAccount accountInfo) {
		this.accountInfo = accountInfo;
	}

	/**
	 * @return the accountInfo
	 */
	public TVFrontendAccount getAccountInfo() {
		return accountInfo;
	}

	/**
	 * @param tabNum
	 *            the tabNum to set
	 */
	public void setTabNum(String tabNum) {
		this.tabNum = tabNum;
	}

	/**
	 * @return the tabNum
	 */
	public String getTabNum() {
		return tabNum;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getTotalPages() {
		return totalPages;
	}

	/**
	 * @param trxTabNum
	 *            the trxTabNum to set
	 */
	public void setTrxTabNum(String trxTabNum) {
		this.trxTabNum = trxTabNum;
	}

	/**
	 * @return the trxTabNum
	 */
	public String getTrxTabNum() {
		return trxTabNum;
	}

	/**
	 * @param subTab
	 *            the subTab to set
	 */
	public void setSubTab(String subTab) {
		this.subTab = subTab;
	}

	/**
	 * @return the subTab
	 */
	public String getSubTab() {
		return subTab;
	}
}
