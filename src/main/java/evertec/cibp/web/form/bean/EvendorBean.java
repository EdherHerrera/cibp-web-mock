package evertec.cibp.web.form.bean;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.list.LazyList;

public class EvendorBean implements java.io.Serializable{
	
	private static final long serialVersionUID = 8334969473213276626L;
	private List<PaymentBean> vendors;
	private String singleAccountVendorFrom; 
	public enum ACTION {
		SEND_SMSCODE, CALL_PHONE, VALIDATE_SMSCODE, VALIDATE_CALLCODE, VALIDATE_RECCODE, NO_PHONE
		, CANCEL_OOB, ALT_PHONE, VALIDATE_ALTSMSCODE, VALIDATE_ALTCALLCODE
	}
	private ACTION action;
	private String code;
	private boolean timeout;
	private boolean validationError;
	
	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}
	
	@SuppressWarnings("unchecked")
	public EvendorBean() {
		vendors = LazyList.decorate(new ArrayList<PaymentBean>(), new Factory() {
			public Object create() {
				return new PaymentBean();
			}
		});
	}

	public List<PaymentBean> getVendors() {
		return vendors;
	}

	public void setVendors(List<PaymentBean> vendors) {
		this.vendors = vendors;
	}

	public String getSingleAccountVendorFrom() {
		return singleAccountVendorFrom;
	}

	public void setSingleAccountVendorFrom(String singleAccountVendorFrom) {
		this.singleAccountVendorFrom = singleAccountVendorFrom;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public boolean isTimeout() {
		return timeout;
	}

	public void setTimeout(boolean timeout) {
		this.timeout = timeout;
	}
	
	public boolean isValidationError() {
		return validationError;
	}

	public void setValidationError(boolean validationError) {
		this.validationError = validationError;
	}
}
