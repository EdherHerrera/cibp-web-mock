package evertec.cibp.web.form.bean;

import java.io.Serializable;

public class PostInitialFundingBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3267174950085765864L;
	private String routingNumber;
	private String accountNumber;
	private String confirmAccountNumber;
	private String accountType;

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getConfirmAccountNumber() {
		return confirmAccountNumber;
	}

	public void setConfirmAccountNumber(String confirmAccountNumber) {
		this.confirmAccountNumber = confirmAccountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
}