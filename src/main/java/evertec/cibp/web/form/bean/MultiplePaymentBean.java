package evertec.cibp.web.form.bean;

import java.math.BigDecimal;

public class MultiplePaymentBean {

	private String accountNumber;
	private BigDecimal amount;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
