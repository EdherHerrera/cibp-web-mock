package evertec.cibp.web.form.bean;

public class MobileApiSettingsBean {

	private int minimumRequiredVersionIphoneApp;
	private int latestVersionIphoneApp;
	private String locatorActiveLayers;

	public int getLatestVersionIphoneApp() {
		return latestVersionIphoneApp;
	}

	public void setLatestVersionIphoneApp(int latestVersionIphoneApp) {
		this.latestVersionIphoneApp = latestVersionIphoneApp;
	}

	public int getMinimumRequiredVersionIphoneApp() {
		return minimumRequiredVersionIphoneApp;
	}

	public void setMinimumRequiredVersionIphoneApp(int minimumRequiredVersionIphoneApp) {
		this.minimumRequiredVersionIphoneApp = minimumRequiredVersionIphoneApp;
	}

	public String getLocatorActiveLayers() {
		return locatorActiveLayers;
	}

	public void setLocatorActiveLayers(String locatorActiveLayers) {
		this.locatorActiveLayers = locatorActiveLayers;
	}

}
