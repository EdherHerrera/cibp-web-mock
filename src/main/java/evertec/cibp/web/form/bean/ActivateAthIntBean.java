package evertec.cibp.web.form.bean;

public class ActivateAthIntBean {

	private String frontEndId;
	private boolean formError = false;

	public String getFrontEndId() {
		return frontEndId;
	}

	public void setFrontEndId(String frontEndId) {
		this.frontEndId = frontEndId;
	}

	/**
	 * @param formError
	 *            the formError to set
	 */
	public void setFormError(boolean formError) {
		this.formError = formError;
	}

	/**
	 * @return the formError
	 */
	public boolean getFormError() {
		return formError;
	}

}
