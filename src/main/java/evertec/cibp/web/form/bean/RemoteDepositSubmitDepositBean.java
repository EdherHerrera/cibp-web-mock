package evertec.cibp.web.form.bean;

import java.math.BigDecimal;

public class RemoteDepositSubmitDepositBean {
	private String frontEndId;
	private BigDecimal amount;
	private String frontImageStr;
	private String backImageStr;
	private String deviceType;
	private String osName;
	private String osVersion;
	private String error;

	public String getFrontEndId() {
		return frontEndId;
	}

	public void setFrontEndId(String frontEndId) {
		this.frontEndId = frontEndId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getFrontImageStr() {
		return frontImageStr;
	}

	public void setFrontImageStr(String frontImageStr) {
		this.frontImageStr = frontImageStr;
	}

	public String getBackImageStr() {
		return backImageStr;
	}

	public void setBackImageStr(String backImageStr) {
		this.backImageStr = backImageStr;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
