package evertec.cibp.web.form.bean;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

import evertec.cibp.core.models.customer.AlertsHistory;

public class AlerthistoryBean implements Serializable {

	private static final long serialVersionUID = 1L;
	// private String accountNumber;
	// private String date;
	private TransferFilterType action = TransferFilterType.ALL;
	// private String modTransfer;
	private int page;
	private int fromIndex;
	private int toIndex;
	private int subfromIndex;
	private int subtoIndex;
	private String message;
	private String index;
	private int total;
	private String channel = "";
	private String category = "";
	private String dropdown;
	private boolean reset;
	private List<AlertsHistory> historyList;
	private List<AlertsHistory> historySubList;
	private LinkedHashMap<String, String> pagesMap = new LinkedHashMap<String, String>();
	private boolean newSearch;

	public boolean isNewSearch() {
		return newSearch;
	}

	public void setNewSearch(boolean newSearch) {
		this.newSearch = newSearch;
	}

	public LinkedHashMap<String, String> getPagesMap() {
		return pagesMap;
	}

	public void setPagesMap(LinkedHashMap<String, String> pagesMap) {
		this.pagesMap = pagesMap;
	}

	public boolean isReset() {
		return reset;
	}

	public void setReset(boolean reset) {
		this.reset = reset;
	}

	public String getDropdown() {
		return dropdown;
	}

	public void setDropdown(String dropdown) {
		this.dropdown = dropdown;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<AlertsHistory> getHistorySubList() {
		return historySubList;
	}

	public void setHistorySubList(List<AlertsHistory> historySubList) {
		this.historySubList = historySubList;
	}

	public int getSubfromIndex() {
		return subfromIndex;
	}

	public void setSubfromIndex(int subfromIndex) {
		this.subfromIndex = subfromIndex;
	}

	public int getSubtoIndex() {
		return subtoIndex;
	}

	public void setSubtoIndex(int subtoIndex) {
		this.subtoIndex = subtoIndex;
	}

	public int getFromIndex() {
		return fromIndex;
	}

	public void setFromIndex(int fromIndex) {
		this.fromIndex = fromIndex;
	}

	public int getToIndex() {
		return toIndex;
	}

	public void setToIndex(int toIndex) {
		this.toIndex = toIndex;
	}

	public List<AlertsHistory> getHistoryList() {
		return historyList;
	}

	public void setHistoryList(List<AlertsHistory> historyList) {
		this.historyList = historyList;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public enum TransferFilterType {
		ALL, SEARCH_BY_CATEGORY, SEARCH_BY_CHANNEL
	};

	// public TransferFilterType getAction() {
	// return action;
	// }

	// public void setAction(TransferFilterType action) {
	// this.action = action;
	// }

	public TransferFilterType getAction() {
		return action;
	}

	public void setAction(TransferFilterType action) {
		this.action = action;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

}
