package evertec.cibp.web.form.bean;

import java.util.Date;
import java.util.Map;

public class CCPortalVacations {
	private Date dateFrom = null;
	private Date dateTo = null;
	private String cities = null;
	private String domestic = null;
	private String phone = null;
	private Map domesticMap = null;
	private String error = null;
	
	private String[] accounts;

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setCities(String cities) {
		this.cities = cities;
	}

	public String getCities() {
		return cities;
	}

	public void setDomestic(String domestic) {
		this.domestic = domestic;
	}

	public String getDomestic() {
		return domestic;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	/**
	 * @param domesticMap
	 *            the domesticMap to set
	 */
	public void setDomesticMap(Map domesticMap) {
		this.domesticMap = domesticMap;
	}

	/**
	 * @return the domesticMap
	 */
	public Map getDomesticMap() {
		return domesticMap;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public String[] getAccounts() {
		return accounts;
	}

	public void setAccounts(String[] accounts) {
		this.accounts = accounts;
	}

}
