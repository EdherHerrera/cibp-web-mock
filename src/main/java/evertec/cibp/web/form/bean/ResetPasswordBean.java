package evertec.cibp.web.form.bean;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.rsa.csd.ws.DeviceRequest;

import evertec.cibp.core.models.enrollment.EnrollableAccount;
import evertec.cibp.core.models.enrollment.EnrollmentReason;

public class ResetPasswordBean {
	private String firstname;
	private String mi;
	private String lastname;
	private String maiden;
	private String email;
	private String emailconfirm;
	private String nickname;
	private String taxId1;
	private String taxId2;
	private String taxId3;
	private Integer dobday;
	private Integer dobmonth;
	private Integer dobyear;
	private String username;
	private String password;
	private String passwordconfirm;
	private String accountnumber;
	private String athnumber;
	private String pin;
	private String agreement;
	private List<EnrollableAccount> accounts;
	private Map<Object, Object> questions1;
	private Map<Object, Object> questions2;
	private Map<Object, Object> questions3;
	private String question1;
	private String question2;
	private String question3;
	private String answer1;
	private String answer2;
	private String answer3;
	private String answer1confirm;
	private String answer2confirm;
	private String answer3confirm;
	// private RsaIdentificationObj rsaobj;
	private DeviceRequest deviceRequest;
	private String permId;
	private String closedAccType, closedAccNumber, closedAccProductId;
	private String accountnumbercca, accountnumberamex;
	private String pinamex;
	private String accountInfoType;
	private String cvv, expMonth, expMonthamex, expYear, expYearamex, zipCode, zipCodeamex;
	private String devicePrint;
	private String computerType;
	private EnrollmentReason motivated;
	private String pm_fp;
	private String captchaAnswer;
	// private List<EnrollableAccount> enrollExistingAccounts;
	// private Map<Object, CustomerProfile> customerProfiles;
	private String productDescription;
	private String mobileAppType;
	private boolean mobileApp;
	//private BigDecimal originalAmount;
	private Integer originationYear;
	private BigDecimal monthlyPayment;
	
	// 1886
	private boolean updateEnrollAccount;

	//MBSFE-306
	private String erroraccountnumber;
	
	private String clearedTaxId1;
	private String clearedTaxId2;
	private String clearedTaxId3;
	private String showTaxId;
	
	
	private String resourceRegainAccess; // resource Regain Access
	
	public String getErroraccountnumber() {
		return erroraccountnumber;
	}

	public void setErroraccountnumber(String erroraccountnumber) {
		this.erroraccountnumber = erroraccountnumber;
	}
	//END MBSFE-306
	
	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getMi() {
		return mi;
	}

	public void setMi(String mi) {
		this.mi = mi;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMaiden() {
		return maiden;
	}

	public void setMaiden(String maidenname) {
		this.maiden = maidenname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailconfirm() {
		return emailconfirm;
	}

	public void setEmailconfirm(String emailconfirm) {
		this.emailconfirm = emailconfirm;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Integer getDobday() {
		return dobday;
	}

	public void setDobday(Integer dobday) {
		this.dobday = dobday;
	}

	public Integer getDobmonth() {
		return dobmonth;
	}

	public void setDobmonth(Integer dobmonth) {
		this.dobmonth = dobmonth;
	}

	public Integer getDobyear() {
		return dobyear;
	}

	public void setDobyear(Integer dobyear) {
		this.dobyear = dobyear;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordconfirm() {
		return passwordconfirm;
	}

	public void setPasswordconfirm(String passwordconfirm) {
		this.passwordconfirm = passwordconfirm;
	}

	public String getAccountnumber() {
		return accountnumber;
	}

	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}

	public String getAthnumber() {
		return athnumber;
	}

	public void setAthnumber(String athnumber) {
		this.athnumber = athnumber;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getAgreement() {
		return agreement;
	}

	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}

	public List<EnrollableAccount> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<EnrollableAccount> accounts) {
		this.accounts = accounts;
	}

	public Map<Object, Object> getQuestions1() {
		return questions1;
	}

	public void setQuestions1(Map<Object, Object> questions1) {
		this.questions1 = questions1;
	}

	public Map<Object, Object> getQuestions2() {
		return questions2;
	}

	public void setQuestions2(Map<Object, Object> questions2) {
		this.questions2 = questions2;
	}

	public Map<Object, Object> getQuestions3() {
		return questions3;
	}

	public void setQuestions3(Map<Object, Object> questions3) {
		this.questions3 = questions3;
	}

	public String getQuestion1() {
		return question1;
	}

	public void setQuestion1(String question1) {
		this.question1 = question1;
	}

	public String getQuestion2() {
		return question2;
	}

	public void setQuestion2(String question2) {
		this.question2 = question2;
	}

	public String getQuestion3() {
		return question3;
	}

	public void setQuestion3(String question3) {
		this.question3 = question3;
	}

	public String getAnswer1() {
		return answer1;
	}

	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	public String getAnswer2() {
		return answer2;
	}

	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	public String getAnswer3() {
		return answer3;
	}

	public void setAnswer3(String answer3) {
		this.answer3 = answer3;
	}

	// public RsaIdentificationObj getRsaobj() {
	// return rsaobj;
	// }
	// public void setRsaobj(RsaIdentificationObj rsaobj) {
	// this.rsaobj = rsaobj;
	// }
	public String getPermId() {
		return permId;
	}

	public void setPermId(String permId) {
		this.permId = permId;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}

	public String getExpYear() {
		return expYear;
	}

	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAccountInfoType() {
		return accountInfoType;
	}

	public void setAccountInfoType(String accountInfoType) {
		this.accountInfoType = accountInfoType;
	}

	public String getTaxId1() {
		return taxId1;
	}

	public void setTaxId1(String taxId1) {
		this.taxId1 = taxId1;
	}

	public String getTaxId2() {
		return taxId2;
	}

	public void setTaxId2(String taxId2) {
		this.taxId2 = taxId2;
	}

	public String getTaxId3() {
		return taxId3;
	}

	public void setTaxId3(String taxId3) {
		this.taxId3 = taxId3;
	}

	public String getAnswer1confirm() {
		return answer1confirm;
	}

	public void setAnswer1confirm(String answer1confirm) {
		this.answer1confirm = answer1confirm;
	}

	public String getAnswer2confirm() {
		return answer2confirm;
	}

	public void setAnswer2confirm(String answer2confirm) {
		this.answer2confirm = answer2confirm;
	}

	public String getAnswer3confirm() {
		return answer3confirm;
	}

	public void setAnswer3confirm(String answer3confirm) {
		this.answer3confirm = answer3confirm;
	}

	public String getDevicePrint() {
		return devicePrint;
	}

	public void setDevicePrint(String devicePrint) {
		this.devicePrint = devicePrint;
	}

	public String getComputerType() {
		return computerType;
	}

	public void setComputerType(String computerType) {
		this.computerType = computerType;
	}

	public EnrollmentReason getMotivated() {
		return motivated;
	}

	public void setMotivated(EnrollmentReason motivated) {
		this.motivated = motivated;
	}

	public String getPm_fp() {
		return pm_fp;
	}

	public void setPm_fp(String pm_fp) {
		this.pm_fp = pm_fp;
	}

	public String getCaptchaAnswer() {
		return captchaAnswer;
	}

	public void setCaptchaAnswer(String captchaAnswer) {
		this.captchaAnswer = captchaAnswer;
	}

	public DeviceRequest getDeviceRequest() {
		return deviceRequest;
	}

	public void setDeviceRequest(DeviceRequest deviceRequest) {
		this.deviceRequest = deviceRequest;
	}

	public String getMobileAppType() {
		return mobileAppType;
	}

	public void setMobileAppType(String mobileAppType) {
		this.mobileAppType = mobileAppType;
	}

	public boolean isMobileApp() {
		return mobileApp;
	}

	public void setMobileApp(boolean mobileApp) {
		this.mobileApp = mobileApp;
	}

	public String getClosedAccType() {
		return closedAccType;
	}

	public void setClosedAccType(String closedAccType) {
		this.closedAccType = closedAccType;
	}

	public String getClosedAccNumber() {
		return closedAccNumber;
	}

	public void setClosedAccNumber(String closedAccNumber) {
		this.closedAccNumber = closedAccNumber;
	}

	public String getClosedAccProductId() {
		return closedAccProductId;
	}

	public void setClosedAccProductId(String closedAccProductId) {
		this.closedAccProductId = closedAccProductId;
	}

	public String getAccountnumbercca() {
		return accountnumbercca;
	}

	public void setAccountnumbercca(String accountnumbercca) {
		this.accountnumbercca = accountnumbercca;
	}

	public String getAccountnumberamex() {
		return accountnumberamex;
	}

	public void setAccountnumberamex(String accountnumberamex) {
		this.accountnumberamex = accountnumberamex;
	}

	public String getPinamex() {
		return pinamex;
	}

	public void setPinamex(String pinamex) {
		this.pinamex = pinamex;
	}

	public String getExpMonthamex() {
		return expMonthamex;
	}

	public void setExpMonthamex(String expMonthamex) {
		this.expMonthamex = expMonthamex;
	}

	public String getExpYearamex() {
		return expYearamex;
	}

	public void setExpYearamex(String expYearamex) {
		this.expYearamex = expYearamex;
	}

	public String getZipCodeamex() {
		return zipCodeamex;
	}

	public void setZipCodeamex(String zipCodeamex) {
		this.zipCodeamex = zipCodeamex;
	}

	public boolean isUpdateEnrollAccount() {
		return updateEnrollAccount;
	}

	public void setUpdateEnrollAccount(boolean updateEnrollAccount) {
		this.updateEnrollAccount = updateEnrollAccount;
	}

	public Integer getOriginationYear() {
		return originationYear;
	}

	public void setOriginationYear(Integer originationYear) {
		this.originationYear = originationYear;
	}

	public BigDecimal getMonthlyPayment() {
		return monthlyPayment;
	}

	public void setMonthlyPayment(BigDecimal monthlyPayment) {
		this.monthlyPayment = monthlyPayment;
	}

	public String getClearedTaxId1() {
		return clearedTaxId1;
	}

	public void setClearedTaxId1(String clearedTaxId1) {
		this.clearedTaxId1 = clearedTaxId1;
	}

	public String getClearedTaxId2() {
		return clearedTaxId2;
	}

	public void setClearedTaxId2(String clearedTaxId2) {
		this.clearedTaxId2 = clearedTaxId2;
	}

	public String getClearedTaxId3() {
		return clearedTaxId3;
	}

	public void setClearedTaxId3(String clearedTaxId3) {
		this.clearedTaxId3 = clearedTaxId3;
	}

	public String getShowTaxId() {
		return showTaxId;
	}

	public void setShowTaxId(String showTaxId) {
		this.showTaxId = showTaxId;
	}
	
	/**
	 * resourceRegainAccess
	 * @return String
	 */
	public String getResourceRegainAccess() {
		return resourceRegainAccess;
	}

	/**
	 * setResourceRegainAccess
	 * @param strResourceRegainAccess
	 */
	public void setResourceRegainAccess(String strResourceRegainAccess) {
		this.resourceRegainAccess = strResourceRegainAccess;
	}
	
}
