package evertec.cibp.web.form.bean;

public class ChangeQuestionsBean {
	private String question1, question2, question3, answer1, confirm1, answer2, confirm2, answer3, confirm3;

	public String getQuestion1() {
		return question1;
	}

	public void setQuestion1(String question1) {
		this.question1 = question1;
	}

	public String getQuestion2() {
		return question2;
	}

	public void setQuestion2(String question2) {
		this.question2 = question2;
	}

	public String getQuestion3() {
		return question3;
	}

	public void setQuestion3(String question3) {
		this.question3 = question3;
	}

	public String getAnswer1() {
		return answer1;
	}

	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	public String getConfirm1() {
		return confirm1;
	}

	public void setConfirm1(String confirm1) {
		this.confirm1 = confirm1;
	}

	public String getAnswer2() {
		return answer2;
	}

	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	public String getConfirm2() {
		return confirm2;
	}

	public void setConfirm2(String confirm2) {
		this.confirm2 = confirm2;
	}

	public String getAnswer3() {
		return answer3;
	}

	public void setAnswer3(String answer3) {
		this.answer3 = answer3;
	}

	public String getConfirm3() {
		return confirm3;
	}

	public void setConfirm3(String confirm3) {
		this.confirm3 = confirm3;
	}
}
