package evertec.cibp.web.form.bean;

public class AlertTypesBean {
	private String answer, pm_fp;
	private boolean remember;
	/*MBSD-217 */
	public enum ACTION {
		EDIT_SMS_OOB
	}
	
	private ACTION action;

	public ACTION getAction() {
		return action;
	}
	/*END--MBSD-217*/
	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getPm_fp() {
		return pm_fp;
	}

	public void setPm_fp(String pm_fp) {
		this.pm_fp = pm_fp;
	}

	public boolean isRemember() {
		return remember;
	}

	public void setRemember(boolean remember) {
		this.remember = remember;
	}
}