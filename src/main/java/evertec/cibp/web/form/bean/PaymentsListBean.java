package evertec.cibp.web.form.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.list.LazyList;

public class PaymentsListBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8334969473213276626L;
	private List<PaymentBean> payments;
	private List<PaymentBean> payrolls;
	private List<PaymentBean> favorites;
	private String singleAccountFrom;
	private String tabNum = "0";

	@SuppressWarnings("unchecked")
	public PaymentsListBean() {
		payments = LazyList.decorate(new ArrayList<PaymentBean>(), new Factory() {
			public Object create() {
				return new PaymentBean();
			}
		});
		favorites = LazyList.decorate(new ArrayList<PaymentBean>(), new Factory() {
			public Object create() {
				return new PaymentBean();
			}
		});
	}

	public List<PaymentBean> getFavorites() {
		return favorites;
	}

	public void setFavorites(List<PaymentBean> favorites) {
		this.favorites = favorites;
	}

	public List<PaymentBean> getPayments() {
		return payments;
	}

	public void setPayments(List<PaymentBean> payments) {
		this.payments = payments;
	}

	public String getSingleAccountFrom() {
		return singleAccountFrom;
	}

	public void setSingleAccountFrom(String singleAccountFrom) {
		this.singleAccountFrom = singleAccountFrom;
	}

	public List<PaymentBean> getPayrolls() {
		return payrolls;
	}

	public void setPayrolls(List<PaymentBean> payrolls) {
		this.payrolls = payrolls;
	}
	
	/**
	 * @param tabNum
	 *            the tabNum to set
	 */
	public void setTabNum(String tabNum) {
		this.tabNum = tabNum;
	}

	/**
	 * @return the tabNum
	 */
	public String getTabNum() {
		return tabNum;
	}
}