package evertec.cibp.web.form.bean;

import java.io.Serializable;

public class AddExistingPayeeBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6321655333907347723L;
	/**
	 * 
	 */
	private String payee;
	private String billingNumber;
	private String billingNumberConf;
	private String nickname;

	private boolean activateEbill;
	private boolean editEmail;
	private String email;
	private String emailConf;
	private String zipcode;
	private boolean emailError;
	private String employeeName;
	private String employeeId;
	private String employeeIdConf;
	private String bank;
	private String accounttype;
	private String city;
    private String address;
    private String state;
    private String zipCode;
    private String comments;
    private String vendorName;
    private String vendorId;
    private String vendorIdConf;
    private String businessName;
    private String routingNumber;
    

	public String getAccounttype() {
		return accounttype;
	}

	public void setAccounttype(String accounttype) {
		this.accounttype = accounttype;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getEmployeeIdConf() {
		return employeeIdConf;
	}

	public void setEmployeeIdConf(String employeeIdConf) {
		this.employeeIdConf = employeeIdConf;
	}

	private String employeeRoutingNumber;

	// private String employeeAccountNumber;

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeRoutingNumber() {
		return employeeRoutingNumber;
	}

	public void setEmployeeRoutingNumber(String employeeRoutingNumber) {
		this.employeeRoutingNumber = employeeRoutingNumber;
	}

	// public String getEmployeeAccountNumber() {
	// return employeeAccountNumber;
	// }
	// public void setEmployeeAccountNumber(String employeeAccountNumber) {
	// this.employeeAccountNumber = employeeAccountNumber;
	// }
	public String getPayee() {
		return payee;
	}

	public void setPayee(String payee) {
		this.payee = payee;
	}

	public String getBillingNumber() {
		return billingNumber;
	}

	public void setBillingNumber(String billingNumber) {
		this.billingNumber = billingNumber;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public boolean getActivateEbill() {
		return activateEbill;
	}

	public void setActivateEbill(boolean activateEbill) {
		this.activateEbill = activateEbill;
	}

	public boolean isEditEmail() {
		return editEmail;
	}

	public void setEditEmail(boolean editEmail) {
		this.editEmail = editEmail;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getEmailConf() {
		return emailConf;
	}

	public void setEmailConf(String emailConf) {
		this.emailConf = emailConf;
	}

	public String getBillingNumberConf() {
		return billingNumberConf;
	}

	public void setBillingNumberConf(String billingNumberConf) {
		this.billingNumberConf = billingNumberConf;
	}

	public boolean getEmailError() {
		return emailError;
	}

	public void setEmailError(boolean emailError) {
		this.emailError = emailError;
	}
	
	public String getAddress() {
        return address;
 }

 public void setAddress(String address) {
        this.address = address;
 }

 public String getCity() {
        return city;
 }

 public void setCity(String city) {
        this.city = city;
 }

 public String getState() {
        return state;
 }

 public void setState(String state) {
        this.state = state;
 }

 public String getZipCode() {
        return zipCode;
 }

 public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
 }

 public String getComments() {
        return comments;
 }

 public void setComments(String comments) {
        this.comments = comments;
 }

 public String getBusinessName() {
        return businessName;
 }

 public void setBusinessName(String businessName) {
        this.businessName = businessName;
 }

 public String getVendorName() {
        return vendorName;
 }

 public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
 }

 public String getVendorId() {
        return vendorId;
 }

 public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
 }

 public String getVendorIdConf() {
        return vendorIdConf;
 }

 public void setVendorIdConf(String vendorIdConf) {
        this.vendorIdConf = vendorIdConf;
 }


 public String getRoutingNumber() {
        return routingNumber;
 }

 public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
 }
}