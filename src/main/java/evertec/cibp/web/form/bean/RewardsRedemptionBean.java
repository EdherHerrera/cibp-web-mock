package evertec.cibp.web.form.bean;

import java.util.List;
import java.util.Map;

import com.evertec.cibp.api.client.model.MicroServiceResponse;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.web.response.TsysLoyaltyRewardsRedemptionResponse;

public class RewardsRedemptionBean {
	/**
	 * 
	 */
	
	private TVFrontendAccount cashRewardsAccount;
	
	private Double redeemAmount;
	private boolean termsAndCond;
	private String redemptionDate;
	private String frontEndId;
	private String redemptionType;
	private String cardName;
	private MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> microServiceResponse;
	
	//Direct deposit fields
	TVFrontendAccount directDepositAccount;
	List<TVFrontendAccount> eligebleDirectDepositAccounts;
	private String acctNumber;

	/**
	 * @return the redeemAmount
	 */
	public Double getRedeemAmount() {
		return redeemAmount;
	}

	/**
	 * @param redeemAmount
	 *            the redeemAmount to set
	 */
	public void setRedeemAmount(Double redeemAmount) {
		this.redeemAmount = redeemAmount;
	}

	/**
	 * @return the termsAndCond
	 */
	public boolean isTermsAndCond() {
		return termsAndCond;
	}

	/**
	 * @param termsAndCond
	 *            the termsAndCond to set
	 */
	public void setTermsAndCond(boolean termsAndCond) {
		this.termsAndCond = termsAndCond;
	}

	/**
	 * @return the redemptionDate
	 */
	public String getRedemptionDate() {
		return redemptionDate;
	}

	/**
	 * @param redemptionDate
	 *            the redemptionDate to set
	 */
	public void setRedemptionDate(String redemptionDate) {
		this.redemptionDate = redemptionDate;
	}

	/**
	 * @return the frontEndId
	 */
	public String getFrontEndId() {
		return frontEndId;
	}

	/**
	 * @param frontEndId
	 *            the frontEndId to set
	 */
	public void setFrontEndId(String frontEndId) {
		this.frontEndId = frontEndId;
	}

	/**
	 * @return the acctNumber
	 */
	public String getAcctNumber() {
		return acctNumber;
	}

	/**
	 * @param acctNumber
	 *            the acctNumber to set
	 */
	public void setAcctNumber(String acctNumber) {
		this.acctNumber = acctNumber;
	}

	/**
	 * @return the redemptionType
	 */
	public String getRedemptionType() {
		return redemptionType;
	}

	/**
	 * @param redemptionTypeMap
	 *            the redemptionTypeMap to set
	 */
	public void setRedemptionType(String redemptionType) {
		this.redemptionType = redemptionType;
	}

	/**
	 * @return the cardName
	 */
	public String getCardName() {
		return cardName;
	}

	/**
	 * @param cardName
	 *            the cardName to set
	 */
	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	/**
	 * @return the microServiceResponse
	 */
	public MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> getMicroServiceResponse() {
		return microServiceResponse;
	}

	/**
	 * @param microServiceResponse
	 *            the microServiceResponse to set
	 */
	public void setMicroServiceResponse(
			MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> microServiceResponse) {
		this.microServiceResponse = microServiceResponse;
	}
	
	public List<TVFrontendAccount> getEligebleDirectDepositAccounts() {
		return eligebleDirectDepositAccounts;
	}

	public void setEligebleDirectDepositAccounts(List<TVFrontendAccount> eligebleDirectDepositAccounts) {
		this.eligebleDirectDepositAccounts = eligebleDirectDepositAccounts;
	}

	public TVFrontendAccount getCashRewardsAccount() {
		return cashRewardsAccount;
	}

	public void setCashRewardsAccount(TVFrontendAccount cashRewardsAccount) {
		this.cashRewardsAccount = cashRewardsAccount;
	}

	public TVFrontendAccount getDirectDepositAccount() {
		return directDepositAccount;
	}

	public void setDirectDepositAccount(TVFrontendAccount directDepositAccount) {
		this.directDepositAccount = directDepositAccount;
	}
}
