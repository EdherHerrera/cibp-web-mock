package evertec.cibp.web.form.bean;

import java.math.BigDecimal;

public class ExternalAccountVerificationBean {

	private BigDecimal amount1;
	private BigDecimal amount1Confirmation;
	private BigDecimal amount2;
	private BigDecimal amount2Confirmation;
	private String accountId;

	public BigDecimal getAmount1() {
		return amount1;
	}

	public BigDecimal getAmount1Confirmation() {
		return amount1Confirmation;
	}

	public void setAmount1Confirmation(BigDecimal amount1Confirmation) {
		this.amount1Confirmation = amount1Confirmation;
	}

	public BigDecimal getAmount2() {
		return amount2;
	}

	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}

	public BigDecimal getAmount2Confirmation() {
		return amount2Confirmation;
	}

	public void setAmount2Confirmation(BigDecimal amount2Confirmation) {
		this.amount2Confirmation = amount2Confirmation;
	}

	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

}
