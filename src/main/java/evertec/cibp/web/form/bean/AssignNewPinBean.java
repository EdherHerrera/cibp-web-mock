package evertec.cibp.web.form.bean;

public class AssignNewPinBean {

	private String plastic, pin, pinconfirm;
	private boolean formError = false;

	public String getPinconfirm() {
		return pinconfirm;
	}

	public void setPinconfirm(String pinconfirm) {
		this.pinconfirm = pinconfirm;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getPlastic() {
		return plastic;
	}

	public void setPlastic(String plastic) {
		this.plastic = plastic;
	}

	/**
	 * @param formError
	 *            the formError to set
	 */
	public void setFormError(boolean formError) {
		this.formError = formError;
	}

	/**
	 * @return the formError
	 */
	public boolean getFormError() {
		return formError;
	}

}
