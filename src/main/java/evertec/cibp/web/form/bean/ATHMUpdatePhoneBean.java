package evertec.cibp.web.form.bean;

public class ATHMUpdatePhoneBean {
	private String _currentPhone = null;
	private String _phoneNumber = null;
	private String _phoneType = null;
	private String _phoneProvider = null;
	private String _pin = null;
	private String _validationReferenceId = null;

	private int _validationCount = 0;

	private boolean _phoneInUse = false;

	public String getCurrentPhone() {
		return _currentPhone;
	}

	public void setCurrentPhone(String currentPhone) {
		_currentPhone = currentPhone;
	}

	public String getPhoneNumber() {
		return _phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		_phoneNumber = phoneNumber;
	}

	public String getPhoneType() {
		return _phoneType;
	}

	public void setPhoneType(String phoneType) {
		_phoneType = phoneType;
	}

	public String getPhoneProvider() {
		return _phoneProvider;
	}

	public void setPhoneProvider(String phoneProvider) {
		_phoneProvider = phoneProvider;
	}

	public String getPin() {
		return _pin;
	}

	public void setPin(String pin) {
		_pin = pin;
	}

	public String getValidationReferenceId() {
		return _validationReferenceId;
	}

	public void setValidationReferenceId(String validationReferenceId) {
		_validationReferenceId = validationReferenceId;
	}

	public int getValidationCount() {
		return _validationCount;
	}

	public void setValidationCount(int validationCount) {
		_validationCount = validationCount;
	}

	public boolean isPhoneInUse() {
		return _phoneInUse;
	}

	public void setPhoneInUse(boolean phoneInUse) {
		_phoneInUse = phoneInUse;
	}
}
