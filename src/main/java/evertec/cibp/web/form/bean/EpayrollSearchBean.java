package evertec.cibp.web.form.bean;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.list.LazyList;

public class EpayrollSearchBean {

	private String date;
	private int action = SEARCH_LAST15;
	private String modPayment;
	private String payeeId;
	
	/**constant favorites **/
	private List<ModPaymentBean> favorites;
	
	/**constant favId **/
	private int favId;
	private String referenceNum;
	private String paymentType="All";

	public String getReferenceNum() {
		return referenceNum;
	}

	public void setReferenceNum(String referenceNum) {
		this.referenceNum = referenceNum;
	}

	public static final int SEARCH_LAST15 = 1;
	public static final int SEARCH_BY_DATE = 2;
	public static final int SEARCH_BY_PAYEE = 3;
	public static final int MOD_PAYMENT = 4;
	public static final int CAN_PAYMENT = 5;
	public static final int MOD_AMT = 6;
	public static final int MOD_AMT_DATE = 7;
	public static final int REVERSE_PAYMENT = 8;

	public EpayrollSearchBean() {
		favorites = LazyList.decorate(new ArrayList(), new Factory() {
			public Object create() {
				return new ModPaymentBean();
			}
		});
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
	}

	public String getModPayment() {
		return modPayment;
	}

	public void setModPayment(String modPayment) {
		this.modPayment = modPayment;
	}

	public String getPayeeId() {
		return payeeId;
	}

	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}

	/**
	 * getFavorites
	 * @return favorites
	 */
	public List<ModPaymentBean> getFavorites() {
		return favorites;
	}

	/**
	 * setFavorites
	 * @param favorites List
	 */
	public void setFavorites(List<ModPaymentBean> favorites) {
		this.favorites = favorites;
	}

	public int getFavId() {
		return favId;
	}

	public void setFavId(int favId) {
		this.favId = favId;
	}
	
	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

}
