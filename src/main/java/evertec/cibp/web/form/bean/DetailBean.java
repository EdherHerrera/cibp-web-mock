package evertec.cibp.web.form.bean;

public class DetailBean {
	private String account;
	private String premiaCycle;
	private String activity = "All";

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPremiaCycle() {
		return premiaCycle;
	}

	public void setPremiaCycle(String premiaCycle) {
		this.premiaCycle = premiaCycle;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

}
