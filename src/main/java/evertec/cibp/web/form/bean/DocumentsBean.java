package evertec.cibp.web.form.bean;

import java.util.List;

import evertec.cibp.core.models.documents.CustomerDocumentsPackage;

public class DocumentsBean {

	private String error;
	private String documentumid;
	private String documentsAccepted;
	private List<CustomerDocumentsPackage> disbursementList;

	/**
	 * @return the documentsAccepted
	 */
	public String getDocumentsAccepted() {
		return documentsAccepted;
	}

	/**
	 * @param documentsAccepted
	 *            the documentsAccepted to set
	 */
	public void setDocumentsAccepted(String documentsAccepted) {
		this.documentsAccepted = documentsAccepted;
	}

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * @return the documentumid
	 */
	public String getDocumentumid() {
		return documentumid;
	}

	/**
	 * @param documentumid
	 *            the documentumid to set
	 */
	public void setDocumentumid(String documentumid) {
		this.documentumid = documentumid;
	}

	/**
	 * @return the disbursementList
	 */
	public List<CustomerDocumentsPackage> getDisbursementList() {
		return disbursementList;
	}

	/**
	 * @param disbursementList
	 *            the disbursementList to set
	 */
	public void setDisbursementList(List<CustomerDocumentsPackage> disbursementList) {
		this.disbursementList = disbursementList;
	}

}
