package evertec.cibp.web.form.bean;

import java.io.Serializable;

public class MortgageLoanBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private String account;
	private String status;
	
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
