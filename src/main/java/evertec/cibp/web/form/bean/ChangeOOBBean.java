package evertec.cibp.web.form.bean;

import java.io.Serializable;

public class ChangeOOBBean implements Serializable {
	
	/**
	 * Adapt this bean to challenge OOB functionality
	 */
	private static final long serialVersionUID = -5474266330249485430L;

	public enum ACTION {
		EDIT_PRIMARY_PHONE, EDIT_BACKUP_PHONE, EDIT_CHALLENGE_TYPE, EDIT_CHALLENGE_FREQUENCY, GENERATE_RECOVERY_CODE, DEACTIVATE_2_STEP
	}
	
	private ACTION action;

	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}
}
