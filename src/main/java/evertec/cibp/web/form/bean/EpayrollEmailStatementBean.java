package evertec.cibp.web.form.bean;

import java.io.Serializable;

public class EpayrollEmailStatementBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String realAccountNumber;
	private String accountType;
	private String emailList;
	private String accountNumber;
	private String relationship;
	private String accountProductType;

	public String getRealAccountNumber() {
		return realAccountNumber;
	}
	
	public void setRealAccountNumber(String realAccountNumber) {
		this.realAccountNumber = realAccountNumber;
	}
	
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getEmailList() {
		return emailList;
	}

	public void setEmailList(String emailList) {
		this.emailList = emailList;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getRelationship() {
		return relationship;
	}
	
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getAccountProductType() {
		return accountProductType;
	}
	
	public void setAccountProductType(String accountProductType) {
		this.accountProductType = accountProductType;
	}
}
