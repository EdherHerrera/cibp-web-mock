package evertec.cibp.web.form.bean;

public class InterestStmtStatusUpdateBean {

	public enum ACTION {
		ACTIVE, DEACTIVE
	};

	private ACTION action;
	private String type;

	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}