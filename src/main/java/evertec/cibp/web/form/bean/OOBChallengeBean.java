package evertec.cibp.web.form.bean;

import java.io.Serializable;
import java.sql.Timestamp;

public class OOBChallengeBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7518724513390986580L;

	public enum ACTION {
		SEND_SMSCODE, CALL_PHONE, VALIDATE_SMSCODE, VALIDATE_CALLCODE, VALIDATE_RECCODE, NO_PHONE
		, CANCEL_OOB
	}

	private ACTION action;
	private String code;
	private Timestamp codeTimestamp;
	private String recoveryCode;
	private boolean timeout;
	private boolean validationError;

	/**
	 * New implementation RSA SDK Mobile
	 */
	private String device_info_rsa; //
	
	private String rsa_cookie; //

	
	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}

	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}

	public Timestamp getCodeTimestamp() {
		return codeTimestamp;
	}
	
	public void setCodeTimestamp(Timestamp codeTimestamp) {
		this.codeTimestamp = codeTimestamp;
	}
	
	public String getRecoveryCode() {
		return recoveryCode;
	}
	
	public void setRecoveryCode(String recoveryCode) {
		this.recoveryCode = recoveryCode;
	}

	public boolean isTimeout() {
		return timeout;
	}

	public void setTimeout(boolean timeout) {
		this.timeout = timeout;
	}

	public boolean isValidationError() {
		return validationError;
	}

	public void setValidationError(boolean validationError) {
		this.validationError = validationError;
	}
	
	/**
	 * getDeviceInfoRsa
	 * @return String
	 */
	public String getDevice_info_rsa () {
		return device_info_rsa;
	}
	
	/**
	 * setDevideInfoRsa
	 * @param strDeviceInfoRsa
	 */
	public void setDevice_info_rsa (String strDeviceInfoRsa) {
		this.device_info_rsa = strDeviceInfoRsa;
	}
	
	/**
	 * @return the rewardPoints
	 */
	public String getRsa_cookie() {
		return rsa_cookie;
	}

	/**
	 * @param rewardPoints the rewardPoints to set
	 */
	public void setRsa_cookie(String strRsaCookie) {
		this.rsa_cookie = strRsaCookie;
	}

		
}
