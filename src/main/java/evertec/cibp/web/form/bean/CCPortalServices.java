package evertec.cibp.web.form.bean;

import java.util.List;
import java.util.Map;

import evertec.prophit.service.bean.AddAdditionalCardBean;
import evertec.prophit.service.bean.LostStolenBean;
import evertec.prophit.service.bean.RequestCardBean;

public class CCPortalServices {

	private String screen;
	private String nickname;
	private String id;

	private String insuranceWorkTime;
	private String insuranceSelfEmployeed;
	private String insuranceAge;
	private String insuranceEmployer;
	private String insuranceCountry;
	private String plasticLast4Digits;
	private Map<String, Object> insuranceAnswerMap;
	private Map<String, Object> lostStolenAnswerMap;
	private Map<String, Object> lostStolenTypeMap;
	private Map<String, Object> insuranceCountryMap;
	private String error;
	private List<CCPlasticsCustInfo> accountPlasticNumbers;
	private LostStolenBean lostStolenInfo = new LostStolenBean();
	// cibp-2277 dsalas
	private AddAdditionalCardBean addAdditionalInfo = new AddAdditionalCardBean();
	private RequestCardBean requestCardInfo = new RequestCardBean();

	// cibp-2761 pin request
	private String pin;
	private String confirmPin;
	private Integer page;

	public RequestCardBean getRequestCardInfo() {
		return requestCardInfo;
	}

	public void setRequestCardInfo(RequestCardBean requestCardInfo) {
		this.requestCardInfo = requestCardInfo;
	}

	public AddAdditionalCardBean getAddAdditionalInfo() {
		return addAdditionalInfo;
	}

	public void setAddAdditionalInfo(AddAdditionalCardBean addAdditionalInfo) {
		this.addAdditionalInfo = addAdditionalInfo;
	}

	/**
	 * @param insuranceWork
	 *            the insuranceWork to set
	 */
	public void setInsuranceWorkTime(String insuranceWorkTime) {
		this.insuranceWorkTime = insuranceWorkTime;
	}

	/**
	 * @return the insuranceWork
	 */
	public String getInsuranceWorkTime() {
		return insuranceWorkTime;
	}

	/**
	 * @param insuranceWorkMap
	 *            the insuranceWorkMap to set
	 */
	public void setInsuranceAnswerMap(Map<String, Object> insuranceAnswerMap) {
		this.insuranceAnswerMap = insuranceAnswerMap;
	}

	/**
	 * @return the insuranceWorkMap
	 */
	public Map<String, Object> getInsuranceAnswerMap() {
		return insuranceAnswerMap;
	}

	/**
	 * @param screen
	 *            the screen to set
	 */
	public void setScreen(String screen) {
		this.screen = screen;
	}

	/**
	 * @return the screen
	 */
	public String getScreen() {
		return screen;
	}

	/**
	 * @param nickname
	 *            the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @param insuranceSelfEmployeed
	 *            the insuranceSelfEmployeed to set
	 */
	public void setInsuranceSelfEmployeed(String insuranceSelfEmployeed) {
		this.insuranceSelfEmployeed = insuranceSelfEmployeed;
	}

	/**
	 * @return the insuranceSelfEmployeed
	 */
	public String getInsuranceSelfEmployeed() {
		return insuranceSelfEmployeed;
	}

	/**
	 * @param insuranceAge
	 *            the insuranceAge to set
	 */
	public void setInsuranceAge(String insuranceAge) {
		this.insuranceAge = insuranceAge;
	}

	/**
	 * @return the insuranceAge
	 */
	public String getInsuranceAge() {
		return insuranceAge;
	}

	public String getInsuranceEmployer() {
		return insuranceEmployer;
	}

	public void setInsuranceEmployer(String insuranceEmployer) {
		this.insuranceEmployer = insuranceEmployer;
	}

	public String getInsuranceCountry() {
		return insuranceCountry;
	}

	public void setInsuranceCountry(String insuranceCountry) {
		this.insuranceCountry = insuranceCountry;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param accountPlasticNumbers
	 *            the accountPlasticNumbers to set
	 */
	public void setAccountPlasticNumbers(List<CCPlasticsCustInfo> accountPlasticNumbers) {
		this.accountPlasticNumbers = accountPlasticNumbers;
	}

	/**
	 * @return the accountPlasticNumbers
	 */
	public List<CCPlasticsCustInfo> getAccountPlasticNumbers() {
		return accountPlasticNumbers;
	}

	/**
	 * @param lostStolenInfo
	 *            the lostStolenInfo to set
	 */
	public void setLostStolenInfo(LostStolenBean lostStolenInfo) {
		this.lostStolenInfo = lostStolenInfo;
	}

	/**
	 * @return the lostStolenInfo
	 */
	public LostStolenBean getLostStolenInfo() {
		return lostStolenInfo;
	}

	/**
	 * @param plasticLast4Digits
	 *            the plasticLast4Digits to set
	 */
	public void setPlasticLast4Digits(String plasticLast4Digits) {
		this.plasticLast4Digits = plasticLast4Digits;
	}

	/**
	 * @return the plasticLast4Digits
	 */
	public String getPlasticLast4Digits() {
		return plasticLast4Digits;
	}

	/**
	 * @param lostStolenAnswerMap
	 *            the lostStolenAnswerMap to set
	 */
	public void setLostStolenAnswerMap(Map<String, Object> lostStolenAnswerMap) {
		this.lostStolenAnswerMap = lostStolenAnswerMap;
	}

	/**
	 * @return the lostStolenAnswerMap
	 */
	public Map<String, Object> getLostStolenAnswerMap() {
		return lostStolenAnswerMap;
	}

	/**
	 * @param lostStolenTypeMap
	 *            the lostStolenTypeMap to set
	 */
	public void setLostStolenTypeMap(Map<String, Object> lostStolenTypeMap) {
		this.lostStolenTypeMap = lostStolenTypeMap;
	}

	/**
	 * @return the lostStolenTypeMap
	 */
	public Map<String, Object> getLostStolenTypeMap() {
		return lostStolenTypeMap;
	}

	public Map<String, Object> getInsuranceCountryMap() {
		return insuranceCountryMap;
	}

	public void setInsuranceCountryMap(Map<String, Object> insuranceCountryMap) {
		this.insuranceCountryMap = insuranceCountryMap;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getConfirmPin() {
		return confirmPin;
	}

	public void setConfirmPin(String confirmPin) {
		this.confirmPin = confirmPin;
	}

	public boolean isPinNumber() {
		try {
			Integer.parseInt(this.pin);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

}
