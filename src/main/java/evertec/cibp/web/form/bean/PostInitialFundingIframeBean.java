package evertec.cibp.web.form.bean;

public class PostInitialFundingIframeBean {

	private String routingNumber;
	private String accountNumber;
	private String confirmAccountNumber;
	private String originalAccountId;
	private String originalAccountNumber;
	private String accountType;

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getConfirmAccountNumber() {
		return confirmAccountNumber;
	}

	public void setConfirmAccountNumber(String confirmAccountNumber) {
		this.confirmAccountNumber = confirmAccountNumber;
	}

	public String getOriginalAccountId() {
		return originalAccountId;
	}

	public void setOriginalAccountId(String originalAccountId) {
		this.originalAccountId = originalAccountId;
	}

	public String getOriginalAccountNumber() {
		return originalAccountNumber;
	}

	public void setOriginalAccountNumber(String originalAccountNumber) {
		this.originalAccountNumber = originalAccountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
}