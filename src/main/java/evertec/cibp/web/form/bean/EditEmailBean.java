package evertec.cibp.web.form.bean;

public class EditEmailBean {
	private String email;
	private String emailconfirm;
	private Boolean skipRSA;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailconfirm() {
		return emailconfirm;
	}

	public void setEmailconfirm(String emailconfirm) {
		this.emailconfirm = emailconfirm;
	}

	/**
	 * @return the skipRSA
	 */
	public Boolean getSkipRSA() {
		return skipRSA;
	}

	/**
	 * @param skipRSA
	 *            the skipRSA to set
	 */
	public void setSkipRSA(Boolean skipRSA) {
		this.skipRSA = skipRSA;
	}

}
