package evertec.cibp.web.form.bean;

import java.io.Serializable;

public class SmsBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4489985892049308163L;

	public enum ACTION {
		SMS_ENROLL, SMS_CODE_VER, DELETEMOB, SMS_RESEND, SMS_RESEND_LIMIT, SMS_GLOBAL_LIMIT, SMS_CODE_VER_SUCCESS, SMS_CODE_VER_FAILED
	};

	private ACTION action;
	private String provider;
	private String phone1;
	private String phone2;
	private String phone3;
	private String smscode;

	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getSmscode() {
		return smscode;
	}

	public void setSmscode(String smscode) {
		this.smscode = smscode;
	}
}
