package evertec.cibp.web.form.bean;

import java.io.Serializable;

public class CCPlasticsCustInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String plasticNum;
	private String last4PlasticNumber;
	private String crvFlag;
	private String custName;
	private String custType;
	private String toAcctNumber;
	private boolean olderThanLastReqDate;

	// cibp-2761
	private boolean pinValidation;
	private boolean pinBranch;
	private boolean pinAllow;
	private boolean pinEmail;

	public CCPlasticsCustInfo() {
		this.pinValidation = false;
		this.pinBranch = false;
		this.pinAllow = false;
		this.pinEmail = false;
	}

	/**
	 * @param crvFlag
	 *            the crvFlag to set
	 */
	public void setCrvFlag(String crvFlag) {
		this.crvFlag = crvFlag;
	}

	/**
	 * @return the crvFlag
	 */
	public String getCrvFlag() {
		return crvFlag;
	}

	/**
	 * @param custName
	 *            the custName to set
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}

	/**
	 * @return the custName
	 */
	public String getCustName() {
		return custName;
	}

	/**
	 * @param custType
	 *            the custType to set
	 */
	public void setCustType(String custType) {
		this.custType = custType;
	}

	/**
	 * @return the custType
	 */
	public String getCustType() {
		return custType;
	}

	/**
	 * @param toAcctNumber
	 *            the toAcctNumber to set
	 */
	public void setToAcctNumber(String toAcctNumber) {
		this.toAcctNumber = toAcctNumber;
	}

	/**
	 * @return the toAcctNumber
	 */
	public String getToAcctNumber() {
		return toAcctNumber;
	}

	/**
	 * @param last4PlasticNumber
	 *            the last4PlasticNumber to set
	 */
	public void setLast4PlasticNumber(String last4PlasticNumber) {
		this.last4PlasticNumber = last4PlasticNumber;
	}

	/**
	 * @return the last4PlasticNumber
	 */
	public String getLast4PlasticNumber() {
		return last4PlasticNumber;
	}

	/**
	 * @param plasticNum
	 *            the plasticNum to set
	 */
	public void setPlasticNum(String plasticNum) {
		this.plasticNum = plasticNum;
	}

	/**
	 * @return the plasticNum
	 */
	public String getPlasticNum() {
		return plasticNum;
	}

	public boolean isOlderThanLastReqDate() {
		return olderThanLastReqDate;
	}

	public void setOlderThanLastReqDate(boolean olderThanLastReqDate) {
		this.olderThanLastReqDate = olderThanLastReqDate;
	}

	public boolean isPinValidation() {
		return pinValidation;
	}

	public void setPinValidation(boolean pinValidation) {
		this.pinValidation = pinValidation;
	}

	public boolean isPinBranch() {
		return pinBranch;
	}

	public void setPinBranch(boolean pinBranch) {
		this.pinBranch = pinBranch;
	}

	public boolean isPinAllow() {
		return pinAllow;
	}

	public void setPinAllow(boolean pinAllow) {
		this.pinAllow = pinAllow;
	}

	public boolean isPinEmail() {
		return pinEmail;
	}

	public void setPinEmail(boolean pinEmail) {
		this.pinEmail = pinEmail;
	}

}
