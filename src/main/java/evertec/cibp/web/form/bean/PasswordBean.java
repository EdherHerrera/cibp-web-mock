package evertec.cibp.web.form.bean;

public class PasswordBean {
    private String answer;
    private Integer failcount;
    private String mobileAppType;
    private boolean mobileApp;
    private String deviceId;
    private String deviceNickname;
    // deviceId + customerToken(MbopId)
    private String deviceToken;
    private String firebaseToken;
    private String mbopDeviceId;
    private String fingerprintId;
    private String appVersion;
    private String osVersion;
    private String networkProvider;
    private String deviceModel;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getFailcount() {
        return failcount;
    }

    public void setFailcount(Integer failcount) {
        this.failcount = failcount;
    }

    public String getMobileAppType() {
        return mobileAppType;
    }

    public void setMobileAppType(String mobileAppType) {
        this.mobileAppType = mobileAppType;
    }

    public boolean isMobileApp() {
        return mobileApp;
    }

    public void setMobileApp(boolean mobileApp) {
        this.mobileApp = mobileApp;
    }

    public String getDeviceNickname() {
        return deviceNickname;
    }

    public void setDeviceNickname(String deviceNickname) {
        this.deviceNickname = deviceNickname;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getMbopDeviceId() {
        return mbopDeviceId;
    }

    public void setMbopDeviceId(String mbopDeviceId) {
        this.mbopDeviceId = mbopDeviceId;
    }

    public String getFingerprintId() {
        return fingerprintId;
    }

    public void setFingerprintId(String fingerprintId) {
        this.fingerprintId = fingerprintId;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getNetworkProvider() {
        return networkProvider;
    }

    public void setNetworkProvider(String networkProvider) {
        this.networkProvider = networkProvider;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }
}