package evertec.cibp.web.form.bean;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.list.LazyList;



public class EPayrollsListBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8334969473213276626L;
	private List<PaymentBean> payrolls;
	private String singleAccountPayrollFrom; 
	
	public enum ACTION {
		SEND_SMSCODE, CALL_PHONE, VALIDATE_SMSCODE, VALIDATE_CALLCODE, VALIDATE_RECCODE, NO_PHONE
		, CANCEL_OOB, ALT_PHONE, VALIDATE_ALTSMSCODE, VALIDATE_ALTCALLCODE
	}

	private ACTION action;
	private String code;
	private Timestamp codeTimestamp;
	private String recoveryCode;
	private boolean timeout;
	private boolean validationError;
	private String tabNum = "0";
	
	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}

	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}

	public Timestamp getCodeTimestamp() {
		return codeTimestamp;
	}
	
	public void setCodeTimestamp(Timestamp codeTimestamp) {
		this.codeTimestamp = codeTimestamp;
	}
	
	public String getRecoveryCode() {
		return recoveryCode;
	}
	
	public void setRecoveryCode(String recoveryCode) {
		this.recoveryCode = recoveryCode;
	}

	public boolean isTimeout() {
		return timeout;
	}

	public void setTimeout(boolean timeout) {
		this.timeout = timeout;
	}

	public boolean isValidationError() {
		return validationError;
	}

	public void setValidationError(boolean validationError) {
		this.validationError = validationError;
	}

	@SuppressWarnings("unchecked")
	public EPayrollsListBean() {
		payrolls = LazyList.decorate(new ArrayList<PaymentBean>(), new Factory() {
			public Object create() {
				return new PaymentBean();
			}
		});
	}

	public List<PaymentBean> getPayrolls() {
		return payrolls;
	}

	public void setPayrolls(List<PaymentBean> payrolls) {
		this.payrolls = payrolls;
	}

	public String getSingleAccountPayrollFrom() {
		return singleAccountPayrollFrom;
	}

	public void setSingleAccountPayrollFrom(String singleAccountPayrollFrom) {
		this.singleAccountPayrollFrom = singleAccountPayrollFrom;
	}
	
	/**
	 * @param tabNum
	 *            the tabNum to set
	 */
	public void setTabNum(String tabNum) {
		this.tabNum = tabNum;
	}

	/**
	 * @return the tabNum
	 */
	public String getTabNum() {
		return tabNum;
	}
}