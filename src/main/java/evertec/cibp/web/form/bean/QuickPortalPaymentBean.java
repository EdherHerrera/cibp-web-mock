package evertec.cibp.web.form.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.vs.beans.Frequency;

public class QuickPortalPaymentBean {

	private String referenceAccountId;
	private String referencePayeeId;
	private List<TVPayment> pendingPayments;
	private String accountFrom;
	private String payeeid;
	private BigDecimal amount;
	private Date effectiveDate;
	private Frequency frequency;
	private Integer instances;
	private String recurrent;
	private String memo;
	private TVPayment tvPayment;
	private String infinite;

	public String getAccountFrom() {
		return accountFrom;
	}

	public void setAccountFrom(String accountFrom) {
		this.accountFrom = accountFrom;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getInstances() {
		return instances;
	}

	public void setInstances(Integer instances) {
		this.instances = instances;
	}

	public String getRecurrent() {
		return recurrent;
	}

	public void setRecurrent(String recurrent) {
		this.recurrent = recurrent;
	}

	public String getInfinite() {
		return infinite;
	}

	public void setInfinite(String infinite) {
		this.infinite = infinite;
	}

	public TVPayment getTvPayment() {
		return tvPayment;
	}

	public void setTvPayment(TVPayment tvPayment) {
		this.tvPayment = tvPayment;
	}

	public String getPayeeid() {
		return payeeid;
	}

	public void setPayeeid(String payeeid) {
		this.payeeid = payeeid;
	}

	public String getReferenceAccountId() {
		return referenceAccountId;
	}

	public void setReferenceAccountId(String referenceAccountId) {
		this.referenceAccountId = referenceAccountId;
	}

	public String getReferencePayeeId() {
		return referencePayeeId;
	}

	public void setReferencePayeeId(String referencePayeeId) {
		this.referencePayeeId = referencePayeeId;
	}

	public List<TVPayment> getPendingPayments() {
		return pendingPayments;
	}

	public void setPendingPayments(List<TVPayment> pendingPayments) {
		this.pendingPayments = pendingPayments;
	}
}
