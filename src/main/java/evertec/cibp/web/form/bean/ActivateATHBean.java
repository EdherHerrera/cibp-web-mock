package evertec.cibp.web.form.bean;

public class ActivateATHBean {

	private String athNumber, pin, pinconfirm;
	private boolean formError = false;

	public String getPinconfirm() {
		return pinconfirm;
	}

	public void setPinconfirm(String pinconfirm) {
		this.pinconfirm = pinconfirm;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getAthNumber() {
		return athNumber;
	}

	public void setAthNumber(String athNumber) {
		this.athNumber = athNumber;
	}

	/**
	 * @param formError
	 *            the formError to set
	 */
	public void setFormError(boolean formError) {
		this.formError = formError;
	}

	/**
	 * @return the formError
	 */
	public boolean getFormError() {
		return formError;
	}

}
