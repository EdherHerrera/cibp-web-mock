package evertec.cibp.web.form.bean;

public class AssignAtmInfoBean {
	private String atmIdAndDate;
	private String error;
	
	public String getAtmIdAndDate() {
		return atmIdAndDate;
	}
	public void setAtmIdAndDate(String atmIdAndDate) {
		this.atmIdAndDate = atmIdAndDate;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
}
