package evertec.cibp.web.form.bean;

import java.util.Date;

public class ModifyUSaveIframeFormBean {

	private String account;
	private String withdrawalOption = "";
	private boolean changedate;
	private Date newDate;
	private boolean submitted;
	private boolean showDateInWithdrawalDateError = false;

	private String datapowerAffiliateAccountNumber = "";
	private String datapowerAffiliateAccountSection = "";
	private String datapowerBankNumber = "";
	private String datapowerBranchNumber = "";
	private String datapowerSavingsPayoutDate = "";
	private Date datapowerSavingsPayoutDateFormatted;
	private String datapowerSavingsPayoutFrequency = "";
	private boolean datapowerCapturedData = false;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getWithdrawalOption() {
		return withdrawalOption;
	}

	public void setWithdrawalOption(String withdrawalOption) {
		this.withdrawalOption = withdrawalOption;
	}

	public boolean isChangedate() {
		return changedate;
	}

	public void setChangedate(boolean changedate) {
		this.changedate = changedate;
	}

	public Date getNewDate() {
		return newDate;
	}

	public void setNewDate(Date newDate) {
		this.newDate = newDate;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}

	public boolean isShowDateInWithdrawalDateError() {
		return showDateInWithdrawalDateError;
	}

	public void setShowDateInWithdrawalDateError(boolean showDateInWithdrawalDateError) {
		this.showDateInWithdrawalDateError = showDateInWithdrawalDateError;
	}

	public String getDatapowerAffiliateAccountNumber() {
		return datapowerAffiliateAccountNumber;
	}

	public void setDatapowerAffiliateAccountNumber(String datapowerAffiliateAccountNumber) {
		this.datapowerAffiliateAccountNumber = datapowerAffiliateAccountNumber;
	}

	public String getDatapowerAffiliateAccountSection() {
		return datapowerAffiliateAccountSection;
	}

	public void setDatapowerAffiliateAccountSection(String datapowerAffiliateAccountSection) {
		this.datapowerAffiliateAccountSection = datapowerAffiliateAccountSection;
	}

	public String getDatapowerBankNumber() {
		return datapowerBankNumber;
	}

	public void setDatapowerBankNumber(String datapowerBankNumber) {
		this.datapowerBankNumber = datapowerBankNumber;
	}

	public String getDatapowerBranchNumber() {
		return datapowerBranchNumber;
	}

	public void setDatapowerBranchNumber(String datapowerBranchNumber) {
		this.datapowerBranchNumber = datapowerBranchNumber;
	}

	public String getDatapowerSavingsPayoutDate() {
		return datapowerSavingsPayoutDate;
	}

	public void setDatapowerSavingsPayoutDate(String datapowerSavingsPayoutDate) {
		this.datapowerSavingsPayoutDate = datapowerSavingsPayoutDate;
	}

	public Date getDatapowerSavingsPayoutDateFormatted() {
		return datapowerSavingsPayoutDateFormatted;
	}

	public void setDatapowerSavingsPayoutDateFormatted(Date datapowerSavingsPayoutDateFormatted) {
		this.datapowerSavingsPayoutDateFormatted = datapowerSavingsPayoutDateFormatted;
	}

	public String getDatapowerSavingsPayoutFrequency() {
		return datapowerSavingsPayoutFrequency;
	}

	public void setDatapowerSavingsPayoutFrequency(String datapowerSavingsPayoutFrequency) {
		this.datapowerSavingsPayoutFrequency = datapowerSavingsPayoutFrequency;
	}

	public boolean isDatapowerCapturedData() {
		return datapowerCapturedData;
	}

	public void setDatapowerCapturedData(boolean datapowerCapturedData) {
		this.datapowerCapturedData = datapowerCapturedData;
	}
}