package evertec.cibp.web.form.bean;

import java.math.BigDecimal;

public class ATHMovilBean {
	private String _action = null;
	private String _tabNum = null;
	private String _account = null;
	private String _displayAccount = null;
	private String _phoneNumber = null;
	private String _recieveSmsAlerts = null;
	private String _message = null;
	private String _referenceNumber = null;
	private String _date = null;
	private String _friendId = null;
	private String _friendName = null;
	private String _friendPhone = null;
	private String _profilePhone = null;
	private String _profileAccount = null;
	private String _email = null;
	private String _confirmEmail = null;
	private String _isPrimaryAccount = null;

	private BigDecimal _amount = null;
	private BigDecimal _dailyTransferLimit = null;
	private BigDecimal _transferLimit = null;

	private String answer;

	public String getAction() {
		return _action;
	}

	public void setAction(String action) {
		_action = action;
	}

	public void setTabNum(String tabNum) {
		this._tabNum = tabNum;
	}

	public String getTabNum() {
		return _tabNum;
	}

	public String getAccount() {
		return _account;
	}

	public void setAccount(String account) {
		_account = account;
	}

	public String getDisplayAccount() {
		return _displayAccount;
	}

	public void setDisplayAccount(String displayAccount) {
		_displayAccount = displayAccount;
	}

	public String getPhoneNumber() {
		return _phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		_phoneNumber = phoneNumber;
	}

	public String getReceiveSmsAlerts() {
		return _recieveSmsAlerts;
	}

	public void setReceiveSmsAlerts(String recieveSmsAlerts) {
		_recieveSmsAlerts = recieveSmsAlerts;
	}

	public String getMessage() {
		return _message;
	}

	public void setMessage(String message) {
		_message = message;
	}

	public String getReferenceNumber() {
		return _referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		_referenceNumber = referenceNumber;
	}

	public BigDecimal getAmount() {
		return _amount;
	}

	public void setAmount(BigDecimal amount) {
		_amount = amount;
	}

	public BigDecimal getDailyTransferLimit() {
		return _dailyTransferLimit;
	}

	public void setDailyTransferLimit(BigDecimal dailyTransferLimit) {
		_dailyTransferLimit = dailyTransferLimit;
	}

	public BigDecimal getTransferLimit() {
		return _transferLimit;
	}

	public void setTransferLimit(BigDecimal transferLimit) {
		_transferLimit = transferLimit;
	}

	public String getDate() {
		return _date;
	}

	public void setDate(String date) {
		_date = date;
	}

	public String getFriendId() {
		return _friendId;
	}

	public void setFriendId(String friendId) {
		_friendId = friendId;
	}

	public String getFriendName() {
		return _friendName;
	}

	public void setFriendName(String friendName) {
		_friendName = friendName;
	}

	public String getFriendPhone() {
		return _friendPhone;
	}

	public void setFriendPhone(String friendPhone) {
		_friendPhone = friendPhone;
	}

	public String getProfilePhone() {
		return _profilePhone;
	}

	public void setProfilePhone(String profilePhone) {
		_profilePhone = profilePhone;
	}

	public String getProfileAccount() {
		return _profileAccount;
	}

	public void setProfileAccount(String profileAccount) {
		_profileAccount = profileAccount;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getConfirmEmail() {
		return _confirmEmail;
	}

	public void setConfirmEmail(String confirmEmail) {
		_confirmEmail = confirmEmail;
	}

	public String getIsPrimaryAccount() {
		return _isPrimaryAccount;
	}

	public void setIsPrimaryAccount(String isPrimaryAccount) {
		_isPrimaryAccount = isPrimaryAccount;
	}

	/**
	 * @return the rsa answer
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * @param answer
	 *            the rsa answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
}
