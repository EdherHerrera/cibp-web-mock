package evertec.cibp.web.form.bean;

import java.math.BigDecimal;
import java.util.List;

import evertec.cibp.core.models.enrollment.EnrollableAccount;

public class UpdateEnrollAcctBean {

	private List<EnrollableAccount> accounts;

	private String accountnumber;

	private String action;

	private String password;
	private String passwordconfirm;

	//MBCA-27 BY:ET56509
	private String pin;
	private String athNumber;
	private String cvv;
	private String zipCode;
	private String expMonth;
	private String expYear;
	
	//MBCA577 BY:ET56509
	private String agreement;
	
	//MBFIS-113 - mdias
	//For MLA/RVA/LEA
	private BigDecimal monthlyPayment;
	private Integer accountOpenYear;
	private String loanAccountNumber;
	
	

	public String getAccountnumber() {
		return accountnumber;
	}

	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}

	public List<EnrollableAccount> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<EnrollableAccount> accounts) {
		this.accounts = accounts;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordconfirm() {
		return passwordconfirm;
	}

	public void setPasswordconfirm(String passwordconfirm) {
		this.passwordconfirm = passwordconfirm;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}


	public String getExpYear() {
		return expYear;
	}

	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}

	public String getAthNumber() {
		return athNumber;
	}

	public void setAthNumber(String athNumber) {
		this.athNumber = athNumber;
	}

	public String getAgreement() {
		return agreement;
	}

	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}

	public BigDecimal getMonthlyPayment() {
		return monthlyPayment;
	}

	public void setMonthlyPayment(BigDecimal monthlypayment) {
		this.monthlyPayment = monthlypayment;
	}

	public Integer getAccountOpenYear() {
		return accountOpenYear;
	}

	public void setAccountOpenYear(Integer accountopenyear) {
		this.accountOpenYear = accountopenyear;
	}

	public String getLoanAccountNumber() {
		return loanAccountNumber;
	}

	public void setLoanAccountNumber(String loanAccountNumber) {
		this.loanAccountNumber = loanAccountNumber;
	}
	
}
