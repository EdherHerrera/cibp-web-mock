package evertec.cibp.web.form.bean;

public class AlertServicesBean {
	private String action;
	private boolean onOffDebitServicePush;

	public boolean isOnOffDebitServicePush() {
		return onOffDebitServicePush;
	}

	public void setOnOffDebitServicePush(boolean onOffDebitServicePush) {
		this.onOffDebitServicePush = onOffDebitServicePush;
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
