package evertec.cibp.web.form.bean;

import java.io.Serializable;

public class EnrollmentViewIncomingCheksAccountListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String accountId;
	private String account;
	private String accountDisposition;
	private String isAdded;
	
	public String getAccountId() {
		return accountId;
	}
	
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	public String getAccount() {
		return account;
	}
	
	public void setAccount(String account) {
		this.account = account;
	}
	
	public String getAccountDisposition() {
		return accountDisposition;
	}
	
	public void setAccountDisposition(String accountDisposition) {
		this.accountDisposition = accountDisposition;
	}
	
	public String getIsAdded() {
		return isAdded;
	}
	
	public void setIsAdded(String isAdded) {
		this.isAdded = isAdded;
	}
}
