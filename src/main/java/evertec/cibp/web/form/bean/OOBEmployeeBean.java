package evertec.cibp.web.form.bean;

import java.io.Serializable;



public class OOBEmployeeBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5479269747447801901L;

	public enum ACTION {
		SEND_SMSCODE, CALL_PHONE, VALIDATE_SMSCODE, VALIDATE_CALLCODE, VALIDATE_RECCODE, NO_PHONE
		, CANCEL_OOB, ALT_PHONE, VALIDATE_ALTSMSCODE, VALIDATE_ALTCALLCODE
	}

	private ACTION action;
	private String code;
	private boolean timeout;
	private boolean validationError;

	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}

	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}


	public boolean isTimeout() {
		return timeout;
	}

	public void setTimeout(boolean timeout) {
		this.timeout = timeout;
	}

	public boolean isValidationError() {
		return validationError;
	}

	public void setValidationError(boolean validationError) {
		this.validationError = validationError;
	}


}