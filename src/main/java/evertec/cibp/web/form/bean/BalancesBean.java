package evertec.cibp.web.form.bean;

public class BalancesBean {
	private String account;
	private String amountgt;
	private String amountlt;
	private boolean emailalertgtmod;
	private boolean smsalertgtmod;
	private boolean emailalertltmod;
	private boolean smsalertltmod;
	// private boolean emailalertnewstmod;
	// private boolean smsalertnewstmod;
	private boolean searching;

	private boolean emailalertgt;
	private boolean smsalertgt;
	private boolean emailalertlt;
	private boolean smsalertlt;
	// private boolean emailalertnewst;
	// private boolean smsalertnewst;
	private String smsid1;
	private String smsid2;
	private String email_id1; // CIBP-1104
	private String email_id2; // CIBP-1104
	private String mod;
	private boolean modAlert; // CIBP-1104

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAmountgt() {
		return amountgt;
	}

	public void setAmountgt(String amountgt) {
		this.amountgt = amountgt;
	}

	public String getAmountlt() {
		return amountlt;
	}

	public void setAmountlt(String amountlt) {
		this.amountlt = amountlt;
	}

	public boolean isSearching() {
		return searching;
	}

	public void setSearching(boolean searching) {
		this.searching = searching;
	}

	public boolean isEmailalertgtmod() {
		return emailalertgtmod;
	}

	public void setEmailalertgtmod(boolean emailalertgtmod) {
		this.emailalertgtmod = emailalertgtmod;
	}

	public boolean isSmsalertgtmod() {
		return smsalertgtmod;
	}

	public void setSmsalertgtmod(boolean smsalertgtmod) {
		this.smsalertgtmod = smsalertgtmod;
	}

	public boolean isEmailalertltmod() {
		return emailalertltmod;
	}

	public void setEmailalertltmod(boolean emailalertltmod) {
		this.emailalertltmod = emailalertltmod;
	}

	public boolean isSmsalertltmod() {
		return smsalertltmod;
	}

	public void setSmsalertltmod(boolean smsalertltmod) {
		this.smsalertltmod = smsalertltmod;
	}

	// public boolean isEmailalertnewstmod() {
	// return emailalertnewstmod;
	// }
	// public void setEemailalertnewstmod(boolean emailalertnewstmod) {
	// this.emailalertnewstmod = emailalertnewstmod;
	// }
	// public boolean isSmsalertnewstmod() {
	// return smsalertnewstmod;
	// }
	// public void setSmsalertnewstmod(boolean smsalertnewstmod) {
	// this.smsalertnewstmod = smsalertnewstmod;
	// }

	// public boolean isSmsalertnewst() {
	// return smsalertnewst;
	// }
	// public void setSmsalertnewst(boolean smsalertnewst) {
	// this.smsalertnewst = smsalertnewst;
	// }
	// public boolean isEmailalertnewst() {
	// return emailalertnewst;
	// }
	// public void setEmailalertnewst(boolean emailalertnewst) {
	// this.emailalertnewst = emailalertnewst;
	// }
	public boolean isEmailalertgt() {
		return emailalertgt;
	}

	public void setEmailalertgt(boolean emailalertgt) {
		this.emailalertgt = emailalertgt;
	}

	public boolean isEmailalertlt() {
		return emailalertlt;
	}

	public void setEmailalertlt(boolean emailalertlt) {
		this.emailalertlt = emailalertlt;
	}

	public boolean isSmsalertgt() {
		return smsalertgt;
	}

	public void setSmsalertgt(boolean smsalertgt) {
		this.smsalertgt = smsalertgt;
	}

	public boolean isSmsalertlt() {
		return smsalertlt;
	}

	public void setSmsalertlt(boolean smsalertlt) {
		this.smsalertlt = smsalertlt;
	}

	public String getMod() {
		return mod;
	}

	public void setMod(String mod) {
		this.mod = mod;
	}

	public String getSmsid1() {
		return smsid1;
	}

	public void setSmsid1(String smsid1) {
		this.smsid1 = smsid1;
	}

	public String getSmsid2() {
		return smsid2;
	}

	public void setSmsid2(String smsid2) {
		this.smsid2 = smsid2;
	}

	public String getEmail_id1() {
		return email_id1;
	}

	public void setEmail_id1(String email_id1) {
		this.email_id1 = email_id1;
	}

	public String getEmail_id2() {
		return email_id2;
	}

	public void setEmail_id2(String email_id2) {
		this.email_id2 = email_id2;
	}

	public boolean isModAlert() {
		return modAlert;
	}

	public void setModAlert(boolean modAlert) {
		this.modAlert = modAlert;
	}
}
