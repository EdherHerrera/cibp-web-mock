package evertec.cibp.web.form.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.vs.beans.Frequency;

public class PaymentBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private String accountFrom;
	private String payeeid;
	private String pifPayeeid;
	private BigDecimal amount;
	private Date effectiveDate;
	private Date receivingDate;
	private Frequency frequency;
	private Integer instances;
	private String recurrent;
	private String memo;
	private TVPayment tvPayment;
	private String infinite;
	private String globalPayeeId;
	private String email;
	private String invoiceNumber;
	private String subject;
	private String comments;
	private String checkSendEmail;

	public String getGlobalPayeeId() {
		return globalPayeeId;
	}

	public void setGlobalPayeeId(String globalPayeeId) {
		this.globalPayeeId = globalPayeeId;
	}

	public String getAccountFrom() {
		return accountFrom;
	}

	public void setAccountFrom(String accountFrom) {
		this.accountFrom = accountFrom;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	public Date getReceivingDate() {
		return receivingDate;
	}
	
	public void setReceivingDate(Date receivingDate) {
		this.receivingDate = receivingDate;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getInstances() {
		return instances;
	}

	public void setInstances(Integer instances) {
		this.instances = instances;
	}

	public String getRecurrent() {
		return recurrent;
	}

	public void setRecurrent(String recurrent) {
		this.recurrent = recurrent;
	}

	public String getInfinite() {
		return infinite;
	}

	public void setInfinite(String infinite) {
		this.infinite = infinite;
	}

	public TVPayment getTvPayment() {
		return tvPayment;
	}

	public void setTvPayment(TVPayment tvPayment) {
		this.tvPayment = tvPayment;
	}

	public String getPayeeid() {
		return payeeid;
	}

	public void setPayeeid(String payeeid) {
		this.payeeid = payeeid;
	}

	public String getPifPayeeid() {
		return pifPayeeid;
	}

	public void setPifPayeeid(String pifPayeeid) {
		this.pifPayeeid = pifPayeeid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getCheckSendEmail() {
		return checkSendEmail;
	}
	
	public void setCheckSendEmail(String checkSendEmail) {
		this.checkSendEmail = checkSendEmail;
	}
}
