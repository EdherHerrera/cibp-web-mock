/**
 * 
 */
package evertec.cibp.web.form.bean;

import evertec.cibp.core.models.account.TVFrontendAccount;

/**
 * Clase evertec.cibp.web.form.bean.GuidedSetupAddEbillBean Sistema: Mi Banco
 * Descripcion: Clase que representa el bean de la capa de presentacion para
 * agregar ebills en la campana guided setup
 * 
 * @author Ing. Michael Picado Fuentes Creada el Mar 15, 2011 2:58:32 PM
 */

public class GuidedSetupAddEbillBean {

	private TVFrontendAccount tvfeAccount;
	private boolean checked;

	/* Constructor de la clase GuidedSetupAddEbillBean */
	public GuidedSetupAddEbillBean() {
		// TODO Auto-generated constructor stub
	}

	/** @return regresa el tvfeAccount */
	public TVFrontendAccount getTvfeAccount() {
		return tvfeAccount;
	}

	/**
	 * @param tvfeAccount
	 *            el tvfeAccount a establecer
	 */
	public void setTvfeAccount(TVFrontendAccount tvfeAccount) {
		this.tvfeAccount = tvfeAccount;
	}

	/** @return regresa el checked */
	public boolean isChecked() {
		return checked;
	}

	/**
	 * @param checked
	 *            el checked a establecer
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

}
