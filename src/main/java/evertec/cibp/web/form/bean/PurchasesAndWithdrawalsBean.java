package evertec.cibp.web.form.bean;

public class PurchasesAndWithdrawalsBean {

	private boolean retrievePlastics;

	public boolean isRetrievePlastics() {
		return retrievePlastics;
	}

	public void setRetrievePlastics(boolean retrievePlastics) {
		this.retrievePlastics = retrievePlastics;
	}

	private String account1;
	private String athplastic1;
	private String ccaAccount;
	private String action;
	private boolean displayPlasticInfo;
	private String minAmountForAlert;

	public String getMinAmountForAlert() {
		return minAmountForAlert;
	}

	public void setMinAmountForAlert(String minAmountForAlert) {
		this.minAmountForAlert = minAmountForAlert;
	}

	public boolean isDisplayPlasticInfo() {
		return displayPlasticInfo;
	}

	public void setDisplayPlasticInfo(boolean displayPlasticInfo) {
		this.displayPlasticInfo = displayPlasticInfo;
	}

	private String purchase_thr_chksms2;

	public String getPurchase_thr_chksms2() {
		return purchase_thr_chksms2;
	}

	public void setPurchase_thr_chksms2(String purchase_thr_chksms2) {
		this.purchase_thr_chksms2 = purchase_thr_chksms2;
	}

	private String withdraw_thr_chksms2;

	public String getWithdraw_thr_chksms2() {
		return withdraw_thr_chksms2;
	}

	public void setWithdraw_thr_chksms2(String withdraw_thr_chksms2) {
		this.withdraw_thr_chksms2 = withdraw_thr_chksms2;
	}

	private String purchase_all_chksms2;
	private String withdraw_all_chksms2;
	private String dlyPurchasetotlschksms2;
	private String dlyWithdrawtotlschksms2;

	public String getPurchase_all_chksms2() {
		return purchase_all_chksms2;
	}

	public void setPurchase_all_chksms2(String purchase_all_chksms2) {
		this.purchase_all_chksms2 = purchase_all_chksms2;
	}

	public String getWithdraw_all_chksms2() {
		return withdraw_all_chksms2;
	}

	public void setWithdraw_all_chksms2(String withdraw_all_chksms2) {
		this.withdraw_all_chksms2 = withdraw_all_chksms2;
	}

	public String getDlyPurchasetotlschksms2() {
		return dlyPurchasetotlschksms2;
	}

	public void setDlyPurchasetotlschksms2(String dlyPurchasetotlschksms2) {
		this.dlyPurchasetotlschksms2 = dlyPurchasetotlschksms2;
	}

	public String getDlyWithdrawtotlschksms2() {
		return dlyWithdrawtotlschksms2;
	}

	public void setDlyWithdrawtotlschksms2(String dlyWithdrawtotlschksms2) {
		this.dlyWithdrawtotlschksms2 = dlyWithdrawtotlschksms2;
	}

	private String dlyPurchasetotlsminamt; /*
											 * Used to store a minimum dollar
											 * threshold amount for the daily
											 * total dollar amount of all
											 * purchases.
											 */

	public String getDlyPurchasetotlsminamt() {
		return dlyPurchasetotlsminamt;
	}

	public void setDlyPurchasetotlsminamt(String dlyPurchasetotlsminamt) {
		this.dlyPurchasetotlsminamt = dlyPurchasetotlsminamt;
	}

	private String dlyPurchasetotlschksms; /*
											 * Allows a user to set a checkbox
											 * for receiving sms notifications
											 * for whenever the daily total for
											 * all purchases is greater than the
											 * value set in the
											 * dlytotls_pch_minamt field.
											 */

	public String getDlyPurchasetotlschksms() {
		return dlyPurchasetotlschksms;
	}

	public void setDlyPurchasetotlschksms(String dlyPurchasetotlschksms) {
		this.dlyPurchasetotlschksms = dlyPurchasetotlschksms;
	}

	private String dlyPurchasetotlschkemail; /*
											 * Allows a user to set a checkbox
											 * for receiving email notifications
											 * for whenever the daily total for
											 * all purchases is greater than the
											 * value set in the
											 * dlytotls_pch_minamt field.
											 */

	public String getDlyPurchasetotlschkemail() {
		return dlyPurchasetotlschkemail;
	}

	public void setDlyPurchasetotlschkemail(String dlyPurchasetotlschkemail) {
		this.dlyPurchasetotlschkemail = dlyPurchasetotlschkemail;
	}

	private String dlyWithdrawtotlsminamt; /*
											 * Used to store a minimum dollar
											 * threshold amount for the daily
											 * total dollar amount of all
											 * withdrawals.
											 */

	public String getDlyWithdrawtotlsminamt() {
		return dlyWithdrawtotlsminamt;
	}

	public void setDlyWithdrawtotlsminamt(String dlyWithdrawtotlsminamt) {
		this.dlyWithdrawtotlsminamt = dlyWithdrawtotlsminamt;
	}

	private String dlyWithdrawtotlschksms; /*
											 * Allows a user to set a checkbox
											 * for receiving sms notifications
											 * for whenever the daily total for
											 * all withdrawals is greater than
											 * the value set in the
											 * dlytotls_wth_minamt field.
											 */

	public String getDlyWithdrawtotlschksms() {
		return dlyWithdrawtotlschksms;
	}

	public void setDlyWithdrawtotlschksms(String dlyWithdrawtotlschksms) {
		this.dlyWithdrawtotlschksms = dlyWithdrawtotlschksms;
	}

	private String dlyWithdrawtotlschkemail; /*
											 * Allows a user to set a checkbox
											 * for receiving email notifications
											 * for whenever the daily total for
											 * all withdrawals is greater than
											 * the value set in the
											 * dlytotls_wth_minamt field.
											 */

	public String getDlyWithdrawtotlschkemail() {
		return dlyWithdrawtotlschkemail;
	}

	public void setDlyWithdrawtotlschkemail(String dlyWithdrawtotlschkemail) {
		this.dlyWithdrawtotlschkemail = dlyWithdrawtotlschkemail;
	}

	private String dlyTransfertotlsminamt; /*
											 * Used to store a minimum dollar
											 * threshold amount for the daily
											 * total dollar amount of all
											 * transfers.
											 */

	public String getDlyTransfertotlsminamt() {
		return dlyTransfertotlsminamt;
	}

	public void setDlyTransfertotlsminamt(String dlyTransfertotlsminamt) {
		this.dlyTransfertotlsminamt = dlyTransfertotlsminamt;
	}

	private String dlyTransfertotlschksms; /*
											 * Allows a user to set a checkbox
											 * for receiving sms notifications
											 * for whenever the daily total for
											 * all transfers is greater than the
											 * value set in the
											 * dlytotls_tpg_minamt field.
											 */

	public String getDlyTransfertotlschksms() {
		return dlyTransfertotlschksms;
	}

	public void setDlyTransfertotlschksms(String dlyTransfertotlschksms) {
		this.dlyTransfertotlschksms = dlyTransfertotlschksms;
	}

	private String dlyTransfertotlschkemail; /*
											 * Allows a user to set a checkbox
											 * for receiving email notifications
											 * for whenever the daily total for
											 * all transfers is greater than the
											 * value set in the
											 * dlytotls_tpg_minamt field.
											 */

	public String getDlyTransfertotlschkemail() {
		return dlyTransfertotlschkemail;
	}

	public void setDlyTransfertotlschkemail(String dlyTransfertotlschkemail) {
		this.dlyTransfertotlschkemail = dlyTransfertotlschkemail;
	}

	private String withdraw_thr_chksms; /*
										 * Allows a user to set a checkbox for
										 * receiving sms notifications for every
										 * withdrawal that occurs against his
										 * configured account that is greater
										 * than the configured withdrawal_minamt
										 * value.
										 */

	public String getWithdraw_thr_chksms() {
		return withdraw_thr_chksms;
	}

	public void setWithdraw_thr_chksms(String withdraw_thr_chksms) {
		this.withdraw_thr_chksms = withdraw_thr_chksms;
	}

	private String withdraw_thr_chkemail; /*
										 * Allows a user to set a checkbox for
										 * receiving email notifications for
										 * every withdrawal that occurs against
										 * his configured account that is
										 * greater than the configured
										 * withdrawal_minamt value.
										 */

	public String getWithdraw_thr_chkemail() {
		return withdraw_thr_chkemail;
	}

	public void setWithdraw_thr_chkemail(String withdraw_thr_chkemail) {
		this.withdraw_thr_chkemail = withdraw_thr_chkemail;
	}

	private String purchase_thr_chksms; /*
										 * Allows a user to set a checkbox for
										 * receiving sms notifications for every
										 * purchase that occurs against his
										 * configured account that is greater
										 * than the configured purchase_minamt
										 * value.
										 */

	public String getPurchase_thr_chksms() {
		return purchase_thr_chksms;
	}

	public void setPurchase_thr_chksms(String purchase_thr_chksms) {
		this.purchase_thr_chksms = purchase_thr_chksms;
	}

	private String purchase_thr_chkemail; /*
										 * Allows a user to set a checkbox for
										 * receiving email notifications for
										 * every purchase that occurs against
										 * his configured account that is
										 * greater than the configured
										 * purchase_minamt value.
										 */

	public String getPurchase_thr_chkemail() {
		return purchase_thr_chkemail;
	}

	public void setPurchase_thr_chkemail(String purchase_thr_chkemail) {
		this.purchase_thr_chkemail = purchase_thr_chkemail;
	}

	private String telepago_thr_chksms; /*
										 * Allows a user to set a checkbox for
										 * receiving sms notifications for every
										 * transfer that occurs against his
										 * configured account that is greater
										 * than the configured telpg_min value.
										 */

	public String getTelepago_thr_chksms() {
		return telepago_thr_chksms;
	}

	public void setTelepago_thr_chksms(String telepago_thr_chksms) {
		this.telepago_thr_chksms = telepago_thr_chksms;
	}

	private String telepago_thr_chkemail; /*
										 * Allows a user to set a checkbox for
										 * receiving email notifications for
										 * every transfer that occurs against
										 * his configured account that is
										 * greater than the configured telpg_min
										 * value
										 */

	public String getTelepago_thr_chkemail() {
		return telepago_thr_chkemail;
	}

	public void setTelepago_thr_chkemail(String telepago_thr_chkemail) {
		this.telepago_thr_chkemail = telepago_thr_chkemail;
	}

	private String telepago_all_chksms; /*
										 * Allows a user to set a checkbox for
										 * receiving sms notifications for every
										 * transfer that occurs against his
										 * configured account. This differs from
										 * the field telepago_chksms, as it
										 * allows a user to set other
										 * configurations for sms notifications
										 * regarding transfers.
										 */

	public String getTelepago_all_chksms() {
		return telepago_all_chksms;
	}

	public void setTelepago_all_chksms(String telepago_all_chksms) {
		this.telepago_all_chksms = telepago_all_chksms;
	}

	private String telepago_all_chkemail; /*
										 * Allows a user to set a checkbox for
										 * receiving email notifications for
										 * every transfer that occurs against
										 * his configured account. This differs
										 * from the field telepago_chkemail, as
										 * it allows a user to set other
										 * configurations for email
										 * notifications regarding transfers.
										 */

	public String getTelepago_all_chkemail() {
		return telepago_all_chkemail;
	}

	public void setTelepago_all_chkemail(String telepago_all_chkemail) {
		this.telepago_all_chkemail = telepago_all_chkemail;
	}

	private String withdraw_all_chksms; /*
										 * Allows a user to set a checkbox for
										 * receiving sms notifications for every
										 * withdrawal that occurs against his
										 * configured account. This differs from
										 * the field withdrawal_chksms, as it
										 * allows a user to set other
										 * configurations for sms notifications
										 * regarding withdrawals.
										 */

	public String getWithdraw_all_chksms() {
		return withdraw_all_chksms;
	}

	public void setWithdraw_all_chksms(String withdraw_all_chksms) {
		this.withdraw_all_chksms = withdraw_all_chksms;
	}

	private String withdraw_all_chkemail; /*
										 * Allows a user to set a checkbox for
										 * receiving email notifications for
										 * every withdrawal that occurs against
										 * his configured account. This differs
										 * from the field withdrawal_chkemail,
										 * as it allows a user to set other
										 * configurations for email
										 * notifications regarding withdrawals.
										 */

	public String getWithdraw_all_chkemail() {
		return withdraw_all_chkemail;
	}

	public void setWithdraw_all_chkemail(String withdraw_all_chkemail) {
		this.withdraw_all_chkemail = withdraw_all_chkemail;
	}

	private String purchase_all_chksms; /*
										 * Allows a user to set a checkbox for
										 * receiving sms notifications for every
										 * purchase that occurs against his
										 * configured account. This differs from
										 * the field purchase_chksms, as it
										 * allows a user to set other
										 * configurations for sms notifications
										 * regarding purchases.
										 */

	public String getPurchase_all_chksms() {
		return purchase_all_chksms;
	}

	public void setPurchase_all_chksms(String purchase_all_chksms) {
		this.purchase_all_chksms = purchase_all_chksms;
	}

	private String purchase_all_chkemail; /*
										 * Allows a user to set a checkbox for
										 * receiving email notifications for
										 * every purchase that occurs against
										 * his configured account. This differs
										 * from the field purchase_chkemail, as
										 * it allows a user to set other
										 * configurations for email
										 * notifications regarding purchases.
										 */

	public String getPurchase_all_chkemail() {
		return purchase_all_chkemail;
	}

	public void setPurchase_all_chkemail(String purchase_all_chkemail) {
		this.purchase_all_chkemail = purchase_all_chkemail;
	}

	private String alertsPurchaseminamt;
	/*
	 * Used to store a minimum dollar threshold amount for purchases. It is
	 * recommended to put a validation check on this field, so that if a user
	 * does enter a value in this field, it should be greater than or equal to
	 * $1. Your client should also validate that if this field does contain a
	 * value, then at least one of the relevant sms/email checkboxes need to be
	 * checked: that is purchase_thr_chksms or purchase_thr_chkemail
	 */

	private String alertsWithdrawminamt;
	/*
	 * Used to store a minimum dollar threshold amount for withdrawals. It is
	 * recommended to put a validation check on this field, so that if a user
	 * does enter a value in this field, it should be greater than or equal to
	 * $1. Your client should also validate that if this field does contain a
	 * value, then at least one of the relevant sms/email checkboxes need to be
	 * checked: that is withdraw_thr_chksms or withdraw_thr_chkemail
	 */

	private String telpgminamt; /*
								 * Used to store a minimum dollar threshold
								 * amount for transfers. It is recommended to
								 * put a validation check on this field, so that
								 * if a user does enter a value in this field,
								 * it should be greater than or equal to $1.
								 * Your client should also validate that if this
								 * field does contain a value, then at least one
								 * of the relevant sms/email checkboxes need to
								 * be checked: that is telepago_thr_chksms or
								 * telepago_thr_chkemail
								 */

	public String getTelpgminamt() {
		return telpgminamt;
	}

	public void setTelpgminamt(String telpgminamt) {
		this.telpgminamt = telpgminamt;
	}

	private String profileNtfyallhour;
	/*
	 * mandatory 2 chars: Possible values are (0) for unchecked (1) for checked.
	 * If this value is set to be 0, then it is recommended to validate the
	 * user’s choices for fields’ ntfystrhour, and ntfyendhour. These fields
	 * should then store values for starting and ending hour.
	 */
	private String profileNtfyendhour;
	/*
	 * optional field and accepts a value of length (2) char. Recommended to be
	 * used to display a two digit hour ranging from 01 – 24 to represent the
	 * end hour for which the user wishes notifications to be sent out to both
	 * sms and email. This field relates to the ntfyallhour field where if
	 * ntfyallhour = 0, then this field must be > 0.
	 */
	private String profileNtfystrhour;

	/*
	 * optional field and accepts a value of length (2) char. Recommended to be
	 * used to display a two digit hour ranging from 00 – 23 to represent the
	 * start hour for which the user wishes notifications to be sent out to both
	 * sms and email. This field relates to the ntfyallhour field where if
	 * ntfyallhour = 0, then this field must be >= 0.
	 */
	// private String alertsWithdrawchkemail;
	/*
	 * Allows a user to set a global check for receiving email notifications. If
	 * this field is checked by a user then we assume that user wants email
	 * notifications for every single withdrawal that occurs against his
	 * configured account. If this field is checked, then your client should
	 * validate, and disable all other email related checkboxes and email
	 * related minimum dollar amounts textfields. The client should also disable
	 * the email field relating to combined daily totals; i.e:
	 * dlytotls_all_chkemail values are (0) for unchecked (1) for checked.
	 */
	// private String alertsWithdrawchksms;
	/*
	 * Allows a user to set a global check for receiving sms notifications. If
	 * this field is checked by a user then we assume that user wants sms
	 * notifications for every single withdrawal that occurs against his
	 * configured account. If this field is checked, then your client should
	 * validate, and disable all other sms related checkboxes and sms related
	 * minimum dollar amounts textfields. The client should also disable the sms
	 * field relating to combined daily totals; i.e: dlytotls_all_chksms values
	 * are (0) for unchecked (1) for checked.
	 */

	// private String alertsPurchasechkemail;
	/*
	 * Allows a user to set a global check for receiving email notifications. If
	 * this field is checked by a user then we assume that user wants email
	 * notifications for every single purchase that occurs against his
	 * configured account. If this field is checked, then your client should
	 * validate, and disable all other email related checkboxes and email
	 * related minimum dollar amounts textfields. The client should also disable
	 * the email field relating to combined daily totals; i.e:
	 * dlytotls_all_chkemail values are (0) for unchecked (1) for checked.
	 */
	// private String alertsPurchasechksms;
	/*
	 * Allows a user to set a global check for receiving sms notifications. If
	 * this field is checked by a user then we assume that user wants sms
	 * notifications for every single purchase that occurs against his
	 * configured account. If this field is checked, then your client should
	 * validate, and disable all other sms related checkboxes and sms related
	 * minimum dollar amounts textfields. The client should also disable the sms
	 * field relating to combined daily totals; i.e: dlytotls_all_chksms values
	 * are (0) for unchecked (1) for checked.
	 */

	public String getProfileNtfyallhour() {
		return profileNtfyallhour;
	}

	public void setProfileNtfyallhour(String profileNtfyallhour) {
		this.profileNtfyallhour = profileNtfyallhour;
	}

	public String getProfileNtfyendhour() {
		return profileNtfyendhour;
	}

	public void setProfileNtfyendhour(String profileNtfyendhour) {
		this.profileNtfyendhour = profileNtfyendhour;
	}

	public String getProfileNtfystrhour() {
		return profileNtfystrhour;
	}

	public void setProfileNtfystrhour(String profileNtfystrhour) {
		this.profileNtfystrhour = profileNtfystrhour;
	}

	// public String getAlertsPurchasechkemail() {
	// return alertsPurchasechkemail;
	// }
	// public void setAlertsPurchasechkemail(String alertsPurchasechkemail) {
	// this.alertsPurchasechkemail = alertsPurchasechkemail;
	// }
	// public String getAlertsPurchasechksms() {
	// return alertsPurchasechksms;
	// }
	// public void setAlertsPurchasechksms(String alertsPurchasechksms) {
	// this.alertsPurchasechksms = alertsPurchasechksms;
	// }
	public String getAlertsPurchaseminamt() {
		return alertsPurchaseminamt;
	}

	public void setAlertsPurchaseminamt(String alertsPurchaseminamt) {
		this.alertsPurchaseminamt = alertsPurchaseminamt;
	}

	// public String getAlertsWithdrawchkemail() {
	// return alertsWithdrawchkemail;
	// }
	// public void setAlertsWithdrawchkemail(String alertsWithdrawchkemail) {
	// this.alertsWithdrawchkemail = alertsWithdrawchkemail;
	// }
	// public String getAlertsWithdrawchksms() {
	// return alertsWithdrawchksms;
	// }
	// public void setAlertsWithdrawchksms(String alertsWithdrawchksms) {
	// this.alertsWithdrawchksms = alertsWithdrawchksms;
	// }
	public String getAlertsWithdrawminamt() {
		return alertsWithdrawminamt;
	}

	public void setAlertsWithdrawminamt(String alertsWithdrawminamt) {
		this.alertsWithdrawminamt = alertsWithdrawminamt;
	}

	public String getAthplastic1() {
		return athplastic1;
	}

	public void setAthplastic1(String athplastic1) {
		this.athplastic1 = athplastic1;
	}

	public String getCcaAccount() {
		return ccaAccount;
	}

	public void setCcaAccount(String ccaAccount) {
		this.ccaAccount = ccaAccount;
	}

	public String getAccount1() {
		return account1;
	}

	public void setAccount1(String account1) {
		this.account1 = account1;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
