package evertec.cibp.web.form.bean;

public class RemoteDepositHistoryBean {
	private String accountNumber = null;
	private String date = null;
	private long depositId = 0;
	private RemoteDepositFilterType action = RemoteDepositFilterType.SEARCH_LAST15;

	public enum RemoteDepositFilterType {
		SEARCH_LAST15, SEARCH_BY_MONTH, SEARCH_BY_ACCOUNT
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public long getDepositId() {
		return depositId;
	}

	public void setDepositId(long depositId) {
		this.depositId = depositId;
	}

	public RemoteDepositFilterType getAction() {
		return action;
	}

	public void setAction(RemoteDepositFilterType action) {
		this.action = action;
	}
}
