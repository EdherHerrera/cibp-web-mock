package evertec.cibp.web.form.bean;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.list.LazyList;

public class changeMlaEstatmentBean {

	private String id;
	private String statusAccount;
	private String accountNumber;
	private String email;
	private String emailConfirm;
	private String zipCode;
	private List<MortgageLoanBean> mortageLoan;
	
	@SuppressWarnings("unchecked")
	public changeMlaEstatmentBean() {
		mortageLoan = LazyList.decorate(new ArrayList<MortgageLoanBean>(), new Factory() {
			public Object create() {
				return new MortgageLoanBean();
			}
		});
	}

	public List<MortgageLoanBean> getMortgageLoan() {
		return mortageLoan;
	}

	public void setMortgageLoan(List<MortgageLoanBean> mortageLoan) {
		this.mortageLoan = mortageLoan;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatusAccount() {
		return statusAccount;
	}

	public void setStatusAccount(String statusAccount) {
		this.statusAccount = statusAccount;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailConfirm() {
		return emailConfirm;
	}

	public void setEmailConfirm(String emailConfirm) {
		this.emailConfirm = emailConfirm;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

}