package evertec.cibp.web.form.bean;

public class EditAccountBean {

	public enum ACTION {
		ADDTRF, MODACT, MODMOB, ADDACTMOB, MODTRFACT, HIDEACT, DELAGG, DELTRF, HIDEPIF, MODPIFACT, DELPIF
	}

	private ACTION action;
	private String id;
	private String nickname;
	private String mobnickname;
	private String hide;

	private String anterior;// CIBP-1857 ISSUE BANCA MOBILE BGARCIA

	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getMobnickname() {
		return mobnickname;
	}

	public void setMobnickname(String mobnickname) {
		this.mobnickname = mobnickname;
	}

	public String getHide() {
		return hide;
	}

	public void setHide(String hide) {
		this.hide = hide;
	}

	// CIBP-1857 ISSUE BANCA MOBILE BGARCIA
	public String getAnterior() {
		return anterior;
	}

	// CIBP-1857 ISSUE BANCA MOBILE BGARCIA
	public void setAnterior(String anterior) {
		this.anterior = anterior;
	}
}
