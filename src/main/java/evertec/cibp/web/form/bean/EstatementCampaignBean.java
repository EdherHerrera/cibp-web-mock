package evertec.cibp.web.form.bean;

public class EstatementCampaignBean {

	char signupStatus = 'X';

	public char getSignupStatus() {
		return signupStatus;
	}

	public void setSignupStatus(char signupStatus) {
		this.signupStatus = signupStatus;
	}
}
