package evertec.cibp.web.form.bean;

public class ModifyPhoneBean {

	private String residentialPhone1;
	private String residentialPhone2;
	private String residentialPhone3;
	private String workPhone1;
	private String workPhone2;
	private String workPhone3;
	private String celPhone1;
	private String celPhone2;
	private String celPhone3;

	public String getResidentialPhone1() {
		return residentialPhone1;
	}

	public void setResidentialPhone1(String residentialPhone1) {
		this.residentialPhone1 = residentialPhone1;
	}

	public String getResidentialPhone2() {
		return residentialPhone2;
	}

	public void setResidentialPhone2(String residentialPhone2) {
		this.residentialPhone2 = residentialPhone2;
	}

	public String getResidentialPhone3() {
		return residentialPhone3;
	}

	public void setResidentialPhone3(String residentialPhone3) {
		this.residentialPhone3 = residentialPhone3;
	}

	public String getWorkPhone1() {
		return workPhone1;
	}

	public void setWorkPhone1(String workPhone1) {
		this.workPhone1 = workPhone1;
	}

	public String getWorkPhone2() {
		return workPhone2;
	}

	public void setWorkPhone2(String workPhone2) {
		this.workPhone2 = workPhone2;
	}

	public String getWorkPhone3() {
		return workPhone3;
	}

	public void setWorkPhone3(String workPhone3) {
		this.workPhone3 = workPhone3;
	}

	public String getCelPhone1() {
		return celPhone1;
	}

	public void setCelPhone1(String celPhone1) {
		this.celPhone1 = celPhone1;
	}

	public String getCelPhone2() {
		return celPhone2;
	}

	public void setCelPhone2(String celPhone2) {
		this.celPhone2 = celPhone2;
	}

	public String getCelPhone3() {
		return celPhone3;
	}

	public void setCelPhone3(String celPhone3) {
		this.celPhone3 = celPhone3;
	}

}
