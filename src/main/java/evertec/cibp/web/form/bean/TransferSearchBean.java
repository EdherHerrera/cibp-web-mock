package evertec.cibp.web.form.bean;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.list.LazyList;

public class TransferSearchBean {
	private String accountNumber;
	private String date;
	private TransferFilterType action = TransferFilterType.SEARCH_LAST15;
	private String modTransfer;
	private int favId;

	private List<ModTransferBean> favorites;

	@SuppressWarnings("unchecked")
	public TransferSearchBean() {
		favorites = LazyList.decorate(new ArrayList(), new Factory() {
			public Object create() {
				return new ModTransferBean();
			}
		});
	}

	public enum TransferFilterType {
		SEARCH_LAST15, SEARCH_BY_DATE, SEARCH_BY_ACCOUNT, MOD_TRANSFER, CAN_TRANSFER, MOD_AMT
	};

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getModTransfer() {
		return modTransfer;
	}

	public void setModTransfer(String modTransfer) {
		this.modTransfer = modTransfer;
	}

	public TransferFilterType getAction() {
		return action;
	}

	public void setAction(TransferFilterType action) {
		this.action = action;
	}

	/**
	 * getFavorites
	 * @return List<ModPaymentBean>
	 */
	public List<ModTransferBean> getFavorites() {
		return favorites;
	}

	/**
	 * setFavorites
	 * @param favorites
	 */
	public void setFavorites(List<ModTransferBean> favorites) {
		this.favorites = favorites;
	}

	public int getFavId() {
		return favId;
	}

	public void setFavId(int favId) {
		this.favId = favId;
	}

}
