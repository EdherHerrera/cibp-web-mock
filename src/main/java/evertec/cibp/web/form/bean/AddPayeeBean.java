package evertec.cibp.web.form.bean;

public class AddPayeeBean {

	private String billingNumber;
	private String billingNumberConf;
	private String nickname;

	private String ckladdress1;
	private String ckladdress2;
	private String cklcity;
	private String cklstate;
	private String cklpostalCode;
	private String cklpostalCode2;
	private String cklphone;
	private String cklphone2;
	private String cklphone3;

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getCkladdress1() {
		return ckladdress1;
	}

	public void setCkladdress1(String ckladdress1) {
		this.ckladdress1 = ckladdress1;
	}

	public String getCkladdress2() {
		return ckladdress2;
	}

	public void setCkladdress2(String ckladdress2) {
		this.ckladdress2 = ckladdress2;
	}

	public String getCklcity() {
		return cklcity;
	}

	public void setCklcity(String cklcity) {
		this.cklcity = cklcity;
	}

	public String getCklstate() {
		return cklstate;
	}

	public void setCklstate(String cklstate) {
		this.cklstate = cklstate;
	}

	public String getCklpostalCode() {
		return cklpostalCode;
	}

	public void setCklpostalCode(String cklpostalCode) {
		this.cklpostalCode = cklpostalCode;
	}

	public String getCklpostalCode2() {
		return cklpostalCode2;
	}

	public void setCklpostalCode2(String cklpostalCode2) {
		this.cklpostalCode2 = cklpostalCode2;
	}

	public String getCklphone() {
		return cklphone;
	}

	public void setCklphone(String cklphone) {
		this.cklphone = cklphone;
	}

	public String getCklphone2() {
		return cklphone2;
	}

	public void setCklphone2(String cklphone2) {
		this.cklphone2 = cklphone2;
	}

	public String getCklphone3() {
		return cklphone3;
	}

	public void setCklphone3(String cklphone3) {
		this.cklphone3 = cklphone3;
	}

	public String getBillingNumber() {
		return billingNumber;
	}

	public void setBillingNumber(String billingNumber) {
		this.billingNumber = billingNumber;
	}

	public String getBillingNumberConf() {
		return billingNumberConf;
	}

	public void setBillingNumberConf(String billingNumberConf) {
		this.billingNumberConf = billingNumberConf;
	}

}