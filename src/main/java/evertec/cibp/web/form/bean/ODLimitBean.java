package evertec.cibp.web.form.bean;

import java.util.List;

import evertec.cibp.core.models.account.TVFrontendAccount;

public class ODLimitBean {

	private List<TVFrontendAccount> accountList;
	private boolean send;

	public List<TVFrontendAccount> getAccountList() {
		return accountList;
	}

	public void setAccountList(List<TVFrontendAccount> accountList) {
		this.accountList = accountList;
	}

	public boolean isSend() {
		return send;
	}

	public void setSend(boolean send) {
		this.send = send;
	}

}
