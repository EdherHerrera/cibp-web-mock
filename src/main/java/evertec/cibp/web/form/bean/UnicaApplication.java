package evertec.cibp.web.form.bean;

import java.io.Serializable;

import evertec.otherservices.beans.unica.UnicaApplicationInfo;

public class UnicaApplication implements Serializable {

	private static final long serialVersionUID = 1L;

	private UnicaApplicationInfo unicaApplicationInfo;

	// Fields for view purposes
	private String productNameVariable;
	private boolean isCredit = false;
	private boolean fromMarketplace = false;

	public UnicaApplicationInfo getUnicaApplicationInfo() {
		return unicaApplicationInfo;
	}

	public void setUnicaApplicationInfo(UnicaApplicationInfo unicaApplicationInfo) {
		this.unicaApplicationInfo = unicaApplicationInfo;
	}

	public String getProductNameVariable() {
		return productNameVariable;
	}

	public void setProductNameVariable(String productNameVariable) {
		this.productNameVariable = productNameVariable;
	}

	public boolean isCredit() {
		return isCredit;
	}

	public void setCredit(boolean isCredit) {
		this.isCredit = isCredit;
	}

	/**
	 * @return the isFromMarketplace
	 */
	public boolean isFromMarketplace() {
		return fromMarketplace;
	}

	/**
	 * @param isFromMarketplace the isFromMarketplace to set
	 */
	public void setFromMarketplace(boolean isFromMarketplace) {
		this.fromMarketplace = isFromMarketplace;
	}
	
}
