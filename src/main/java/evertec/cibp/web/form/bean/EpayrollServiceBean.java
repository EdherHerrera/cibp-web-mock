package evertec.cibp.web.form.bean;

public class EpayrollServiceBean {
	private String name;
	private String phone1;
	private String phone2;
	private String phone3;
	private String businessName;
	private String businessPhone1;
	private String businessPhone2;
	private String businessPhone3;
	
	private String residentialAddress1;
	private String residentialAddress2;
	private String residentialCity;
	private String residentialState;
	private String residentialZipCode1;
	private String residentialZipCode2;
	
	private String faxPhone1;
	private String faxPhone2;
	private String faxPhone3;
	
	private String ssne1;
	private String ssne2;
	private String ssne3;
	private String account;
	private String athplastic1;
	private String payrollEstimatedAmount;
	private String vendorEstimatedAmount;
	private String termsConfirmation;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getPhone3() {
		return phone3;
	}
	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getBusinessPhone1() {
		return businessPhone1;
	}
	public void setBusinessPhone1(String businessPhone1) {
		this.businessPhone1 = businessPhone1;
	}
	public String getBusinessPhone2() {
		return businessPhone2;
	}
	public void setBusinessPhone2(String businessPhone2) {
		this.businessPhone2 = businessPhone2;
	}
	public String getBusinessPhone3() {
		return businessPhone3;
	}
	public void setBusinessPhone3(String businessPhone3) {
		this.businessPhone3 = businessPhone3;
	}
	public String getResidentialAddress1() {
		return residentialAddress1;
	}
	public void setResidentialAddress1(String residentialAddress1) {
		this.residentialAddress1 = residentialAddress1;
	}
	public String getResidentialAddress2() {
		return residentialAddress2;
	}
	public void setResidentialAddress2(String residentialAddress2) {
		this.residentialAddress2 = residentialAddress2;
	}
	public String getResidentialCity() {
		return residentialCity;
	}
	public void setResidentialCity(String residentialCity) {
		this.residentialCity = residentialCity;
	}
	public String getResidentialState() {
		return residentialState;
	}
	public void setResidentialState(String residentialState) {
		this.residentialState = residentialState;
	}
	public String getResidentialZipCode1() {
		return residentialZipCode1;
	}
	public void setResidentialZipCode1(String residentialZipCode1) {
		this.residentialZipCode1 = residentialZipCode1;
	}
	public String getResidentialZipCode2() {
		return residentialZipCode2;
	}
	public void setResidentialZipCode2(String residentialZipCode2) {
		this.residentialZipCode2 = residentialZipCode2;
	}
	public String getFaxPhone1() {
		return faxPhone1;
	}
	public void setFaxPhone1(String faxPhone1) {
		this.faxPhone1 = faxPhone1;
	}
	public String getFaxPhone2() {
		return faxPhone2;
	}
	public void setFaxPhone2(String faxPhone2) {
		this.faxPhone2 = faxPhone2;
	}
	public String getFaxPhone3() {
		return faxPhone3;
	}
	public void setFaxPhone3(String faxPhone3) {
		this.faxPhone3 = faxPhone3;
	}
	public String getSsne1() {
		return ssne1;
	}
	public void setSsne1(String ssne1) {
		this.ssne1 = ssne1;
	}
	public String getSsne2() {
		return ssne2;
	}
	public void setSsne2(String ssne2) {
		this.ssne2 = ssne2;
	}
	public String getSsne3() {
		return ssne3;
	}
	public void setSsne3(String ssne3) {
		this.ssne3 = ssne3;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getAthplastic1() {
		return athplastic1;
	}
	public void setAthplastic1(String athplastic1) {
		this.athplastic1 = athplastic1;
	}
	public String getPayrollEstimatedAmount() {
		return payrollEstimatedAmount;
	}
	public void setPayrollEstimatedAmount(String payrollEstimatedAmount) {
		this.payrollEstimatedAmount = payrollEstimatedAmount;
	}
	public String getVendorEstimatedAmount() {
		return vendorEstimatedAmount;
	}
	public void setVendorEstimatedAmount(String vendorEstimatedAmount) {
		this.vendorEstimatedAmount = vendorEstimatedAmount;
	}
	public String getTermsConfirmation() {
		return termsConfirmation;
	}
	public void setTermsConfirmation(String termsConfirmation) {
		this.termsConfirmation = termsConfirmation;
	}
}
