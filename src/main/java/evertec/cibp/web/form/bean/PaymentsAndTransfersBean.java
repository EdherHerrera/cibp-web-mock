package evertec.cibp.web.form.bean;

public class PaymentsAndTransfersBean {
	private boolean paymentEmailConfirmation;
	private boolean paymentMobileConfirmation;
	private boolean paymentPushNotifConfirmation;

	private boolean paymentCanEmailConfirmation;
	private boolean paymentCanMobileConfirmation;
	private boolean paymentCanPushNotifConfirmation;

	private boolean paymentModCanEmailConfirmation;
	private boolean paymentModCanMobileConfirmation;
	private boolean paymentModCanPushNotifConfirmation;

	private boolean receiveBillEmailConfirmation;
	private boolean receiveBillMobileConfirmation;

	private boolean transferEmailConfirmation;
	private boolean transferMobileConfirmation;
	private boolean transferPushNotifConfirmation;

	private boolean transferCanEmailConfirmation;
	private boolean transferCanMobileConfirmation;
	private boolean transferCanPushNotifConfirmation;

	private boolean transferModCanEmailConfirmation;
	private boolean transferModCanMobileConfirmation;
	private boolean transferModCanPushNotifConfirmation;
	
	private boolean payrollMobileConfirmation;
	private boolean payrollPushNotifConfirmation;

	private boolean addpayrollMobileConfirmation;
	private boolean addpayrollPushNotifConfirmation;
	
	private boolean editpayrollMobileConfirmation;
	private boolean editpayrollPushNotifConfirmation;
	
	private boolean vendorPaymentConfirmation;
	private boolean vendorPaymentPushNotifConfirmation;
	
	private boolean addVendorConfirmation;
	private boolean addVendorPushNotifConfirmation;
	
	private boolean editVendorConfirmation;
	private boolean editVendorPushNotifConfirmation;
	
	private boolean fpsCanEmailConfirmation;
	private boolean fpsMobileConfirmation;
	private boolean fpsPushNotifConfirmation;

	private String action;

	public boolean isTransferCanEmailConfirmation() {
		return transferCanEmailConfirmation;
	}

	public void setTransferCanMobileConfirmation(boolean transferCanMobileConfirmation) {
		this.transferCanMobileConfirmation = transferCanMobileConfirmation;
	}

	public boolean isTransferCanMobileConfirmation() {
		return transferCanMobileConfirmation;
	}

	public void setTransferCanEmailConfirmation(boolean transferCanEmailConfirmation) {
		this.transferCanEmailConfirmation = transferCanEmailConfirmation;
	}

	public boolean isPaymentCanEmailConfirmation() {
		return paymentCanEmailConfirmation;
	}

	public void setPaymentCanMobileConfirmation(boolean paymentCanMobileConfirmation) {
		this.paymentCanMobileConfirmation = paymentCanMobileConfirmation;
	}

	public boolean isPaymentCanMobileConfirmation() {
		return paymentCanMobileConfirmation;
	}

	public void setPaymentCanEmailConfirmation(boolean paymentCanEmailConfirmation) {
		this.paymentCanEmailConfirmation = paymentCanEmailConfirmation;
	}

	public boolean isPaymentEmailConfirmation() {
		return paymentEmailConfirmation;
	}

	public void setPaymentEmailConfirmation(boolean paymentEmailConfirmation) {
		this.paymentEmailConfirmation = paymentEmailConfirmation;
	}

	public boolean isPaymentMobileConfirmation() {
		return paymentMobileConfirmation;
	}

	public void setPaymentMobileConfirmation(boolean paymentMobileConfirmation) {
		this.paymentMobileConfirmation = paymentMobileConfirmation;
	}

	public boolean isPaymentModCanEmailConfirmation() {
		return paymentModCanEmailConfirmation;
	}

	public void setPaymentModCanEmailConfirmation(boolean paymentModCanEmailConfirmation) {
		this.paymentModCanEmailConfirmation = paymentModCanEmailConfirmation;
	}

	public boolean isPaymentModCanMobileConfirmation() {
		return paymentModCanMobileConfirmation;
	}

	public void setPaymentModCanMobileConfirmation(boolean paymentModCanMobileConfirmation) {
		this.paymentModCanMobileConfirmation = paymentModCanMobileConfirmation;
	}

	public boolean isreceiveBillEmailConfirmation() {
		return receiveBillEmailConfirmation;
	}

	public void setreceiveBillEmailConfirmation(boolean receiveBillEmailConfirmation) {
		this.receiveBillEmailConfirmation = receiveBillEmailConfirmation;
	}

	public boolean isreceiveBillMobileConfirmation() {
		return receiveBillMobileConfirmation;
	}

	public void setReceiveBillMobileConfirmation(boolean receiveBillMobileConfirmation) {
		this.receiveBillMobileConfirmation = receiveBillMobileConfirmation;
	}

	public boolean isTransferEmailConfirmation() {
		return transferEmailConfirmation;
	}

	public void setTransferEmailConfirmation(boolean transferEmailConfirmation) {
		this.transferEmailConfirmation = transferEmailConfirmation;
	}

	public boolean isTransferMobileConfirmation() {
		return transferMobileConfirmation;
	}

	public void setTransferMobileConfirmation(boolean transferMobileConfirmation) {
		this.transferMobileConfirmation = transferMobileConfirmation;
	}

	
	public boolean isTransferModCanEmailConfirmation() {
		return transferModCanEmailConfirmation;
	}

	public void setTransferModCanEmailConfirmation(boolean transferModCanEmailConfirmation) {
		this.transferModCanEmailConfirmation = transferModCanEmailConfirmation;
	}

	public boolean isTransferModCanMobileConfirmation() {
		return transferModCanMobileConfirmation;
	}

	public void setTransferModCanMobileConfirmation(boolean transferModCanMobileConfirmation) {
		this.transferModCanMobileConfirmation = transferModCanMobileConfirmation;
	}

	public boolean isPaymentPushNotifConfirmation() {
		return paymentPushNotifConfirmation;
	}

	public void setPaymentPushNotifConfirmation(boolean paymentPushNotifConfirmation) {
		this.paymentPushNotifConfirmation = paymentPushNotifConfirmation;
	}

	public boolean isPaymentModCanPushNotifConfirmation() {
		return paymentModCanPushNotifConfirmation;
	}

	public void setPaymentModCanPushNotifConfirmation(boolean paymentModCanPushNotifConfirmation) {
		this.paymentModCanPushNotifConfirmation = paymentModCanPushNotifConfirmation;
	}

	public boolean isTransferPushNotifConfirmation() {
		return transferPushNotifConfirmation;
	}

	public void setTransferPushNotifConfirmation(boolean transferPushNotifConfirmation) {
		this.transferPushNotifConfirmation = transferPushNotifConfirmation;
	}

	public boolean isTransferModCanPushNotifConfirmation() {
		return transferModCanPushNotifConfirmation;
	}

	public void setTransferModCanPushNotifConfirmation(boolean transferModCanPushNotifConfirmation) {
		this.transferModCanPushNotifConfirmation = transferModCanPushNotifConfirmation;
	}

	public boolean isPaymentCanPushNotifConfirmation() {
		return paymentCanPushNotifConfirmation;
	}

	public void setPaymentCanPushNotifConfirmation(boolean paymentCanPushNotifConfirmation) {
		this.paymentCanPushNotifConfirmation = paymentCanPushNotifConfirmation;
	}

	public boolean isTransferCanPushNotifConfirmation() {
		return transferCanPushNotifConfirmation;
	}

	public void setTransferCanPushNotifConfirmation(boolean transferCanPushNotifConfirmation) {
		this.transferCanPushNotifConfirmation = transferCanPushNotifConfirmation;
	}
	
	public boolean isPayrollMobileConfirmation() {
		return payrollMobileConfirmation;
	}

	public void setPayrollMobileConfirmation(boolean payrollMobileConfirmation) {
		this.payrollMobileConfirmation = payrollMobileConfirmation;
	}
	
	public boolean isAddpayrollMobileConfirmation() {
		return addpayrollMobileConfirmation;
	}

	public void setAddpayrollMobileConfirmation(boolean addpayrollMobileConfirmation) {
		this.addpayrollMobileConfirmation = addpayrollMobileConfirmation;
	}
	
	public boolean isEditpayrollMobileConfirmation() {
		return editpayrollMobileConfirmation;
	}

	public void setEditpayrollMobileConfirmation(boolean editpayrollMobileConfirmation) {
		this.editpayrollMobileConfirmation = editpayrollMobileConfirmation;
	}
	
	public boolean isVendorPaymentConfirmation() {
		return vendorPaymentConfirmation;
	}

	public void setVendorPaymentConfirmation(boolean vendorPaymentConfirmation) {
		this.vendorPaymentConfirmation = vendorPaymentConfirmation;
	}
	
	public boolean isAddVendorConfirmation() {
		return addVendorConfirmation;
	}

	public void setAddVendorConfirmation(boolean addVendorConfirmation) {
		this.addVendorConfirmation = addVendorConfirmation;
	}
	
	public boolean isEditVendorConfirmation() {
		return editVendorConfirmation;
	}

	public void setEditVendorConfirmation(boolean editVendorConfirmation) {
		this.editVendorConfirmation = editVendorConfirmation;
	}

	public boolean isPayrollPushNotifConfirmation() {
		return payrollPushNotifConfirmation;
	}

	public void setPayrollPushNotifConfirmation(boolean payrollPushNotifConfirmation) {
		this.payrollPushNotifConfirmation = payrollPushNotifConfirmation;
	}

	public boolean isAddpayrollPushNotifConfirmation() {
		return addpayrollPushNotifConfirmation;
	}

	public void setAddpayrollPushNotifConfirmation(boolean addpayrollPushNotifConfirmation) {
		this.addpayrollPushNotifConfirmation = addpayrollPushNotifConfirmation;
	}

	public boolean isEditpayrollPushNotifConfirmation() {
		return editpayrollPushNotifConfirmation;
	}

	public void setEditpayrollPushNotifConfirmation(boolean editpayrollPushNotifConfirmation) {
		this.editpayrollPushNotifConfirmation = editpayrollPushNotifConfirmation;
	}

	public boolean isVendorPaymentPushNotifConfirmation() {
		return vendorPaymentPushNotifConfirmation;
	}

	public void setVendorPaymentPushNotifConfirmation(boolean vendorPaymentPushNotifConfirmation) {
		this.vendorPaymentPushNotifConfirmation = vendorPaymentPushNotifConfirmation;
	}

	public boolean isAddVendorPushNotifConfirmation() {
		return addVendorPushNotifConfirmation;
	}

	public void setAddVendorPushNotifConfirmation(boolean addVendorPushNotifConfirmation) {
		this.addVendorPushNotifConfirmation = addVendorPushNotifConfirmation;
	}

	public boolean isEditVendorPushNotifConfirmation() {
		return editVendorPushNotifConfirmation;
	}

	public void setEditVendorPushNotifConfirmation(boolean editVendorPushNotifConfirmation) {
		this.editVendorPushNotifConfirmation = editVendorPushNotifConfirmation;
	}

	
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public boolean isFpsCanEmailConfirmation() {
		return fpsCanEmailConfirmation;
	}

	public void setFpsCanEmailConfirmation(boolean fpsCanEmailConfirmation) {
		this.fpsCanEmailConfirmation = fpsCanEmailConfirmation;
	}

	public boolean isFpsMobileConfirmation() {
		return fpsMobileConfirmation;
	}

	public void setFpsMobileConfirmation(boolean fpsMobileConfirmation) {
		this.fpsMobileConfirmation = fpsMobileConfirmation;
	}

	/**
	 * @return the fpsPushNotifConfirmation
	 */
	public boolean isFpsPushNotifConfirmation() {
		return fpsPushNotifConfirmation;
	}

	/**
	 * @param fpsPushNotifConfirmation the fpsPushNotifConfirmation to set
	 */
	public void setFpsPushNotifConfirmation(boolean fpsPushNotifConfirmation) {
		this.fpsPushNotifConfirmation = fpsPushNotifConfirmation;
	}
}

