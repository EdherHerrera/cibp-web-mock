package evertec.cibp.web.form.bean;

public class PIFSmallDepositsBean {

	private String accountId;
	private String deposit1;
	private String deposit2;
	private String confirmation1;
	private String confirmation2;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getDeposit1() {
		return deposit1;
	}

	public void setDeposit1(String deposit1) {
		this.deposit1 = deposit1;
	}

	public String getDeposit2() {
		return deposit2;
	}

	public void setDeposit2(String deposit2) {
		this.deposit2 = deposit2;
	}

	public String getConfirmation1() {
		return confirmation1;
	}

	public void setConfirmation1(String confirmation1) {
		this.confirmation1 = confirmation1;
	}

	public String getConfirmation2() {
		return confirmation2;
	}

	public void setConfirmation2(String confirmation2) {
		this.confirmation2 = confirmation2;
	}
}