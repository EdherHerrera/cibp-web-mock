package evertec.cibp.web.form.bean;

public class EditEmailEstatementCampaignBean {
	private String email;
	private String emailconfirm;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailconfirm() {
		return emailconfirm;
	}

	public void setEmailconfirm(String emailconfirm) {
		this.emailconfirm = emailconfirm;
	}

}
