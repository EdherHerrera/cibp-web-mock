package evertec.cibp.web.form.bean;

import java.math.BigInteger;
import java.util.List;

import com.rsa.csd.ws.DeviceRequest;

import evertec.cibp.core.models.enrollment.EnrollableAccount;

public class SSDSEnrollmentBean {
	private String firstname;
	private String lastname;
	private String email;
	private String emailconfirm;
	private String taxId;
	private int dobday;
	private int dobmonth;
	private int dobyear;
	private String username;
	private String password;
	private String accountnumber;
	private String athnumber;
	private String agreement;
	private List<EnrollableAccount> accounts;
	private DeviceRequest deviceRequest;
	private BigInteger permId;
	private String accountInfoType;
	private String maidenName;
	private boolean validated;
	private String cibpServiceUser;
	private String cibpServicePass;
	private boolean enrollTelepago;

	public String getEnrollTelepago() {
		return String.valueOf(enrollTelepago);
	}

	public boolean isEnrollTelepago() {
		return enrollTelepago;
	}

	public void setEnrollTelepago(boolean enrollTelepago) {
		this.enrollTelepago = enrollTelepago;
	}

	public String getCibpServiceUser() {
		return cibpServiceUser;
	}

	public void setCibpServiceUser(String cibpServiceUser) {
		this.cibpServiceUser = cibpServiceUser;
	}

	public String getCibpServicePass() {
		return cibpServicePass;
	}

	public void setCibpServicePass(String cibpServicePass) {
		this.cibpServicePass = cibpServicePass;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getDobday() {
		return dobday;
	}

	public void setDobday(int dobday) {
		this.dobday = dobday;
	}

	public int getDobmonth() {
		return dobmonth;
	}

	public void setDobmonth(int dobmonth) {
		this.dobmonth = dobmonth;
	}

	public int getDobyear() {
		return dobyear;
	}

	public void setDobyear(int dobyear) {
		this.dobyear = dobyear;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		if (username != null)
			this.username = username.toLowerCase();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAccountnumber() {
		return accountnumber;
	}

	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}

	public String getAthnumber() {
		return athnumber;
	}

	public void setAthnumber(String athnumber) {
		this.athnumber = athnumber;
	}

	public String getAgreement() {
		return agreement;
	}

	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}

	public List<EnrollableAccount> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<EnrollableAccount> accounts) {
		this.accounts = accounts;
	}

	public BigInteger getPermId() {
		return permId;
	}

	public void setPermId(BigInteger permId) {
		this.permId = permId;
	}

	public String getAccountInfoType() {
		return accountInfoType;
	}

	public void setAccountInfoType(String accountInfoType) {
		this.accountInfoType = accountInfoType;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getMaidenName() {
		return maidenName;
	}

	public void setMaidenName(String maidenName) {
		this.maidenName = maidenName;
	}

	public String getEmailconfirm() {
		return emailconfirm;
	}

	public void setEmailconfirm(String emailconfirm) {
		this.emailconfirm = emailconfirm;
	}

	public boolean isValidated() {
		return validated;
	}

	public void setValidated(boolean validated) {
		this.validated = validated;
	}

	public DeviceRequest getDeviceRequest() {
		return deviceRequest;
	}

	public void setDeviceRequest(DeviceRequest deviceRequest) {
		this.deviceRequest = deviceRequest;
	}
}
