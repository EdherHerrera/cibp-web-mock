package evertec.cibp.web.form.bean;

public class EbppStatusUpdateBean {

	public enum ACTION {
		UPDATE_EBILL_STATUS, UPDATE_ESTMT_STATUS, ALGARO_UPDATE
	};

	private String id;
	private ACTION action;
	private String email;
	private String emailConfirm;
	private String zipCode;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailConfirm() {
		return emailConfirm;
	}

	public void setEmailConfirm(String emailConfirm) {
		this.emailConfirm = emailConfirm;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

}