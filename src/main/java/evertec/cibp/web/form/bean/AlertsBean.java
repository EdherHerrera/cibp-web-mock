package evertec.cibp.web.form.bean;

public class AlertsBean implements Cloneable {
	private boolean paymentEmailConfirmation;
	private boolean paymentMobileConfirmation;
	private boolean transferEmailConfirmation;
	private boolean transferMobileConfirmation;

	private boolean paymentModifyEmailConfirmation;
	private boolean paymentModifyMobileConfirmation;
	private boolean transferModifyEmailConfirmation;
	private boolean transferModifyMobileConfirmation;

	private boolean paymentCancelEmailConfirmation;
	private boolean paymentCancelMobileConfirmation;
	private boolean transferCancelEmailConfirmation;
	private boolean transferCancelMobileConfirmation;

	private boolean account1MobileConfirmation;
	private boolean account2MobileConfirmation;
	private String account1;
	private String account1operator;
	private String account1amount;
	private String account2;
	private String account2operator;
	private String account2amount;
	private String smsid1;
	private String smsid2;
	private String email_id1; // CIBP-1104
	private String email_id2; // CIBP-1104
	private String action;
	private String mod;
	private boolean modAlert; // CIBP-1104

	// CIBP-1104
	private boolean passwordChangeEmail;
	private boolean passwordChangeMobile;

	private boolean blockedPasswordEmail;
	private boolean blockedPasswordMobile;

	private boolean blockedSecretQuestionsEmail;
	private boolean blockedSecretQuestionsMobile;

	private boolean secretQuestionsUpdatedEmail;
	private boolean secretQuestionsUpdatedMobile;

	private boolean emailUpdatedEmail;
	private boolean emailUpdatedMobile;

	private boolean phoneNumberUpdatedEmail;
	private boolean phoneNumberUpdatedMobile;

	private boolean addressUpdatedEmail;
	private boolean addressUpdatedMobile;

	private boolean mobileBankingPhoneNumberUpdatedEmail;
	private boolean mobileBankingPhoneNumberUpdatedMobile;

	private boolean account1EmailConfirmation;
	private boolean account2EmailConfirmation;

	// FFEIC
	private boolean newDeviceEmail;
	private boolean newDeviceMobile;

	// FIN FFEIC

	private String bothBalAcc1;
	private String bothBalAcc2;

	public String getBothBalAcc1() {
		return bothBalAcc1;
	}

	public void setBothBalAcc1(String bothBalAcc1) {
		this.bothBalAcc1 = bothBalAcc1;
	}

	public String getBothBalAcc2() {
		return bothBalAcc2;
	}

	public void setBothBalAcc2(String bothBalAcc2) {
		this.bothBalAcc2 = bothBalAcc2;
	}

	public boolean isPaymentEmailConfirmation() {
		return paymentEmailConfirmation;
	}

	public void setPaymentEmailConfirmation(boolean paymentEmailConfirmation) {
		this.paymentEmailConfirmation = paymentEmailConfirmation;
	}

	public boolean isPaymentMobileConfirmation() {
		return paymentMobileConfirmation;
	}

	public void setPaymentMobileConfirmation(boolean paymentMobileConfirmation) {
		this.paymentMobileConfirmation = paymentMobileConfirmation;
	}

	public boolean isTransferEmailConfirmation() {
		return transferEmailConfirmation;
	}

	public void setTransferEmailConfirmation(boolean transferEmailConfirmation) {
		this.transferEmailConfirmation = transferEmailConfirmation;
	}

	public boolean isTransferMobileConfirmation() {
		return transferMobileConfirmation;
	}

	public void setTransferMobileConfirmation(boolean transferMobileConfirmation) {
		this.transferMobileConfirmation = transferMobileConfirmation;
	}

	public boolean isAccount1MobileConfirmation() {
		return account1MobileConfirmation;
	}

	public void setAccount1MobileConfirmation(boolean account1MobileConfirmation) {
		this.account1MobileConfirmation = account1MobileConfirmation;
	}

	public boolean isAccount2MobileConfirmation() {
		return account2MobileConfirmation;
	}

	public void setAccount2MobileConfirmation(boolean account2MobileConfirmation) {
		this.account2MobileConfirmation = account2MobileConfirmation;
	}

	public String getAccount1() {
		return account1;
	}

	public void setAccount1(String account1) {
		this.account1 = account1;
	}

	public String getAccount1operator() {
		return account1operator;
	}

	public void setAccount1operator(String account1operator) {
		this.account1operator = account1operator;
	}

	public String getAccount1amount() {
		return account1amount;
	}

	public void setAccount1amount(String account1amount) {
		this.account1amount = account1amount;
	}

	public String getAccount2() {
		return account2;
	}

	public void setAccount2(String account2) {
		this.account2 = account2;
	}

	public String getAccount2operator() {
		return account2operator;
	}

	public void setAccount2operator(String account2operator) {
		this.account2operator = account2operator;
	}

	public String getAccount2amount() {
		return account2amount;
	}

	public void setAccount2amount(String account2amount) {
		this.account2amount = account2amount;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getSmsid1() {
		return smsid1;
	}

	public void setSmsid1(String smsid1) {
		this.smsid1 = smsid1;
	}

	public String getSmsid2() {
		return smsid2;
	}

	public void setSmsid2(String smsid2) {
		this.smsid2 = smsid2;
	}

	public String getMod() {
		return mod;
	}

	public void setMod(String mod) {
		this.mod = mod;
	}

	public boolean isPaymentModifyEmailConfirmation() {
		return paymentModifyEmailConfirmation;
	}

	public void setPaymentModifyEmailConfirmation(boolean paymentModifyEmailConfirmation) {
		this.paymentModifyEmailConfirmation = paymentModifyEmailConfirmation;
	}

	public boolean isPaymentModifyMobileConfirmation() {
		return paymentModifyMobileConfirmation;
	}

	public void setPaymentModifyMobileConfirmation(boolean paymentModifyMobileConfirmation) {
		this.paymentModifyMobileConfirmation = paymentModifyMobileConfirmation;
	}

	public boolean isTransferModifyEmailConfirmation() {
		return transferModifyEmailConfirmation;
	}

	public void setTransferModifyEmailConfirmation(boolean transferModifyEmailConfirmation) {
		this.transferModifyEmailConfirmation = transferModifyEmailConfirmation;
	}

	public boolean isTransferModifyMobileConfirmation() {
		return transferModifyMobileConfirmation;
	}

	public void setTransferModifyMobileConfirmation(boolean transferModifyMobileConfirmation) {
		this.transferModifyMobileConfirmation = transferModifyMobileConfirmation;
	}

	public boolean isPaymentCancelEmailConfirmation() {
		return paymentCancelEmailConfirmation;
	}

	public void setPaymentCancelEmailConfirmation(boolean paymentCancelEmailConfirmation) {
		this.paymentCancelEmailConfirmation = paymentCancelEmailConfirmation;
	}

	public boolean isPaymentCancelMobileConfirmation() {
		return paymentCancelMobileConfirmation;
	}

	public void setPaymentCancelMobileConfirmation(boolean paymentCancelMobileConfirmation) {
		this.paymentCancelMobileConfirmation = paymentCancelMobileConfirmation;
	}

	public boolean isTransferCancelEmailConfirmation() {
		return transferCancelEmailConfirmation;
	}

	public void setTransferCancelEmailConfirmation(boolean transferCancelEmailConfirmation) {
		this.transferCancelEmailConfirmation = transferCancelEmailConfirmation;
	}

	public boolean isTransferCancelMobileConfirmation() {
		return transferCancelMobileConfirmation;
	}

	public void setTransferCancelMobileConfirmation(boolean transferCancelMobileConfirmation) {
		this.transferCancelMobileConfirmation = transferCancelMobileConfirmation;
	}

	// CIBP-1104
	public boolean isPasswordChangeEmail() {
		return passwordChangeEmail;
	}

	public void setPasswordChangeEmail(boolean passwordChangeEmail) {
		this.passwordChangeEmail = passwordChangeEmail;
	}

	public boolean isPasswordChangeMobile() {
		return passwordChangeMobile;
	}

	public void setPasswordChangeMobile(boolean passwordChangeMobile) {
		this.passwordChangeMobile = passwordChangeMobile;
	}

	public boolean isBlockedPasswordEmail() {
		return blockedPasswordEmail;
	}

	public void setBlockedPasswordEmail(boolean blockedPasswordEmail) {
		this.blockedPasswordEmail = blockedPasswordEmail;
	}

	public boolean isBlockedPasswordMobile() {
		return blockedPasswordMobile;
	}

	public void setBlockedPasswordMobile(boolean blockedPasswordMobile) {
		this.blockedPasswordMobile = blockedPasswordMobile;
	}

	public boolean isBlockedSecretQuestionsEmail() {
		return blockedSecretQuestionsEmail;
	}

	public void setBlockedSecretQuestionsEmail(boolean blockedSecretQuestionsEmail) {
		this.blockedSecretQuestionsEmail = blockedSecretQuestionsEmail;
	}

	public boolean isBlockedSecretQuestionsMobile() {
		return blockedSecretQuestionsMobile;
	}

	public void setBlockedSecretQuestionsMobile(boolean blockedSecretQuestionsMobile) {
		this.blockedSecretQuestionsMobile = blockedSecretQuestionsMobile;
	}

	public boolean isSecretQuestionsUpdatedEmail() {
		return secretQuestionsUpdatedEmail;
	}

	public void setSecretQuestionsUpdatedEmail(boolean secretQuestionsUpdatedEmail) {
		this.secretQuestionsUpdatedEmail = secretQuestionsUpdatedEmail;
	}

	public boolean isSecretQuestionsUpdatedMobile() {
		return secretQuestionsUpdatedMobile;
	}

	public void setSecretQuestionsUpdatedMobile(boolean secretQuestionsUpdatedMobile) {
		this.secretQuestionsUpdatedMobile = secretQuestionsUpdatedMobile;
	}

	public boolean isEmailUpdatedEmail() {
		return emailUpdatedEmail;
	}

	public void setEmailUpdatedEmail(boolean emailUpdatedEmail) {
		this.emailUpdatedEmail = emailUpdatedEmail;
	}

	public boolean isEmailUpdatedMobile() {
		return emailUpdatedMobile;
	}

	public void setEmailUpdatedMobile(boolean emailUpdatedMobile) {
		this.emailUpdatedMobile = emailUpdatedMobile;
	}

	public boolean isPhoneNumberUpdatedEmail() {
		return phoneNumberUpdatedEmail;
	}

	public void setPhoneNumberUpdatedEmail(boolean phoneNumberUpdatedEmail) {
		this.phoneNumberUpdatedEmail = phoneNumberUpdatedEmail;
	}

	public boolean isPhoneNumberUpdatedMobile() {
		return phoneNumberUpdatedMobile;
	}

	public void setPhoneNumberUpdatedMobile(boolean phoneNumberUpdatedMobile) {
		this.phoneNumberUpdatedMobile = phoneNumberUpdatedMobile;
	}

	public boolean isAddressUpdatedEmail() {
		return addressUpdatedEmail;
	}

	public void setAddressUpdatedEmail(boolean addressUpdatedEmail) {
		this.addressUpdatedEmail = addressUpdatedEmail;
	}

	public boolean isAddressUpdatedMobile() {
		return addressUpdatedMobile;
	}

	public void setAddressUpdatedMobile(boolean addressUpdatedMobile) {
		this.addressUpdatedMobile = addressUpdatedMobile;
	}

	public boolean isMobileBankingPhoneNumberUpdatedEmail() {
		return mobileBankingPhoneNumberUpdatedEmail;
	}

	public void setMobileBankingPhoneNumberUpdatedEmail(boolean mobileBankingPhoneNumberUpdatedEmail) {
		this.mobileBankingPhoneNumberUpdatedEmail = mobileBankingPhoneNumberUpdatedEmail;
	}

	public boolean isMobileBankingPhoneNumberUpdatedMobile() {
		return mobileBankingPhoneNumberUpdatedMobile;
	}

	public void setMobileBankingPhoneNumberUpdatedMobile(boolean mobileBankingPhoneNumberUpdatedMobile) {
		this.mobileBankingPhoneNumberUpdatedMobile = mobileBankingPhoneNumberUpdatedMobile;
	}

	public boolean isAccount1EmailConfirmation() {
		return account1EmailConfirmation;
	}

	public void setAccount1EmailConfirmation(boolean account1EmailConfirmation) {
		this.account1EmailConfirmation = account1EmailConfirmation;
	}

	public boolean isAccount2EmailConfirmation() {
		return account2EmailConfirmation;
	}

	public void setAccount2EmailConfirmation(boolean account2EmailConfirmation) {
		this.account2EmailConfirmation = account2EmailConfirmation;
	}

	// FIN ---- CIBP-1104
	public boolean isModAlert() {
		return modAlert;
	}

	public void setModAlert(boolean modAlert) {
		this.modAlert = modAlert;
	}

	public String getEmail_id1() {
		return email_id1;
	}

	public void setEmail_id1(String email_id1) {
		this.email_id1 = email_id1;
	}

	public String getEmail_id2() {
		return email_id2;
	}

	public void setEmail_id2(String email_id2) {
		this.email_id2 = email_id2;
	}

	public boolean isNewDeviceEmail() {
		return newDeviceEmail;
	}

	public void setNewDeviceEmail(boolean newDeviceEmail) {
		this.newDeviceEmail = newDeviceEmail;
	}

	public boolean isNewDeviceMobile() {
		return newDeviceMobile;
	}

	public void setNewDeviceMobile(boolean newDeviceMobile) {
		this.newDeviceMobile = newDeviceMobile;
	}

	public final AlertsBean clone() {
		try {
			AlertsBean cloned = (AlertsBean) super.clone();
			return cloned;
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
}
