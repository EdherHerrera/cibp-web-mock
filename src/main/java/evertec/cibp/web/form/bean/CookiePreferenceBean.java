package evertec.cibp.web.form.bean;

import java.io.Serializable;
/**
 * Cookie Preference Bean
 * 
 * @author ET58344, Evertec Inc.
 * @date 02-02-2021
 * @version 1.0 
 */
public class CookiePreferenceBean implements Serializable {
	
	/**
	 * Serial version uid
	 */
	private static final long serialVersionUID = -7156461828297694605L;

	private String checkedSwAnalytic = "false";
	private String checkedSwFunctional = "false";
	private String checkedSwAdvertising = "false";
	
	/**
	 * Get checked Analytic
	 * @author ET58344, Evertec Inc.
	 * @date 02-02-2021
	 * @return the checkedSwAnalytic string
	 */
	
	public String getCheckedSwAnalytic() {
		return checkedSwAnalytic;
	}
	
	/**
	 * Set checked Analytic
	 * @author ET58344, Evertec Inc.
	 * @date 02-02-2021
	 * @param checkedSwAnalytic the checkedSwAnalytic to set
	 */
	public void setCheckedSwAnalytic(String checkedSwAnalytic) {
		this.checkedSwAnalytic = checkedSwAnalytic;
	}
	
	/**
	 * Get checked Functional
	 * @author ET58344, Evertec Inc.
	 * @date 02-02-2021
	 * @return the checkedSwFunctional string
	 */
	public String getCheckedSwFunctional() {
		return checkedSwFunctional;
	}
	
	/**
	 * Set checked Functional
	 * @author ET58344, Evertec Inc.
	 * @date 02-02-2021
	 * @param checkedSwFunctional the checkedSwFunctional to set
	 */
	public void setCheckedSwFunctional(String checkedSwFunctional) {
		this.checkedSwFunctional = checkedSwFunctional;
	}

	/**
	 * Get checked Advertising
	 * @author ET58344, Evertec Inc.
	 * @date 02-02-2021
	 * @return the checkedSwAdvertising string
	 */
	public String getCheckedSwAdvertising() {
		return checkedSwAdvertising;
	}
	
	/**
	 * Set checked Analytic
	 * @author ET58344, Evertec Inc.
	 * @date 02-02-2021
	 * @param checkedSwAdvertising the checkedSwAdvertising to set
	 */
	public void setCheckedSwAdvertising(String checkedSwAdvertising) {
		this.checkedSwAdvertising = checkedSwAdvertising;
	}
	
	
}
