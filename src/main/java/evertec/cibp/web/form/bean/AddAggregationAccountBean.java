package evertec.cibp.web.form.bean;

import java.util.List;

import evertec.cibp.core.models.aggregation.AggregationDiscoverAccountsBean;

public class AddAggregationAccountBean {
	private String username;
	private String password;
	private Integer institutionid;
	private List<AggregationDiscoverAccountsBean> aggregationdiscoveraccountsList;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getInstitutionid() {
		return institutionid;
	}

	public void setInstitutionid(Integer institutionid) {
		this.institutionid = institutionid;
	}

	public List<AggregationDiscoverAccountsBean> getAggregationdiscoveraccountsList() {
		return aggregationdiscoveraccountsList;
	}

	public void setAggregationdiscoveraccountsList(List<AggregationDiscoverAccountsBean> aggregationdiscoveraccountsList) {
		this.aggregationdiscoveraccountsList = aggregationdiscoveraccountsList;
	}
}
