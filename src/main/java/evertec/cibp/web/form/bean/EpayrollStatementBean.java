package evertec.cibp.web.form.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.list.LazyList;

public class EpayrollStatementBean implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String name;
	
	private String businessName;
	
	private String ssn;
	
	private String phone1;
	private String phone2;
	private String phone3;
	
	private String businessPhone1;
	private String businessPhone2;
	private String businessPhone3;
	
	private String emails;
	
	private String termsConfirmation;
	
	private String account;
	private String productDescription;
	private String relationship;
	private String type;
	private String isValid;
	
	private List<EpayrollEmailStatementBean> emailList;
	
	@SuppressWarnings("unchecked")
	public EpayrollStatementBean(){
		emailList = LazyList.decorate(new ArrayList<EpayrollEmailStatementBean>(), new Factory() {
			public Object create() {
				return new EpayrollEmailStatementBean();
			}
		});
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public String getBusinessPhone1() {
		return businessPhone1;
	}

	public void setBusinessPhone1(String businessPhone1) {
		this.businessPhone1 = businessPhone1;
	}

	public String getBusinessPhone2() {
		return businessPhone2;
	}

	public void setBusinessPhone2(String businessPhone2) {
		this.businessPhone2 = businessPhone2;
	}

	public String getBusinessPhone3() {
		return businessPhone3;
	}

	public void setBusinessPhone3(String businessPhone3) {
		this.businessPhone3 = businessPhone3;
	}

	public String getEmails() {
		return emails;
	}

	public void setEmails(String emails) {
		this.emails = emails;
	}

	public String getTermsConfirmation() {
		return termsConfirmation;
	}

	public void setTermsConfirmation(String termsConfirmation) {
		this.termsConfirmation = termsConfirmation;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getIsValid() {
		return isValid;
	}
	
	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}

	public List<EpayrollEmailStatementBean> getEmailList() {
		return emailList;
	}

	public void setEmailList(List<EpayrollEmailStatementBean> emailList) {
		this.emailList = emailList;
	}
}
