/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evertec.cibp.web.form.bean;

import java.util.List;

/**
 * 
 * @author ET23558
 */
public class CampaignGenericBean_MovedToCore {

	public static final String ACTIVE_ODLIMIT = "A";
	private List campaignProductsId;
	private List invalidSufix;

	/**
	 * @return the campaignProductsId
	 */
	public List getCampaignProductsId() {
		return campaignProductsId;
	}

	/**
	 * @param campaignProductsId
	 *            the campaignProductsId to set
	 */
	public void setCampaignProductsId(List campaignProductsId) {
		this.campaignProductsId = campaignProductsId;
	}

	/**
	 * @return the invalidSufix
	 */
	public List getInvalidSufix() {
		return invalidSufix;
	}

	/**
	 * @param invalidSufix
	 *            the invalidSufix to set
	 */
	public void setInvalidSufix(List invalidSufix) {
		this.invalidSufix = invalidSufix;
	}
}
