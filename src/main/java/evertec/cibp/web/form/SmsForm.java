package evertec.cibp.web.form;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.EventType;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.SMSEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.SMSProfileModifyRequest;
import evertec.cibp.core.services.requests.SendSmsGeneratedCodeRequest;
import evertec.cibp.core.services.requests.SmsEnrollUserRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.SmsAlertsResponse;
import evertec.cibp.core.services.responses.SmsAlertsResponse.Result;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.SmsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SmsForm extends AbstractWizardFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private static final String USERSESSION = "userSession";
	public SmsForm() {
		super();
		setCommandClass(SmsBean.class);
		setCommandName("sms");
		setPages(new String[] { "sms", "sms" });
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		SmsBean infobean = (SmsBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		request.getSession().removeAttribute("pageSms");

		if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			request.getSession().removeAttribute("smsExito");
		}
		
		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setPath("/");
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		if (cookie != null) 
			ESAPI.httpUtilities().addCookie(response, cookie);

		
		String phone = (String) WebUtils.getSessionAttribute(request, "smsPhonereceived");
		if (infobean.getPhone1() == null && infobean.getPhone2() == null && infobean.getPhone3() == null
				&& infobean.getProvider() == null) {
			infobean.setPhone1(phone.substring(0, 3));
			infobean.setPhone2(phone.substring(3, 6));
			infobean.setPhone3(phone.substring(6, phone.length()));
			infobean.setProvider((String) WebUtils.getSessionAttribute(request, "smsProvider"));
		}

		SmsBean.ACTION action = infobean.getAction();

		if (action == SmsBean.ACTION.SMS_CODE_VER) {
			String smscode = infobean.getSmscode();
			String provider = (String) WebUtils.getSessionAttribute(request, "smsProvider");
			String generatedCode = (String) WebUtils.getSessionAttribute(request, "smsGeneratedCode");
			if (smscode.equalsIgnoreCase(generatedCode)) {
				if (customer.getEntitlementByType(EntitlementType.MOBILE_BANKING) != null
						&& userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)) != null) {
					// means Customer is enrolled, must modify phone number
					SMSEntitlement ent = new SMSEntitlement(
							customer.getEntitlementByType(EntitlementType.MOBILE_BANKING));
					try {
						SMSProfileModifyRequest modreq = new SMSProfileModifyRequest(
								Utils.generateCustomerInteractionEvent(request), customer,
								userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)), ent, phone,
								provider);
						customerService.modifySMSUserProfile(modreq);
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession.setStatusmessage("Your phone number for Mi Banco Alerts has been modified.");
						else
							userSession
									.setStatusmessage("Su n&uacute;mero de tel&eacute;fono para Mi Banco Alertas ha sido modificado.");

						/* CIBP-1104 */
						boolean hasalert = false;
						AlertsEntitlement a_ent = new AlertsEntitlement(
								customer.getEntitlementByType(EntitlementType.ALERTS));
						if (a_ent != null && a_ent.getAlerts() != null) {
							Set<AlertType> alerts = a_ent.getAlerts();
							if (alerts.contains(AlertType.MOBILE_PHONE_EDIT_EMAIL) || alerts.contains(AlertType.MOBILE_PHONE_EDIT_SMS)
									|| alerts.contains(AlertType.MOBILE_PHONE_EDIT_PUSH)) {
								hasalert = true;
							}
						}
						/*
						 * CIBP-1104 se activa envio alerta de cambio de
						 * telefono banca movil
						 */
						if (hasalert) {
							customerService.sendMobilePhoneEditConfirmation(customer,
									userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
									userSession.getPreferredLanguage());
						}
						/* FIN--- CIBP-1104 */

						model.put("phoneUpdated", true);
						if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
							request.getSession().setAttribute("smsExito", true);
						}

						AccountMaskUtil maskutil = new AccountMaskUtil();
						List<TVFrontendAccount> accounts = userSession.getAccounts();
						List<evertec.cibp.core.services.smsAlerts.SmsBean> smsBeans = new LinkedList<evertec.cibp.core.services.smsAlerts.SmsBean>();
						for (TVFrontendAccount account : accounts) {
							String suffix;
							if (account != null && account.getAccountNumberSuffix() != null)
								suffix = account.getAccountNumberSuffix();
							else
								suffix = "";
							if ((!suffix.equalsIgnoreCase("")) && suffix.equalsIgnoreCase("RES")
									|| suffix.equalsIgnoreCase("SAV"))
								continue;
							if (account != null && !account.getHide()
									&& (account.getAccountProductId().equalsIgnoreCase("009")// Acceso
																								// Popular
											|| account.getAccountProductId().equalsIgnoreCase("015")// Cuenta
																									// Popular
											|| account.getAccountProductId().equalsIgnoreCase("017")// Chequera
																									// Popular
											|| account.getAccountProductId().equalsIgnoreCase("020")// Avance
																									// 50
											|| account.getAccountProductId().equalsIgnoreCase("023")// Multicuenta
																									// Staff
											|| account.getAccountProductId().equalsIgnoreCase("024")// Popular
																									// Plus
											|| account.getAccountProductId().equalsIgnoreCase("025")// Multicuenta
																									// Popular
											|| account.getAccountProductId().equalsIgnoreCase("026")// IDEAL
											|| account.getAccountProductId().equalsIgnoreCase("028")// MaxiPremium
											|| account.getAccountProductId().equalsIgnoreCase("029")// Private
																									// Management
																									// Account
											|| account.getAccountProductId().equalsIgnoreCase("049")// Ahorro
																									// a
																									// Toda
																									// Hora
																									// Staff
											|| account.getAccountProductId().equalsIgnoreCase("051")// Popular
																									// Savings
											|| account.getAccountProductId().equalsIgnoreCase("052")// Ahorro
																									// a
																									// Toda
																									// Hora
											|| account.getAccountProductId().equalsIgnoreCase("060")// ATH
																									// POP
											|| account.getAccountProductId().equalsIgnoreCase("062")// ENVIA
																									// ATH
											|| account.getAccountProductId().equalsIgnoreCase("073")// Acceso
																									// ETA
											|| account.getAccountProductId().equalsIgnoreCase("075")// Popular
																									// Sec
																									// Investors
																									// Plus
											|| account.getAccountProductId().equalsIgnoreCase("078")// Payroll
																									// Card
											|| account.getAccountProductId().equalsIgnoreCase("080")// Acceso
																									// Universitario
											|| account.getAccountProductId().equalsIgnoreCase("081")// e-account
											|| account.getAccountProductId().equalsIgnoreCase("082")// Redi
																									// Equity
											|| account.getAccountProductId().equalsIgnoreCase("083")// Redi
																									// Equity
																									// Staff
											|| account.getAccountProductId().equalsIgnoreCase("087")// CIMA
									|| account.getAccountProductId().equalsIgnoreCase("089")))// Tarjeta
																								// de
																								// Nomina
							{
								try {
									CardPlasticFrontend[] plastics = userSession.getCardPlastics(account);

									if (plastics.length > 0
											&& !(account.getCurrentAthSmsAlerts() != null && account
													.getCurrentAthSmsAlerts().size() > 0)) {

										HashMap<Object, Object> smsInfo = new HashMap<>();
										for (int i = 0; i < plastics.length; i++) {
											evertec.cibp.core.services.smsAlerts.SmsBean smsBeanRequest = new evertec.cibp.core.services.smsAlerts.SmsBean();
											smsBeanRequest.setAcctNum(account.getAccountNumber());
											smsBeanRequest.setTaxId(customer.getTaxId());
											smsBeanRequest.setCardNum(plastics[i].getCardPlasticAccountRecord()
													.getCardPANId());

											SmsAlertsResponse smsAlertsResponse = customerService.getSmsAlertInfo(
													smsBeanRequest,
													Utils.generateCustomerInteractionEvent(request));
											if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
													&& smsAlertsResponse.getResponseSmsBean() != null) {
												smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
														smsAlertsResponse.getResponseSmsBean());
												smsBeans.add(smsAlertsResponse.getResponseSmsBean());
											} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
													&& smsAlertsResponse.getResponseSmsBean() == null) {
												smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
														null);
												if (!smsAlertsResponse.isAuditable())
													logger.error("Error Updating SMS Phone # while editing Cel #: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(smsBeanRequest.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ smsAlertsResponse.getAuditString());
												else
													logger.error("Error Updating SMS Phone # while editing Cel #: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(smsBeanRequest.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId());
											} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
													&& smsAlertsResponse.getErrorString().contains(
															"ERROR: SmsInfo NOT SENT.")) {
												smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
														null);
												if (!smsAlertsResponse.isAuditable())
													logger.error("Error Updating SMS Phone # while editing Cel #: Unable to send ATH SmsInfo request for \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(smsBeanRequest.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId());
												else
													logger.error("Error Updating SMS Phone # while editing Cel #: Unable to send ATH SmsInfo request for \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(smsBeanRequest.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ "\n"
															+ smsAlertsResponse.getAuditString());
											} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
													&& (smsAlertsResponse.getException() != null || !GenericValidator
															.isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
												smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
														null);
												if (!smsAlertsResponse.isAuditable()
														&& !GenericValidator.isBlankOrNull(smsAlertsResponse
																.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null)
													logger.error("Error Updating SMS Phone # while editing Cel #: calling ATH SmsInfo \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(smsBeanRequest.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ "\n"
															+ smsAlertsResponse.getSmsAlertsException()
															+ "\n"
															+ smsAlertsResponse.getException());
												else if (!smsAlertsResponse.isAuditable()
														&& !GenericValidator.isBlankOrNull(smsAlertsResponse
																.getSmsAlertsException())
														&& smsAlertsResponse.getException() == null) {
													logger.error("Error Updating SMS Phone # while editing Cel #: calling ATH SmsInfo \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(smsBeanRequest.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ "\n"
															+ smsAlertsResponse.getSmsAlertsException());
												} else if (!smsAlertsResponse.isAuditable()
														&& GenericValidator.isBlankOrNull(smsAlertsResponse
																.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null) {
													logger.error("Error Updating SMS Phone # while editing Cel #: calling ATH SmsInfo \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(smsBeanRequest.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ "\n"
															+ smsAlertsResponse.getException());
												} else if (smsAlertsResponse.isAuditable()
														&& !GenericValidator.isBlankOrNull(smsAlertsResponse
																.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null)
													logger.error("Error Updating SMS Phone # while editing Cel #: calling ATH SmsInfo \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(smsBeanRequest.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ "\n"
															+ smsAlertsResponse.getSmsAlertsException()
															+ "\n"
															+ smsAlertsResponse.getAuditString());
												else {
													logger.error("Error Updating SMS Phone # while editing Cel #: calling ATH SmsInfo \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(smsBeanRequest.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ "\n"
															+ smsAlertsResponse.getAuditString());
												}
											} else {
												smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
														null);
												logger.error("Error Updating SMS Phone # while editing Cel #: calling ATH SmsInfo \nacct: "
														+ maskutil.mask(account.getAccountNumber())
														+ "\ncard: "
														+ maskutil.mask(smsBeanRequest.getCardNum())
														+ "\n"
														+ customer.getFullName()
														+ "\nProfileID: "
														+ customer.getProfileId() + "\n");
											}
										}
										if (smsInfo.size() > 0)
											account.setCurrentAthSmsAlerts(smsInfo);
									}

									if (plastics.length > 0 && account.getCurrentAthSmsAlerts() != null
											&& account.getCurrentAthSmsAlerts().size() > 0) {
										// CardPlasticAccountRecord[]
										// plastics=account.getCardPlasticAccountRecords();
										for (int i = 0; i < plastics.length; i++) {
											if (account.getCurrentAthSmsAlerts().containsKey(
													plastics[i].getCardPlasticAccountRecord().getCardPANId())
													&& (account.getCurrentAthSmsAlerts().get(
															plastics[i].getCardPlasticAccountRecord().getCardPANId()) != null)) {
												smsBeans.add((evertec.cibp.core.services.smsAlerts.SmsBean) account
														.getCurrentAthSmsAlerts().get(
																plastics[i].getCardPlasticAccountRecord()
																		.getCardPANId()));
											}
										}
									}

									List<evertec.cibp.core.services.smsAlerts.SmsBean> forUpdating = new LinkedList<evertec.cibp.core.services.smsAlerts.SmsBean>();

									if (smsBeans != null && !smsBeans.isEmpty()) {
										for (evertec.cibp.core.services.smsAlerts.SmsBean sms : smsBeans) {

											// org.apache.commons.beanutils.BeanUtils.copyProperties(smsBeanRequest,
											// a);
											sms.setPrimaryphone(phone);
											sms.setPrimaryPhoneCarrier(provider);
											forUpdating.add(sms);
										}
									}

									if (forUpdating != null && !forUpdating.isEmpty()) {

										for (evertec.cibp.core.services.smsAlerts.SmsBean sms : forUpdating) {
											evertec.cibp.core.services.smsAlerts.SmsBean newBean = new evertec.cibp.core.services.smsAlerts.SmsBean();

											org.apache.commons.beanutils.BeanUtils.copyProperties(newBean, sms);

											SmsAlertsResponse smsAlertsResponse = customerService.updateSmsAlert(
													newBean, Utils.generateCustomerInteractionEvent(request));
											if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
													&& smsAlertsResponse.getResponseSmsBean() != null) {
												account.getCurrentAthSmsAlerts().put(sms.getCardNum(),
														smsAlertsResponse.getResponseSmsBean());
											} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
													&& smsAlertsResponse.getResponseSmsBean() == null) {
												if (!smsAlertsResponse.isAuditable())
													logger.error("Error Updating SMS Phone # while editing Cel #: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(sms.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ smsAlertsResponse.getAuditString());
												else
													logger.error("Error Updating SMS Phone # while editing Cel #: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(sms.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId());
											} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
													&& smsAlertsResponse.getErrorString().contains(
															"ERROR: SmsInfo NOT SENT.")) {
												if (!smsAlertsResponse.isAuditable())
													logger.error("Error Updating SMS Phone # while editing Cel #: Unable to send ATH smsUpdate request for \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(sms.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId());
												else
													logger.error("Error Updating SMS Phone # while editing Cel #: Unable to send ATH smsUpdate request for \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(sms.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ "\n"
															+ smsAlertsResponse.getAuditString());
											} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
													&& (smsAlertsResponse.getException() != null || !GenericValidator
															.isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
												if (!smsAlertsResponse.isAuditable()
														&& !GenericValidator.isBlankOrNull(smsAlertsResponse
																.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null)
													logger.error("Error Updating SMS Phone # while editing Cel #: calling ATH smsUpdate \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(sms.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ "\n"
															+ smsAlertsResponse.getSmsAlertsException()
															+ "\n"
															+ smsAlertsResponse.getException());
												else if (!smsAlertsResponse.isAuditable()
														&& !GenericValidator.isBlankOrNull(smsAlertsResponse
																.getSmsAlertsException())
														&& smsAlertsResponse.getException() == null) {
													logger.error("Error Updating SMS Phone # while editing Cel #: calling ATH smsUpdate \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(sms.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ "\n"
															+ smsAlertsResponse.getSmsAlertsException());
												} else if (!smsAlertsResponse.isAuditable()
														&& GenericValidator.isBlankOrNull(smsAlertsResponse
																.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null) {
													logger.error("Error Updating SMS Phone # while editing Cel #: calling ATH smsUpdate \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(sms.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ "\n"
															+ smsAlertsResponse.getException());
												} else if (smsAlertsResponse.isAuditable()
														&& !GenericValidator.isBlankOrNull(smsAlertsResponse
																.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null)
													logger.error("Error Updating SMS Phone # while editing Cel #: calling ATH smsUpdate \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(sms.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ "\n"
															+ smsAlertsResponse.getSmsAlertsException()
															+ "\n"
															+ smsAlertsResponse.getAuditString());
												else {
													logger.error("Error Updating SMS Phone # while editing Cel #: calling ATH smsUpdate \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(sms.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ "\n"
															+ smsAlertsResponse.getAuditString());
												}
											} else {

												if (!smsAlertsResponse.isAuditable())
													logger.error("Error Updating SMS Phone # while editing Cel #: calling ATH smsUpdate \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(sms.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId() + "\n");
												else {
													logger.error("Error Updating SMS Phone # while editing Cel #: calling ATH smsUpdate \nacct: "
															+ maskutil.mask(account.getAccountNumber())
															+ "\ncard: "
															+ maskutil.mask(sms.getCardNum())
															+ "\n"
															+ customer.getFullName()
															+ "\nProfileID: "
															+ customer.getProfileId()
															+ "\n"
															+ smsAlertsResponse.getAuditString());
												}
											}

										}

									}

								} catch (Exception e) {
									logger.error(
											"Error Updating SMS Phone # while editing Cel #: \nacct: "
													+ maskutil.mask(account.getAccountNumber()) + "\n"
													+ "\nProfileID: " + customer.getProfileId(), e);
								}

							}
						}

						return new ModelAndView("sms", model);

					} catch (Exception e) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession
									.setStatusmessage("Unable to edit your phone number this time, please try later.");
						else
							userSession
									.setStatusmessage("Problemas al cambiar su n&uacute;mero de tel&eacute;fono de Mi Banco Text en este momento, favor intentar luego.");
						model.put("unableSMSEnroll", true);
						return new ModelAndView("sms", model);
					}
				} else {
					try {

						SmsEnrollUserRequest smsenr = new SmsEnrollUserRequest(
								Utils.generateCustomerInteractionEvent(request), customer, phone, provider);
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD2267)) {
							smsenr.setEnrollmentSource("CIBP");
						}
						ResultStatus status = customerService.smsEnrollUser(smsenr);
						if (status == ResultStatus.SUCCESS) {
							CustomerInteractionEvent generateCustomerInteractionEvent = Utils
									.generateCustomerInteractionEvent(request);
							SMSProfile smsprofile = userSession.getSmsProfile(generateCustomerInteractionEvent);
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								userSession
										.setStatusmessage("Your inscription has been completed successfully.");
								customerService.sendSMSWelcome(customer, smsprofile, PreferredLanguage.ENGLISH);
							} else {
								userSession
										.setStatusmessage("Su inscripci&oacute;n ha sido completada exitosamente.");
								customerService.sendSMSWelcome(customer, smsprofile, PreferredLanguage.SPANISH);
							}
							model.put("smsSuccessEnroll", true);

							if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
								request.getSession().setAttribute("smsExito", true);
							}
							return new ModelAndView("sms", model);

						} else {
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								userSession
										.setStatusmessage("Unable to enroll your phone number at this time. Please try later.");
							else
								userSession
										.setStatusmessage("Problemas al inscribir tu n&uacute;mero de tel&eacute;fono en este momento. Favor intentar luego.");
							model.put("unableSMSEnroll", true);
							return new ModelAndView("sms", model);
						}
					} catch (Exception e) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession
									.setStatusmessage("Unable to enroll your phone number at this time. Please try later.");
						else
							userSession
									.setStatusmessage("Problemas al inscribir tu n&uacute;mero de tel&eacute;fono en este momento. Favor intentar luego.");

					}

				}
			} else {
				error.rejectValue("smscode", "sms.code.invalid", "The code entered is not valid");
				return new ModelAndView("sms", model);
			}
		} else {
			request.getSession().setAttribute("pageSms", 1);
		}
		
		return new ModelAndView("sms", model);
		// return showForm(request, response, error, model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);

		if (userSession.getGuidedSetupSession() != null && userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, true);
		} else {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, userSession.isInformationChallenge());
		}

		if (request.getSession().getAttribute("pageSms") != null) {
			page = Integer.parseInt(request.getSession().getAttribute("pageSms").toString());
			request.getSession().setAttribute("sms", command);
		}
		request.getSession().setAttribute("pageSms", page);

		model.put("page", Integer.toString(page));
		model.put("customer", userSession.getCustomerProfile());
		
		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setPath("/");
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		if (cookie != null)
			model.put("devicetoken", cookie.getValue());
		
		if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
			try {
				CustomerInteractionEvent generateCustomerInteractionEvent = Utils
						.generateCustomerInteractionEvent(request);
				SMSProfile smsprofile = userSession.getSmsProfile(generateCustomerInteractionEvent);
				if (!smsprofile.getCtns().isEmpty()) {
					model.put("EnrollSMS", true);
					model.put("smsPhone", ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn());
					//MBSD-218 - OOB
					boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);
					if(oobFlag){
						if (userSession.getOobEnroll()){
							model.put("EnrollOOB", true);
						} else {
							model.put("EnrollOOB", false);
						}
					}else {
						model.put("EnrollOOB", false);
					}
					//MBSD-218 - OOB
				} else
					model.put("EnrollSMS", false);

			} catch (Exception e) {// SMSProfileNotFoundException e) {
				// TODO: Manejar Mismatched Entitlement

			}

		} else
			model.put("EnrollSMS", false);
		/** **** Reference Data for SMS ENROLL ****** */
		Map<String, Object> smsproviders = new LinkedHashMap<>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			smsproviders.put("none", "Select a provider");
		else
			smsproviders.put("none", "Selecciona un proveedor");

		/*
		 * 
		 * codigos anterior smsproviders.put("VER", "CLARO"); // VER = Verizon
		 * smsproviders.put("CIN", "AT&amp;T MOBILITY"); smsproviders.put("MOV",
		 * "Open Mobile");// MOV = Movistar smsproviders.put("CEN",
		 * "Centennial Wireless");
		 * 
		 * CIBP-1857 BLAS ALONSO GARCIA RUIZ SE AGREGA NUEVOS PROVEEDORES DE SMS
		 */
		smsproviders.put("PR1", "OPEN");
		smsproviders.put("PR7", "CLARO");
		smsproviders.put("PR6", "CENTENNIAL");
		smsproviders.put("US3", "AT&T");
		smsproviders.put("US4", "T-MOBILE");
		smsproviders.put("US5", "SPRINT");

		model.put("smsproviders", smsproviders);

		/*
		 * CIBP-1857 BLAS ALONSO GARCIA RUIZ SE AGREGA VARIABLE EN MODELO PARA
		 * CONTROLAR EL CHEQUEO DE TERMINOS Y CONDICIONES
		 */
		SmsBean infobean = (SmsBean) command;
		if (infobean.getAction() == null)
			model.put("agreeval", "");
		else
			model.put("agreeval", "checked");

		String msgerr = (String) WebUtils.getSessionAttribute(request, "msgerr");
		if (msgerr != null) {
			errors.rejectValue("provider", "", msgerr);
			return model;
		}

		if (userSession.getInformationQuestion() != null) {
			String question = (String) userSession.getInformationQuestion().values().toArray()[0];
			model.put("question", question);
		}

		Boolean showMobile = (Boolean) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.MOBILE_FRAME);
		String phone = (String) WebUtils.getSessionAttribute(request, "smsPhonereceived");
		if (showMobile != null && showMobile && phone != null) {
			page = Integer.valueOf(1);
			model.put("page", Integer.toString(page));
			request.getSession().setAttribute("pageSms", page);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.MOBILE_FRAME, null);
			return model;
		}
		
		return model;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		SmsBean infobean = (SmsBean) command;
		SmsBean.ACTION action = infobean.getAction();

		String delete = request.getParameter("d");

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();

		if (delete != null) {
			Boolean informationChallenge = userSession.isInformationChallenge();
			if (!informationChallenge) {
				if (userSession.getInformationQuestion() == null) {
					this.getRSAChallenge(request, userSession);
				}
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
						ChallengeInformationUtils.MOBILE_DELETE);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.MOBILE_DELETE, true);
				errors.rejectValue("", "", "");
			}
		}

		else {

			if (request.getSession().getAttribute("pageSms") != null) {
				page = Integer.parseInt(request.getSession().getAttribute("pageSms").toString());
				request.getSession().setAttribute("sms", command);
			}

			if (page == 0) {
				if (action == SmsBean.ACTION.SMS_ENROLL) {
					boolean er = false;
					if (infobean.getProvider() == null || infobean.getProvider().equals("none")) {
						errors.rejectValue("provider", "sms.provider.invalid", "Please select a provider");
						er = true;
					}
					if (infobean.getPhone1() == null || infobean.getPhone1().equals("")
							|| !infobean.getPhone1().matches("\\d{3}")) {
						errors.rejectValue("phone3", "sms.phone.invalid", "Please enter a correct phone number");
						er = true;
					} else if (infobean.getPhone2() == null || infobean.getPhone2().equals("")
							|| !infobean.getPhone2().matches("\\d{3}")) {
						errors.rejectValue("phone3", "sms.phone.invalid", "Please enter a correct phone number");
						er = true;
					} else if (infobean.getPhone3() == null || infobean.getPhone3().equals("")
							|| !infobean.getPhone3().matches("\\d{4}")) {
						errors.rejectValue("phone3", "sms.phone.invalid", "Please enter a correct phone number");
						er = true;
					}
					String phone = infobean.getPhone1() + infobean.getPhone2() + infobean.getPhone3();
					if (errors.getAllErrors().isEmpty()) {
						Boolean informationChallenge = userSession.isInformationChallenge();
						if (!informationChallenge
								&& (userSession.getGuidedSetupSession() == null || !userSession.getGuidedSetupSession()
										.isGuidedSetupMode())) {
							if (userSession.getInformationQuestion() == null) {
								this.getRSAChallenge(request, userSession);
							}
							er = true;
							errors.rejectValue("", "", "");
							WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
							Map<String, Object> parameters = new HashMap<>();
							parameters.put(ChallengeInformationUtils.PROVIDER, infobean.getProvider());
							parameters.put(ChallengeInformationUtils.PHONE, phone);
							WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
									ChallengeInformationUtils.MOBILE_FRAME);
							WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS,
									parameters);
						}
					}

					request.getSession().removeAttribute("pageSms");
					if (!er) {
						Calendar cal = Calendar.getInstance();
						long mil = cal.getTimeInMillis();
						String code = String.valueOf(mil);
						code = code.substring(code.length() - 4, code.length());
						WebUtils.setSessionAttribute(request, "smsGeneratedCode", code);
						WebUtils.setSessionAttribute(request, "smsPhonereceived", phone);
						WebUtils.setSessionAttribute(request, "smsProvider", infobean.getProvider());
						String msgerr = "";
						try {
							SendSmsGeneratedCodeRequest codereq = new SendSmsGeneratedCodeRequest(
									Utils.generateCustomerInteractionEvent(request), customer, code, phone,
									infobean.getProvider());
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								customerService.sendGeneratedCode(codereq, PreferredLanguage.ENGLISH);
							else
								customerService.sendGeneratedCode(codereq, PreferredLanguage.SPANISH);
							return;
						} catch (Exception e) {
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								msgerr = "Unable to enroll your phone number at this time. Please try later.";
							else
								msgerr = "Problemas al inscribir tu n&uacute;mero de tel&eacute;fono en este momento. Favor intentar luego.";
							errors.rejectValue("provider", "", msgerr);
							return;
						}
					}
				}

			}// end if page == 0

			else if (page == 1) {
				if (action == SmsBean.ACTION.SMS_CODE_VER) {
					String generatedCode = (String) WebUtils.getSessionAttribute(request, "smsGeneratedCode");

					if (infobean.getSmscode() == null || infobean.getSmscode().equals("")
							|| !infobean.getSmscode().matches("\\d{4}")
							|| !infobean.getSmscode().equalsIgnoreCase(generatedCode))
						errors.rejectValue("smscode", "sms.code.invalid", "The code entered is not valid");
				}
			}
		}
	}

	private void getRSAChallenge(HttpServletRequest request, UserSession userSession) throws Exception {
		// ////////////////////

		String language = Utils.getRSALanguage(request);
		String username = userSession.getUsername();

		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), request);
		/**
		 * MBNA-1955
		 * Method rsaAnalyze : CIBP-WEB microservice integration
		 *
		 */
		//Microservice implementation
		RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
		AnalyzeResponse anResp = userSession.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

		deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
		deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

		userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
		userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

		userSession.setDeviceRequest(deviceRequest);

		/*
		 * if (deviceRequest != null && cookie != null){
		 * cookie.setValue(deviceRequest.getDeviceTokenCookie());
		 * cookie.setPath("/"); response.addCookie(cookie); }
		 */
		if (ActionCode.CHALLENGE != anResp.getRiskResult().getTriggeredRule().getActionCode()) {
			userSession.setInformationChallenge(true);
		} else {
			String email = null;
			if (userSession.getCustomerProfile() != null)
				email = userSession.getCustomerProfile().getEmail();

			QuestionResponse personalQuestion;

			if (userSession.getInformationQuestion() == null) {
				RSAActionRequest actionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(request,
						userSession.getUsername()), userSession.getUsername(), "", email, 0, userSession.isRemember(),
						userSession.getDeviceRequest());

				if (userSession.getCustomerProfile() != null)
					actionRequest.setUsername(userSession.getCustomerProfile().getUsername());
				else if (userSession.getUsernameReservation() != null)
					actionRequest.setUsername(userSession.getUsernameReservation().getUsername());
				else
					actionRequest.setUsername(username);

				actionRequest.setDeviceRequest(userSession.getDeviceRequest());
				actionRequest.setRsaSessionId(userSession.getRsaSessionId());
				actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());

				actionRequest.setRsaLang(Utils.getRSALanguage(request));

				personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

			} else {
				personalQuestion = new QuestionResponse();
				personalQuestion.setStatusResult(ResultStatus.SUCCESS);
				personalQuestion.setQuestion(userSession.getInformationQuestion());
			}
			if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
				userSession.setInformationQuestion(personalQuestion.getQuestion());

			} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
				throw new ModelAndViewDefiningException(new ModelAndView("error"));
			} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
				throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
			}
		}

		// //////////////////////
	}

}
