package evertec.cibp.web.form;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyAllCustomerEmailsRequest;
import evertec.cibp.core.services.requests.ModifyEZSTMTEntitlementRequest;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.form.bean.changeMlaEstatmentBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class ChangeMlaEstatmentForm extends SimpleFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;
	
	// MBSFE-691
	private String invalidEmailDomains;
	// END MBSFE-691
	
	public ChangeMlaEstatmentForm() {
		super();
		setCommandClass(changeMlaEstatmentBean.class);
		setCommandName("changeMlaEstatment");
		setFormView("changeMlaEstatment");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		changeMlaEstatmentBean changeMlaEstatmentBean = (changeMlaEstatmentBean) command;
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		BankingSession bankingSession = userSession.getBankingSession();
		String statusAccount = changeMlaEstatmentBean.getStatusAccount();
		CustomerServices customerservice = userSession.getCustomerService();
		CustomerInteractionEvent generateCustomerInteractionEvent = Utils.generateCustomerInteractionEvent(request,userSession.getCustomerProfile().getUsername());
		AccountMaskUtil mask = new AccountMaskUtil();
		StringBuilder message = new StringBuilder();
		String to_replace = ((statusAccount.equalsIgnoreCase("Active")) ? "N" : "Y");
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		Date date = new Date();
		
		if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.EZSTMT) != null) {
			CustomerEntitlement ezstmtEntitlement = userSession.getCustomerProfile()
					.getEntitlementByType(EntitlementType.EZSTMT);
			CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request);
			String id = changeMlaEstatmentBean.getId();
			String account = changeMlaEstatmentBean.getAccountNumber();
			
			TVFrontendAccount tvp = Utils.getAccountById(id, userSession.getAccounts());
			if (tvp != null) {
				// Update the customer email if necessary
				if (changeMlaEstatmentBean.getEmail() != null && !changeMlaEstatmentBean.getEmail().equals("")
						&& GenericValidator.isEmail(changeMlaEstatmentBean.getEmail())
						&& !changeMlaEstatmentBean.getEmail().equals(customerProfile.getEmail())
						&& changeMlaEstatmentBean.getEmail().equals(changeMlaEstatmentBean.getEmailConfirm())) {
					
					ModifyAllCustomerEmailsRequest macer = new ModifyAllCustomerEmailsRequest(
							Utils.generateCustomerInteractionEvent(request), customerProfile, 
							customerservice, userSession.getAccounts(), bankingSession, 
							userSession.getEbpp2UserId(), userSession.getPreferredLanguage(), 
							changeMlaEstatmentBean.getEmail()); 
					customerProfile = CustomerUtils.modifyAllEmails(macer);
					userSession.setCustomerProfile(customerProfile);
				}
				model.put("act", tvp);
				model.put("description", tvp.getNickname());
				model.put("nickname", tvp.getNickname());
				
				try {
					String accountsStatus="";
					try{
						accountsStatus = (!ezstmtEntitlement.getString("MLA_ESTMT").equals(null) ? ezstmtEntitlement.getString("MLA_ESTMT") : "");
					}catch(Exception e){
						accountsStatus=""; 
					}
					
					if(accountsStatus.length() > 0 && accountsStatus.contains(account)){
						accountsStatus = replaceAccountStatus(accountsStatus, account, to_replace);
					}else{
						if(accountsStatus.length() > 0){
							accountsStatus = accountsStatus.concat(",");
						}
						accountsStatus = accountsStatus.concat(account).concat("-").concat(dateFormat.format(date)).concat("-").concat(to_replace);
					}
					
					ModifyEZSTMTEntitlementRequest modifyRequest = new ModifyEZSTMTEntitlementRequest(interactionEvent,customerProfile, ezstmtEntitlement, accountsStatus);
					customerservice.modifyCustomerEZSTMTEntitlement(modifyRequest);
					if(statusAccount.equalsIgnoreCase("Inactive") || statusAccount.equalsIgnoreCase("Not registered")){
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession.setStatusmessage("The option of receiving e-Statements has been activated for " + tvp.getNickname());
						else
							userSession.setStatusmessage("Has activado la opci&oacute;n de recibir e-Statement de " + tvp.getNickname());
					}else{
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession.setStatusmessage("Your e-Statement " + tvp.getNickname() + " was deactivated.");
						else
							userSession.setStatusmessage("Has desactivado la opci&oacute;n de recibir e-Statement de "
									+ tvp.getNickname()); 
					}
					
					message.append("Account:").append(tvp.getNickname()).append(" ").append(mask.mask(tvp.getAccountNumber())).append(" ").append("E-STATEMENT STATUS:").append(to_replace);
				} catch (Exception e) {
					if(statusAccount.equalsIgnoreCase("Active")){
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession.setStatusmessage("Unable to deactivate your e-Statement "
									+ tvp.getNickname() + " at this time.");
						else
							userSession.setStatusmessage("Error desactivando tu e-Statement " + tvp.getNickname()
								+ " en este momento.");
					}else{
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession.setStatusmessage("Unable to activate your e-Statement "
									+ tvp.getNickname() + " at this time.");
						else
							userSession.setStatusmessage("Error activando tu e-Statement " + tvp.getNickname()
								+ " en este momento.");
					}
					
					message = new StringBuilder();
					String errorStatus = ((to_replace.equalsIgnoreCase("Y")) ? "Activate" : "Inactivate");;
					message.append("Account:").append(tvp.getNickname()).append(" ").append(mask.mask(tvp.getAccountNumber())).append(" Error:").append(e.getMessage()).append(", when try to ").append(errorStatus);
					generateCustomerInteractionEvent.setCustomStringData(CustomerInteractionEventType.UPDATE_MLA_ESTMT_STATUS.toString(), message.toString());
					userSession.getCustomerService().logMLAStatusAddDelete(generateCustomerInteractionEvent, InteractionSeverity.INFO);
					
					return showForm(request, errors, this.getFormView(), model);
				}			
			}
		}
		
		generateCustomerInteractionEvent.setCustomStringData(CustomerInteractionEventType.UPDATE_MLA_ESTMT_STATUS.toString(), message.toString());
		userSession.getCustomerService().logMLAStatusAddDelete(generateCustomerInteractionEvent, InteractionSeverity.INFO);
		
		model.put("reload", "true");
		model.put("completeprocess", true);
		return showForm(request, errors, "changeMlaEstatment", model);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		
		changeMlaEstatmentBean changeMlaEstatmentBean = (changeMlaEstatmentBean) command;
		
		boolean emailError = false;

		if (changeMlaEstatmentBean.getEmail() != null && !changeMlaEstatmentBean.getEmail().equals("")
				&& !GenericValidator.isEmail(changeMlaEstatmentBean.getEmail())) {
			errors.rejectValue("email", "", "Please verify your email");
			emailError = true;
		} else if (changeMlaEstatmentBean.getEmail() != null && !changeMlaEstatmentBean.getEmail().equals("")
				&& GenericValidator.isEmail(changeMlaEstatmentBean.getEmail())
				&& !changeMlaEstatmentBean.getEmail().equalsIgnoreCase(changeMlaEstatmentBean.getEmailConfirm())) {
			errors.rejectValue("emailConfirm", "", "Your email must be equal");
			emailError = true;
		}
		// MBSFE-691
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE691) && changeMlaEstatmentBean.getEmail() != null && !emailError){ 
			String[] domain = changeMlaEstatmentBean.getEmail().split("@");
			if(domain.length > 1 && Utils.haveType(this.invalidEmailDomains, domain[1])) {
				errors.rejectValue("emailConfirm", "email.invaliddomain", "Your e-mail address is incorrect. Please try again.");
			}
		}
	}
	
	
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		changeMlaEstatmentBean changeMlaEstatmentBean = (changeMlaEstatmentBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		AccountMaskUtil mask = new AccountMaskUtil();
		String id = (String) request.getParameter("id");
		TVFrontendAccount tvp = Utils.getAccountById(id, userSession.getAccounts());
		String statusAccount = (String) request.getParameter("statusAccount");
		if (tvp == null)
			return model;
		changeMlaEstatmentBean.setId(id);
		changeMlaEstatmentBean.setStatusAccount(statusAccount);
		changeMlaEstatmentBean.setAccountNumber(tvp.getAccountNumber());
		model.put("id", id);
		model.put("statusAccount", statusAccount);
		model.put("accountNumber", tvp.getAccountNumber());
		model.put("nickname", tvp.getNickname() + " " + mask.mask(tvp.getAccountNumber()));
		if(statusAccount.equalsIgnoreCase("Active")){
			model.put("deactivateEstatement", true);
		}else{
			model.put("activateEstatement", true);
		}
		model.put("act", tvp);
		model.put("customerEmail", customerProfile.getEmail());
		
		return model;
	}
	
	public String replaceAccountStatus(String accountsStatus, String account, String to_replace){
		
	    if(accountsStatus == null || accountsStatus.length() <= 0){
	        return accountsStatus;
	    }
	    
		String[] mlaList = accountsStatus.split(",");
		for (String acc: mlaList){
			if (acc.contains(account)){
				String[] mlaInfo = acc.split("-");
				if (mlaInfo.length == 3) {
					DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
					Date date = new Date();
					return accountsStatus.replace(acc, account+"-"+dateFormat.format(date)+"-"+to_replace);
				}
				break;
			}
		}
		
		return accountsStatus;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	// MBSFE-691
	public String getInvalidEmailDomains() {
		return invalidEmailDomains;
	}

	public void setInvalidEmailDomains(String invalidEmailDomains) {
		this.invalidEmailDomains = invalidEmailDomains;
	}
	// END MBSFE-691
}
