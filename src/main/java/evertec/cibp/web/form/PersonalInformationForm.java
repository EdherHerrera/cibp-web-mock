/**
 * 
 */
package evertec.cibp.web.form;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerPassword;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.applications.AddressApplication;
import evertec.cibp.core.models.customer.applications.AddressApplication.AppliedToAccount;
import evertec.cibp.core.models.customer.applications.ApplicationStatus;
import evertec.cibp.core.models.customer.applications.PhoneApplication;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyCustomerEmailRequest;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.requests.adaptiveauth.EnrollmentInformationinRSARequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.RsaQuestionsUpdateResponse;
import evertec.cibp.core.services.responses.SmsAlertsResponse;
import evertec.cibp.core.services.responses.SmsAlertsResponse.Result;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.ValidationQuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.EditEmailBean;
import evertec.cibp.web.form.bean.PersonalInformationBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.CookieUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * @author jocampo
 * 
 */
public class PersonalInformationForm extends CancellableFormController implements MessageSourceAware {

	private MessageSource messageSource;

	// jocampo FFEIC
	private Integer maxPasswordRecords;

	// FIN FFEIC
	
	private String alertsAccountTypes;

	public PersonalInformationForm() {
		super();
		setCommandClass(PersonalInformationBean.class);
		setCommandName("personal");
		setFormView("personalinformation");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Boolean informationChallenge = session.isInformationChallenge();
		
		//OOB
				/**
				 * Codigo para ayudar a la verificación y de los estados de OOB
				 * */
		boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);
		
		if(oobFlag && session.isOobAuthenticated() && session.getOobEnroll() 
				&& session.getTasksListOOB() != null && session.getTasksListOOB().contains(ASK_FOR_OOB.PERSONAL_INFORMATION)) {
			
			if (GenericValidator.isBlankOrNull(request.getParameter("oob"))) {
				model.put("text", "empty-");
			} else {
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
				
				session.getTasksListOOB().remove(ASK_FOR_OOB.PERSONAL_INFORMATION);
				
				excecuteAction(request, response, model);
				session.setInformationChallenge(true);
				
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
				model.put("text", "success-");
			}
			return new ModelAndView("text", model);
		}
		
		if (!informationChallenge) {

			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);

			CustomerProfile profile = session.getCustomerProfile();

			boolean dummyQuestions = (profile == null);

			String answer = request.getParameter("answer");

			if (GenericValidator.isBlankOrNull(answer)) {
				model.put("text", "empty-");
			}

			else {

				// PersonalInformationBean personal = (PersonalInformationBean)
				// command;

				// String answer = personal.getAnswer();

				RSAActionRequest rsaActionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(
						request, session.getUsername()), session.getUsername(), answer, dummyQuestions ? null
						: profile.getEmail(), 0, session.isRemember(), session.getDeviceRequest());
				String username = session.getUsername();
				if (session.getCustomerProfile() != null)
					rsaActionRequest.setUsername(session.getCustomerProfile().getUsername());
				else if (session.getUsernameReservation() != null)
					rsaActionRequest.setUsername(session.getUsernameReservation().getUsername());
				else
					rsaActionRequest.setUsername(username);

				rsaActionRequest.setRsaSessionId(session.getRsaSessionId());
				rsaActionRequest.setRsaTransactionId(session.getRsaTransactionId());
				rsaActionRequest.setQuestion(session.getInformationQuestion());
				rsaActionRequest.setRsaLang(null);
				ValidationQuestionResponse responseAnswer = session.getCustomerService().getSignOnCustomerServiceModule().validateRSAQuestion(
						rsaActionRequest);
				session.setQuestion(null);
				session.setRsaSessionId(responseAnswer.getRsaSessionId());
				session.setRsaTransactionId(responseAnswer.getRsaTransactionId());
				
				Cookie cookie = WebUtils.getCookie(request, "PMData");
				if (cookie == null)
					cookie = new Cookie("PMData", "");

				cookie.setPath("/");
				cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
				ESAPI.httpUtilities().addCookie(response, cookie);

				if (ResultStatus.SUCCESS.equals(responseAnswer.getStatusResult())) {
					cookie.setPath("/");
					cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
					ESAPI.httpUtilities().addCookie(response, cookie);
					excecuteAction(request, response, model);
					session.setInformationChallenge(true);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
					model.put("text", "success-");
				} else if (ResultStatus.BLOCKED.equals(responseAnswer.getStatusResult())) {
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
					/* CIBP-1104 */
					boolean hasalert = false;
					AlertsEntitlement ent = new AlertsEntitlement(session.getCustomerProfile().getEntitlementByType(
							EntitlementType.ALERTS));
					if (ent != null && ent.getAlerts() != null) {
						Set<AlertType> alerts = ent.getAlerts();
						if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
								|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
							hasalert = true;
						}
					}
					/*
					 * CIBP-1104 se activa envio alerta de bloqueo de preguntas
					 * secretas
					 */
					if (hasalert) {
						session.getCustomerService().sendQuestionsBlockedConfirmation(session.getCustomerProfile(),
								session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
								session.getPreferredLanguage());
					}
					/* FIN--- CIBP-1104 */

					// errors.rejectValue("answer", "account.block.message",
					// "Account Blocked");
					CookieUtils.addCookie(response, 60 * 60 * 24 * 365, "username", username);

					model.put("text", "blocked-");
					// return new ModelAndView("forward:login", model);
					// return showForm(request, response, errors);
				} else if (ResultStatus.EXCEPTION.equals(responseAnswer.getStatusResult())) {
					cookie.setPath("/");
					cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
					ESAPI.httpUtilities().addCookie(response, cookie);
					model.put("text", "exception-");
					// return new ModelAndView("forward:login", model);
				} else if (ResultStatus.ERROR.equals(responseAnswer.getStatusResult())) {
					errors.rejectValue("answer", "login.question.error.message", "Invalid answer");
					cookie.setPath("/");
					cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
					ESAPI.httpUtilities().addCookie(response, cookie);

					String email = null;
					if (session.getCustomerProfile() != null)
						email = session.getCustomerProfile().getEmail();

					QuestionResponse personalQuestion;

					RSAActionRequest actionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(
							request, session.getUsername()), session.getUsername(), "", email, 0, session.isRemember(),
							session.getDeviceRequest());

					if (session.getCustomerProfile() != null)
						actionRequest.setUsername(session.getCustomerProfile().getUsername());
					else if (session.getUsernameReservation() != null)
						actionRequest.setUsername(session.getUsernameReservation().getUsername());
					else
						actionRequest.setUsername(username);

					// session.setRsaSessionId(anResp.getIdentificationData().getSessionId());
					// session.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

					actionRequest.setDeviceRequest(session.getDeviceRequest());
					actionRequest.setRsaSessionId(session.getRsaSessionId());
					actionRequest.setRsaTransactionId(session.getRsaTransactionId());

					actionRequest.setRsaLang(Utils.getRSALanguage(request));

					personalQuestion = session.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

					if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
						String question = (String) personalQuestion.getQuestion().values().toArray()[0];
						session.setInformationQuestion(personalQuestion.getQuestion());
						model.put("text", "error-" + question);
						cookie.setPath("/");
						cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
						ESAPI.httpUtilities().addCookie(response, cookie);
					} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
						// throw new ModelAndViewDefiningException(new
						// ModelAndView("error"));
						model.put("text", "exception-");
					} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
						/* CIBP-1104 */
						boolean hasalert = false;
						AlertsEntitlement ent = new AlertsEntitlement(session.getCustomerProfile()
								.getEntitlementByType(EntitlementType.ALERTS));
						if (ent != null && ent.getAlerts() != null) {
							Set<AlertType> alerts = ent.getAlerts();
							if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
									|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
								hasalert = true;
							}
						}
						/*
						 * CIBP-1104 se activa envio alerta de bloqueo de
						 * preguntas secretas
						 */
						if (hasalert) {
							session.getCustomerService().sendQuestionsBlockedConfirmation(session.getCustomerProfile(),
									session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
									session.getPreferredLanguage());
						}
						/* FIN--- CIBP-1104 */

						// errors.rejectValue("answer", "account.block.message",
						// "Account Blocked");
						CookieUtils.addCookie(response, 60 * 60 * 24 * 365, "username", username);

						model.put("text", "blocked-");
					}
				}
			}
			return new ModelAndView("text", model);
		}
		return new ModelAndView("redirect:personalinformation");
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		final String gdprFrameStr = "gdprFrame"; //gdrp frame string
				
		Boolean showMobile = (Boolean) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.MOBILE_FRAME);
		
		if (showMobile != null && showMobile) {
			model.put("mobileFrame", true);
		} else {
			model.put("mobileFrame", false);
		}
		
		//OOB
		/**
		 * Codigo para ayudar a la verificación y de los estados de OOB
		 * */
		boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);
		/**
		 * Verificación del flag
		 * */
		if(oobFlag){
			model.put("showOOB", true);
		}else{
			model.put("showOOB", false);
		}
		
		boolean outreachFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OUTREACH);
		
		if(outreachFlag){
			model.put("outreachFlag", true);
			model.put("outreachPath", "outreach?info");
		}else{
			model.put("outreachFlag", false);
		}
		
		model.put("oobChallengeFinish", false);		
		if(oobFlag){

			if(session.getOobEnroll() && session.isOobAuthenticated() 
				&& session.getTasksListOOB() != null && session.getTasksListOOB().contains(ASK_FOR_OOB.PERSONAL_INFORMATION)){
				model.put("oobChallengeFinish", true);
			} else if(session.getOobEnroll() && session.getTasksListOOB() != null && session.getTasksListOOB().contains(ASK_FOR_OOB.ALERT_TYPES))
				session.getTasksListOOB().remove(ASK_FOR_OOB.ALERT_TYPES);
		}
		
		if(session.getOobEnroll()){
			model.put("oobController", "changeOOB");
			//*MBSD-218 OOB LALICEA
			Boolean deleteMobileOOB = (Boolean) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.MOBILE_DELETE_OOB);
			if (deleteMobileOOB != null && deleteMobileOOB){
				model.put("deactivateOOB",true);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.MOBILE_DELETE_OOB, null);
			}
			//*MBSD-218 OOB LALICEA	END		
		}else{
			model.put("oobController", "enrollOOB");
		}
		Boolean showOOb = (Boolean) WebUtils.getSessionAttribute(request, "SHOW_OOB");
		if (showOOb != null && showOOb) {
			model.put("oobFrame", true);
		} else {
			model.put("oobFrame", false);
		}
		
		//MBFIS-11
		Boolean secOOb = (Boolean) WebUtils.getSessionAttribute(request, Utils.SHOW_OOB);
		if(secOOb != null && secOOb) {
			model.put("oobFrame", true);
			WebUtils.setSessionAttribute(request, Utils.SHOW_OOB, null);
		}
		//END MBFIS-11

		//MBFM-376
		Boolean showGDPR = null; //show gdpr value
		Object showGDPRObj = WebUtils.getSessionAttribute(request, Utils.SHOW_GDPR); //show gdpr boolean

		if(showGDPRObj instanceof Boolean) {
			showGDPR = (Boolean) showGDPRObj;
		}

		if(showGDPR != null && showGDPR) {
			model.put(gdprFrameStr, Boolean.TRUE);
			WebUtils.setSessionAttribute(request, Utils.SHOW_GDPR, null);
		}
		//END MBFM-376

		model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, session.isInformationChallenge());
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ALERTS_FRAME, null);

		model = this.loadPersonalInformation(request, model, session);
		return model;

	}

	@SuppressWarnings("unchecked")
	private void excecuteAction(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		String action = (String) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION);
		Map<String, Object> parameters = (Map<String, Object>) WebUtils.getSessionAttribute(request,
				ChallengeInformationUtils.INFORMATION_PARAMETERS);
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile customer = userSession.getCustomerProfile();
		
		boolean outreachFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OUTREACH);
		
		if (action != null) {
			if (action.equals(ChallengeInformationUtils.PASSWORD_ACTION)) {
				boolean hasalert = false;
				AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
				if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
					Set<AlertType> alerts = ent.getAlerts();
					if (alerts.contains(AlertType.CHANGE_PASS_EMAIL) || alerts.contains(AlertType.CHANGE_PASS_SMS)
							|| alerts.contains(AlertType.CHANGE_PASS_PUSH)) {
						hasalert = true;
					}
				}
				/* FIN--- CIBP-1104 */

				ModifyCustomerRequest modifyCustomerRequest = new ModifyCustomerRequest(
						Utils.generateCustomerInteractionEvent(request), customer.getProfileId(),
						customer.getPasswordHash(), customer.getPasswordSalt(), "", 0, "", customer.getStatus(), null,
						userSession.getDeviceRequest());
				Integer generatedRandomSalt = PasswordHashUtil.genereateRandomSalt();
				String newpass = (String) parameters.get(ChallengeInformationUtils.PASSWORD_ACTION);
				modifyCustomerRequest.setNewPasswordHash(PasswordHashUtil.generateSaltedHash(newpass,
						generatedRandomSalt));
				modifyCustomerRequest.setNewPasswordSalt(generatedRandomSalt);
				modifyCustomerRequest.setProfileId(customer.getProfileId());
				modifyCustomerRequest.setDeviceRequest(userSession.getDeviceRequest());
				ModifyCustomerProfileResponse r = userSession.getCustomerService().modifyCustomerPassword(
						modifyCustomerRequest);
				if (ResultStatus.SUCCESS.equals(r.getStatus())) {
					// jocampo FFEIC
					CustomerPassword cp = new CustomerPassword(customer.getProfileId(), customer.getPasswordHash(),
							new Date(), customer.getPasswordSalt());
					userSession.getPasswords().add(0, cp);
					List<CustomerPassword> passwordsDelete = null;
					if (userSession.getPasswords().size() > this.maxPasswordRecords) {
						passwordsDelete = userSession.getPasswords().subList(this.maxPasswordRecords,
								userSession.getPasswords().size());
						userSession.setPasswords(userSession.getPasswords().subList(0, this.maxPasswordRecords));
					}
					customerService.getCustomerProfileService().updateCustomerPasswords(userSession.getPasswords(),
							passwordsDelete);
					// FIN FFEIC

					userSession.setCustomerProfile(r.getCustomerProfile());
					model.put("confirm", true);
					customer.setPasswordSalt(generatedRandomSalt);
					customer.setPasswordHash(PasswordHashUtil.generateSaltedHash((String) parameters.get("password"),
							generatedRandomSalt));

					Cookie cookie = WebUtils.getCookie(request, "PMData");
					if (cookie == null)
						cookie = new Cookie("PMData", "");

					cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
					cookie.setPath("/");

					ESAPI.httpUtilities().addCookie(response, cookie);
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Password was changed");
					else
						userSession.setStatusmessage("Contrase&ntilde;a cambiada.");
					/* CIBP-1104 se activa envio alerta de cambio de contrasena */
					if (hasalert) {
						customerService.sendChangePasswordConfirmation(customer,
								userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
								userSession.getPreferredLanguage());
					}
				} else {
					model.put("confirm", true);
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Unable to change password");
					else
						userSession.setStatusmessage("Contrase&ntilde;a no pudo ser cambiada");
				}
			} else if (action.equals(ChallengeInformationUtils.QUESTIONS_ACTION)) {

				String q1 = (String) parameters.get(ChallengeInformationUtils.QUESTION_1);
				String q2 = (String) parameters.get(ChallengeInformationUtils.QUESTION_2);
				String q3 = (String) parameters.get(ChallengeInformationUtils.QUESTION_3);
				String a1 = (String) parameters.get(ChallengeInformationUtils.ANSWER_1);
				String a2 = (String) parameters.get(ChallengeInformationUtils.ANSWER_2);
				String a3 = (String) parameters.get(ChallengeInformationUtils.ANSWER_3);

				boolean hasalert = false;
				AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
				if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
					Set<AlertType> alerts = ent.getAlerts();
					if (alerts.contains(AlertType.QUESTIONS_EDIT_EMAIL) || alerts.contains(AlertType.QUESTIONS_EDIT_SMS)
							|| alerts.contains(AlertType.QUESTIONS_EDIT_PUSH)) {
						hasalert = true;
					}
				}
				/* FIN--- CIBP-1104 */
				EnrollmentInformationinRSARequest eirsa = new EnrollmentInformationinRSARequest(
						Utils.generateCustomerInteractionEvent(request), userSession.getUsername(),
						userSession.getCustomerProfile().getEmail(), false,userSession.getDeviceRequest());
				eirsa.setQuestionsAndAnswer( q1,q2, q3, a1, a2, a3);

				RsaQuestionsUpdateResponse enrollRSAresponse = customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(eirsa);

				Cookie cookie = WebUtils.getCookie(request, "PMData");
				if (cookie == null)
					cookie = new Cookie("PMData", "");

				cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
				cookie.setPath("/");

				ESAPI.httpUtilities().addCookie(response, cookie);

				if (ResultStatus.SUCCESS.equals(enrollRSAresponse.getStatus())) {
					model.put("confirm", true);
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Security Questions have been changed.");
					else
						userSession.setStatusmessage("Preguntas de seguridad han sido cambiadas.");
					/*
					 * CIBP-1104 se activa envio alerta de cambio de preguntas
					 * secretas
					 */
					if (hasalert) {
						customerService.sendQuestionsEditConfirmation(customer,
								userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
								userSession.getPreferredLanguage());
					}

				} else {
					model.put("confirm", true);
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Unable to change security questions.");
					else
						userSession.setStatusmessage("Preguntas no pudieron ser cambiadas.");
				}
			} else if (action.equals(ChallengeInformationUtils.EMAIL_ACTION)) {
				this.updateEmail(request, model, parameters);
			} else if (action.equals(ChallengeInformationUtils.PHONES_ACTION)) {

				String resphone = (String) parameters.get(ChallengeInformationUtils.RESIDENTIAL_PHONE);
				String work = (String) parameters.get(ChallengeInformationUtils.WORK_PHONE);
				String cel = (String) parameters.get(ChallengeInformationUtils.MOBILE_PHONE);

				/* CIBP-1104 */
				boolean hasalert = false;
				AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
				if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
					Set<AlertType> alerts = ent.getAlerts();
					if (alerts.contains(AlertType.PHONE_EDIT_EMAIL) || alerts.contains(AlertType.PHONE_EDIT_SMS)
							|| alerts.contains(AlertType.PHONE_EDIT_PUSH)) {
						hasalert = true;
					}
				}
				/* FIN--- CIBP-1104 */

				boolean saveApplication = false;
				PhoneApplication phoneapp = new PhoneApplication(resphone, work, cel, ApplicationStatus.NEW);
				try {
					/**
					 * MBNA-1895
					 * Method savePhoneApplication : CIBP-WEB microservice integration
					 * 
					 */
					CustomerFacade.getInstance().savePhoneApplication(phoneapp, request);
					saveApplication = true;
			    	final CustomerInteractionEvent interaction = Utils.generateCustomerInteractionEvent(request, userSession.getUsername()); // the event
			    	
			    	customerService.getInteractionService().logCustomerEvent(interaction, 
			        		 CustomerInteractionEventType.MYINFO_PHONE_CHANGE, InteractionSeverity.INFO);
				} catch (RuntimeException e) {
					saveApplication = false;
				}

				model.put("completed", true);
				if (!saveApplication) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Unable to send your change request, Please try later.");
					else
						userSession
								.setStatusmessage("Su solicitud de cambio no se pudo procesar en este momento, favor intentar luego.");
				} else {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession
								.setStatusmessage("Your change request will be processed in the next two business days.");
					else
						userSession
								.setStatusmessage("Tu solicitud de cambio ser&aacute; procesada en los pr&oacute;ximos dos d&iacute;as laborables");

					/*
					 * CIBP-1104 se activa envio alerta de modificacion de
					 * telefono
					 */
					if (hasalert) {
						customerService.sendPhoneEditConfirmation(customer,
								userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
								userSession.getPreferredLanguage());
					}
				}
			} else if (action.equals(ChallengeInformationUtils.ADDRESSES_ACTION)) {
				List<TVFrontendAccount> accts = userSession.getAccounts();
				String[] acts = (String[]) parameters.get(AddressApplication.ACCOUNTS);
				AppliedToAccount apl = (AppliedToAccount) parameters.get(AddressApplication.APPLIED_TO);
				
//				This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//				{
				String residentialAddress1 = (String) parameters.get(AddressApplication.RESIDENTIAL_ADDRESS1);
				String residentialAddress2 = (String) parameters.get(AddressApplication.RESIDENTIAL_ADDRESS2);
				String residentialCity = (String) parameters.get(AddressApplication.RESIDENTIAL_CITY);
				String residentialState = (String) parameters.get(AddressApplication.RESIDENTIAL_STATE);
				String residentialZipCode = (String) parameters.get(AddressApplication.RESIDENTIAL_ZIPCODE);

				String oldResidentialAddress1 = "";
				String oldResidentialAddress2 = "";
				String oldResidentialCity = "";
				String oldResidentialState = "";
				String oldResidentialZipCode = "";
//				}
				
				String newPostalAddress1 = (String) parameters.get(AddressApplication.NEW_POSTAL_ADDRESS1);
				String newPostalAddress2 = (String) parameters.get(AddressApplication.NEW_POSTAL_ADDRESS2);
				String newPostalCity = (String) parameters.get(AddressApplication.NEW_POSTAL_CITY);
				String newPostalState = (String) parameters.get(AddressApplication.NEW_POSTAL_STATE);
				String newPostalZipCode = (String) parameters.get(AddressApplication.NEW_POSTAL_ZIPCODE);

				String oldPostalAddress1 = (String) parameters.get(AddressApplication.OLD_POSTAL_ADDRESS1);
				String oldPostalAddress2 = (String) parameters.get(AddressApplication.OLD_POSTAL_ADDRESS2);
				String oldPostalCity = (String) parameters.get(AddressApplication.OLD_POSTAL_CITY);
				String oldPostalState = (String) parameters.get(AddressApplication.OLD_POSTAL_STATE);
				String oldPostalZipCode = (String) parameters.get(AddressApplication.OLD_POSTAL_ZIPCODE);
				
//				This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//				{
				String addressChangeType = (String) parameters.get(ChallengeInformationUtils.ADDRESS_CHANGE_TYPE);
				boolean appliesToBoth = false;
				if(parameters.get(ChallengeInformationUtils.APPLIES_TO_BOTH) != null)
					appliesToBoth = (Boolean) parameters.get(ChallengeInformationUtils.APPLIES_TO_BOTH);
//				}
				
				Set<String> accounts = new HashSet<String>();
				
				if(outreachFlag){
					if (acts != null && acts.length > 0) {
						for (int i = 0; i < acts.length; i++) {
							String string = acts[i];
							TVFrontendAccount account = Utils.getAccountById(string, accts);
							accounts.add(account.getAccountNumber() + account.getSubtype());
						}
					}
				}
				// Only get the accounts if the change is postal or both
				else if (addressChangeType.equals("POSTAL") || addressChangeType.equals("BOTH")) {
					if (acts != null && acts.length > 0) {
						for (int i = 0; i < acts.length; i++) {
							String string = acts[i];
							TVFrontendAccount account = Utils.getAccountById(string, accts);
							accounts.add(account.getAccountNumber() + account.getSubtype());
						}
					}
				}	

				/* CIBP-1104 */
				boolean hasalert = false;
				AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
				if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
					Set<AlertType> alerts = ent.getAlerts();
					if (alerts.contains(AlertType.ADRESS_EDIT_EMAIL) || alerts.contains(AlertType.ADRESS_EDIT_SMS)
							|| alerts.contains(AlertType.ADRESS_EDIT_PUSH)) {
						hasalert = true;
					}
				}
				/* FIN--- CIBP-1104 */
				if(!outreachFlag){
					// Clear out all the residential values for postal changes
					if (addressChangeType.equals("POSTAL") || (addressChangeType.equals("BOTH") && !appliesToBoth)) {
						residentialAddress1 = "";
						residentialAddress2 = "";
						residentialCity = "";
						residentialState = "";
						residentialZipCode = "";
					}
					// Clear out all the postal values for residential changes
					else if (addressChangeType.equals("RESIDENTIAL")) {
						newPostalAddress1 = "";
						newPostalAddress2 = "";
						newPostalCity = "";
						newPostalState = "";
						newPostalZipCode = "";
					}
				}

				boolean saveApplication = false;
				
				AddressApplication addressApplication = null;
				
				if(outreachFlag){
					addressApplication = new AddressApplication("",
							"", "", "", "",
							"", "", "", "",
							"", newPostalAddress1, newPostalAddress2, newPostalCity, newPostalZipCode,
							newPostalState, oldPostalAddress1, oldPostalAddress2, oldPostalCity, oldPostalZipCode,
							oldPostalState, apl, accounts, ApplicationStatus.NEW);
				}else{
					addressApplication = new AddressApplication(residentialAddress1,
							residentialAddress2, residentialCity, residentialState, residentialZipCode,
							oldResidentialAddress1, oldResidentialAddress2, oldResidentialCity, oldResidentialState,
							oldResidentialZipCode, newPostalAddress1, newPostalAddress2, newPostalCity, newPostalZipCode,
							newPostalState, oldPostalAddress1, oldPostalAddress2, oldPostalCity, oldPostalZipCode,
							oldPostalState, apl, accounts, ApplicationStatus.NEW);
				}
				
				try {
					CustomerFacade.getInstance().saveAddressApplication(addressApplication,customer, request);
					saveApplication = true;
					
			    	final CustomerInteractionEvent interaction = Utils.generateCustomerInteractionEvent(request, userSession.getUsername()); // the event
			    	
			    	customerService.getInteractionService().logCustomerEvent(interaction, 
			        		 CustomerInteractionEventType.MYINFO_ADDRESS_CHANGE, InteractionSeverity.INFO);
				} catch (RuntimeException e) {
					saveApplication = false;
				}

				model.put("completed", true);
				if (!saveApplication) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Unable to send your change request, Please try later.");
					else
						userSession
								.setStatusmessage("Su solicitud de cambio no se pudo procesar en este momento, favor intentar luego.");
				} else {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession
								.setStatusmessage("Your change request will be processed in the next 2 business days.");
					else
						userSession
								.setStatusmessage("Su solicitud de cambio ser&aacute; procesada en 2 d&iacute;as laborables.");
					/*
					 * CIBP-1104 se activa envio alerta de modificacion de
					 * direccion
					 */
					if (hasalert) {
						customerService.sendAdressEditConfirmation(customer,
								userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
								userSession.getPreferredLanguage());
					}
				}
			}
		}
	}

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	// CIBP-2089 bgarcia
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	private Map<String, Object> loadPersonalInformation(HttpServletRequest request, Map<String, Object> model,
			UserSession session) {

		String showAlerts = (String) request.getParameter("showAlerts");
		if (GenericValidator.isBlankOrNull(showAlerts))
			model.put("showAlerts", false);
		else
			model.put("showAlerts", true);

		model.put("customer", session.getCustomerProfile());

		if (session.getCustomerProfile() != null) {
			if (session.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
				SMSProfile smsprofile = session.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
				if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
						&& smsprofile.getCtns().size() > 0) {
					model.put("EnrollSMS", true);
					model.put("smsPhone", ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn());
				} else
					model.put("EnrollSMS", false);
			} else
				model.put("EnrollSMS", false);
		}

		int account_total = 0;
		List<TVFrontendAccount> accts = session.getAccounts();
		Map<String, TVFrontendAccount> filteraccounts = new LinkedHashMap<String, TVFrontendAccount>();
		for (Iterator<TVFrontendAccount> iterator = accts.iterator(); iterator.hasNext();) {
			TVFrontendAccount frontendAccount = (TVFrontendAccount) iterator.next();
			if (filteraccounts.get(frontendAccount.getAccountNumber()) == null)
				account_total++;
		}

		model.put("account_total", account_total);

		return model;
	}

	private void updateEmail(HttpServletRequest request, Map<String, Object> model, Map<String, Object> parameters) {
		EditEmailBean dude = (EditEmailBean) parameters.get(ChallengeInformationUtils.EMAIL_ACTION);
		String email = dude.getEmail();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		try {

			/* CIBP-1104 */
			CustomerProfile customer = userSession.getCustomerProfile();
			// FFEIC
			String oldEmail = customer.getEmail();
			// FIN FFEIC
			boolean hasalert = false;
			AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
			if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
				Set<AlertType> alerts = ent.getAlerts();
				if (alerts.contains(AlertType.EMAIL_EDIT_EMAIL) || alerts.contains(AlertType.EMAIL_EDIT_SMS) 
						|| alerts.contains(AlertType.EMAIL_EDIT_PUSH)) {
					hasalert = true;
				}
			}
			/* FIN--- CIBP-1104 */

			// CIBP-2089 bgarcia
			if (getEBPPMessageSourceStatus()
					&& customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {

				ModifyCustomerEmailRequest emailmod = new ModifyCustomerEmailRequest(
						Utils.generateCustomerInteractionEvent(request), userSession.getCustomerProfile(), email);
				userSession.setCustomerProfile(userSession.getCustomerService().modifyCustomerEmail(emailmod,
						getEBPPMessageSourceStatus()));

				model.put("confirm", true);
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Email has been changed.");
				else
					userSession.setStatusmessage("E-mail ha sido cambiado.");
				/* CIBP-1104 se activa envio alerta de cambio de email */
				if (hasalert) {
					customerService.sendEmailEditConfirmation(customer,
							userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
							userSession.getPreferredLanguage(), oldEmail);
				}

				AccountMaskUtil maskutil = new AccountMaskUtil();
				List<TVFrontendAccount> accounts = userSession.getAccounts();
				List<evertec.cibp.core.services.smsAlerts.SmsBean> smsBeans = new LinkedList<evertec.cibp.core.services.smsAlerts.SmsBean>();
				for (TVFrontendAccount account : accounts) {
					String suffix;
					if (account != null && account.getAccountNumberSuffix() != null)
						suffix = account.getAccountNumberSuffix();
					else
						suffix = "";
					if ((!suffix.equalsIgnoreCase("")) && suffix.equalsIgnoreCase("RES")
							|| suffix.equalsIgnoreCase("SAV"))
						continue;
					
					boolean cont = false;
					if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE231)){
						cont = Utils.haveType(this.alertsAccountTypes, account.getAccountProductId());
					}else{
						cont = this.alertsAccountTypes.contains(account.getAccountProductId());
					}
					if (account != null && !account.getHide() && cont) {
						try {
							// if(!account.isCardPlasticsLoaded()){}
							CardPlasticFrontend[] plastics = userSession.getCardPlastics(account);

							if (plastics.length > 0
									&& !(account.getCurrentAthSmsAlerts() != null && account.getCurrentAthSmsAlerts()
											.size() > 0)) {
								// CardPlasticAccountRecord[]
								// plastics=account.getCardPlasticAccountRecords();
								HashMap<Object, Object> smsInfo = new HashMap<Object, Object>();
								for (int i = 0; i < plastics.length; i++) {
									evertec.cibp.core.services.smsAlerts.SmsBean smsBeanRequest = new evertec.cibp.core.services.smsAlerts.SmsBean();
									smsBeanRequest.setAcctNum(account.getAccountNumber());
									smsBeanRequest.setTaxId(customer.getTaxId());
									smsBeanRequest.setCardNum(plastics[i].getCardPlasticAccountRecord().getCardPANId());

									SmsAlertsResponse smsAlertsResponse = customerService.getSmsAlertInfo(
											smsBeanRequest, Utils.generateCustomerInteractionEvent(request));
									if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
											&& smsAlertsResponse.getResponseSmsBean() != null) {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
												smsAlertsResponse.getResponseSmsBean());
										smsBeans.add(smsAlertsResponse.getResponseSmsBean());
									} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
											&& smsAlertsResponse.getResponseSmsBean() == null) {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
										if (!smsAlertsResponse.isAuditable())
											logger.error("Error Updating SMS email while editing email: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId() + smsAlertsResponse.getAuditString());
										else
											logger.error("Error Updating SMS email while editing email: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId());
									} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
											&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
										if (!smsAlertsResponse.isAuditable())
											logger.error("Error Updating SMS email while editing email: Unable to send ATH SmsInfo request for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId());
										else
											logger.error("Error Updating SMS email while editing email: Unable to send ATH SmsInfo request for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
									} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
											&& (smsAlertsResponse.getException() != null || !GenericValidator
													.isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
										if (!smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
												&& smsAlertsResponse.getException() != null)
											logger.error("Error Updating SMS email while editing email: calling ATH SmsInfo \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getSmsAlertsException()
													+ "\n"
													+ smsAlertsResponse.getException());
										else if (!smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
												&& smsAlertsResponse.getException() == null) {
											logger.error("Error Updating SMS email while editing email: calling ATH SmsInfo \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getSmsAlertsException());
										} else if (!smsAlertsResponse.isAuditable()
												&& GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
												&& smsAlertsResponse.getException() != null) {
											logger.error("Error Updating SMS email while editing email: calling ATH SmsInfo \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId() + "\n" + smsAlertsResponse.getException());
										} else if (smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
												&& smsAlertsResponse.getException() != null)
											logger.error("Error Updating SMS email while editing email: calling ATH SmsInfo \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getSmsAlertsException()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
										else {
											logger.error("Error Updating SMS email while editing email: calling ATH SmsInfo \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
										}
									} else {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
										logger.error("Error Updating SMS email while editing email: calling ATH SmsInfo \nacct: "
												+ maskutil.mask(account.getAccountNumber())
												+ "\ncard: "
												+ maskutil.mask(smsBeanRequest.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n");
									}
								}
								if (smsInfo.size() > 0)
									account.setCurrentAthSmsAlerts(smsInfo);
							}

							if (plastics.length > 0 && account.getCurrentAthSmsAlerts() != null
									&& account.getCurrentAthSmsAlerts().size() > 0) {
								// CardPlasticAccountRecord[]
								// plastics=account.getCardPlasticAccountRecords();
								for (int i = 0; i < plastics.length; i++) {
									if (account.getCurrentAthSmsAlerts().containsKey(
											plastics[i].getCardPlasticAccountRecord().getCardPANId())
											&& (account.getCurrentAthSmsAlerts().get(
													plastics[i].getCardPlasticAccountRecord().getCardPANId()) != null)) {
										smsBeans.add((evertec.cibp.core.services.smsAlerts.SmsBean) account
												.getCurrentAthSmsAlerts().get(
														plastics[i].getCardPlasticAccountRecord().getCardPANId()));
									}
								}
							}

							List<evertec.cibp.core.services.smsAlerts.SmsBean> forUpdating = new LinkedList<evertec.cibp.core.services.smsAlerts.SmsBean>();

							if (smsBeans != null && smsBeans.size() > 0) {
								for (evertec.cibp.core.services.smsAlerts.SmsBean sms : smsBeans) {

									// org.apache.commons.beanutils.BeanUtils.copyProperties(smsBeanRequest,
									// a);
									sms.setPrimaryemail(email);
									forUpdating.add(sms);
								}
							}

							if (forUpdating != null && forUpdating.size() > 0) {

								for (evertec.cibp.core.services.smsAlerts.SmsBean sms : forUpdating) {
									evertec.cibp.core.services.smsAlerts.SmsBean newBean = new evertec.cibp.core.services.smsAlerts.SmsBean();

									org.apache.commons.beanutils.BeanUtils.copyProperties(newBean, sms);

									SmsAlertsResponse smsAlertsResponse = customerService.updateSmsAlert(
											newBean, Utils.generateCustomerInteractionEvent(request));
									if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
											&& smsAlertsResponse.getResponseSmsBean() != null) {
										account.getCurrentAthSmsAlerts().put(sms.getCardNum(),
												smsAlertsResponse.getResponseSmsBean());
									} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
											&& smsAlertsResponse.getResponseSmsBean() == null) {
										if (!smsAlertsResponse.isAuditable())
											logger.error("Error Updating SMS email while editing email: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ smsAlertsResponse.getAuditString());
										else
											logger.error("Error Updating SMS email while editing email: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: " + customer.getProfileId());
									} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
											&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
										if (!smsAlertsResponse.isAuditable())
											logger.error("Error Updating SMS email while editing email: Unable to send ATH smsUpdate request for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: " + customer.getProfileId());
										else
											logger.error("Error Updating SMS email while editing email: Unable to send ATH smsUpdate request for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
									} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
											&& (smsAlertsResponse.getException() != null || !GenericValidator
													.isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
										if (!smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
												&& smsAlertsResponse.getException() != null)
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getSmsAlertsException()
													+ "\n"
													+ smsAlertsResponse.getException());
										else if (!smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
												&& smsAlertsResponse.getException() == null) {
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getSmsAlertsException());
										} else if (!smsAlertsResponse.isAuditable()
												&& GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
												&& smsAlertsResponse.getException() != null) {
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getException());
										} else if (smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
												&& smsAlertsResponse.getException() != null)
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getSmsAlertsException()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
										else {
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
										}
									} else {

										if (!smsAlertsResponse.isAuditable())
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: " + customer.getProfileId() + "\n");
										else {
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
										}
									}

								}

							}

						} catch (Exception e) {
							logger.error(
									"Error Updating SMS email while editing email: \nacct: "
											+ maskutil.mask(account.getAccountNumber()) + "\n" + "\nProfileID: "
											+ customer.getProfileId(), e);
						}

					}
				}

			} else {// CIBP-2089 bgarcia
				model.put("confirm", true);
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("We are sorry, the system isn&acute;t available. Please try later.");
				else
					userSession
							.setStatusmessage("Lo sentimos, el sistema no est&aacute; disponible. Por favor intenta mas tarde.");
			}

		} catch (Exception e) {
			model.put("confirm", true);
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Unable to change email.");
			else
				userSession.setStatusmessage("Email no pudo ser cambiado.");
		}
	}

	public Integer getMaxPasswordRecords() {
		return maxPasswordRecords;
	}

	public void setMaxPasswordRecords(Integer maxPasswordRecords) {
		this.maxPasswordRecords = maxPasswordRecords;
	}

	public String getAlertsAccountTypes() {
		return alertsAccountTypes;
	}

	public void setAlertsAccountTypes(String alertsAccountTypes) {
		this.alertsAccountTypes = alertsAccountTypes;
	}

}
