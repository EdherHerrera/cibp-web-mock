package evertec.cibp.web.form;


import evertec.cibp.campaign.loader.CampaignPaths;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.customer.CustomerPhone;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.devices.CustomerDevice;
import evertec.cibp.core.models.customer.devices.DeviceStatus;
import evertec.cibp.core.models.customer.devices.ProductType;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.mbop.MbopCustomerInfo;
import evertec.cibp.core.models.mbop.MbopStatus;
import evertec.cibp.core.services.requests.ValidateLoginInfoRequest;
import evertec.cibp.core.services.requests.opac.CISOpacCustomerInfoRequest;
import evertec.cibp.core.services.responses.CustomerDeviceResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.core.services.responses.opac.CISOpacCustomerInfoResponse;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.PasswordBean;
import evertec.cibp.web.form.dto.DeviceInfoDto;
import evertec.cibp.web.models.devices.DeviceInfo;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PasswordForm extends CancellableFormController {
    /** Logger */
    protected final Log logger = LogFactory.getLog(getClass());
    private CampaignPaths paths;
    private String updateiPads;
    private String delayParameter;
    //(OPAC) MBSB-448
    private Integer eaccountMinAgeLimit;
    //(OPAC) MBSB-1444
    private String wealthCodes;
    private String preferredCodes;
    private String[] opacCountries;

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        PasswordBean passwordBean = (PasswordBean) super.formBackingObject(request);

        String type = ServletRequestUtils.getStringParameter(request, "agentType", "web");
        if ("android".equalsIgnoreCase(type) || "ios".equalsIgnoreCase(type)) {
            passwordBean.setMobileAppType(type);
            passwordBean.setMobileApp(Boolean.TRUE);

        }
        return passwordBean;
    }

    public PasswordForm() {
        super();
        setCommandClass(PasswordBean.class);
        setCommandName("password");
        setFormView("password");
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
            BindException errors) throws Exception {

        UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        PasswordBean password = (PasswordBean) command;
        String answer = password.getAnswer();
        Map<String, Object> model = new HashMap<>();

        //MBFM-99
        final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
        model.put(cookieAnalyticEnabled, session != null && session.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
        //END MBFM-99

        DeviceInfo deviceInfo = new DeviceInfoDto().passwordBeanToDeviceInfo(password);

        session.setDeviceInfo(deviceInfo);

        // FFEIC
        String header = request.getHeader("user-agent");
        // FIN FFEIC

        if (GenericValidator.isBlankOrNull(answer)) {
            errors.rejectValue("answer", "login.username.required", "Required");
            if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1529)){
                if(session.getUserAgentType().equals(UserAgentType.ANDROID) || session.getUserAgentType().equals(UserAgentType.IPHONE)){
                    errors.rejectValue("answer", "password.answer.validation", "Remember that your password may have capital letters, numbers and/or special characters.");
                }
            }
            return showForm(request, response, errors);
        }

        int passwordSalt = 0;
        if (session.getCustomerProfile() != null && session.getCustomerProfile().getPasswordSalt() != 0)
            passwordSalt = session.getCustomerProfile().getPasswordSalt();
        else if (session.getPasswordSalt() != 0)
            passwordSalt = session.getPasswordSalt();

        String passwordHash = PasswordHashUtil.generateSaltedHash(answer, passwordSalt);
        ValidateLoginInfoRequest loginRequest = new ValidateLoginInfoRequest(Utils.generateCustomerInteractionEvent(request), session.getUsername(), passwordHash,
                session.getDeviceRequest(), request.getRemoteAddr(), DeviceUtils.getCustomerDevice(header).toString());


        if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)) {

            if(session.getUserAgentType() == UserAgentType.WEB ||  session.getUserAgentType() == UserAgentType.IPHONE ||  session.getUserAgentType() == UserAgentType.ANDROID) {

                loginRequest.getInteractionEvent().setCustomStringData(DeviceUtils.CONNECTION_INFO, DeviceUtils.getBrowserInfo(header, session));

            } else if (session.getUserAgentType() == UserAgentType.JSON) {

                loginRequest.getInteractionEvent().setCustomStringData(DeviceUtils.CONNECTION_INFO, DeviceUtils.getMobileInfo(header, deviceInfo));
            }

        }

        String userAgent = request.getHeader("user-agent");
        String customerDevices = "";
        if(userAgent != null) {			
            customerDevices = DeviceUtils.getCustomerDevice(userAgent).toString();
        }
        loginRequest.setCustomerDevices(customerDevices);

        ModifyCustomerProfileResponse responseAnswer = null;

        if(CustomerUtils.isSecurities(session.getCustomerProfile()) 
                && session.getCustomerProfile().getStatus().equals(CustomerStatus.ENROLLED)) {
            responseAnswer = session.getCustomerService().validatePasswordSecurities(loginRequest);
        }
        else {
            responseAnswer = session.getCustomerService().validatePassword(loginRequest);
        }

        if (ResultStatus.SUCCESS.equals(responseAnswer.getStatus())) {

            if (StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_DROP))
                session.setDeviceId(password.getDeviceId());

            logger.warn("Device Info\n username: " + session.getCustomerProfile().getUsername() + "\n"
                    + "  deviceToken: " + password.getDeviceToken() + "\n" + "  deviceId: " + password.getDeviceId()
                    + "\n" + "  deviceNickname: " + password.getDeviceNickname());

            //since mbop info is not saved as part of the profile
            //for each request to get the profile the mbop ID has to be setted again
            String mbopId = session.getCustomerProfile().getMbopId(); 

            session.getTasksList().remove(ASK_FOR.VALIDATE_PASSWORD);
            if (responseAnswer.getUsernameReservation() != null){
                session.setUsernameReservation(responseAnswer.getUsernameReservation());
            }
            else{
                session.setCustomerProfile(responseAnswer.getCustomerProfile());
                session.getCustomerProfile().setMbopId(mbopId);
            }

            if (session.getUserAgentType() == UserAgentType.JSON) {
                if (!GenericValidator.isBlankOrNull(password.getDeviceToken())) {
                    this.addWidgetDevice(request, password);
                }

                if (StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_DROP)) {
                    if (!GenericValidator.isBlankOrNull(password.getFingerprintId())) {
                        this.addFingerprintDevice(request, password);
                    }
                }
            }
            session.setOobLogin(false); //OOB is not used anymore as login
            /*
             * cibp-1867, aqui se redirige despues de un login exitoso a la
             * pagina que indique el redirect, siempre y cuando no sea nulo
             */

            if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA573) 
                    && evertec.cibp.core.utils.AccountUtils.getIsTransactional(session.getCustomerProfile()).equalsIgnoreCase("N")){

                //Get all profiles with customers tax id.
                List<CustomerProfile> profiles;

                //(MBNA) MBNA-1099
                profiles = CustomerFacade.getInstance().getCustomerProfileById("taxId",session.getCustomerProfile().getTaxId(), session);



                //remove current profile from list
                profiles.remove(session.getCustomerProfile());

                for(CustomerProfile profile : profiles){

                    if(evertec.cibp.core.utils.AccountUtils.getIsTransactional(profile).equalsIgnoreCase("Y")){
                        model.put("nonTransHasTrans", "true");
                        model.put("username", session.getUsername());
                        return new ModelAndView("forward:login", model);
                    }
                }
            }

            //MBFIS-253
            // Customer isSecurities and has deleted RSA information
            if (CustomerUtils.isSecurities(session.getCustomerProfile()) 
                    && session.getTasksList().contains(ASK_FOR.RSA_ENROLL)){				
                return new ModelAndView("redirect:securityinformationsec");
            }
            //END MBFIS-253

            String redirect = (String) WebUtils.getSessionAttribute(request, "redirect");
            if (redirect != null) {
                /*
                 * aqui ya se valido el usuario y password, como es correcto
                 * entonces se establece el loginCompleteSuccess en true, para
                 * poder llevarlo a otra pagina sin ningun problema
                 */
                session.setLoginCompleteSuccess(true);

                return new ModelAndView("redirect:" + redirect);
            }

        } else if (ResultStatus.BLOCKED.equals(responseAnswer.getStatus())) {
            /* CIBP-1104 */
            boolean hasalert = false;
            AlertsEntitlement ent = new AlertsEntitlement(session.getCustomerProfile().getEntitlementByType(
                    EntitlementType.ALERTS));
            if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
                Set<AlertType> alerts = ent.getAlerts();
                if (alerts.contains(AlertType.ACCESS_BLOCK_EMAIL) || alerts.contains(AlertType.ACCESS_BLOCK_SMS) 
                        || alerts.contains(AlertType.ACCESS_BLOCK_PUSH)) {
                    hasalert = true;
                }
            }
            /* CIBP-1104 se activa envio alerta de blockeo de password */
            if (hasalert) {
                session.getCustomerService().sendAccessBlockedConfirmation(session.getCustomerProfile(),
                        session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
                        session.getPreferredLanguage());
            }
            /* FIN--- CIBP-1104 */

            //MBFIS-141 - ET57991
            if (CustomerUtils.isSecurities(session.getCustomerProfile()) && AccountUtils.getIsTransactional(session.getCustomerProfile()).equalsIgnoreCase("N")) { 
                return new ModelAndView("redirect:forgotpasswordsec");
            }
            //END MBFIS-141

            model.put("passblocked", "true");
            model.put("username", session.getUsername());
            request.setAttribute("firstTime", false);

            model.put("MBSFE291", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE291));
            if(!header.startsWith("Android") && !header.startsWith("iPhone")){
                return new ModelAndView("redirect:regainaccess?resource=BLOCKED", model);
            }else{
                return new ModelAndView("forward:login", model);
            }


            // errors.rejectValue("answer", "password.answer.blocked",
            // "<a href=\"resetpassword\" style=\"color:red;\">Account blocked</a>");
            // return showForm(request, response, errors);
        } else if (ResultStatus.DENY.equals(responseAnswer.getStatus())) {
            model.put("error", "RSA Denied");
            return new ModelAndView("deny", model);

        } else if (!ResultStatus.SUCCESS.equals(responseAnswer.getStatus())) {
            //logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException) - Unexpected RSA Condition - loginRequest=" + loginRequest + ", responseAnswer=" + responseAnswer, null); //$NON-NLS-1$ //$NON-NLS-2$
            errors.rejectValue("answer", "password.answer.invalid", "Invalid");
            if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1529)){
                if(session.getUserAgentType().equals(UserAgentType.ANDROID) || session.getUserAgentType().equals(UserAgentType.IPHONE)){
                    errors.rejectValue("answer", "password.answer.validation", "Remember that your password may have capital letters, numbers and/or special characters.");
                }
            }
            return showForm(request, response, errors);
        }

        return new ModelAndView(SignonSequenceUtils.getNextView(session));
    }

    @Override
    protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
            throws Exception {
        PasswordBean passwordBean = (PasswordBean) command;
        Map<String, Object> model = new HashMap<>();
        UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        model.put("username", session.getUsername());
        model.put("mobileApp", passwordBean.isMobileApp());
        model.put("mobileAppType", passwordBean.getMobileAppType());

        //MBFM-99
        final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
        model.put(cookieAnalyticEnabled, session.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
        //END MBFM-99

        //MBFIS-154
        model.put("isSecuritiesCustomer", CustomerUtils.isSecurities(session.getCustomerProfile()));		
        model.put("MBFIS_MOBILE", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_MOBILE));
        model.put("rsaCookie", session.getDeviceRequest() != null ? session.getDeviceRequest().getDeviceTokenCookie() : "");
        //END MBFIS-154
        if(!GenericValidator.isBlankOrNull(updateiPads) && updateiPads.equalsIgnoreCase("Y"))
            model.put("iPadUpdate", updateiPads);

        /* CIBP-2353 */
        File file = new File(this.paths.getFilePath() + "fixedEnCampaign.vm");
        if (file != null && file.exists()) {
            model.put("fixedCampaign", "true");
        }
        file = new File(this.paths.getFilePath() + "divEnWantToDo.vm");
        if (file != null && file.exists()) {
            model.put("WantToDo", "true");
        }
        final boolean regainSecurities = CustomerUtils.isSecurities(session.getCustomerProfile()) && 
        		AccountUtils.getIsTransactional(session.getCustomerProfile()).equalsIgnoreCase("N");
        WebUtils.setSessionAttribute(request, "regainSecurities", regainSecurities);
        /* FIN CIBP-2353 */

        /* MBDP-2226 */

        if(StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC)){

            session.setForeignCustomer(false);

            CISOpacCustomerInfoRequest cisRequest = new CISOpacCustomerInfoRequest();
            CustomerProfile customerProfile = session.getCustomerProfile();
            cisRequest.setCustomerPermId(customerProfile.getPermId().toString());
            boolean canOpenAccount = false;

            try{

                CISOpacCustomerInfoResponse cisResponse = session.getCustomerService().getCISCustomerInfo(cisRequest);

                if (customerProfile.getPermId()!= null && cisResponse != null){
                    for(String opacCountry: opacCountries){
                        if( cisResponse.getCountry().equalsIgnoreCase(opacCountry) 
                                && StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC) 
                                && !(customerProfile.getUserInterface() == UserInterface.COMMERCIAL) 
                                && (session.retornaEdad(cisResponse.getDateOFBirth()) >= this.eaccountMinAgeLimit) ){
                            canOpenAccount= true;
                            break;
                        }
                    }
                    if(canOpenAccount){

                        if(session.getCanOpenDepositAccount() == null) {
                            session.setCanOpenDepositAccount(session.getCustomerService().canOpenDepositAccount(
                                    session.getCustomerProfile(), String.valueOf(customerProfile.getPermId()),
                                    session.getCustomerWealthPreferred(this.wealthCodes, this.preferredCodes)));                    	
                        }

                        session.setForeignCustomer(!session.getCanOpenDepositAccount());

                    }
				}
				
			}catch(Exception e){
				logger.error("Error al obtener informacion de apertura de cuenta: " + e.getMessage());
			}
			
			model.put("canOpenAccount", canOpenAccount);
			model.put("isForeignCustomer", session.getForeignCustomer());
		}
		
		/* MBDP-2226 */
		
		// Register Customer Devices for Realtime Account Balance
		if (session.getUserAgentType() == UserAgentType.JSON) {
			MbopCustomerInfo mbopCustInfo = session.getCustomerService().getMbopCustomerInfoByProfileId(
					session.getCustomerProfile().getProfileId());
			if (mbopCustInfo == null) {
				mbopCustInfo = new MbopCustomerInfo();
				mbopCustInfo.setEnrollmentSource(CustomerEnrollmentSource.WEB);
				mbopCustInfo.setProfileId(session.getCustomerProfile().getProfileId());
				mbopCustInfo.setStatus(MbopStatus.ACTIVE);
				
				//(MBNA) MBNA-3112
                CustomerFacade.getInstance().saveMbopCustomerInfo(mbopCustInfo, session);


            }
            model.put("customerToken", mbopCustInfo.getMbopId());

            if (StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_DROP)) {
                CustomerPhone searchCriteria = new CustomerPhone();
                searchCriteria.setProfileId(session.getCustomerProfile().getProfileId());

                /**
                 * MBNA-2986
                 * Method getCustomerPhone : CIBP-WEB microservice integration
                 * 
                 */
                CustomerPhone respPhone = CustomerFacade.getInstance().getCustomerPhone(searchCriteria, session);

                if (respPhone != null)
                    model.put("phoneNumber", respPhone.getPhoneNumber());
            }

        }

        //MBSFE-1529
        for (int i = 0; i < errors.getErrorCount(); i++) {
            FieldError error = (FieldError) errors.getFieldErrors().get(i);
            if(error.getField().equals("answer")){
                model.put("answerError", "answerTip");
            }
        }
        model.put("MBSFE1529", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1529));
        //END MBSFE-1529

        return model;
    }

    private void addWidgetDevice(HttpServletRequest request, PasswordBean password) {
        CustomerDevice device = new CustomerDevice();
        UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        device.setProfileId(session.getCustomerProfile().getProfileId());
        device.setUsername(session.getCustomerProfile().getUsername());
        device.setDeviceMbopId(password.getDeviceToken());
        device.setProductType(ProductType.WIDGET);
        device.setDeviceStatus(DeviceStatus.ACTIVE);
        device.setDeviceRegDate(new Date());
        device.setDeviceNickname(password.getDeviceNickname());
        CustomerDeviceResponse deviceResponse = session.getCustomerService().getCustomerDeviceByDeviceMbopId(
                password.getDeviceToken());

        this.addDevice(request, device, deviceResponse);
    }

    private void addFingerprintDevice(HttpServletRequest request, PasswordBean password) {
        CustomerDevice device = new CustomerDevice();
        UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        device.setProfileId(session.getCustomerProfile().getProfileId());
        device.setDeviceMbopId(password.getFingerprintId());
        device.setProductType(ProductType.FINGERPRINT);
        device.setDeviceStatus(DeviceStatus.ACTIVE);
        device.setDeviceRegDate(new Date());
        device.setDeviceNickname(password.getDeviceNickname());
        CustomerDeviceResponse deviceResponse = session.getCustomerService().getCustomerDeviceByDeviceMbopIdAndType(device);

        session.setFingerprintId(password.getFingerprintId());
        this.addDevice(request, device, deviceResponse);
    }

    private void addDevice(HttpServletRequest request, CustomerDevice device, CustomerDeviceResponse deviceResponse) {
        if (deviceResponse != null && deviceResponse.getResponseStatus() == ResultStatus.NOT_FOUND
                && !GenericValidator.isBlankOrNull(deviceResponse.getResponseDescription())
                && deviceResponse.getResponseDescription().equals("No Results")) {
            Utils.addCustomerDevice(request, device, delayParameter);
        } else if(deviceResponse != null && deviceResponse.getDevice() != null) {
            UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
            session.getCustomerDevices();
            session.setCustomerDevices(deviceResponse.getDevice());
        }
    }

    public CampaignPaths getPaths() {
        return paths;
    }

    public void setPaths(CampaignPaths paths) {
        this.paths = paths;
    }

    public String getUpdateiPads() {
        return updateiPads;
    }

    public void setUpdateiPads(String updateiPads) {
        this.updateiPads = updateiPads;
    }

    public String getDelayParameter() {
        return delayParameter;
    }

    public void setDelayParameter(String delayParameter) {
        this.delayParameter = delayParameter;
    }

    public Integer getEaccountMinAgeLimit() {
        return eaccountMinAgeLimit;
    }

    public void setEaccountMinAgeLimit(Integer eaccountMinAgeLimit) {
        this.eaccountMinAgeLimit = eaccountMinAgeLimit;
    }

    /**
     * @return the wealthCodes
     */
    public String getWealthCodes() {
        return wealthCodes;
    }

    /**
     * @return the preferredCodes
     */
    public String getPreferredCodes() {
        return preferredCodes;
    }

    /**
     * @return the opacCountries
     */
    public String[] getOpacCountries() {
        return opacCountries;
    }

    /**
     * @param wealthCodes the wealthCodes to set
     */
    public void setWealthCodes(String wealthCodes) {
        this.wealthCodes = wealthCodes;
    }

    /**
     * @param preferredCodes the preferredCodes to set
     */
    public void setPreferredCodes(String preferredCodes) {
        this.preferredCodes = preferredCodes;
    }

    /**
     * @param opacCountries the opacCountries to set
     */
    public void setOpacCountries(String[] opacCountries) {
        this.opacCountries = opacCountries;
    }
}
