package evertec.cibp.web.form;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.CreditCardAccountRecord;
import bppr.tv.beans.accounts.types.CardAccount;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerPayeePreferences;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVAccountFavoritePayment;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.statements.AccountPDFStmtInfo;
import evertec.cibp.core.models.statements.AccountStmtInfo;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.PaymentDisplayHistoryBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.UserAccount;
import evertec.prophit.service.bean.CustomerInquiryBean;
import evertec.prophit.service.bean.EbillIndType;

public class PaymentDisplayHistoryInformation extends CancellableFormController {

	private String cancelView;
	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;

	public PaymentDisplayHistoryInformation() {
		super();
		setCommandClass(PaymentDisplayHistoryBean.class);
		setCommandName("pmtHistoryInfo");
		setFormView("pmtHistoryInfo");
	}

	@Override
	protected boolean isFormSubmission(HttpServletRequest request) {
		return ((request.getParameter("id") != null && !request.getParameter("id").equals("")));
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object pmtHistoryInfo,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		List<TVFrontEndPayee> payees = userSession.getPayessBasicEbpp(true);
		payees = userSession.getPayeesPaymentHistoryInfo();

		String today = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
		model.put("today", today);

		PaymentDisplayHistoryBean paymentsList = (PaymentDisplayHistoryBean) pmtHistoryInfo;
		String id = paymentsList.getId();
		if (id != null && id.equals("ALL")) {
			model.put("text", true);
			return new ModelAndView("text", model);
		}

		String favid = paymentsList.getFavid();
		model.put("indexfav", favid);

		List<TVPayment> pendingPayments = null;
		TVPayment lastpayment = null;
		TVFrontEndPayee payee = null;
		if (id == null) {
			return new ModelAndView("pmtHistoryInfo", model);
		}

		else {
			payee = Utils.getPayeebyId(id, payees);
			List<TVAccountFavoritePayment> favorites = userSession.getFavoritePayments();
			if (favorites != null) {
				for (Iterator<TVAccountFavoritePayment> iterator = favorites.iterator(); iterator.hasNext();) {
					TVAccountFavoritePayment name = (TVAccountFavoritePayment) iterator.next();

					if (name.getPayee() != null && name.getPayee().getFrontEndId().equals(id)) {
						if (name.getPendingPayments() != null) {
							for (Iterator<TVPayment> iterator2 = name.getPendingPayments().iterator(); iterator2
									.hasNext();) {
								TVPayment pmt = (TVPayment) iterator2.next();
								TVFrontEndPayee py = Utils.getPayeebyPayeeId(pmt.getPayeeId(), payees);
								if (py != null && py.getFrontEndId().equals(id)) {
									pendingPayments = name.getPendingPayments();
								}

								pmt.setPayee(py);
								payee = py;
								// pmt.setSource(new
								// TVAccountTransferSource(Utils.getAccountSourceByNumber(pmt.getSource().getAccount().getAccountNumber(),
								// userSession.getAccountsPaymentFrom())));
							}
						}
						if (name.getLastPayment() != null) {
							lastpayment = (TVPayment) name.getLastPayment();
							TVFrontEndPayee py = Utils.getPayeebyPayeeId(lastpayment.getPayeeId(), payees);
							if (py != null)
								payee = py;
						}
						break;
					}

				}
			}

			// The EBPP2 user id is not yet in session; add it
			if (!userSession.isEbpp2UserIdLookedUp()) {
				EbppEntitlement ent = null;
				if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.EBILLS) != null) {
					ent = new EbppEntitlement(userSession.getCustomerProfile().getEntitlementByType(
							EntitlementType.EBILLS));

					// If the username is blank, we need to look in the current
					// EBPP accounts ONLY
					if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
						userSession.setEbpp2UserId("");
					} else {
						userSession.setEbpp2UserId(ent.getUsername());
					}
					userSession.setEbpp2UserIdLookedUp(true);
				}
			}

			// The user has an EBPP2 ID
			if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {
						
				// Get the list of user accounts
				List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccountsAndSummaries();

				// Loop through all the accounts and match them with the proper
				// userAccount
				if (payee != null && ebpp2UserAccounts != null && ebpp2UserAccounts.size() > 0) {
					payee.setEbpp2UserAccount(Utils.getEbpp2UserAccountsByAccount(payee.getBillingAccount(),
							userSession));
					payee.setAvailabletoEbill(true);
				}
				
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)){
					if(Utils.getPayeeisCCA(payee.getBillingAccount(), userSession)){
						TVFrontendAccount accountfromPayee = Utils.getAccountByAccountNumber(payee.getBillingAccount(), userSession.getAccounts());
						UserAccount userAcc = new UserAccount();
						model.put("eBillsFlag", true);
						if(accountfromPayee != null && accountfromPayee.getIsAvailableElecStmt()) {
							try{
								CustomerInquiryBean responseTsys =  customerService.prophItService()
										.makeCustomerInquiry(payee.getBillingAccount());
									if (responseTsys != null) {
										if (responseTsys.getEbillInd().equals(EbillIndType.ACTIVE.toString())) {
											payee.setSuscribetoEbill(true);
											payee.setAvailabletoEbill(true);
										}else if(responseTsys.getEbillInd().equals(EbillIndType.ELIGIBLE.toString())){
											payee.setSuscribetoEbill(false);
											payee.setAvailabletoEbill(true);
										}else if(responseTsys.getEbillInd().equals(EbillIndType.NOELIGIBLE.toString())){							
											payee.setAvailabletoEbill(false);
										}
									}
								} catch (Exception e) {
									CardAccount ca = (CardAccount) accountfromPayee.getBackendObject();
									if(ca != null) {
										CreditCardAccountRecord ccar = (CreditCardAccountRecord) ca.getRecords();
										if(ccar.geteStatementIndicator() != null && ccar.geteStatementIndicator().trim().equals("A")) {
											payee.setSuscribetoEbill(true); //Is comercial account or tsys have error
										}
									}
									logger.error(e.getMessage());
								}
							List<AccountStmtInfo> ccaDetails = userSession.getCcaEbills(request);						
							if(accountfromPayee!=null){
								AccountStmtInfo acctdetails = Utils.getCCAEbillDetails(request, accountfromPayee, ccaDetails, userSession);

								if (acctdetails != null && acctdetails.getAccountStatements() != null
										&& !acctdetails.getAccountStatements().isEmpty()) {
									AccountPDFStmtInfo lastStatement = acctdetails.getAccountStatements().get(0);

									model.put("statement", acctdetails);
									model.put("statementPDF", lastStatement);
								}
								
								/*Use Ajax Call tsysStatusPayees */							
								userAcc.setAccountLabel("CCA_TSYS");
								
								SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
								CustomerPayeePreferences cPP = payee.getCustomerPayeePreferences();
								String lastPaymentDateString = cPP.getLastPaymentDate();
								if (lastPaymentDateString != null && !lastPaymentDateString.isEmpty()) {
									Date lastPaymentDate = (Date) format.parse(lastPaymentDateString);
									if (accountfromPayee.getStatements().get(1).getEndDate().after(lastPaymentDate)
											|| accountfromPayee.getStatements().get(1).getEndDate()
													.equals(lastPaymentDate)) {
										userAcc.setLastEbillPaid(true);
									}
								}
							}
						}else {
							payee.setAvailabletoEbill(false);
						}						
						payee.setEbpp2UserAccount(userAcc);
					}//End if is cca
					else{
						//*Hacer la validacion por global payee id*/	
						//359 Visa, 223 MC y 1983 Amex
						if(payee.getGlobalPayeeId() == 359 
								|| payee.getGlobalPayeeId() == 223
								|| payee.getGlobalPayeeId() == 1983){
							UserAccount userAcc = new UserAccount();
							model.put("eBillsFlag", true);					
							userAcc.setAccountLabel("CCA_NOT_FROM_USER");
							payee.setEbpp2UserAccount(userAcc);
							payee.setAvailabletoEbill(false);
						}
					}
				}//End if ebills flag
			
			}

			model.put("pendingPayments", pendingPayments);
			model.put("lastpayment", lastpayment);
			model.put("payee", payee);
			return new ModelAndView("pmtHistoryInfo", model);
		}

	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Map referenceData(HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		return super.referenceData(request);
	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		return new ModelAndView(cancelView);
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	@SuppressWarnings("unused")
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

}
