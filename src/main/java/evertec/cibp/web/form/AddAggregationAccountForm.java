package evertec.cibp.web.form;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.aggregation.AggregationDiscoverAccountsBean;
import evertec.cibp.core.models.aggregation.AggregationInstitutionKeysBean;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.form.bean.AddAggregationAccountBean;
import evertec.cibp.web.session.UserSession;

public class AddAggregationAccountForm extends AbstractWizardFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public AddAggregationAccountForm() {
		super();
		setCommandClass(AddAggregationAccountBean.class);
		setCommandName("aggaccount");
		setPages(new String[] { "addaggregationaccount", "addaggregationaccount" });
	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		AddAggregationAccountBean dude = (AddAggregationAccountBean) command;

		Integer aggcustomerid = customerService.getAggregationCustomerIdByUsername(userSession.getUsername());
		List<AggregationDiscoverAccountsBean> aggregationdiscoveraccountsList = dude
				.getAggregationdiscoveraccountsList();
		for (AggregationDiscoverAccountsBean o : aggregationdiscoveraccountsList) {
			customerService.addAggregationAccount(o.getAccountnumber(), o.getAccountnickname(), aggcustomerid,
					dude.getInstitutionid(), "USD", userSession.getUsername(), o.getBalanceamount(), new Date(), false);
		}

		return new ModelAndView("redirect:editaccount");
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		Map<String, Object> model = new HashMap<>();
		AddAggregationAccountBean dude = (AddAggregationAccountBean) command;

		Integer id = dude.getInstitutionid();
		if (id == null)
			id = Integer.parseInt(request.getParameter("institutionid"));

		dude.setInstitutionid(id);
		model.put("id", id);
		if (page == 0) {
			Map<Object, Object> institutionKeys = new LinkedHashMap<Object, Object>();
			List<AggregationInstitutionKeysBean> losdude = customerService.getAggregationInstitutionKeys(id);
			for (AggregationInstitutionKeysBean undude : losdude) {
				institutionKeys.put(undude.getTpInstitutionKeyId(), undude);
			}
			model.put("institutionKeys", institutionKeys);
		} else if (page == 1) {
			Map<Object, Object> keyValues = new LinkedHashMap<Object, Object>();
			Set<Entry<Object, Object>> a = request.getParameterMap().entrySet();
			for (Entry<Object, Object> e : a) {
				String string = e.getKey().toString();
				if (!string.equalsIgnoreCase("_target1"))
					keyValues.put(e.getKey(), request.getParameter(string));
			}
			List<AggregationDiscoverAccountsBean> discoverAggregationAccounts = customerService
					.discoverAggregationAccounts(id, keyValues);
			dude.setAggregationdiscoveraccountsList(discoverAggregationAccounts);
			model.put("discoverAccounts", discoverAggregationAccounts);
		}

		model.put("page", page);
		return model;
	}

}
