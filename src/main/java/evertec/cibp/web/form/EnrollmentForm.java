package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.models.profile.ProfileType;
import evertec.cibp.web.controller.validator.PasswordConstraintValidator;
import evertec.cibp.web.form.dto.EnrollmentFormDto;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.DeviceRequest;

import bppr.tv.beans.Owner;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.AccountRecord;
import edu.emory.mathcs.backport.java.util.Arrays;
import evertec.cibp.core.models.accountopening.EaccountMessage;
import evertec.cibp.core.models.accountopening.dci.DCICustomer;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.campaign.GuidedSetupInfo;
import evertec.cibp.core.models.campaign.ViewCampaign;
import evertec.cibp.core.models.campaign.ViewCampaignId;
import evertec.cibp.core.models.csr.CustomerSearchFilter;
import evertec.cibp.core.models.customer.CustomerAuthMethod;
import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerProfileStatus;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.MultipleCustomerProfileResponse;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.enrollment.EnrollableAccount;
import evertec.cibp.core.models.enrollment.EnrollmentInfo;
import evertec.cibp.core.models.enrollment.EnrollmentReason;
import evertec.cibp.core.models.enrollment.EnrollmentStatus;
import evertec.cibp.core.models.enrollment.EnrollmentType;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.CardCVVAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.CardPINAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.ConnectionInfoValidationRequest;
import evertec.cibp.core.services.requests.DepositAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.EnrollmentInformationRequest;
import evertec.cibp.core.services.requests.ModifyAllCustomerEmailsRequest;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.requests.NonTransAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.PayrollCardAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.PersonalInformationValidationRequest;
import evertec.cibp.core.services.responses.ConnectionInfoValidationResponse;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse.EnrollmentValidationStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse.AccountValidationStatus;
import evertec.cibp.core.utils.CibpValidationUtils;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.InternationalCharacterUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.encryptedPin.PinEncryptionTool;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.CaptchaSettings;
import evertec.cibp.web.form.bean.EnrollmentBean;
import evertec.cibp.web.form.validator.EnrollmentFormPageValidator;
import evertec.cibp.web.session.CaptchaEnabledForm;
import evertec.cibp.web.session.CaptchaSession;
import evertec.cibp.web.session.EnrollmentSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.responses.UnicaEnrollPrefillResponse;

public class EnrollmentForm extends AbstractWizardFormController implements CaptchaEnabledForm,
		GlobalEntitlementAwareController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private String cancelView;
	private CustomerServices customerService;
	private PinEncryptionTool pinTool;
	private CaptchaSettings captchaSettings;
	private String nerdsPrefillUrl;
	private boolean availableUsername = true; // CIBP-1947
	private static String UNICA_URL_PARAM_NAME = "applicationId";
	private static String REQUEST_POST_METHOD = "POST";
	private String comercialProducts;
	private String paymentvalidationrange;
	private String ntenrollmentaccounts;
	private String wordListDictionary;
	private UserAgentType userAgentType;
	private Integer ageRestriction;

	// MBSFE-691
	private String invalidEmailDomains;
	
	// END MBSFE-691	
	private static final String ENROLLMENT_PASSWORD_CONTAINS_USERNAME = "enrollment.pwd.contains.username";
	private static final String PASSWORD_ERROR = "password";
	private static final String PASSWORD_CONTAINS_USERNAME = "Password must not contains username";

	private void initForm(){
		setCommandClass(EnrollmentBean.class);
		setCommandName("enrollment");
		setPages(new String[] { "enrollment", "enrollment", "enrollment", "enrollment", "enrollment", "bsmartEnrollAlert", "iphone/bsmartEnrollAlert"});

	}

	public EnrollmentForm() {
		super();
		initForm();
	}

	public EnrollmentForm(String cancelView, CustomerServices customerService,
						  PinEncryptionTool pinTool, CaptchaSettings captchaSettings,
						  EnrollmentFormDto enrollmentFormDto, PropertyEditorRegistrar propertyEditorRegistrar) {
		super();
		initForm();
		this.cancelView = cancelView;
		this.customerService = customerService;
		this.pinTool = pinTool;
		this.captchaSettings = captchaSettings;
		this.nerdsPrefillUrl = enrollmentFormDto.getNerdsPrefillUrl();
		this.comercialProducts = enrollmentFormDto.getComercialProducts();
		this.paymentvalidationrange = enrollmentFormDto.getPaymentvalidationrange();
		this.ntenrollmentaccounts = enrollmentFormDto.getNtenrollmentaccounts();
		this.wordListDictionary = enrollmentFormDto.getWordListDictionary();
		this.ageRestriction = enrollmentFormDto.getAgeRestriction();
		this.invalidEmailDomains = enrollmentFormDto.getInvalidEmailDomains();
		this.setPropertyEditorRegistrar(propertyEditorRegistrar);
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		EnrollmentBean enrollmentBean = (EnrollmentBean) super.formBackingObject(request);

		String type = ServletRequestUtils.getStringParameter(request, "agentType", "web");
		if ("android".equalsIgnoreCase(type) || "ios".equalsIgnoreCase(type)) {
			enrollmentBean.setMobileAppType(type);
			enrollmentBean.setMobileApp(Boolean.TRUE);

		} else {
			String userAgent = request.getHeader("user-agent");
			if (userAgent.toLowerCase().indexOf("android") > -1)
				enrollmentBean.setMobileAppType("android");
		}

		// Solicitud Unica
		String externalId = request.getParameter(UNICA_URL_PARAM_NAME);
		if (!GenericValidator.isBlankOrNull(externalId) && REQUEST_POST_METHOD.equalsIgnoreCase(request.getMethod())) {
			UnicaEnrollPrefillResponse response = customerService.getEnrollmentPrefillInfo(externalId);

			if (response != null && "00".equals(response.getDPStatus().getStatusCode())
					&& ResponseStatus.SUCCESS.equals(response.getResponseStatus())
					&& response.getPrefillValues() != null) {
				enrollmentBean.setFirstname(response.getPrefillValues().getFirstName());

				String lastName = response.getPrefillValues().getLastName();
				if (!GenericValidator.isBlankOrNull(lastName)) {
					lastName = lastName.trim();
					String[] lastNames = lastName.split(" ");
					enrollmentBean.setLastname(lastNames[0]);
					if (lastNames.length > 1 && !GenericValidator.isBlankOrNull(lastNames[1]))
						enrollmentBean.setMaiden(lastNames[1]);
				}

				enrollmentBean.setEmail(response.getPrefillValues().getEmailAddress());

				String birthdate = response.getPrefillValues().getBirthDate();
				if (!GenericValidator.isBlankOrNull(birthdate) && birthdate.length() > 10) {
					birthdate = birthdate.substring(0, 10);
					if (birthdate.split("-").length == 3) {
						try {
							int day = Integer.parseInt(birthdate.split("-")[2]);
							int month = Integer.parseInt(birthdate.split("-")[1]) - 1;
							int year = Integer.parseInt(birthdate.split("-")[0]);
							enrollmentBean.setDobday(day);
							enrollmentBean.setDobmonth(month);
							enrollmentBean.setDobyear(year);
						} catch (NumberFormatException e) {
							logger.error("Solicitud Unica enrollment prefill: Error converting birthdate to correct format");
						}
					}
				}
			}
		}

		return enrollmentBean;
	}

	// This overrides the default validator and uses a multiple page aware
	// validator.
	@Override
	protected void validatePage(Object command, Errors errors, int page, boolean finish) {

		new EnrollmentFormPageValidator(page,userAgentType).validate(command, errors);

	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		EnrollmentSession eS = (EnrollmentSession) WebUtils.getSessionAttribute(request, "enrollmentSession");
		Map<String, Object> model = new HashMap<>();
		EnrollmentBean e = (EnrollmentBean) command;

		EnrollmentInfo ei = new EnrollmentInfo();
		ei.setPermId(e.getPermId());
		ei.setTaxId(eS.getValidatedTaxId());
		ei.setCreationDate(new Date());
		ei.setActivationDate(new Date());
		ei.setBirthDate(eS.getSubmitedDob().getTime());
		ei.setEmail(e.getEmail());
		ei.setFirstName(WordUtils.capitalizeFully(e.getFirstname()));
		ei.setLastName(WordUtils.capitalizeFully(e.getLastname()));
		ei.setMaidenName(WordUtils.capitalizeFully(e.getMaiden()));
		ei.setUsername(e.getUsername().toLowerCase());
		
		String[] ntAccountsArray = getNtenrollmentaccounts().split(",");
		@SuppressWarnings("unchecked")
		List<String> ntAccounts = Arrays.asList(ntAccountsArray);		
		//Send Non Transac Account as parameters.
		ei.setNonTransacAccounts(ntAccounts);
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA31))
		{			
			if(ntAccounts.contains(e.getAccountInfoType()))
			{
				ei.setTransactionalCust(false);	
			}else{
				ei.setTransactionalCust(true);	
			}	
		}else
		{
			ei.setTransactionalCust(true);	
		}

		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL))
			ei.setBsmartTerms(e.getBsmartAgreement().equalsIgnoreCase("C"));
		else
			ei.setBsmartTerms(false);
		
		
		int genereatedRandomSalt = PasswordHashUtil.genereateRandomSalt();
		ei.setPasswordHash(PasswordHashUtil.generateSaltedHash(e.getPassword(), genereatedRandomSalt));
		ei.setPasswordSalt(genereatedRandomSalt);
		if (eS.isFailed() || eS.getDobCounter() >= 3 || eS.getActCounter() >= 3 || eS.getActInfCounter() >= 3
				|| eS.getExpdateCounter() >= 3 || eS.getZipCounter() >= 3) {

			ei.setAccountTypeFail(e.getAccountInfoType());
			if (eS.getDobCounter() >= 3)
				ei.setDobFail(eS.getSubmitedDob().getTime());
			if (eS.getExpdateCounter() >= 3)
				ei.setExpDateFail("\nEXP DATE:" + e.getExpMonth() + "/" + e.getExpYear());
			if (eS.getZipCounter() >= 3)
				ei.setZipCodeFail("AMEX".equalsIgnoreCase(e.getAccountInfoType()) ? e.getZipCodeamex() : e.getZipCode());
			else if (eS.getActCounter() >= 3 || eS.getActInfCounter() >= 3) {
			
				/**
				 * This switch statement has been enhanced 
				 * In addition, no flag has been included since this
				 * adds performance and provides cleaner and readable code. 
				 * Made by Michael Ortiz : ET56883 - Topic Talked in Tech Talk
				 */
				
				switch (e.getAccountInfoType()) {
				case "CCA":
					ei.setAccountNumberFail(e.getAccountnumbercca());
					break;
				case "AMEX":
					ei.setAccountNumberFail(e.getAccountnumberamex());
					break;
				case "IDA":
					ei.setAthNumberFail(e.getAthnumber());
					break;
				case "PFC":
					ei.setAthNumberFail(e.getAthnumber());
					break;
				case "MLA":
					ei.setAccountNumberFail(e.getAccountnumber());
					break;
				case "RVA|LEA" :
					ei.setAccountNumberFail(e.getAccountNumberAuto());
					break;
				default:
					ei.setAccountNumberFail(e.getAccountnumber());
					break;
				}

			}

			ei.setInitialCustomerStatus(CustomerStatus.PENDING_CSR_ACTIVATION);
		} else {
			ei.setInitialCustomerStatus(CustomerStatus.ENROLLED);
		}
		ei.setEnrollmentSource(CustomerEnrollmentSource.WEB);
		ei.setInitialAuthenticationMethod(CustomerAuthMethod.PASSWD_RSA);
		ei.setEnrollmentStatus(EnrollmentStatus.COMPLETE);
		ei.setEnrollmentAccountNumber(eS.getValidatedAcct());
		ei.setEnrollmentAccountSubType(eS.getValidatedAcctType());
		ei.setEnrollmentAccountProductId(eS.getValidatedAcctProductId());
		

		if ("IDA".equalsIgnoreCase(e.getAccountInfoType()))
			ei.setEnrollmentPlasticNumber(e.getAthnumber());
		else
			ei.setEnrollmentPlasticNumber(eS.getValidatedAcct());

		List<CustomerFlagType> flags = new LinkedList<CustomerFlagType>();
		if (eS.getDobCounter() >= 3)
			flags.add(CustomerFlagType.VALIDATE_DOB);
		if (eS.getActCounter() >= 3 || eS.getActInfCounter() >= 3 || eS.getExpdateCounter() >= 3
				|| eS.getZipCounter() >= 3)
			flags.add(CustomerFlagType.MUST_VALIDATE_ENROLLACCOUNT);
		// Set ESIGN ENFORCEMENT FLAG
		flags.add(CustomerFlagType.ESIGN_ENFORCEMENT);
		ei.setCustomerFlagTypes(flags);
		ei.setEnrollmentReason(e.getMotivated());

		boolean saveDevice = false;
		// This keep the user computer only if all enrollment information was
		// successfully validated
		if ("private".equals(e.getComputerType()) && ei.getInitialCustomerStatus().equals(CustomerStatus.ENROLLED)) {
			saveDevice = true;
			e.getDeviceRequest().setDevicePrint(e.getDevicePrint());
		}

		EnrollmentInformationRequest enrollReq = new EnrollmentInformationRequest(null, false, ei,
				EnrollmentType.ACTIVE, e.getDeviceRequest(), e.getQuestion1(), e.getQuestion2(), e.getQuestion3(),
				e.getAnswer1(), e.getAnswer2(), e.getAnswer3(), saveDevice, true);
		enrollReq.setRsaLang(null);
		enrollReq.setSaveDevice(saveDevice);

		PreferredLanguage lang;
		if ("en".equals(Utils.getRSALanguage(request)))
			lang = PreferredLanguage.ENGLISH;
		else
			lang = PreferredLanguage.SPANISH;

		CustomerProfile profile = customerService.enrollUserwithRSA(enrollReq, lang);
		if (profile.hasFlag(CustomerFlagType.ESIGN_ENFORCEMENT)) {
			CustomerInteractionEvent cie = new CustomerInteractionEvent(request.getRemoteAddr(), request.getSession()
					.getId(), profile.getUsername(), profile.getProfileId(), Utils.getServerHostname());
			customerService.logEsignFlagAdded(cie);
		}

		// This set rsa cookie at user
		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setPath("/");
		cookie.setValue(e.getDeviceRequest().getDeviceTokenCookie());

		ESAPI.httpUtilities().addCookie(response, cookie);
		
		//as a new enrollment the analytics value sent is based
		//on this attribute
		WebUtils.setSessionAttribute(request, "newMbopId", true);

		if (eS.getRemoteToken() != null && !eS.getRemoteToken().equals("")
				&& profile.getStatus() == CustomerStatus.ENROLLED) {
			String url = customerService.tokenGetAuthorizedReturnUrl(profile, eS.getRemoteToken());
			if (url != null && !url.equals("")) {
				// DCI
				ModifyCustomerRequest modifyCustomerRequest = new ModifyCustomerRequest(
						Utils.generateCustomerInteractionEvent(request), profile.getProfileId(),
						CustomerFlagType.DCI_QUESTIONS_REQUIRED);
				CustomerFacade.getInstance().addCustomerFlag(new UserSession(customerService, profile, null), modifyCustomerRequest);
				DCICustomer dciCustomer = new DCICustomer(profile, Calendar.getInstance().getTime(), null, null, null,
						null, null);
				customerService.addDCICustomer(dciCustomer, Utils.generateCustomerInteractionEvent(request));

				return new ModelAndView("redirect:" + url);
			}
		} else if (profile.getStatus() == CustomerStatus.ENROLLED) {

			// If the enrollment is successful, perform an automatic
			// "behind-the-scenes" login
			DeviceRequest deviceRequest = e.getDeviceRequest();
			// CustomerInteractionEvent interactionEvent =
			// Utils.generateCustomerInteractionEvent(request);
			
			List<ASK_FOR> list = new LinkedList<ASK_FOR>();
			boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);
			
			UserSession userSession = null;
			if(oobFlag){
				//			add listOOB to UserSession
				List<ASK_FOR_OOB> listOOB = new LinkedList<ASK_FOR_OOB>();
				userSession = new UserSession(customerService, profile, list, listOOB);
			}else{
				userSession = new UserSession(customerService, profile, list);
			}
			userSession.setCustomerProfile(profile);
			userSession.setRemoteToken("");
			userSession.setDeviceRequest(deviceRequest);
			userSession.setUsername(profile.getUsername());
			list.add(ASK_FOR.SESSION_INIT);
			userSession.setTasksList(list);
			userSession.setLanguage(Utils.getRSALanguage(request));

			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA242))
			{
				
				if(!ntAccounts.contains(e.getAccountInfoType()))
				{
					userSession.getGuidedSetupSession().setGuidedSetupMode(true);	
					userSession.getGuidedSetupSession().setIndividualguidedSetupCampaignId(GuidedSetupInfo.ID_GUIDED_SETUP);
				}

			} else
				{	

					/*
					 * mpicado, cibp-2218, se pone esta variable para el caso de todos
					 * los clientes nuevos, para poder entrar en el modo guided setup
					 */
					userSession.getGuidedSetupSession().setGuidedSetupMode(true);
				
				/*
				 * CIBP-2780 Arturo Lizano Vargas Set the guided setup id
				 */
				userSession.getGuidedSetupSession().setIndividualguidedSetupCampaignId(GuidedSetupInfo.ID_GUIDED_SETUP);
				}
			// Set up the newUserForAutomaticPFMEntitlementCreation variable to
			// true only for new customers
			userSession.setNewUserForAutomaticPFMEntitlementCreation(true);

			WebUtils.setSessionAttribute(request, "userSession", userSession);
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)){
				WebUtils.setSessionAttribute(request, "fromEnrollment", true);
			}
			saveCampaignStatus(userSession, ViewCampaign.VER_LUEGO, GuidedSetupInfo.ID_GUIDED_SETUP);
			
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)){
				ModifyAllCustomerEmailsRequest macer = new ModifyAllCustomerEmailsRequest(
						Utils.generateCustomerInteractionEvent(request), profile, 
						customerService, null, null, 
						null, null, 
						profile.getEmail()); 
				CustomerUtils.modifyCISEmail(macer);
			}

			return new ModelAndView("redirect:welcome", model);
		}

		
		if (ei.getInitialCustomerStatus() == CustomerStatus.PENDING_CSR_ACTIVATION) {
			model.put("incomplete", true);
			model.put("mobileApp", e.isMobileApp());
			model.put("mobileAppType", e.getMobileAppType());
		} else {
			model.put("confirm", true);
		}
	
		return new ModelAndView("enrollment", model);
	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		return new ModelAndView(cancelView);
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		EnrollmentSession enrollmentSession = (EnrollmentSession) WebUtils.getSessionAttribute(request,
				"enrollmentSession");
		EnrollmentBean enrollmentBean = (EnrollmentBean) command;
        userAgentType = enrollmentSession.getUserAgentType();
		String firstname = "";
		String lastname = "";
		String email = "";
		if (page == 0) {
			
			boolean error = false;
			firstname = enrollmentBean.getFirstname();
			lastname = enrollmentBean.getLastname();
			email = enrollmentBean.getEmail();
			String maiden = enrollmentBean.getMaiden();
			String emailconfirm = enrollmentBean.getEmailconfirm();
			String taxId1 = enrollmentBean.getTaxId1();
			String taxId2 = enrollmentBean.getTaxId2();
			String taxId3 = enrollmentBean.getTaxId3();
			Integer dobyear = enrollmentBean.getDobyear();
			Integer dobday = enrollmentBean.getDobday();
			Integer dobmonth = enrollmentBean.getDobmonth();

			if (GenericValidator.isBlankOrNull(firstname)) {
				errors.rejectValue("firstname", "enrollment.field.required", "Required");
				error = true;
			} else if (!CibpValidationUtils.isValidEnrollmentFirstName(firstname)
					|| !GenericValidator.maxLength(firstname, 40)) {
				errors.rejectValue("firstname", "enrollment.firstname.invalid", "Invalid Firstname");
				error = true;
			}

			if (GenericValidator.isBlankOrNull(lastname)) {
				errors.rejectValue("lastname", "enrollment.field.required", "Required");
				error = true;
			} else if (!CibpValidationUtils.isValidEnrollmentLastName(lastname)
					|| !GenericValidator.maxLength(lastname, 40)) {
				errors.rejectValue("lastname", "enrollment.lastname.invalid", "Invalid Lastname");
				error = true;
			}

			if (!GenericValidator.isBlankOrNull(maiden)
					&& (!CibpValidationUtils.isValidEnrollmentMaidentName(maiden) || !GenericValidator.maxLength(
							maiden, 40))) {
				errors.rejectValue("maiden", "enrollment.maternal.invalid", "Invalid Mother's maiden name");
				error = true;
			}
			
			// MBSFE-691
			String[] domain = email.split("@");
			// END MBSFE-691
			
			if (GenericValidator.isBlankOrNull(email)) {
				errors.rejectValue("email", "enrollment.field.required", "Required");
				error = true;
			} else if (!GenericValidator.isEmail(email)) {
				errors.rejectValue("email", "enrollment.email.invalid", "Invalid Email");
				error = true;
			} else if((!email.equalsIgnoreCase(emailconfirm) && StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE13))
					||(!email.equals(emailconfirm) && !StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE13))){
				errors.rejectValue("emailconfirm", "enrollment.emailconfirm.notmatch", "Email values must match");
				error = true;
			
			// MBSFE-691
			} else if (Utils.haveType(this.invalidEmailDomains, domain[1])
					&& StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE691)) {
				errors.rejectValue("email", "email.invaliddomain",
						"Your e-mail address is incorrect. Please try again.");
				error = true;
			}
			if (GenericValidator.isBlankOrNull(taxId1) || GenericValidator.isBlankOrNull(taxId2)
					|| GenericValidator.isBlankOrNull(taxId3)) {
				errors.rejectValue("taxId3", "enrollment.field.required", "Required");
				error = true;
			} else if (!GenericValidator.matchRegexp(taxId1, "\\d{3}")
					|| !GenericValidator.matchRegexp(taxId2, "\\d{2}")
					|| !GenericValidator.matchRegexp(taxId3, "\\d{4}")) {
				errors.rejectValue("taxId3", "enrollment.taxId.invalid", "Invalid SSN");
				error = true;
			}

			if (dobyear == null || !GenericValidator.matchRegexp(String.valueOf(dobyear), "\\d{4}")) {
				errors.rejectValue("dobyear", "enrollment.dobyear.invalid", "Please enter a valid year");
				error = true;
			}

			if (dobday == null || dobday.equals(0) || !GenericValidator.matchRegexp(String.valueOf(dobday), "\\d{1,2}")) {
				errors.rejectValue("dobday", "enrollment.dobday.invalid", "Please enter a valid day");
				error = true;
			}

			if (dobmonth == null || !GenericValidator.matchRegexp(String.valueOf(dobmonth), "\\d{1,2}")) {
				errors.rejectValue("dobmonth", "enrollment.dobmonth.invalid", "Please enter a valid month");
				error = true;
			}
			

			CaptchaSession captchaSession = (CaptchaSession) WebUtils.getSessionAttribute(request, "captchaSession");

			// We dispose of the Captcha. A new one must always be used.
			WebUtils.setSessionAttribute(request, "captchaSession", null);

			if (!captchaSession.isCaptchaValid(enrollmentBean.getCaptchaAnswer())) {
				errors.rejectValue("captchaAnswer", "enrollment.captcha.invalid", "Please verify input");
				error = true;
			}

			GregorianCalendar dob = null;
			if (!error) {
				// Is the client 13 or older?
				// CIBP-2881
				int year = dobyear;
				int month = dobmonth;
				int day = dobday;
				dob = new GregorianCalendar(year, month, day);
				GregorianCalendar now = new GregorianCalendar();
				int age = now.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
				if (dob.get(Calendar.MONTH) > now.get(Calendar.MONTH)
						|| (dob.get(Calendar.MONTH) == now.get(Calendar.MONTH) && dob.get(Calendar.DAY_OF_MONTH) > now
								.get(Calendar.DAY_OF_MONTH)))
					age--;

				if (age < this.ageRestriction) {
					errors.rejectValue("birthday", "enrollment.dob.underage",
							"Dear customer, at this moment we cannot grant access to the Mi Banco Online "
							+ "platform since our online services are not directed to minors under the age of thirteen (13) "
							+ "as established in the terms and conditions of this service. We apologize for any inconvenience.");
					error = true;
				}
			}

			if (!error) {

				String taxId = taxId1 + taxId2 + taxId3;

				PersonalInformationValidationRequest personalRequest = new PersonalInformationValidationRequest(null,
						taxId, dob, enrollmentSession.getDobCounter() > 2 ? true : false, false);
				personalRequest.setValidateMultipleProfiles(true);
				
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE179) 
						&& enrollmentSession.getRemoteToken() != null && !enrollmentSession.getRemoteToken().equals("")){
					personalRequest.setOaoCodes(true);
				}
				
				PersonalInformationValidationResponse customerEnrollment = customerService
						.isEnrollableCustomer(personalRequest);
				
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE179) 
						&& enrollmentSession.getRemoteToken() != null && !enrollmentSession.getRemoteToken().equals("")){
					enrollmentSession.setEaccountMessage(customerEnrollment.getEaccountMessage());
				}

				EnrollmentValidationStatus status = customerEnrollment.getStatus();
				enrollmentSession.setEnrollExistingAccounts(customerEnrollment.getExistingAccounts());

				// end enrollment. Customer exists on the database or counter
				// reached blocked count limit
				if (status.equals(PersonalInformationValidationResponse.EnrollmentValidationStatus.ALREADY_ENROLL)) {
					errors.reject("enrollment.error.alreadyenrolled", "Already enrolled, please login");
					hasDblProfile(request, taxId);
				} else if (status
						.equals(PersonalInformationValidationResponse.EnrollmentValidationStatus.CUSTOMER_NOT_EXIST)) {
					errors.rejectValue("dobyear", "enrollment.dob.invalid", "Please verify input");
					errors.rejectValue("taxId3", "enrollment.taxId1.customernotexist", "Please retry your ssn");
					enrollmentSession.addCounterBlock();
				} else if (status
						.equals(PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE_DOB_INVALID)) {
					errors.rejectValue("dobyear", "enrollment.dob.invalid", "Please verify input");
					enrollmentSession.setDobCounter(enrollmentSession.getDobCounter() + 1);
				} else if (status.equals(PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE)
						|| status
								.equals(PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE_DOB_BLOCKED)) {
					enrollmentSession.setAccounts(customerEnrollment.getEnrollableAccounts());
					enrollmentSession.setSubmitedDob(dob);
					enrollmentSession.setValidatedTaxId(taxId);
					
					verifyDbleProfileStepOne(request, taxId, enrollmentSession);

					if (status.equals(PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE))
						enrollmentSession.setDobCounter(0);
					if (status
							.equals(PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE_DOB_BLOCKED))
						enrollmentSession.setDobCounter(3);
				}
				
				if (customerEnrollment.getCustomerPermId() != null) {
					enrollmentBean.setPermId(customerEnrollment.getCustomerPermId());
				}
			}

		}

		else if (page == 1) {

			boolean error = false;
			String accountnumber = "";
			String taxId = enrollmentSession.getValidatedTaxId();
			String athnumber = enrollmentBean.getAthnumber();
			String pin = "";
			String cvv = enrollmentBean.getCvv();
			String expMonth = enrollmentBean.getExpMonth();
			String expYear = enrollmentBean.getExpYear();
			String zipCode = enrollmentBean.getZipCode();
			ValidateAccountStatusResponse validateAccountInfo = null;
			String accountType = enrollmentBean.getAccountInfoType();
			enrollmentBean.setAccountInfoType(accountType);
			// Non Transactional
			Integer accountOpenYear  = null;
			BigDecimal monthlyPayment = null;
			String accountnumberFieldName = "accountnumber";
			
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA490)) {
				switch (accountType) {
				case "CCA":
					accountnumberFieldName += "cca";
					break;
				case "AMEX":
					accountnumberFieldName += "amex";
					break;
				case "RVA|LEA":
					accountnumberFieldName = "accountNumberAuto";
					break;
				}
			}
			else { 
				accountnumberFieldName = "accountnumber" + ("cca".equalsIgnoreCase(accountType) ? "cca" : ("amex".equalsIgnoreCase(accountType) ? "amex" : ""));
			}

			/*
			 * if(enrollmentBean.getEnrollExistingAccounts()!=null &&
			 * enrollmentBean.getEnrollExistingAccounts().size()>0){ for
			 * (Iterator<EnrollableAccount> iterator =
			 * enrollmentBean.getEnrollExistingAccounts().iterator();
			 * iterator.hasNext();) { EnrollableAccount name =
			 * (EnrollableAccount) iterator.next();
			 * if(name.getAccountId().trim().equalsIgnoreCase(accountnumber)) {
			 * errors.reject("enrollment.error.alreadyenrolled",
			 * "Already enrolled, please login"); error=true; return; } } }
			 */

			if ("IDA".equalsIgnoreCase(accountType)) {
				accountnumber = enrollmentBean.getAccountnumber();
				pin = enrollmentBean.getPin();
				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{9}")) {
					errors.rejectValue("accountnumber", "enrollment.accountnumber.invalid", "Invalid account Number");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(athnumber) || !GenericValidator.matchRegexp(athnumber, "\\d{16}")) {
					errors.rejectValue("athnumber", "enrollment.athnumber.invalid", "Invalid ATH Number");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(pin) || !GenericValidator.matchRegexp(pin, "\\d{4}")) {
					errors.rejectValue("pin", "enrollment.pin.invalid", "Invalid PIN");
					error = true;
				}
				if (!error) {
					String encryptedPin = pinTool.encrypt(pin);
					DepositAccountInformationValidationRequest depReq = new DepositAccountInformationValidationRequest(
							null, accountnumber, athnumber, taxId, encryptedPin, true, true);
					validateAccountInfo = customerService.validateDepositAccountInfo(depReq);
				}

			} else if ("CCA".equalsIgnoreCase(accountType)) {
				accountnumber = enrollmentBean.getAccountnumbercca();
				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{16}")) {
					errors.rejectValue("accountnumbercca", "enrollment.accountnumber.invalid", "Invalid account Number");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(cvv) || !GenericValidator.matchRegexp(cvv, "\\d{3}")) {
					errors.rejectValue("cvv", "enrollment.cvv.invalid", "Invalid CVV code");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(zipCode) || !GenericValidator.matchRegexp(zipCode, "\\d{5}")) {
					errors.rejectValue("zipCode", "enrollment.zipCode.invalid", "Invalid zip code");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(expMonth) || expMonth.equals("0")) {
					errors.rejectValue("expMonth", "enrollment.expMonth.invalid", "Invalid exp month");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(expYear) || !GenericValidator.matchRegexp(expYear, "\\d{4}")) {
					errors.rejectValue("expYear", "enrollment.expYear.invalid", "Invalid exp year");
					error = true;
				}
				if (!error) {
					CardCVVAccountInformationValidationRequest cvvReq = new CardCVVAccountInformationValidationRequest(
							null, accountnumber, taxId, cvv, expMonth, expYear, zipCode, true);
					validateAccountInfo = customerService.validateCrediCardCVVAccountInfo(cvvReq);
				}

			} else if ("AMEX".equalsIgnoreCase(accountType)) {
				accountnumber = enrollmentBean.getAccountnumberamex();
				pin = enrollmentBean.getPinamex();
				expMonth = enrollmentBean.getExpMonthamex();
				expYear = enrollmentBean.getExpYearamex();
				zipCode = enrollmentBean.getZipCodeamex();
				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{15}")) {
					errors.rejectValue("accountnumberamex", "enrollment.accountnumber.invalid",
							"Invalid account Number");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(pin) || !GenericValidator.matchRegexp(pin, "\\d{4}")) {
					errors.rejectValue("pinamex", "enrollment.pin.invalid", "Invalid PIN code");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(zipCode) || !GenericValidator.matchRegexp(zipCode, "\\d{5}")) {
					errors.rejectValue("zipCodeamex", "enrollment.zipCode.invalid", "Invalid zip code");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(expMonth) || expMonth.equals("0")) {
					errors.rejectValue("expMonthamex", "enrollment.expMonth.invalid", "Invalid exp month");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(expYear) || !GenericValidator.matchRegexp(expYear, "\\d{4}")) {
					errors.rejectValue("expYearamex", "enrollment.expYear.invalid", "Invalid exp year");
					error = true;
				}
				if (!error) {
					// Since this is AMEX, add a '0' to the front of the card
					// number
					accountnumber = "0" + accountnumber;
					String encryptedPin = pinTool.encrypt(pin);
					CardPINAccountInformationValidationRequest cardPinReq = new CardPINAccountInformationValidationRequest(
							null, accountnumber, taxId, encryptedPin, expMonth, expYear, zipCode, true, true, true);
					validateAccountInfo = customerService.validateCrediCardPINAccountInfo(cardPinReq);
				}

			} else if ("PFC".equalsIgnoreCase(accountType)) {
				accountnumber = enrollmentBean.getAccountnumber();
				pin = enrollmentBean.getPin();
				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{16}")) {
					errors.rejectValue("accountnumber", "enrollment.accountnumber.invalid", "Invalid account Number");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(pin) || !GenericValidator.matchRegexp(pin, "\\d{4}")) {
					errors.rejectValue("pin", "enrollment.pin.invalid", "Invalid PIN code");
					error = true;
				}
				if (!error) {
					String encryptedPin = pinTool.encrypt(pin);
					PayrollCardAccountInformationValidationRequest payrollReq = new PayrollCardAccountInformationValidationRequest(
							null, accountnumber, taxId, encryptedPin, true);
					validateAccountInfo = customerService.validatePayrollCardAccountInfo(payrollReq);
				}
			} 
			else if ("MLA".equalsIgnoreCase(accountType)) {
				//Alexis Gonzalez
				//MBCA-31
				//validate form element with MLA account type
				
				//Regex to validate number or number with comma only
				String valRegExp = "(\\$)?([1-9]{1}[0-9]{0,2})(\\,\\d{3})*$|^(\\$)?([1-9]{1}[0-9]{0,2})(\\d{3})*$";
				
				accountnumber = enrollmentBean.getAccountnumber();
				accountOpenYear = enrollmentBean.getAccountopenyear();
				monthlyPayment = enrollmentBean.getMonthlypayment();
					
				if (GenericValidator.isBlankOrNull(accountnumber) || !GenericValidator.matchRegexp(accountnumber, "\\d{10}"))  {
					errors.rejectValue("accountnumber", "enrollment.accountnumber.invalid", "Invalid account Number.");
					error = true;
				}
				if((accountOpenYear == null || !accountOpenYear.toString().matches("\\d{4}")) && !error) {
					//La historia pide que solo aparesca el error en accountnumber
					errors.rejectValue("accountnumber", "enrollment.accountnumber.invalid", "Invalid original amount.");
					error = true;
				} 
				if((monthlyPayment == null || !monthlyPayment.toPlainString().matches(valRegExp)) && !error) {
					//La historia pide que solo aparesca el error en accountnumber
						errors.rejectValue("accountnumber", "enrollment.accountnumber.invalid", "Invalid original amount.");
						error = true;
				}

				if (!error) {	
					NonTransAccountInformationValidationRequest ntReq = new NonTransAccountInformationValidationRequest(
							null, accountnumber,taxId,true, accountOpenYear, monthlyPayment,paymentvalidationrange, true,accountType);
					validateAccountInfo = customerService.validateNonTransAccountInfo(ntReq);
				}
			}
			
			else if ("RVA".equalsIgnoreCase(accountType) || "LEA".equalsIgnoreCase(accountType) || "RVA|LEA".equalsIgnoreCase(accountType)) {
				/**
				 * MBCA-409 Auto Enrollment 
				 * Made by Michael Ortiz
				 */
				
				
				String valRegExp = "(\\$)?([1-9]{1}[0-9]{0,2})(\\,\\d{3})*$|^(\\$)?([1-9]{1}[0-9]{0,2})(\\d{3})*$";
				accountnumber = enrollmentBean.getAccountNumberAuto();
				accountOpenYear = enrollmentBean.getAccountOpenYearAuto();
				monthlyPayment = enrollmentBean.getMonthlyPaymentAuto();
				accountType = "RVA"; // Provide default value (Since RVA or LEA uses same form)**
				
				if (GenericValidator.isBlankOrNull(accountnumber) || !GenericValidator.matchRegexp(accountnumber, "\\d{17}"))  {
					errors.rejectValue("accountNumberAuto", "enrollment.accountnumber.invalid", "Invalid account Number.");
					error = true;
				}
				if((accountOpenYear == null || !accountOpenYear.toString().matches("\\d{4}")) && !error) {
					errors.rejectValue("accountNumberAuto", "enrollment.accountnumber.invalid", "Invalid original amount.");
					error = true;
				} 
				if((monthlyPayment == null || !monthlyPayment.toPlainString().matches(valRegExp)) && !error) {
					errors.rejectValue("accountNumberAuto", "enrollment.accountnumber.invalid", "Invalid original amount.");
					error = true;
				}
				
				// ** If the entered account exists on the enrollable account list, change the account subtype.
				for (int i = 0; i < enrollmentSession.getAccounts().size(); i++) {
					if (enrollmentSession.getAccounts().get(i).getAccountId().equals(accountnumber)) {
						accountType = enrollmentSession.getAccounts().get(i).getAccountType();
						enrollmentBean.setAccountInfoType(accountType);
						break;
					}
				}

				if (!error) {
					NonTransAccountInformationValidationRequest ntReq = new NonTransAccountInformationValidationRequest(
							null, accountnumber,taxId,true, accountOpenYear, monthlyPayment,paymentvalidationrange, true, accountType);
					validateAccountInfo = customerService.validateNonTransAccountInfo(ntReq);
				}
			
			}
			else {
				
				errors.rejectValue("accountnumber", "", "");
				enrollmentBean.setAccountInfoType(enrollmentSession.getAccounts().get(0).getAccountType());
			}
	

			if (validateAccountInfo == null || validateAccountInfo.getStatus() == null)
				error = true;

			if (!error)  {
				boolean cmBsmartEnroll = false;
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC458) 
						&& validateRetailAccountExist(taxId, accountnumber, enrollmentSession)) {
					dbleProfileAlertAndSendEmail(request, taxId);
				}
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC458)) {
					final String bsmartEnrollVar = "comBsmartEnrollment";//
					boolean commercialBsmartEnrollment = false;//
					if(enrollmentSession.getEnrollExistingAccounts() != null) {
					commercialBsmartEnrollment = isBsmartCommercialEnrollment(accountnumber, enrollmentSession);//
					}
					WebUtils.setSessionAttribute(request, bsmartEnrollVar, commercialBsmartEnrollment);
					cmBsmartEnroll = commercialBsmartEnrollment;
				}
								
				// ValidateInfo status... validation...
				if (validateAccountInfo.getStatus() == AccountValidationStatus.VALID) {
					enrollmentSession.setValidatedAcct(accountnumber);
					enrollmentSession.setValidatedAcctType(accountType);
					enrollmentSession.setValidatedAcctProductId(validateAccountInfo.getAccountProductId());
					enrollmentBean.setPermId(validateAccountInfo.getPermId());

					String prodId = validateAccountInfo.getAccount().getRecords().getProductId();
					
					if (enrollmentBean.isHasmultipleProfiles() && validateAccountInfo.getAccount() != null) {
						Account validateAccount = validateAccountInfo.getAccount();
						Owner ow = null;
						String owner = "";
						
						if (validateAccount.getRecords() != null) {
							AccountRecord recd = validateAccount.getRecords();
							if (recd.getOwners() != null && recd.getOwners().length == 1) {
								ow = recd.getOwners()[0];
								if (ow.getRelationshipType() != null)
									owner = ow.getOwnershipType();
							}
						}
						if (!owner.equalsIgnoreCase("PRIMARY"))// 001 PRIMARY
						{
							// Multiples profiles no primary owner to the
							// selected account
							if (enrollmentSession.getActInfCounter() >= 3)
								return;
							enrollmentSession.addActInfCounter();
							errors.rejectValue(accountnumberFieldName, "enrollment.accountnumber.invalid",
									"Invalid Account");
							return;
						}
					}
					
					if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE179) 
							&& enrollmentSession.getRemoteToken() != null && !enrollmentSession.getRemoteToken().equals("")){
						if ((!GenericValidator.isBlankOrNull(prodId)) && (!GenericValidator.isBlankOrNull(this.comercialProducts))
								&& ((this.comercialProducts.indexOf(prodId) != -1))) {
							enrollmentSession.setEaccountMessage(EaccountMessage.MESSAGE_2);
						}
					}
					
					if(cmBsmartEnroll && StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC458))
						redirectBsmartDisclaimer(request, errors, enrollmentSession.getUserAgentType());


					return;
				} else if (validateAccountInfo.getStatus() == AccountValidationStatus.ACCOUNT_BLOCKED) {
					// Se comento para evitar el mantener el anoying blocked
					// status
					// enrollmentSession.setBlocked(true);
					// enrollmentSession.setActCounter(3);

					// Se pone un blocked en el command en vez de en la sesion
					// por pedido de Camille,
					// para que no se tenga que cerrar el browser
					errors.rejectValue("accountnumber", "", "");
					enrollmentBean.setBlocked(true);
					return;
				}
				// MBCA-781 All enrollmentSession.add*Counter() calls where moved above the if validation to ensure only three 
				// attempts are allowed and prevent bug when the user entered the correct information on the fourth attempt.
				else if (validateAccountInfo.getStatus() == AccountValidationStatus.ATH_NOT_MATCH
						|| validateAccountInfo.getStatus() == AccountValidationStatus.ACCOUNT_INVALID
						|| validateAccountInfo.getStatus() == AccountValidationStatus.INVALID) {

					enrollmentSession.addActInfCounter();
					if (enrollmentSession.getActInfCounter() >= 3)
						return;
					
					errors.rejectValue(accountnumberFieldName, "enrollment.accountnumber.invalid", "Invalid Account");
					return;

				} else if (validateAccountInfo.getStatus() == AccountValidationStatus.INVALID_EXPDATE) {
					enrollmentSession.addExpdateCounter();					
					if (enrollmentSession.getExpdateCounter() >= 3)
						return;
					errors.rejectValue(accountnumberFieldName, "enrollment.accountnumber.invalid", "Invalid Account");
					return;

				} else if (validateAccountInfo.getStatus() == AccountValidationStatus.INVALID_ZIPCODE) {
					enrollmentSession.addZipCounter();					
					if (enrollmentSession.getZipCounter() >= 3)
						return;
					errors.rejectValue(accountnumberFieldName, "enrollment.accountnumber.invalid", "Invalid Account");
					return;

				} else {
					enrollmentSession.addActCounter();					
					if (enrollmentSession.getActCounter() >= 3)
						return;
					errors.rejectValue(accountnumberFieldName, "enrollment.accountnumber.invalid", "Invalid Account");
					return;
				}
			}
		}

		else if (page == 2) {
			final boolean blnBcomWasDisplayed = true;//
			final String blnVar = "bcomWasDisplayed";//
			
			WebUtils.setSessionAttribute(request, blnVar, blnBcomWasDisplayed);
			
			availableUsername = true;
			boolean error = false;
			String username = enrollmentBean.getUsername();
			String password = enrollmentBean.getPassword();
			String passwordconfirm = enrollmentBean.getPasswordconfirm();
			
			// String rePwd = "^[A-Za-z\\d]+$";
			// String rePwd =
			// "^(\\d+[a-zA-Z]+|[a-zA-Z]+\\d+)+(\\d+[a-zA-Z]*|[a-zA-Z]+\\d*)*$";
			// String rePwd = "^[A-Za-z\\d\\.\\-@\\$%&\\*\\?]+$";
			// String rePwd =
			// "(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,15})$";

			if (!(!GenericValidator.isBlankOrNull(username)
			// && GenericValidator.maxLength(username, 20) //CIBP-1947 - se
			// realiza una validacion aparte
			// && GenericValidator.minLength(username, 6)
			&& CibpValidationUtils.isValidUsername(username))) {

				errors.rejectValue("username", "enrollment.username.invalid", "Invalid Username");
				error = true;
				availableUsername = false; // CIBP-1947
			}// CIBP-1947 - valida y presenta mensaje de advertencia si se
				// escoge un username menor de 6 caracteres
			else if (!(GenericValidator.maxLength(username, 20) && GenericValidator.minLength(username, 6))) {
				errors.rejectValue("username", "enrollment.username.lengthInvalid", "Invalid Username");
				error = true;
				availableUsername = false; // CIBP-1947
			} else if (!customerService.isUsernameAvailable(username.trim())) { // CIBP-1947
				availableUsername = false;
				errors.rejectValue("username", "enrollment.username.noavailability", new Object[] { username },
						"Invalid");
				error = true;
			}

			if (!enrollmentBean.isCheckAvaliability()) {

				if (GenericValidator.isBlankOrNull(password)) {
					errors.rejectValue("password", "changepassword.password.required", "Required");
					error = true;
				} else if (GenericValidator.isBlankOrNull(passwordconfirm)) {
					errors.rejectValue("passwordconfirm", "changepassword.confirmpassword.required", "Required");
					error = true;
				} else if (!password.equals(passwordconfirm)) {
					errors.rejectValue("passwordconfirm", "changepassword.confirmpassword.mustmatch", "Fields must match");
					error = true;
				} else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989)
						&& password.toLowerCase().contains(username.toLowerCase())) {
					errors.rejectValue(PASSWORD_ERROR, ENROLLMENT_PASSWORD_CONTAINS_USERNAME, PASSWORD_CONTAINS_USERNAME);
					error = true;
				}
				CustomerProfile profile = new CustomerProfile();
				profile.setFirstName(firstname);
				profile.setLastName(lastname);
				profile.setUsername(username);
				Map<String, String> paramsPass = new HashMap<String, String>();
				paramsPass.put(ChallengeInformationUtils.NEW_PASS, password);
				paramsPass.put(ChallengeInformationUtils.WORD_LIST, wordListDictionary);
				paramsPass.put(ChallengeInformationUtils.LANGUAGE, Utils.getRSALanguage(request));
				PasswordConstraintValidator passwordConstraintValidator = new PasswordConstraintValidator();
				Map<String, String> paramsResult = passwordConstraintValidator.validateRules(paramsPass,profile, Boolean.FALSE);
				if (paramsResult.get("validate").equals("false")) {
					errors.rejectValue("password", paramsResult.get("message"), paramsResult.get("message"));
					error = true;
				}
			}

			if (!error && !enrollmentBean.isCheckAvaliability()) {
				
				DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(enrollmentBean.getPm_fp(), null, request);
				enrollmentBean.setDeviceRequest(deviceRequest);

				ConnectionInfoValidationRequest conectionRequest = new ConnectionInfoValidationRequest(null, username,
						enrollmentSession.getValidatedTaxId(), deviceRequest, Utils.getRSALanguage(request), true);
				ConnectionInfoValidationResponse validateConnectionInformation = customerService
						.validateConnectionInformation(conectionRequest);
				if (validateConnectionInformation.isUserNameValid()) {
					enrollmentBean.setDeviceRequest(deviceRequest);
					enrollmentBean.setQuestions1(validateConnectionInformation.getQuestions1());
					enrollmentBean.setQuestions2(validateConnectionInformation.getQuestions2());
					enrollmentBean.setQuestions3(validateConnectionInformation.getQuestions3());
				} else {
					availableUsername = false; // CIBP-1947
					errors.rejectValue("username", "enrollment.username.noavailability", new Object[] { username },
							"Invalid");
				}
			}

		}

		else if (page == 3) {
			// Validator takes care of this.
		}

		else if (page == 4) {
			if((enrollmentSession.getValidatedAcctProductId() == null 
					|| !enrollmentSession.getValidatedAcctProductId().equalsIgnoreCase("086"))){
				((EnrollmentBean) command).setBsmartAgreement("NA");
			}
			
		}
		
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		GlobalEntitlement enrollentEntitlement = customerService.getGlobalEntitlement(EntitlementType.ENROLLMENT);
		EnrollmentBean bean = (EnrollmentBean) command;
		EnrollmentSession enrollmentSession = (EnrollmentSession) WebUtils.getSessionAttribute(request,
				"enrollmentSession");
		
		WebUtils.setSessionAttribute(request, Utils.DEEP_LINK, null);
		
		if (page == 0) {
			if (enrollmentSession == null)
				enrollmentSession = new EnrollmentSession();
			else {
				EnrollmentSession oldSession = enrollmentSession;
				enrollmentSession = new EnrollmentSession();
				enrollmentSession.setCounterBlock(oldSession.getCounterBlock());
				enrollmentSession.setBlocked(oldSession.isBlocked());

				if (StringUtils.isEmpty(bean.getFirstname()) && StringUtils.isEmpty(bean.getLastname()))
					enrollmentSession.setDobCounter(0);
				else
					enrollmentSession.setDobCounter(oldSession.getDobCounter());

				WebUtils.setSessionAttribute(request, "enrollmentSession", enrollmentSession);
			}
		} else {
			enrollmentSession = (EnrollmentSession) WebUtils.getSessionAttribute(request, "enrollmentSession");
			if (enrollmentSession == null) {
				errors.reject("", "Enrollment Session is null.  Please retry enrollment");
				model.put("mobileApp", bean.isMobileApp());
				model.put("mobileAppType", bean.getMobileAppType());
				return model;
			}
		}

		if (enrollentEntitlement == null || enrollentEntitlement.getStatus().equals(EntitlementStatus.DISABLED)) {
			model.put("enrollmentAvailable", false);
			model.put("mobileApp", bean.isMobileApp());
			model.put("mobileAppType", bean.getMobileAppType());
			return model;
		} else {
			model.put("enrollmentAvailable", true);
		}
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE179) && enrollmentSession != null 
				&& enrollmentSession.getEaccountMessage() != null && !enrollmentSession.getEaccountMessage().equals(EaccountMessage.NO_MESSAGE)){
			model.put("eaccountMessage", enrollmentSession.getEaccountMessage());
			model.put("noOpenOAOCode", "true");
			model.put("page", Integer.valueOf(0));
			return model;
		}

		CaptchaSession captchaSession = new CaptchaSession(captchaSettings);
		WebUtils.setSessionAttribute(request, "captchaSession", captchaSession);
		EnrollmentBean enrollmentBean = (EnrollmentBean) command;

		String rtoken = (String) request.getParameter("rtoken");
		rtoken = InternationalCharacterUtils.sanatizeInputs(rtoken, false);

		if (page == 2) {
			String accountnumber = enrollmentBean.getAccountnumber();

			// Se añadio una validacion para los nuevos account numbers que
			// existen. IDA,CCA,AMEX.
			if ("AMEX".equalsIgnoreCase(enrollmentBean.getAccountInfoType())) {
				accountnumber = enrollmentBean.getAccountnumberamex();
				if (accountnumber.length() == 15 && accountnumber.startsWith("377")) {
					accountnumber = "0" + accountnumber;
					enrollmentBean.setAccountnumber(accountnumber);
				}
			} else if ("CCA".equalsIgnoreCase(enrollmentBean.getAccountInfoType())) {
				accountnumber = enrollmentBean.getAccountnumbercca();
			} else if("RVA|LEA".equalsIgnoreCase(enrollmentBean.getAccountInfoType()) || "RVA".equalsIgnoreCase(enrollmentBean.getAccountInfoType()) || "LEA".equalsIgnoreCase(enrollmentBean.getAccountInfoType())) {
				accountnumber = enrollmentBean.getAccountNumberAuto();
			}

			// Is this user already enrolled?
			if (enrollmentSession.getEnrollExistingAccounts() != null
					&& enrollmentSession.getEnrollExistingAccounts().size() > 0)
				for (Iterator<EnrollableAccount> iterator = enrollmentSession.getEnrollExistingAccounts().iterator(); iterator
						.hasNext();) {
					EnrollableAccount name = (EnrollableAccount) iterator.next();
					if (name.getAccountId().trim().equalsIgnoreCase(accountnumber)) {
						model.put("alreadyEnrolled", true);
					}
				}

			// CIBP-1947/validacion de username disponibles en inscripcion
			// (lguzman)
			model.put("validAvailableUsername", false);
			if (!GenericValidator.isBlankOrNull(bean.getUsername())
					&& GenericValidator.maxLength(bean.getUsername(), 20)
					&& GenericValidator.minLength(bean.getUsername(), 6) // CIBP-1947
																			// -
																			// se
																			// realiza
																			// una
																			// validacion
																			// aparte
					&& CibpValidationUtils.isValidUsername(bean.getUsername())) {
				if (!availableUsername) { // Valida en onBindAndValidate
											// CIBP-1947-lguzman
					int auxLenghtUsername = bean.getUsername().trim().length();
					int tamNumInUsername = 0;

					// Identifica cuantos valores numericos hay a la derecha del
					// username
					for (int i = 0; i < bean.getUsername().trim().length(); i++) {
						if (bean.getUsername().trim().substring(auxLenghtUsername - 1, auxLenghtUsername)
								.matches("[0-9]*")) {
							tamNumInUsername++;
							auxLenghtUsername--;
						} else {
							break;
						}
					}
					// crea las 4 opciones de username
					auxLenghtUsername = bean.getUsername().trim().length();
					int numInUsername = 0;
					if (tamNumInUsername != 0) // si encontro numero en la parte
												// derecha del username lo
												// extrae
						numInUsername = Integer.parseInt(bean.getUsername().trim()
								.substring(auxLenghtUsername - tamNumInUsername, auxLenghtUsername));
					String usernameWithOutNum = bean.getUsername().trim()
							.substring(0, auxLenghtUsername - tamNumInUsername);
					Map<Object, String> usernamesDispo = new LinkedHashMap<Object, String>();
					
					numInUsername++;// siguiente valor a probar
					for(int opciones = 0; opciones < 4; opciones++) {
						// valida que no exista en la base de datos
						// CUSTOMER_PROFILE
						final String zero = "0";
				        String tempuser = usernameWithOutNum + numInUsername;
				        if (tempuser.length() <  bean.getUsername().trim().length()){
				        	tempuser = usernameWithOutNum;
						    for (int i = 0; i < bean.getUsername().trim().length()-tempuser.length(); i++) {
						    	tempuser += zero;
					        }
					        tempuser += numInUsername;
				        }
						if (customerService.isUsernameAvailable(tempuser)) {
							usernamesDispo.put(tempuser, tempuser);
							
						}
						numInUsername++;
					}

					model.put("availableUsername", false);
					model.put("validAvailableUsername", true);
					model.put("usernames", usernamesDispo);
				} else {
					model.put("availableUsername", true);
					model.put("validAvailableUsername", true);
				}
			}

		}

		if (!GenericValidator.isBlankOrNull(rtoken)) {
			model.put("remoteToken", rtoken);
			enrollmentSession.setRemoteToken(rtoken);
		}
		// La cuenta esta blocked en boveda.
		if (enrollmentBean.isBlocked())
			model.put("blocked", true);

		WebUtils.setSessionAttribute(request, "enrollmentSession", enrollmentSession);
		
		
		model.put("page", Integer.toString(page));
		Map<String, String> agree = new HashMap<String, String>();
		// if("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
		agree.put("agree", "");
		model.put("agree", agree);
		

		// months
		Map<Object, String> months = new LinkedHashMap<Object, String>();
		SimpleDateFormat df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.MONTH, 0);
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			months.put("", "Mes");
		else
			months.put("", "Month");

		for (int i = 0; i <= 11; i++) {
			String month = df.format(cal.getTime());
			months.put(i, month);
			cal.add(Calendar.MONTH, 1);
		}
		model.put("months", months);

		// days
		Map<Object, String> days = new LinkedHashMap<Object, String>();
		df = new SimpleDateFormat("d");
		cal.set(Calendar.DAY_OF_MONTH, 1);
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			days.put(0, "D&iacute;a");
		else
			days.put(0, "Day");

		for (int i = 1; i <= 31; i++) {
			String day = df.format(cal.getTime());
			days.put(i, day);
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		model.put("days", days);

		// years
		Map<Object, String> years = new LinkedHashMap<Object, String>();
		df = new SimpleDateFormat("yyyy");
		cal = GregorianCalendar.getInstance();
		cal.add(Calendar.YEAR, -this.ageRestriction);
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			years.put(0, "A&ntilde;o");
		else
			years.put(0, "Year");

		for (int i = 0; i <= 100; i++) {
			String year = df.format(cal.getTime());
			years.put(Integer.parseInt(year), year);
			cal.add(Calendar.YEAR, -1);
		}
		model.put("years", years);

		Map<Object, String> motives = new LinkedHashMap<Object, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
			motives.put(EnrollmentReason.OTHER, "Selecione una opci&oacute;n");
			motives.put(EnrollmentReason.FRIEND, "Familiar / Amigo(a)");
			motives.put(EnrollmentReason.BRANCH_OFFICE, "Oficial Bancario");
			motives.put(EnrollmentReason.TELEPHONE_CONSULTANT, "Consultor Telef&oacute;nico");
			motives.put(EnrollmentReason.MAIL, "Correo");
			motives.put(EnrollmentReason.NEWSPAPER, "Peri&oacute;dico");
			motives.put(EnrollmentReason.RADIO, "Radio");
			motives.put(EnrollmentReason.TELEVISION, "Televisi&oacute;n");
			motives.put(EnrollmentReason.EMAIL, "E-mail");
			motives.put(EnrollmentReason.POPULAR_COM, "www.popular.com");
			motives.put(EnrollmentReason.INTERNET_AD, "Anuncio de Internet");
			motives.put(EnrollmentReason.SEARCH_ENGINE, "B&uacute;squeda en l&iacute;nea");
			motives.put(EnrollmentReason.OTHER, "Otro");
		} else {
			motives.put(EnrollmentReason.OTHER, "Select an option");
			motives.put(EnrollmentReason.FRIEND, "Relative / Friend");
			motives.put(EnrollmentReason.BRANCH_OFFICE, "Branch Officer");
			motives.put(EnrollmentReason.TELEPHONE_CONSULTANT, "Telephone Consultant");
			motives.put(EnrollmentReason.MAIL, "Mail");
			motives.put(EnrollmentReason.NEWSPAPER, "Newspaper");
			motives.put(EnrollmentReason.RADIO, "Radio");
			motives.put(EnrollmentReason.TELEVISION, "Television");
			motives.put(EnrollmentReason.EMAIL, "E-mail");
			motives.put(EnrollmentReason.POPULAR_COM, "www.popular.com");
			motives.put(EnrollmentReason.INTERNET_AD, "Internet ad");
			motives.put(EnrollmentReason.SEARCH_ENGINE, "Search-engine");
			motives.put(EnrollmentReason.OTHER, "Other");
		}
		model.put("motives", motives);

		// expYears
		Map<Object, String> expYears = new LinkedHashMap<Object, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			expYears.put(0, "A&ntilde;o:");
		else
			expYears.put(0, "Year:");
		df = new SimpleDateFormat("yyyy");
		cal = GregorianCalendar.getInstance();
		for (int i = 1; i <= 9; i++) {
			String year = df.format(cal.getTime());
			expYears.put(Integer.parseInt(year), year);
			cal.add(Calendar.YEAR, 1);
		}
		model.put("expYears", expYears);

		// expMonths
		Map<Object, String> expMonths = new LinkedHashMap<Object, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			expMonths.put("", "Mes:");
		else
			expMonths.put("", "Month:");
		df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
		cal = GregorianCalendar.getInstance();
		cal.set(Calendar.MONTH, 0);
		for (int i = 1; i <= 12; i++) {
			String month = df.format(cal.getTime());
			expMonths.put(i, month);
			cal.add(Calendar.MONTH, 1);

		}
		model.put("expMonths", expMonths);

		String accountInfoType = enrollmentBean.getAccountInfoType();
		boolean hasIDA = false;
		boolean hasCCA = false;
		boolean hasAMEX = false;
		boolean hasPFC = false;
		boolean hasREDI_HELOCS = false;
		boolean hasMLA = false;
		boolean hasAuto = false;

		if (page == 1 && GenericValidator.isBlankOrNull(accountInfoType)) {
			
			List<EnrollableAccount> accounts = enrollmentSession.getAccounts();
			for (EnrollableAccount enrollableAccount : accounts) {
				String accountType = enrollableAccount.getAccountType();
				
				switch (accountType) {
					case "IDA": {
						model.put("accountIDA", true);
						hasIDA = true;
						break;
					}
					case "CCA": {
						model.put("accountCCA", true);
						hasCCA = true;
						if ((enrollableAccount.getProductId() != null)
								&& (enrollableAccount.getProductId().equalsIgnoreCase("EVHLC") 
										|| enrollableAccount.getProductId().equalsIgnoreCase("EVHLP"))) {
							model.put("accountREDI_HELOCS", true);
							hasREDI_HELOCS = true;
						} else {
							model.put("CCAwithoutREDI_HELOCS", true);
						}
						break;
					}
					case "AMEX": {
						model.put("accountAMEX", true);
						hasAMEX = true;
						break;
					}
					case "PFC": {
						model.put("accountPFC", true);
						hasPFC = true;
					}
						break;
					case "MLA": {
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA31)) {
							model.put("accountMLA", true);
							hasMLA = true;
						}
						break;
					}
					case "RVA": {
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA490)) {
							model.put("accountAuto", true);
							hasAuto = true;
						}
						break;
					}
					case "LEA": {
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA490)) {
							model.put("accountAuto", true);
							hasAuto = true;
						}
						break;
					}
					default:
						break;
				}
			}
			
			if (hasIDA)
				enrollmentBean.setAccountInfoType("IDA");
			else if (hasCCA) {
				enrollmentBean.setAccountInfoType("CCA");
				if (hasREDI_HELOCS)
					enrollmentBean.setProductDescription("Redi Equity");
			} else if (hasAMEX)
				enrollmentBean.setAccountInfoType("AMEX");
			else if (hasPFC)
				enrollmentBean.setAccountInfoType("PFC");
			else if(hasMLA)
				enrollmentBean.setAccountInfoType("MLA");
			else if(hasAuto) 
				enrollmentBean.setAccountInfoType("RVA|LEA");

		} else if (page == 1 && !GenericValidator.isBlankOrNull(accountInfoType)) {
			List<EnrollableAccount> accounts = enrollmentSession.getAccounts();
			for (EnrollableAccount enrollableAccount : accounts) {
				String accountType = enrollableAccount.getAccountType();
				
				switch (accountType) {
					case "IDA":
						model.put("accountIDA", true);
						break;
					case "CCA": {
						model.put("accountCCA", true);
						if ((enrollableAccount.getProductId() != null)
								&& (enrollableAccount.getProductId().equalsIgnoreCase("EVHLC") 
										|| enrollableAccount.getProductId().equalsIgnoreCase("EVHLP"))) {
							model.put("accountREDI_HELOCS", true);
						} else {
							model.put("CCAwithoutREDI_HELOCS", true);
						}
						break;
					}
					case "AMEX": 
						model.put("accountAMEX", true);
						break;
					case "PFC":
						model.put("accountPFC", true);
						break;
					case "MLA": {
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA31)) 
							model.put("accountMLA", true);
						break;
					}
					case "RVA":
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA490)) 
							model.put("accountAuto", true);
						break;
					case "LEA":
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA490)) 
							model.put("accountAuto", true);
						break;
					default:
						break;
				}
				
				if ((enrollableAccount.getProductId() != null)
						&& (enrollableAccount.getProductId().equalsIgnoreCase("EVHLC") || enrollableAccount
								.getProductId().equalsIgnoreCase("EVHLP"))) {
					model.put("accountREDI_HELOCS", true);
					hasREDI_HELOCS = true;
				}
			}
		}

		// Questions
		Map<String, String> questions = new LinkedHashMap<String, String>();
		String selectquestiontext = "Select a question:";
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			selectquestiontext = "Seleccione una pregunta:";
		
		questions.put("", selectquestiontext);
		model.put("questionsLabel", selectquestiontext);

		if (page >= 3 && page < 5) {
			Map<Object, Object> q = new HashMap<Object, Object>();
			if (!bean.isMobileApp()) {
				q.put("", selectquestiontext);
			}
			q.putAll(enrollmentBean.getQuestions1());
			model.put("questions1", q);
			
			Map<Object, Object> q2 = new HashMap<Object, Object>();
			if (!bean.isMobileApp()) {
				q2.put("", selectquestiontext);
			}
			q2.putAll(enrollmentBean.getQuestions2());
			questions = new LinkedHashMap<String, String>();
			questions.put("", selectquestiontext);
			model.put("questions2", q2);

			Map<Object, Object> q3 = new HashMap<Object, Object>();
			if (!bean.isMobileApp()) {
				q3.put("", selectquestiontext);
			}
			q3.putAll(enrollmentBean.getQuestions3());
			questions = new LinkedHashMap<String, String>();
			questions.put("", selectquestiontext);
			model.put("questions3", q3);

		} else {
			model.put("questions1", questions);
			model.put("questions2", questions);
			model.put("questions3", questions);
		}
		
		//MBSFE-303 
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE303)) {
			List<FieldError> fieldErrors = new ArrayList<>();

			for (int i = 0; i < errors.getErrorCount(); i++) {
				fieldErrors.add((FieldError) errors.getFieldErrors().get(i));
			}

			int errorCount = errors.getErrorCount();

			if (page == 0) {

				if (errorCount == 1) {
					FieldError fieldError = (FieldError) errors.getAllErrors().get(0);
					if (fieldError.getField().equals("dobmonth") || fieldError.getField().equals("dobday")
							|| fieldError.getField().equals("dobyear")) {
						model.put("errorsTips", "date");
					} else {
						FieldError field = (FieldError) errors.getAllErrors().get(0);
						model.put("errorsTips", field.getField());
					}
				} else if (errorCount > 1) {
					for (FieldError fieldError : fieldErrors) {
						if (fieldError.getField().equals("dobmonth") || fieldError.getField().equals("dobday")
								|| fieldError.getField().equals("dobyear")) {
							errorCount--;
						}
					}
					if (errorCount > 0) {
						model.put("errorsTips", "none");
					} else {
						model.put("errorsTips", "date");
					}
				}

			} else if (page == 1 && bean.getAccountInfoType() != null) {
					
				switch (bean.getAccountInfoType()) {

				case "CCA":
					if (errorCount == 1) {
						FieldError fieldError = (FieldError) errors.getAllErrors().get(0);
						if (fieldError.getField().equals("expMonth") || fieldError.getField().equals("expYear")) {
							model.put("errorsTips", "expDateCCA");
						} else {
							FieldError field = (FieldError) errors.getAllErrors().get(0);
							model.put("errorsTips", field.getField());
						}
					} else if (errorCount > 1) {
						for (FieldError fieldError : fieldErrors) {
							if (fieldError.getField().equals("expMonth") || fieldError.getField().equals("expYear")) {
								errorCount--;
							}
						}
						if (errorCount > 0) {
							model.put("errorsTips", "none");
						} else {
							model.put("errorsTips", "expDateCCA");
						}
					}
					break;
				case "AMEX":
					if (errorCount == 1) {
						FieldError fieldError = (FieldError) errors.getAllErrors().get(0);
						if (fieldError.getField().equals("expMonthamex")
								|| fieldError.getField().equals("expYearamex")) {
							model.put("errorsTips", "expDateAmex");
						} else {
							model.put("errorsTips", fieldError.getField());
						}
					} else if (errorCount > 1) {
						for (FieldError fieldError : fieldErrors) {
							if (fieldError.getField().equals("expMonthamex")
									|| fieldError.getField().equals("expYearamex")) {
								errorCount--;
							}
						}
						if (errorCount > 0) {
							model.put("errorsTips", "none");
						} else {
							model.put("errorsTips", "expDateAmex");
						}
					}
					break;
				default:
					if (errorCount == 1) {
						FieldError fieldError = (FieldError) errors.getAllErrors().get(0);
						model.put("errorsTips", fieldError.getField());
					} else {
						model.put("errorsTips", "none");

					}
					break;
				}
			} else {
				if (errorCount == 1) {
					FieldError fieldError = (FieldError) errors.getAllErrors().get(0);
					model.put("errorsTips", fieldError.getField());
				} else {
					model.put("errorsTips", "none");

				}
			}
		}
		//MBSFE-303 end
		
		model.put("errors", errors.getAllErrors());
		model.put("MBSFE303", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE303));
		model.put("epayrollEnhFlag", StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLLENHA));
		model.put("isEvendorActivated", StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR));
		model.put("openaccounturl", nerdsPrefillUrl);
		model.put("mobileApp", bean.isMobileApp());
		model.put("mobileAppType", bean.getMobileAppType());
		//Enrollment with MLA Account Flag
		model.put("enrollmentWithMLAFlag", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA31));
		model.put("isAutoEnrollAllowed", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA490));
		model.put("MBSFE1590", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1590));
		return model;
	}

	private void saveCampaignStatus(UserSession userSession, Integer statusCampaign, String campaignId) {
		CustomerServices customerService = userSession.getCustomerService();
		Calendar instance = Calendar.getInstance();
		ViewCampaign vc = new ViewCampaign();
		ViewCampaignId id = new ViewCampaignId();
		vc.setCount(1);
		id.setIdCampaign(campaignId);

		if (userSession.getCustomerProfile().getProfileId() == null)
			id.setProfileId(userSession.getCustomerProfile().getUsername());
		else
			id.setProfileId(userSession.getCustomerProfile().getProfileId());

		vc.setId(id);
		vc.setStatus(statusCampaign);
		vc.setType(CampaignType.GUIDED_SETUP);
		vc.setViewDate(instance.getTime());
		try {
			ViewCampaign vcExist = customerService.getViewCampaign(userSession.getCustomerProfile(), vc.getId()
					.getIdCampaign());
			if (vcExist == null) {
				if (statusCampaign.equals(ViewCampaign.VER_LUEGO))
					vc.setCount(0);
				customerService.saveViewCampaign(vc);
			}

		} catch (Exception e) {
			System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
		}
	}
	
	/** This method validate if retail profile exist with ssn entered
	 * @param strSsn
	 * @param strAccountNumber
	 * @param customerEnrollment
	 * @return boolean
	 */
	protected boolean validateRetailAccountExist (String strSsn, String strAccountNumber, EnrollmentSession customerEnrollment) {		
		final CustomerProfileService profileService = customerService.getCustomerProfileService();//
		final CustomerSearchFilter filter = new CustomerSearchFilter();//
		final List<EnrollableAccount> accounts = customerEnrollment.getAccounts();//
		boolean dbleProfileExist = false;//
		filter.setSsn(strSsn);		
		final MultipleCustomerProfileResponse customerProfileResponse = profileService.getCustomerProfileByFilter(filter);//
		
		if(customerProfileResponse.getStatus() != CustomerProfileStatus.NOT_FOUND ) {
		dbleProfileExist = isDblProfile(customerProfileResponse, strAccountNumber, accounts);
		}
		return dbleProfileExist;
	}
	
	/** 
	 * @param customerProfileResponse
	 * @param strAccountNumber
	 * @param accounts
	 * @return boolean
	 */
	private boolean isDblProfile(MultipleCustomerProfileResponse customerProfileResponse, String strAccountNumber, List<EnrollableAccount> accounts) {
		boolean dbleProfileExist = false;//
		final String strFlexiCuenta = "021";//
		final String strBsmart = "086";//
		boolean hasComercialAccount = false;//
		final List<CustomerProfile> profiles = customerProfileResponse.getCustomerProfiles();//
		
		if (GenericValidator.isBlankOrNull(strAccountNumber)) {
			
		hasComercialAccount = hasCommercialEnrollableAcct(accounts, strFlexiCuenta, strBsmart, profiles);				
						
		for(CustomerProfile cProfile : profiles) {						
			if(cProfile.getUserInterface().name().equals(ProfileType.Type.RETAIL.toString()) && !hasComercialAccount) {
				dbleProfileExist = true;
				}
			}
		} else if (accountIsEnrolled(strAccountNumber, profiles)) { 
				dbleProfileExist = true;
		}else if (!isNotRetailAccount(strAccountNumber, accounts, profiles)) { 
				dbleProfileExist = true;
		}			
		return dbleProfileExist; 
	}
	
	/** This method validate if client can enroll commercial account
	 * @param accounts
	 * @param strFlexiCuenta
	 * @param strBsmart
	 * @param profiles
	 * @return boolean
	 */
	private boolean hasCommercialEnrollableAcct (List<EnrollableAccount> accounts
			, String strFlexiCuenta, String strBsmart, List<CustomerProfile> profiles) {
		boolean commercialAccount = false;//
		
		for(EnrollableAccount cAccount : accounts) {
			
			if((cAccount.getProductId().equals(strFlexiCuenta) 
					|| cAccount.getProductId().equals(strBsmart)) 
					&& !accountIsEnrolled(cAccount.getAccountId(), profiles)) {
				commercialAccount = true;				
			}
					
		}
		
		return commercialAccount;
	}
	
	/** This method verify if the account is not retail
	 * @param strAccountNumber
	 * @param accounts
	 * @param profiles
	 * @return boolean
	 */
	private boolean isNotRetailAccount(String strAccountNumber, List<EnrollableAccount>  accounts, List<CustomerProfile> profiles) {
		final String bSmart = "086";//
		final String flexiCuenta = "021";//
		boolean isNotRetail = true;//	
		
		if(hasRetailProfileEnrolled(profiles)) {
			for(EnrollableAccount eAccount : accounts) {			
				if(!eAccount.getProductId().equals(bSmart) 
					&& !eAccount.getProductId().equals(flexiCuenta) 
					&& eAccount.getAccountId().equals(strAccountNumber)) {
				isNotRetail = false;
				}				
			}
		}
				
		return isNotRetail;
	}
	
	
	/** This method verify if the account is enrolled
	 * @param strAccount
	 * @param profiles
	 * @return boolean
	 */
	private boolean accountIsEnrolled(String strAccount, List<CustomerProfile> profiles) {
		boolean accountEnrolled = false;//
		
		for(CustomerProfile cProfile : profiles) {		
			if(cProfile.getEnrollmentAccountNumber().equals(strAccount)) {
				accountEnrolled = true;
			}		
		}				
		return accountEnrolled;	
	}
	
	/** This method stop the enrollment proccess and send email alert to the user
	 * @param request
	 * @param strSsn
	 * @throws ModelAndViewDefiningException
	 */
	protected void dbleProfileAlertAndSendEmail(HttpServletRequest request, String strSsn) throws ModelAndViewDefiningException {
		final String strExistingProfileView = "redirect:doubleprofileValidationScreen";//
		final Map<String, Boolean> model = new HashMap<>(1);//
		final String strUserAgent = request.getHeader("user-agent");//
		final boolean isMobileApp = !GenericValidator.isBlankOrNull(strUserAgent) &&
				strUserAgent.matches(".*MiBanco\\.app.*");//
		final CustomerProfileService profileService = customerService.getCustomerProfileService();//
		final CustomerSearchFilter filter = new CustomerSearchFilter();//
		filter.setSsn(strSsn);
		final MultipleCustomerProfileResponse customerProfileResponse = profileService.getCustomerProfileByFilter(filter);//
		final String userAgentMobileVar = "userAgentMobileApp";//
		
		sendEmailDbleProfileAlert(request, customerProfileResponse);
		
		model.put(userAgentMobileVar, isMobileApp);
		throw new ModelAndViewDefiningException(new ModelAndView(strExistingProfileView, model));

	}
	

	/**
	 * @param request
	 * @param response
	 * 
	 */
	protected void sendEmailDbleProfileAlert(HttpServletRequest request, MultipleCustomerProfileResponse response) {
		final String strLenguage = Utils.getRSALanguage(request);//
		final List<CustomerProfile> profiles = response.getCustomerProfiles();//
		final List<String> customerEmails = new ArrayList<>(0);//
		PreferredLanguage preferredLenguage = PreferredLanguage.SPANISH;//
		final String strEn = "en";//
		final CustomerInteractionEvent customerEvent = Utils.generateCustomerInteractionEvent(request);//
		final String labelEnroll = "enrollmentSession";//
		final EnrollmentSession uSession = EnrollmentSession.class.cast(WebUtils.getSessionAttribute(request, labelEnroll));//
		final String connectionInfo = "CONNECTION_INFO";//
		
		customerEvent.setCustomStringData(connectionInfo, Utils.generateDblPrEmailTraceInfo(request, uSession));
		
		if (!GenericValidator.isBlankOrNull(strLenguage) && strEn.equals(strLenguage)) { 
			preferredLenguage = PreferredLanguage.ENGLISH;
		}
		for(CustomerProfile cProfile : profiles) {
			logDbleProfileAttempt(cProfile, customerEvent);
			if(!emailWasSend(cProfile.getEmail(), customerEmails)) {
				customerService.sendEmailAlertDoubleProfile(cProfile, preferredLenguage);
			}
		}
	}
	
	/** 
	 * @param strEmail
	 * @param customerEmails
	 * @return boolean
	 */
	private boolean emailWasSend(String strEmail, List<String> customerEmails) {
		boolean wasSend = true;//
		
		if(!customerEmails.contains(strEmail)) {
			customerEmails.add(strEmail);
			wasSend = false;
		}
		return wasSend;
	}
	
	/**
	 * @param request
	 * @param strTaxId
	 * @param enrollmentSession
	 * @throws ModelAndViewDefiningException
	 * 
	 */
	private void verifyDbleProfileStepOne(HttpServletRequest request 
			, String strTaxId, EnrollmentSession enrollmentSession)
	throws ModelAndViewDefiningException{		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC458) 
				&& validateRetailAccountExist(strTaxId, null, enrollmentSession)) {
			dbleProfileAlertAndSendEmail(request, strTaxId);
		}
	}
	
	/**
	 * @param profiles
	 * @return boolean
	 */
	private boolean hasRetailProfileEnrolled (List<CustomerProfile> profiles) {
		boolean hasRetailProfileEnrolled = false;//
				
		for(CustomerProfile cProfile : profiles) {						
			if(cProfile.getUserInterface().name().equals(ProfileType.Type.RETAIL.toString())) {
				hasRetailProfileEnrolled = true;
				}
			}
		return hasRetailProfileEnrolled;
	}
	
	/**Send Email with doubleProfileAlert
	 * @param request
	 * @param strSsn
	 * @throws ModelAndViewDefiningException
	 * 
	 * */
	private void hasDblProfile (HttpServletRequest request, String strSsn) 
			throws  ModelAndViewDefiningException{
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC458)) {
		dbleProfileAlertAndSendEmail(request, strSsn);
		}
	}
	
	/**Verify if is a Bsmart Commercial Enroll
	 * @param strAccountNumber
	 * @param enrollmentSesssion
	 * @return boolean
	 * 
	 * */
	private boolean isBsmartCommercialEnrollment (String strAccountNumber, EnrollmentSession enrollmentSesssion){
		final String taxId = enrollmentSesssion.getValidatedTaxId();
		final String bSmart = "086";//
		boolean isBsmart = false;//
		boolean isCommercialEnroll = false;//
		final CustomerProfileService profileService = customerService.getCustomerProfileService();//
		final CustomerSearchFilter filter = new CustomerSearchFilter();//
		filter.setSsn(taxId);
		final MultipleCustomerProfileResponse customerProfileResponse = profileService.getCustomerProfileByFilter(filter);//
		
		for(EnrollableAccount eAccount : enrollmentSesssion.getAccounts()) {
			if(eAccount.getAccountId().equals(strAccountNumber) && eAccount.getProductId().equals(bSmart)) {
				isBsmart = true;
			}
		}
		
		if(hasRetailProfileEnrolled(customerProfileResponse.getCustomerProfiles())) {
			isCommercialEnroll = true;
		}
		
		final boolean isCommercialBsmart = isBsmart && isCommercialEnroll;//
		
		return isCommercialBsmart;
		
	}
	
	/** Log Double Profile Send Email Event
	 * @param cProfile
	 * @param customerEvent
	 */
	private void logDbleProfileAttempt (CustomerProfile cProfile, CustomerInteractionEvent customerEvent) {
			
		customerEvent.setUsername(cProfile.getUsername());
		customerEvent.setProfileId(cProfile.getProfileId());
		customerService.logDbleEnrollmentAttemptInteraction(customerEvent);
	}
	
	/**
	 * 
	 * @param request
	 * @param errors
	 * @throws Exception
	 */
	private void redirectBsmartDisclaimer(HttpServletRequest request, BindException errors, UserAgentType userAgent) throws Exception {
		final String strUserAgent = request.getHeader("user-agent");//
		final boolean isMobile = !GenericValidator.isBlankOrNull(strUserAgent) && 
				(strUserAgent.matches(".*MiBanco\\.app.*") || !userAgent.equals(UserAgentType.WEB));		
		final int pageToShow = isMobile? 6 : 5;

		if(strUserAgent.matches(".*MiBanco\\.app.*") ||  strUserAgent.matches(".*WebView*"))
			WebUtils.setSessionAttribute(request, "userAgentMobileApp", DeviceUtils.isUserAgentMobileApp(request));
		
		throw new ModelAndViewDefiningException(showPage(request,errors, pageToShow));
	}
	
	public String getCancelView() {
		return cancelView;
	}

	public void setCancelView(String cancelView) {
		this.cancelView = cancelView;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public void setCaptchaSettings(CaptchaSettings captchaSettings) {
		this.captchaSettings = captchaSettings;
	}

	public PinEncryptionTool getPinTool() {
		return pinTool;
	}

	public void setPinTool(PinEncryptionTool pinTool) {
		this.pinTool = pinTool;
	}

	public String getNerdsPrefillUrl() {
		return nerdsPrefillUrl;
	}

	public void setNerdsPrefillUrl(String nerdsPrefillUrl) {
		this.nerdsPrefillUrl = nerdsPrefillUrl;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.ENROLLMENT;
	}

	public Integer getAgeRestriction() {
		return ageRestriction;
	}

	public void setAgeRestriction(Integer ageRestriction) {
		this.ageRestriction = ageRestriction;
	}

	public String getComercialProducts() {
		return comercialProducts;
	}

	public void setComercialProducts(String comercialProducts) {
		this.comercialProducts = comercialProducts;
	}

	public String getPaymentvalidationrange() {
		return paymentvalidationrange;
	}

	public void setPaymentvalidationrange(String paymentvalidationrange) {
		this.paymentvalidationrange = paymentvalidationrange;
	}

	public String getNtenrollmentaccounts() {
		return ntenrollmentaccounts;
	}

	public void setNtenrollmentaccounts(String ntenrollmentaccounts) {
		this.ntenrollmentaccounts = ntenrollmentaccounts;
	}

	// MBSFE-691
	public String getInvalidEmailDomains() {
		return invalidEmailDomains;
	}

	public void setInvalidEmailDomains(String invalidEmailDomains) {
		this.invalidEmailDomains = invalidEmailDomains;
	}
	// END MBSFE-691


	public String getWordListDictionary() {
		return wordListDictionary;
	}

	public void setWordListDictionary(String wordListDictionary) {
		this.wordListDictionary = wordListDictionary;
	}

	public CaptchaSettings getCaptchaSettings() {
		return captchaSettings;
	}
}
