package evertec.cibp.web.form;

import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.EventType;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.adaptiveauth.EnrollmentInformationinRSARequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.RsaQuestionsUpdateResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionSelectionResponse;
import evertec.cibp.web.form.bean.ChangeQuestionsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class ChangeQuestionsForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private boolean submitted = false;

	public ChangeQuestionsForm() {
		super();
		setCommandClass(ChangeQuestionsBean.class);
		setCommandName("changequestions");
		setFormView("changequestions");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		ChangeQuestionsBean dude = (ChangeQuestionsBean) command;

		CustomerServices customerService = userSession.getCustomerService();

		CustomerProfile customer = userSession.getCustomerProfile();
		boolean hasalert = false;

		if(!Utils.validateTokenCsrfGuard(request)) {
			throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
		}

		
		AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
		if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
			Set<AlertType> alerts = ent.getAlerts();
			if (alerts.contains(AlertType.QUESTIONS_EDIT_EMAIL) || alerts.contains(AlertType.QUESTIONS_EDIT_SMS)
					|| alerts.contains(AlertType.QUESTIONS_EDIT_PUSH)) {
				hasalert = true;
			}
		}

		EnrollmentInformationinRSARequest eirsa = new EnrollmentInformationinRSARequest(
				Utils.generateCustomerInteractionEvent(request), userSession.getUsername(),
				userSession.getCustomerProfile().getEmail(), false,userSession.getDeviceRequest());
		eirsa.setQuestionsAndAnswer(  dude.getQuestion1(),
				dude.getQuestion2(), dude.getQuestion3(), dude.getAnswer1(), dude.getAnswer2(), dude.getAnswer3());

		RsaQuestionsUpdateResponse enrollRSAresponse = customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(eirsa);

		this.submitted = true;

		if (!GenericValidator.isBlankOrNull(enrollRSAresponse.getDeviceTokenCookie())) { 
			ESAPI.httpUtilities().addCookie(response, RSAUtils.getCookie(enrollRSAresponse.getDeviceTokenCookie(), request));
		}

		if (ResultStatus.SUCCESS.equals(enrollRSAresponse.getStatus())) {
			model.put("confirm", true);
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Security Questions have been changed.");
			else
				userSession.setStatusmessage("Preguntas de seguridad han sido cambiadas.");
			if (hasalert) {
				customerService.sendQuestionsEditConfirmation(customer,
						userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
						userSession.getPreferredLanguage());
			}

		} else {
			model.put("confirm", true);
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Unable to change security questions.");
			else
				userSession.setStatusmessage("Preguntas no pudieron ser cambiadas.");
		}
		model.put("submitted", this.submitted);
		
		// MBSD-1996 RSA Upgrade Recommendations - Edit Security Questions (Update cookie)
		Cookie cookie = WebUtils.getCookie(request, "PMData") != null ? WebUtils.getCookie(request, "PMData") : new Cookie("PMData", "");
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		ESAPI.httpUtilities().addCookie(response, cookie);

		return showForm(request, response, errors, model);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		ChangeQuestionsBean dude = (ChangeQuestionsBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "question1", "changequestions.q1.required", "Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "question2", "changequestions.q2.required", "Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "question3", "changequestions.q3.required", "Required");

		String a1 = dude.getAnswer1();
		String a2 = dude.getAnswer2();
		String a3 = dude.getAnswer3();
		String c1 = dude.getConfirm1();
		String c2 = dude.getConfirm2();
		String c3 = dude.getConfirm3();

		String re = "^\\w+(\\s{1}\\w+)*$";

		if (GenericValidator.isBlankOrNull(a1) || !GenericValidator.matchRegexp(a1, re)) {
			errors.rejectValue("answer1", "changequestion.answer.notvalid", "Please enter a valid answer");
		} else if (!GenericValidator.isBlankOrNull(a1) && !a1.equals(c1)) {
			errors.rejectValue("confirm1", "changequestion.answerconfirm.notmatch", "Answers values must match");
		}

		if (GenericValidator.isBlankOrNull(a2) || !GenericValidator.matchRegexp(a2, re)) {
			errors.rejectValue("answer2", "changequestion.answer.notvalid", "Please enter a valid answer");
		} else if (!GenericValidator.isBlankOrNull(a2) && !a2.equals(c2)) {
			errors.rejectValue("confirm2", "changequestion.answerconfirm.notmatch", "Answers values must match");
		}

		if (GenericValidator.isBlankOrNull(a3) || !GenericValidator.matchRegexp(a3, re)) {
			errors.rejectValue("answer3", "changequestion.answer.notvalid", "Please enter a valid answer");
		} else if (!GenericValidator.isBlankOrNull(a3) && !a3.equals(c3)) {
			errors.rejectValue("confirm3", "changequestion.answerconfirm.notmatch", "Answers values must match");
		}

		if (errors.getAllErrors().size() == 0) {
			Boolean informationChallenge = userSession.isInformationChallenge();
			if (informationChallenge != null && !informationChallenge) {
				
				String username = userSession.getUsername();
				
				if (userSession.getInformationQuestion() == null  && !userSession.getOobEnroll()) {
					
					String language = Utils.getRSALanguage(request);
						
					DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), request);
					RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, Utils.getRSALanguage(request), userSession.getUsername(), EventType.CLIENT_DEFINED.getValue());
					AnalyzeResponse anResp = userSession.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

					deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
					deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

					userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
					userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

					userSession.setDeviceRequest(deviceRequest);

					if (!(ActionCode.CHALLENGE == anResp.getRiskResult().getTriggeredRule().getActionCode())) {
						userSession.setInformationChallenge(true);
					} else {
						String email = null;
						if (userSession.getCustomerProfile() != null)
							email = userSession.getCustomerProfile().getEmail();

						QuestionResponse personalQuestion;

						if (userSession.getInformationQuestion() == null) {
							RSAActionRequest actionRequest = new RSAActionRequest(
									Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
									userSession.getUsername(), "", email, 0, userSession.isRemember(),
									userSession.getDeviceRequest());

							if (userSession.getCustomerProfile() != null)
								actionRequest.setUsername(userSession.getCustomerProfile().getUsername());
							else if (userSession.getUsernameReservation() != null)
								actionRequest.setUsername(userSession.getUsernameReservation().getUsername());
							else
								actionRequest.setUsername(username);

							actionRequest.setDeviceRequest(userSession.getDeviceRequest());
							actionRequest.setRsaSessionId(userSession.getRsaSessionId());
							actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());

							actionRequest.setRsaLang(Utils.getRSALanguage(request));

							personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

						} else {
							personalQuestion = new QuestionResponse();
							personalQuestion.setStatusResult(ResultStatus.SUCCESS);
							personalQuestion.setQuestion(userSession.getInformationQuestion());
						}
						if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
							userSession.setInformationQuestion(personalQuestion.getQuestion());

						} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
							throw new ModelAndViewDefiningException(new ModelAndView("error"));
						} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
							throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
						}
					}
				}
				
				errors.rejectValue("", "", "");
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
				Map<String, String> parameters = new HashMap<String, String>();
				parameters.put(ChallengeInformationUtils.QUESTION_1, dude.getQuestion1());
				parameters.put(ChallengeInformationUtils.QUESTION_2, dude.getQuestion2());
				parameters.put(ChallengeInformationUtils.QUESTION_3, dude.getQuestion3());
				parameters.put(ChallengeInformationUtils.ANSWER_1, dude.getAnswer1());
				parameters.put(ChallengeInformationUtils.ANSWER_2, dude.getAnswer2());
				parameters.put(ChallengeInformationUtils.ANSWER_3, dude.getAnswer3());
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
					ChallengeInformationUtils.QUESTIONS_ACTION);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, parameters);
			}
		}
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, userSession.isInformationChallenge());

		Utils.generateTokenCsrfGuard(request);
		
		// FFEIC + SMS
		String username = userSession.getUsername();
		model.put("username", username);

		if (userSession.getInformationQuestion() != null) {
			String question = (String) userSession.getInformationQuestion().values().toArray()[0];
			model.put("question", question);
		}

		Map<String, String> questions = new LinkedHashMap<String, String>();
		String selectquestiontext = "Select a question:";
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			selectquestiontext = "Selecciona una pregunta:";
		questions.put("", selectquestiontext);

		if (!this.submitted && userSession.getInformationQuestion() == null) {
			RSAActionRequest actionrequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(request),
					userSession.getUsername(), "", userSession.getCustomerProfile().getEmail(), 0, false,
					RSAUtils.getDeviceRequest(userSession.getPm_fp(), null, request));
			actionrequest.setUsername(userSession.getUsername());
			actionrequest.setDeviceRequest(userSession.getDeviceRequest());

			actionrequest.setRsaLang("sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "ES" : "EN");

			QuestionSelectionResponse questionResponse = userSession.getCustomerService().getSignOnCustomerServiceModule().getUserRsaQuestionsSelection(actionrequest);
			
			WebUtils.setSessionAttribute(request, "analyzeResponse", questionResponse.getAnalyzeResponse());
			
			if (questionResponse.getAnalyzeResponse() != null) {
				userSession.setRsaSessionId(questionResponse.getAnalyzeResponse().getIdentificationData().getSessionId());
				userSession.setRsaTransactionId(questionResponse.getAnalyzeResponse().getIdentificationData().getTransactionId());
			}
			
			DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(), null, request);
			deviceRequest.setDeviceTokenCookie(questionResponse.getAnalyzeResponse().getDeviceResult().getDeviceData()
					.getDeviceTokenCookie());
			deviceRequest
					.setDeviceTokenFSO(questionResponse.getAnalyzeResponse().getDeviceResult().getDeviceData().getDeviceTokenCookie());
			userSession.setDeviceRequest(deviceRequest);
			
			model.put("devicetoken", deviceRequest.getDeviceTokenCookie());

			Map<Object, Object> q = new HashMap<Object, Object>(); 
			q.put("", selectquestiontext);
			q.putAll(questionResponse.getQuestionMap1());
			model.put("questions1", q);
			WebUtils.setSessionAttribute(request, "rsaQuestions1", q);

			Map<Object, Object> q2 = new HashMap<Object, Object>(); 
			q2.put("", selectquestiontext);
			q2.putAll(questionResponse.getQuestionMap2());
			questions = new LinkedHashMap<String, String>();
			questions.put("", selectquestiontext);
			model.put("questions2", q2);
			WebUtils.setSessionAttribute(request, "rsaQuestions2", q2);

			Map<Object, Object> q3 = new HashMap<Object, Object>();
			q3.put("", selectquestiontext);
			q3.putAll(questionResponse.getQuestionMap3());
			questions = new LinkedHashMap<String, String>();
			questions.put("", selectquestiontext);
			model.put("questions3", q3);
			WebUtils.setSessionAttribute(request, "rsaQuestions3", q3);
		} else {
			model.put("questions1", WebUtils.getSessionAttribute(request, "rsaQuestions1"));
			model.put("questions2", WebUtils.getSessionAttribute(request, "rsaQuestions2"));
			model.put("questions3", WebUtils.getSessionAttribute(request, "rsaQuestions3"));
		}
		
		this.submitted = false;
		model.put("submitted", this.submitted);
		return model;
	}
}
