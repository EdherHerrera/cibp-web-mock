package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.sms.beans.SMSAccount;
import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TVFrontendDepositAccount;
import evertec.cibp.core.models.aggregation.AggregatedAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.SMSEntitlement;
import evertec.cibp.core.models.pfm.PFMAccountWrapper;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.AddSMSAccountRequest;
import evertec.cibp.core.services.requests.SmsAccountNicknameModifyRequest;
import evertec.cibp.core.services.requests.SmsAccountsGetterRequest;
import evertec.cibp.core.utils.InternationalCharacterUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.CryptoUtils;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.facade.AccountsFacade;
import evertec.cibp.web.form.bean.EditAccountBean;
import evertec.cibp.web.form.bean.MortgageLoanBean;
import evertec.cibp.web.request.ModifyAccountPreferenceRequest;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.UserAccount;
import evertec.otherservices.requests.UpdateUserAccountReq;

/**
 * @author mtaborda
 *
 */
public class EditAccountForm extends CancellableFormController implements MessageSourceAware {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;
	private String[] iraProductIds;
	private String[] caeProductIds;
	private String insuranceRemoved;
	private String loggerKey;
	private String loggerInitVector;
	//MBSFE 297
	private String pcbCCANotEbillAllowed;

	public EditAccountForm() {
		super();
		setCommandClass(EditAccountBean.class);
		setCommandName("editaccount");
		setFormView("editaccount");
	}

	@SuppressWarnings("unused")
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		BankingSession bankingSession = userSession.getBankingSession();
		EditAccountBean actbean = (EditAccountBean) command;

		// TODO poner un if para cuando solo queramos actulaizar las cuentas
		CustomerProfile profile = userSession.getCustomerProfile();
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		List<TVFrontendAccount> trfAccounts = userSession.getTrfAccounts();

		if (actbean.getAction() != null && actbean.getAction() == EditAccountBean.ACTION.ADDTRF) {
			String accountnumber = "";
			String section = "C";
			String enrollact = profile.getEnrollmentAccountNumber();
			boolean isadd = customerService.addThirdPartyAccount(profile, bankingSession,
					Utils.getAccountByAccountNumber(enrollact, accounts), accountnumber, section,
					Utils.generateCustomerInteractionEvent(request));
			trfAccounts = userSession.getTrfAccounts();
			model.put("trfAccounts", trfAccounts);
		} else if (actbean.getAction() != null && actbean.getAction() == EditAccountBean.ACTION.HIDEACT) {
			boolean hide = actbean.getHide().equalsIgnoreCase("true") ? true : false;
			model.put("text", hide);
			TVFrontendAccount accountById = Utils.getAccountById(actbean.getId(), accounts);
			if (accountById == null)
				accountById = Utils.getAccountById(actbean.getId(), userSession.getTrfAccounts());

			try {
				if (accountById != null) {
					// Accounts facade implementation
					ModifyAccountPreferenceRequest modifyAccountPreferenceRequest = new ModifyAccountPreferenceRequest();
					modifyAccountPreferenceRequest.setAccount(accountById);
					modifyAccountPreferenceRequest.setCustomerProfile(profile);
					modifyAccountPreferenceRequest.setHide(hide);
					modifyAccountPreferenceRequest.setUserSession(userSession);
					
					if ((accountById.getCustomerAccountPreferences() != null) && (accountById.getCustomerAccountPreferences().getNickname() != null)) {
						modifyAccountPreferenceRequest.setNickname(accountById.getCustomerAccountPreferences().getNickname());
					} else {
						modifyAccountPreferenceRequest.setNickname(accountById.getNickname());
					}
					
					AccountsFacade.getInstance().modifyAccountVisibility(modifyAccountPreferenceRequest);
				} else
					hide = !hide;

				// Modify PFM as well
				if (customerService.getGlobalEntitlement(EntitlementType.PFM).getStatus() == EntitlementStatus.ENABLED) {
					if (profile.hasEntitlement(EntitlementType.PFM)) {

						// If we don't have the PFM user id here, the portal
						// screen is closed
						// We need to obtain the PFM id and the accounts here
						CustomerEntitlement entitlementByType = userSession.getCustomerProfile().getEntitlementByType(
								EntitlementType.PFM);

						if (entitlementByType != null) {
							if (userSession.getPfmId() == null) {

								// Get the PFM ID
								String pfmId = entitlementByType.getString("PFM_USER_ID");

								if (!(pfmId == null || pfmId.equals(""))) {
									userSession.setPfmId(entitlementByType.getString("PFM_USER_ID"));

									// Now get the accounts
									List<PFMAccountWrapper> pfmAccounts = customerService.getPfmAccounts(
											userSession.getPfmId(), userSession.getAccounts());

									// Set up the indices
									for (int i = 0; i < pfmAccounts.size(); i++)
										pfmAccounts.get(i).setIndex(i);

									userSession.setPfmAccounts(pfmAccounts);
								}
							}
						}

						if (userSession.getPfmId() != null && userSession.getPfmAccounts() != null
								&& userSession.getPfmAccounts().size() > 0) {
							String account = accountById.getAccountNumber();
							if (accountById.getSubtype().equals("IDA") || accountById.getSubtype().equals("PFC")
									|| accountById.getSubtype().equals("CCA")) {
								PFMAccountWrapper pfmAccount = new PFMAccountWrapper();
								for (Iterator<PFMAccountWrapper> iterator = userSession.getPfmAccounts().iterator(); iterator
										.hasNext();) {
									PFMAccountWrapper test = (PFMAccountWrapper) iterator.next();
									if (test.getAccountId().endsWith("CARD")) {
										if (test.getAccountId().substring(0, 16).equals(account)) {
											pfmAccount = test;
											break;
										}
									} else {
										if (test.getAccountId().substring(0, 9).equals(account)) {
											pfmAccount = test;
											// Checking, savings or reserve?
											if (accountById.getAccountSection() != null
													&& accountById.getAccountSection().equals("S"))
												pfmAccount.setSection("S");
											else if (accountById.getAccountSection() != null
													&& accountById.getAccountSection().equals("R"))
												pfmAccount.setSection("R");
											else
												pfmAccount.setSection("C");
											break;
										}
									}
								}

								// If the account exist, update its status
								if (pfmAccount != null && pfmAccount.getAccountId() != null) {
									pfmAccount.setIncludeFlag(!hide);
									List<PFMAccountWrapper> pfmAccountsToUpdate = new LinkedList<PFMAccountWrapper>();
									pfmAccountsToUpdate.add(pfmAccount);
									if (customerService.updatePFMAccounts(userSession.getPfmId(), pfmAccountsToUpdate)) {
										// Update the session values
										userSession.getPfmAccounts().get(pfmAccount.getIndex())
												.setIncludeFlag(pfmAccount.isIncludeFlag());
									}
								}
							}
						}
					}
				}
				if (accountById != null && accountById.getCustomerAccountPreferences() != null)
					accountById.getCustomerAccountPreferences().setHide(hide);
				model.put("text", hide);
				return new ModelAndView("text", model);

			} catch (Exception e) {
				if (accountById != null && accountById.getCustomerAccountPreferences() != null)
					accountById.getCustomerAccountPreferences().setHide(!hide);
				model.put("text", !hide);
				return new ModelAndView("text", model);
			}

		} else if (actbean.getAction() != null && actbean.getAction() == EditAccountBean.ACTION.MODACT) {

			TVFrontendAccount accountById = Utils.getAccountById(actbean.getId(), accounts);
			boolean isvalid = false;
			if (!GenericValidator.isBlankOrNull(actbean.getNickname())
					&& GenericValidator.maxLength(actbean.getNickname(), 30)
					&& GenericValidator.matchRegexp(actbean.getNickname(), "^[A-Za-z\\d-]+(\\s[A-Za-z\\d-]+)*$"))
				isvalid = true;

			if (isvalid) {
				// Accounts facade implementation
				ModifyAccountPreferenceRequest modifyAccountPreferenceRequest = new ModifyAccountPreferenceRequest();
				modifyAccountPreferenceRequest.setAccount(accountById);
				modifyAccountPreferenceRequest.setCustomerProfile(profile);
				modifyAccountPreferenceRequest.setHide(false);
				modifyAccountPreferenceRequest.setNickname(actbean.getNickname());
				modifyAccountPreferenceRequest.setUserSession(userSession);
				AccountsFacade.getInstance().modifyAccountPreference(modifyAccountPreferenceRequest);
				
				userSession.resetAccountPreferences();

				if (customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {

					// Are we registered in EBPP2?
					if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

						// Get the list of user accounts
						List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

						// Loop through all the accounts and match them with the
						// proper userAccount
						if (ebpp2UserAccounts != null && ebpp2UserAccounts.size() > 0) {
							accountById.setEbpp2UserAccount(Utils.getEbpp2UserAccountsByAccount(
									accountById.getAccountNumber(), userSession));

							// If we found the account, update it with the new
							// nickname
							if (accountById.getEbpp2UserAccount() != null) {
								UpdateUserAccountReq req = new UpdateUserAccountReq();
								req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
								req.setBillerAccountID(accountById.getEbpp2UserAccount().getBillerAccountID());
								req.setLabel(actbean.getNickname());
								req.setActive(accountById.getEbpp2UserAccount().isActive());
								req.setEbillNotif(accountById.getEbpp2UserAccount().isReceiveEbillNotification());
								req.setEstmNotif(accountById.getEbpp2UserAccount().isReceiveEstatementNotification());
								customerService.updateEbpp2UserAccount(req);
							}
						}
					}
				}

				model.put("text", actbean.getNickname());
			} else
				model.put("text", accountById.getNickname());

			return new ModelAndView("text", model);

		} else if (actbean.getAction() != null && actbean.getAction() == EditAccountBean.ACTION.MODTRFACT) {

			TVFrontendAccount accountById = Utils.getAccountById(actbean.getId(), userSession.getTrfAccounts());
			boolean isvalid = false;
			if (!GenericValidator.isBlankOrNull(actbean.getNickname())
					&& GenericValidator.maxLength(actbean.getNickname(), 30)
					&& GenericValidator.matchRegexp(actbean.getNickname(), "^[A-Za-z\\d-]+(\\s[A-Za-z\\d-]+)*$"))
				isvalid = true;

			if (isvalid) {
				// Accounts facade implementation
				ModifyAccountPreferenceRequest modifyAccountPreferenceRequest = new ModifyAccountPreferenceRequest();
				modifyAccountPreferenceRequest.setAccount(accountById);
				modifyAccountPreferenceRequest.setCustomerProfile(profile);
				modifyAccountPreferenceRequest.setHide(false);
				modifyAccountPreferenceRequest.setNickname(actbean.getNickname());
				modifyAccountPreferenceRequest.setUserSession(userSession);
				AccountsFacade.getInstance().modifyAccountPreference(modifyAccountPreferenceRequest);
				
				userSession.resetAccountPreferences();

				model.put("text", actbean.getNickname());
			} else
				model.put("text", accountById.getNickname());

			return new ModelAndView("text", model);

		} else if (actbean.getAction() != null && actbean.getAction() == EditAccountBean.ACTION.MODMOB) {

			TVFrontendAccount accountById = Utils.getAccountById(actbean.getId(), accounts);
			if ((accountById.getSmsAccount() != null && accountById.getSmsAccountNickname() != null)
					&& !GenericValidator.isBlankOrNull(actbean.getMobnickname())
					&& GenericValidator.maxLength(actbean.getMobnickname(), 35)
					&& GenericValidator.matchRegexp(actbean.getMobnickname(), "^[A-Za-z\\d-]+(\\s[A-Za-z\\d-]+)*$")) {
				// CIBP-1857 ISSUE BANCA MOBILE BGARCIA
				SmsAccountNicknameModifyRequest smsmod = new SmsAccountNicknameModifyRequest(
						Utils.generateCustomerInteractionEvent(request), profile, new SMSEntitlement(
								profile.getEntitlementByType(EntitlementType.MOBILE_BANKING)),
						accountById.getSmsAccount(), actbean.getMobnickname());
				smsmod.setSmsProfile(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)));
				try {

					smsmod.setId(accountById.getSmsAccountNickname().getId());
					customerService.modifySMSAccountNickname(smsmod);
					SmsAccountsGetterRequest actget = new SmsAccountsGetterRequest(
							Utils.generateCustomerInteractionEvent(request), profile, new SMSEntitlement(
									profile.getEntitlementByType(EntitlementType.MOBILE_BANKING)),
							userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)), accounts);
					customerService.getSMSAccounts(actget);

					model.put("text", actbean.getMobnickname());
				} catch (RuntimeException e) {
					model.put("text", "error :(");
				}
				model.put("text", actbean.getMobnickname());
			} else {
				if (accountById.getSmsAccountNickname() == null) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						model.put("text", "Set a nickname");
					else
						model.put("text", "Asignar nombre");

				} else
					model.put("text", accountById.getSmsAccountNickname().getShortName());
				return new ModelAndView("text", model);
			}
			return new ModelAndView("text", model);
		} else if (actbean.getAction() != null && actbean.getAction() == EditAccountBean.ACTION.ADDACTMOB) {

			if (!GenericValidator.isBlankOrNull(actbean.getMobnickname())
					&& GenericValidator.maxLength(actbean.getMobnickname(), 35)
					&& GenericValidator.matchRegexp(actbean.getMobnickname(), "^[A-Za-z\\d-]+(\\s[A-Za-z\\d-]+)*$")) {

				TVFrontendAccount accountById = Utils.getAccountById(actbean.getId(), accounts);
				if (accountById.getSmsAccountNickname() == null) {
					SMSEntitlement ent = new SMSEntitlement(
							profile.getEntitlementByType(EntitlementType.MOBILE_BANKING));
					// CIBP-1857 ISSUE BANCA MOBILE BGARCIA
					AddSMSAccountRequest addsmsact = new AddSMSAccountRequest(
							Utils.generateCustomerInteractionEvent(request), profile, userSession.getSmsProfile(Utils
									.generateCustomerInteractionEvent(request)), ent, accountById.getAccountNumber(),
							accountById.getSubtype(), accountById.getAccountSection(), actbean.getMobnickname(),
							accountById.getProductDefinition().getProductId(), null);
					SMSAccount act = customerService.addSMSAccount(addsmsact);
					List<SMSAccount> l = new LinkedList<SMSAccount>();
					l.add(act);

					accountById.setSmsAccount(l);

					accountById.setSmsAccountNickname(act);

					model.put("text", actbean.getMobnickname());

					SmsAccountsGetterRequest actget = new SmsAccountsGetterRequest(
							Utils.generateCustomerInteractionEvent(request), profile, ent,
							userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)), accounts);
					customerService.getSMSAccounts(actget);
					if (accounts != null && accounts.size() > 0) {
						model.put("accounts", accounts);
					}
				} else {
					// CIBP-1857 ISSUE BANCA MOBILE BGARCIA
					SmsAccountNicknameModifyRequest smsmod1 = new SmsAccountNicknameModifyRequest(
							Utils.generateCustomerInteractionEvent(request), profile, new SMSEntitlement(
									profile.getEntitlementByType(EntitlementType.MOBILE_BANKING)),
							accountById.getSmsAccount(), actbean.getMobnickname());
					smsmod1.setId(accountById.getSmsAccountNickname().getId());
					smsmod1.setSmsProfile(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)));
					customerService.modifySMSAccountNickname(smsmod1);
					accountById.getSmsAccountNickname().setShortName(actbean.getMobnickname());
					model.put("text", actbean.getMobnickname());
					SmsAccountsGetterRequest actget = new SmsAccountsGetterRequest(
							Utils.generateCustomerInteractionEvent(request), profile, new SMSEntitlement(
									profile.getEntitlementByType(EntitlementType.MOBILE_BANKING)),
							userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)), accounts);
					customerService.getSMSAccounts(actget);
					return new ModelAndView("text", model);
				}
				return new ModelAndView("text", model);
			} else {
				TVFrontendAccount accountById = Utils.getAccountById(actbean.getId(), accounts);
				if (accountById.getSmsAccountNickname() == null) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						model.put("text", "Set a nickname");
					else
						model.put("text", "Asignar nombre");

				} else
					model.put("text", accountById.getSmsAccountNickname().getShortName());
				return new ModelAndView("text", model);
			}

		} else if (actbean.getAction() != null && actbean.getAction() == EditAccountBean.ACTION.DELAGG) {

			String id = actbean.getId();
			List<AggregatedAccount> aggAccounts = userSession.getAggregatedAccounts();
			AggregatedAccount aggact = Utils.getAggregatedAccountById(Integer.parseInt(id), aggAccounts);
			try {
				customerService.deleteAggregationAccount(Integer.parseInt(id));
				userSession.getAggregatedAccounts().remove(aggact);
				userSession.setStatusmessage("Deleted External Account: " + aggact.getNickName() + ""
						+ aggact.getAccountNumber());
			} catch (Exception e) {
				userSession.setStatusmessage("Could not delete External Account");
			}
		} else if (actbean.getAction() != null && actbean.getAction() == EditAccountBean.ACTION.DELTRF) {
			String id = actbean.getId();
			if (id == null || "".equals(id))
				return showForm(request, errors, "editaccount", model);

			TVFrontendAccount trfact = Utils.getAccountById(id, trfAccounts);
			boolean deletetrf = userSession.getCustomerService().deleteThirdPartyAccount(
					userSession.getCustomerProfile(),
					userSession.getBankingSession(),
					Utils.getAccountByAccountNumber(userSession.getCustomerProfile().getEnrollmentAccountNumber(),
							userSession.getAccounts()), trfact);
			if (deletetrf) {
				userSession.getTrfAccounts().remove(trfact);
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Other account was deleted.");
				else
					userSession.setStatusmessage("Cuenta externa fue borrada.");
			} else {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Unable to delete other account.");
				else
					userSession.setStatusmessage("Error borrando cuenta.");
			}

		} else if (actbean.getAction() != null && actbean.getAction() == EditAccountBean.ACTION.HIDEPIF) {

			boolean hide = actbean.getHide().equalsIgnoreCase("true") ? true : false;
			model.put("text", hide);

			try {
				boolean updated = customerService.setPifExternalAccountShow(actbean.getId(), hide);
				if (!updated)
					model.put("text", !hide);
			} catch (Exception e) {
				model.put("text", !hide);
			}
			return new ModelAndView("text", model);
		} else if (actbean.getAction() != null && actbean.getAction() == EditAccountBean.ACTION.MODPIFACT) {

			boolean isvalid = false;
			String originalNickname = customerService.getPifExternalAccountNickname(actbean.getId());

			if (!GenericValidator.isBlankOrNull(actbean.getNickname())
					&& GenericValidator.maxLength(actbean.getNickname(), 30)
					&& GenericValidator.matchRegexp(actbean.getNickname(), "^[A-Za-z\\d-]+(\\s[A-Za-z\\d-]+)*$"))
				isvalid = true;

			if (isvalid) {
				boolean updated = customerService.setPifExternalAccountNickname(actbean.getId(), actbean.getNickname());
				if (updated)
					model.put("text", actbean.getNickname());
				else
					model.put("text", originalNickname);
			} else
				model.put("text", originalNickname);

			return new ModelAndView("text", model);
		} else if (actbean.getAction() != null && actbean.getAction() == EditAccountBean.ACTION.DELPIF) {
			String id = actbean.getId();
			if (id == null || "".equals(id))
				return showForm(request, errors, "editaccount", model);

			// TO-DO: Delete the account with the Legasuite message
			// TO-DO: Delete the account from Telepago

			boolean deletepif = customerService.deletePIFExternalAccount(actbean.getId());

			if (deletepif) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Other Bank account was deleted.");
				else
					userSession.setStatusmessage("Cuenta de otro Banco fue borrada.");
			} else {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Unable to delete other Bank account.");
				else
					userSession.setStatusmessage("Error borrando cuenta de otro Banco.");
			}

		}

		return showForm(request, errors, "editaccount", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		List<MortgageLoanBean> status = new ArrayList<MortgageLoanBean>();
		List<TVFrontendAccount> trfAccounts = userSession.getTrfAccounts();
		if (trfAccounts != null && trfAccounts.size() > 0)
			model.put("trfAccounts", trfAccounts);
		model.put("statusAccount", request.getParameter("statusAccount"));
		// Interest Statements Opt-In Opt-Out Show Flag
		boolean foundRES = false;
		boolean foundIRA = false;
		boolean foundIRAQT = false;
		boolean foundCAEQT = false;
		int iteratorIndex = 0;
		CustomerEntitlement ezstmtEntitlement = userSession.getCustomerProfile().getEntitlementByType(EntitlementType.EZSTMT);
		String accountsStatus = ((ezstmtEntitlement.getString("MLA_ESTMT")!=null) ? ezstmtEntitlement.getString("MLA_ESTMT") : "") ;
		
		if (accounts != null) {
			for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				if (foundRES && foundIRA && foundIRAQT && foundCAEQT)
					break;
				TVFrontendAccount account = (TVFrontendAccount) iterator.next();
				if (!foundRES || (foundRES && account.getSubtype().equalsIgnoreCase("MLA"))) {
					if (account.getSubtype().equals("MLA") | account.getSubtype().equals("RVA")
							| account.getSubtype().equals("LEA")) {
						model.put("showInterestStmtBoxRES", true);
						MortgageLoanBean mla = new MortgageLoanBean();
						if(account.getSubtype().equalsIgnoreCase("MLA")){
								mla.setAccount(account.getFrontEndId());
								mla.setStatus(Utils.mlaAccountStatus(account.getAccountNumber(), accountsStatus));
								status.add(mla);
						}
						foundRES = true;
					}
				}
				if (!foundIRA) {
					if (account.getSubtype().equals("CDA")) {
						model.put("showInterestStmtBoxIRA", true);
						foundIRA = true;
					}
				}
				if (!foundIRAQT) {
					for (String iraProductId : iraProductIds) {
						if (account.getProductDefinition().getProductId().equals(iraProductId)) {
							model.put("showInterestStmtBoxIRAQT", true);
							foundIRAQT = true;
							break;
						}
					}
				}
				if (!foundCAEQT) {
					for (String caeProductId : caeProductIds) {
						if (account.getProductDefinition().getProductId().equals(caeProductId)) {
							model.put("showInterestStmtBoxCAEQT", true);
							foundCAEQT = true;
							break;
						}
					}
				}
				iteratorIndex=iteratorIndex+1;
			}
		}

		if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.EZSTMT)) {
			if (customerService.isInterestStmtRESActive(userSession.getCustomerProfile()))
				model.put("statusInterestStmtRES", "ACTIVE");
			else
				model.put("statusInterestStmtRES", "DEACTIVE");

			if (customerService.isInterestStmtIRAActive(userSession.getCustomerProfile()))
				model.put("statusInterestStmtIRA", "ACTIVE");
			else
				model.put("statusInterestStmtIRA", "DEACTIVE");

			if (customerService.isInterestStmtIRAQTActive(userSession.getCustomerProfile()))
				model.put("statusInterestStmtIRAQT", "ACTIVE");
			else
				model.put("statusInterestStmtIRAQT", "DEACTIVE");

			if (customerService.isInterestStmtCAEQTActive(userSession.getCustomerProfile()))
				model.put("statusInterestStmtCAEQT", "ACTIVE");
			else
				model.put("statusInterestStmtCAEQT", "DEACTIVE");
		} else {
			model.put("statusInterestStmtRES", "DEACTIVE");
			model.put("statusInterestStmtIRA", "DEACTIVE");
			model.put("statusInterestStmtIRAQT", "DEACTIVE");
			model.put("statusInterestStmtCAEQT", "DEACTIVE");
		}
		// End Ez Statment Opt-In Opt-Out Show Flag

		SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
		if (smsprofile != null) {
			model.put("enrollSMS", true);

			SmsAccountsGetterRequest actget = new SmsAccountsGetterRequest(
					Utils.generateCustomerInteractionEvent(request), profile, new SMSEntitlement(
							profile.getEntitlementByType(EntitlementType.MOBILE_BANKING)),
					userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)), accounts);
			customerService.getSMSAccounts(actget);
		} else
			model.put("enrollSMS", false);

		// The EBPP2 user id is not yet in session; add it
		if (!userSession.isEbpp2UserIdLookedUp()) {
			EbppEntitlement ent = null;
			if (profile.getEntitlementByType(EntitlementType.EBILLS) != null) {
				ent = new EbppEntitlement(profile.getEntitlementByType(EntitlementType.EBILLS));

				// If the username is blank, we need to look in the current EBPP
				// accounts ONLY
				if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
					userSession.setEbpp2UserId("");
				} else {
					userSession.setEbpp2UserId(ent.getUsername());
				}
				userSession.setEbpp2UserIdLookedUp(true);
			}
		}

		// The user has an EBPP2 ID
		if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

			// Get the list of user accounts
			List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

			//Validación de variable not null
			if (ebpp2UserAccounts != null) {

				//MBFSE-145 NOTE: method contains feature flag validation
				logEbillsAndStatementStatus(request, ebpp2UserAccounts, userSession);

			}
			// Loop through all the accounts and match them with the proper
			// userAccount
			if (ebpp2UserAccounts != null && ebpp2UserAccounts.size() > 0) {
				for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
					TVFrontendAccount act = (TVFrontendAccount) iterator.next();
					if (act.getIsAvailableElecStmt()) {
						act.setEbpp2UserAccount(Utils.getEbpp2UserAccountsByAccount(act.getAccountNumber(), userSession));
					}
				}
			}
		}
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)){
			userSession.resetIdaAccount();
			for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount act = (TVFrontendAccount) iterator.next();
				if(act.getSubtype().equals("IDA")){
					List<TVFrontendDepositAccount> idaAccounts = userSession.getIdaAccounts();
					for(Iterator<TVFrontendDepositAccount> iterator2 = idaAccounts.iterator(); iterator2.hasNext();){
						TVFrontendDepositAccount idaAccount = (TVFrontendDepositAccount) iterator2.next();
						if(idaAccount.getAccountKey().equals(act.getAccountKey())){
							act.setStmtDisposition(idaAccount.getStmtDisposition());
							break;
						}
					}
				}else if(act.getSubtype().equals("CCA")){
					// TODO: Use Ajax call	tsysStatus
				}
			}
			model.put("eBillsFlag", true);
		}
		
		if (accounts != null && accounts.size() > 0) {

			List<TVFrontendAccount> portalAccounts = new LinkedList<TVFrontendAccount>();
			List<TVFrontendAccount> secins = new LinkedList<TVFrontendAccount>();
			List<TVFrontendAccount> programs = new LinkedList<TVFrontendAccount>();
			for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount name = (TVFrontendAccount) iterator.next();
				if (name.getSubtype().equals("INS") || name.getSubtype().equals("SEC")){
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE675)) {
						if(!this.insuranceRemoved.contains(name.getProductDefinition().getProductId())){
							secins.add(name);
						}
					}
					else{
						secins.add(name);
					}					
				}
				else if (name.getSubtype().equals("PRM"))
					programs.add(name);
				else
					portalAccounts.add(name);
			}
			if (secins != null && secins.size() > 0)
				model.put("secins", secins);
			if (programs != null && programs.size() > 0) {
				if (portalAccounts != null)
					portalAccounts.addAll(programs);
			}
			if (portalAccounts != null && portalAccounts.size() > 0)
				model.put("accounts", portalAccounts);
		}

		if (profile.hasEntitlement(EntitlementType.MOBILE_BANKING))
			model.put("mobilebanking", true);
		else
			model.put("mobilebanking", false);

		if (userSession.hasVirginIslandAccount())
			model.put("hasVIAccount", true);

		String id = request.getParameter("id");
		if (id != null) {
			id = InternationalCharacterUtils.sanatizeInputs(id, false);
			model.put("accountToAddEstatement", id);
		}

		// Post Initial Funding
		List<PIFExternalAccount> pifExternalAccounts = new LinkedList<PIFExternalAccount>();
		pifExternalAccounts = customerService.getPIFExternalAccounts(userSession.getCustomerProfile().getTaxId());

		if (pifExternalAccounts.size() > 0) {
			boolean pifAccountLimitReached = customerService.pifAccountLimitReached(userSession.getCustomerProfile()
					.getTaxId());
			model.put("pifExternalAccounts", pifExternalAccounts);
			model.put("pifAccountLimitReached", pifAccountLimitReached);
		}

		boolean canPerformPostInitialFunding;
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD792))
			canPerformPostInitialFunding = userSession.getInterbankFundingAllowed();
		else
			canPerformPostInitialFunding = customerService.canPerformPostInitialFunding(userSession.getAccounts());		
		model.put("canPerformPostInitialFunding", canPerformPostInitialFunding);
		
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)){
			model.put("interbankFundingAllowed", userSession.getInterbankFundingAllowed());
		}
		else{
			model.put("interbankFundingAllowed", Boolean.TRUE);
		}
		
		model.put("status", status);
		
		//MBSFE 297
		model.put("pcbCCANotEbillAllowed", this.pcbCCANotEbillAllowed);
		model.put("MBSFE297", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE297));
		//END MBSFE 297
		
		
		return model;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	@SuppressWarnings("unused")
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

	public String[] getIraProductIds() {
		return iraProductIds;
	}

	public void setIraProductIds(String[] iraProductIds) {
		this.iraProductIds = iraProductIds;
	}

	public String[] getCaeProductIds() {
		return caeProductIds;
	}

	public void setCaeProductIds(String[] caeProductIds) {
		this.caeProductIds = caeProductIds;
	}
	
	private void logEbillsAndStatementStatus(HttpServletRequest request, List<UserAccount> ebpp2UserAccounts, UserSession userSession){
		
		String ebpp2Message = "Listing E-bills and E-statments status\n";
		
		CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request);
		
			if (!userSession.getLogEbppStatus()){
				userSession.setLogEbppStatus(Boolean.TRUE);
				CryptoUtils cryptoUtils = new CryptoUtils();
				for (UserAccount userAccount : ebpp2UserAccounts) {
					ebpp2Message += "\n\n";
					ebpp2Message += "--------\n"; 
					ebpp2Message += "Ebpp2 User Account Number: "+ userAccount.getAccountNumber() + "\n";
					ebpp2Message += "Recive e-bills? " + userAccount.isReceiveEbillNotification() + "\n";
					ebpp2Message += "Recive e-statments? " + userAccount.isReceiveEstatementNotification() + "\n";
				}
				event.setCustomStringData("list ebpp accounts", cryptoUtils.encrypt(this.loggerKey, this.loggerInitVector, ebpp2Message));
				userSession.getCustomerService().logListingEbillsAndStatemntStatus(event, InteractionSeverity.INFO);
			
		}
	}

	public String getInsuranceRemoved() {
		return insuranceRemoved;
	}

	public void setInsuranceRemoved(String insuranceRemoved) {
		this.insuranceRemoved = insuranceRemoved;
	}

	public void setLoggerKey(String loggerKey) {
		this.loggerKey = loggerKey;
	}

	public void setLoggerInitVector(String loggerInitVector) {
		this.loggerInitVector = loggerInitVector;
	}

	public void setPcbCCANotEbillAllowed(String pcbCCANotEbillAllowed) {
		this.pcbCCANotEbillAllowed = pcbCCANotEbillAllowed;
	}
	
}
