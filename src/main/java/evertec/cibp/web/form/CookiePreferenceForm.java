package evertec.cibp.web.form;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.form.bean.CookiePreferenceBean;
import evertec.cibp.web.resources.GDPRResources;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import joptsimple.internal.Strings;
import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Cookie Preference Form
 *
 * @author ET58344, Evertec Inc.
 * @version 1.0
 * @since 12-28-2020
 */
@SuppressWarnings("deprecation")
public class CookiePreferenceForm extends CancellableFormController implements GDPRResources {

	/**
	 * Constant USERSESSION
	 **/
	private final static String USER_SESSION = "userSession"; //string
	private final static String LANGUAGE_EN = "en"; //language english
	private static String END_POINT_EN = Strings.EMPTY; //end point english
	private static String END_POINT_ES = Strings.EMPTY; //end point spanish
	private CustomerServices customerService; //customer service injection
	private String showSectionJson = Strings.EMPTY; //enable/disable properties
	private final static Logger log = LoggerFactory.getLogger(CookiePreferenceForm.class); //logger

	/**
	 * Defatul contructor
	 * @author ET58344, Evertec Inc.
	 * @since 03-02-2021
	 */
	public CookiePreferenceForm() {
		super();
		setCommandClass(CookiePreferenceBean.class);
		setCommandName("cookiePreferenceView");
		setFormView("cookiePreferenceView");
		
		this.customerService = null;
	}
	
	/**
	 * Injection parameter contructor
	 * @author ET58344, Evertec Inc.
	 * @since 03-02-2021
	 * @param customerService service
	 * @param showSectionJson status toggle
	*/
	public CookiePreferenceForm(CustomerServices customerService, String endPointEN, String endPointES,
								String showSectionJson) {
		this();
		this.customerService = customerService;
		END_POINT_EN = endPointEN;
		END_POINT_ES = endPointES;
		this.showSectionJson = showSectionJson;
	}

	/**
	 * On Submit method
	 *
	 * @param request  HttpServletRequest
	 * @param response HttpServletResponse
	 * @param command  Object
	 * @param error    BindException
	 * @return close session
	 * @throws Exception exeption
	 */
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		Map<String, Object> model = new HashMap<>(); //model
		return showForm(request, response, error, model);
	}

	/**
	 * Reference Data method
	 *
	 * @param request HttpServletRequest
	 * @param command Object
	 * @param errors  BindException
	 * @return close session
	 * @throws Exception exeption
	 */
	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>(); //model
		Object session = WebUtils.getSessionAttribute(request, USER_SESSION); //session obj
		UserSession userSession = new UserSession(); //user session
		userSession = validateSession(session, userSession);
		
		final String strEnabled = "ENABLED"; //string
		final String strSwAnalyticStatus = "swAnalyticStatus"; //string
		final String strSwFunctionalStatus = "swFunctionalStatus"; //string
		final String strSwAdvertisingStatus = "swAdvertisingStatus"; //string
		
		userSession.setCookiePreferenceStatusList(customerService.getCookiePreferenceStatusService()
				.getCookiePreferenceStatusByProfileId(userSession.getCustomerProfile().getProfileId()));

		for (CookiePreferenceStatus eachCookiePreference : userSession.getCookiePreferenceStatusList()) {

			if (eachCookiePreference.getCategory().equals(CookieCategory.ANALYTICS.toString())) {
				model.put(strSwAnalyticStatus,
						eachCookiePreference.getStatus().equals(strEnabled) ? Boolean.TRUE : Boolean.FALSE);

			} else if (eachCookiePreference.getCategory().equals(CookieCategory.FUNCTIONAL.toString())) {
				model.put(strSwFunctionalStatus,
						eachCookiePreference.getStatus().equals(strEnabled) ? Boolean.TRUE : Boolean.FALSE);

			} else if (eachCookiePreference.getCategory().equals(CookieCategory.ADVERTISING.toString())) {
				model.put(strSwAdvertisingStatus,
						eachCookiePreference.getStatus().equals(strEnabled) ? Boolean.TRUE : Boolean.FALSE);
			}
		}

		JsonObject sectionValues = new Gson().fromJson(showSectionJson, JsonObject.class); //section values

		readPrivacySettingsJSON(model, request);

		final String ENABLE = "ENABLE";

		model.put(STR_ADVERTISING, sectionValues.get(STR_ADVERTISING).getAsString().equals(ENABLE));
		model.put(STR_FUNCTIONAL, sectionValues.get(STR_FUNCTIONAL).getAsString().equals(ENABLE));

		return model;
	}
	
	/**
	 * This method was created by kiuwan defect (ciclomatyc complexity in reference data)
	 * @author ET58344, Evertec Inc.
	 * @since 03-03-2021
	 * @param session object
	 * @param userSession user session
	 * @return UserSession
	 */
	private UserSession validateSession(Object session, UserSession userSession) {
		
		if(session instanceof UserSession) {
			userSession = (UserSession) session;
		}
		return userSession;
	}

	/**
	 * Get customer service
	 * 
	 * @author ET58344, Evertec Inc
	 * @since 02-18-2021
	 * @return CustomerServices the customerService value
	 */
	public CustomerServices getCustomerService() {
		return customerService;
	}

	/**
	 * Set customer service
	 * 
	 * @author ET58344, Evertec Inc
	 * @since 02-18-2021
	 * @param customerService the customerService to set
	 */
	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	/**
	 * Get show section json
	 *
	 * @author ET58344, Evertec Inc
	 * @since 02-07-2022
	 * @return String json value
	 */
	public String getShowSectionJson() {
		return showSectionJson;
	}

	/**
	 * Set customer service
	 *
	 * @author ET58344, Evertec Inc
	 * @since 02-07-2022
	 * @param showSectionJson the json string to set
	 */
	public void setShowSectionJson(String showSectionJson) {
		this.showSectionJson = showSectionJson;
	}

	/**
	 * Read JSON from URL
	 * @param language language
	 * @return JSONObject object
	 * @throws IOException exception
	 */
	private JSONObject getJSONFromUrl(String language) throws IOException {

		String url; //url

		if (LANGUAGE_EN.equalsIgnoreCase(language)) {
			url = END_POINT_EN;
		} else {
			url = END_POINT_ES;
		}
		HttpsURLConnection con = null; //https connection

		try {
			con = openHttpsConnection(url, con);
			BufferedReader br = null; //buffered reader
			if (con != null) {
				br = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));
			}
			StringBuilder sb = new StringBuilder(); //string builder
			String line; //line
			if (br != null) {
				while ((line = br.readLine()) != null) {
					sb.append(line).append("\n");
				}
				br.close();
			}
			String resultJson = sb.toString(); //result json
			StringEscapeUtils.escapeJava(resultJson); //verify injection
			return new JSONObject(resultJson);
		} catch(SSLException | MalformedURLException e) {
			logger.info(EXCEPTION_MSG);
			throw e;
		} finally {
			if (con != null) {
				con.disconnect();
			}
		}
	}

	/**
	 * This method create the connection for url passed
	 *
	 * @author ET58344, Evertec Inc.
	 * @since 06-17-2021
	 * @param url url
	 * @param con connection
	 * @return HttpsURLConnection object
	 * @throws IOException exception
	 */
	private HttpsURLConnection openHttpsConnection(String url, HttpsURLConnection con) throws IOException {

		try {
			final URL u = new URL(url); //url
			final URLConnection urlConnection = u.openConnection(); //url connection

			if(urlConnection instanceof  HttpsURLConnection) {
				con = (HttpsURLConnection) urlConnection;
			}
			if (con != null) {
				con.connect();
			}
			return con;
		} catch(SSLException | MalformedURLException e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Read privacy settings JSON from Popular Rackspace
	 * @author ET58344, Evertec Inc.
	 * @since 06-11-2021
	 * @param model model
	 * @param request request
	 * @throws IOException exception
	 */
	private void readPrivacySettingsJSON(Map<String, Object> model, HttpServletRequest request) throws IOException {

		final JSONObject jsonObject = getJSONFromUrl(Utils.getRSALanguage(request)); //json object
		final JSONObject jsonInformation = jsonObject.getJSONObject(STR_JSON_INFORMATION); //information
		final JSONArray jsonCategories = jsonObject.getJSONArray(STR_JSON_CATEGORIES); //categories
		model.put(INFORMATION_TITLE, jsonInformation.getString(STR_TITLE));
		model.put(INFORMATION_TEXT, jsonInformation.getString(STR_TEXT));

		JSONObject jsonObj = new JSONObject(); //obj for
		String categoryTitle; //string for
		for(Object eachJson : jsonCategories) {

			jsonObj =  verifyJSONType(eachJson, jsonObj);
			categoryTitle =  jsonObj.getString(STR_CATEGORY);
			putModelCategoryValues(categoryTitle, model, jsonObj);
		}
	}

	/**
	 * This method was separated for Kiuwan cyclomatic complexity..
	 *
	 * @author ET58344, Evertec Inc.
	 * @since 06-17-2021
	 * @param categoryTitle category
	 * @param model model
	 * @param jsonObj json
	 */
	private void putModelCategoryValues(String categoryTitle, Map<String, Object> model, JSONObject jsonObj) {

		if (categoryTitle.toLowerCase().contains(STR_STRICTLY) || categoryTitle.toLowerCase().contains(STR_ESTRICTAMENTE)) {
			model.put(STRICTLY_COOKIES_TITLE, categoryTitle);
			model.put(STRICTLY_COOKIES_TEXT, jsonObj.getString(STR_TEXT));
		} else if(categoryTitle.toLowerCase().contains(STR_ANALYTICS) || categoryTitle.toLowerCase().contains(STR_RENDIMIENTO)) {
			model.put(ANALYTICS_TITLE, categoryTitle);
			model.put(ANALYTICS_TEXT, jsonObj.getString(STR_TEXT));
		} else {
			putModelCategoryValuesII(categoryTitle, model, jsonObj);
		}
	}

	/**
	 * This method was separated for Kiuwan cyclomatic complexity..
	 *
	 * @author ET58344, Evertec Inc.
	 * @since 06-17-2021
	 * @param categoryTitle category
	 * @param model model
	 * @param jsonObj json
	 */
	private void putModelCategoryValuesII(String categoryTitle, Map<String, Object> model, JSONObject jsonObj) {

		if(categoryTitle.toLowerCase().contains(STR_FUNCTIONAL) || categoryTitle.toLowerCase().contains(STR_FUNCIONAL)) {
			model.put(FUNCTIONAL_TITLE, categoryTitle);
			model.put(FUNCTIONAL_TEXT, jsonObj.getString(STR_TEXT));
		} else if(categoryTitle.toLowerCase().contains(STR_ADVERTISING) || categoryTitle.toLowerCase().contains(STR_PUBLICIDAD)) {
			model.put(ADVERTISING_TITLE, categoryTitle);
			model.put(ADVERTISING_TEXT, jsonObj.getString(STR_TEXT));
		}
	}

	/**
	 * Method that verify the json type for object
	 *
	 * @author ET58344, Evertec Inc.
	 * @since 06-17-2021
	 * @param object object
	 * @param jsonObject json
	 * @return JSONObject result
	 */
	private JSONObject verifyJSONType(Object object, JSONObject jsonObject) {

		if(object instanceof  JSONObject) {
			jsonObject = (JSONObject) object;
		}
		return jsonObject;
	}
}