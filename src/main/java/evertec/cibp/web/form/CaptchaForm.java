package evertec.cibp.web.form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.web.form.bean.CaptchaSettings;
import evertec.cibp.web.session.CaptchaSession;
import evertec.cibp.web.view.CaptchaView;

public class CaptchaForm extends AbstractController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		CaptchaSession captchaSession = (CaptchaSession) WebUtils.getSessionAttribute(request, "captchaSession");

		// TODO: Implement a CaptchaSession Interceptor?
		if (captchaSession == null) {
			captchaSession = new CaptchaSession(new CaptchaSettings());// throw
																		// new
																		// RuntimeException("Required Captcha Session not found");
		}

		return new ModelAndView(new CaptchaView(), "captcha", captchaSession.getImageCaptcha());
	}

}
