package evertec.cibp.web.form.insurance.bean;

import java.io.Serializable;

public class FamilyCareBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8359120510651832508L;
	private String fname;
	private String direccion;
	private String lname;
	private String mlname;
	private String city;
	private String state;
	private String zipcode;
	private int customerAge;
	private String customerAccount;
	private String plan;
	private String amount;
	private String tele_residencial;
	private String tele_trabajo;
	private String beneficiario_1;
	private String beneficiario_1_1;
	private int porcentaje_1;
	private String beneficiario_2;
	private String beneficiario_2_2;
	private int porcentaje_2;
	private String beneficiario_3;
	private String beneficiario_3_3;
	private int porcentaje_3;
	private String beneficiario_4;
	private String beneficiario_4_4;
	private int porcentaje_4;
	private String gender;
	private String dependiente_1;
	private String dependiente_1_1;
	private String relacion_1;
	private int relacion_1_mes;
	private int relacion_1_dia;
	private int relacion_1_anio;
	private String dependiente_2;
	private String dependiente_2_2;
	private String relacion_2;
	private int relacion_2_mes;
	private int relacion_2_dia;
	private int relacion_2_anio;
	private String dependiente_3;
	private String dependiente_3_3;
	private String relacion_3;
	private int relacion_3_mes;
	private int relacion_3_dia;
	private int relacion_3_anio;
	private String dependiente_4;
	private String dependiente_4_4;
	private String relacion_4;
	private int relacion_4_mes;
	private int relacion_4_dia;
	private int relacion_4_anio;
	private String dependiente_5;
	private String dependiente_5_5;
	private String relacion_5;
	private int relacion_5_mes;
	private int relacion_5_dia;
	private int relacion_5_anio;
	private String dependiente_6;
	private String dependiente_6_6;
	private String relacion_6;
	private int relacion_6_mes;
	private int relacion_6_dia;
	private int relacion_6_anio;
	private String dependiente_7;
	private String dependiente_7_7;
	private String relacion_7;
	private int relacion_7_mes;
	private int relacion_7_dia;
	private int relacion_7_anio;

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public FamilyCareBean() {
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getMlname() {
		return mlname;
	}

	public void setMlname(String mlname) {
		this.mlname = mlname;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public int getCustomerAge() {
		return customerAge;
	}

	public void setCustomerAge(int customerAge) {
		this.customerAge = customerAge;
	}

	public String getCustomerAccount() {
		return customerAccount;
	}

	public void setCustomerAccount(String customerAccount) {
		this.customerAccount = customerAccount;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTele_residencial() {
		return tele_residencial;
	}

	public void setTele_residencial(String tele_residencial) {
		this.tele_residencial = tele_residencial;
	}

	public String getTele_trabajo() {
		return tele_trabajo;
	}

	public void setTele_trabajo(String tele_trabajo) {
		this.tele_trabajo = tele_trabajo;
	}

	public String getBeneficiario_1() {
		return beneficiario_1;
	}

	public void setBeneficiario_1(String beneficiario_1) {
		this.beneficiario_1 = beneficiario_1;
	}

	public String getBeneficiario_1_1() {
		return beneficiario_1_1;
	}

	public void setBeneficiario_1_1(String beneficiario_1_1) {
		this.beneficiario_1_1 = beneficiario_1_1;
	}

	public int getPorcentaje_1() {
		return porcentaje_1;
	}

	public void setPorcentaje_1(int porcentaje_1) {
		this.porcentaje_1 = porcentaje_1;
	}

	public String getBeneficiario_2() {
		return beneficiario_2;
	}

	public void setBeneficiario_2(String beneficiario_2) {
		this.beneficiario_2 = beneficiario_2;
	}

	public String getBeneficiario_2_2() {
		return beneficiario_2_2;
	}

	public void setBeneficiario_2_2(String beneficiario_2_2) {
		this.beneficiario_2_2 = beneficiario_2_2;
	}

	public int getPorcentaje_2() {
		return porcentaje_2;
	}

	public void setPorcentaje_2(int porcentaje_2) {
		this.porcentaje_2 = porcentaje_2;
	}

	public String getBeneficiario_3() {
		return beneficiario_3;
	}

	public void setBeneficiario_3(String beneficiario_3) {
		this.beneficiario_3 = beneficiario_3;
	}

	public String getBeneficiario_3_3() {
		return beneficiario_3_3;
	}

	public void setBeneficiario_3_3(String beneficiario_3_3) {
		this.beneficiario_3_3 = beneficiario_3_3;
	}

	public int getPorcentaje_3() {
		return porcentaje_3;
	}

	public void setPorcentaje_3(int porcentaje_3) {
		this.porcentaje_3 = porcentaje_3;
	}

	public String getBeneficiario_4() {
		return beneficiario_4;
	}

	public void setBeneficiario_4(String beneficiario_4) {
		this.beneficiario_4 = beneficiario_4;
	}

	public String getBeneficiario_4_4() {
		return beneficiario_4_4;
	}

	public void setBeneficiario_4_4(String beneficiario_4_4) {
		this.beneficiario_4_4 = beneficiario_4_4;
	}

	public int getPorcentaje_4() {
		return porcentaje_4;
	}

	public void setPorcentaje_4(int porcentaje_4) {
		this.porcentaje_4 = porcentaje_4;
	}

	public String getDependiente_1() {
		return dependiente_1;
	}

	public void setDependiente_1(String dependiente_1) {
		this.dependiente_1 = dependiente_1;
	}

	public String getDependiente_1_1() {
		return dependiente_1_1;
	}

	public void setDependiente_1_1(String dependiente_1_1) {
		this.dependiente_1_1 = dependiente_1_1;
	}

	public String getRelacion_1() {
		return relacion_1;
	}

	public void setRelacion_1(String relacion_1) {
		this.relacion_1 = relacion_1;
	}

	public int getRelacion_1_mes() {
		return relacion_1_mes;
	}

	public void setRelacion_1_mes(int relacion_1_mes) {
		this.relacion_1_mes = relacion_1_mes;
	}

	public int getRelacion_1_dia() {
		return relacion_1_dia;
	}

	public void setRelacion_1_dia(int relacion_1_dia) {
		this.relacion_1_dia = relacion_1_dia;
	}

	public int getRelacion_1_anio() {
		return relacion_1_anio;
	}

	public void setRelacion_1_anio(int relacion_1_anio) {
		this.relacion_1_anio = relacion_1_anio;
	}

	public String getDependiente_2() {
		return dependiente_2;
	}

	public void setDependiente_2(String dependiente_2) {
		this.dependiente_2 = dependiente_2;
	}

	public String getDependiente_2_2() {
		return dependiente_2_2;
	}

	public void setDependiente_2_2(String dependiente_2_2) {
		this.dependiente_2_2 = dependiente_2_2;
	}

	public String getRelacion_2() {
		return relacion_2;
	}

	public void setRelacion_2(String relacion_2) {
		this.relacion_2 = relacion_2;
	}

	public int getRelacion_2_mes() {
		return relacion_2_mes;
	}

	public void setRelacion_2_mes(int relacion_2_mes) {
		this.relacion_2_mes = relacion_2_mes;
	}

	public int getRelacion_2_dia() {
		return relacion_2_dia;
	}

	public void setRelacion_2_dia(int relacion_2_dia) {
		this.relacion_2_dia = relacion_2_dia;
	}

	public int getRelacion_2_anio() {
		return relacion_2_anio;
	}

	public void setRelacion_2_anio(int relacion_2_anio) {
		this.relacion_2_anio = relacion_2_anio;
	}

	public String getDependiente_3() {
		return dependiente_3;
	}

	public void setDependiente_3(String dependiente_3) {
		this.dependiente_3 = dependiente_3;
	}

	public String getDependiente_3_3() {
		return dependiente_3_3;
	}

	public void setDependiente_3_3(String dependiente_3_3) {
		this.dependiente_3_3 = dependiente_3_3;
	}

	public String getRelacion_3() {
		return relacion_3;
	}

	public void setRelacion_3(String relacion_3) {
		this.relacion_3 = relacion_3;
	}

	public int getRelacion_3_mes() {
		return relacion_3_mes;
	}

	public void setRelacion_3_mes(int relacion_3_mes) {
		this.relacion_3_mes = relacion_3_mes;
	}

	public int getRelacion_3_dia() {
		return relacion_3_dia;
	}

	public void setRelacion_3_dia(int relacion_3_dia) {
		this.relacion_3_dia = relacion_3_dia;
	}

	public int getRelacion_3_anio() {
		return relacion_3_anio;
	}

	public void setRelacion_3_anio(int relacion_3_anio) {
		this.relacion_3_anio = relacion_3_anio;
	}

	public String getDependiente_4() {
		return dependiente_4;
	}

	public void setDependiente_4(String dependiente_4) {
		this.dependiente_4 = dependiente_4;
	}

	public String getDependiente_4_4() {
		return dependiente_4_4;
	}

	public void setDependiente_4_4(String dependiente_4_4) {
		this.dependiente_4_4 = dependiente_4_4;
	}

	public String getRelacion_4() {
		return relacion_4;
	}

	public void setRelacion_4(String relacion_4) {
		this.relacion_4 = relacion_4;
	}

	public int getRelacion_4_mes() {
		return relacion_4_mes;
	}

	public void setRelacion_4_mes(int relacion_4_mes) {
		this.relacion_4_mes = relacion_4_mes;
	}

	public int getRelacion_4_dia() {
		return relacion_4_dia;
	}

	public void setRelacion_4_dia(int relacion_4_dia) {
		this.relacion_4_dia = relacion_4_dia;
	}

	public int getRelacion_4_anio() {
		return relacion_4_anio;
	}

	public void setRelacion_4_anio(int relacion_4_anio) {
		this.relacion_4_anio = relacion_4_anio;
	}

	public String getDependiente_5() {
		return dependiente_5;
	}

	public void setDependiente_5(String dependiente_5) {
		this.dependiente_5 = dependiente_5;
	}

	public String getDependiente_5_5() {
		return dependiente_5_5;
	}

	public void setDependiente_5_5(String dependiente_5_5) {
		this.dependiente_5_5 = dependiente_5_5;
	}

	public String getRelacion_5() {
		return relacion_5;
	}

	public void setRelacion_5(String relacion_5) {
		this.relacion_5 = relacion_5;
	}

	public int getRelacion_5_mes() {
		return relacion_5_mes;
	}

	public void setRelacion_5_mes(int relacion_5_mes) {
		this.relacion_5_mes = relacion_5_mes;
	}

	public int getRelacion_5_dia() {
		return relacion_5_dia;
	}

	public void setRelacion_5_dia(int relacion_5_dia) {
		this.relacion_5_dia = relacion_5_dia;
	}

	public int getRelacion_5_anio() {
		return relacion_5_anio;
	}

	public void setRelacion_5_anio(int relacion_5_anio) {
		this.relacion_5_anio = relacion_5_anio;
	}

	public String getDependiente_6() {
		return dependiente_6;
	}

	public void setDependiente_6(String dependiente_6) {
		this.dependiente_6 = dependiente_6;
	}

	public String getDependiente_6_6() {
		return dependiente_6_6;
	}

	public void setDependiente_6_6(String dependiente_6_6) {
		this.dependiente_6_6 = dependiente_6_6;
	}

	public String getRelacion_6() {
		return relacion_6;
	}

	public void setRelacion_6(String relacion_6) {
		this.relacion_6 = relacion_6;
	}

	public int getRelacion_6_mes() {
		return relacion_6_mes;
	}

	public void setRelacion_6_mes(int relacion_6_mes) {
		this.relacion_6_mes = relacion_6_mes;
	}

	public int getRelacion_6_dia() {
		return relacion_6_dia;
	}

	public void setRelacion_6_dia(int relacion_6_dia) {
		this.relacion_6_dia = relacion_6_dia;
	}

	public int getRelacion_6_anio() {
		return relacion_6_anio;
	}

	public void setRelacion_6_anio(int relacion_6_anio) {
		this.relacion_6_anio = relacion_6_anio;
	}

	public String getDependiente_7() {
		return dependiente_7;
	}

	public void setDependiente_7(String dependiente_7) {
		this.dependiente_7 = dependiente_7;
	}

	public String getDependiente_7_7() {
		return dependiente_7_7;
	}

	public void setDependiente_7_7(String dependiente_7_7) {
		this.dependiente_7_7 = dependiente_7_7;
	}

	public String getRelacion_7() {
		return relacion_7;
	}

	public void setRelacion_7(String relacion_7) {
		this.relacion_7 = relacion_7;
	}

	public int getRelacion_7_mes() {
		return relacion_7_mes;
	}

	public void setRelacion_7_mes(int relacion_7_mes) {
		this.relacion_7_mes = relacion_7_mes;
	}

	public int getRelacion_7_dia() {
		return relacion_7_dia;
	}

	public void setRelacion_7_dia(int relacion_7_dia) {
		this.relacion_7_dia = relacion_7_dia;
	}

	public int getRelacion_7_anio() {
		return relacion_7_anio;
	}

	public void setRelacion_7_anio(int relacion_7_anio) {
		this.relacion_7_anio = relacion_7_anio;
	}
}
