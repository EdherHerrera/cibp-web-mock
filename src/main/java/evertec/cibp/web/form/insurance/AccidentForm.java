package evertec.cibp.web.form.insurance;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.insurance.Accident;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.BankingSessionRequest;
import evertec.cibp.core.services.responses.BankingSessionResponse;
import evertec.cibp.web.form.insurance.bean.AccidentBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.session.UserSessionType;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class AccidentForm extends AbstractWizardFormController {

	private static String INSURED_DEPENDENT = "D";
	private static String INSURED_SPOUSE = "S";
	private static int MAX_DEPENDENT_AGE = 17;

	private String accidentAccounts;
	private Integer maxAge;
	private Integer minAge;

	public AccidentForm() {
		super();
		setCommandClass(AccidentBean.class);
		setCommandName("accident");
		setPages(new String[] { "insuranceAccident" });
	}

	public String getAccidentAccounts() {
		return accidentAccounts;
	}

	public void setAccidentAccounts(String accidentAccounts) {
		this.accidentAccounts = accidentAccounts;
	}

	public Integer getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(Integer maxAge) {
		this.maxAge = maxAge;
	}

	public Integer getMinAge() {
		return minAge;
	}

	public void setMinAge(Integer minAge) {
		this.minAge = minAge;
	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		AccidentBean bean = (AccidentBean) command;
		int customerBirthDay = Integer.parseInt(request.getParameter("birthday_day"));
		int customerBirthMonth = Integer.parseInt(request.getParameter("birthday_month")) - 1;
		int customerBirthYear = Integer.parseInt(request.getParameter("birthday_year"));

		Date customerBirthDate;
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.set(customerBirthYear, customerBirthMonth, customerBirthDay);
		customerBirthDate = calendar.getTime();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		// GENERO INDEFINIDO
		char gender = 'I';

		if (customerService.getDataCustomerProfile(userSession.getBankingSession().getPermId().toString()) != null) {
			String[] data = customerService.getDataCustomerProfile(userSession.getBankingSession().getPermId()
					.toString());
			if (data != null) {
				if (data[4] != null && !data[4].trim().equals("")) {
					gender = data[4].toLowerCase().charAt(0);
				}
			} else {
				gender = customerService.getGenderCustomerProfile(userSession.getBankingSession().getPermId()
						.toString());
			}
		}

		if (bean.getCustomerAge() <= this.maxAge && bean.getCustomerAge() >= this.minAge) {

			Accident accident = new Accident();
			accident.setAmount(Double.parseDouble(bean.getAmount().replaceAll("[$|,]", "").trim()));

			accident.setBeneficiary1Name(bean.getBeneficiario_1());
			accident.setBeneficiary1LastName(bean.getBeneficiario_1_1());
			accident.setBeneficiary1Percent(bean.getPorcentaje_1());

			accident.setBeneficiary2Name(bean.getBeneficiario_2());
			accident.setBeneficiary2LastName(bean.getBeneficiario_2_2());
			accident.setBeneficiary2Percent(bean.getPorcentaje_2());

			accident.setBeneficiary3Name(bean.getBeneficiario_3());
			accident.setBeneficiary3LastName(bean.getBeneficiario_3_3());
			accident.setBeneficiary3Percent(bean.getPorcentaje_3());

			accident.setBeneficiary4Name(bean.getBeneficiario_4());
			accident.setBeneficiary4LastName(bean.getBeneficiario_4_4());
			accident.setBeneficiary4Percent(bean.getPorcentaje_4());

			accident.setCustomerAccount(bean.getCustomerAccount());
			accident.setCustomerAddress(bean.getDireccion());
			accident.setCustomerAge(bean.getCustomerAge());
			accident.setCustomerCity(bean.getCity());
			// accident.setCustomerDateOfBirth(userSession.getCustomerProfile().getBirthDate());
			// Se usaba la del profile del customer y no la que el customer
			// escogia/cambiaba
			accident.setCustomerDateOfBirth(customerBirthDate);
			accident.setCustomerLastName(bean.getLname());
			accident.setCustomerMaidenName(bean.getMlname());
			accident.setCustomerName(bean.getFname());
			accident.setCustomerSsn(userSession.getCustomerProfile().getTaxId());
			accident.setCustomerState(bean.getState());
			accident.setCustomerZipCode(bean.getZipcode());
			accident.setHomePhone(bean.getTele_residencial());
			accident.setWorkPhone(bean.getTele_trabajo());
			accident.setStatus(Accident.STATUS_SIN_PROCESAR);
			accident.setPlan(Integer.parseInt(bean.getPlan()));

			if (gender == ' ')
				accident.setGender("M");
			else
				accident.setGender(String.valueOf(gender).toUpperCase());

			accident.setDependent1Name(bean.getDependiente_1());
			accident.setDependent1LastName(bean.getDependiente_1_1());

			accident.setDependent2Name(bean.getDependiente_2());
			accident.setDependent2LastName(bean.getDependiente_2_2());

			accident.setDependent3Name(bean.getDependiente_3());
			accident.setDependent3LastName(bean.getDependiente_3_3());

			accident.setDependent4Name(bean.getDependiente_4());
			accident.setDependent4LastName(bean.getDependiente_4_4());

			accident.setDependent5Name(bean.getDependiente_5());
			accident.setDependent5LastName(bean.getDependiente_5_5());

			accident.setDependent6Name(bean.getDependiente_6());
			accident.setDependent6LastName(bean.getDependiente_6_6());

			accident.setDependent7Name(bean.getDependiente_7());
			accident.setDependent7LastName(bean.getDependiente_7_7());

			if (bean.getRelacion_1_mes() > 0 && bean.getRelacion_1_dia() > 0 && bean.getRelacion_1_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_1_anio(), bean.getRelacion_1_mes() - 1, bean.getRelacion_1_dia(), 0, 0, 0);
				accident.setDependent1DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1103
				accident.setDependent1Relationship(bean.getRelacion_1()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_1()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					accident.setDependent1Relationship(INSURED_DEPENDENT); // if
																			// dependent
																			// was
																			// chosen,
																			// a
																			// T
																			// is
																			// sent
																			// by
																			// default
			}

			if (bean.getRelacion_2_mes() > 0 && bean.getRelacion_2_dia() > 0 && bean.getRelacion_2_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_2_anio(), bean.getRelacion_2_mes() - 1, bean.getRelacion_2_dia(), 0, 0, 0);
				accident.setDependent2DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1103
				accident.setDependent2Relationship(bean.getRelacion_2()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_2()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					accident.setDependent2Relationship(INSURED_DEPENDENT); // if
																			// dependent
																			// was
																			// chosen,
																			// a
																			// T
																			// is
																			// sent
																			// by
																			// default
			}

			if (bean.getRelacion_3_mes() > 0 && bean.getRelacion_3_dia() > 0 && bean.getRelacion_3_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_3_anio(), bean.getRelacion_3_mes() - 1, bean.getRelacion_3_dia(), 0, 0, 0);
				accident.setDependent3DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1103
				accident.setDependent3Relationship(bean.getRelacion_3()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_3()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					accident.setDependent3Relationship(INSURED_DEPENDENT); // if
																			// dependent
																			// was
																			// chosen,
																			// a
																			// T
																			// is
																			// sent
																			// by
																			// default
			}

			if (bean.getRelacion_4_mes() > 0 && bean.getRelacion_4_dia() > 0 && bean.getRelacion_4_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_4_anio(), bean.getRelacion_4_mes() - 1, bean.getRelacion_4_dia(), 0, 0, 0);
				accident.setDependent4DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1103
				accident.setDependent4Relationship(bean.getRelacion_4()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_4()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					accident.setDependent4Relationship(INSURED_DEPENDENT); // if
																			// dependent
																			// was
																			// chosen,
																			// a
																			// T
																			// is
																			// sent
																			// by
																			// default
			}

			if (bean.getRelacion_5_mes() > 0 && bean.getRelacion_5_dia() > 0 && bean.getRelacion_5_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_5_anio(), bean.getRelacion_5_mes() - 1, bean.getRelacion_5_dia(), 0, 0, 0);
				accident.setDependent5DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1103
				accident.setDependent5Relationship(bean.getRelacion_5()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_5()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					accident.setDependent5Relationship(INSURED_DEPENDENT); // if
																			// dependent
																			// was
																			// chosen,
																			// a
																			// T
																			// is
																			// sent
																			// by
																			// default
			}

			if (bean.getRelacion_6_mes() > 0 && bean.getRelacion_6_dia() > 0 && bean.getRelacion_6_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_6_anio(), bean.getRelacion_6_mes() - 1, bean.getRelacion_6_dia(), 0, 0, 0);
				accident.setDependent6DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1103
				accident.setDependent6Relationship(bean.getRelacion_6()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_6()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					accident.setDependent6Relationship(INSURED_DEPENDENT); // if
																			// dependent
																			// was
																			// chosen,
																			// a
																			// T
																			// is
																			// sent
																			// by
																			// default
			}

			if (bean.getRelacion_7_mes() > 0 && bean.getRelacion_7_dia() > 0 && bean.getRelacion_7_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_7_anio(), bean.getRelacion_7_mes() - 1, bean.getRelacion_7_dia(), 0, 0, 0);
				accident.setDependent7DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1103
				accident.setDependent7Relationship(bean.getRelacion_7()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_7()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					accident.setDependent7Relationship(INSURED_DEPENDENT); // if
																			// dependent
																			// was
																			// chosen,
																			// a
																			// T
																			// is
																			// sent
																			// by
																			// default
			}

			try {
				if (!customerService.existAccident(accident)) {
					customerService.saveAccident(accident);

					// Successful - show a different page for men and women
					if (customerService.getDataCustomerProfile(userSession.getBankingSession().getPermId().toString()) != null) {
						if (gender == 'f') {
							if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
								model.put("page", 2); // Thank you page for
														// women in English
							else
								model.put("page", 3); // Thank you page for
														// women in Spanish
						} else if (gender == 'm') {
							if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
								model.put("page", 4); // Thank you page for men
														// in English
							else
								model.put("page", 5); // Thank you page for men
														// in Spanish
						} else {
							if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
								model.put("page", 6); // Thank you page for
														// undetermined gender
														// in English
							else
								model.put("page", 7); // Thank you page for
														// undetermined gender
														// in Spanish
						}
					} else {
						if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
							model.put("page", 6); // Thank you page for
													// undetermined gender in
													// English
						else
							model.put("page", 7); // Thank you page for
													// undetermined gender in
													// Spanish
					}
				} else {
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
						model.put("page", 8); // Duplicate page in English
					else
						model.put("page", 9); // Duplicate page in Spanish
				}
			} catch (Exception e) {
				model.put("error", "Error: " + e.getMessage());
				return new ModelAndView("forward:insuranceAccident", model);
			}
		} else {
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
				model.put("page", 10); // Default error page in English
			else
				model.put("page", 11); // Default error page in Spanish
		}

		return new ModelAndView("insuranceAccident", model);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		Map<String, Object> model = new HashMap<>();
		AccidentBean bean = (AccidentBean) command;

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		if (request.getParameter("nothanks") != null) {
			userSession.setPromotion("");
			throw new ModelAndViewDefiningException(new ModelAndView("redirect:portal"));
		}

		if (userSession.getBankingSession() == null) {
			this.getBankingSession(userSession, request);
			if (userSession.getBankingSession() == null)
				throw new ProgrammingError("We could not establish a banking session (accident)");
		}

		Date dateage = profile.getBirthDate();
		int age = Utils.getAge(dateage);
		model.put("customerAge", age);

		if (age <= this.maxAge && age >= this.minAge) {

			model.put("maxAge", this.maxAge);
			model.put("minAge", this.minAge);
			Calendar c = Calendar.getInstance();
			c.setTime(dateage);
			model.put("day", c.get(Calendar.DAY_OF_MONTH));
			model.put("month", c.get(Calendar.MONTH) + 1);
			if (c.get(Calendar.YEAR) < 100)
				model.put("year", c.get(Calendar.YEAR) + 1900);
			else {
				if (c.get(Calendar.YEAR) > Calendar.getInstance().get(Calendar.YEAR))
					model.put("year", c.get(Calendar.YEAR) - 100);
				else
					model.put("year", c.get(Calendar.YEAR));
			}

			bean.setFname(profile.getFirstName());
			bean.setLname(profile.getLastName());
			bean.setMlname(profile.getMaidenName());

			// Catch any profile null pointers here
			try {

				if (customerService.getDataCustomerProfile(userSession.getBankingSession().getPermId().toString()) != null) {
					String[] data = customerService.getDataCustomerProfile(userSession.getBankingSession().getPermId()
							.toString());

					bean.setDireccion(data[0]);
					bean.setCity(data[1]);
					bean.setZipcode(data[2]);
					bean.setState(data[3]);
					bean.setTele_residencial(data[5]);
				}
			} catch (Exception e) {
			}

			Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsPaymentFrom();

			Map<String, String> accountsFromMap = new LinkedHashMap<String, String>();
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				accountsFromMap.put("none", "Select an account");
				model.put("page", 0); // Main page for Family Care in English
			} else {
				accountsFromMap.put("none", "Selecciona una cuenta");
				model.put("page", 1); // Main page for Spanish
			}

			if (accountsFrom != null) {
				AccountMaskUtil mask = new AccountMaskUtil();
				NumberTool numberTool = new NumberTool();

				String[] accountsList = this.accidentAccounts.split("-");

				List<Object> lc = new ArrayList<Object>();
				for (int i = 0; i < accountsList.length; i++) {
					if (i % 2 != 0) {
						String[] cuentas = accountsList[i].split(",");
						lc.add(cuentas);
					} else {
						lc.add(accountsList[i]);
					}
				}

				for (Iterator<TVAccountTransferSource> iterator = accountsFrom.iterator(); iterator.hasNext();) {
					TVAccountTransferSource from = (TVAccountTransferSource) iterator.next();
					TVFrontendAccount frontFrom = from.getAccount();
					if (frontFrom != null && !frontFrom.getHide()) {
						if (this.isValidAccount(lc, frontFrom.getAccountProductId(), frontFrom.getBankId())
								&& frontFrom.getPortalBalance() != null)
							accountsFromMap.put(
									from.getAccount().getAccountNumber(),
									StringUtils.abbreviate(frontFrom.getNickname(), 15)
											+ " "
											+ mask.mask(frontFrom.getAccountNumber())
											+ " "
											+ numberTool.format("currency", frontFrom.getPortalBalance())
											+ ((frontFrom.getAccountNumberSuffix() != null && !frontFrom
													.getAccountNumberSuffix().equals("")) ? " "
													+ frontFrom.getAccountNumberSuffix() : ""));
					}
				}

			}

			model.put("accounts", accountsFromMap);

		} else {
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
				model.put("page", 10); // Default error page in English
			else
				model.put("page", 11); // Default error page in Spanish

			return model;
		}

		return model;
	}

	private boolean isValidAccount(List<Object> lista, String productId, String bancId) {
		boolean entro = false;
		for (int i = 0; i < lista.size(); i++) {
			if (i % 2 == 0) {
				if (bancId.equals(lista.get(i).toString()))
					entro = true;
				else
					continue;
			} else {
				if (entro) {
					String[] listaCuentas = (String[]) lista.get(i);
					for (int x = 0; x < listaCuentas.length; x++)
						if (listaCuentas[x].equals(productId))
							return true;

					return false;
				}
			}
		}

		return false;
	}

	private void getBankingSession(UserSession userSession, HttpServletRequest request) {
		CustomerProfile profile = userSession.getCustomerProfile();

		if (profile.getEntitlementByType(EntitlementType.BANKING) == null)
			throw new ProgrammingError("Customer profile banking entitlement null");

		if (userSession.getCustomerProfile() != null)
			userSession.setUserSessionType(UserSessionType.CUSTOMER_PROFILE);

		if (userSession.getBankingSession() == null
				&& userSession.getUserSessionType().equals(UserSessionType.CUSTOMER_PROFILE)) {

			// Verify if the Customer initially sent a signon token
			String remoteToken = userSession.getRemoteToken();
			if (!StringUtils.isEmpty(remoteToken))
				throw new ProgrammingError("Remote token no esta vacio (womanCare)");

			CustomerEntitlement ent = profile.getEntitlementByType(EntitlementType.BANKING);
			if (ent != null) {
				evertec.cibp.core.models.entitlement.BankingEntitlement entt = new BankingEntitlement(ent);
				BankingSessionRequest sessionRequest = new BankingSessionRequest(
						Utils.generateCustomerInteractionEvent(request), userSession.getCustomerProfile(), null, entt,
						false);
				BankingSessionResponse bankingresponse = userSession.getCustomerService().getBankingSession(
						sessionRequest);

				if (bankingresponse.getErrorCode() == 0) {
					userSession.setBankingSession(bankingresponse.getBankingSession());
					if (userSession.getCustomerProfile() != null
							&& userSession.getCustomerProfile().getPermId() == null) {
						userSession.getCustomerService().updateCustomerPermId(userSession.getCustomerProfile(),
								bankingresponse.getBankingSession().getPermId());
					}
				} else if (bankingresponse.getErrorCode() == 1880)
					throw new ProgrammingError("User TV blocked (accident)");
				else if (bankingresponse.getErrorCode() == 1740 && userSession.getCustomerProfile() != null
						&& userSession.getAccountApplications(true).getCompletedApplications() != null
						&& userSession.getAccountApplications(true).getCompletedApplications().size() >= 1) {
					throw new ProgrammingError("We cannot redirect you to the portal (accident)");
				} else
					throw new ProgrammingError("Unable to get Customer Banking Session. Reason:"
							+ bankingresponse.getErrorCode());
			} else
				throw new ProgrammingError("Customer Banking Entitlement is not defined.");
		}
	}
}
