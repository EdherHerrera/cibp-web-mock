package evertec.cibp.web.form.insurance;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.insurance.FamilyCare;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.BankingSessionRequest;
import evertec.cibp.core.services.responses.BankingSessionResponse;
import evertec.cibp.web.form.insurance.bean.FamilyCareBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.session.UserSessionType;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class FamilyCareForm extends AbstractWizardFormController {

	private static String INSURED_DEPENDENT = "D";
	private static String INSURED_SPOUSE = "S";
	private static int MAX_DEPENDENT_AGE = 17;

	private String familyCareAccounts;
	private Integer maxAge;
	private Integer minAge;

	public FamilyCareForm() {
		super();
		setCommandClass(FamilyCareBean.class);
		setCommandName("familyCare");
		setPages(new String[] { "insuranceFamilyCare" });
	}

	public String getFamilyCareAccounts() {
		return familyCareAccounts;
	}

	public void setFamilyCareAccounts(String familyCareAccounts) {
		this.familyCareAccounts = familyCareAccounts;
	}

	public Integer getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(Integer maxAge) {
		this.maxAge = maxAge;
	}

	public Integer getMinAge() {
		return minAge;
	}

	public void setMinAge(Integer minAge) {
		this.minAge = minAge;
	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		FamilyCareBean bean = (FamilyCareBean) command;

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		int customerBirthDay = Integer.parseInt(request.getParameter("birthday_day"));
		int customerBirthMonth = Integer.parseInt(request.getParameter("birthday_month")) - 1;
		int customerBirthYear = Integer.parseInt(request.getParameter("birthday_year"));

		Date customerBirthDate;
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.set(customerBirthYear, customerBirthMonth, customerBirthDay);
		customerBirthDate = calendar.getTime();

		// GENERO INDEFINIDO
		char gender = 'I';

		if (customerService.getDataCustomerProfile(userSession.getBankingSession().getPermId().toString()) != null) {
			String[] data = customerService.getDataCustomerProfile(userSession.getBankingSession().getPermId()
					.toString());
			if (data != null) {
				if (data[4] != null && !data[4].trim().equals("")) {
					gender = data[4].toLowerCase().charAt(0);
				}
			} else {
				gender = customerService.getGenderCustomerProfile(userSession.getBankingSession().getPermId()
						.toString());
			}
		}

		if (bean.getCustomerAge() <= this.maxAge && bean.getCustomerAge() >= this.minAge) {

			FamilyCare familyCare = new FamilyCare();
			familyCare.setAmount(Double.parseDouble(bean.getAmount().replaceAll("[$|,]", "").trim()));

			familyCare.setBeneficiary1Name(bean.getBeneficiario_1());
			familyCare.setBeneficiary1LastName(bean.getBeneficiario_1_1());
			familyCare.setBeneficiary1Percent(bean.getPorcentaje_1());

			familyCare.setBeneficiary2Name(bean.getBeneficiario_2());
			familyCare.setBeneficiary2LastName(bean.getBeneficiario_2_2());
			familyCare.setBeneficiary2Percent(bean.getPorcentaje_2());

			familyCare.setBeneficiary3Name(bean.getBeneficiario_3());
			familyCare.setBeneficiary3LastName(bean.getBeneficiario_3_3());
			familyCare.setBeneficiary3Percent(bean.getPorcentaje_3());

			familyCare.setBeneficiary4Name(bean.getBeneficiario_4());
			familyCare.setBeneficiary4LastName(bean.getBeneficiario_4_4());
			familyCare.setBeneficiary4Percent(bean.getPorcentaje_4());

			familyCare.setCustomerAccount(bean.getCustomerAccount());
			familyCare.setCustomerAddress(bean.getDireccion());
			familyCare.setCustomerAge(bean.getCustomerAge());
			familyCare.setCustomerCity(bean.getCity());
			// familyCare.setCustomerDateOfBirth(userSession.getCustomerProfile().getBirthDate());
			// Se usaba la del profile del customer y no la que el customer
			// escogia/cambiaba
			familyCare.setCustomerDateOfBirth(customerBirthDate);
			familyCare.setCustomerLastName(bean.getLname());
			familyCare.setCustomerMaidenName(bean.getMlname());
			familyCare.setCustomerName(bean.getFname());
			familyCare.setCustomerSsn(userSession.getCustomerProfile().getTaxId());
			familyCare.setCustomerState(bean.getState());
			familyCare.setCustomerZipCode(bean.getZipcode());
			familyCare.setHomePhone(bean.getTele_residencial());
			familyCare.setWorkPhone(bean.getTele_trabajo());
			familyCare.setStatus(FamilyCare.STATUS_SIN_PROCESAR);
			familyCare.setPlan(Integer.parseInt(bean.getPlan()));

			if (gender == ' ')
				familyCare.setGender("M");
			else
				familyCare.setGender(String.valueOf(gender).toUpperCase());

			familyCare.setDependent1Name(bean.getDependiente_1());
			familyCare.setDependent1LastName(bean.getDependiente_1_1());

			familyCare.setDependent2Name(bean.getDependiente_2());
			familyCare.setDependent2LastName(bean.getDependiente_2_2());

			familyCare.setDependent3Name(bean.getDependiente_3());
			familyCare.setDependent3LastName(bean.getDependiente_3_3());

			familyCare.setDependent4Name(bean.getDependiente_4());
			familyCare.setDependent4LastName(bean.getDependiente_4_4());

			familyCare.setDependent5Name(bean.getDependiente_5());
			familyCare.setDependent5LastName(bean.getDependiente_5_5());

			familyCare.setDependent6Name(bean.getDependiente_6());
			familyCare.setDependent6LastName(bean.getDependiente_6_6());

			familyCare.setDependent7Name(bean.getDependiente_7());
			familyCare.setDependent7LastName(bean.getDependiente_7_7());

			if (bean.getRelacion_1_mes() > 0 && bean.getRelacion_1_dia() > 0 && bean.getRelacion_1_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_1_anio(), bean.getRelacion_1_mes() - 1, bean.getRelacion_1_dia(), 0, 0, 0);
				familyCare.setDependent1DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1100
				familyCare.setDependent1Relationship(bean.getRelacion_1()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_1()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					familyCare.setDependent1Relationship(INSURED_DEPENDENT); // if
																				// dependent
																				// was
																				// chosen,
																				// a
																				// T
																				// is
																				// sent
																				// by
																				// default
			}

			if (bean.getRelacion_2_mes() > 0 && bean.getRelacion_2_dia() > 0 && bean.getRelacion_2_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_2_anio(), bean.getRelacion_2_mes() - 1, bean.getRelacion_2_dia(), 0, 0, 0);
				familyCare.setDependent2DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1100
				familyCare.setDependent2Relationship(bean.getRelacion_2()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_2()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					familyCare.setDependent2Relationship(INSURED_DEPENDENT); // if
																				// dependent
																				// was
																				// chosen,
																				// a
																				// T
																				// is
																				// sent
																				// by
																				// default
			}

			if (bean.getRelacion_3_mes() > 0 && bean.getRelacion_3_dia() > 0 && bean.getRelacion_3_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_3_anio(), bean.getRelacion_3_mes() - 1, bean.getRelacion_3_dia(), 0, 0, 0);
				familyCare.setDependent3DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1100
				familyCare.setDependent3Relationship(bean.getRelacion_3()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_3()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					familyCare.setDependent3Relationship(INSURED_DEPENDENT); // if
																				// dependent
																				// was
																				// chosen,
																				// a
																				// T
																				// is
																				// sent
																				// by
																				// default
			}

			if (bean.getRelacion_4_mes() > 0 && bean.getRelacion_4_dia() > 0 && bean.getRelacion_4_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_4_anio(), bean.getRelacion_4_mes() - 1, bean.getRelacion_4_dia(), 0, 0, 0);
				familyCare.setDependent4DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1100
				familyCare.setDependent4Relationship(bean.getRelacion_4()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_4()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					familyCare.setDependent4Relationship(INSURED_DEPENDENT); // if
																				// dependent
																				// was
																				// chosen,
																				// a
																				// T
																				// is
																				// sent
																				// by
																				// default
			}

			if (bean.getRelacion_5_mes() > 0 && bean.getRelacion_5_dia() > 0 && bean.getRelacion_5_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_5_anio(), bean.getRelacion_5_mes() - 1, bean.getRelacion_5_dia(), 0, 0, 0);
				familyCare.setDependent5DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1100
				familyCare.setDependent5Relationship(bean.getRelacion_5()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_5()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					familyCare.setDependent5Relationship(INSURED_DEPENDENT); // if
																				// dependent
																				// was
																				// chosen,
																				// a
																				// T
																				// is
																				// sent
																				// by
																				// default
			}

			if (bean.getRelacion_6_mes() > 0 && bean.getRelacion_6_dia() > 0 && bean.getRelacion_6_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_6_anio(), bean.getRelacion_6_mes() - 1, bean.getRelacion_6_dia(), 0, 0, 0);
				familyCare.setDependent6DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1100
				familyCare.setDependent6Relationship(bean.getRelacion_6()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_6()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					familyCare.setDependent6Relationship(INSURED_DEPENDENT); // if
																				// dependent
																				// was
																				// chosen,
																				// a
																				// T
																				// is
																				// sent
																				// by
																				// default
			}

			if (bean.getRelacion_7_mes() > 0 && bean.getRelacion_7_dia() > 0 && bean.getRelacion_7_anio() > 0) {
				Calendar cal = Calendar.getInstance();
				cal.set(bean.getRelacion_7_anio(), bean.getRelacion_7_mes() - 1, bean.getRelacion_7_dia(), 0, 0, 0);
				familyCare.setDependent7DateOfBirth(cal.getTime());

				// CIBP-2594 defect 1100
				familyCare.setDependent7Relationship(bean.getRelacion_7()); // set
																			// what
																			// was
																			// received
																			// from
																			// the
																			// view
				if (!INSURED_SPOUSE.equals(bean.getRelacion_7()) && Utils.getAge(cal.getTime()) <= MAX_DEPENDENT_AGE)
					familyCare.setDependent7Relationship(INSURED_DEPENDENT); // if
																				// dependent
																				// was
																				// chosen,
																				// a
																				// T
																				// is
																				// sent
																				// by
																				// default
			}

			try {
				if (!customerService.existFamilyCare(familyCare)) {
					customerService.saveFamilyCare(familyCare);

					int age = familyCare.getCustomerAge();
					// Successful - show a different page for men and women
					if (gender == 'f' && age < 65) {
						if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
							model.put("page", 2); // Thank you page for women in
													// English
						else
							model.put("page", 3); // Thank you page for women in
													// Spanish
					} else if (gender == 'm') {
						if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
							model.put("page", 4); // Thank you page for men in
													// English
						else
							model.put("page", 5); // Thank you page for men in
													// Spanish
					} else {
						if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
							model.put("page", 6); // Thank you page for
													// undetermined gender in
													// English
						else
							model.put("page", 7); // Thank you page for
													// undetermined gender in
													// Spanish
					}
				} else {
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
						model.put("page", 8); // Duplicate page in English
					else
						model.put("page", 9); // Duplicate page in Spanish
				}
			} catch (Exception e) {
				model.put("error", "Error: " + e.getMessage());
				return new ModelAndView("forward:insuranceFamilyCare", model);
			}
		} else {
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
				model.put("page", 10); // Default error page in English
			else
				model.put("page", 11); // Default error page in Spanish
		}

		return new ModelAndView("insuranceFamilyCare", model);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		Map<String, Object> model = new HashMap<>();
		FamilyCareBean bean = (FamilyCareBean) command;

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		if (request.getParameter("nothanks") != null) {
			userSession.setPromotion("");
			throw new ModelAndViewDefiningException(new ModelAndView("redirect:portal"));
		}

		if (userSession.getBankingSession() == null) {
			this.getBankingSession(userSession, request);
			if (userSession.getBankingSession() == null)
				throw new ProgrammingError("We could not establish a banking session (familyCare)");
		}

		Date dateage = profile.getBirthDate();
		int age = Utils.getAge(dateage);
		model.put("customerAge", age);

		if (age <= this.maxAge && age >= this.minAge) {

			model.put("maxAge", this.maxAge);
			model.put("minAge", this.minAge);
			Calendar c = Calendar.getInstance();
			c.setTime(dateage);
			model.put("day", c.get(Calendar.DAY_OF_MONTH));
			model.put("month", c.get(Calendar.MONTH) + 1);
			if (c.get(Calendar.YEAR) < 100)
				model.put("year", c.get(Calendar.YEAR) + 1900);
			else {
				if (c.get(Calendar.YEAR) > Calendar.getInstance().get(Calendar.YEAR))
					model.put("year", c.get(Calendar.YEAR) - 100);
				else
					model.put("year", c.get(Calendar.YEAR));
			}

			bean.setFname(profile.getFirstName());
			bean.setLname(profile.getLastName());
			bean.setMlname(profile.getMaidenName());

			// Catch any profile null pointers here
			try {

				if (customerService.getDataCustomerProfile(userSession.getBankingSession().getPermId().toString()) != null) {
					String[] data = customerService.getDataCustomerProfile(userSession.getBankingSession().getPermId()
							.toString());

					bean.setDireccion(data[0]);
					bean.setCity(data[1]);
					bean.setZipcode(data[2]);
					bean.setState(data[3]);
					bean.setTele_residencial(data[5]);
				}
			} catch (Exception e) {
			}

			Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsPaymentFrom();

			Map<String, String> accountsFromMap = new LinkedHashMap<String, String>();
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				accountsFromMap.put("none", "Select an account");
				model.put("page", 0); // Main page for Family Care in English
			} else {
				accountsFromMap.put("none", "Selecciona una cuenta");
				model.put("page", 1); // Main page for Spanish
			}

			if (accountsFrom != null) {
				AccountMaskUtil mask = new AccountMaskUtil();
				NumberTool numberTool = new NumberTool();

				String[] accountsList = this.familyCareAccounts.split("-");

				List<Object> lc = new ArrayList<Object>();
				for (int i = 0; i < accountsList.length; i++) {
					if (i % 2 != 0) {
						String[] cuentas = accountsList[i].split(",");
						lc.add(cuentas);
					} else {
						lc.add(accountsList[i]);
					}
				}

				for (Iterator<TVAccountTransferSource> iterator = accountsFrom.iterator(); iterator.hasNext();) {
					TVAccountTransferSource from = (TVAccountTransferSource) iterator.next();
					TVFrontendAccount frontFrom = from.getAccount();
					if (frontFrom != null && !frontFrom.getHide()) {
						if (this.isValidAccount(lc, frontFrom.getAccountProductId(), frontFrom.getBankId())
								&& frontFrom.getPortalBalance() != null)
							accountsFromMap.put(
									from.getAccount().getAccountNumber(),
									StringUtils.abbreviate(frontFrom.getNickname(), 15)
											+ " "
											+ mask.mask(frontFrom.getAccountNumber())
											+ " "
											+ numberTool.format("currency", frontFrom.getPortalBalance())
											+ ((frontFrom.getAccountNumberSuffix() != null && !frontFrom
													.getAccountNumberSuffix().equals("")) ? " "
													+ frontFrom.getAccountNumberSuffix() : ""));
					}
				}

			}

			model.put("accounts", accountsFromMap);

		} else {
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
				model.put("page", 10); // Default error page in English
			else
				model.put("page", 11); // Default error page in Spanish

			return model;
		}

		return model;
	}

	private boolean isValidAccount(List<Object> lista, String productId, String bancId) {
		boolean entro = false;
		for (int i = 0; i < lista.size(); i++) {
			if (i % 2 == 0) {
				if (bancId.equals(lista.get(i).toString()))
					entro = true;
				else
					continue;
			} else {
				if (entro) {
					String[] listaCuentas = (String[]) lista.get(i);
					for (int x = 0; x < listaCuentas.length; x++)
						if (listaCuentas[x].equals(productId))
							return true;

					return false;
				}
			}
		}

		return false;
	}

	private void getBankingSession(UserSession userSession, HttpServletRequest request) {
		CustomerProfile profile = userSession.getCustomerProfile();

		if (profile.getEntitlementByType(EntitlementType.BANKING) == null)
			throw new ProgrammingError("Customer profile banking entitlement null");

		if (userSession.getCustomerProfile() != null)
			userSession.setUserSessionType(UserSessionType.CUSTOMER_PROFILE);

		if (userSession.getBankingSession() == null
				&& userSession.getUserSessionType().equals(UserSessionType.CUSTOMER_PROFILE)) {

			// Verify if the Customer initially sent a signon token
			String remoteToken = userSession.getRemoteToken();
			if (!StringUtils.isEmpty(remoteToken))
				throw new ProgrammingError("Remote token no esta vacio (womanCare)");

			CustomerEntitlement ent = profile.getEntitlementByType(EntitlementType.BANKING);
			if (ent != null) {
				evertec.cibp.core.models.entitlement.BankingEntitlement entt = new BankingEntitlement(ent);
				BankingSessionRequest sessionRequest = new BankingSessionRequest(
						Utils.generateCustomerInteractionEvent(request), userSession.getCustomerProfile(), null, entt,
						false);
				BankingSessionResponse bankingresponse = userSession.getCustomerService().getBankingSession(
						sessionRequest);

				if (bankingresponse.getErrorCode() == 0) {
					userSession.setBankingSession(bankingresponse.getBankingSession());
					if (userSession.getCustomerProfile() != null
							&& userSession.getCustomerProfile().getPermId() == null) {
						userSession.getCustomerService().updateCustomerPermId(userSession.getCustomerProfile(),
								bankingresponse.getBankingSession().getPermId());
					}
				} else if (bankingresponse.getErrorCode() == 1880)
					throw new ProgrammingError("User TV blocked (familyCare)");
				else if (bankingresponse.getErrorCode() == 1740 && userSession.getCustomerProfile() != null
						&& userSession.getAccountApplications(true).getCompletedApplications() != null
						&& userSession.getAccountApplications(true).getCompletedApplications().size() >= 1) {
					throw new ProgrammingError("We cannot redirect you to the portal (familyCare)");
				} else
					throw new ProgrammingError("Unable to get Customer Banking Session. Reason:"
							+ bankingresponse.getErrorCode());
			} else
				throw new ProgrammingError("Customer Banking Entitlement is not defined.");
		}
	}
}
