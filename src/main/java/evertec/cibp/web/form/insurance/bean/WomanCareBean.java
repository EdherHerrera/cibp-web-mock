/**
 * 
 */
package evertec.cibp.web.form.insurance.bean;

import java.io.Serializable;

/**
 * Clase evertec.cibp.web.form.insurance.bean.WomanCareBean Sistema: Mi Banco -
 * Insurance Descripcion: Clase que representa los datos de la pagina de
 * womanCare
 * 
 * @author Ing. Michael Picado Fuentes Creada el Oct 25, 2010 11:03:00 AM
 */

public class WomanCareBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1508461114453353571L;
	private String customerName;
	private String customerAddress;
	private String customerLastName;
	private String customerMaidenName;
	private String customerCity;
	private String customerState;
	private String customerZipCode;
	private int customerAge;
	private String customerAccount;
	private int plan;// plan 1,2 o 3 los que hay hasta el momento
	private String amount;
	private boolean hasCancer;

	// mpicado,campo nuevo solicitado por insurance 212/12/2010
	private String customerPhoneNumber;

	/* Constructor de la clase WomanCareBean */
	public WomanCareBean() {
		// TODO Auto-generated constructor stub
	}

	/** @return regresa el customerName */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            el customerName a establecer
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/** @return regresa el customerAddress */
	public String getCustomerAddress() {
		return customerAddress;
	}

	/**
	 * @param customerAddress
	 *            el customerAddress a establecer
	 */
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	/** @return regresa el customerLastName */
	public String getCustomerLastName() {
		return customerLastName;
	}

	/**
	 * @param customerLastName
	 *            el customerLastName a establecer
	 */
	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	/** @return regresa el customerMaidenName */
	public String getCustomerMaidenName() {
		return customerMaidenName;
	}

	/**
	 * @param customerMaidenName
	 *            el customerMaidenName a establecer
	 */
	public void setCustomerMaidenName(String customerMaidenName) {
		this.customerMaidenName = customerMaidenName;
	}

	/** @return regresa el customerCity */
	public String getCustomerCity() {
		return customerCity;
	}

	/**
	 * @param customerCity
	 *            el customerCity a establecer
	 */
	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	/** @return regresa el customerState */
	public String getCustomerState() {
		return customerState;
	}

	/**
	 * @param customerState
	 *            el customerState a establecer
	 */
	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}

	/** @return regresa el customerZipCode */
	public String getCustomerZipCode() {
		return customerZipCode;
	}

	/**
	 * @param customerZipCode
	 *            el customerZipCode a establecer
	 */
	public void setCustomerZipCode(String customerZipCode) {
		this.customerZipCode = customerZipCode;
	}

	/** @return regresa el customerAge */
	public int getCustomerAge() {
		return customerAge;
	}

	/**
	 * @param customerAge
	 *            el customerAge a establecer
	 */
	public void setCustomerAge(int customerAge) {
		this.customerAge = customerAge;
	}

	/** @return regresa el customerAccount */
	public String getCustomerAccount() {
		return customerAccount;
	}

	/**
	 * @param customerAccount
	 *            el customerAccount a establecer
	 */
	public void setCustomerAccount(String customerAccount) {
		this.customerAccount = customerAccount;
	}

	/** @return regresa el plan */
	public int getPlan() {
		return plan;
	}

	/**
	 * @param plan
	 *            el plan a establecer
	 */
	public void setPlan(int plan) {
		this.plan = plan;
	}

	/** @return regresa el amount */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            el amount a establecer
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/** @return regresa el hasCancer */
	public boolean isHasCancer() {
		return hasCancer;
	}

	/**
	 * @param hasCancer
	 *            el hasCancer a establecer
	 */
	public void setHasCancer(boolean hasCancer) {
		this.hasCancer = hasCancer;
	}

	/** @return regresa el customerPhoneNumber */
	public String getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}

	/**
	 * @param customerPhoneNumber
	 *            el customerPhoneNumber a establecer
	 */
	public void setCustomerPhoneNumber(String customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}

}
