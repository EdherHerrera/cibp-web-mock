/**
 * 
 */
package evertec.cibp.web.form.insurance;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.insurance.WomanCare;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.BankingSessionRequest;
import evertec.cibp.core.services.responses.BankingSessionResponse;
import evertec.cibp.web.form.insurance.bean.WomanCareBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.session.UserSessionType;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

/**
 * Clase evertec.cibp.web.form.insurance.WomanCareForm Sistema: Mi banco -
 * Insurance Descripcion: Clase que maneja la campana Woman Care Insurance
 * 
 * @author Ing. Michael Picado Fuentes Creada el Oct 25, 2010 10:03:30 AM
 */

public class WomanCareForm extends AbstractWizardFormController {

	/**
	 * atributo <code>cuentasWomanCareInsurance del tipo String</code> estas son
	 * las cuentas disponibles para pagar los seguros womanCare
	 */
	private String cuentasWomanCareInsurance;
	/**
	 * atributo <code>edadPermitida del tipo Integer</code> edad maxima
	 * permitida para aplicar en la solicitud
	 */
	private Integer edadPermitida;

	/* Constructor de la clase WomanCareForm */
	public WomanCareForm() {
		super();
		setCommandClass(WomanCareBean.class);
		setCommandName("womanCare");
		setPages(new String[] { "insuranceWomanCare" });
	}

	/** @return regresa el cuentasWomanCareInsurance */
	public String getCuentasWomanCareInsurance() {
		return cuentasWomanCareInsurance;
	}

	/**
	 * @param cuentasWomanCareInsurance
	 *            el cuentasWomanCareInsurance a establecer
	 */
	public void setCuentasWomanCareInsurance(String cuentasWomanCareInsurance) {
		this.cuentasWomanCareInsurance = cuentasWomanCareInsurance;
	}

	public Integer getEdadPermitida() {
		return edadPermitida;
	}

	public void setEdadPermitida(Integer edadPermitida) {
		this.edadPermitida = edadPermitida;
	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		WomanCareBean wcb = (WomanCareBean) command;
		int customerBirthDay = Integer.parseInt(request.getParameter("birthday_day"));
		int customerBirthMonth = Integer.parseInt(request.getParameter("birthday_month")) - 1;
		int customerBirthYear = Integer.parseInt(request.getParameter("birthday_year"));

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		Date customerBirthDate;
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.set(customerBirthYear, customerBirthMonth, customerBirthDay);
		customerBirthDate = calendar.getTime();

		// hay que volver a verificar por la edad por si en el formulario se
		// cambio la misma, mpicado, cibp-2261
		if (wcb.getCustomerAge() <= this.edadPermitida) {

			/*
			 * se valida si ha tenido cancer de seno utero u ovarios, si no lo
			 * posee tiene derecho a optar por el seguro sino, no cumple con los
			 * requisitos
			 */
			if (!wcb.isHasCancer()) {

				WomanCare womanCare = new WomanCare(userSession.getCustomerProfile().getTaxId(), wcb.getCustomerName(),
						wcb.getCustomerAddress(), wcb.getCustomerLastName(), wcb.getCustomerMaidenName(),
						wcb.getCustomerCity(), wcb.getCustomerState(), wcb.getCustomerZipCode(),
						wcb.getCustomerPhoneNumber(), wcb.getCustomerAge(), customerBirthDate,
						wcb.getCustomerAccount(), wcb.getPlan(), Double.parseDouble(wcb.getAmount()
								.replaceAll("[$|,]", "").trim()), WomanCare.STATUS_SIN_PROCESAR);
				try {
					if (!customerService.existWomanCare(womanCare)) {
						customerService.saveWomanCare(womanCare);
						if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
							model.put("page", 5);// pagina de gracias en ingles
						else
							model.put("page", 2);// pagina de gracias en espanol
					} else {
						if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
							model.put("page", 7);// pagina de error de duplicado
													// ingles
						else
							model.put("page", 6);// pagina de error de duplicado
													// espanol
					}
				} catch (Exception e) {
					System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
					model.put("error", "Error: unable to submit your information");
					return new ModelAndView("forward:iwc", model);
				}
			} else {
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
					model.put("page", 9);// pagina diagnosticada con cancer en
											// ingles
				else
					model.put("page", 8);// pagina diagnosticada con cancer en
											// espanol
			}
		}

		else {

			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
				model.put("page", 4);// pagina de error en ingles
			else
				model.put("page", 1);// pagina de error en espanol
		}

		return new ModelAndView("insuranceWomanCare", model);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

	}

	private int retornaEdad(Date dateEdad) {
		Calendar c = Calendar.getInstance();
		int actualYear = c.get(Calendar.YEAR);
		int actualMonth = c.get(Calendar.MONTH);
		int actualDay = c.get(Calendar.DAY_OF_MONTH);
		if (actualDay == 0)// el dia 0 es domingo
			actualDay = 7;

		Calendar cEdad = Calendar.getInstance();
		cEdad.setTime(dateEdad);
		int dia = cEdad.get(Calendar.DAY_OF_MONTH);
		int mes = cEdad.get(Calendar.MONTH);
		int anno = cEdad.get(Calendar.YEAR);
		/*
		 * las fechas de nacimiento de mi banco estan solo con dos digitos por
		 * lo que esto da problemas, la fecha real y logica seria sumarle 1900 a
		 * ese numero, por lo que si en mi banco dice que nacio en el 84 el anno
		 * real seria 1984, existen casos en que java entiende fechas como si
		 * fueran del 2000 por ejemplo, en la base de datos hay un 46 y MB lo
		 * interpreta como un 2046, para eso tambien se puso una condicion, lo
		 * ideal seria que en la DB se guarde el anno con los cuatro digitos.
		 * Esto no aplica para los nacidos despues del 2000 ya que no se tendria
		 * la edad suficiente para tener mayoria de edad...(por el momento,
		 * 2011)
		 */
		if (anno < 100)
			anno += 1900;
		else {
			if (anno > actualYear)
				anno -= 100;
		}

		int edad = actualYear - anno;
		if (mes > actualMonth)
			edad--;
		else {
			if (actualMonth == mes) {
				if (dia > actualDay)
					edad--;
			}
		}

		return edad;
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		Map<String, Object> model = new HashMap<>();
		WomanCareBean wcb = (WomanCareBean) command;

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		if (request.getParameter("nothanks") != null) {
			userSession.setPromotion("");
			throw new ModelAndViewDefiningException(new ModelAndView("redirect:portal"));
		}

		// valido que exista el banking session para poder enviar el permid del
		// signon a la bobeda
		if (userSession.getBankingSession() == null) {
			this.getBankingSession(userSession, request);
			if (userSession.getBankingSession() == null)
				throw new ProgrammingError("No se pudo establecer el banking session (womanCare)");
		}

		String[] data = customerService.getDataCustomerProfile(userSession.getBankingSession().getPermId().toString());
		char gender = ' ';
		if (data != null) {
			gender = data[4].toLowerCase().charAt(0);
		} else {
			data = new String[6];
			gender = customerService.getGenderCustomerProfile(userSession.getBankingSession().getPermId().toString());
		}

		if (gender == 'f') {// genero femenino

			Date dateEdad = profile.getBirthDate();
			int edad = this.retornaEdad(dateEdad);
			if (edad <= this.edadPermitida) {// edad limite para mostrar esta
												// pagina
				model.put("edadPermitida", this.edadPermitida);
				Calendar c = Calendar.getInstance();
				c.setTime(dateEdad);
				model.put("dia", c.get(Calendar.DAY_OF_MONTH));
				model.put("mes", c.get(Calendar.MONTH) + 1); // le sumo uno ya
																// que en el
																// date los
																// meses van de
																// 0 a 11
				/*
				 * las fechas de nacimiento de mi banco estan solo con dos
				 * digitos por lo que esto da problemas, la fecha real y logica
				 * seria sumarle 1900 a ese numero, por lo que si en mi banco
				 * dice que nacio en el 84 el anno real seria 1984, esto no
				 * aplica para los nacidos despues del 2000 ya que no se tendria
				 * la edad suficiente para tener mayoria de edad...
				 */
				if (c.get(Calendar.YEAR) < 100)
					model.put("anno", c.get(Calendar.YEAR) + 1900);
				else {
					if (c.get(Calendar.YEAR) > Calendar.getInstance().get(Calendar.YEAR))
						model.put("anno", c.get(Calendar.YEAR) - 100);
					else
						model.put("anno", c.get(Calendar.YEAR));
				}

				wcb.setCustomerName(profile.getFirstName());
				wcb.setCustomerLastName(profile.getLastName());
				wcb.setCustomerMaidenName(profile.getMaidenName());

				wcb.setCustomerAddress(data[0]);
				wcb.setCustomerCity(data[1]);
				wcb.setCustomerZipCode(data[2]);
				wcb.setCustomerState(data[3]);
				wcb.setCustomerPhoneNumber(data[5]);

				/*
				 * mpicado, pongo los valores del radio para saber si ya ha
				 * tenido cancer de utero y entre otros, funcionalidad nueva
				 * solicitada por Rafa Bretana el 13/12//2010, agregada el
				 * 14/12/2010
				 */
				Map<Object, String> cancerValues = new LinkedHashMap<Object, String>();
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
					cancerValues.put(true, "Yes");
				else
					cancerValues.put(true, "Si");

				cancerValues.put(false, "No");
				model.put("cancerValues", cancerValues);

				Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsPaymentFrom();

				Map<String, String> accountsFromMap = new LinkedHashMap<String, String>();
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
					accountsFromMap.put("none", "Select an account");
					model.put("page", 3);// pagina principal de woman care en
											// ingles
				} else {
					accountsFromMap.put("none", "Selecciona una cuenta");
					model.put("page", 0);// pagina principal de woman care en
											// espanol
				}

				if (accountsFrom != null) {
					AccountMaskUtil mask = new AccountMaskUtil();
					NumberTool numberTool = new NumberTool();

					// se obtiene la lista de cuentas para cada banco
					String[] listaBancos = this.cuentasWomanCareInsurance.split("-");

					List<Object> lc = new ArrayList<Object>();
					for (int i = 0; i < listaBancos.length; i++) {
						if (i % 2 != 0) {// aqui se agregarian las cuentas para
											// cada codigo de banco, en los
											// numeros impares, empezando por 1
							String[] cuentas = listaBancos[i].split(",");
							lc.add(cuentas);
						} else {
							lc.add(listaBancos[i]);// este seria el codigo del
													// banco, en las posiciones
													// pares empezando con 0
						}
					}

					for (Iterator<TVAccountTransferSource> iterator = accountsFrom.iterator(); iterator.hasNext();) {
						TVAccountTransferSource from = (TVAccountTransferSource) iterator.next();
						TVFrontendAccount frontFrom = from.getAccount();
						if (frontFrom != null && !frontFrom.getHide()) {
							// if(frontFrom != null &&
							// frontFrom.getSubtype().equals("IDA") &&
							// (frontFrom.getAccountNumberSuffix()== null || (
							// frontFrom.getAccountNumberSuffix()!= null &&
							// frontFrom.getAccountNumberSuffix().equals("SAV")
							// )) && frontFrom.getPortalBalance() != null)
							// solo las cuentas de chekings o savings son
							// permitidas
							if (this.isValidAccount(lc, frontFrom.getAccountProductId(), frontFrom.getBankId())
									&& frontFrom.getPortalBalance() != null)
								accountsFromMap.put(
										from.getAccount().getAccountNumber(),
										StringUtils.abbreviate(frontFrom.getNickname(), 15)
												+ " "
												+ mask.mask(frontFrom.getAccountNumber())
												+ " "
												+ numberTool.format("currency", frontFrom.getPortalBalance())
												+ ((frontFrom.getAccountNumberSuffix() != null && !frontFrom
														.getAccountNumberSuffix().equals("")) ? " "
														+ frontFrom.getAccountNumberSuffix() : ""));
						}
					}

				}

				model.put("accounts", accountsFromMap);

			}// end del if de la edad
			else {
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
					model.put("page", 4);// pagina de error en ingles
				else
					model.put("page", 1);// pagina de error en espanol

				return model;
			}
		} else {// genero masculino
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
				model.put("page", 4);// pagina de error en ingles
			else
				model.put("page", 1);// pagina de error en espanol

			return model;
		}

		return model;
	}

	/**
	 * Metodo isValidAccount Valida que una cuenta sea valida segun la propiedad
	 * especificada en el cibp properties, representada por el atributo
	 * cuentasWomanCareInsurance de esta clase
	 * 
	 * @param lista
	 *            lista con los bancos y las cuentas para cada banco
	 * @param productId
	 *            codigo de producto de la cuenta
	 * @param bancId
	 *            codigo del banco al que pertenece la cuenta
	 * @return true si la cuenta es valida, false de lo contrario
	 */
	private boolean isValidAccount(List<Object> lista, String productId, String bancId) {
		boolean entro = false;
		for (int i = 0; i < lista.size(); i++) {
			if (i % 2 == 0) {// es par, en los campos pares se encuentran los
								// codigos de banco
				if (bancId.equals(lista.get(i).toString()))
					entro = true;
				else
					continue;
			} else {// es impar, en estos campos se encuntran las cuentas del
					// codigo de banco de la posicion anterior
				if (entro) {
					String[] listaCuentas = (String[]) lista.get(i);
					for (int x = 0; x < listaCuentas.length; x++)
						if (listaCuentas[x].equals(productId))
							return true;

					return false;
				}
			}
		}

		return false;
	}

	/**
	 * Metodo getBankingSession Metodo que intenta obtener el banking session
	 * del usuario si este fuera nulo, esto por si se enrta a esta pagina desde
	 * un link externo a mi banco y no se ha entrado al portal que es donde se
	 * carga el banking session
	 */
	private void getBankingSession(UserSession userSession, HttpServletRequest request) {
		CustomerProfile profile = userSession.getCustomerProfile();

		if (profile.getEntitlementByType(EntitlementType.BANKING) == null)
			throw new ProgrammingError("Customer profile banking entitlement null");

		if (userSession.getCustomerProfile() != null)
			userSession.setUserSessionType(UserSessionType.CUSTOMER_PROFILE);

		if (userSession.getBankingSession() == null
				&& userSession.getUserSessionType().equals(UserSessionType.CUSTOMER_PROFILE)) {

			// Verify if the Customer initially sent a signon token
			String remoteToken = userSession.getRemoteToken();
			if (!StringUtils.isEmpty(remoteToken))
				throw new ProgrammingError("Remote token no esta vacio (womanCare)");

			CustomerEntitlement ent = profile.getEntitlementByType(EntitlementType.BANKING);
			if (ent != null) {
				evertec.cibp.core.models.entitlement.BankingEntitlement entt = new BankingEntitlement(ent);
				BankingSessionRequest sessionRequest = new BankingSessionRequest(
						Utils.generateCustomerInteractionEvent(request), userSession.getCustomerProfile(), null, entt,
						false);
				BankingSessionResponse bankingresponse = userSession.getCustomerService().getBankingSession(
						sessionRequest);

				if (bankingresponse.getErrorCode() == 0) {
					userSession.setBankingSession(bankingresponse.getBankingSession());
					if (userSession.getCustomerProfile() != null
							&& userSession.getCustomerProfile().getPermId() == null) {
						userSession.getCustomerService().updateCustomerPermId(userSession.getCustomerProfile(),
								bankingresponse.getBankingSession().getPermId());
					}
				} else if (bankingresponse.getErrorCode() == 1880)
					throw new ProgrammingError("Usuario tv bloqueado (womanCare)");
				else if (bankingresponse.getErrorCode() == 1740 && userSession.getCustomerProfile() != null
						&& userSession.getAccountApplications(true).getCompletedApplications() != null
						&& userSession.getAccountApplications(true).getCompletedApplications().size() >= 1) {
					throw new ProgrammingError("No se va a redirigir al portal en esta ocasion (womanCare)");
				} else
					throw new ProgrammingError("Unable to get Customer Banking Session. Reason:"
							+ bankingresponse.getErrorCode());
			} else
				throw new ProgrammingError("Customer Banking Entitlement is not defined.");
		}
	}

}
