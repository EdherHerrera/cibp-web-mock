package evertec.cibp.web.form.outreach.bean;

import java.io.Serializable;

public class OutReachBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6470869761117187663L;

	public enum ACTION {
		CONFIRM, SKIP, VALIDATE_ADDRESS, VALIDATE_PERSONAL_INFO, EDIT_ADDRESS, EDIT_PERSONAL_INFO, COMPLETE_FORM;
	}
	
	private String taxId;
	
	private ACTION action;
	private String occupation;
	private String employee;
	private String email;
	private String phoneNumber;
	
	private String addressType;
	
	private String physicalAddressName;
	private String physicalAddressHouse;
	private String physicalAddressStreetType;
	private String physicalAddressStreetName;
	private String physicalAddressCity;
	private String physicalAddressCountry;
	private String physicalAddressPostalCode;
	
	private String occupationDisplay;
	private String address1;
	private String address3;
	private String address2;
	
	private boolean flagOcupation;
	private boolean flagEmployee;
	private boolean flagEmail;
	private boolean flagPhone;
	private boolean flagAddress;
	
	private String newAddress1;
	private String newAddress2;
	private String addressTown;
	private String addressCountry;
	private String addressCountryPR;
	private String addressState;
	private String addressPostalCode;
	private String addressPostalCode2;
	
	public ACTION getAction() {
		return action;
	}

	public void setAction(ACTION action) {
		this.action = action;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return (phoneNumber!=null)?phoneNumber.trim():"";
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getPhysicalAddressName() {
		return physicalAddressName;
	}

	public void setPhysicalAddressName(String physicalAddressName) {
		this.physicalAddressName = physicalAddressName;
	}

	public String getPhysicalAddressHouse() {
		return physicalAddressHouse;
	}

	public void setPhysicalAddressHouse(String physicalAddressHouse) {
		this.physicalAddressHouse = physicalAddressHouse;
	}

	public String getPhysicalAddressStreetType() {
		return physicalAddressStreetType;
	}

	public void setPhysicalAddressStreetType(String physicalAddressStreetType) {
		this.physicalAddressStreetType = physicalAddressStreetType;
	}

	public String getPhysicalAddressStreetName() {
		return physicalAddressStreetName;
	}

	public void setPhysicalAddressStreetName(String physicalAddressStreetName) {
		this.physicalAddressStreetName = physicalAddressStreetName;
	}

	public String getPhysicalAddressCity() {
		return physicalAddressCity;
	}

	public void setPhysicalAddressCity(String physicalAddressCity) {
		this.physicalAddressCity = physicalAddressCity;
	}

	public String getPhysicalAddressCountry() {
		return physicalAddressCountry;
	}

	public void setPhysicalAddressCountry(String physicalAddressCountry) {
		this.physicalAddressCountry = physicalAddressCountry;
	}

	public String getPhysicalAddressPostalCode() {
		return physicalAddressPostalCode;
	}

	public void setPhysicalAddressPostalCode(String physicalAddressPostalCode) {
		this.physicalAddressPostalCode = physicalAddressPostalCode;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getOccupationDisplay() {
		return occupationDisplay;
	}

	public void setOccupationDisplay(String occupationDisplay) {
		this.occupationDisplay = occupationDisplay;
	}

	public boolean isFlagOcupation() {
		return flagOcupation;
	}

	public void setFlagOcupation(boolean flagOcupation) {
		this.flagOcupation = flagOcupation;
	}

	public boolean isFlagEmployee() {
		return flagEmployee;
	}

	public void setFlagEmployee(boolean flagEmployee) {
		this.flagEmployee = flagEmployee;
	}

	public boolean isFlagEmail() {
		return flagEmail;
	}

	public void setFlagEmail(boolean flagEmail) {
		this.flagEmail = flagEmail;
	}

	public boolean isFlagPhone() {
		return flagPhone;
	}

	public void setFlagPhone(boolean flagPhone) {
		this.flagPhone = flagPhone;
	}

	public boolean isFlagAddress() {
		return flagAddress;
	}

	public void setFlagAddress(boolean flagAddress) {
		this.flagAddress = flagAddress;
	}

	public String getNewAddress1() {
		return newAddress1;
	}

	public void setNewAddress1(String newAddress1) {
		this.newAddress1 = newAddress1;
	}

	public String getNewAddress2() {
		return newAddress2;
	}

	public void setNewAddress2(String newAddress2) {
		this.newAddress2 = newAddress2;
	}

	public String getAddressTown() {
		return addressTown;
	}

	public void setAddressTown(String addressTown) {
		this.addressTown = addressTown;
	}

	public String getAddressCountry() {
		return addressCountry;
	}

	public void setAddressCountry(String addressCountry) {
		this.addressCountry = addressCountry;
	}

	public String getAddressCountryPR() {
		return addressCountryPR;
	}

	public void setAddressCountryPR(String addressCountryPR) {
		this.addressCountryPR = addressCountryPR;
	}

	public String getAddressState() {
		return addressState;
	}

	public void setAddressState(String addressState) {
		this.addressState = addressState;
	}

	public String getAddressPostalCode() {
		return addressPostalCode;
	}

	public void setAddressPostalCode(String addressPostalCode) {
		this.addressPostalCode = addressPostalCode;
	}

	public String getAddressPostalCode2() {
		return addressPostalCode2;
	}

	public void setAddressPostalCode2(String addressPostalCode2) {
		this.addressPostalCode2 = addressPostalCode2;
	}

	

}
