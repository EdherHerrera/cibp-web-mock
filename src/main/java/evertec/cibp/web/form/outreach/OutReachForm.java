package evertec.cibp.web.form.outreach;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerActions;
import evertec.cibp.core.models.customer.CustomerCodes;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.CustomerUpdateInCISRequest;
import evertec.cibp.core.services.requests.ModifyAllCustomerEmailsRequest;
import evertec.cibp.core.services.requests.outreach.CISOutreachCustomerInfoRequest;
import evertec.cibp.core.services.requests.outreach.CISOutreachRequest;
import evertec.cibp.core.services.responses.outreach.CISOutreachCustomerInfoResponse;
import evertec.cibp.core.utils.CibpValidationUtils;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.interruptions.OOBInterruptionForm;
import evertec.cibp.web.form.outreach.bean.OutReachBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.Country;
import evertec.cibp.web.utils.Utils;

public class OutReachForm extends CancellableFormController {

	private static final String EMAIL_INVALID = "The email is invalid";

	private static final String OUTREACH_ERROR_GENERIC = "outreach.error.generic";

	private static final String ADDRESS_3_INVALID = "The address 3 is invalid";

	private static final String WEBVIEW = "WebView";
	private static final String USERSESSION = "userSession";
	private static final String USERAGENT = "user-agent";
	private static final String CLOSE = "close";
	private static final String FORMCOMPLETE = "formComplete";
	private static final String MAXSKIPBYUSER = "maxSkipByUser";

	private static final Logger logger = Logger.getLogger(OutReachForm.class);

	public final static String OUTREACH_CUSTOMER_CODE = "OUTREACH";
	private static final String ADDRESS = "address:";
	private static final String MOBILE_PHONE = "mobile:";
	private static final String MAIL = "mail:";
	private static final String EMPLOYER = "employer:";
	private static final String OCCUPATION = "occupation:";
	private static final String PORTALDISPLAY = "portalDisplay";
	private CustomerServices customerServices;
	private MessageSource messageSource;
	private String[] occupationsEng;
	private String[] occupationsEsp;
	private String occupationsRemoved;

	private String[] topCountries;
	
	// MBSFE-691
	private String invalidEmailDomains;
	// END MBSFE-691

	public OutReachForm() {
		super();
		setCommandClass(OutReachBean.class);
		setCommandName("outreach");
		setFormView("outreach");
	}
	
	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException, Exception {
		final UserSession userSession = Utils.getUserSession(request);
		if (userSession.isElegibleToOutreach() && !userSession.isAlreadyShownOutreach()) {
			return super.handleRequest(request,response);
	    } else {
	    	if (request.getParameter("close") != null) {
	    		return super.handleRequest(request,response);
	    	} else {
	    		return OOBInterruptionForm.applyCompatibilityWithOuchReach(userSession);
	    	}
	    }
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		OutReachBean bean = (OutReachBean)super.formBackingObject(request);
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);

		String lang = Utils.getRSALanguage(request);

		if (userSession != null && userSession.getCustomerProfile() != null
				&& !userSession.isQuestionAnswered()) {
			//request to ciscusta
			if(bean.getTaxId() == null){
				CISOutreachCustomerInfoRequest cisRequest = new CISOutreachCustomerInfoRequest();
				cisRequest.setCustomerPermId(userSession.getCustomerProfile().getPermId().toString());
				CISOutreachCustomerInfoResponse cisResponse = this.customerServices.getCISDemographicCustomerInfo(cisRequest);
				if(cisResponse != null){
					String email; 
					if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1874)){
						if(!userSession.getCustomerProfile().getEmail().equalsIgnoreCase(cisResponse.getEmail())){
							bean.setFlagEmail(true);
						}
						email = userSession.getCustomerProfile().getEmail();
					}else{
						email = cisResponse.getEmail();
					}
					bean.setEmail(email);
					bean.setEmployee(cisResponse.getEmployee());
					if(cisResponse.getPhoneNumber().length() == 10){
						bean.setPhoneNumber(cisResponse.getPhoneNumber().substring(0, 3) + "-"
											+ cisResponse.getPhoneNumber().substring(3, 6) + "-"
											+ cisResponse.getPhoneNumber().substring(6, 10));
					}
					else{
						bean.setPhoneNumber("");
					}
					bean.setAddress1(cisResponse.getAddress1());
					bean.setAddress2(cisResponse.getAddress2());
					bean.setAddress3(cisResponse.getAddress3());
					bean.setEmployee(cisResponse.getEmployee());
					bean.setTaxId(userSession.getCustomerProfile().getTaxId());

					if(cisResponse.getOccupation() != null && !cisResponse.getOccupation().isEmpty()){
						if(!this.occupationsRemoved.contains(cisResponse.getOccupation())){
							if(lang.equals("en")){
								for(int i = 0; i < this.occupationsEng.length; i++){
									String[] occupation = this.occupationsEng[i].split("-");
									if(occupation.length == 2 && occupation[0].equals(cisResponse.getOccupation())
											&& !this.occupationsRemoved.contains(cisResponse.getOccupation())){
										bean.setOccupation(occupation[0]);
										bean.setOccupationDisplay(occupation[1]);
										break;
									}
								}
							}
							else{
								for(int i = 0; i < this.occupationsEsp.length; i++){
									String[] occupation = this.occupationsEsp[i].split("-");
									if(occupation.length == 2 && occupation[0].equals(cisResponse.getOccupation())
											&& !this.occupationsRemoved.contains(cisResponse.getOccupation())){
										bean.setOccupation(occupation[0]);
										bean.setOccupationDisplay(occupation[1]);
										break;
									}
								}
							}
						}
					}
					if(bean.getOccupation() == null || bean.getOccupation().equals("")){
						bean.setOccupation("-1");
						bean.setOccupationDisplay("");
					}
				}
				else{
					Locale language = new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang );
					userSession.setStatusmessage(this.messageSource.getMessage("outreach.cis.error",null, language));
					userSession.setOutReachConfirmation("outreach.cis.error");
				}
			}
		}

		return bean;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		OutReachBean bean = (OutReachBean) command;
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		if (userSession == null) {
				String header = request.getHeader(USERAGENT);
				if (header != null && header.contains(WEBVIEW))
					return new ModelAndView("redirect:outreach?close&noSession", CLOSE, true);
				else
					return new ModelAndView("redirect:login", "noSessionError", null);
		} else if (bean.getAction() == OutReachBean.ACTION.CONFIRM)
			return confirm(request, response, bean, userSession, error);
		else if (bean.getAction() == OutReachBean.ACTION.SKIP)
			return skipProcess(request, response, bean, userSession, error);
		else if( bean.getAction() == OutReachBean.ACTION.COMPLETE_FORM){
			userSession.setAlreadyShownOutreach(true);
			userSession.setOutReachConfirmation("");
			String header = request.getHeader(USERAGENT);
	        if (isMobile(userSession, header)){
	        	return new ModelAndView("redirect:outreach?close", "close", true);
	        }
			return new ModelAndView(SignonSequenceUtils.getNextView(userSession));
		}
		else
			return showForm(request, response, error, model);
	}


	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		model.put(PORTALDISPLAY, false);
		if (userSession == null) {
			model.put("noSessionError", true);
			return model;
		}
		String lang = Utils.getRSALanguage(request);
		Locale language = new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang );
		//if tax_id is null then CIS returned an error when attempting to get the customer info
		//status message setted in the backing bean
		if(userSession.getStatusmessage() != null && !userSession.getStatusmessage().equals("")){
			if(userSession.isAlreadyShownOutreach()){
				model.put("redirectInfo", true);
				model.put(PORTALDISPLAY, true);
			}
			else{
				userSession.setAlreadyShownOutreach(Boolean.TRUE);
				model.put("redirectWelcome", true);
			}
			return model;
		}

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1804)
				&& request.getAttribute(FORMCOMPLETE) != null
				&& request.getAttribute(FORMCOMPLETE).equals(FORMCOMPLETE)) {
			model.put(FORMCOMPLETE, true);
			model.put("MBSD1804", true);
			userSession.setStatusmessage(null);
			if(userSession.isAlreadyShownOutreach()){
				model.put(PORTALDISPLAY, true);
				model.put("hideHeader", true);
			}
			return model;
		}

		Map<Object, String> occupationsMap = new LinkedHashMap<Object, String>();
		occupationsMap.put("-1", "");
		if(lang.equals("en")){
			for(int i = 0; i < this.occupationsEng.length; i++){
				String[] occupation = this.occupationsEng[i].split("-");
				if(!this.occupationsRemoved.contains(occupation[0])){
					occupationsMap.put(occupation[0], occupation[1]);
				}
			}
		}
		else{
			for(int i = 0; i < this.occupationsEsp.length; i++){
				String[] occupation = this.occupationsEsp[i].split("-");
				if(!this.occupationsRemoved.contains(occupation[0])){
					occupationsMap.put(occupation[0], occupation[1]);
				}
			}
		}
		model.put("occupationsMap", occupationsMap);
		model.put(MAXSKIPBYUSER, false);
		model.put("redirect", false);
		if (request.getParameter(CLOSE) != null)
			return model;
		else if (!userSession.isAlreadyShownOutreach() && userSession.isElegibleToOutreach()) {
			if (!userSession.getOobEnroll() && userSession.isQuestionAnswered()) {
				model.put("challengeQuestion", true);
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1803)){
					model.put("MBSD1803",true);
					CustomerServices customerService = userSession.getCustomerService();
					CustomerProfile customerProfile = userSession.getCustomerProfile();
					boolean enableSkip = enableSkip(customerProfile, customerService);
					if(!enableSkip){
						model.put(MAXSKIPBYUSER, true);
					}
					model.put("mobile", isMobile(userSession, request.getHeader(USERAGENT)));

				}
			} else
				//This workarround only applys when the text goes on javascript.
				model.put("urbLabel", StringEscapeUtils.unescapeHtml(this.messageSource.getMessage("outreach.address.type.urbanization.label2",null, new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang ))));
		} else if(userSession.isAlreadyShownOutreach()){
			if (!userSession.getOobEnroll() && userSession.isQuestionAnswered()) {
				model.put("challengeQuestion", true);
			}
				model.put(PORTALDISPLAY, true);
		} else {
			model.put("redirectPortal", true);
		}
		String header = request.getHeader(USERAGENT);
		// Hide header for iOS devices
		if ((userSession.getUserAgentType() == UserAgentType.IPHONE || userSession.getUserAgentType() == UserAgentType.IPAD)
					&& header.contains(WEBVIEW) || request.getParameter("info") != null)
			model.put("hideHeader", true);
		else
			model.put("hideHeader", false);

		model.put("urbLabel", StringEscapeUtils.unescapeHtml(this.messageSource.getMessage("outreach.address.type.urbanization.label2",null, language)));
		model.put("msgAddr1", StringEscapeUtils.unescapeHtml(this.messageSource.getMessage("outreach.field.address1.invalid",null, language)));
		model.put("msgAddr2", StringEscapeUtils.unescapeHtml(this.messageSource.getMessage("outreach.field.address2.invalid",null, language)));
		model.put("roadLabelTip", StringEscapeUtils.unescapeHtml(this.messageSource.getMessage("outreach.address.road.label",null, language)));
		model.put("roadHouseNumberTip", StringEscapeUtils.unescapeHtml(this.messageSource.getMessage("outreach.address.home.label",null, language)));
		model.put("addressNumberTip", StringEscapeUtils.unescapeHtml(this.messageSource.getMessage("outreach.address.number.label",null, language)));
		model.put("msgCity", StringEscapeUtils.unescapeHtml(this.messageSource.getMessage("outreach.field.city.invalid",null, language)));
		model.put("buldingNumber", StringEscapeUtils.unescapeHtml(this.messageSource.getMessage("outreach.address.type.building.number.label",null, language)));
		model.put("MBSD1634", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1634));
		model.put("MBSD1796", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1796));
		model.put("MBSD1994", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1994));

		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1796)){
			//countries above international separator, set by property
			List<Country> topCountries = new ArrayList<Country>();
			//countries below international separator
			List<Country> countries = new ArrayList<Country>();
			//country list as retiro movil
			String[] locales = Locale.getISOCountries();
			String clang = "es";
			if(lang.equalsIgnoreCase("en")){
				clang = "en";
			}
	    	for (String countryCode : locales) {
	    		Locale obj = new Locale(clang, countryCode);
	    		//if top countries are set skip validation
	    		if(topCountries.size() != this.topCountries.length){
	    			boolean found = false;
	    			for(int i = 0; i < this.topCountries.length; i++){
	    				String[] topCountrie = this.topCountries[i].split("-");
	    				if(obj.getCountry().equals(topCountrie[0])){
	    					topCountries.add(Integer.valueOf(topCountrie[1]),new Country(obj.getCountry(), obj.getDisplayCountry(obj)));
	    					found = true;
	    					break;
	    				}
	    			}
	    			if(!found){
    					countries.add(new Country(obj.getCountry(), obj.getDisplayCountry(obj)));
    				}
	    		}
	    		else{
	    			countries.add(new Country(obj.getCountry(), obj.getDisplayCountry(obj)));
	    		}
	    	 }
	    	model.put("topCountries", topCountries);
	    	Collections.sort(countries);
	    	model.put("countries", countries);

		}

		return model;
	}

	private ModelAndView goToPortal(HttpServletRequest request,
			HttpServletResponse response, OutReachBean command, UserSession userSession, BindException error) throws Exception {
		if(userSession.isAlreadyShownOutreach()){
			Map<String, Object> model = new HashMap<>();
			model.put("confirm", "true");
			String lang = Utils.getRSALanguage(request);
			if(!StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1804)){
				userSession.setStatusmessage(null);
			}else{
				userSession.setStatusmessage(this.messageSource.
					getMessage("outreach.portal.confirmation",null, new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang )));
			}
			return showForm(request, response, error, model);
		}
		else{
			userSession.setAlreadyShownOutreach(true);
		}
		String header = request.getHeader(USERAGENT);
        if (isMobile(userSession, header)) {
        	return new ModelAndView("redirect:outreach?close", "close", true);
        }
        	
		return new ModelAndView(SignonSequenceUtils.getNextView(userSession));

	}

	private ModelAndView confirm(HttpServletRequest request, HttpServletResponse response,
			OutReachBean bean, UserSession userSession, BindException error) throws Exception {
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();

		CustomerUpdateInCISRequest requestUpdate = new CustomerUpdateInCISRequest();
		requestUpdate.setCustomerPermId(userSession.getCustomerProfile().getPermId().toString());
		requestUpdate.setDCICodeUpdateDate(new Date());
		List<CustomerCodes> customerCodes = this.customerServices.getOutreachCustomerCodes();
		List<String> outreachCodes = new ArrayList<String>();
		for (int i = 0; i < customerCodes.size(); i++) {
			outreachCodes.add(("000" + customerCodes.get(i).getCustomerCode().trim()).substring(customerCodes.get(i).getCustomerCode().trim().length()));
		}
		requestUpdate.setCodesToRemove(outreachCodes);
		setValue(requestUpdate);

		//customer audit log
		CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request);

		if (bean.isFlagOcupation() || bean.isFlagEmployee() || bean.isFlagEmail() || bean.isFlagPhone()
				|| bean.isFlagAddress()) {
			String oldValue ="";
			String newValue="";
			String labelNew= "[new]";
			String labelOld= "[old]";
			CustomerActions[] actions = new CustomerActions[5];
			List<String> actionsName = new ArrayList<String>();
			boolean errors = false;
			CISOutreachCustomerInfoRequest requestCIS = new CISOutreachCustomerInfoRequest();
			requestCIS.setCustomerPermId(userSession.getCustomerProfile().getPermId().toString());
			CISOutreachCustomerInfoResponse responseCIS = this.customerServices
					.getCISDemographicCustomerInfo(requestCIS);
			Map<CustomerActions,String> mapChanges = new HashMap<CustomerActions, String>();
			if (bean.isFlagOcupation() && !bean.getOccupation().equals(responseCIS.getOccupation())) {
				if (!isEmpty(bean.getOccupation())) {
					actions[0] = userSession.isAlreadyShownOutreach() ? CustomerActions.OUTREACH_OCCUPATION_MY_INFO : CustomerActions.OUTREACH_OCCUPATION;
					actionsName.add(userSession.isAlreadyShownOutreach() ? CustomerActions.OUTREACH_OCCUPATION_MY_INFO.name() : CustomerActions.OUTREACH_OCCUPATION.name());
					requestUpdate.setOccupation(bean.getOccupation());
					String lang = Utils.getRSALanguage(request);
					StringBuilder valueE = new StringBuilder();

					if(lang.equals("en")){
						for(int i = 0; i < this.occupationsEng.length; i++){
							String[] occupation = this.occupationsEng[i].split("-");
							if(occupation.length == 2 && occupation[0].equals(responseCIS.getOccupation())){
								oldValue += OCCUPATION + occupation[1] + " ";
								valueE.append(labelOld).append(OCCUPATION).append(occupation[1]).append(" ");
							}
							if(occupation.length == 2 && occupation[0].equals(bean.getOccupation())){
								newValue += OCCUPATION + occupation[1] + " ";
								valueE.append(labelNew).append(OCCUPATION).append(occupation[1]).append(" ");
							}
						}
					}
					else{
						for(int i = 0; i < this.occupationsEsp.length; i++){
							String[] occupation = this.occupationsEsp[i].split("-");
							if(occupation.length == 2 && occupation[0].equals(responseCIS.getOccupation())){
								oldValue += OCCUPATION + occupation[1] + " ";
								valueE.append(labelOld).append(OCCUPATION).append(occupation[1]).append(" ");
							}
							if(occupation.length == 2 && occupation[0].equals(bean.getOccupation())){
								newValue += OCCUPATION + occupation[1] + " ";
								valueE.append(labelNew).append(OCCUPATION).append(occupation[1]).append(" ");
							}
						}
					}
					mapChanges.put(actions[0], valueE.toString());
				}else{
					logger.error("The Occupation is invalid");
					errors = true;
				}
			}

			if (bean.isFlagEmployee() && !bean.getEmployee().toUpperCase().equals(responseCIS.getEmployee())){
				if (!isEmpty(bean.getEmployee()) ) {
					StringBuilder valueE = new StringBuilder();
					actions[1] = userSession.isAlreadyShownOutreach() ? CustomerActions.OUTREACH_EMPLOYER_MY_INFO : CustomerActions.OUTREACH_EMPLOYER;
					actionsName.add(userSession.isAlreadyShownOutreach() ? CustomerActions.OUTREACH_EMPLOYER_MY_INFO.name() : CustomerActions.OUTREACH_EMPLOYER.name());
					requestUpdate.setEmployeerSchool(bean.getEmployee().toUpperCase());
					oldValue += EMPLOYER + responseCIS.getEmployee() + " ";
					valueE.append(labelOld).append(EMPLOYER).append(responseCIS.getEmployee()).append(" ");
					newValue += EMPLOYER + bean.getEmployee().toUpperCase() + " ";
					valueE.append(labelNew).append(EMPLOYER).append(bean.getEmployee()).append(" ");
					mapChanges.put(actions[1], valueE.toString());
				} else {
					logger.error("The employer is invalid");
					errors = true;
				}
			}else if (responseCIS.getEmployee().isEmpty()){
				logger.error("The employer is invalid");
				errors = true;
			}

			if (bean.isFlagEmail() && !bean.getEmail().toUpperCase().equals(responseCIS.getEmail())) {
				if (!isEmpty(bean.getEmail()) && GenericValidator.isEmail(bean.getEmail().toUpperCase())) {
					StringBuilder valueE = new StringBuilder();
					actions[2] = userSession.isAlreadyShownOutreach() ? CustomerActions.OUTREACH_EMAIL_MY_INFO
							: CustomerActions.OUTREACH_EMAIL;
					requestUpdate.setEmail(bean.getEmail());
					oldValue += MAIL + responseCIS.getEmail() + " ";
					valueE.append(labelOld).append(MAIL).append(responseCIS.getEmail()).append(" ");
					newValue += MAIL + bean.getEmail() + " ";
					valueE.append(labelNew).append(MAIL).append(bean.getEmail()).append(" ");
					mapChanges.put(actions[2], valueE.toString());
				} else {
					logger.error(EMAIL_INVALID);
					errors = true;
				}
			} else if (!CibpValidationUtils.isValidEmail(responseCIS.getEmail())) {
				logger.error(EMAIL_INVALID);
				errors = true;
			}
			
			// MBSFE-691
			String[] domain = bean.getEmail().split("@");
			if (Utils.haveType(this.invalidEmailDomains, domain[1])
					&& GenericValidator.isEmail(bean.getEmail().toUpperCase())
					&& StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE691)) {
				logger.error("Your e-mail address is incorrect. Please try again.");
				errors = true;
			}
			// END MBSFE-691

			if(bean.isFlagPhone()){
				StringBuilder valueE = new StringBuilder();
				String phoneNumber = "";
				String[] phoneArr = bean.getPhoneNumber().split("-");
				for (int i = 0; i < phoneArr.length; i++) {
					phoneNumber += phoneArr[i];
				}
				if(phoneNumber.isEmpty() || CibpValidationUtils.isValidPhoneNumber(phoneNumber)){
					if (!phoneNumber.equals(responseCIS.getPhoneNumber())) {
						actions[3] = userSession.isAlreadyShownOutreach() ? CustomerActions.OUTREACH_PHONE_MY_INFO : CustomerActions.OUTREACH_PHONE;
						actionsName.add(userSession.isAlreadyShownOutreach() ? CustomerActions.OUTREACH_PHONE_MY_INFO.name() : CustomerActions.OUTREACH_PHONE.name());
						requestUpdate.setPhoneNumber(phoneNumber);
						oldValue += MOBILE_PHONE + responseCIS.getPhoneNumber() + " ";
						valueE.append(labelOld).append(MOBILE_PHONE).append(responseCIS.getPhoneNumber()).append(" ");
						newValue += MOBILE_PHONE + phoneNumber + " ";
						valueE.append(labelOld).append(MOBILE_PHONE).append(phoneNumber).append(" ");
						mapChanges.put(actions[3], valueE.toString());
					}
				}else {
					logger.error("The phone number is invalid");
					errors = true;
				}
			}

			if (bean.isFlagAddress()) {
				String newAddress = "";
				StringBuilder valueE = new StringBuilder();
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1634)) {

					if ((bean.getNewAddress1() == null || isEmpty(bean.getNewAddress1()))
							&& (bean.getNewAddress2() == null || isEmpty(bean.getNewAddress2()))) {
						logger.error("The address is required");
						errors = true;
					} else {
						if (bean.getNewAddress1() != null && !isEmpty(bean.getNewAddress1())) {
							if (CibpValidationUtils.isValidAddressNoPostal(bean.getNewAddress1().toUpperCase())
									&& CibpValidationUtils.isValidUSaveAddress(bean.getNewAddress1().toUpperCase())) {
								requestUpdate.setAddress1(bean.getNewAddress1().toUpperCase());
								newAddress += bean.getNewAddress1().toUpperCase();
							} else {
								logger.error("The address 1 is invalid");
								errors = true;
							}
						} else {
							requestUpdate.setAddress1("");
						}

						if (bean.getNewAddress2() != null && !isEmpty(bean.getNewAddress2())) {
							if (CibpValidationUtils.isValidAddressNoPostal(bean.getNewAddress2().toUpperCase())
									&& CibpValidationUtils.isValidUSaveAddress(bean.getNewAddress2().toUpperCase())) {
								requestUpdate.setAddress2(bean.getNewAddress2().toUpperCase());
								newAddress += " " + bean.getNewAddress2().toUpperCase();
							} else {
								logger.error("The address 2 is invalid");
								errors = true;
							}
						} else {
							requestUpdate.setAddress2("");
						}
					}


					if (!isEmpty(bean.getAddressCountry())) {
						String address3 = "";

						if (bean.getAddressTown() != null && !isEmpty(bean.getAddressTown())
								&& CibpValidationUtils.isValidAddressNoPostal(bean.getAddressTown())) {
							address3 = bean.getAddressTown().toUpperCase() + " ";
						}

						if(bean.getAddressCountry().equals("VI") || bean.getAddressCountry().equals("US")) {
							if (!isEmpty(bean.getAddressState())) {
								address3 += " " + bean.getAddressState().toUpperCase() + " ";
							}else {
								logger.error("The address state is invalid");
								errors = true;
							}
						}

						if (!isEmpty(bean.getAddressCountry())) {
							address3 += bean.getAddressCountry().toUpperCase();
						} else {
							logger.error("The address Country is invalid");
							errors = true;
						}

						if (bean.getAddressCountry().equals("PR") || bean.getAddressCountry().equals("VI")
								|| bean.getAddressCountry().equals("US")) {
							if (bean.getAddressPostalCode() != null && !isEmpty(bean.getAddressPostalCode()) && CibpValidationUtils.isValidZipcode(bean.getAddressPostalCode())) {
								address3 += " " + bean.getAddressPostalCode().toUpperCase();
							} else {
								logger.error(ADDRESS_3_INVALID);
								errors = true;
							}
						} else {
							if(bean.getAddressPostalCode() != null && !isEmpty(bean.getAddressPostalCode()) ) {
								if (CibpValidationUtils.isValidInternationalZipcode(bean.getAddressPostalCode())) {
									address3 += " " + bean.getAddressPostalCode().toUpperCase();
								} else {
									logger.error(ADDRESS_3_INVALID);
									errors = true;
								}
							}
						}

						if (bean.getAddressPostalCode2() != null && !isEmpty(bean.getAddressPostalCode2()))
							address3 += "-" + bean.getAddressPostalCode2().toUpperCase();

						if (CibpValidationUtils.isValidAddressNoPostal(address3) && (bean.getNewAddress1() != null
								? CibpValidationUtils.isValidUSaveAddress(bean.getNewAddress1().toUpperCase())
								: true)) {
							actions[4] = userSession.isAlreadyShownOutreach() ? CustomerActions.OUTREACH_ADDRESS_MY_INFO
									: CustomerActions.OUTREACH_ADDRESS;
							actionsName.add(userSession.isAlreadyShownOutreach()
									? CustomerActions.OUTREACH_ADDRESS_MY_INFO.name()
									: CustomerActions.OUTREACH_ADDRESS.name());
							requestUpdate.setAddress3(address3.toUpperCase());
							newAddress += " " + address3.toUpperCase();
						} else {
							logger.error(ADDRESS_3_INVALID);
							errors = true;
						}

					} else {
						logger.error(ADDRESS_3_INVALID);
						errors = true;
					}

					oldValue += ADDRESS + new StringBuilder()
							.append(responseCIS.getAddress1() != null ? responseCIS.getAddress1() + " " : " ")
							.append(responseCIS.getAddress2() != null ? responseCIS.getAddress2() + " " : " ")
							.append(responseCIS.getAddress3() != null ? responseCIS.getAddress3() + " " : " ")
							.toString();
					valueE.append(labelOld).append(ADDRESS)
							.append(responseCIS.getAddress1() != null ? responseCIS.getAddress1() + " " : " ")
							.append(responseCIS.getAddress2() != null ? responseCIS.getAddress2() + " " : " ")
							.append(responseCIS.getAddress3() != null ? responseCIS.getAddress3() + " " : " ");
					newValue += ADDRESS + newAddress;
					valueE.append(labelNew).append(ADDRESS).append(newAddress).append("");
					mapChanges.put(actions[4], valueE.toString());

				}else {

					//---------------------------------OLD--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					if (!isEmpty(bean.getNewAddress1())
							&& CibpValidationUtils.isValidAddressNoPostal(bean.getNewAddress1().toUpperCase())
							&& CibpValidationUtils.isValidUSaveAddress(bean.getNewAddress1().toUpperCase())) {
						requestUpdate.setAddress1(bean.getNewAddress1().toUpperCase());
						newAddress += bean.getNewAddress1().toUpperCase();
					} else {
						logger.error("The address 1 is invalid");
						errors = true;
					}
					if (bean.getNewAddress2() != null) {
						if (!isEmpty(bean.getNewAddress2())
								&& CibpValidationUtils.isValidAddressNoPostal(bean.getNewAddress2().toUpperCase())
								&& CibpValidationUtils.isValidUSaveAddress(bean.getNewAddress2().toUpperCase())) {
							requestUpdate.setAddress2(bean.getNewAddress2().toUpperCase());
							newAddress += " " + bean.getNewAddress2().toUpperCase();
						} else {
							logger.error("The address 2 is invalid");
							errors = true;
						}
					} else {
						requestUpdate.setAddress2("");
					}

					if ((!isEmpty(bean.getAddressCountry()) || !isEmpty(bean.getAddressCountryPR()))
							&& !isEmpty(bean.getAddressPostalCode())
							&& CibpValidationUtils.isValidZipcode(bean.getAddressPostalCode())) {

						String address3 = "";

						if (!isEmpty(bean.getAddressTown())
								&& CibpValidationUtils.isValidAddressNoPostal(bean.getAddressTown())
								&& CibpValidationUtils.isValidUSaveAddress(bean.getNewAddress1().toUpperCase())) {
							address3 = bean.getAddressTown().toUpperCase() + " ";
						}

						if (!isEmpty(bean.getAddressType())) {
							if ((bean.getAddressType().equals("1") || bean.getAddressType().equals("2")
									|| bean.getAddressType().equals("3")) && !isEmpty(bean.getAddressCountryPR())
									&& isEmpty(bean.getAddressCountry())) {
								address3 += bean.getAddressCountryPR().toUpperCase();
							} else if ((bean.getAddressType().equals("4") || bean.getAddressType().equals("5"))
									&& !isEmpty(bean.getAddressCountry()) && isEmpty(bean.getAddressCountryPR())) {
								address3 += bean.getAddressCountry().toUpperCase();
							} else {
								logger.error("The address type is invalid");
								errors = true;
							}
						} else {
							logger.error("The address type is invalid");
							errors = true;
						}

						if (!isEmpty(bean.getAddressState())) {
							address3 += " " + bean.getAddressState().toUpperCase();
						}
						address3 += " " + bean.getAddressPostalCode().toUpperCase();

						if (!isEmpty(bean.getAddressPostalCode2()))
							address3 += "-" + bean.getAddressPostalCode2().toUpperCase();

						if (CibpValidationUtils.isValidAddressNoPostal(address3)
								&& CibpValidationUtils.isValidUSaveAddress(bean.getNewAddress1().toUpperCase())) {
							actions[4] = userSession.isAlreadyShownOutreach() ? CustomerActions.OUTREACH_ADDRESS_MY_INFO
									: CustomerActions.OUTREACH_ADDRESS;
							actionsName.add(userSession.isAlreadyShownOutreach()
									? CustomerActions.OUTREACH_ADDRESS_MY_INFO.name()
									: CustomerActions.OUTREACH_ADDRESS.name());
							requestUpdate.setAddress3(address3.toUpperCase());
							newAddress += " " + address3.toUpperCase();
						} else {
							logger.error(ADDRESS_3_INVALID);
							errors = true;
						}
					}
					else {
						logger.error(ADDRESS_3_INVALID);
						errors = true;
					}

					oldValue += ADDRESS + new StringBuilder()
							.append(responseCIS.getAddress1() != null ? responseCIS.getAddress1() + " " : " ")
							.append(responseCIS.getAddress2() != null ? responseCIS.getAddress2() + " " : " ")
							.append(responseCIS.getAddress3() != null ? responseCIS.getAddress3() + " " : " ")
							.toString();
					valueE.append(labelOld).append(ADDRESS)
							.append(responseCIS.getAddress1() != null ? responseCIS.getAddress1() + " " : " ")
							.append(responseCIS.getAddress2() != null ? responseCIS.getAddress2() + " " : " ")
							.append(responseCIS.getAddress3() != null ? responseCIS.getAddress3() + " " : " ");
					newValue += ADDRESS + newAddress;
					valueE.append(labelNew).append(ADDRESS).append(newAddress).append("");
					mapChanges.put(actions[4], valueE.toString());
				}


			}

			if (!errors) {
				if(this.customerServices.updateCustomerInCis(requestUpdate)){
					for (int i = 0 ; i < actions.length ; i++) {
						if(actions[i] != null){
							customerService.logCustomerUsageData(profile, actions[i]);
							if(actions[i].equals(CustomerActions.OUTREACH_EMAIL) || actions[i].equals(CustomerActions.OUTREACH_EMAIL_MY_INFO)){
								sendEmailAlert(request, userSession, customerService, profile, requestUpdate);
								event.putString("data", mapChanges.get(actions[i]));
								event.setInteraction(CustomerInteractionEventType.EMAIL_CHANGE);
								this.customerServices.logOutreachEvent(event, InteractionSeverity.INFO);
							}
							if (actions[i].equals(CustomerActions.OUTREACH_OCCUPATION) || actions[i].equals(CustomerActions.OUTREACH_OCCUPATION_MY_INFO)){
								event.putString("data", mapChanges.get(actions[i]));
								event.setInteraction(CustomerInteractionEventType.OCCUPATION_CHANGE);
								this.customerServices.logOutreachEvent(event, InteractionSeverity.INFO);
							}
							if (actions[i].equals(CustomerActions.OUTREACH_EMPLOYER) || actions[i].equals(CustomerActions.OUTREACH_EMPLOYER_MY_INFO)){
								event.putString("data", mapChanges.get(actions[i]));
								event.setInteraction(CustomerInteractionEventType.EMPLOYER_CHANGE);
								this.customerServices.logOutreachEvent(event, InteractionSeverity.INFO);
							}

							if (actions[i].equals(CustomerActions.OUTREACH_PHONE) || actions[i].equals(CustomerActions.OUTREACH_PHONE_MY_INFO)){

								event.putString("data", mapChanges.get(actions[i]));
								event.setInteraction(CustomerInteractionEventType.PHONE_NUMBER_CHANGE);
								this.customerServices.logOutreachEvent(event, InteractionSeverity.INFO);

							}

							if (actions[i].equals(CustomerActions.OUTREACH_ADDRESS) || actions[i].equals(CustomerActions.OUTREACH_ADDRESS_MY_INFO)){

								event.putString("data", mapChanges.get(actions[i]));
								event.setInteraction(CustomerInteractionEventType.ADDRESS_CHANGE);
								this.customerServices.logOutreachEvent(event, InteractionSeverity.INFO);

							}
						}
			        }



					String logValues = "[old]" + oldValue + "[new]" + newValue;
					event.putString("data", logValues);
					if(userSession.isAlreadyShownOutreach()){
						customerService.logCustomerUsageData(profile, CustomerActions.OUTREACH_CONFIRM_MY_INFO);
						event.setInteraction(CustomerInteractionEventType.OUTREACH_CONFIRMATION);
						this.customerServices.logOutreachEvent(event, InteractionSeverity.INFO);
					}
					else{
						customerService.logCustomerUsageData(profile, CustomerActions.OUTREACH_CONFIRM);
						event.setInteraction(CustomerInteractionEventType.OUTREACH_INTERRUPTION_CONFIRMATION);
						this.customerServices.logOutreachEvent(event, InteractionSeverity.INFO);
					}
					customerService.setSkipCount(profile, true);
					//Send email alert
					if(actionsName.size() > 0){
						boolean emailSent = customerService.sendOutreachUpdateEmail(userSession.getCustomerProfile(),
								userSession.getPreferredLanguage(), actionsName);
						if (!emailSent) {
							OutReachForm.logger.error(OutReachForm.class.getName()+":"+"Could not send outreach update confirmation email to " + userSession.getCustomerProfile().getUsername() + " ("
									+ userSession.getCustomerProfile().getEmail() + ")");
						}
					}
					userSession.setOutReachConfirmation("outreach.portal.confirmation");
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1804)){
						request.setAttribute(FORMCOMPLETE, FORMCOMPLETE);
						request.setAttribute("resultMsg", "outreach.portal.confirmation");
						return showForm(request, response, error);
					}
					return goToPortal(request, response, bean, userSession, error);
				}else {
					Map<String, Object> model = new HashMap<>();
					String lang = Utils.getRSALanguage(request);
					model.put("errors", StringEscapeUtils.unescapeHtml(this.messageSource.getMessage(OUTREACH_ERROR_GENERIC,null, new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang ))));
					return showForm(request, response, error, model);
				}
			} else {
				Map<String, Object> model = new HashMap<>();
				String lang = Utils.getRSALanguage(request);
				model.put("errors", StringEscapeUtils.unescapeHtml(this.messageSource.getMessage(OUTREACH_ERROR_GENERIC,null, new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang ))));
				return showForm(request, response, error, model);
			}

		} else {

			if(bean.getEmail()==null || bean.getEmail().isEmpty()){
				Map<String, Object> model = new HashMap<>();
				logger.error(EMAIL_INVALID);
				String lang = Utils.getRSALanguage(request);
				model.put("errors", StringEscapeUtils.unescapeHtml(this.messageSource.getMessage("outreach.field.email.required",null, new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang ))));
				return showForm(request, response, error, model);
			}

			setValue(requestUpdate);
			if(this.customerServices.updateCustomerInCis(requestUpdate)){
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1052)){
					userSession.setOutReachConfirmation("outreach.portal.confirmation");
					customerService.setSkipCount(profile, true);
					String logValues = "";
					event.putString("data", logValues);
					if(userSession.isAlreadyShownOutreach()){
						customerService.logCustomerUsageData(profile, CustomerActions.OUTREACH_CONFIRM_MY_INFO);
						event.setInteraction(CustomerInteractionEventType.OUTREACH_CONFIRMATION);
						this.customerServices.logOutreachEvent(event, InteractionSeverity.INFO);
					}
					else{
						customerService.logCustomerUsageData(profile, CustomerActions.OUTREACH_CONFIRM);
						event.setInteraction(CustomerInteractionEventType.OUTREACH_INTERRUPTION_CONFIRMATION);
						this.customerServices.logOutreachEvent(event, InteractionSeverity.INFO);
					}
				}
				else{
					userSession.setOutReachConfirmation("outreach.portal.confirmation");
					customerService.logCustomerUsageData(profile, CustomerActions.OUTREACH_CONFIRM);
					customerService.setSkipCount(profile, true);
					String logValues = "";
					event.putString("data", logValues);
					if(userSession.isAlreadyShownOutreach()){
						event.setInteraction(CustomerInteractionEventType.OUTREACH_CONFIRMATION);
						this.customerServices.logOutreachEvent(event, InteractionSeverity.INFO);
					}
					else{
						event.setInteraction(CustomerInteractionEventType.OUTREACH_INTERRUPTION_CONFIRMATION);
						this.customerServices.logOutreachEvent(event, InteractionSeverity.INFO);
					}
				}				
			}else{
				userSession.setOutReachConfirmation("outreach.cis.error");
			}
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1804)){
				request.setAttribute(FORMCOMPLETE, FORMCOMPLETE);
				request.setAttribute("resultMsg", userSession.getOutReachConfirmation());
				return showForm(request, response, error);
			}
			return goToPortal(request, response, bean, userSession, error);
		}
	}

	/** Send a email when the email was change*/
	private void sendEmailAlert(HttpServletRequest request, UserSession userSession,
			CustomerServices customerService, CustomerProfile profile, CustomerUpdateInCISRequest requestUpdate) {
		ModifyAllCustomerEmailsRequest macer = new ModifyAllCustomerEmailsRequest(
		Utils.generateCustomerInteractionEvent(request), profile,
		customerService, userSession.getAccounts(), userSession.getBankingSession(),
		userSession.getEbpp2UserId(), userSession.getPreferredLanguage(), requestUpdate.getEmail());
		profile = CustomerUtils.modifyAllEmails(macer);
		userSession.setCustomerProfile(profile);
	}

	private void setValue(CustomerUpdateInCISRequest requestUpdate) {
		requestUpdate.setOccupation(null);
		requestUpdate.setEmployee(null);
		requestUpdate.setEmail(null);
		requestUpdate.setPhoneNumber(null);
		requestUpdate.setAddress1(null);
		requestUpdate.setAddress2(null);
		requestUpdate.setAddress3(null);
	}

	private boolean isEmpty(String newAddress){
		return newAddress == null || newAddress.isEmpty();
	}

	private ModelAndView skipProcess(HttpServletRequest request, HttpServletResponse response,
				OutReachBean bean, UserSession userSession, BindException error) throws Exception {

		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile customerProfile = userSession.getCustomerProfile();

		Map<String, Object> model = new HashMap<>();
		boolean enableSkip = enableSkip(customerProfile, customerService);
		if(enableSkip){
			//Increment Skip count for Monthly Report
			customerService.logCustomerUsageData(customerProfile, CustomerActions.OUTREACH_SKIP);
			CustomerUpdateInCISRequest requestUpdate = new CustomerUpdateInCISRequest();
			requestUpdate.setCustomerPermId(userSession.getCustomerProfile().getPermId().toString());
			requestUpdate.setDCICodeUpdateDate(new Date());
			setValue(requestUpdate);

			//customer audit log
			CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request);
			event.setInteraction(CustomerInteractionEventType.OUTREACH_INTERRUPTION_SKIP);
			event.putString("data", "");
			this.customerServices.logOutreachEvent(event, InteractionSeverity.INFO);
			customerService.setSkipCount(customerProfile, false);
			return goToPortal(request, response, bean, userSession, error);
		}else{
			model.put(MAXSKIPBYUSER, true);
			return showForm(request, response, error, model);
		}
	}

	private boolean enableSkip(CustomerProfile customerProfile, CustomerServices customerService){
		String confParametersValue = customerService.getConfParametersByShortNameAndApp(CISOutreachRequest.MAX_SKIP_PARAMETER , CISOutreachRequest.OUTREACH_APPLICATION).getValue();
		int maxSkip = Integer.valueOf(confParametersValue);
		int skipUser = customerService.getSkipCount(customerProfile) + 1;
		return skipUser <= maxSkip;
	}

	public void setCustomerServices(CustomerServices customerServices) {
		this.customerServices = customerServices;
	}

	public void setOccupationsEng(String[] occupationsEng) {

		this.occupationsEng = occupationsEng;
		if(this.occupationsEng != null)
			 Arrays.sort(this.occupationsEng, new Comparator<String>()
			    {
			      public int compare(String s1, String s2)
			      {
			    	String[] occupation1 = s1.split("-");
			    	String[] occupation2 = s2.split("-");
			        return occupation1[1].compareTo(occupation2[1]);
			      }
			    });
	}

	public void setOccupationsEsp(String[] occupationsEsp) {
		this.occupationsEsp = occupationsEsp;
		if(this.occupationsEsp != null)
			 Arrays.sort(this.occupationsEsp, new Comparator<String>()
			    {
			      public int compare(String s1, String s2)
			      {
			    	String[] occupation1 = s1.split("-");
			    	String[] occupation2 = s2.split("-");
			        return occupation1[1].compareTo(occupation2[1]);
			      }
			    });
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public void setOccupationsRemoved(String occupationsRemoved) {
		this.occupationsRemoved = occupationsRemoved;
	}

	public void setTopCountries(String[] topCountries) {
		this.topCountries = topCountries;
	}

	/**
	 * Verify if comes from mobile native app petition
	 * @param userSession
	 * @param header
	 * @return true if is a webview
	 */
	private boolean isMobile(UserSession userSession, String header) {
		return (userSession.getUserAgentType() == UserAgentType.IPHONE ||
		        userSession.getUserAgentType() == UserAgentType.IPAD ||
		        userSession.getUserAgentType() == UserAgentType.ANDROID) && header.contains(WEBVIEW);
	}
	
	// MBSFE-691
	public String getInvalidEmailDomains() {
		return invalidEmailDomains;
	}

	public void setInvalidEmailDomains(String invalidEmailDomains) {
		this.invalidEmailDomains = invalidEmailDomains;
	}
	// END MBSFE-691

}
