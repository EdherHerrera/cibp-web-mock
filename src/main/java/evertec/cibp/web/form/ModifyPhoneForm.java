package evertec.cibp.web.form;

import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.EventType;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.applications.ApplicationStatus;
import evertec.cibp.core.models.customer.applications.PhoneApplication;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ModifyPhoneBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ModifyPhoneForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	
	public ModifyPhoneForm() {
		super();
		setCommandClass(ModifyPhoneBean.class);
		setCommandName("modifyPhone");
		setFormView("modifyPhone");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		ModifyPhoneBean bean = (ModifyPhoneBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();

		String residentialPhone1 = bean.getResidentialPhone1();
		String residentialPhone2 = bean.getResidentialPhone2();
		String residentialPhone3 = bean.getResidentialPhone3();
		String workPhone1 = bean.getWorkPhone1();
		String workPhone2 = bean.getWorkPhone2();
		String workPhone3 = bean.getWorkPhone3();
		String celPhone1 = bean.getCelPhone1();
		String celPhone2 = bean.getCelPhone2();
		String celPhone3 = bean.getCelPhone3();

		String resphone = residentialPhone1 + residentialPhone2 + residentialPhone3;
		String work = workPhone1 + workPhone2 + workPhone3;
		String cel = celPhone1 + celPhone2 + celPhone3;

		/* CIBP-1104 */
		boolean hasalert = false;
		AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
		if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
			Set<AlertType> alerts = ent.getAlerts();
			if (alerts.contains(AlertType.PHONE_EDIT_EMAIL) || alerts.contains(AlertType.PHONE_EDIT_SMS)
					|| alerts.contains(AlertType.PHONE_EDIT_PUSH)) {
				hasalert = true;
			}
		}
		/* FIN--- CIBP-1104 */

		boolean saveApplication = false;
		PhoneApplication phoneapp = new PhoneApplication(resphone, work, cel, ApplicationStatus.NEW);
		try {
			/**
			 * MBNA-1895
			 * Method savePhoneApplication : CIBP-WEB microservice integration
			 * 
			 */
			CustomerFacade.getInstance().savePhoneApplication(phoneapp, request);
			saveApplication = true;
			
	    	final CustomerInteractionEvent interaction = Utils.generateCustomerInteractionEvent(request, userSession.getUsername()); // the event
	    	
	    	customerService.getInteractionService().logCustomerEvent(interaction, 
	        		 CustomerInteractionEventType.MYINFO_PHONE_CHANGE, InteractionSeverity.INFO);
		} catch (RuntimeException e) {
			
			saveApplication = false;
		}

		model.put("completed", true);
		if (!saveApplication) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Unable to send your change request, Please try later.");
			else
				userSession
						.setStatusmessage("Su solicitud de cambio no se pudo procesar en este momento, favor intentar luego.");
		} else {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Your change request will be processed in the next two business days.");
			else
				userSession
						.setStatusmessage("Tu solicitud de cambio ser&aacute; procesada en los pr&oacute;ximos dos d&iacute;as laborables");

			/* CIBP-1104 se activa envio alerta de modificacion de telefono */
			if (hasalert) {
				customerService.sendPhoneEditConfirmation(customer,
						userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
						userSession.getPreferredLanguage());
			}
		}
		
		// MBSD-1999 RSA Upgrade Recommendations - Edit phone numbers (Update cookie)
		Cookie cookie = WebUtils.getCookie(request, "PMData") != null ? WebUtils.getCookie(request, "PMData") : new Cookie("PMData", "");
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		ESAPI.httpUtilities().addCookie(response, cookie);


		return showForm(request, error, "modifyPhone", model);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		ModifyPhoneBean bean = (ModifyPhoneBean) command;

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String residentialPhone1 = bean.getResidentialPhone1();
		String residentialPhone2 = bean.getResidentialPhone2();
		String residentialPhone3 = bean.getResidentialPhone3();
		String workPhone1 = bean.getWorkPhone1();
		String workPhone2 = bean.getWorkPhone2();
		String workPhone3 = bean.getWorkPhone3();
		String celPhone1 = bean.getCelPhone1();
		String celPhone2 = bean.getCelPhone2();
		String celPhone3 = bean.getCelPhone3();
		
		if (GenericValidator.isBlankOrNull(residentialPhone1) && GenericValidator.isBlankOrNull(residentialPhone2)
				&& GenericValidator.isBlankOrNull(residentialPhone3) && GenericValidator.isBlankOrNull(workPhone1)
				&& GenericValidator.isBlankOrNull(workPhone2) && GenericValidator.isBlankOrNull(workPhone3)
				&& GenericValidator.isBlankOrNull(celPhone1) && GenericValidator.isBlankOrNull(celPhone2)
				&& GenericValidator.isBlankOrNull(celPhone3)) {

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("celPhone3", "", "Please enter at least one valid phone number.");
			else
				errors.rejectValue("celPhone3", "", "Por favor entre por lo menos un tel&eacute;fono valido.");
		} else {
			if (!GenericValidator.isBlankOrNull(residentialPhone1)
					|| !GenericValidator.isBlankOrNull(residentialPhone2)
					|| !GenericValidator.isBlankOrNull(residentialPhone3))
				if (GenericValidator.isBlankOrNull(residentialPhone1)
						|| !GenericValidator.matchRegexp(residentialPhone1, "^\\d{3}$")
						|| GenericValidator.isBlankOrNull(residentialPhone2)
						|| !GenericValidator.matchRegexp(residentialPhone2, "^\\d{3}$")
						|| GenericValidator.isBlankOrNull(residentialPhone3)
						|| !GenericValidator.matchRegexp(residentialPhone3, "^\\d{4}$")) {

					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("residentialPhone3", "", "Please enter a home phone number.");
					else
						errors.rejectValue("residentialPhone3", "", "Por favor entre un tel&eacute;fono residencial.");
				}
			if (!GenericValidator.isBlankOrNull(workPhone1) || !GenericValidator.isBlankOrNull(workPhone2)
					|| !GenericValidator.isBlankOrNull(workPhone3))
				if (GenericValidator.isBlankOrNull(workPhone1) || !GenericValidator.matchRegexp(workPhone1, "^\\d{3}$")
						|| GenericValidator.isBlankOrNull(workPhone2)
						|| !GenericValidator.matchRegexp(workPhone2, "^\\d{3}$")
						|| GenericValidator.isBlankOrNull(workPhone3)
						|| !GenericValidator.matchRegexp(workPhone3, "^\\d{4}$")) {

					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("workPhone3", "", "Please enter a work phone number.");
					else
						errors.rejectValue("workPhone3", "", "Por favor entre un tel&eacute;fono de trabajo.");
				}
			if (!GenericValidator.isBlankOrNull(celPhone1) || !GenericValidator.isBlankOrNull(celPhone2)
					|| !GenericValidator.isBlankOrNull(celPhone3))
				if (GenericValidator.isBlankOrNull(celPhone1) || !GenericValidator.matchRegexp(celPhone1, "^\\d{3}$")
						|| GenericValidator.isBlankOrNull(celPhone2)
						|| !GenericValidator.matchRegexp(celPhone2, "^\\d{3}$")
						|| GenericValidator.isBlankOrNull(celPhone3)
						|| !GenericValidator.matchRegexp(celPhone3, "^\\d{4}$")) {

					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("celPhone3", "", "Please enter a mobile telephone number.");
					else
						errors.rejectValue("celPhone3", "", "Por favor entre un tel&eacute;fono m&oacute;vil.");
				}
		}
		if (errors.getAllErrors().size() == 0) {
			Boolean informationChallenge = false;
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)) {
				if (!userSession.getOobEnroll()) {
					informationChallenge = userSession.isInformationChallenge();
				} else {
					if (!userSession.isOobAuthenticated())
						userSession.setOobChallenge(true);
					informationChallenge = userSession.isOobAuthenticated();
				}
			} else {
				informationChallenge = userSession.isInformationChallenge();
			}
			
			if (informationChallenge != null && !informationChallenge) {

				// ////////////////////

				String language = Utils.getRSALanguage(request);
				String username = userSession.getUsername();
				
				if (userSession.getInformationQuestion() == null && !userSession.getOobEnroll()) {

					if (informationChallenge != null && !informationChallenge) {

						DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), request);
						RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
						AnalyzeResponse anResp = userSession.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

						deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData()
								.getDeviceTokenCookie());
						deviceRequest
								.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

						userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
						userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

						userSession.setDeviceRequest(deviceRequest);

						if (!(ActionCode.CHALLENGE == anResp.getRiskResult().getTriggeredRule().getActionCode())) {
							userSession.setInformationChallenge(true);
						} else {
							String email = null;
							if (userSession.getCustomerProfile() != null)
								email = userSession.getCustomerProfile().getEmail();

							QuestionResponse personalQuestion;

							if (userSession.getInformationQuestion() == null) {
								RSAActionRequest actionRequest = new RSAActionRequest(
										Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
										userSession.getUsername(), "", email, 0, userSession.isRemember(),
										userSession.getDeviceRequest());

								if (userSession.getCustomerProfile() != null)
									actionRequest.setUsername(userSession.getCustomerProfile().getUsername());
								else if (userSession.getUsernameReservation() != null)
									actionRequest.setUsername(userSession.getUsernameReservation().getUsername());
								else
									actionRequest.setUsername(username);

								actionRequest.setDeviceRequest(userSession.getDeviceRequest());
								actionRequest.setRsaSessionId(userSession.getRsaSessionId());
								actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());

								actionRequest.setRsaLang(Utils.getRSALanguage(request));

								personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(
										actionRequest);
								
								// MBSD-1999 RSA Upgrade Recommendations - Edit phone numbers (Update device request)
								deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request);
								deviceRequest.setDeviceTokenCookie(personalQuestion.getDeviceRequest().getDeviceTokenCookie());
								deviceRequest.setDeviceTokenFSO(personalQuestion.getDeviceRequest().getDeviceTokenFSO());
								userSession.setDeviceRequest(deviceRequest);
								userSession.setRsaSessionId(personalQuestion.getSessionId());
								userSession.setRsaTransactionId(personalQuestion.getRsaTransactionId());
							} else {
								personalQuestion = new QuestionResponse();
								personalQuestion.setStatusResult(ResultStatus.SUCCESS);
								personalQuestion.setQuestion(userSession.getInformationQuestion());
							}
							if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
								userSession.setInformationQuestion(personalQuestion.getQuestion());

							} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
								throw new ModelAndViewDefiningException(new ModelAndView("error"));
							} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
								throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
							}
						}
					}
				}
				
				errors.rejectValue("", "", "");
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
				Map<String, String> parameters = new HashMap<String, String>();
				String resphone = residentialPhone1 + residentialPhone2 + residentialPhone3;
				String work = workPhone1 + workPhone2 + workPhone3;
				String cel = celPhone1 + celPhone2 + celPhone3;
				parameters.put(ChallengeInformationUtils.RESIDENTIAL_PHONE, resphone);
				parameters.put(ChallengeInformationUtils.WORK_PHONE, work);
				parameters.put(ChallengeInformationUtils.MOBILE_PHONE, cel);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
						ChallengeInformationUtils.PHONES_ACTION);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, parameters);
			}
		}
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, userSession.isInformationChallenge());

		// FFEIC + SMS
		String username = userSession.getUsername();
		model.put("username", username);

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setPath("/");
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		if (cookie != null)
			model.put("devicetoken", cookie.getValue());

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)) {
			if (!userSession.getOobEnroll()) {
				if (userSession.getInformationQuestion() != null) {
					String question = (String) userSession.getInformationQuestion().values().toArray()[0];
					model.put("question", question);
				} 
			} else {
				if (!userSession.isOobAuthenticated() && userSession.getOobChallenge() != null && userSession.getOobChallenge()) {
					model.put("oobchallenge", true);
					List<ASK_FOR_OOB> listNavOOB = new ArrayList<ASK_FOR_OOB>();
					listNavOOB.add(ASK_FOR_OOB.OOB_CHALLENGE);
					listNavOOB.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
					userSession.setTasksListOOB(listNavOOB);
					userSession.setOobChallenge(false);
				}
			}
		} else {
			if (userSession.getInformationQuestion() != null) {
				String question = (String) userSession.getInformationQuestion().values().toArray()[0];
				model.put("question", question);
			} 
		}
		
		// /////////////////////////////////////

		return model;
	}
	
}
