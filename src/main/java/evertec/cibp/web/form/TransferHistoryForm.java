package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.transfer.TVAccountTransfer;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.models.transfer.TVAccountTransferTarget;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.TransferHistoryResponse;
import evertec.cibp.core.services.responses.TransferResponse;
import evertec.cibp.core.utils.FrequencyUtils;
import evertec.cibp.vs.beans.TransferFrequency;
import evertec.cibp.vs.models.TransferFilterType;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.form.bean.ModTransferBean;
import evertec.cibp.web.form.bean.TransferSearchBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.TransferSort;
import evertec.cibp.web.utils.Utils;

//CIBP-1680 se agrega  implements MessageSourceAware para acceso a los archivos
public class TransferHistoryForm extends CancellableFormController implements GlobalEntitlementAwareController,
		MessageSourceAware {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private int maxTransfers = 15;

	private MessageSource messageSource;

	public TransferHistoryForm() {
		super();
		setCommandClass(TransferSearchBean.class);
		setCommandName("transfersearch");
		setFormView("transferhistory");
		setBindOnNewForm(true);
	}

	protected boolean isFormSubmission(HttpServletRequest request) {
		return true;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		TransferSearchBean searchBean = (TransferSearchBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();

		TransferSearchBean.TransferFilterType action = (searchBean.getAction() == null) ? TransferSearchBean.TransferFilterType.SEARCH_LAST15
				: searchBean.getAction();
		Map<String, Object> model = new LinkedHashMap<String, Object>();

		if (action == TransferSearchBean.TransferFilterType.SEARCH_BY_DATE
				&& "LAST_15".equalsIgnoreCase(searchBean.getDate()))
			action = TransferSearchBean.TransferFilterType.SEARCH_LAST15;
		if (action == TransferSearchBean.TransferFilterType.SEARCH_BY_ACCOUNT
				&& "LAST_15".equalsIgnoreCase(searchBean.getAccountNumber()))
			action = TransferSearchBean.TransferFilterType.SEARCH_LAST15;

		if (action == TransferSearchBean.TransferFilterType.MOD_TRANSFER) {
			boolean hasalert = false;
			AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
			if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
				Set<AlertType> alerts = ent.getAlerts();
				if (alerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_EMAIL)
						|| alerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_SMS)
						|| alerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_PUSH)) {
					hasalert = true;
				}
			}

			searchBean.setAction(TransferSearchBean.TransferFilterType.SEARCH_LAST15);

			String tr = searchBean.getModTransfer();
			TVAccountTransfer transfer = null;
			ModTransferBean modbean = null;
			int i = 0;
			for (Iterator<ModTransferBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext();) {
				ModTransferBean name = (ModTransferBean) iterator.next();
				if (name != null)
					if (tr.equalsIgnoreCase(name.getModTransferId())) {
						transfer = Utils.getTransferById(tr, userSession.getPendingTransfers());
						modbean = name;
						break;
					}
				i++;
			}

			if (transfer == null) {
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
					userSession.setStatusmessage("Unable to modify transfer at this time");
				else
					userSession.setStatusmessage("Error modificando la transferencia en este momento");
				return showForm(request, response, error, model);
			}

			TVAccountTransferSource accountTransferSource = new TVAccountTransferSource(new TVFrontendAccount(transfer
					.getTVTransfer().getAccountFrom()));
			TVAccountTransferTarget accountTransferTarget = new TVAccountTransferTarget(new TVFrontendAccount(transfer
					.getTVTransfer().getAccountTo()));
			TVAccountTransfer transferOriginal = new TVAccountTransfer(accountTransferSource, accountTransferTarget);
			transferOriginal.getTVTransfer().setFinalProcessingDate(transfer.getTVTransfer().getEffectiveDate());
			transferOriginal.getTVTransfer().setAmount(transfer.getAmount());
			transferOriginal.setSource(transfer.getSource());
			transferOriginal.getTVTransfer().setFrequency(transfer.getFrequency());
			transferOriginal.getTVTransfer().setRemainingInstances(transfer.getTVTransfer().getRemainingInstances());
			transferOriginal.getTVTransfer().setReferenceNumber(transfer.getTVTransfer().getReferenceNumber());
			// CIBP-1104
			// For null due dates
			Calendar effDt = Calendar.getInstance();
			try {
				effDt.setTime(transfer.getDueDate());
			} catch (Exception e) {
				transferOriginal.getTVTransfer().setDueDate(transfer.getTVTransfer().getEffectiveDate());
			}

			String freq = modbean.getModFreq();
			String inst = modbean.getModInstances();
			String modInstancesType = modbean.getModInstancesType();
			int instances = 1;
			boolean err = false;

			if ("Infinite".equalsIgnoreCase(modInstancesType)
					|| ((inst != null && inst.matches("\\d+")) && Integer.parseInt(inst) > 750))
				instances = 999;
			else if ("variable".equalsIgnoreCase(modInstancesType)
					&& (inst.matches("\\d+") && Integer.parseInt(inst) < 2)) {
				error.rejectValue("favorites[" + i + "].modInstances", "", "Please select two or more instances");
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
					userSession.setStatusmessage("Unable to modify transfer");
				else
					userSession.setStatusmessage("Error modificando la transferencia");
				model.put("freqerror", true);
				model.put("freqerrorid", i);
				err = true;
			} else if (inst != null && !inst.equals(""))
				instances = Integer.parseInt(inst);

			if (!err) {

				TransferResponse reference = null;
				Date dt = modbean.getModDate();
				GregorianCalendar cal = (GregorianCalendar) Calendar.getInstance();
				if (dt != null)
					cal.setTime(dt);

				reference = customerService.modifyTransfer(customer, userSession.getBankingSession(), transfer,
						transfer.getAmount(), cal, TransferFrequency.valueOf(freq).getVaultCode(), instances,
						Utils.generateCustomerInteractionEvent(request));

				List<TVAccountTransfer> listtran = new LinkedList<TVAccountTransfer>();
				listtran.add(transferOriginal); // CIBP-1104

				//MBIM-75
				if (reference != null) {
					if(reference.getTransfer().getTVTransfer().getDueDate() == null) {
						reference.getTransfer().getTVTransfer()
						.setDueDate(reference.getTransfer().getTVTransfer().getEffectiveDate());
					}
					reference.getTransfer().getTVTransfer().setFrequency(TransferFrequency.valueOf(freq).getVaultCode());
					reference.getTransfer().getTVTransfer().setRemainingInstances(instances);
					Calendar c = Calendar.getInstance();
					c.setTime(modbean.getModDate());
					reference.getTransfer().getTVTransfer().setDueDate(c);
					listtran.add(reference.getTransfer());
				}
				//END MBIM-75

				if (reference != null && reference.getStatus() == ResultStatus.SUCCESS) {
					userSession.resetTransfers();
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Transfer was modified.");
					else
						userSession.setStatusmessage("Transferencia ha sido modificada.");

					if (hasalert) {
						customerService.sendTransferModifyConfirmation(customer, listtran, null,
								userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
								userSession.getPreferredLanguage());
					}
				} else {
					if (reference != null) {

						String er = getTelepagoErrorDescriptor(reference.getErrorEBA(), Utils.getRSALanguage(request)
								.toLowerCase());
						String error_str = "";
						if (er == null || er.equals("")) {
							userSession.setErrorDetail(reference.getErrorMessage());
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								userSession.setErrorDetailCod("Error Code: " + reference.getErrorEBA());
								error_str = "Unable to modify transfer at this time"
										+ "<a href='javascript:Show()'> Learn More</a>";
							} else {
								userSession.setErrorDetailCod("C&oacute;digo de error: " + reference.getErrorEBA());
								error_str = "Error modificando la transferencia en este momento."
										+ "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
							}
						} else {
							userSession.setErrorDetail(er);
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								error_str = "Unable to modify transfer at this time"
										+ "<a href='javascript:Show()'> Learn More</a>";
								userSession.setErrorDetailCod("Error Code: " + reference.getErrorEBA());
							} else {
								userSession.setErrorDetailCod("C&oacute;digo de error: " + reference.getErrorEBA());
								error_str = "Error modificando la transferencia en este momento."
										+ "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
							}
						}
						userSession.setStatusmessage(error_str);
					} else {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession.setStatusmessage("Unable to modify transfer at this time");
						else
							userSession.setStatusmessage("Error modificando la transferencia en este momento.");
					}
				}
			}
		} else if (action == TransferSearchBean.TransferFilterType.MOD_AMT) {
			boolean hasalert = false;
			AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
			if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
				Set<AlertType> alerts = ent.getAlerts();
				if (alerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_EMAIL)
						|| alerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_SMS)
						|| alerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_PUSH)) {
					hasalert = true;
				}
			}

			searchBean.setAction(TransferSearchBean.TransferFilterType.SEARCH_LAST15);

			String tr = searchBean.getModTransfer();
			TVAccountTransfer transfer = null;
			ModTransferBean modbean = null;
			for (Iterator<ModTransferBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext();) {
				ModTransferBean name = (ModTransferBean) iterator.next();
				if (name != null)
					if (tr.equalsIgnoreCase(name.getModTransferId())) {
						transfer = Utils.getTransferById(tr, userSession.getPendingTransfers());
						modbean = name;
						break;
					}
			}

			if (transfer == null) {
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
					userSession.setStatusmessage("Unable to modify transfer at this time");
				else
					userSession.setStatusmessage("Error modificando la transferencia en este momento");
				return showForm(request, response, error, model);
			}

			TVAccountTransferSource accountTransferSource = new TVAccountTransferSource(new TVFrontendAccount(transfer
					.getTVTransfer().getAccountFrom()));
			TVAccountTransferTarget accountTransferTarget = new TVAccountTransferTarget(new TVFrontendAccount(transfer
					.getTVTransfer().getAccountTo()));
			TVAccountTransfer transferOriginal = new TVAccountTransfer(accountTransferSource, accountTransferTarget);
			transferOriginal.getTVTransfer().setFinalProcessingDate(transfer.getTVTransfer().getEffectiveDate());
			transferOriginal.getTVTransfer().setAmount(transfer.getAmount());
			transferOriginal.setSource(transfer.getSource());
			transferOriginal.getTVTransfer().setFrequency(transfer.getFrequency());
			transferOriginal.getTVTransfer().setRemainingInstances(transfer.getTVTransfer().getRemainingInstances());
			transferOriginal.getTVTransfer().setReferenceNumber(transfer.getTVTransfer().getReferenceNumber());
			// CIBP-1104
			// For null due dates
			Calendar effDt = Calendar.getInstance();
			try {
				effDt.setTime(transfer.getDueDate());
			} catch (Exception e) {
				transferOriginal.getTVTransfer().setDueDate(transfer.getTVTransfer().getEffectiveDate());
			}

			// IS A SINGLE PAYMENT

			TransferResponse reference = null;
			BigDecimal modamt = modbean.getModAmount();
			if (modamt != null && modamt.doubleValue() >= new Double(0.01d)) {
				transfer.getTVTransfer().setDueDate(transfer.getTVTransfer().getEffectiveDate());
				String freq = transfer.getFrequency();
				int instances = (int) transfer.getTVTransfer().getRemainingInstances();
				freq = FrequencyUtils.getStringFrequency(freq);
				transfer.getTVTransfer().setAmount(modamt);
				Calendar eff = Calendar.getInstance();
				eff.setTime(transfer.getEffectiveDate());

				reference = customerService.modifyTransfer(customer, userSession.getBankingSession(), transfer, modamt,
						eff, TransferFrequency.valueOf(freq).getVaultCode(), instances,
						Utils.generateCustomerInteractionEvent(request));
			}
			if (reference == null) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Unable to modify transfer at this time");
				else
					userSession.setStatusmessage("Error modificando la transferencia en este momento.");
			} else {

				List<TVAccountTransfer> listtran = new LinkedList<TVAccountTransfer>();
				// CIBP-1104
				// For null due dates
				effDt = Calendar.getInstance();
				try {
					effDt.setTime(reference.getTransfer().getDueDate());
				} catch (Exception e) {
					reference.getTransfer().getTVTransfer()
							.setDueDate(reference.getTransfer().getTVTransfer().getEffectiveDate());
				}
				// CIBP-1104
				listtran.add(transferOriginal); // CIBP-1104
				listtran.add(reference.getTransfer());

				if (reference != null && reference.getStatus() == ResultStatus.SUCCESS) {
					userSession.resetTransfers();
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
						userSession.setStatusmessage("Transfer was modified");
					else
						userSession.setStatusmessage("Transferencia ha sido modificada");

					if (hasalert) {
						customerService.sendTransferModifyConfirmation(customer, listtran, null,
								userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
								userSession.getPreferredLanguage());
					}
				} else {
					if (reference != null) {

						String er = getTelepagoErrorDescriptor(reference.getErrorEBA(), Utils.getRSALanguage(request)
								.toLowerCase());
						String error_str = "";
						if (er == null || er.equals("")) {
							userSession.setErrorDetail(reference.getErrorMessage());
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								error_str = "Unable to modify transfer at this time"
										+ "<a href='javascript:Show()'> Learn More</a>";
								userSession.setErrorDetailCod("Error Code: " + reference.getErrorEBA());
							} else {
								error_str = "Error modificando la transferencia en este momento."
										+ "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
								userSession.setErrorDetailCod("C&oacute;digo de error: " + reference.getErrorEBA());
							}
						} else {
							userSession.setErrorDetail(er);
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								error_str = "Unable to modify transfer at this time"
										+ "<a href='javascript:Show()'> Learn More</a>";
								userSession.setErrorDetailCod("Error Code: " + reference.getErrorEBA());

							} else {
								userSession.setErrorDetailCod("C&oacute;digo de error: " + reference.getErrorEBA());
								error_str = "Error modificando la transferencia en este momento."
										+ "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
							}
						}
						userSession.setStatusmessage(error_str);
					}
				}
			}

		} else if (action == TransferSearchBean.TransferFilterType.CAN_TRANSFER) {
			boolean hasalert = false;
			AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
			if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
				Set<AlertType> alerts = ent.getAlerts();
				// Defect 920 Change TRANSFERS_CANCEL_EMAIL to
				// TRANSFERS_CANCEL_FUTURE_EMAIL
				if (alerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_EMAIL)
						|| alerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_SMS)
						|| alerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_PUSH)) {
					hasalert = true;
				}
			}

			searchBean.setAction(TransferSearchBean.TransferFilterType.SEARCH_LAST15);
			String tr = searchBean.getModTransfer();

			TVAccountTransfer transfer = null;
			if (tr != null && !tr.equalsIgnoreCase("") && userSession.getPendingTransfers() != null)
				transfer = Utils.getTransferById(tr, userSession.getPendingTransfers());

			if (transfer == null) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Unable to delete transfer at this time");
				else
					userSession.setStatusmessage("Error borrando la transferencia en este momento.");
				return showForm(request, response, error, model);
			}

			TransferResponse tres = customerService.cancelTransfer(customer, userSession.getBankingSession(), transfer,
					Utils.generateCustomerInteractionEvent(request));
			List<TVAccountTransfer> listtran = new LinkedList<TVAccountTransfer>();
			// CIBP-1104
			// For null due dates
			Calendar effDt = Calendar.getInstance();
			try {
				effDt.setTime(transfer.getDueDate());
			} catch (Exception e) {
				transfer.getTVTransfer().setDueDate(transfer.getTVTransfer().getEffectiveDate());
			}

			listtran.add(transfer);
			if (tres != null && tres.getStatus() == ResultStatus.SUCCESS) {
				userSession.resetTransfers();
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Transfer was deleted.");
				else
					userSession.setStatusmessage("Transferencia ha sido borrada.");

				if (hasalert) {
					customerService.sendTransferCancelConfirmation(customer, listtran, null,
							userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
							userSession.getPreferredLanguage());
				}
			} else {
				if (tres != null) {

					String er = getTelepagoErrorDescriptor(tres.getErrorEBA(), Utils.getRSALanguage(request)
							.toLowerCase());
					String error_str = "";
					if (er == null || er.equals("")) {
						userSession.setErrorDetail(tres.getErrorMessage());
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							userSession.setErrorDetailCod("Error Code: " + tres.getErrorEBA());
							error_str = "Unable to delete transfer at this time"
									+ "<a href='javascript:Show()' > Learn More</a>";
						} else {
							userSession.setErrorDetailCod("C&oacute;digo de error: " + tres.getErrorEBA());
							error_str = "Error borrando la transferencia en este momento."
									+ "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
						}
					} else {
						userSession.setErrorDetail(er);
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							error_str = "Unable to delete transfer at this time"
									+ "<a href='javascript:Show()' > Learn More</a>";
							userSession.setErrorDetailCod("Error Code: " + tres.getErrorEBA());
						} else {
							userSession.setErrorDetailCod("C&oacute;digo de error: " + tres.getErrorEBA());
							error_str = "Error borrando la transferencia en este momento."
									+ "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
						}
					}
					userSession.setStatusmessage(error_str);
				} else {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Unable to delete transfer at this time");
					else
						userSession.setStatusmessage("Error borrando la transferencia en este momento.");
				}
			}

		}

		return showForm(request, response, error, model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		AccountMaskUtil mask = new AccountMaskUtil();
		NumberTool numberTool = new NumberTool();
		TransferSearchBean searchBean = (TransferSearchBean) command;
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();

		Map<String, String> accountsFromMap = new LinkedHashMap<String, String>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			accountsFromMap.put("none", "Select an account");
		else
			accountsFromMap.put("none", "Seleccione una cuenta");
		for (Iterator<TVAccountTransferSource> iterator = userSession.getAccountsTransferFrom().iterator(); iterator
				.hasNext();) {
			TVAccountTransferSource from = (TVAccountTransferSource) iterator.next();
			TVFrontendAccount frontFrom = from.getAccount();

			if (frontFrom != null && frontFrom.getPortalBalance() != null)
				accountsFromMap.put(
						from.getFrontEndId(),
						frontFrom.getNickname()
								+ " "
								+ mask.mask(frontFrom.getAccountNumber())
								+ ((frontFrom.getAccountNumberSuffix() != null && !frontFrom.getAccountNumberSuffix()
										.equals("")) ? " " + frontFrom.getAccountNumberSuffix() : "") + " "
								+ numberTool.format("currency", frontFrom.getPortalBalance()));
			else
				accountsFromMap.put(from.getFrontEndId(), (frontFrom != null ? frontFrom.getNickname() : "")
						+ " "
						+ mask.mask(frontFrom.getAccountNumber())
						+ ((frontFrom.getAccountNumberSuffix() != null && !frontFrom.getAccountNumberSuffix()
								.equals("")) ? " " + frontFrom.getAccountNumberSuffix() : ""));
		}

		Map<String, String> accountsToMap = new LinkedHashMap<String, String>();
		Map<String, TVAccountTransferTarget> accountsToObjectMap = new LinkedHashMap<String, TVAccountTransferTarget>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			accountsToMap.put("LAST_15", "All Accounts");
		else
			accountsToMap.put("LAST_15", "Todas las cuentas");
		for (Iterator<TVAccountTransferTarget> iterator = userSession.getAccountsTransferTo().iterator(); iterator
				.hasNext();) {
			TVAccountTransferTarget to = (TVAccountTransferTarget) iterator.next();
			TVFrontendAccount frontTo = to.getAccount();

			StringBuffer sb = new StringBuffer();

			sb.append(frontTo.getNickname()).append(" ").append(mask.mask(frontTo.getAccountNumber()));

			if (frontTo.getAccountNumberSuffix() != null && !frontTo.getAccountNumberSuffix().equals("")) {
				sb.append(" ");
				sb.append(frontTo.getAccountNumberSuffix());
			}

			if (frontTo.getPortalBalance() != null)
				sb.append(" ").append(numberTool.format("currency", frontTo.getPortalBalance()));

			accountsToMap.put(to.getFrontEndId(), sb.toString());
			accountsToObjectMap.put(to.getFrontEndId(), to);
		}

		Map<String, Object> model = new HashMap<>();

		model.put("fromaccounts", accountsFromMap);
		model.put("toaccounts", accountsToMap);
		model.put("toaccountsobjects", accountsToObjectMap);
		model.put("favorites", userSession.getFavoritesTransfers());

		Map<String, Object> datemap = new LinkedHashMap<String, Object>();
		List<Date> allDates = new LinkedList<Date>();
		// 12 meses
		Calendar now = Calendar.getInstance();
		SimpleDateFormat simp = new SimpleDateFormat("yyyyMM");
		SimpleDateFormat month1 = new SimpleDateFormat("MMMMM");

		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			datemap.put("LAST_15", "Recent Transfers");
		else
			datemap.put("LAST_15", "Transferencias Recientes");

		for(int i = 1; i < 11; i++) {
			now.add(Calendar.MONTH, -1);
			datemap.put(simp.format(now.getTime()), month1.format(now.getTime()) + " " + now.get(Calendar.YEAR));
			allDates.add(new Date(now.getTimeInMillis()));
		}
		
		model.put("dateMap", datemap);
		model.put("alldates", allDates);
		model.put("showMenu", true);
		model.put("language", Utils.getRSALanguage(request));

		// Set up today's date
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Calendar n = Calendar.getInstance();
		if (Calendar.SATURDAY == n.get(Calendar.DAY_OF_WEEK))
			n.add(Calendar.DAY_OF_WEEK, 2);
		else if (Calendar.SUNDAY == n.get(Calendar.DAY_OF_WEEK))
			n.add(Calendar.DAY_OF_WEEK, 1);

		model.put("todayDate", df.format(n.getTime()));
		df = new SimpleDateFormat("MM");
		model.put("month", df.format(n.getTime()));
		df = new SimpleDateFormat("dd");
		model.put("day", df.format(n.getTime()));

		TransferSearchBean.TransferFilterType action = (searchBean.getAction() == null) ? TransferSearchBean.TransferFilterType.SEARCH_LAST15
				: searchBean.getAction();
		int transferSize = maxTransfers;
		int month = 0;
		int year = Calendar.getInstance().get(Calendar.YEAR);

		TVFrontendAccount account = null;
		if (action == TransferSearchBean.TransferFilterType.SEARCH_BY_DATE
				&& "LAST_15".equalsIgnoreCase(searchBean.getDate()))
			action = TransferSearchBean.TransferFilterType.SEARCH_LAST15;
		if (action == TransferSearchBean.TransferFilterType.SEARCH_BY_ACCOUNT
				&& "LAST_15".equalsIgnoreCase(searchBean.getAccountNumber()))
			action = TransferSearchBean.TransferFilterType.SEARCH_LAST15;
		TransferFilterType searchType = TransferFilterType.LAST_15;

		if (action == TransferSearchBean.TransferFilterType.SEARCH_LAST15) {
			searchType = TransferFilterType.LAST_15;
			transferSize = 15;
		} else if (action == TransferSearchBean.TransferFilterType.SEARCH_BY_DATE) {
			searchType = TransferFilterType.BY_MONTH;
			String date = searchBean.getDate();
			if (date != null && !"none".equals(date) && !date.equalsIgnoreCase("LAST_15")) {
				year = Integer.parseInt(date.substring(0, 4));
				month = Integer.parseInt(date.substring(4, date.length())) - 1;
			} else {
				searchType = TransferFilterType.LAST_15;
				transferSize = 15;
				date = null;
			}
		} else if (action == TransferSearchBean.TransferFilterType.SEARCH_BY_ACCOUNT) {
			searchType = TransferFilterType.BY_ACCOUNT;
			String act = searchBean.getAccountNumber();
			if (act != null && !"none".equals(act))
				account = Utils.getAccountTarget(act, userSession.getAccountsTransferTo()).getAccount();
			else {
				searchType = TransferFilterType.LAST_15;
				transferSize = 15;
			}
		}

		Calendar searchdate = Calendar.getInstance();
		if (year != 0)
			searchdate.set(year, month, 1);

		TransferHistoryResponse transferResponse = customerService.getTransferHistory(customer,
				userSession.getBankingSession(), searchType, transferSize, account, searchdate);

		List<TVAccountTransfer> transfers = new LinkedList<TVAccountTransfer>();
		List<TVAccountTransfer> pending = new LinkedList<TVAccountTransfer>();

		// Get the user's PIF accounts
		List<PIFExternalAccount> pifExternalAccounts = customerService.getConfirmedPIFExternalAccounts(userSession
				.getCustomerProfile().getTaxId());

		if (transferResponse.getTransfers() != null)
			for (Iterator<TVAccountTransfer> iterator = transferResponse.getTransfers().iterator(); iterator.hasNext();) {
				TVAccountTransfer name = (TVAccountTransfer) iterator.next();

				TVFrontendAccount s = Utils.getAccountSourceByKey(name.getSource().getAccount().getAccountKey(),
						userSession.getAccountsTransferFrom());
				if (s != null)
					name.setSource(new TVAccountTransferSource(Utils.getAccountSourceByKey(name.getSource()
							.getAccount().getAccountKey(), userSession.getAccountsTransferFrom())));
				TVFrontendAccount t = Utils.getAccountTargetByKey(name.getTarget().getAccount().getAccountKey(),
						userSession.getAccountsTransferTo());
				if (t != null)
					name.setTarget(new TVAccountTransferTarget(Utils.getAccountTargetByKey(name.getTarget()
							.getAccount().getAccountKey(), userSession.getAccountsTransferTo())));
				if (name.getSource().getAccount().getNickname().equals("")
						|| name.getSource().getAccount().getNickname() == null)
					name.getSource().getAccount().setNickname(assignSourceNickname(userSession, name));
				if (name.getTarget().getAccount().getNickname().equals("")
						|| name.getTarget().getAccount().getNickname() == null)
					name.getTarget().getAccount().setNickname(assignTargetNickname(userSession, name));

				// If the source still does not have a nickname, check against
				// the PIF accounts
				if (name.getSource().getAccount().getNickname().equals("")
						|| name.getSource().getAccount().getNickname() == null) {
					if (pifExternalAccounts != null) {
						for (Iterator<PIFExternalAccount> pifIterator = pifExternalAccounts.iterator(); pifIterator
								.hasNext();) {
							PIFExternalAccount from = (PIFExternalAccount) pifIterator.next();
							if (from.getAccountNumber().equals(name.getSource().getAccount().getAccountNumber())
									&& from.getRoutingNumber().equals(name.getSource().getAccount().getBankId())) {
								name.getSource().getAccount().setNickname(from.getNickname());
								break;
							}
						}
					}
				}

				// If the source STILL does not have a nickname, check against
				// the PIF accounts by section
				if (name.getSource().getAccount().getNickname().equals("")
						|| name.getSource().getAccount().getNickname() == null) {
					if (pifExternalAccounts != null) {
						for (Iterator<PIFExternalAccount> pifIterator = pifExternalAccounts.iterator(); pifIterator
								.hasNext();) {
							PIFExternalAccount from = (PIFExternalAccount) pifIterator.next();
							if (from.getAccountNumber().equals(name.getSource().getAccount().getAccountNumber())) {
								// Checking
								if (name.getSource().getAccount().getAccountSection().equals("C")
										&& from.getAccountType() == 'C') {
									name.getSource().getAccount().setNickname(from.getNickname());
									break;
								}
								// Savings
								else if (name.getSource().getAccount().getAccountSection().equals("S")
										&& from.getAccountType() == 'S') {
									name.getSource().getAccount().setNickname(from.getNickname());
									break;
								} else {
									name.getSource().getAccount().setNickname(from.getNickname());
									break;
								}
							}
						}
					}
				}

				transfers.add(name);
			}

		BigDecimal totalInProcess = new BigDecimal(0.0d);

		if (transferResponse.getInprocessTransfers() != null)
			for (Iterator<TVAccountTransfer> iterator = transferResponse.getInprocessTransfers().iterator(); iterator
					.hasNext();) {
				TVAccountTransfer name = (TVAccountTransfer) iterator.next();

				TVFrontendAccount s = Utils.getAccountSourceByKey(name.getSource().getAccount().getAccountKey(),
						userSession.getAccountsTransferFrom());
				if (s != null)
					name.setSource(new TVAccountTransferSource(Utils.getAccountSourceByKey(name.getSource()
							.getAccount().getAccountKey(), userSession.getAccountsTransferFrom())));
				TVFrontendAccount t = Utils.getAccountTargetByKey(name.getTarget().getAccount().getAccountKey(),
						userSession.getAccountsTransferTo());
				if (t != null)
					name.setTarget(new TVAccountTransferTarget(Utils.getAccountTargetByKey(name.getTarget()
							.getAccount().getAccountKey(), userSession.getAccountsTransferTo())));
				else {
					t = Utils.getAccountByKey(name.getTarget().getAccount().getAccountKey(), userSession.getAccounts());
					if (t != null)
						name.setTarget(new TVAccountTransferTarget(Utils.getAccountTargetByKey(name.getTarget()
								.getAccount().getAccountKey(), userSession.getAccountsTransferTo())));
				}

				if (name.getTVTransfer().getRemainingInstances() == 0
						&& name.getTVTransfer().getFinalProcessingDate() == null) {
					name.getTVTransfer().setRemainingInstances(999);
				}
				totalInProcess = totalInProcess.add(name.getAmount());
				if (name.getSource().getAccount().getNickname().equals("")
						|| name.getSource().getAccount().getNickname() == null)
					name.getSource().getAccount().setNickname(assignSourceNickname(userSession, name));
				if (name.getTarget().getAccount().getNickname().equals("")
						|| name.getTarget().getAccount().getNickname() == null)
					name.getTarget().getAccount().setNickname(assignTargetNickname(userSession, name));

				// If the source still does not have a nickname, check against
				// the PIF accounts
				if (name.getSource().getAccount().getNickname().equals("")
						|| name.getSource().getAccount().getNickname() == null) {
					if (pifExternalAccounts != null) {
						for (Iterator<PIFExternalAccount> pifIterator = pifExternalAccounts.iterator(); pifIterator
								.hasNext();) {
							PIFExternalAccount from = (PIFExternalAccount) pifIterator.next();
							if (from.getAccountNumber().equals(name.getSource().getAccount().getAccountNumber())
									&& from.getRoutingNumber().equals(name.getSource().getAccount().getBankId())) {
								name.getSource().getAccount().setNickname(from.getNickname());
								break;
							}
						}
					}
				}

				pending.add(name);
			}

		model.put("totalInProcess", totalInProcess);
		Collections.sort(transfers, new TransferSort());
		model.put("processedTransfers", transfers);
		Collections.sort(pending, new TransferSort());
		model.put("pendingTransfers", pending);
		userSession.setPendingTransfers(pending);

		return model;

	}

	public int getMaxTransfers() {
		return maxTransfers;
	}

	public void setMaxTransfers(int maxTransfers) {
		this.maxTransfers = maxTransfers;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		TransferSearchBean searchBean = (TransferSearchBean) command;
		TransferSearchBean.TransferFilterType action = searchBean.getAction();

		if (action == TransferSearchBean.TransferFilterType.MOD_TRANSFER) {
			String tr = searchBean.getModTransfer();
			TVAccountTransfer payment = null;
			ModTransferBean modbean = null;
			for (Iterator<ModTransferBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext();) {
				ModTransferBean name = (ModTransferBean) iterator.next();
				if (name != null)
					if (tr.equalsIgnoreCase(name.getModTransferId())) {
						payment = Utils.getTransferById(tr, userSession.getPendingTransfers());
						modbean = name;
						break;
					}
			}

			if (payment != null) {
				// IS A RECURRING PAYMENT
				String freq = modbean.getModFreq();
				String inst = modbean.getModInstances();
				String modInstancesType = modbean.getModInstancesType();
				Date modDate = modbean.getModDate();
				Calendar calModDate = Calendar.getInstance();
				int favId = searchBean.getFavId();
				boolean err = false;

				if (GenericValidator.isBlankOrNull(payment.getFrequency())
						|| GenericValidator.isBlankOrNull(freq)
						|| GenericValidator.isBlankOrNull(modInstancesType)
						|| (!"infinite".equalsIgnoreCase(modInstancesType) && (!GenericValidator.matchRegexp(inst,
								"^([1-9]|0[1-9]|[1-9][0-9]|[1-9][0-9]{1,2}|[0-9]{1,2}[0-9])$"))) || modDate == null) {

					err = true;

					if (GenericValidator.isBlankOrNull(payment.getFrequency()) || GenericValidator.isBlankOrNull(freq))
						errors.rejectValue("favorites[" + favId + "].modFreq", "transferhistory.frequency.invalid",
								"Frequency is required");
					if (GenericValidator.isBlankOrNull(modInstancesType)
							|| (!"infinite".equalsIgnoreCase(modInstancesType) && (!GenericValidator.matchRegexp(inst,
									"^([1-9]|0[1-9]|[1-9][0-9]|[1-9][0-9]{1,2}|[0-9]{1,2}[0-9])$"))))
						errors.rejectValue("favorites[" + favId + "].modInstances",
								"transferhistory.instances.invalid", "Instances must be filled");
					if (modDate == null) {
						modbean.setPendingDate(modDate);
						errors.rejectValue("favorites[" + favId + "].modDate", "transferhistory.date.invalid",
								"Date is required");

					} else {
						calModDate.setTime(modDate);
						Calendar now = Calendar.getInstance();
						err = calModDate.before(now);
						if (err) {
							modbean.setPendingDate(modDate);
							errors.rejectValue("favorites[" + favId + "].modDate", "transferhistory.dateafter.invalid",
									"Date must be after today.");
						} else {
							now.add(Calendar.YEAR, 1);
							err = calModDate.after(now);
							if (err) {
								modbean.setPendingDate(modDate);
								errors.rejectValue("favorites[" + favId + "].modDate",
										"transferhistory.dateafter.invalid", "Date must be after today.");
							}
						}
					}
				}
			}
		} else if (action == TransferSearchBean.TransferFilterType.MOD_AMT) {
			String modPayment = searchBean.getModTransfer();
			ModTransferBean modbean = null;
			int favId = searchBean.getFavId();

			for (Iterator<ModTransferBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext();) {
				ModTransferBean name = (ModTransferBean) iterator.next();
				if (name != null)
					if (modPayment.equalsIgnoreCase(name.getModTransferId())) {
						modbean = name;
						break;
					}
			}
			// IS A SINGLE PAYMENT
			if (modbean != null) {
				BigDecimal amt = modbean.getModAmount();
				if (amt == null || amt.doubleValue() < new Double(0.01d)) {
					errors.rejectValue("favorites[" + favId + "].modAmount", "", "Amount is required");
				}
			} else {
				errors.rejectValue("favorites[" + favId + "].modAmount", "", "Amount is required");
			}

		}
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.TRANSFERS;
	}

	public String assignSourceNickname(UserSession userSession, TVAccountTransfer transfer) {
		// Get all the accounts and the source account key
		String nickname = "";
		String accountKey = transfer.getSource().getAccount().getCustomerAccountPreferences().getKey();
		List<TVFrontendAccount> accounts = userSession.getAccounts();

		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount acc = (TVFrontendAccount) iterator.next();
			if (acc.getAccountKey().equals(accountKey)) {
				nickname = acc.getNickname();
				break;
			}
		}
		return nickname;
	}

	public String assignTargetNickname(UserSession userSession, TVAccountTransfer transfer) {
		// Get all the accounts and the source account key
		String nickname = "";
		String accountKey = transfer.getTarget().getAccount().getCustomerAccountPreferences().getKey();
		List<TVFrontendAccount> accounts = userSession.getAccounts();

		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount acc = (TVFrontendAccount) iterator.next();
			if (acc.getAccountKey().equals(accountKey)) {
				nickname = acc.getNickname();
				break;
			}
		}
		return nickname;
	}

	private String getTelepagoErrorDescriptor(String codigo, String len) {
		String status = "";
		try {
			if (!"en".equals(len))
				len = "es";
			status = this.messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
		} catch (Exception e) {
			status = "";
		}

		return status;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
}
