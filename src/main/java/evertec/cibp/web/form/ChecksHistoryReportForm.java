package evertec.cibp.web.form;


import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import com.google.common.collect.ImmutableList;

import evertec.cibp.core.models.customer.applications.ApplicationStatus;
import evertec.cibp.core.models.customer.applications.CustomerApplication;
import evertec.cibp.core.models.customer.applications.CustomerApplicationType;
import evertec.cibp.core.models.fraud.prevention.IncomingChecks;
import evertec.cibp.core.models.fraud.prevention.IncomingChecksDecision;
import evertec.cibp.core.models.fraud.prevention.IncomingChecksStatus;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.AccountsSectionOverwriteService;
import evertec.cibp.core.utils.FraudPreventionUtils;
import evertec.cibp.web.form.bean.ChecksHistoryReportBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.HasSectionUtil;
import evertec.cibp.web.utils.Utils;

public class ChecksHistoryReportForm extends CancellableFormController{
	
	protected final Log myLogger = LogFactory.getLog(getClass());
	private CustomerServices customerServices;
	private AccountsSectionOverwriteService accountsSectionOverwriteServie;
	private MessageSource messageSource;
	private String businessAccounts;
	
	public ChecksHistoryReportForm() {
		super();
		setCommandClass(ChecksHistoryReportBean.class);
		setCommandName("checkshistory");
		setFormView("checkshistoryreport");
	}
	
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		Map<String, Object> model = new LinkedHashMap<>();
		return showForm(request, error, "checkshistoryreport", model);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
	}
	
	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		
		Calendar now = Calendar.getInstance();
		model.put("day", now.get(Calendar.DAY_OF_MONTH));
		model.put("thisYear", now.get(Calendar.YEAR));
		model.put("month", now.get(Calendar.MONTH)+1);
		model.put("nextYear", now.get(Calendar.YEAR)+1);
		
		Map<String, String> accountsFilterModel = new LinkedHashMap<>();
		AccountMaskUtil maskutil = new AccountMaskUtil();
		
		if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
			accountsFilterModel.put("none", "All accounts");
		else
			accountsFilterModel.put("none", "Todas las cuentas");
		
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<CustomerApplication> listCustApp = Utils.getCustomerAppbyType(userSession.getCustomerApplications(), CustomerApplicationType.VIEW_HISTORY_CHECKS_MODIFY, ApplicationStatus.APPROVED);
		List<String> acctArray  = FraudPreventionUtils.getAccountsbyAppType(listCustApp);
		List<ChecksHistoryReportBean> checkHistReport = new ArrayList<>();
		List<String> noAvailEccounts = userSession.getCustomerService().getDCINotAvailableEaccounts(userSession.getCustomerProfile());
		List<String> notAllowedAccounts = HasSectionUtil.hasAccountsSectionNotAllowed(HasSectionUtil.FRAUD_PREVENTION_SERVICE_SECTION, userSession);
		List<String> businessAccounts = ImmutableList.copyOf(this.getBusinessAccounts().split(","));
		String acctSelect = request.getParameter("account");
		String dateFrom = request.getParameter("dateFrom");
		String dateTo = request.getParameter("dateTo");	
		int maxResult;
		
		
		String lblDateFrom;
		String lblDateTo;
		
		if(dateFrom==null || "".equals(dateFrom))
			lblDateFrom ="mm/dd/yyyy";
		else
			lblDateFrom = dateFrom;
		
		if(dateTo==null || "".equals(dateTo))
			lblDateTo ="mm/dd/yyyy";
		else
			lblDateTo = dateTo;
		
		model.put("lblDateFrom", lblDateFrom);
		model.put("lblDateTo", lblDateTo);
		String accountOpen= "Default";
		for (Iterator<TVAccountTransferSource> iterator = userSession.getAccountsPaymentFrom().iterator(); iterator.hasNext();) {
			TVAccountTransferSource account = iterator.next();
			ChecksHistoryReportBean bean = new ChecksHistoryReportBean();
			String productId = account.getAccount().getProductDefinition().getProductId();
			String accountNumber = account.getAccount().getAccountNumber();
			String nickName = account.getAccount().getNickname();
			
			try {
				if(request.getParameter("maxResult"+accountNumber) == null) 
					maxResult = 5;
				else {
					maxResult = Integer.parseInt(request.getParameter("maxResult"+accountNumber));
					accountOpen = accountNumber;
				}
		    } catch (NumberFormatException e) {
		    	maxResult = 5;
		    }
			
			Boolean isAcctSelected = true;
			if(acctSelect!=null && !acctSelect.equalsIgnoreCase("none")) {
				if(!accountNumber.equalsIgnoreCase(acctSelect))
					isAcctSelected = false;		
			}
			
			if(acctArray.contains(accountNumber) && !notAllowedAccounts.contains(accountNumber) && businessAccounts.contains(productId) && !noAvailEccounts.contains(accountNumber)) {
				accountsFilterModel.put(accountNumber, nickName+" ... "+maskutil.mask(accountNumber));
				if(isAcctSelected) {
					Map<Object, Object> searchCriteria = new HashMap<>();
					searchCriteria.put("accountNumber", accountNumber);
					searchCriteria.put("IncomingChecksStatus", IncomingChecksStatus.ALL);
					searchCriteria.put("dateFrom", dateFrom);
					searchCriteria.put("dateTo", dateTo);
					List<IncomingChecks> incomingChecks = customerServices.getIncomingChecksByFilter(searchCriteria, maxResult);
					List<IncomingChecks> incomingChecksTot = customerServices.getIncomingChecksByFilter(searchCriteria, 15);		
					boolean showLoadMore = false;
					if(incomingChecksTot.size() > 5 && maxResult == 5)
						showLoadMore = true;
					
					bean.setShowLoadMore(showLoadMore);
					bean.setAccount(accountNumber);
					bean.setAccountMask(maskutil.mask(accountNumber));
					bean.setNickName(nickName);
					bean.setCheckSize(incomingChecks.size());
					List<IncomingChecks> incoCheckList = new ArrayList<>();
					List<String> checkNumberList = new ArrayList<>();
					List<BigDecimal> checkAmountList = new ArrayList<>();
					List<String> checkDateList = new ArrayList<>();
					List<String> incoCheckIdList = new ArrayList<>();
					List<IncomingChecksDecision> checkDecList = new ArrayList<>();
					List<Boolean> isDefault = new ArrayList<>();
					if(!incomingChecks.isEmpty())
					{
						boolean haveChecks = false;
						for (Iterator<IncomingChecks> iteratorChecks = incomingChecks.iterator(); iteratorChecks.hasNext();) {
							IncomingChecks check = iteratorChecks.next();
							haveChecks = true;
							incoCheckList.add(check);
							checkNumberList.add(check.getCheckId());
							checkAmountList.add(check.getAmount());
							incoCheckIdList.add(check.getIncomingCheckId());
							String newDate = new SimpleDateFormat("MM/d/Y").format(check.getIssueDate());  
							checkDateList.add(newDate);
							checkDecList.add(check.getDecision());
							if(check.getProfileIdDecision() != null)
								isDefault.add(false);
							else
								isDefault.add(true);
							
							model.put("haveChecks", haveChecks);
						}
						
						
					}
					if(!incoCheckList.isEmpty())
					{
						bean.setCheckNumberList(checkNumberList);
						bean.setChecksList(incoCheckList);
						bean.setAmount(checkAmountList);
						bean.setIssueDate(checkDateList);
						bean.setDecision(checkDecList);
						bean.setIsDefault(isDefault);
						bean.setIncoCheckIdList(incoCheckIdList);
						
					}
					checkHistReport.add(bean);
				}
			}
		}
		model.put("accountsFilter", accountsFilterModel);
		model.put("checkReport", checkHistReport);
		model.put("accountOpen", accountOpen);
		return model;
	}
	
	public String getBusinessAccounts() {
		if (businessAccounts == null) {
			businessAccounts = "";
		}
		return businessAccounts;
	}

	public void setBusinessAccounts(String businessAccounts) {
		this.businessAccounts = businessAccounts;
	}
	
	public CustomerServices getCustomerServices() {
		return customerServices;
	}
	
	public void setCustomerServices(CustomerServices customerServices) {
		this.customerServices = customerServices;
	}
	
	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public AccountsSectionOverwriteService getAccountsSectionOverwriteServie() {
		return accountsSectionOverwriteServie;
	}

	public void setAccountsSectionOverwriteServie(AccountsSectionOverwriteService accountsSectionOverwriteServie) {
		this.accountsSectionOverwriteServie = accountsSectionOverwriteServie;
	}

}
