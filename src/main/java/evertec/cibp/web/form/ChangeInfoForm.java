package evertec.cibp.web.form;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.core.utils.CibpValidationUtils;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.form.bean.ChangeInfoBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChangeInfoForm extends AbstractWizardFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private String cancelView;

	public ChangeInfoForm() {
		super();
		setCommandClass(ChangeInfoBean.class);
		setCommandName("changeinfo");
		setPages(new String[] { "changeinfo", "changeinfo", "changeinfo" });
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();
		ChangeInfoBean info = (ChangeInfoBean) command;
		ModifyCustomerRequest modifyCustomerRequest = new ModifyCustomerRequest(
				Utils.generateCustomerInteractionEvent(request), profile.getProfileId(), profile.getPasswordHash(),
				profile.getPasswordSalt(), "", 0, "", profile.getStatus(), null, session.getDeviceRequest());
		boolean fail = false;

		List<ASK_FOR> tasksList = session.getTasksList();
		if (tasksList.contains(ASK_FOR.CHANGE_PASSWORD)) {
			if (GenericValidator.isBlankOrNull(info.getPassword())) {
				errors.rejectValue("password", "", "Required");
				fail = true;
			} else if (!info.getPassword().equals(info.getPasswordconfirm())) {
				errors.rejectValue("passwordconfirm", "", "Passwords fields must match");
				fail = true;
			} else {
				Integer generatedRandomSalt = PasswordHashUtil.genereateRandomSalt();
				modifyCustomerRequest.setNewPasswordHash(PasswordHashUtil.generateSaltedHash(info.getPassword(),
						generatedRandomSalt));
				modifyCustomerRequest.setNewPasswordSalt(generatedRandomSalt);
				modifyCustomerRequest.setProfileId(profile.getProfileId());
			}

		}
		if (tasksList.contains(ASK_FOR.CHANGE_USERNAME)) {
			if (GenericValidator.isBlankOrNull(info.getUsername())) {
				errors.rejectValue("username", "", "Required");
				fail = true;
			} else if (!CibpValidationUtils.isValidUsername(info.getUsername())) {
				errors.rejectValue("username", "", "Invalid username");
				fail = true;
			} else {
				modifyCustomerRequest.setNewUsername(info.getUsername());
			}

		}
		if (fail) {
			return showForm(request, response, errors);
		}
		modifyCustomerRequest.setDeviceRequest(session.getDeviceRequest());

		modifyCustomerRequest.setRsaLang("sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "ES" : "EN");

		ModifyCustomerProfileResponse r = session.getCustomerService().modifyCustomerUsername(modifyCustomerRequest);
		if (ResultStatus.SUCCESS.equals(r.getStatus())) {
			session.setCustomerProfile(r.getCustomerProfile());
			tasksList.remove(ASK_FOR.CHANGE_USERNAME);
			tasksList.remove(ASK_FOR.CHANGE_PASSWORD);
		}

		return new ModelAndView("redirect:portal");
	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		return new ModelAndView(cancelView);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<String, Object> model = new HashMap<>();

		List<ASK_FOR> tasksList = session.getTasksList();
		for (ASK_FOR thing : tasksList) {
			switch (thing) {
			case CHANGE_PASSWORD:
				model.put("changepassword", "true");
				break;
			case CHANGE_USERNAME:
				model.put("changeusername", "true");
				break;

			default:
				break;
			}
		}

		return model;
	}

	public String getCancelView() {
		return cancelView;
	}

	public void setCancelView(String cancelView) {
		this.cancelView = cancelView;
	}

}
