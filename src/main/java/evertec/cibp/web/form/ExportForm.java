package evertec.cibp.web.form;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractStatement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.StatementRequest;
import evertec.cibp.core.utils.InternationalCharacterUtils;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.form.bean.ExportFormBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.view.FileView;

public class ExportForm extends SimpleFormController {

	protected final Log logger = LogFactory.getLog(getClass());
	private int maxStatements;
	private int maxTransactions;
	private String intuitCredentials;

	public SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	Date dl = Calendar.getInstance().getTime();

	public ExportForm() {
		super();
		setCommandClass(ExportFormBean.class);
		setCommandName("export");
		setFormView("export");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object export,
			BindException error) throws Exception {

		Map<String, Object> model = new HashMap<>();
		model.put("maxStatements", maxStatements);
		model.put("maxTransactions", maxTransactions);
		model.put("intuitCredentials", intuitCredentials);

		ExportFormBean transactionData = (ExportFormBean) export;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		CustomerServices customerService = userSession.getCustomerService();
		BankingSession bankingSession = userSession.getBankingSession();
		CustomerProfile customerProfile = userSession.getCustomerProfile();

		String key = transactionData.getAccount();
		if (key == null || key.equals("") || key.equals("none") || key.equals("Select an account"))
			return showForm(request, response, error, model);

		TVFrontendAccount account = Utils.getAccountById(key, userSession.getAccounts());
		model.put("account", account);

		if (!account.getFeatures().isShowStatement()) {
			error.reject("account.invalid");
			return showForm(request, response, error, model);
		}

		String cycle = transactionData.getExpcycle();
		String expType = transactionData.getExportTypeFormat();
		String searchType = transactionData.getSearchType();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		Calendar startDate = null;
		Calendar endDate = null;
		if (transactionData.getExpStartDate() != null) {
			startDate = Calendar.getInstance();
			startDate.setTime(transactionData.getExpStartDate());
		}
		if (transactionData.getExpEndDate() != null) {
			endDate = Calendar.getInstance();
			endDate.setTime(transactionData.getExpEndDate());
		}

		model.put("searchType", searchType);
		model.put("expType", expType);
		model.put("cycle", cycle);
		model.put("startDate", startDate);
		model.put("endDate", endDate);
		/**
		 * Here is the IDA, CCA statements implementation
		 */
		List<AbstractStatement> availableStatements = null;
		if (account.getStatements() == null) {
			StatementRequest stmtrq = new StatementRequest(Utils.generateCustomerInteractionEvent(request),
					customerProfile, bankingSession, account, maxStatements);
			availableStatements = customerService.getAvailableStatements(stmtrq).getStatementList();
			account.setStatements(availableStatements);
		} else
			availableStatements = account.getStatements();

		LinkedHashMap<String, AbstractStatement> accountStatementMap = new LinkedHashMap<String, AbstractStatement>();
		LinkedHashMap<String, String> statementsMap = new LinkedHashMap<String, String>();
		statementsMap = Utils.getStatements(availableStatements, accountStatementMap, Utils.getRSALanguage(request),
				account.getSubtype().equals("PRM"));
		model.put("statementsMap", statementsMap);
		model.put("availableStatements", availableStatements);

		if (availableStatements != null) {
			if (availableStatements.get(availableStatements.size() - 1).getStartDate() != null)
				model.put("availableStartDate",
						dateFormat.format(availableStatements.get(availableStatements.size() - 1).getStartDate()));
			if (searchType != null && searchType.equals("ByDt") && startDate != null
					&& startDate.before(availableStatements.get(availableStatements.size() - 1).getStartDate())) {
				error.rejectValue(
						"expEndDate",
						"",
						"StartDate must be after "
								+ dateFormat.format(availableStatements.get(availableStatements.size() - 1)
										.getStartDate()));
				return showForm(request, response, error, model);
			}
			if (searchType != null && searchType.equals("ByDt") && endDate != null
					&& endDate.after(Calendar.getInstance())) {
				error.rejectValue("expEndDate", "", "End Date can not be in the future");
				return showForm(request, response, error, model);
			}

		}

		return new ModelAndView(new FileView(), model);

	}

	protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		// SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Map<String, Object> model = new HashMap<>();

		String key = (String) request.getParameter("account");
		key = InternationalCharacterUtils.sanatizeInputs(key, false);
		TVFrontendAccount account = Utils.getAccountById(key, userSession.getAccounts());
		model.put("account", account);

		model.put("isCommercial", (userSession.getCustomerProfile().getUserInterface() == UserInterface.COMMERCIAL));

		AccountMaskUtil mask = new AccountMaskUtil();
		Map<String, String> stmtAccountsMap = new LinkedHashMap<String, String>();
		if (Utils.getRSALanguage(request).equals("sp"))
			stmtAccountsMap.put("none", "Selecciona una cuenta");
		else
			stmtAccountsMap.put("none", "Select an account");
		for (Iterator<TVFrontendAccount> iter = userSession.getAccounts().iterator(); iter.hasNext();) {
			TVFrontendAccount element = (TVFrontendAccount) iter.next();
			if (element.getFeatures().isShowStatement()
					&& !element.getHide()
					&& (element.getSubtype().equals("IDA") || element.getSubtype().equals("PFC") || element
							.getSubtype().equals("CCA"))) {
				String key1 = StringUtils.abbreviate(element.getNickname(), 15)
						+ " "
						+ mask.mask(element.getAccountNumber())
						+ ((element.getAccountNumberSuffix() != null && !element.getAccountNumberSuffix().equals("")) ? " "
								+ element.getAccountNumberSuffix()
								: "");
				stmtAccountsMap.put(String.valueOf(element.getFrontEndId()), key);
				if (element.getPortalBalance() != null)
					key1 = key1 + " " + new NumberTool().format("currency", element.getPortalBalance());

				stmtAccountsMap.put(String.valueOf(element.getFrontEndId()), key1);
			}
		}
		model.put("stmtAccountsMap", stmtAccountsMap);

		/**
		 * Here is the IDA, CCA statements implementation
		 */
		List<AbstractStatement> availableStatements = null;
		if (account == null)
			availableStatements = null;
		else {
			if (account.getStatements() == null) {
				StatementRequest stmtrq = new StatementRequest(Utils.generateCustomerInteractionEvent(request),
						userSession.getCustomerProfile(), userSession.getBankingSession(), account, maxStatements);
				availableStatements = userSession.getCustomerService().getAvailableStatements(stmtrq)
						.getStatementList();
				account.setStatements(availableStatements);
			} else
				availableStatements = account.getStatements();
		}

		if (availableStatements != null && availableStatements.size() > 0
				&& availableStatements.get(availableStatements.size() - 1).getStartDate() != null) {
			dl = availableStatements.get(availableStatements.size() - 1).getStartDate();

			model.put("availableStartDate", dl);

		}

		LinkedHashMap<String, AbstractStatement> accountStatementMap = new LinkedHashMap<String, AbstractStatement>();
		LinkedHashMap<String, String> statementsMap = new LinkedHashMap<String, String>();

		// Se puso una validacion para evitar error de nullpointer
		if (availableStatements != null) {
			statementsMap = Utils.getStatements(availableStatements, accountStatementMap,
					Utils.getRSALanguage(request), account.getSubtype().equals("PRM"));
			model.put("statementsMap", statementsMap);
		}

		// Set up today's date

		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Calendar n = Calendar.getInstance();
		model.put("todayDate", df.format(n.getTime()));
		df = new SimpleDateFormat("MM");
		model.put("month", df.format(n.getTime()));
		df = new SimpleDateFormat("dd");
		model.put("day", df.format(n.getTime()));

		// Dia limite

		df = new SimpleDateFormat("yyyy");
		model.put("year1", df.format(dl));
		df = new SimpleDateFormat("MM");
		model.put("monthl", df.format(dl));
		df = new SimpleDateFormat("dd");
		model.put("dayl", df.format(dl));

		return model;
	}

	public AbstractStatement getStatementById(String id, List<AbstractStatement> statements) {
		for (Iterator<AbstractStatement> iterator = statements.iterator(); iterator.hasNext();) {
			AbstractStatement act = (AbstractStatement) iterator.next();
			if (act.getFrontEndId().equals(id))
				return act;
		}
		return null;
	}

	public int getMaxStatements() {
		return maxStatements;
	}

	public void setMaxStatements(int maxStatements) {
		this.maxStatements = maxStatements;
	}

	public int getMaxTransactions() {
		return maxTransactions;
	}

	public void setMaxTransactions(int maxTransactions) {
		this.maxTransactions = maxTransactions;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		ExportFormBean transactionData = (ExportFormBean) command;

		String cycle = transactionData.getExpcycle();
		String key = transactionData.getAccount();
		String expType = transactionData.getExportTypeFormat();
		String searchType = transactionData.getSearchType();

		Calendar startDate = null;
		Calendar endDate = null;
		if (transactionData.getExpStartDate() != null) {
			startDate = Calendar.getInstance();
			startDate.setTime(transactionData.getExpStartDate());
		}
		if (transactionData.getExpEndDate() != null) {
			endDate = Calendar.getInstance();
			endDate.setTime(transactionData.getExpEndDate());
		}

		if (GenericValidator.isBlankOrNull(key) || "none".equalsIgnoreCase(key)) {
			errors.rejectValue("account", "export.account.invalid", "Please select an account");
		}

		// If there is no selection at all, alert the user
		if ((GenericValidator.isBlankOrNull(cycle)) && transactionData.getExpStartDate() == null
				&& transactionData.getExpEndDate() == null) {
			errors.rejectValue("expcycle", "export.cycle.invalid", "Please select a statement or dates to export");
		} else {
			if ("ByStmt".equalsIgnoreCase(searchType)) {
				if (GenericValidator.isBlankOrNull(cycle))
					errors.rejectValue("expcycle", "export.stmt.invalid", "Please select a statement cycle to export");
			} else if ("ByDt".equalsIgnoreCase(searchType)) // CIBP-1465 bgarcia
			{
				if (startDate == null || endDate == null)
					errors.rejectValue("expEndDate", "export.date.invalid",
							"Please enter a start Date and end Date to export");
			} else if (GenericValidator.isBlankOrNull(searchType) && !cycle.equals("none"))
				transactionData.setSearchType("ByStmt");
			else if (GenericValidator.isBlankOrNull(searchType) && startDate != null || endDate != null)
				transactionData.setSearchType("ByDt");
			else
				errors.rejectValue("searchType", "export.searchtype.invalid",
						"Please select a type of search to export");
		}

		if (GenericValidator.isBlankOrNull(expType))
			errors.rejectValue("exportTypeFormat", "export.formatType.invalid", "Please select a format to export");

	}

	/**
	 * @return the intuitCredentials
	 */
	public String getIntuitCredentials() {
		return intuitCredentials;
	}

	/**
	 * @param intuitCredentials
	 *            the intuitCredentials to set
	 */
	public void setIntuitCredentials(String intuitCredentials) {
		this.intuitCredentials = intuitCredentials;
	}

}
