package evertec.cibp.web.form;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.oob.CustomerOOBChallengeType;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.RSAEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.SendSmsGeneratedCodeRequest;
import evertec.cibp.core.services.requests.adaptiveauth.ChallengeOobRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RsaChallengeRequest;
import evertec.cibp.core.services.requests.adaptiveauth.ValidateOobChallengeRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.adaptiveauth.ChallengeOobResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.form.bean.OOBEmployeeBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import evertec.rsa.RSAConnector.Lang;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class OOBEmployeeForm extends AbstractWizardFormController
		implements GlobalEntitlementAwareController, MessageSourceAware {

	private boolean redirectToBlock = false;
	private boolean redirectToLogin = false;
	private boolean allowAddEmployee = false;
	private String cancelView;
	protected final Log log = LogFactory.getLog(getClass());
	protected CustomerServices customerService;
	private MessageSource messageSource;

	private String telenominaID;
	private String[] productId;

	public OOBEmployeeForm() {
		super();
		setCommandClass(OOBEmployeeBean.class);
		setCommandName("oobemployee");
		setPages(new String[] { "oobemployee" });
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		OOBEmployeeBean bean = (OOBEmployeeBean) command;
		OOBEmployeeBean.ACTION action = bean.getAction();

		bean.setTimeout(false);
		bean.setValidationError(false);

		// Validate SMS & voice call challenges

		if (action == OOBEmployeeBean.ACTION.VALIDATE_SMSCODE || action == OOBEmployeeBean.ACTION.VALIDATE_CALLCODE
				|| action == OOBEmployeeBean.ACTION.VALIDATE_ALTSMSCODE
				|| action == OOBEmployeeBean.ACTION.VALIDATE_ALTCALLCODE) {

			ValidateOobChallengeRequest validateRequest = new ValidateOobChallengeRequest();
			validateRequest.setUsername(userSession.getUsername());
			validateRequest.setDeviceRequest(userSession.getDeviceRequest());
			validateRequest.setRsaSessionId(userSession.getRsaSessionId());
			validateRequest.setRsaTransactionId(userSession.getRsaTransactionId());
			validateRequest.setDeviceTokenCookie(userSession.getDeviceRequest().getDeviceTokenCookie());
			validateRequest.setAnswer(bean.getCode());

			if (action == OOBEmployeeBean.ACTION.VALIDATE_SMSCODE
					|| action == OOBEmployeeBean.ACTION.VALIDATE_ALTSMSCODE) {
				validateRequest.setCredentialType(CredentialType.USER_DEFINED);
				validateRequest.setBindDevice(true);
			} else {
				validateRequest.setCredentialType(CredentialType.OOBPHONE);
				validateRequest.setBindDevice(false);
			}

			ResultStatus statusResult = customerService.rsaValidateOobChallenge(validateRequest);
			userSession.getDeviceRequest().setDeviceTokenCookie(validateRequest.getDeviceTokenCookie());
			if (statusResult != null) {
				if (ResultStatus.RSA_UNABLE.equals(statusResult)) {
					errors.rejectValue("code", "login.oob.codevalidation.error",
							"Unable to validate code at this time.");
					bean.setValidationError(true);
				} else if (ResultStatus.RSA_EXPIRED.equals(statusResult)) {
					errors.rejectValue("code", "login.oob.codeexpired.error", "Code has expired.");
					bean.setTimeout(true);
				} else if (ResultStatus.RSA_BLOCKED.equals(statusResult)) {
					redirectToBlock = true;
					log.error("RSA User " + userSession.getUsername() + " blocked by OOB challenge tries");
				} else if (ResultStatus.RSA_FAILED.equals(statusResult)) {
					if (action == OOBEmployeeBean.ACTION.VALIDATE_SMSCODE
							|| action == OOBEmployeeBean.ACTION.VALIDATE_ALTSMSCODE) {
						errors.rejectValue("code", "sms.code.invalidRSA", "Incorrect code");
					} else
						errors.rejectValue("code", "login.oob.call.error",
								"Input the code on your phone, before clicking continue.");
				}
				if (ResultStatus.RSA_ALLOW.equals(statusResult)) {
					allowAddEmployee = true;
				}
			}

		}

	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object payrolllist,
			BindException error) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String payeeId = (String) WebUtils.getSessionAttribute(request, "payeeId");
		////// OOB
		Cookie cookie = new Cookie("PMData", userSession.getDeviceRequest().getDeviceTokenCookie());
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		ESAPI.httpUtilities().addCookie(response, cookie);


		if (redirectToLogin) {
			redirectToLogin = false;
			return new ModelAndView("redirect:login");

		} else if (redirectToBlock) {
			redirectToBlock = false;
			return new ModelAndView("redirect:login?username=" + userSession.getUsername());
		}

		Map<String, Object> model = new HashMap<>();
		model.put("telenominaID", telenominaID);
		model.put("productId", productId);
		model.put("statusmessage", userSession.getStatusmessage());
		model.put("allowAddEmployee", allowAddEmployee);

		if (allowAddEmployee) {
			userSession.setOobAddEmployeeAut(true);
			userSession.setOobAuthenticated(true);
			request.getSession().removeAttribute("payeeId");
			return new ModelAndView("redirect:addexistingpayee?payee=" + payeeId);
		} else {
			return new ModelAndView("oobcommercialchallenge?payeeId=" + payeeId, model);
		}

	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {
		redirectToBlock = false;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		OOBEmployeeBean bean = (OOBEmployeeBean) command;
		String payeeId = request.getParameter("payeeId") != null ? request.getParameter("payeeId") : "4401";
		WebUtils.setSessionAttribute(request, "payeeId", payeeId);
		Map<String, Object> model = new HashMap<>();
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);
		RsaChallengeRequest rsaChallengeRequest = new RsaChallengeRequest(userSession.getUsername(),deviceRequest);
		CredentialType credentialType = customerService.rsaGetChallengeMethod(rsaChallengeRequest);
		userSession.setDeviceRequest(deviceRequest);
		model.put("payeeId", payeeId);
		model.put("isEvendorActivated", StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR));
		if (credentialType == null) {
			model.put("devicetoken", userSession.getDeviceRequest().getDeviceTokenCookie());
			return model;
		}
		if (credentialType.equals(CredentialType.OOBPHONE))
			model.put("challengeType", CustomerOOBChallengeType.OOBPHONE.toString());
		else
			model.put("challengeType", CustomerOOBChallengeType.OOBSMS.toString());

		model.put("telenominaID", telenominaID);

		OOBEmployeeBean.ACTION action = bean.getAction();

		if (action == null) {
			action = OOBEmployeeBean.ACTION.SEND_SMSCODE;

		}

		// OOB challenge method

		CustomerProfile profile = userSession.getCustomerProfile();
		CustomerEntitlement rsaEnt = profile.getEntitlementByType(EntitlementType.RSA);

		String phone = null;
		String provider = null;

		if (action == OOBEmployeeBean.ACTION.SEND_SMSCODE || action == OOBEmployeeBean.ACTION.VALIDATE_SMSCODE
				|| action == OOBEmployeeBean.ACTION.VALIDATE_CALLCODE) {
			// Retrieve primary phone number (MB Text)
			model.put("from", "phone");
			if (profile.getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
				SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
				if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
						&& !smsprofile.getCtns().isEmpty()) {
					phone = ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn();
					provider = ((SMSCTN) smsprofile.getCtns().iterator().next()).getProvider();
				}
			}
			String altPhone = rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE);
			if (altPhone != null && !altPhone.equalsIgnoreCase("N")) {
				model.put("hasAltPhone", true);
				model.put("altphone", RSAUtils.setPhoneMask(altPhone));
			} else
				model.put("hasAltPhone", false);
		} else if (rsaEnt != null
				&& (action == OOBEmployeeBean.ACTION.ALT_PHONE || action == OOBEmployeeBean.ACTION.VALIDATE_ALTSMSCODE
						|| action == OOBEmployeeBean.ACTION.VALIDATE_ALTCALLCODE)) {
			// Retrieve alternate phone number
			phone = rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE);
			provider = rsaEnt.getString(RSAEntitlement.ALTERNATEPROVIDER);
			model.put("from", "altphone");
		}

		if (phone == null) {

			if (action == OOBEmployeeBean.ACTION.VALIDATE_RECCODE) {
				model.put("from", "reccode");
			} else {
				if (credentialType.equals(CredentialType.OOBPHONE))
					errors.rejectValue("code", "login.oob.call.unable");
				else
					errors.rejectValue("code", "login.oob.codeError.message");

				return model;
			}

		} else {
			model.put("phone", RSAUtils.setPhoneMask(phone));
		}

		// SMS challenge
		if (credentialType.equals(CredentialType.USER_DEFINED) && action != OOBEmployeeBean.ACTION.VALIDATE_SMSCODE
				&& action != OOBEmployeeBean.ACTION.VALIDATE_ALTSMSCODE) {

			boolean news = true;
			String lang = request.getParameter("lang");
			news = (GenericValidator.isBlankOrNull(lang) ? true : false);

			if (news && (action == OOBEmployeeBean.ACTION.SEND_SMSCODE || action == OOBEmployeeBean.ACTION.ALT_PHONE)) {
				if (this.generateCode(request, phone, provider, true, errors))
					model.put("codeSent", true);
				else
					model.put("codeSent", false);

			} else {
				// Don't make another challenge if language is changed
				String code = "";
				if (page == 0)
					code = (String) WebUtils.getSessionAttribute(request, "oobCode");
				if (GenericValidator.isBlankOrNull(code)) {
					this.generateCode(request, phone, provider, (page == 2 ? true : false), errors);
				}
			}

			// Voice call challenge
		} else {
			if (action != OOBEmployeeBean.ACTION.VALIDATE_SMSCODE
					&& action != OOBEmployeeBean.ACTION.VALIDATE_ALTSMSCODE
					&& action != OOBEmployeeBean.ACTION.VALIDATE_CALLCODE
					&& action != OOBEmployeeBean.ACTION.VALIDATE_ALTCALLCODE) {
				if (action == OOBEmployeeBean.ACTION.CALL_PHONE) {
					if (this.makeCall(request, phone, errors))
						model.put("callMade", true);
					else
						model.put("callMade", false);
				} else {
					this.makeCall(request, phone, errors);
				}
				bean.setCode((String) WebUtils.getSessionAttribute(request, "oobCode"));
			}
		}

		// Send device token for PMData cookie
		model.put("devicetoken", userSession.getDeviceRequest().getDeviceTokenCookie());
		model.put("redirectToBlock", redirectToBlock);
		model.put("username", userSession.getUsername());

		return model;

	}

	private boolean generateCode(HttpServletRequest request, String phone, String provider, boolean newChallenge,
			Errors errors) {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		String user = userSession.getUsername();

		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);

		ChallengeOobRequest challengeOobRequest = new ChallengeOobRequest();
		challengeOobRequest.setCredentialType(CredentialType.USER_DEFINED);
		challengeOobRequest.setChallengephone(phone);
		challengeOobRequest.setDeviceRequest(deviceRequest);
		challengeOobRequest.setUsername(user);
		challengeOobRequest.setRsaSessionId(userSession.getRsaSessionId());
		challengeOobRequest.setRsaTransactionId(userSession.getRsaTransactionId());
		challengeOobRequest.setLanguage("EN".equalsIgnoreCase(Utils.getRSALanguage(request)) ? Lang.en : Lang.es);
		if (userSession.isLoginCompleteSuccess())
			challengeOobRequest.setNewChallenge(true);
		else
			challengeOobRequest.setNewChallenge(newChallenge);

		try {
			ChallengeOobResponse challengeOobResponse = customerService.getSignOnCustomerServiceModule().challengeOob(challengeOobRequest);

			deviceRequest.setDeviceTokenCookie(challengeOobResponse.getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(challengeOobResponse.getDeviceTokenCookie());
			userSession.setDeviceRequest(deviceRequest);
			userSession.setRsaSessionId(challengeOobResponse.getRsaSessionId());
			userSession.setRsaTransactionId(challengeOobResponse.getRsaTransactionId());

			if (ResultStatus.RSA_UNABLE.equals(challengeOobResponse.getResultStatus())) {
				errors.rejectValue("code", "sms.code.error", "Unable to send code at this time.");
				WebUtils.setSessionAttribute(request, "oobCode", null);
				return false;

			} else if (ResultStatus.RSA_BLOCKED.equals(challengeOobResponse.getResultStatus())) {
				redirectToBlock = true;
				WebUtils.setSessionAttribute(request, "oobCode", null);
				log.error("RSA User " + userSession.getUsername() + " blocked by OOB SMS challenge");
				return false;
			}

			String code = challengeOobResponse.getOtp();

			SendSmsGeneratedCodeRequest codereq = new SendSmsGeneratedCodeRequest(
					Utils.generateCustomerInteractionEvent(request), customer, code, phone, provider);

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				customerService.sendGeneratedCode(codereq, PreferredLanguage.ENGLISH);
			else
				customerService.sendGeneratedCode(codereq, PreferredLanguage.SPANISH);

			WebUtils.setSessionAttribute(request, "oobCode", code);

			return true;

		} catch (Exception e) {
			errors.rejectValue("code", "sms.code.error", "Unable to send code at this time.");
			WebUtils.setSessionAttribute(request, "oobCode", null);
			return false;
		}
	}

	private boolean makeCall(HttpServletRequest request, String phone, Errors errors) {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String user = userSession.getUsername();

		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);

		ChallengeOobRequest challengeOobRequest = new ChallengeOobRequest();
		challengeOobRequest.setCredentialType(CredentialType.OOBPHONE);
		challengeOobRequest.setChallengephone(phone);
		challengeOobRequest.setDeviceRequest(deviceRequest);
		challengeOobRequest.setUsername(user);
		challengeOobRequest.setRsaSessionId(userSession.getRsaSessionId());
		challengeOobRequest.setRsaTransactionId(userSession.getRsaTransactionId());
		challengeOobRequest.setLanguage("EN".equalsIgnoreCase(Utils.getRSALanguage(request)) ? Lang.en : Lang.es);
		challengeOobRequest.setNewChallenge(true);
		
		try {
			ChallengeOobResponse challengeOobResponse = customerService.getSignOnCustomerServiceModule().challengeOob(challengeOobRequest);

			deviceRequest.setDeviceTokenCookie(challengeOobResponse.getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(challengeOobResponse.getDeviceTokenCookie());
			userSession.setDeviceRequest(deviceRequest);
			userSession.setRsaSessionId(challengeOobResponse.getRsaSessionId());
			userSession.setRsaTransactionId(challengeOobResponse.getRsaTransactionId());

			if (ResultStatus.RSA_UNABLE.equals(challengeOobResponse.getResultStatus())) {
				redirectToLogin = true;
				errors.rejectValue("code", "login.oob.call.unable", "Unable to generate call at this time.");
				WebUtils.setSessionAttribute(request, "oobCode", null);
				return false;
			} else if (ResultStatus.RSA_BLOCKED.equals(challengeOobResponse.getResultStatus())) {
				redirectToBlock = true;
				WebUtils.setSessionAttribute(request, "oobCode", null);
				log.error("RSA User " + userSession.getUsername() + " blocked by OOB call challenge");
				return false;
			}

			WebUtils.setSessionAttribute(request, "oobCode", challengeOobResponse.getToken());
			return true;

		} catch (Exception e) {
			errors.rejectValue("code", "login.oob.call.unable", "Unable to generate call at this time.");
			WebUtils.setSessionAttribute(request, "oobCode", null);
			return false;
		}
	}

	public String getCancelView() {
		return cancelView;
	}

	public void setCancelView(String cancelView) {
		this.cancelView = cancelView;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.BILLPAY;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String[] getProductId() {
		return productId;
	}

	public String getTelenominaID() {
		return telenominaID;
	}

	public void setTelenominaID(String telenominaID) {
		this.telenominaID = telenominaID;
	}

	public void setProductId(String[] productId) {
		this.productId = productId;
	}

}
