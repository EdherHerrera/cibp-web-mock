package evertec.cibp.web.form;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.EventType;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerActions;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.oob.CustomerOOBChallengeType;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.DeleteUserRSARequest;
import evertec.cibp.core.services.requests.ModifyRSAEntitlementRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.ValidationQuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.RsaOobResponse;
import evertec.cibp.core.utils.ApplicationsDefinition;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.form.bean.EnrollOOBBean;
import evertec.cibp.web.form.bean.EnrollOOBBean.ACTION;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class EnrollOOBForm extends AbstractWizardFormController {
	/** Logger */
	protected final Log log = LogFactory.getLog(getClass());
	static final String ENROLLOOB = "enrollOOB";
	static final String USERSESSION = "userSession";

	public EnrollOOBForm() {
		super();
		setCommandClass(EnrollOOBBean.class);
		setCommandName(ENROLLOOB);
		setPages(new String[] { ENROLLOOB, ENROLLOOB, ENROLLOOB, ENROLLOOB, ENROLLOOB, ENROLLOOB, ENROLLOOB });
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		// init smsUpdateSuccess var
		String smsUpdateSuccess = "smsUpdateSuccess";
		if (WebUtils.getSessionAttribute(request, smsUpdateSuccess) == null) {
			WebUtils.setSessionAttribute(request, smsUpdateSuccess, false);
		}

		Map<String, Object> model = new HashMap<>();
		EnrollOOBBean infobean = (EnrollOOBBean) command;
		EnrollOOBBean.ACTION action = infobean.getAction();
		// Is enrolled in Mobile_Banking
		if (userSession.getCustomerProfile() != null) {
			if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
				SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
				if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
						&& !smsprofile.getCtns().isEmpty()) {
					// verify the action and smsUpdateSuccess var
					if ((action != EnrollOOBBean.ACTION.CHOOSE_PREFERENCES
							|| (boolean) WebUtils.getSessionAttribute(request, smsUpdateSuccess)) && page == 2) {
						page = 3;
						infobean.setAction(null);
					}
					String ctn = ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn();
					model.put("smsPhone", RSAUtils.setPhoneMask(ctn));
					model.put("smsPhoneNoMask",
							ctn.substring(0, 3) + "-" + ctn.substring(3, 6) + "-" + ctn.substring(6));
					WebUtils.setSessionAttribute(request, smsUpdateSuccess, false);
				} else {
					WebUtils.setSessionAttribute(request, smsUpdateSuccess, false);
				}
			} else {
				WebUtils.setSessionAttribute(request, smsUpdateSuccess, false);
			}
		}

		model.put("page", page);

		if (GenericValidator.isBlankOrNull(infobean.getCallType())) {
			infobean.setCallType(CustomerOOBChallengeType.OOBSMS.toString());
		}
		if (GenericValidator.isBlankOrNull(infobean.getFrecuency())) {
			infobean.setFrecuency("FALSE");
		}

		// Generate Recovery Code
		if (GenericValidator.isBlankOrNull(infobean.getRecoveryCode())) {
			String code = RSAUtils.generateOOBCode();
			infobean.setRecoveryCode(code);
		}
		model.put("oneTimeCode", infobean.getRecoveryCode());

		switch (page) {
		case 0:
			break;
		case 1:
			userSession.setInformationChallenge(false);

			QuestionResponse personalQuestion;

			if (userSession.getInformationQuestion() == null) {

				DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
						userSession.getDeviceRequest().getDeviceTokenCookie(), request);

				RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, Utils.getRSALanguage(request), userSession.getUsername(), EventType.CLIENT_DEFINED.getValue());
				AnalyzeResponse anResp = userSession.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

				deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
				deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
				userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
				userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

				userSession.setDeviceRequest(deviceRequest);

				String email = null;
				if (userSession.getCustomerProfile() != null)
					email = userSession.getCustomerProfile().getEmail();

				RSAActionRequest actionRequest = new RSAActionRequest(
						Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
						userSession.getUsername(), "", email, 0, userSession.isRemember(),
						userSession.getDeviceRequest());

				actionRequest.setDeviceRequest(userSession.getDeviceRequest());
				actionRequest.setRsaSessionId(userSession.getRsaSessionId());
				actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());
				actionRequest.setRsaLang(Utils.getRSALanguage(request));

				personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

				if (personalQuestion != null && personalQuestion.getDeviceRequest() != null)
					userSession.getDeviceRequest()
							.setDeviceTokenCookie(personalQuestion.getDeviceRequest().getDeviceTokenCookie());

			} else {
				personalQuestion = new QuestionResponse();
				personalQuestion.setStatusResult(ResultStatus.SUCCESS);
				personalQuestion.setQuestion(userSession.getInformationQuestion());
			}

			if (personalQuestion != null && ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {

				userSession.setInformationQuestion(personalQuestion.getQuestion());
				String question = (String) userSession.getInformationQuestion().values().toArray()[0];
				model.put("question", question);

			} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())
					|| ResultStatus.EXCEPTION.equals(personalQuestion.getStatusResult())) {
				throw new ModelAndViewDefiningException(new ModelAndView("error"));
			} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
				throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
			}
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			/** **** Reference Data for SMS Alternaty Phone ****** */
			Map<String, Object> smsproviders = new LinkedHashMap<String, Object>();
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				smsproviders.put("none", "Select a provider");
			else
				smsproviders.put("none", "Selecciona un proveedor");
			smsproviders.put("PR1", "OPEN");
			smsproviders.put("PR7", "CLARO");
			smsproviders.put("PR6", "CENTENNIAL");
			smsproviders.put("US3", "AT&T");
			smsproviders.put("US4", "T-MOBILE");
			smsproviders.put("US5", "SPRINT");
			model.put("smsproviders", smsproviders);
			if (infobean.getAction() == null)
				model.put("agreeval", "");
			else
				model.put("agreeval", "checked");

			String step1 = "step1";
			String step2 = "step2";
			if (infobean.getAction() != null) {
				if (infobean.getAction().equals(ACTION.CHOOSE_PREFERENCES)) {
					model.put(step1, true);
					model.put(step2, false);
				} else {
					model.put(step1, true);
					model.put(step2, false);
				}
			} else {
				model.put(step1, true);
				model.put(step2, false);
			}

			break;
		case 5:

			break;
		case 6:
			break;
		default:
			break;

		}

		model.put("devicetoken", userSession.getDeviceRequest().getDeviceTokenCookie());

		return model;
	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		// remove var to the session
		request.getSession().removeAttribute("smsUpdateSuccess");
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);
		Cookie cookie = new Cookie("PMData", deviceRequest.getDeviceTokenCookie());
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		ESAPI.httpUtilities().addCookie(response, cookie);
		// Redirect to changeOOB
		return showForm(request, errors, "changeOOB");
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		EnrollOOBBean dude = (EnrollOOBBean) command;
		CustomerServices customerservice = userSession.getCustomerService();
		CustomerProfile customerProfile = userSession.getCustomerProfile();

		if (page == 0) {
			// Check if Mi Banco Text enrollment is needed
			if (customerProfile != null) {
				if (customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
					SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
					if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
							&& !smsprofile.getCtns().isEmpty()) {
						dude.setMiBancoTextEnroll(false);
					} else {
						dude.setMiBancoTextEnroll(true);
					}
				} else {
					dude.setMiBancoTextEnroll(true);
				}
			}
		}

		else if (page == 1) {
			if (!userSession.isInformationChallenge()) {

				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);

				CustomerProfile profile = userSession.getCustomerProfile();

				boolean dummyQuestions = (profile == null);

				String answer = dude.getAnswer();

				if (GenericValidator.isBlankOrNull(answer)) {
					errors.rejectValue("answer", "enrollOOB.register.required", "Required");
				} else {

					RSAActionRequest rsaActionRequest = new RSAActionRequest(
							Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
							userSession.getUsername(), answer, dummyQuestions ? null : profile.getEmail(), 0,
							userSession.isRemember(), userSession.getDeviceRequest());

					rsaActionRequest.setRsaSessionId(userSession.getRsaSessionId());
					rsaActionRequest.setRsaTransactionId(userSession.getRsaTransactionId());
					rsaActionRequest.setQuestion(userSession.getInformationQuestion());
					rsaActionRequest.setRsaLang(Utils.getRSALanguage(request));
					ValidationQuestionResponse responseAnswer = userSession.getCustomerService().getSignOnCustomerServiceModule().validateRSAQuestion(rsaActionRequest);
					userSession.setQuestion(null);
					userSession.setRsaSessionId(responseAnswer.getRsaSessionId());
					userSession.setRsaTransactionId(responseAnswer.getRsaTransactionId());

					if (ResultStatus.SUCCESS.equals(responseAnswer.getStatusResult())) {
						userSession.setInformationChallenge(true);
						userSession.setInformationQuestion(null);
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
					} else if (ResultStatus.BLOCKED.equals(responseAnswer.getStatusResult())) {
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
						boolean hasalert = false;
						AlertsEntitlement ent = new AlertsEntitlement(
								userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS));
						if (ent != null && ent.getAlerts() != null) {
							Set<AlertType> alerts = ent.getAlerts();
							if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
									|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
								hasalert = true;
							}
						}
						if (hasalert) {
							userSession.getCustomerService().sendQuestionsBlockedConfirmation(
									userSession.getCustomerProfile(),
									userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
									userSession.getPreferredLanguage());
						}
						errors.rejectValue("", "", "");
						throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
					} else if (ResultStatus.EXCEPTION.equals(responseAnswer.getStatusResult())) {
						errors.rejectValue("", "", "");
					} else if (ResultStatus.ERROR.equals(responseAnswer.getStatusResult())) {
						errors.rejectValue("answer", "login.question.error.message", "Invalid answer");
						String email = null;
						if (userSession.getCustomerProfile() != null)
							email = userSession.getCustomerProfile().getEmail();
						QuestionResponse personalQuestion;
						RSAActionRequest actionRequest = new RSAActionRequest(
								Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
								userSession.getUsername(), "", email, 0, userSession.isRemember(),
								userSession.getDeviceRequest());

						actionRequest.setDeviceRequest(userSession.getDeviceRequest());
						actionRequest.setRsaSessionId(userSession.getRsaSessionId());
						actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());

						actionRequest.setRsaLang(Utils.getRSALanguage(request));

						personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

						if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
							userSession.setInformationQuestion(personalQuestion.getQuestion());
							errors.rejectValue("", "", "");
						} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
							errors.rejectValue("", "", "");
						} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
							WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
							WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
							WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS,
									null);

							boolean hasalert = false;
							AlertsEntitlement ent = new AlertsEntitlement(
									userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS));
							if (ent != null && ent.getAlerts() != null) {
								Set<AlertType> alerts = ent.getAlerts();
								if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
										|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
									hasalert = true;
								}
							}
							if (hasalert) {
								userSession.getCustomerService()
										.sendQuestionsBlockedConfirmation(userSession.getCustomerProfile(),
												userSession
														.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
												userSession.getPreferredLanguage());
							}
							throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
						}
					}
				}
			}
		}

		else if (page == 2) {
			// validar que este inscrito en sms
			// Is enrolled in Mobile_Banking
			if (userSession.getCustomerProfile() != null) {
				if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
					SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
					if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
							&& !smsprofile.getCtns().isEmpty()) {

					} else
						errors.rejectValue("smsError", "enrollOOB.register.sms.error",
								"No inscrito en mi banca movil.");
				} else
					errors.rejectValue("smsError", "enrollOOB.register.sms.error", "No inscrito en mi banca movil.");
			}
		}

		else if (page == 4) {
			if (dude.getAction() == ACTION.CHOOSE_PREFERENCES) {
				validatePhone(errors, dude);
				String phone = dude.getPhone1() + dude.getPhone2() + dude.getPhone3();
				dude.setAltPhoneRegistered(true);
				if (errors.getAllErrors().isEmpty()) {
					WebUtils.setSessionAttribute(request, "altPhonereceived", phone);
					WebUtils.setSessionAttribute(request, "altProvider", dude.getProvider());
					dude.setAlternatyEnroll(true);
				}
			}

			if (errors.getAllErrors().isEmpty() && ((dude.getAlternatyEnroll() != null && dude.getAlternatyEnroll())
					|| dude.getAction() == ACTION.ALT_SKIP)) {
				try {
					
					/**
					* MBNA-1961
					* Method addCustomerOOBEnroll : CIBP-WEB microservice integration
					* 
					*/
					customerservice.addCustomerOOBEnroll(customerProfile);
					dude.getSmscode();

					String challengeType = dude.getCallType();
					String alternatePhone = "N";
					String alternateProvider = "N";
					if (dude.getAlternatyEnroll() != null && dude.getAlternatyEnroll()) {
						alternatePhone = (String) WebUtils.getSessionAttribute(request, "altPhonereceived");
						alternateProvider = (String) WebUtils.getSessionAttribute(request, "altProvider");
					}

					DeleteUserRSARequest deleteUserRSARequest = new DeleteUserRSARequest(
							userSession.getCustomerProfile().getUsername(), ApplicationsDefinition.CIBP);

					// Try to delete RSA user
					boolean rsaUserDeleted = false;
					RsaOobResponse enrollResp = new RsaOobResponse();
					CustomerServices customerService = userSession.getCustomerService();
					rsaUserDeleted = customerService.deleteUserRSA(deleteUserRSARequest);

					// Enroll user in RSA w/ OOB
					if (rsaUserDeleted) {
						String email = null;
						if (userSession.getCustomerProfile() != null)
							email = userSession.getCustomerProfile().getEmail();

						RSAActionRequest actionRequest = new RSAActionRequest(
								Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
								userSession.getUsername(), "", email, 0, userSession.isRemember(),
								userSession.getDeviceRequest());

						DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
								userSession.getDeviceRequest().getDeviceTokenCookie(), request);
						enrollResp = userSession.getCustomerService().getSignOnCustomerServiceModule()
								.enrollRSAOOB(actionRequest, challengeType);

					}

					// Update RSA entitlement
					if (enrollResp.getResultStatus() != null
							&& enrollResp.getResultStatus().equals(ResultStatus.SUCCESS)) {

						Integer codeSalt = PasswordHashUtil.genereateRandomSalt();
						String codeHash = (PasswordHashUtil.generateSaltedHash(dude.getRecoveryCode(), codeSalt));

						Boolean alwaysChallenge = (dude.getFrecuency().equalsIgnoreCase("FALSE")) ? false : true;
						CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request);
						ModifyRSAEntitlementRequest modifyRequest = new ModifyRSAEntitlementRequest(interactionEvent,
								customerProfile, alternatePhone, alternateProvider, alwaysChallenge, true, codeHash,
								codeSalt.toString());
						customerservice.modifyCustomerRSAEntitlement(modifyRequest);

						customerservice.cleanOOBRecoveryCodeBlock(customerProfile);

						userSession.getDeviceRequest().setDeviceTokenCookie(enrollResp.getDeviceTokenCookie());

						customerService.sendOobEnrollEmail(customerProfile, userSession.getPreferredLanguage());

						userSession.setOobEnroll(true);

						// Increment OOB counts for Monthly Report
						if (challengeType.equalsIgnoreCase(CustomerOOBChallengeType.OOBPHONE.toString()))
							customerService.logCustomerUsageData(customerProfile, CustomerActions.OOB_VOICE_ENROLL);
						else
							customerService.logCustomerUsageData(customerProfile, CustomerActions.OOB_SMS_ENROLL);

						if (alwaysChallenge)
							customerService.logCustomerUsageData(customerProfile, CustomerActions.OOB_ALWAYS_FREQ);
						else
							customerService.logCustomerUsageData(customerProfile, CustomerActions.OOB_ASREQ_FREQ);

						if (!alternatePhone.equals("N"))
							customerService.logCustomerUsageData(customerProfile, CustomerActions.OOB_ALT_PHONE);

						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							userSession.setStatusmessage(
									"Your enrollment in 2 Step Verification has been completed successfully.");
						} else {
							userSession.setStatusmessage(
									"Tu inscripci&oacute;n de Verificaci&oacute;n de 2 Pasos ha sido completada exitosamente.");
						}
					} else {
						userSession.setOobEnroll(false);
						errors.rejectValue("", "", "Error en la inscripcion");
					}
				} catch (Exception e) {
					log.error(e.getMessage());
					userSession.setOobEnroll(false);
					errors.rejectValue("", "", "Error en la inscripcion");
				}
			}

		}

	}// End OnBind

	private void validatePhone(BindException errors, EnrollOOBBean dude) {
		if (dude.getProvider() == null || dude.getProvider().equals("none")) {
			errors.rejectValue("provider", "sms.provider.invalid", "Please select a provider");
		}
		if ((dude.getPhone1() == null || dude.getPhone1().equals("") || !dude.getPhone1().matches("\\d{3}"))
				|| (dude.getPhone2() == null || dude.getPhone2().equals("") || !dude.getPhone2().matches("\\d{3}"))
				|| (dude.getPhone3() == null || dude.getPhone3().equals("") || !dude.getPhone3().matches("\\d{4}"))) {
			errors.rejectValue("phone3", "sms.phone.invalid", "Please enter a correct phone number");
		}
	}

}