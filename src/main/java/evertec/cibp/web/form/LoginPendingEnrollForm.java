package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import edu.emory.mathcs.backport.java.util.Arrays;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.enrollment.EnrollableAccount;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.CardCVVAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.CardPINAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.CustomerDOBValidationRequest;
import evertec.cibp.core.services.requests.DepositAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.ModifyCustomerBirthDateRequest;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.requests.NonTransAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.PayrollCardAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.PersonalInformationValidationRequest;
import evertec.cibp.core.services.responses.PersonalDOBValidationResponse;
import evertec.cibp.core.services.responses.PersonalDOBValidationResponse.ValidationStatus;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse.EnrollmentValidationStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse.AccountValidationStatus;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.encryptedPin.PinEncryptionTool;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.form.bean.ValidateInfoBean;
import evertec.cibp.web.form.bean.ValidateInfoBean.ACTION;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.utils.ValidationUtil;

public class LoginPendingEnrollForm extends AbstractWizardFormController implements GlobalEntitlementAwareController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private PinEncryptionTool pinTool;

	private Integer ageRestriction;
	private String paymentvalidationrange;
	private String nonTransProductFlow;
	


	public LoginPendingEnrollForm() {
		super();
		setCommandClass(ValidateInfoBean.class);
		setCommandName("loginPendEnroll");
		setPages(new String[] { "loginPendEnroll", "loginPendEnroll" });
	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		ValidateInfoBean info = (ValidateInfoBean) command;
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();
		CustomerServices customerService = session.getCustomerService();

		List<ASK_FOR> tasksList = session.getTasksList();
		List<ACTION> removetasks = info.getActionsProcessed();
		if (removetasks.contains(ACTION.DOB)) {
			tasksList.remove(ASK_FOR.VALIDATE_DOB);
			customerService.resetCustomerFlag(new ModifyCustomerRequest(
					Utils.generateCustomerInteractionEvent(request), profile.getProfileId(), "", 0, "", 0, "", null,
					CustomerFlagType.VALIDATE_DOB, session.getDeviceRequest()));
		}
		if (removetasks.contains(ACTION.ENROLLACT)) {
			tasksList.remove(ASK_FOR.VALIDATE_ENROLLACCOUNT);
			customerService.resetCustomerFlag(new ModifyCustomerRequest(
					Utils.generateCustomerInteractionEvent(request), profile.getProfileId(), "", 0, "", 0, "", null,
					CustomerFlagType.MUST_VALIDATE_ENROLLACCOUNT, session.getDeviceRequest()));
		}

		if (!tasksList.contains(ASK_FOR.VALIDATE_DOB) && !tasksList.contains(ASK_FOR.VALIDATE_ENROLLACCOUNT)) {
			customerService.createInitialCustomerEntitlements(profile);
		}

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
		cookie.setPath("/");

		ESAPI.httpUtilities().addCookie(response, cookie);
		return new ModelAndView(SignonSequenceUtils.getNextView(session));
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		ValidateInfoBean info = (ValidateInfoBean) command;
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();
		CustomerServices customerService = session.getCustomerService();

		ValidateInfoBean.ACTION action = info.getAction();

		if (action == ValidateInfoBean.ACTION.DOB) {
			Integer year = info.getDobyear();
			Integer month = info.getDobmonth();
			Integer day = info.getDobday();

			boolean error = false;
			if (year == null || !GenericValidator.matchRegexp(String.valueOf(year), "\\d{4}")) {
				errors.rejectValue("dobyear", "enrollment.dobyear.invalid", "Please enter a valid year");
				error = true;
			}

			if (day == null || !GenericValidator.matchRegexp(String.valueOf(day), "\\d{1,2}")) {
				errors.rejectValue("dobday", "enrollment.dobday.invalid", "Please enter a valid day");
				error = true;
			}

			if (month == null || !GenericValidator.matchRegexp(String.valueOf(month), "\\d{1,2}")) {
				errors.rejectValue("dobmonth", "enrollment.dobmonth.invalid", "Please enter a valid month");
				error = true;
			}

			GregorianCalendar dob = null;
			if (!error) {
				// Is the client 13 or older?
				// CIBP-2881
				dob = new GregorianCalendar(year, month, day);
				GregorianCalendar now = new GregorianCalendar();
				int age = now.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
				if (dob.get(Calendar.MONTH) > now.get(Calendar.MONTH)
						|| (dob.get(Calendar.MONTH) == now.get(Calendar.MONTH) && dob.get(Calendar.DAY_OF_MONTH) > now
						.get(Calendar.DAY_OF_MONTH)))
					age--;

				if (age < this.ageRestriction) {
					errors.rejectValue("birthday", "enrollment.dob.underage",
							"Dear customer, at this moment we cannot grant access to the Mi Banco Online "
									+ "platform since our online services are not directed to minors under the age of thirteen (13) "
									+ "as established in the terms and conditions of this service. We apologize for any inconvenience.");
					error = true;
				}
			}

			if (!error) {
				GregorianCalendar birth = new GregorianCalendar(year, month, day);
				CustomerInteractionEvent generateCustomerInteractionEvent = Utils
						.generateCustomerInteractionEvent(request);
				CustomerDOBValidationRequest req = new CustomerDOBValidationRequest(generateCustomerInteractionEvent,
						profile.getPermId(), birth);
				PersonalDOBValidationResponse res = customerService.validateCustomerDOB(req);
				if (res.getStatus() == ValidationStatus.VALID) {
					ModifyCustomerBirthDateRequest birthmod = new ModifyCustomerBirthDateRequest(
							generateCustomerInteractionEvent, profile, birth.getTime());
					customerService.modifyCustomerBirthDate(birthmod);
					info.actionsProcessed.add(ACTION.DOB);
				} else if (res.getStatus() == ValidationStatus.DOB_BLOCKED) {
					info.setDobCounter(3);
					errors.rejectValue("dobyear", "enrollment.dob.invalid", "Please verify input");
					return;
				} else {
					info.setDobCounter(info.getDobCounter() + 1);
					errors.rejectValue("dobyear", "enrollment.dob.invalid", "Please verify input");
				}
			}
		} else if (action == ValidateInfoBean.ACTION.ENROLLACT) {

			String accountnumber = info.getAccountnumber();
			String athnumber = info.getAthnumber();
			String pin = info.getPin();
			String cvv = info.getCvv();
			String taxId = profile.getTaxId();
			String expMonth = info.getExpMonth();
			String expYear = info.getExpYear();
			String zipCode = info.getZipCode();
			ValidateAccountStatusResponse validateAccountInfo = null;
			String accountType = info.getAccountType();
			Integer originationYear = info.getOriginationYear();
			BigDecimal paymentAmount = info.getPaymentAmount();
			info.setAccountType(accountType);
			boolean error = false;


			if (accountType.equalsIgnoreCase("IDA")) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{9}")) {
					errors.rejectValue("accountnumber", "enrollment.accountnumber.invalid", "Invalid account Number");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(athnumber) || !GenericValidator.matchRegexp(athnumber, "\\d{16}")) {
					errors.rejectValue("athnumber", "enrollment.athnumber.invalid", "Invalid ATH Number");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(pin) || !GenericValidator.matchRegexp(pin, "\\d{4}")) {
					errors.rejectValue("pin", "enrollment.pin.invalid", "Invalid PIN");
					error = true;
				}

				
				if (!error && ValidationUtil.hasAlreadyEnrolledProfile(request, profile, accountnumber)) 
				{

					error = true;
					errors.reject("enrollment.error.alreadyenrolled", "Already enrolled, please login");

				}


				if (!error) {
					String encryptedPin = pinTool.encrypt(pin);
					DepositAccountInformationValidationRequest depReq = new DepositAccountInformationValidationRequest(
							null, accountnumber, athnumber, taxId, encryptedPin, true, true);
					validateAccountInfo = customerService.validateDepositAccountInfo(depReq);
				}

			} else if (accountType.equalsIgnoreCase("CCA")) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{16}")) {
					errors.rejectValue("accountnumber", "enrollment.accountnumber.invalid", "Invalid account Number");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(cvv) || !GenericValidator.matchRegexp(cvv, "\\d{3}")) {
					errors.rejectValue("cvv", "enrollment.cvv.invalid", "Invalid CVV code");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(zipCode) || !GenericValidator.matchRegexp(zipCode, "\\d{5}")) {
					errors.rejectValue("zipCode", "enrollment.zipCode.invalid", "Invalid zip code");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(expMonth) || expMonth.equals("0")) {
					errors.rejectValue("expMonth", "enrollment.expMonth.invalid", "Invalid exp month");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(expYear) || !GenericValidator.matchRegexp(expYear, "\\d{4}")) {
					errors.rejectValue("expYear", "enrollment.expYear.invalid", "Invalid exp year");
					error = true;
				}
				
				if (!error && ValidationUtil.hasAlreadyEnrolledProfile(request, profile, accountnumber)) 
				{

					error = true;
					errors.reject("enrollment.error.alreadyenrolled", "Already enrolled, please login");

				}

				if (!error) {
					CardCVVAccountInformationValidationRequest cvvReq = new CardCVVAccountInformationValidationRequest(
							null, accountnumber, taxId, cvv, expMonth, expYear, zipCode, true);
					validateAccountInfo = customerService.validateCrediCardCVVAccountInfo(cvvReq);
				}

			} else if (accountType.equalsIgnoreCase("AMEX")) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{15}")) {
					errors.rejectValue("accountnumber", "", "Invalid account Number");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(pin) || !GenericValidator.matchRegexp(pin, "\\d{4}")) {
					errors.rejectValue("pin", "", "Invalid PIN code");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(zipCode) || !GenericValidator.matchRegexp(zipCode, "\\d{5}")) {
					errors.rejectValue("zipCode", "", "Invalid zip code");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(expMonth) || expMonth.equals("0")) {
					errors.rejectValue("expMonth", "enrollment.expMonth.invalid", "Invalid exp month");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(expYear) || !GenericValidator.matchRegexp(expYear, "\\d{4}")) {
					errors.rejectValue("expYear", "", "Invalid exp year");
					error = true;
				}

				
				if (!error && ValidationUtil.hasAlreadyEnrolledProfile(request, profile, accountnumber)) 
				{

					error = true;
					errors.reject("enrollment.error.alreadyenrolled", "Already enrolled, please login");

				}

				if (!error) {
					String encryptedPin = pinTool.encrypt(pin);
					CardPINAccountInformationValidationRequest cardPinReq = new CardPINAccountInformationValidationRequest(
							null, accountnumber, taxId, encryptedPin, expMonth, expYear, zipCode, true, true, true);
					validateAccountInfo = customerService.validateCrediCardPINAccountInfo(cardPinReq);
				}

			} else if (accountType.equalsIgnoreCase("PFC")) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{16}")) {
					errors.rejectValue("accountnumber", "", "Invalid account Number");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(pin) || !GenericValidator.matchRegexp(pin, "\\d{4}")) {
					errors.rejectValue("pin", "", "Invalid PIN code");
					error = true;
				}

				if (!error && ValidationUtil.hasAlreadyEnrolledProfile(request, profile, accountnumber)) 
				{

					error = true;
					errors.reject("enrollment.error.alreadyenrolled", "Already enrolled, please login");

				}

				if (!error) {
					String encryptedPin = pinTool.encrypt(pin);
					PayrollCardAccountInformationValidationRequest payrollReq = new PayrollCardAccountInformationValidationRequest(
							null, accountnumber, taxId, encryptedPin, true);
					validateAccountInfo = customerService.validatePayrollCardAccountInfo(payrollReq);
				}
			}
			else if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA88) && accountType.equalsIgnoreCase("nonTranAcc"))
			{
				String digitLength = "\\d+";			
				if (GenericValidator.isBlankOrNull(accountnumber)|| !GenericValidator.matchRegexp(accountnumber, digitLength)){
					errors.rejectValue("accountnumber", "loginPendEnroll.account.nontransactional.accountNumber.error", "Invalid account Number");
					error = true;
				}	
				

				if (!ValidationUtil.validOriginationYear(originationYear) && !error)
				{

					errors.rejectValue("accountnumber", "loginPendEnroll.account.nontransactional.accountNumber.error", "Invalid account Number");
					error = true;

				}
				if(!ValidationUtil.validPaymentAmount(paymentAmount) && !error)
				{

					errors.rejectValue("accountnumber", "loginPendEnroll.account.nontransactional.accountNumber.error", "Invalid account Number");
					error = true;

				}

				if (!error && ValidationUtil.hasAlreadyEnrolledProfile(request, profile, accountnumber)) 
				{

					error = true;
					errors.reject("enrollment.error.alreadyenrolled", "Already enrolled, please login");

				}			


				String NonTransAccType = null;
				List<EnrollableAccount> accounts = session.getEnrollableAccounts();				
					if (accounts.isEmpty()) {
						NonTransAccType = "MLA";
					}else {
						for (EnrollableAccount enrollableAccount : accounts) {							
							if(enrollableAccount.getAccountId().equals(accountnumber)) {
								NonTransAccType = enrollableAccount.getAccountType();
								accountType = enrollableAccount.getAccountType();
							}
						}
					}
				
				if(NonTransAccType == null) {
					//Hack: Set default value to force trip to vault and force counter
					NonTransAccType = "MLA";
				}

				if(!error)
				{
					NonTransAccountInformationValidationRequest ntReq = new NonTransAccountInformationValidationRequest(
							null, accountnumber,taxId,true, originationYear, paymentAmount,paymentvalidationrange, true, NonTransAccType);
					validateAccountInfo = customerService.validateNonTransAccountInfo(ntReq);
				}
			}

			/*
			 * if (accountType.equalsIgnoreCase("IDA")) {
			 * 
			 * String encryptedPin = pinUtil.getEncryptedPin(pin);
			 * DepositAccountInformationValidationRequest depReq = new
			 * DepositAccountInformationValidationRequest
			 * (session.getCustomerInteractionEvent(), accountnumber, athnumber,
			 * taxId, encryptedPin, true, true); validateAccountInfo =
			 * customerService.validateDepositAccountInfo(depReq);
			 * 
			 * } else if (accountType.equalsIgnoreCase("CCA")) {
			 * 
			 * 
			 * CardCVVAccountInformationValidationRequest cvvReq = new
			 * CardCVVAccountInformationValidationRequest
			 * (session.getCustomerInteractionEvent(), accountnumber, taxId,
			 * cvv, expMonth, expYear, zipCode, true); validateAccountInfo =
			 * customerService.validateCrediCardCVVAccountInfo(cvvReq);
			 * 
			 * } else if (accountType.equalsIgnoreCase("AMEX")) {
			 * 
			 * String encryptedPin = pinUtil.getEncryptedPin(pin);
			 * CardPINAccountInformationValidationRequest cardPinReq = new
			 * CardPINAccountInformationValidationRequest
			 * (session.getCustomerInteractionEvent(), accountnumber, taxId,
			 * encryptedPin, expMonth, expYear, zipCode, true, true, true);
			 * validateAccountInfo =
			 * customerService.validateCrediCardPINAccountInfo(cardPinReq);
			 * 
			 * } else if (accountType.equalsIgnoreCase("PFC")) {
			 * 
			 * String encryptedPin = pinUtil.getEncryptedPin(pin);
			 * PayrollCardAccountInformationValidationRequest payrollReq = new
			 * PayrollCardAccountInformationValidationRequest
			 * (session.getCustomerInteractionEvent(), accountnumber, taxId,
			 * encryptedPin, true); validateAccountInfo =
			 * customerService.validatePayrollCardAccountInfo(payrollReq);
			 * 
			 * }
			 */

			if (!error) {
				if (validateAccountInfo.getStatus() == AccountValidationStatus.VALID) 
				{
					profile = customerService.modifyEnrollmentAccountInformation(profile, accountnumber, accountType,
							validateAccountInfo.getAccountProductId());
					session.setCustomerProfile(profile);
					info.actionsProcessed.add(ACTION.ENROLLACT);
					return;
				} else if (validateAccountInfo.getStatus() == AccountValidationStatus.ACCOUNT_BLOCKED) 
				{
					errors.rejectValue("accountnumber", "", "Account is block, please call to CSR");
					return;
				} else if (validateAccountInfo.getStatus() == AccountValidationStatus.ATH_NOT_MATCH
						|| validateAccountInfo.getStatus() == AccountValidationStatus.ACCOUNT_INVALID
						|| validateAccountInfo.getStatus() == AccountValidationStatus.INVALID
						|| validateAccountInfo.getStatus() == AccountValidationStatus.INVALID_EXPDATE
						|| validateAccountInfo.getStatus() == AccountValidationStatus.INVALID_ZIPCODE) 
				{
					
					info.setActInfCounter(info.getActInfCounter() + 1);

					if (info.getActInfCounter() >= 3) 
					{
						//MBCA426
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA426))
						{
							customerService.getCustomerProfileService().updateCustomerStatus(profile, CustomerStatus.PENDING_CSR_ACTIVATION);
							session.getCustomerProfile().setStatus(CustomerStatus.PENDING_CSR_ACTIVATION);
							errors.rejectValue("accountnumber", "", "Account is block, please call to CSR");
							return;
						}else
						{
							errors.rejectValue("accountnumber", "", "Account is block, please call to CSR");
							return;
						}
					}					
					errors.rejectValue("accountnumber", "enrollment.accountnumber.invalid", "Invalid Account");
					return;

				} else 
				{					
					//MBCA-246 Alexis G
					//This look as a duplicate code but its acts as a insurance in case of new code from vault not included in MiBanco CustomerFlagType Enum
					info.setActInfCounter(info.getActInfCounter() + 1);
					if (info.getActInfCounter() >= 3) 
					{
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA426))
						{
							customerService.getCustomerProfileService().updateCustomerStatus(profile, CustomerStatus.PENDING_CSR_ACTIVATION);
							session.getCustomerProfile().setStatus(CustomerStatus.PENDING_CSR_ACTIVATION);
							return;	
						}else
						{
						errors.rejectValue("accountnumber", "", "Account is block, please call to CSR");
						return;
						}
					}					
					errors.rejectValue("accountnumber", "enrollment.accountnumber.invalid", "Invalid Account");
					return;
				}

			}
		}
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();
		Map<String, Object> model = new HashMap<>();
		ValidateInfoBean info = (ValidateInfoBean) command;
		model.put("customername", profile.getFirstName());
		model.put("page", String.valueOf(page));
		boolean dobValidaton = false;
		boolean actvalidation = false;
		List<ASK_FOR> tasksList = session.getTasksList();
		//MBCA88 by et56509


		for (ASK_FOR thing : tasksList) {
			switch (thing) {
			case VALIDATE_DOB:
				model.put("validatedob", true);
				dobValidaton = true;
				break;
			case VALIDATE_ENROLLACCOUNT:
				model.put("validateaccount", true);
				actvalidation = true;
				break;
			default:
				break;
			}
		}

		if (info.getActInfCounter() >= 3 || profile.getStatus().equals(CustomerStatus.PENDING_CSR_ACTIVATION)) {
			model.put("blocked", true);
		}
		if (info.getDobCounter() >= 3 || profile.getStatus().equals(CustomerStatus.PENDING_CSR_ACTIVATION)) {
			model.put("blocked", true);
		}

		List<ACTION> removetasks = info.getActionsProcessed();
		if (dobValidaton && !removetasks.contains(ACTION.DOB)) {
			model.put("showdobarea", true);
		} else if (actvalidation && !removetasks.contains(ACTION.ENROLLACT)
				&& (dobValidaton && !removetasks.contains(ACTION.DOB))) {
			model.put("showdobarea", true);
		} else if (actvalidation && !removetasks.contains(ACTION.ENROLLACT)
				&& (dobValidaton && removetasks.contains(ACTION.DOB))) {
			model.put("showdobarea", false);
			model.put("showacctarea", true);
		} else if (actvalidation && !removetasks.contains(ACTION.ENROLLACT) && !dobValidaton) {
			model.put("showacctarea", true);
		}

		if (dobValidaton) {

			if (info.getDobCounter() >= 3) {
				model.put("blocked", true);
				return model;
			}

			// months
			Map<Object, String> months = new LinkedHashMap<Object, String>();
			SimpleDateFormat df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
			Calendar cal = GregorianCalendar.getInstance();
			cal.set(Calendar.MONTH, 0);

			for (int i = 0; i < 12; i++) {
				String month = df.format(cal.getTime());
				months.put(i, month);
				cal.add(Calendar.MONTH, 1);
			}
			model.put("months", months);

			// days
			Map<Object, String> days = new LinkedHashMap<Object, String>();
			df = new SimpleDateFormat("d");
			cal.set(Calendar.DAY_OF_MONTH, 1);

			for (int i = 1; i <= 31; i++) {
				String day = df.format(cal.getTime());
				days.put(i, day);
				cal.add(Calendar.DAY_OF_MONTH, 1);
			}
			model.put("days", days);

			// years
			Map<Object, String> years = new LinkedHashMap<Object, String>();
			df = new SimpleDateFormat("yyyy");
			cal = GregorianCalendar.getInstance();
			cal.add(Calendar.YEAR, -this.ageRestriction);
			for (int i = 0; i <= 100; i++) {
				String year = df.format(cal.getTime());
				years.put(Integer.parseInt(year), year);
				cal.add(Calendar.YEAR, -1);
			}
			model.put("years", years);

		}

		if (actvalidation) {

			if (info.getActInfCounter() >= 3) {
				model.put("blocked", true);
				return model;
			}

			// expYears
			Map<Object, String> expYears = new LinkedHashMap<Object, String>();
			if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
				expYears.put("", "A&ntilde;os:");
			else
				expYears.put("", "Year:");
			SimpleDateFormat df = new SimpleDateFormat("yyyy");
			Calendar cal = GregorianCalendar.getInstance();

			for (int i = 0; i <= 8; i++) {
				String year = df.format(cal.getTime());
				expYears.put(Integer.parseInt(year), year);
				cal.add(Calendar.YEAR, 1);
			}
			model.put("expYears", expYears);

			// expMonths
			Map<Object, String> expMonths = new LinkedHashMap<Object, String>();
			if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
				expMonths.put("", "Mes:");
			else
				expMonths.put("", "Month:");
			df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
			cal = GregorianCalendar.getInstance();
			cal.set(Calendar.MONTH, 0);

			for (int i = 1; i <= 12; i++) {
				String month = df.format(cal.getTime());
				expMonths.put(i, month);
				cal.add(Calendar.MONTH, 1);
			}
			model.put("expMonths", expMonths);

			String accountInfoType = info.getAccountType();
			boolean hasREDI_HELOCS = false;
			boolean hasIDA = false;
			boolean hasCCA = false;
			boolean hasAMEX = false;
			boolean hasPFC = false;
			boolean hasNonTranAcc = false;
			if (GenericValidator.isBlankOrNull(accountInfoType)) {

				Calendar birthDate = Calendar.getInstance();
				birthDate.setTime(profile.getBirthDate());

				PersonalInformationValidationRequest personalRequest = new PersonalInformationValidationRequest(null,
						profile.getTaxId(), birthDate, false, false);
				personalRequest.setValidateMultipleProfiles(true);
				CustomerServices customerService = session.getCustomerService();
				PersonalInformationValidationResponse customerEnrollment = customerService
						.isEnrollableCustomer(personalRequest);
				
				//MBCA-742 Alexis: Set enrollable account to session for later use onBindandValidate
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA742)){					
					if(!customerEnrollment.getEnrollableAccounts().isEmpty()) {
						session.setEnrollableAccounts(customerEnrollment.getEnrollableAccounts());
					}
				}				

				List<EnrollableAccount> accounts = customerEnrollment.getEnrollableAccounts();
				if (accounts != null) {
					if (accounts.size() == 0)
						model.put("accountIDA", "TRUE");
					else {
						for (EnrollableAccount enrollableAccount : accounts) {
							String accountType = enrollableAccount.getAccountType();
							if (accountType.equalsIgnoreCase("IDA")) {
								model.put("accountIDA", true);
								hasIDA = true;
							} else if (accountType.equalsIgnoreCase("CCA")) {
								model.put("accountCCA", true);
								hasCCA = true;
								if ((enrollableAccount.getProductId() != null)
										&& (enrollableAccount.getProductId().equalsIgnoreCase("EVHLC") || enrollableAccount
												.getProductId().equalsIgnoreCase("EVHLP"))) {
									model.put("accountREDI_HELOCS", true);
									hasREDI_HELOCS = true;
								} else {
									model.put("CCAwithoutREDI_HELOCS", true);
								}
							} else if (accountType.equalsIgnoreCase("AMEX")) {
								model.put("accountAMEX", true);
								hasAMEX = true;

							} else if (accountType.equalsIgnoreCase("PFC")) {
								model.put("accountPFC", true);
								hasPFC = true;
							}
							else if( StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA88) && getNonTransactionalAccountSet().contains(accountType))
							{
								model.put("nonTranAcc", true);
								hasNonTranAcc = true;
							}
						}

						if(hasIDA)
						{
							info.setAccountType("IDA");
						} else if(hasCCA)
						{
							info.setAccountType("CCA");
						} else if(hasREDI_HELOCS)
						{
							info.setProductDescription("Redi Equity");
						} else if(hasAMEX)
						{
							info.setAccountType("AMEX");
						} else if(hasPFC)
						{
							info.setAccountType("PFC");
						}
						else if(hasNonTranAcc)
						{
							info.setAccountType("nonTranAcc");
						}
					}

				} else
					model.put("accountIDA", "TRUE");
			} else {
				if (accountInfoType.equalsIgnoreCase("IDA")) {
					model.put("accountIDA", "TRUE");
				} else if (accountInfoType.equalsIgnoreCase("CCA")) {
					model.put("accountCCA", "TRUE");
					if ((info.getProductDescription() != null)
							&& (info.getProductDescription().equalsIgnoreCase("Redi Equity"))) {
						model.put("accountREDI_HELOCS", true);
					} else {
						model.put("CCAwithoutREDI_HELOCS", true);
					}
				} else if (accountInfoType.equalsIgnoreCase("AMEX")) {
					model.put("accountAMEX", "TRUE");
				} else if (accountInfoType.equalsIgnoreCase("PFC")) {
					model.put("accountPFC", true);
				}
				else if ( StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA88) && getNonTransactionalAccountSet().contains(accountInfoType)  || accountInfoType.equalsIgnoreCase("nonTranAcc"))
				{
					model.put("nonTranAcc", true);
				}
			}
		} else
			model.put("accountIDA", true);

		return model;
	}
	@SuppressWarnings("unchecked")
	private List<String> getNonTransactionalAccountSet() {
		String[] ntAccountsArray = getNonTransProductFlow().split(",");
		return Arrays.asList(ntAccountsArray);
	}

	public PinEncryptionTool getPinTool() {
		return pinTool;
	}

	public void setPinTool(PinEncryptionTool pinTool) {
		this.pinTool = pinTool;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.ENROLLMENT;
	}

	public Integer getAgeRestriction() {
		return ageRestriction;
	}

	public void setAgeRestriction(Integer ageRestriction) {
		this.ageRestriction = ageRestriction;
	}

	public String getPaymentvalidationrange() {
		return paymentvalidationrange;
	}

	public void setPaymentvalidationrange(String paymentvalidationrange) {
		this.paymentvalidationrange = paymentvalidationrange;
	}

	public String getNonTransProductFlow() {
		return nonTransProductFlow;
	}

	public void setNonTransProductFlow(String nonTransProductFlow) {
		this.nonTransProductFlow = nonTransProductFlow;
	}

	
}
