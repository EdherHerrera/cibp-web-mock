package evertec.cibp.web.form;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;

import com.evertec.cibp.api.client.model.Error;
import com.evertec.cibp.api.client.model.MicroServiceResponse;

import evertec.cibp.core.models.account.AccountFeatures.ACCOUNT_TYPE_DISPLAY;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TsysLoyaltyRewardsInfo;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.web.facade.CreditCardFacade;
import evertec.cibp.web.form.bean.RewardsRedemptionBean;
import evertec.cibp.web.request.TsysLoyaltyRewardsRedemptionRequest;
import evertec.cibp.web.request.TsysLoyaltyRewardsRedemptionRequest.DirectDepositAccountInfo;
import evertec.cibp.web.request.TsysLoyaltyRewardsRedemptionRequest.RedemptionType;
import evertec.cibp.web.response.TsysLoyaltyRewardsRedemptionResponse;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.creditcards.exception.TsysLoyaltyRewardsException;

/**
 * RewardsRedemptionForm
 * class to help to make redemption on tsys
 * @author Evertec
 * @since N/A
 * @version 1.0
 * @see N/A
 */
@SuppressWarnings("deprecation")
public class RewardsRedemptionForm extends AbstractWizardFormController implements MessageSourceAware {

	/**
	 * LOGGER Utils class log
	 */
	protected final Log loggerInstance = LogFactory.getLog(getClass()); // Logger Utils object

	/**
	 * messageSource object tsys response value
	 */
	private MessageSource messageSource = null; // Message Source object

	/**
	 * 
	 */
	private final Format df = new SimpleDateFormat("MM/dd/yyyy"); // date format string

	/** Has a json map with bank ids as keys and routing numbers as values
	 * e.g. {"001":"021502011",...} */
	private String routingNumberJson = ""; // routing number

	private static final String LOG_ENTERED_PROCESS_FINISH = "Entered processFinish in RewardsRedemptionForm"; // Logger String 
	private static final String LOG_ATTEMPTING_PERFORM_REDEMPTION = "Attempting to perform redemption"; // Logger String
	private static final String LOG_INVALID_REDEMPTION_AMOUNT = "Invalid Redemption ammout was selected"; // Logger String
	private static final String LOG_INVALID_REDEMPTION_TYPE = "Invalid RedemptionType was selected"; // Logger String
	private static final String LOG_TERMS_CONDITIONS_NOT_SELECTED = "Terms and Conditions were not selected"; // Logger String
	private static final String LOG_REDEMPTION_SUCCESS = "Redemption was successfully executed."; // Logger String
	private static final String LOG_SEND_REDEMPTION_REQUEST = "Sending redemption request: %s"; // Logger String
	private static final String LOG_REDEMPTION_TYPE_NOT_SELECTED = "RedemptionType was not selected"; // Logger String
	private static final String LOG_ENTERED_REFERENCE_DATA = "Entered referenceData in RewardsRedemptionForm"; // Logger String 
	private static final String LOG_REDEMPTION_REWARDS_PAGE = "RewardsRedemptionForm page = "; // Logger String
	private static final String LOG_ENTERED_ON_BIND_VALIDATE = "Entered onBindAndValidate in RewardsRedemptionForm"; // Logger String 
	private static final String LOG_VALID_DIRECT_DEPOSIT_NOT_SELECTED = "RedemptionType is DIRECT_DEPOSIT but a valid depostirt account was not selected"; // Logger String
	private static final String LOG_REDEMPTION_COULD_NOT_BE_COMPLETED = "Redemption could not be compleated. Error code = %s, Error description = %s"; // Logger String
	private static final String LOG_ENTERED_PERFORM_CASH_REWARDS_REDEMPTION = "Entered performCashRewardsRedemption in RewardsRedemptionForm"; // Logger String 
	private static final String LOG_ERROR_PARSING_ROUTING_NUMBER = "Error parsing routing number map JSON in cibp.properties named: routing.numbers.by.bank.id.json"; // Logger String

	private static final String REDEMPTION_SUCESS_VALUE = "redemptionSuccess"; // Value String
	private static final String PAGE_VALUE = "page"; // Value String
	private static final String REDEMPTION_BEAN_VALUE = "redemptionBean"; // Value String
	private static final String REWARDS_REDEMPTION_VALUE = "rewardsRedemption"; // Value String
	private static final String REDEMPTION_TYPE_VALUE = "redemptionType"; // Value String
	private static final String PATH_ERROR_TSYS_FORM_VALUE = "tsys.loyalty.rewards.error.form"; // Value String
	private static final String COMPLETE_REDEMPTION_FORM_VALUE = "Complete the remption form"; // Value String
	private static final String FRONT_END_ID_VALUE = "frontEndId"; // Value String
	private static final String MESSAGE_SOURCE_PATH_VALUE = "tsys.loyalty.rewards.low.ammount.error.message"; // Value String
	private static final String LOW_AMOUNT_ERROR_MESSAGE_VALUE = "lowAmmountErrorMessage"; // Value String
	private static final String AMOUNT_RANGE_MESSAGE_VALUE = "ammountRangeMessage"; // Value String
	private static final String PAGE_TWO_VALUE = "2"; // Form Page Two
	private static final String REDEEM_AMOUNT_VALUE = "redeemAmount"; // Value String
	private static final String TERMS_CONDITIONS_VALUE = "termsAndCond"; // Value String
	private static final String NONE_VALUE = "none"; // Value String
	private static final String NAME_VALUE = "name"; // Value String
	private static final String AMOUNT_REDEEMED_VALUE = "amountRedeemed"; // Value String
	private static final String CONFIRMATION_NUMBER_VALUE = "confirmationNumber"; // Value String
	private static final String HAS_ACCOUNT_DEPOSIT_VALUE = "hasAccountDeposit"; // Value String
	private static final String ACCOUNT_TYPE_CHECKING = "Checkings"; // Value String
	private static final String ACCOUNT_TYPE_SAVING = "Savings"; // Value String
	
	/**
	 * Constructor for RewardsRedemptionForm form View and bean
	 * definition
	 */
	public RewardsRedemptionForm() {
		super();
		setCommandClass(RewardsRedemptionBean.class);
		setCommandName(REDEMPTION_BEAN_VALUE);
		setPages(new String[] { REWARDS_REDEMPTION_VALUE, REWARDS_REDEMPTION_VALUE, REWARDS_REDEMPTION_VALUE });
	}

	/**
	 * processFinish
	 * @param request
	 * @param response
	 * @param bean
	 * @param error
	 * @throws Exception
	 * @return ModelAndView
	 */
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object bean,
			BindException error) throws Exception {
		final Log logger = loggerInstance; // Logger Object
		logger.info(LOG_ENTERED_PROCESS_FINISH);
		Map<String, Object> model = new HashMap<>(0); // model-view return

		RewardsRedemptionBean redemptionBean = null; // info redemption request
		if (bean instanceof RewardsRedemptionBean) {
			redemptionBean = (RewardsRedemptionBean) bean;
		}
		final UserSession userSession = Utils.getUserSession(request); // UserSession
		final CustomerProfile profile = userSession.getCustomerProfile(); // Model Customer Profile
		logger.info(StringEscapeUtils.escapeJava(LOG_ATTEMPTING_PERFORM_REDEMPTION));
		final TsysLoyaltyRewardsRedemptionResponse rewardsRedemption = performCashRewardsRedemption(redemptionBean, userSession); // Redemption Response

		if (rewardsRedemption != null && redemptionBean != null) {
			model.put(REDEMPTION_SUCESS_VALUE, true);
			model.put(CONFIRMATION_NUMBER_VALUE, rewardsRedemption.getConfirmationNumber());
			updatetsysLoyaltyRewardsInfo(rewardsRedemption, redemptionBean.getCashRewardsAccount());
			if (!GenericValidator.isBlankOrNull(profile.getEmail())) {
				model.put(NAME_VALUE, profile.getFirstName());
				model.put(AMOUNT_REDEEMED_VALUE, rewardsRedemption.getCashbackAmount().getValue());
				userSession.getCustomerService().sendCashRewardsOneTimeRedemptionEmail(profile, model, userSession.getPreferredLanguage());
			}
		}
		else {
			model.put(REDEMPTION_SUCESS_VALUE, false);
		}

		model.put(PAGE_VALUE, PAGE_TWO_VALUE);
		model.put(REDEMPTION_BEAN_VALUE, redemptionBean);
		return new ModelAndView(REWARDS_REDEMPTION_VALUE, model);
	}

	/**
	 * referenceData
	 * @param request
	 * @param command
	 * @param errors
	 * @param page
	 * @throws Exception
	 * @return Map<Object, Object>
	 *          model-view return
	 */
	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		final Log logger = loggerInstance; // Logger Object
		logger.info(LOG_ENTERED_REFERENCE_DATA);

		final MessageSource messageSourceValue = this.messageSource; // Object message source value

		RewardsRedemptionBean redemptionBean = null; // data info request
		if (command instanceof RewardsRedemptionBean) {
			redemptionBean = (RewardsRedemptionBean) command;
		}
		Map<String, Object> model = new HashMap<>(0); // model-view return
		TVFrontendAccount ccaAccountByFrontEndId = null; // cca Account
		String frontEndId = ""; // cashRewards account

		final UserSession userSession = Utils.getUserSession(request); // UserSession

		logger.info(StringEscapeUtils.escapeJava(LOG_REDEMPTION_REWARDS_PAGE + page));

		if (page == 0 && redemptionBean != null) {
			
			//If the customer returns the terms and conditions must be accepted again.
			redemptionBean.setTermsAndCond(false);
			
			frontEndId = request.getParameter(FRONT_END_ID_VALUE);

			ccaAccountByFrontEndId = Utils.getAccountById(frontEndId, userSession.getCcaAccounts());

			final TsysLoyaltyRewardsInfo tsysLoyaltyRewardsInfo = ccaAccountByFrontEndId.getTsysLoyaltyRewardsInfo(); //CashRewards Info

			final NumberTool numberTool = new NumberTool(); // Utils
			final String minimumRequiredBalance = numberTool.format("currency", tsysLoyaltyRewardsInfo.getMinimumRewardsBalance()); // minimun balance redemption

			final Object[] objects = new Object[] {minimumRequiredBalance}; // object to error message
			final String lowAmmountErrorMessage = messageSourceValue.getMessage(MESSAGE_SOURCE_PATH_VALUE, objects, "The minimum amount to redeem your Cash Rewards is {0}.", new Locale(userSession.getPreferredLanguage().getCode())); // Error Message

			final String ammountRangeMessage = messageSourceValue.getMessage("tsys.loyalty.rewards.compareAmounts", new Object[] {minimumRequiredBalance, tsysLoyaltyRewardsInfo.getAvailableRewardsBalance()},  "The amount to be inserted must range between {0} and {1}.", new Locale(userSession.getPreferredLanguage().getCode())); // amount message tsys response

			model.put(LOW_AMOUNT_ERROR_MESSAGE_VALUE, lowAmmountErrorMessage);
			model.put(AMOUNT_RANGE_MESSAGE_VALUE, ammountRangeMessage);

			redemptionBean.setFrontEndId(ccaAccountByFrontEndId.getFrontEndId());
			redemptionBean.setCashRewardsAccount(ccaAccountByFrontEndId);
			redemptionBean.setRedemptionDate(formatDate(new Date()));
			redemptionBean.setAcctNumber(ccaAccountByFrontEndId.getAccountNumber());
			redemptionBean.setCardName(ccaAccountByFrontEndId.getDescription());

			final List<TVFrontendAccount> userAcct = userSession.getAccounts(); // User Accounts
			List<TVFrontendAccount> eligebleDirectDepositAccounts = new ArrayList<>(0); // elegeble user accounts to redemption

			final String ida = "IDA"; // user account type ida
			if (userAcct != null) {
				for (Iterator<TVFrontendAccount> iter = userAcct.iterator(); iter.hasNext();) {
					TVFrontendAccount acct = iter.next(); // single object iterator

					if (acct.getSubtype().equalsIgnoreCase(ida) && Utils.excludeAccounts(acct, userSession)) {
						model.put(HAS_ACCOUNT_DEPOSIT_VALUE, true);
						eligebleDirectDepositAccounts.add(acct);
					}
					redemptionBean.setEligebleDirectDepositAccounts(eligebleDirectDepositAccounts);
				}
			}
		} else if (page == 1 && redemptionBean != null) {
			redemptionBean.setDirectDepositAccount(Utils.getAccountById(redemptionBean.getAcctNumber(),
					redemptionBean.getEligebleDirectDepositAccounts()));
		}

		model.put(PAGE_VALUE, page);

		model.put(REDEMPTION_BEAN_VALUE, redemptionBean);
		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {
		final Log logger = loggerInstance; // Logger Object
		logger.info(StringEscapeUtils.escapeJavaScript(LOG_ENTERED_ON_BIND_VALIDATE));

		final UserSession userSession = Utils.getUserSession(request); //UserSession
		RewardsRedemptionBean redemptionBean = null; //Info Redemption Info
		if (command instanceof RewardsRedemptionBean) {
			redemptionBean = (RewardsRedemptionBean) command;
		}

		if (redemptionBean != null) {
			final TVFrontendAccount directDepositAccount = Utils.getAccountById(redemptionBean.getAcctNumber(), 
					userSession.getAccounts()); // The selected TVFrontEndAccount for direct deposit.

			if (GenericValidator.isBlankOrNull(redemptionBean.getRedemptionType())) {
				errors.rejectValue(REDEMPTION_TYPE_VALUE, PATH_ERROR_TSYS_FORM_VALUE, COMPLETE_REDEMPTION_FORM_VALUE);
				logger.info(StringEscapeUtils.escapeJava(LOG_REDEMPTION_TYPE_NOT_SELECTED));
			} 

			if (RedemptionType.DIRECT_DEPOSIT.toString().equals(redemptionBean.getRedemptionType())) {
				if (GenericValidator.isBlankOrNull(redemptionBean.getAcctNumber()) ||
						redemptionBean.getAcctNumber().equalsIgnoreCase(NONE_VALUE) ||
						directDepositAccount == null) {
					logger.info(StringEscapeUtils.escapeJava(LOG_VALID_DIRECT_DEPOSIT_NOT_SELECTED));
					errors.rejectValue(REDEMPTION_TYPE_VALUE, PATH_ERROR_TSYS_FORM_VALUE, COMPLETE_REDEMPTION_FORM_VALUE);
				}
			}

			if (!redemptionBean.isTermsAndCond()) {
				logger.info(StringEscapeUtils.escapeJava(LOG_TERMS_CONDITIONS_NOT_SELECTED));
				errors.rejectValue(TERMS_CONDITIONS_VALUE, PATH_ERROR_TSYS_FORM_VALUE, COMPLETE_REDEMPTION_FORM_VALUE);
			}
			if (!RedemptionType.STATEMENT_CREDIT.toString().equals(redemptionBean.getRedemptionType())
					&& !RedemptionType.DIRECT_DEPOSIT.toString().equals(redemptionBean.getRedemptionType())) {
				errors.rejectValue(REDEMPTION_TYPE_VALUE, PATH_ERROR_TSYS_FORM_VALUE, COMPLETE_REDEMPTION_FORM_VALUE);
				logger.info(LOG_INVALID_REDEMPTION_TYPE);
			}

			final TsysLoyaltyRewardsInfo tsysLoyaltyRewardsInfo = redemptionBean.getCashRewardsAccount().getTsysLoyaltyRewardsInfo(); // tsys info

			final boolean isInvalidRedemption = redemptionBean.getRedeemAmount()==null || redemptionBean.getRedeemAmount().isNaN() || 
					redemptionBean.getRedeemAmount() < tsysLoyaltyRewardsInfo.getMinimumRewardsBalance() ||
					redemptionBean.getRedeemAmount() > tsysLoyaltyRewardsInfo.getAvailableBalanceDouble(); // is Valid Redemption

					if (isInvalidRedemption) {
						errors.rejectValue(REDEEM_AMOUNT_VALUE, PATH_ERROR_TSYS_FORM_VALUE, COMPLETE_REDEMPTION_FORM_VALUE);
						logger.info(LOG_INVALID_REDEMPTION_AMOUNT);
					}
		}

	}

	/**
	 * performCashRewardsRedemption
	 * @param redemptionBean
	 * @param userSession
	 * @return TsysLoyaltyRewardsRedemptionResponse tsys redemption response
	 * @throws TsysLoyaltyRewardsException
	 */
	protected TsysLoyaltyRewardsRedemptionResponse performCashRewardsRedemption(RewardsRedemptionBean redemptionBean,
			UserSession userSession) throws TsysLoyaltyRewardsException {

		final Log logger = loggerInstance; // Logger Object
		logger.info(LOG_ENTERED_PERFORM_CASH_REWARDS_REDEMPTION);

		final RedemptionType redemptionType = RedemptionType.valueOf(redemptionBean.getRedemptionType()); // Redemption Type

		TsysLoyaltyRewardsRedemptionRequest rewardsRedemptionRequest = new TsysLoyaltyRewardsRedemptionRequest(); // Model Request Object
		rewardsRedemptionRequest.setRedemptionType(redemptionType);
		rewardsRedemptionRequest.setRewardsValue(redemptionBean.getRedeemAmount().toString());

		if (redemptionType == RedemptionType.DIRECT_DEPOSIT) {
			try {
				DirectDepositAccountInfo depositAccountInfo = new DirectDepositAccountInfo(); // Object with request redemption info
				depositAccountInfo.setAccountNumber(redemptionBean.getDirectDepositAccount().getAccountNumber());
				
				final String accountTypeDisplay = redemptionBean.getDirectDepositAccount().getFeatures().getAccountDisplayType(); //Account Type Display value
				
				if (accountTypeDisplay != null && accountTypeDisplay.equals(ACCOUNT_TYPE_DISPLAY.SAVING.toString())) {
					depositAccountInfo.setAccountType(ACCOUNT_TYPE_SAVING);
				} else {
					depositAccountInfo.setAccountType(ACCOUNT_TYPE_CHECKING);
				}
				
				final JSONObject routingNumbersMap = new JSONObject(routingNumberJson); // Routing Map Value Json
				final String routingNumber = routingNumbersMap.getString(redemptionBean.getDirectDepositAccount().getBankId()); // Routing Map Value
				depositAccountInfo.setRoutingNumber(routingNumber);

				rewardsRedemptionRequest.setTransferAccountInfo(depositAccountInfo);
			} catch (JSONException e) {
				logger.error(LOG_ERROR_PARSING_ROUTING_NUMBER, e);
				return null;
			}
		}

		logger.info(String.format(LOG_SEND_REDEMPTION_REQUEST, rewardsRedemptionRequest.toString()));
		
		final MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> redemptionResponse = CreditCardFacade.getInstance()
				.tsysLoyaltyRewardsRedemption(userSession, null, 
						redemptionBean.getCashRewardsAccount().getAccountNumber(),
						rewardsRedemptionRequest); // Object return

		if (redemptionResponse.getData() != null && !redemptionResponse.getData().isEmpty()) {
			logger.info(LOG_REDEMPTION_SUCCESS);

			//Only one element is in the data array if it is not empty.
			return redemptionResponse.getData().get(0);
		} else {

			final Error error = redemptionResponse.getErrors().get(0); // error Object return

			logger.error(StringEscapeUtils.escapeJava(String.format(LOG_REDEMPTION_COULD_NOT_BE_COMPLETED, error.getCode(), error.getDetail())));
			return null;
		}
	}

	/**
	 * updatetsysLoyaltyRewardsInfo update amounts tsys Casg rewards
	 * @param rewardsRedemptionResponse
	 * @param cashRewardsAccount
	 */
	private void updatetsysLoyaltyRewardsInfo(TsysLoyaltyRewardsRedemptionResponse rewardsRedemptionResponse, TVFrontendAccount cashRewardsAccount) {
		final boolean isValidInfo = rewardsRedemptionResponse != null && 
				rewardsRedemptionResponse.getRewardsBalance() != null && 
				rewardsRedemptionResponse.getRewardsBalance().getValue() != null; // is valid values

		if (isValidInfo) {
			TsysLoyaltyRewardsInfo info = cashRewardsAccount.getTsysLoyaltyRewardsInfo(); // account amount values response

			info.setAvailableRewardsBalance(rewardsRedemptionResponse.getRewardsBalance().getValue());
			cashRewardsAccount.setTsysLoyaltyRewardsInfo(info);
		}
	}
	
	/**
	 * Get thread safe date string
	 * @param date
	 * @return string representation of the date
	 */
	public synchronized String formatDate(Date date) {
		return df.format(date); 
	}


	/**
	 * getMessageSource
	 * @return MessageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * setMessageSource
	 * @param messageSource
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * getRoutingNumberJson
	 * @return String
	 */
	public String getRoutingNumberJson() {
		return routingNumberJson;
	}

	/**
	 * setRoutingNumberJson
	 * @param routingNumberJson
	 */
	public void setRoutingNumberJson(String routingNumberJson) {
		this.routingNumberJson = routingNumberJson;
	}
	
}
