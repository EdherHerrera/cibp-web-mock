package evertec.cibp.web.form;

import com.rsa.csd.ws.DeviceRequest;
import edu.emory.mathcs.backport.java.util.Arrays;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.customer.CustomerPassword;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerSignonStats;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.ebill.Ebpp2BillerProductIdResponse;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.BankingSessionRequest;
import evertec.cibp.core.services.requests.CardCVVAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.CardPINAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.DepositAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.requests.NonTransAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.PayrollCardAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.ValidateLoginInfoRequest;
import evertec.cibp.core.services.requests.adaptiveauth.EnrollmentInformationinRSARequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.RsaQuestionsUpdateResponse;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse.AccountValidationStatus;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionSelectionResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.encryptedPin.PinEncryptionTool;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ValidateInfoBean;
import evertec.cibp.web.form.bean.ValidateInfoBean.ACTION;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.utils.ValidationUtil;
import evertec.otherservices.beans.UserAccount;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.RegisterUserAccountReq;
import evertec.otherservices.responses.RegisterUserAccountResp;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class LoginSSDSForcedForm extends AbstractWizardFormController implements GlobalEntitlementAwareController {
	
	private static final String INVALID_USERNAME = "Invalid Username";
	private static final String INCORRECT_PASSWORD = "Incorrect Password";
	private static final String CHANGEPASSWORD_PASSWORD_PREVIOUSLY_USED = "changepassword.password.previously.used";
	private static final String PARAMETER_PASSWORDS = "passwords";
	private static final String ENROLLMENT_PWD_INVALID = "enrollment.pwd.invalid";
	private static final String INVALID_PASSWORD = "Invalid Password";
	private static final String PASSWORD_CONTAINS_USERNAME = "Password must not contains username";
	private static final String ENROLLMENT_PASSWORD_CONTAINS_USERNAME = "enrollment.pwd.contains.username";
	private static final String PASSWORD_ERROR = "password";
	private static final String ENROLLMENT_USERNAME_NOAVAILABILITY = "enrollment.username.noavailability";
	private static final String INVALID = "Invalid";
	private static final String ACCOUNT_NUMBER = "accountnumber";
	private static final String PLEASE_VERIFY_INPUT = "Please verify input";
	private static final String EMAIL2 = "email";
	private static final String ENROLLMENT_EMAIL_INVALID = "enrollment.email.invalid";
	private static final String INVALID_EMAIL = "Invalid Email";
	private static final String ANSWERS_VALUES_MUST_MATCH = "Answers values must match";
	private static final String ENROLLMENT_ANSWERCONFIRM_NOTMATCH = "enrollment.answerconfirm.notmatch";
	private static final String ENROLLMENT_ANSWER_INVALID = "enrollment.answer.invalid";
	private static final String AGREE = "agree";
	private static final String INACTIVE = "inactive";
	private static final String PM_DATA = "PMData";
	private static final String USER_SESSION = "userSession";
	private static final String LOGIN_SSDS_FORCED = "loginSsdsForced";
	private static final String VALID_AVAILABLE_USERNAME = "validAvailableUsername";
	
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private PinEncryptionTool pinTool;
	private boolean availableUsername = true; // CIBP-1947
	private MessageSource messageSource;
	private String paymentvalidationrange;
	private String nonTransProductFlow;
	
	// MBSFE-691
	private String invalidEmailDomains;
	// END MBSFE-691

	// jocampo FFEIC
	private Integer maxPasswordRecords;

	// FIN FFEIC

	public LoginSSDSForcedForm() {
		super();
		setCommandClass(ValidateInfoBean.class);
		setCommandName(LOGIN_SSDS_FORCED);
		setPages(new String[] { LOGIN_SSDS_FORCED, LOGIN_SSDS_FORCED, LOGIN_SSDS_FORCED, LOGIN_SSDS_FORCED,
				LOGIN_SSDS_FORCED });
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new HashMap<>();
		ValidateInfoBean info = (ValidateInfoBean) command;
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION);
		CustomerServices customerService = session.getCustomerService();
		CustomerProfile profile = session.getCustomerProfile();

		Cookie cookie = WebUtils.getCookie(request, PM_DATA) != null ? WebUtils.getCookie(request, PM_DATA)
				: new Cookie(PM_DATA, "");

		cookie.setPath("/");
		if (session.getDeviceRequest() != null)
			cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
		ESAPI.httpUtilities().addCookie(response, cookie);


		List<ASK_FOR> tasksList = session.getTasksList();
		if (tasksList.contains(ASK_FOR.SSDS_FORCED_LOGIN)) {
			List<ACTION> removetasks = info.getActionsProcessed();
			if (removetasks.contains(ACTION.EMAIL) && (removetasks.contains(ACTION.PIN) || removetasks.contains(ACTION.NONTRANSACT))
					&& removetasks.contains(ACTION.USERNAME) && removetasks.contains(ACTION.PASSWORD)
					&& removetasks.contains(ACTION.QUESTIONS) && removetasks.contains(ACTION.CONTRACT)) {
				// All tasks for SSDS or FORCED first time login were validated
				tasksList.remove(ASK_FOR.SSDS_FORCED_LOGIN);
				session.getCustomerService().createInitialCustomerEntitlements(session.getCustomerProfile());

				// IF gofree o cima = batch
				if (session.getCustomerProfile().getEnrollmentSource() == CustomerEnrollmentSource.BATCH
						&& session.getCustomerProfile().hasEntitlement(EntitlementType.EBILLS)) {
					BankingSessionRequest bankingSessionRequest = new BankingSessionRequest(
							Utils.generateCustomerInteractionEvent(request), session.getCustomerProfile(), null,
							new BankingEntitlement(session.getCustomerProfile().getEntitlementByType(
									EntitlementType.BANKING)), false);
					BankingSession bankingSession = session.getCustomerService()
							.getBankingSession(bankingSessionRequest).getBankingSession();
					if (bankingSession != null) {
						List<TVFrontendAccount> userAccounts = session.getCustomerService().getCustomerAccounts(
								session.getCustomerProfile(), null, bankingSession);
						for (Iterator<TVFrontendAccount> iterator = userAccounts.iterator(); iterator.hasNext();) {
							TVFrontendAccount act = (TVFrontendAccount) iterator.next();

							if (act.getFeatures().isEStatement()
									&& (act.getProductDefinition().getProductId().equals("081") || act
											.getProductDefinition().getProductId().equals("087"))) {

								// The user has an EBPP2 ID
								if (session.isEbpp2UserIdLookedUp() && !session.getEbpp2UserId().equals("")) {

									// This is the list that will contain all
									// the accounts and their
									// respective summaries
									boolean refresh = false;

									// Get the EBPP2 biller product information
									// for account
									Ebpp2BillerProductIdResponse ebpp2BillerProductIdResponse = customerService
											.getEbpp2BillerProductIdInformationDepositAccount(act
													.getProductDefinition().getProductId());

									int billerProductId = ebpp2BillerProductIdResponse.getBillerProductId();

									// Find all the user's e-accounts
									if (session.isHasEAccounts()) {
										for (TVFrontendAccount account : session.getAccounts()) {
											if (account.getAccountProductId() != null
													&& account.getAccountProductId().equals(
															act.getProductDefinition().getProductId())) {
												boolean found = false;

												// Is this account already
												// active?
												for (UserAccount userAccount : session.getEbpp2UserAccounts()) {
													if (userAccount
															.getAccountNumber()
															.replaceFirst("^0+(?!$)", "")
															.equals(account.getAccountNumber().replaceFirst("^0+(?!$)",
																	""))) {
														found = true;
														break;
													}
												}

												// If the account is not active
												// in EBPP2, activate it
												if (!found) {
													boolean continueProcess = customerService
															.activateEbpp2AccountInIDA(bankingSession, account);
													if (continueProcess) {
														RegisterUserAccountReq req = new RegisterUserAccountReq();
														req.setAccountNumber(account.getAccountNumber());
														req.setBillerId(1);
														req.setBillerProductId(billerProductId);
														req.setLabel(account.getNickname());
														req.setNss(profile.getTaxId());
														req.setUserId(Integer.parseInt(session.getEbpp2UserId()));
														req.setZip("00000");
														RegisterUserAccountResp resp = customerService
																.registerEbpp2UserAccount(req);
														if (!resp.getResponseStatus().equals(ResponseStatus.ERROR)
																&& resp.getRegisterResponse().equals("0"))
															refresh = true;
													}
												}
											}
										}

										// If we registered any e-accounts,
										// refresh the user
										// accounts and summaries
										if (refresh) {
											session.setEbpp2UserAccounts(null);
											session.setEbpp2UserAccountsAndSummaries(null);
										}
									}
								}
							}
						}
					}
				} else
				// in case ssds
				{
					BankingSessionRequest bankingSessionRequest = new BankingSessionRequest(
							Utils.generateCustomerInteractionEvent(request), session.getCustomerProfile(), null,
							new BankingEntitlement(session.getCustomerProfile().getEntitlementByType(
									EntitlementType.BANKING)), false);
					BankingSession bankingSession = session.getCustomerService()
							.getBankingSession(bankingSessionRequest).getBankingSession();
					if (bankingSession != null) {
						List<TVFrontendAccount> userAccounts = session.getCustomerService().getCustomerAccounts(
								session.getCustomerProfile(), null, bankingSession);
						for (Iterator<TVFrontendAccount> iterator = userAccounts.iterator(); iterator.hasNext();) {
							TVFrontendAccount act = (TVFrontendAccount) iterator.next();
							if (act.getAccountNumber().equals(profile.getEnrollmentAccountNumber())) {
								// In case the account information is not
								// complete(same day open - inscription )
								profile = customerService.modifyEnrollmentAccountInformation(profile,
										act.getAccountNumber(), act.getSubtype(), act.getTVAccount().getRecords()
												.getProductId());
							}
						}
					}
				}
			}
		} else if (tasksList.contains(ASK_FOR.RSA_ENROLL_USERNAME)) {
			// Username was migrated with invalid username, after username
			// change successful the custoemr will go direct to portal
			List<ACTION> removetasks = info.getActionsProcessed();
			if (removetasks.contains(ACTION.EMAIL) && (removetasks.contains(ACTION.PIN) || removetasks.contains(ACTION.NONTRANSACT))
					&& removetasks.contains(ACTION.USERNAME) && removetasks.contains(ACTION.PASSWORD)
					&& removetasks.contains(ACTION.QUESTIONS) && removetasks.contains(ACTION.CONTRACT)) {
				tasksList.remove(ASK_FOR.RSA_ENROLL_USERNAME);

				ModifyCustomerRequest reqs = new ModifyCustomerRequest(Utils.generateCustomerInteractionEvent(request),
						profile.getProfileId(), profile.getPasswordHash(), profile.getPasswordSalt(), "", 0, "",
						profile.getStatus(), null, getDeviceRequest(request, session, info.getPm_fp()));
				reqs.setCustomerFlagType(CustomerFlagType.MUST_CHANGE_USERNAME);
				customerService.resetCustomerFlag(reqs);

				reqs.setCustomerFlagType(CustomerFlagType.MUST_CHANGE_PASSWORD);
				customerService.resetCustomerFlag(reqs);
			}
		}

		//MBCA-349 set a customer a transactional or non-transactional based on enrollment account sub-type.
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA349) && profile.getIsTransactional() == null) {	
			
			String isTransactional = "Y";
			
			if (getNonTransactionalAccountSet().contains(profile.getEnrollmentAccountSubtype())) {
				isTransactional = "N";
			}

			customerService.setIsTransactional(profile, isTransactional);
		}
		
		if (info.getPinCounter() >= 3) {
			tasksList.add(ASK_FOR.VALIDATE_PIN);
			ModifyCustomerRequest modReq = new ModifyCustomerRequest(Utils.generateCustomerInteractionEvent(request),
					session.getCustomerProfile().getProfileId(), "", 0, null, 0, null, null,
					CustomerFlagType.VALIDATE_PIN, getDeviceRequest(request, session, info.getPm_fp()));
			CustomerFacade.getInstance().addCustomerFlag(session, modReq);
			model.put("blocked", true);
			return showForm(request, error, LOGIN_SSDS_FORCED, model);
			// TODO add a customer flag in db to the cleint and present an error
			// message in login - Customer is blocked
		}

		//MBSD-2337 Sign-On Email Alert
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD2337) 
				&& profile.getEnrollmentSource() == CustomerEnrollmentSource.CSR 
				&& profile.getStatus().equals(CustomerStatus.ENROLLED))
			customerService.sendEmailSignOnEmailAlert(profile, profile.getEmail(), session.getPreferredLanguage());
				
		return new ModelAndView(SignonSequenceUtils.getNextView(session));
	}

	@SuppressWarnings("unchecked")
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		ValidateInfoBean info = (ValidateInfoBean) command;
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION);
		CustomerProfile profile = session.getCustomerProfile();
		CustomerServices customerService = session.getCustomerService();
		
		Boolean inactive = (Boolean)WebUtils.getSessionAttribute(request, INACTIVE);
		if(inactive == null){
			inactive = Boolean.FALSE;
		}
	
		ValidateInfoBean.ACTION action = info.getAction();

		if (page == 0) {
			
			// MBSFE-691
			String[] domain = info.getEmail().split("@");
			// END MBSFE-69
			
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE13)){
				if (action == ValidateInfoBean.ACTION.EMAIL) {
					String email = info.getEmail();
					String emailConf = info.getEmailconfirm();
					if (GenericValidator.isBlankOrNull(info.getEmail())) {
						errors.rejectValue(EMAIL2, ENROLLMENT_EMAIL_INVALID, INVALID_EMAIL);
					} else if (!GenericValidator.isBlankOrNull(info.getEmail())
							&& !GenericValidator.isEmail(info.getEmail())) {
						errors.rejectValue(EMAIL2, ENROLLMENT_EMAIL_INVALID, INVALID_EMAIL);
					} else if (!GenericValidator.isBlankOrNull(info.getEmail())
							&& !info.getEmail().equalsIgnoreCase(info.getEmailconfirm())) {
						errors.rejectValue("emailconfirm", "enrollment.emailconfirm.notmatch", "Email values must match");
						
					// MBSFE-691
					} else if (Utils.haveType(this.invalidEmailDomains, domain[1])
							&& StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE691)) {
						errors.rejectValue(EMAIL2, "email.invaliddomain",
								"Your e-mail address is incorrect. Please try again.");
					// END MBSFE-691

					} else if (email != null && emailConf != null && email.equalsIgnoreCase(emailConf)) {
						if (email.equalsIgnoreCase(profile.getEmail()))
							info.actionsProcessed.add(ACTION.EMAIL);
						else
							errors.rejectValue(EMAIL2, ENROLLMENT_EMAIL_INVALID, INVALID_EMAIL);
					} else {
						errors.rejectValue(EMAIL2, ENROLLMENT_EMAIL_INVALID, INVALID_EMAIL);
					}
				}
			}
			else{
				if (action == ValidateInfoBean.ACTION.EMAIL) {
					String email = info.getEmail();
					String emailConf = info.getEmailconfirm();
					if (GenericValidator.isBlankOrNull(info.getEmail())) {
						errors.rejectValue(EMAIL2, ENROLLMENT_EMAIL_INVALID, INVALID_EMAIL);
					} else if (!GenericValidator.isBlankOrNull(info.getEmail())
							&& !GenericValidator.isEmail(info.getEmail())) {
						errors.rejectValue(EMAIL2, ENROLLMENT_EMAIL_INVALID, INVALID_EMAIL);
					} else if (!GenericValidator.isBlankOrNull(info.getEmail())
							&& !info.getEmail().equals(info.getEmailconfirm())) {
						errors.rejectValue("emailconfirm", "enrollment.emailconfirm.notmatch", "Email values must match");
					
					// MBSFE-691
					} else if (Utils.haveType(this.invalidEmailDomains, domain[1])
							&& StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE691)) {
						errors.rejectValue(EMAIL2, "email.invaliddomain",
								"Your e-mail address is incorrect. Please try again.");
					// END MBSFE-691
						
					} else if (email != null && emailConf != null && email.equals(emailConf)) {
						if (email.equals(profile.getEmail()))
							info.actionsProcessed.add(ACTION.EMAIL);
						else
							errors.rejectValue(EMAIL2, ENROLLMENT_EMAIL_INVALID, INVALID_EMAIL);
					} else {
						errors.rejectValue(EMAIL2, ENROLLMENT_EMAIL_INVALID, INVALID_EMAIL);
					}
				}
			}
			
		}

		else if (page == 1) {

			ValidateAccountStatusResponse res = null;
			if (action == ValidateInfoBean.ACTION.PIN) {
				String pin = info.getPin();
				if (!(!GenericValidator.isBlankOrNull(pin) && GenericValidator.matchRegexp(pin, "^\\d{4}$"))) {
					errors.rejectValue("pin", "enrollment.pin.invalid", "Invalid PIN code");
					return;
				}
				String encryptedPin = pinTool.encrypt(pin);
				if (info.getAccountType().equalsIgnoreCase("IDA")) {
					DepositAccountInformationValidationRequest req = new DepositAccountInformationValidationRequest(
							Utils.generateCustomerInteractionEvent(request), profile.getEnrollmentAccountNumber(), "",
							profile.getTaxId(), encryptedPin, true, false);
					res = customerService.validateDepositAccountInfo(req);

				} else if (info.getAccountType().equalsIgnoreCase("AMEX")) {
					CardPINAccountInformationValidationRequest req = new CardPINAccountInformationValidationRequest(
							Utils.generateCustomerInteractionEvent(request), profile.getEnrollmentAccountNumber(),
							profile.getTaxId(), encryptedPin, "", "", "", true, false, false);
					res = customerService.validateCrediCardPINAccountInfo(req);

				} else if (info.getAccountType().equalsIgnoreCase("PFC")) {
					PayrollCardAccountInformationValidationRequest req = new PayrollCardAccountInformationValidationRequest(
							Utils.generateCustomerInteractionEvent(request), profile.getEnrollmentAccountNumber(),
							profile.getTaxId(), encryptedPin, true);
					res = customerService.validatePayrollCardAccountInfo(req);
				}

				if (res != null && res.getStatus() == AccountValidationStatus.VALID) {
					info.actionsProcessed.add(ACTION.PIN);
					info.setPinCounter(0);
				} else if (res != null && res.getStatus() == AccountValidationStatus.ACCOUNT_BLOCKED
						|| info.getPinCounter() >= 3) {
					info.setPinCounter(3);
					info.actionsProcessed.add(ACTION.PIN);
				} else {
					info.setPinCounter(info.getPinCounter() + 1);
					errors.rejectValue("pin", "loginrsa.accountinfo.invalid", PLEASE_VERIFY_INPUT);
					return;
				}
			} else if (action == ValidateInfoBean.ACTION.EXPDATE) {
				String cvv = info.getCvv();
				String expMonth = info.getExpMonth();
				String expYear = info.getExpYear();
				boolean err = false;
				if (GenericValidator.isBlankOrNull(cvv) || !GenericValidator.matchRegexp(cvv, "^\\d{3}$")) {
					errors.rejectValue("cvv", "enrollment.cvv.invalid", "Invalid CVV code");
					err = true;
				}
				if (GenericValidator.isBlankOrNull(expMonth) || expMonth.equals("") || expMonth.equals("0")) {
					errors.rejectValue("expMonth", "enrollment.expMonth.invalid", "Invalid exp month");
					err = true;
				}
				if (GenericValidator.isBlankOrNull(expYear) || expYear.equals("")
						|| !GenericValidator.matchRegexp(expYear, "^\\d{4}$")) {
					errors.rejectValue("expYear", "enrollment.expYear.invalid", "Invalid exp year");
					err = true;
				}

				if (err)
					return;

				CardCVVAccountInformationValidationRequest req = new CardCVVAccountInformationValidationRequest(
						Utils.generateCustomerInteractionEvent(request), profile.getEnrollmentAccountNumber(),
						profile.getTaxId(), cvv, expMonth, expYear, "", false);
				res = customerService.validateCrediCardCVVAccountInfo(req);

				if (res.getStatus() == AccountValidationStatus.VALID) {
					info.actionsProcessed.add(ACTION.PIN);
					info.setPinCounter(0);
				} else if (res.getStatus() == AccountValidationStatus.ACCOUNT_BLOCKED || info.getPinCounter() >= 3) {
					info.setPinCounter(3);
					errors.rejectValue("cvv", "loginssds.cvv.blocked", "Account blocked.");
					info.actionsProcessed.add(ACTION.PIN);
				} else {
					info.setPinCounter(info.getPinCounter() + 1);
					errors.rejectValue("cvv", "loginrsa.accountinfo.invalid", PLEASE_VERIFY_INPUT);
					return;
				}
			}
			else if(action == ValidateInfoBean.ACTION.NONTRANSACT) {
						
				//Non-Transactional Account Validation				
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA459) && getNonTransactionalAccountSet().contains(info.getAccountType())) {			
					
					boolean error = !ValidationUtil.validAccountNumber(profile.getEnrollmentAccountSubtype(), info.getAccountnumber()) || !ValidationUtil.validLoanFields(info.getOriginationYear(), info.getPaymentAmount());
					
					if(error) {
						errors.rejectValue(ACCOUNT_NUMBER, "enrollment.accountnumber.invalid", "Invalid account Number");
						return;
					}
					
					NonTransAccountInformationValidationRequest ntReq = new NonTransAccountInformationValidationRequest(null, info.getAccountnumber(),profile.getTaxId(), 
							true, info.getOriginationYear(), info.getPaymentAmount(),paymentvalidationrange, true, info.getAccountType());
						res = customerService.validateNonTransAccountInfo(ntReq);
					
					
					if (res.getStatus() == AccountValidationStatus.VALID) {
						info.actionsProcessed.add(ACTION.NONTRANSACT);
						info.setPinCounter(0);
					} 
					else if (res.getStatus() == AccountValidationStatus.ACCOUNT_BLOCKED || info.getPinCounter() >= 2) {
						info.setPinCounter(3);
						errors.rejectValue(ACCOUNT_NUMBER, "loginssds.cvv.blocked", "Account blocked.");
						info.actionsProcessed.add(ACTION.NONTRANSACT);
						//Customer_Flags					
						if (!profile.hasFlag(CustomerFlagType.PASSWORD_RESET_BLOCK)) {
							customerService.blockCustomerResetPassword(profile, Utils.generateCustomerInteractionEvent(request), session.getDeviceId());
						}
						if(profile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)) {
							CustomerFacade.getInstance().removeCustomerFlag(profile, CustomerFlagType.MUST_CHANGE_USERNAME, session);
						}					
					} 
					else {
						info.setPinCounter(info.getPinCounter() + 1);
						errors.rejectValue(ACCOUNT_NUMBER, "enrollment.accountnumber.invalid", PLEASE_VERIFY_INPUT);
						return;
					}
				}
			}
		} 
		else if (page == 2) {

			availableUsername = true;
			if (action == ValidateInfoBean.ACTION.USERNAME || action == ValidateInfoBean.ACTION.PASSWORD) {
				String rePwd1 = "^[a-zA-Z0-9]+$";
				String username = info.getUsername();
				String password = info.getPassword();
				String passConf = info.getPasswordconfirm();
				// String rePwd =
				// "^(\\d+[a-zA-Z]+|[a-zA-Z]+\\d+)+(\\d+[a-zA-Z]*|[a-zA-Z]+\\d*)*$";
				boolean err = false;

				if (profile.getEnrollmentSource() == CustomerEnrollmentSource.BATCH
						|| profile.getEnrollmentSource() == CustomerEnrollmentSource.MIGRATION_BATCH
						|| profile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME))

				{
					// Username change is forced
					if (GenericValidator.isBlankOrNull(username) ||
					// !GenericValidator.maxLength(username, 20) ||
					// !GenericValidator.minLength(username, 6) ||
							!GenericValidator.matchRegexp(username, rePwd1)) {
						errors.rejectValue("username", "enrollment.username.invalid", "Please choose a valid Username");
						err = true;
					}// CIBP-1947 - valida y presenta mensaje de advertencia si
						// se escoge un username menor de 6 caracteres
					else if (!(GenericValidator.maxLength(username, 20) && GenericValidator.minLength(username, 6))) {
						errors.rejectValue("username", "enrollment.username.lengthInvalid", INVALID_USERNAME);
						err = true;
					}

					if (username.trim().equals(profile.getUsername().trim())) {
						//Fix by Alexis Gonzalez on MBCA-459
						availableUsername = false;
						errors.rejectValue("username", "loginssds.username.new", "Please choose a new Username");
						err = true;
					} else if (!customerService.isUsernameAvailable(username.trim())) { // CIBP-1947
						availableUsername = false;
						errors.rejectValue("username", ENROLLMENT_USERNAME_NOAVAILABILITY, new Object[] { username },
								INVALID);
						err = true;
					}
					if (!info.isCheckAvaliability()) {
						// si no hay error en username valida password
						if (GenericValidator.isBlankOrNull(password) || !GenericValidator.maxLength(password, 15)
								|| !GenericValidator.minLength(password, 8) || !Utils.isValidPassword(password)
								|| !password.equalsIgnoreCase(passConf)) {
							errors.rejectValue(PASSWORD_ERROR, ENROLLMENT_PWD_INVALID, INVALID_PASSWORD);
							err = true;
						} else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989) &&
								password.toLowerCase().contains(username.toLowerCase())) {
							errors.rejectValue(PASSWORD_ERROR, ENROLLMENT_PASSWORD_CONTAINS_USERNAME, PASSWORD_CONTAINS_USERNAME);
							err = true;
						} else {
							// FFEIC
							List<CustomerPassword> passwords = (List<CustomerPassword>) WebUtils.getSessionAttribute(
									request, PARAMETER_PASSWORDS);
							if (passwords == null) {
								passwords = customerService.getCustomerProfileService().getCustomerPasswords(profile);
								WebUtils.setSessionAttribute(request, PARAMETER_PASSWORDS, passwords);
							}
							// NO EQUALS ACTUAL PASS
							int originalSalt = profile.getPasswordSalt();
							String originalHash = profile.getPasswordHash();
							String newHash = PasswordHashUtil.generateSaltedHash(password, originalSalt);
							if (newHash.equals(originalHash)) {
								errors.rejectValue(PASSWORD_ERROR, CHANGEPASSWORD_PASSWORD_PREVIOUSLY_USED,
										INCORRECT_PASSWORD);
								err = true;
							}
							// NO EQUALS LAST PASS
							else {
								for (int i = 0; i < passwords.size(); i++) {
									String newPassOldHash = PasswordHashUtil.generateSaltedHash(password, passwords
											.get(i).getPasswordSalt());
									if (passwords.get(i).getPasswordHash().equals(newPassOldHash)) {
										errors.rejectValue(PASSWORD_ERROR, CHANGEPASSWORD_PASSWORD_PREVIOUSLY_USED,
												INCORRECT_PASSWORD);
										err = true;
										break;
									}
								}
							}
							// FIN FFEIC
						}
					}

					if (err || info.isCheckAvaliability())
						return;
					else {
						ModifyCustomerRequest req = new ModifyCustomerRequest(
								Utils.generateCustomerInteractionEvent(request), profile.getProfileId(), "", 0,
								username, 0, "", null, null, getDeviceRequest(request, session, info.getPm_fp()));

						req.setRsaLang("sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "ES" : "EN");

						ModifyCustomerProfileResponse res = customerService.modifyCustomerUsername(req);
						if (res.getStatus() != ResultStatus.SUCCESS) {
							// CIBP-1947
							availableUsername = false;
							errors.rejectValue("username", ENROLLMENT_USERNAME_NOAVAILABILITY,
									new Object[] { username }, INVALID);
							// errors.rejectValue("username", "",
							// "Invalid Username");
						} else {
							if (profile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)) {
								ModifyCustomerRequest reqs = new ModifyCustomerRequest(
										Utils.generateCustomerInteractionEvent(request), profile.getProfileId(),
										profile.getPasswordHash(), profile.getPasswordSalt(), "", 0, "",
										profile.getStatus(), null, session.getDeviceRequest());
								reqs.setCustomerFlagType(CustomerFlagType.MUST_CHANGE_USERNAME);
								customerService.resetCustomerFlag(reqs);
							}
							profile.setUsername(username);
							session.setUsername(username);
							info.actionsProcessed.add(ACTION.USERNAME);
						}

						int genereatedRandomSalt = PasswordHashUtil.genereateRandomSalt();
						String newhash = PasswordHashUtil.generateSaltedHash(password, genereatedRandomSalt);

						ModifyCustomerRequest req1 = new ModifyCustomerRequest(
								Utils.generateCustomerInteractionEvent(request), profile.getProfileId(),
								profile.getPasswordHash(), profile.getPasswordSalt(), username, genereatedRandomSalt,
								newhash, null, null, session.getDeviceRequest());
						ModifyCustomerProfileResponse res2 = customerService.modifyCustomerPassword(req1);
						if (res2.getStatus() != ResultStatus.SUCCESS) {
							errors.rejectValue(PASSWORD_ERROR, ENROLLMENT_PWD_INVALID, INVALID_PASSWORD);
						} else {
							info.actionsProcessed.add(ACTION.PASSWORD);
							if (profile.hasFlag(CustomerFlagType.MUST_CHANGE_PASSWORD)) {
								ModifyCustomerRequest reqs = new ModifyCustomerRequest(
										Utils.generateCustomerInteractionEvent(request), profile.getProfileId(),
										profile.getPasswordHash(), profile.getPasswordSalt(), "", 0, "",
										profile.getStatus(), null, session.getDeviceRequest());
								reqs.setCustomerFlagType(CustomerFlagType.MUST_CHANGE_PASSWORD);
								customerService.resetCustomerFlag(reqs);
							}
							// FFEIC
							List<CustomerPassword> passwords = (List<CustomerPassword>) WebUtils.getSessionAttribute(
									request, PARAMETER_PASSWORDS);
							// FIN FFEIC
							try {
								// jocampo FFEIC
								CustomerPassword cp = new CustomerPassword(profile.getProfileId(),
										profile.getPasswordHash(), new Date(), profile.getPasswordSalt());
								passwords.add(0, cp);
								List<CustomerPassword> passwordsDelete = null;
								if (passwords.size() > this.maxPasswordRecords) {
									passwordsDelete = passwords.subList(this.maxPasswordRecords, passwords.size());
									passwords = passwords.subList(0, this.maxPasswordRecords);
								}
								customerService.getCustomerProfileService().updateCustomerPasswords(passwords,
										passwordsDelete);
								// FIN FFEIC
							} catch (Exception e) {
								this.logger.error("Error updating customer passwords profile id : "
										+ profile.getProfileId());
							}
						}
					}
				} else {					
					if(inactive){
						username = profile.getUsername();
					}
					// Username change is not forced
					if (!(!GenericValidator.isBlankOrNull(username) && // GenericValidator.maxLength(username,
																		// 20,
																		// 0)
					// && GenericValidator.minLength(username, 6, 0) &&
					GenericValidator.matchRegexp(username, rePwd1))) {
						errors.rejectValue("username", "enrollment.username.invalid", INVALID_USERNAME);
						err = true;
					}// CIBP-1947 - valida y presenta mensaje de advertencia si
						// se escoge un username menor de 6 caracteres
					else if (!(GenericValidator.maxLength(username, 20, 0) && GenericValidator
							.minLength(username, 6, 0))) {
						errors.rejectValue("username", "enrollment.username.lengthInvalid", INVALID_USERNAME);
						err = true;
					}

					if(!inactive){
						// CIBP-1947
						if (!customerService.isUsernameAvailable(username.trim())) {
							availableUsername = false;
							errors.rejectValue("username", ENROLLMENT_USERNAME_NOAVAILABILITY, new Object[] { username },
									INVALID);
							err = true;
						}
					}
					if (!info.isCheckAvaliability()) {
						if (GenericValidator.isBlankOrNull(password) || !GenericValidator.maxLength(password, 15, 0)
								|| !GenericValidator.minLength(password, 8, 0) || !Utils.isValidPassword(password)
								|| !password.equalsIgnoreCase(passConf)) {
							errors.rejectValue(PASSWORD_ERROR, ENROLLMENT_PWD_INVALID, INVALID_PASSWORD);
							err = true;
						} else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989) &&
								password.toLowerCase().contains(username.toLowerCase())) {
							errors.rejectValue(PASSWORD_ERROR, ENROLLMENT_PASSWORD_CONTAINS_USERNAME, PASSWORD_CONTAINS_USERNAME);
							err = true;
						} else {
							// FFEIC
							List<CustomerPassword> passwords = (List<CustomerPassword>) WebUtils.getSessionAttribute(
									request, PARAMETER_PASSWORDS);
							if (passwords == null) {
								passwords = customerService.getCustomerProfileService().getCustomerPasswords(profile);
								WebUtils.setSessionAttribute(request, PARAMETER_PASSWORDS, passwords);
							}
							// NO EQUALS ACTUAL PASS
							int originalSalt = profile.getPasswordSalt();
							String originalHash = profile.getPasswordHash();
							String newHash = PasswordHashUtil.generateSaltedHash(password, originalSalt);
							if (newHash.equals(originalHash)) {
								errors.rejectValue(PASSWORD_ERROR, CHANGEPASSWORD_PASSWORD_PREVIOUSLY_USED,
										INCORRECT_PASSWORD);
								err = true;
							}
							// NO EQUALS LAST PASS
							else {
								for (int i = 0; i < passwords.size(); i++) {
									String newPassOldHash = PasswordHashUtil.generateSaltedHash(password, passwords
											.get(i).getPasswordSalt());
									if (passwords.get(i).getPasswordHash().equals(newPassOldHash)) {
										errors.rejectValue(PASSWORD_ERROR, CHANGEPASSWORD_PASSWORD_PREVIOUSLY_USED,
												INCORRECT_PASSWORD);
										err = true;
										break;
									}
								}
							}
							// FIN FFEIC
						}
					}
					if (err || info.isCheckAvaliability())
						return;

					else {
						ModifyCustomerRequest req = new ModifyCustomerRequest(
								Utils.generateCustomerInteractionEvent(request), profile.getProfileId(), "", 0,
								username, 0, "", null, null, getDeviceRequest(request, session, info.getPm_fp()));

						req.setRsaLang("sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "ES" : "EN");

						if(!inactive){
							ModifyCustomerProfileResponse res = customerService.modifyCustomerUsername(req);
							if (res.getStatus() != ResultStatus.SUCCESS) {
								// CIBP-1947
								availableUsername = false;
								errors.rejectValue("username", ENROLLMENT_USERNAME_NOAVAILABILITY,
										new Object[] { username }, INVALID);
								// errors.rejectValue("username", "",
								// "Invalid Username");
							} else {
								profile.setUsername(username);
								session.setUsername(username);
								info.actionsProcessed.add(ACTION.USERNAME);
								if (profile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)) {
									ModifyCustomerRequest reqs = new ModifyCustomerRequest(
											Utils.generateCustomerInteractionEvent(request), profile.getProfileId(),
											profile.getPasswordHash(), profile.getPasswordSalt(), "", 0, "",
											profile.getStatus(), null, session.getDeviceRequest());
									reqs.setCustomerFlagType(CustomerFlagType.MUST_CHANGE_USERNAME);
									customerService.resetCustomerFlag(reqs);
								}
							}
						}				

						int genereatedRandomSalt = PasswordHashUtil.genereateRandomSalt();
						String newhash = PasswordHashUtil.generateSaltedHash(password, genereatedRandomSalt);

						ModifyCustomerRequest req1 = new ModifyCustomerRequest(
								Utils.generateCustomerInteractionEvent(request), profile.getProfileId(),
								profile.getPasswordHash(), profile.getPasswordSalt(), username, genereatedRandomSalt,
								newhash, null, null, session.getDeviceRequest());
						ModifyCustomerProfileResponse res2 = customerService.modifyCustomerPassword(req1);
						if (res2.getStatus() != ResultStatus.SUCCESS) {
							errors.rejectValue(PASSWORD_ERROR, ENROLLMENT_PWD_INVALID, INVALID_PASSWORD);
						} else {
							info.actionsProcessed.add(ACTION.PASSWORD);
							if(inactive){
								session.setCustomerProfile(res2.getCustomerProfile());
								CustomerSignonStats css = res2.getCustomerProfile().getSignonStats();
								css.setLastRegainAccessPassChange(Calendar.getInstance().getTime());
								CustomerFacade.getInstance().updateCustomerSignonStats(css, session);
							}
							WebUtils.setSessionAttribute(request, USER_SESSION, session);
							if (profile.hasFlag(CustomerFlagType.MUST_CHANGE_PASSWORD)) {
								ModifyCustomerRequest reqs = new ModifyCustomerRequest(
										Utils.generateCustomerInteractionEvent(request), profile.getProfileId(),
										profile.getPasswordHash(), profile.getPasswordSalt(), "", 0, "",
										profile.getStatus(), null, session.getDeviceRequest());
								reqs.setCustomerFlagType(CustomerFlagType.MUST_CHANGE_PASSWORD);
								customerService.resetCustomerFlag(reqs);
							}
							// FFEIC
							List<CustomerPassword> passwords = (List<CustomerPassword>) WebUtils.getSessionAttribute(
									request, PARAMETER_PASSWORDS);
							// FIN FFEIC
							try {
								// jocampo FFEIC
								CustomerPassword cp = new CustomerPassword(profile.getProfileId(),
										profile.getPasswordHash(), new Date(), profile.getPasswordSalt());
								passwords.add(0, cp);
								List<CustomerPassword> passwordsDelete = null;
								if (passwords.size() > this.maxPasswordRecords) {
									passwordsDelete = passwords.subList(this.maxPasswordRecords, passwords.size());
									passwords = passwords.subList(0, this.maxPasswordRecords);
								}
								customerService.getCustomerProfileService().updateCustomerPasswords(passwords,
										passwordsDelete);
								// FIN FFEIC
							} catch (Exception e) {
								this.logger.error("Error updating customer passwords profile id : "
										+ profile.getProfileId());
							}
						}

					}
				}

			}
		}
		if (page == 3) {

			if (action == ValidateInfoBean.ACTION.QUESTIONS) {

				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "question1", "", "Required");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "question2", "", "Required");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "question3", "", "Required");

				String a1 = info.getAnswer1();
				String a2 = info.getAnswer2();
				String a3 = info.getAnswer3();
				String a1c = info.getAnswer1confirm();
				String a2c = info.getAnswer2confirm();
				String a3c = info.getAnswer3confirm();

				String computerType = info.getComputerType();

				String re = "^\\w+(\\s{1}\\w+)*$";
				if (GenericValidator.isBlankOrNull(a1) || !GenericValidator.matchRegexp(a1, re)) {
					errors.rejectValue("answer1", ENROLLMENT_ANSWER_INVALID, "Please enter a valid answer");
				} else if (!GenericValidator.isBlankOrNull(a1) && !a1.equals(a1c)) {
					errors.rejectValue("answer1confirm", ENROLLMENT_ANSWERCONFIRM_NOTMATCH,
							ANSWERS_VALUES_MUST_MATCH);
				}
				if (GenericValidator.isBlankOrNull(a2) || !GenericValidator.matchRegexp(a2, re)) {
					errors.rejectValue("answer2", ENROLLMENT_ANSWER_INVALID, "Please enter a valid answer");
				} else if (!GenericValidator.isBlankOrNull(a2) && !a2.equals(a2c)) {
					errors.rejectValue("answer2confirm", ENROLLMENT_ANSWERCONFIRM_NOTMATCH,
							ANSWERS_VALUES_MUST_MATCH);
				}
				if (GenericValidator.isBlankOrNull(a3) || !GenericValidator.matchRegexp(a3, re)) {
					errors.rejectValue("answer3", ENROLLMENT_ANSWER_INVALID, "Please enter a valid answer.");
				} else if (!GenericValidator.isBlankOrNull(a3) && !a3.equals(a3c)) {
					errors.rejectValue("answer3confirm", ENROLLMENT_ANSWERCONFIRM_NOTMATCH,
							ANSWERS_VALUES_MUST_MATCH);
				}
				if (!"private".equals(computerType) && !"public".equals(computerType)) {
					errors.rejectValue("computerType", "enrollment.computerType.required",
							"Please select a computer type you are using");
				}
			}
		}

		else if (page == 4) {

			if (GenericValidator.isBlankOrNull(info.getAgreement()) || !info.getAgreement().equalsIgnoreCase(AGREE)) {
				errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
			} else {

				String computerType = info.getComputerType();
				boolean saveDevice = info.isSaveDevice();
				if ("private".equals(computerType)) {
					saveDevice = true;
				}

				EnrollmentInformationinRSARequest req = new EnrollmentInformationinRSARequest(
						Utils.generateCustomerInteractionEvent(request), profile.getUsername(),
						profile.getEmail(), saveDevice,getDeviceRequest(request,session,info.getPm_fp()));
				req.setQuestionsAndAnswer( info.getQuestion1(),
						info.getQuestion2(), info.getQuestion3(), info.getAnswer1(), info.getAnswer2(),info.getAnswer3());
				
				if(inactive){
					RsaQuestionsUpdateResponse updateRSAresponse = customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(req);
					boolean success = false;
					if (ResultStatus.SUCCESS.equals(updateRSAresponse.getStatus())) {
						success = true;						
					}
					else{
						ModifyCustomerProfileResponse enrollRSAresponse = session.getCustomerService().getSignOnCustomerServiceModule()
								.enrollRSA(req);
						if (ResultStatus.SUCCESS.equals(enrollRSAresponse.getStatus())) {
							success = true;
						}
					}
					if(success){
						String header = request.getHeader("user-agent");

						String customerDevices = "";
						if(header != null) {			
							customerDevices = DeviceUtils.getCustomerDevice(header).toString();
						}
						
						String passwordHash = session.getCustomerProfile().getPasswordHash();
						ValidateLoginInfoRequest loginRequest = 
								new ValidateLoginInfoRequest(Utils.generateCustomerInteractionEvent(request), session.getCustomerProfile().getUsername(), 
										passwordHash, session.getDeviceRequest(), request.getRemoteAddr(), customerDevices);
						
						loginRequest.setCustomerDevices(customerDevices);
						
						ModifyCustomerProfileResponse responseAnswer = session.getCustomerService().validatePassword(loginRequest);
						if (ResultStatus.SUCCESS.equals(responseAnswer.getStatus())) {
							info.actionsProcessed.add(ACTION.QUESTIONS);
							info.actionsProcessed.add(ACTION.CONTRACT);
							session.getTasksList().clear();
							session.getDeviceRequest().setDeviceTokenCookie(updateRSAresponse.getDeviceTokenCookie());
							session.getDeviceRequest().setDeviceTokenFSO(updateRSAresponse.getDeviceTokenCookie());
							session.getTasksList().add(ASK_FOR.SESSION_INIT);
							session.setLanguage(Utils.getRSALanguage(request));
							WebUtils.setSessionAttribute(request, USER_SESSION, session);
							WebUtils.setSessionAttribute(request, INACTIVE, null);
						}
					}
				}
				else{
					ModifyCustomerProfileResponse enrollRSAresponse = session.getCustomerService().getSignOnCustomerServiceModule()
							.enrollRSA(req);
					if (ResultStatus.SUCCESS.equals(enrollRSAresponse.getStatus())) {
						info.actionsProcessed.add(ACTION.QUESTIONS);
						info.actionsProcessed.add(ACTION.CONTRACT);
					}
				}
			}

		}
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION);
		CustomerProfile profile = session.getCustomerProfile();
		Map<String, Object> model = new HashMap<>();
		ValidateInfoBean info = (ValidateInfoBean) command;
		model.put("customername", profile.getFirstName());
	
		WebUtils.setSessionAttribute(request, Utils.DEEP_LINK, null);

		CustomerServices customerService = session.getCustomerService();

		if (profile.getEnrollmentSource() == CustomerEnrollmentSource.SSDS) {
			SimpleDateFormat sfor = new SimpleDateFormat("yyyyMMdd");
			if (sfor.format(profile.getCreationDate()).equals(sfor.format(Calendar.getInstance().getTime()))) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.reject("",
							"Your Internet Banking account has not been activated yet. You will be able to log in starting tomorrow.");
				else
					errors.reject("",
							"Tu acceso a Banca por Internet todav&iacute;a no se ha activado. Podr&aacute;s conectarte a partir de ma&ntilde;ana..");

				model.put("page", "0");
				return model;
			}
		}

		String type = profile.getEnrollmentAccountSubtype();

		List<ASK_FOR> tasksList = session.getTasksList();
		for (ASK_FOR thing : tasksList) {
			switch (thing) {
			case SSDS_FORCED_LOGIN:
			case RSA_ENROLL_USERNAME:
				model.put("validateemail", true);

				if (profile.getEnrollmentAccountSubtype().equals("PFC")
						|| profile.getEnrollmentAccountSubtype().equals("IDA")
						|| profile.getEnrollmentAccountSubtype().equals("AMEX")) {
					model.put("validatepin", true);
				} else if (profile.getEnrollmentAccountSubtype().equals("CCA")) {
					if (profile.getEnrollmentAccountNumber().startsWith("0377")
							|| (profile.getEnrollmentAccountNumber().length() == 15 && profile
									.getEnrollmentAccountNumber().startsWith("377"))) {
						type = "AMEX";
						model.put("validatepin", true);
					} else
						model.put("validatecvvexpiration", true);
				} else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA459)
						&& getNonTransactionalAccountSet().contains(profile.getEnrollmentAccountSubtype())) {
					model.put("isNonTransactional", true);					
				}
				
				info.setAccountType(type);

				if (profile.getEnrollmentSource() == CustomerEnrollmentSource.BATCH
						|| profile.getEnrollmentSource() == CustomerEnrollmentSource.MIGRATION_BATCH
						|| profile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)) {
					model.put("forcedusername", true);
				}
				Boolean inactive = (Boolean)WebUtils.getSessionAttribute(request, INACTIVE);
				if(inactive != null){
					model.put("active", false);
				}
				else{
					model.put("active", true);
				}
				
				if (profile.getEnrollmentSource() == CustomerEnrollmentSource.BATCH
						|| profile.getEnrollmentSource() == CustomerEnrollmentSource.MIGRATION_BATCH
						|| profile.getEnrollmentSource() == CustomerEnrollmentSource.SSDS
						|| profile.getEnrollmentSource() == CustomerEnrollmentSource.MIGRATION_SSDS
						|| profile.hasFlag(CustomerFlagType.MUST_CHANGE_PASSWORD)) {
					model.put("forcedpassword", true);
				}
				model.put("changepassword", true);

				model.put("selectquestions", true);
				model.put("showcontract", true);
				break;
			default:
				break;
			}
		}

		// CIBP-1947/validacion de username disponibles en inscripcion (lguzman)
		if (page == 2) {
			model.put(VALID_AVAILABLE_USERNAME, false);
			if (!GenericValidator.isBlankOrNull(info.getUsername())
					&& GenericValidator.maxLength(info.getUsername(), 20)
					&& GenericValidator.minLength(info.getUsername(), 6)
					&& GenericValidator.matchRegexp(info.getUsername(), "^[a-zA-Z0-9]+$")) {

				if (!availableUsername) { // Valida en onBindAndValidate
					int auxLenghtUsername = info.getUsername().trim().length();
					int tamNumInUsername = 0;

					// Identifica cuantos valores numericos hay a la derecha del
					// username
					for (int i = 0; i < info.getUsername().trim().length(); i++) {
						if (info.getUsername().trim().substring(auxLenghtUsername - 1, auxLenghtUsername)
								.matches("[0-9]*")) {
							tamNumInUsername++;
							auxLenghtUsername--;
						} else {
							break;
						}
					}
					// crea las 4 opciones de username
					auxLenghtUsername = info.getUsername().trim().length();
					int numInUsername = 0;
					if (tamNumInUsername != 0) // si encontro numero en la parte
												// derecha del username lo
												// extrae
						numInUsername = Integer.parseInt(info.getUsername().trim()
								.substring(auxLenghtUsername - tamNumInUsername, auxLenghtUsername));
					String usernameWithOutNum = info.getUsername().trim()
							.substring(0, auxLenghtUsername - tamNumInUsername);
					Map<Object, String> usernamesDispo = new LinkedHashMap<Object, String>();
					numInUsername++;// siguiente valor a probar
					
					for(int opciones = 0; opciones < 4; opciones++) {
						// valida que no exista en la base de datos
						// CUSTOMER_PROFILE/USERNAME_RESERVATION
						if (customerService.isUsernameAvailable(usernameWithOutNum + numInUsername)) {
							usernamesDispo.put(usernameWithOutNum + numInUsername, usernameWithOutNum + numInUsername);
						}
						numInUsername++;
					}

					model.put("availableUsername", false);
					model.put(VALID_AVAILABLE_USERNAME, true);
					model.put("usernames", usernamesDispo);
				} else {
					model.put("availableUsername", true);
					model.put(VALID_AVAILABLE_USERNAME, true);
				}
			}

		}

		model.put("page", Integer.toString(page));
		if (info.getPinCounter() >= 3 ||(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA459) && profile.hasFlag(CustomerFlagType.PASSWORD_RESET_BLOCK))) {
			model.put("blocked", true);
		}

		Map<String, String> agree = new HashMap<String, String>();
		agree.put(AGREE, "");
		model.put(AGREE, agree);

		// poner reference para cvv y exp date
		// expYears
		Map<Object, String> expYears = new LinkedHashMap<Object, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			expYears.put("", "A&ntilde;os:");
		else
			expYears.put("", "Year:");
		SimpleDateFormat expdf = new SimpleDateFormat("yyyy");
		Calendar expcal = GregorianCalendar.getInstance();
		for (int i = 0; i <= 8; i++) {
			String year = expdf.format(expcal.getTime());
			expYears.put(Integer.parseInt(year), year);
			expcal.add(Calendar.YEAR, 1);
		}
		model.put("expYears", expYears);

		// expMonths
		Map<Object, String> expMonths = new LinkedHashMap<Object, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			expMonths.put("", "Mes:");
		else
			expMonths.put("", "Month:");
		expdf = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
		expcal = GregorianCalendar.getInstance();
		expcal.set(Calendar.MONTH, 0);
		for (int i = 1; i <= 12; i++) {
			String month = expdf.format(expcal.getTime());
			expMonths.put(i, month);
			expcal.add(Calendar.MONTH, 1);

		}
		model.put("expMonths", expMonths);

		// BIRTHDAY
		// months
		Map<Object, String> months = new LinkedHashMap<Object, String>();
		SimpleDateFormat df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.MONTH, 0);
		for (int i = 0; i < 12; i++) {
			String month = df.format(cal.getTime());
			months.put(i, month);
			cal.add(Calendar.MONTH, 1);
		}
		model.put("months", months);

		// days
		Map<Object, String> days = new LinkedHashMap<Object, String>();
		df = new SimpleDateFormat("d");
		cal.set(Calendar.DAY_OF_MONTH, 1);
		for (int i = 1; i <= 31; i++) {
			String day = df.format(cal.getTime());
			days.put(i, day);
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		model.put("days", days);

		// years
		Map<Object, String> years = new LinkedHashMap<Object, String>();
		df = new SimpleDateFormat("yyyy");
		cal = GregorianCalendar.getInstance();
		for (int i = 0; i <= 100; i++) {
			String year = df.format(cal.getTime());
			years.put(Integer.parseInt(year), year);
			cal.add(Calendar.YEAR, -1);
		}
		model.put("years", years);

		// Questions
		if (page >= 3) {

			Map<String, String> questions = new LinkedHashMap<String, String>();
			String selectquestiontext = "Select a question:";
			if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
				selectquestiontext = "Seleccione una pregunta:";
			questions.put("", selectquestiontext);
			model.put("questionsLabel", selectquestiontext);

			CustomerInteractionEvent generateCustomerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
			RSAActionRequest actionrequest = new RSAActionRequest(generateCustomerInteractionEvent,
					profile.getUsername(), "", profile.getEmail(), 0, false, session.getDeviceRequest());
			actionrequest.setRsaLang("sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "ES" : "EN");
			QuestionSelectionResponse questionResponse = customerService.rsaGetQuestionsSelection(actionrequest);
			if (questionResponse.getStatusResult().equals(ResultStatus.SUCCESS)) {
				Map<Object, Object> q = new HashMap<Object, Object>();
				q.put("", selectquestiontext);
				q.putAll(questionResponse.getQuestionMap1());
				model.put("questions1", q);

				Map<Object, Object> q2 = new HashMap<Object, Object>();
				q2.put("", selectquestiontext);
				q2.putAll(questionResponse.getQuestionMap2());
				questions = new LinkedHashMap<String, String>();
				questions.put("", selectquestiontext);
				model.put("questions2", q2);

				Map<Object, Object> q3 = new HashMap<Object, Object>();
				q3.put("", selectquestiontext);
				q3.putAll(questionResponse.getQuestionMap3());
				questions = new LinkedHashMap<String, String>();
				questions.put("", selectquestiontext);
				model.put("questions3", q3);
			}

		}

		Cookie cookie = WebUtils.getCookie(request, PM_DATA) != null ? WebUtils.getCookie(request, PM_DATA)
				: new Cookie(PM_DATA, "");

		if (cookie != null) {
			cookie.setPath("/");
			if (session.getDeviceRequest() != null)
				cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());

			model.put("devicetoken", cookie.getValue());
		}
		
		model.put("MBSFE1531", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1531));

		return model;
	}

	public PinEncryptionTool getPinTool() {
		return pinTool;
	}

	public void setPinTool(PinEncryptionTool pinTool) {
		this.pinTool = pinTool;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.ENROLLMENT;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	private DeviceRequest getDeviceRequest(HttpServletRequest request, UserSession session, String devicePrint) {
		Cookie cookie = WebUtils.getCookie(request, PM_DATA) != null ? WebUtils.getCookie(request, PM_DATA)
				: new Cookie(PM_DATA, "");
		cookie.setMaxAge(31536000);
		cookie.setPath("/");

		DeviceRequest deviceRequest = new DeviceRequest();
		deviceRequest.setDevicePrint(devicePrint);
		deviceRequest.setIpAddress(request.getRemoteAddr());
		deviceRequest.setDeviceTokenCookie(cookie.getValue());
		deviceRequest.setDeviceTokenFSO(cookie.getValue());

		deviceRequest.setHttpAccept(request.getHeader("Accept"));
		deviceRequest.setHttpAcceptChars(request.getHeader("Accept-Charset"));
		deviceRequest.setHttpAcceptEncoding(request.getHeader("Accept-Encoding"));
		deviceRequest.setHttpAcceptLanguage(request.getHeader("Accept-Language"));
		deviceRequest.setHttpReferrer(request.getHeader("Referer"));
		deviceRequest.setUserAgent(request.getHeader("User-Agent"));

		session.setDeviceRequest(deviceRequest);

		return deviceRequest;
	}
	
	@SuppressWarnings("unchecked")
	private List<String> getNonTransactionalAccountSet() {
		String[] ntAccountsArray = getNonTransProductFlow().split(",");
		return Arrays.asList(ntAccountsArray);
	}

	public Integer getMaxPasswordRecords() {
		return maxPasswordRecords;
	}

	public void setMaxPasswordRecords(Integer maxPasswordRecords) {
		this.maxPasswordRecords = maxPasswordRecords;
	}
	
	// MBSFE-691
	public String getInvalidEmailDomains() {
		return invalidEmailDomains;
	}

	public void setInvalidEmailDomains(String invalidEmailDomains) {
		this.invalidEmailDomains = invalidEmailDomains;
	}
	// END MBSFE-691
	
	public String getPaymentvalidationrange() {
		return paymentvalidationrange;
	}

	public void setPaymentvalidationrange(String paymentvalidationrange) {
		this.paymentvalidationrange = paymentvalidationrange;
	}

	public String getNonTransProductFlow() {
		return nonTransProductFlow;
	}

	public void setNonTransProductFlow(String nonTransProductFlow) {
		this.nonTransProductFlow = nonTransProductFlow;
	}
	
	
}
