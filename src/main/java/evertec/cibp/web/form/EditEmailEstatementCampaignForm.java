package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyAllCustomerEmailsRequest;
import evertec.cibp.core.services.requests.ModifyCustomerEmailRequest;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.EditEmailEstatementCampaignBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

public class EditEmailEstatementCampaignForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;
	
	// MBSFE-691
	private String invalidEmailDomains;
	// END MBSFE-691

	public EditEmailEstatementCampaignForm() {
		super();
		setCommandClass(EditEmailEstatementCampaignBean.class);
		setCommandName("editemailestatementcampaign");
		setFormView("editemailestatementcampaign");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new HashMap<>();
		EditEmailEstatementCampaignBean dude = (EditEmailEstatementCampaignBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile customer = userSession.getCustomerProfile();

		try {
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)){
				
				ModifyAllCustomerEmailsRequest macer = new ModifyAllCustomerEmailsRequest(
						Utils.generateCustomerInteractionEvent(request), customer, 
						customerService, userSession.getAccounts(), userSession.getBankingSession(), 
						userSession.getEbpp2UserId(), userSession.getPreferredLanguage(), 
						dude.getEmail()); 
				customer = CustomerUtils.modifyAllEmails(macer);
				userSession.setCustomerProfile(customer);
				
				model.put("confirm", true);
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Email has been changed.");
				else
					userSession.setStatusmessage("E-mail ha sido cambiado.");
				
			}else{
			// The user has an EBPP2 ID
				if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {
					// Update the customer email
					customerService.updateEbpp2User(userSession.getEbpp2UserId(), dude.getEmail(), customer);
					ModifyCustomerEmailRequest emailmod = new ModifyCustomerEmailRequest(
							Utils.generateCustomerInteractionEvent(request), customer, dude.getEmail());
	
					customer = customerService.modifyCustomerEmail(emailmod);
					userSession.setCustomerProfile(customer);
				} else {
					ModifyCustomerEmailRequest emailmod = new ModifyCustomerEmailRequest(
							Utils.generateCustomerInteractionEvent(request), userSession.getCustomerProfile(),
							dude.getEmail());
					customerService.modifyCustomerEmail(emailmod);
					userSession.setCustomerProfile(userSession.getCustomerService().modifyCustomerEmail(emailmod));
				}
	
				model.put("confirm", true);
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Email has been changed.");
				else
					userSession.setStatusmessage("E-mail ha sido cambiado.");
			}
		} catch (Exception e) {
			error.rejectValue("email", "", "Error!");
			model.put("confirm", true);
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Unable to change email.");
			else
				userSession.setStatusmessage("Email no pudo ser cambiado.");
		}

		return showForm(request, response, error, model);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		EditEmailEstatementCampaignBean dude = (EditEmailEstatementCampaignBean) command;
		
		// MBSFE-691
		String[] domain = dude.getEmail().split("@");
		// END MBSFE-69
		
		if (GenericValidator.isBlankOrNull(dude.getEmail()))
			errors.rejectValue("email", "editemail.required", "Required");
		else if (!GenericValidator.isEmail(dude.getEmail()))
			errors.rejectValue("email", "editemail.invalid", "Invalid format");
		else if (!dude.getEmail().equals(dude.getEmailconfirm()))
			errors.rejectValue("emailconfirm", "editemail.mustmatch", "Fields must match");
		
		// MBSFE-691
		else if (Utils.haveType(this.invalidEmailDomains, domain[1])
				&& StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE691)) {
			errors.rejectValue("email", "email.invaliddomain", "Your e-mail address is incorrect. Please try again.");
		}
		// END MBSFE-691
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	private boolean isEbppAvailable() {
		boolean ebppStatus = false;

		if (this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
				.equals(EntitlementStatus.ENABLED.toString())) {
			ebppStatus = true;
		} else {
			ebppStatus = false;
		}

		return ebppStatus;
	}
	
	// MBSFE-691
	public String getInvalidEmailDomains() {
		return invalidEmailDomains;
	}

	public void setInvalidEmailDomains(String invalidEmailDomains) {
		this.invalidEmailDomains = invalidEmailDomains;
	}
	// END MBSFE-691

}
