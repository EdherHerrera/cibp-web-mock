package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.ActivationCardAccountResponse;
import evertec.cibp.core.services.responses.ActivationCardAccountResponse.Status;
import evertec.cibp.encryptedPin.PinEncryptionTool;
import evertec.cibp.web.form.bean.AssignNewPinBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

/**
 * Class that manages input for the AssignNewPin form functionality MBSFE-1342
 */
public class AssignNewPinForm extends CancellableFormController {

	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	private static final String VIEW = "assignNewPin";
	private static final String MESSAGE = "message";
	private static final String ACTIVATED = "activated";
	private static final String NOT_ACTIVATED = "notactivated";

	private PinEncryptionTool pinTool;

	/**
	 * Constructor for Assign New Pin form View and bean definition
	 */
	public AssignNewPinForm() {
		super();
		setCommandClass(AssignNewPinBean.class);
		setCommandName(VIEW);
		setFormView(VIEW);
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		AssignNewPinBean bean = (AssignNewPinBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		//MBSE 1560
		CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request);		
		List<TVFrontendAccount> plastics = userSession.getCardPlasticAccounts();
		String athnum = bean.getPlastic();
		TVFrontendAccount frontendAccount;
		frontendAccount = Utils.getAccountById(athnum, plastics);
		athnum = frontendAccount.getAccountNumber();

		String pin = bean.getPin();
		String encryptedPin = pinTool.encrypt(pin);

		boolean validation;

		ActivationCardAccountResponse activationResponse = customerService.assignNewPlasticPin(customer,
				userSession.getBankingSession(), athnum, encryptedPin,event);

		if (activationResponse != null) {
			if (activationResponse.getResponseStatus() == Status.ACTIVATED)
				validation = true;
			else
				validation = false;
		} else
			validation = false;

		if (athnum.startsWith("4549")) {
			if (!validation) {
				model.put(NOT_ACTIVATED, true);

				if (activationResponse != null && activationResponse.getResponseStatus() == Status.CARD_NOT_FOUND) {
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						model.put(MESSAGE,
								"Your request to assign a personal identification number &#40;PIN&#41; was not authorized.");
					} else {
						model.put(MESSAGE,
								"Tu solicitud de asignar un n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; no fue autorizada.");
					}
				} else if (activationResponse != null
						&& activationResponse.getResponseStatus() == Status.PIN_ALREADY_VALIDATED) {
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						model.put(MESSAGE,
								"<span class='pinAlreadyValidatedMessage'>This International ATH Visa card already has a personal identification number &#40;PIN&#41; assigned.&nbsp;"
										+ "If you want to change your PIN, you can do it in the <span style='font-weight: bold'>Change ATH Card PIN</span> section.&nbsp;"
										+ "If you don&#39;t remember your PIN, visit any of our branches.</span>");
					} else {
						model.put(MESSAGE,
								"<span class='pinAlreadyValidatedMessage'>La tarjeta tiene n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; asignado anteriormente.&nbsp;"
										+ "Si deseas cambiar tu PIN puedes hacerlo en la sección <span style='font-weight: bold'>Cambia el PIN de tu ATH</span>.&nbsp;"
										+ "Si no recuerdas tu PIN, visita cualquiera de nuestras sucursales.</span>");
					}
				} else {
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						model.put(MESSAGE,
								"Your request to activate your International ATH Visa card cannot be processed at this moment.");
					} else {
						model.put(MESSAGE,
								"Tu solicitud para activar tu tarjeta ATH Internacional VISA no pudo ser procesada en este momento.");
					}
				}
			} else {
				model.put(ACTIVATED, true);
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
					model.put(MESSAGE,
							"This International ATH Visa card has been successfully activated and your personal identification number &#40;PIN&#41; has been assigned.");
				} else {
					model.put(MESSAGE,
							"Tu tarjeta ATH Internacional Visa ha sido activada y su n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; fue asignado exitosamente.");
				}

			}
		} else {
			if (!validation) {
				model.put(NOT_ACTIVATED, true);

				if (activationResponse != null && activationResponse.getResponseStatus() == Status.CARD_NOT_FOUND) {
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						model.put(MESSAGE,
								"Your request to assign a personal identification number &#40;PIN&#41; was not authorized.");
					} else {
						model.put(MESSAGE,
								"Tu solicitud de asignar un n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; no fue autorizada.");
					}
				} else if (activationResponse != null
						&& activationResponse.getResponseStatus() == Status.PIN_ALREADY_VALIDATED) {
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						model.put(MESSAGE, "<span class='pinAlreadyValidatedMessage'>This ATH card already has a personal identification number &#40;PIN&#41; assigned.&nbsp;"
								+ "If you want to change your PIN, you can do it in the <span style='font-weight: bold'>Change ATH Card PIN</span> section.&nbsp;"
								+ "If you don&#39;t remember your PIN, visit any of our branches.</span>");
					} else {
						model.put(MESSAGE, "<span class='pinAlreadyValidatedMessage'>La tarjeta tiene n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; asignado anteriormente.&nbsp;"
								+ "Si deseas cambiar tu PIN puedes hacerlo en la sección <span style='font-weight: bold'>Cambia el PIN de tu ATH</span>.&nbsp;"
								+ "Si no recuerdas tu PIN, visita cualquiera de nuestras sucursales.</span>");
					}
				} else {
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						model.put(MESSAGE,
								"Your request to assign a personal identification number &#40;PIN&#41; cannot be processed at this moment.");
					} else {
						model.put(MESSAGE,
								"Tu solicitud de asignar un n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; no pudo ser procesada en este momento.");
					}
				}
			} else {
				model.put(ACTIVATED, true);
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
					model.put(MESSAGE,
							"Your personal identification number &#40;PIN&#41; was successfully assigned and is ready for use.");
				} else {
					model.put(MESSAGE,
							"Tu n&uacute;mero de identificaci&oacute;n personal &#40;PIN&#41; fue asignado exitosamente y est&aacute; lista para utilizarse.");
				}
			}
		}

		return showForm(request, error, VIEW, model);

	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {

		AssignNewPinBean bean = (AssignNewPinBean) command;

		if (bean.getPlastic().equals("")) {
			errors.rejectValue("plastic", "activateath.validation.plastic", "Select a valid account");
			bean.setFormError(true);
		}
		if (!GenericValidator.matchRegexp(bean.getPin(), "\\d{4}")) {
			errors.rejectValue("pin", "new.pin.pin.invalid", "Please enter a valid PIN");
			bean.setFormError(true);
		}
		if (GenericValidator.isBlankOrNull(bean.getPinconfirm()) || !bean.getPin().equals(bean.getPinconfirm())) {
			errors.rejectValue("pinconfirm", "new.pin.pinconfirmation.invalid",
					"Please enter a valid PIN confirmation");
			bean.setFormError(true);
		}
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<TVFrontendAccount> frontendAccounts = userSession.getCardPlasticAccounts();
		AccountMaskUtil maskUtil = new AccountMaskUtil();
		Map<Object, Object> plasticsMap = new LinkedHashMap<>();

		if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
			plasticsMap.put("", "Select ATH<sup>&reg;</sup> Card");
		} else {
			plasticsMap.put("", "Selecciona una Tarjeta ATH<sup>&reg;</sup>");
		}

		// Carga las ATH en el VM
		for (Iterator<TVFrontendAccount> iterator = frontendAccounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount frontendAccount = (TVFrontendAccount) iterator.next();

			CardPlasticAccountRecord record = (CardPlasticAccountRecord) frontendAccount.getTVAccount().getRecords();
			if (record != null) {

				boolean isActive = true;

				// If ATH Card is an ATH International with INACTIVE status should not pass
				if (frontendAccount.getAccountNumber().startsWith("4549")
						&& record.getStatus().trim().equals("INACTIVE")) {
					isActive = false;
				}

				if (isActive) {
					TVFrontendAccount depositAccount = Utils.getAccountByAccountNumber(
							record.getAccount().getAccountID(), userSession.getPortalAccounts());
					if (depositAccount != null) {
						plasticsMap.put(frontendAccount.getFrontEndId(),
								depositAccount.getNickname() + " " + maskUtil.mask(frontendAccount.getAccountNumber()));
					}
				}
			}
		}

		model.put("plastics", plasticsMap);

		return model;
	}

	public PinEncryptionTool getPinTool() {
		return pinTool;
	}

	public void setPinTool(PinEncryptionTool pinTool) {
		this.pinTool = pinTool;
	}

}
