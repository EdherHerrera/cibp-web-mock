package evertec.cibp.web.form;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.CardCVVAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.CardPINAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.DepositAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.requests.PayrollCardAccountInformationValidationRequest;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse.AccountValidationStatus;
import evertec.cibp.encryptedPin.PinEncryptionTool;
import evertec.cibp.web.form.bean.ValidateInfoBean;
import evertec.cibp.web.form.bean.ValidateInfoBean.ACTION;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.Utils;

public class LoginPinCvvForm extends AbstractWizardFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private PinEncryptionTool pinTool;

	public LoginPinCvvForm() {
		super();
		setCommandClass(ValidateInfoBean.class);
		setCommandName("loginPinCvv");
		setPages(new String[] { "loginPinCvv", "loginPinCvv" });
	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		ValidateInfoBean info = (ValidateInfoBean) command;
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();
		CustomerServices customerService = session.getCustomerService();

		List<ASK_FOR> tasksList = session.getTasksList();
		List<ACTION> removetasks = info.getActionsProcessed();
		if (removetasks.contains(ACTION.PIN) && info.getActInfCounter() < 3) {
			tasksList.remove(ASK_FOR.VALIDATE_PIN);
			customerService.resetCustomerFlag(new ModifyCustomerRequest(
					Utils.generateCustomerInteractionEvent(request), profile.getProfileId(), "", 0, "", 0, "", null,
					CustomerFlagType.VALIDATE_PIN, session.getDeviceRequest()));
		} else if (info.getActInfCounter() >= 3) {
			model.put("blocked", true);
			showForm(request, errors, "loginPinCvv", model);
		}
		return new ModelAndView(SignonSequenceUtils.getNextView(session));
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		ValidateInfoBean info = (ValidateInfoBean) command;
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();
		CustomerServices customerService = session.getCustomerService();

		ValidateInfoBean.ACTION action = info.getAction();

		if (action == ValidateInfoBean.ACTION.PIN) {

			String accountnumber = profile.getEnrollmentAccountNumber();
			String pin = info.getPin();
			String cvv = info.getCvv();
			String taxId = profile.getTaxId();
			String expMonth = info.getExpMonth();
			String expYear = info.getExpYear();
			ValidateAccountStatusResponse validateAccountInfo = null;

			String accountType = profile.getEnrollmentAccountSubtype();
			info.setAccountType(accountType);
			if (accountType.equalsIgnoreCase("IDA")) {

				String encryptedPin = pinTool.encrypt(pin);
				DepositAccountInformationValidationRequest depReq = new DepositAccountInformationValidationRequest(
						Utils.generateCustomerInteractionEvent(request), accountnumber, "", taxId, encryptedPin, true,
						false);
				validateAccountInfo = customerService.validateDepositAccountInfo(depReq);

			} else if (accountType.equalsIgnoreCase("CCA")) {
				CardCVVAccountInformationValidationRequest cvvReq = new CardCVVAccountInformationValidationRequest(
						Utils.generateCustomerInteractionEvent(request), accountnumber, taxId, cvv, expMonth, expYear,
						"", false);
				validateAccountInfo = customerService.validateCrediCardCVVAccountInfo(cvvReq);

			} else if (accountType.equalsIgnoreCase("AMEX")) {

				String encryptedPin = pinTool.encrypt(pin);
				CardPINAccountInformationValidationRequest cardPinReq = new CardPINAccountInformationValidationRequest(
						Utils.generateCustomerInteractionEvent(request), accountnumber, taxId, encryptedPin, expMonth,
						expYear, "", true, false, false);
				validateAccountInfo = customerService.validateCrediCardPINAccountInfo(cardPinReq);

			} else if (accountType.equalsIgnoreCase("PFC")) {

				String encryptedPin = pinTool.encrypt(pin);
				PayrollCardAccountInformationValidationRequest payrollReq = new PayrollCardAccountInformationValidationRequest(
						Utils.generateCustomerInteractionEvent(request), accountnumber, taxId, encryptedPin, true);
				validateAccountInfo = customerService.validatePayrollCardAccountInfo(payrollReq);

			}
			if (validateAccountInfo != null && validateAccountInfo.getStatus() == AccountValidationStatus.VALID) {
				profile = customerService.modifyEnrollmentAccountInformation(profile, accountnumber, accountType,
						validateAccountInfo.getAccountProductId());
				session.setCustomerProfile(profile);
				info.actionsProcessed.add(ACTION.PIN);
				return;
			} else if (validateAccountInfo != null
					&& validateAccountInfo.getStatus() == AccountValidationStatus.ACCOUNT_BLOCKED) {
				info.setActInfCounter(3);
				// errors.rejectValue("pin", "",
				// "Account is block, please call to CSR");
				// return;
			} else {
				if (info.getActInfCounter() < 3) {
					info.setActInfCounter(info.getActInfCounter() + 1);
					errors.rejectValue("pin", "enrollment.accountnumber.invalid", "Invalid Account");
					return;
				}

			}

			if (info.getActInfCounter() >= 3) {
				errors.rejectValue("pin", "", "Account is block, please call to CSR");
				return;
			}
		}
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();
		Map<String, Object> model = new HashMap<>();
		ValidateInfoBean info = (ValidateInfoBean) command;
		model.put("customername", profile.getFirstName());
		model.put("page", String.valueOf(page));
		List<ASK_FOR> tasksList = session.getTasksList();
		for (ASK_FOR thing : tasksList) {
			switch (thing) {
			case VALIDATE_PIN:
				model.put("validatepin", true);
				break;
			default:
				break;
			}
		}
		if (info.getActInfCounter() >= 3) {
			model.put("blocked", true);
		}

		// expYears
		Map<Object, String> expYears = new LinkedHashMap<Object, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			expYears.put("", "A&ntilde;os:");
		else
			expYears.put("", "Year:");
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		Calendar cal = GregorianCalendar.getInstance();
		for (int i = 0; i <= 8; i++) {
			String year = df.format(cal.getTime());
			expYears.put(Integer.parseInt(year), year);
			cal.add(Calendar.YEAR, 1);
		}
		model.put("expYears", expYears);

		// expMonths
		Map<Object, String> expMonths = new LinkedHashMap<Object, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			expMonths.put("", "Mes:");
		else
			expMonths.put("", "Month:");
		df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
		cal = GregorianCalendar.getInstance();
		cal.set(Calendar.MONTH, 0);
		for (int i = 1; i <= 12; i++) {
			String month = df.format(cal.getTime());
			expMonths.put(i, month);
			cal.add(Calendar.MONTH, 1);

		}
		model.put("expMonths", expMonths);

		String accountInfoType = profile.getEnrollmentAccountSubtype();
		if (accountInfoType.equalsIgnoreCase("IDA")) {
			model.put("accountIDA", "TRUE");
		} else if (accountInfoType.equalsIgnoreCase("CCA")) {
			if ((profile.getEnrollmentAccountNumber().length() == 15 && profile.getEnrollmentAccountNumber()
					.startsWith("377"))
					|| profile.getEnrollmentAccountNumber().length() == 16
					&& profile.getEnrollmentAccountNumber().startsWith("0377")) {
				model.put("accountAMEX", "TRUE");
			} else
				model.put("accountCCA", "TRUE");
		} else if (accountInfoType.equalsIgnoreCase("AMEX")) {
			model.put("accountAMEX", "TRUE");
		} else if (accountInfoType.equalsIgnoreCase("PFC")) {
			model.put("accountPFC", "TRUE");
		}
		return model;
	}

	public PinEncryptionTool getPinTool() {
		return pinTool;
	}

	public void setPinTool(PinEncryptionTool pinTool) {
		this.pinTool = pinTool;
	}

}
