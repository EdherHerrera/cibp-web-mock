package evertec.cibp.web.form;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import edu.emory.mathcs.backport.java.util.Collections;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractStatement;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.PayrollEntitlement;
import evertec.cibp.core.models.epayroll.PayrollStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.StopPaymentRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.StopPaymentResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.AccountsFacade;
import evertec.cibp.web.form.bean.AccountServiceBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;
import evertec.dp.models.xsd.photocopy.PhotocopyResponse_Type;

/**
 * Class that input for the Account Services
 *
 */
public class AccountServicesForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	
	private String wealthCodes;
	private String preferredCodes;
	private static final String SHOW_INSIDE_IFRAME = "showInsideIframe";
	private static final String ERROR_MSG = "Error processing your request";
	private static final String SUCCESS = "SUCCESS";
	private static final String EXPAND_OD_LIMIT = "expandODLimit";
	private static final String EXPAND_USAVE = "expandUSave";
	private static final String EN_SELECT_ACCOUNT = "Select an account";
	private static final String SP_SELECT_ACCOUNT = "Selecciona una cuenta";
	private static final String INVALID_ACCOUNT = "Invalid Account Selection";
	private static final String ACCOUNT = "account";
	private static final String AGREEMENT_ERROR_VAR_NAME = "accountservices.agreement.error";
	private static final String AGREEMENT = "agreement";
	private static final String AGREEMENT_DEFAULT_MSG = "Please agree.";
	private static final String AGREE = "agree";

	/**
	 * Constructor for the Account Services form
	 * View and bean definition
	 */
	public AccountServicesForm() {
		super();
		setCommandClass(AccountServiceBean.class);
		setCommandName("accountservices");
		setFormView("accountservices");
		setBindOnNewForm(true);
	}

	@Override
	protected boolean isFormSubmission(HttpServletRequest request) {
		return request.getParameter("action") != null;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		AccountServiceBean searchBean = (AccountServiceBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		// CIBP-1767
		String showInsideIframe = request.getParameter(SHOW_INSIDE_IFRAME);
		model.put(SHOW_INSIDE_IFRAME, showInsideIframe);

		AccountServiceBean.ACTION action = searchBean.getAction();

		if (action == AccountServiceBean.ACTION.CHECKCOPY) {
			TVFrontendAccount account = Utils.getAccountById(searchBean.getAccount(), userSession.getAccounts());
			Calendar checkdate = Calendar.getInstance();
			checkdate.setTime(searchBean.getCheckDate());
			
			/**
			 * MBNA-2411
			 * Method getCheckCopy : CIBP-WEB microservice integration
			 * 
			 */
			//Microservice implementation
			PhotocopyResponse_Type resp = AccountsFacade.getInstance()
														.getCheckCopy(request,
																	  account.getAccountNumber(),
																	  searchBean.getCheckAmount(), 
																	  searchBean.getCheckNumber(), 
																	  checkdate, 
																	  account.getBankId());
			
			if (Integer.parseInt(resp.getStatusCode()) != 0)
				model.put("chkCpError", ERROR_MSG);
			else {
				if (resp.getRefId() != null)
					model.put("chkCpReference", resp.getRefId());
				model.put("chkCpSuccess", SUCCESS);
			}
		}

		else if (action == AccountServiceBean.ACTION.STATEMENTCOPY) {
			TVFrontendAccount account = Utils.getAccountById(searchBean.getAccount(), userSession.getAccounts());
			Calendar startDate;
			Calendar endDate;

			if (searchBean.getStatementId() != null && !"".equalsIgnoreCase(searchBean.getStatementId())
					&& !"none".equalsIgnoreCase(searchBean.getStatementId())) {
				// do something with statements
				AbstractStatement stmt = getStatementById(searchBean.getStatementId(), account.getStatements());

				startDate = Calendar.getInstance();
				startDate.setTime(stmt.getStartDate());
				endDate = Calendar.getInstance();
				endDate.setTime(stmt.getEndDate());
			}

			startDate = new GregorianCalendar(searchBean.getStmtStartyear(), searchBean.getStmtStartmonth(),
					searchBean.getStmtStartday());
			endDate = new GregorianCalendar(searchBean.getStmtEndyear(), searchBean.getStmtEndmonth(),
					searchBean.getStmtEndday());

			/**
			 * Method getStatementCopy : CIBP-WEB microservice integration
			 * 
			 */
			PhotocopyResponse_Type resp = AccountsFacade.getInstance().getStatementCopy(account.getAccountNumber(), startDate, endDate, account.getBankId(), userSession);
			
			if (Integer.parseInt(resp.getStatusCode()) != 0)
				model.put("stmtCpError", ERROR_MSG);
			else {
				if (resp.getRefId() != null)
					model.put("stmtCpReference", resp.getRefId());
				model.put("stmtCpSuccess", SUCCESS);
			}
		}

		else if (action == AccountServiceBean.ACTION.STOPCHECK) {
			TVFrontendAccount account = Utils.getAccountById(searchBean.getAccount(), userSession.getAccounts());
			String checkNumber = searchBean.getStopCheckNumber();

			StopPaymentRequest chkrequest = new StopPaymentRequest(Utils.generateCustomerInteractionEvent(request),
					customer, userSession.getBankingSession(), account, checkNumber);
			StopPaymentResponse chkres = customerService.requestStopPayment(chkrequest);
			if (chkres.getStatus() != ResultStatus.SUCCESS)
				model.put("stopPmtError", ERROR_MSG);
			else {
				if (chkres.getReferenceNumber() != null)
					model.put("stopPmtReference", chkres.getReferenceNumber());
				model.put("stopPmtSuccess", SUCCESS);
			}
		}
		if (action == AccountServiceBean.ACTION.ORDERCHECK) {
			TVFrontendAccount account = Utils.getAccountById(searchBean.getAccount(), userSession.getAccounts());
			
			/**
			 * MBNA-1171
			 * Method getHarlandUrlByAccount : CIBP-WEB microservice integration
			 * 
			 */
			String url = AccountsFacade.getInstance().getHarlandUrlByAccount(customer, account, "https://www.bancopopular.com/cibp-web/actions/accountservices", userSession);
			

			return new ModelAndView("redirect:" + url);

		}

		return showForm(request, response, error, model);
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		AccountMaskUtil mask = new AccountMaskUtil();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		// CIBP-1767
		String showInsideIframe = request.getParameter(SHOW_INSIDE_IFRAME);
		model.put(SHOW_INSIDE_IFRAME, showInsideIframe);
		
		boolean hasRejectedTermsBsmart = false;
		boolean hasBsmartAccount = false;
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)
				&& userSession.getCustomerProfile().getUserInterface() != UserInterface.COMMERCIAL 
				&& !userSession.isWealth(wealthCodes, preferredCodes)){
			
			hasRejectedTermsBsmart = this.hasRejectedTermsBsmart(userSession);
			hasBsmartAccount = this.hasBSmartAccount(userSession, request);
		}
		model.put("hasRejectedTermsBsmart", hasRejectedTermsBsmart);
		model.put("hasBsmartAccount", hasBsmartAccount);

		String expandODLimit;
		if (request.getParameter(EXPAND_OD_LIMIT) != null)
			expandODLimit = request.getParameter(EXPAND_OD_LIMIT);
		else
			expandODLimit = "";

		String expandUSave;
		if (request.getParameter(EXPAND_USAVE) != null)
			expandUSave = request.getParameter(EXPAND_USAVE);
		else
			expandUSave = "";
		model.put(EXPAND_OD_LIMIT, expandODLimit);
		model.put(EXPAND_USAVE, expandUSave);

		// CIBP-2057 flag for overdraft view
		if (request.getParameter("pres") != null)
			model.put("pres", 1);

		int odLimitAccounts = 0;
		String odProductIds = "009,020,023,024,025,026,028,029,049,051,052,069,072,081,087";
		for (Iterator<TVFrontendAccount> iter = accounts.iterator(); iter.hasNext();) {
			TVFrontendAccount element = iter.next();
			String suffix;
			if (element.getAccountNumberSuffix() != null)
				suffix = element.getAccountNumberSuffix();
			else
				suffix = "";
			if ((!"".equalsIgnoreCase(suffix)) && "RES".equalsIgnoreCase(suffix) || "SAV".equalsIgnoreCase(suffix))
				continue;
			String productId = element.getAccountProductId();
			if (!GenericValidator.isBlankOrNull(productId) && productId.length() == 3 && odProductIds.contains(productId)){
				odLimitAccounts++;
			}
		}
		model.put("odLimitAccounts", odLimitAccounts);
		/****** Reference Data for Request Checks Photocopies service *******/
		Map<String, String> chkCpAccounts = new LinkedHashMap<>();
		if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
			chkCpAccounts.put("none", EN_SELECT_ACCOUNT);
		else
			chkCpAccounts.put("none", SP_SELECT_ACCOUNT);
		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount name = iterator.next();
			if (name.getFeatures().isCheckCopy() && !name.getHide())

				chkCpAccounts
						.put(String.valueOf(name.getFrontEndId()),
								name.getNickname()
										+ " "
										+ mask.mask(name.getAccountNumber())
										+ ((name.getAccountNumberSuffix() != null && !"".equals(name.getAccountNumberSuffix())) ? " " + name.getAccountNumberSuffix() : ""));
		}
		model.put("copyChecksAccounts", chkCpAccounts);

		/************* Reference Data for Statements Photocopies *************/
		Map<String, String> stmtCpAccounts = new LinkedHashMap<>();
		if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
			stmtCpAccounts.put("none", EN_SELECT_ACCOUNT);
		else
			stmtCpAccounts.put("none", SP_SELECT_ACCOUNT);
		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount name = iterator.next();

			if (name.getFeatures().isStatementCopy() && !name.getHide()) {
				stmtCpAccounts
						.put(String.valueOf(name.getFrontEndId()),
								name.getNickname()
										+ " "
										+ mask.mask(name.getAccountNumber())
										+ ((name.getAccountNumberSuffix() != null && !"".equals(name.getAccountNumberSuffix())) ? " " + name.getAccountNumberSuffix() : ""));
			}
		}
		model.put("stmtCpAccounts", stmtCpAccounts);

		// months
		Map<Object, String> months = new LinkedHashMap<>();
		SimpleDateFormat df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.MONTH, 0);
		for (int i = 0; i < 12; i++) {
			String month = df.format(cal.getTime());
			months.put(i, month);
			cal.add(Calendar.MONTH, 1);
		}
		model.put("months", months);

		// days
		Map<Object, String> days = new LinkedHashMap<>();
		df = new SimpleDateFormat("d");
		cal.set(Calendar.DAY_OF_MONTH, 1);
		for (int i = 1; i <= 31; i++) {
			String day = df.format(cal.getTime());
			days.put(i, day);
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		model.put("days", days);

		// years
		Map<Object, String> years = new LinkedHashMap<>();
		df = new SimpleDateFormat("yyyy");
		cal = GregorianCalendar.getInstance();
		for (int i = 0; i <= 10; i++) {
			String year = df.format(cal.getTime());
			years.put(Integer.parseInt(year), year);
			cal.add(Calendar.YEAR, -1);
		}
		model.put("years", years);

		/****** Reference Data for Stop Payments service *******/
		Map<String, String> stopCheckAccounts = new LinkedHashMap<>();
		if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
			stopCheckAccounts.put("none", EN_SELECT_ACCOUNT);
		else
			stopCheckAccounts.put("none", SP_SELECT_ACCOUNT);
		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount name = iterator.next();
			if (name.getFeatures().isStopPayment() && !name.getHide())
				stopCheckAccounts
						.put(String.valueOf(name.getFrontEndId()),
								name.getNickname()
										+ " "
										+ mask.mask(name.getAccountNumber())
										+ ((name.getAccountNumberSuffix() != null && !"".equals(name.getAccountNumberSuffix())) ? " " + name.getAccountNumberSuffix() : ""));
		}
		model.put("stopCheckAccounts", stopCheckAccounts);

		/****** Reference Data for Order check Request service *******/
		Map<String, String> checkOrderAccounts = new LinkedHashMap<>();
		TVFrontendAccount checkAccount = null;
		int k = 0;
		if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
			checkOrderAccounts.put("none", EN_SELECT_ACCOUNT);
		else
			checkOrderAccounts.put("none", SP_SELECT_ACCOUNT);
		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount name = iterator.next();
			if (name.getFeatures().isCheckOrder() && !name.getHide()) {
				checkOrderAccounts
						.put(String.valueOf(name.getFrontEndId()),
								name.getNickname()
										+ " "
										+ mask.mask(name.getAccountNumber())
										+ ((name.getAccountNumberSuffix() != null && !"".equals(name.getAccountNumberSuffix())) ? " " + name.getAccountNumberSuffix() : ""));
				if (k == 0)
					checkAccount = name;
				k = k + 1;

			}
		}
		if (k == 1) {
			if(checkAccount != null){
				model.put("checkOrderAccount", String.valueOf(checkAccount.getFrontEndId()));
			}else{
				model.put("checkOrderAccount", "");
				logger.error("The checkAccount object is null");
			}
			model.put("ChkOrderAccountsNumber", "ONE");
		} else if (k > 1) {
			model.put("checkOrderAccounts", checkOrderAccounts);
			model.put("ChkOrderAccountsNumber", "MULTIPLE");
		}

		/************************* Activate ATH **********************/

		Map<String, String> athtypes = new LinkedHashMap<>();
		if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
			athtypes.put("none", "Select ATH type");
			athtypes.put("athregular", "Regular ATH");
			athtypes.put("athvisa", "ATH with visa");
		} else {
			athtypes.put("none", "Selecciona el tipo de ATH");
			athtypes.put("athregular", "ATH Regular");
			athtypes.put("athvisa", "ATH con visa");
		}
		model.put("athtypes", athtypes);

		/************************* U Save **********************/
		boolean hasUSaveAccount = Utils.hasUSaveAccount(userSession.getAccounts());
		model.put("hasUSaveAccount", hasUSaveAccount);

		// Should we open up with a U Save account?
		String usaveKey = request.getParameter(ACCOUNT);
		if (usaveKey != null) {
			model.put("usaveKey", usaveKey);
		}
		
		model.put("checksNotification", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE14));
		model.put("MBSFE49", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE49));
		
		//MBSFE-1342
		model.put("MBSFE1342", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1342));
		//END MBSFE-1342
		
		return model;

	}

	/**
	 * Method to get an AbstractStatement from ID
	 * @param id The statement ID
	 * @param statements The statements list
	 * @return an AbstractStatement if found, null otherwise
	 */
	public AbstractStatement getStatementById(String id, List<AbstractStatement> statements) {
		for (Iterator<AbstractStatement> iterator = statements.iterator(); iterator.hasNext();) {
			AbstractStatement act = iterator.next();
			if (act.getFrontEndId().equals(id))
				return act;
		}
		return null;
	}
	
	
	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		AccountServiceBean searchBean = (AccountServiceBean) command;
		AccountServiceBean.ACTION action = searchBean.getAction();

		if (action == AccountServiceBean.ACTION.CHECKCOPY) {
			if (searchBean.getAccount() == null || "none".equals(searchBean.getAccount()))
				errors.rejectValue(ACCOUNT, "", INVALID_ACCOUNT);
			if (searchBean.getCheckAmount() == null)
				errors.rejectValue("checkAmount", "", "Please enter a check amount");
			if (searchBean.getCheckDate() == null)
				errors.rejectValue("checkDate", "", "Please enter a check date");
			if (GenericValidator.isBlankOrNull(searchBean.getCheckNumber()))
				errors.rejectValue("checkAmount", "", "Please enter a check number");
			if (GenericValidator.isBlankOrNull(searchBean.getAgreement())
					|| !AGREE.equalsIgnoreCase(searchBean.getAgreement())) {
				errors.rejectValue(AGREEMENT, AGREEMENT_ERROR_VAR_NAME, AGREEMENT_DEFAULT_MSG);
			}
		} else if (action == AccountServiceBean.ACTION.STATEMENTCOPY) {
			if (searchBean.getAccount() == null || "none".equals(searchBean.getAccount()))
				errors.rejectValue(ACCOUNT, "", INVALID_ACCOUNT);
			if (searchBean.getStatementId() == null || "none".equals(searchBean.getStatementId())) {
				if (searchBean.getStmtStartday() == -1 || searchBean.getStmtStartyear() == -1
						|| searchBean.getStmtEndday() == -1 || searchBean.getStmtEndyear() == -1)
					errors.rejectValue("stmtEndyear", "", "Please verify the dates entered");
			}
			if (GenericValidator.isBlankOrNull(searchBean.getAgreement())
					|| !AGREE.equalsIgnoreCase(searchBean.getAgreement())) {
				errors.rejectValue(AGREEMENT, AGREEMENT_ERROR_VAR_NAME, AGREEMENT_DEFAULT_MSG);
			}

		} else if (action == AccountServiceBean.ACTION.STOPCHECK) {
			if (searchBean.getAccount() == null || "none".equals(searchBean.getAccount()))
				errors.rejectValue(ACCOUNT, "", INVALID_ACCOUNT);
			if (GenericValidator.isBlankOrNull(searchBean.getAgreement())
					|| !AGREE.equalsIgnoreCase(searchBean.getAgreement())) {
				errors.rejectValue(AGREEMENT, AGREEMENT_ERROR_VAR_NAME, AGREEMENT_DEFAULT_MSG);
			}
		} else if (action == AccountServiceBean.ACTION.ORDERCHECK
				&& (searchBean.getAccount() == null || "none".equals(searchBean.getAccount()))) {
		
				errors.rejectValue(ACCOUNT, "", INVALID_ACCOUNT);
		}

	}

	private boolean hasRejectedTermsBsmart(UserSession userSession){
		CustomerEntitlement ent = userSession.getCustomerProfile().getEntitlementByType(EntitlementType.PAYROLL);
		evertec.cibp.core.models.entitlement.PayrollEntitlement entt;
		if(ent != null){
			entt = new PayrollEntitlement(ent);
			if("true".equalsIgnoreCase(entt.getHasbsmart()) && entt.getAcceptterms().equalsIgnoreCase(PayrollStatus.REJECT.name())){
				return true;
			}
		}
		return false;
		
	}
	
	private boolean hasBSmartAccount(UserSession userSession, HttpServletRequest request){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		CustomerEntitlement ent = userSession.getCustomerProfile().getEntitlementByType(EntitlementType.PAYROLL);
		evertec.cibp.core.models.entitlement.PayrollEntitlement entt;
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		String enrollAcc = userSession.getCustomerProfile().getEnrollmentAccountNumber();
		if(ent == null){
			for(TVFrontendAccount account : accounts){
				if("086".equalsIgnoreCase(account.getAccountProductId()) && !account.getAccountNumber().equalsIgnoreCase(enrollAcc)){
					WebUtils.setSessionAttribute(request, "hasBsmartAcct", true);
					return true;
				}
			}
			Date date = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DAY_OF_YEAR, 90);
			date = calendar.getTime();
			entt = new PayrollEntitlement(String.valueOf(accounts.size()), String.valueOf(false), PayrollStatus.REJECT.toString(), 
					format.format(date),EntitlementStatus.ENABLED);
			userSession.getCustomerService().addEntitlement(userSession.getCustomerProfile(), entt.getCustomerEntitlement());
			return false;
		}
		else{
			entt = new PayrollEntitlement(ent);
			if("false".equalsIgnoreCase(entt.getHasbsmart()) || entt.getAmountaccounts().equalsIgnoreCase(String.valueOf(accounts.size()))){
				return false;
			}
			else{
				for(TVFrontendAccount account : accounts){
					if("086".equalsIgnoreCase(account.getAccountProductId())){
						WebUtils.setSessionAttribute(request, "hasBsmartAcct", true);
						return true;
					}
				}
			}
		}
		return false;
	}

	public String getWealthCodes() {
		return wealthCodes;
	}

	public void setWealthCodes(String wealthCodes) {
		this.wealthCodes = wealthCodes;
	}

	public String getPreferredCodes() {
		return preferredCodes;
	}

	public void setPreferredCodes(String preferredCodes) {
		this.preferredCodes = preferredCodes;
	}
	
}
