package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.ActivationCardAccountResponse;
import evertec.cibp.core.services.responses.ActivationCardAccountResponse.Status;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.form.bean.ActivateAthIntBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

//MBSFE-1342
public class ActivateAthIntForm extends CancellableFormController {

	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	private static final String VIEW = "activateAthInt";
	private static final String MESSAGE = "message";
	private static final String ACTIVATED = "activated";
	private static final String NOT_ACTIVATED = "notactivated";
	private static final String STATUS_ACTIVE = "ACTIVE";

	/**
	 * Constructor for the Activate ATH International form View and bean
	 * definition
	 */
	public ActivateAthIntForm() {
		super();
		setCommandClass(ActivateAthIntBean.class);
		setCommandName(VIEW);
		setFormView(VIEW);
	}

	/**
	 * OnSubmit method
	 * @param request
	 * @param response
	 * @param command
	 * @param error
	 * @return View
	 * @throws Exception
	 */
	@SuppressWarnings("finally")
	@Override
	protected ModelAndView onSubmit (HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		ActivateAthIntBean bean = (ActivateAthIntBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		BankingSession bankingSession = userSession.getBankingSession();
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		//MBSE 1560
		CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request);
		List<TVFrontendAccount> plastics = userSession.getCardPlasticAccounts();
		ActivationCardAccountResponse activationResult = new ActivationCardAccountResponse();
	
		String frontEndId = bean.getFrontEndId();
		TVFrontendAccount frontendAccount;
		frontendAccount = Utils.getAccountById(frontEndId, plastics);

		try {

			activationResult = customerService.activatePlasticIntAccount(customerProfile, bankingSession,
					frontendAccount.getAccountNumber(), event);

			if (activationResult.getResponseStatus() == Status.ACTIVATED) {
				frontendAccount.setStatus(STATUS_ACTIVE);
				model.put(ACTIVATED, true);
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
					model.put(MESSAGE, "This International ATH Visa card has been successfully activated.");
				} else {
					model.put(MESSAGE, "Su tarjeta Visa Internacional ATH ha sido activada exitosamente.");
				}
			} else if (activationResult.getResponseStatus() == Status.CARD_NOT_FOUND
					|| activationResult.getResponseStatus() == Status.CARD_ACTIVATION_FAIL) {
				model.put(NOT_ACTIVATED, true);
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
					model.put(MESSAGE,
							"Your request to activate your International ATH Visa card cannot be processed at this moment.");
				} else {
					model.put(MESSAGE,
							"Tu solicitud para activar tu tarjeta ATH Internacional VISA no pudo ser procesada en este momento.");
				}
			}

		} catch (RuntimeException e) {
			model.put(NOT_ACTIVATED, true);
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				model.put(MESSAGE,
						"Your request to activate your International ATH Visa card cannot be processed at this moment.");
			} else {
				model.put(MESSAGE,
						"Tu solicitud para activar tu tarjeta ATH Internacional VISA no pudo ser procesada en este momento.");
			}
		} finally {
			return showForm(request, error, VIEW, model);
		}
	}

	/**
	 * ReferenceData method
	 * @param request
	 * @param command
	 * @param errors
	 * @return a Model
	 * @throws Exception
	 */
	@SuppressWarnings("finally")
	protected Map<String, Object> referenceData (HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<TVFrontendAccount> frontendAccounts = userSession.getCardPlasticAccounts();
		List<StatusAccountWrapper> plasticsVM = new ArrayList<>();

		try {

			//This is taking care of Frame height
			int heightFrame = 150;
			
			// Carga las ATH en el VM
			for (Iterator<TVFrontendAccount> iterator = frontendAccounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount frontendAccount = iterator.next();
				if (frontendAccount.getAccountNumber().startsWith("4549")) {
					CardPlasticAccountRecord record = (CardPlasticAccountRecord) frontendAccount.getTVAccount()
							.getRecords();
					if (record != null) {
						TVFrontendAccount depositAccount = Utils.getAccountByAccountNumber(
								record.getAccount().getAccountID(), userSession.getPortalAccounts());
						if (depositAccount != null) {
							frontendAccount.setNickname(depositAccount.getNickname());
							plasticsVM.add(new StatusAccountWrapper(frontendAccount));
							heightFrame = heightFrame+20;
						}

					}
				}
			}
			
			model.put("heightFrame", (Integer.toString(heightFrame) + "px"));

			if (plasticsVM.size() != 0) {
				model.put("plastics", plasticsVM);
			} else {
				model.put(NOT_ACTIVATED, true);
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
					model.put(MESSAGE,
							"You don’t have any International ATH<sup>&reg;</sup> card available for activation.");
				} else {
					model.put(MESSAGE,
							"No tiene tarjeta ATH<sup>&reg;</sup> Internacional disponible para activación.");
				}
			}

		} catch (RuntimeException e) {
			model.put(NOT_ACTIVATED, true);
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				model.put(MESSAGE,
						"Your request to activate your International ATH Visa card cannot be processed at this moment.");
			} else {
				model.put(MESSAGE,
						"Tu solicitud para activar tu tarjeta ATH Internacional VISA no pudo ser procesada en este momento.");
			}
		} finally {
			return model;
		}
	}

}
