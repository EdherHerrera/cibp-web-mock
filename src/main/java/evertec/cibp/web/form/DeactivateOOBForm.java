package evertec.cibp.web.form;

import evertec.cibp.core.models.customer.CustomerActions;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.adaptiveauth.EnrollmentInformationinRSARequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.RsaQuestionsUpdateResponse;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionSelectionResponse;
import evertec.cibp.web.form.bean.ChangeQuestionsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DeactivateOOBForm extends AbstractWizardFormController {
	/** Logger */
	protected final Log log = LogFactory.getLog(getClass());

	private String alertsAccountTypes;
	static final String DEACTIVEOOB = "deactivateOOB";

	public DeactivateOOBForm() {
		super();
		setCommandClass(ChangeQuestionsBean.class);
		setCommandName(DEACTIVEOOB);
		setPages(new String[] { DEACTIVEOOB, DEACTIVEOOB });
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		model.put("page", Integer.toString(page));
		
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		List<ASK_FOR_OOB> listNavOOB = userSession.getTasksListOOB();
		if(listNavOOB != null && listNavOOB.contains(ASK_FOR_OOB.DEACTIVATE_2_STEP)){
			userSession.getTasksListOOB().remove(ASK_FOR_OOB.DEACTIVATE_2_STEP);
			request.setAttribute("firstTime", false);
		}
		
		if (page == 0) {
			return model;
		} else {
			Map<String, String> questions = new LinkedHashMap<>();
			String selectquestiontext = "Select a question:";
			if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
				selectquestiontext = "Seleccione una pregunta:";
			questions.put("", selectquestiontext);

			CustomerInteractionEvent generateCustomerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
			RSAActionRequest actionrequest = new RSAActionRequest(generateCustomerInteractionEvent, userSession.getCustomerProfile()
					.getUsername().toLowerCase(), "", userSession.getCustomerProfile().getEmail(), 0, false, userSession.getDeviceRequest());
			actionrequest.setRsaLang("sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "ES" : "EN");
			QuestionSelectionResponse questionResponse = userSession.getCustomerService().rsaGetQuestionsSelection(
					actionrequest);
			
			userSession.getDeviceRequest().setDeviceTokenCookie(questionResponse.getRsaCookie());

			if (questionResponse.getStatusResult().equals(ResultStatus.SUCCESS)) {
				Map<Object, Object> q = new HashMap<>();
				q.put("", selectquestiontext);
				q.putAll(questionResponse.getQuestionMap1());
				model.put("questions1", q);

				Map<Object, Object> q2 = new HashMap<>();
				q2.put("", selectquestiontext);
				q2.putAll(questionResponse.getQuestionMap2());
				questions = new LinkedHashMap<>();
				questions.put("", selectquestiontext);
				model.put("questions2", q2);

				Map<Object, Object> q3 = new HashMap<>();
				q3.put("", selectquestiontext);
				q3.putAll(questionResponse.getQuestionMap3());
				questions = new LinkedHashMap<>();
				questions.put("", selectquestiontext);
				model.put("questions3", q3);
			}
			return model;
		}
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page) {

		if (page == 0)
			return;

		ChangeQuestionsBean bean = (ChangeQuestionsBean) command;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "question1", "changequestions.q1.required", "Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "question2", "changequestions.q2.required", "Required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "question3", "changequestions.q3.required", "Required");

		String a1 = bean.getAnswer1();
		String a2 = bean.getAnswer2();
		String a3 = bean.getAnswer3();
		String c1 = bean.getConfirm1();
		String c2 = bean.getConfirm2();
		String c3 = bean.getConfirm3();
		
		String errorCode1 =  "changequestion.answer.notvalid";
		String errorCode2 = "changequestion.answerconfirm.notmatch";
		String msg1 = "Please enter a valid answer";
		String msg2 = "Answers values must match";		
		String re = "^\\w+(\\s{1}\\w+)*$";

		if (GenericValidator.isBlankOrNull(a1) || !GenericValidator.matchRegexp(a1, re)) {
			errors.rejectValue("answer1", errorCode1, msg1);
		} else if (!GenericValidator.isBlankOrNull(a1) && !a1.equals(c1)) {
			errors.rejectValue("confirm1", errorCode2, msg2);
		}

		if (GenericValidator.isBlankOrNull(a2) || !GenericValidator.matchRegexp(a2, re)) {
			errors.rejectValue("answer2", errorCode1, msg1);
		} else if (!GenericValidator.isBlankOrNull(a2) && !a2.equals(c2)) {
			errors.rejectValue("confirm2", errorCode2, msg2);
		}

		if (GenericValidator.isBlankOrNull(a3) || !GenericValidator.matchRegexp(a3, re)) {
			errors.rejectValue("answer3", errorCode1, msg1);
		} else if (!GenericValidator.isBlankOrNull(a3) && !a3.equals(c3)) {
			errors.rejectValue("confirm3", errorCode2, msg2);
		}

	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		ChangeQuestionsBean bean = (ChangeQuestionsBean) command;

		EnrollmentInformationinRSARequest eirsa = new EnrollmentInformationinRSARequest(
				Utils.generateCustomerInteractionEvent(request), userSession.getUsername(),
				userSession.getCustomerProfile().getEmail(), false,userSession.getDeviceRequest());
		eirsa.setQuestionsAndAnswer( bean.getQuestion1(),bean.getQuestion2(), bean.getQuestion3(), bean.getAnswer1(), bean.getAnswer2(), bean.getAnswer3());

		RsaQuestionsUpdateResponse updateRSAresponse = customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(eirsa);
		
		if(!updateRSAresponse.getStatus().equals(ResultStatus.SUCCESS)){
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession
				.setStatusmessage("Unable to deactivate 2 Step Verification at this time, please try later.");
			else
				userSession
				.setStatusmessage("Problemas al desactivar su Verificaci&oacute;n de 2 Pasos en este momento, favor intentar luego.");
			model.put("unableUpdate", true);
			log.error("Deactivate OOB: Unable to delete RSA user.");
			return new ModelAndView(DEACTIVEOOB, model);
		}
		
		userSession.getDeviceRequest().setDeviceTokenCookie(updateRSAresponse.getDeviceTokenCookie());
		// Increment OOB Removed By Edit count for Monthly Report
		customerService.logCustomerUsageData(userSession.getCustomerProfile(), CustomerActions.OOB_EDIT_DEL);
		
		// Reset RSA entitlement
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerEntitlement rsaEnt = customer.getEntitlementByType(EntitlementType.RSA);
		if (rsaEnt != null)
			customerService.getCustomerProfileService().removeEntitlement(customer, rsaEnt);
		RSAUtils.createRSAEntitlement(customerService, customer);
		userSession.setOobEnroll(false);
		
		// Send confirmation email
		customerService.sendOobDeactivationEmail(userSession.getCustomerProfile(), userSession.getPreferredLanguage());

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		cookie.setPath("/");
		ESAPI.httpUtilities().addCookie(response, cookie);
		
		model.put("oobDeactivated", true);

		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			userSession
			.setStatusmessage("2 Step Verification has been deactivated and your security questions have been updated.");
		else
			userSession
			.setStatusmessage("Has desactivado la Verificaci&oacute;n de 2 Pasos y actualizado tus preguntas secretas.");

		return new ModelAndView(DEACTIVEOOB, model);
	}

	public String getAlertsAccountTypes() {
		return alertsAccountTypes;
	}

	public void setAlertsAccountTypes(String alertsAccountTypes) {
		this.alertsAccountTypes = alertsAccountTypes;
	}
}
