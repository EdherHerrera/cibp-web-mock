package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.AlertsHistory;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.web.form.bean.AlerthistoryBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

public class AlertHistoryForm extends CancellableFormController implements MessageSourceAware {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private int maxElements = 20;
	private String alertsHistoryDays;

	private MessageSource messageSource;

	public AlertHistoryForm() {
		super();
		setCommandClass(AlerthistoryBean.class);
		setCommandName("alerthistory");
		setFormView("alerthistory");
		setBindOnNewForm(true);
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		AlerthistoryBean searchBean = (AlerthistoryBean) command;
		Map<String, Object> model = new LinkedHashMap<String, Object>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String lang;

		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			lang = "en";
		else
			lang = "es";

		model.put("lang", lang);

		userSession.setAlertHistoryBean(searchBean);
		return showForm(request, response, error, model);
		// return new ModelAndView("alerthistory", model);
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		AlerthistoryBean searchBean = new AlerthistoryBean();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		List<AlertsHistory> historylist = new LinkedList<AlertsHistory>();
		List<AlertsHistory> historySubList = new LinkedList<AlertsHistory>();
		;
		String lang;

		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			lang = "en";
		else
			lang = "es";

		if (!GenericValidator.isBlankOrNull(request.getParameter("reset"))
				&& request.getParameter("reset").equalsIgnoreCase("true")) {
			userSession.resetAlertHistoryBean();
			if (!GenericValidator.isBlankOrNull(request.getParameter("channeltemp")))
				searchBean.setChannel(request.getParameter("channeltemp"));
			if (!GenericValidator.isBlankOrNull(request.getParameter("categorytemp")))
				searchBean.setCategory(request.getParameter("categorytemp"));

			searchBean.setReset(true);
			// userSession.resetAlertHistoryBean();
			userSession.resetAlertsHistory();
			searchBean.setPage(0);

		} else if (!GenericValidator.isBlankOrNull(request.getParameter("reset"))
				&& request.getParameter("reset").equalsIgnoreCase("false")
				&& !GenericValidator.isBlankOrNull(request.getParameter("newSearch"))
				&& request.getParameter("newSearch").equalsIgnoreCase("true")) {

			if (!GenericValidator.isBlankOrNull(request.getParameter("channeltemp")))
				searchBean.setChannel(request.getParameter("channeltemp"));
			if (!GenericValidator.isBlankOrNull(request.getParameter("categorytemp")))
				searchBean.setCategory(request.getParameter("categorytemp"));

			searchBean.setReset(false);
			searchBean.setNewSearch(true);
			// searchBean.setPage(0);
			// userSession.resetAlertsHistory();
			userSession.resetAlertHistoryBean();
			if (!GenericValidator.isBlankOrNull(request.getParameter("page")))
				searchBean.setPage(Integer.parseInt(request.getParameter("page")));
			else
				searchBean.setPage(0);
		}

		else {
			searchBean.setReset(false);

			if (!GenericValidator.isBlankOrNull(request.getParameter("page")))
				searchBean.setPage(Integer.parseInt(request.getParameter("page")));
			else
				searchBean.setPage(0);

			if (userSession.getAlertHistoryBean() != null
					&& (!GenericValidator.isBlankOrNull(userSession.getAlertHistoryBean().getCategory()))) {
				searchBean.setCategory(userSession.getAlertHistoryBean().getCategory());
			}

			if (userSession.getAlertHistoryBean() != null
					&& (!GenericValidator.isBlankOrNull(userSession.getAlertHistoryBean().getChannel()))) {
				searchBean.setChannel(userSession.getAlertHistoryBean().getChannel());
			}
		}

		historylist = userSession.getAlertsHistoryBySSN(profile.getTaxId(),alertsHistoryDays);

		String category = searchBean.getCategory();
		String channel = searchBean.getChannel();
		if (GenericValidator.isBlankOrNull(category))
			category = "0";
		if (GenericValidator.isBlankOrNull(channel))
			channel = "all";

		if (category.equalsIgnoreCase("0") && channel.equalsIgnoreCase("all")) {
			historySubList.addAll(historylist);
		}
		if (category.equalsIgnoreCase("1") && channel.equalsIgnoreCase("all")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("Balances"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Balances"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("2") && channel.equalsIgnoreCase("all")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("ATH Payments and Withdrawals"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Compras y retiros con mi ATH"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("3") && channel.equalsIgnoreCase("all")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("Payments and Transfers"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Pagos y Transferencias"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("4") && channel.equalsIgnoreCase("all")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("Changes in Personal Information"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Cambios en Info Personal"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("5") && channel.equalsIgnoreCase("all")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("Other"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Otro"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("0") && channel.equalsIgnoreCase("SMS")) {
			for (AlertsHistory history : historylist) {
				if (history.getChannel().equalsIgnoreCase("SMS"))
					historySubList.add(history);
			}
		}
		if (category.equalsIgnoreCase("1") && channel.equalsIgnoreCase("SMS")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("Balances") && history.getChannel().equalsIgnoreCase("SMS"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Balances") && history.getChannel().equalsIgnoreCase("SMS"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("2") && channel.equalsIgnoreCase("SMS")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("ATH Payments and Withdrawals")
							&& history.getChannel().equalsIgnoreCase("SMS"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Compras y retiros con mi ATH")
							&& history.getChannel().equalsIgnoreCase("SMS"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("3") && channel.equalsIgnoreCase("SMS")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("Payments and Transfers")
							&& history.getChannel().equalsIgnoreCase("SMS"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Pagos y Transferencias")
							&& history.getChannel().equalsIgnoreCase("SMS"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("4") && channel.equalsIgnoreCase("SMS")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("Changes in Personal Information")
							&& history.getChannel().equalsIgnoreCase("SMS"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Cambios en Info Personal")
							&& history.getChannel().equalsIgnoreCase("SMS"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("5") && channel.equalsIgnoreCase("SMS")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("Other") && history.getChannel().equalsIgnoreCase("SMS"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Otro") && history.getChannel().equalsIgnoreCase("SMS"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("0") && channel.equalsIgnoreCase("EMAIL")) {
			for (AlertsHistory history : historylist) {
				if (history.getChannel().equalsIgnoreCase("EMAIL"))
					historySubList.add(history);
			}
		}
		if (category.equalsIgnoreCase("1") && channel.equalsIgnoreCase("EMAIL")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("Balances") && history.getChannel().equalsIgnoreCase("EMAIL"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Balances") && history.getChannel().equalsIgnoreCase("EMAIL"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("2") && channel.equalsIgnoreCase("EMAIL")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("ATH Payments and Withdrawals")
							&& history.getChannel().equalsIgnoreCase("EMAIL"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Compras y retiros con mi ATH")
							&& history.getChannel().equalsIgnoreCase("EMAIL"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("3") && channel.equalsIgnoreCase("EMAIL")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("Payments and Transfers")
							&& history.getChannel().equalsIgnoreCase("EMAIL"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Pagos y Transferencias")
							&& history.getChannel().equalsIgnoreCase("EMAIL"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("4") && channel.equalsIgnoreCase("EMAIL")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("Changes in Personal Information")
							&& history.getChannel().equalsIgnoreCase("EMAIL"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Cambios en Info Personal")
							&& history.getChannel().equalsIgnoreCase("EMAIL"))
						historySubList.add(history);
				}
			}
		}
		if (category.equalsIgnoreCase("5") && channel.equalsIgnoreCase("EMAIL")) {
			String tempcategory;
			for (AlertsHistory history : historylist) {
				if (lang.equalsIgnoreCase("en")) {
					tempcategory = history.getCategoryEnglish();
					if (tempcategory.equalsIgnoreCase("Other") && history.getChannel().equalsIgnoreCase("EMAIL"))
						historySubList.add(history);
				} else {
					tempcategory = history.getCategorySpanish();
					if (tempcategory.equalsIgnoreCase("Otro") && history.getChannel().equalsIgnoreCase("EMAIL"))
						historySubList.add(history);
				}
			}
		}

		int total = historySubList.size();
		searchBean.setTotal(total);

		int fromIndex = 0;
		int toIndex = maxElements;
		if (total > 0) {
			fromIndex = searchBean.getPage() * maxElements;
			toIndex = fromIndex + maxElements;
			if (toIndex > total) {
				toIndex = total;
			}
		} else {
			fromIndex = 0;
			toIndex = 0;
		}
		searchBean.setFromIndex(fromIndex);
		searchBean.setToIndex(toIndex);
		searchBean.setHistorySubList(historySubList.subList(searchBean.getFromIndex(), searchBean.getToIndex()));

		int totalPages = searchBean.getTotal() / maxElements;
		if ((searchBean.getTotal() % maxElements) > 0)
			totalPages++;

		LinkedHashMap<String, String> pagesMap = new LinkedHashMap<String, String>();

		for (int i1 = 1; i1 <= totalPages && totalPages >= 1; i1++) {
			pagesMap.put(String.valueOf(i1), String.valueOf(i1));
		}
		searchBean.setPagesMap(pagesMap);
		if (searchBean.isNewSearch())
			userSession.setAlertHistoryBean(searchBean);

		return searchBean;
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		// AlerthistoryBean searchBean = (AlerthistoryBean) command;

		Map<Object, Object> categoryMap = new LinkedHashMap<Object, Object>();
		Map<Object, Object> channelMap = new LinkedHashMap<Object, Object>();

		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			categoryMap.put("0", "All");
			categoryMap.put("1", "Balances");
			categoryMap.put("2", "ATH Payments and Withdrawals");
			categoryMap.put("3", "Payments and Transfers");
			categoryMap.put("4", "Changes in Personal Information");
			categoryMap.put("5", "Other");

			channelMap.put("all", "All");
			channelMap.put("SMS", "SMS");
			channelMap.put("EMAIL", "EMAIL");

			model.put("lang", "en");
		} else {
			categoryMap.put("0", "Todos");
			categoryMap.put("1", "Balances");
			categoryMap.put("2", "Compras y retiros con mi ATH");
			categoryMap.put("3", "Pagos y Transferencias");
			categoryMap.put("4", "Cambios en Info Personal");
			categoryMap.put("5", "Otro");

			channelMap.put("all", "Todos");
			channelMap.put("SMS", "M\u00F3vil");
			channelMap.put("EMAIL", "EMAIL");

			model.put("lang", "es");
		}

		model.put("categories", categoryMap);
		model.put("channels", channelMap);
		if (userSession.getAlertHistoryBean() != null) {
			model.put("alerthistory", userSession.getAlertHistoryBean());
		}

		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}


	public void setAlertsHistoryDays(String alertsHistoryDays) {
		this.alertsHistoryDays = alertsHistoryDays;
	}
	
	

}
