package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.accountopening.dci.DCIAnswer;
import evertec.cibp.core.models.accountopening.dci.DCIAnswerType;
import evertec.cibp.core.models.accountopening.dci.DCICustomer;
import evertec.cibp.core.models.accountopening.dci.DCICustomerAnswer;
import evertec.cibp.core.models.accountopening.dci.DCIQuestion;
import evertec.cibp.core.models.accountopening.dci.DCIQuestionAnswer;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.UserSession;

public class DCIQuestionsPortalForm extends CancellableFormController {

	private String wealthCodes;
	private String preferredCodes;
	private String volumeCodes;

	protected final Log logger = LogFactory.getLog(getClass());

	public DCIQuestionsPortalForm() {
		super();
		setCommandClass(DCIQuestionsPortalForm.class);
		setCommandName("dciQuestionsPortal");
		setFormView("dciQuestionsPortal");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		CustomerServices customerService = session.getCustomerService();
		Map<String, Object> model = new HashMap<>();
		if (session.getDciQuestions() == null) {
			session.setDciQuestions(customerService.getDCIQuestions());
		}
		if (session.getDciQuestionAnswers() == null) {
			session.setDciQuestionAnswers(customerService.getDCIQuestionAnswer());
		}
		List<DCIQuestion> questions = session.getDciQuestions();
		List<DCIQuestionAnswer> questionAnswers = session.getDciQuestionAnswers();
		List<String> pendingQuestions = new ArrayList<String>();
		String errorMsg = "";
		List<DCICustomerAnswer> customerAnswers = new ArrayList<DCICustomerAnswer>();
		DCICustomer dciCustomer = new DCICustomer();
		dciCustomer.setProfile(session.getCustomerProfile());
		for (int i = 0; i < questions.size(); i++) {
			DCIQuestion question = questions.get(i);
			if (question.getAttributeData() != null && question.getString(DCIQuestion.REQUIRED) != null) {
				Boolean depends = question.getBoolean(DCIQuestion.DEPENDS_ON);
				if (depends == null || !depends || pendingQuestions.contains(question.getId())) {
					String display = question.getString(DCIQuestion.ANSWER_TYPE);
					if (display != null) {
						if (display.equals(DCIQuestion.ANSWER_SINGLE)) {
							String requestQuestion = "aq" + question.getId();
							String requestAnswer = request.getParameter(requestQuestion);
							if (requestAnswer != null && !requestAnswer.equals("null")) {
								DCIAnswer answer = new DCIAnswer();
								answer.setId(requestAnswer);
								DCIQuestionAnswer temp = new DCIQuestionAnswer(question, answer);
								if (!questionAnswers.contains(temp)) {
									errorMsg += question.getId() + "-";
								} else {
									Integer qaIndex = questionAnswers.indexOf(temp);
									DCIQuestionAnswer questionAnswer = questionAnswers.get(qaIndex);
									String otro = question.getString(DCIQuestion.SHOW_OTHER);
									if (questionAnswer.getAnswer().getEnglish().equalsIgnoreCase(DCIAnswer.OTHER)
											&& otro != null) {
										String otherQuestion = "oq" + question.getId();
										String otherResponse = request.getParameter(otherQuestion);
										if (otherResponse == null || otherResponse.equals("")
												|| otherResponse.length() > 255) {
											errorMsg += question.getId() + "-";
										} else {
											customerAnswers.add(new DCICustomerAnswer(dciCustomer, question, answer,
													otherResponse));
										}
									} else {
										customerAnswers.add(new DCICustomerAnswer(dciCustomer, question, answer, null));
									}
									if (questionAnswer.getAttributeData() != null) {
										String sIndex = questionAnswer.getString(DCIQuestionAnswer.INDEX);
										if (sIndex != null) {
											Integer index = Integer.valueOf(sIndex);
											for (int j = 0; j < index; j++) {
												String nextQ = DCIQuestionAnswer.NEXT_QUESTION + (j + 1);
												if (nextQ != null) {
													pendingQuestions.add(questionAnswer.getString(nextQ));
												}
											}
										}
									}
								}
							} else {
								errorMsg += question.getId() + "-";
							}
						}
						if (display.equals(DCIQuestion.ANSWER_MULTIPLE)) {
							String requestQuestion = "aq" + question.getId();
							String requestAnswers = request.getParameter(requestQuestion);
							if (requestAnswers != null && !requestAnswers.equals("null")) {
								String[] multipleAnswers = requestAnswers.split(",");
								for (int j = 0; j < multipleAnswers.length; j++) {
									String requestAnswer = multipleAnswers[j];
									DCIAnswer answer = new DCIAnswer();
									answer.setId(requestAnswer);
									DCIQuestionAnswer temp = new DCIQuestionAnswer(question, answer);
									if (!questionAnswers.contains(temp)) {
										errorMsg += question.getId() + "-";
									} else {
										customerAnswers.add(new DCICustomerAnswer(dciCustomer, question, answer, null));
									}
									/*
									 * else{ Integer qaIndex =
									 * questionAnswers.indexOf(temp);
									 * DCIQuestionAnswer questionAnswer =
									 * questionAnswers.get(qaIndex);
									 * if(questionAnswer
									 * .getAnswer().getEnglish()
									 * .equalsIgnoreCase(DCIAnswer.OTHER)){
									 * String otherQuestion = "oq" +
									 * question.getId(); String otherResponse =
									 * request.getParameter(otherQuestion);
									 * if(otherResponse == null ||
									 * otherResponse.equals("")){ errorMsg +=
									 * question.getId() + "-"; } }
									 * if(questionAnswer.getAttributeData() !=
									 * null){ String sIndex =
									 * questionAnswer.getString
									 * (DCIQuestionAnswer.INDEX); if(sIndex !=
									 * null){ Integer index =
									 * Integer.valueOf(sIndex); for(int k = 0; k
									 * < index; k++ ){ String nextQ =
									 * DCIQuestionAnswer.NEXT_QUESTION + (k +
									 * 1); if(nextQ != null){
									 * pendingQuestions.add
									 * (questionAnswer.getString(nextQ)); } } }
									 * } }
									 */
								}
							} else {
								errorMsg += question.getId() + "-";
							}

						}
						if (display.equals(DCIQuestion.ANSWER_CHECKBOX)) {
							String requestQuestion = "cb" + question.getId();
							String[] requestAnswers = request.getParameterValues(requestQuestion);
							if (requestAnswers != null && requestAnswers.length > 0) {
								for (int j = 0; j < requestAnswers.length; j++) {
									String requestAnswer = requestAnswers[j];
									DCIAnswer answer = new DCIAnswer();
									answer.setId(requestAnswer);
									DCIQuestionAnswer temp = new DCIQuestionAnswer(question, answer);
									if (!questionAnswers.contains(temp)) {
										errorMsg += question.getId() + "-";
									} else {
										Integer qaIndex = questionAnswers.indexOf(temp);
										DCIQuestionAnswer questionAnswer = questionAnswers.get(qaIndex);
										String otro = question.getString(DCIQuestion.SHOW_OTHER);
										if (questionAnswer.getAnswer().getEnglish().equalsIgnoreCase(DCIAnswer.OTHER)
												&& otro != null) {
											String otherQuestion = "oq" + question.getId();
											String otherResponse = request.getParameter(otherQuestion);
											if (otherResponse == null || otherResponse.equals("")
													|| otherResponse.length() > 255) {
												errorMsg += question.getId() + "-";
											} else {
												customerAnswers.add(new DCICustomerAnswer(dciCustomer, question,
														answer, otherResponse));
											}
										} else {
											customerAnswers.add(new DCICustomerAnswer(dciCustomer, question, answer,
													null));
										}
										if (questionAnswer.getAttributeData() != null) {
											String sIndex = questionAnswer.getString(DCIQuestionAnswer.INDEX);
											if (sIndex != null) {
												Integer index = Integer.valueOf(sIndex);
												for (int k = 0; k < index; k++) {
													String nextQ = DCIQuestionAnswer.NEXT_QUESTION + (k + 1);
													if (nextQ != null) {
														pendingQuestions.add(questionAnswer.getString(nextQ));
													}
												}
											}
										}
									}
								}
							} else {
								errorMsg += question.getId() + "-";
							}
						}
					}
				}
			}
		}

		if (errorMsg.equals("")) {
			errorMsg = "success";
			dciCustomer.setFirstShow(Calendar.getInstance().getTime());
			dciCustomer.setCreationDate(Calendar.getInstance().getTime());
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC)){
				List<DCICustomerAnswer> addicionalAnswers = completeCustomerAnswers(customerAnswers, questions, dciCustomer);
				if(!addicionalAnswers.isEmpty() ){
					customerAnswers.addAll(addicionalAnswers);
				}
			}
			session.setDciCustomerAnswers(customerAnswers);
			session.setDciCustomer(dciCustomer);
		}

		model.put("text", errorMsg);

		return new ModelAndView("text", model);
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {
		Map<String, Object> model = new HashMap<>();

		String type = ServletRequestUtils.getStringParameter(request, "accountType");

		if (type == null) {
			model.put("missingPar", true);
			return model;
		}
		model.put("missingPar", false);
		model.put("accountType", type);

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<DCIAnswer> countries = session.getCustomerService().getAnswersbyType(DCIAnswerType.COUNTRY);
		String banned = "";
		for (int i = 0; i < countries.size(); i++) {
			if (countries.get(i).getAttributeData() != null) {
				if (countries.get(i).getBanned().equalsIgnoreCase("true")) {
					if (banned.equals("")) {
						banned += countries.get(i).getEnglish();
					} else {
						banned += ", " + countries.get(i).getEnglish();
					}
				}
			}
		}
		if (!banned.equals("")) {
			model.put("banned", banned);
		}

		boolean isWealth = session.isWealth(this.wealthCodes, this.preferredCodes);
		if (isWealth) {
			model.put("codes", this.volumeCodes);
		} else {
			model.put("codes", "");
		}

		return model;
	}
	
	private List<DCICustomerAnswer> completeCustomerAnswers(List<DCICustomerAnswer> custAnswers,
			List<DCIQuestion> questions, DCICustomer dciCustomer) {
		Boolean addAnswer = true;
		List<DCICustomerAnswer> listAnswers = new ArrayList<DCICustomerAnswer>();
		for (Iterator<DCIQuestion> iterator = questions.iterator(); iterator.hasNext();) {
			DCIQuestion dciQuestion = (DCIQuestion) iterator.next();
			for (Iterator<DCICustomerAnswer> iterator2 = custAnswers.iterator(); iterator2.hasNext();) {
				DCICustomerAnswer custAnswer = (DCICustomerAnswer) iterator2.next();
				if (custAnswer.getQuestion().equals(dciQuestion)) {
					addAnswer = false;
				}
			}
			if (addAnswer) {
				DCICustomerAnswer custanswer = new DCICustomerAnswer();
				DCIAnswer answer = new DCIAnswer();
				answer.setId("NO_ANSWER");
				custanswer.setAnswer(answer);
				custanswer.setQuestion(dciQuestion);
				custanswer.setDciCustomer(dciCustomer);
				listAnswers.add(custanswer);
			}
			addAnswer = true;
		}
		return listAnswers;
	}

	public String getWealthCodes() {
		return wealthCodes;
	}

	public void setWealthCodes(String wealthCodes) {
		this.wealthCodes = wealthCodes;
	}

	public String getPreferredCodes() {
		return preferredCodes;
	}

	public void setPreferredCodes(String preferredCodes) {
		this.preferredCodes = preferredCodes;
	}

	public String getVolumeCodes() {
		return volumeCodes;
	}

	public void setVolumeCodes(String volumeCodes) {
		this.volumeCodes = volumeCodes;
	}

}
