package evertec.cibp.web.form;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyRSAEntitlementRequest;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.form.bean.ChangeOOBBean;
import evertec.cibp.web.form.bean.RecoveryCodeOOBBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

public class RecoveryCodeOOBForm extends AbstractWizardFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public RecoveryCodeOOBForm() {
		super();
		setCommandClass(RecoveryCodeOOBBean.class);
		setCommandName("recoveryCodeOOB");
		setPages(new String[] { "recoveryCodeOOB", "recoveryCodeOOB" });
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			userSession.setStatusmessage("Your recovery code for 2 Step Verification has been modified.");
		else
			userSession.setStatusmessage("Tu c&oacute;digo de recuperaci&oacute;n de Verificaci&oacute;n de 2 Pasos ha sido modificado.");

		model.put("formFinished", true);
		request.getSession().removeAttribute("pageRecoveryCodeOOB");
		request.getSession().removeAttribute("oobRecoveryCode");
		
		return new ModelAndView("recoveryCodeOOB", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {
		Map<String, Object> model = new LinkedHashMap<>();
		RecoveryCodeOOBBean codeBean = (RecoveryCodeOOBBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		List<ASK_FOR_OOB> listNavOOB = userSession.getTasksListOOB();
		if(listNavOOB != null && listNavOOB.contains(ASK_FOR_OOB.GENERATE_RECOVERY_CODE)){
			userSession.getTasksListOOB().remove(ASK_FOR_OOB.GENERATE_RECOVERY_CODE);
			request.setAttribute("firstTime", false);
		}
		
		if (request.getSession().getAttribute("pageRecoveryCodeOOB") != null) {
			page = Integer.parseInt(request.getSession().getAttribute("pageRecoveryCodeOOB").toString());
			request.getSession().setAttribute("recoveryCodeOOB", command);
		}

		if (page == 1) {
			CustomerProfile customer = userSession.getCustomerProfile();
			CustomerServices customerService = userSession.getCustomerService();
			CustomerEntitlement ent = customer.getEntitlementByType(EntitlementType.RSA);

			if (ent != null) {
				CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request);

				//Generate and save Recovery Code
				try {
					String recoveryCode = RSAUtils.generateOOBCode();
					codeBean.setCode(recoveryCode);
					
					Integer codeSalt = PasswordHashUtil.genereateRandomSalt();
					String codeHash = (PasswordHashUtil.generateSaltedHash(recoveryCode, codeSalt));
					
					Boolean alwaysChallenge = (ent.getString("ALWAYSCHALLENGE").equalsIgnoreCase("FALSE")) ? false : true;
					ModifyRSAEntitlementRequest modifyRequest = new ModifyRSAEntitlementRequest(interactionEvent, 
							customer, ent.getString("ALTERNATE_PHONE"), 
							ent.getString("ALTERNATE_PROVIDER"), alwaysChallenge, false, codeHash, codeSalt.toString());
					customerService.modifyCustomerRSAEntitlement(modifyRequest);

					WebUtils.setSessionAttribute(request, "oobRecoveryCode", recoveryCode);
					model.put("recoveryCode", recoveryCode);
					customerService.cleanOOBRecoveryCodeBlock(customer);
					// Send confirmation email when generate new recovery code. 
					boolean emailSent = customerService.sendOobUpdateEmail(customer,
							userSession.getPreferredLanguage(),ChangeOOBBean.ACTION.GENERATE_RECOVERY_CODE.name());
					if(!emailSent) {
						this.logger.error(RecoveryCodeOOBForm.class.getName()+":"+"Could not send new oob recovery code confirmation email to " + customer.getUsername() + " ("
								+ customer.getEmail() + ")");
					}
				} catch (Exception e) {
					model.put("formError", true);
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Unable to modify your recovery code at this time, please try later.");
					else
						userSession.setStatusmessage("Problemas al modificar su c&oacute;digo de confirmaci&oacute;n en este momento, favor intentar luego.");
				}

			}

			if (codeBean.getCode() == null) {
				model.put("recoveryCode", (String) WebUtils.getSessionAttribute(request, "oobRecoveryCode"));
			} else {
				model.put("recoveryCode", codeBean.getCode());
			}

			request.getSession().setAttribute("pageRecoveryCodeOOB", page);
		}

		model.put("page", Integer.toString(page));
		return model;
	}

}
