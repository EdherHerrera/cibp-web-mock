package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis.encoding.Base64;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.RDCEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.WebResponseStatus;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.RemoteDepositSubmitDepositBean;
import evertec.cibp.web.request.RDCEnrollmentInfoRequest;
import evertec.cibp.web.resources.StringResources;
import evertec.cibp.web.resources.ViewResources;
import evertec.cibp.web.response.RDCEnrollmentInfoResponse;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.StringResourcesUtils;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.RDC.DepositFIS;
import evertec.otherservices.beans.RDC.DepositImageSourceFIS;
import evertec.otherservices.beans.RDC.DeviceInfoFIS;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.SubmitDepositReq;
import evertec.otherservices.responses.SubmitDepositResp;

/**
 * 
 * @author Evertec CIBP
 * @tag 1.0
 * @version 1.0
 * @since RemoteDepositSubmitDepositForm
 * @see RemoteDepositSubmitDepositForm
 */
public class RemoteDepositSubmitDepositForm extends BaseRemoteDepositForm {

    protected final Log logger = LogFactory.getLog(getClass()); // Log instance
    
    private static final double MIN_VALUE_DEPOSIT = 0.01d;

    /**
     * constructor RemoteDepositSubmitDepositForm
     */
    public RemoteDepositSubmitDepositForm() {
        super();
        final String depositInfo = "depositinfo"; // Command Name
        setCommandClass(RemoteDepositSubmitDepositBean.class);
        setCommandName(depositInfo);
        setFormView(ViewResources.REMOTE_DEPOSIT_SUBMIT);
//
    }

    /**
     * onSubmit
     * @param request
     * @param response
     * @param submitDeposit
     * @param error
     * @return ModelAndView
     * @throws Exception
     */
    @Override
    protected ModelAndView onSubmit (HttpServletRequest request, HttpServletResponse response, Object submitDeposit,
                                    BindException error) throws Exception {

    	String errorDescription = "";
    	String statusResponse = "";
    	ResponseStatus statusResponseService = null;
        Map<String, Object> model = new HashMap<>(0);
        final UserSession mUserSession = Utils.getUserSession(request); // UserSession
        RemoteDepositSubmitDepositBean depositBean = (RemoteDepositSubmitDepositBean) submitDeposit;
        final CustomerProfile profile = mUserSession.getCustomerProfile();
        final CustomerServices customerService = mUserSession.getCustomerService();
        final CustomerEntitlement rdcProfileEntitlement = profile.getEntitlementByType(EntitlementType.RDC);
        
        final boolean isValidInfo = ((rdcProfileEntitlement.getCustomerEntitlementStatus() == EntitlementStatus.ENABLED || rdcProfileEntitlement
                .getCustomerEntitlementStatus() == EntitlementStatus.OVERRIDE)
        		 && customerService.getGlobalEntitlement(EntitlementType.RDC).getStatus() == EntitlementStatus.ENABLED);
        
        if (isValidInfo) {
        	checkIfUserEnrolled(request);
            if (mUserSession.isFisEnrolled() && mUserSession.getFisAccounts() != null
                    && mUserSession.getFisAccounts().size() > 0) {
                final RDCEntitlement rdcEnt = mUserSession.getFisInfo();
                final String logInitFIS = "Init FIS submit deposit for user: " + mUserSession.getUsername();
                getLogger().info(StringEscapeUtils.escapeJava(logInitFIS));
                
                // Search the key in the hash by value
                final SubmitDepositReq submitReq = getSubmitDepositRequest(mUserSession, rdcEnt, depositBean);
                final TVFrontendAccount account = Utils.getAccountById(depositBean.getFrontEndId(), mUserSession.getFisFilterAccounts());
                final CustomerInteractionEvent interaction = getInteractionEvent(request, rdcEnt, submitReq, account);
                final SubmitDepositResp submitResp = customerService.submitDeposit(submitReq, interaction);

                final String processingError = "Processing error";
                final String depositId = "depositId";
                
                if (submitResp.getDepositId() > 0 && submitResp.getResponseStatus() == ResponseStatus.SUCCESS) {
                    model.put(depositId, submitResp.getDepositId());
                } else if (submitResp.getResponseDescription().equalsIgnoreCase(processingError)) {
                	errorDescription = StringResourcesUtils.getStringResource(messageSource, request, StringResources.REMOTE_DEPOSIT_ERROR_GENERIC);
                } else {
                	errorDescription = submitResp.getResponseDescription();
                }
                statusResponseService = submitResp.getResponseStatus();
                final String logEndFISDeposit = "End FIS submit deposit for user: " + mUserSession.getUsername();
                getLogger().info(StringEscapeUtils.escapeJava(logEndFISDeposit));
            } else {
            	errorDescription = StringResourcesUtils.getStringResource(messageSource, request, StringResources.REMOTE_DEPOSIT_ERROR_GENERIC);
            	final String errorStr = "ERROR";
            	statusResponse = errorStr;
            }
        } else {
        	final String maintenanceStr = "maintenance";
        	
        	statusResponse = maintenanceStr;
            errorDescription = StringResourcesUtils.getStringResource(messageSource, request, StringResources.REMOTE_DEPOSIT_ERROR_MAINTENANCE);
        }
        
        final String statusStr = "status";
        final String errorStr = "error";
        
        model.put(statusStr, statusResponseService != null ? statusResponseService : statusResponse);
        if (!errorDescription.isEmpty()) {
        	model.put(errorStr, errorDescription);
		}

        return showForm(request, error, ViewResources.REMOTE_DEPOSIT_SUBMIT, model);
    }

    /**
     * onBindAndValidate
     * @param request
     * @param submitDeposit
     * @param errors Exception
     * @throws Exception
     */
    protected void onBindAndValidate (HttpServletRequest request, Object submitDeposit, BindException errors)
            throws Exception {
        RemoteDepositSubmitDepositBean depositBean = (RemoteDepositSubmitDepositBean) submitDeposit;
        final UserSession mUserSession = Utils.getUserSession(request); // UserSession
        setRDCLimitFieldsFromDB(mUserSession);
        boolean errorFound = false;
        String errorMessage = "";
        if (depositBean != null) {
            final List<TVFrontendAccount> accounts = mUserSession.getFisFilterAccounts();
            final String accountId = depositBean.getFrontEndId();

            final TVFrontendAccount account = Utils.getAccountById(accountId, accounts);
            
            BigDecimal dailyAmount = null;
            BigDecimal amountToCompare = null;
            String errorMessageResource = "";

            if (StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)) {
            	dailyAmount = mDailyAmount;
            } else if (mUserSession.isWealth(wealthCodes, preferredCodes)) {
            	dailyAmount = wealthDailyAmount;
            } else {
            	dailyAmount = regularDailyAmount;
            }
            
            if (account != null && account.getCustomerAccountPreferences() != null
                    && !GenericValidator.isBlankOrNull(account.getCustomerAccountPreferences().getDepositLimit())) {
                final BigDecimal accountLimit = new BigDecimal(account.getCustomerAccountPreferences().getDepositLimit());
                amountToCompare = accountLimit;
            } else {
            	amountToCompare = dailyAmount;
            } 
            
            errorFound = true;
            if (depositBean.getAmount().compareTo(amountToCompare) > 0) {
                errorMessageResource = StringResources.REMOTE_DEPOSIT_ERROR_AMOUNT_LARGE_MAX;
            } else if (depositBean.getAmount().doubleValue() < MIN_VALUE_DEPOSIT) {
                errorMessageResource = StringResources.REMOTE_ERROR_AMOUNT;
            } else if (depositBean.getFrontImageStr() == null) {
                errorMessageResource = StringResources.REMOTE_DEPOSIT_ERROR_FRONT_IMAGE;
            } else if (depositBean.getBackImageStr() == null) {
                errorMessageResource = StringResources.REMOTE_DEPOSIT_ERROR_BACK_IMAGE;
            } else {
            	errorFound = false;
            }
            
            if (errorFound) {
            	errorMessage = StringResourcesUtils.getStringResource(messageSource, request, errorMessageResource); 
            	if (depositBean.getAmount().compareTo(amountToCompare) > 0) {
            		errorMessage += ": $" + dailyAmount;
            	}
            }
        } else {
            errorFound = true;
            errorMessage = StringResourcesUtils.getStringResource(messageSource, request, StringResources.REMOTE_DEPOSIT_ERROR_GENERIC); 
        }

        if (errorFound) {
        	if (depositBean != null) {
        		depositBean.setError(errorMessage);
			}
            errors.reject(errorMessage);
        }
    }
    
    /**
     * getDepositFIS
     * @param userSession
     * @param depositBean
     * @return deposit
     */
    private DepositFIS getDepositFIS (UserSession userSession, RemoteDepositSubmitDepositBean depositBean) {
    	DepositFIS deposit = new DepositFIS();
    	DeviceInfoFIS devInfo = new DeviceInfoFIS();
    	final DepositImageSourceFIS imgSrc = new DepositImageSourceFIS("Mobile");
    	
        deposit.setAccountId(userSession.getFisAccounts().get(depositBean.getFrontEndId()));
        deposit.setAmount(depositBean.getAmount());
        deposit.setBackImage(Base64.decode(URLDecoder.decode(depositBean.getBackImageStr())));
        deposit.setFrontImage(Base64.decode(URLDecoder.decode(depositBean.getFrontImageStr())));

        devInfo.setDeviceType(depositBean.getDeviceType());
        devInfo.setOsName(depositBean.getOsName());
        devInfo.setOsVersion(depositBean.getOsVersion());
        deposit.setDeviceInfo(devInfo);
        deposit.setImageSource(imgSrc);
        
        return deposit;
    }

    /**
     * getInteractionEvent
     * @param request
     * @param rdcEnt
     * @param submitReq
     * @param tvFrontendAccount
     * @return CustomerInteractionEvent
     */
    private CustomerInteractionEvent getInteractionEvent (HttpServletRequest request, RDCEntitlement rdcEnt, 
    		SubmitDepositReq submitReq, TVFrontendAccount tvFrontendAccount) {
    	final String userId = "FIS User Id";
    	final String accountId = "FIS Acct Id";
    	final String account = "Account";
    	final String amount = "Amount";
    	final String osName = "osName";
    	final String osVersion = "osVersion";
    	final String deviceType = "deviceType";
    	
    	CustomerInteractionEvent interaction = Utils.generateCustomerInteractionEvent(request);
    	
        interaction.setCustomStringData(userId, rdcEnt.getRdcCustomerId());
        interaction.setCustomStringData(accountId, String.valueOf(submitReq.getDeposit().getAccountId()));
        interaction.setCustomStringData(account, tvFrontendAccount.getAccountNumber());
        interaction.setCustomStringData(amount, String.valueOf(submitReq.getDeposit().getAmount()));
        interaction.setCustomStringData(osName, submitReq.getDeposit().getDeviceInfo().getOsName());
        interaction.setCustomStringData(osVersion, submitReq.getDeposit().getDeviceInfo().getOsVersion());
        interaction.setCustomStringData(deviceType, submitReq.getDeposit().getDeviceInfo().getDeviceType());
        
        return interaction;
    }
    
    /**
     * getSubmitDepositRequest
     * @param userSession
     * @param rdcEnt
     * @param depositBean
     * @return SubmitDepositReq
     */
    private SubmitDepositReq getSubmitDepositRequest (UserSession userSession, RDCEntitlement rdcEnt,
    		RemoteDepositSubmitDepositBean depositBean) {
    	SubmitDepositReq submitReq = new SubmitDepositReq();
    	
        submitReq.setDeposit(getDepositFIS(userSession, depositBean));
        submitReq.setBid(rdcBid);
        submitReq.setUserID(rdcEnt.getRdcCustomerId());
        return submitReq;
    }
    
    /**
     * checkIfUserEnrolled
     */
    private void checkIfUserEnrolled (HttpServletRequest request) {
    	final UserSession mUserSession = Utils.getUserSession(request); // UserSession
    	if (!mUserSession.isFisEnrolled()) {
        	final RDCEnrollmentInfoRequest rdcEnrollmentReq = generateEnrollmentInfoRequest(mUserSession);
        	final RDCEnrollmentInfoResponse rdcEnrollmentResp = Utils.enrollUserInRDC(rdcEnrollmentReq, mUserSession, request);

        	final boolean isUserFisEnrolled = (rdcEnrollmentResp != null && rdcEnrollmentResp.getResponseStatus() 
        			!= WebResponseStatus.SUCCESS) || rdcEnrollmentReq == null;
            if (isUserFisEnrolled) {
            	mUserSession.setFisEnrolled(false);
            	mUserSession.setFisAccounts(null);
            }
        }
    }
    
    
    /**
     * getGlobalEntitlement
     * @return EntitlementType
     */
    public EntitlementType getGlobalEntitlement() {
        return EntitlementType.RDC;
    }
    
    /**
     * getLogger
     * @return Log
     */
    public Log getLogger() {
    	return logger;
    }
    
}
