package evertec.cibp.web.form;


import evertec.cibp.campaign.loader.CampaignPaths;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.ValidationQuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.form.bean.QuestionChallengeBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.Utils;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.MobileDevice;


public class QuestionChallengeForm extends CancellableFormController implements MessageSourceAware {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;
	private CampaignPaths paths;
	private String updateiPads;
	
	public QuestionChallengeForm() {
		super();
		setCommandClass(QuestionChallengeBean.class);
		setCommandName("question");
		setFormView("question");
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();
		Map<String, Object> model = new LinkedHashMap<String, Object>();
		boolean dummyQuestions = (profile == null);

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, session.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		QuestionChallengeBean question = (QuestionChallengeBean) command;
		question.setDevice_info_rsa(request.getParameter("device_info_rsa"));
		String answer = question.getAnswer();
		if (GenericValidator.isBlankOrNull(answer)) {
			errors.rejectValue("answer", "login.username.required", "Required");
			return showForm(request, response, errors);
		}
		RSAActionRequest rsaActionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(request,
				session.getUsername()), session.getUsername(), answer, dummyQuestions ? null : profile.getEmail(), 0,
				question.isRemember(), session.getDeviceRequest());
		
		final boolean isSDKMobileRequest = !GenericValidator.isBlankOrNull(question.getDevice_info_rsa());
		
		if (isSDKMobileRequest) {
			final MobileDevice  md = new MobileDevice();
			
	        md.setMobileSdkData(question.getDevice_info_rsa());
	        session.getDeviceRequest().setDeviceIdentifier(new com.rsa.csd.ws.DeviceIdentifier[]{md}); 
	        if (!GenericValidator.isBlankOrNull(question.getRsa_cookie())) {
	        	session.getDeviceRequest().setDeviceTokenCookie(question.getRsa_cookie());
	        }
		}

		String header =session.getDeviceRequest().getUserAgent();
		DeviceUtils.getCustomerDevice(header);
		rsaActionRequest.setCustomerDevices(DeviceUtils.getCustomerDevice(header).toString());
	
		String username = session.getUsername();
		if (session.getCustomerProfile() != null)
			rsaActionRequest.setUsername(session.getCustomerProfile().getUsername());
		else if (session.getUsernameReservation() != null)
			rsaActionRequest.setUsername(session.getUsernameReservation().getUsername());
		else
			rsaActionRequest.setUsername(username);

		rsaActionRequest.setRsaSessionId(session.getRsaSessionId());
		rsaActionRequest.setRsaTransactionId(session.getRsaTransactionId());
		rsaActionRequest.setQuestion(session.getQuestion());
		rsaActionRequest.setRsaLang(Utils.getRSALanguage(request));
		ValidationQuestionResponse responseAnswer = session.getCustomerService().getSignOnCustomerServiceModule().validateRSAQuestion(rsaActionRequest);
		session.setQuestion(null);
		session.setRsaSessionId(responseAnswer.getRsaSessionId());
		session.setRsaTransactionId(responseAnswer.getRsaTransactionId());

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setPath("/");
		cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
		model.put("rsa_cookie", cookie.getValue());
		ESAPI.httpUtilities().addCookie(response, cookie);

		if (ResultStatus.SUCCESS.equals(responseAnswer.getStatusResult())) {
			session.getTasksList().remove(ASK_FOR.QUESTION_CHALLENGE);
			// if(question.isRemember())
			cookie.setPath("/");
			cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
			ESAPI.httpUtilities().addCookie(response, cookie);
			// FFEIC 2 DEFECT 519
			session.setRemember(question.isRemember());
			session.setInformationChallenge(question.isRemember());
			session.setQuestionAnswered(true);
			// END FFEIC 2
		} else if (ResultStatus.BLOCKED.equals(responseAnswer.getStatusResult())) {
			/* CIBP-1104 */
			boolean hasalert = false;
			AlertsEntitlement ent = new AlertsEntitlement(session.getCustomerProfile().getEntitlementByType(
					EntitlementType.ALERTS));
			if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
				Set<AlertType> alerts = ent.getAlerts();
				if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
						|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
					hasalert = true;
				}
			}
			/* CIBP-1104 se activa envio alerta de bloqueo de preguntas secretas */
			if (hasalert) {
				session.getCustomerService().sendQuestionsBlockedConfirmation(session.getCustomerProfile(),
						session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
						session.getPreferredLanguage());
			}
			/* FIN--- CIBP-1104 */

			// errors.rejectValue("answer", "account.block.message",
			// "Account Blocked");
			model.put("passblocked", true);
			model.put("username", username);
			request.setAttribute("firstTime", false);
			return new ModelAndView("forward:login", model);
			// return showForm(request, response, errors);
		} else if (ResultStatus.EXCEPTION.equals(responseAnswer.getStatusResult())) {
			cookie.setPath("/");
			cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
			ESAPI.httpUtilities().addCookie(response, cookie);
			request.setAttribute("firstTime", false);
			return new ModelAndView("forward:login", model);
		} else if (ResultStatus.ERROR.equals(responseAnswer.getStatusResult())) {
			errors.rejectValue("answer", "login.question.error.message", "Invalid answer");
			cookie.setPath("/");
			cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
			ESAPI.httpUtilities().addCookie(response, cookie);
			return showForm(request, response, errors);
		}
		return new ModelAndView(SignonSequenceUtils.getNextView(session));

	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		String language = Utils.getRSALanguage(request);
		Locale locale = "es".equalsIgnoreCase(language) || "sp".equalsIgnoreCase(language) ? new Locale("es")
				: Locale.ENGLISH;
		Map<String, Object> model = new HashMap<>();
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String username = session.getUsername();
		model.put("username", username);

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, session.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99
		
		if(!GenericValidator.isBlankOrNull(updateiPads) && updateiPads.equalsIgnoreCase("Y"))
			model.put("iPadUpdate", updateiPads);

		String email = null;
		if (session.getCustomerProfile() != null)
			email = session.getCustomerProfile().getEmail();

		QuestionResponse personalQuestion;

		if (session.getQuestion() == null) {
			RSAActionRequest actionRequest = new RSAActionRequest(
					Utils.generateCustomerInteractionEvent(request, session.getUsername()), session.getUsername(), "",
					email, 0, false, session.getDeviceRequest());

			if (session.getCustomerProfile() != null)
				actionRequest.setUsername(session.getCustomerProfile().getUsername());
			else if (session.getUsernameReservation() != null)
				actionRequest.setUsername(session.getUsernameReservation().getUsername());
			else
				actionRequest.setUsername(username);

			actionRequest.setDeviceRequest(session.getDeviceRequest());
			actionRequest.setRsaSessionId(session.getRsaSessionId());
			actionRequest.setRsaTransactionId(session.getRsaTransactionId());

			actionRequest.setRsaLang(Utils.getRSALanguage(request));

			personalQuestion = session.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);
		} else {
			personalQuestion = new QuestionResponse();
			personalQuestion.setStatusResult(ResultStatus.SUCCESS);
			personalQuestion.setQuestion(session.getQuestion());
		}
		if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
			String question = (String) personalQuestion.getQuestion().values().toArray()[0];
			session.setQuestion(personalQuestion.getQuestion());
			model.put("question", question);
			Map<Object, String> rememberMap = new LinkedHashMap<Object, String>();
			rememberMap.put(true, messageSource.getMessage("login.question.label.yes", null, "Yes", locale));
			rememberMap.put(false, messageSource.getMessage("login.question.label.no", null, "No", locale));
			model.put("remember", rememberMap);

			Cookie cookie = WebUtils.getCookie(request, "PMData");
			if (cookie == null)
				cookie = new Cookie("PMData", "");

			cookie.setPath("/");
			cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
			if (cookie != null) {
				model.put("devicetoken", cookie.getValue());
				model.put("rsa_cookie", cookie.getValue());
			}

		} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
			throw new ModelAndViewDefiningException(new ModelAndView("error"));
		} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
			model.put("passblocked", true);
			model.put("username", username);
			// return new ModelAndView("forward:login", model);
			throw new ModelAndViewDefiningException(new ModelAndView("forward:login", model));
		}

		String serverHostname = Utils.getServerHostname();

		model.put("serverName", serverHostname);
		/* CIBP-2353 */
		File file = new File(this.paths.getFilePath() + "fixedEnCampaign.vm");
		if (file != null && file.exists()) {
			model.put("fixedCampaign", "true");
		}
		file = new File(this.paths.getFilePath() + "divEnWantToDo.vm");
		if (file != null && file.exists()) {
			model.put("WantToDo", "true");
		}
		/* FIN CIBP-2353 */

		return model;
	}

	public CampaignPaths getPaths() {
		return paths;
	}

	public void setPaths(CampaignPaths paths) {
		this.paths = paths;
	}

	public String getUpdateiPads() {
		return updateiPads;
	}

	public void setUpdateiPads(String updateiPads) {
		this.updateiPads = updateiPads;
	}
}
