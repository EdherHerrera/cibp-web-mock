package evertec.cibp.web.form;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractStatement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.facade.AccountsFacade;
import evertec.cibp.web.form.bean.AccountServiceBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;
import evertec.dp.models.xsd.photocopy.PhotocopyResponse_Type;

public class StatementCopyForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public StatementCopyForm() {
		super();
		setCommandClass(AccountServiceBean.class);
		setCommandName("accountservices");
		setFormView("statementcopy");
		setBindOnNewForm(true);
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		AccountServiceBean searchBean = (AccountServiceBean) command;

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();

		TVFrontendAccount account = Utils.getAccountById(searchBean.getAccount(), userSession.getAccounts());
		Calendar startDate = null;
		Calendar endDate = null;

		if (searchBean.getRangeBy().equalsIgnoreCase("date")) {
			startDate = new GregorianCalendar(searchBean.getStmtStartyear(), searchBean.getStmtStartmonth(),
					searchBean.getStmtStartday());
			endDate = new GregorianCalendar(searchBean.getStmtEndyear(), searchBean.getStmtEndmonth(),
					searchBean.getStmtEndday());
		}

		else if (searchBean.getRangeBy().equalsIgnoreCase("stmt")) {

			if (searchBean.getStatementId() != null && !searchBean.getStatementId().equalsIgnoreCase("")
					&& !searchBean.getStatementId().equalsIgnoreCase("1")
					&& !searchBean.getStatementId().equalsIgnoreCase("none")) {
				// do something with statements
				AbstractStatement stmt = getStatementById(searchBean.getStatementId(), account.getStatements());

				startDate = Calendar.getInstance();
				startDate.setTime(stmt.getStartDate());
				endDate = Calendar.getInstance();
				endDate.setTime(stmt.getEndDate());
			}

			else {
				// do something with statements
				AbstractStatement stmt = getStatementById("2", account.getStatements());
				// if no selection we look for the data for current

				if (stmt != null) {
					startDate = Calendar.getInstance();
					startDate.setTime(stmt.getStartDate());
					endDate = Calendar.getInstance();
					endDate.setTime(Calendar.getInstance().getTime());
				} else {
					model.put("complete", true);
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						// userSession.setStatusmessage("No statement is available for the cycle selected.");
						model.put("message", "No statement is available for the cycle selected.");
					else
						// userSession.setStatusmessage("Estado de cuenta no existe para el ciclo seleccionado.");
						model.put("message", "Estado de cuenta no existe para el ciclo seleccionado.");
					error.rejectValue("statementId", "", "Statement not found for this account");
					return showForm(request, error, "statementcopy", model);
				}
			}
		}

		// StatementPhotocopyRequest chkrequest = new
		// StatementPhotocopyRequest(Utils.generateCustomerInteractionEvent(request),
		// customer, userSession.getBankingSession(), account, startDate,
		// endDate);
		// StatementPhotocopyResponse chkres =
		// customerService.requestStatementPhotocopy(chkrequest);
		// PhotocopyResponse_Type resp = customerService.getStatementCopy(
		// account.getAccountNumber(), startDate, endDate,
		// account.getBankId());
		// DEFECTO 792
		// Add 2 days to the end date
		if (endDate != null)
			endDate.add(Calendar.DATE, 2);
		
		PhotocopyResponse_Type resp = AccountsFacade.getInstance().getStatementCopy(account.getAccountNumber(), startDate, endDate, account.getBankId(), userSession);
		
		if (Integer.parseInt(resp.getStatusCode()) != 0) {
			model.put("complete", true);
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				// userSession.setStatusmessage("Unable to process your request of a statement copy at this time.");
				model.put("message", "Unable to process your request of a statement copy at this time.");
			else
				// userSession.setStatusmessage("Su solicitud de copia de estado de cuenta no se pudo procesar en este momento.");
				model.put("message", "Su solicitud de copia de estado de cuenta no se pudo procesar en este momento.");
		} else {
			model.put("complete", true);
			String ref = "";
			if (resp.getRefId() != null)
				ref = resp.getRefId();

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				if (ref != null && !"".equals(ref))
					ref = "Reference: " + ref;
				// userSession.setStatusmessage("Your request of statement copy was send. "
				// + ref);
				model.put("message", "Your request of statement copy was send. " + ref);
			} else {
				if (ref != null && !"".equals(ref))
					ref = "Referencia: " + ref;
				// userSession.setStatusmessage("Su solicitud de copia de estado de cuenta fue enviada exitosamente. "
				// + ref);
				model.put("message", "Su solicitud de copia de estado de cuenta fue enviada exitosamente. " + ref);
			}
		}

		return showForm(request, error, "statementcopy", model);

	}

	protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		AccountMaskUtil mask = new AccountMaskUtil();

		/************* Reference Data for Statements Photocopies *************/
		Map<String, String> stmtCpAccounts = new LinkedHashMap<>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			stmtCpAccounts.put("none", "Select an account");
		else
			stmtCpAccounts.put("none", "Selecciona una cuenta");
		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount name = (TVFrontendAccount) iterator.next();
			// if(name.getFeatures().isShowStatement() &&
			// !name.getSubtype().equals("PFC") &&
			// !name.getSubtype().equals("MLA") &&
			// !name.getSubtype().equals("PRM"))
			if (name.getFeatures().isStatementCopy() && !name.getHide()) {
				stmtCpAccounts
						.put(String.valueOf(name.getFrontEndId()),
								name.getNickname()
										+ " "
										+ mask.mask(name.getAccountNumber())
										+ ((name.getAccountNumberSuffix() != null && !name.getAccountNumberSuffix()
												.equals("")) ? " " + name.getAccountNumberSuffix() : ""));
			}
		}
		model.put("stmtCpAccounts", stmtCpAccounts);

		// months
		Map<Object, String> months = new LinkedHashMap<>();
		SimpleDateFormat df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.MONTH, 0);
		for (int i = 0; i < 12; i++) {
			String month = df.format(cal.getTime());
			months.put(i, month);
			cal.add(Calendar.MONTH, 1);
		}
		model.put("months", months);

		// days
		Map<Object, String> days = new LinkedHashMap<>();
		df = new SimpleDateFormat("d");
		cal.set(Calendar.DAY_OF_MONTH, 1);
		for (int i = 1; i <= 31; i++) {
			String day = df.format(cal.getTime());
			days.put(i, day);
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		model.put("days", days);

		// years
		Map<Object, String> years = new LinkedHashMap<>();
		df = new SimpleDateFormat("yyyy");
		cal = GregorianCalendar.getInstance();
		for (int i = 0; i <= 10; i++) {
			String year = df.format(cal.getTime());
			years.put(Integer.parseInt(year), year);
			cal.add(Calendar.YEAR, -1);
		}
		model.put("years", years);

		Map<String, String> agree = new HashMap<>();
		agree.put("agree", "");
		model.put("agree", agree);

		return model;

	}

	public AbstractStatement getStatementById(String id, List<AbstractStatement> statements) {
		for (Iterator<AbstractStatement> iterator = statements.iterator(); iterator.hasNext();) {
			AbstractStatement act = (AbstractStatement) iterator.next();
			if (act.getFrontEndId().equals(id))
				return act;
		}
		return null;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		AccountServiceBean searchBean = (AccountServiceBean) command;

		if (searchBean.getAccount() == null || searchBean.getAccount().equals("none"))
			errors.rejectValue("account", "accountservices.acct.required", "Invalid Account Selection");
		if ((searchBean.getRangeBy() != null && searchBean.getRangeBy().equals("date"))
				&& searchBean.getStatementId() == null
				|| searchBean.getStatementId().equals("none")
				&& (searchBean.getStmtStartday() == -1 || searchBean.getStmtStartyear() == -1
						|| searchBean.getStmtEndday() == -1 || searchBean.getStmtEndyear() == -1))
			errors.rejectValue("stmtEndyear", "", "Please verify the dates entered");
		else if ((searchBean.getRangeBy() != null && searchBean.getRangeBy().equalsIgnoreCase("stmt"))
				&& (searchBean.getStatementId() == null || searchBean.getStatementId().equals("none") || searchBean
						.getStatementId().equalsIgnoreCase("")))
			errors.rejectValue("statementId", "", "Please select an statement");
		if (GenericValidator.isBlankOrNull(searchBean.getAgreement())
				|| !searchBean.getAgreement().equalsIgnoreCase("agree")) {
			errors.rejectValue("agreement", "accountservices.agreement.error", "Please agree.");
		}

	}

}
