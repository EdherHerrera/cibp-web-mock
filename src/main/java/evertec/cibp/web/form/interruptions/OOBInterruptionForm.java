package evertec.cibp.web.form.interruptions;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.owasp.csrfguard.CsrfGuardException;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.eloan.nerds.services.NerdsException;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.CustomerProfileVault;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.services.modules.OOBInterruptionModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

/**
 * RSA Mobile Redesign
 *
 */
public class OOBInterruptionForm extends MultiActionController {
	
	private static final Logger logger = Logger.getLogger(OOBInterruptionForm.class);

	/** Customer Service */
	private CustomerServices customerService;
	
	/** OOB Interruption Module */
	private OOBInterruptionModule oobInterruptionModule;

	/** Get Messages */
	private MessageSource messageSource;
	private Integer skipsAvailable;
	
	private static final String PROVIDER_ARRAY = "US3,US4,PR7,US5,PR1";
	private static final String DEFAULT_CARRIER = "NOC";

	private static final String SKIPS_ENABLED = "skipsEnabled";
	private static final List<String> services = setServices();

	private static  List<String> setServices() {
		List<String> services = new ArrayList<>();
		services.add("/loginInterruptionPage");
		services.add("/oobInterruptionPage");
		services.add("/rsaQuestion");
		services.add("/validateQuestion");
		services.add("/getOobInterruptionContactInfo");
		services.add("/rsaSMSCode");
		services.add("/rsaSMSEnroll");
		services.add("/rsaSMSConfirmCode");
		services.add("/saveOOBVoicePhoneNumber");
		services.add("/updateRsaMobileProfile");
		services.add("/rsaOOBEnroll");
		services.add("/saveEventsOOBInterruption");
		services.add("/cibp-web/actions/mobileChangeEmail/validate");
		services.add("/mobileChangeEmail/validate");
		services.add("/mobileChangeEmail");
		services.add("/rsaChallengeMethod");
		services.add("rsaChallengeMethod");
		services.add("/editemail");
		services.add("editemail");
		return services;
	}
	
	/**
	 * Constructor Server
	 * @param messageSource
	 * @param customerService
	 */
	public OOBInterruptionForm(MessageSource messageSource, CustomerServices customerService, Integer skipsAvailable, 
			OOBInterruptionModule oobInterruptionModule) {
		this.customerService = customerService;
		this.messageSource = messageSource;
		this.skipsAvailable = skipsAvailable;
		this.oobInterruptionModule = oobInterruptionModule;
	}
	
	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException, Exception {
		final UserSession userSession = Utils.getUserSession(request);
		if (userSession.getIsLoginInterruptionEnable()) {
			return super.handleRequest(request,response);
	    } else {
	    	RedirectView redirectView = new RedirectView("redirect:portal");
			redirectView.setExposeModelAttributes(false);
			
			return new ModelAndView(redirectView); 
	    }
	}
	
	/**
	 * oobInterruptionPage
	 * @param request
	 * @return ModelAndView
	 * @throws CsrfGuardException 
	 * @throws Exception
	 */
	public ModelAndView oobInterruptionPage(HttpServletRequest request,HttpServletResponse response) throws NerdsException, CsrfGuardException  {
		Map<String, Object> model = new HashMap<>();

		String EDIT_RSA_PAGE = "rsaEdit";
		String ONSEN_LIBS = "onsenUI";
		String TIME_INTERVAL = "timeInterval";
		int timeInterval = 5;

		final UserSession userSession = Utils.getUserSession(request);
		Utils.generateTokenCsrfGuard(request);

		final String skipAction = request.getParameter("skipAction");
		final String nextAction = request.getParameter("nextAction");
		final CustomerProfile customerProfile = userSession.getCustomerProfile();

		Integer userSkipsNumber = RSAUtils.getUserSkips(customerProfile);
		Boolean skipsEnabled = userSkipsNumber < skipsAvailable;

		if (userSession != null && (skipAction != null || nextAction != null)) {
			ArrayList<ASK_FOR> list = new ArrayList<>(userSession.getTasksList());
			list.remove(ASK_FOR.LOGIN_INTERRUPTION_PAGE);
			userSession.setTasksList(list);

			if (skipAction != null) {
				RSAUtils.incrementUserSkips(customerService, customerProfile);
				oobInterruptionModule.logOOBInterruptionEvents(request, userSession, "oob_intpg_skip");
			}

			String viewName = SignonSequenceUtils.getNextView(userSession).replace("redirect:", "");
			String paramCloseMobile = Utils.isMobile(userSession) ? "?action=close" : "";
			RedirectView view = new RedirectView(viewName + paramCloseMobile, true);
			
			userSession.setIsLoginInterruptionEnabled(false);
			
			return new ModelAndView(view);
		}
		
		oobInterruptionModule.logOOBInterruptionEvents(request, userSession, "oob_intpg_display");

		model.put(TIME_INTERVAL, timeInterval);
		model.put(ONSEN_LIBS, true);
		model.put(EDIT_RSA_PAGE, false);
		model.put("agentType", getUserAgentMobileApp(request));
		model.put("username", userSession.getCustomerProfile().getFirstName());
		model.put(SKIPS_ENABLED, skipsEnabled);
		model.put("rsalayout", "layout/rsa-layout.vm");
		model.put("view", "vm/oobInterruptionPage.vm");

		return new ModelAndView("oobInterruptionPage", model);
	}
	
	/**
	 * getContactInfo
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 * @return
	 */
	public ModelAndView getOobInterruptionContactInfo(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		response.getLocale();
		UserSession userSession = KiuwanUtils.cast(UserSession.class,
				WebUtils.getSessionAttribute(request, "userSession"));

		model.put("rsalayout", "layout/json/json-layout.vm");
		model.put("view", "vm/oobInterruptionMessage.vm");

		if (Utils.isMobile(userSession)) {// Two Unit test test user agent
			model.put("onsenUI", true);
		}

		if (userSession.isInformationChallenge()) {
			Map.Entry<String, String> phoneDetail = getPhoneNumber(userSession, request);
			
			try {

				if (phoneDetail != null) {

					model.put("phoneType", Optional.ofNullable(phoneDetail.getKey()).filter(pd -> !pd.isEmpty())
							.orElse(StringUtils.EMPTY));

					String phoneValue = Optional.ofNullable(phoneDetail.getValue()).filter(pd -> !pd.isEmpty())
							.orElse(StringUtils.EMPTY);

					if (!GenericValidator.isBlankOrNull(phoneValue)) {
						model.put("customerPhone", String.format("(%s) ***-**%s", phoneValue.substring(0, 3),
								phoneValue.substring(phoneValue.length() - 2)));
					} else {
						model.put("customerPhone", StringUtils.EMPTY);
					}
				}

				CustomerProfile customerProfile = Optional.ofNullable(userSession.getCustomerProfile())
						.orElse(new CustomerProfile());
				final String userEmail = Optional.ofNullable(customerProfile.getEmail()).filter(e -> !e.isEmpty())
						.orElse(StringUtils.EMPTY);

				if (!userEmail.isEmpty()) {
					final String domain = userEmail.substring(userEmail.indexOf("@"), userEmail.length());

					model.put("customerEmail",
							String.format("%s****%s****%s", userEmail.substring(0, 3),
									userEmail.substring(userEmail.indexOf("@"), userEmail.indexOf("@") + 2),
									domain.substring(domain.indexOf("."), domain.length())));
				} else {
					model.put("customerEmail", StringUtils.EMPTY);

				}

			} catch (Exception ex) {
				logger.error("Error getting phone or email: " + ex.getMessage());

			}

			model.put("defCarrier", "NOC");

			model.put("result", "success");
		} else {
			model.put("result", "fail");
		}

		return new ModelAndView("rsaForm", model);
	}
	
	/**
	 * Enroll in SMS
	 * @param request
	 * @param response
	 * @throws Exception
	 * @return
	 */
	public ModelAndView rsaSMSConfirmCode(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		response.getLocale();
		UserSession userSession = KiuwanUtils.cast(UserSession.class, WebUtils.getSessionAttribute(request, "userSession"));

		model.put("rsalayout", "layout/json/json-layout.vm");
		model.put("view", "vm/rsaMessage.vm");

		if (Utils.isMobile(userSession)) {// Two Unit test test user agent
			model.put("onsenUI", true);
		}

		String code = KiuwanUtils.cast(String.class, WebUtils.getSessionAttribute(request, "smsGeneratedCode"));
		String codeNumberInp = Optional.ofNullable(request.getParameter("codeNumberInp")).filter(cn -> !cn.isEmpty())
				.orElse("");

		if (GenericValidator.isBlankOrNull(codeNumberInp) || GenericValidator.isBlankOrNull(code)
				|| !codeNumberInp.equals(code)) {// Test here
			model.put("result", "fail");
			return new ModelAndView("rsaForm", model);
		}

		model.put("result", "success");
		return new ModelAndView("rsaForm", model);
	}
	
	/**
	 * Get phone number
	 * @param userSession
	 * @param request
	 * @return String phoneNumber is exist
	 */
	protected Map.Entry<String, String> getPhoneNumber(UserSession userSession, HttpServletRequest request) {
		String ctn = StringUtils.EMPTY;
		String type = "";
		SMSCTN smsctn = null;
		
		final String CIS_NUMBER = "cis_number";
		final String CIBP_NUMBER = "cibp_number";
		
		if (userSession.getCustomerProfile() != null && userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null)   {
				SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
				if (smsprofile != null && !GenericValidator.isBlankOrNull(smsprofile.getSmsProfileId()) && smsprofile.getCtns() != null
						&& !smsprofile.getCtns().isEmpty()) {
					
					smsctn = KiuwanUtils.cast(SMSCTN.class,smsprofile.getCtns().iterator().next());
					ctn = KiuwanUtils.cast(String.class,smsctn.getCtn());
					type = CIBP_NUMBER;
			} 
		} else {
			CustomerProfileVault customerProfileVault = userSession.getCustomerProfileVault();
			ctn = Optional.ofNullable(customerProfileVault.getHomeNumber()).orElse(StringUtils.EMPTY);
			
			if (GenericValidator.isBlankOrNull(ctn)) {
				String mobilePhone = Optional.ofNullable(customerProfileVault.getMobilePhoneNumber())
						.filter(m -> !m.isEmpty()).orElse(StringUtils.EMPTY);
				String secondaryPhone = Optional.ofNullable(customerProfileVault.getSecondaryPhone())
						.filter(s -> !s.isEmpty()).orElse(StringUtils.EMPTY);

				ctn = !GenericValidator.isBlankOrNull(mobilePhone) ? mobilePhone : secondaryPhone;
			}
			type = CIS_NUMBER;
		}
		
		WebUtils.setSessionAttribute(request, "smsPhonereceived", ctn);
		WebUtils.setSessionAttribute(request, "smsProvider", "NOC");
		return new AbstractMap.SimpleEntry<String, String>(type, ctn);
	}
	
	/**
	 * applyForInterruptionPageWebVersion
	 * @param request
	 * @param userSession
	 * @return ModelAndView
	 */
	public static ModelAndView applyForInterruptionPageWebVersion (UserSession userSession) {
		ModelAndView modelAndView = null;
		if (!userSession.isElegibleToOutreach()) {
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC920_OOB_INTERRUPTION_PAGE) && RSAUtils.isOOBInterruption(userSession)) {
				if (userSession.getUserAgentType() != UserAgentType.JSON) {
					List<ASK_FOR> tasks = userSession.getTasksList();
					tasks.add(tasks.indexOf(ASK_FOR.SESSION_INIT), ASK_FOR.LOGIN_INTERRUPTION_PAGE);
					
					userSession.setIsLoginInterruptionEnabled(true);
					
					RedirectView redirectView = new RedirectView(SignonSequenceUtils.getNextView(userSession));
					redirectView.setExposeModelAttributes(false);
					
					modelAndView = new ModelAndView(redirectView); 
				}
			}
		}
		return modelAndView;
	}
	
	/**
	 * applyForInterruptionPageWebVersion
	 * @param request
	 * @param userSession
	 * @return ModelAndView
	 */
	public static void applyForInterruptionPageMobileVersion (UserSession userSession, Map<String, Object> model) {
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC920_OOB_INTERRUPTION_PAGE) && RSAUtils.isOOBInterruption(userSession)) {
			if (userSession.getUserAgentType() == UserAgentType.JSON) {
				if (userSession.isElegibleToOutreach() && !userSession.isAlreadyShownOutreach()) {
					model.put("interruptionPage", false);
				} else {
					model.put("outreach", true);
					model.put("interruptionPage", true);
				}
				
				userSession.setIsLoginInterruptionEnabled(true);
			}
		}
	}
	
	public static ModelAndView applyCompatibilityWithOuchReach (UserSession userSession) {
		ModelAndView modelView = null;
		
		if (RSAUtils.isOOBInterruption(userSession)) {
			modelView = new ModelAndView("forward:loginInterruptionPage");
	    } else {
	    	modelView = new ModelAndView("redirect:outreach?close", "close", true);
	    }
		
		return modelView;
	}
	
	protected String getUserAgentMobileApp(HttpServletRequest request) {
		final String userAgent = request.getHeader("user-agent");
		String typeAgent = "web";
		if (!GenericValidator.isBlankOrNull(userAgent)) {
			if (userAgent.contains("Android") || userAgent.contains("iPhone") ||
					userAgent.contains("MiBanco.app") ||
					userAgent.contains("WebView")) {
				typeAgent = "mobile";
			} else if (userAgent.contains("ipad")) {
				typeAgent = "ipad";
			}
		}
		return typeAgent;
	}
	
	public static boolean canContinue (UserSession userSession, HttpServletRequest request) {
		boolean result = true;
		if (userSession.getUserAgentType() != UserAgentType.JSON) {
			if (userSession.getIsLoginInterruptionEnable() 
					&& !services.contains(request.getPathInfo())) {
				result = false;
			}
		}
		return result;
	} 
	
	public ModelAndView saveEventsOOBInterruption(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> model = new HashMap<>();
		final UserSession userSession = Utils.getUserSession(request);
		String status = "";
		String eventType = Optional.ofNullable(request.getParameter("eventType")).orElse(StringUtils.EMPTY);
		
		model.put("rsalayout", "layout/json/json-layout.vm");
		model.put("view", "vm/oobInterruptionMessage.vm");
		
		if(!eventType.isEmpty()) {
			oobInterruptionModule.logOOBInterruptionEvents(request, userSession, eventType);
			status = "success";			
		}

		model.put("result", status);
		return new ModelAndView("rsaForm", model);

	}
	
	public ModelAndView saveOOBVoicePhoneNumber(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		response.getLocale();
		model.put("rsalayout", "layout/json/json-layout.vm");
		model.put("view", "vm/rsaMessage.vm");

		String phone = StringUtils.EMPTY;
		String provider = StringUtils.EMPTY;
		String result = "fail";

		Boolean resend = Optional.ofNullable(request.getParameter("resend")).filter(r -> !r.isEmpty() && Boolean.valueOf(r).equals(true)).isPresent();

		if (resend) {
			phone = KiuwanUtils.cast(String.class, WebUtils.getSessionAttribute(request, "smsPhonereceived"));
			provider = KiuwanUtils.cast(String.class, WebUtils.getSessionAttribute(request, "smsProvider"));
		} else {

			phone = Optional.ofNullable(request.getParameter("mainNumberInp")).filter(m -> !m.isEmpty())
					.orElse(StringUtils.EMPTY);
			provider = Optional.ofNullable(request.getParameter("carrier")).filter(c -> !c.isEmpty())
					.orElse(StringUtils.EMPTY);

		}
		if (GenericValidator.isLong(phone) && (PROVIDER_ARRAY.contains(provider))
				|| (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)
						&& provider.equalsIgnoreCase(DEFAULT_CARRIER))) {
			WebUtils.setSessionAttribute(request, "smsGeneratedCode", "OOBPHONECODE");
			WebUtils.setSessionAttribute(request, "smsPhonereceived", phone);
			WebUtils.setSessionAttribute(request, "smsProvider", provider);

			result = "success";
		}

		model.put("result", result);
		return new ModelAndView("rsaForm", model);
	}
	
	/**
	 * @return the customerService
	 */
	public CustomerServices getCustomerService() {
		return customerService;
	}

	/**
	 * @param customerService the customerService to set
	 */
	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public Integer getSkipsAvailable() {
		return skipsAvailable;
	}

	public void setSkipsAvailable(Integer skipsAvailable) {
		this.skipsAvailable = skipsAvailable;
	}

	/**
	 * @return the oobInterruptionModule
	 */
	public OOBInterruptionModule getOobInterruptionModule() {
		return oobInterruptionModule;
	}

	/**
	 * @param oobInterruptionModule the oobInterruptionModule to set
	 */
	public void setOobInterruptionModule(OOBInterruptionModule oobInterruptionModule) {
		this.oobInterruptionModule = oobInterruptionModule;
	}
	
}