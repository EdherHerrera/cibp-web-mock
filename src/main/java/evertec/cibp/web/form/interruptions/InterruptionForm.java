package evertec.cibp.web.form.interruptions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import com.eloan.nerds.services.NerdsException;

import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;

public class InterruptionForm extends MultiActionController {

	public InterruptionForm() {
		
	}
	/**
	 * checkLoginInterruptionPage
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws Exception
	 */
	public ModelAndView loginInterruptionPage (HttpServletRequest request, HttpServletResponse response) throws NerdsException {
		final UserSession userSession = KiuwanUtils.cast(UserSession.class, WebUtils.getSessionAttribute(request, "userSession")); // user session
		
		ModelAndView modelView = null;

		if (userSession != null && RSAUtils.isOOBInterruption(userSession)) {
			modelView = new ModelAndView("forward:oobInterruptionPage");
		}
		
		return modelView;
	}

}
