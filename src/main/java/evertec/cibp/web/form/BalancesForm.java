package evertec.cibp.web.form;

import bppr.sms.beans.SMSAccount;
import bppr.sms.beans.SMSEbppAccount;
import bppr.sms.beans.SMSProfile;
import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.EventType;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.AlertCondition;
import evertec.cibp.core.models.customer.CustomerBalanceAlert;
import evertec.cibp.core.models.customer.CustomerEmailBalanceAlert;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.EmailAlertCondition;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.SMSEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.AddSMSAccountRequest;
import evertec.cibp.core.services.requests.ModifyCustomerAlertsRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.AlertsBalanceFacade;
import evertec.cibp.web.facade.AlertsFacade;
import evertec.cibp.web.form.bean.BalancesBean;
import evertec.cibp.web.form.mobile.bean.MobileAlert;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BalancesForm extends CancellableFormController implements MessageSourceAware {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(PersonalInformationChangesForm.class);
	public boolean isSubmit = false;
	
	private static final String USERSESSION = "userSession";
	/** Logger */
	private MessageSource messageSource;
	
	// MBSFE-633
	private String alertsProductTypes;
	// END MBSFE-633

	public BalancesForm() {
		super();
		setCommandClass(BalancesBean.class);
		setCommandName("balances");
		setFormView("balances");
	}

	@SuppressWarnings("unused")
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();
		AlertsEntitlement entitlement = null;
		boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);
		if (!profile.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement alert = new AlertsEntitlement();
			customerService.getCustomerProfileService().addEntitlement(profile, alert.getCustomerEntitlement());
		}

		entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		Set<AlertType> customeralerts = new HashSet<>();

		if (entitlement != null && entitlement.getAlerts() != null)
			customeralerts = entitlement.getAlerts();

		ModifyCustomerAlertsRequest req = new ModifyCustomerAlertsRequest();
		req.setCustomerProfile(userSession.getCustomerProfile());
		req.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
		
		// MBSD-2002 RSA Upgrade Recommendations
		Cookie cookie = new Cookie("PMData", userSession.getDeviceRequest().getDeviceTokenCookie());
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		ESAPI.httpUtilities().addCookie(response, cookie);
		
		if(oobFlag && userSession.getOobEnroll() && userSession.isOobAuthenticated()) {
			@SuppressWarnings("unchecked")
			Map<Object, Object> parameters = (Map<Object, Object>) WebUtils.getSessionAttribute(request,
					ChallengeInformationUtils.INFORMATION_PARAMETERS);
			if(parameters != null && parameters.get(ChallengeInformationUtils.BALANCE_ACTION) != null) {
				BalancesBean responseBean = (BalancesBean) parameters.get(ChallengeInformationUtils.BALANCE_ACTION);
				org.apache.commons.beanutils.BeanUtils.copyProperties(command, responseBean);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.SHOW_UPDATE_ALERT, userSession.isOobAuthenticated());
			}
		}
		BalancesBean a = (BalancesBean) command;
		String emailalertgt = a.isEmailalertgt() ? "true" : "false";
		String smsalertgt = a.isSmsalertgt() ? "true" : "false";
		String emailalertlt = a.isEmailalertlt() ? "true" : "false";
		String smsalertlt = a.isSmsalertlt() ? "true" : "false";
		String amountgt = a.getAmountgt() == null ? "" : a.getAmountgt();
		String amountlt = a.getAmountlt() == null ? "" : a.getAmountlt();

		final CustomerInteractionEvent customerInteractionEvent = Utils.generateCustomerInteractionEvent(request); //customerInteractionEvent
		
		try {
			// retrives account list that user have with any alert setted
			List<SMSEbppAccount> smsebppaccounts = userSession.getSmsEbppaccounts(customerInteractionEvent);
			List<CustomerEmailBalanceAlert> emailaccounts = userSession.getEmailaccounts(customerInteractionEvent);
			List<SMSAccount> smsaccounts = userSession.getSmsaccounts(customerInteractionEvent);
			CustomerEmailBalanceAlert beanAlert = new CustomerEmailBalanceAlert();
			TVFrontendAccount frontact = null;

			if (a.getAccount() != null && !"none".equalsIgnoreCase(a.getAccount()))
				frontact = Utils.getAccountById(a.getAccount(), userSession.getAccounts());

			if (frontact == null) {
				model.put("text", false);
				return new ModelAndView("text", model);
			}
			// Sets common data
			beanAlert.setAccountNumber(frontact.getAccountNumber());
			beanAlert.setAccountProductId(frontact.getProductDefinition().getProductId());
			beanAlert.setAccountSection(frontact.getAccountSection());
			beanAlert.setAccountSubtype(frontact.getSubtype());
			beanAlert.setCustomerProfile(userSession.getCustomerProfile());
			beanAlert.setNickname(frontact.getNickname());
			beanAlert.setPermId(userSession.getCustomerProfile().getPermId());
			beanAlert.setAlertposicion("X");
			String language = "";
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				language = "I";
			else
				language = "E";

			beanAlert.setAlertlanguage(language);

			SMSEntitlement smsent = null;
			if (a.isSearching()) { // if selects any account from dropdown list,
									// search for all existing email alerts
				if (customeralerts.contains(AlertType.BALANCE_EMAIL)) {
					for (Iterator<CustomerEmailBalanceAlert> iterator = emailaccounts.iterator(); iterator.hasNext();) {
						CustomerEmailBalanceAlert account2 = (CustomerEmailBalanceAlert) iterator.next();
						if (account2.getAccountNumber().equals(frontact.getAccountNumber())
								&& account2.getAccountSection().equalsIgnoreCase(frontact.getAccountSection())
								&& account2.getAccountSubtype().equalsIgnoreCase(
										frontact.getProductDefinition().getProductType())
								&& account2.getAccountProductId().equalsIgnoreCase(
										frontact.getProductDefinition().getProductId())) { // Search
																							// for
																							// email
																							// alerts
							if (account2.getAlertCondition().getOperator().equals("GT")) {
								amountgt = account2.getAlertCondition().getAmountHigh().toString();
								emailalertgt = "true";
								a.setEmailalertgt(true);
							} else if (account2.getAlertCondition().getOperator().equals("LT")) {
								amountlt = account2.getAlertCondition().getAmountLow().toString();
								emailalertlt = "true";
								a.setEmailalertlt(true);
							}
						}
					}
				}
				int iAmountgt;
				int iAmountlt;
				try {
					iAmountgt = Integer.parseInt(amountgt);
				} catch (NumberFormatException nfEx) {
					iAmountgt = 0;
				} catch (Exception Ex) {
					iAmountgt = 0;
				}
				try {
					iAmountlt = Integer.parseInt(amountlt);
				} catch (NumberFormatException nfEx) {
					iAmountlt = 0;
				} catch (Exception Ex) {
					iAmountlt = 0;
				}
				if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)) {
					for (Iterator<SMSAccount> iterator = smsaccounts.iterator(); iterator.hasNext();) {
						SMSAccount account2 = (SMSAccount) iterator.next();
						if (account2.getAccountNumber().equals(frontact.getAccountNumber())
								&& account2.getAccountSection().equalsIgnoreCase(frontact.getAccountSection())
								&& account2.getAccountSubtype().equalsIgnoreCase(
										frontact.getProductDefinition().getProductType())
								&& account2.getAccountProductId().equalsIgnoreCase(
										frontact.getProductDefinition().getProductId())) { // Search
																							// for
																							// email
																							// alerts
							if(account2.getAlertCondition()!= null 
									&& account2.getAlertCondition().getOperator() != null){
								if (account2.getAlertCondition().getOperator().equals("GT")) {
									if (iAmountgt == 0)
										amountgt = account2.getAlertCondition().getAmountHigh().toString();
									smsalertgt = "true";
									a.setSmsalertgt(true);
								} else if (account2.getAlertCondition().getOperator().equals("LT")) {
									if (iAmountlt == 0)
										amountlt = account2.getAlertCondition().getAmountLow().toString();
									smsalertlt = "true";
									a.setSmsalertlt(true);
								}
							}							
						}
					}
					

				}
				model.put("account", frontact);
			} else {
				Boolean informationChallenge = userSession.isInformationChallenge();
				if (!informationChallenge
						&& (userSession.getGuidedSetupSession() == null || !userSession.getGuidedSetupSession()
								.isGuidedSetupMode())) {
					if(oobFlag && userSession.getOobEnroll()) {
						model.put("oobEnrolled", true);
						model.put("oobChallenge", "loginoob");
					} else if (userSession.getInformationQuestion() != null) {
						String question = (String) userSession.getInformationQuestion().values().toArray()[0];
						model.put("question", question);
					}
					model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, userSession.isInformationChallenge());
				} else {
					

					if (a.isEmailalertgt() || a.isEmailalertlt()) { // if any
																	// email
																	// alert is
																	// checked
																	// then
						if (!customeralerts.contains(AlertType.BALANCE_EMAIL)) { // verify
																					// if
																					// have
																					// "balance email"
																					// alert
																					// type
							customeralerts.add(AlertType.BALANCE_EMAIL);
							req.setAlerts(customeralerts);
							profile = customerService.modifyCustomerAlerts(req);
							userSession.setCustomerProfile(profile);
						}
					}

					if (!a.isEmailalertgt() || !a.isEmailalertlt()) { // if any
																		// email
																		// alert
																		// is
																		// unchecked
						if (!a.isEmailalertgt()) { // if alert were exists,
													// delete
							String alertId = findEmailAlert(emailaccounts, frontact.getAccountNumber(),
									frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
									frontact.getProductDefinition().getProductId(), "GT");
							boolean isAlertDeleted = false;
							if (!alertId.equals("")) {
								beanAlert.setId(alertId);
								/**
								 * MBNA-3493
								 * Method DeleteCustomerEmailBalanceAlert :CIBP-WEB microservice integration
								 * 
								 * */
								isAlertDeleted = AlertsFacade.getInstance().deleteCustomerEmailBalanceAlert(beanAlert);
							}
							if (isAlertDeleted) {
								emailalertgt = "false";
								a.setEmailalertgt(false);
								a.setEmailalertgtmod(false);
							}
						}
						if (!a.isEmailalertlt()) { // if alert were exists,
													// delete
							String alertId = findEmailAlert(emailaccounts, frontact.getAccountNumber(),
									frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
									frontact.getProductDefinition().getProductId(), "LT");
							boolean isAlertDeleted = false;
							if (!alertId.equals("")) {
								beanAlert.setId(alertId);
								/**
								 * MBNA-3493
								 * Method DeleteCustomerEmailBalanceAlert :CIBP-WEB microservice integration
								 * 
								 * */
								isAlertDeleted = AlertsFacade.getInstance().deleteCustomerEmailBalanceAlert(beanAlert);
							}
							if (isAlertDeleted) {
								emailalertlt = "false";
								a.setEmailalertlt(false);
								a.setEmailalertltmod(false);
							}
						}
					}

					// if any email alert is checked, firts time or any amount
					// is modified
					if (a.isEmailalertgt()) { // if email checkmark is
												// checked...
												// ALERT_CONDITION_OPERATOR = GT
						EmailAlertCondition alertCond = new EmailAlertCondition();
						alertCond.setAmountHigh(new BigDecimal(a.getAmountgt()));
						alertCond.setOperator("GT");
						beanAlert.setAlertCondition(alertCond);
						/** Insert, Update or Delete **/
						String alertId = findEmailAlert(emailaccounts, frontact.getAccountNumber(),
								frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
								frontact.getProductDefinition().getProductId(), "GT");
						if (!alertId.equals(""))
							beanAlert.setId(alertId);
						if (!alertId.equals("")) {
							/**
							 * MBNA-3502
							 * Method UpdateCustomerEmailBalanceAlert : CIBP-WEB microservice integration
							 * 
							 */
							//Microservice implementation
							CustomerEmailBalanceAlert beanAlertConfirmation = AlertsFacade.getInstance().updateCustomerEmailBalanceAlert(beanAlert, userSession);
							emailalertgt = "true";
							a.setEmailalertgt(true);
							a.setEmailalertgtmod(true);
						} else if (alertId.equals("")) { 
							// if alert not exists
							// then insert
							
							/**
							 * MBNA-3499
							 * Method InsertCustomerEmailBalanceAlert : CIBP-WEB microservice integration
							 * 
							 */
							//Microservice implementation
							CustomerEmailBalanceAlert beanAlertConfirmation = AlertsFacade.getInstance().insertCustomerEmailBalanceAlert(beanAlert, userSession);
							
							emailalertgt = "true";
						}
					}
					if (a.isEmailalertlt()) { // if email checkmark is
												// checked...
												// ALERT_CONDITION_OPERATOR = LT
						EmailAlertCondition alertCond = new EmailAlertCondition();
						alertCond.setAmountLow(new BigDecimal(a.getAmountlt()));
						alertCond.setOperator("LT");
						beanAlert.setAlertCondition(alertCond);
						/** Insert, Update or Delete **/
						String alertId = findEmailAlert(emailaccounts, frontact.getAccountNumber(),
								frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
								frontact.getProductDefinition().getProductId(), "LT");
						if (!alertId.equals(""))
							beanAlert.setId(alertId);
						if (!alertId.equals("")) { 
							/**
							 * MBNA-3502
							 * Method UpdateCustomerEmailBalanceAlert : CIBP-WEB microservice integration
							 * 
							 */
							//Microservice implementation
							CustomerEmailBalanceAlert beanAlertConfirmation = AlertsFacade.getInstance().updateCustomerEmailBalanceAlert(beanAlert, userSession);
							emailalertlt = "true";
							a.setEmailalertlt(true);
							a.setEmailalertltmod(true);
						} else if (alertId.equals("")) { 
							// if alert not exists
							// then insert
							
							/**
							 * MBNA-3499
							 * Method InsertCustomerEmailBalanceAlert : CIBP-WEB microservice integration
							 * 
							 */
							//Microservice implementation
							CustomerEmailBalanceAlert beanAlertConfirmation = AlertsFacade.getInstance().insertCustomerEmailBalanceAlert(beanAlert, userSession);
							
							emailalertlt = "true";
						}
					}
					
					if (a.isSmsalertgt() || a.isSmsalertlt()) { // if any sms
																// alert is
																// checked then
						if (profile.hasEntitlement(EntitlementType.MOBILE_BANKING))
							smsent = new SMSEntitlement(profile.getEntitlementByType(EntitlementType.MOBILE_BANKING));
					}

					String smsAlertId = "";
					SMSProfile SmsProfile = userSession.getSmsProfile(customerInteractionEvent);


					if (!a.isSmsalertgt() || !a.isSmsalertlt()) { // if any sms
																	// alert is
																	// unchecked
						if (!a.isSmsalertgt()) { // if alert were exists, delete
							smsAlertId = findSmsAlert(smsaccounts, frontact.getAccountNumber(),
									frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
									frontact.getProductDefinition().getProductId(), "GT");
							if (!smsAlertId.equals("")) {
								customerService.DeleteSMSAccount(SmsProfile, smsAlertId);
								smsalertgt = "false";
								a.setSmsalertgt(false);
								a.setSmsalertgtmod(false);
							}
						}
						if (!a.isSmsalertlt()) { // if alert were exists, delete
							smsAlertId = findSmsAlert(smsaccounts, frontact.getAccountNumber(),
									frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
									frontact.getProductDefinition().getProductId(), "LT");
							if (!smsAlertId.equals("")) {
								customerService.DeleteSMSAccount(SmsProfile, smsAlertId);
								smsalertlt = "false";
								a.setSmsalertlt(false);
								a.setSmsalertltmod(false);
							}
						}
						
					}

					SMSAccount smsact = new SMSAccount();
					// SMSEbppAccount smsebppact = new SMSEbppAccount();
					if (a.isSmsalertgt()) { // if sms checkmark is checked...
											// ALERT_CONDITION_OPERATOR = GT
						AddSMSAccountRequest re = new AddSMSAccountRequest(customerInteractionEvent, profile,
								SmsProfile, smsent, frontact.getAccountNumber(), frontact.getSubtype(),
								frontact.getAccountSection(), frontact.getNickname(), frontact.getProductDefinition()
										.getProductId(), "X");
						re.setConditionHigh(new BigDecimal(a.getAmountgt()));
						re.setCondition("GT");
						/** Insert, Update or Delete **/
						String alertId = findSmsAlert(smsaccounts, frontact.getAccountNumber(),
								frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
								frontact.getProductDefinition().getProductId(), "GT");

						if (!alertId.equals("")) { // if exists alert and
													// smsalertgt is unchecked
													// then delete
							customerService.DeleteSMSAccount(SmsProfile, alertId);
							smsalertgt = "true";
							a.setSmsalertgt(true);
							a.setSmsalertgtmod(true);
							smsact = customerService.addSMSAccountWithAlertCondition(re);
						} else if (alertId.equals("")) { // if alert does not
															// exists and
															// smsalertgt is
															// checked then
															// insert
							re.setAddAlert(true);
							smsact = customerService.addSMSAccountWithAlertCondition(re);
							smsalertgt = "true";
						}
					}
					if (a.isSmsalertlt()) { // if sms checkmark is checked...
											// ALERT_CONDITION_OPERATOR = GT
						AddSMSAccountRequest re = new AddSMSAccountRequest(customerInteractionEvent, profile,
								SmsProfile, smsent, frontact.getAccountNumber(), frontact.getSubtype(),
								frontact.getAccountSection(), frontact.getNickname(), frontact.getProductDefinition()
										.getProductId(), "X");
						re.setConditionLow(new BigDecimal(a.getAmountlt()));
						re.setCondition("LT");
						/** Insert, Update or Delete **/
						String alertId = findSmsAlert(smsaccounts, frontact.getAccountNumber(),
								frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
								frontact.getProductDefinition().getProductId(), "LT");

						if (!alertId.equals("")) { // if exists alert and
													// smsalertlt is unchecked
													// then delete
							customerService.DeleteSMSAccount(SmsProfile, alertId);
							smsalertlt = "true";
							a.setSmsalertlt(true);
							a.setSmsalertltmod(true);
							smsact = customerService.addSMSAccountWithAlertCondition(re);
						} else if (alertId.equals("")) { // if alert not exists
															// and smsalertlt is
															// checked then
															// insert
							re.setAddAlert(true);
							smsact = customerService.addSMSAccountWithAlertCondition(re);
							smsalertlt = "true";
						}
					}
					
					updateCustomerBalanceAlert(userSession, a, request, frontact, language);
					
					model.put("AlertUpdated", "true");
				}
			}

		} catch (RuntimeException e) {
			logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
			model.put("text", false);

			return new ModelAndView("text", model);
		}
		if (!a.isEmailalertgt() && !a.isSmsalertgt()) { // if both sms and email
														// alert are unchecked,
														// clean amount value.
			amountgt = "";
		}
		if (!a.isEmailalertlt() && !a.isSmsalertlt()) { // if both sms and email
														// alert are unchecked,
														// clean amount value.
			amountlt = "";
		}
		userSession.resetEmailAccounts();
		userSession.resetSmsAccounts();
		userSession.resetSmsEbppAccounts();
		userSession.resetBalanceAlerts();

		model.put("emailalertgt", emailalertgt);
		model.put("smsalertgt", smsalertgt);
		model.put("emailalertlt", emailalertlt);
		model.put("smsalertlt", smsalertlt);
		model.put("amountgt", amountgt);
		model.put("amountlt", amountlt);

		return new ModelAndView("alertsSearching", model);
	}


	public static void updateCustomerBalanceAlert(UserSession userSession, BalancesBean balancesBean, HttpServletRequest request, 
			TVFrontendAccount frontact, String language) {
		if (Utils.isPushEnabled()) {
			
			CustomerServices customerService = userSession.getCustomerService();
			CustomerProfile profile = userSession.getCustomerProfile();
			CustomerBalanceAlert customerBalanceAlertLT = Utils.findBalanceAlert(userSession.getBalanceAlerts(), frontact.getAccountNumber(), 
					frontact.getAccountSection(), MobileAlert.LESS_THAN);
			CustomerInteractionEvent customerBalanceAlertInteractionEvent = null; // CustomerInteractionEvent
			
			if (customerBalanceAlertLT != null) {
				if (!balancesBean.isEmailalertlt() && !balancesBean.isSmsalertlt() && !customerBalanceAlertLT.isChannelPushActive()) {
					// if pushChannel is inactive, and is deleting email and sms, then delete alert.
					AlertsBalanceFacade.getInstance().deleteCustomerBalanceAlert(userSession, customerBalanceAlertLT);
				}
				// if push is active and is deleting email and sms, dont't delete alert, instead modified it;
			} else {
				// if customerBalanceAlertLT == null create new balance alert
				customerBalanceAlertLT = new CustomerBalanceAlert();
			}
			
			if (!GenericValidator.isBlankOrNull(balancesBean.getAmountlt()) && 
					(balancesBean.isEmailalertlt() || balancesBean.isSmsalertlt()) || customerBalanceAlertLT.isChannelPushActive()) {
				// if amout is != null, if adding email o sms o push channel is active insert or update alert;
				
				AlertCondition alertCondition = new AlertCondition(MobileAlert.LESS_THAN, new BigDecimal(balancesBean.getAmountlt()));
				
				customerBalanceAlertLT = Utils.updateCustomerBalanceAlert(customerBalanceAlertLT, profile, language, frontact);
				customerBalanceAlertLT.setAlertCondition(alertCondition);
				customerBalanceAlertLT.setIsChannelEmailActive(balancesBean.isEmailalertlt());
				customerBalanceAlertLT.setIsChannelSMSActive(balancesBean.isSmsalertlt());
				if (balancesBean.isSmsalertlt()){
					if (customerBalanceAlertLT.isChannelPushActive()) {
						// if push channel is active and sms to activate create log delete push alert
						customerBalanceAlertInteractionEvent = Utils.createCustomerBalanceAlertInteractionEvent(request, customerBalanceAlertLT);									
					}
					customerBalanceAlertLT.setIsChannelPushActive(false);
				}
				
				boolean saveSuccess = true;
				
				try {
					AlertsBalanceFacade.getInstance().insertCustomerBalanceAlert(userSession, customerBalanceAlertLT);
				} catch (Exception Ex) {
					saveSuccess = false;
				}
				
				if (customerBalanceAlertInteractionEvent != null) {
					customerService.logPushNotificationDeleteEvent(customerBalanceAlertInteractionEvent, saveSuccess);
					
					// clear customerBalanceAlertInteractionEvent to reuse variable 
					customerBalanceAlertInteractionEvent = null;
				}
			}
			
			CustomerBalanceAlert customerBalanceAlertGT = Utils.findBalanceAlert(userSession.getBalanceAlerts(), frontact.getAccountNumber(), 
					frontact.getAccountSection(), MobileAlert.GREATER_THAN);
			
			if (customerBalanceAlertGT != null) {
				if (!balancesBean.isEmailalertgt() && !balancesBean.isSmsalertgt() && !customerBalanceAlertGT.isChannelPushActive()) {
					// if pushChannel is inactive, and is deleting email and sms, then delete alert.
					AlertsBalanceFacade.getInstance().deleteCustomerBalanceAlert(userSession, customerBalanceAlertGT);
				}
				// if push is active and is deleting email and sms, dont't delete alert, instead modified it;
			} else {
				// if customerBalanceAlertLT == null create new balance alert
				customerBalanceAlertGT = new CustomerBalanceAlert();
			}
			
			if (!GenericValidator.isBlankOrNull(balancesBean.getAmountlt()) && 
					(balancesBean.isEmailalertgt() || balancesBean.isSmsalertgt()) || customerBalanceAlertGT.isChannelPushActive()) {
				// if amout is != null, if adding email o sms o push channel is active insert or update alert;
				
				
				AlertCondition alertCondition = new AlertCondition(MobileAlert.GREATER_THAN, new BigDecimal(balancesBean.getAmountgt()));
				
				customerBalanceAlertGT = Utils.updateCustomerBalanceAlert(customerBalanceAlertGT, profile, language, frontact);
				customerBalanceAlertGT.setAlertCondition(alertCondition);
				customerBalanceAlertGT.setIsChannelEmailActive(balancesBean.isEmailalertgt());
				customerBalanceAlertGT.setIsChannelSMSActive(balancesBean.isSmsalertgt());
				if (balancesBean.isSmsalertgt()){
					if (customerBalanceAlertGT.isChannelPushActive()) {
						
						// if push channel is active and sms to activate create log delete push alert
						
						customerBalanceAlertInteractionEvent = Utils.createCustomerBalanceAlertInteractionEvent(request, customerBalanceAlertGT);									
					}
					customerBalanceAlertGT.setIsChannelPushActive(false);

				}
				boolean saveSuccess = true;
				
				try {
					AlertsBalanceFacade.getInstance().insertCustomerBalanceAlert(userSession, customerBalanceAlertGT);
				} catch (Exception Ex) {
					saveSuccess = false;
				}
				
				if (customerBalanceAlertInteractionEvent != null) {
					customerService.logPushNotificationDeleteEvent(customerBalanceAlertInteractionEvent, saveSuccess);
				}
			}
		}
	}

    /**
    * @return messageSource variable
    */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);

		AccountMaskUtil maskutil = new AccountMaskUtil();
		Map<Object, Object> accountsMap = new LinkedHashMap<>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			accountsMap.put("none", "Select an account");
		else
			accountsMap.put("none", "Seleccione una cuenta");

		List<TVFrontendAccount> accounts = userSession.getAccounts();
		// List<Subscription> estmts =
		// userSession.getEBPPSubscriptions(EbillSubscriptionType.ESTATEMENT,this.getEBPPMessageSourceStatus());
		// List<Subscription> ebills =
		// userSession.getEBPPSubscriptions(EbillSubscriptionType.EBILLS,this.getEBPPMessageSourceStatus());

		// if(estmts != null && estmts.size()>0)
		// {
		// for (Iterator<TVFrontendAccount> iterator = accounts.iterator();
		// iterator.hasNext();) {
		// TVFrontendAccount act = (TVFrontendAccount) iterator.next();
		//
		// if(act.getFeatures().isEStatement()){
		// for (Iterator<Subscription> iterator2 = estmts.iterator();
		// iterator2.hasNext();) {
		// Subscription sub = (Subscription) iterator2.next();
		//
		// if(sub.getCustAcctNumber().trim().matches("\\d+") &&
		// sub.getCustAcctNumber().trim().length() < 18 &&
		// act.getAccountNumber().trim().length() < 18 &&
		// act.getAccountNumber().trim().matches("\\d+"))
		// {
		// if(Long.parseLong(act.getAccountNumber().trim()) ==
		// Long.parseLong(sub.getCustAcctNumber().trim()))
		// {
		// act.setEbppSubscription(sub);
		// break;
		// }
		// }
		// else
		// if(sub.getCustAcctNumber().trim().equalsIgnoreCase(act.getAccountNumber().trim()))
		// {
		// act.setEbppSubscription(sub);
		// break;
		// }
		// }
		// }
		//
		// }
		// }
		// if(ebills != null && ebills.size() >0)
		// {
		// for (Iterator<Subscription> iterator = ebills.iterator();
		// iterator.hasNext();) {
		// Subscription ebill = (Subscription) iterator.next();
		// if(ebill != null)
		// {
		// String actnumber = ebill.getCustAcctNumber();
		// if(actnumber.startsWith("377") && actnumber.length() ==15)
		// actnumber = "0" + actnumber;
		//
		// TVFrontendAccount account =
		// Utils.getAccountByAccountNumber(actnumber, accounts);
		// if(account != null){
		// account.setEbppSubscription(ebill);
		// }
		// }
		// }
		// }		

		for (TVFrontendAccount account : accounts) {
			if (!account.getHide()) { // CIBP-1104 para mostrar solo las activas
				if (account.getFeatures().isMobileBanking()) {	
					// MBSFE-633
					// Según se indica en los criterios de aceptación, se valida
					// que se presenten unicamente tarjetas de tipo CCA,IDA,PRM
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE633)) {
						boolean withoutProdTypeRe = false; // Without Product
															// Type Restriction
						withoutProdTypeRe = Utils.haveType(this.alertsProductTypes, account.getSubtype());

						if (withoutProdTypeRe) {
							String sux = "";
							if (account.getAccountNumberSuffix() != null)
								sux = account.getAccountNumberSuffix();
							accountsMap.put(account.getFrontEndId(), account.getNickname() + " "
									+ maskutil.mask(account.getAccountNumber()) + " " + sux);
						}

					} else {
						String sux = "";
						if (account.getAccountNumberSuffix() != null)
							sux = account.getAccountNumberSuffix();
						accountsMap.put(account.getFrontEndId(),
								account.getNickname() + " " + maskutil.mask(account.getAccountNumber()) + " " + sux);
					}
					// MBSFE-633
				}
			}
		}
		if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING))
			model.put("mobilealert", true);
		else
			model.put("mobilealert", false);

		model.put("accounts", accountsMap);

		/**
		 * oob flag and oob enroll verification 
		 * */
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && userSession.getOobEnroll()) {
			if(userSession.getTasksListOOB() != null) {
				if(userSession.getTasksListOOB() != null && !userSession.getTasksListOOB().contains(ASK_FOR_OOB.OOB_CHALLENGE)) {
					userSession.getTasksListOOB().add(0,ASK_FOR_OOB.OOB_CHALLENGE);
				}
				if(userSession.getTasksListOOB() != null && !userSession.getTasksListOOB().contains(ASK_FOR_OOB.BALANCE)) {
					userSession.getTasksListOOB().add(ASK_FOR_OOB.BALANCE);
				}
			} else {
				List<ASK_FOR_OOB> listNavOOB = new ArrayList<>();
				listNavOOB.add(ASK_FOR_OOB.OOB_CHALLENGE);
				listNavOOB.add(ASK_FOR_OOB.ALERT_TYPES);
				listNavOOB.add(ASK_FOR_OOB.BALANCE);
				userSession.setTasksListOOB(listNavOOB);
			}
			model.put("oobEnrolled", true);
			userSession.setInformationChallenge(userSession.isOobAuthenticated());
		}
		// CIBP-2804
		if (userSession.getGuidedSetupSession() != null && userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, true);
		} else {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, userSession.isInformationChallenge());
		}
		Boolean showUpdate = (Boolean) WebUtils.getSessionAttribute(request,
				ChallengeInformationUtils.SHOW_UPDATE_ALERT);

		if (showUpdate != null && showUpdate) {
			model.put("showUpdate", true);
		} else {
			model.put("showUpdate", false);
		}

		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.SHOW_UPDATE_ALERT, null);

		Boolean showBalance = (Boolean) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.BALANCE_FRAME);
		if (showBalance != null && showBalance) {
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.BALANCE_FRAME, null);
			Map<String, Object> modelResponse = (Map<String, Object>) WebUtils.getSessionAttribute(request,
					ChallengeInformationUtils.BALANCE_MODEL);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.BALANCE_MODEL, null);
			BalancesBean responseBean = (BalancesBean) modelResponse.get(ChallengeInformationUtils.BALANCE_ACTION);
			org.apache.commons.beanutils.BeanUtils.copyProperties(command, responseBean);
			model.putAll(modelResponse);
		}

		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		BalancesBean dude = (BalancesBean) command;
		if (errors.getAllErrors().isEmpty() && !dude.isSearching()) {
			Boolean informationChallenge = session.isInformationChallenge();
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)) {
				if (!session.getOobEnroll()) {
					informationChallenge = session.isInformationChallenge();
				} else {
					informationChallenge = session.isOobAuthenticated();
					session.setOobChallenge(true);
				}
			} else {
				informationChallenge = session.isInformationChallenge();
			}
			if (informationChallenge != null
					&& !informationChallenge
					&& (session.getGuidedSetupSession() == null || !session.getGuidedSetupSession().isGuidedSetupMode())) {

				// ////////////////////

				String language = Utils.getRSALanguage(request);
				String username = session.getUsername();

				if (session.getInformationQuestion() == null
						&& !(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)
								&& session.getOobEnroll())) {


					DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(session.getPm_fp(),session.getDeviceRequest().getDeviceTokenCookie(), request);
					RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
					AnalyzeResponse anResp = session.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

						deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData()
								.getDeviceTokenCookie());
						deviceRequest
								.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

						session.setRsaSessionId(anResp.getIdentificationData().getSessionId());
						session.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

						session.setDeviceRequest(deviceRequest);

						if (ActionCode.CHALLENGE != anResp.getRiskResult().getTriggeredRule().getActionCode()) {
							session.setInformationChallenge(true);
						} else {
							String email = null;
							if (session.getCustomerProfile() != null)
								email = session.getCustomerProfile().getEmail();

							QuestionResponse personalQuestion;

							if (session.getInformationQuestion() == null) {
								RSAActionRequest actionRequest = new RSAActionRequest(
										Utils.generateCustomerInteractionEvent(request, session.getUsername()),
										session.getUsername(), "", email, 0, session.isRemember(),
										session.getDeviceRequest());

								if (session.getCustomerProfile() != null)
									actionRequest.setUsername(session.getCustomerProfile().getUsername());
								else if (session.getUsernameReservation() != null)
									actionRequest.setUsername(session.getUsernameReservation().getUsername());
								else
									actionRequest.setUsername(username);

								actionRequest.setDeviceRequest(session.getDeviceRequest());
								actionRequest.setRsaSessionId(session.getRsaSessionId());
								actionRequest.setRsaTransactionId(session.getRsaTransactionId());

								actionRequest.setRsaLang(Utils.getRSALanguage(request));

								personalQuestion = session.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);
								
								// MBSD-2002 RSA Upgrade Recommendations
								deviceRequest = RSAUtils.getDeviceRequest(session.getPm_fp(), session.getDeviceRequest().getDeviceTokenCookie(), request);
								deviceRequest.setDeviceTokenCookie(personalQuestion.getDeviceRequest().getDeviceTokenCookie());
								deviceRequest.setDeviceTokenFSO(personalQuestion.getDeviceRequest().getDeviceTokenFSO());
								session.setDeviceRequest(deviceRequest);
								session.setRsaSessionId(personalQuestion.getSessionId());
								session.setRsaTransactionId(personalQuestion.getRsaTransactionId());

							} else {
								personalQuestion = new QuestionResponse();
								personalQuestion.setStatusResult(ResultStatus.SUCCESS);
								personalQuestion.setQuestion(session.getInformationQuestion());
							}
							if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
								session.setInformationQuestion(personalQuestion.getQuestion());

							} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
								throw new ModelAndViewDefiningException(new ModelAndView("error"));
							} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
								throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
							}
						}
					}
				}

				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
				Map<String, Object> parameters = new HashMap<>();
				parameters.put(ChallengeInformationUtils.BALANCE_ACTION, command);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
						ChallengeInformationUtils.BALANCE_ACTION);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, parameters);
			}
		}
	

	private String findEmailAlert(List<CustomerEmailBalanceAlert> emailsAlertsAccount, String account,
			String accountSection, String productType, String productId, String pos) {
		if (emailsAlertsAccount != null && !emailsAlertsAccount.isEmpty()) {
			for (Iterator<CustomerEmailBalanceAlert> iterator = emailsAlertsAccount.iterator(); iterator.hasNext();) {
				CustomerEmailBalanceAlert account2 = (CustomerEmailBalanceAlert) iterator.next();
				
				if (account2.getAlertCondition().getOperator().equalsIgnoreCase(pos)
						&& account2.getAccountNumber().equals(account)
						&& account2.getAccountSection().equalsIgnoreCase(accountSection)
						&& account2.getAccountSubtype().equalsIgnoreCase(productType)
						&& account2.getAccountProductId().equalsIgnoreCase(productId))
					return account2.getId();
			}
		}
		return "";
	}

	private String findSmsAlert(List<SMSAccount> smsAlertsAccount, String account, String accountSection,
			String productType, String productId, String pos) {
		if (smsAlertsAccount != null && !smsAlertsAccount.isEmpty()) {
			for (Iterator<SMSAccount> iterator = smsAlertsAccount.iterator(); iterator.hasNext();) {
				SMSAccount account2 = (SMSAccount) iterator.next();
				if (account2.getAlertCondition()!= null &&  //IM 2080104
						account2.getAlertCondition().getOperator() != null &&						
						account2.getAlertCondition().getOperator().equalsIgnoreCase(pos)
						&& account2.getAccountNumber().equals(account)
						&& account2.getAccountSection().equalsIgnoreCase(accountSection)
						&& account2.getAccountSubtype().equalsIgnoreCase(productType)
						&& account2.getAccountProductId().equalsIgnoreCase(productId))
					return account2.getId();
			}
		}
		return "";
	}

	// MBSFE-633
	public String getAlertsProductTypes() {
		return alertsProductTypes;
	}

	public void setAlertsProductTypes(String alertsProductTypes) {
		this.alertsProductTypes = alertsProductTypes;
	}
	// END MBSFE-633

}
