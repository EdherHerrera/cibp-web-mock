package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.signup.SignupCampaign;
import evertec.cibp.web.session.UserSession;

public class RegefinishForm extends CancellableFormController {

	private CampaignsForm campaignsForm;

	public RegefinishForm() {
		super();
		setCommandClass(SignupCampaign.class);
		setCommandName("regefinish");
		setFormView("regefinish");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		WebUtils.setSessionAttribute(request, "skiprege", new Boolean(true));
		return new ModelAndView("redirect:portal");

	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		if (accounts == null) {
			throw new ProgrammingError("Unable to get user accounts");
		}

		CustomerProfile profile = userSession.getCustomerProfile();
		if (accounts != null && accounts.size() > 0) {
			List<TVFrontendAccount> portalAccounts = new LinkedList<TVFrontendAccount>();
			for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount name = (TVFrontendAccount) iterator.next();
				String accountNumberSuffix = name.getAccountNumberSuffix();
				if (!name.getHide()
						&& getCampaignsForm().getCampaignGenericBean().getCampaignProductsId()
								.contains(name.getAccountProductId())
						&& !getCampaignsForm().getCampaignGenericBean().getInvalidSufix().contains(accountNumberSuffix)) {
					if (name.getSubtype().equals("INS") || name.getSubtype().equals("SEC")) {
						continue;
					} else if (name.getSubtype().equals("PRM")) {
						continue;
					} else {
						name.getOdLimit();
						portalAccounts.add(name);
					}
				}
			}

			if (portalAccounts != null && portalAccounts.size() > 0) {
				model.put("accounts", portalAccounts);
			}
			model.put("customername", profile.getFirstName());
		}

		return model;

	}

	/**
	 * @return the campaignsForm
	 */
	public CampaignsForm getCampaignsForm() {
		return campaignsForm;
	}

	/**
	 * @param campaignsForm
	 *            the campaignsForm to set
	 */
	public void setCampaignsForm(CampaignsForm campaignsForm) {
		this.campaignsForm = campaignsForm;
	}

}
