package evertec.cibp.web.form;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.types.DepositAccount;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.holiday.Holiday;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.AccountsFacade;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ModifyUSaveIframeFormBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;
import evertec.dp.models.xsd.ifx.BankInfo_Type;
import evertec.dp.models.xsd.popular.DepAcctId_Type;
import evertec.dp.service.affiliateAccountInquiry.AffiliatedAcctInqResponse_Type;

public class ModifyUSaveIframeForm extends AbstractWizardFormController {

	protected final Log logger = LogFactory.getLog(getClass());
	private CustomerServices customerService;

	public ModifyUSaveIframeForm() {
		super();
		setCommandClass(ModifyUSaveIframeFormBean.class);
		setCommandName("modifyusave");
		setPages(new String[] { "modifyusave", "modifyusave", "modifyusave" });
	}

	protected int getTargetPage(HttpServletRequest request, int currentPage) {
		return super.getTargetPage(request, currentPage);
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new HashMap<>();
		model.put("page", "2");

		ModifyUSaveIframeFormBean bean = (ModifyUSaveIframeFormBean) command;

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		TVFrontendAccount account = Utils.getAccountById(bean.getAccount(), userSession.getAccounts());
		
		String depositeAccountNumber, depositAccountIndicator;
		List<TVFrontendAccount> withdrawalToAccounts = Utils.getAllDepositAccounts(userSession.getAccounts());
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE626)) {
			DepositAccount depositAccount = (DepositAccount) Utils.getAccountById(bean.getDatapowerAffiliateAccountNumber(), withdrawalToAccounts).getBackendObject();
			depositeAccountNumber = depositAccount.getAccountID();
			depositAccountIndicator = depositAccount.getSectionIndicator();
			bean.setDatapowerAffiliateAccountSection(depositAccountIndicator);
		} else {
			depositeAccountNumber = bean.getDatapowerAffiliateAccountNumber();
			depositAccountIndicator = bean.getDatapowerAffiliateAccountSection();
		}

		// CIBP-1767
		// This format the account services inside the portal
		String showInsideIframe = request.getParameter("showInsideIframe");
		model.put("showInsideIframe", showInsideIframe);

		// Send the update message with all the information obtained for the
		// account
		// Make sure the date is null ONLY when the user either selected no date
		// or made no change
		Date newDate = new Date();
		if (bean.getWithdrawalOption().equals("N")
				|| (bean.getWithdrawalOption().equals("") && bean.getDatapowerSavingsPayoutFrequency().equals("N")))
			newDate = null;
		else if ((bean.getWithdrawalOption().equals("D") || bean.getWithdrawalOption().equals("A"))
				&& bean.getDatapowerSavingsPayoutFrequency().equals("")) {
			newDate = bean.getNewDate();
		} else if (bean.getWithdrawalOption().equals("D") || bean.getWithdrawalOption().equals("A")) {
			if (bean.isChangedate() || bean.getDatapowerSavingsPayoutFrequency().equals("N"))
				newDate = bean.getNewDate();
			else
				newDate = bean.getDatapowerSavingsPayoutDateFormatted();
		} else if (bean.getWithdrawalOption().equals("") && bean.getDatapowerSavingsPayoutFrequency().equals("D")
				&& bean.isChangedate())
			newDate = bean.getNewDate();
		else if (bean.getWithdrawalOption().equals("") && bean.getDatapowerSavingsPayoutFrequency().equals("D")
				&& !bean.isChangedate())
			newDate = bean.getDatapowerSavingsPayoutDateFormatted();
		else if (bean.getWithdrawalOption().equals("") && bean.getDatapowerSavingsPayoutFrequency().equals("A")
				&& bean.isChangedate())
			newDate = bean.getNewDate();
		else if (bean.getWithdrawalOption().equals("") && bean.getDatapowerSavingsPayoutFrequency().equals("A")
				&& !bean.isChangedate())
			newDate = bean.getDatapowerSavingsPayoutDateFormatted();

		String newWithdrawalOption = bean.getWithdrawalOption();
		if (newWithdrawalOption.equals(""))
			newWithdrawalOption = bean.getDatapowerSavingsPayoutFrequency();
		
		boolean success = customerService.updateUSaveAccount(account.getAccountNumber(),
				depositeAccountNumber, depositAccountIndicator,
				bean.getDatapowerBankNumber(), bean.getDatapowerBranchNumber(), newDate, newWithdrawalOption);

		model.put("success", success);

		return showForm(request, error, "modifyusave", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		NumberTool numberTool = new NumberTool();
		if (errors.getErrorCount() == 0) {
			model.put("errorShowCal", false);
		}
		// CIBP-1767
		// This format the account services inside the portal
		String showInsideIframe = request.getParameter("showInsideIframe");
		model.put("showInsideIframe", showInsideIframe);

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		AccountMaskUtil mask = new AccountMaskUtil();
		Map<String, String> usaveAccounts = new LinkedHashMap<String, String>();
		Map<String, Object> h = new LinkedHashMap<String, Object>();
		Map<String, Object> ho = new LinkedHashMap<String, Object>();
		List<Holiday> holidays = customerService.getHolidays();
		if (holidays != null)
			for (Holiday holiday : holidays) {
				h.put(holiday.getId(),
						new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(holiday.getDate().getTime()));
			}
		model.put("holidays", h);
		// CIBP-2233 U-Save - Fecha de desembolso
		List<Holiday> holidaysAll = customerService.getAllHolidays();
		if (holidaysAll != null)
			for (Holiday holiday : holidaysAll) {
				ho.put(holiday.getId(),
						new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(holiday.getDate().getTime()));
			}
		model.put("holidaysAll", ho);

		// Get the information for this account
		ModifyUSaveIframeFormBean bean = (ModifyUSaveIframeFormBean) command;
		
		if (bean.getAccount() == null) {
			String usaveKey = (String) request.getParameter("account");
			if (usaveKey != null) {
				if (usaveKey.equals("none")) {
					page = 0;
					model.put("page", "0");
				} else {
					bean.setAccount(usaveKey);
					page = 1;
					model.put("page", "1");
				}
			} else {
				page = 0;
				model.put("page", "0");
			}
		} else {
			if (bean.getAccount().equals("none")) {
				page = 0;
				model.put("page", "0");
			} else {
				page = 1;
				model.put("page", "1");
			}
		}

		// Get the USave accounts
		if (Utils.getRSALanguage(request).equals("sp"))
			usaveAccounts.put("none", "Seleccione una cuenta");
		else
			usaveAccounts.put("none", "Select an account");

		for (Iterator<TVFrontendAccount> iter = accounts.iterator(); iter.hasNext();) {
			TVFrontendAccount element = (TVFrontendAccount) iter.next();
			if (!element.getHide() && element.getAccountProductId().equals("090")) {
				String key = StringUtils.abbreviate(element.getNickname(), 15)
						+ " "
						+ mask.mask(element.getAccountNumber())
						+ ((element.getAccountNumberSuffix() != null && !element.getAccountNumberSuffix().equals("")) ? " "
								+ element.getAccountNumberSuffix()
								: "");
				usaveAccounts.put(String.valueOf(element.getFrontEndId()), key);
			}
		}

		model.put("usaveAccounts", usaveAccounts);

		// Set up today's date
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Calendar n = Calendar.getInstance();
		model.put("todayDate", df.format(n.getTime()));
		df = new SimpleDateFormat("MM");
		model.put("month", df.format(n.getTime()));
		df = new SimpleDateFormat("dd");
		model.put("day", df.format(n.getTime()));

		// Set up tomorrow's date
		SimpleDateFormat df2 = new SimpleDateFormat("MM/dd/yyyy");
		Calendar t = Calendar.getInstance();
		t.add(Calendar.DAY_OF_MONTH, 1);
		for (int z = 1; (t.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
				|| t.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || h.containsValue(df2.format(t.getTime())));) {
			t.add(Calendar.DAY_OF_MONTH, z);
		}
		model.put("tomorrowDate", df2.format(t.getTime()));
		df2 = new SimpleDateFormat("MM");
		model.put("tomorrowMonth", df2.format(t.getTime()));
		df2 = new SimpleDateFormat("dd");
		model.put("tomorrowDay", df2.format(t.getTime()));

		df = new SimpleDateFormat("MM/dd/yyyy");
		Calendar thresholdDate = Calendar.getInstance();
		thresholdDate.add(Calendar.YEAR, 40);
		model.put("thresholddate", df.format(thresholdDate.getTime()));

		if (page == 1) {

			AffiliatedAcctInqResponse_Type resp = null;
			TVFrontendAccount acct = Utils.getAccountById(bean.getAccount(), userSession.getAccounts());
			if (acct == null) {
				page = 2;
				model.put("page", page);
				model.put("success", false);
			} else {

				// Try up to five times
				if (!bean.isDatapowerCapturedData()) {
					for (int i = 0; i < 5; i++) {
						resp = AccountsFacade.getInstance().getUSaveInformation(acct.getAccountNumber(), userSession);

						if (resp != null) {
							break;
						}

					}
				}

				// If we do not obtain a response, make the user choose again
				if (errors.getErrorCount() > 0) {
					page = 1;
					FieldError e = null;
					if (errors.getFieldError("newDate") != null) {
						e = errors.getFieldError("newDate");
						if (e.getDefaultMessage().indexOf("Invalid date") != -1) {
							model.put("errorShowCal", true);
						}
					} else
						model.put("errorShowCal", false);

					if ((bean.getDatapowerSavingsPayoutFrequency().equals("D") || bean
							.getDatapowerSavingsPayoutFrequency().equals("A"))
							&& !bean.getDatapowerSavingsPayoutDate().equals("")) {

						Calendar cal = Calendar.getInstance();
						cal.set(Integer.parseInt(bean.getDatapowerSavingsPayoutDate().substring(0, 4)),
								Integer.parseInt(bean.getDatapowerSavingsPayoutDate().substring(5, 7)) - 1,
								Integer.parseInt(bean.getDatapowerSavingsPayoutDate().substring(8)));
						Date savingsPayoutDateFormatted = cal.getTime();
						bean.setDatapowerSavingsPayoutDateFormatted(savingsPayoutDateFormatted);
						model.put("actualDate", savingsPayoutDateFormatted);
					}

					model.put("withdrawalOption", "");
					model.put("actualFrequencyValue", "");
					model.put("actualFrequency", bean.getDatapowerSavingsPayoutFrequency());
					model.put("page", "1");
					model.put("success", false);
				} else if (resp == null) {
					page = 2;
					model.put("page", "2");
					model.put("success", false);
				} else {

					if (resp.getDepAcctIdTo() != null && resp.getDepAcctIdTo().getAcctId() != null) {
						// Retrieve the USave information
						DepAcctId_Type depAcct = resp.getDepAcctIdTo();
						DepAcctId_Type usaveAcct = resp.getDepAcctId();
						BankInfo_Type bankInfo = usaveAcct.getBankInfo();
						
						if (!StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE626)) {
							bean.setDatapowerAffiliateAccountNumber(depAcct.getAcctId());
						}
						bean.setDatapowerAffiliateAccountSection(depAcct.getSectionInd());
						bean.setDatapowerBankNumber(bankInfo.getBankId());
						bean.setDatapowerBranchNumber(bankInfo.getBranchId());
						bean.setDatapowerSavingsPayoutDate(resp.getPayOutDt());
						bean.setDatapowerCapturedData(true);

						if (resp.getPayOutFreq().indexOf("Annual") > 0)
							bean.setDatapowerSavingsPayoutFrequency("A");
						else if (resp.getPayOutFreq().indexOf("NoPayOutDate") > 0)
							bean.setDatapowerSavingsPayoutFrequency("N");
						else
							bean.setDatapowerSavingsPayoutFrequency("D");

						if ((bean.getDatapowerSavingsPayoutFrequency().equals("D") || bean
								.getDatapowerSavingsPayoutFrequency().equals("A"))
								&& !bean.getDatapowerSavingsPayoutDate().equals("")) {

							Calendar cal = Calendar.getInstance();
							cal.set(Integer.parseInt(bean.getDatapowerSavingsPayoutDate().substring(0, 4)),
									Integer.parseInt(bean.getDatapowerSavingsPayoutDate().substring(5, 7)) - 1,
									Integer.parseInt(bean.getDatapowerSavingsPayoutDate().substring(8)));
							Date savingsPayoutDateFormatted = cal.getTime();
							bean.setDatapowerSavingsPayoutDateFormatted(savingsPayoutDateFormatted);
							model.put("actualDate", savingsPayoutDateFormatted);
						}

						model.put("actualFrequency", bean.getDatapowerSavingsPayoutFrequency());
						model.put("withdrawalOption", "");
						model.put("actualFrequencyValue", "");
					} else {
						model.put("noAccount", true);
						page = 2;
						model.put("page", page);
					}
				}
			}
			
			// MBSFE 626: Get deposit accounts
				
				List<TVFrontendAccount> withdrawalToAccounts = Utils.getAllDepositAccounts(userSession.getAccounts());
				Map<String, String> accountsToMap = new LinkedHashMap<>();
				Map<String, String> accountsToMapClone = new LinkedHashMap<>();
			
			if(resp != null){
				DepAcctId_Type depAcct = resp.getDepAcctIdTo();
				TVFrontendAccount debitAccountNumber = Utils.getAccountByAccountNumber(depAcct.getAcctId(),
						withdrawalToAccounts);
	
				for (Iterator<TVFrontendAccount> iterator = withdrawalToAccounts.iterator(); iterator.hasNext();) {
					TVFrontendAccount account = iterator.next();
					
						if (!account.getHide() && !account.getAccountSection().equals("R")) {
							if (account.getPortalBalance() != null)
								accountsToMap.put(
										account.getFrontEndId(),
										StringUtils.abbreviate(account.getNickname(), 15)
										+ " "
										+ mask.mask(account.getAccountNumber())
										+ ((account.getAccountNumberSuffix() != null && !account
										.getAccountNumberSuffix().equals("")) ? " "
										+ account.getAccountNumberSuffix() : "") + " "
										+ numberTool.format("currency", account.getPortalBalance()));
							else
								accountsToMap.put(
										account.getFrontEndId(),
										StringUtils.abbreviate(account.getNickname(), 15)
										+ " "
										+ mask.mask(account.getAccountNumber())
										+ ((account.getAccountNumberSuffix() != null && !account
										.getAccountNumberSuffix().equals("")) ? " "
										+ account.getAccountNumberSuffix() : ""));
						}
						
						if (account != null && account.equals(debitAccountNumber)) {
							accountsToMapClone = (LinkedHashMap<String, String>)((LinkedHashMap<String, String>)accountsToMap).clone();
							String acc = accountsToMap.get(account.getFrontEndId()).toString();
							accountsToMapClone.remove(account.getFrontEndId());
							accountsToMap.clear();
							accountsToMap.put(account.getFrontEndId(), acc);
							accountsToMap.putAll(accountsToMapClone);
						}
				}
			}

			model.put("toaccounts", accountsToMap);
			model.put("MBSFE626", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE626));

			// END MBSFE 626
		}

		return model;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		ModifyUSaveIframeFormBean bean = (ModifyUSaveIframeFormBean) command;
		bean.setShowDateInWithdrawalDateError(false);
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		List<Holiday> holidays = customerService.getHolidays();
		Map<String, Object> h = new LinkedHashMap<String, Object>();
		if (holidays != null)
			for (Holiday holiday : holidays) {
				h.put(holiday.getId(),
						new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(holiday.getDate().getTime()));
			}
		if (GenericValidator.isBlankOrNull(bean.getAccount()) || "none".equals(bean.getAccount())) {

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("account", "", "Please select an account");
			} else {
				errors.rejectValue("account", "", "Favor de seleccionar una cuenta");
			}
		}
		
		// MBSFE 626
		if (bean.getDatapowerAffiliateAccountNumber() == null
				|| "none".equalsIgnoreCase(bean.getDatapowerAffiliateAccountNumber())) {
			errors.rejectValue("recurringDebitAccount", "productselection.usave.debitfromaccount.required",
					"Select the account");
		}
		// END MBSFE 626
		
		if (bean.isSubmitted()) {
			if (bean.getWithdrawalOption().equals("")) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					errors.rejectValue("withdrawalOption", "", "Please select a new frequency");
				} else {
					errors.rejectValue("withdrawalOption", "", "Favor de seleccionar una nueva frecuencia");
				}
			} else {
				if (bean.getWithdrawalOption().equals("D") || bean.getWithdrawalOption().equals("A")) {

					Date effectiveDate = bean.getNewDate();
					Calendar today = Calendar.getInstance();

					// Set the max date (forty years in future)
					Calendar thresholdDate = Calendar.getInstance();
					thresholdDate.add(Calendar.YEAR, 40);

					if (effectiveDate != null) {
						// Validate no future dates
						Calendar n = Calendar.getInstance();
						n.setTime(effectiveDate);

						if ((n.get(Calendar.YEAR) < today.get(Calendar.YEAR))
								|| n.get(Calendar.YEAR) == today.get(Calendar.YEAR)
								&& n.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR)) {
							errors.rejectValue("newDate", "productselection.usave.withdrawaldate.past",
									"You must select a future date");
						} else if (n.after(thresholdDate)) {
							errors.rejectValue("newDate", "productselection.usave.withdrawaldate.oneyear",
									"You must select a date less than the limit date");
							bean.setShowDateInWithdrawalDateError(true);
						} else if (h.containsValue(df.format(effectiveDate))) {
							errors.rejectValue("newDate", "productselection.usave.invaliddate.weekend",
									new Object[] { df.format(effectiveDate) },
									"Invalid date: " + df.format(effectiveDate) + ". Please choose working days only.");
						} else {
							// We cannot withdraw on a weekend
							Calendar effectiveDateCal = Calendar.getInstance();
							effectiveDateCal.setTime(effectiveDate);

							if (Calendar.SATURDAY == effectiveDateCal.get(Calendar.DAY_OF_WEEK)
									|| Calendar.SUNDAY == effectiveDateCal.get(Calendar.DAY_OF_WEEK)) {
								errors.rejectValue("newDate", "productselection.usave.invaliddate.weekend",
										new Object[] { df.format(effectiveDate) },
										"Invalid date: " + df.format(effectiveDate)
												+ ". Please choose working days only.");
							}
						}
					} else {
						errors.rejectValue("newDate", "productselection.usave.withdrawaldate.select", "Select the date");
					}
				}
			}
		}
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerServices) {
		this.customerService = customerServices;
	}
}
