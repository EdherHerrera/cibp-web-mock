package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.RewardOrder;
import bppr.tv.beans.accounts.factory.TVAccountFactory;
import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import bppr.tv.beans.accounts.types.CardAccount;
import bppr.tv.beans.accounts.types.DepositAccount;
import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TVFrontendCreditAccount;
import evertec.cibp.core.models.account.TVFrontendDepositAccount;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.base.AbstractStatement;
import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.models.base.RunningBalanceTransaction;
import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.RewardOrdersRequest;
import evertec.cibp.core.services.requests.StatementRequest;
import evertec.cibp.core.services.requests.TransactionRequest;
import evertec.cibp.core.services.responses.PremiaTransactionResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.RewardOrdersResponse;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.form.bean.TransactionFormBean;
import evertec.cibp.web.models.vault.AccountStatement;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.CookieUtils;
import evertec.cibp.web.utils.PFMUtil;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.utils.TransactionUtil;
import evertec.cibp.web.enums.CookieCategory;

public class TransactionForm extends CancellableFormController implements MessageSourceAware {

	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;
	private int maxStatements;
	private int maxTransactions;
	private String unversalProducts;
	private boolean pfmAutomaticEntitlementCreation;
	private String chargesSummaryCodes;
	private String transactionPosCodes; //transaction pos codes
	private String transactionEftCodes; //transaction eft codes
	private String transactionWithdrawalAthCodes; //transaction withdrawal codes
	private String transactionAthMovilCodes; //transaction ath codes
	private String transactionDetailTypeText; //transactions type text

	public TransactionForm() {
		super();
		setCommandClass(TransactionFormBean.class);
		setCommandName("transaction");
		setFormView("transaction");
		setBindOnNewForm(true);
	}

	/**
	 * Constructor for servlet injection parameters.
	 *
	 * @author ET58344, Evertec Inc.
	 * @since 09-28-2021
	 * @param maxStatements parameter 1
	 * @param maxTransactions parameter 2
	 * @param unversalProducts parameter 3
	 * @param pfmAutomaticEntitlementCreation parameter 4
	 * @param chargesSummaryCodes parameter 5
	 * @param transactionPosCodes transaction pos codes
	 * @param transactionEftCodes transaction eft codes
	 * @param transactionWithdrawalAthCodes transaction withdrawal codes
	 * @param transactionAthMovilCodes transaction ath codes
	 * @param transactionDetailTypeText transaction type text
	 */
	public TransactionForm(int maxStatements, int maxTransactions, String unversalProducts,
						   boolean pfmAutomaticEntitlementCreation, String chargesSummaryCodes,
						   String transactionPosCodes, String transactionEftCodes, String transactionWithdrawalAthCodes,
						   String transactionAthMovilCodes, String transactionDetailTypeText) {
		this();
		this.maxStatements = maxStatements;
		this.maxTransactions = maxTransactions;
		this.unversalProducts = unversalProducts;
		this.pfmAutomaticEntitlementCreation = pfmAutomaticEntitlementCreation;
		this.chargesSummaryCodes = chargesSummaryCodes;
		this.transactionPosCodes = transactionPosCodes;
		this.transactionEftCodes = transactionEftCodes;
		this.transactionWithdrawalAthCodes = transactionWithdrawalAthCodes;
		this.transactionAthMovilCodes = transactionAthMovilCodes;
		this.transactionDetailTypeText = transactionDetailTypeText;

	}

	protected boolean isFormSubmission(HttpServletRequest request) {
		return (request.getParameter("account") != null);

	}

	private String getImagePath(TransactionFormBean transaction, UserSession userSession) {
		String imgPath = null;

		final String jetblue_mgjbg = "MGJBG"; // "MGJBG"
		final String jetblue_mwjbb = "MWJBB"; // MWJBB
		final String jetblue_mgjbc = "MGJBC"; // MGJBC
		 final String premia_business_sec = "VBSEC"; // PREMIA BUSINESS SECURED
		 final String premia_business_rwd = "VBSG1"; // PREMIA BUSINESS RWD
		 final String premia_business_max = "VBSG2"; // PREMIA BUSINESS MAX

		
		 // MBCA816 Alexis
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)) {

			HashSet<String> transCode = new HashSet<String>() {
				{
					add("VSELT");
					add("APAXI");
					add("VWREG");
					add("VWVSI");
					add("APREG");
					add("UAREG");
					add("UVREG");
					add("UVVSI");
					add("VSICN");
					add("VSNOV");
					add("VSRTN");
					add("VSSEC");
					add("UVNOV");
					add("AEELT");
					add("AEICN");
					add("AEULT");
					add("UAULT");
					add("MCEDG");
					add("UMEDG");
					add("VWASI");
					add("VWAIN");
					add("MWAWR");
					add("VWVSI");
					add("VPPRE");
					add("VPCBK");
					add("VWAIP");
					add("MWREG");
					add(jetblue_mgjbc);
					add(jetblue_mwjbb);
					add(jetblue_mgjbg);
					add(premia_business_sec);
					add(premia_business_rwd);
					add(premia_business_max);
				}
			};

			if (transCode.contains(transaction.getAccountInfo().getProductDefinition().getProductId())) {
				imgPath = messageSource.getMessage(
						"credit.card.portal.img." + transaction.getAccountInfo().getProductDefinition().getProductId(),
						null, new Locale(userSession.getLanguage()));
			} else if (transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
					.indexOf("AMEX") > -1
					|| transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
							.indexOf("AMERICAN") > -1
					|| transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
							.indexOf("AE") > -1) {
				imgPath = messageSource.getMessage("credit.card.portal.img.amex", null,
						new Locale(userSession.getLanguage()));
			} else if (transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
					.indexOf("VISA") > -1) {
				imgPath = messageSource.getMessage("credit.card.portal.img.visa", null,
						new Locale(userSession.getLanguage()));
			} else if (transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
					.indexOf("MASTERCARD") > -1
					|| transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
							.indexOf("MC") > -1
					|| transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
							.indexOf("M/C") > -1) {
				imgPath = messageSource.getMessage("credit.card.portal.img.mastercard", null,
						new Locale(userSession.getLanguage()));
			}

		} else {
			// Old Code
			if (transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase("VSELT")
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase("APAXI")
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase("VWREG")
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase("VWVSI")
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase("APREG")
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase("UAREG")
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase("UVREG")
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase("UVVSI")
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase("VSICN")
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase("VSNOV")
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase("VWAIP")
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase("MWREG")
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase(jetblue_mgjbc)
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase(jetblue_mwjbb)
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase(jetblue_mgjbg)
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase(premia_business_sec)
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase(premia_business_rwd)
					|| transaction.getAccountInfo().getProductDefinition().getProductId().equalsIgnoreCase(premia_business_sec)) {
				imgPath = messageSource.getMessage(
						"credit.card.portal.img." + transaction.getAccountInfo().getProductDefinition().getProductId(),
						null, new Locale(userSession.getLanguage()));
			} else if (transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
					.indexOf("AMEX") > -1
					|| transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
							.indexOf("AMERICAN") > -1
					|| transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
							.indexOf("AE") > -1) {
				imgPath = messageSource.getMessage("credit.card.portal.img.amex", null,
						new Locale(userSession.getLanguage()));
			} else if (transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
					.indexOf("VISA") > -1) {
				imgPath = messageSource.getMessage("credit.card.portal.img.visa", null,
						new Locale(userSession.getLanguage()));
			} else if (transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
					.indexOf("MASTERCARD") > -1
					|| transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
							.indexOf("MC") > -1
					|| transaction.getAccountInfo().getProductDefinition().getProductDescription().toUpperCase()
							.indexOf("M/C") > -1) {
				imgPath = messageSource.getMessage("credit.card.portal.img.mastercard", null,
						new Locale(userSession.getLanguage()));
			}

		}
		return imgPath;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object transaction,
			BindException error) throws Exception {

		Map<String, Object> model = new HashMap<>();
		Cookie versionCookie = CookieUtils.getCookie(request, "client-version");

		model.put("pinrequeststatus", this.getPinRequestStatus());

		TransactionFormBean transactionData = (TransactionFormBean) transaction;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		userSession.setCheckImageMap(null);
		CustomerServices customerService = userSession.getCustomerService();
		BankingSession bankingSession = userSession.getBankingSession();
		CustomerProfile customerProfile = userSession.getCustomerProfile();

		int pagereq = transactionData.getPageReq();
		int totalpages = transactionData.getTotalPages();
		String strShowAllPages = request.getParameter("allPages");
		String view = request.getParameter("view");
		boolean showAllPages = false;

		if (strShowAllPages != null && strShowAllPages.equalsIgnoreCase("yes"))
			showAllPages = true;

		String cycle = transactionData.getCycle();
		String key = transactionData.getAccount();
		String category = transactionData.getCategory();
		String activity = transactionData.getActivity();
		model.put("activity", activity);
		model.put("selectCategory", category);
		model.put("saccount", key);
		model.put("scycle", cycle);
		model.put("spageReq", pagereq);
		model.put("totalPages", totalpages);
		model.put("showAllPages", strShowAllPages);
		model.put("view", view);
		if (this.getShowAllMessageSourceStatus())
			model.put("hideShowAll", true);
		else
			model.put("hideShowAll", false);

		LinkedHashMap<String, String> activityMap = new LinkedHashMap<String, String>();

		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
			model.put("Lang", "sp");
			activityMap.put("All", "Todas");
		} else {
			model.put("Lang", "en");
			activityMap.put("All", "All");
		}
		if (GenericValidator.isBlankOrNull(key) || "none".equalsIgnoreCase(key))
			return showForm(request, response, error);

		TVFrontendAccount account = Utils.getAccountById(key, userSession.getAccounts());
		if (account == null)
			return showForm(request, response, error);

		if (account != null && (account.getAccountProductId().equalsIgnoreCase("009")// Acceso
																						// Popular
				|| account.getAccountProductId().equalsIgnoreCase("020")// Avance
																		// 50
				|| account.getAccountProductId().equalsIgnoreCase("023")// Multicuenta
																		// Staff
				|| account.getAccountProductId().equalsIgnoreCase("024")// Popular
																		// Plus
				|| account.getAccountProductId().equalsIgnoreCase("025")// Multicuenta
																		// Popular
				|| account.getAccountProductId().equalsIgnoreCase("026")// Ideal
				|| account.getAccountProductId().equalsIgnoreCase("028")// MaxiPremium
				|| account.getAccountProductId().equalsIgnoreCase("029")// Private
																		// Management
																		// Account
				|| account.getAccountProductId().equalsIgnoreCase("049")// Ahorro
																		// a
																		// Toda
																		// Hora
																		// Staff
				|| account.getAccountProductId().equalsIgnoreCase("051")// Ahorro
																		// 24
				|| account.getAccountProductId().equalsIgnoreCase("052")// Ahorro
																		// a
																		// Toda
																		// Hora
				|| account.getAccountProductId().equalsIgnoreCase("069")// Multicuenta
																		// Subsidiary
																		// Staff
				|| account.getAccountProductId().equalsIgnoreCase("072")// Ahorro
																		// a
																		// Toda
																		// Hora
																		// Subsidiary
																		// Staff
				|| account.getAccountProductId().equalsIgnoreCase("081")// e-account
				|| account.getAccountProductId().equalsIgnoreCase("087")))// CIMA
		{
			model.put("showOdLimitLink", true);
		} else
			model.put("showOdLimitLink", false);

		model.put("accountLast4", " x" + account.getAccountNumber().substring(account.getAccountNumber().length() - 4));

		if (account.getSubtype().equalsIgnoreCase("IDA") || account.getSubtype().equalsIgnoreCase("PFC")
				|| account.getSubtype().equalsIgnoreCase("CCA")) {

			// tab Number for Credit Cards
			String tabNum = request.getParameter("tabNum");
			if (tabNum == null || tabNum.equalsIgnoreCase(""))
				tabNum = "1";

			transactionData.setTabNum(tabNum);

			// tab Number for Deposit Accounts
			String trxTabNum = request.getParameter("trxTabNum");
			if (trxTabNum == null || trxTabNum.equalsIgnoreCase(""))
				trxTabNum = "1";

			transactionData.setSubTab("lost");

			transactionData.setTrxTabNum(trxTabNum);

			transactionData.setAccountInfo(account);

			TVAccountProductDefinition productDefinition = account.getProductDefinition();
			if (productDefinition != null && account.getSubtype().equalsIgnoreCase("IDA")) {
				if (productDefinition.getId().substring(productDefinition.getId().length() - 1).equalsIgnoreCase("S")) {
					model.put("SUBTYPE", "SAVING");
				}
				if (productDefinition.getId().substring(productDefinition.getId().length() - 1).equalsIgnoreCase("R")) {
					model.put("SUBTYPE", "RESERVE");
				}
			}

			if (account.getSubtype().equalsIgnoreCase("CCA")) {
				if (versionCookie == null
						|| (versionCookie != null && !"mobile".equalsIgnoreCase(versionCookie.getValue()))) {
					transactionData.setImgPath(this.getImagePath(transactionData, userSession));
				}

				if (activity.contains("x"))
					model.put("cardnumberlabel", true);
				else
					model.put("cardnumberlabel", false);

				if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
					activityMap.put("All", "Todas");
				else
					activityMap.put("All", "All");

				if (versionCookie == null
						|| (versionCookie != null && !"mobile".equalsIgnoreCase(versionCookie.getValue()))) {
					if (account.getCardPlasticAccountRecords().length < 1) {
						AbstractAccount accountDetails = userSession.getCustomerService().getAccountDetails(
								userSession.getCustomerProfile(), userSession.getBankingSession(), account, false);
						TVFrontendCreditAccount frontendCreditAccount = (TVFrontendCreditAccount) accountDetails;
						account.setCardPlasticAccountRecords(frontendCreditAccount.getCardPlasticAccountRecords());
						TVAccountFactory fact = new TVAccountFactory();
						final CardPlasticAccountRecord[] record = account.getCardPlasticAccountRecords(); //Get card plastic account records
						List<CardAccount> associatedCards = new LinkedList<CardAccount>();
						for (int i = 0; i < account.getCardPlasticAccountRecords().length; i++) {
							if (!(record[i].getCardPANId().equalsIgnoreCase(account.getAccountNumber()))) {
								CardAccount ccaAccount = fact.createCardPlasticAccount(record[i].getCardPANId());
								associatedCards.add(ccaAccount);
							}
						}
						model.put("cardIssued", associatedCards.size());
						account.setAssociatedCards(associatedCards);
					} else {
						model.put("cardIssued", account.getCardPlasticAccountRecords().length);
					}
				}
			}
		}

		model.put("account", account);

		if (!account.getFeatures().isShowStatement()) {
			error.reject("account.invalid");
			return showForm(request, response, error);
		}

		/**
		 * Here is the MLA transactions implementation ONlY Current transactions
		 * that include a max of 50 transactions and a max of 16 months of
		 * transactions
		 */

		model.put("ACH_PAYMENT_NT", StaticMessageSource.isFlagEnabled(StaticMessageSource.ACH_PAYMENT_NT));
		model.put("isTransactional",
				AccountUtils.getIsTransactional(userSession.getCustomerProfile()).equalsIgnoreCase("Y"));

		if (account.getSubtype().equals("MLA")) {
			userSession.setAccountMLA(account.getFrontEndId());
			model.put("selectedAccount", "MLA");
			model.put("flagMortgage", StaticMessageSource.isFlagEnabled(StaticMessageSource.MORTGAGE_AUTO_ACH));
			TransactionsResponse transactionResponse = null;
			// get MLA transactions
			Calendar startDate = Calendar.getInstance();
			startDate.add(Calendar.MONTH, -16);
			startDate.set(Calendar.DAY_OF_MONTH, 1);

			if (userSession.getDisplayTransactionDowntime()) {
				model.put("transactiondowntime", "true");
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[1]);
				} else {
					model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[0]);
				}
			} else {

				TransactionRequest req = new TransactionRequest(Utils.generateCustomerInteractionEvent(request), null,
						customerProfile, bankingSession, account, maxTransactions, 1, false, startDate, null, null);
				transactionResponse = customerService.getMortgageTransactions(req);

				if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
					return showForm(request, response, error, model);

				List<AbstractTransaction> transactions = setTransactionsCodeReason(transactionResponse.getTransactions());
				if (transactions != null && transactions.size() > 0)
					model.put("transactions", transactions);
			}
			return showForm(request, response, error, model);

		}

		/**
		 * Here is the IDA, CCA, PRM statements implementation
		 */
		List<AbstractStatement> availableStatements = null;
		if (account.getStatements() == null) {

			StatementRequest stmtrq = new StatementRequest(Utils.generateCustomerInteractionEvent(request),
					customerProfile, bankingSession, account, maxStatements);
			availableStatements = customerService.getAvailableStatements(stmtrq).getStatementList();
			account.setStatements(availableStatements);
		} else
			availableStatements = account.getStatements();		

		LinkedHashMap<String, AbstractStatement> accountStatementMap = new LinkedHashMap<String, AbstractStatement>();
		LinkedHashMap<String, Map> detailedStatementsMap = new LinkedHashMap<String, Map>();
		LinkedHashMap<String, String> statementsMap = new LinkedHashMap<String, String>();
		statementsMap = Utils.getStatements(availableStatements, accountStatementMap, Utils.getRSALanguage(request),
				account.getSubtype().equals("PRM"));
		detailedStatementsMap = Utils.getDetailedStatements(availableStatements, Utils.getRSALanguage(request),
				account.getSubtype().equals("PRM"));
		model.put("statementsMap", statementsMap);
		model.put("detailedStatementsMap", detailedStatementsMap);

		TransactionsResponse transactionResponse = null;

		/**
		 * IDA and PFC Current and Statement Transactions
		 */
		if (account.getSubtype().equals("IDA") || account.getSubtype().equals("PFC")) {
			model.put("selectedAccount", "DEP");

			if (account.getAccountNumberSuffix() != null && "RES".equals(account.getAccountNumberSuffix()))
				model.put("selectedAccountRes", "true");

			if (cycle != null && !cycle.equals("1") && !cycle.equals("-1")) // Means
																			// Current
			// Transactions
			{
				// get transactions by statement
				// get statements transactions
				AbstractStatement stm = Utils.getStatementById(cycle, availableStatements);
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
					if (stm.getPdfID().isEmpty() || stm.getPdfID().equalsIgnoreCase("")) {
						model.put("pdfId", 0);
					} else {
						model.put("pdfId", stm.getPdfID());
					}
					model.put("eBillsFlag", StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS));
				}
				model.put("lastStatementBalance", stm.getStartBalance());
				HashMap<String, String> imgKeyMap = new HashMap<String, String>();

				if (showAllPages) {

					if (userSession.getDisplayTransactionDowntime()) {
						model.put("transactiondowntime", "true");
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[1]);
						} else {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[0]);
						}
					} else {

						List<AbstractTransaction> transactions = new LinkedList<AbstractTransaction>();
						for (int z = 1; z <= totalpages; z++) {
							TransactionRequest depreq = new TransactionRequest(
									Utils.generateCustomerInteractionEvent(request), stm, customerProfile,
									bankingSession, account, maxTransactions, z, true, null, null, null);
							transactionResponse = customerService.getDepositStatementTransactions(depreq);
							if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
								return showForm(request, response, error, model);

							imgKeyMap.putAll(transactionResponse.getImageKeyMap());
							transactions.addAll(transactionResponse.getTransactions());
						}

						userSession.setCheckImageMap(imgKeyMap);

						if (transactions != null && transactions.size() > 0)
							model.put("transactions", setTransactionsCodeReason(transactions));
					}

				} else {

					if (userSession.getDisplayTransactionDowntime()) {
						model.put("transactiondowntime", "true");
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[1]);
						} else {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[0]);
						}
					} else {

						TransactionRequest depreq = new TransactionRequest(
								Utils.generateCustomerInteractionEvent(request), stm, customerProfile, bankingSession,
								account, maxTransactions, pagereq, true, null, null, null);
						transactionResponse = customerService.getDepositStatementTransactions(depreq);
						if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
							return showForm(request, response, error, model);

						userSession.setCheckImageMap(transactionResponse.getImageKeyMap());
						List<AbstractTransaction> transactions = setTransactionsCodeReason(transactionResponse.getTransactions());
						if (transactions != null && transactions.size() > 0)
							model.put("transactions", transactions);
					}
				}
			} else {
				// get current transactions
				if (showAllPages) {
					if (userSession.getDisplayTransactionDowntime()) {
						model.put("transactiondowntime", "true");
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[1]);
						} else {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[0]);
						}
					} else {

						List<AbstractTransaction> transactions = new LinkedList<AbstractTransaction>();
						List<AbstractTransaction> inprocess = new LinkedList<AbstractTransaction>();
						BigDecimal totalInprocess = new BigDecimal(0.0d);
						BigDecimal availBal = null;
						RunningBalanceTransaction name = null;
						HashMap<String, String> imgKeyMap = new HashMap<String, String>();

						for (int z = 1; z <= totalpages; z++) {
							TransactionRequest depreq2 = new TransactionRequest(
									Utils.generateCustomerInteractionEvent(request), null, customerProfile,
									bankingSession, account, maxTransactions, z, true, null, null, null);
							transactionResponse = customerService.getDepositCurrentTransactions(depreq2);

							if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
								return showForm(request, response, error, model);

							imgKeyMap.putAll(transactionResponse.getImageKeyMap());
							transactions.addAll(transactionResponse.getTransactions());
						}

						userSession.setCheckImageMap(imgKeyMap);

						for (Iterator<AbstractTransaction> iterator = transactions.iterator(); iterator.hasNext();) {
							name = (RunningBalanceTransaction) iterator.next();
							if (name.getDate() == null
									|| name.getDescription().toUpperCase().indexOf("IN PROCESS") != -1) {
								inprocess.add(name);
								if (name.getType().equals("CR"))
									totalInprocess = totalInprocess.add(name.getAmount());
								else
									totalInprocess = totalInprocess.subtract(name.getAmount());
							}

						}

						if (inprocess != null && inprocess.size() > 0) {
							transactions.removeAll(inprocess);
							model.put("inprocess", setTransactionsCodeReason(inprocess));
							if (totalInprocess.doubleValue() >= 0.0d)
								model.put("inprocessType", "CR");
							else
								model.put("inprocessType", "DB");
							model.put("totalInprocess", totalInprocess.abs());

						}

						// mpicado, cibp-2216
						// BigDecimal availBal = null;
						BigDecimal bookBalance = null;
						if (account != null) {
							availBal = account.getPortalBalance();
							AbstractAccount accountDetails = userSession.getCustomerService().getAccountDetails(
									userSession.getCustomerProfile(), userSession.getBankingSession(), account, false);
							if (accountDetails != null) {
								if (account.getAccountSection().equals("S"))
									bookBalance = ((TVFrontendDepositAccount) accountDetails).getSavingsLedgerBalance();
								else if (account.getAccountSection().equals("C"))
									bookBalance = ((TVFrontendDepositAccount) accountDetails)
											.getCheckingLedgerBalance();
							}
						}
						// CIBP-2216
						if (name == null && transactions.size() == 0 && availBal != bookBalance)// No
																								// transactions
																								// case,
																								// add
																								// In
																								// Process
																								// section
						{
							name = new RunningBalanceTransaction();
							name.setBalance(bookBalance); // Other transactions
						}

						if (availBal != null && name != null && name.getBalance() != null && showAllPages && ((availBal
								.abs().add(name.getBalance().abs().negate()).abs().doubleValue()) >= 0.01d)) {
							RunningBalanceTransaction other = new RunningBalanceTransaction();
							other.setDescription("IN PROCESS OTHER TRANSACTIONS");
							BigDecimal difamt = availBal.add(name.getBalance().negate());
							other.setAmount(difamt.abs());
							if (difamt.doubleValue() >= 0.01d)
								other.setType("CR");
							else
								other.setType("DB");
							inprocess.add(other);
							if (other.getType().equals("CR"))
								totalInprocess = totalInprocess.add(other.getAmount());
							else
								totalInprocess = totalInprocess.subtract(other.getAmount());
							model.put("inprocess", setTransactionsCodeReason(inprocess));
							model.put("totalInprocess", totalInprocess.abs());
							if (totalInprocess.doubleValue() >= 0.0d)
								model.put("inprocessType", "CR");
							else
								model.put("inprocessType", "DB");

						}
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
							model.put("pdfId", 0);
							model.put("eBillsFlag",
									StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS));
						}

						if (transactions != null && transactions.size() > 0)
							model.put("transactions", setTransactionsCodeReason(transactions));
					}
				} else {
					if (userSession.getDisplayTransactionDowntime()) {
						model.put("transactiondowntime", "true");
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[1]);
						} else {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[0]);
						}
					} else {
						TransactionRequest depreq2 = new TransactionRequest(
								Utils.generateCustomerInteractionEvent(request), null, customerProfile, bankingSession,
								account, maxTransactions, pagereq, true, null, null, null);
						transactionResponse = customerService.getDepositCurrentTransactions(depreq2);

						if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
							return showForm(request, response, error, model);

						userSession.setCheckImageMap(transactionResponse.getImageKeyMap());
						List<AbstractTransaction> transactions = transactionResponse.getTransactions();
						List<AbstractTransaction> inprocess = new LinkedList<AbstractTransaction>();
						BigDecimal totalInprocess = new BigDecimal(0.0d);
						RunningBalanceTransaction name = null;
						for (Iterator<AbstractTransaction> iterator = transactions.iterator(); iterator.hasNext();) {
							name = (RunningBalanceTransaction) iterator.next();
							if (name.getDate() == null
									|| name.getDescription().toUpperCase().indexOf("IN PROCESS") != -1) {
								inprocess.add(name);
								if (name.getType().equals("CR"))
									totalInprocess = totalInprocess.add(name.getAmount());
								else
									totalInprocess = totalInprocess.subtract(name.getAmount());
							}
						}

						if (inprocess != null && inprocess.size() > 0) {
							transactions.removeAll(inprocess);
							model.put("inprocess", setTransactionsCodeReason(inprocess));
							if (totalInprocess.doubleValue() >= 0.0d)
								model.put("inprocessType", "CR");
							else
								model.put("inprocessType", "DB");
							model.put("totalInprocess", totalInprocess.abs());

						}

						// mpicado, cibp-2216
						BigDecimal availBal = null;
						BigDecimal bookBalance = null;
						if (account != null) {
							availBal = account.getPortalBalance();
							AbstractAccount accountDetails = userSession.getCustomerService().getAccountDetails(
									userSession.getCustomerProfile(), userSession.getBankingSession(), account, false);
							if (accountDetails != null) {
								if (account.getAccountSection().equals("S"))
									bookBalance = ((TVFrontendDepositAccount) accountDetails).getSavingsLedgerBalance();
								else if (account.getAccountSection().equals("C"))
									bookBalance = ((TVFrontendDepositAccount) accountDetails)
											.getCheckingLedgerBalance();
							}

						}
						// CIBP-2216
						if (name == null && transactions.size() == 0 && availBal != bookBalance)// No
																								// transactions
																								// case,
																								// add
																								// In
																								// Process
																								// section
						{
							name = new RunningBalanceTransaction();
							name.setBalance(bookBalance); // Other transactions
						}

						if (availBal != null && name != null && name.getBalance() != null
								&& transactionResponse.getPagesAvailable() == pagereq && ((availBal.abs()
										.add(name.getBalance().abs().negate()).abs().doubleValue()) >= 0.01d)) {
							RunningBalanceTransaction other = new RunningBalanceTransaction();
							other.setDescription("IN PROCESS OTHER TRANSACTIONS");
							BigDecimal difamt = availBal.add(name.getBalance().negate());
							other.setAmount(difamt.abs());
							if (difamt.doubleValue() >= 0.01d)
								other.setType("CR");
							else
								other.setType("DB");
							inprocess.add(other);
							if (other.getType().equals("CR"))
								totalInprocess = totalInprocess.add(other.getAmount());
							else
								totalInprocess = totalInprocess.subtract(other.getAmount());
							model.put("inprocess", setTransactionsCodeReason(inprocess));
							model.put("totalInprocess", totalInprocess.abs());
							if (totalInprocess.doubleValue() >= 0.0d)
								model.put("inprocessType", "CR");
							else
								model.put("inprocessType", "DB");

						}
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
							model.put("pdfId", 0);
							model.put("eBillsFlag",
									StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS));
						}

						if (transactions != null && transactions.size() > 0)
							model.put("transactions", setTransactionsCodeReason(transactions));
					}
				}
			}
			/***********************************************************************
			 * Here is the map for the pages available in a particular
			 * statement. *
			 ***********************************************************************/
			int totalPages = 1;
			if (transactionResponse != null)
				totalPages = transactionResponse.getPagesAvailable();

			LinkedHashMap<String, String> pagesMap = new LinkedHashMap<String, String>();
			for (int i = 1; i <= totalPages && totalPages > 1; i++) {
				pagesMap.put(String.valueOf(i), String.valueOf(i));

			}

			model.put("pagesMap", pagesMap);
			model.put("totalPages", totalPages);

		} // end if
		/**
		 * CCA current and Statement Transactions
		 */
		else if (account.getSubtype().equals("CCA")) {
			model.put("selectedAccount", "CCA");
			
			model.put("flagHideChargegardLink",StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2535));
			
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)) {
				//If the account url query string is present onSubmit is executed otherwise referenceData is executed.
				//This is to show the redemption widget to VPCBK CCA accounts on the top right corner.
				model.put("CASH_REWARDS_FLAG", StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS));
				//Only primary VPCBK owners are allowed to redeem.
				model.put("isPrimaryOwner", account.isPrimaryAccount());
				
				//If this data precent it is inserted for the vm to use, otherwise the vm, 
				//adds an ajax call that presents the response and inserted the data into 
				//TsysLoyaltyRewardsInfo in the TVFrontEndAccount object.
				if(account.getTsysLoyaltyRewardsInfo() != null) {
					if(account.getTsysLoyaltyRewardsInfo().getCanRedeemRewards() != null)
						model.put("tsysLoyaltyRewardsCanRedeem", account.getTsysLoyaltyRewardsInfo().getCanRedeemRewards().toString());
					if(account.getTsysLoyaltyRewardsInfo().getAvailableRewardsBalance() != null) {
						model.put("tsysLoyaltyRewardsBalance", account.getTsysLoyaltyRewardsInfo().getAvailableRewardsBalance());
						model.put("tsysLoyaltyRewardsIsGreaterOrEqualsTen", account.getTsysLoyaltyRewardsInfo().verifyCashRewardsAvailableBalance());
					}
				}
			}
			
			// MBCA-812 Show credit card benefits based on location
			model.put("mbca812", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA812));
			model.put("hasVirginIslandsAccount",
					userSession.hasVirginIslandAccount() || userSession.hasBritishVirginIslandAccount());

			if (cycle != null && !cycle.equals("1")) // Means Current
														// Transactions
			{
				// get statements transactions
				AbstractStatement stm = Utils.getStatementById(cycle, availableStatements);
			if (stm != null) {		  
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
					if (GenericValidator.isBlankOrNull(stm.getPdfID())) {
						model.put("pdfId", 0);
					} else {
						model.put("pdfId", stm.getPdfID());
					}
					model.put("eBillsFlag", StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS));
				}
				model.put("lastStatementBalance", stm.getEndBalance());

				if (showAllPages) {
					if (userSession.getDisplayTransactionDowntime()) {
						model.put("transactiondowntime", "true");
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[1]);
							
						} else {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[0]);
						}
					} else {
						List<AbstractTransaction> transactions = new LinkedList<AbstractTransaction>();
							TransactionRequest ccareq;
							for (int z = 1; z <= totalpages; z++) {

								ccareq = new TransactionRequest(
										Utils.generateCustomerInteractionEvent(request), stm, customerProfile, bankingSession,
										account, maxTransactions, z, false, null, null, null);

								transactionResponse = customerService.getCreditCardStatementTransactions(ccareq);
								if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
									return showForm(request, response, error, model);

								transactions.addAll(transactionResponse.getTransactions());
						}
						if (transactions != null && transactions.size() > 0)
							model.put("transactions", setTransactionsCodeReason(transactions));
					}
				} else {
					if (userSession.getDisplayTransactionDowntime()) {
						model.put("transactiondowntime", "true");
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[1]);
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[0]);						
						}
					} else {
						TransactionRequest ccareq = new TransactionRequest(
								Utils.generateCustomerInteractionEvent(request), stm, customerProfile, bankingSession,
								account, maxTransactions, pagereq, false, null, null, null);
						
						transactionResponse = customerService.getCreditCardStatementTransactions(ccareq);
						if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
							return showForm(request, response, error, model);

						List<AbstractTransaction> transactions = setTransactionsCodeReason(transactionResponse.getTransactions());
						if (transactions != null && transactions.size() > 0)
							model.put("transactions", transactions);
					}
				}
				// TODO me falta el finance charge transaction

			} else {
				// OBTAIN CURRENT TRANSACTIONS
				if (showAllPages) {
					if (userSession.getDisplayTransactionDowntime()) {
						model.put("transactiondowntime", "true");
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[1]);
						} else {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[0]);
						}
					} else {
						List<AbstractTransaction> transactions = new LinkedList<AbstractTransaction>();
						TransactionRequest ccareq2;
							for (int z = 1; z <= totalpages; z++) {
								ccareq2 = CreateTransactionRequest(request, customerProfile, bankingSession, account, z); 
								
								/**
								 * MBNA-1955 Method rsaAnalyze : CIBP-WEB microservice integration
								 * 
								 */
								//Microservice implementation
								//transactionResponse = CreditCardFacade.getInstance().getCreditCardCurrentTransactionsMicroservice(ccareq2); 56221
								transactionResponse = customerService.getCreditCardCurrentTransactions(ccareq2);

								if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
									return showForm(request, response, error, model);

								transactions.addAll(transactionResponse.getTransactions());

							}
							if (transactions != null && transactions.size() > 0)
								model.put("transactions", transactions);
					}
				} else {
					if (userSession.getDisplayTransactionDowntime()) {
						model.put("transactiondowntime", "true");
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[1]);
						} else {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[0]);
						}
					} else {
						TransactionRequest ccareq2 = new TransactionRequest(
								Utils.generateCustomerInteractionEvent(request), null, customerProfile, bankingSession,
								account, maxTransactions, pagereq, false, null, null, null);
						/**
						 * MBNA-1955
						 * Method rsaAnalyze : CIBP-WEB microservice integration
						 * 
						 */
						//Microservice implementation
						//transactionResponse = CreditCardFacade.getInstance().getCreditCardCurrentTransactionsMicroservice(ccareq2);56221
						transactionResponse = customerService.getCreditCardCurrentTransactions(ccareq2);

						if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
							return showForm(request, response, error, model);

						List<AbstractTransaction> transactions = transactionResponse.getTransactions();
						if (transactions != null && transactions.size() > 0)
							model.put("transactions", transactions);
					}
				}
			}
			
			
			} else {
				
				// OBTAIN CURRENT TRANSACTIONS
				if (showAllPages) {
					if (userSession.getDisplayTransactionDowntime()) {
						model.put("transactiondowntime", "true");
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[1]);
						} else {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[0]);
						}
					} else {
						List<AbstractTransaction> transactions = new LinkedList<AbstractTransaction>();
						TransactionRequest ccareq2;
						for (int z = 1; z <= totalpages; z++) {
							ccareq2 = CreateTransactionRequest(request, customerProfile, bankingSession, account, z); 
																				
																			   
							/**
				   
							 * MBNA-1955 Method rsaAnalyze : CIBP-WEB microservice integration
							 * 
							 */
							// Microservice implementation
							//transactionResponse = CreditCardFacade.getInstance().getCreditCardCurrentTransactionsMicroservice(ccareq2);
							transactionResponse = customerService.getCreditCardCurrentTransactions(ccareq2);

							if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
								return showForm(request, response, error, model);

							transactions.addAll(transactionResponse.getTransactions());

						}
						if (transactions != null && transactions.size() > 0)
							model.put("transactions", setTransactionsCodeReason(transactions));
					}
				} else {
					if (userSession.getDisplayTransactionDowntime()) {
						model.put("transactiondowntime", "true");
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[1]);
						} else {
							model.put("transactiondowntimemessage",
									userSession.getDisplayTransactionDowntimeMessages()[0]);
						}
					} else {
						TransactionRequest ccareq2 = new TransactionRequest(
								Utils.generateCustomerInteractionEvent(request), null, customerProfile, bankingSession,
								account, maxTransactions, pagereq, false, null, null, null);
						/**
				  
						 * MBNA-1955 Method rsaAnalyze : CIBP-WEB microservice integration
						 * 
						 */
						// Microservice implementation
						//transactionResponse = CreditCardFacade.getInstance().getCreditCardCurrentTransactionsMicroservice(ccareq2);
						transactionResponse = customerService.getCreditCardCurrentTransactions(ccareq2);
																					  

						if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
							return showForm(request, response, error, model);

						List<AbstractTransaction> transactions = setTransactionsCodeReason(transactionResponse.getTransactions());
						if (transactions != null && transactions.size() > 0)
							model.put("transactions", transactions);
					}
				}
			}
			/***********************************************************************
			 * Here is the map for the pages available in a particular
			 * statement. *
			 ***********************************************************************/

			int totalPages = 1;
			if (transactionResponse != null)
				totalPages = transactionResponse.getPagesAvailable();

			LinkedHashMap<String, String> pagesMap = new LinkedHashMap<String, String>();
			for (int i = 1; i <= totalPages && totalPages > 1; i++) {
				pagesMap.put(String.valueOf(i), String.valueOf(i));
			}
			if (account.getSubtype().equals("CCA")) {
				if (account.getCardPlasticAccountRecords() != null
						&& account.getCardPlasticAccountRecords().length > 0) {
					final CardPlasticAccountRecord[] record = account.getCardPlasticAccountRecords(); //Get card plastic account records
					final int recordLength = record.length; //Record length
					for (int i = 0; i < recordLength; i++) {
						String cardPanId = record[i].getCardPANId();
						String hiddenPanId;
						try {
							hiddenPanId = "x" + cardPanId.substring(cardPanId.length() - 4, cardPanId.length());
						} catch (Exception e) {
							logger.error("CardPANid from acctInq NOT NULL but less than 4 digits: ", e);
							hiddenPanId = "xxxx";
						}
						if (activity.equalsIgnoreCase(hiddenPanId)) {
							model.put("filterByPlastic", true);
							break;
						} else
							model.put("filterByPlastic", false);
					}
				} else
					model.put("filterByPlastic", false);

				if (transactionResponse != null) {
					if (transactionResponse.isCardPanId()) {
						model.put("showCardColumn", true);
						if (account.getCardPlasticAccountRecords() != null
								&& account.getCardPlasticAccountRecords().length > 0) {
							final CardPlasticAccountRecord[] record = account.getCardPlasticAccountRecords(); //Get card plastic account records
							final int recordLength = record.length; //Record length
							for (int i = 0; i < recordLength; i++) {
								String cardPanId = record[i].getCardPANId();
								String hiddenPanId;
								try {

									hiddenPanId = "x" + cardPanId.substring(cardPanId.length() - 4, cardPanId.length());
								} catch (Exception e) {
									logger.error("CardPANid from acctInq NOT NULL but less than 4 digits: ", e);
									hiddenPanId = "xxxx";
								}
								if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
									activityMap.put(hiddenPanId, "Tarjeta que termina en " + hiddenPanId);
								} else {
									activityMap.put(hiddenPanId, "Credit card ending in " + hiddenPanId);
								}

							}
						}
					} else
						model.put("showCardColumn", false);
					if ((unversalProducts.indexOf(account.getAccountProductId()) != -1)
							&& transactionResponse.isChargeCard()
							&& "sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						activityMap.put("C", "Transacciones a cargo");
					}
					if ((unversalProducts.indexOf(account.getAccountProductId()) != -1)
							&& transactionResponse.isRevolvingCard()
							&& "sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						activityMap.put("R", "Transacciones a cr&eacute;dito");
					}
					if ((unversalProducts.indexOf(account.getAccountProductId()) != -1)
							&& transactionResponse.isChargeCard()
							&& !"sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						activityMap.put("C", "Charge Transactions");
					}
					if ((unversalProducts.indexOf(account.getAccountProductId()) != -1)
							&& transactionResponse.isRevolvingCard()
							&& !"sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						activityMap.put("R", "Credit Transactions");
					}
				}
			}
			model.put("pagesMap", pagesMap);
			model.put("totalPages", totalPages);

		} // end else cca
		/**
		 * PRM - Premia Current and Statement Transactions Agregated and not
		 * agregated.
		 */
		else if (account.getSubtype().equals("PRM")) {
			model.put("selectedAccount", "PRM");
			// PRM Transactions only are by category
			AbstractStatement stm = Utils.getStatementById(cycle, availableStatements);
			model.put("endbalance", stm.getEndBalance());

			PremiaTransactionResponse prmResponse = null;

			if ("CARD".equals(category)) // Tarjetas
			{
				String[] cats = { "1" };
				model.put("selectCategory", "CARD");
				if (userSession.getDisplayTransactionDowntime()) {
					model.put("transactiondowntime", "true");
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[1]);
					} else {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[0]);
					}
				} else {
					TransactionRequest prmreq3 = new TransactionRequest(Utils.generateCustomerInteractionEvent(request),
							stm, customerProfile, bankingSession, account, maxTransactions, pagereq, false, null, null,
							null);
					prmreq3.setCategories(cats);
					prmResponse = customerService.getPremiaTransactionsByCategory(prmreq3);

					if (prmResponse.getStatusResult() != ResultStatus.SUCCESS)
						return showForm(request, response, error, model);

					model.put("transactions", setTransactionsCodeReason(prmResponse.getTransactions()));
					model.put("categoryTotal", prmResponse.getCategoryTotal());
				}
			} else if ("ACCOUNT".equals(category)) // Cuentas
			{
				String[] cats = { "2" };
				model.put("selectCategory", "ACCOUNT");
				if (userSession.getDisplayTransactionDowntime()) {
					model.put("transactiondowntime", "true");
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[1]);
					} else {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[0]);
					}
				} else {
					TransactionRequest prmreq4 = new TransactionRequest(Utils.generateCustomerInteractionEvent(request),
							stm, customerProfile, bankingSession, account, maxTransactions, pagereq, false, null, null,
							null);
					prmreq4.setCategories(cats);
					prmResponse = customerService.getPremiaTransactionsByCategory(prmreq4);

					if (prmResponse.getStatusResult() != ResultStatus.SUCCESS)
						return showForm(request, response, error, model);

					model.put("transactions", setTransactionsCodeReasonCall(prmResponse.getTransactions()));
					model.put("categoryTotal", prmResponse.getCategoryTotal());
				}
			} else if ("ELECTRONIC".equals(category)) // Servicios Electronicos
			{
				String[] cats = { "3" };
				model.put("selectCategory", "ELECTRONIC");
				if (userSession.getDisplayTransactionDowntime()) {
					model.put("transactiondowntime", "true");
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[1]);
					} else {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[0]);
					}
				} else {
					TransactionRequest prmreq5 = new TransactionRequest(Utils.generateCustomerInteractionEvent(request),
							stm, customerProfile, bankingSession, account, maxTransactions, pagereq, false, null, null,
							null);
					prmreq5.setCategories(cats);
					prmResponse = customerService.getPremiaTransactionsByCategory(prmreq5);

					if (prmResponse.getStatusResult() != ResultStatus.SUCCESS)
						return showForm(request, response, error, model);

					model.put("transactions", setTransactionsCodeReasonCall(prmResponse.getTransactions()));
					model.put("categoryTotal", prmResponse.getCategoryTotal());
				}

			} else if ("LOAN".equals(category)) // Prestamos
			{
				String[] cats = { "4" };
				model.put("selectCategory", "LOAN");
				if (userSession.getDisplayTransactionDowntime()) {
					model.put("transactiondowntime", "true");
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[1]);
					} else {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[0]);
					}
				} else {
					TransactionRequest prmreq6 = new TransactionRequest(Utils.generateCustomerInteractionEvent(request),
							stm, customerProfile, bankingSession, account, maxTransactions, pagereq, false, null, null,
							null);
					prmreq6.setCategories(cats);
					prmResponse = customerService.getPremiaTransactionsByCategory(prmreq6);

					if (prmResponse.getStatusResult() != ResultStatus.SUCCESS)
						return showForm(request, response, error, model);

					model.put("transactions", setTransactionsCodeReasonCall(prmResponse.getTransactions()));
					model.put("categoryTotal", prmResponse.getCategoryTotal());
				}

			} else if ("ADJUST".equals(category)) // Servicios Electronicos
			{
				String[] cats = { "5", "7", "8" };
				model.put("selectCategory", "ADJUST");
				if (userSession.getDisplayTransactionDowntime()) {
					model.put("transactiondowntime", "true");
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[1]);
					} else {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[0]);
					}
				} else {
					// Adjustment include(5=Promo, 7=Adjust, 8=expiration))
					TransactionRequest prmreq7 = new TransactionRequest(Utils.generateCustomerInteractionEvent(request),
							stm, customerProfile, bankingSession, account, maxTransactions, pagereq, false, null, null,
							null);
					prmreq7.setCategories(cats);
					prmResponse = customerService.getPremiaTransactionsByCategory(prmreq7);

					if (prmResponse.getStatusResult() != ResultStatus.SUCCESS)
						return showForm(request, response, error, model);

					model.put("transactions", setTransactionsCodeReasonCall(prmResponse.getTransactions()));
					model.put("categoryTotal", prmResponse.getCategoryTotal());
				}

			} else if ("REDEMPT".equals(category)) // Redenciones
			{
				model.put("selectCategory", "REDEMPT");
				boolean inprocess = true;
				// Get Inprocess Redemptions
				if (userSession.getDisplayTransactionDowntime()) {
					model.put("transactiondowntime", "true");
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[1]);
					} else {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[0]);
					}
				} else {
					RewardOrdersRequest orderRq = new RewardOrdersRequest(
							Utils.generateCustomerInteractionEvent(request), customerProfile, bankingSession, account,
							maxTransactions, 1, inprocess);
					RewardOrdersResponse orderRs = customerService.getPremiaRedemptions(orderRq);

					List<RewardOrder> inprocessOrders = orderRs.getRewards();
					model.put("inprocessRedemp", inprocessOrders);
					BigDecimal totalInprocess = new BigDecimal(0.0d);

					if (inprocessOrders != null)
						for (Iterator<RewardOrder> iterator = inprocessOrders.iterator(); iterator.hasNext();) {
							RewardOrder trans = (RewardOrder) iterator.next();
							totalInprocess = totalInprocess.add(new BigDecimal(trans.getPointAmt()));
						}
					model.put("totalInProcess", totalInprocess);

					// Get Processed Redemptions
					inprocess = false;
					RewardOrdersRequest orderRq1 = new RewardOrdersRequest(
							Utils.generateCustomerInteractionEvent(request), customerProfile, bankingSession, account,
							maxTransactions, 1, inprocess);
					RewardOrdersResponse orderRs1 = customerService.getPremiaRedemptions(orderRq1);
					List<RewardOrder> orders = orderRs1.getRewards();
					model.put("redempts", orders);
					BigDecimal totalorders = new BigDecimal(0.0d);
					if (orders != null)
						for (Iterator<RewardOrder> iterator = orders.iterator(); iterator.hasNext();) {
							RewardOrder trans = (RewardOrder) iterator.next();
							trans.setStatusDescription("transaction.redem.status." + trans.getStatusDescription());
							totalorders = totalorders.add(new BigDecimal(trans.getPointAmt()));
						}
					model.put("totalOrders", totalorders);
				}
			} else // if(category.equals("ALL"))
			{
				// Category = All Categories
				model.put("selectCategory", "ALL");
				String[] cats = { "1", "2", "3", "4", "5", "6", "7", "8" };
				if (userSession.getDisplayTransactionDowntime()) {
					model.put("transactiondowntime", "true");
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[1]);
					} else {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[0]);
					}
				} else {
					TransactionRequest prmreq2 = new TransactionRequest(Utils.generateCustomerInteractionEvent(request),
							stm, customerProfile, bankingSession, account, maxTransactions, pagereq, false, null, null,
							null);
					prmreq2.setCategories(cats);
					prmResponse = customerService.getPremiaTransactionsByCategory(prmreq2);

					if (prmResponse.getStatusResult() != ResultStatus.SUCCESS)
						return showForm(request, response, error, model);

					model.put("transactions", setTransactionsCodeReasonCall(prmResponse.getTransactions()));
					model.put("categoryTotal", prmResponse.getCategoryTotal());
				}

			}

			// Buscar multiples paginas no aplica a redenciones
			if (!"REDEMPT".equals(category)) {
				int totalPages = 1;
				if (prmResponse != null)
					totalPages = prmResponse.getTotalPages();

				LinkedHashMap<String, String> pagesMap = new LinkedHashMap<String, String>();
				for (int i = 1; i <= totalPages && totalPages > 1; i++) {
					pagesMap.put(String.valueOf(i), String.valueOf(i));

				}

				model.put("pagesMap", pagesMap);
				model.put("totalPages", totalPages);
			}

		}
		model.put("activityMap", activityMap);

		return showForm(request, response, error, model);

	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		TransactionFormBean transactionData = (TransactionFormBean) command;
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();
		Map<String, Object> model = new HashMap<>();
		Cookie versionCookie = CookieUtils.getCookie(request, "client-version");
		List<TVFrontendAccount> portalAccounts = userSession.getPortalAccounts();
		model.put("MBSFE346", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE346).toString());
		model.put("pinrequeststatus", this.getPinRequestStatus());
		model.put("eBillsFlag", StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS));
		// MBCA298
		model.put("MBCA298_Flag", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA298));
		model.put("CommEstaFlag", StaticMessageSource.isFlagEnabled(StaticMessageSource.MOD_COMMERCIAL_STATEMENTS));
		String isTab = request.getParameter("isTab");
		model.put("isTab", !GenericValidator.isBlankOrNull(isTab) && isTab.equals("1"));
		
		final String flagMBSE2513 = "flagMBSE2513"; //model key for flag MBSE2513
		model.put(flagMBSE2513, StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513));

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		if (this.getShowAllMessageSourceStatus())
			model.put("hideShowAll", true);
		else
			model.put("hideShowAll", false);

		if (profile != null && profile.hasEntitlement(EntitlementType.BILLPAY) && customerService
				.getGlobalEntitlement(EntitlementType.BILLPAY).getStatus() == EntitlementStatus.ENABLED)
			model.put("billpayEnable", true);

		TVFrontendAccount firstAccount = null;
		model.put("totalPages", 1);
		
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			model.put("Lang", "sp");
		else
			model.put("Lang", "en");
		List<TVFrontendAccount> userAccounts = userSession.getAccounts();
		AccountMaskUtil mask = new AccountMaskUtil();
		Map<String, String> stmtAccountsMap = new LinkedHashMap<String, String>();
		Map<String, TVFrontendAccount> stmtAccounts = new LinkedHashMap<String, TVFrontendAccount>();

		if (userAccounts != null)
			for (Iterator<TVFrontendAccount> iter = userAccounts.iterator(); iter.hasNext();) {
				TVFrontendAccount element = (TVFrontendAccount) iter.next();
				if (element.getFeatures().isShowStatement() && !element.getHide()) {
					if (firstAccount == null)
						firstAccount = element;

					String key = StringUtils.abbreviate(element.getNickname(), 15) + " "
							+ mask.mask(element.getAccountNumber())
							+ ((element.getAccountNumberSuffix() != null
									&& !element.getAccountNumberSuffix().equals(""))
											? " " + element.getAccountNumberSuffix() : "");
					if (element.getPortalBalance() != null) {
						if (element.getProductDefinition().getProductType().equals("PRM"))
							key = key + " " + new NumberTool().format("integer", element.getPortalBalance());
						else
							key = key + " " + new NumberTool().format("currency", element.getPortalBalance());
					}

					stmtAccounts.put(String.valueOf(element.getFrontEndId()), element);
					stmtAccountsMap.put(String.valueOf(element.getFrontEndId()), key);
				}
			}
		model.put("stmtAccountsMap", stmtAccountsMap);
		model.put("stmtAccounts", stmtAccounts);
		LinkedHashMap<String, String> activityMap = new LinkedHashMap<String, String>();
		LinkedHashMap<String, String> categoryMap = new LinkedHashMap<String, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
			categoryMap.put("ALL", "todas");
			categoryMap.put("CARD", "tarjetas");
			categoryMap.put("ACCOUNT", "cuentas");
			categoryMap.put("ELECTRONIC", "Servicios Electronicos");
			categoryMap.put("LOAN", "Prestamos");
			categoryMap.put("ADJUST", "ajustes");
			categoryMap.put("REDEMPT", "redenciones");

		} else {
			categoryMap.put("ALL", "all");
			categoryMap.put("CARD", "cards");
			categoryMap.put("ACCOUNT", "accounts");
			categoryMap.put("ELECTRONIC", "electronic services");
			categoryMap.put("LOAN", "loans");
			categoryMap.put("ADJUST", "adjustments");
			categoryMap.put("REDEMPT", "redemptions");
		}
		model.put("categoryMap", categoryMap);

		// CIBP-2769 Enable PFM for new users of OAO
		boolean hasNewOaoAccount = false;
		if (profile.getEnrollmentSource().equals(CustomerEnrollmentSource.ONLINE_AO)) {
			TVFrontendAccount acct = null;
			for (Iterator<TVFrontendAccount> iterator = portalAccounts.iterator(); iterator.hasNext();) {
				acct = (TVFrontendAccount) iterator.next();
				if (acct.isNewAccount() && acct.getAccountProductId().equals("081")
						&& profile.getEnrollmentAccountNumber().equals(acct.getAccountNumber())) {
					hasNewOaoAccount = true;
					break;
				}
			}
		}
		// CIBP-2769 End

		// PFM - Get the PFM entitlement
		PFMUtil pfmUtil = new PFMUtil();
		boolean showUpdateInProcess = false;
		CustomerEntitlement entitlementByType = null;

		if (!profile.hasEntitlement(EntitlementType.PFM)) {
			if (pfmAutomaticEntitlementCreation
					|| (!pfmAutomaticEntitlementCreation && userSession.isNewUserForAutomaticPFMEntitlementCreation())
					|| hasNewOaoAccount) {
				pfmUtil.createPfmEntitlement(userSession.getCustomerService(), profile);
			}
			// First time in PFM
			if (userSession.getPfmFirstTime() == null)
				userSession.setPfmFirstTime("Y");
		} else {

			if (userSession.getPfmFirstTime() == null)
				userSession.setPfmFirstTime("N");
		}

		model.put("pfmFirstTime", userSession.getPfmFirstTime().equals("Y"));
		entitlementByType = profile.getEntitlementByType(EntitlementType.PFM);

		// PFM - is the PFM system up?
		if (customerService.getGlobalEntitlement(EntitlementType.PFM).getStatus() == EntitlementStatus.ENABLED) {

			// Set up the view PFM and view portal variables
			if (entitlementByType != null) {
				// If the system is in update AND the pfm user id does not
				// exist, DO NOT SHOW PFM
				if (entitlementByType.getString("PFM_USER_ID") != null
						&& entitlementByType.getString("PFM_USER_ID").equals("") && userSession.isInPfmUpdatedLookedUp()
						&& userSession.isInPfmUpdate()) {
					model.put("viewPfmAdvTrx", "N");
					model.put("viewPfm", "N");
					model.put("pfmEnabled", "N");
					showUpdateInProcess = true;
				} else {
					/*
					 * CIBP-2702 - PFM will be hidden and the classic view will
					 * be the default if we are in PFM UPDATE
					 */
					if (userSession.isInPfmUpdatedLookedUp() && userSession.isInPfmUpdate())
						model.put("viewPfmAdvTrx", "N");
					else
						model.put("viewPfmAdvTrx", entitlementByType.getString("VIEW_ADV_TRX"));

					model.put("viewPfm", entitlementByType.getString("VIEW_PFM"));
					model.put("pfmEnabled", "Y");

					// PFM - Get the past 24 months
					Date[] past24Months = new Date[24];
					Calendar cal = Calendar.getInstance();
					past24Months[0] = cal.getTime();
					model.put("past24Months_0", past24Months[0]);

					for (int i = 1; i < 24; i++) {
						cal.add(Calendar.MONTH, -1);
						past24Months[i] = cal.getTime();
						model.put("past24Months_" + i, past24Months[i]);
					}

					// PFM - Get the language
					boolean isEnglish = userSession.getLanguage().equalsIgnoreCase("en");
					model.put("isenglish", isEnglish);
				}
			} else {
				model.put("viewPfmAdvTrx", "N");
				model.put("viewPfm", "N");
				model.put("pfmEnabled", "N");
			}
		} else {
			model.put("viewPfmAdvTrx", "N");
			model.put("viewPfm", "N");
			model.put("pfmEnabled", "N");
		}
		model.put("showUpdateInProcess", showUpdateInProcess);
		
		// MBSFE-1187
		TVFrontendAccount accountCharges = null;
		if(transactionData.getAccountInfo() != null){
			accountCharges = transactionData.getAccountInfo();
		}
		else{
			accountCharges = firstAccount;
		}
		
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1187)) {
			if (accountCharges != null && (accountCharges.getSubtype().equalsIgnoreCase("IDA")
							|| accountCharges.getSubtype().equalsIgnoreCase("PFC"))) {
				try {
					DepositAccount da = (DepositAccount) accountCharges.getBackendObject();
					model.put("hasValidCode", Utils.haveType(this.chargesSummaryCodes, da.getRecords().getProductId()));
					
					if(userSession.getAccountsChargesStatements() == null) {
						userSession.setAccountsChargesStatements(new HashMap<>());
					}
					if(userSession.getAccountsChargesStatements().get(accountCharges.getAccountNumber()) == null){
						List<AbstractStatement> availableStatements = null;
						if (accountCharges != null && accountCharges.getStatements() == null) {
							StatementRequest stmtrq = new StatementRequest(Utils.generateCustomerInteractionEvent(request), profile,
									userSession.getBankingSession(), accountCharges, maxStatements);
							availableStatements = customerService.getAvailableStatements(stmtrq).getStatementList();
							accountCharges.setStatements(availableStatements);
						} else if (accountCharges != null)
							availableStatements = accountCharges.getStatements();
						userSession.getAccountsChargesStatements().put(accountCharges.getAccountNumber(), this.totalAccountCharges(availableStatements));
					}
					model.put("accountChargesStatements", userSession.getAccountsChargesStatements().get(accountCharges.getAccountNumber()));
					
				} catch (Exception e) {
					logger.error("Error while casting account for charges summary.");
				}
			}
		}

		model.put("MBSFE1187", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1187));
		// END MBSFE-1187

		BankingSession bankingSession = userSession.getBankingSession();
		String key = transactionData.getAccount();
		String activity = transactionData.getActivity();
		model.put("activity", activity);
		if (GenericValidator.isBlankOrNull(key) && firstAccount != null) {
			model.put("selectCategory", "ALL");
			model.put("accountLast4",
					" x" + firstAccount.getAccountNumber().substring(firstAccount.getAccountNumber().length() - 4));
			if (firstAccount != null) {
				model.put("saccount", firstAccount.getFrontEndId());
				model.put("account", firstAccount);
				transactionData.setAccount(firstAccount.getFrontEndId());
			}
			model.put("scycle", 1);
			model.put("spageReq", 1);

			if (firstAccount != null && (firstAccount.getAccountProductId().equalsIgnoreCase("009")// Acceso
																									// Popular
					|| firstAccount.getAccountProductId().equalsIgnoreCase("020")// Avance
																					// 50
					|| firstAccount.getAccountProductId().equalsIgnoreCase("023")// Multicuenta
																					// Staff
					|| firstAccount.getAccountProductId().equalsIgnoreCase("024")// Popular
																					// Plus
					|| firstAccount.getAccountProductId().equalsIgnoreCase("025")// Multicuenta
																					// Popular
					|| firstAccount.getAccountProductId().equalsIgnoreCase("026")// Ideal
					|| firstAccount.getAccountProductId().equalsIgnoreCase("028")// MaxiPremium
					|| firstAccount.getAccountProductId().equalsIgnoreCase("029")// Private
																					// Management
																					// Account
					|| firstAccount.getAccountProductId().equalsIgnoreCase("049")// Ahorro
																					// a
																					// Toda
																					// Hora
																					// Staff
					|| firstAccount.getAccountProductId().equalsIgnoreCase("051")// Ahorro
																					// 24
					|| firstAccount.getAccountProductId().equalsIgnoreCase("052")// Ahorro
																					// a
																					// Toda
																					// Hora
					|| firstAccount.getAccountProductId().equalsIgnoreCase("069")// Multicuenta
																					// Subsidiary
																					// Staff
					|| firstAccount.getAccountProductId().equalsIgnoreCase("072")// Ahorro
																					// a
																					// Toda
																					// Hora
																					// Subsidiary
																					// Staff
					|| firstAccount.getAccountProductId().equalsIgnoreCase("081")// e-account
					|| firstAccount.getAccountProductId().equalsIgnoreCase("087")))// CIMA
			{
				model.put("showOdLimitLink", true);
			} else
				model.put("showOdLimitLink", false);

			if (firstAccount != null && (firstAccount.getSubtype().equals("MLA")
					|| firstAccount.getSubtype().equals("LEA") || firstAccount.getSubtype().equals("RVA"))) {

				model.put("ACH_PAYMENT_NT", StaticMessageSource.isFlagEnabled(StaticMessageSource.ACH_PAYMENT_NT));

				model.put("isTransactional", AccountUtils.getIsTransactional(userSession.getCustomerProfile()));

				model.put("selectedAccount", "MLA");
				model.put("flagMortgage", StaticMessageSource.isFlagEnabled(StaticMessageSource.MORTGAGE_AUTO_ACH));
				TransactionsResponse transactionResponse = null;
				final String MLA="MLA"; //account subtype literal
				
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513) && firstAccount.getSubtype().equals(MLA)) {
					final String tabNum = "0"; //literal for number 0
					
					transactionData.setTabNum(tabNum);
				}
				// get MLA transactions
				if (userSession.getDisplayTransactionDowntime()) {
					model.put("transactiondowntime", "true");
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[1]);
					} else {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[0]);
					}
				} else {
					Calendar startDate = Calendar.getInstance();
					startDate.add(Calendar.MONTH, -16);
					startDate.set(Calendar.DAY_OF_MONTH, 1);
					TransactionRequest req = new TransactionRequest(Utils.generateCustomerInteractionEvent(request),
							null, profile, bankingSession, firstAccount, maxTransactions, 1, false, startDate, null,
							null);
					transactionResponse = customerService.getMortgageTransactions(req);
					if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
						return model;
					List<AbstractTransaction> transactions = setTransactionsCodeReasonCall(transactionResponse.getTransactions());
					if (transactions != null && transactions.size() > 0)
						model.put("transactions", transactions);
				}
				return model;
			}

			AbstractAccount accountDetails = null;
			if (firstAccount != null && firstAccount.getSubtype().equals("CCA")) {
				if (activity.contains("x"))
					model.put("cardnumberlabel", true);
				else
					model.put("cardnumberlabel", false);
				if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
					activityMap.put("All", "Todas");
				else
					activityMap.put("All", "All");

				transactionData.setAccountInfo(firstAccount);

				if (versionCookie == null
						|| (versionCookie != null && !"mobile".equalsIgnoreCase(versionCookie.getValue()))) {
					if (firstAccount.getCardPlasticAccountRecords().length < 1) {
						// TVFrontendCreditAccount frontendCreditAccount =
						// (TVFrontendCreditAccount)customerService.getAccountDetails(customerProfile,
						// bankingSession, account, false);
						accountDetails = userSession.getCustomerService().getAccountDetails(
								userSession.getCustomerProfile(), userSession.getBankingSession(), firstAccount, false);
						TVFrontendCreditAccount frontendCreditAccount = (TVFrontendCreditAccount) accountDetails;
						firstAccount.setCardPlasticAccountRecords(frontendCreditAccount.getCardPlasticAccountRecords());
						TVAccountFactory fact = new TVAccountFactory();
						final CardPlasticAccountRecord[] record = firstAccount.getCardPlasticAccountRecords(); //Get card plastic account records
						List<CardAccount> associatedCards = new LinkedList<CardAccount>();
						for (int i = 0; i < firstAccount.getCardPlasticAccountRecords().length; i++) {
							if (!(record[i].getCardPANId().equalsIgnoreCase(firstAccount.getAccountNumber()))) {
								CardAccount ccaAccount = fact.createCardPlasticAccount(record[i].getCardPANId());
								associatedCards.add(ccaAccount);
							}
						}
						firstAccount.setAssociatedCards(associatedCards);
					}
				}

				if (firstAccount.getSubtype().equalsIgnoreCase("CCA")) {
					if (versionCookie == null
							|| (versionCookie != null && !"mobile".equalsIgnoreCase(versionCookie.getValue()))) {
						transactionData.setImgPath(this.getImagePath(transactionData, userSession));
					}
				}
			} else if ("IDA".equalsIgnoreCase(firstAccount.getSubtype())
					|| "PFC".equalsIgnoreCase(firstAccount.getSubtype())) {
				// tab Number for Deposit Accounts
				String trxTabNum = request.getParameter("trxTabNum");
				if (trxTabNum == null || trxTabNum.equalsIgnoreCase(""))
					trxTabNum = "1";

				transactionData.setTrxTabNum(trxTabNum);

				transactionData.setAccountInfo(firstAccount);

			}

			List<AbstractStatement> availableStatements = null;
			if (firstAccount != null && firstAccount.getStatements() == null) {
				StatementRequest stmtrq = new StatementRequest(Utils.generateCustomerInteractionEvent(request), profile,
						bankingSession, firstAccount, maxStatements);
				availableStatements = customerService.getAvailableStatements(stmtrq).getStatementList();
				firstAccount.setStatements(availableStatements);
			} else if (firstAccount != null)
				availableStatements = firstAccount.getStatements();

			LinkedHashMap<String, AbstractStatement> accountStatementMap = new LinkedHashMap<String, AbstractStatement>();
			LinkedHashMap<String, String> statementsMap = new LinkedHashMap<String, String>();
			statementsMap = Utils.getStatements(availableStatements, accountStatementMap, Utils.getRSALanguage(request),
					"PRM".equalsIgnoreCase(firstAccount.getSubtype()));
			model.put("statementsMap", statementsMap);
			TransactionsResponse transactionResponse = null;

			if ("IDA".equalsIgnoreCase(firstAccount.getSubtype())
					|| "PFC".equalsIgnoreCase(firstAccount.getSubtype())) {

				// get current transactions
				model.put("selectedAccount", "DEP");

				if (userSession.getDisplayTransactionDowntime()) {
					model.put("transactiondowntime", "true");
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[1]);
					} else {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[0]);
					}
				} else {
					TransactionRequest depreq2 = new TransactionRequest(Utils.generateCustomerInteractionEvent(request),
							null, profile, bankingSession, firstAccount, maxTransactions, 1, true, null, null, null);
					transactionResponse = customerService.getDepositCurrentTransactions(depreq2);
					if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
						return model;

					List<AbstractTransaction> transactions = transactionResponse.getTransactions();
					List<AbstractTransaction> inprocess = new LinkedList<AbstractTransaction>();
					BigDecimal totalInprocess = new BigDecimal(0.0d);
					for (Iterator<AbstractTransaction> iterator = transactions.iterator(); iterator.hasNext();) {
						RunningBalanceTransaction name = (RunningBalanceTransaction) iterator.next();
						if (name.getDate() == null || name.getDescription().toUpperCase().indexOf("IN PROCESS") != -1) {
							inprocess.add(name);
							if (name.getType().equals("CR"))
								totalInprocess = totalInprocess.add(name.getAmount());
							else
								totalInprocess = totalInprocess.subtract(name.getAmount());
						}
					}
					if (inprocess != null && inprocess.size() > 0) {
						transactions.removeAll(inprocess);
						model.put("inprocess", setTransactionsCodeReasonCall(inprocess));
						if (totalInprocess.doubleValue() >= 0.0d)
							model.put("inprocessType", "CR");
						else
							model.put("inprocessType", "DB");
						model.put("totalInprocess", totalInprocess.abs());
					}

					if (transactions != null && transactions.size() > 0)
						model.put("transactions", setTransactionsCodeReasonCall(transactions));
				}

				int totalPages = 1;
				if (transactionResponse != null)
					totalPages = transactionResponse.getPagesAvailable();

				LinkedHashMap<String, String> pagesMap = new LinkedHashMap<String, String>();
				for (int i = 1; i <= totalPages && totalPages > 1; i++) {
					pagesMap.put(String.valueOf(i), String.valueOf(i));

				}
				model.put("pagesMap", pagesMap);
				model.put("totalPages", totalPages);
			} // end if
			else if (firstAccount.getSubtype().equals("CCA")) {
				model.put("selectedAccount", "CCA");
				model.put("flagHideChargegardLink",StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2535));

				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)) {
					//If the account url query string is not present this is executed.
					//This is to show the redemption widget to VPCBK CCA accounts on the top right corner.
					model.put("CASH_REWARDS_FLAG", StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS));
					//Only primary VPCBK owners are allowed to redeem.
					model.put("isPrimaryOwner", firstAccount.isPrimaryAccount());
					//If this data precent it is inserted for the vm to use, otherwise the vm, 
					//adds an ajax call that presents the response and inserted the data into 
					//TsysLoyaltyRewardsInfo in the TVFrontEndAccount object.
					if(firstAccount.getTsysLoyaltyRewardsInfo() != null) {
						if(firstAccount.getTsysLoyaltyRewardsInfo().getCanRedeemRewards() != null)
							model.put("tsysLoyaltyRewardsCanRedeem", firstAccount.getTsysLoyaltyRewardsInfo().getCanRedeemRewards().toString());
						if(firstAccount.getTsysLoyaltyRewardsInfo().getAvailableRewardsBalance() != null) {
							model.put("tsysLoyaltyRewardsBalance", firstAccount.getTsysLoyaltyRewardsInfo().getAvailableRewardsBalance());
							model.put("tsysLoyaltyRewardsIsGreaterOrEqualsTen", firstAccount.getTsysLoyaltyRewardsInfo().verifyCashRewardsAvailableBalance());
						}
					}
				}
				
				// MBCA-812 Show credit card benefits based on location
				model.put("mbca812", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA812));
				model.put("hasVirginIslandsAccount",
						userSession.hasVirginIslandAccount() || userSession.hasBritishVirginIslandAccount());

				if (unversalProducts.indexOf(firstAccount.getAccountProductId()) != -1) {
					model.put("universalProduct", true);
					TVFrontendAccount account = transactionData.getAccountInfo();

					AbstractStatement stm = Utils.getStatementById("2", availableStatements);
					if (stm != null) {
						model.put("noStatement", false);
						if (stm.getChargeCardBalance() != null && stm.getEndBalance() != null) {
							accountDetails = customerService.getAccountDetails(profile, bankingSession, account, true);
							TVFrontendCreditAccount frontendCreditAccount = (TVFrontendCreditAccount) accountDetails;
							frontendCreditAccount.setLastChargeCardBalance(stm.getChargeCardBalance());
							frontendCreditAccount.setLastRevolvingCardBalance(
									stm.getEndBalance().subtract(frontendCreditAccount.getLastChargeCardBalance()));
							model.put("frontendCreditAccount", frontendCreditAccount);

						} else {
							logger.error("Error: Account Summary CCA -- chargecard balance and/or end balance is null");
						}
					} else if (stm == null)
						model.put("noStatement", true);
				} else {
					model.put("universalProduct", false);
				}

				// OBTAIN CURRENT TRANSACTIONS
				if (userSession.getDisplayTransactionDowntime()) {
					model.put("transactiondowntime", "true");
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[1]);
					} else {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[0]);
					}
				} else {
					TransactionRequest ccareq2 = new TransactionRequest(Utils.generateCustomerInteractionEvent(request),
							null, profile, bankingSession, firstAccount, maxTransactions, 1, false, null, null, null);
					/**
					 * MBNA-1955
					 * Method rsaAnalyze : CIBP-WEB microservice integration
					 * 
					 */
					//Microservice implementation
					//transactionResponse = CreditCardFacade.getInstance().getCreditCardCurrentTransactionsMicroservice(ccareq2);56221
					transactionResponse = customerService.getCreditCardCurrentTransactions(ccareq2);

					if (transactionResponse.getStatusResult() != ResultStatus.SUCCESS)
						return model;

					List<AbstractTransaction> transactions = setTransactionsCodeReasonCall(transactionResponse.getTransactions());
					if (transactions != null && transactions.size() > 0)
						model.put("transactions", transactions);
				}
				/***********************************************************************
				 * Here is the map for the pages available in a particular
				 * statement. *
				 ***********************************************************************/

				int totalPages = 1;
				if (transactionResponse != null)
					totalPages = transactionResponse.getPagesAvailable();

				LinkedHashMap<String, String> pagesMap = new LinkedHashMap<String, String>();
				for (int i = 1; i <= totalPages && totalPages > 1; i++) {
					pagesMap.put(String.valueOf(i), String.valueOf(i));

				}

				if (firstAccount.getCardPlasticAccountRecords() != null
						&& firstAccount.getCardPlasticAccountRecords().length > 0) {
					final CardPlasticAccountRecord[] record = firstAccount.getCardPlasticAccountRecords(); //Get card plastic account records
					final int recordLength = record.length; //Record length
					for (int i = 0; i < recordLength; i++) {
						String cardPanId = record[i].getCardPANId();

						String hiddenPanId;
						try {
							hiddenPanId = "x" + cardPanId.substring(cardPanId.length() - 4, cardPanId.length());
						} catch (Exception e) {
							logger.error("CardPANid from acctInq NOT NULL but less than 4 digits: ", e);
							hiddenPanId = "xxxx";
						}

						if (activity.equalsIgnoreCase(hiddenPanId)) {
							model.put("filterByPlastic", true);
							break;
						} else
							model.put("filterByPlastic", false);
					}
				} else
					model.put("filterByPlastic", false);

				if (transactionResponse != null) {
					if (transactionResponse.isCardPanId()) {
						model.put("showCardColumn", true);
						if (firstAccount.getCardPlasticAccountRecords() != null
								&& firstAccount.getCardPlasticAccountRecords().length > 0) {
							final CardPlasticAccountRecord[] record = firstAccount.getCardPlasticAccountRecords(); //Get card plastic account records
							final int recordLength = record.length; //Record length
							for (int i = 0; i < recordLength; i++) {
								String cardPanId = record[i].getCardPANId();

								String hiddenPanId;
								try {
									hiddenPanId = "x" + cardPanId.substring(cardPanId.length() - 4, cardPanId.length());
								} catch (Exception e) {
									logger.error("CardPANid from acctInq NOT NULL but less than 4 digits: ", e);
									hiddenPanId = "xxxx";
								}

								if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
									activityMap.put(hiddenPanId, "Tarjeta que termina en " + hiddenPanId);
								} else {
									activityMap.put(hiddenPanId, "Credit card ending in " + hiddenPanId);
								}

							}
						}
					} else
						model.put("showCardColumn", false);
					if ((unversalProducts.indexOf(firstAccount.getAccountProductId()) != -1)
							&& transactionResponse.isChargeCard()
							&& "sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						activityMap.put("C", "Transacciones a cargo");
					}
					if ((unversalProducts.indexOf(firstAccount.getAccountProductId()) != -1)
							&& transactionResponse.isRevolvingCard()
							&& "sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						activityMap.put("R", "Transacciones a cr&eacute;dito");
					}
					if ((unversalProducts.indexOf(firstAccount.getAccountProductId()) != -1)
							&& transactionResponse.isChargeCard()
							&& !"sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						activityMap.put("C", "Charge Transactions");
					}
					if ((unversalProducts.indexOf(firstAccount.getAccountProductId()) != -1)
							&& transactionResponse.isRevolvingCard()
							&& !"sp".equalsIgnoreCase(Utils.getRSALanguage(request))) {
						activityMap.put("R", "Credit Transactions");
					}
				}

				model.put("activityMap", activityMap);

				model.put("pagesMap", pagesMap);
				model.put("totalPages", totalPages);

			} // end else cca
			else if (firstAccount.getSubtype().equals("PRM")) {
				model.put("selectedAccount", "PRM");
				// PRM Transactions only are by category
				AbstractStatement stm = Utils.getStatementById("1", availableStatements);
				// Category = All Categories
				model.put("selectCategory", "ALL");
				String[] cats = { "1", "2", "3", "4", "5", "7", "8" };
				if (userSession.getDisplayTransactionDowntime()) {
					model.put("transactiondowntime", "true");
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[1]);
					} else {
						model.put("transactiondowntimemessage", userSession.getDisplayTransactionDowntimeMessages()[0]);
					}
				} else {
					TransactionRequest prmreq2 = new TransactionRequest(Utils.generateCustomerInteractionEvent(request),
							stm, profile, bankingSession, firstAccount, maxTransactions, 1, false, null, null, null);
					prmreq2.setCategories(cats);
					PremiaTransactionResponse prmResponse = customerService.getPremiaTransactionsByCategory(prmreq2);
					if (prmResponse.getStatusResult() != ResultStatus.SUCCESS)
						return model;

					model.put("transactions", setTransactionsCodeReasonCall(prmResponse.getTransactions()));
					model.put("categoryTotal", prmResponse.getCategoryTotal());
				}
			}

		} else {
			if (transactionData.getAccountInfo() != null
					&& unversalProducts.indexOf(transactionData.getAccountInfo().getAccountProductId()) != -1) {
				model.put("universalProduct", true);
				TVFrontendAccount account = transactionData.getAccountInfo();
				List<AbstractStatement> availableStatements = null;
				if (transactionData.getAccountInfo().getStatements() == null) {
					StatementRequest stmtrq = new StatementRequest(Utils.generateCustomerInteractionEvent(request),
							profile, bankingSession, account, maxStatements);
					availableStatements = customerService.getAvailableStatements(stmtrq).getStatementList();
					account.setStatements(availableStatements);
				} else
					availableStatements = account.getStatements();
				AbstractStatement stm = Utils.getStatementById("2", availableStatements);
				if (stm != null) {
					model.put("noStatement", false);
					if (stm.getChargeCardBalance() != null && stm.getEndBalance() != null) {
						AbstractAccount accountDetails = null;
						accountDetails = customerService.getAccountDetails(profile, bankingSession, account, true);
						TVFrontendCreditAccount frontendCreditAccount = (TVFrontendCreditAccount) accountDetails;
						frontendCreditAccount.setLastChargeCardBalance(stm.getChargeCardBalance());
						frontendCreditAccount.setLastRevolvingCardBalance(
								stm.getEndBalance().subtract(frontendCreditAccount.getLastChargeCardBalance()));
						model.put("frontendCreditAccount", frontendCreditAccount);

					} else {
						logger.error("Error: Account Summary CCA -- chargecard balance and/or end balance is null");
					}
				} else if (stm == null)
					model.put("noStatement", true);
			} else {
				model.put("universalProduct", false);
			}
			return model;
		}
		return model;
	}

	public int getMaxStatements() {
		return maxStatements;
	}

	public void setMaxStatements(int maxStatements) {
		this.maxStatements = maxStatements;
	}

	public int getMaxTransactions() {
		return maxTransactions;
	}

	public void setMaxTransactions(int maxTransactions) {
		this.maxTransactions = maxTransactions;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getUnversalProducts() {
		return unversalProducts;
	}

	public void setUnversalProducts(String unversalProducts) {
		this.unversalProducts = unversalProducts;
	}

	/**
	 * CIBP-1767
	 * 
	 * @return true si debe esconder el show all (transactions.showAll.hide =
	 *         ENABLED)
	 */
	private boolean getShowAllMessageSourceStatus() {
		String hideShowAll = messageSource.getMessage("transactions.showAll.hide", null, Locale.ENGLISH);
		if (hideShowAll.equalsIgnoreCase("ENABLED"))
			return true;
		else
			return false;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.BILLPAY;
	}

	public boolean isPfmAutomaticEntitlementCreation() {
		return pfmAutomaticEntitlementCreation;
	}

	public void setPfmAutomaticEntitlementCreation(boolean pfmAutomaticEntitlementCreation) {
		this.pfmAutomaticEntitlementCreation = pfmAutomaticEntitlementCreation;
	}

	private String getPinRequestStatus() {
		String pinRequestStatus = messageSource.getMessage("ccservices.pinrequest", null, Locale.ENGLISH);
		if (pinRequestStatus == null || pinRequestStatus.equals("")) {
			pinRequestStatus = "DISABLED";
		}
		return pinRequestStatus;
	}

	public String getChargesSummaryCodes() {
		return chargesSummaryCodes;
	}

	public void setChargesSummaryCodes(String chargesSummaryCodes) {
		this.chargesSummaryCodes = chargesSummaryCodes;
	}

	public List<List<AccountStatement>> totalAccountCharges(List<AbstractStatement> availableStatements) {
		List<List<AccountStatement>> totalCharges = new LinkedList<List<AccountStatement>>();
		List<AccountStatement> charges = null;
		Calendar c = Calendar.getInstance();
		int year = 0;
		for (AbstractStatement statement : availableStatements) {
			if(statement.getEndDate() != null){
				c.setTime(statement.getEndDate());
				if(c.get(Calendar.YEAR) != year){
					if(charges != null){
						totalCharges.add(charges);
					}
					year = c.get(Calendar.YEAR);
					charges = new ArrayList<AccountStatement>();
				}
				charges.add(0,new AccountStatement(c.get(Calendar.YEAR), "month."+(c.get(Calendar.MONTH) + 1), "$"+statement.getTotalChargeMonthly(), "$"+statement.getTotalChargeYearly()));
			}
		}
		if(charges != null){
			totalCharges.add(charges);
		}
		return totalCharges;
	}


	/**
	 * This method call transaction util method to get list transaction detail.
	 *
	 * @author ET58344, Evertec Inc.
	 * @since 08-27-2021
	 * @param transactions transaction list
	 * @return List<TransactionDetail> result
	 */
	private List<AbstractTransaction> setTransactionsCodeReason(List<AbstractTransaction> transactions){
		final String comaStr = ","; //coma string
		final String transactionCodes = transactionPosCodes.concat(comaStr).concat(transactionEftCodes).concat(comaStr)
				.concat(transactionWithdrawalAthCodes).concat(comaStr).concat(transactionAthMovilCodes);
		TransactionUtil.getTransactions(transactions, transactionCodes, transactionDetailTypeText);
		return transactions;
	}

	/**
	 * This method call setTransactionsCodeReason method to prevent overload of methods.
	 *
	 * @author ET58344, Evertec Inc.
	 * @since 09-07-2021
	 * @param transactions transaction list
	 * @return List<TransactionDetail> result
	 */
	private List<AbstractTransaction> setTransactionsCodeReasonCall(List<AbstractTransaction> transactions){

		return setTransactionsCodeReason(transactions);
	}
	public TransactionRequest CreateTransactionRequest(HttpServletRequest request, CustomerProfile customerProfile, BankingSession bankingSession, TVFrontendAccount account, int iteration) 
	{
		TransactionRequest transactionRequest = new TransactionRequest(	Utils.generateCustomerInteractionEvent(request), null, customerProfile,
				bankingSession, account, maxTransactions, iteration, false, null, null, null);
		
		return transactionRequest;
	}																			  
}
