package evertec.cibp.web.form;

import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.MobileDevice;

import evertec.cibp.campaign.loader.CampaignPaths;
import evertec.cibp.core.models.BinaryData.BinaryRecord;
import evertec.cibp.core.models.BinaryData.BinaryStatus;
import evertec.cibp.core.models.BinaryData.BinaryTypes;
import evertec.cibp.core.models.accountopening.EaccountMessage;
import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.sso.SSOToken;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ValidateLoginInfoRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.InternationalCharacterUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.form.bean.LoginBean;
import evertec.cibp.web.services.modules.CookiePreferenceServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.CookieUtils;
import evertec.cibp.web.utils.DateUtil;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.utils.requests.GenericRequestUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ArrayList;

public class LoginForm extends CancellableFormController implements GlobalEntitlementAwareController {
	
	/**
	 * reward Points
	 */
	private static final String REWARD_POINTS = "rewardPoints"; //reward string
	private static final String INACTIVE = "inactive"; //inactive string
	protected final Log log = LogFactory.getLog(getClass()); //logger

	private CustomerServices customerService; //customer services
	private CookiePreferenceServiceModule cookiePreferenceService; //cookie service module
	private CampaignPaths paths;// CIBP-2353
	private String updateiPads; //update ipads string
	private Integer inactiveMonthsValidation; //inactive months integer

	/**
	 * Default constructor
	 * @author Evertec Inc.
	 * @since 03-03-2021
	 */
	public LoginForm() {
		super();
		setCommandClass(LoginBean.class);
		setCommandName("login");
		setFormView("login");

		this.setCancelView(StringUtils.EMPTY);
		this.customerService = null;
		this.cookiePreferenceService = null;
		this.setPropertyEditorRegistrar(null);
		this.paths = null;
		this.updateiPads = StringUtils.EMPTY;
		this.inactiveMonthsValidation = null;
	}

	/**
	 * Injection parameter constructor
	 * @author ET58344, Evertec Inc.
	 * @since 03-02-2021
	 *
	 * @param strCancelView cancel view string
	 * @param customerService customer service
	 * @param cookiePreferenceService cookie preference service
	 * @param propertyEditorRegistrar property editor
	 * @param paths paths string
	 * @param updateiPads update ipads
	 * @param inactiveMonthsValidation inactive months integer
	 */
	public LoginForm(String strCancelView, CustomerServices customerService, CookiePreferenceServiceModule cookiePreferenceService,
			PropertyEditorRegistrar propertyEditorRegistrar, CampaignPaths paths, String updateiPads, Integer inactiveMonthsValidation) {
		this();
		this.setCancelView(strCancelView);
		this.customerService = customerService;
		this.cookiePreferenceService = cookiePreferenceService;
		this.setPropertyEditorRegistrar(propertyEditorRegistrar);
		this.paths = paths;
		this.updateiPads = updateiPads;
		this.inactiveMonthsValidation = inactiveMonthsValidation;
	}

	@Override
	protected boolean isFormSubmission(HttpServletRequest request) {

		boolean issubmit = (request.getParameter("rtoken") != null || request.getParameter("username") != null);
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1668)) {
			if(request.getParameter(Utils.DEEP_LINK) != null) {
				WebUtils.setSessionAttribute(request, Utils.DEEP_LINK, request.getParameter(Utils.DEEP_LINK));
			}
		}
		else {
			WebUtils.setSessionAttribute(request, Utils.DEEP_LINK, null);
		}
		return issubmit;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		WebUtils.setSessionAttribute(request, "userSession", null);

		LoginBean login = (LoginBean) command;
		String username = "";

		String rtoken = request.getParameter("rtoken");
		rtoken = InternationalCharacterUtils.sanatizeInputs(rtoken, false);
		if (rtoken != null && !rtoken.equals("")) {
			SSOToken token = customerService.tokenGetInformation(rtoken);
			if (token.getReferenceData() != null && token.getReferenceData().indexOf("username") != -1) {
				String referenceData[] = token.getReferenceData().split("&");
				for (int i = 0; i < referenceData.length; i++) {
					String ref = referenceData[i];
					if (ref.indexOf("username=") != -1)
						username = ref.substring(9, ref.length()).toLowerCase();
				}

				if (GenericValidator.isBlankOrNull(username)) {
					errors.rejectValue("username", "login.username.required", "Required");
					return showForm(request, response, errors);
				}
			}
		} else
			username = login.getUsername();

		if (GenericValidator.isBlankOrNull(username)) {
			errors.rejectValue("username", "login.username.required", "Required");
			return showForm(request, response, errors);
		} else if (!GenericValidator.maxLength(username, 80)) {
			errors.rejectValue("username", "login.username.invalid", "Invalid");
			return showForm(request, response, errors);
		}

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");
		cookie.setMaxAge(31536000);
		
		final boolean isSDKMobileRequest = !GenericValidator.isBlankOrNull(login.getDevice_info_rsa());
		DeviceRequest deviceRequest = generateDeviceRequest(request, isSDKMobileRequest, login, cookie, username);

		ValidateLoginInfoRequest validateLoginInfoRequest = new ValidateLoginInfoRequest(Utils.generateCustomerInteractionEvent(request, username), username, "", deviceRequest);
		if (isSDKMobileRequest) {
			validateLoginInfoRequest.setCustomerDevices(DeviceUtils.getCustomerDevice(request.getHeader("User-Agent")).toString());
		} else {
			validateLoginInfoRequest.setCustomerDevices(DeviceUtils.getCustomerDevice(deviceRequest.getUserAgent()).toString());
		}

		validateLoginInfoRequest.setRsaLang("sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "ES" : "EN");
		UsernameValidationResponse validateUsernameResponse = customerService.validateUsername(validateLoginInfoRequest);
		
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBDP3535) 
				&& validateUsernameResponse.getCustomerProfile() != null
				&& validateUsernameResponse.getCustomerProfile().hasFlag(CustomerFlagType.PENDING_ENROLL)) {
			Map<String, Object> model = new HashMap<>(); 
			model.put("pendingEnroll", true);
			model.put("username", username);
			return showForm(request, response, errors, model); 
		}
		
		// MBFIS - FIXES FLAG
		if (!StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP)
				&& validateUsernameResponse.getCustomerProfile() != null
				&& ResultStatus.SUCCESS.equals(validateUsernameResponse.getStatusResult())
				&& CustomerUtils.isSecurities(validateUsernameResponse.getCustomerProfile())
				&& validateUsernameResponse.getCustomerProfile().getStatus().equals(CustomerStatus.ENROLLED)) {
			return new ModelAndView("redirect:forgotpasswordsec");
		}
		
		// MBFIS-517 & 518 & 519
		if ( validateUsernameResponse.getCustomerProfile() != null && CustomerUtils.isSecurities(validateUsernameResponse.getCustomerProfile())
				&& validateUsernameResponse.getCustomerProfile().hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)) {
			WebUtils.setSessionAttribute(request, "mustChangeUsername", true);
			response.sendRedirect("forgotpasswordsec");
			return showForm(request, response, errors);
		}
		// END MBFIS-517 & 518 & 519
		
		//OAO CODES VALIDATION
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE179)){
			if (rtoken != null && !rtoken.equals("")) {
				try{
					if(validateUsernameResponse.getCustomerProfile().getUserInterface().equals(UserInterface.COMMERCIAL)){
						Map<String, Object> model = new HashMap<>();
						model.put("eaccountMessage", EaccountMessage.MESSAGE_2);
						model.put("noOpenOAOCode", "true");
						model.put("loginLoop", "T");
						return showForm(request, response, errors, model);
					}
					else{
						EaccountMessage message = this.customerService.getEaccountOpeningMessage(validateUsernameResponse.getCustomerProfile().getTaxId(), validateUsernameResponse.getCustomerProfile().getPermId());
						if(!message.equals(EaccountMessage.NO_MESSAGE)){
							Map<String, Object> model = new HashMap<>(); 
							model.put("eaccountMessage", message);
							model.put("noOpenOAOCode", "true");
							model.put("loginLoop", "T");
							return showForm(request, response, errors, model);
						}
					}
				}
				catch(Exception e){
					Map<String, Object> model = new HashMap<>(); 
					model.put("eaccountMessage", EaccountMessage.ERROR_MESSAGE);
					model.put("noOpenOAOCode", "true");
					model.put("loginLoop", "T");
					return showForm(request, response, errors, model);
				}
			}
		}

		if (deviceRequest != null && cookie != null) {
			cookie.setValue(deviceRequest.getDeviceTokenCookie());
			cookie.setPath("/");
			ESAPI.httpUtilities().addCookie(response, cookie);
		}
		
		//if mbop info is created on a login attempt 
		//analytics value is based on this attribute
		if(validateUsernameResponse.getMbopId()){
			WebUtils.setSessionAttribute(request, "newMbopId", true);
		}
		else{
			WebUtils.setSessionAttribute(request, "newMbopId", false);
		}

		String view = "error";
		String recoverycode = request.getParameter("recoverycode"); 
		boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);
		
		if (ResultStatus.SUCCESS.equals(validateUsernameResponse.getStatusResult()) || (oobFlag && recoverycode != null)) {
			
			if (!validateUsernameResponse.getCustomerProfile().hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)) {
				
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBM2159) &&
						validateUsernameResponse.getCustomerProfile().hasFlag(CustomerFlagType.MUST_CHANGE_PASSWORD)) {
					return this.getLoginSsdsForced(request, validateUsernameResponse.getCustomerProfile());
					
				} else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1374)
						&& validateUsernameResponse.getCustomerProfile().getSignonStats() != null
						&& validateUsernameResponse.getCustomerProfile().getSignonStats().getLastSignOnSuccess() != null) {
					Date lastSignOnSuccess = validateUsernameResponse.getCustomerProfile().getSignonStats().getLastSignOnSuccess();
					Date lastPassChange = validateUsernameResponse.getCustomerProfile().getSignonStats().getLastRegainAccessPassChange();
					Calendar calendar = Calendar.getInstance();
					calendar.add(Calendar.MONTH, -(this.inactiveMonthsValidation));
					int compareSignOn = DateUtil.compareOnlyDate(lastSignOnSuccess, calendar.getTime());
					int comparePassReset;
					if(lastPassChange == null){
						comparePassReset = 0;
					}
					else{
						comparePassReset = DateUtil.compareOnlyDate(lastPassChange, calendar.getTime());
					}
					if((compareSignOn == 0 || compareSignOn == -1) && (comparePassReset == 0 || comparePassReset == -1)){
						return this.getLoginSsdsForced(request, validateUsernameResponse.getCustomerProfile());
					}
				}
			}
			
			UserSession userSession = null;
			if(oobFlag){
				// Manually add tasks if recovery code use is requested 
				if (recoverycode != null) {
					List<ASK_FOR> tasks = new LinkedList<ASK_FOR>();
					tasks.add(ASK_FOR.RECOVERY_CODE);
					tasks.add(ASK_FOR.VALIDATE_PASSWORD);
					tasks.add(ASK_FOR.SESSION_INIT);
					validateUsernameResponse.setTasksList(tasks);
				}
			
				// Add TaskslistOOB to userSession
				userSession = new UserSession(customerService, validateUsernameResponse.getCustomerProfile(),
						validateUsernameResponse.getTasksList(), validateUsernameResponse.getTasksListOOB());
			}else{
				userSession = new UserSession(customerService, validateUsernameResponse.getCustomerProfile(),
						validateUsernameResponse.getTasksList());
			}
			userSession.setRewardPoints(login.getRewardPoints() != null && login.getRewardPoints().equals("true"));
			userSession.setPm_fp(login.getPm_fp());
			// FFEIC
			if (userSession.getCustomerProfile() != null && userSession.getCustomerProfile().getSignonStats() != null) {
				if (userSession.getCustomerProfile().getSignonStats().getLastSignOnSuccess() != null) {
					SimpleDateFormat df = null;
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						df = new SimpleDateFormat("MM/dd/yyyy  hh:mm a");
					} else {
						df = new SimpleDateFormat("dd/MM/yyyy  hh:mm a");

					}
					userSession.setLastLoginDate(df.format(userSession.getCustomerProfile().getSignonStats()
							.getLastSignOnSuccess()));
				}
				if (userSession.getCustomerProfile().getSignonStats().getLastSignOnSuccessIp() != null) {
					userSession.setLastLoginIp(userSession.getCustomerProfile().getSignonStats()
							.getLastSignOnSuccessIp());
				}
			}
			// FIN FFEIC

			if (login.isFromvi()) {
				BinaryRecord record = customerService.getImagebyTypeandStatus(BinaryTypes.LOGIN_IMAGE_VI,
						BinaryStatus.CURRENT);
				userSession.setImageLogin(record);
			} else {
				BinaryRecord record = customerService.getImagebyTypeandStatus(BinaryTypes.LOGIN_IMAGE_PR,
						BinaryStatus.CURRENT);
				userSession.setImageLogin(record);
			}

			deviceRequest = validateUsernameResponse.getDeviceRequest();
			userSession.setRsaSessionId(validateUsernameResponse.getRsaSessionId());
			userSession.setRsaTransactionId(validateUsernameResponse.getRsaTransactionId());
			userSession.setCustomerProfile(validateUsernameResponse.getCustomerProfile());
			if (validateUsernameResponse.getCustomerProfile() == null)
				userSession.setPasswordSalt(validateUsernameResponse.getPasswordSalt());

			// if(!validateUsernameResponse.getTasksList().contains(ASK_FOR.DUMMY_QUESTIONS))
			// response.addCookie(rsaId.getCookie());
			userSession.setRemoteToken(rtoken);
			userSession.setDeviceRequest(deviceRequest);
			userSession.setUsername(username);
			userSession.setTasksList(validateUsernameResponse.getTasksList());

			// FFEIC 2 DEFECT 519
			if (userSession.getTasksList() != null && !userSession.getTasksList().contains(ASK_FOR.QUESTION_CHALLENGE)) {
				userSession.setRemember(true);
				userSession.setInformationChallenge(true);
			}
			// END FFEIC 2

			userSession.setEnrollableAccounts(validateUsernameResponse.getAccountsToValidate());
			userSession.setLanguage(Utils.getRSALanguage(request));

			//MBFM-99 - MBFM-166
			List<CookiePreferenceStatus> cookiePreferences = customerService.getCookiePreferenceStatusService()
					.getCookiePreferenceStatusByProfileId(userSession.getCustomerProfile().getProfileId()); //profile preferences
			if(cookiePreferences != null && !cookiePreferences.isEmpty()) {
				userSession.setCookiePreferenceStatusList(cookiePreferences);
			} else {
				userSession.setCookiePreferenceStatusList(cookiePreferenceService.createProfileCookiePreference(
						userSession.getCustomerProfile().getProfileId()));
			}
			cookiePreferenceService.deleteAllCookiePreference(userSession.getCookiePreferenceStatusList(), response);
			//END MBFM-99 - MBFM-166

			WebUtils.setSessionAttribute(request, "userSession", userSession);
			userSession.setOobLogin(true);
			view = SignonSequenceUtils.getNextView(userSession);
			
			// mpicado, cibp-1867
			userSession.setPromotion(login.getPromo());

			if (deviceRequest != null && cookie != null) {
				cookie.setValue(deviceRequest.getDeviceTokenCookie());
				cookie.setPath("/");
				ESAPI.httpUtilities().addCookie(response, cookie);
			}

		} else if (validateUsernameResponse.getStatusResult().equals(ResultStatus.FRAUD_BLOCKED)) {// CIBP-2694
			log.warn("Returned BLOCKED (" + validateUsernameResponse.getStatusResult() + ")for Username: "
					+ username);
			errors.rejectValue("username", "login.username.fraud.block", "Access Blocked");
			Map<String, Object> model = new HashMap<>();
			if(!GenericValidator.isBlankOrNull(updateiPads) && updateiPads.equalsIgnoreCase("Y"))
				model.put("iPadUpdate", updateiPads);
			model.put("fraudblocked", "true");
			model.put("username", username);
			model.put("rsa_cookie", cookie.getValue());
			return showForm(request, response, errors, model);
		} else if (ResultStatus.PASS_RESET_BLOCKED.equals(validateUsernameResponse.getStatusResult())) {
			// Esto es un mensaje de blocked en vez de llevar a otro view
			log.warn("Returned BLOCKED (" + validateUsernameResponse.getStatusResult() + ")for Username: "
					+ username);
			errors.rejectValue("username", "login.username.blocked", "Access Blocked");
			Map<String, Object> model = new HashMap<>();
			if(!GenericValidator.isBlankOrNull(updateiPads) && updateiPads.equalsIgnoreCase("Y"))
				model.put("iPadUpdate", updateiPads);
			model.put("rsaresetblocked", "true");
			model.put("username", username);
			WebUtils.setSessionAttribute(request, "passblocked", "true");
			model.put("MBSFE291", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE291));
			model.put("rsa_cookie", cookie.getValue());
			return showForm(request, response, errors, model);
			
		} else if (ResultStatus.PASS_BLOCKED.equals(validateUsernameResponse.getStatusResult())
				|| ResultStatus.RSA_BLOCKED.equals(validateUsernameResponse.getStatusResult())
				|| ResultStatus.BLOCKED.equals(validateUsernameResponse.getStatusResult())) {
			//MBFIS-141 ET57991
			if (CustomerUtils.isSecurities(validateUsernameResponse.getCustomerProfile()) && AccountUtils.getIsTransactional(validateUsernameResponse.getCustomerProfile()).equalsIgnoreCase("N")) { 
				return new ModelAndView("redirect:forgotpasswordsec");
			}
			//END-141
			// Esto es un mensaje de blocked en vez de llevar a otro view
			log.warn("Returned BLOCKED  (" + validateUsernameResponse.getStatusResult() + ") for Username: "
					+ username);
			errors.rejectValue("username", "login.username.blocked", "Access Blocked");
			Map<String, Object> model = new HashMap<>();
			if(!GenericValidator.isBlankOrNull(updateiPads) && updateiPads.equalsIgnoreCase("Y"))
				model.put("iPadUpdate", updateiPads);
			model.put("username", username);
			WebUtils.setSessionAttribute(request, "passblocked", "true");
			
			// Display recovery code message if enrolled in OOB
			if (oobFlag && ResultStatus.RSA_BLOCKED.equals(validateUsernameResponse.getStatusResult())) {
				CredentialType challengeType = validateUsernameResponse.getChallengeType();
				if (challengeType != null && 
						(challengeType.equals(CredentialType.OOBPHONE) || challengeType.equals(CredentialType.USER_DEFINED))) {
					model.put("oobblocked", "true");
					WebUtils.setSessionAttribute(request, "oobblocked", true);
				}
			}
			model.put("rsa_cookie", cookie.getValue());
				model.put("MBSFE291", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE291));
				return showForm(request, response, errors, model);
		}

		else if (ResultStatus.DENY.equals(validateUsernameResponse.getStatusResult())) {
			// Rsa deny
			log.warn("RSA Returned DENY for Username: " + username);
			errors.rejectValue("username", "login.username.rsa.blocked", "Access Blocked");
			Map<String, Object> model = new HashMap<>();
			model.put("rsa_cookie", cookie.getValue());
			if(!GenericValidator.isBlankOrNull(updateiPads) && updateiPads.equalsIgnoreCase("Y"))
				model.put("iPadUpdate", updateiPads);
			return showForm(request, response, errors, model);
		} else if (ResultStatus.ERROR.equals(validateUsernameResponse.getStatusResult())
				|| ResultStatus.DISABLED.equals(validateUsernameResponse.getStatusResult())) {
			log.warn("Returned ERROR for Username: " + username);
			errors.rejectValue("username", "login.username.invalid", "Invalid username");
			Map<String, Object> model = new HashMap<>();
			model.put("rsa_cookie", cookie.getValue());
			if(!GenericValidator.isBlankOrNull(updateiPads) && updateiPads.equalsIgnoreCase("Y"))
				model.put("iPadUpdate", updateiPads);
			return showForm(request, response, errors, model);
		}

		return new ModelAndView(view);
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {

		LoginBean bean = new LoginBean();
		if (request.getParameter("vi") != null)
			bean.setFromvi(true);
		// if(!GenericValidator.isBlankOrNull(request.getParameter("promo")))
		// bean.setPromo(ServletRequestUtils.getStringParameter(request,
		// "promo"));

		return bean;
	}
	
	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> m = new HashMap<String, Object>();
		
		WebUtils.setSessionAttribute(request, INACTIVE, null);
		WebUtils.setSessionAttribute(request, Utils.DEEP_LINK, null);
		
		WebUtils.setSessionAttribute(request, "language", Utils.getRSALanguage(request));

		String username = (String) WebUtils.getSessionAttribute(request, "username");
		if (username != null && request.getParameter("LoginErrorResponse") == null) {
			m.put("username", username);
			WebUtils.setSessionAttribute(request, "username", null);
			WebUtils.setSessionAttribute(request, "passblocked", "true");
		}

		else {
			LoginBean bean = (LoginBean) command;
			
			GenericRequestUtil.putAsStringBooleanParameter(request, m, REWARD_POINTS);
			
			if (request.getParameter("expired") != null)
				m.put("expiredSession", true);
			
			/*Start Incident 2712087 Login Loop*/
			String user=request.getParameter("user");
			String loginLoop = request.getParameter("LoginErrorResponse");
			String comercialUser = request.getParameter("comUser");
			
			if (loginLoop != null && user != null) {
				m.put("loginLoop", loginLoop);
				m.put("username", user);
				WebUtils.setSessionAttribute(request, "passblocked", "false");
				m.put("comercialUser", comercialUser);
				request.setAttribute("LoginErrorResponse", "");
			}else{
				m.put("loginLoop", "F");	
			}
			/*END Incident 2712087 Login Loop*/
			
			if (request.getParameter("noSessionError") != null) {
				String noSessionError = "";
				if (CookieUtils.isValidCookie(request, "sessionError")) {
					noSessionError = request.getParameter("noSessionError");
					m.put("noSessionError", noSessionError);
				}
			}

			// Is the login from VI?
			if (bean.isFromvi() || WebUtils.getCookie(request, "vi") != null && request.getParameter("pr") == null) {
				m.put("vi", true);
			}

			// cibp-1867
			if (!GenericValidator.isBlankOrNull(ServletRequestUtils.getStringParameter(request, "promo"))) {
				m.put("promo", true);
				bean.setPromo(ServletRequestUtils.getStringParameter(request, "promo"));
			}

			String serverHostname = Utils.getServerHostname();
			m.put("serverName", serverHostname);
			// m.put("clientIPCode",Utils.getHiddenToken(request.getRemoteAddr()));
			WebUtils.setSessionAttribute(request, "skipwba", null);

			/* CIBP-2353 */
			File file = new File(this.paths.getFilePath() + "fixedEnCampaign.vm");
			if (file != null && file.exists()) {
				m.put("fixedCampaign", "true");
			}
			file = new File(this.paths.getFilePath() + "divEnWantToDo.vm");
			if (file != null && file.exists()) {
				m.put("WantToDo", "true");
			}
			/* FIN CIBP-2353 */

			// iphone new css
			String userAgent = request.getHeader("user-agent");

			if (userAgent != null) {

				if (userAgent.toLowerCase().indexOf("iphone") > 0) {
					request.getSession().setAttribute("mobile", "iphone");
				} else if (userAgent.toLowerCase().indexOf("android") > 0) {
					request.getSession().setAttribute("mobile", "android");
				}
			}
		}
		final String popupCookieFlag="MBSD3806"; //ENHA.20191018.popup.cookie.track.message

		m.put(popupCookieFlag, StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3806).toString());
		return m;
	}

	protected ModelAndView showForm(HttpServletRequest request, HttpServletResponse response, BindException errors)
			throws Exception {
		Cookie userCookie = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equalsIgnoreCase("username")) {
					userCookie = cookies[i];
					if (userCookie.getValue() != null) {
						WebUtils.setSessionAttribute(request, "username", userCookie.getValue());
					}
					CookieUtils.addCookie(response, 0, "username", null);
					break;
				}
			}
		}

		return showForm(request, errors, getFormView());
	}
	
	private ModelAndView getLoginSsdsForced(HttpServletRequest request, CustomerProfile profile) {
		List<ASK_FOR> pending = new ArrayList<ASK_FOR>();
		pending.add(ASK_FOR.RSA_ENROLL_USERNAME);
		WebUtils.setSessionAttribute(request, INACTIVE, Boolean.TRUE);
		WebUtils.setSessionAttribute(request, "userSession", 
											new UserSession(
													this.customerService, 
													profile, 
													pending));
		return new ModelAndView(new RedirectView("loginSsdsForced"));
	}

	/**
	 * generateDeviceRequest
	 * @param request
	 * @param login
	 * @param cookie
	 * @param username
	 * @return DeviceRequest model
	 */
	private DeviceRequest generateDeviceRequest (HttpServletRequest request, boolean isSDKMobileRequest,
			LoginBean login, Cookie cookie, String username) {
		DeviceRequest deviceRequest = new DeviceRequest ();
		String cookieValue = !GenericValidator.isBlankOrNull(login.getRsa_cookie()) ? login.getRsa_cookie() : cookie.getValue();

		deviceRequest.setIpAddress(request.getRemoteAddr());
		deviceRequest.setDeviceTokenCookie(cookieValue);
		deviceRequest.setUserAgent(request.getHeader("User-Agent"));
		Boolean isEnabledSDK = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC615_MOBILE_SDK_DATA);

		if (isEnabledSDK && isSDKMobileRequest) {
			final MobileDevice  md = new MobileDevice();

	        md.setMobileSdkData(login.getDevice_info_rsa());
	        deviceRequest.setDeviceIdentifier(new com.rsa.csd.ws.DeviceIdentifier[]{md});
		} else {
			deviceRequest.setHttpAccept(request.getHeader("Accept"));
			deviceRequest.setHttpAcceptLanguage(request.getHeader("Accept-Language"));
			deviceRequest.setHttpAcceptChars(request.getHeader("Accept-Charset"));
			deviceRequest.setHttpAcceptEncoding(request.getHeader("Accept-Encoding"));
			deviceRequest.setHttpReferrer(request.getHeader("Referer"));
			//deviceRequest.setDeviceTokenFSO(cookieValue);

			String userAgent = request.getHeader("user-agent");

			if (userAgent != null) {
				deviceRequest.setDevicePrint(login.getPm_fp());
			} else {
				deviceRequest.setDevicePrint("");
				login.setPm_fp("");
				log.error("USER AGENT NULL for user:" + username);
			}
		}

		return deviceRequest;
	}


	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.SIGNON;
	}

	public CampaignPaths getPaths() {
		return paths;
	}

	public void setPaths(CampaignPaths paths) {
		this.paths = paths;
	}

	public String getUpdateiPads() {
		return updateiPads;
	}

	public void setUpdateiPads(String updateiPads) {
		this.updateiPads = updateiPads;
	}

	public void setInactiveMonthsValidation(Integer inactiveMonthsValidation) {
		this.inactiveMonthsValidation = inactiveMonthsValidation;
	}
	
	/**
	 * Set cookie preference service module
	 *
	 * @author ET58344, Evertec Inc.
	 * @date 02-22-2021
	 * @return CookiePreferenceServiceModule the cookiePreferenceService
	 */
	public CookiePreferenceServiceModule getCookiePreferenceService() {
		return cookiePreferenceService;
	}

	/**
	 * Set cookie preference service module
	 *
	 * @author ET58344, Evertec Inc.
	 * @date 02-22-2021
	 * @param cookiePreferenceService the cookiePreferenceService to set
	 */
	public void setCookiePreferenceService(CookiePreferenceServiceModule cookiePreferenceService) {
		this.cookiePreferenceService = cookiePreferenceService;
	}
}
