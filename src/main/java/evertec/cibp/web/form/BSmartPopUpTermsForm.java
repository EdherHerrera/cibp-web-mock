package evertec.cibp.web.form;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import edu.emory.mathcs.backport.java.util.Collections;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.PayrollEntitlement;
import evertec.cibp.core.models.epayroll.PayrollStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.session.UserSession;

public class BSmartPopUpTermsForm extends MultiActionController {

	public ModelAndView bsmartPopUpTerms(HttpServletRequest request, HttpServletResponse response) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();
		String opt = request.getParameter("opc");
		int opcion = 0;
		String urlEnd="portal";
		if (opt != null)
			opcion = new Integer(opt);

		CustomerEntitlement ent = profile.getEntitlementByType(EntitlementType.PAYROLL);
		PayrollEntitlement entt = null;
		if (ent != null) {
			entt = new PayrollEntitlement(ent);
		}

		switch (opcion) {
		case 1: {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date); // Configuramos la fecha que se recibe
			calendar.add(Calendar.DAY_OF_YEAR, 90); // numer
			date = calendar.getTime();
			entt.setAcceptTerms(PayrollStatus.ACEPT.toString());
			entt.setLimitedDate(format.format(date));
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR)){
				urlEnd = "makeEPayroll";
			}
			else{
				urlEnd = "makePayment?tabNum=1";
			}
			try {
				customerService.aceptTermsBsmartAccount(profile, entt.getCustomerEntitlement());
			} catch (IllegalArgumentException e) {
				System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
			} catch (SQLException e) {
				System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
			}
			break;
		}

		case 2: {
			entt.setAcceptTerms(PayrollStatus.REMINDME.toString());
			CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(entt.getCustomerEntitlement()));
			break;
		}

		case 3: {
			entt.setAcceptTerms(PayrollStatus.REJECT.toString());
			CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(entt.getCustomerEntitlement()));
			break;
		}
		default: {
			break;
		}
		}
		userSession.setEpayrollBulletNull();
		userSession.setCustomerProfile(profile);
		WebUtils.setSessionAttribute(request, "userSession", userSession);
		//return new ModelAndView("redirect:makePayment");
		return new ModelAndView(new RedirectView(urlEnd, true));
	}
}
