/**
 * 
 */
package evertec.cibp.web.form;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.models.campaign.ActiveCampaignView;
import evertec.cibp.core.models.campaign.CampaignGuidedSetupType;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.campaign.GuidedSetupInfo;
import evertec.cibp.core.models.campaign.ViewCampaign;
import evertec.cibp.core.models.campaign.ViewCampaignId;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * Clase evertec.cibp.web.form.GuidedSetupCampaignForm Sistema: Mi Banco
 * Descripcion: Controlador de las acciones de la capa de presentacion
 * relacionadas al Guided Setup Campaign
 * 
 * @author Ing. Michael Picado Fuentes Creada el Mar 1, 2011 8:51:28 AM
 */
public class GuidedSetupCampaignForm extends MultiActionController {

	/**
	 * atributo
	 * <code>maxIntentos del tipo Integer, numero de intentos maximo para omitir la campana</code>
	 */
	private Integer maxIntentos;

	/* Constructor de la clase GuidedSetupCampaignForm */
	public GuidedSetupCampaignForm() {
		// TODO Auto-generated constructor stub
	}

	public ModelAndView gsEbills(HttpServletRequest request, HttpServletResponse response) {

		UserSession userSession = new UserSession();//user session
		final String strUserSession = "userSession";//user session string

		if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
			userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
		}
		Map<String, Object> model = new HashMap<>(); //model result

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return new ModelAndView("gsEbills", model);
	}

	public ModelAndView gsSMS(HttpServletRequest request, HttpServletResponse response) {

		UserSession userSession = new UserSession();//user session
		final String strUserSession = "userSession";//user session string

		if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
			userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
		}
		Map<String, Object> model = new HashMap<>(); //model result

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return new ModelAndView("gsSMS", model);
	}

	public ModelAndView gsPIF(HttpServletRequest request, HttpServletResponse response) {

		UserSession userSession = new UserSession();//user session
		final String strUserSession = "userSession";//user session string

		if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
			userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
		}
		Map<String, Object> model = new HashMap<>(); //model result

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return new ModelAndView("gsPIF", model);
	}

	public ModelAndView gsPayments(HttpServletRequest request, HttpServletResponse response) {

		UserSession userSession = new UserSession();//user session
		final String strUserSession = "userSession";//user session string

		if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
			userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
		}
		Map<String, Object> model = new HashMap<>(); //model result

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return new ModelAndView("gsPayments", model);
	}

	// mpicado, cip-2218
	public ModelAndView guidedSetupCampaign(HttpServletRequest request, HttpServletResponse response) {

		UserSession userSession = new UserSession();//user session
		final String strUserSession = "userSession";//user session string

		if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
			userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
		}
		Map<String, Object> model = new HashMap<>(); //model result

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		/*
		 * Opciones 0 = Presiono "Continuar" 1 = Presiono "Ver luego" 2 =
		 * Presiono "No, gracias" 3 = Presiono la "X" }
		 */

		String opc = request.getParameter("opc");

		if (opc != null && opc.equals("0")) {
			String gsPage = this.nextView("", userSession.getGuidedSetupSession().isHasPIFguidedSetup(), userSession
					.getGuidedSetupSession().isHasEBPPGuidedSetup(), userSession.getGuidedSetupSession()
					.isHasPaymentsGuidedSetup());
			model.put("gsHasPIF", userSession.getGuidedSetupSession().isHasPIFguidedSetup());
			model.put("gsHasEBPP", userSession.getGuidedSetupSession().isHasEBPPGuidedSetup());
			model.put("gsHasPayments", userSession.getGuidedSetupSession().isHasPaymentsGuidedSetup());
			model.put("gsPage", gsPage);
			/* Guardar la estadistica de vista */
			String campaignId = userSession.getGuidedSetupSession().getIndividualguidedSetupCampaignId();
			userSession.getCustomerService().campaignStatistics(campaignId, "VIEW",
					CampaignType.GUIDED_SETUP.toString());
			return new ModelAndView(gsPage, model);
		}

		if (opc != null && opc.equals("1"))
			this.saveCampaignStatus(userSession, ViewCampaign.VER_LUEGO, userSession.getGuidedSetupSession()
					.getIndividualguidedSetupCampaignId());
		if (opc != null && opc.equals("2")) {
			String salir = request.getParameter("salir");
			if (salir != null) {

				this.saveCampaignStatus(userSession, ViewCampaign.NO_GRACIAS, userSession.getGuidedSetupSession()
						.getIndividualguidedSetupCampaignId());
				String salirGuided = request.getParameter("salirGuided");
				if (salirGuided == null) {
					request.getSession().invalidate();
					return new ModelAndView("redirect:login");
				} else {
					this.resetAll(userSession);
					return new ModelAndView("forward:portal");
				}
			}
			this.saveCampaignStatus(userSession, ViewCampaign.NO_GRACIAS, userSession.getGuidedSetupSession()
					.getIndividualguidedSetupCampaignId());
			if (userSession.getGuidedSetupSession().getIndividualguidedSetupCampaignId()
					.equals(GuidedSetupInfo.ID_GUIDED_SETUP))
				this.marcarCampIndVista(userSession, ViewCampaign.NO_GRACIAS);
		}
		if (opc != null && opc.equals("3"))
			this.saveCampaignStatus(userSession, ViewCampaign.OMITIDA, userSession.getGuidedSetupSession()
					.getIndividualguidedSetupCampaignId());

		this.resetAll(userSession);
		return new ModelAndView("redirect:portal");// Fix guided setup issue
	}

	public ModelAndView nextStep(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		model.put("gsHasPIF", userSession.getGuidedSetupSession().isHasPIFguidedSetup());
		model.put("gsHasEBPP", userSession.getGuidedSetupSession().isHasEBPPGuidedSetup());
		model.put("gsHasPayments", userSession.getGuidedSetupSession().isHasPaymentsGuidedSetup());

		String pageRequest = request.getParameter("page");
		String gsPage = this.nextView(pageRequest, userSession.getGuidedSetupSession().isHasPIFguidedSetup(),
				userSession.getGuidedSetupSession().isHasEBPPGuidedSetup(), userSession.getGuidedSetupSession()
						.isHasPaymentsGuidedSetup());
		model.put("gsPage", gsPage);
		// bgarcia, CIBP-2218
		if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			model.put("isGuidedSetup", userSession.getGuidedSetupSession().isGuidedSetupMode());
			model.put("individualguidedSetup", userSession.getGuidedSetupSession().isIndividualguidedSetup());
		}
		if (gsPage.equals("gsSMS")) {
			if (userSession.getCustomerProfile() != null) {
				if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
					SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
					if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
							&& smsprofile.getCtns().size() > 0)
						model.put("smsPhone", ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn());
				}
			}
		}

		if (gsPage.equals("portal")) {
			this.saveCampaignStatus(userSession, ViewCampaign.VISTA, userSession.getGuidedSetupSession()
					.getIndividualguidedSetupCampaignId());
			if (userSession.getGuidedSetupSession().getIndividualguidedSetupCampaignId()
					.equals(GuidedSetupInfo.ID_GUIDED_SETUP))
				this.marcarCampIndVista(userSession, ViewCampaign.VISTA);
			this.resetAll(userSession);
			return new ModelAndView("forward:portal");
		}
		// CIBP-2353 Statistics
		model.put("guidedSetupId", userSession.getGuidedSetupSession().getIndividualguidedSetupCampaignId());
		return new ModelAndView(gsPage, model);
	}

	public ModelAndView gsVistazoFrame(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> model = new HashMap<>();
		return new ModelAndView("gsVistazoFrame", model);
	}

	/**
	 * Metodo saveCampaignStatus Metodo que guarda o actualiza el status de una
	 * campana para saber si ya fue vista o omitida y entre otros.
	 * 
	 * @param userSession
	 *            session del usuario
	 * @param statusCampaign
	 *            status en que se guardara la campana, por ejemplo VISTA,
	 *            OMITIDA, etc.. @see
	 *            evertec.cibp.core.models.campaign.ViewCampaign
	 * @param campaignId
	 *            id de la campana a guardar
	 * @author mpicado
	 */
	private void saveCampaignStatus(UserSession userSession, Integer statusCampaign, String campaignId) {
		boolean maxOmitidas = false;
		CustomerServices customerService = userSession.getCustomerService();
		Calendar instance = Calendar.getInstance();
		ViewCampaign vc = new ViewCampaign();
		ViewCampaignId id = new ViewCampaignId();
		vc.setCount(1);
		id.setIdCampaign(campaignId);

		if (userSession.getCustomerProfile().getProfileId() == null)
			id.setProfileId(userSession.getCustomerProfile().getUsername());
		else
			id.setProfileId(userSession.getCustomerProfile().getProfileId());

		vc.setId(id);
		vc.setStatus(statusCampaign);
		vc.setType(CampaignType.GUIDED_SETUP);
		vc.setViewDate(instance.getTime());
		try {
			ViewCampaign vcExist = customerService.getViewCampaign(userSession.getCustomerProfile(), vc.getId()
					.getIdCampaign());
			if (vcExist == null) {
				if (statusCampaign.equals(ViewCampaign.VER_LUEGO))
					vc.setCount(0);
				customerService.saveViewCampaign(vc);
			} else {
				if (statusCampaign.equals(ViewCampaign.OMITIDA)) {
					vc.setCount(vcExist.getCount() + 1);
					// si ya llego al num maximo de intentos de OMITIDA la marco
					// como NO GRACIAS para que ya no la vuelva a ver
					if (vc.getCount() >= this.maxIntentos) {
						maxOmitidas = true;
						vc.setStatus(ViewCampaign.NO_GRACIAS);
					}
				}
				if (!statusCampaign.equals(ViewCampaign.VER_LUEGO)) {
					if (!statusCampaign.equals(ViewCampaign.OMITIDA))
						vc.setCount(vcExist.getCount());
					customerService.mergeViewCampaign(vc);
				}

				/*
				 * mpicado, esto es para que marque las camp�nas individuales
				 * como vistas una vez que ya se llego al max de intentos de
				 * omitir el guided setup
				 */
				if (maxOmitidas)
					this.marcarCampIndVista(userSession, ViewCampaign.NO_GRACIAS);
			}
		} catch (Exception e) {
			System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
		}
	}

	/**
	 * Metodo resetAll Metodo que hace un reset de todas las variables
	 * utilizadas para el guided setup
	 * 
	 * @param userSession
	 * @author mpicado
	 */
	private void resetAll(UserSession userSession) {
		userSession.getGuidedSetupSession().setIndividualguidedSetupCampaignId("");
		userSession.getGuidedSetupSession().setIndividualguidedSetup(false);
		userSession.getGuidedSetupSession().setGuidedSetupMode(false);
		userSession.getGuidedSetupSession().setHasPIFguidedSetup(false);
		userSession.getGuidedSetupSession().setGuidedSetupMostrado(true);
	}

	public ModelAndView salir(HttpServletRequest request, HttpServletResponse response) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		this.saveCampaignStatus(userSession, ViewCampaign.VISTA, userSession.getGuidedSetupSession()
				.getIndividualguidedSetupCampaignId());
		this.resetAll(userSession);
		return new ModelAndView("forward:portal");
	}

	public ModelAndView ejecutarProceso(HttpServletRequest request, HttpServletResponse response) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		WebUtils.setSessionAttribute(request, "userSession", userSession);
		return new ModelAndView("gsPayments");
	}

	/*
	 * 
	 * Bgarcia MARCA LAS CAMPA�AS INDIVIDUALES DE GUIDED_SETUP QUE ESTAN
	 * VIGENTES COMO VISTA POR EL CLIENTE
	 */
	private void marcarCampIndVista(UserSession userSession, Integer status) {
		List<ActiveCampaignView> l = userSession.getCustomerService()
				.getActiveCampaignByType(CampaignType.GUIDED_SETUP);
		for (Iterator<ActiveCampaignView> iter = l.iterator(); iter.hasNext();) {
			ActiveCampaignView c = (ActiveCampaignView) iter.next();
			if (!userSession.getCustomerService().isViewedCampaign(userSession.getCustomerProfile(), c)
					&& !c.getId().equals(ActiveCampaignView.ID_GUIDED_SETUP))
				this.saveCampaignStatus(userSession, status, c.getId());
		}
	}

	private String nextView(String comingFrom, boolean hasPIF, boolean hasEBPP, boolean hasPayments) {
		if (comingFrom != null) {
			if (comingFrom.equals("") && hasEBPP)
				return this.viewByCamaignType(CampaignGuidedSetupType.ESTATEMENT_EBILL);
			if (comingFrom.equals("") && !hasEBPP && hasPayments)
				return this.viewByCamaignType(CampaignGuidedSetupType.PAGO);
			if (comingFrom.equals("") && !hasEBPP && !hasPayments) {
				return "gsSMS";
			}
			if (comingFrom.equals("gsEbills") && hasPayments)
				return this.viewByCamaignType(CampaignGuidedSetupType.PAGO);
			if (comingFrom.equals("gsEbills") && !hasPayments)
				return "gsSMS";
			if (comingFrom.equals("gsPayments"))
				return "gsSMS";
			if (comingFrom.equals("gsSMS"))
				return this.viewByCamaignType(CampaignGuidedSetupType.ALERTA);
			if (comingFrom.equals("gsAlerts") && !hasPIF)
				return "gsVistazo";
			if (comingFrom.equals("gsAlerts") && hasPIF)
				return this.viewByCamaignType(CampaignGuidedSetupType.PIF);
			if (comingFrom.equals("gsPIF"))
				return "gsVistazo";
			if (comingFrom.equals("gsVistazo")) {
				return "portal";
			}
		}

		return "";
	}

	private String viewByCamaignType(CampaignGuidedSetupType type) {
		switch (type) {
		case PIF:
			return "gsPIF";
		case ALERTA:
			return "gsAlerts";
		case ESTATEMENT_EBILL:
			return "gsEbills";
		case PAGO:
			return "gsPayments";
		default:
			return "guidedSetupCampaign";
		}

	}

	/** @return regresa el maxIntentos */
	public Integer getMaxIntentos() {
		return maxIntentos;
	}

	/**
	 * @param maxIntentos
	 *            el maxIntentos a establecer
	 */
	public void setMaxIntentos(Integer maxIntentos) {
		this.maxIntentos = maxIntentos;
	}

}
