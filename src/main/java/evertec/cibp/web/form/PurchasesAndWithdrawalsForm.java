package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rsa.csd.ws.EventType;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import edu.emory.mathcs.backport.java.util.Collections;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyCustomerAlertsRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.SmsAlertsResponse;
import evertec.cibp.core.services.responses.SmsAlertsResponse.Result;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.smsAlerts.SmsBean;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.AlertsFacade;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.PurchasesAndWithdrawalsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.AlertTypesUtils;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

public class PurchasesAndWithdrawalsForm extends CancellableFormController {
	/**
	 * Logger for this class
	 */
	private static final Logger log = Logger.getLogger(PersonalInformationChangesForm.class);
	private String alertsAccountTypes;

	// MBSFE-633
	private String alertsProductTypes;
	// END MBSFE-633

	/** Logger */

	public PurchasesAndWithdrawalsForm() {
		super();
		setCommandClass(PurchasesAndWithdrawalsBean.class);
		setCommandName("purchasesandwithdrawals");
		setFormView("purchasesandwithdrawals");
	}

	private void changeRecordInDB(CustomerProfile profile, SmsBean smsBeanRequest, CustomerServices customerService) {
		AccountMaskUtil maskutil = new AccountMaskUtil();
		try {
			// Paxus profiles for receiving alerts for every transaction.
			if ((!GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chksms())
					&& smsBeanRequest.getPurchase_all_chksms().equalsIgnoreCase("1"))
					|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chksms())
							&& smsBeanRequest.getWithdraw_all_chksms().equalsIgnoreCase("1"))
					|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chkemail())
							&& smsBeanRequest.getPurchase_all_chkemail().equalsIgnoreCase("1"))
					|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chkemail())
							&& smsBeanRequest.getWithdraw_all_chkemail().equalsIgnoreCase("1"))) {

				/**
				 * MBNA-3445
				 * Method addATHAlertEveryTransactionProfile : CIBP-WEB microservice integration
				 * 
				 * */
				boolean added = AlertsFacade.getInstance().addATHAlertEveryTransactionProfile(profile.getProfileId(),
						smsBeanRequest.getAcctNum(), smsBeanRequest.getCardNum());
				
				if (!added) {
					log.error("ATHEveryTransaction Record already exists in cibp database for profileID: "
							+ profile.getProfileId() + "\nacct: " + maskutil.mask(smsBeanRequest.getAcctNum())
							+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName());
				}
			}
			// Paxus profiles for receving alerts for every transaction
			if ((GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chksms())
					|| smsBeanRequest.getPurchase_all_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chksms())
							|| smsBeanRequest.getWithdraw_all_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chkemail())
							|| smsBeanRequest.getPurchase_all_chkemail().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chkemail())
							|| smsBeanRequest.getWithdraw_all_chkemail().equalsIgnoreCase("0"))) {

				AlertTypesUtils.removeAlertEveryTransactionProfileById(customerService, profile, smsBeanRequest, log);
				
			}
			// Paxus profiles for receiving SMS Alerts
			if ((!GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_thr_chksms())
					&& smsBeanRequest.getPurchase_thr_chksms().equalsIgnoreCase("1"))
					|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_thr_chksms())
							&& smsBeanRequest.getWithdraw_thr_chksms().equalsIgnoreCase("1"))
					|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getDlyPurchasetotlschksms())
							&& smsBeanRequest.getDlyPurchasetotlschksms().equalsIgnoreCase("1"))
					|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getDlyWithdrawtotlschksms())
							&& smsBeanRequest.getDlyWithdrawtotlschksms().equalsIgnoreCase("1"))
					|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chksms())
							&& smsBeanRequest.getPurchase_all_chksms().equalsIgnoreCase("1"))
					|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chksms())
							&& smsBeanRequest.getWithdraw_all_chksms().equalsIgnoreCase("1"))) {

				boolean added = customerService.addATHSmsAlertProfile(profile.getProfileId(),
						smsBeanRequest.getAcctNum(), smsBeanRequest.getCardNum());
				if (!added) {
					log.error("ATHSMSAlert Record already exists in cibp database for profileID: "
							+ profile.getProfileId() + "\nacct: " + maskutil.mask(smsBeanRequest.getAcctNum())
							+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName());
				}
				// return;
			}
			// Paxus profiles for receving sms alerts
			if ((GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_thr_chksms())
					|| smsBeanRequest.getPurchase_thr_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_thr_chksms())
							|| smsBeanRequest.getWithdraw_thr_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyPurchasetotlschksms())
							|| smsBeanRequest.getDlyPurchasetotlschksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyWithdrawtotlschksms())
							|| smsBeanRequest.getDlyWithdrawtotlschksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chksms())
							|| smsBeanRequest.getPurchase_all_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chksms())
							|| smsBeanRequest.getWithdraw_all_chksms().equalsIgnoreCase("0"))) {

				boolean removed = customerService.removeATHSmsAlertProfile(profile.getProfileId(),
						smsBeanRequest.getAcctNum(), smsBeanRequest.getCardNum());
				if (!removed) {
					log.error("Could not remove ATHSMSAlert Record. DOES NOT exist in cibp database for profileID: "
							+ profile.getProfileId() + "\nacct: " + maskutil.mask(smsBeanRequest.getAcctNum())
							+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName());
				}
			}

			// Paxus profiles regardless of what type alerts are configured.
			if (!((GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_thr_chksms())
					|| smsBeanRequest.getPurchase_thr_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_thr_chksms())
							|| smsBeanRequest.getWithdraw_thr_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyPurchasetotlschksms())
							|| smsBeanRequest.getDlyPurchasetotlschksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyWithdrawtotlschksms())
							|| smsBeanRequest.getDlyWithdrawtotlschksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chksms())
							|| smsBeanRequest.getPurchase_all_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chksms())
							|| smsBeanRequest.getWithdraw_all_chksms().equalsIgnoreCase("0"))

					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_thr_chkemail())
							|| smsBeanRequest.getPurchase_thr_chkemail().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_thr_chkemail())
							|| smsBeanRequest.getWithdraw_thr_chkemail().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyPurchasetotlschkemail())
							|| smsBeanRequest.getDlyPurchasetotlschkemail().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyWithdrawtotlschkemail())
							|| smsBeanRequest.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chkemail())
							|| smsBeanRequest.getPurchase_all_chkemail().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chkemail())
							|| smsBeanRequest.getWithdraw_all_chkemail().equalsIgnoreCase("0")))) {

				boolean added = customerService.addATHAnyAlertProfile(profile.getProfileId(),
						smsBeanRequest.getAcctNum(), smsBeanRequest.getCardNum());
				if (!added) {
					log.error("ATHAnyAlert Record already exists in cibp database for profileID: "
							+ profile.getProfileId() + "\nacct: " + maskutil.mask(smsBeanRequest.getAcctNum())
							+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName());
				}
			}
			// Paxus profiles regardless of what type alerts are configured.
			if ((GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_thr_chksms())
					|| smsBeanRequest.getPurchase_thr_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_thr_chksms())
							|| smsBeanRequest.getWithdraw_thr_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyPurchasetotlschksms())
							|| smsBeanRequest.getDlyPurchasetotlschksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyWithdrawtotlschksms())
							|| smsBeanRequest.getDlyWithdrawtotlschksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chksms())
							|| smsBeanRequest.getPurchase_all_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chksms())
							|| smsBeanRequest.getWithdraw_all_chksms().equalsIgnoreCase("0"))

					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_thr_chkemail())
							|| smsBeanRequest.getPurchase_thr_chkemail().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_thr_chkemail())
							|| smsBeanRequest.getWithdraw_thr_chkemail().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyPurchasetotlschkemail())
							|| smsBeanRequest.getDlyPurchasetotlschkemail().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyWithdrawtotlschkemail())
							|| smsBeanRequest.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chkemail())
							|| smsBeanRequest.getPurchase_all_chkemail().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chkemail())
							|| smsBeanRequest.getWithdraw_all_chkemail().equalsIgnoreCase("0"))) {

				boolean removed = customerService.removeATHAnyAlertProfile(profile.getProfileId(),
						smsBeanRequest.getAcctNum(), smsBeanRequest.getCardNum());
				if (!removed) {
					log.error("Could not remove ATHAnyAlert Record. DOES NOT exist in cibp database for profileID: "
							+ profile.getProfileId() + "\nacct: " + maskutil.mask(smsBeanRequest.getAcctNum())
							+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName());
				}
			}

		} catch (Exception e) {
			log.error("Error adding/removing ATH SMS ALERTS PROFILE to/from cibp database for profileID: "
					+ profile.getProfileId() + "\nacct: " + maskutil.mask(smsBeanRequest.getAcctNum()) + "\ncard: "
					+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName() + "\n"
					+ ExceptionUtils.getFullStackTrace(e));
		}

	}

	@SuppressWarnings("unchecked")
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<String, Object> model = new HashMap<>();
		AccountMaskUtil maskutil = new AccountMaskUtil();
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();

		Cookie cookie = new Cookie("PMData", userSession.getDeviceRequest().getDeviceTokenCookie());
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		ESAPI.httpUtilities().addCookie(response, cookie);

		if (!profile.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement alert = new AlertsEntitlement();
			customerService.getCustomerProfileService().addEntitlement(profile, alert.getCustomerEntitlement());
		}

		AlertsEntitlement entitlement = null;
		entitlement = new AlertsEntitlement(
				userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS));
		Set<AlertType> customeralerts = new HashSet<AlertType>();
		if (entitlement != null && entitlement.getAlerts() != null)
			customeralerts = entitlement.getAlerts();
		if (customeralerts.contains(AlertType.ATH_EVERY_TRANSACTION))
			model.put("showEveryTransactionOption", true);
		else
			model.put("showEveryTransactionOption", false);

		ModifyCustomerAlertsRequest req = new ModifyCustomerAlertsRequest();
		req.setCustomerProfile(userSession.getCustomerProfile());
		req.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));

		// OOB : verify if come from OOB Challenge
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && userSession.getOobEnroll()
				&& userSession.isOobAuthenticated()) {
			// get the information that changes if exists.
			Map<String, Object> parameters = (Map<String, Object>) WebUtils.getSessionAttribute(request,
					ChallengeInformationUtils.INFORMATION_PARAMETERS);
			if (parameters != null && parameters.get(ChallengeInformationUtils.ATH_ACTION) != null) {
				PurchasesAndWithdrawalsBean responseBean = (PurchasesAndWithdrawalsBean) parameters
						.get(ChallengeInformationUtils.ATH_ACTION);
				org.apache.commons.beanutils.BeanUtils.copyProperties(command, responseBean);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
			}
		}
		PurchasesAndWithdrawalsBean a = (PurchasesAndWithdrawalsBean) command;
		TVFrontendAccount account = Utils.getAccountById(a.getAccount1(), userSession.getAccounts());

		String action = "";
		if (!GenericValidator.isBlankOrNull(a.getAction()))
			action = a.getAction();

		String cardNumber = "";
		if (!GenericValidator.isBlankOrNull(a.getAthplastic1())) {

			CardPlasticFrontend[] plasticFrontendArray = userSession.getCardPlastics(account);

			if (plasticFrontendArray != null && plasticFrontendArray.length > 0) {
				for (int i = 0; i < plasticFrontendArray.length; i++) {
					if (plasticFrontendArray[i].getFrontEndId().equalsIgnoreCase(a.getAthplastic1())) {
						cardNumber = plasticFrontendArray[i].getCardPlasticAccountRecord().getCardPANId();
						break;
					}
				}
			}

		}

		if (action.equalsIgnoreCase("2")) {

			boolean mobileBanking;
			boolean errors = false;
			if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)) {
				mobileBanking = true;
			} else
				mobileBanking = false;

			if (!mobileBanking && !GenericValidator.isBlankOrNull(a.getWithdraw_thr_chksms2())) {
				a.setWithdraw_thr_chksms(a.getWithdraw_thr_chksms2());
			}
			if (!mobileBanking && !GenericValidator.isBlankOrNull(a.getPurchase_thr_chksms2())) {
				a.setPurchase_thr_chksms(a.getPurchase_thr_chksms2());
			}

			if (!mobileBanking && !GenericValidator.isBlankOrNull(a.getWithdraw_all_chksms2())) {
				a.setWithdraw_all_chksms(a.getWithdraw_all_chksms2());
			}
			if (!mobileBanking && !GenericValidator.isBlankOrNull(a.getPurchase_all_chksms2())) {
				a.setPurchase_all_chksms(a.getPurchase_all_chksms2());
			}

			if (!mobileBanking && !GenericValidator.isBlankOrNull(a.getDlyPurchasetotlschksms2())) {
				a.setDlyPurchasetotlschksms(a.getDlyPurchasetotlschksms2());
			}
			if (!mobileBanking && !GenericValidator.isBlankOrNull(a.getDlyWithdrawtotlschksms2())) {
				a.setDlyWithdrawtotlschksms(a.getDlyWithdrawtotlschksms2());
			}

			String phone = null;
			String provider = null;
			SMSProfile smsProfile = null;
			if (mobileBanking) {
				if (userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)) != null)
					smsProfile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
				if (smsProfile != null && smsProfile.getCtns() != null) {
					Set<SMSCTN> set = smsProfile.getCtns();
					for (SMSCTN ctnBean : set) {
						provider = ctnBean.getProvider();
						phone = ctnBean.getCtn();
					}
				}
			}

			String statusMessage;

			if (GenericValidator.isBlankOrNull(a.getPurchase_thr_chkemail()))
				a.setPurchase_thr_chkemail("0");
			if (GenericValidator.isBlankOrNull(a.getPurchase_thr_chksms()))
				a.setPurchase_thr_chksms("0");
			if (GenericValidator.isBlankOrNull(a.getWithdraw_thr_chkemail()))
				a.setWithdraw_thr_chkemail("0");
			if (GenericValidator.isBlankOrNull(a.getWithdraw_thr_chksms()))
				a.setWithdraw_thr_chksms("0");
			if (GenericValidator.isBlankOrNull(a.getDlyPurchasetotlschkemail()))
				a.setDlyPurchasetotlschkemail("0");
			if (GenericValidator.isBlankOrNull(a.getDlyPurchasetotlschksms()))
				a.setDlyPurchasetotlschksms("0");
			if (GenericValidator.isBlankOrNull(a.getDlyWithdrawtotlschkemail()))
				a.setDlyWithdrawtotlschkemail("0");
			if (GenericValidator.isBlankOrNull(a.getDlyWithdrawtotlschksms()))
				a.setDlyWithdrawtotlschksms("0");
			if (GenericValidator.isBlankOrNull(a.getPurchase_all_chkemail()))
				a.setPurchase_all_chkemail("0");
			if (GenericValidator.isBlankOrNull(a.getPurchase_all_chksms()))
				a.setPurchase_all_chksms("0");
			if (GenericValidator.isBlankOrNull(a.getWithdraw_all_chkemail()))
				a.setWithdraw_all_chkemail("0");
			if (GenericValidator.isBlankOrNull(a.getWithdraw_all_chksms()))
				a.setWithdraw_all_chksms("0");

			String alertsPurchaseError = "";

			String alertsEveryPurchaseError = "";
			String alertsEveryWithdrawError = "";
			String alertsDailyTotalPurchaseError = "";
			String alertsDailytotalWithdrawError = "";

			String alertsWithdrawError = "";
			String alertsStartEndError = "";
			String onlyDigits;
			String leaveBlank;
			String amountRequired;
			String register;
			String alertTimes;

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				onlyDigits = "Only digits greater than zero allowed.";
				leaveBlank = "Leave amount blank or select email/sms option(s).";
				amountRequired = "Amount required if selecting email/sms option(s).";
				register = "Must register cell phone number to receive sms alerts.";
				alertTimes = "Verify alert start and end times.";
			} else {
				onlyDigits = "Solo d\u00EDgitos mayor que zero son v\u00E1lidos.";
				leaveBlank = "Dejar cantidad en blanco o marcar opcion(es) email/sms.";
				amountRequired = "Cantidad requerida si marcas opcion(es) email/sms.";
				register = "Requerido registrar # celular para recibir alertas sms.";
				alertTimes = "Verificar los tiempos para env\u00EDos de alertas.";
			}
			final String regExp = "^[1-9][0-9]*$";
			if (!GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
					&& !GenericValidator.matchRegexp(a.getAlertsPurchaseminamt(), regExp)) {
				alertsPurchaseError = onlyDigits;
				errors = true;
			}
			if (!GenericValidator.isBlankOrNull(a.getAlertsWithdrawminamt())
					&& !GenericValidator.matchRegexp(a.getAlertsWithdrawminamt(), regExp)) {
				alertsWithdrawError = onlyDigits;
				errors = true;
			}

			if (!GenericValidator.isBlankOrNull(a.getDlyPurchasetotlsminamt())
					&& !GenericValidator.matchRegexp(a.getDlyPurchasetotlsminamt(), regExp)) {
				alertsDailyTotalPurchaseError = onlyDigits;
				errors = true;
			}
			if (!GenericValidator.isBlankOrNull(a.getDlyWithdrawtotlsminamt())
					&& !GenericValidator.matchRegexp(a.getDlyWithdrawtotlsminamt(), regExp)) {
				alertsDailytotalWithdrawError = onlyDigits;
				errors = true;
			}

			if (!GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
					&& GenericValidator.matchRegexp(a.getAlertsPurchaseminamt(), regExp)
					&& a.getPurchase_thr_chkemail().equalsIgnoreCase("0")
					&& a.getPurchase_thr_chksms().equalsIgnoreCase("0")) {
				if (!alertsPurchaseError.equals(""))
					alertsPurchaseError += "<br>";

				alertsPurchaseError += leaveBlank;
				errors = true;
			}
			if (!GenericValidator.isBlankOrNull(a.getAlertsWithdrawminamt())
					&& GenericValidator.matchRegexp(a.getAlertsWithdrawminamt(), regExp)
					&& a.getWithdraw_thr_chkemail().equalsIgnoreCase("0")
					&& a.getWithdraw_thr_chksms().equalsIgnoreCase("0")) {
				if (!alertsWithdrawError.equals(""))
					alertsWithdrawError += "<br>";

				alertsWithdrawError += leaveBlank;
				errors = true;
			}
			if (!GenericValidator.isBlankOrNull(a.getDlyPurchasetotlsminamt())
					&& GenericValidator.matchRegexp(a.getDlyPurchasetotlsminamt(), regExp)
					&& a.getDlyPurchasetotlschkemail().equalsIgnoreCase("0")
					&& a.getDlyPurchasetotlschksms().equalsIgnoreCase("0")) {
				if (!alertsDailyTotalPurchaseError.equals(""))
					alertsDailyTotalPurchaseError += "<br>";

				alertsDailyTotalPurchaseError += leaveBlank;
				errors = true;
			}
			if (!GenericValidator.isBlankOrNull(a.getDlyWithdrawtotlsminamt())
					&& GenericValidator.matchRegexp(a.getDlyWithdrawtotlsminamt(), regExp)
					&& a.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0")
					&& a.getDlyWithdrawtotlschksms().equalsIgnoreCase("0")) {
				if (!alertsDailytotalWithdrawError.equals(""))
					alertsDailytotalWithdrawError += "<br>";

				alertsDailytotalWithdrawError += leaveBlank;
				errors = true;
			}
			if (GenericValidator.isBlankOrNull(a.getDlyPurchasetotlsminamt())
					&& (a.getDlyPurchasetotlschkemail().equalsIgnoreCase("1")
							|| a.getDlyPurchasetotlschksms().equalsIgnoreCase("1"))) {
				if (!alertsDailyTotalPurchaseError.equals(""))
					alertsDailyTotalPurchaseError += "<br>";

				alertsDailyTotalPurchaseError += amountRequired;
				errors = true;
			}
			if (GenericValidator.isBlankOrNull(a.getDlyWithdrawtotlsminamt())
					&& (a.getDlyWithdrawtotlschkemail().equalsIgnoreCase("1")
							|| a.getDlyWithdrawtotlschksms().equalsIgnoreCase("1"))) {
				if (!alertsDailytotalWithdrawError.equals(""))
					alertsDailytotalWithdrawError += "<br>";

				alertsDailytotalWithdrawError += amountRequired;
				errors = true;
			}

			if (GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
					&& (a.getPurchase_thr_chkemail().equalsIgnoreCase("1")
							|| a.getPurchase_thr_chksms().equalsIgnoreCase("1"))) {
				if (!alertsPurchaseError.equals(""))
					alertsPurchaseError += "<br>";

				alertsPurchaseError += amountRequired;
				errors = true;
			}
			if (GenericValidator.isBlankOrNull(a.getAlertsWithdrawminamt())
					&& (a.getWithdraw_thr_chkemail().equalsIgnoreCase("1")
							|| a.getWithdraw_thr_chksms().equalsIgnoreCase("1"))) {
				if (!alertsWithdrawError.equals(""))
					alertsWithdrawError += "<br>";

				alertsWithdrawError += amountRequired;
				errors = true;
			}

			if (a.getDlyPurchasetotlschksms().equalsIgnoreCase("1") && !mobileBanking) {
				if (!alertsDailyTotalPurchaseError.equals(""))
					alertsDailyTotalPurchaseError += "<br>";

				alertsDailyTotalPurchaseError += register;
				errors = true;
			}
			if (a.getDlyWithdrawtotlschksms().equalsIgnoreCase("1") && !mobileBanking) {
				if (!alertsDailytotalWithdrawError.equals(""))
					alertsDailytotalWithdrawError += "<br>";

				alertsDailytotalWithdrawError += register;
				errors = true;
			}

			if (a.getPurchase_thr_chksms().equalsIgnoreCase("1") && !mobileBanking) {
				if (!alertsPurchaseError.equals(""))
					alertsPurchaseError += "<br>";

				alertsPurchaseError += register;
				errors = true;
			}
			if (a.getWithdraw_thr_chksms().equalsIgnoreCase("1") && !mobileBanking) {
				if (!alertsWithdrawError.equals(""))
					alertsWithdrawError += "<br>";

				alertsWithdrawError += register;
				errors = true;
			}

			if (a.getPurchase_all_chksms().equalsIgnoreCase("1") && !mobileBanking) {
				if (!alertsEveryPurchaseError.equals(""))
					alertsEveryPurchaseError += "<br>";

				alertsEveryPurchaseError += register;
				errors = true;
			}
			if (a.getWithdraw_all_chksms().equalsIgnoreCase("1") && !mobileBanking) {
				if (!alertsEveryWithdrawError.equals(""))
					alertsEveryWithdrawError += "<br>";

				alertsEveryWithdrawError += register;
				errors = true;
			}
			if (a.getProfileNtfyallhour().equalsIgnoreCase("0")
					&& (GenericValidator.isBlankOrNull(a.getProfileNtfystrhour())
							|| GenericValidator.isBlankOrNull(a.getProfileNtfyendhour()))) {
				alertsStartEndError = alertTimes;
				errors = true;
			}

			if (!alertsPurchaseError.equals(""))
				model.put("alertsPurchaseError", alertsPurchaseError);
			if (!alertsWithdrawError.equals(""))
				model.put("alertsWithdrawError", alertsWithdrawError);
			if (!alertsStartEndError.equals(""))
				model.put("alertsStartEndError", alertsStartEndError);

			if (!alertsEveryPurchaseError.equals(""))
				model.put("alertsEveryPurchaseError", alertsEveryPurchaseError);
			if (!alertsEveryWithdrawError.equals(""))
				model.put("alertsEveryWithdrawError", alertsEveryWithdrawError);
			if (!alertsDailyTotalPurchaseError.equals(""))
				model.put("alertsDailyTotalPurchaseError", alertsDailyTotalPurchaseError);
			if (!alertsDailytotalWithdrawError.equals(""))
				model.put("alertsDailytotalWithdrawError", alertsDailytotalWithdrawError);

			if (errors)
				return showForm(request, response, error, model);

			// smsDelete
			if (a.getPurchase_thr_chkemail().equalsIgnoreCase("0") && a.getPurchase_thr_chksms().equalsIgnoreCase("0")
					&& a.getWithdraw_thr_chkemail().equalsIgnoreCase("0")
					&& a.getWithdraw_thr_chksms().equalsIgnoreCase("0")
					&& a.getDlyPurchasetotlschkemail().equalsIgnoreCase("0")
					&& a.getDlyPurchasetotlschksms().equalsIgnoreCase("0")
					&& a.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0")
					&& a.getDlyWithdrawtotlschksms().equalsIgnoreCase("0")
					&& a.getPurchase_all_chkemail().equalsIgnoreCase("0")
					&& a.getPurchase_all_chksms().equalsIgnoreCase("0")
					&& a.getWithdraw_all_chkemail().equalsIgnoreCase("0")
					&& a.getWithdraw_all_chksms().equalsIgnoreCase("0")) {

				SmsBean smsBeanRequest = new SmsBean();
				smsBeanRequest.setAcctNum(account.getAccountNumber());
				smsBeanRequest.setTaxId(profile.getTaxId());
				smsBeanRequest.setCardNum(cardNumber);
				try {
					if (account.getCurrentAthSmsAlerts() != null && account.getCurrentAthSmsAlerts().size() > 0) {
						HashMap<Object, Object> smsInfo = account.getCurrentAthSmsAlerts();
						if (smsInfo.containsKey(cardNumber) && smsInfo.get(cardNumber) != null) {
							SmsAlertsResponse smsAlertsResponse = customerService.deleteSmsAlert(smsBeanRequest,
									Utils.generateCustomerInteractionEvent(request));
							if (smsAlertsResponse.getResult().equals(Result.SUCCESS)) {
								changeRecordInDB(profile, smsBeanRequest, customerService);
								if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
									statusMessage = "Profile Deleted";
								} else {
									statusMessage = "Perfil Borrado";
								}
								model.put("statusMessage", statusMessage);
								account.getCurrentAthSmsAlerts().remove(cardNumber);
							}

							else if (smsAlertsResponse.getResult().equals(Result.ERROR)
									&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
								if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
									statusMessage = "Error deleting.";
								} else {
									statusMessage = "Error borrando.";
								}
								model.put("statusMessage", statusMessage);
								log.error("Error: Unable to send ATH SmsDelete request for \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId());
							} else {
								if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
									statusMessage = "Unable to delete. Profile not found.";
								} else {
									statusMessage = "No se pudo Borrar. Perfil no encontrado.";
								}
								model.put("statusMessage", statusMessage);
								log.error("Error: Unable to delete ATH Alert (Profile not found) for \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId());
							}
						} else {
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								statusMessage = "Unable to delete. Profile not found.";
							} else {
								statusMessage = "No se pudo Borrar. Perfil no encontrado.";
							}
							model.put("statusMessage", statusMessage);
							log.error("Error: Unable to delete ATH Alert (Profile not found) for \nacct: "
									+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
									+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
									+ "\nProfileID: " + profile.getProfileId());
						}
					} else {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							statusMessage = "Unable to delete. Profile not found.";
						} else {
							statusMessage = "No se pudo Borrar. Perfil no encontrado.";
						}
						model.put("statusMessage", statusMessage);
					}
				} catch (Exception e) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						statusMessage = "Error deleting.";
					} else {
						statusMessage = "Error borrando.";
					}
					model.put("statusMessage", statusMessage);
					log.error("Error: customerService.deleteSmsAlert(...) \nacct: "
							+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
							+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + "\nProfileID: "
							+ profile.getProfileId(), e);
				}
				return showForm(request, response, error, model);
			}

			// update or add sms profile alerts
			if (!(a.getPurchase_thr_chkemail().equalsIgnoreCase("0") && a.getPurchase_thr_chksms().equalsIgnoreCase("0")
					&& a.getWithdraw_thr_chkemail().equalsIgnoreCase("0")
					&& a.getWithdraw_thr_chksms().equalsIgnoreCase("0")
					&& a.getDlyPurchasetotlschkemail().equalsIgnoreCase("0")
					&& a.getDlyPurchasetotlschksms().equalsIgnoreCase("0")
					&& a.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0")
					&& a.getDlyWithdrawtotlschksms().equalsIgnoreCase("0")
					&& a.getPurchase_all_chkemail().equalsIgnoreCase("0")
					&& a.getPurchase_all_chksms().equalsIgnoreCase("0")
					&& a.getWithdraw_all_chkemail().equalsIgnoreCase("0")
					&& a.getWithdraw_all_chksms().equalsIgnoreCase("0"))) {

				SmsBean smsBeanRequest = new SmsBean();

				org.apache.commons.beanutils.BeanUtils.copyProperties(smsBeanRequest, a);

				if (!GenericValidator.isBlankOrNull(phone))
					smsBeanRequest.setPrimaryphone(phone);
				if (!GenericValidator.isBlankOrNull(provider))
					smsBeanRequest.setPrimaryPhoneCarrier(provider);
				smsBeanRequest.setAcctNum(account.getAccountNumber());
				smsBeanRequest.setCardNum(cardNumber);
				smsBeanRequest.setProfileOnOffFlag("1");
				smsBeanRequest.setTaxId(profile.getTaxId());
				smsBeanRequest.setRealName(profile.getFullName());
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					smsBeanRequest.setLanguagecode("EN");
				} else {
					smsBeanRequest.setLanguagecode("ES");
				}
				smsBeanRequest.setPrimaryemail(profile.getEmail());

				// Attempt update profile
				if (account.getCurrentAthSmsAlerts() != null && account.getCurrentAthSmsAlerts().size() > 0) {
					try {
						HashMap<Object, Object> smsInfo = account.getCurrentAthSmsAlerts();
						if (smsInfo.containsKey(cardNumber) && smsInfo.get(cardNumber) != null) {
							SmsAlertsResponse smsAlertsResponse = customerService.updateSmsAlert(smsBeanRequest,
									Utils.generateCustomerInteractionEvent(request));
							if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
									&& smsAlertsResponse.getResponseSmsBean() != null) {
								account.getCurrentAthSmsAlerts().put(cardNumber,
										smsAlertsResponse.getResponseSmsBean());
								changeRecordInDB(profile, smsBeanRequest, customerService);
								if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
									statusMessage = "Profile Updated";
								} else {
									statusMessage = "Perfil actualizado";
								}
								model.put("statusMessage", statusMessage);
								return showForm(request, response, error, model);
							} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
									&& smsAlertsResponse.getResponseSmsBean() == null) {
								changeRecordInDB(profile, smsBeanRequest, customerService);
								if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
									statusMessage = "Profile Updated.\nError retrieving updated info.";
								} else {
									statusMessage = "Perfil actualizado.\nError devolviendo info actualizada.";
								}
								model.put("statusMessage", statusMessage);
								if (!smsAlertsResponse.isAuditable())
									log.error(
											"Error: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
													+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
													+ profile.getFullName() + "\nProfileID: " + profile.getProfileId()
													+ smsAlertsResponse.getAuditString());
								else
									log.error(
											"Error: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
													+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
													+ profile.getFullName() + "\nProfileID: " + profile.getProfileId());
								return showForm(request, response, error, model);
							} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
									&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
								if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
									statusMessage = "Unable to Update Profile.";
								} else {
									statusMessage = "No se pudo actualizar.";
								}
								model.put("statusMessage", statusMessage);
								if (!smsAlertsResponse.isAuditable())
									log.error("Error: Unable to send ATH smsUpdate request for \nacct: "
											+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
											+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
											+ "\nProfileID: " + profile.getProfileId());
								else
									log.error("Error: Unable to send ATH smsUpdate request for \nacct: "
											+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
											+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
											+ "\nProfileID: " + profile.getProfileId() + "\n"
											+ smsAlertsResponse.getAuditString());
								return showForm(request, response, error, model);
							} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
									&& (smsAlertsResponse.getException() != null || !GenericValidator
											.isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
								if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
									statusMessage = "Unable to Update Profile.";
								} else {
									statusMessage = "No se pudo actualizar.";
								}
								model.put("statusMessage", statusMessage);
								if (!smsAlertsResponse.isAuditable()
										&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
										&& smsAlertsResponse.getException() != null)
									log.error("Error: calling ATH smsUpdate \nacct: "
											+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
											+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
											+ "\nProfileID: " + profile.getProfileId() + "\n"
											+ smsAlertsResponse.getSmsAlertsException() + "\n"
											+ smsAlertsResponse.getException());
								else if (!smsAlertsResponse.isAuditable()
										&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
										&& smsAlertsResponse.getException() == null) {
									log.error("Error: calling ATH smsUpdate \nacct: "
											+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
											+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
											+ "\nProfileID: " + profile.getProfileId() + "\n"
											+ smsAlertsResponse.getSmsAlertsException());
								} else if (!smsAlertsResponse.isAuditable()
										&& GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
										&& smsAlertsResponse.getException() != null) {
									log.error("Error: calling ATH smsUpdate \nacct: "
											+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
											+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
											+ "\nProfileID: " + profile.getProfileId() + "\n"
											+ smsAlertsResponse.getException());
								} else if (smsAlertsResponse.isAuditable()
										&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
										&& smsAlertsResponse.getException() != null)
									log.error("Error: calling ATH smsUpdate \nacct: "
											+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
											+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
											+ "\nProfileID: " + profile.getProfileId() + "\n"
											+ smsAlertsResponse.getSmsAlertsException() + "\n"
											+ smsAlertsResponse.getAuditString());
								else {
									log.error("Error: calling ATH smsUpdate \nacct: "
											+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
											+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
											+ "\nProfileID: " + profile.getProfileId() + "\n"
											+ smsAlertsResponse.getAuditString());
								}
								return showForm(request, response, error, model);
							} else {
								if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
									statusMessage = "Unable to Update Profile.";
								} else {
									statusMessage = "No se pudo actualizar.";
								}
								model.put("statusMessage", statusMessage);
								if (!smsAlertsResponse.isAuditable())
									log.error("Error: calling ATH smsUpdate \nacct: "
											+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
											+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
											+ "\nProfileID: " + profile.getProfileId() + "\n");
								else {
									log.error("Error: calling ATH smsUpdate \nacct: "
											+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
											+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
											+ "\nProfileID: " + profile.getProfileId() + "\n"
											+ smsAlertsResponse.getAuditString());
								}
								return showForm(request, response, error, model);
							}
						}
					} catch (Exception e) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							statusMessage = "Unable to Update Profile.";
						} else {
							statusMessage = "No se pudo actualizar.";
						}
						model.put("statusMessage", statusMessage);
						log.error("Error: customerService.updateSmsAlert(...) \nacct: "
								+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
								+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + "\nProfileID: "
								+ profile.getProfileId(), e);
						return showForm(request, response, error, model);
					}

				}

				// Add smsProfile alerts
				try {
					SmsAlertsResponse smsAlertsResponse = customerService.addSmsAlert(smsBeanRequest,
							Utils.generateCustomerInteractionEvent(request));
					if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
							&& smsAlertsResponse.getResponseSmsBean() != null) {
						changeRecordInDB(profile, smsBeanRequest, customerService);

						if (account.getCurrentAthSmsAlerts() != null)
							account.getCurrentAthSmsAlerts().put(cardNumber, smsAlertsResponse.getResponseSmsBean());
						else {
							HashMap<Object, Object> smsInfo = new HashMap<>();
							smsInfo.put(cardNumber, smsAlertsResponse.getResponseSmsBean());
							account.setCurrentAthSmsAlerts(smsInfo);
						}

						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							statusMessage = "Profile Added";
						} else {
							statusMessage = "Perfil agregado";
						}
						model.put("statusMessage", statusMessage);
						return showForm(request, response, error, model);
					} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
							&& smsAlertsResponse.getResponseSmsBean() == null) {
						changeRecordInDB(profile, smsBeanRequest, customerService);
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							statusMessage = "Profile Added.\nError retrieving added info.";
						} else {
							statusMessage = "Perfil agregado.\nError devolviendo info agregada.";
						}
						model.put("statusMessage", statusMessage);
						if (!smsAlertsResponse.isAuditable())
							log.error("Error: ATH smsAdd successful, BUT error retrieving added details for \nacct: "
									+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
									+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
									+ "\nProfileID: " + profile.getProfileId() + smsAlertsResponse.getAuditString());
						else
							log.error("Error: ATH smsAdd successful, BUT error retrieving added details for \nacct: "
									+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
									+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
									+ "\nProfileID: " + profile.getProfileId());
						return showForm(request, response, error, model);
					} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
							&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							statusMessage = "Unable to Add Profile.";
						} else {
							statusMessage = "No se pudo Agregar.";
						}
						model.put("statusMessage", statusMessage);
						if (!smsAlertsResponse.isAuditable())
							log.error("Error: Unable to send ATH smsAdd request for \nacct: "
									+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
									+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
									+ "\nProfileID: " + profile.getProfileId());
						else
							log.error("Error: Unable to send ATH smsAdd request for \nacct: "
									+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
									+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
									+ "\nProfileID: " + profile.getProfileId() + "\n"
									+ smsAlertsResponse.getAuditString());
						return showForm(request, response, error, model);
					} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
							&& (smsAlertsResponse.getException() != null
									|| !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							statusMessage = "Unable to Add Profile.";
						} else {
							statusMessage = "No se pudo Agregar.";
						}
						model.put("statusMessage", statusMessage);
						if (!smsAlertsResponse.isAuditable()
								&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
								&& smsAlertsResponse.getException() != null)
							log.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
									+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
									+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n"
									+ smsAlertsResponse.getSmsAlertsException() + "\n"
									+ smsAlertsResponse.getException());
						else if (!smsAlertsResponse.isAuditable()
								&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
								&& smsAlertsResponse.getException() == null) {
							log.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
									+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
									+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n"
									+ smsAlertsResponse.getSmsAlertsException());
						} else if (!smsAlertsResponse.isAuditable()
								&& GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
								&& smsAlertsResponse.getException() != null) {
							log.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
									+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
									+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n"
									+ smsAlertsResponse.getException());
						} else if (smsAlertsResponse.isAuditable()
								&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
								&& smsAlertsResponse.getException() != null)
							log.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
									+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
									+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n"
									+ smsAlertsResponse.getSmsAlertsException() + "\n"
									+ smsAlertsResponse.getAuditString());
						else {
							log.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
									+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
									+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n"
									+ smsAlertsResponse.getAuditString());
						}
						return showForm(request, response, error, model);
					} else {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							statusMessage = "Unable to Add Profile.";
						} else {
							statusMessage = "No se pudo Agregar.";
						}
						model.put("statusMessage", statusMessage);
						if (!smsAlertsResponse.isAuditable())
							log.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
									+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
									+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n");
						else {
							log.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
									+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
									+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n"
									+ smsAlertsResponse.getAuditString());
						}
						return showForm(request, response, error, model);
					}
				} catch (Exception e) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						statusMessage = "Unable to Add Profile.";
					} else {
						statusMessage = "No se pudo agregar.";
					}
					model.put("statusMessage", statusMessage);
					log.error("Error: customerService.addSmsAlert(...) \nacct: "
							+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
							+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + "\nProfileID: "
							+ profile.getProfileId(), e);
					return showForm(request, response, error, model);
				}

			}

		}

		Map<Object, Object> accountsMap = new LinkedHashMap<>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			accountsMap.put("none", "Select an ATH\u00AE Card");
		else
			accountsMap.put("none", "Seleccione una Tarjeta ATH\u00AE");

		if (a.isRetrievePlastics()) {

			CardPlasticFrontend[] plastics = userSession.getCardPlastics(account);
			if (plastics == null || plastics.length == 0) {
				model.put("retrievingPlastics", true);
				model.put("accountDetails", accountsMap);
				return new ModelAndView("athPlastics", model);
			}
			if (plastics.length > 0 && account.getCurrentAthSmsAlerts() != null
					&& account.getCurrentAthSmsAlerts().size() > 0) {
				for (int i = 0; i < plastics.length; i++) {
					String cardType;
					if (plastics[i].getCardPlasticAccountRecord().getCardPANId().startsWith("0215")) {
						cardType = "ATH\u00AE Regular";
					} else if (plastics[i].getCardPlasticAccountRecord().getCardPANId().startsWith("4549")) {
						cardType = "ATH\u00AE Internacional";
					} else if (plastics[i].getCardPlasticAccountRecord().getCardPANId().startsWith("60119052")) {// 60119052
																													// ATH
																													// –
																													// Discover
						cardType = "ATH \u002D Discover";
					} else {
						cardType = "";
					}
					accountsMap.put(plastics[i].getFrontEndId(),
							cardType + " " + maskutil.mask(plastics[i].getCardPlasticAccountRecord().getCardPANId()));
				}
			}
			if (plastics.length > 0
					&& !(account.getCurrentAthSmsAlerts() != null && account.getCurrentAthSmsAlerts().size() > 0)) {
				HashMap<Object, Object> smsInfo = new HashMap<>();
				for (int i = 0; i < plastics.length; i++) {
					String cardType;
					if (plastics[i].getCardPlasticAccountRecord().getCardPANId().startsWith("0215")) {
						cardType = "ATH\u00AE Regular";
					} else if (plastics[i].getCardPlasticAccountRecord().getCardPANId().startsWith("4549")) {
						cardType = "ATH\u00AE Internacional";
					} else if (plastics[i].getCardPlasticAccountRecord().getCardPANId().startsWith("60119052")) {// 60119052
																													// ATH
																													// –
																													// Discover
						cardType = "ATH \u002D Discover";
					} else {
						cardType = "";
					}
					accountsMap.put(plastics[i].getFrontEndId(),
							cardType + " " + maskutil.mask(plastics[i].getCardPlasticAccountRecord().getCardPANId()));
					SmsBean smsBeanRequest = new SmsBean();
					smsBeanRequest.setAcctNum(account.getAccountNumber());
					smsBeanRequest.setTaxId(profile.getTaxId());
					smsBeanRequest.setCardNum(plastics[i].getCardPlasticAccountRecord().getCardPANId());
					try {
						SmsAlertsResponse smsAlertsResponse = customerService.getSmsAlertInfo(smsBeanRequest,
								Utils.generateCustomerInteractionEvent(request));
						if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
								&& smsAlertsResponse.getResponseSmsBean() != null) {
							smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
									smsAlertsResponse.getResponseSmsBean());
						} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
								&& smsAlertsResponse.getResponseSmsBean() == null) {
							smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
							if (!smsAlertsResponse.isAuditable())
								log.error(
										"Error: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
												+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
												+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
												+ profile.getFullName() + "\nProfileID: " + profile.getProfileId()
												+ smsAlertsResponse.getAuditString());
							else
								log.error(
										"Error: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
												+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
												+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
												+ profile.getFullName() + "\nProfileID: " + profile.getProfileId());
						} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
								&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
							smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
							if (!smsAlertsResponse.isAuditable())
								log.error("Error: Unable to send ATH SmsInfo request for \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId());
							else
								log.error("Error: Unable to send ATH SmsInfo request for \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n"
										+ smsAlertsResponse.getAuditString());
						} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
								&& (smsAlertsResponse.getException() != null || !GenericValidator
										.isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
							smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
							if (!smsAlertsResponse.isAuditable()
									&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
									&& smsAlertsResponse.getException() != null)
								log.error("Error: calling ATH SmsInfo \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n"
										+ smsAlertsResponse.getSmsAlertsException() + "\n"
										+ smsAlertsResponse.getException());
							else if (!smsAlertsResponse.isAuditable()
									&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
									&& smsAlertsResponse.getException() == null) {
								log.error("Error: calling ATH SmsInfo \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n"
										+ smsAlertsResponse.getSmsAlertsException());
							} else if (!smsAlertsResponse.isAuditable()
									&& GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
									&& smsAlertsResponse.getException() != null) {
								log.error("Error: calling ATH SmsInfo \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n"
										+ smsAlertsResponse.getException());
							} else if (smsAlertsResponse.isAuditable()
									&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
									&& smsAlertsResponse.getException() != null)
								log.error("Error: calling ATH SmsInfo \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n"
										+ smsAlertsResponse.getSmsAlertsException() + "\n"
										+ smsAlertsResponse.getAuditString());
							else {
								log.error("Error: calling ATH SmsInfo \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n"
										+ smsAlertsResponse.getAuditString());
							}
						} else {
							smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
						}
					} catch (Exception e) {
						smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
						log.error("Error: customerService.getSmsAlertInfo(...) \nacct: "
								+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
								+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + "\nProfileID: "
								+ profile.getProfileId(), e);
					}
				}
				if (smsInfo.size() > 0)
					account.setCurrentAthSmsAlerts(smsInfo);
			}
			model.put("retrievingPlastics", true);
			model.put("accountDetails", accountsMap);
			return new ModelAndView("athPlastics", model);
		}

		if (a.isDisplayPlasticInfo()) {
			if (account.getCurrentAthSmsAlerts() != null && account.getCurrentAthSmsAlerts().size() > 0) {

				HashMap<Object, Object> smsInfo = account.getCurrentAthSmsAlerts();
				if (smsInfo.containsKey(cardNumber) && smsInfo.get(cardNumber) != null) {
					SmsBean infoBean = (SmsBean) smsInfo.get(cardNumber);
					model.put("ProfileFound", true);

					if (infoBean.getPurchase_all_chkemailSpecified()
							&& !infoBean.getPurchase_all_chkemail().equalsIgnoreCase("0")) {
						model.put("everypurchasechkemailSpecified", true);
					} else {
						model.put("everypurchasechkemailSpecified", false);

					}
					if (infoBean.getPurchase_all_chksmsSpecified()
							&& !infoBean.getPurchase_all_chksms().equalsIgnoreCase("0")) {
						model.put("everypurchasechksmsSpecified", true);
					} else
						model.put("everypurchasechksmsSpecified", false);

					if (infoBean.getPurchase_thr_chkemailSpecified()
							&& !infoBean.getPurchase_thr_chkemail().equalsIgnoreCase("0")) {
						model.put("purchasechkemailSpecified", true);
					} else {
						model.put("purchasechkemailSpecified", false);

					}
					if (infoBean.getPurchase_thr_chksmsSpecified()
							&& !infoBean.getPurchase_thr_chksms().equalsIgnoreCase("0")) {
						model.put("purchasechksmsSpecified", true);
					} else
						model.put("purchasechksmsSpecified", false);

					if (infoBean.getPurchaseminamtSpecified()) {
						model.put("purchaseminamtSpecified", true);
						model.put("purchase_min_amt", infoBean.getAlertsPurchaseminamt());
					} else
						model.put("purchaseminamtSpecified", false);

					if (infoBean.getDlyPurchasetotlschkemailSpecified()
							&& !infoBean.getDlyPurchasetotlschkemail().equalsIgnoreCase("0")) {
						model.put("dailytotalpurchasechkemailSpecified", true);
					} else {
						model.put("dailytotalpurchasechkemailSpecified", false);

					}
					if (infoBean.getDlyPurchasetotlschksmsSpecified()
							&& !infoBean.getDlyPurchasetotlschksms().equalsIgnoreCase("0")) {
						model.put("dailytotalpurchasechksmsSpecified", true);
					} else
						model.put("dailytotalpurchasechksmsSpecified", false);

					if (infoBean.getDlyPurchasetotlsminamtSpecified()) {
						model.put("dailytotalpurchaseminamtSpecified", true);
						model.put("dailytotalpurchase_min_amt", infoBean.getDlyPurchasetotlsminamt());
					} else
						model.put("dailytotalpurchaseminamtSpecified", false);

					if (infoBean.getWithdraw_all_chkemailSpecified()
							&& !infoBean.getWithdraw_all_chkemail().equalsIgnoreCase("0")) {
						model.put("everywithdrawchkemailSpecified", true);
					} else
						model.put("everywithdrawchkemailSpecified", false);

					if (infoBean.getWithdraw_all_chksmsSpecified()
							&& !infoBean.getWithdraw_all_chksms().equalsIgnoreCase("0")) {
						model.put("everywithdrawchksmsSpecified", true);
					} else
						model.put("everywithdrawchksmsSpecified", false);

					if (infoBean.getWithdraw_thr_chkemailSpecified()
							&& !infoBean.getWithdraw_thr_chkemail().equalsIgnoreCase("0")) {
						model.put("withdrawchkemailSpecified", true);
					} else
						model.put("withdrawchkemailSpecified", false);

					if (infoBean.getWithdraw_thr_chksmsSpecified()
							&& !infoBean.getWithdraw_thr_chksms().equalsIgnoreCase("0")) {
						model.put("withdrawchksmsSpecified", true);
					} else
						model.put("withdrawchksmsSpecified", false);

					if (infoBean.getWithdrawminamtSpecified()) {
						model.put("withdrawminamtSpecified", true);
						model.put("withdraw_min_amt", infoBean.getAlertsWithdrawminamt());
					} else
						model.put("withdrawminamtSpecified", false);

					if (infoBean.getDlyWithdrawtotlschkemailSpecified()
							&& !infoBean.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0")) {
						model.put("dailytotalwithdrawchkemailSpecified", true);
					} else {
						model.put("dailytotalwithdrawchkemailSpecified", false);

					}
					if (infoBean.getDlyWithdrawtotlschksmsSpecified()
							&& !infoBean.getDlyWithdrawtotlschksms().equalsIgnoreCase("0")) {
						model.put("dailytotalwithdrawchksmsSpecified", true);
					} else
						model.put("dailytotalwithdrawchksmsSpecified", false);

					if (infoBean.getDlyWithdrawtotlsminamtSpecified()) {
						model.put("dailytotalwithdrawminamtSpecified", true);
						model.put("dailytotalwithdraw_min_amt", infoBean.getDlyWithdrawtotlsminamt());
					} else
						model.put("dailytotalwithdrawminamtSpecified", false);

					if (!GenericValidator.isBlankOrNull(infoBean.getProfileNtfyallhour())
							&& !infoBean.getProfileNtfyallhour().equalsIgnoreCase("0")) {
						model.put("NotifyAllHour", true);
						model.put("NotifyStartHour", false);
						model.put("NotifyEndHour", false);

					} else {
						model.put("NotifyAllHour", false);
						model.put("NotifyStartHour", true);
						model.put("NotifyEndHour", true);
						model.put("NotifyStartHourValue", infoBean.getProfileNtfystrhour());
						model.put("NotifyEndHourValue", infoBean.getProfileNtfyendhour());
					}
				} else {
					model.put("dailytotalwithdrawminamtSpecified", false);
					model.put("dailytotalwithdrawchksmsSpecified", false);
					model.put("dailytotalwithdrawchkemailSpecified", false);
					model.put("dailytotalpurchaseminamtSpecified", false);
					model.put("dailytotalpurchasechkemailSpecified", false);
					model.put("dailytotalpurchasechksmsSpecified", false);
					model.put("everywithdrawchksmsSpecified", false);
					model.put("everywithdrawchkemailSpecified", false);
					model.put("everypurchasechkemailSpecified", false);
					model.put("everypurchasechksmsSpecified", false);
					model.put("purchasechkemailSpecified", false);
					model.put("purchasechksmsSpecified", false);
					model.put("purchaseminamtSpecified", false);
					model.put("withdrawchkemailSpecified", false);
					model.put("withdrawchksmsSpecified", false);
					model.put("withdrawminamtSpecified", false);
					model.put("NotifyAllHour", false);
					model.put("NotifyStartHour", false);
					model.put("NotifyEndHour", false);
					model.put("ProfileFound", false);
				}
			} else {
				model.put("dailytotalwithdrawminamtSpecified", false);
				model.put("dailytotalwithdrawchksmsSpecified", false);
				model.put("dailytotalwithdrawchkemailSpecified", false);
				model.put("dailytotalpurchaseminamtSpecified", false);
				model.put("dailytotalpurchasechkemailSpecified", false);
				model.put("dailytotalpurchasechksmsSpecified", false);
				model.put("everywithdrawchksmsSpecified", false);
				model.put("everywithdrawchkemailSpecified", false);
				model.put("everypurchasechkemailSpecified", false);
				model.put("everypurchasechksmsSpecified", false);
				model.put("purchasechkemailSpecified", false);
				model.put("purchasechksmsSpecified", false);
				model.put("purchaseminamtSpecified", false);
				model.put("withdrawchkemailSpecified", false);
				model.put("withdrawchksmsSpecified", false);
				model.put("withdrawminamtSpecified", false);
				model.put("NotifyAllHour", false);
				model.put("NotifyStartHour", false);
				model.put("NotifyEndHour", false);
				model.put("ProfileFound", false);
			}
			return new ModelAndView("plasticsInfo", model);

		}

		return showForm(request, response, error, model);

	}

	@SuppressWarnings("unchecked")
	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		AccountMaskUtil maskutil = new AccountMaskUtil();

		AlertsEntitlement entitlement = null;
		entitlement = new AlertsEntitlement(
				userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS));
		Set<AlertType> customeralerts = new HashSet<>();
		if (entitlement != null && entitlement.getAlerts() != null)
			customeralerts = entitlement.getAlerts();
		if (customeralerts.contains(AlertType.ATH_EVERY_TRANSACTION))
			model.put("showEveryTransactionOption", true);
		else
			model.put("showEveryTransactionOption", false);

		PurchasesAndWithdrawalsBean bean = (PurchasesAndWithdrawalsBean) command;

		Boolean showATH = (Boolean) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.ATH_FRAME);
		if (showATH != null && showATH) {
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ATH_FRAME, null);
			Map<String, Object> modelResponse = (Map<String, Object>) WebUtils.getSessionAttribute(request,
					ChallengeInformationUtils.ATH_MODEL);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ATH_MODEL, null);
			PurchasesAndWithdrawalsBean responseBean = (PurchasesAndWithdrawalsBean) modelResponse
					.get(ChallengeInformationUtils.ATH_ACTION);
			org.apache.commons.beanutils.BeanUtils.copyProperties(command, responseBean);
			model.putAll(modelResponse);
		}

		Map<Object, Object> accountsMap = new LinkedHashMap<>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			accountsMap.put("none", "Select an account");
		else
			accountsMap.put("none", "Seleccione una cuenta");

		List<TVFrontendAccount> accounts = userSession.getAccounts();
		for (TVFrontendAccount account : accounts) {
			String suffix;
			if (account != null && account.getAccountNumberSuffix() != null)
				suffix = account.getAccountNumberSuffix();
			else
				suffix = "";
			if ((!suffix.equalsIgnoreCase("")) && suffix.equalsIgnoreCase("RES") || suffix.equalsIgnoreCase("SAV"))
				continue;

			boolean cont = false;
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE231)) {
				cont = Utils.haveType(this.alertsAccountTypes, account.getAccountProductId());
			} else {
				cont = this.alertsAccountTypes.contains(account.getAccountProductId());
			}
			if (account != null && !account.getHide() && cont) {

				/*
				 * CIBP-2898 009 Acceso Popular 015 Cuenta Popular 017 Chequera
				 * Popular 020 Avance 50 021 Flexi Cuenta 023 Multicuenta Staff
				 * 024 Popular Plus 025 Multicuenta Popular 026 IDEAL 028
				 * MaxiPremium 029 Private Management Account 049 Ahorro a Toda
				 * Hora Staff 051 Popular Savings 052 Ahorro a Toda Hora 060 ATH
				 * POP 062 ENVIA ATH 073 Acceso ETA 075 Popular Sec Investors
				 * Plus 078 Payroll Card 080 Acceso Universitario 081 e-account
				 * 082 Redi Equity 083 Redi Equity Staff 086 Bsmart 087 CIMA 089
				 * Tarjeta de Nomina
				 */

				// MBSFE-633
				// Según se indica en los criterios de aceptación, se valida
				// que se presenten unicamente tarjetas de tipo IDA
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE633)) {
					boolean withoutProdTypeRe = false; // Without Product Type
														// Restriction
					withoutProdTypeRe = Utils.haveType(this.alertsProductTypes, account.getSubtype());

					if (withoutProdTypeRe) {
						accountsMap.put(account.getFrontEndId(),
								account.getNickname() + " " + maskutil.mask(account.getAccountNumber()));
					}

				} else {
					accountsMap.put(account.getFrontEndId(),
							account.getNickname() + " " + maskutil.mask(account.getAccountNumber()));
				}
				// MBSFE-633
			}
		}
		model.put("accounts", accountsMap);

		Map<Object, Object> athsMap = new LinkedHashMap<>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			athsMap.put("none", "Select a ATH\u00AE Card");
		else
			athsMap.put("none", "Seleccione una Tarjeta ATH\u00AE");

		if (!GenericValidator.isBlankOrNull(bean.getAccount1())) {
			TVFrontendAccount account = Utils.getAccountById(bean.getAccount1(), userSession.getAccounts());

			CardPlasticFrontend[] plasticFrontendArray = userSession.getCardPlastics(account);

			if (plasticFrontendArray != null && plasticFrontendArray.length > 0) {
				for (int i = 0; i < plasticFrontendArray.length; i++) {
					String cardType;
					if (plasticFrontendArray[i].getCardPlasticAccountRecord().getCardPANId().startsWith("0215")) {
						cardType = "ATH\u00AE Regular";
					} else if (plasticFrontendArray[i].getCardPlasticAccountRecord().getCardPANId()
							.startsWith("4549")) {
						cardType = "ATH\u00AE Internacional";
					} else if (plasticFrontendArray[i].getCardPlasticAccountRecord().getCardPANId()
							.startsWith("60119052")) {// 60119052 ATH – Discover
						cardType = "ATH \u002D Discover";
					} else {
						cardType = "";
					}
					athsMap.put(plasticFrontendArray[i].getFrontEndId(), cardType + " "
							+ maskutil.mask(plasticFrontendArray[i].getCardPlasticAccountRecord().getCardPANId()));
				}
			}
		}
		if (!GenericValidator.isBlankOrNull(bean.getAthplastic1())) {
			if (!GenericValidator.isBlankOrNull(bean.getPurchase_all_chkemail())
					&& !bean.getPurchase_all_chkemail().equalsIgnoreCase("0")) {
				model.put("everypurchasechkemailSpecified", true);
			} else {
				model.put("everypurchasechkemailSpecified", false);

			}
			if (!GenericValidator.isBlankOrNull(bean.getPurchase_all_chksms())
					&& !bean.getPurchase_all_chksms().equalsIgnoreCase("0")) {
				model.put("everypurchasechksmsSpecified", true);
			} else
				model.put("everypurchasechksmsSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getDlyPurchasetotlschkemail())
					&& !bean.getDlyPurchasetotlschkemail().equalsIgnoreCase("0")) {
				model.put("dailytotalpurchasechkemailSpecified", true);
			} else
				model.put("dailytotalpurchasechkemailSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getDlyPurchasetotlschksms())
					&& !bean.getDlyPurchasetotlschksms().equalsIgnoreCase("0")) {
				model.put("dailytotalpurchasechksmsSpecified", true);
			} else
				model.put("dailytotalpurchasechksmsSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getDlyPurchasetotlsminamt())) {
				model.put("dailytotalpurchaseminamtSpecified", true);
				model.put("dailytotalpurchase_min_amt", bean.getDlyPurchasetotlsminamt());
			} else
				model.put("dailytotalpurchaseminamtSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getPurchase_thr_chkemail())
					&& !bean.getPurchase_thr_chkemail().equalsIgnoreCase("0")) {
				model.put("purchasechkemailSpecified", true);
			} else {
				model.put("purchasechkemailSpecified", false);

			}
			if (!GenericValidator.isBlankOrNull(bean.getPurchase_thr_chksms())
					&& !bean.getPurchase_thr_chksms().equalsIgnoreCase("0")) {
				model.put("purchasechksmsSpecified", true);
			} else
				model.put("purchasechksmsSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getAlertsPurchaseminamt())) {
				model.put("purchaseminamtSpecified", true);
				model.put("purchase_min_amt", bean.getAlertsPurchaseminamt());
			} else
				model.put("purchaseminamtSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getWithdraw_all_chkemail())
					&& !bean.getWithdraw_all_chkemail().equalsIgnoreCase("0")) {
				model.put("everywithdrawchkemailSpecified", true);
			} else
				model.put("everywithdrawchkemailSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getWithdraw_all_chksms())
					&& !bean.getWithdraw_all_chksms().equalsIgnoreCase("0")) {
				model.put("everywithdrawchksmsSpecified", true);
			} else
				model.put("everywithdrawchksmsSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getDlyWithdrawtotlschkemail())
					&& !bean.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0")) {
				model.put("dailytotalwithdrawchkemailSpecified", true);
			} else
				model.put("dailytotalwithdrawchkemailSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getDlyWithdrawtotlschksms())
					&& !bean.getDlyWithdrawtotlschksms().equalsIgnoreCase("0")) {
				model.put("dailytotalwithdrawchksmsSpecified", true);
			} else
				model.put("dailytotalwithdrawchksmsSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getDlyWithdrawtotlsminamt())) {
				model.put("dailytotalwithdrawminamtSpecified", true);
				model.put("dailytotalwithdraw_min_amt", bean.getDlyWithdrawtotlsminamt());
			} else
				model.put("dailytotalwithdrawminamtSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getWithdraw_thr_chkemail())
					&& !bean.getWithdraw_thr_chkemail().equalsIgnoreCase("0")) {
				model.put("withdrawchkemailSpecified", true);
			} else
				model.put("withdrawchkemailSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getWithdraw_thr_chksms())
					&& !bean.getWithdraw_thr_chksms().equalsIgnoreCase("0")) {
				model.put("withdrawchksmsSpecified", true);
			} else
				model.put("withdrawchksmsSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getAlertsWithdrawminamt())) {
				model.put("withdrawminamtSpecified", true);
				model.put("withdraw_min_amt", bean.getAlertsWithdrawminamt());
			} else
				model.put("withdrawminamtSpecified", false);

			if (!GenericValidator.isBlankOrNull(bean.getProfileNtfyallhour())
					&& !bean.getProfileNtfyallhour().equalsIgnoreCase("0")) {
				model.put("NotifyAllHour", true);
				model.put("NotifyStartHour", false);
				model.put("NotifyEndHour", false);
			} else {
				model.put("NotifyAllHour", false);
				model.put("NotifyStartHour", true);
				model.put("NotifyEndHour", true);
				model.put("NotifyStartHourValue", bean.getProfileNtfystrhour());
				model.put("NotifyEndHourValue", bean.getProfileNtfyendhour());
			}
			model.put("plastics", athsMap);
		}

		if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)) {
			model.put("mobilealert", true);
		}

		/**
		 * oob flag and oob enroll verification
		 */
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && userSession.getOobEnroll()) {

			if (userSession.getTasksListOOB() != null) {
				if (!userSession.getTasksListOOB().contains(ASK_FOR_OOB.OOB_CHALLENGE))
					userSession.getTasksListOOB().add(0, ASK_FOR_OOB.OOB_CHALLENGE);

				if (!userSession.getTasksListOOB().contains(ASK_FOR_OOB.ATH_ALERT_TYPES))
					userSession.getTasksListOOB().add(ASK_FOR_OOB.ATH_ALERT_TYPES);

				if (userSession.getTasksListOOB().contains(ASK_FOR_OOB.CCA_ALERT_TYPES))
					userSession.getTasksListOOB().remove(ASK_FOR_OOB.CCA_ALERT_TYPES);
			} else {
				List<ASK_FOR_OOB> listNavOOB = new ArrayList<>();
				listNavOOB.add(ASK_FOR_OOB.OOB_CHALLENGE);
				listNavOOB.add(ASK_FOR_OOB.ALERT_TYPES);
				listNavOOB.add(ASK_FOR_OOB.ATH_ALERT_TYPES);
				userSession.setTasksListOOB(listNavOOB);
			}
			model.put("oobEnrolled", true);
			model.put("oobChallenge", "loginoob");
			userSession.setInformationChallenge(userSession.isOobAuthenticated());
		}
		Boolean informationChallenge = userSession.isInformationChallenge();
		// CIBP-2804
		if (userSession.getGuidedSetupSession() != null && userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, true);
		} else {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, informationChallenge);
		}
		if (!informationChallenge && userSession.getInformationQuestion() != null) {
			String question = (String) userSession.getInformationQuestion().values().toArray()[0];
			model.put("question", question);
		}

		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
		boolean ccaALertsFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.CCA_ALERTS);
		PurchasesAndWithdrawalsBean a = (PurchasesAndWithdrawalsBean) command;
		
		if (ccaALertsFlag) {
			if (a.getAction() != null && a.getAction().equalsIgnoreCase("2")) {
				AlertTypesUtils.rsaChallengeForAlertController(request, command, errors, "ATH");
			}
		} else {
			UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			if (a.getAction() != null && a.getAction().equals("2")) {
				Boolean informationChallenge = session.isInformationChallenge();
				if (informationChallenge != null && !informationChallenge && (session.getGuidedSetupSession() == null
						|| !session.getGuidedSetupSession().isGuidedSetupMode())) {

					errors.addError(null);

					String language = Utils.getRSALanguage(request);
					String username = session.getUsername();

					if (session.getInformationQuestion() == null
							&& !(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)
									&& session.getOobEnroll())) {

						DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(session.getPm_fp(),
								session.getDeviceRequest().getDeviceTokenCookie(), request);
						RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
						AnalyzeResponse anResp = session.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

						deviceRequest
								.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
						deviceRequest
								.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

						session.setRsaSessionId(anResp.getIdentificationData().getSessionId());
						session.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

						session.setDeviceRequest(deviceRequest);

						if (ActionCode.CHALLENGE != anResp.getRiskResult().getTriggeredRule().getActionCode()) {
							session.setInformationChallenge(true);
						} else {
							String email = null;
							if (session.getCustomerProfile() != null)
								email = session.getCustomerProfile().getEmail();

							QuestionResponse personalQuestion;

							if (session.getInformationQuestion() == null) {
								RSAActionRequest actionRequest = new RSAActionRequest(
										Utils.generateCustomerInteractionEvent(request, session.getUsername()),
										session.getUsername(), "", email, 0, session.isRemember(),
										session.getDeviceRequest());

								if (session.getCustomerProfile() != null)
									actionRequest.setUsername(session.getCustomerProfile().getUsername());
								else if (session.getUsernameReservation() != null)
									actionRequest.setUsername(session.getUsernameReservation().getUsername());
								else
									actionRequest.setUsername(username);

								actionRequest.setDeviceRequest(session.getDeviceRequest());
								actionRequest.setRsaSessionId(session.getRsaSessionId());
								actionRequest.setRsaTransactionId(session.getRsaTransactionId());

								actionRequest.setRsaLang(Utils.getRSALanguage(request));

								personalQuestion = session.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

							} else {
								personalQuestion = new QuestionResponse();
								personalQuestion.setStatusResult(ResultStatus.SUCCESS);
								personalQuestion.setQuestion(session.getInformationQuestion());
							}
							if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
								session.setInformationQuestion(personalQuestion.getQuestion());

							} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
								throw new ModelAndViewDefiningException(new ModelAndView("error"));
							} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
								throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
							}
						}

					}
					
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
					Map<String, Object> parameters = new HashMap<>();
					parameters.put(ChallengeInformationUtils.ATH_ACTION, command);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
							ChallengeInformationUtils.ATH_ACTION);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS,
							parameters);
				}
			}
		}
	}

	public String getAlertsAccountTypes() {
		return alertsAccountTypes;
	}

	public void setAlertsAccountTypes(String alertsAccountTypes) {
		this.alertsAccountTypes = alertsAccountTypes;
	}

	// MBSFE-633
	public String getAlertsProductTypes() {
		return alertsProductTypes;
	}

	public void setAlertsProductTypes(String alertsProductTypes) {
		this.alertsProductTypes = alertsProductTypes;
	}
	// END MBSFE-633

}
