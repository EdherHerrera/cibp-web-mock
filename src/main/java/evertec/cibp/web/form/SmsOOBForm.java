package evertec.cibp.web.form;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.EventType;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.SMSEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.SMSProfileModifyRequest;
import evertec.cibp.core.services.requests.SendSmsGeneratedCodeRequest;
import evertec.cibp.core.services.requests.SmsEnrollUserRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.SmsAlertsResponse;
import evertec.cibp.core.services.responses.SmsAlertsResponse.Result;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.ChangeOOBBean;
import evertec.cibp.web.form.bean.SmsOOBBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SmsOOBForm extends AbstractWizardFormController {
	/** Logger */
	protected final Log log = LogFactory.getLog(getClass());
	private static final String USERSESSION = "userSession";
	private static final String SMSOOB = "smsOOB";
	private static final String PAGESMSOOB = "pageSmsOOB";
	private static final String SMSEXITO = "smsExito";
	private static final String SMSPHONERECEIVEOOB = "smsPhonereceivedOOB";
	private static final String SMSPROVIDEROOB = "smsProviderOOB";
	private static final String SMSGENERATEDCODEOOB = "smsGeneratedCodeOOB";
	private static final String PROVIDER = "provider";

	public SmsOOBForm() {
		super();
		setCommandClass(SmsOOBBean.class);
		setCommandName(SMSOOB);
		setPages(new String[] { SMSOOB, SMSOOB });
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		SmsOOBBean infobean = (SmsOOBBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		request.getSession().removeAttribute(PAGESMSOOB);
		userSession.setStatusmessage("");

		String profileID = "\nProfileID: ";
		String nCard = "\ncard: ";
		String errorMsgLog = "Error Updating SMS OOB Phone # while editing Cel #: calling ATH SmsInfo \nacct: ";
		String errorMsgLogUpdate = "Error Updating SMS OOB Phone # while editing Cel #: calling ATH smsUpdate \nacct: ";
		String errorUnable = "Unable to enroll in Mi Banco Text at this time, please try later.";
		String errorProblemas = "Problemas al inscribir en Mi Banco Text en este momento, favor intentar luego.";

		if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			request.getSession().removeAttribute(SMSEXITO);
		}

		SmsOOBBean.ACTION action = infobean.getAction();
		
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);
		Cookie cookie = new Cookie("PMData", deviceRequest.getDeviceTokenCookie());
		cookie.setPath("/");
		cookie.setMaxAge(31536000); 
		ESAPI.httpUtilities().addCookie(response, cookie);


		if (action == SmsOOBBean.ACTION.SMS_CODE_VER) {
			String smscode = infobean.getSmscode();
			String phone = (String) WebUtils.getSessionAttribute(request, SMSPHONERECEIVEOOB);
			String provider = (String) WebUtils.getSessionAttribute(request, SMSPROVIDEROOB);
			String generatedCode = (String) WebUtils.getSessionAttribute(request, SMSGENERATEDCODEOOB);
			if (smscode.equalsIgnoreCase(generatedCode)) {
				if (customer.getEntitlementByType(EntitlementType.MOBILE_BANKING) != null
						&& userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)) != null) {
					// means Customer is enrolled, must modify phone number
					SMSEntitlement ent = new SMSEntitlement(
							customer.getEntitlementByType(EntitlementType.MOBILE_BANKING));
					try {
						SMSProfileModifyRequest modreq = new SMSProfileModifyRequest(
								Utils.generateCustomerInteractionEvent(request), customer,
								userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)), ent, phone,
								provider);
						customerService.modifySMSUserProfile(modreq);
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession.setStatusmessage("Your Mi Banco Text phone number has been modified.");
						else
							userSession.setStatusmessage(
									"Su n&uacute;mero de tel&eacute;fono Mi Banco Text ha sido modificado.");

						WebUtils.setSessionAttribute(request, "smsUpdateSuccess", true);

						/* CIBP-1104 */
						boolean hasalert = false;
						AlertsEntitlement alertEnt = new AlertsEntitlement(
								customer.getEntitlementByType(EntitlementType.ALERTS));
						if (alertEnt != null && alertEnt.getAlerts() != null) {
							Set<AlertType> alerts = alertEnt.getAlerts();
							if (alerts.contains(AlertType.MOBILE_PHONE_EDIT_EMAIL) || alerts.contains(AlertType.MOBILE_PHONE_EDIT_SMS)
									|| alerts.contains(AlertType.MOBILE_PHONE_EDIT_PUSH)) {
								hasalert = true;
							}
						}
						/*
						 * CIBP-1104 se activa envio alerta de cambio de
						 * telefono banca movil
						 */
						if (hasalert) {
							customerService.sendMobilePhoneEditConfirmation(customer,
									userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
									userSession.getPreferredLanguage());
						}
						/* FIN--- CIBP-1104 */

						/*
						 * MBSD-1171 Send a email always the customer edit the
						 * primary phone on OOB.
						 */
						boolean emailSent = customerService.sendOobUpdateEmail(customer,
								userSession.getPreferredLanguage(), ChangeOOBBean.ACTION.EDIT_PRIMARY_PHONE.name());
						if (!emailSent) {
							this.log.error(SmsOOBForm.class.getName() + ":"
									+ "Could not send oob update frequency confirmation email to "
									+ customer.getUsername() + " (" + customer.getEmail() + ")");
						}
						/* FIN--- MBSD-1171 */

						model.put("phoneUpdated", true);

						if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
							request.getSession().setAttribute(SMSEXITO, true);
						}

						AccountMaskUtil maskutil = new AccountMaskUtil();
						List<TVFrontendAccount> accounts = userSession.getAccounts();
						List<evertec.cibp.core.services.smsAlerts.SmsBean> smsBeans = new LinkedList<>();
						for (TVFrontendAccount account : accounts) {
							String suffix;
							if (account != null && account.getAccountNumberSuffix() != null)
								suffix = account.getAccountNumberSuffix();
							else
								suffix = "";
							if ((!suffix.equalsIgnoreCase("")) && suffix.equalsIgnoreCase("RES")
									|| suffix.equalsIgnoreCase("SAV"))
								continue;
							if (account != null && !account.getHide()
									&& (account.getAccountProductId().equalsIgnoreCase("009")// Acceso
																								// Popular
											|| account.getAccountProductId().equalsIgnoreCase("015")// Cuenta
																									// Popular
											|| account.getAccountProductId().equalsIgnoreCase("017")// Chequera
																									// Popular
											|| account.getAccountProductId().equalsIgnoreCase("020")// Avance
																									// 50
											|| account.getAccountProductId().equalsIgnoreCase("023")// Multicuenta
																									// Staff
											|| account.getAccountProductId().equalsIgnoreCase("024")// Popular
																									// Plus
											|| account.getAccountProductId().equalsIgnoreCase("025")// Multicuenta
																									// Popular
											|| account.getAccountProductId().equalsIgnoreCase("026")// IDEAL
											|| account.getAccountProductId().equalsIgnoreCase("028")// MaxiPremium
											|| account.getAccountProductId().equalsIgnoreCase("029")// Private
																									// Management
																									// Account
											|| account.getAccountProductId().equalsIgnoreCase("049")// Ahorro
																									// a
																									// Toda
																									// Hora
																									// Staff
											|| account.getAccountProductId().equalsIgnoreCase("051")// Popular
																									// Savings
											|| account.getAccountProductId().equalsIgnoreCase("052")// Ahorro
																									// a
																									// Toda
																									// Hora
											|| account.getAccountProductId().equalsIgnoreCase("060")// ATH
																									// POP
											|| account.getAccountProductId().equalsIgnoreCase("062")// ENVIA
																									// ATH
											|| account.getAccountProductId().equalsIgnoreCase("073")// Acceso
																									// ETA
											|| account.getAccountProductId().equalsIgnoreCase("075")// Popular
																									// Sec
																									// Investors
																									// Plus
											|| account.getAccountProductId().equalsIgnoreCase("078")// Payroll
																									// Card
											|| account.getAccountProductId().equalsIgnoreCase("080")// Acceso
																									// Universitario
											|| account.getAccountProductId().equalsIgnoreCase("081")// e-account
											|| account.getAccountProductId().equalsIgnoreCase("082")// Redi
																									// Equity
											|| account.getAccountProductId().equalsIgnoreCase("083")// Redi
																									// Equity
																									// Staff
											|| account.getAccountProductId().equalsIgnoreCase("087")// CIMA
											|| account.getAccountProductId().equalsIgnoreCase("089")))// Tarjeta
																										// de
																										// Nomina
							{

								try {

									CardPlasticFrontend[] plastics = userSession.getCardPlastics(account);

									if (plastics.length > 0 && !(account.getCurrentAthSmsAlerts() != null
											&& account.getCurrentAthSmsAlerts().size() > 0)) {
										HashMap<Object, Object> smsInfo = new HashMap<>();

										for (int i = 0; i < plastics.length; i++) {
											evertec.cibp.core.services.smsAlerts.SmsBean smsBeanRequest = new evertec.cibp.core.services.smsAlerts.SmsBean();
											smsBeanRequest.setAcctNum(account.getAccountNumber());
											smsBeanRequest.setTaxId(customer.getTaxId());
											smsBeanRequest.setCardNum(
													plastics[i].getCardPlasticAccountRecord().getCardPANId());

											SmsAlertsResponse smsAlertsResponse = customerService.getSmsAlertInfo(
													smsBeanRequest, Utils.generateCustomerInteractionEvent(request));
											if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
													&& smsAlertsResponse.getResponseSmsBean() != null) {
												smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
														smsAlertsResponse.getResponseSmsBean());
												smsBeans.add(smsAlertsResponse.getResponseSmsBean());
											} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
													&& smsAlertsResponse.getResponseSmsBean() == null) {
												smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
														null);
												if (!smsAlertsResponse.isAuditable())
													log.error(
															"Error Updating SMS OOB Phone # while editing Cel #: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
																	+ maskutil.mask(account.getAccountNumber()) + nCard
																	+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
																	+ customer.getFullName() + profileID
																	+ customer.getProfileId()
																	+ smsAlertsResponse.getAuditString());
												else
													log.error(
															"Error Updating SMS OOB Phone # while editing Cel #: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
																	+ maskutil.mask(account.getAccountNumber()) + nCard
																	+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
																	+ customer.getFullName() + profileID
																	+ customer.getProfileId());
											} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
													&& smsAlertsResponse.getErrorString()
															.contains("ERROR: SmsInfo NOT SENT.")) {
												smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
														null);
												if (!smsAlertsResponse.isAuditable())
													log.error(
															"Error Updating SMS OOB Phone # while editing Cel #: Unable to send ATH SmsInfo request for \nacct: "
																	+ maskutil.mask(account.getAccountNumber()) + nCard
																	+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
																	+ customer.getFullName() + profileID
																	+ customer.getProfileId());
												else
													log.error(
															"Error Updating SMS OOB Phone # while editing Cel #: Unable to send ATH SmsInfo request for \nacct: "
																	+ maskutil.mask(account.getAccountNumber()) + nCard
																	+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
																	+ customer.getFullName() + profileID
																	+ customer.getProfileId() + "\n"
																	+ smsAlertsResponse.getAuditString());
											} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
													&& (smsAlertsResponse.getException() != null
															|| !GenericValidator.isBlankOrNull(
																	smsAlertsResponse.getSmsAlertsException()))) {
												smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
														null);
												if (!smsAlertsResponse.isAuditable()
														&& !GenericValidator.isBlankOrNull(
																smsAlertsResponse.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null)
													log.error(errorMsgLog + maskutil.mask(account.getAccountNumber())
															+ nCard + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + "\n"
															+ smsAlertsResponse.getSmsAlertsException() + "\n"
															+ smsAlertsResponse.getException());
												else if (!smsAlertsResponse.isAuditable()
														&& !GenericValidator.isBlankOrNull(
																smsAlertsResponse.getSmsAlertsException())
														&& smsAlertsResponse.getException() == null) {
													log.error(errorMsgLog + maskutil.mask(account.getAccountNumber())
															+ nCard + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + "\n"
															+ smsAlertsResponse.getSmsAlertsException());
												} else if (!smsAlertsResponse.isAuditable()
														&& GenericValidator.isBlankOrNull(
																smsAlertsResponse.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null) {
													log.error(errorMsgLog + maskutil.mask(account.getAccountNumber())
															+ nCard + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + "\n"
															+ smsAlertsResponse.getException());
												} else if (smsAlertsResponse.isAuditable()
														&& !GenericValidator.isBlankOrNull(
																smsAlertsResponse.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null)
													log.error(errorMsgLog + maskutil.mask(account.getAccountNumber())
															+ nCard + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + "\n"
															+ smsAlertsResponse.getSmsAlertsException() + "\n"
															+ smsAlertsResponse.getAuditString());
												else {
													log.error(errorMsgLog + maskutil.mask(account.getAccountNumber())
															+ nCard + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + "\n"
															+ smsAlertsResponse.getAuditString());
												}
											} else {
												smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
														null);
												log.error(errorMsgLog + maskutil.mask(account.getAccountNumber())
														+ nCard + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
														+ customer.getFullName() + profileID + customer.getProfileId()
														+ "\n");
											}
										}
										if (smsInfo.size() > 0)
											account.setCurrentAthSmsAlerts(smsInfo);
									}

									if (plastics.length > 0 && account.getCurrentAthSmsAlerts() != null
											&& account.getCurrentAthSmsAlerts().size() > 0) {
										for (int i = 0; i < plastics.length; i++) {
											if (account.getCurrentAthSmsAlerts().containsKey(
													plastics[i].getCardPlasticAccountRecord().getCardPANId())
													&& (account.getCurrentAthSmsAlerts().get(plastics[i]
															.getCardPlasticAccountRecord().getCardPANId()) != null)) {
												smsBeans.add((evertec.cibp.core.services.smsAlerts.SmsBean) account
														.getCurrentAthSmsAlerts()
														.get(plastics[i].getCardPlasticAccountRecord().getCardPANId()));
											}
										}
									}

									List<evertec.cibp.core.services.smsAlerts.SmsBean> forUpdating = new LinkedList<>();

									if (smsBeans != null && !smsBeans.isEmpty()) {
										for (evertec.cibp.core.services.smsAlerts.SmsBean sms : smsBeans) {
											sms.setPrimaryphone(phone);
											sms.setPrimaryPhoneCarrier(provider);
											forUpdating.add(sms);
										}
									}

									if (forUpdating != null && !forUpdating.isEmpty()) {

										for (evertec.cibp.core.services.smsAlerts.SmsBean sms : forUpdating) {
											evertec.cibp.core.services.smsAlerts.SmsBean newBean = new evertec.cibp.core.services.smsAlerts.SmsBean();

											org.apache.commons.beanutils.BeanUtils.copyProperties(newBean, sms);

											SmsAlertsResponse smsAlertsResponse = customerService.updateSmsAlert(
													newBean, Utils.generateCustomerInteractionEvent(request));
											if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
													&& smsAlertsResponse.getResponseSmsBean() != null) {
												account.getCurrentAthSmsAlerts().put(sms.getCardNum(),
														smsAlertsResponse.getResponseSmsBean());
											} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
													&& smsAlertsResponse.getResponseSmsBean() == null) {
												if (!smsAlertsResponse.isAuditable())
													log.error(
															"Error Updating SMS OOB Phone # while editing Cel #: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
																	+ maskutil.mask(account.getAccountNumber()) + nCard
																	+ maskutil.mask(sms.getCardNum()) + "\n"
																	+ customer.getFullName() + profileID
																	+ customer.getProfileId()
																	+ smsAlertsResponse.getAuditString());
												else
													log.error(
															"Error Updating SMS OOB Phone # while editing Cel #: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
																	+ maskutil.mask(account.getAccountNumber()) + nCard
																	+ maskutil.mask(sms.getCardNum()) + "\n"
																	+ customer.getFullName() + profileID
																	+ customer.getProfileId());
											} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
													&& smsAlertsResponse.getErrorString()
															.contains("ERROR: SmsInfo NOT SENT.")) {
												if (!smsAlertsResponse.isAuditable())
													log.error(
															"Error Updating SMS OOB Phone # while editing Cel #: Unable to send ATH smsUpdate request for \nacct: "
																	+ maskutil.mask(account.getAccountNumber()) + nCard
																	+ maskutil.mask(sms.getCardNum()) + "\n"
																	+ customer.getFullName() + profileID
																	+ customer.getProfileId());
												else
													log.error(
															"Error Updating SMS OOB Phone # while editing Cel #: Unable to send ATH smsUpdate request for \nacct: "
																	+ maskutil.mask(account.getAccountNumber()) + nCard
																	+ maskutil.mask(sms.getCardNum()) + "\n"
																	+ customer.getFullName() + profileID
																	+ customer.getProfileId() + "\n"
																	+ smsAlertsResponse.getAuditString());
											} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
													&& (smsAlertsResponse.getException() != null
															|| !GenericValidator.isBlankOrNull(
																	smsAlertsResponse.getSmsAlertsException()))) {
												if (!smsAlertsResponse.isAuditable()
														&& !GenericValidator.isBlankOrNull(
																smsAlertsResponse.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null)
													log.error(errorMsgLogUpdate
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(sms.getCardNum()) + "\n"
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + "\n"
															+ smsAlertsResponse.getSmsAlertsException() + "\n"
															+ smsAlertsResponse.getException());
												else if (!smsAlertsResponse.isAuditable()
														&& !GenericValidator.isBlankOrNull(
																smsAlertsResponse.getSmsAlertsException())
														&& smsAlertsResponse.getException() == null) {
													log.error(errorMsgLogUpdate
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(sms.getCardNum()) + "\n"
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + "\n"
															+ smsAlertsResponse.getSmsAlertsException());
												} else if (!smsAlertsResponse.isAuditable()
														&& GenericValidator.isBlankOrNull(
																smsAlertsResponse.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null) {
													log.error(errorMsgLogUpdate
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(sms.getCardNum()) + "\n"
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + "\n"
															+ smsAlertsResponse.getException());
												} else if (smsAlertsResponse.isAuditable()
														&& !GenericValidator.isBlankOrNull(
																smsAlertsResponse.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null)
													log.error(errorMsgLogUpdate
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(sms.getCardNum()) + "\n"
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + "\n"
															+ smsAlertsResponse.getSmsAlertsException() + "\n"
															+ smsAlertsResponse.getAuditString());
												else {
													log.error(errorMsgLogUpdate
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(sms.getCardNum()) + "\n"
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + "\n"
															+ smsAlertsResponse.getAuditString());
												}
											} else {

												if (!smsAlertsResponse.isAuditable())
													log.error(errorMsgLogUpdate
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(sms.getCardNum()) + "\n"
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + "\n");
												else {
													log.error(errorMsgLogUpdate
															+ maskutil.mask(account.getAccountNumber()) + nCard
															+ maskutil.mask(sms.getCardNum()) + "\n"
															+ customer.getFullName() + profileID
															+ customer.getProfileId() + "\n"
															+ smsAlertsResponse.getAuditString());
												}
											}

										}

									}

								} catch (Exception e) {
									log.error("Error Updating SMS OOB Phone # while editing Cel #: \nacct: "
											+ maskutil.mask(account.getAccountNumber()) + "\n" + profileID
											+ customer.getProfileId(), e);
								}

							}
						}

						return new ModelAndView(SMSOOB, model);

					} catch (Exception e) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession
									.setStatusmessage("Unable to edit your phone number this time, please try later.");
						else
							userSession.setStatusmessage(
									"Problemas al cambiar su n&uacute;mero de tel&eacute;fono de Mi Banco Text en este momento, favor intentar luego.");
						model.put("unableSMSEnroll", true);
						return new ModelAndView(SMSOOB, model);
					}
				} else {
					try {

						SmsEnrollUserRequest smsenr = new SmsEnrollUserRequest(
								Utils.generateCustomerInteractionEvent(request), customer, phone, provider);
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD2267)) {
							smsenr.setEnrollmentSource("CIBP");
						}						
						ResultStatus status = customerService.smsEnrollUser(smsenr);
						if (status == ResultStatus.SUCCESS) {
							CustomerInteractionEvent generateCustomerInteractionEvent = Utils
									.generateCustomerInteractionEvent(request);
							SMSProfile smsprofile = userSession.getSmsProfile(generateCustomerInteractionEvent);
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								customerService.sendSMSWelcome(customer, smsprofile, PreferredLanguage.ENGLISH);
							} else {
								customerService.sendSMSWelcome(customer, smsprofile, PreferredLanguage.SPANISH);
							}
							model.put("smsSuccessEnroll", true);

							if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
								request.getSession().setAttribute(SMSEXITO, true);
							}

							return new ModelAndView(SMSOOB, model);

						} else {
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								userSession.setStatusmessage(errorUnable);
							else
								userSession.setStatusmessage(errorProblemas);
							model.put("unableSMSEnroll", true);
							return new ModelAndView(SMSOOB, model);
						}
					} catch (Exception e) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession.setStatusmessage(errorUnable);
						else
							userSession.setStatusmessage(errorProblemas);

					}

				}
			} else {
				error.rejectValue("smscode", "sms.code.invalid", "The code entered is not valid");
				return new ModelAndView(SMSOOB, model);
			}
		} else {
			request.getSession().setAttribute(PAGESMSOOB, 1);
		}
		return new ModelAndView(SMSOOB, model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		SmsOOBBean infobean = (SmsOOBBean) command;

		List<ASK_FOR_OOB> listNavOOB = userSession.getTasksListOOB();
		if (listNavOOB != null && listNavOOB.contains(ASK_FOR_OOB.EDIT_PRIMARY_PHONE)) {
			userSession.getTasksListOOB().remove(ASK_FOR_OOB.EDIT_PRIMARY_PHONE);
			request.setAttribute("firstTime", false);
		}

		if (userSession.getGuidedSetupSession() != null && userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, true);
		} else {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, userSession.isInformationChallenge());
		}

		if (request.getSession().getAttribute(PAGESMSOOB) != null) {
			page = Integer.parseInt(request.getSession().getAttribute(PAGESMSOOB).toString());
			request.getSession().setAttribute(SMSOOB, infobean);
		}
		request.getSession().setAttribute(PAGESMSOOB, page);

		model.put("page", Integer.toString(page));
		model.put("customer", userSession.getCustomerProfile());
		String enrollSMS = "EnrollSMS";
		if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
			try {
				CustomerInteractionEvent generateCustomerInteractionEvent = Utils
						.generateCustomerInteractionEvent(request);
				SMSProfile smsprofile = userSession.getSmsProfile(generateCustomerInteractionEvent);
				if (!smsprofile.getCtns().isEmpty()) {
					model.put(enrollSMS, true);
					model.put("smsPhone", ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn());
				} else
					model.put(enrollSMS, false);

			} catch (Exception e) {
				log.error(e.getMessage());
			}

		} else
			model.put(enrollSMS, false);
		/** **** Reference Data for SMS ENROLL ****** */
		Map<String, Object> smsproviders = new LinkedHashMap<>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			smsproviders.put("none", "Select a provider");
		else
			smsproviders.put("none", "Selecciona un proveedor");

		smsproviders.put("PR1", "OPEN");
		smsproviders.put("PR7", "CLARO");
		smsproviders.put("PR6", "CENTENNIAL");
		smsproviders.put("US3", "AT&T");
		smsproviders.put("US4", "T-MOBILE");
		smsproviders.put("US5", "SPRINT");

		model.put("smsproviders", smsproviders);

		/*
		 * CIBP-1857 BLAS ALONSO GARCIA RUIZ SE AGREGA VARIABLE EN MODELO PARA
		 * CONTROLAR EL CHEQUEO DE TERMINOS Y CONDICIONES
		 */
		if (infobean.getAction() == null)
			model.put("agreeval", "");
		else
			model.put("agreeval", "checked");

		String msgerr = (String) WebUtils.getSessionAttribute(request, "msgerr");
		if (msgerr != null) {
			errors.rejectValue(PROVIDER, "", msgerr);
			return model;
		}

		if (userSession.getInformationQuestion() != null) {
			String question = (String) userSession.getInformationQuestion().values().toArray()[0];
			model.put("question", question);
		}

		Boolean showMobile = (Boolean) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.MOBILE_FRAME);
		String phone = (String) WebUtils.getSessionAttribute(request, SMSPHONERECEIVEOOB);
		if (showMobile != null && showMobile && phone != null) {
			page = 1;
			model.put("page", 1);
			request.getSession().setAttribute(PAGESMSOOB, 1);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.MOBILE_FRAME, null);
			return model;
		}

		return model;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		SmsOOBBean infobean = (SmsOOBBean) command;
		SmsOOBBean.ACTION action = infobean.getAction();

		String delete = request.getParameter("d");

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();

		String errorUnable = "Unable to enroll in Mi Banco Text at this time, please try later.";
		String errorProblemas = "Problemas al inscribir en Mi Banco Text en este momento, favor intentar luego.";

		if (delete != null) {
			Boolean informationChallenge = userSession.isInformationChallenge();
			if (!informationChallenge) {
				if (userSession.getInformationQuestion() == null) {
					this.getRSAChallenge(request, userSession);
				}
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
						ChallengeInformationUtils.MOBILE_DELETE);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.MOBILE_DELETE, true);
				errors.rejectValue("", "", "");
			}
		}

		else {

			if (request.getSession().getAttribute(PAGESMSOOB) != null) {
				page = Integer.parseInt(request.getSession().getAttribute(PAGESMSOOB).toString());
				request.getSession().setAttribute(SMSOOB, infobean);
			}

			if (page == 0) {
				if (action == SmsOOBBean.ACTION.SMS_ENROLL) {
					boolean er = false;
					if (infobean.getProvider() == null || infobean.getProvider().equals("none")) {
						errors.rejectValue(PROVIDER, "sms.provider.invalid", "Please select a provider");
						er = true;
					}
					if (infobean.getPhone1() == null || infobean.getPhone1().equals("")
							|| !infobean.getPhone1().matches("\\d{3}") || infobean.getPhone2() == null
							|| infobean.getPhone2().equals("") || !infobean.getPhone2().matches("\\d{3}")
							|| infobean.getPhone3() == null || infobean.getPhone3().equals("")
							|| !infobean.getPhone3().matches("\\d{4}")) {
						errors.rejectValue("phone3", "sms.phone.invalid", "Please enter a correct phone number");
						er = true;
					}
					String phone = infobean.getPhone1() + infobean.getPhone2() + infobean.getPhone3();
					if (!errors.getAllErrors().isEmpty()) {
						Boolean informationChallenge = userSession.isInformationChallenge();
						if (!informationChallenge && (userSession.getGuidedSetupSession() == null
								|| !userSession.getGuidedSetupSession().isGuidedSetupMode())) {
							if (userSession.getInformationQuestion() == null) {
								this.getRSAChallenge(request, userSession);
							}
							er = true;
							errors.rejectValue("", "", "");
							WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
							Map<String, Object> parameters = new HashMap<>();
							parameters.put(ChallengeInformationUtils.PROVIDER, infobean.getProvider());
							parameters.put(ChallengeInformationUtils.PHONE, phone);
							WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
									ChallengeInformationUtils.MOBILE_FRAME);
							WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS,
									parameters);
						}
					}

					request.getSession().removeAttribute(PAGESMSOOB);
					if (!er) {
						Calendar cal = Calendar.getInstance();
						long mil = cal.getTimeInMillis();
						String code = String.valueOf(mil);
						code = code.substring(code.length() - 4, code.length());
						WebUtils.setSessionAttribute(request, SMSGENERATEDCODEOOB, code);
						WebUtils.setSessionAttribute(request, SMSPHONERECEIVEOOB, phone);
						WebUtils.setSessionAttribute(request, SMSPROVIDEROOB, infobean.getProvider());
						String msgerr = "";
						try {
							SendSmsGeneratedCodeRequest codereq = new SendSmsGeneratedCodeRequest(
									Utils.generateCustomerInteractionEvent(request), customer, code, phone,
									infobean.getProvider());
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								customerService.sendGeneratedCode(codereq, PreferredLanguage.ENGLISH);
							else
								customerService.sendGeneratedCode(codereq, PreferredLanguage.SPANISH);
							return;
						} catch (Exception e) {
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
								msgerr = errorUnable;
							else
								msgerr = errorProblemas;
							errors.rejectValue(PROVIDER, "", msgerr);
							return;
						}
					}
				}

			} // end if page == 0

			else if (page == 1 && action == SmsOOBBean.ACTION.SMS_CODE_VER) {
				String generatedCode = (String) WebUtils.getSessionAttribute(request, SMSGENERATEDCODEOOB);

				if (infobean.getSmscode() == null || infobean.getSmscode().equals("")
						|| !infobean.getSmscode().matches("\\d{4}")
						|| !infobean.getSmscode().equalsIgnoreCase(generatedCode))
					errors.rejectValue("smscode", "sms.code.invalid", "The code entered is not valid");

			}
		}
	}

	private void getRSAChallenge(HttpServletRequest request, UserSession userSession) throws Exception {

		String language = Utils.getRSALanguage(request);
		String username = userSession.getUsername();

		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);
		RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
		AnalyzeResponse anResp = userSession.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

		deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
		deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

		userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
		userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

		userSession.setDeviceRequest(deviceRequest);

		if (ActionCode.CHALLENGE != anResp.getRiskResult().getTriggeredRule().getActionCode()) {
			userSession.setInformationChallenge(true);
		} else {
			String email = null;
			if (userSession.getCustomerProfile() != null)
				email = userSession.getCustomerProfile().getEmail();

			QuestionResponse personalQuestion;

			if (userSession.getInformationQuestion() == null) {
				RSAActionRequest actionRequest = new RSAActionRequest(
						Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
						userSession.getUsername(), "", email, 0, userSession.isRemember(),
						userSession.getDeviceRequest());

				if (userSession.getCustomerProfile() != null)
					actionRequest.setUsername(userSession.getCustomerProfile().getUsername());
				else if (userSession.getUsernameReservation() != null)
					actionRequest.setUsername(userSession.getUsernameReservation().getUsername());
				else
					actionRequest.setUsername(username);

				actionRequest.setDeviceRequest(userSession.getDeviceRequest());
				actionRequest.setRsaSessionId(userSession.getRsaSessionId());
				actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());

				actionRequest.setRsaLang(Utils.getRSALanguage(request));

				personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

			} else {
				personalQuestion = new QuestionResponse();
				personalQuestion.setStatusResult(ResultStatus.SUCCESS);
				personalQuestion.setQuestion(userSession.getInformationQuestion());
			}
			if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
				userSession.setInformationQuestion(personalQuestion.getQuestion());

			} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
				throw new ModelAndViewDefiningException(new ModelAndView("error"));
			} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
				throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
			}
		}
	}
	

}
