package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rsa.csd.ws.EventType;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;

import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.PushNotificationInteraction;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyCustomerAlertsRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.PersonalInformationChangesBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

public class PersonalInformationChangesForm extends CancellableFormController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(PersonalInformationChangesForm.class);
	public boolean isSubmit = false;
	private static final String USERSESSION = "userSession";

	/** Logger */

	public PersonalInformationChangesForm() {
		super();
		setCommandClass(PersonalInformationChangesBean.class);
		setCommandName("personalinformationchanges");
		setFormView("personalinformationchanges");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request, userSession.getUsername());
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();
		AlertsEntitlement entitlement = null;
		AlertType pushCategory = null;
		boolean pushAdded = false;
		
		boolean isEntitlementChanged = false;
		String viewName = "text";

		/*
		 * Se verifica si el profile tiene el Entitlement de ALERTS , en caso de
		 * no tenerlo se le agrega REF JIRA CIBP-1777
		 */
		if (!profile.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement alert = new AlertsEntitlement();
			customerService.getCustomerProfileService().addEntitlement(profile, alert.getCustomerEntitlement());
		}
		// if(profile.hasEntitlement(EntitlementType.ALERTS))
		entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		Set<AlertType> customeralerts = new HashSet<AlertType>();
		if (entitlement != null && entitlement.getAlerts() != null)
			customeralerts = entitlement.getAlerts();

		ModifyCustomerAlertsRequest req = new ModifyCustomerAlertsRequest();
		req.setCustomerProfile(userSession.getCustomerProfile());
		req.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
		
		if (!userSession.getUserAgentType().equals(UserAgentType.IPHONE) && !userSession.getUserAgentType().equals(UserAgentType.ANDROID)) {
			//OOB : verify if come from OOB Challenge
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && userSession.getOobEnroll() && userSession.isOobAuthenticated()) {
				//get the information that changes if exists.
				@SuppressWarnings("unchecked")
				Map<String, Object> parameters = (Map<String, Object>) WebUtils.getSessionAttribute(request,
						ChallengeInformationUtils.INFORMATION_PARAMETERS);
				if(parameters != null && parameters.get(ChallengeInformationUtils.PERSONAL_INFORMATION_CHANGES_ACTION) != null) {
					PersonalInformationChangesBean responseBean = (PersonalInformationChangesBean) parameters.get(ChallengeInformationUtils.PERSONAL_INFORMATION_CHANGES_ACTION);
					org.apache.commons.beanutils.BeanUtils.copyProperties(command, responseBean);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.SHOW_UPDATE_ALERT, userSession.isOobAuthenticated());
				}
			}
		} else {
			viewName = "mobileAlertsJson";
			model.put("success", false);
		}

		PersonalInformationChangesBean a = (PersonalInformationChangesBean) command;
		/* CIBP-1104 */
		if (a.isPasswordChangeMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.CHANGE_PASS_SMS);
				// Remove Push, only one of these may be active
				if (customeralerts.contains(AlertType.CHANGE_PASS_PUSH)) {
					customeralerts.remove(AlertType.CHANGE_PASS_PUSH);
					pushCategory = AlertType.CHANGE_PASS_PUSH;
				}

			} else {
				if (customeralerts.contains(AlertType.CHANGE_PASS_SMS))
					customeralerts.remove(AlertType.CHANGE_PASS_SMS);
			}
			isEntitlementChanged = true;
		}

		else if (a.isBlockedPasswordEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.ACCESS_BLOCK_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.ACCESS_BLOCK_EMAIL))
					customeralerts.remove(AlertType.ACCESS_BLOCK_EMAIL);
			}
			isEntitlementChanged = true;
		} else if (a.isBlockedPasswordMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.ACCESS_BLOCK_SMS);
				// Remove Push, only one of these may be active
				if (customeralerts.contains(AlertType.ACCESS_BLOCK_PUSH)) {
					customeralerts.remove(AlertType.ACCESS_BLOCK_PUSH);
					pushCategory = AlertType.ACCESS_BLOCK_PUSH;
				}

			} else {
				if (customeralerts.contains(AlertType.ACCESS_BLOCK_SMS))
					customeralerts.remove(AlertType.ACCESS_BLOCK_SMS);
			}
			isEntitlementChanged = true;
		}

		else if (a.isBlockedSecretQuestionsEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.QUESTIONS_BLOCK_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL))
					customeralerts.remove(AlertType.QUESTIONS_BLOCK_EMAIL);
			}
			isEntitlementChanged = true;
		} else if (a.isBlockedSecretQuestionsMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.QUESTIONS_BLOCK_SMS);
				// Remove Push, only one of these may be active
				if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
					customeralerts.remove(AlertType.QUESTIONS_BLOCK_PUSH);
					pushCategory = AlertType.QUESTIONS_BLOCK_PUSH;
				}

			} else {
				if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_SMS))
					customeralerts.remove(AlertType.QUESTIONS_BLOCK_SMS);
			}
			isEntitlementChanged = true;
		} else if (a.isSecretQuestionsUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.QUESTIONS_EDIT_SMS);
				// Remove Push, only one of these may be active
				if (customeralerts.contains(AlertType.QUESTIONS_EDIT_PUSH)) {
					customeralerts.remove(AlertType.QUESTIONS_EDIT_PUSH);
					pushCategory = AlertType.QUESTIONS_EDIT_PUSH;
				}

			} else {
				if (customeralerts.contains(AlertType.QUESTIONS_EDIT_SMS))
					customeralerts.remove(AlertType.QUESTIONS_EDIT_SMS);
			}
			isEntitlementChanged = true;
		} else if (a.isEmailUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.EMAIL_EDIT_SMS);
				// Remove Push, only one of these may be active
				if (customeralerts.contains(AlertType.EMAIL_EDIT_PUSH)) {
					customeralerts.remove(AlertType.EMAIL_EDIT_PUSH);
					pushCategory = AlertType.EMAIL_EDIT_PUSH;
				}

			} else {
				if (customeralerts.contains(AlertType.EMAIL_EDIT_SMS))
					customeralerts.remove(AlertType.EMAIL_EDIT_SMS);
			}
			isEntitlementChanged = true;
		}

		else if (a.isPhoneNumberUpdatedEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PHONE_EDIT_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.PHONE_EDIT_EMAIL))
					customeralerts.remove(AlertType.PHONE_EDIT_EMAIL);
			}
			isEntitlementChanged = true;
		} else if (a.isPhoneNumberUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PHONE_EDIT_SMS);
				// Remove Push, only one of these may be active
				if (customeralerts.contains(AlertType.PHONE_EDIT_PUSH)) {
					customeralerts.remove(AlertType.PHONE_EDIT_PUSH);
					pushCategory = AlertType.PHONE_EDIT_PUSH;
				}
			} else {
				if (customeralerts.contains(AlertType.PHONE_EDIT_SMS))
					customeralerts.remove(AlertType.PHONE_EDIT_SMS);
			}
			isEntitlementChanged = true;
		}

		else if (a.isAddressUpdatedEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.ADRESS_EDIT_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.ADRESS_EDIT_EMAIL))
					customeralerts.remove(AlertType.ADRESS_EDIT_EMAIL);
			}
			isEntitlementChanged = true;
		} else if (a.isAddressUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.ADRESS_EDIT_SMS);
				// Remove Push, only one of these may be active
				if (customeralerts.contains(AlertType.ADRESS_EDIT_PUSH)) {
					customeralerts.remove(AlertType.ADRESS_EDIT_PUSH);
					pushCategory = AlertType.ADRESS_EDIT_PUSH;
				}
			} else {
				if (customeralerts.contains(AlertType.ADRESS_EDIT_SMS))
					customeralerts.remove(AlertType.ADRESS_EDIT_SMS);
			}
			isEntitlementChanged = true;
		}

		else if (a.isMobileBankingPhoneNumberUpdatedEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.MOBILE_PHONE_EDIT_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_EMAIL))
					customeralerts.remove(AlertType.MOBILE_PHONE_EDIT_EMAIL);
			}
			isEntitlementChanged = true;
		} else if (a.isMobileBankingPhoneNumberUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.MOBILE_PHONE_EDIT_SMS);
				// Remove Push, only one of these may be active
				if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_PUSH)) {
					customeralerts.remove(AlertType.MOBILE_PHONE_EDIT_PUSH);
					pushCategory = AlertType.MOBILE_PHONE_EDIT_PUSH;
				}
			} else {
				if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_SMS))
					customeralerts.remove(AlertType.MOBILE_PHONE_EDIT_SMS);
			}
			isEntitlementChanged = true;
		}
		else if (a.isNewDeviceEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.NEW_DEVICE_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.NEW_DEVICE_EMAIL))
					customeralerts.remove(AlertType.NEW_DEVICE_EMAIL);
			}
			isEntitlementChanged = true;
		} else if (a.isNewDeviceMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.NEW_DEVICE_SMS);
				// Remove Push, only one of these may be active
				if (customeralerts.contains(AlertType.NEW_DEVICE_PUSH)) {
					customeralerts.remove(AlertType.NEW_DEVICE_PUSH);
					pushCategory = AlertType.NEW_DEVICE_PUSH;
				}
			} else {
				if (customeralerts.contains(AlertType.NEW_DEVICE_SMS))
					customeralerts.remove(AlertType.NEW_DEVICE_SMS);
			}
			isEntitlementChanged = true;
		} else if (a.isNewFingerprintMobile()) {
            boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
            if (al) {
                  customeralerts.add(AlertType.NEW_FINGERPRINT_SMS);
  				// Remove Push, only one of these may be active
  				if (customeralerts.contains(AlertType.NEW_FINGERPRINT_PUSH)) {
  					customeralerts.remove(AlertType.NEW_FINGERPRINT_PUSH);
  					pushCategory = AlertType.NEW_FINGERPRINT_PUSH;
  				}
            } else {
                  if (customeralerts.contains(AlertType.NEW_FINGERPRINT_SMS))
                         customeralerts.remove(AlertType.NEW_FINGERPRINT_SMS);
            }
			isEntitlementChanged = true;
		}
		
		// Push Notifications
		else if (a.isPasswordChangePush()) {
			pushAdded = a.getAction().equalsIgnoreCase("true") ? true : false;
			pushCategory = AlertType.CHANGE_PASS_PUSH;
			if (pushAdded) {
				customeralerts.add(AlertType.CHANGE_PASS_PUSH);
				// Remove SMS, only one of these may be active
				if (customeralerts.contains(AlertType.CHANGE_PASS_SMS))
					customeralerts.remove(AlertType.CHANGE_PASS_SMS);
			} else {
				if (customeralerts.contains(AlertType.CHANGE_PASS_PUSH))
					customeralerts.remove(AlertType.CHANGE_PASS_PUSH);
			}
			isEntitlementChanged = true;
		}
		else if (a.isBlockedPasswordPush()) {
			pushAdded = a.getAction().equalsIgnoreCase("true") ? true : false;
			pushCategory = AlertType.ACCESS_BLOCK_PUSH;
			if (pushAdded) {
				customeralerts.add(AlertType.ACCESS_BLOCK_PUSH);
				// Remove SMS, only one of these may be active
				if (customeralerts.contains(AlertType.ACCESS_BLOCK_SMS))
					customeralerts.remove(AlertType.ACCESS_BLOCK_SMS);
			} else {
				if (customeralerts.contains(AlertType.ACCESS_BLOCK_PUSH))
					customeralerts.remove(AlertType.ACCESS_BLOCK_PUSH);
			}
			isEntitlementChanged = true;
		}
		else if (a.isBlockedSecretQuestionsPush()) {
			pushAdded = a.getAction().equalsIgnoreCase("true") ? true : false;
			pushCategory = AlertType.QUESTIONS_BLOCK_PUSH;
			if (pushAdded) {
				customeralerts.add(AlertType.QUESTIONS_BLOCK_PUSH);
				// Remove SMS, only one of these may be active
				if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_SMS))
					customeralerts.remove(AlertType.QUESTIONS_BLOCK_SMS);
			} else {
				if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_PUSH))
					customeralerts.remove(AlertType.QUESTIONS_BLOCK_PUSH);
			}
			isEntitlementChanged = true;
		}
		else if (a.isSecretQuestionsUpdatedPush()) {
			pushAdded = a.getAction().equalsIgnoreCase("true") ? true : false;
			pushCategory = AlertType.QUESTIONS_EDIT_PUSH;
			if (pushAdded) {
				customeralerts.add(AlertType.QUESTIONS_EDIT_PUSH);
				// Remove SMS, only one of these may be active
				if (customeralerts.contains(AlertType.QUESTIONS_EDIT_SMS))
					customeralerts.remove(AlertType.QUESTIONS_EDIT_SMS);
			} else {
				if (customeralerts.contains(AlertType.QUESTIONS_EDIT_PUSH))
					customeralerts.remove(AlertType.QUESTIONS_EDIT_PUSH);
			}
			isEntitlementChanged = true;
		}
		else if (a.isEmailUpdatedPush()) {
			pushAdded = a.getAction().equalsIgnoreCase("true") ? true : false;
			pushCategory = AlertType.EMAIL_EDIT_PUSH;
			if (pushAdded) {
				customeralerts.add(AlertType.EMAIL_EDIT_PUSH);
				// Remove SMS, only one of these may be active
				if (customeralerts.contains(AlertType.EMAIL_EDIT_SMS))
					customeralerts.remove(AlertType.EMAIL_EDIT_SMS);
			} else {
				if (customeralerts.contains(AlertType.EMAIL_EDIT_PUSH))
					customeralerts.remove(AlertType.EMAIL_EDIT_PUSH);
			}
			isEntitlementChanged = true;
		}
		else if (a.isPhoneNumberUpdatedPush()) {
			pushAdded = a.getAction().equalsIgnoreCase("true") ? true : false;
			pushCategory = AlertType.PHONE_EDIT_PUSH;
			if (pushAdded) {
				customeralerts.add(AlertType.PHONE_EDIT_PUSH);
				// Remove SMS, only one of these may be active
				if (customeralerts.contains(AlertType.PHONE_EDIT_SMS))
					customeralerts.remove(AlertType.PHONE_EDIT_SMS);
			} else {
				if (customeralerts.contains(AlertType.PHONE_EDIT_PUSH))
					customeralerts.remove(AlertType.PHONE_EDIT_PUSH);
			}
			isEntitlementChanged = true;
		}
		else if (a.isAddressUpdatedPush()) {
			pushAdded = a.getAction().equalsIgnoreCase("true") ? true : false;
			pushCategory = AlertType.ADRESS_EDIT_PUSH;
			if (pushAdded) {
				customeralerts.add(AlertType.ADRESS_EDIT_PUSH);
				// Remove SMS, only one of these may be active
				if (customeralerts.contains(AlertType.ADRESS_EDIT_SMS))
					customeralerts.remove(AlertType.ADRESS_EDIT_SMS);
			} else {
				if (customeralerts.contains(AlertType.ADRESS_EDIT_PUSH))
					customeralerts.remove(AlertType.ADRESS_EDIT_PUSH);
			}
			isEntitlementChanged = true;
		}
		else if (a.isMobileBankingPhoneNumberUpdatedPush()) {
			pushAdded = a.getAction().equalsIgnoreCase("true") ? true : false;
			pushCategory = AlertType.MOBILE_PHONE_EDIT_PUSH;
			if (pushAdded) {
				customeralerts.add(AlertType.MOBILE_PHONE_EDIT_PUSH);
				// Remove SMS, only one of these may be active
				if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_SMS))
					customeralerts.remove(AlertType.MOBILE_PHONE_EDIT_SMS);
			} else {
				if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_PUSH))
					customeralerts.remove(AlertType.MOBILE_PHONE_EDIT_PUSH);
			}
			isEntitlementChanged = true;
		}
		else if (a.isNewDevicePush()) {
			pushAdded = a.getAction().equalsIgnoreCase("true") ? true : false;
			pushCategory = AlertType.NEW_DEVICE_PUSH;
			if (pushAdded) {
				customeralerts.add(AlertType.NEW_DEVICE_PUSH);
				// Remove SMS, only one of these may be active
				if (customeralerts.contains(AlertType.NEW_DEVICE_SMS))
					customeralerts.remove(AlertType.NEW_DEVICE_SMS);
			} else {
				if (customeralerts.contains(AlertType.NEW_DEVICE_PUSH))
					customeralerts.remove(AlertType.NEW_DEVICE_PUSH);
			}
			isEntitlementChanged = true;
		}
		else if (a.isNewFingerprintPush()) {
			pushAdded = a.getAction().equalsIgnoreCase("true") ? true : false;
			pushCategory = AlertType.NEW_FINGERPRINT_PUSH;
			if (pushAdded) {
				customeralerts.add(AlertType.NEW_FINGERPRINT_PUSH);
				// Remove SMS, only one of these may be active
				if (customeralerts.contains(AlertType.NEW_FINGERPRINT_SMS))
					customeralerts.remove(AlertType.NEW_FINGERPRINT_SMS);
			} else {
				if (customeralerts.contains(AlertType.NEW_FINGERPRINT_PUSH))
					customeralerts.remove(AlertType.NEW_FINGERPRINT_PUSH);
			}
			isEntitlementChanged = true;
		}
		
		if (isEntitlementChanged) {
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				model.put("success", true);
				this.logPushNotificationEvent(pushCategory, pushAdded, true, customerService, interactionEvent);
				return new ModelAndView(viewName, model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e);
				model.put("text", false);
				this.logPushNotificationEvent(pushCategory, pushAdded, false, customerService, interactionEvent);
				return new ModelAndView(viewName, model);

			}
		}
		
		model.put("text", false);
		
		// MBSD-2006 RSA Upgrade Recommendations - Personal Information Changes (Update cookie)
		Cookie cookie = WebUtils.getCookie(request, "PMData") != null ? WebUtils.getCookie(request, "PMData") : new Cookie("PMData", "");
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		ESAPI.httpUtilities().addCookie(response, cookie);

		
		return new ModelAndView(viewName, model);


	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		CustomerProfile profile = userSession.getCustomerProfile();

		AlertsEntitlement entitlement = null;
		if (profile.hasEntitlement(EntitlementType.ALERTS))
			entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		Set<AlertType> customeralerts = new HashSet<AlertType>();
		if (entitlement != null && entitlement.getAlerts() != null)
			customeralerts = entitlement.getAlerts();

		if (profile.hasEntitlement(EntitlementType.ALERTS))
			entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		if (customeralerts.contains(AlertType.CHANGE_PASS_EMAIL))
			model.put("chaPassemail", true);

		if (customeralerts.contains(AlertType.CHANGE_PASS_SMS))
			model.put("chaPasssms", true);

		if (customeralerts.contains(AlertType.ACCESS_BLOCK_EMAIL))
			model.put("accBlockemail", true);

		if (customeralerts.contains(AlertType.ACCESS_BLOCK_SMS))
			model.put("accBlocksms", true);

		if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL))
			model.put("qstBlockemail", true);

		if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_SMS))
			model.put("qstBlocksms", true);

		if (customeralerts.contains(AlertType.QUESTIONS_EDIT_EMAIL))
			model.put("qstEditemail", true);

		if (customeralerts.contains(AlertType.QUESTIONS_EDIT_SMS))
			model.put("qstEditsms", true);

		if (customeralerts.contains(AlertType.EMAIL_EDIT_EMAIL))
			model.put("emailEditemail", true);

		if (customeralerts.contains(AlertType.EMAIL_EDIT_SMS))
			model.put("emailEditsms", true);

		if (customeralerts.contains(AlertType.PHONE_EDIT_EMAIL))
			model.put("phoneEditemail", true);

		if (customeralerts.contains(AlertType.PHONE_EDIT_SMS))
			model.put("phoneEditsms", true);

		if (customeralerts.contains(AlertType.ADRESS_EDIT_EMAIL))
			model.put("adressEditemail", true);

		if (customeralerts.contains(AlertType.ADRESS_EDIT_SMS))
			model.put("adressEditsms", true);

		if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_EMAIL))
			model.put("mphoneEditemail", true);

		if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_SMS))
			model.put("mphoneEditsms", true);

		if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)) {
			model.put("mobilealert", true);
		}

		if (customeralerts.contains(AlertType.NEW_DEVICE_EMAIL))
			model.put("newDeviceemail", true);

		if (customeralerts.contains(AlertType.NEW_DEVICE_SMS))
			model.put("newDevicesms", true);
		
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT315)) {
			model.put("MBMT315", true);
			
			if (customeralerts.contains(AlertType.NEW_FINGERPRINT_EMAIL))
				model.put("newFingerprintemail", true);
			
			if (customeralerts.contains(AlertType.NEW_FINGERPRINT_SMS))
				model.put("newFingerprintsms", true);
			
		}

		/**
		 * oob flag and oob enroll verification 
		 * */		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && userSession.getOobEnroll()) {
			
			if(userSession.getTasksListOOB() != null) {
				if(userSession.getTasksListOOB() != null && !userSession.getTasksListOOB().contains(ASK_FOR_OOB.OOB_CHALLENGE)) {
					userSession.getTasksListOOB().add(0,ASK_FOR_OOB.OOB_CHALLENGE);
				}
				if(userSession.getTasksListOOB() != null && !userSession.getTasksListOOB().contains(ASK_FOR_OOB.PERSONAL_INFORMATION_CHANGES)) {
					userSession.getTasksListOOB().add(ASK_FOR_OOB.PERSONAL_INFORMATION_CHANGES);
				}
			} else {
				List<ASK_FOR_OOB> listNavOOB = new ArrayList<ASK_FOR_OOB>();
				listNavOOB.add(ASK_FOR_OOB.OOB_CHALLENGE);
				listNavOOB.add(ASK_FOR_OOB.ALERT_TYPES);
				listNavOOB.add(ASK_FOR_OOB.PERSONAL_INFORMATION_CHANGES);
				userSession.setTasksListOOB(listNavOOB);
			}
			model.put("oobEnrolled", true);
			model.put("oobChallenge", "loginoob");
			userSession.setInformationChallenge(userSession.isOobAuthenticated());
		}
		
		
		if (userSession.getGuidedSetupSession() != null && userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, true);
		} else {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, userSession.isInformationChallenge());
		}

		Boolean showUpdate = (Boolean) WebUtils.getSessionAttribute(request,
				ChallengeInformationUtils.SHOW_UPDATE_ALERT);

		if (showUpdate != null && showUpdate) {
			model.put("showUpdate", true);
		} else {
			model.put("showUpdate", false);
		}

		if (userSession.getInformationQuestion() != null) {
			String question = (String) userSession.getInformationQuestion().values().toArray()[0];
			model.put("question", question);
		}

		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.SHOW_UPDATE_ALERT, null);

		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		Boolean informationChallenge = userSession.isInformationChallenge();
		
		if (userSession.getUserAgentType().equals(UserAgentType.IPHONE) || userSession.getUserAgentType().equals(UserAgentType.ANDROID))
			return;

		if (informationChallenge != null
				&& !informationChallenge
				&& (userSession.getGuidedSetupSession() == null || !userSession.getGuidedSetupSession()
						.isGuidedSetupMode())) {

			String language = Utils.getRSALanguage(request);
			String username = userSession.getUsername();

			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && userSession.getOobEnroll()) {
				informationChallenge = userSession.isOobAuthenticated();
				userSession.setOobChallenge(true);
			
			} else if (userSession.getInformationQuestion() == null) {

				if (informationChallenge != null && !informationChallenge) {

					DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), request);

					RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
					AnalyzeResponse anResp = userSession.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

					deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
					deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

					userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
					userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

					userSession.setDeviceRequest(deviceRequest);

					if (ActionCode.CHALLENGE != anResp.getRiskResult().getTriggeredRule().getActionCode()) {
						userSession.setInformationChallenge(true);
					} else {
						String email = null;
						if (userSession.getCustomerProfile() != null)
							email = userSession.getCustomerProfile().getEmail();

						QuestionResponse personalQuestion;

						if (userSession.getInformationQuestion() == null) {
							RSAActionRequest actionRequest = new RSAActionRequest(
									Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
									userSession.getUsername(), "", email, 0, userSession.isRemember(),
									userSession.getDeviceRequest());

							if (userSession.getCustomerProfile() != null)
								actionRequest.setUsername(userSession.getCustomerProfile().getUsername());
							else if (userSession.getUsernameReservation() != null)
								actionRequest.setUsername(userSession.getUsernameReservation().getUsername());
							else
								actionRequest.setUsername(username);

							actionRequest.setDeviceRequest(userSession.getDeviceRequest());
							actionRequest.setRsaSessionId(userSession.getRsaSessionId());
							actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());

							actionRequest.setRsaLang(Utils.getRSALanguage(request));

							personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

							// MBSD-2006: RSA Upgrade Recommendations - Personal Information Changes (Update device request)
							deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request);
							deviceRequest.setDeviceTokenCookie(personalQuestion.getDeviceRequest().getDeviceTokenCookie());
							deviceRequest.setDeviceTokenFSO(personalQuestion.getDeviceRequest().getDeviceTokenFSO());
							userSession.setDeviceRequest(deviceRequest);
							userSession.setRsaSessionId(personalQuestion.getSessionId());
							userSession.setRsaTransactionId(personalQuestion.getRsaTransactionId());
							
						} else {
							personalQuestion = new QuestionResponse();
							personalQuestion.setStatusResult(ResultStatus.SUCCESS);
							personalQuestion.setQuestion(userSession.getInformationQuestion());
						}
						if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
							userSession.setInformationQuestion(personalQuestion.getQuestion());

						} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
							throw new ModelAndViewDefiningException(new ModelAndView("error"));
						} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
							throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
						}
					}
				}
			}

			errors.rejectValue("", "", "");
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.PERSONAL_INFORMATION_CHANGES_FRAME, true);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
			Map<String, Object> parameters = new HashMap<>();
			parameters.put(ChallengeInformationUtils.PERSONAL_INFORMATION_CHANGES_ACTION, command);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
					ChallengeInformationUtils.PERSONAL_INFORMATION_CHANGES_ACTION);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, parameters);
		}
	}
	
	private void logPushNotificationEvent(AlertType pushCategory, Boolean pushAdded, Boolean success, CustomerServices service, CustomerInteractionEvent event) {
		if (pushCategory != null) {
			event.setCustomStringData(PushNotificationInteraction.CATEGORY, pushCategory.toString());
			if (pushAdded)
				service.logPushNotificationAddEvent(event, success);
			else 
				service.logPushNotificationDeleteEvent(event, success);
		}
	}

}
