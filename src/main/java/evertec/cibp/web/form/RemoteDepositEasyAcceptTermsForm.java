package evertec.cibp.web.form;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.RDCEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.form.bean.RemoteDepositEasyAcceptTermsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.enums.ResponseStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RemoteDepositEasyAcceptTermsForm extends CancellableFormController {

	protected final Log logger = LogFactory.getLog(getClass());
	private CustomerServices customerService;

	private MessageSource messageSource;

	public RemoteDepositEasyAcceptTermsForm() {
		super();
		setCommandClass(RemoteDepositEasyAcceptTermsBean.class);
		setFormView("remoteDepositEasyAcceptTerms");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object submitDeposit,
			BindException error) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		CustomerProfile customerProfile = userSession.getCustomerProfile();
		try {

			if (customerProfile.getEntitlementByType(EntitlementType.RDC) != null) {
				Date today = new Date();
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
				RDCEntitlement ent = new RDCEntitlement(
						customerProfile.getEntitlementByType(EntitlementType.RDC));
                   ent.setRdcEasyDepositAcceptedTerms(true);
                   ent.setRdcDateEasyDepositAcceptedTerms(df.format(today));
				customerService.updateRDCEntitlementCustomerId(customerProfile, ent);
                model.put("status", ResponseStatus.SUCCESS);
                model.put("error", "");
                /// MBC 1840
                userSession.getCustomerService().acceptTermsConditionsAction(Utils.generateCustomerInteractionEvent(request));
                if (customerProfile.getEmail() != null && !customerProfile.getEmail().trim().equals(""))
        			userSession.getCustomerService().sendEmailAcceptedTermsConditionsRDC(customerProfile, userSession.getPreferredLanguage());
			}

		} catch (Exception ex) {
			logger.error(
					"Error Deposity Easy Accept terms"
							+ customerProfile.getFullName() + "\nProfileID: "
							+ customerProfile.getProfileId() + "\n");
			model.put("error", "Error Deposity Easy Accept terms");
            model.put("status", ResponseStatus.ERROR);
		}

		return showForm(request, error, "remoteDepositEasyAcceptTerms", model);
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.RDC;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}


	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}


}
