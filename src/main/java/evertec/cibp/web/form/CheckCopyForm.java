package evertec.cibp.web.form;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.facade.AccountsFacade;
import evertec.cibp.web.form.bean.AccountServiceBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;
import evertec.dp.models.xsd.photocopy.PhotocopyResponse_Type;

public class CheckCopyForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public CheckCopyForm() {
		super();
		setCommandClass(AccountServiceBean.class);
		setCommandName("accountservices");
		setFormView("checkcopy");
		setBindOnNewForm(true);
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		AccountServiceBean searchBean = (AccountServiceBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();

		TVFrontendAccount account = Utils.getAccountById(searchBean.getAccount(), userSession.getAccounts());
		Calendar checkdate = Calendar.getInstance();
		checkdate.setTime(searchBean.getCheckDate());
		// CheckPhotocopyRequest chkrequest = new
		// CheckPhotocopyRequest(Utils.generateCustomerInteractionEvent(request),
		// customer, userSession.getBankingSession(), account,
		// searchBean.getCheckAmount(), searchBean.getCheckNumber(), checkdate);
		// CheckPhotocopyResponse chkres =
		// customerService.requestCheckPhotocopy(chkrequest);

		
		/**
		 * MBNA-2411
		 * Method getCheckCopy : CIBP-WEB microservice integration
		 * 
		 */
		//Microservice implementation
		PhotocopyResponse_Type resp = AccountsFacade.getInstance()
													.getCheckCopy(request,
																  account.getAccountNumber(),
																  searchBean.getCheckAmount(), 
																  searchBean.getCheckNumber(), 
																  checkdate, 
																  account.getBankId());

		if (Integer.parseInt(resp.getStatusCode()) != 0) {
			model.put("error", true);
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				// userSession.setStatusmessage("Unable to process your request of a check copy at this time.");
				model.put("message", "Unable to process your request of a check copy at this time.");
			} else {
				// userSession.setStatusmessage("Su solicitud de copia de cheque no se pudo procesar en este momento.");
				model.put("message", "Su solicitud de copia de cheque no se pudo procesar en este momento.");
			}
		} else {

			String ref = "";
			if (resp.getRefId() != null)
				ref = resp.getRefId();

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				if (ref != null && !"".equals(ref))
					ref = "Reference: " + ref;
				// userSession.setStatusmessage("Your request of check copy was send. "
				// + ref);
				model.put("message", "Your request of check copy was send. " + ref);
			} else {
				if (ref != null && !"".equals(ref))
					ref = "Referencia: " + ref;
				// userSession.setStatusmessage("Su solicitud de copia de cheque fue enviada exitosamente. "
				// + ref);
				model.put("message", "Su solicitud de copia de cheque fue enviada exitosamente. " + ref);
			}
			model.put("ref", ref);
		}

		return showForm(request, error, "checkcopy", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		AccountMaskUtil mask = new AccountMaskUtil();

		/****** Reference Data for Request Checks Photocopies service *******/
		Map<String, String> chkCpAccounts = new LinkedHashMap<String, String>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			chkCpAccounts.put("none", "Select an account");
		else
			chkCpAccounts.put("none", "Selecciona una cuenta");
		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount name = (TVFrontendAccount) iterator.next();
			if (name.getFeatures().isCheckCopy() && !name.getHide())

				chkCpAccounts
						.put(String.valueOf(name.getFrontEndId()),
								name.getNickname()
										+ " "
										+ mask.mask(name.getAccountNumber())
										+ ((name.getAccountNumberSuffix() != null && !name.getAccountNumberSuffix()
												.equals("")) ? " " + name.getAccountNumberSuffix() : ""));
		}
		model.put("copyChecksAccounts", chkCpAccounts);

		// Set up today's date
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Calendar n = Calendar.getInstance();

		if (Calendar.SATURDAY == n.get(Calendar.DAY_OF_WEEK))
			n.add(Calendar.DAY_OF_WEEK, 2);
		else if (Calendar.SUNDAY == n.get(Calendar.DAY_OF_WEEK))
			n.add(Calendar.DAY_OF_WEEK, 1);

		model.put("todayDate", df.format(n.getTime()));
		df = new SimpleDateFormat("MM");
		model.put("month", df.format(n.getTime()));
		df = new SimpleDateFormat("dd");
		model.put("day", df.format(n.getTime()));

		Map<String, String> agree = new HashMap<String, String>();
		agree.put("agree", "");
		model.put("agree", agree);

		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		AccountServiceBean searchBean = (AccountServiceBean) command;

		if (searchBean.getAccount() == null || searchBean.getAccount().equals("none")) {
			errors.rejectValue("account", "accountservices.acct.invalid", "Invalid Account Selection");
			searchBean.setFormError(true);
		}
		if (searchBean.getCheckAmount() == null) {
			errors.rejectValue("checkAmount", "accountservices.checkamt.invalid", "Please enter a check amount");
			searchBean.setFormError(true);
		}
		if (searchBean.getCheckDate() == null) {
			errors.rejectValue("checkDate", "accountservices.checkdate.invalid", "Please enter a check date");
			searchBean.setFormError(true);
		}
		if (GenericValidator.isBlankOrNull(searchBean.getAgreement())
				|| !searchBean.getAgreement().equalsIgnoreCase("agree")) {
			errors.rejectValue("agreement", "accountservices.agreement.error", "Please agree.");
			searchBean.setFormError(true);
		}
		if (searchBean.getCheckNumber() == null || searchBean.getCheckNumber().equals("")) {
			errors.rejectValue("checkNumber", "accountservices.checknumber.invalid", "Please enter the check number.");
			searchBean.setFormError(true);
		}

	}

}
