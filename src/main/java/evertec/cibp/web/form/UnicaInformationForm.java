package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.documents.CIBPConfigurableTextsTypes;
import evertec.cibp.core.models.documents.ConfigurableTextSubSections;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.form.bean.UnicaApplication;
import evertec.cibp.web.form.bean.UnicaFormBean;
import evertec.cibp.web.session.UserSession;

public class UnicaInformationForm extends AbstractWizardFormController implements MessageSourceAware {

	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;
	private CustomerServices customerService;
	private String unicaOfferMinSurplusQty;

	public UnicaInformationForm() {
		super();
		setCommandClass(UnicaFormBean.class);
		setCommandName("unicainformation");
		setPages(new String[] { "unicainformation" });
	}

	protected boolean isFormSubmission(HttpServletRequest request) {
		return (request.getParameter("account") != null);

	}

	/**
	 * Method to get the object that represents a specific application
	 * 
	 * @param applicationId
	 *            The application ID
	 * @param appList
	 *            The applications list
	 * @return The application object
	 */
	private UnicaApplication getApplication(String applicationId, List<UnicaApplication> appList) {
		UnicaApplication unicaApp = null;

		if (!GenericValidator.isBlankOrNull(applicationId) && appList.size() > 0) {
			// get specific application from list
			for (UnicaApplication app : appList) {
				if (app.getUnicaApplicationInfo().getApplicationId().equals(applicationId)) {
					unicaApp = app;
					break;
				}
			}
		}
		return unicaApp;
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String applicationId = request.getParameter("account");
		UnicaApplication unicaApp = this.getApplication(applicationId, userSession.getUnicaApplications());

		if (unicaApp != null) {
			if (unicaApp.getUnicaApplicationInfo().getInsertCodes() != null
					&& unicaApp.getUnicaApplicationInfo().getInsertCodes().length > 0) {

				@SuppressWarnings("unchecked")
				List<String> nextSteps = new LinkedList<String>();

				for (int i = 0; i < unicaApp.getUnicaApplicationInfo().getInsertCodes().length; i++) {
					String insertCode = unicaApp.getUnicaApplicationInfo().getInsertCodes()[i].trim().toUpperCase();
					String insertCodeText = userSession.getDBConfiguredText(CIBPConfigurableTextsTypes.UNICA,
							ConfigurableTextSubSections.INSERTCODES, insertCode);
					if (!GenericValidator.isBlankOrNull(insertCodeText)) {
						nextSteps.add(insertCodeText);
					}
				}
				if (nextSteps.size() > 0)
					model.put("nextSteps", nextSteps);
			}
			model.put("offerAvailable", showOfferSection(unicaApp, userSession, model));
		}

		model.put("unicaApp", unicaApp);
		return model;
	}

	/**
	 * Method to determine if the Offer section should be displayed
	 * 
	 * @param unicaApp
	 *            The UnicaApplicationInfo object
	 * @param userSession
	 *            The userSession object
	 * @return True if the offer section should be displayed, false otherwise.
	 */
	private boolean showOfferSection(UnicaApplication unicaApp, UserSession userSession, Map<String, Object> model) {
		boolean showOffer = false;
		if (!unicaApp.isCredit()) {
			double surplus = 0.0;
			double minSurplusNeeded = 500.00; // Default quantity
			if (!GenericValidator.isBlankOrNull(unicaApp.getUnicaApplicationInfo().getApprovedAmount())
					&& !GenericValidator.isBlankOrNull(unicaApp.getUnicaApplicationInfo().getRqstAmount())) {
				double approvedAmount = Double.parseDouble(unicaApp.getUnicaApplicationInfo().getApprovedAmount());
				double requestedAmount = Double.parseDouble(unicaApp.getUnicaApplicationInfo().getRqstAmount());
				surplus = approvedAmount - requestedAmount;
			}

			// convert property from cibp.properties into double value
			if (!GenericValidator.isBlankOrNull(unicaOfferMinSurplusQty)) {
				try {
					minSurplusNeeded = Double.parseDouble(unicaOfferMinSurplusQty);
				} catch (NumberFormatException e) {
					minSurplusNeeded = 500.00;
				}
			}

			if (surplus >= minSurplusNeeded)
				showOffer = !(userSession.isUnicaOfferAccepted(unicaApp.getUnicaApplicationInfo().getApplicationId()));
		}
		
		if(showOffer){
			String offerFedbox = "";
			String offerMessage = "";
			String offerCountry = "PR";
			ConfigurableTextSubSections subSection = ConfigurableTextSubSections.OFFER_PR;
			if(userSession.hasVirginIslandAccount()){
				offerCountry = "VI";
				subSection = ConfigurableTextSubSections.OFFER_VI;
			}else if(userSession.hasBritishVirginIslandAccount()){
				offerCountry = "BVI";
				subSection = ConfigurableTextSubSections.OFFER_BVI;
			}
			
			offerMessage = userSession.getDBConfiguredText(CIBPConfigurableTextsTypes.UNICA,
					subSection, "MESSAGE_"+offerCountry);
			offerFedbox = userSession.getDBConfiguredText(CIBPConfigurableTextsTypes.UNICA,
					subSection, "FDBX_LINK_"+offerCountry);
			
			model.put("offerMessage",offerMessage);
			model.put("offerFedBox", offerFedbox);
		}
		return showOffer;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerServices) {
		this.customerService = customerServices;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * @return the unicaOfferMinSurplusQty
	 */
	public String getUnicaOfferMinSurplusQty() {
		return unicaOfferMinSurplusQty;
	}

	/**
	 * @param unicaOfferMinSurplusQty
	 *            the unicaOfferMinSurplusQty to set
	 */
	public void setUnicaOfferMinSurplusQty(String unicaOfferMinSurplusQty) {
		this.unicaOfferMinSurplusQty = unicaOfferMinSurplusQty;
	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2,
			BindException arg3) throws Exception {
		return null;
	}
}
