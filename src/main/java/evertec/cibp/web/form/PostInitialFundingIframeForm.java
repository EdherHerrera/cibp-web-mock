package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.InternationalCharacterUtils;
import evertec.cibp.web.form.bean.PostInitialFundingIframeBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

public class PostInitialFundingIframeForm extends SimpleFormController {

	protected final Log logger = LogFactory.getLog(getClass());
	private CustomerServices customerService;

	public PostInitialFundingIframeForm() {
		super();
		setCommandClass(PostInitialFundingIframeBean.class);
		setCommandName("pif");
		setFormView("postinitialfundingiframe");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String taxId = userSession.getCustomerProfile().getTaxId();

		PostInitialFundingIframeBean bean = (PostInitialFundingIframeBean) command;
		String routingNumber = bean.getRoutingNumber();
		String accountNumber = bean.getAccountNumber();
		char accountType = bean.getAccountType().charAt(0);

		String nickname = customerService.getBankName(routingNumber);
		if (nickname.equals(""))
			nickname = "EXT ACCOUNT";

		// Delete the original account
		boolean deleted = customerService.deletePIFExternalAccount(bean.getOriginalAccountId());

		if (deleted) {
			// Add the new account
			// Verify that the small deposits sent have not reached the max
			String accountId = taxId + routingNumber + accountNumber;
			if (customerService.pifSmallDepositsLimitReached(accountId)) {

				customerService.addBlock5PIFAccount(routingNumber, accountNumber, taxId, nickname, accountType);

				// Send the email
				CustomerProfile profile = userSession.getCustomerProfile();
				PIFExternalAccount account = customerService.getPifExternalAccount(accountId);
				String bankAccount = "";
				if (account.getAccountNumber().length() > 4)
					bankAccount = "x" + account.getAccountNumber().substring(account.getAccountNumber().length() - 4);
				else
					bankAccount = "x" + account.getAccountNumber();
				customerService.sendPIFBlock5Confirmation(profile, bankAccount, account.getBankName(),
						userSession.getPreferredLanguage());
			} else {
				if (!customerService.addExternalAccount(routingNumber, accountNumber, taxId, nickname, accountType)) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("You already have registered this account.");
					else
						userSession.setStatusmessage("Ya tienes esta cuenta registrada.");
				} else {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession
								.setStatusmessage("Verification deposits sent, check your other Bank account statement.");
					else
						userSession
								.setStatusmessage("Dep&oacute;sitos de verificaci&oacute;n enviados, revisa tu estado de cuenta del otro Banco.");
				}
			}
			model.put("completeprocess", true);
			return showForm(request, response, errors, model);
		} else {

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("The account could not be modified.");
			else
				userSession.setStatusmessage("La cuenta no pude ser modificada.");

			model.put("completeprocess", true);
			return showForm(request, response, errors, model);
		}
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		PostInitialFundingIframeBean bean = (PostInitialFundingIframeBean) command;
		String id = (String) request.getParameter("id");

		if (id != null) {

			id = InternationalCharacterUtils.sanatizeInputs(id, false);
			PIFExternalAccount account = customerService.getPifExternalAccount(id);

			model.put("originalAccountId", id);
			model.put("originalAccountNumber", account.getAccountNumber());
		} else {
			model.put("originalAccountId", bean.getOriginalAccountId());
			model.put("originalAccountNumber", bean.getAccountNumber());
		}

		return model;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		boolean err = false;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		PostInitialFundingIframeBean bean = (PostInitialFundingIframeBean) command;
		String routingNumber = bean.getRoutingNumber();
		String accountNumber = bean.getAccountNumber();
		String confirmAccountNumber = bean.getConfirmAccountNumber();
		String taxId = userSession.getCustomerProfile().getTaxId();
		String accountType = bean.getAccountType();

		// 9 digits for routing number
		if (GenericValidator.isBlankOrNull(routingNumber)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("routingNumber", "", "Required");
				err = true;
			} else {
				errors.rejectValue("routingNumber", "", "Requerido");
				err = true;
			}
		} else if (!GenericValidator.isInt(routingNumber) || routingNumber.length() != 9) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("routingNumber", "", "Invalid routing number");
				err = true;
			} else {
				errors.rejectValue("routingNumber", "", "Ruta y tr&aacute;nsito invalido");
				err = true;
			}
		} else if (routingNumber.equals("021502011") || routingNumber.equals("021606674")
				|| routingNumber.equals("021502105")) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("routingNumber", "", "BPPR is not a valid external bank");
				err = true;
			} else {
				errors.rejectValue("routingNumber", "", "BPPR no es un banco externo valido");
				err = true;
			}
		} else if ("".equals(customerService.getBankName(routingNumber))) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("routingNumber", "", "Please select a valid routing number");
				err = true;
			} else {
				errors.rejectValue("routingNumber", "",
						"Favor de entrar un n&uacute;mero de ruta y tr&aacute;nsito valido");
				err = true;
			}
		}

		// Account numbers must match
		if (GenericValidator.isBlankOrNull(accountNumber)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("accountNumber", "", "Required");
				err = true;
			} else {
				errors.rejectValue("accountNumber", "", "Requerido");
				err = true;
			}
		}
		if (GenericValidator.isBlankOrNull(confirmAccountNumber)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("confirmAccountNumber", "", "Required");
				err = true;
			} else {
				errors.rejectValue("confirmAccountNumber", "", "Requerido");
				err = true;
			}
		}
		if (!GenericValidator.isBlankOrNull(accountNumber)) {
			// Make sure each digit is valid
			for (int i = 0; i < accountNumber.length(); i++) {
				if (!GenericValidator.isInt(accountNumber.substring(i, i + 1))) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("accountNumber", "", "Please select a valid account number");
						err = true;
					} else {
						errors.rejectValue("accountNumber", "", "Favor de entrar un n&uacute;mero de cuenta valido");
						err = true;
					}
					break;
				}
			}
		}
		// 14 digit max
		if (!GenericValidator.isBlankOrNull(accountNumber) && accountNumber.length() > 14) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("accountNumber", "", "Invalid account length");
				err = true;
			} else {
				errors.rejectValue("accountNumber", "", "Largo de n&uacute;mero de cuenta no permitido");
				err = true;
			}
		}
		if (!GenericValidator.isBlankOrNull(accountNumber) && !accountNumber.equals(confirmAccountNumber)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("confirmAccountNumber", "", "Account number does not match");
				err = true;
			} else {
				errors.rejectValue("confirmAccountNumber", "", "N&uacute;mero de cuenta no coincide");
				err = true;
			}
		}

		// Checking/Savings
		if (GenericValidator.isBlankOrNull(accountType)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("accountType", "", "Please select the account type");
				err = true;
			} else {
				errors.rejectValue("accountType", "", "Favor de seleccionar el tipo de cuenta");
				err = true;
			}
		}

		// Is the account locked?
		if (customerService.isAccountLocked(routingNumber, accountNumber, taxId)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("accountNumber", "", "Invalid account number");
				userSession.setStatusmessage("The account you are trying to add is blocked for security reasons.");
				err = true;
			} else {
				errors.rejectValue("accountNumber", "", "N&uacute;mero de cuenta invalido");
				userSession
						.setStatusmessage("La cuenta que est&aacute;s intentando a&ntilde;adir esta bloqueada por razones de seguridad.");
				err = true;
			}
		}

		if (err) {
			UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
				session.setStatusmessage("Please verify information entered");
			else
				session.setStatusmessage("Verifica la informaci&oacute;n entrada");
		}
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerServices) {
		this.customerService = customerServices;
	}
}
