package evertec.cibp.web.form.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import evertec.cibp.web.form.bean.ExternalAccountVerificationBean;

public class ExternalAccountVerificationValidator implements Validator {

	public boolean supports(Class clazz) {
		return clazz.equals(ExternalAccountVerificationBean.class);
	}

	public void validate(Object target, Errors errors) {

		ExternalAccountVerificationBean bean = (ExternalAccountVerificationBean) target;

		ValidationUtils.rejectIfEmpty(errors, "amount1", "external.amtError", "Please enter amount");
		ValidationUtils.rejectIfEmpty(errors, "amount1Confirmation", "external.amtError", "Please enter amount");
		ValidationUtils.rejectIfEmpty(errors, "amount2", "external.amtError", "Please enter amount");
		ValidationUtils.rejectIfEmpty(errors, "amount2Confirmation", "external.amtError", "Please enter amount");

		if (bean.getAmount1() != null && bean.getAmount1Confirmation() != null) {
			if (!bean.getAmount1().equals(bean.getAmount1Confirmation())) {
				errors.rejectValue("amount1Confirmation", "external.amtDiscrepancy", "Amounts don't match");
			}
		}

		if (bean.getAmount2() != null && bean.getAmount2Confirmation() != null) {
			if (!bean.getAmount2().equals(bean.getAmount2Confirmation())) {
				errors.rejectValue("amount2Confirmation", "external.amtDiscrepancy", "Amounts don't match");
			}
		}

	}

}
