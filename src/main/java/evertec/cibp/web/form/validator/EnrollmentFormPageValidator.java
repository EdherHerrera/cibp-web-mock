package evertec.cibp.web.form.validator;

import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.EnrollmentBean;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * EnrollmentFormPageValidator
 * @author et55596
 * @since 1.2
 * @version 1.0 
 */
public class EnrollmentFormPageValidator implements Validator {
    /** page **/
    
	private int page;
    private UserAgentType userAgentType;

    @SuppressWarnings({"unchecked", "rawtypes"})
    public boolean supports(Class clazz) {
        return clazz.isAssignableFrom(EnrollmentBean.class);
    }

    public void validate(Object command, Errors errors) {
        EnrollmentBean enrollment = EnrollmentBean.class.cast(command);
        Predicate<UserAgentType> userAgentTypePredicate = Objects::nonNull;
        userAgentTypePredicate.and(uat -> uat.equals(UserAgentType.IPHONE))
            .or(uat -> uat.equals(UserAgentType.ANDROID));
        // Switch statement to incrementally validate EnrollmentBean
        // No break statements are use at the end of each case to make
        // validation incremental. For example if case 4 is found, 3, 2, 1, 0
        // will all execute
        // and so forth. (One a match is found, all subsequent cases are
        // executed.)

        switch (page) {

            case 4:
            	validateCaseFourth(enrollment, userAgentTypePredicate, errors);
                break;

            case 3:
            	vadaliteCaseThird(enrollment, errors);              
                break;

            default:

                break;
        }
    }
    
    public void validateCaseFourth(EnrollmentBean enrollment, Predicate<UserAgentType> userAgentTypePredicate, Errors errors) {
    	
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)
                && GenericValidator.isBlankOrNull(enrollment.getBsmartAgreement())) {

                if (userAgentTypePredicate.test(userAgentType)) {
                    errors.rejectValue("bsmartAgreement", "bsmart.mobile.enrollment.agreement", "You have to select one of the following options");
                } else {
                    errors.rejectValue("bsmartAgreement", "bsmart.enrollment.agreement", "Seleccionar un tipo");
                }
            }

            if (GenericValidator.isBlankOrNull(enrollment.getAgreement())
                || !enrollment.getAgreement().equalsIgnoreCase("agree")) {
                if (userAgentTypePredicate.test(userAgentType) && GenericValidator.isBlankOrNull(enrollment.getBsmartAgreement())){
                    errors.rejectValue("agreement", "enrollment.mobile.agreement.error", "You have to accept in order to continue");
                } else{
                    errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
                }

            }
    	
    }
    
    public void vadaliteCaseThird(EnrollmentBean enrollment, Errors errors) {
    	
    	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "question1", "enrollment.question.required", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "question2", "enrollment.question.required", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "question3", "enrollment.question.required", "Required");

        String a1 = enrollment.getAnswer1();
        String a2 = enrollment.getAnswer2();
        String a3 = enrollment.getAnswer3();
        String a1c = enrollment.getAnswer1confirm();
        String a2c = enrollment.getAnswer2confirm();
        String a3c = enrollment.getAnswer3confirm();

        String computerType = enrollment.getComputerType();

        String re = "^\\w+(\\s{1}\\w+)*$";
        if (GenericValidator.isBlankOrNull(a1) || !GenericValidator.matchRegexp(a1, re)) {
            errors.rejectValue("answer1", "enrollment.answer.invalid", "Please enter a valid answer");
        } else if (!GenericValidator.isBlankOrNull(a1) && !a1.equals(a1c)) {
            errors.rejectValue("answer1confirm", "enrollment.answerconfirm.notmatch", "Answers values must match");
        }
       
        validateAnswerTwo(errors, a2, a2c, re);
        
        validateAnswerThree(errors, a3, a3c, re);
        
        if (!"private".equals(computerType) && !"public".equals(computerType)) {
            errors.rejectValue("computerType", "enrollment.computerType.required",
                "Please select a computer type you are using");
        }
  	
    }
    
    public void validateAnswerTwo(Errors errors, String a2, String a2c, String re) {
        if (GenericValidator.isBlankOrNull(a2) || !GenericValidator.matchRegexp(a2, re)) {
            errors.rejectValue("answer2", "enrollment.answer.invalid", "Please enter a valid answer");
        } else if (!GenericValidator.isBlankOrNull(a2) && !a2.equals(a2c)) {
            errors.rejectValue("answer2confirm", "enrollment.answerconfirm.notmatch", "Answers values must match");
        }
   	
    }
    
    public void validateAnswerThree(Errors errors, String a3, String a3c, String re) {
    	
        if (GenericValidator.isBlankOrNull(a3) || !GenericValidator.matchRegexp(a3, re)) {
            errors.rejectValue("answer3", "enrollment.answer.invalid", "Please enter a valid answer.");
        } else if (!GenericValidator.isBlankOrNull(a3) && !a3.equals(a3c)) {
            errors.rejectValue("answer3confirm", "enrollment.answerconfirm.notmatch", "Answers values must match");
        }
    	
    }

    public EnrollmentFormPageValidator(int page) {
        super();
        this.page = page;
    }

    public EnrollmentFormPageValidator(int page, UserAgentType userAgentType) {
        super();
        this.page = page;
        this.userAgentType = userAgentType;
    }

}
