package evertec.cibp.web.form.validator;

import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.SSDSEnrollmentBean;
import evertec.cibp.web.utils.Utils;

public class SSDSEnrollmentFormPageValidator implements Validator {

	private int page;
	
	// MBSFE-691
	private String invalidEmailDomains;
	// END MBSFE-691


	@SuppressWarnings("unchecked")
	public boolean supports(Class clazz) {
		return clazz.isAssignableFrom(SSDSEnrollmentBean.class);
	}

	public void validate(Object command, Errors errors) {
		SSDSEnrollmentBean enrollment = (SSDSEnrollmentBean) command;

		// Switch statement to incrementally validate EnrollmentBean
		// No break statements are use at the end of each case to make
		// validation incremental. For example if case 4 is found, 3, 2, 1, 0
		// will all execute
		// and so forth. (One a match is found, all subsequent cases are
		// executed.)

		switch (page) {

		case 1: {
			if (GenericValidator.isBlankOrNull(enrollment.getAgreement())
					|| !enrollment.getAgreement().equalsIgnoreCase("agree")) {
				errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
			}
		}

		case 0: {

			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastname", "enrollment.field.required", "Required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "enrollment.field.required", "Required");

			// MBSFE-691
			String[] domain = enrollment.getEmail().split("@");
			// END MBSFE-691
			
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE13)) {
				if (!GenericValidator.isBlankOrNull(enrollment.getEmail())
						&& !GenericValidator.isEmail(enrollment.getEmail())) {
					errors.rejectValue("email", "enrollment.email.invalid", "Invalid Email");
				} else if (!GenericValidator.isBlankOrNull(enrollment.getEmail())
						&& !enrollment.getEmail().equalsIgnoreCase(enrollment.getEmailconfirm())) {
					errors.rejectValue("emailconfirm", "enrollment.emailconfirm.notmatch", "Email values must match");
				}
				
				// MBSFE-691
				else if (Utils.haveType(this.invalidEmailDomains, domain[1])
						&& StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE691)) {
					errors.rejectValue("email", "email.invaliddomain",
							"Your e-mail address is incorrect. Please try again.");
				}
				// END MBSFE-691
				
			} else {
				if (!GenericValidator.isBlankOrNull(enrollment.getEmail())
						&& !GenericValidator.isEmail(enrollment.getEmail())) {
					errors.rejectValue("email", "enrollment.email.invalid", "Invalid Email");
				} else if (!GenericValidator.isBlankOrNull(enrollment.getEmail())
						&& !enrollment.getEmail().equals(enrollment.getEmailconfirm())) {
					errors.rejectValue("emailconfirm", "enrollment.emailconfirm.notmatch", "Email values must match");
				}
				
				// MBSFE-691
				else if (Utils.haveType(this.invalidEmailDomains, domain[1])
						&& StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE691)) {
					errors.rejectValue("email", "email.invaliddomain",
							"Your e-mail address is incorrect. Please try again.");
				}
				// END MBSFE-691
				
			}

			if (GenericValidator.isBlankOrNull(enrollment.getTaxId())) {
				errors.rejectValue("taxId", "enrollment.field.required", "Required");
			}
			if (!GenericValidator.isBlankOrNull(enrollment.getTaxId())
					&& !GenericValidator.matchRegexp(enrollment.getTaxId(), "\\d{9}")) {
				errors.rejectValue("taxId", "enrollment.taxId.invalid", "Invalid SSN");
			}
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dobmonth", "enrollment.dobmonth.invalid",
					"Please enter a valid month");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dobday", "enrollment.dobday.invalid",
					"Please enter a valid day");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dobyear", "enrollment.dobyear.invalid",
					"Please enter a valid year");

			if (!GenericValidator.matchRegexp(String.valueOf(enrollment.getDobyear()), "\\d{4}"))
				errors.rejectValue("dobmonth", "enrollment.dobmonth.invalid", "Please enter a valid month");

			if (!GenericValidator.matchRegexp(String.valueOf(enrollment.getDobday()), "\\d{1,2}"))
				errors.rejectValue("dobday", "enrollment.dobday.invalid", "Please enter a valid day");
			if (!GenericValidator.matchRegexp(String.valueOf(enrollment.getDobmonth()), "\\d{1,2}"))
				errors.rejectValue("dobyear", "enrollment.dobyear.invalid", "Please enter a valid year");

			String username = enrollment.getUsername();
			if (!(!GenericValidator.isBlankOrNull(username) && GenericValidator.maxLength(username, 20, 0)
					&& GenericValidator.minLength(username, 6, 0) && GenericValidator.matchRegexp(username, "\\w+"))) {
				errors.rejectValue("username", "", "Invalid Username");
			}

		}

		}

	}

	public SSDSEnrollmentFormPageValidator(int page) {
		super();
		this.page = page;
	}

	// MBSFE-691
	public SSDSEnrollmentFormPageValidator() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getInvalidEmailDomains() {
		return invalidEmailDomains;
	}

	public void setInvalidEmailDomains(String invalidEmailDomains) {
		this.invalidEmailDomains = invalidEmailDomains;
	}
	// END MBSFE-691
	
}
