/**
 * 
 */
package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import com.evertecinc.www.PremiaPointsActivation.PointsCostCenter;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TVFrontendCreditAccount;
import evertec.cibp.core.models.account.TVFrontendDepositAccount;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.EStatementInteraction;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.ebill.Ebpp2BillerProductIdResponse;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.PremiaPointsRequest;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.GuidedSetupAddEbillBean;
import evertec.cibp.web.form.bean.GuidedSetupAddEbillListBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.UserAccount;
import evertec.otherservices.requests.RegisterUserAccountReq;
import evertec.otherservices.responses.RegisterUserAccountResp;
import evertec.prophit.service.bean.CustomerInquiryBean;
import evertec.prophit.service.bean.EbillIndType;
import evertec.prophit.service.bean.MaintenceEbillBean;
import evertec.prophit.service.bean.MaintenceEbillResponse;


/**
 * Clase evertec.cibp.web.form.GuidedSetupAddEbills Sistema: Mi Banco
 * Descripcion: Clase que controla las acciones de la capa de presentacion para
 * agregar ebills en la campana guided setup
 * 
 * @author Ing. Michael Picado Fuentes Creada el Mar 15, 2011 2:43:47 PM
 *         cibp-2218
 */

public class GuidedSetupAddEbills extends AbstractWizardFormController implements MessageSourceAware {

	// mpicado, atributo que se agrega para poder leer el archivo downtime,
	// ocupa el implements del MessageSourceAware
	private MessageSource messageSource;
	protected CustomerServices customerService;
	private String eSignValidDepositAccounts;

	/* Constructor de la clase GuidedSetupAddEbills */
	public GuidedSetupAddEbills() {
		super();
		setCommandClass(GuidedSetupAddEbillListBean.class);
		setCommandName("gsAddEbill");
		setPages(new String[] { "gsAddEbills" });
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {
		GuidedSetupAddEbillListBean gsAddEbillList = (GuidedSetupAddEbillListBean) command;
		boolean registrar = false;
		for (int i = 0; i < gsAddEbillList.getListAddEbillBeans().size(); i++) {
			if (gsAddEbillList.getListAddEbillBeans().get(i).isChecked()) {
				registrar = true;
				break;
			}
		}
		if (!registrar) {
			errors.reject("guidedSetup.ebills.registrar");
		}
	}

	/**
	 * vea @see
	 * org.springframework.web.servlet.mvc.AbstractWizardFormController#
	 * processFinish(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse, java.lang.Object,
	 * org.springframework.validation.BindException)
	 */
	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		GuidedSetupAddEbillListBean gsAddEbillList = (GuidedSetupAddEbillListBean) command;
		List<TVFrontendAccount> agregados = new ArrayList<>();
		List<TVFrontendAccount> conError = new ArrayList<>();

		CustomerProfile profile = userSession.getCustomerProfile();
		BankingSession bankingSession = userSession.getBankingSession();
		// ESTATEMENTS
		boolean resp;
		for (int i = 0; i < gsAddEbillList.getListAddEbillBeans().size(); i++) {
			if (gsAddEbillList.getListAddEbillBeans().get(i).isChecked()) {
				TVFrontendAccount account = gsAddEbillList.getListAddEbillBeans().get(i).getTvfeAccount();
				resp = this.activaEstatement(account, userSession,request);
				// CIBP-2771 register the account as payee, only CCA
				if (account.getSubtype().equals("CCA")) {
					GlobalPayee global = null;
					if (account.getAccountNumber().startsWith("03"))// means
						// amex
						// 03778
						global = PaymentsFacade.getInstance().getGlobalPayeeById(1983, userSession);
					else if (account.getAccountNumber().startsWith("4")) // visa
						// 4549.....
						global = PaymentsFacade.getInstance().getGlobalPayeeById(359, userSession);
					else if (account.getAccountNumber().startsWith("5") && !account.getAccountNumber().startsWith("50"))// mastercard
						// 5310
						// 53100585
						// 546620
						global = PaymentsFacade.getInstance().getGlobalPayeeById(223, userSession);

					if (global != null) {
						boolean isHighRiskPayee = false;
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1701)) {
							PaymentsFacade.getInstance().getGlobalPayeePreference(global, userSession);

							if (global.getPreference() != null)
								 isHighRiskPayee = global.getPreference().getHighRisk();
						}
						boolean resPayee = customerService.addCustomerPayeeWithoutEbill(profile, bankingSession,
								global, account.getAccountNumber(), account.getNickname(),
								Utils.generateCustomerInteractionEvent(request), null, null, null, null, false, isHighRiskPayee);
						if (resPayee) {
							// System.out.println("Added CCA as payee");
						}
					}
					// End CIBP-2771
				}
				if (resp)
					agregados.add(account);
				else
					conError.add(account);
			}
		}

		model.put("agregados", agregados);
		model.put("conError", conError);
		model.put("email", userSession.getCustomerProfile().getEmail());

		// If there is more than one successful activation, clear the EBPP2
		// session variables
		if (agregados != null && !agregados.isEmpty()) {
			userSession.setEbpp2UserAccounts(null);
			userSession.setEbpp2UserAccountsAndSummaries(null);
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)){
				userSession.resetIdaAccount();
			}
		}

		String userMail = userSession.getCustomerProfile().getEmail();
			if(userMail != null && userMail.matches( "^(.+)@(.+)$")) {	
				request.getSession().setAttribute("email", userMail); 	
			}

		request.getSession().setAttribute("agregados", agregados);
		request.getSession().setAttribute("conError", conError);
		model.put("page", 1); // pagina de confirmacion

		return new ModelAndView("gsAddEbills", model);
	}

	/**
	 * vea @see
	 * org.springframework.web.servlet.mvc.AbstractWizardFormController#
	 * referenceData(javax.servlet.http.HttpServletRequest, java.lang.Object,
	 * org.springframework.validation.Errors, int)
	 */
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		GuidedSetupAddEbillListBean gsAddEbillList = (GuidedSetupAddEbillListBean) command;

		gsAddEbillList.setListAddEbillBeans(new ArrayList<GuidedSetupAddEbillBean>());

		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>();

		// mpicado, esto lo hago para crear una lista nueva que apunte a
		// distintas posiciones de memoria en los objetos TVFrontendAccount
		for (int z = 0; z < userSession.getAccounts().size(); z++)
			// For the ebill check defect 1414
			accounts.add(userSession.getAccounts().get(z).clone());

		List<TVFrontendAccount> removeACC = new ArrayList<>();

		if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {
			List<UserAccount> accountsEBBP2 = userSession.getEbpp2UserAccounts();
			for (TVFrontendAccount account : accounts) {
				if (account.getIsAvailableElecStmt()) {
					for (UserAccount acc : accountsEBBP2) {
						if (acc != null && acc.isActive()
								&& acc.getAccountNumber().contains(account.getAccountNumber())) {
							removeACC.add(account);
						}
					}
				} else {
					removeACC.add(account);
				}
			}
		}


		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)){
			for(Iterator<TVFrontendAccount> iteratorAcc = accounts.iterator(); iteratorAcc.hasNext();){
				TVFrontendAccount tvAccount = iteratorAcc.next();

				if(tvAccount.getIsAvailableElecStmt() && tvAccount.getSubtype().equalsIgnoreCase("IDA")){
					List<TVFrontendDepositAccount> idaAccounts = userSession.getIdaAccounts();
					for(Iterator<TVFrontendDepositAccount> iterator2 = idaAccounts.iterator(); iterator2.hasNext();){
						TVFrontendDepositAccount idaAccount = iterator2.next();
						if(idaAccount.getAccountKey().equals(tvAccount.getAccountKey())){
							if (idaAccount.getStmtDisposition() != null
									&& (idaAccount.getStmtDisposition().equalsIgnoreCase("E") 
											|| idaAccount.getStmtDisposition().equalsIgnoreCase("V")
											|| idaAccount.getStmtDisposition().isEmpty())) {
								removeACC.add(tvAccount);
							}else{
								continue;
							}
						}
					}
				}
				else if(tvAccount.getIsAvailableElecStmt() && tvAccount.getSubtype().equalsIgnoreCase("CCA")){
					try {
						CustomerInquiryBean responseTsys = userSession.getCustomerService().prophItService()
								.makeCustomerInquiry(tvAccount.getAccountNumber());
						if (responseTsys != null
								&& responseTsys.getEbillInd().equalsIgnoreCase(EbillIndType.ACTIVE.toString())
								|| responseTsys.getEbillInd().equalsIgnoreCase(EbillIndType.NOELIGIBLE.toString())) {
							removeACC.add(tvAccount);
						}
					} catch (Exception e) {
						logger.info("Error getting information from TSYS for account " + tvAccount.getAccountNumber());
						removeACC.add(tvAccount);

					}

				}
			}

		}

		// elimino todas las cuentas no seleccionables para activar
		// ebill/estatement
		accounts.removeAll(removeACC);

		for (int i = 0; i < accounts.size(); i++) {
			GuidedSetupAddEbillBean aeBean = new GuidedSetupAddEbillBean();
			aeBean.setTvfeAccount(accounts.get(i));
			aeBean.setChecked(false);
			gsAddEbillList.getListAddEbillBeans().add(aeBean);
		}

		if (userSession.getGuidedSetupSession().isResubmitParent()) {
			model.put("submitgs", true);
			userSession.getGuidedSetupSession().setResubmitParent(false);
		}
		if (request.getSession().getAttribute("email") == null) {
			model.put("page", 0);// pagina principal
		} else {
			model.put("agregados", request.getSession().getAttribute("agregados"));
			model.put("conError", request.getSession().getAttribute("conError"));
			model.put("email", userSession.getCustomerProfile().getEmail());
			model.put("page", 1);
		}

		model.put("email", userSession.getCustomerProfile().getEmail());
		return model;
	}

	private boolean activaEstatement(TVFrontendAccount tvp, UserSession userSession, HttpServletRequest request ) {

		CustomerProfile profile = userSession.getCustomerProfile();

		// The EBPP2 user id is not yet in session; add it
		if (!userSession.isEbpp2UserIdLookedUp()) {
			EbppEntitlement ent = null;
			if (profile.getEntitlementByType(EntitlementType.EBILLS) != null) {
				ent = new EbppEntitlement(profile.getEntitlementByType(EntitlementType.EBILLS));

				// If the username is blank, we need to look in the current EBPP
				// accounts ONLY
				if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
					userSession.setEbpp2UserId("");
				} else {
					userSession.setEbpp2UserId(ent.getUsername());
				}
				userSession.setEbpp2UserIdLookedUp(true);
			}
		}

		// The user has an EBPP2 ID
		if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

			RegisterUserAccountReq req = new RegisterUserAccountReq();

			// CCA
			if (tvp.getSubtype() != null && tvp.getSubtype().equals("CCA")) {

				//add flag and activation in TSYS instead of EBP2
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)){
					try {
						CustomerInquiryBean responseTsys =  userSession.getCustomerService().prophItService()
								.makeCustomerInquiry(tvp.getAccountNumber());    
						if (responseTsys != null) {
							if (responseTsys.getEbillInd().equalsIgnoreCase(EbillIndType.ELIGIBLE.toString())) {
								MaintenceEbillBean maintenceEBillBean = new MaintenceEbillBean();
								maintenceEBillBean.setAcctNum(tvp.getAccountNumber());
								maintenceEBillBean.setCaseNumber(responseTsys.getCaseNumber());
								maintenceEBillBean.setChannelId(responseTsys.getChannelId());
								maintenceEBillBean.setEbillInd(EbillIndType.ENROLL.toString());
								maintenceEBillBean.setImpersonateId(responseTsys.getImpersonateId());
								maintenceEBillBean.setKeepCaseOpen(responseTsys.getKeepCaseOpen());
								maintenceEBillBean.setNewContact(responseTsys.getNewContact());
								maintenceEBillBean.setOrgId(responseTsys.getOrgId());
								maintenceEBillBean.setOrgLevel(responseTsys.getOrgLevel());
								MaintenceEbillResponse responseMaintananceTsys = userSession.getCustomerService().prophItService().maintenceEBill(maintenceEBillBean);
								CustomerInteractionEvent customerInteractionEvent;
								if (responseMaintananceTsys != null
										&& responseMaintananceTsys.getMessage().indexOf("Successfull") != -1) {
									userSession.getCustomerService().sendEbillsCCAEmailActivationSuccess(
											userSession.getCustomerProfile(), PreferredLanguage.SPANISH,
											tvp.getNickname());
									//Log Statements
									customerInteractionEvent = Utils
											.generateCustomerInteractionEvent(request);
									customerInteractionEvent.setCustomStringData("CCA accountNumber: ",
											tvp.getAccountNumber());
									customerInteractionEvent.setCustomStringData(
											EStatementInteraction.STATUS,
											"Account: " + tvp.getAccountNumber() + " activate.");
									customerService.logUpdateStatementStatus(customerInteractionEvent,
											InteractionSeverity.INFO);
									// Premia Report

									PremiaPointsRequest premiaRequest = new PremiaPointsRequest(null,
											String.valueOf(profile.getPermId()), tvp.getAccountNumber(),
											profile.getTaxId(), "Anadir Factura Electronica", "098",
											PointsCostCenter.INSFE, profile.getUsername(), "MiBanco", true);
									userSession.getCustomerService().premiaPointsActivation(premiaRequest);

									return true;
								}else{
									customerInteractionEvent = Utils
											.generateCustomerInteractionEvent(request);
									customerInteractionEvent.setCustomStringData("CCA accountNumber: ",
											tvp.getAccountNumber());
									customerInteractionEvent.setCustomStringData(
											EStatementInteraction.STATUS, "Unable to activate account:"
													+ tvp.getAccountNumber());
									customerService.logUpdateStatementStatus(customerInteractionEvent,
											InteractionSeverity.INFO);
									return false;
								}
							}
						}else{
							return false;
						}
					} catch (Exception e) {
						//error with catch
						logger.info("Error getting information from TSYS for account " + tvp.getAccountNumber());
						return false;
					}
				}
				else
				{
					int billerProductId = 0;
					AbstractAccount accountDetails = userSession.getCustomerService().getAccountDetails(profile,
							userSession.getBankingSession(), tvp, true);
					TVFrontendCreditAccount frontendCreditAccount = (TVFrontendCreditAccount) accountDetails;
					if (frontendCreditAccount.getProductIdBCO() != null
							&& !frontendCreditAccount.getProductIdBCO().equals(""))
						billerProductId = Integer.parseInt(frontendCreditAccount.getProductIdBCO());
					else {
						// Corporate cards start with 454903
						if (tvp.getAccountNumber().startsWith("454903"))
							billerProductId = 59;
						// Business cards start with 454925
						else if (tvp.getAccountNumber().startsWith("454925"))
							billerProductId = 60;
						// VISA Advantage Business = 61
						else if (tvp.getProductDefinition().getProductId().equals("VBABS"))
							billerProductId = 61;
						// All others are individual cards
						else
							billerProductId = 55;
					}

					req.setAccountNumber(tvp.getAccountNumber());
					req.setBillerId(1);
					req.setBillerProductId(billerProductId);
					req.setLabel(tvp.getNickname());
					req.setNss(profile.getTaxId());
					req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
					req.setZip("00000");

					// Register the account
					RegisterUserAccountResp resp = new RegisterUserAccountResp();

					try {
						resp = userSession.getCustomerService().registerEbpp2UserAccount(req);
					} catch (Exception e) {
						logger.info("Unable to add the e-Bill for account: " + tvp.getNickname());
						return false;
					}

					if (resp == null || resp.getRegisterResponse() == null)
						return false;

					return resp.getRegisterResponse().equals("0");

				}
			}
			// e-Statement
			else {
				//add flag and activation directly to IDA instead of EBP2
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)){
					TVFrontendDepositAccount idaAccount = Utils.getIdaAccountByAccountNumber(tvp.getAccountNumber(),userSession.getIdaAccounts());
					if (idaAccount != null) {
						boolean idaStatusChange = userSession.getCustomerService().activateEstatementIDAeBills(
								userSession.getBankingSession(), tvp.getAccountNumber(), idaAccount.getIdaAcctInq());
						CustomerInteractionEvent customerInteractionEvent;
						if (idaStatusChange) {
							PremiaPointsRequest premiaRequest = new PremiaPointsRequest(null,
									String.valueOf(profile.getPermId()), idaAccount.getAccountNumber(),
									profile.getTaxId(), "Activacion de estatement", "098", PointsCostCenter.DESTMT,
									profile.getUsername(), "MiBanco", true);
							userSession.getCustomerService().premiaPointsActivation(premiaRequest);
							//Log Statements
							customerInteractionEvent = Utils
									.generateCustomerInteractionEvent(request);
							customerInteractionEvent.setCustomStringData("IDA accountNumber: ",
									tvp.getAccountNumber());
							customerInteractionEvent.setCustomStringData(
									EStatementInteraction.STATUS,
									"Account: " + tvp.getAccountNumber() + " activate.");
							customerService.logUpdateStatementStatus(customerInteractionEvent,
									InteractionSeverity.INFO);
							return true;
						}else{
							customerInteractionEvent = Utils
									.generateCustomerInteractionEvent(request);
							customerInteractionEvent.setCustomStringData("IDA accountNumber: ",
									tvp.getAccountNumber());
							customerInteractionEvent.setCustomStringData(
									EStatementInteraction.STATUS, "Unable to activate account:"
											+ tvp.getAccountNumber());
							customerService.logUpdateStatementStatus(customerInteractionEvent,
									InteractionSeverity.INFO);
							return false;
						}
					}
					return false;
				}
				else{
					int billerProductId = 0;
					String productId = tvp.getProductDefinition().getProductId();

					if (productId != null) {
						Ebpp2BillerProductIdResponse ebpp2BillerProductIdResponse = userSession.getCustomerService()
								.getEbpp2BillerProductIdInformationDepositAccount(tvp);

						if (ebpp2BillerProductIdResponse != null)
							billerProductId = ebpp2BillerProductIdResponse.getBillerProductId();
					}

					// Do we have a valid biller product ID?
					if (billerProductId > 0) {

						// If this is an estatement, activate it in IDA
						boolean continueProcess = userSession.getCustomerService().activateEbpp2AccountInIDA(
								userSession.getBankingSession(), tvp);

						if (continueProcess) {
							req.setAccountNumber(tvp.getAccountNumber());
							req.setBillerId(1);
							req.setBillerProductId(billerProductId);
							req.setLabel(tvp.getNickname());
							req.setNss(profile.getTaxId());
							req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
							req.setZip("00000");

							// Register the account
							RegisterUserAccountResp resp = new RegisterUserAccountResp();

							try {
								resp = userSession.getCustomerService().registerEbpp2UserAccount(req);
							} catch (Exception e) {
								logger.info("Unable to add the e-Statement for account: " + tvp.getNickname());
								return false;
							}

							if (resp == null || resp.getRegisterResponse() == null)
								return false;

							return resp.getRegisterResponse().equals("0");
						} else
							return false;
					} else
						return false;
				}	
			}	
		}

		return false;
	}


	/** @return regresa el messageSource */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource
	 *            el messageSource a establecer
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String geteSignValidDepositAccounts() {
		return eSignValidDepositAccounts;
	}

	public void seteSignValidDepositAccounts(String eSignValidDepositAccounts) {
		this.eSignValidDepositAccounts = eSignValidDepositAccounts;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

}
