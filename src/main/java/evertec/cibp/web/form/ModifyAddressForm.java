package evertec.cibp.web.form;

import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.EventType;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.applications.AddressApplication;
import evertec.cibp.core.models.customer.applications.AddressApplication.AppliedToAccount;
import evertec.cibp.core.models.customer.applications.ApplicationStatus;
import evertec.cibp.core.models.customer.applications.CustomerApplication;
import evertec.cibp.core.models.customer.applications.CustomerApplicationType;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.utils.CibpValidationUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ModifyAddressBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ModifyAddressForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private long maxTimeModifyAddress;

	public ModifyAddressForm() {
		super();
		setCommandClass(ModifyAddressBean.class);
		setCommandName("modifyAddress");
		setFormView("modifyAddress");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		ModifyAddressBean bean = (ModifyAddressBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		List<TVFrontendAccount> accts = userSession.getAccounts();

		String residentialAddress1 = bean.getResidentialAddress1();
		String residentialAddress2 = bean.getResidentialAddress2();
		String residentialCity = bean.getResidentialCity();
		String residentialState = bean.getResidentialState();
		String residentialZipCode1 = bean.getResidentialZipCode1();
		String residentialZipCode2 = bean.getResidentialZipCode2();

		String oldResidentialAddress1 = "";
		String oldResidentialAddress2 = "";
		String oldResidentialCity = "";
		String oldResidentialState = "";
		String oldResidentialZipCode1 = "";
		String oldResidentialZipCode2 = "";

		String newPostalAddress1 = bean.getNewPostalAddress1();
		String newPostalAddress2 = bean.getNewPostalAddress2();
		String newPostalCity = bean.getNewPostalCity();
		String newPostalState = bean.getNewPostalState();
		String newPostalZipCode1 = bean.getNewPostalZipCode1();
		String newPostalZipCode2 = bean.getNewPostalZipCode2();

		String oldPostalAddress1 = bean.getOldPostalAddress1();
		String oldPostalAddress2 = bean.getOldPostalAddress2();
		String oldPostalCity = bean.getOldPostalCity();
		String oldPostalState = bean.getOldPostalState();
		String oldPostalZipCode1 = bean.getOldPostalZipCode1();
		String oldPostalZipCode2 = bean.getOldPostalZipCode2();
//		This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//		{
		String addressChangeType = bean.getAddressChangeType();
//		}
		
		AppliedToAccount apl = bean.getAppliedTo();
		boolean appliesToBoth = ServletRequestUtils.getBooleanParameter(request, "appliesToBoth", false);
		Set<String> accounts = new HashSet<String>();

		boolean outreachFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OUTREACH);
		
		if(outreachFlag){
			// Only get the accounts if the change is postal or both
			String[] acts = bean.getAccounts();
			if (acts != null && acts.length > 0) {
				for (int i = 0; i < acts.length; i++) {
					String string = acts[i];
					TVFrontendAccount account = Utils.getAccountById(string, accts);
					accounts.add(account.getAccountNumber() + account.getSubtype());
				}
			} else if (apl == AppliedToAccount.SPECIFIC_ACCOUNTS) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					error.rejectValue("accounts", "", "Select accounts you want to applied this change");
				else
					error.rejectValue("accounts", "", "Seleccione las cuentas en las que se reflejara el cambio");
			}
		}	
//		This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//		{
		else if (addressChangeType.equals("POSTAL") || addressChangeType.equals("BOTH")) {
			String[] acts = bean.getAccounts();
			if (acts != null && acts.length > 0) {
				for (int i = 0; i < acts.length; i++) {
					String string = acts[i];
					TVFrontendAccount account = Utils.getAccountById(string, accts);
					accounts.add(account.getAccountNumber() + account.getSubtype());
				}
			} else if (apl == AppliedToAccount.SPECIFIC_ACCOUNTS) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					error.rejectValue("accounts", "", "Select accounts you want to applied this change");
				else
					error.rejectValue("accounts", "", "Seleccione las cuentas en las que se reflejara el cambio");
			}
		}	

		boolean hasalert = false;
		AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
		if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
			Set<AlertType> alerts = ent.getAlerts();
			if (alerts.contains(AlertType.ADRESS_EDIT_EMAIL) || alerts.contains(AlertType.ADRESS_EDIT_SMS)
					|| alerts.contains(AlertType.ADRESS_EDIT_PUSH)) {
				hasalert = true;
			}
		}
		
//		This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//		{
		String residentialZipcode = residentialZipCode1;
		if (residentialZipCode2 != null && !residentialZipCode2.equalsIgnoreCase("")
				&& residentialZipCode2.length() == 4)
			residentialZipcode = residentialZipcode + "-" + residentialZipCode2;
//		}
		
		String oldResidentialZipcode = oldResidentialZipCode1;
		if (oldResidentialZipCode2 != null && !oldResidentialZipCode2.equalsIgnoreCase("")
				&& oldResidentialZipCode2.length() == 4)
			oldResidentialZipcode = oldResidentialZipcode + "-" + oldResidentialZipCode2;

		String newPostalZipCode = newPostalZipCode1;
		if (newPostalZipCode2 != null && !newPostalZipCode2.equalsIgnoreCase("") && newPostalZipCode2.length() == 4)
			newPostalZipCode = newPostalZipCode1 + "-" + newPostalZipCode2;

		String oldPostalZipCode = oldPostalZipCode1;
		if (oldPostalZipCode2 != null && !oldPostalZipCode2.equalsIgnoreCase("") && oldPostalZipCode2.length() == 4)
			oldPostalZipCode = oldPostalZipCode + "-" + oldPostalZipCode2;
		
//		This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//		{
		// Clear out all the residential values for postal changes
		if(!outreachFlag){
				if (addressChangeType.equals("POSTAL") || (addressChangeType.equals("BOTH") && !appliesToBoth)) {
					residentialAddress1 = "";
					residentialAddress2 = "";
					residentialCity = "";
					residentialState = "";
					residentialZipCode1 = "";
					residentialZipCode2 = "";
					residentialZipcode = "";
				}
				// Clear out all the postal values for residential changes
				else if (addressChangeType.equals("RESIDENTIAL")) {
					newPostalAddress1 = "";
					newPostalAddress2 = "";
					newPostalCity = "";
					newPostalState = "";
					newPostalZipCode1 = "";
					newPostalZipCode2 = "";
					newPostalZipCode = "";
				}
		}
//		}
		
		boolean saveApplication = false;
		
		AddressApplication addressApplication = null;
		
		if(outreachFlag){
			addressApplication = new AddressApplication("", "",
					"", "", "", oldResidentialAddress1, oldResidentialAddress2,
					oldResidentialCity, oldResidentialState, oldResidentialZipcode, newPostalAddress1, newPostalAddress2,
					newPostalCity, newPostalZipCode, newPostalState, oldPostalAddress1, oldPostalAddress2, oldPostalCity,
					oldPostalZipCode, oldPostalState, apl, accounts, ApplicationStatus.NEW);
		}
//		This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//		{
		else{
			addressApplication = new AddressApplication(residentialAddress1, residentialAddress2,
					residentialCity, residentialState, residentialZipcode, oldResidentialAddress1, oldResidentialAddress2,
					oldResidentialCity, oldResidentialState, oldResidentialZipcode, newPostalAddress1, newPostalAddress2,
					newPostalCity, newPostalZipCode, newPostalState, oldPostalAddress1, oldPostalAddress2, oldPostalCity,
					oldPostalZipCode, oldPostalState, apl, accounts, ApplicationStatus.NEW);
		}
//		}
		try {
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBIM250)){
				List<CustomerApplication> applications = customerService.getCustomerApplicationsByTypeAndStatus(customer, 
						CustomerApplicationType.ADDRESS_MODIFY, ApplicationStatus.NEW);
				if (applications != null && applications.size()>0){
					long actualTime = new Date().getTime();
					long lastRegisterTime = applications.get(0).getCreationDate().getTime();
					long completeTime = actualTime - lastRegisterTime;
					
					if (completeTime > maxTimeModifyAddress){
						CustomerFacade.getInstance().saveAddressApplication(addressApplication,customer, request);
						saveApplication = true;
					}
				}else {
					CustomerFacade.getInstance().saveAddressApplication(addressApplication,customer, request);
					saveApplication = true;
				}
			}else{
				CustomerFacade.getInstance().saveAddressApplication(addressApplication,customer, request);
				saveApplication = true;
			}
			
	    	final CustomerInteractionEvent interaction = Utils.generateCustomerInteractionEvent(request, userSession.getUsername()); // the event
	    	
	    	customerService.getInteractionService().logCustomerEvent(interaction, 
	        		 CustomerInteractionEventType.MYINFO_ADDRESS_CHANGE, InteractionSeverity.INFO);
		} catch (RuntimeException e) {
			saveApplication = false;
		}

		model.put("completed", true);
		if (!saveApplication) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Unable to send your change request, Please try later.");
			else
				userSession
						.setStatusmessage("Su solicitud de cambio no se pudo procesar en este momento, favor intentar luego.");
		} else {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Your change request will be processed in the next 2 business days.");
			else
				userSession
						.setStatusmessage("Su solicitud de cambio ser&aacute; procesada en 2 d&iacute;as laborables.");
			// CIBP-1104 se activa envio alerta de modificacion de direccion
			if (hasalert) {
				customerService.sendAdressEditConfirmation(customer,
						userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
						userSession.getPreferredLanguage());
			}
		}
		model.put("type", "");
		model.put("appliesToBoth", appliesToBoth);
		
		// MBSD-2000 RSA Upgrade Recommendations - Edit mailing address (Update cookie)
		Cookie cookie = WebUtils.getCookie(request, "PMData") != null ? WebUtils.getCookie(request, "PMData") : new Cookie("PMData", "");
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		ESAPI.httpUtilities().addCookie(response, cookie);


		return showForm(request, error, "modifyAddress", model);
	}

	@Override
	@SuppressWarnings("unused")
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		Map<String, Object> model = new HashMap<>();
		boolean appliesToBoth = ServletRequestUtils.getBooleanParameter(request, "appliesToBoth", false);
		model.put("appliesToBoth", appliesToBoth);

		ModifyAddressBean bean = (ModifyAddressBean) command;
//		This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//		{
		String residentialAddress1 = bean.getResidentialAddress1();
		String residentialAddress2 = bean.getResidentialAddress2();
		String residentialCity = bean.getResidentialCity();
		String residentialState = bean.getResidentialState();
		String residentialZipCode1 = bean.getResidentialZipCode1();
		String residentialZipCode2 = bean.getResidentialZipCode2();

		String oldResidentialAddress1 = bean.getOldResidentialAddress1();
		String oldResidentialAddress2 = bean.getOldResidentialAddress2();
		String oldResidentialCity = bean.getOldResidentialCity();
		String oldResidentialState = bean.getOldResidentialState();
		String oldResidentialZipCode1 = bean.getOldResidentialZipCode1();
		String oldResidentialZipCode2 = bean.getOldResidentialZipCode2();
//		}
		
		String newPostalAddress1 = bean.getNewPostalAddress1();
		String newPostalAddress2 = bean.getNewPostalAddress2();
		String newPostalCity = bean.getNewPostalCity();
		String newPostalState = bean.getNewPostalState();
		String newPostalZipCode1 = bean.getNewPostalZipCode1();
		String newPostalZipCode2 = bean.getNewPostalZipCode2();

		String oldPostalAddress1 = bean.getOldPostalAddress1();
		String oldPostalAddress2 = bean.getOldPostalAddress2();
		String oldPostalCity = bean.getOldPostalCity();
		String oldPostalState = bean.getOldPostalState();
		String oldPostalZipCode1 = bean.getOldPostalZipCode1();
		String oldPostalZipCode2 = bean.getOldPostalZipCode2();
//		This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//		{
		String addressChangeType = bean.getAddressChangeType();
//		}
		
		AppliedToAccount apl = bean.getAppliedTo();
		
//		This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//		{
		boolean outreachFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OUTREACH);
		
		if(!outreachFlag){
			// Only verify the residential address if the type is RESIDENTIAL or
			// BOTH
			if (addressChangeType.equals("RESIDENTIAL") || (addressChangeType.equals("BOTH") && appliesToBoth)) {
				if (GenericValidator.isBlankOrNull(residentialAddress1)
						|| !CibpValidationUtils.isValidAddress(residentialAddress1)) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("residentialAddress1", "", "Please enter a valid address.");
					else
						errors.rejectValue("residentialAddress1", "", "Favor entrar una direcci&oacute;n valida.");
				}
				if (!GenericValidator.isBlankOrNull(residentialAddress2)
						&& !CibpValidationUtils.isValidAddress(residentialAddress2)) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("residentialAddress2", "", "Please enter a valid address.");
					else
						errors.rejectValue("residentialAddress2", "", "Favor entrar una direcci&oacute;n valida.");
				}
				if (GenericValidator.isBlankOrNull(residentialCity)
						|| !GenericValidator.matchRegexp(residentialState, "^[A-Za-z ]+$")) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("residentialCity", "", "Please enter a valid city.");
					else
						errors.rejectValue("residentialCity", "", "Favor entrar una ciudad valida.");
				}
				if (GenericValidator.isBlankOrNull(residentialState)
						|| !GenericValidator.matchRegexp(residentialState, "^[A-Za-z ]+$")) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("residentialState", "", "Please enter a valid state.");
					else
						errors.rejectValue("residentialState", "", "Favor entrar un estado valido.");
				}
				if (GenericValidator.isBlankOrNull(residentialZipCode1)
						|| !GenericValidator.matchRegexp(residentialZipCode1, "^\\d{5}$")) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("residentialZipCode2", "", "Please enter a valid zipcode.");
					else
						errors.rejectValue("residentialZipCode2", "", "Favor entrar un c&oacute;digo postal valido.");
				} else if (!GenericValidator.isBlankOrNull(residentialZipCode2)
						&& !GenericValidator.matchRegexp(residentialZipCode2, "^\\d{4}$")) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("residentialZipCode2", "", "Please enter a valid zipcode.");
					else
						errors.rejectValue("residentialZipCode2", "", "Favor entrar un c&oacute;digo postal valido.");
				}
			}

		}

		if(outreachFlag){
			if (GenericValidator.isBlankOrNull(newPostalAddress1)
					|| !CibpValidationUtils.isValidAddress(newPostalAddress1)) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("newPostalAddress1", "", "Please enter a valid address.");
				else
					errors.rejectValue("newPostalAddress1", "", "Favor entrar una direcci&oacute;n valida.");
			}
			if (!GenericValidator.isBlankOrNull(newPostalAddress2)
					&& !CibpValidationUtils.isValidAddress(newPostalAddress2)) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("newPostalAddress2", "", "Please enter a valid address.");
				else
					errors.rejectValue("newPostalAddress2", "", "Favor entrar una direcci&oacute;n valida.");
			}
			if (GenericValidator.isBlankOrNull(newPostalCity)
					|| !GenericValidator.matchRegexp(newPostalCity, "^[A-Za-z ]+$")) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("newPostalCity", "", "Please enter a valid city.");
				else
					errors.rejectValue("newPostalCity", "", "Favor entrar una ciudad valida.");
			}
			if (GenericValidator.isBlankOrNull(newPostalState)
					|| !GenericValidator.matchRegexp(newPostalState, "^[A-Za-z ]+$")) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("newPostalState", "", "Please enter a valid state.");
				else
					errors.rejectValue("newPostalState", "", "Favor entrar un estado valido.");
			}
			if (GenericValidator.isBlankOrNull(newPostalZipCode1)
					|| !GenericValidator.matchRegexp(newPostalZipCode1, "^\\d{5}$")) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("newPostalZipCode2", "", "Please enter a valid zipcode.");
				else
					errors.rejectValue("newPostalZipCode2", "", "Favor entrar un c&oacute;digo postal valido.");
			} else if (!GenericValidator.isBlankOrNull(newPostalZipCode2)
					&& !GenericValidator.matchRegexp(newPostalZipCode2, "^\\d{4}$")) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("newPostalZipCode2", "", "Please enter a valid zipcode.");
				else
					errors.rejectValue("newPostalZipCode2", "", "Favor entrar un c&oacute;digo postal valido.");
			}
		}
//		This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//		{
		// Only verify the new postal address if the address change type is
		// POSTAL
		else if (addressChangeType.equals("POSTAL") || addressChangeType.equals("BOTH")) {
			if (GenericValidator.isBlankOrNull(newPostalAddress1)
					|| !CibpValidationUtils.isValidAddress(newPostalAddress1)) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("newPostalAddress1", "", "Please enter a valid address.");
				else
					errors.rejectValue("newPostalAddress1", "", "Favor entrar una direcci&oacute;n valida.");
			}
			if (!GenericValidator.isBlankOrNull(newPostalAddress2)
					&& !CibpValidationUtils.isValidAddress(newPostalAddress2)) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("newPostalAddress2", "", "Please enter a valid address.");
				else
					errors.rejectValue("newPostalAddress2", "", "Favor entrar una direcci&oacute;n valida.");
			}
			if (GenericValidator.isBlankOrNull(newPostalCity)
					|| !GenericValidator.matchRegexp(newPostalCity, "^[A-Za-z ]+$")) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("newPostalCity", "", "Please enter a valid city.");
				else
					errors.rejectValue("newPostalCity", "", "Favor entrar una ciudad valida.");
			}
			if (GenericValidator.isBlankOrNull(newPostalState)
					|| !GenericValidator.matchRegexp(newPostalState, "^[A-Za-z ]+$")) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("newPostalState", "", "Please enter a valid state.");
				else
					errors.rejectValue("newPostalState", "", "Favor entrar un estado valido.");
			}
			if (GenericValidator.isBlankOrNull(newPostalZipCode1)
					|| !GenericValidator.matchRegexp(newPostalZipCode1, "^\\d{5}$")) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("newPostalZipCode2", "", "Please enter a valid zipcode.");
				else
					errors.rejectValue("newPostalZipCode2", "", "Favor entrar un c&oacute;digo postal valido.");
			} else if (!GenericValidator.isBlankOrNull(newPostalZipCode2)
					&& !GenericValidator.matchRegexp(newPostalZipCode2, "^\\d{4}$")) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("newPostalZipCode2", "", "Please enter a valid zipcode.");
				else
					errors.rejectValue("newPostalZipCode2", "", "Favor entrar un c&oacute;digo postal valido.");
			}
		}
//		}
		
		
		// Verify the old postal address in all cases
		if (GenericValidator.isBlankOrNull(oldPostalAddress1) || !CibpValidationUtils.isValidAddress(oldPostalAddress1)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("oldPostalAddress1", "", "Please enter a valid address.");
			else
				errors.rejectValue("oldPostalAddress1", "", "Favor entrar una direcci&oacute;n valida.");
		}
		if (!GenericValidator.isBlankOrNull(oldPostalAddress2)
				&& !CibpValidationUtils.isValidAddress(oldPostalAddress2)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("oldPostalAddress2", "", "Please enter a valid address.");
			else
				errors.rejectValue("oldPostalAddress2", "", "Favor entrar una direcci&oacute;n valida.");
		}
		if (GenericValidator.isBlankOrNull(oldPostalCity)
				|| !GenericValidator.matchRegexp(oldPostalCity, "^[A-Za-z ]+$")) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("oldPostalCity", "", "Please enter a valid city.");
			else
				errors.rejectValue("oldPostalCity", "", "Favor entrar una ciudad valida.");
		}
		if (GenericValidator.isBlankOrNull(oldPostalState)
				|| !GenericValidator.matchRegexp(oldPostalState, "^[A-Za-z ]+$")) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("oldPostalState", "", "Please enter a valid state.");
			else
				errors.rejectValue("oldPostalState", "", "Favor entrar un estado valido.");
		}
		if (GenericValidator.isBlankOrNull(oldPostalZipCode1)
				|| !GenericValidator.matchRegexp(oldPostalZipCode1, "^\\d{5}$")) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("oldPostalZipCode2", "", "Please enter a valid zipcode.");
			else
				errors.rejectValue("oldPostalZipCode2", "", "Favor entrar un c&oacute;digo postal valido.");
		} else if (!GenericValidator.isBlankOrNull(oldPostalZipCode2)
				&& !GenericValidator.matchRegexp(oldPostalZipCode2, "^\\d{4}$")) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				errors.rejectValue("oldPostalZipCode2", "", "Please enter a valid zipcode.");
			else
				errors.rejectValue("oldPostalZipCode2", "", "Favor entrar un c&oacute;digo postal valido.");
		}

		if(outreachFlag){
			if (apl == null) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					errors.rejectValue("appliedTo", "", "Please select the accounts do you want to change.");
				else
					errors.rejectValue("appliedTo", "",
							"Favor seleccionar las cuentas que reflejar&iacute;an el cambio.");
			} else if (apl == AppliedToAccount.SPECIFIC_ACCOUNTS) {
				if (bean.getAccounts() == null || bean.getAccounts().length == 0) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("accounts", "", "Please select the accounts do you want to change.");
					else
						errors.rejectValue("accounts", "",
								"Favor seleccionar las cuentas que reflejar&iacute;an el cambio.");
				}
			}
		}
		
//		This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//		{
		else // Only verify the accounts if the address type is POSTAL or BOTH
			if (addressChangeType.equals("POSTAL") || addressChangeType.equals("BOTH")) {
				if (apl == null) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						errors.rejectValue("appliedTo", "", "Please select the accounts do you want to change.");
					else
						errors.rejectValue("appliedTo", "",
								"Favor seleccionar las cuentas que reflejar&iacute;an el cambio.");
				} else if (apl == AppliedToAccount.SPECIFIC_ACCOUNTS) {
					if (bean.getAccounts() == null || bean.getAccounts().length == 0) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							errors.rejectValue("accounts", "", "Please select the accounts do you want to change.");
						else
							errors.rejectValue("accounts", "",
									"Favor seleccionar las cuentas que reflejar&iacute;an el cambio.");
					}
				}
			}
//		}
		
		if (errors.getAllErrors().size() == 0) {
			Boolean informationChallenge = false;
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)) {
				if (!userSession.getOobEnroll()) {
					informationChallenge = userSession.isInformationChallenge();
				} else {
					if (!userSession.isOobAuthenticated())
						userSession.setOobChallenge(true);
					informationChallenge = userSession.isOobAuthenticated();
				}
			} else {
				informationChallenge = userSession.isInformationChallenge();
			}
			
			if (informationChallenge != null && !informationChallenge) {

				String language = Utils.getRSALanguage(request);
				String username = userSession.getUsername();

				if (userSession.getInformationQuestion() == null && !userSession.getOobEnroll()) {

					if (informationChallenge != null && !informationChallenge) {

						DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), request);
						RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, Utils.getRSALanguage(request), userSession.getUsername(), EventType.CLIENT_DEFINED.getValue());
						AnalyzeResponse anResp = userSession.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

						deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData()
								.getDeviceTokenCookie());
						deviceRequest
								.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

						userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
						userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

						userSession.setDeviceRequest(deviceRequest);

						if (!(ActionCode.CHALLENGE == anResp.getRiskResult().getTriggeredRule().getActionCode())) {
							userSession.setInformationChallenge(true);
						} else {
							String email = null;
							if (userSession.getCustomerProfile() != null)
								email = userSession.getCustomerProfile().getEmail();

							QuestionResponse personalQuestion;

							if (userSession.getInformationQuestion() == null) {
								RSAActionRequest actionRequest = new RSAActionRequest(
										Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
										userSession.getUsername(), "", email, 0, userSession.isRemember(),
										userSession.getDeviceRequest());

								if (userSession.getCustomerProfile() != null)
									actionRequest.setUsername(userSession.getCustomerProfile().getUsername());
								else if (userSession.getUsernameReservation() != null)
									actionRequest.setUsername(userSession.getUsernameReservation().getUsername());
								else
									actionRequest.setUsername(username);

								actionRequest.setDeviceRequest(userSession.getDeviceRequest());
								actionRequest.setRsaSessionId(userSession.getRsaSessionId());
								actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());

								actionRequest.setRsaLang(Utils.getRSALanguage(request));

								personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(
										actionRequest);
								
								// MBSD-2000 RSA Upgrade Recommendations - Edit mailing address (Update device request)
								deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request);
								deviceRequest.setDeviceTokenCookie(personalQuestion.getDeviceRequest().getDeviceTokenCookie());
								deviceRequest.setDeviceTokenFSO(personalQuestion.getDeviceRequest().getDeviceTokenFSO());
								userSession.setDeviceRequest(deviceRequest);
								userSession.setRsaSessionId(personalQuestion.getSessionId());
								userSession.setRsaTransactionId(personalQuestion.getRsaTransactionId());

							} else {
								personalQuestion = new QuestionResponse();
								personalQuestion.setStatusResult(ResultStatus.SUCCESS);
								personalQuestion.setQuestion(userSession.getInformationQuestion());
							}
							if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
								userSession.setInformationQuestion(personalQuestion.getQuestion());

							} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
								throw new ModelAndViewDefiningException(new ModelAndView("error"));
							} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
								throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
							}
						}
					}
				}

				// //////////////////////

				errors.rejectValue("", "", "");
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put(AddressApplication.ACCOUNTS, bean.getAccounts());
				parameters.put(AddressApplication.APPLIED_TO, bean.getAppliedTo());
				
//				This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//				{
				if(!outreachFlag){
					parameters.put(ChallengeInformationUtils.ADDRESS_CHANGE_TYPE, addressChangeType);
					parameters.put(ChallengeInformationUtils.APPLIES_TO_BOTH, appliesToBoth);
					
					String residentialZipcode = residentialZipCode1;
					if (residentialZipCode2 != null && !residentialZipCode2.equalsIgnoreCase("")
							&& residentialZipCode2.length() == 4)
						residentialZipcode = residentialZipcode + "-" + residentialZipCode2;
					
					parameters.put(AddressApplication.RESIDENTIAL_ADDRESS1, bean.getResidentialAddress1());
					parameters.put(AddressApplication.RESIDENTIAL_ADDRESS2, bean.getResidentialAddress2());
					parameters.put(AddressApplication.RESIDENTIAL_CITY, bean.getResidentialCity());
					parameters.put(AddressApplication.RESIDENTIAL_STATE, bean.getResidentialState());
					parameters.put(AddressApplication.RESIDENTIAL_ZIPCODE, residentialZipcode);
					
				}
//				}
				
				String newPostalZipCode = newPostalZipCode1;
				if (newPostalZipCode2 != null && !newPostalZipCode2.equalsIgnoreCase("")
						&& newPostalZipCode2.length() == 4)
					newPostalZipCode = newPostalZipCode1 + "-" + newPostalZipCode2;

				String oldPostalZipCode = oldPostalZipCode1;
				if (oldPostalZipCode2 != null && !oldPostalZipCode2.equalsIgnoreCase("")
						&& oldPostalZipCode2.length() == 4)
					oldPostalZipCode = oldPostalZipCode + "-" + oldPostalZipCode2;

				parameters.put(AddressApplication.NEW_POSTAL_ADDRESS1, bean.getNewPostalAddress1());
				parameters.put(AddressApplication.NEW_POSTAL_ADDRESS2, bean.getNewPostalAddress2());
				parameters.put(AddressApplication.NEW_POSTAL_CITY, bean.getNewPostalCity());
				parameters.put(AddressApplication.NEW_POSTAL_STATE, bean.getNewPostalState());
				parameters.put(AddressApplication.NEW_POSTAL_ZIPCODE, newPostalZipCode);

				parameters.put(AddressApplication.OLD_POSTAL_ADDRESS1, bean.getOldPostalAddress1());
				parameters.put(AddressApplication.OLD_POSTAL_ADDRESS2, bean.getOldPostalAddress2());
				parameters.put(AddressApplication.OLD_POSTAL_CITY, bean.getOldPostalCity());
				parameters.put(AddressApplication.OLD_POSTAL_STATE, bean.getOldPostalState());
				parameters.put(AddressApplication.OLD_POSTAL_ZIPCODE, oldPostalZipCode);

				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
						ChallengeInformationUtils.ADDRESSES_ACTION);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, parameters);
			}
		}
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		boolean appliesToBoth = ServletRequestUtils.getBooleanParameter(request, "appliesToBoth", false);
		List<TVFrontendAccount> accts = userSession.getAccounts();
		Map<String, TVFrontendAccount> filteraccounts = new LinkedHashMap<String, TVFrontendAccount>();

		for (Iterator<TVFrontendAccount> iterator = accts.iterator(); iterator.hasNext();) {
			TVFrontendAccount frontendAccount = (TVFrontendAccount) iterator.next();
			if (filteraccounts.get(frontendAccount.getAccountNumber()) == null)
				filteraccounts.put(frontendAccount.getAccountNumber(), frontendAccount);
		}
		
		boolean outreachFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OUTREACH);
		
		if(outreachFlag){
			model.put("outreachFlag", true);
		}else{
			model.put("outreachFlag", false);
		}

		// Get the states
		Map<Object, String> states = new LinkedHashMap<Object, String>();
		states.put("PR", "PR");
		states.put("VI", "VI");
		states.put("BVI", "BVI");

		states.put("AL", "AL");
		states.put("AK", "AK");
		states.put("AZ", "AZ");
		states.put("AR", "AR");
		states.put("CA", "CA");
		states.put("CO", "CO");
		states.put("CT", "CT");
		states.put("DE", "DE");
		states.put("DC", "DC");
		states.put("FL", "FL");

		states.put("GA", "GA");
		states.put("HI", "HI");
		states.put("ID", "ID");
		states.put("IL", "IL");
		states.put("IN", "IN");
		states.put("IA", "IA");
		states.put("KS", "KS");
		states.put("KY", "KY");
		states.put("LA", "LA");
		states.put("ME", "ME");

		states.put("MD", "MD");
		states.put("MA", "MA");
		states.put("MI", "MI");
		states.put("MN", "MN");
		states.put("MS", "MS");
		states.put("MO", "MO");
		// cibp-2414 dsalas
		states.put("MT", "MT");
		states.put("MI", "MI");
		states.put("NE", "NE");
		states.put("NV", "NV");
		states.put("NH", "NH");

		states.put("NJ", "NJ");
		states.put("NM", "NM");
		states.put("NY", "NY");
		states.put("NC", "NC");
		states.put("ND", "ND");
		states.put("OH", "OH");
		states.put("OK", "OK");
		states.put("OR", "OR");
		states.put("PA", "PA");
		states.put("RI", "RI");

		states.put("SC", "SC");
		states.put("SD", "SD");
		states.put("TN", "TN");
		states.put("TX", "TX");
		states.put("UT", "UT");
		states.put("VT", "VT");
		states.put("VA", "VA");
		states.put("WA", "WA");
		states.put("WV", "WV");
		states.put("WI", "WI");
		states.put("WY", "WY");

		model.put("states", states);

		// Collection<TVFrontendAccount> col = filteraccounts.values();

		model.put("customerAccounts", filteraccounts.values());
		model.put("appliesToBoth", appliesToBoth);

		// CustomerProfile customer = userSession.getCustomerProfile();
		// CustomerServices customerService = userSession.getCustomerService();

		model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, userSession.isInformationChallenge());

		// FFEIC + SMS
		String username = userSession.getUsername();
		model.put("username", username);

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setPath("/");
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		if (cookie != null)
			model.put("devicetoken", cookie.getValue());

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)) {
			if (!userSession.getOobEnroll()) {
				if (userSession.getInformationQuestion() != null) {
					String question = (String) userSession.getInformationQuestion().values().toArray()[0];
					model.put("question", question);
				} 
			} else {
				if (!userSession.isOobAuthenticated() && userSession.getOobChallenge() != null && userSession.getOobChallenge()) {
					model.put("oobchallenge", true);
					List<ASK_FOR_OOB> listNavOOB = new ArrayList<ASK_FOR_OOB>();
					listNavOOB.add(ASK_FOR_OOB.OOB_CHALLENGE);
					listNavOOB.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
					userSession.setTasksListOOB(listNavOOB);
					userSession.setOobChallenge(false);
				}
			}
		} else {
			if (userSession.getInformationQuestion() != null) {
				String question = (String) userSession.getInformationQuestion().values().toArray()[0];
				model.put("question", question);
			} 
		}
		
		// /////////////////////////////////////

		return model;
	}
	
	public long getMaxTimeModifyAddress() {
		return maxTimeModifyAddress;
	}

	public void setMaxTimeModifyAddress(long maxTimeModifyAddress) {
		this.maxTimeModifyAddress = maxTimeModifyAddress;
	}

}
