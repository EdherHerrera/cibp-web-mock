package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import bppr.tv.beans.accounts.types.DepositAccount;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.entitlement.ATHMovilEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ATHMSendBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AthmUtils;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.athm.OsPhoneInfo;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.AthmMakeTransferReq;
import evertec.otherservices.requests.AthmStatusReq;
import evertec.otherservices.responses.AthmMakeTransferResp;
import evertec.otherservices.responses.AthmStatusResp;

public class ATHMSendMoneyForm extends AbstractWizardFormController implements MessageSourceAware {
	protected final Log logger = LogFactory.getLog(getClass());
	protected CustomerServices customerService = null;	
	private MessageSource messageSource = null;
	private String fiId = null;
	private String fiToken = null;
	private String maxDailyTransfer;
	private String balanceDelay = "";
	private static int MAX_MESSAGE_CHARS = 45;

	public ATHMSendMoneyForm() {
		super();
		setCommandClass(ATHMSendBean.class);
		setCommandName("athmsendmoney");
		setPages(new String[] { "athmsendmoney", "athmsendmoney", "athmsendmoney" });
	}
    
	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		ATHMSendBean athmSendBean = (ATHMSendBean) super.formBackingObject(request);
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		UserAgentType agentType = UserAgentType.WEB;
		
		// Use new Flag to present the View Type
		if (userSession != null && userSession.getUserAgentType() != null) {
			agentType = userSession.getUserAgentType();
			if (agentType == UserAgentType.JSON) {
				athmSendBean.setMobileAppType(agentType.toString());
				athmSendBean.setMobileApp(Boolean.TRUE);
			}
		}
		
		return athmSendBean;
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		Map<String, Object> model = new HashMap<>();//model result
		UserSession userSession = new UserSession();//user session
		final String strUserSession = "userSession";//user session string

		if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
			userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
		}
		ATHMSendBean athmSendBean = (ATHMSendBean) command;
		userSession.setLoadebpp(false);
		model.put("maxDailyTransfer", maxDailyTransfer);

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		GlobalEntitlement entitlement = customerService.getGlobalEntitlement(EntitlementType.ATHMOVIL);
		model.put("page", page);

		if (athmSendBean != null && athmSendBean.getPortal() == null)
			athmSendBean.setPortal("Y".equalsIgnoreCase(request.getParameter("portal")));
		
		ATHMovilEntitlement athmEntitlement = new ATHMovilEntitlement(userSession.getCustomerProfile()
				.getEntitlementByType(EntitlementType.ATHMOVIL));
		
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.ATHM_SSO) && athmEntitlement.getPhoneNumber() == null 
				&& athmSendBean != null) {
			String language = "sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "es" : Utils.getRSALanguage(request);
			Locale locale = new Locale(language);
			
			athmSendBean.setErrorFound(true);
			athmSendBean.setError(messageSource.getMessage("athmobile.apps.sso.update.message", null, locale));
			return model;
		}

		if (entitlement == null || entitlement.getStatus() != EntitlementStatus.ENABLED) {
			model.put("down", "true");
			return model;
		}

		if (page == 0) {
			if (athmSendBean != null) {
				athmSendBean.setAccount(athmEntitlement.getPlasticAccount());
				loadAccountData(request, command, model);
				athmSendBean.setDisplayAccount((String) model.get("account"));

				if (athmSendBean.getFriend() != null && !athmSendBean.getFriend().equals("other")) {
					athmSendBean.setFriend(AthmUtils.formatPhoneNumber(athmSendBean.getFriend()));
				}
			}
      
      // In mobile apps it works differently. 
      // Need fee info on first request to send money form
      
      if(athmSendBean.isMobileApp())
        model.put("fee", "$" + userSession.getAthmServiceCharge("DISPLAY").getServiceCharge().toString());
			
      loadFriendsData(request, command, model);
			model.put("unregistered", userSession.isAthmUnregistered());
		} else if (page == 1 || page == 2) {
			if (athmSendBean != null) {
				String phoneNumber = null;
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

				model.put("reference", athmSendBean.getReferenceNumber());
				model.put("from", athmSendBean.getDisplayAccount());
				if (athmSendBean.getFriend().equalsIgnoreCase("other")) {
					phoneNumber = AthmUtils.formatPhoneNumberDashesOnly(athmSendBean.getPhoneNumber());
				} else {
					loadFriendsData(request, command, model);
					phoneNumber = athmSendBean.getFriend();
					if (!athmSendBean.isMobileApp()) {
						@SuppressWarnings("unchecked")
						String name = ((Map<String, String>) model.get("friendsMap")).get(AthmUtils
								.formatPhoneNumber(phoneNumber));
						phoneNumber = name + " " + AthmUtils.formatPhoneNumberDashesOnly(phoneNumber);
					} else {
						phoneNumber = AthmUtils.formatPhoneNumberDashesOnly(phoneNumber);
					}
				}
				model.put("to", phoneNumber);
				model.put("amount", AthmUtils.formatAmount(new BigDecimal(athmSendBean.getAmount())));
				model.put("date", sdf.format(new Date()));
				model.put("fee", "$" + userSession.getAthmServiceCharge("DISPLAY").getServiceCharge().toString());
				model.put("message", AthmUtils.divideLongMessageWord(athmSendBean.getMessage()));
				model.put("errors", errors.hasFieldErrors());
				String infoMsg = (String)WebUtils.getSessionAttribute(request, "infoMsg");
				if(infoMsg != null){
					WebUtils.setSessionAttribute(request, "infoMsg", null);
					model.put("infoMsg", infoMsg);
				}
				else{
					model.put("infoMsg", "null");
				}
			}
		}
		return model;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		ATHMSendBean athmSendBean = (ATHMSendBean) command;
		
		String language = "sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "es" : Utils.getRSALanguage(request);
		Locale locale = new Locale(language);

		TVFrontendAccount frontendAccount = null;
		BigDecimal balance = null;
		boolean error = false;

		String phoneNumber = "";
		
		if (athmSendBean.isMobileApp()){
			phoneNumber = athmSendBean.getPhoneNumber();
			athmSendBean.setFriend(athmSendBean.getPhoneNumber());
			
			athmSendBean.setError("");
			athmSendBean.setErrorFound(false);
		} else {
			if (athmSendBean.getFriend().equalsIgnoreCase("other")) {
				phoneNumber = athmSendBean.getPhoneNumber();
			} else {
				phoneNumber = athmSendBean.getFriend();
			}
		}

		switch (page) {
		case 0:
			BigDecimal amount = null;
			try {
				amount = new BigDecimal(athmSendBean.getAmount());
			} catch (NumberFormatException e) {
				amount = null;
			}

			if (athmSendBean.getPortal()) {
				if (athmSendBean.getFriend().equals("none")) {
					errors.rejectValue("phoneNumber", "athmovil.phone.required", "*");
					error = true;
				} else {
					if (athmSendBean.getFriend().equals("other")
							&& GenericValidator.isBlankOrNull(athmSendBean.getPhoneNumber())) {
						errors.rejectValue("phoneNumber", "athmovil.phone.required", "*");
						error = true;
					}
				}
				if (amount == null && !GenericValidator.isBlankOrNull(athmSendBean.getAmount())) {
					errors.rejectValue("amount", "athmovil.amount.invalid", "Invalid.");
					error = true;
				} else if (amount == null) {
					errors.rejectValue("amount", "athmovil.amount.required", "*");
					error = true;
				} else if (amount.doubleValue() < 1) {
					errors.rejectValue("amount", "athmovil.amount.limit.minimum.transfer", "Minimum Limit");
					error = true;
				}
				if (!GenericValidator.isBlankOrNull(athmSendBean.getMessage())
						&& athmSendBean.getMessage().length() > MAX_MESSAGE_CHARS) {
					errors.rejectValue("message", "athmovil.message.invalid", "*");
					error = true;
				}

				if (!error) {
					ATHMovilEntitlement entitlement = new ATHMovilEntitlement(userSession.getCustomerProfile()
							.getEntitlementByType(EntitlementType.ATHMOVIL));
					if (AthmUtils.getNumericPhoneNumber(phoneNumber) == null || phoneNumber.startsWith("0")) {
						errors.rejectValue("phoneNumber", "athmovil.phone.invalid", "Invalid phone number");
					} else if (AthmUtils.getNumericPhoneNumber(phoneNumber).equals(
							AthmUtils.getNumericPhoneNumber(entitlement.getPhoneNumber())))
						errors.rejectValue("phoneNumber", "athmovil.phone.invalidphone",
								"The number must be different from the one you are subscribed with.");
					if (amount.doubleValue() < new Double(0.01d))
						errors.rejectValue("amount", "athmovil.amount.invalid", "Invalid amount");
					else {
						frontendAccount = AthmUtils.getAccountByAccountNumber(athmSendBean.getAccount(), userSession);
						if (frontendAccount != null) {
							balance = frontendAccount.getAvailableBalance();
							if (balance != null) {
								if (balance.compareTo(amount) < 0) {
									errors.rejectValue("amount", "athmovil.amount.funds",
											"Account has insufficient funds");
								}
							} else {
								balance = frontendAccount.getPortalBalance();

								if (balance != null) {
									if (balance.compareTo(amount) < 0) {
										errors.rejectValue("amount", "athmovil.amount.funds",
												"Account has insufficient funds");
									}
								}
							}
						} else {
							errors.rejectValue("error", "athmovil.account.from.required",
									"Account is required. Please choose an account in Manage Profile.");
						}
					}
				}
			} else // Portal box
			{
				// Required fields validations
				if (athmSendBean.getFriend().equals("none")) {
					errors.rejectValue("friend", "", "*");
					error = true;
				} else {
					if (athmSendBean.getFriend().equals("other")
							&& GenericValidator.isBlankOrNull(athmSendBean.getPhoneNumber())) {
						errors.rejectValue("phoneNumber", "", "*");
						error = true;
					}
				}

				if (GenericValidator.isBlankOrNull(athmSendBean.getAmount())) {
					errors.rejectValue("amount", "", "*");
					error = true;
				}

				if (error) {
					errors.rejectValue("error", "athmobile.portal.box.enroll.required2", "*Required");
				} else { // Invalid data conditions
					if (amount == null || (amount.doubleValue() < new Double(0.01d))) {
						errors.rejectValue("amount", "", "*");
						error = true;
					} else if (amount.doubleValue() < 1) {
						errors.rejectValue("amount", "", "*");
						errors.rejectValue("error", "athmovil.amount.limit.minimum.transfer", "Minimum Limit");
						
						athmSendBean.setError(messageSource.getMessage("athmovil.amount.limit.minimum.transfer", null, locale));
						athmSendBean.setErrorFound(true);
						return;
					}

					if (AthmUtils.getNumericPhoneNumber(phoneNumber) == null || phoneNumber.startsWith("0")) {
						errors.rejectValue("phoneNumber", "", "*");
						error = true;
						
						athmSendBean.setError(messageSource.getMessage("athmobile.apps.enroll.step1.phone.invalid", null, locale));
						athmSendBean.setErrorFound(true);
					}
					if (!GenericValidator.isBlankOrNull(athmSendBean.getMessage())
							&& athmSendBean.getMessage().length() > MAX_MESSAGE_CHARS) {
						errors.rejectValue("message", "", "*");
						error = true;
					}

					if (error) {
						if (!GenericValidator.isBlankOrNull(athmSendBean.getMessage())
								&& athmSendBean.getMessage().length() > MAX_MESSAGE_CHARS)
							errors.rejectValue("error", "athmovil.message.invalid", "Invalid");
						else
							errors.rejectValue("error", "athmovil.phone.invalid", "Invalid");
					} else {
						frontendAccount = AthmUtils.getAccountByAccountNumber(athmSendBean.getAccount(), userSession);
						if (frontendAccount != null) {
							balance = frontendAccount.getAvailableBalance();
							if (balance != null) {
								if (balance.compareTo(amount) < 0) {
									errors.rejectValue("error", "athmovil.amount.funds",
											"Account has insufficient funds");
									
									athmSendBean.setError(messageSource.getMessage("athmovil.amount.funds", null, locale));
									athmSendBean.setErrorFound(true);
								}
							} else {
								balance = frontendAccount.getPortalBalance();

								if (balance != null) {
									if (balance.compareTo(amount) < 0) {
										errors.rejectValue("error", "athmovil.amount.funds",
												"Account has insufficient funds");
										
										athmSendBean.setError(messageSource.getMessage("athmovil.amount.funds", null, locale));
										athmSendBean.setErrorFound(true);
									}
								} else {
									balance = frontendAccount.getPortalBalance();

									if (balance != null) {
										if (balance.compareTo(amount) < 0) {
											errors.rejectValue("error", "athmovil.amount.funds",
													"Account has insufficient funds");
											
											athmSendBean.setError(messageSource.getMessage("athmovil.amount.funds", null, locale));
											athmSendBean.setErrorFound(true);
										}
									}
								}
							}
						} else {
							errors.rejectValue("error", "athmovil.account.from.required",
									"Account is required. Please choose an account in Manage Profile.");
							
							athmSendBean.setError(messageSource.getMessage("athmovil.account.from.required", null, locale));
							athmSendBean.setErrorFound(true);
						}
					}
				}
			}
			break;
		case 1:
			try {
				ATHMovilEntitlement entitlement = new ATHMovilEntitlement(userSession.getCustomerProfile()
						.getEntitlementByType(EntitlementType.ATHMOVIL));
				
				AthmMakeTransferResp status = null;
				
				boolean updatedPlastic = false;
				
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT134)){
					Integer expDate = Integer.valueOf(entitlement.getPlasticExpirationDate());
					SimpleDateFormat yearMonthFormat = new SimpleDateFormat("yyyyMM");
					Integer currentDate = Integer.valueOf(yearMonthFormat.format(Calendar.getInstance().getTime()));
					//IF ENTITLEMENT EXPIRATION DATE HAS EXPIRED DATA IS UPDATE BEFORE ATHM SERVICES
					if(expDate < currentDate){
						String oldPlastic = entitlement.getPlasticNumber();
						boolean success = updateNewPlasticInformation(entitlement, userSession);
						String newPlastic = entitlement.getPlasticNumber();
						if(!success){
							String errorMsg = messageSource.getMessage("athmovil.no.backup.card", null, locale);
							errorMsg = errorMsg.replace("XXXX", "x" + oldPlastic.substring(oldPlastic.length() - 4));
							errors.rejectValue("error", null, errorMsg);
							logger.error("SendMoney: Error updating primary card");
							athmSendBean.setError(errorMsg);
							athmSendBean.setErrorFound(true);
							return;
						}
						else{
							if(!oldPlastic.equals(newPlastic)){
								if(newPlastic.startsWith("4549")){
									if(oldPlastic.startsWith("4549")){
										String infoMsg = messageSource.getMessage("athmovil.from.int.to.int.card", null, locale);
										infoMsg = infoMsg.replaceFirst("XXXX", "x" + oldPlastic.substring(oldPlastic.length() - 4));
										infoMsg = infoMsg.replace("XXXX", "x" + newPlastic.substring(oldPlastic.length() - 4));
										WebUtils.setSessionAttribute(request, "infoMsg", infoMsg);
									}
									else{
										String infoMsg = messageSource.getMessage("athmovil.from.regular.to.int.card", null, locale);
										infoMsg = infoMsg.replaceFirst("XXXX", "x" + oldPlastic.substring(oldPlastic.length() - 4));
										infoMsg = infoMsg.replace("XXXX", "x" + newPlastic.substring(oldPlastic.length() - 4));
										WebUtils.setSessionAttribute(request, "infoMsg", infoMsg);
									}
								}
								else{
									if(oldPlastic.startsWith("4549")){
										String infoMsg = messageSource.getMessage("athmovil.from.int.to.regular.card", null, locale);
										infoMsg = infoMsg.replaceFirst("XXXX", "x" + oldPlastic.substring(oldPlastic.length() - 4));
										infoMsg = infoMsg.replace("XXXX", "x" + newPlastic.substring(oldPlastic.length() - 4));
										WebUtils.setSessionAttribute(request, "infoMsg", infoMsg);
									}
									else{
										String infoMsg = messageSource.getMessage("athmovil.from.regular.to.regular.card", null, locale);
										infoMsg = infoMsg.replaceFirst("XXXX", "x" + oldPlastic.substring(oldPlastic.length() - 4));
										infoMsg = infoMsg.replace("XXXX", "x" + newPlastic.substring(oldPlastic.length() - 4));
										WebUtils.setSessionAttribute(request, "infoMsg", infoMsg);
									}
								}
							}
						}
					}
					// We need to refresh the plastic in the Entitlement before Sending Money.
					updatedPlastic = updatePrimaryCard(userSession);
				}
				else{
					// We need to refresh the plastic in the Entitlement before Sending Money.
					updatedPlastic = updatePrimaryCard(userSession);
					
				}

				if (!updatedPlastic) {
					errors.rejectValue("error", "athmovil.amount.failed", "Unable to complete transfer");
					logger.error("SendMoney: Error updating primary card");
					athmSendBean.setError(messageSource.getMessage("athmovil.amount.failed", null, locale));
					athmSendBean.setErrorFound(true);
				} else {
					status = sendMoney(AthmUtils.getNumericPhoneNumber(phoneNumber),
							new BigDecimal(athmSendBean.getAmount()), athmSendBean.getMessage(), entitlement,
							userSession.getAthmServiceCharge("CHARGE").getServiceCharge());
					
					if (status == null){
						errors.rejectValue("error", "athmovil.amount.failed", "Unable to complete transfer");
						athmSendBean.setError(messageSource.getMessage("athmovil.amount.failed", null, locale));
						athmSendBean.setErrorFound(true);
					}
					else {
						if (status.getResponseStatus().equals(ResponseStatus.ERROR)) {
							logger.error("Send money failed. ATH Movil call generated an exception");
							errors.rejectValue("error", "athmovil.error.servicedown",
									"The service is not available at this moment. Please try again later.");
							athmSendBean.setError(messageSource.getMessage("athmovil.error.servicedown", null, locale));
							athmSendBean.setErrorFound(true);
						} else // Send money
								// ------------------------------------------------------
						{
							if ("32".equals(status.getStatusCode())) { // Expiration
																		// Date
								updatePlasticInformation(entitlement, userSession);

								status = sendMoney(AthmUtils.getNumericPhoneNumber(phoneNumber), new BigDecimal(
										athmSendBean.getAmount()), athmSendBean.getMessage(), entitlement, userSession
										.getAthmServiceCharge("CHARGE").getServiceCharge());
							}
							if (status.getResponseStatus() != null
									&& status.getResponseStatus().equals(ResponseStatus.SUCCESS)
									&& ("200".equals(status.getStatusCode()) // Success
									|| "26".equals(status.getStatusCode()))) // Pending
							{
								athmSendBean.setReferenceNumber(status.getReferenceNumber());
								userSession.resetATHMTransferInfoHash();
								// update balance after transaction
								logger.debug("-- ATHM: Send money successful. Call update acct balance");
								List<TVFrontendAccount> frontEndAccounts2Update = new LinkedList<TVFrontendAccount>();
								TVFrontendAccount frontendAccountToUpdate = Utils.getAccountByAccountNumber(
										entitlement.getPlasticAccount(), userSession.getAccounts());
								frontEndAccounts2Update.add(frontendAccountToUpdate);
								userSession.updateAccountBalance(frontEndAccounts2Update,balanceDelay);
							}
							// Case when quantity in transaction is more than
							// the allowed daily limit
							else if (status.getResponseStatus() != null
									&& status.getResponseStatus().equals(ResponseStatus.SUCCESS)
									&& "48".equals(status.getStatusCode())) {

								String maxReachedMessage = messageSource.getMessage("athmovil.amount.limit.transfer",
										null, "", locale);
								maxReachedMessage = maxReachedMessage.replace("(?)", maxDailyTransfer);

								errors.rejectValue("error", "", maxReachedMessage);
								logger.info("Transfer amount limit reached");
								
								athmSendBean.setError(maxReachedMessage);
								athmSendBean.setErrorFound(true);
							}
							// Case when accumulated transactions reached the
							// daily limit
							else if (status.getResponseStatus() != null
									&& status.getResponseStatus().equals(ResponseStatus.SUCCESS)
									&& "47".equals(status.getStatusCode())) {
								errors.rejectValue("error", "athmovil.amount.limit.daily",
										"Daily transfer amount limit has been reached.");
								logger.info("Daily amount limit reached");
								
								athmSendBean.setError(messageSource.getMessage("athmovil.amount.limit.daily", null, locale));
								athmSendBean.setErrorFound(true);
								
							} else {
								errors.rejectValue("error", "athmovil.amount.failed", "Unable to complete transfer");
								logger.error("Error sending money: ATHM status: " + status.getStatusDescription());

								athmSendBean.setError(messageSource.getMessage("athmovil.amount.failed", null, locale));
								athmSendBean.setErrorFound(true);
							}
						}
					}
					userSession.resetTransfers();
				}
				// Customer audit log
				if (errors.getErrorCount() > 0) {
					userSession.getCustomerService().logMakeAthmTransferError(
							Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));
				} else {
					userSession.getCustomerService().logMakeAthmTransfer(
							Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));
				}
			} catch (Exception e) {
				errors.rejectValue("error", "athmovil.amount.failed", "Unable to complete transfer");
				logger.error("Error sending money: " + e.getMessage(), e);
				userSession.getCustomerService().logMakeAthmTransferError(
						Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));
				
				athmSendBean.setError(messageSource.getMessage("athmovil.amount.failed", null, locale));
				athmSendBean.setErrorFound(true);
			}
			break;

		default:
			break;
		}
	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		return proceessCancelFinishResult(request);
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		return proceessCancelFinishResult(request);
	}

	private void loadAccountData(HttpServletRequest request, Object command, Map<String, Object> model) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		ATHMovilEntitlement entitlement = new ATHMovilEntitlement(userSession.getCustomerProfile()
				.getEntitlementByType(EntitlementType.ATHMOVIL));

		String id = entitlement.getPlasticAccount();
		String accountDisplay = "";

		if (id != null && id.trim().length() > 0) {
			TVFrontendAccount frontendAccount = AthmUtils.getAccountByAccountNumber(id, userSession);

			if (frontendAccount != null) {
				Map<String, String> accountDetail = AthmUtils.getAccountDetails(frontendAccount);
				model.put("accountDetail", accountDetail);
        
        if (accountDetail != null)
					accountDisplay = accountDetail.get("accountName") + " " + accountDetail.get("accountLast4Num");
			}
		}
     
		model.put("account", accountDisplay);
	}

	private void loadFriendsData(HttpServletRequest request, Object command, Map<String, Object> model) {
		Map<String, String> friendsMap = new LinkedHashMap<String, String>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		ATHMovilEntitlement entitlement = new ATHMovilEntitlement(userSession.getCustomerProfile()
				.getEntitlementByType(EntitlementType.ATHMOVIL));

		String language = "sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "es" : Utils.getRSALanguage(request);
		Locale locale = new Locale(language);

		friendsMap.put("none", messageSource.getMessage("athmovil.profile.phone.default", null, locale));
		friendsMap.put("other", messageSource.getMessage("athmovil.profile.phone.other", null, locale));

		OsPhoneInfo[] contactList = userSession.getAthmContacts(fiId, fiToken, entitlement.getUserId());
		if (contactList != null) {
			for (OsPhoneInfo contact : contactList) {
				friendsMap.put(AthmUtils.formatPhoneNumber(contact.getPhoneNumber()), contact.getName().trim());
			}
		}
		model.put("friendsMap", friendsMap);
	}

	private void updatePlasticInformation(ATHMovilEntitlement entitlement, UserSession userSession) {

		TVFrontendAccount frontendAccount = Utils.getAccountByAccountNumber(entitlement.getPlasticAccount(),
				userSession.getAccounts());

		TVFrontendAccount frontendCardPlasticAccount = null;

		AbstractAccount accountDetails = userSession.getCustomerService().getCardPlasticAccount(
				userSession.getCustomerProfile(), userSession.getBankingSession(), frontendAccount, true);

		DepositAccount depositAccount = (DepositAccount) accountDetails.getBackendObject();
		List<TVFrontendAccount> cardPlasticAccountList = userSession.getCardPlasticAccounts();
		CardPlasticAccountRecord[] cardPlasticAccountRecordList = depositAccount.getCardPlasticAccountRecords();
		CardPlasticAccountRecord cardPlasticAccountRecord = null;

		if (cardPlasticAccountList != null && cardPlasticAccountRecordList != null) {
			for (int i = 0; i < cardPlasticAccountRecordList.length; i++) {
				cardPlasticAccountRecord = cardPlasticAccountRecordList[i];
				for (Iterator<TVFrontendAccount> iterator = cardPlasticAccountList.iterator(); iterator.hasNext();) {
					frontendCardPlasticAccount = iterator.next();
					if (cardPlasticAccountRecord.getCardPANId() != null && cardPlasticAccountRecord.getCardPANId()
							.equals(frontendCardPlasticAccount.getAccountNumber())) {
						break;
					} else
						frontendCardPlasticAccount = null;
				}
				if (frontendCardPlasticAccount != null)
					break;
			}

			if (frontendCardPlasticAccount != null) {
				accountDetails = userSession.getCustomerService().getCardPlasticAccount(
						userSession.getCustomerProfile(), userSession.getBankingSession(), frontendCardPlasticAccount,
						true);

				depositAccount = (DepositAccount) accountDetails.getBackendObject();

				cardPlasticAccountRecordList = depositAccount.getCardPlasticAccountRecords();

				if (cardPlasticAccountRecordList != null && cardPlasticAccountRecordList.length > 0) {
					cardPlasticAccountRecord = cardPlasticAccountRecordList[0];

					if (cardPlasticAccountRecord.getExpirationDate() != null) {
						SimpleDateFormat yearMonthFormat = new SimpleDateFormat("yyyyMM");
						Calendar calendar = cardPlasticAccountRecord.getExpirationDate();
						entitlement.setPlasticExpirationDate(yearMonthFormat.format(calendar.getTime()));
					}
					if (cardPlasticAccountRecord.getCardPANId() != null
							&& cardPlasticAccountRecord.getCardPANId().trim().length() > 0) {
						entitlement.setPlasticNumber(cardPlasticAccountRecord.getCardPANId());
					}
					
					CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(entitlement.getCustomerEntitlement()));
				}
			}
		}
	}
	
	private Boolean updateNewPlasticInformation(ATHMovilEntitlement entitlement, UserSession userSession) {
		boolean success = false;
				
		CardPlasticAccountRecord ath = null;
		AbstractAccount accountDetails = null;
		DepositAccount depositAccount = null;
		CardPlasticAccountRecord[] cardPlasticAccountRecordList = null;
		//TRYING TO GET THE SAME ATH CARD
		for(int j = 0; j < userSession.getCardPlasticAccounts().size(); j++){
			TVFrontendAccount plasticAccount = userSession.getCardPlasticAccounts().get(j);
			if(entitlement.getPlasticNumber().equals(plasticAccount.getAccountNumber())){
				accountDetails = userSession.getCustomerService().getCardPlasticAccount(
						userSession.getCustomerProfile(), userSession.getBankingSession(),plasticAccount, true);
				if(accountDetails != null && accountDetails.getBackendObject() != null){
					depositAccount = (DepositAccount) accountDetails.getBackendObject();
					cardPlasticAccountRecordList = depositAccount.getCardPlasticAccountRecords();
					if(cardPlasticAccountRecordList != null){
						for (int k = 0; k < cardPlasticAccountRecordList.length; k++) {
							if(entitlement.getPlasticNumber().equals(cardPlasticAccountRecordList[k].getCardPANId())){
								ath = cardPlasticAccountRecordList[k];
								break;
							}
						}
					}
				}
			}
			if(ath != null){
				break;
			}
		}
		if(ath != null){
			success = this.updateEntitlement(ath, userSession, entitlement);
		}
		//ATH CARD WAS NOT FOUND, LOOKING FOR ANOTHER VALID ATH CARD RELATED TO THE DEPOSIT ACCOUNT
		//IF ATH CARD IN ENTITLEMENT IS REGULAR LOOK FOR A VALID REGULAR ATH, IF NOT FOUND TRY ALTERNATIVE INTERNATIONAL
		//IF ATH CARD IN ENTITLEMENT IS INTERNATIONAL LOOK FOR A VALID INTERNATIONAL ATH, IF NOT FOUND TRY ALTERNATIVE REGULAR
		if(ath == null || !success){
			TVFrontendAccount frontendAccount = Utils.getAccountByAccountNumber(entitlement.getPlasticAccount(),
					userSession.getAccounts());
			TVFrontendAccount frontendCardPlasticAccount = null;
			TVFrontendAccount alternativePlastic = null;
			accountDetails = userSession.getCustomerService().getCardPlasticAccount(
					userSession.getCustomerProfile(), userSession.getBankingSession(), frontendAccount, true);
			depositAccount = (DepositAccount) accountDetails.getBackendObject();
			List<TVFrontendAccount> cardPlasticAccountList = userSession.getCardPlasticAccounts();
			cardPlasticAccountRecordList = depositAccount.getCardPlasticAccountRecords();
			CardPlasticAccountRecord cardPlasticAccountRecord = null;
			
			boolean athRegularEnt = (entitlement.getPlasticNumber().startsWith("0215")) ? true : false;

			if (cardPlasticAccountList != null && cardPlasticAccountRecordList != null) {
				for (int i = 0; i < cardPlasticAccountRecordList.length; i++) {
					cardPlasticAccountRecord = cardPlasticAccountRecordList[i];
					for (Iterator<TVFrontendAccount> iterator = cardPlasticAccountList.iterator(); iterator.hasNext();) {
						frontendCardPlasticAccount = iterator.next();

						if (cardPlasticAccountRecord.getCardPANId() != null
								&& cardPlasticAccountRecord.getCardPANId().equals(
										frontendCardPlasticAccount.getAccountNumber())) {
							
							break;
						} else
							frontendCardPlasticAccount = null;
					}
					if (frontendCardPlasticAccount != null){
						//SAVE THE FIRST OPTION IN CASE NO MATCH BETWEEN REGULAR OR INTERNATIONAL ATH CARDS
						if(alternativePlastic == null){
							alternativePlastic = frontendCardPlasticAccount;
						}
						if(athRegularEnt){
							if (frontendCardPlasticAccount.getAccountNumber().startsWith("0215")){
								accountDetails = userSession.getCustomerService().getCardPlasticAccount(
										userSession.getCustomerProfile(), userSession.getBankingSession(), frontendCardPlasticAccount,
										true);

								depositAccount = (DepositAccount) accountDetails.getBackendObject();

								cardPlasticAccountRecordList = depositAccount.getCardPlasticAccountRecords();

								if (cardPlasticAccountRecordList != null && cardPlasticAccountRecordList.length > 0) {
									cardPlasticAccountRecord = cardPlasticAccountRecordList[0];
									success = this.updateEntitlement(cardPlasticAccountRecord, userSession, entitlement);;
									break;
								}
							}
						}
						else{
							if (frontendCardPlasticAccount.getAccountNumber().startsWith("4549")){
								accountDetails = userSession.getCustomerService().getCardPlasticAccount(
										userSession.getCustomerProfile(), userSession.getBankingSession(), frontendCardPlasticAccount,
										true);

								depositAccount = (DepositAccount) accountDetails.getBackendObject();

								cardPlasticAccountRecordList = depositAccount.getCardPlasticAccountRecords();

								if (cardPlasticAccountRecordList != null && cardPlasticAccountRecordList.length > 0) {
									cardPlasticAccountRecord = cardPlasticAccountRecordList[0];
									success = this.updateEntitlement(cardPlasticAccountRecord, userSession, entitlement);;
									break;
								}
							}
						}
					}
				}
				if(!success && alternativePlastic != null){
					accountDetails = userSession.getCustomerService().getCardPlasticAccount(
							userSession.getCustomerProfile(), userSession.getBankingSession(), alternativePlastic,
							true);

					depositAccount = (DepositAccount) accountDetails.getBackendObject();

					cardPlasticAccountRecordList = depositAccount.getCardPlasticAccountRecords();

					if (cardPlasticAccountRecordList != null && cardPlasticAccountRecordList.length > 0) {
						cardPlasticAccountRecord = cardPlasticAccountRecordList[0];
						success = this.updateEntitlement(cardPlasticAccountRecord, userSession, entitlement);;
					}
				}
			}
		}
		return success;
	}
	
	private boolean updateEntitlement(CardPlasticAccountRecord cardPlasticAccountRecord, UserSession userSession, ATHMovilEntitlement entitlement){
		if (cardPlasticAccountRecord.getExpirationDate() != null) {
			if(cardPlasticAccountRecord.getExpirationDate().getTime().compareTo(Calendar.getInstance().getTime()) < 0 ){
				return false;
			}
			SimpleDateFormat yearMonthFormat = new SimpleDateFormat("yyyyMM");
			Calendar calendar = cardPlasticAccountRecord.getExpirationDate();
			entitlement.setPlasticExpirationDate(yearMonthFormat.format(calendar.getTime()));
		}
		if (cardPlasticAccountRecord.getCardPANId() != null
				&& cardPlasticAccountRecord.getCardPANId().trim().length() > 0) {
			entitlement.setPlasticNumber(cardPlasticAccountRecord.getCardPANId());
		}
	
		CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(entitlement.getCustomerEntitlement()));
		return true;
	}
	
	private boolean updatePrimaryCard(UserSession userSession) {
		try {
			AthmStatusResp updateAccount = null;
			ATHMovilEntitlement entitlement = new ATHMovilEntitlement(userSession.getCustomerProfile()
					.getEntitlementByType(EntitlementType.ATHMOVIL));

			AthmStatusReq updateAcctReq = new AthmStatusReq();
			updateAcctReq.setAccSection(entitlement.getPlasticAccountType());
			updateAcctReq.setExpDate(entitlement.getPlasticExpirationDate());
			updateAcctReq.setFiID(fiId);
			updateAcctReq.setFiToken(fiToken);
			updateAcctReq.setPlasticNumber(entitlement.getPlasticNumber());
			updateAcctReq.setUserId(entitlement.getUserId());
			
			updateAccount = customerService.changePrimaryCard(updateAcctReq);

			if (updateAccount == null || updateAccount.getResponseStatus() == null
					|| !updateAccount.getResponseStatus().equals(ResponseStatus.SUCCESS)
					|| !("200".equals(updateAccount.getStatusCode()))) {
				return false;
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private AthmMakeTransferResp sendMoney(Long phoneNumber, BigDecimal amount, String message,
			ATHMovilEntitlement entitlement, BigDecimal serviceCharge) {

		AthmMakeTransferResp mkTransferResp = null;
		AthmMakeTransferReq mkTransferReq = new AthmMakeTransferReq();
		mkTransferReq.setAmount(amount.toString());
		mkTransferReq.setFiID(fiId);
		mkTransferReq.setFiToken(fiToken);
		mkTransferReq.setMessage(message);
		mkTransferReq.setTargetPhone(phoneNumber.toString());
		mkTransferReq.setUserID(entitlement.getUserId());
		mkTransferReq.setServiceCharge(serviceCharge);
		mkTransferResp = customerService.makeTransfer(mkTransferReq);
		return mkTransferResp;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getFiId() {
		return fiId;
	}
	
	public String getBalanceDelay() {
		return balanceDelay;
	}

	public void setBalanceDelay(String balanceDelay) {
		this.balanceDelay = balanceDelay;
	}

	public void setFiId(String fiId) {
		this.fiId = fiId;
	}

	public String getFiToken() {
		return fiToken;
	}

	public void setFiToken(String fiToken) {
		this.fiToken = fiToken;
	}

	/**
	 * @return the maxDailyTransfer
	 */
	public String getMaxDailyTransfer() {
		return maxDailyTransfer;
	}

	/**
	 * @param maxDailyTransfer
	 *            the maxDailyTransfer to set
	 */
	public void setMaxDailyTransfer(String maxDailyTransfer) {
		this.maxDailyTransfer = maxDailyTransfer;
	}

	/**
	 * Process cancel & finish result method.
	 *
	 * @param request value
	 * @return ModelAndView result
	 * @author Modified by ET58344
	 * @since 08-31-2020
	 */
	private ModelAndView proceessCancelFinishResult (HttpServletRequest request) {

		Map<String, Object> model = new HashMap<>();//model result
		final String strUserSession = "userSession";//user session string
		final String strAthmSendMoney = "athmsendmoney";//athm string
		final String strPage = "page";//page string
		final int pageNumber = 2;//page number

		UserSession userSession = new UserSession();//user session
		if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
			userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
		}

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		model.put(strPage, pageNumber);
		return new ModelAndView(strAthmSendMoney, model);
	}
}
