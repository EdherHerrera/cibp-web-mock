package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.PostInitialFundingBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

public class PostInitialFundingForm extends AbstractWizardFormController {

	protected final Log logger = LogFactory.getLog(getClass());
	private CustomerServices customerService;
	private String bypassAccounts;

	public PostInitialFundingForm() {
		super();
		setCommandClass(PostInitialFundingBean.class);
		setCommandName("pif");
		setPages(new String[] { "postinitialfunding", "postinitialfunding" });
	}

	protected int getTargetPage(HttpServletRequest request, int currentPage) {
		return super.getTargetPage(request, currentPage);
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new HashMap<>();

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		userSession.setStatusmessage(null);

		model.put("page", "1");
		model.put("submitgs", true);
		return showForm(request, error, "postinitialfunding", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			if (request.getSession().getAttribute("pifPage") != null) {
				page = Integer.parseInt(request.getSession().getAttribute("pifPage").toString());

			}
			request.getSession().setAttribute("pif_form", command);
			request.getSession().setAttribute("pifPage", page);
		}
		model.put("page", page);
		if (request.getSession().getAttribute("exito") != null) {
			Boolean exito = (Boolean) request.getSession().getAttribute("exito");
			if (exito) {
				model.put("page", "1");
				userSession.setStatusmessage("");
				if (request.getAttribute("lang") != null) {
					request.getSession().setAttribute("exito", null);
				}

				return model;
			}
		}

		if (page == 0) {
			model.put("mbdp", StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC));
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)){
				
				model.put("canPerformPostInitialFunding", Boolean.TRUE);
				model.put("addPifAccountsEnabled", Boolean.TRUE);
				model.put("addPifAccountsCCAEnabled", Boolean.TRUE);

				boolean pifAccountLimitReached = customerService.pifAccountLimitReached(userSession.getCustomerProfile().getTaxId());
				model.put("pifAccountLimitReached", pifAccountLimitReached);
				
				request.getSession().removeAttribute("pifPage");
			}
			else{
				boolean flagMBSD792 = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD792);
				boolean canPerformPostInitialFunding;
				if (flagMBSD792) 
					canPerformPostInitialFunding = userSession.getInterbankFundingAllowed();
				else
					canPerformPostInitialFunding = customerService.canPerformPostInitialFunding(userSession.getAccounts());
				model.put("flagMBSD792", flagMBSD792);
				model.put("canPerformPostInitialFunding", canPerformPostInitialFunding);

				boolean hasCCA = userSession.hasCCA();

				boolean addPifAccountsEnabled = customerService.isAddPifAccountsEnabled();

				// CIBP-2796
				// PMA accounts will bypass the deposit account check
				if (!addPifAccountsEnabled) {
					addPifAccountsEnabled = userSession.hasPifBypassAccount(this.bypassAccounts);
				}

				boolean addPifAccountsCCAEnabled = customerService.isAddPifAccountsCCAEnabled();
				model.put("addPifAccountsEnabled", addPifAccountsEnabled);

				if (!hasCCA && !flagMBSD792) {
					model.put("addPifAccountsCCAEnabled", hasCCA);
				} else {
					model.put("addPifAccountsCCAEnabled", addPifAccountsCCAEnabled);
				}
				
				if (canPerformPostInitialFunding) {
					boolean pifAccountLimitReached = customerService.pifAccountLimitReached(userSession
							.getCustomerProfile().getTaxId());
					model.put("pifAccountLimitReached", pifAccountLimitReached);
				}
				request.getSession().removeAttribute("pifPage");
			}			
		} else if (page == 1) {
			PostInitialFundingBean bean = (PostInitialFundingBean) command;
			if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
				if (bean == null) {
					bean = (PostInitialFundingBean) request.getSession().getAttribute("pif_form");
				}
			}

			// Add the account

			String routingNumber = bean.getRoutingNumber();
			String accountNumber = bean.getAccountNumber();
			char accountType = bean.getAccountType().charAt(0);

			String taxId = userSession.getCustomerProfile().getTaxId();

			String nickname = customerService.getBankName(routingNumber);
			if (nickname.equals(""))
				nickname = "EXT ACCOUNT";

			// Verify that the small deposits sent have not reached the max
			String accountId = taxId + routingNumber + accountNumber;
			if (customerService.pifSmallDepositsLimitReached(accountId)) {

				customerService.addBlock5PIFAccount(routingNumber, accountNumber, taxId, nickname, accountType);

				// Send the email
				CustomerProfile profile = userSession.getCustomerProfile();
				PIFExternalAccount account = customerService.getPifExternalAccount(accountId);
				String bankAccount = "";
				if (account.getAccountNumber().length() > 4)
					bankAccount = "x" + account.getAccountNumber().substring(account.getAccountNumber().length() - 4);
				else
					bankAccount = "x" + account.getAccountNumber();
				customerService.sendPIFBlock5Confirmation(profile, bankAccount, account.getBankName(),
						userSession.getPreferredLanguage());
			} else {
				boolean exito = customerService.addExternalAccount(routingNumber, accountNumber, taxId, nickname,
						accountType);
				if (!exito) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("You already have registered this account.");
					else
						userSession.setStatusmessage("Ya tienes esta cuenta registrada.");

					model.put("canPerformPostInitialFunding", true);
					model.put("pifAccountLimitReached", false);
					model.put("page", "0");

				}
				if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
					model.put("submitgs", true);
					request.getSession().setAttribute("exito", exito);
				}

			}
		}

		return model;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		boolean err = false;
		boolean sessionErrorSet = false;

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		PostInitialFundingBean bean = (PostInitialFundingBean) command;
		String routingNumber = bean.getRoutingNumber();
		String accountNumber = bean.getAccountNumber();
		String confirmAccountNumber = bean.getConfirmAccountNumber();
		String taxId = userSession.getCustomerProfile().getTaxId();
		String accountType = bean.getAccountType();

		// 9 digits for routing number
		if (GenericValidator.isBlankOrNull(routingNumber)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("routingNumber", "", "Required");
				err = true;
			} else {
				errors.rejectValue("routingNumber", "", "Requerido");
				err = true;
			}
		} else if (!GenericValidator.isInt(routingNumber) || routingNumber.length() != 9) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("routingNumber", "", "Invalid routing number");
				err = true;
			} else {
				errors.rejectValue("routingNumber", "", "Ruta y tr&aacute;nsito invalido");
				err = true;
			}
		} else if (routingNumber.equals("021502011") || routingNumber.equals("021606674")
				|| routingNumber.equals("021502105")) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("routingNumber", "", "BPPR is not a valid external bank");
				err = true;
			} else {
				errors.rejectValue("routingNumber", "", "BPPR no es un banco externo valido");
				err = true;
			}
		} else if ("".equals(customerService.getBankName(routingNumber))) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("routingNumber", "", "Please select a valid routing number");
				err = true;
			} else {
				errors.rejectValue("routingNumber", "",
						"Favor de entrar un n&uacute;mero de ruta y tr&aacute;nsito valido");
				err = true;
			}
		}

		// Account numbers must match
		if (GenericValidator.isBlankOrNull(accountNumber)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("accountNumber", "", "Required");
				err = true;
			} else {
				errors.rejectValue("accountNumber", "", "Requerido");
				err = true;
			}
		}
		if (GenericValidator.isBlankOrNull(confirmAccountNumber)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("confirmAccountNumber", "", "Required");
				err = true;
			} else {
				errors.rejectValue("confirmAccountNumber", "", "Requerido");
				err = true;
			}
		}
		if (!GenericValidator.isBlankOrNull(accountNumber)) {
			// Make sure each digit is valid
			for (int i = 0; i < accountNumber.length(); i++) {
				if (!GenericValidator.isInt(accountNumber.substring(i, i + 1))) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("accountNumber", "", "Please select a valid account number");
						err = true;
					} else {
						errors.rejectValue("accountNumber", "", "Favor de entrar un n&uacute;mero de cuenta valido");
						err = true;
					}
					break;
				}
			}
		}
		// 14 digit max
		if (!GenericValidator.isBlankOrNull(accountNumber) && accountNumber.length() > 14) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("accountNumber", "", "Invalid account length");
				err = true;
			} else {
				errors.rejectValue("accountNumber", "", "Largo de n&uacute;mero de cuenta no permitido");
				err = true;
			}
		}
		if (!GenericValidator.isBlankOrNull(accountNumber) && !accountNumber.equals(confirmAccountNumber)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("confirmAccountNumber", "", "Account number does not match");
				err = true;
			} else {
				errors.rejectValue("confirmAccountNumber", "", "N&uacute;mero de cuenta no coincide");
				err = true;
			}
		}

		// Checking/Savings
		if (GenericValidator.isBlankOrNull(accountType)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("accountType", "", "Please select the account type");
				err = true;
			} else {
				errors.rejectValue("accountType", "", "Favor de seleccionar el tipo de cuenta");
				err = true;
			}
		}

		// Is the account locked?
		if (customerService.isAccountLocked(routingNumber, accountNumber, taxId)) {
			sessionErrorSet = true;
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("accountNumber", "", "Invalid account number");
				if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
					userSession.setStatusmessage("The account you are trying to add is blocked for security reasons.");
				}
				err = true;
			} else {
				errors.rejectValue("accountNumber", "", "N&uacute;mero de cuenta invalido");
				if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
					userSession
							.setStatusmessage("La cuenta que est&aacute;s intentando a&ntilde;adir esta bloqueada por razones de seguridad.");
				}
				err = true;
			}
		}

		if (err && !sessionErrorSet && !userSession.getGuidedSetupSession().isGuidedSetupMode()) {

			if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
				userSession.setStatusmessage("Please verify information entered");
			else
				userSession.setStatusmessage("Verifica la informaci&oacute;n entrada");

		}
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerServices) {
		this.customerService = customerServices;
	}

	public String getBypassAccounts() {
		return bypassAccounts;
	}

	public void setBypassAccounts(String bypassAccounts) {
		this.bypassAccounts = bypassAccounts;
	}
}
