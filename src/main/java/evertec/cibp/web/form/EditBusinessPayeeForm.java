package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVAccountFavoritePayment;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.DeleteCustomerPayeeResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.EditPayeeBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.UserAccount;
import evertec.otherservices.requests.UpdateUserAccountReq;

public class EditBusinessPayeeForm extends CancellableFormController implements GlobalEntitlementAwareController,
		MessageSourceAware {
	/** Logger */
	protected final Log log = LogFactory.getLog(getClass());
	private MessageSource messageSource;
	private String telenominaID;
	private String vendorID;
	
	public String getTelenominaID() {
		return telenominaID;
	}

	public void setTelenominaID(String telenominaID) {
		this.telenominaID = telenominaID;
	}
	
	public String getVendorID() {
		return vendorID;
	}

	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}

	public EditBusinessPayeeForm() {
		super();
		setCommandClass(EditPayeeBean.class);
		setCommandName("cp");
		setFormView("editbusinesspayee");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		BankingSession bankingSession = userSession.getBankingSession();
		CustomerProfile profile = userSession.getCustomerProfile();
		EditPayeeBean payeeBean = (EditPayeeBean) command;
		model.put("telenominaID", telenominaID);

		List<TVFrontEndPayee> payees = userSession.getPayees(true);

		if (payeeBean.getAction() != null && payeeBean.getAction() == EditPayeeBean.ACTION.FREQUENT
				|| payeeBean.getAction() == EditPayeeBean.ACTION.FREQUENT_PAYMENT_CONFIRMATION) {
			String p = payeeBean.getPayee();
			boolean freq = payeeBean.getFrequent().equalsIgnoreCase("true") ? true : false;

			TVFrontEndPayee tvp;
			try {
				tvp = Utils.getPayeebyId(p, payees);
				if (tvp != null) {
					customerService.modifyPayeeFrequentPreference(profile, bankingSession, tvp, freq);
					tvp.getCustomerPayeePreferences().setFavorite(freq);
					model.put("text", freq);

					if (payeeBean.getAction() == EditPayeeBean.ACTION.FREQUENT){
						userSession.resetFavoritePayments();
						userSession.setPendingResetPayerees(Boolean.TRUE);
					}
				} else
					model.put("text", !freq);

				return new ModelAndView("text", model);

			} catch (Exception e) {
				model.put("text", !freq);
				return new ModelAndView("text", model);
			}
		}
		else if (payeeBean.getAction() != null && payeeBean.getAction() == EditPayeeBean.ACTION.MODIFY) {
			TVFrontEndPayee tvp = Utils.getPayeebyId(payeeBean.getPayee(), payees);
			boolean isvalid = false;
			if (!GenericValidator.isBlankOrNull(payeeBean.getNickname())
					&& GenericValidator.maxLength(payeeBean.getNickname(), 30)
					&& GenericValidator.matchRegexp(payeeBean.getNickname(), "^[A-Za-z\\d-]+(\\s[A-Za-z\\d-]+)*$"))
				isvalid = true;

			if (isvalid) {
				
				/**
				 * MBNA-2668
				 * Method modifyPayeeNickname : CIBP-WEB microservice integration
				 * 
				 */
				PaymentsFacade.getInstance().modifyPayeeNickName(profile, bankingSession, tvp, payeeBean.getNickname(), userSession);

				if (customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED
						&& this.getEBPPMessageSourceStatus() && userSession.isEbpp2UserIdLookedUp() 
						&& !userSession.getEbpp2UserId().equals("")) {

						// Get the list of user accounts
						List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

						// Loop through all the accounts and match them with the
						// proper userAccount
						if (ebpp2UserAccounts != null && !ebpp2UserAccounts.isEmpty()) {
							tvp.setEbpp2UserAccount(Utils.getEbpp2UserAccountsByAccount(tvp.getBillingAccount(),
									userSession));

							// If we found the account, update it with the new
							// nickname
							if (tvp.getEbpp2UserAccount() != null) {
								UpdateUserAccountReq req = new UpdateUserAccountReq();
								req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
								req.setBillerAccountID(tvp.getEbpp2UserAccount().getBillerAccountID());
								req.setLabel(tvp.getNickname());
								req.setActive(tvp.getEbpp2UserAccount().isActive());
								req.setEbillNotif(tvp.getEbpp2UserAccount().isReceiveEbillNotification());
								req.setEstmNotif(tvp.getEbpp2UserAccount().isReceiveEstatementNotification());
								customerService.updateEbpp2UserAccount(req);
							}
						}
				}
				model.put("text", payeeBean.getNickname());
			} else {
				model.put("text", tvp.getNickname());
			}
			return new ModelAndView("text", model);
		} else if (payeeBean.getAction() != null && payeeBean.getAction() == EditPayeeBean.ACTION.DELETE) {
			String id = payeeBean.getPayee();
			TVFrontEndPayee pay = Utils.getPayeebyId(id, payees);
			DeleteCustomerPayeeResponse deleteok = null;
			AccountMaskUtil util = new AccountMaskUtil();
			String payeeInf = "";
			if (pay != null) {
				// CIBP-2548 JOCAMPO
				if (pay.getGlobalPayeeIdString() != null && pay.getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
					pay.setTelenomina(true);
				} else {
					pay.setTelenomina(false);
				}

				payeeInf = pay.getNickname() + " " + util.mask(pay.getBillingAccount());

				boolean isHighRiskPayee = false;
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1701) && pay.getGlobalPayee() != null ) {
					
					PaymentsFacade.getInstance().getGlobalPayeePreference(pay.getGlobalPayee(), userSession);

					if (pay.getGlobalPayee().getPreference() != null)
						isHighRiskPayee = pay.getGlobalPayee().getPreference().getHighRisk();
				}
				// The user has an EBPP2 ID
				if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

					// Get the list of user accounts
					List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

					// Loop through all the accounts and match them with the
					// proper userAccount
					if (ebpp2UserAccounts != null && !ebpp2UserAccounts.isEmpty()) {
						pay.setEbpp2UserAccount(Utils.getEbpp2UserAccountsByAccount(pay.getBillingAccount(),
								userSession));
					}

					// If we found the account, delete it as well
					if (pay.getEbpp2UserAccount() != null) {

						deleteok = customerService.deleteCustomerPayeeWithErrorDescriptionForEbpp2(profile,
								bankingSession, pay, userSession.getEbpp2UserId(), pay.getEbpp2UserAccount()
										.getBillerAccountID(), Utils.generateCustomerInteractionEvent(request), isHighRiskPayee);
					} else {

						deleteok = customerService.deleteCustomerPayeeWithErrorDescription(profile, bankingSession,
								pay, Utils.generateCustomerInteractionEvent(request), isHighRiskPayee);
					}
				}
				// Use the original EBPP implementation
				else {

					// mpicado CIBP-1799, se cambio el llamado al metodo
					// getEbppCustomerModel() por el que se creo sobrecargado
					// getEbppCustomerModel(boolean ebppStatus)
					deleteok = customerService.deleteCustomerPayeeWithErrorDescription(profile, bankingSession, pay,
							Utils.generateCustomerInteractionEvent(request), isHighRiskPayee);
				}

				if (deleteok.getResult().equals(DeleteCustomerPayeeResponse.Result.SUCCESS)) {
					userSession.resetEbpp2Accounts();
					userSession.resetPayees();
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Payee " + payeeInf + " was deleted.");
					else
						userSession.setStatusmessage("Ha borrado el comercio: " + payeeInf);
				} else {
					/*
					 * CIBP-1680 bgarcia Se agrega la logica para poner el
					 * detalle del mensaje
					 */
					String er = getTelepagoErrorDescriptor(deleteok.getErrorEBA(), Utils.getRSALanguage(request)
							.toLowerCase());
					String error = "";
					if (er == null || er.equals("")) {
						userSession.setErrorDetail(deleteok.getErrorString());
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							error = "Unable to delete payee " + payeeInf
									+ " <a href='#' onClick='Show()'>Learn More</a>";
							userSession.setErrorDetailCod("Error Code: " + deleteok.getErrorEBA());
						} else {
							error = "Error borrando comercio: " + payeeInf
									+ " <a href='#' onClick='Show()'>Conoce M&aacute;s</a>";
							userSession.setErrorDetailCod("C&oacute;digo de error: " + deleteok.getErrorEBA());
						}
					} else {
						userSession.setErrorDetail(er);

						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							error = "Unable to delete payee " + payeeInf
									+ " <a href='#' onClick='Show()'>Learn More</a>";
							userSession.setErrorDetailCod("Error Code: " + deleteok.getErrorEBA());
						} else {
							userSession.setErrorDetailCod("C&oacute;digo de error: " + deleteok.getErrorEBA());
							error = "Error borrando comercio: " + payeeInf
									+ " <a href='#' onClick='Show()'>Conoce M&aacute;s</a>";
						}
					}
					userSession.setStatusmessage(error);
				}
			}
			/*
			 * mpicado, no se hace uso del modelo por lo que no es necesario a
			 * esperar que se hagan las demas condiciones y se utilice el return
			 * global que usa el model. La idea es llamar al showForm sin el
			 * model
			 */
			return showForm(request, response, errors);
		} else if (payeeBean.getAction() != null && payeeBean.getAction() == EditPayeeBean.ACTION.DELETE_EBILL) {

			String p = payeeBean.getPayee();
			boolean deleteok = false;
			String payeeInf = "";

			// The user has an EBPP2 ID
			if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

				// Get the list of user accounts
				List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

				// Loop through all the accounts and match them with the proper
				// userAccount
				boolean found = false;
				UserAccount ebpp2UserAccount = new UserAccount();
				if (ebpp2UserAccounts != null && !ebpp2UserAccounts.isEmpty()) {
					for (Iterator<UserAccount> iterator2 = ebpp2UserAccounts.iterator(); iterator2.hasNext();) {
						UserAccount userAccount = iterator2.next();
						if (userAccount.getAccountNumber().replaceFirst("^0+(?!$)", "")
								.equals(p.replaceFirst("^0+(?!$)", ""))) {
							ebpp2UserAccount = userAccount;
							payeeInf = ebpp2UserAccount.getBillerProductName() + " "
									+ ebpp2UserAccount.getAccountNumber();
							found = true;
							break;
						}
					}

					// If we found the account, remove it
					if (found) {
						deleteok = customerService.removeEbpp2UserAccount(userSession.getEbpp2UserId(),
								ebpp2UserAccount.getBillerAccountID());

						if (deleteok) {
							userSession.resetEbpp2Accounts();
						}
					}
				}
			}
			if (deleteok) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("You have deleted e-bill: " + payeeInf);
				else
					userSession.setStatusmessage("Has borrado el e-bill: " + payeeInf);
			} else {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Unable to delete e-bill: " + payeeInf);
				else
					userSession.setStatusmessage("Error borrando e-bill: " + payeeInf);
			}
		}

		return showForm(request, response, errors, model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		if (!userSession.isLoadebpp())
			userSession.resetPayees();

		/*
		 * aqui establezco el valor de el MessageSource para que lo pueda usar
		 * el bean del core, me parece que podria hacerse accesando el context
		 * pero tuve algunos problemas al hacerlo.. mpicado CIBP-1799
		 */
		userSession.getCustomerService().setEBPPStatus(this.getEBPPMessageSourceStatus());

		// mpicado CIBP-1799 se cambia el llamado por el metodo sobrecargado que
		// se creo, para poder enviarle el status de EBPP
		List<TVFrontEndPayee> payees = new ArrayList<>();
		
		if(userSession.getPayrollsPayments() != null && !userSession.getPayrollsPayments().isEmpty()){
			for(TVAccountFavoritePayment payAux : userSession.getPayrollsPayments()){
				payees.add(payAux.getPayee());
			}
		}
		
		if(userSession.getVendorPayments() != null && !userSession.getVendorPayments().isEmpty()){
			for(TVAccountFavoritePayment payAux : userSession.getVendorPayments()){
				payees.add(payAux.getPayee());
			}
		}
		
		
		
		model.put("telenominaID", telenominaID);
		model.put("vendorID", vendorID);
		model.put("isVendorEnabled", StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR));
		// Get the list of user accounts
		List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

		// The user has an EBPP2 ID
		if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {
			// Loop through all the payees and match them with the proper
			// userAccount
			if (payees != null && !payees.isEmpty()) {
				for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
					TVFrontEndPayee payee = iterator.next();
					// Loop through all the accounts and match them with the
					// proper userAccount
					if (ebpp2UserAccounts != null && !ebpp2UserAccounts.isEmpty()) {
						payee.setEbpp2UserAccount(Utils.getEbpp2UserAccountsByAccount(payee.getBillingAccount(),
								userSession));
					}
					//new tsys check					
					if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)){
						if(Utils.getPayeeisCCA(payee.getBillingAccount(), userSession)){
							UserAccount userAcc = new UserAccount();
							model.put("eBillsFlag", true);
							/*Use Ajax Call tsysStatusPayees */							
							userAcc.setAccountLabel("CCA_TSYS_AJAX");
							payee.setEbpp2UserAccount(userAcc);
						}//End if is cca
						else{
							//*Hacer la validacion por global payee id*/	
							//359 Visa, 223 MC y 1983 Amex
							if(payee.getGlobalPayeeId() == 359 
									|| payee.getGlobalPayeeId() == 223
									|| payee.getGlobalPayeeId() == 1983){
								UserAccount userAcc = new UserAccount();
								model.put("eBillsFlag", true);					
								userAcc.setAccountLabel("CCA_NOT_FROM_USER");
								payee.setEbpp2UserAccount(userAcc);
							}
						}
					}//End if ebills flag	
				}
			}
		}
		model.put("payees", payees);
		
		//MBSFE-7 hide ebill AEE payee
		model.put("MBSFE7", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE7).toString());
		model.put("states", this.getStates());
		return model;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.BILLPAY;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		return status.equals(EntitlementStatus.ENABLED.toString());
	}

	/*
	 * CIBP-1680 bgarcia Obtiene el texto de error actualizado
	 */
	private String getTelepagoErrorDescriptor(String codigo, String len) {
		String status = "";
		try {
			if (!"en".equals(len))
				len = "es";
			status = this.messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
		} catch (Exception e) {
			status = "";
		}

		return status;
	}
	
	
	private Map<Object, String> getStates() {
		Map<Object, String> states = new LinkedHashMap<>();
		states.put("PR", "PR");
		states.put("USVI", "USVI");
		states.put("BVI", "BVI");
		states.put("AL", "AL"); 
		states.put("AK", "AK"); 
		states.put("AZ", "AZ"); 
		states.put("AR", "AR"); 
		states.put("CA", "CA");
		states.put("CO", "CO"); 
		states.put("CT", "CT"); 
		states.put("DE", "DE"); 
		states.put("DC", "DC"); 
		states.put("FL", "FL");
		states.put("GA", "GA"); 
		states.put("HI", "HI"); 
		states.put("ID", "ID"); 
		states.put("IL", "IL"); 
		states.put("IN", "IN");
		states.put("IA", "IA"); 
		states.put("KS", "KS"); 
		states.put("KY", "KY"); 
		states.put("LA", "LA"); 
		states.put("ME", "ME");
		states.put("MD", "MD"); 
		states.put("MA", "MA"); 
		states.put("MI", "MI"); 
		states.put("MN", "MN"); 
		states.put("MS", "MS");
		states.put("MO", "MO");
		states.put("MT", "MT"); 
		states.put("MI", "MI"); 
		states.put("NE", "NE"); 
		states.put("NV", "NV"); 
		states.put("NH", "NH");
		states.put("NJ", "NJ"); 
		states.put("NM", "NM"); 
		states.put("NY", "NY"); 
		states.put("NC", "NC"); 
		states.put("ND", "ND");
		states.put("OH", "OH"); 
		states.put("OK", "OK"); 
		states.put("OR", "OR"); 
		states.put("PA", "PA"); 
		states.put("RI", "RI");
		states.put("SC", "SC"); 
		states.put("SD", "SD"); 
		states.put("TN", "TN"); 
		states.put("TX", "TX"); 
		states.put("UT", "UT");
		states.put("VT", "VT"); 
		states.put("VA", "VA"); 
		states.put("WA", "WA"); 
		states.put("WV", "WV"); 
		states.put("WI", "WI");
		states.put("WY", "WY");

		return states;

	}

}
