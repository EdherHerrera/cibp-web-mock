package evertec.cibp.web.form;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.enums.TsysAlertStatus;
import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.core.utils.alerts.AlertCCAUtils;
import evertec.creditcards.exception.TsysException;
import evertec.creditcards.model.Phone;
import evertec.creditcards.model.Program;
import evertec.creditcards.model.alerts.AlertCustomerIdentifiers;
import evertec.creditcards.model.alerts.AlertDetails;
import evertec.creditcards.model.alerts.UserNotificationPreferences;
import evertec.creditcards.request.alerts.TsysAlertPreferenceCreateUpdateRequest;
import evertec.creditcards.response.alerts.TsysAlertPreferencesResponse;
import evertec.creditcards.response.alerts.preferences.TsysAlertPreferenceCreateUpdateResponse;
import evertec.creditcards.response.alerts.preferences.contacts.TsysRetrieveAlertContactInformationResponse;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.facades.TsysAlertFacade;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.PurchasesAndWithdrawalsBean;
import evertec.cibp.web.form.bean.SmsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.AlertTypesUtils;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.Utils;
import evertec.creditcards.model.CustomerIdentifiers;
import evertec.creditcards.request.alerts.TsysConsumerEssentialsPhoneNumbersRequest;
import evertec.creditcards.response.TsysConsumerEssentialsPhoneNumbersResponse;

public class CcaPurchasesAndWithdrawalsForm extends CancellableFormController {

	private static final Logger LOGGER = Logger.getLogger(CcaPurchasesAndWithdrawalsForm.class);

	private String tsysAlertsMinAlertQty;
	private String tsysAlertsCreditCardFilterCode;
	private String tsysAlertsAddIsCCAFilterCodeComercial;
	private String tsysAlertsAddIsCCAFilterCodeRetail;
	private String tsysAlertsCreditCardFilterNTIPCode;
	private String originalAmount; //first amount got from TSYS

	private final static String PURCHASE_AMOUNT_STR = "PURCHASE_AMOUNT"; //alert category
	private final static String TRUE_STR = "true"; //true string for active flag
	private final static String FALSE_STR = "false"; //true string for active flag
	private final static String PROGRAM_3_STR = "PROGRAM_3"; //program 3 name string
	private final static String ALT_2_PHONE_NUMBER_STR = "ALT_2"; //alternate 2 string
	private final static String ALT2_ALERT_CONTACT_STR = "ALT2"; //alternate 2 string
	private final static String NO_INDICATION_STR = "NO_INDICATION"; //no indication string
	private final static String NOT_REQUIRED_STR = "NOT_REQUIRED"; //not required string
	private final static String COUNTRY_CODE_STR = "+1"; //US country code
	private final String purchaseMinAmountSpec = "purchaseminamtSpecified"; //min amount specified string
	private final String purchaseMinAmount = "purchase_min_amt"; //min amount string
	private final String purchaseCheckSmsSpec = "purchasechksmsSpecified"; //check sms specified
	private boolean saveActivateAlertAuditLog = false; //flag to save or not audit log, active alert

	public CcaPurchasesAndWithdrawalsForm() {
		super();
		setCommandClass(PurchasesAndWithdrawalsBean.class);
		setCommandName("purchasesandwithdrawalscc");
		setFormView("purchasesandwithdrawalscc");
		this.tsysAlertsMinAlertQty = null;
		this.tsysAlertsCreditCardFilterCode = null;
		this.tsysAlertsAddIsCCAFilterCodeComercial = null;
		this.tsysAlertsAddIsCCAFilterCodeRetail = null;
		this.tsysAlertsCreditCardFilterNTIPCode = null;
	}

	/**
	 * Constructor parameters.
	 * @param tsysAlertsMinAlertQty min amount
	 * @param tsysAlertsCreditCardFilterCode filter code
	 * @param tsysAlertsAddIsCCAFilterCodeComercial filter commercial code
	 * @param tsysAlertsAddIsCCAFilterCodeRetail filter retail code
	 * @param tsysAlertsCreditCardFilterNTIPCode filter ntip code
	 */
	public CcaPurchasesAndWithdrawalsForm(String tsysAlertsMinAlertQty, String tsysAlertsCreditCardFilterCode,
										  String tsysAlertsAddIsCCAFilterCodeComercial, String tsysAlertsAddIsCCAFilterCodeRetail,
										  String tsysAlertsCreditCardFilterNTIPCode){

		this();
		this.tsysAlertsMinAlertQty = tsysAlertsMinAlertQty;
		this.tsysAlertsCreditCardFilterCode = tsysAlertsCreditCardFilterCode;
		this.tsysAlertsAddIsCCAFilterCodeComercial = tsysAlertsAddIsCCAFilterCodeComercial;
		this.tsysAlertsAddIsCCAFilterCodeRetail = tsysAlertsAddIsCCAFilterCodeRetail;
		this.tsysAlertsCreditCardFilterNTIPCode = tsysAlertsCreditCardFilterNTIPCode;
	}

	@SuppressWarnings("unchecked")
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<String, Object> model = new HashMap<>();
		AccountMaskUtil maskUtil = new AccountMaskUtil();
		CustomerServices customerService = userSession.getCustomerService();
		CardPlasticFrontend[] ccaPlastics; //plastics

		Cookie cookie = new Cookie("PMData", userSession.getDeviceRequest().getDeviceTokenCookie());
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		ESAPI.httpUtilities().addCookie(response, cookie);

		// OOB : verify if come from OOB Challenge
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && userSession.getOobEnroll()
				&& userSession.isOobAuthenticated()) {
			// get the information that changes if exists.
			Map<String, Object> parameters = null; //parameters
			Object infoParamObj = WebUtils.getSessionAttribute(request,
					ChallengeInformationUtils.INFORMATION_PARAMETERS); //information parameters object

			if(infoParamObj instanceof Map) {
				 parameters = (Map<String, Object>) infoParamObj;
			}
			setSessionAttributes(request, command, parameters);
		}

		PurchasesAndWithdrawalsBean bean = (PurchasesAndWithdrawalsBean) command;
		bean.setAthplastic1(this.getAthPlastic(bean.getAthplastic1()));
		bean.setMinAmountForAlert(getTsysAlertsMinAlertQty());
		String action = "";

		if (!GenericValidator.isBlankOrNull(bean.getAction())) {
			action = bean.getAction();
		}
		model.put("ccaAccounts", this.getAccountsMap(request));

		// Execute Validation
		if (action.equalsIgnoreCase("2")) {

			bean.setAlertsPurchaseminamt(bean.getAlertsPurchaseminamt().replaceAll(",",""));
			boolean errors = AlertTypesUtils.purchasesAndWithdrawalsErrors(request, model, command);

			if (errors) {
				return showForm(request, response, error, model);
			}

			/*Remove SMS Alert in TSYS calling alert preferences*/
			if (bean.getPurchase_thr_chksms().equalsIgnoreCase("0")) {

				this.removeAlertPreference(bean, request, model);
				return showForm(request, response, error, model);
			}

			/*If the user activate sms checkbox, we call alert contact information and alert preferences api*/
			if (!("0".equalsIgnoreCase(bean.getPurchase_thr_chksms()))) {

				TsysRetrieveAlertContactInformationResponse contactInformationResponse = this
						.getTsysAlertContactInformation(userSession, bean); //contact information result object

				if(contactInformationResponse != null) {
					Program program3 = this.getAlertProgram3(contactInformationResponse.getPrograms());

					if(program3 != null) {
						Phone alt2Phone = program3.getPhones().stream().filter(eachPhone ->
								ALT2_ALERT_CONTACT_STR.equals(eachPhone.getType())).findAny().orElse(null); //phone

						if (alt2Phone != null) {
							//Se validan escenarios en los que alt2 o alt2 selelected existan
							this.saveUpdateAlertPreference(request, bean, model, program3);
						} else {
							//Aca solo se valida el caso en que alt2 sea null, es decir se llama a crear contacto y a seleccionar para la alerta
							this.addPhoneNumberContactAndAlertPreference(request, bean, model);
						}
					} else{
						model.put("statusMessage", AlertTypesUtils.getErrorStatusMessage(request));
					}
				} else{
					model.put("statusMessage", AlertTypesUtils.getErrorStatusMessage(request));
				}

				return showForm(request, response, error, model);
			}
		}

		Map<Object, Object> plastics = new LinkedHashMap<>(); //plastics
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			plastics.put("none", "Select Card");
		} else {
			plastics.put("none", "Selecciona Tarjeta");
		}

		List<TVFrontendAccount> ccaAccounts = userSession.getCcaAccounts(); //cca accounts
		/*If para remover plasticos inactivos*/
		if (action.equalsIgnoreCase("0")) {

			TVFrontendAccount account = Utils.getAccountByAccountNumber(bean.getCcaAccount(), ccaAccounts); //account
			ccaPlastics = Utils.getCCACardPlastics(customerService, userSession.getCustomerProfile(),
					userSession.getBankingSession(), account); //plastics
			ccaPlastics = Utils.removeInactiveCardPlastics(userSession, ccaPlastics);

			if (!GenericValidator.isBlankOrNull(bean.getCcaAccount())) {

				if (ccaPlastics.length > 0) {

					for(CardPlasticFrontend ccaPlastic : ccaPlastics) {
						String cardType = ccaPlastic.getCardPlasticAccountRecord().getEmbossedName();
						plastics.put(ccaPlastic.getCardPlasticAccountRecord().getCardPANId(), cardType + " "
								+ maskUtil.mask(ccaPlastic.getCardPlasticAccountRecord().getCardPANId()));
					}
					model.put("accountDetails", plastics);
					return new ModelAndView("athPlastics", model);
				}
			}
		}

		/*If para traer informacion de alertas existentes en TSYS*/
		if (action.equalsIgnoreCase("1")) {

			if (!GenericValidator.isBlankOrNull(bean.getAthplastic1())) {

				this.saveActivateAlertAuditLog = false;
				//Look for the plastic preferences in the session
				Map<String, CardPlasticFrontend> preferences = userSession.getTsysCCACustomerAlerts();

				if (preferences != null && preferences.size() > 0 && preferences.containsKey(bean.getAthplastic1())) {
					CardPlasticFrontend plasticAlerts = preferences.get(bean.getAthplastic1());
					UserNotificationPreferences info = this.getPurchaseAmountNotificationPreferences(
							plasticAlerts.getTsysCcaAlertPreferences().getUserNotificationPreferences());

						if (info != null && TRUE_STR.equals(info.getActiveFlag()) && info.getAlertDetails() != null
								&& !info.getAlertDetails().isEmpty()) {

							model.put(purchaseMinAmountSpec, true);
							model.put(purchaseMinAmount, getAlertPurchaseAmount(info));
							model.put(purchaseCheckSmsSpec, true);
						} else{
							this.saveActivateAlertAuditLog = true;
						}
				} else {
					this.inquiryTsysAlertPreferences(userSession, bean, model);
				}
			}

			return new ModelAndView("ccaPlasticsInfo", model);
		}

		return showForm(request, response, error, model);
	}


	/**
	 * Set session attributes, save in session action and parameters.
	 * @param request http request
	 * @param command bean object
	 * @param parameters parameters
	 * @throws IllegalAccessException ex
	 * @throws InvocationTargetException ex
	 */
	private void setSessionAttributes(HttpServletRequest request, Object command, Map<String, Object> parameters) throws IllegalAccessException, InvocationTargetException {

		if (parameters != null && parameters.get(ChallengeInformationUtils.CCA_ACTION) != null) {

			Object objectBean = parameters.get(ChallengeInformationUtils.CCA_ACTION); //object bean
			PurchasesAndWithdrawalsBean responseBean = null; //response bean

			if(objectBean instanceof PurchasesAndWithdrawalsBean){
				responseBean = (PurchasesAndWithdrawalsBean) objectBean;
			}

			org.apache.commons.beanutils.BeanUtils.copyProperties(command, responseBean);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>(); //model
		UserSession session = KiuwanUtils.cast(UserSession.class, WebUtils.getSessionAttribute(request, "userSession")); //session
		PurchasesAndWithdrawalsBean bean = KiuwanUtils.cast(PurchasesAndWithdrawalsBean.class, command); //bean
		Boolean showCCA = KiuwanUtils.cast(Boolean.class, WebUtils.getSessionAttribute(request, ChallengeInformationUtils.CCA_FRAME)); //show cca

		if (showCCA != null && showCCA) {

			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CCA_FRAME, null);
			Map<String, Object> modelResponse = (Map<String, Object>) WebUtils.getSessionAttribute(request,
					ChallengeInformationUtils.CCA_MODEL); //model response
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CCA_MODEL, null);
			PurchasesAndWithdrawalsBean responseBean = (PurchasesAndWithdrawalsBean) modelResponse
					.get(ChallengeInformationUtils.CCA_ACTION); //response bean
			org.apache.commons.beanutils.BeanUtils.copyProperties(command, responseBean);
			model.putAll(modelResponse);
		}
		model.put("ccaAccounts", this.getAccountsMap(request));

		if (session.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)) {
			model.put("mobilealert", true);
		}
		//Alexis: MBCA627 Email Notification Activation in Tsys flag
		model.put("mbca627", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA627));
		Map<Object, Object> plastics = new LinkedHashMap<>(); //plastics

		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			plastics.put("none", "Select Card");
		} else {
			plastics.put("none", "Selecciona Tarjeta");
		}

		this.loadPlastics(session, bean, plastics, model);
		this.oobEnrollmentVerification(request, model);
		this.validateInfoChallenge(request, model);
		return model;
	}

	/**
	 * This method loads all active plastics related to an account.
	 *
	 * @param session user session
	 * @param bean controller bean
	 * @param plastics plastics
	 * @param model model map
	 * @author Popular Inc.
	 * @since 11-02-2022
	 *
	 */
	private void loadPlastics(UserSession session, PurchasesAndWithdrawalsBean bean, Map<Object, Object> plastics,
							  Map<String, Object> model){

		AccountMaskUtil maskUtil = new AccountMaskUtil(); //mask util
		List<TVFrontendAccount> ccaActs = session.getCcaAccounts(); //front end accounts

		if (!GenericValidator.isBlankOrNull(bean.getAthplastic1())) {
			CardPlasticFrontend[] ccaPlastics; //plastics
			TVFrontendAccount account = Utils.getAccountByAccountNumber(bean.getCcaAccount(), ccaActs); //front end account
			ccaPlastics = Utils.getCCACardPlastics(session.getCustomerService(), session.getCustomerProfile(),
					session.getBankingSession(), account);
			ccaPlastics = Utils.removeInactiveCardPlastics(session, ccaPlastics);

			if (!GenericValidator.isBlankOrNull(bean.getCcaAccount())) {

				String cardType; //card type

				for (CardPlasticFrontend ccaPlastic : ccaPlastics) {

					cardType = ccaPlastic.getCardPlasticAccountRecord().getEmbossedName();
					plastics.put(ccaPlastic.getCardPlasticAccountRecord().getCardPANId(), cardType + " "
							+ maskUtil.mask(ccaPlastic.getCardPlasticAccountRecord().getCardPANId()));
				}
				model.put("accountDetails", plastics);
			}
		}
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
		PurchasesAndWithdrawalsBean bean = (PurchasesAndWithdrawalsBean) command;

		if (bean.getAction() != null && bean.getAction().equalsIgnoreCase("2")) {
			AlertTypesUtils.rsaChallengeForAlertController(request, command, errors, "CCA");
		}

	}

	private Map<Object, Object> getAccountsMap(HttpServletRequest request) {

		Map<Object, Object> accountsMap = new LinkedHashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String filterCommercial = getTsysAlertsAddIsCCAFilterCodeComercial();
		String filterRetail = getTsysAlertsAddIsCCAFilterCodeRetail();
		String filterCode = getTsysAlertsCreditCardFilterCode();
		String filterNTIPCode = getTsysAlertsCreditCardFilterNTIPCode();

		List<TVFrontendAccount> ccaActs = AlertTypesUtils.getCCAAccountsListForAlertsFiltered(userSession, filterCommercial, filterRetail, filterCode, filterNTIPCode);

		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			accountsMap.put("none", "Select a Credit Card");
		} else {
			accountsMap.put("none", "Selecciona una Tarjeta de Cr\u00E9dito");
		}

		Utils.getCCAAccountsMap(ccaActs, accountsMap);
		return accountsMap;
	}

	@SuppressWarnings("unchecked")
	private SmsBean getSMSProfile(UserSession userSession, HttpServletRequest request) {
		SMSProfile smsProfile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
		boolean mobileBanking = AlertTypesUtils.isMobileBanking(userSession);
		SmsBean bean = new SmsBean();

		if (mobileBanking && (smsProfile != null && smsProfile.getCtns() != null)) {
			Set<SMSCTN> set = smsProfile.getCtns();
			for (SMSCTN ctnBean : set) {
				bean.setPhone1(ctnBean.getCtn());
				bean.setProvider(ctnBean.getProvider());
			}
		}
		return bean;
	}

	private void validateInfoChallenge(HttpServletRequest request, Map<String, Object> model) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Boolean informationChallenge = userSession.isInformationChallenge();
		// CIBP-2804
		if (userSession.getGuidedSetupSession() != null && userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, true);
		} else {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, informationChallenge);
		}
		if (!informationChallenge) {
			if (userSession.getInformationQuestion() != null) {
				String question = (String) userSession.getInformationQuestion().values().toArray()[0];
				model.put("question", question);
			}
		}
	}

	private void oobEnrollmentVerification(HttpServletRequest request, Map<String, Object> model) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && userSession.getOobEnroll()) {
			if (userSession.getTasksListOOB() != null) {
				if (!userSession.getTasksListOOB().contains(ASK_FOR_OOB.OOB_CHALLENGE))
					userSession.getTasksListOOB().add(0, ASK_FOR_OOB.OOB_CHALLENGE);

				if (!userSession.getTasksListOOB().contains(ASK_FOR_OOB.CCA_ALERT_TYPES))
					userSession.getTasksListOOB().add(ASK_FOR_OOB.CCA_ALERT_TYPES);

				if (userSession.getTasksListOOB().contains(ASK_FOR_OOB.ATH_ALERT_TYPES))
					userSession.getTasksListOOB().remove(ASK_FOR_OOB.ATH_ALERT_TYPES);
			} else {
				List<ASK_FOR_OOB> listNavOOB = new ArrayList<ASK_FOR_OOB>();
				listNavOOB.add(ASK_FOR_OOB.OOB_CHALLENGE);
				listNavOOB.add(ASK_FOR_OOB.ALERT_TYPES);
				listNavOOB.add(ASK_FOR_OOB.CCA_ALERT_TYPES);
				userSession.setTasksListOOB(listNavOOB);
			}
			model.put("oobEnrolled", true);
			model.put("oobChallenge", "loginoob");
			userSession.setInformationChallenge(userSession.isOobAuthenticated());
		}
	}

	private String getAthPlastic(String plastic) {
		if (plastic != null && plastic.startsWith("0") && plastic.length() > 15) {
			return plastic.substring(1, plastic.length());
		}
		return plastic;
	}

	/**
	 * Get alert preferences for selected card from TSYS and save it in session.
	 * @param userSession user session
	 * @param bean bean object
	 * @param model model map
	 * @since 08-17-2022
	 * @author Popular Inc.
	 */
	private void inquiryTsysAlertPreferences(UserSession userSession, PurchasesAndWithdrawalsBean bean,
																	 Map<String, Object> model) {

 		CustomerIdentifiers customerIdentifiers = Utils.getGeneralInformationCustomerIdentifiers(bean.getAthplastic1(),
				userSession); //GI customer identifiers
		TsysAlertPreferencesResponse alertPreferencesResponse = new TsysAlertPreferencesResponse(); //response
		CustomerServices customerService = userSession.getCustomerService(); //customer service
		AccountMaskUtil maskUtil = new AccountMaskUtil(); //mask util

		if(customerIdentifiers != null) {
			AlertCustomerIdentifiers alertCustomerIdentifiers = new AlertCustomerIdentifiers(); //alert customer identifiers
			alertCustomerIdentifiers.setCustomerId(customerIdentifiers.getCustomerId());
			alertCustomerIdentifiers.setAccountId(customerIdentifiers.getAccountId());
			alertCustomerIdentifiers.setEnrolledCustomerId(customerIdentifiers.getCustomerId());
			try {
				alertPreferencesResponse = TsysAlertFacade.getInstance()
						.tsysAlertPreferenceByAccountCustomerId(alertCustomerIdentifiers); //responses
			} catch (TsysException ex) {
				LOGGER.error("Tsys error in method tsysAlertPreferenceByAccountCustomerId, with user: " + userSession.getUsername() +
						", account: " + maskUtil.mask(bean.getAthplastic1()) + ", error detail: " + ex.getDetail());
				alertPreferencesResponse = null;
			}

			if(alertPreferencesResponse != null) {

				UserNotificationPreferences userNotificationPreferences = getPurchaseAmountNotificationPreferences(
						alertPreferencesResponse.getUserNotificationPreferences());

				if(TRUE_STR.equals(userNotificationPreferences.getActiveFlag())) {

					model.put(purchaseMinAmountSpec, true);
					model.put(purchaseMinAmount, getAlertPurchaseAmount(userNotificationPreferences));
					model.put(purchaseCheckSmsSpec, true);
				} else{
					this.saveActivateAlertAuditLog = true;
				}

				TVFrontendAccount account = Utils.getAccountByAccountNumber(bean.getCcaAccount(),
						userSession.getCcaAccounts()); //tv front end account
				CardPlasticFrontend[] ccaPlastics = Utils.getCCACardPlastics(customerService, userSession.getCustomerProfile(),
						userSession.getBankingSession(), account); //cca plastics from vault
				this.saveCCAPlasticAlertInSession(userSession, ccaPlastics, bean, alertPreferencesResponse);
			} else {
				LOGGER.error("TSYS CCA Alerts: alert preferences return is null.");
				model.put("error", true);
			}
		} else{
			model.put("error", true);
		}
	}

	/**
	 * Return alert card format amount.
	 * @param notificationPreferences notification preferences
	 * @return String amount
	 * @since 08-17-2022
	 * @author Popular Inc.
	 */
	private String getAlertPurchaseAmount(UserNotificationPreferences notificationPreferences){

		final String detailName = "thresholdAmount"; //detail name
		String finalAmount = null; //final formatted amount
		String alertAmount = "0"; //amount value without format

		if(notificationPreferences != null) {
			AlertDetails alertDetail = notificationPreferences.getAlertDetails().stream().filter(alertDetails ->
					detailName.equals(alertDetails.getName())).findAny().orElse(null);

			if (alertDetail != null) {
				alertAmount = alertDetail.getSelectedValue().get(0);
			}

			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA77)) {
				NumberFormat decimalInstance = DecimalFormat.getInstance(); //number format

				if (decimalInstance instanceof DecimalFormat) {
					DecimalFormat formatter = (DecimalFormat) decimalInstance;
					formatter.applyPattern("#,##0.00");
					finalAmount = formatter.format(new BigDecimal(alertAmount));
				}
			} else {
				finalAmount = (new BigDecimal(alertAmount)).toString();
			}
		}
		setOriginalAmount(finalAmount);
		return finalAmount;
	}

	/**
	 * Save cca plastic alert preference in session.
	 * @param userSession user session
	 * @param ccaPlastics cca plastics
	 * @param bean bean object
	 * @param ccaAlertPreferencesResponse cca alert preferences response
	 * @since 08-17-2022
	 * @author Popular Inc.
	 */
	private void saveCCAPlasticAlertInSession(UserSession userSession, CardPlasticFrontend[] ccaPlastics,
											  PurchasesAndWithdrawalsBean bean,
											  TsysAlertPreferencesResponse ccaAlertPreferencesResponse) {
		for (CardPlasticFrontend card : ccaPlastics) {
			// Modify the Card Object PanID without 0 in the front
			String panID = this.getAthPlastic(card.getCardPlasticAccountRecord().getCardPANId());

			if (panID != null && panID.startsWith("0")) {
				panID = panID.substring(1);
			}
			if (!GenericValidator.isBlankOrNull(panID) && panID.equals(bean.getAthplastic1())) {
				card.setTsysCcaAlertPreferences(ccaAlertPreferencesResponse);
				userSession.setTsysCCACustomerAlerts(card, bean.getAthplastic1());
			}
		}
	}

	/**
	 * Get notification preferences by alert name.
	 * @param notificationList list with notifications
	 * @return UserNotificationPreferences or null
	 * @since 08-17-2022
	 * @author Popular Inc.
	 */
	private UserNotificationPreferences getPurchaseAmountNotificationPreferences(List<UserNotificationPreferences> notificationList){
		return notificationList.stream().filter(preferences ->
				PURCHASE_AMOUNT_STR.equals(preferences.getAlert())).findAny().orElse(null);
	}

	/**
	 * Tsys retrieve alert contact information response.
	 * @param userSession user session
	 * @param bean alert bean
	 * @return TsysRetrieveAlertContactInformationResponse response
	 * @author Popular Inc.
	 * @since 08-26-2022
	 */
	private TsysRetrieveAlertContactInformationResponse getTsysAlertContactInformation(UserSession userSession,
																					   PurchasesAndWithdrawalsBean bean){
		CustomerIdentifiers customerIdentifiers = Utils.getGeneralInformationCustomerIdentifiers(bean.getAthplastic1(),
				userSession); //GI customer identifiers
		AccountMaskUtil maskUtil = new AccountMaskUtil(); //mask util

		if(customerIdentifiers != null) {
			try {
				return TsysAlertFacade.getInstance().tsysRetrieveAlertContactInformation(customerIdentifiers);
			} catch (TsysException ex) {
				LOGGER.error("Tsys error in method tsysRetrieveAlertContactInformation, with user: " + userSession.getUsername() +
						", account: " + maskUtil.mask(bean.getAthplastic1()) + ", error detail: " + ex.getDetail());
			}
		}
		return null;
	}

	/**
	 * Get alert program by name.
	 * @param programArrayList program list
	 * @return Program found
	 * @author Popular Inc.
	 * @since 08-26-2022
	 */
	private Program getAlertProgram3(ArrayList<Program> programArrayList){
		return programArrayList.stream().filter(program -> PROGRAM_3_STR.equals(program.getProgramName())).findAny()
				.orElse(null);
	}

	/**
	 * Save or update phone contact in TSYS.
	 * @param bean controller bean
	 * @param userSession user session
	 * @param request request
	 * @return boolean value
	 * @author Popular Inc.
	 * @since 08-29-2022
	 */
	private boolean saveAlertPhoneContactTSYS(PurchasesAndWithdrawalsBean bean, UserSession userSession,
											  HttpServletRequest request){

		CustomerIdentifiers customerIdentifiers = Utils.getGeneralInformationCustomerIdentifiers(bean.getAthplastic1(),
				userSession); //customer identifiers
		AccountMaskUtil maskUtil = new AccountMaskUtil(); //mask util

		if(customerIdentifiers != null){
			TsysConsumerEssentialsPhoneNumbersRequest phoneNumbersRequest = new TsysConsumerEssentialsPhoneNumbersRequest(); //phone number request
			TsysConsumerEssentialsPhoneNumbersResponse phoneNumbersResponse; //phone number response
			SmsBean smsBean = this.getSMSProfile(userSession, request); //sms profile bean

			if(smsBean.getPhone1() != null){
				phoneNumbersRequest.setPhoneNumber(smsBean.getPhone1());
				phoneNumbersRequest.setType(ALT_2_PHONE_NUMBER_STR);
				phoneNumbersRequest.setConsentToCall(NO_INDICATION_STR);

				try {
					phoneNumbersResponse = TsysAlertFacade.getInstance().tsysUpdateCustomerPhoneNumbersByIdentifiers(
						customerIdentifiers.getAccountId(), customerIdentifiers.getCustomerId(), phoneNumbersRequest);

					if(phoneNumbersResponse != null){
						return true;
					}
				} catch(TsysException ex){
					LOGGER.error("Tsys error in method tsysUpdateCustomerPhoneNumbersByIdentifiers, with user: " + userSession.getUsername() +
							", account: " + maskUtil.mask(bean.getAthplastic1()) + ", error detail: " + ex.getDetail());
				}
			}
		}
		return false;
	}

	/**
	 * Save alert contact information TSYS.
	 *
	 * @param bean controller bean
	 * @param userSession user session
	 * @param request http request
	 * @return boolean result
	 * @author Popular Inc.
	 * @since 08-30-2022
	 */
	private boolean saveAlertContactInformationTSYS(PurchasesAndWithdrawalsBean bean, UserSession userSession,
													HttpServletRequest request){

		CustomerIdentifiers customerIdentifiers = Utils.getGeneralInformationCustomerIdentifiers(bean.getAthplastic1(),
				userSession); //customer identifiers
		AccountMaskUtil maskUtil = new AccountMaskUtil(); //mask util

		if(customerIdentifiers != null){
			List<Program> programList = new ArrayList<>(); //list of programs
			SmsBean smsBean = this.getSMSProfile(userSession, request); //sms profile bean

			try {
				ArrayList<Phone> selectedPhones = new ArrayList<>(); //selected alt2 phone
				Phone phone = new Phone(); //phone alt2
				phone.setType(ALT2_ALERT_CONTACT_STR);
				phone.setVerificationStatus(NOT_REQUIRED_STR);
				phone.setValue(COUNTRY_CODE_STR + smsBean.getPhone1());

				selectedPhones.add(phone);
				Program program3 = new Program(); //program 3
				program3.setProgramName(PROGRAM_3_STR);
				program3.setSelectedPhones(selectedPhones);
				programList.add(program3);

				TsysAlertFacade.getInstance().tsysAlertContactInformationUpdate(customerIdentifiers, programList);
				return true;
			} catch(TsysException ex){
				LOGGER.error("Tsys error in method tsysAlertContactInformationUpdate, with user: " + userSession.getUsername() +
						", account: " + maskUtil.mask(bean.getAthplastic1()) + ", error detail: " + ex.getDetail());
			}
		}
		return false;
	}

	/**
	 *Add phone number contact and alert preference.
	 * @param request http request
	 * @param bean controller bean
	 * @param model map model
	 * @author Popular Inc.
	 * @since 08-30-2022
	 */
	private void addPhoneNumberContactAndAlertPreference(HttpServletRequest request,
														 PurchasesAndWithdrawalsBean bean, Map<String, Object> model){

		Object sessionObj =  WebUtils.getSessionAttribute(request, "userSession"); //session object
		UserSession userSession = new UserSession(); //user session

		if(sessionObj instanceof UserSession) {
			userSession = (UserSession) sessionObj; //user session
		}

		if (this.saveAlertPhoneContactTSYS(bean, userSession, request)) {

			if (this.saveAlertContactInformationTSYS(bean, userSession, request)) {

				if(this.saveUpdatePurchaseAmountAlertPreferencesTSYS(request, bean, userSession, TRUE_STR)){
					model.put("statusMessage", AlertTypesUtils.getSuccessStatusMessage(request));
					userSession.updateTsysCCACustomerAlerts(bean.getAthplastic1());

					if(this.saveActivateAlertAuditLog) {
						Utils.registerTsysAlertHistoryLog(request, bean.getAthplastic1(), userSession, TsysAlertStatus.ACTIVATE);
					}
				}
			} else{
				model.put("statusMessage", AlertTypesUtils.getErrorStatusMessage(request));
			}
		} else {
			model.put("statusMessage", AlertTypesUtils.getErrorStatusMessage(request));
		}
	}

	/**
	 * Add or update alert preference, including contact.
	 * @param request http request
	 * @param bean controller bean
	 * @param model model map
	 * @param program3 program 3
	 * @author Popular Inc.
	 * @since 09-13-2022
	 */
	private void saveUpdateAlertPreference(HttpServletRequest request, PurchasesAndWithdrawalsBean bean, Map<String, Object> model,
										  Program program3){

		Object sessionObj =  WebUtils.getSessionAttribute(request, "userSession"); //session object
		UserSession userSession = new UserSession(); //user session

		if(sessionObj instanceof UserSession) {
			userSession = (UserSession) sessionObj; //user session
		}
		SmsBean smsBean = this.getSMSProfile(userSession, request); //sms profile bean

		if(smsBean.getPhone1() != null) {
			this.validateAlt2PhoneContact(program3, smsBean, request, bean, model, userSession);
			return;
		}
		model.put("statusMessage", AlertTypesUtils.getErrorStatusMessage(request));
	}

	/**
	 * Alt2 phone contact validation.
	 * @param program3 alert program
	 * @param smsBean sms bean
	 * @param request http request
	 * @param bean controller bean
	 * @param model model map
	 * @param userSession user session
	 * @author Popular Inc.
	 * @since 09-29-2022
	 */
	private void validateAlt2PhoneContact(Program program3, SmsBean smsBean, HttpServletRequest request,
										  PurchasesAndWithdrawalsBean bean, Map<String, Object> model,
										  UserSession userSession){

		Phone alt2Phone = program3.getPhones().stream().filter(eachPhone ->
				ALT2_ALERT_CONTACT_STR.equals(eachPhone.getType())).findAny().orElse(null); //alt2 phone
		Phone alt2SelectedPhone = program3.getSelectedPhones().stream().filter(eachPhone ->
				ALT2_ALERT_CONTACT_STR.equals(eachPhone.getType())).findAny().orElse(null); //alt2 selected phone

		if (alt2Phone != null && smsBean.getPhone1().equals(AlertCCAUtils.removePRCountryCodeContactNumber(alt2Phone.getValue()))) {
			this.validateAlt2SelectedPhone(alt2SelectedPhone, smsBean, bean, request, model);
		} else {
			this.addPhoneNumberContactAndAlertPreference(request, bean, model);
		}
	}

	/**
	 * Validate alternate 2 selected phone.
	 * @param alt2SelectedPhone selected phone
	 * @param smsBean sms bean
	 * @param bean controller bean
	 * @param request http request
	 * @param model model map
	 * @author Popular Inc.
	 * @since 09-20-2022
	 */
	private void validateAlt2SelectedPhone(Phone alt2SelectedPhone, SmsBean smsBean, PurchasesAndWithdrawalsBean bean,
											  HttpServletRequest request, Map<String, Object> model) {

		Object sessionObj = WebUtils.getSessionAttribute(request, "userSession"); //session object
		UserSession userSession; //user session
		boolean continueProcess = false; //continue proccess flag

		if (sessionObj instanceof UserSession) {
			userSession = (UserSession) sessionObj; //user session

			if (alt2SelectedPhone != null && smsBean.getPhone1()
					.equals(AlertCCAUtils.removePRCountryCodeContactNumber(alt2SelectedPhone.getValue()))) {
				continueProcess = true;
			} else {
				if (this.saveAlertContactInformationTSYS(bean, userSession, request)) {
					continueProcess = true;
				}
			}

			if(continueProcess) {
				if (this.saveUpdatePurchaseAmountAlertPreferencesTSYS(request, bean, userSession, TRUE_STR)) {
					model.put("statusMessage", AlertTypesUtils.getSuccessStatusMessage(request));
					userSession.updateTsysCCACustomerAlerts(bean.getAthplastic1());

					if (this.saveActivateAlertAuditLog) {
						Utils.registerTsysAlertHistoryLog(request, bean.getAthplastic1(), userSession, TsysAlertStatus.ACTIVATE);
					}
					return;
				}
			}
			model.put("statusMessage", AlertTypesUtils.getErrorStatusMessage(request));
		}
	}

	/**
	 * Save or update alert preferences in TSYS.
	 * @param request http request
	 * @param bean controller bean
	 * @param userSession user session
	 * @param alertStatus true or false
	 * @return boolean result
	 * @author Popular Inc.
	 * @since 09-13-2022
	 */
	private boolean saveUpdatePurchaseAmountAlertPreferencesTSYS(HttpServletRequest request, PurchasesAndWithdrawalsBean bean, UserSession userSession,
																 String alertStatus){

		TsysAlertPreferenceCreateUpdateRequest alertPreferenceRequest =
				new TsysAlertPreferenceCreateUpdateRequest(); //alert preference request body
		alertPreferenceRequest.setUserNotificationPreferences(new ArrayList<>());
		CustomerIdentifiers customerIdentifiers = Utils.getGeneralInformationCustomerIdentifiers(bean.getAthplastic1(),
				userSession); //customer identifiers
		Map<String, CardPlasticFrontend> preferences = userSession.getTsysCCACustomerAlerts(); //alert preferences
		AccountMaskUtil maskUtil = new AccountMaskUtil(); //mask util

		if(customerIdentifiers != null) {
			if (preferences != null && preferences.size() > 0 && preferences.containsKey(bean.getAthplastic1())) {

				CardPlasticFrontend plasticAlerts = preferences.get(bean.getAthplastic1()); //card plastic front ent
				String finalAmount = ""; //final amount
				DecimalFormat decimalFormat = new DecimalFormat("0.00"); //format

				if(!"".equals(bean.getAlertsPurchaseminamt())) {
					finalAmount = decimalFormat.format(new BigDecimal(bean.getAlertsPurchaseminamt())); //final amount
				}
				alertPreferenceRequest.setUserNotificationPreferences(AlertCCAUtils.changeTypeAndUpdateNotificationPreferencesType(plasticAlerts
						.getTsysCcaAlertPreferences().getUserNotificationPreferences(), alertStatus, PURCHASE_AMOUNT_STR, finalAmount));
				alertPreferenceRequest.setCustomerIdentifiers(customerIdentifiers);

				TsysAlertPreferenceCreateUpdateResponse response = null; //tsys response
				try {
					response = TsysAlertFacade.getInstance()
							.tsysUpdateAlertsPreferences(alertPreferenceRequest);
				} catch (TsysException ex) {
					LOGGER.error("Tsys error in method tsysUpdateAlertsPreferences, with user: " + userSession.getUsername() +
							", account: " + maskUtil.mask(bean.getAthplastic1()) + ", error detail: " + ex.getDetail());
				}

				this.registerAlertAmountChangedHistoryLog(response, finalAmount, request, bean, userSession);
				return response != null && response.getSuccess();
			}
		}
		return false;
	}

	/**
	 * Register alert amount changed history log.
	 * @param response change alert response
	 * @param finalAmount final amount
	 * @param request http request
	 * @param bean controller bean
	 * @param userSession user session
	 * @author Popular Inc.
	 * @since 09-04-2022
	 */
	private void registerAlertAmountChangedHistoryLog(TsysAlertPreferenceCreateUpdateResponse response, String finalAmount,
													  HttpServletRequest request, PurchasesAndWithdrawalsBean bean, UserSession userSession){

		if(getOriginalAmount() != null && !"".equals(finalAmount) && !getOriginalAmount().equals(finalAmount.replaceAll(",", "")) &&
			response != null && response.getSuccess()){
			Utils.registerTsysChangedAlertAmountHistoryLog(request, bean.getAthplastic1(), userSession,
					getOriginalAmount(), finalAmount);
		}
	}

	/**
	 * This method remove alert preference in TSYS.
	 * @param bean controller bean
	 * @param request http request
	 * @param model model map
	 * @author Popular Inc.
	 * @since 09-14-2022
	 */
	private void removeAlertPreference(PurchasesAndWithdrawalsBean bean, HttpServletRequest request, Map<String, Object> model){

		Object sessionObj =  WebUtils.getSessionAttribute(request, "userSession"); //session object
		UserSession userSession = new UserSession(); //user session

		if(sessionObj instanceof UserSession) {
			userSession = (UserSession) sessionObj; //user session
		}

		if(this.saveUpdatePurchaseAmountAlertPreferencesTSYS(request, bean, userSession, FALSE_STR)){
			model.put("statusMessage", AlertTypesUtils.getSuccessStatusMessage(request));
			userSession.updateTsysCCACustomerAlerts(bean.getAthplastic1());
			Utils.registerTsysAlertHistoryLog(request, bean.getAthplastic1(), userSession, TsysAlertStatus.DEACTIVATE);
			setOriginalAmount(null);
			return;
		}

		model.put("statusMessage", AlertTypesUtils.getDeletingErrorStatusMessage(request));
	}

	public String getTsysAlertsMinAlertQty() {
		return tsysAlertsMinAlertQty;
	}

	public void setTsysAlertsMinAlertQty(String tsysAlertsMinAlertQty) {
		this.tsysAlertsMinAlertQty = tsysAlertsMinAlertQty;
	}

	public String getTsysAlertsCreditCardFilterCode() {
		return tsysAlertsCreditCardFilterCode;
	}

	public void setTsysAlertsCreditCardFilterCode(String tsysAlertsCreditCardFilterCode) {
		this.tsysAlertsCreditCardFilterCode = tsysAlertsCreditCardFilterCode;
	}

	public String getTsysAlertsAddIsCCAFilterCodeComercial() {
		return tsysAlertsAddIsCCAFilterCodeComercial;
	}

	public void setTsysAlertsAddIsCCAFilterCodeComercial(String tsysAlertsAddIsCCAFilterCodeComercial) {
		this.tsysAlertsAddIsCCAFilterCodeComercial = tsysAlertsAddIsCCAFilterCodeComercial;
	}

	public String getTsysAlertsAddIsCCAFilterCodeRetail() {
		return tsysAlertsAddIsCCAFilterCodeRetail;
	}

	public void setTsysAlertsAddIsCCAFilterCodeRetail(String tsysAlertsAddIsCCAFilterCodeRetail) {
		this.tsysAlertsAddIsCCAFilterCodeRetail = tsysAlertsAddIsCCAFilterCodeRetail;
	}

	public String getTsysAlertsCreditCardFilterNTIPCode() {
		return tsysAlertsCreditCardFilterNTIPCode;
	}

	public void setTsysAlertsCreditCardFilterNTIPCode(String tsysAlertsCreditCardFilterNTIPCode) {
		this.tsysAlertsCreditCardFilterNTIPCode = tsysAlertsCreditCardFilterNTIPCode;
	}

	public boolean isSaveActivateAlertAuditLog() {
		return saveActivateAlertAuditLog;
	}

	public void setSaveActivateAlertAuditLog(boolean saveActivateAlertAuditLog) {
		this.saveActivateAlertAuditLog = saveActivateAlertAuditLog;
	}

	public String getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(String originalAmount) {
		this.originalAmount = originalAmount;
	}
}