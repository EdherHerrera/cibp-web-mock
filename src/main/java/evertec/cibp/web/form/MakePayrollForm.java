package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.services.requests.adaptiveauth.ChallengeOobRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RsaChallengeRequest;
import evertec.cibp.core.services.requests.adaptiveauth.ValidateOobChallengeRequest;
import evertec.cibp.core.services.responses.adaptiveauth.ChallengeOobResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceRequest;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.factory.TVAccountFactory;
import bppr.tv.beans.accounts.types.DepositAccount;
import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.customer.oob.CustomerOOBChallengeType;
import evertec.cibp.core.models.entitlement.BillPayEntitlement;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.RSAEntitlement;
import evertec.cibp.core.models.holiday.Holiday;
import evertec.cibp.core.models.payee.GlobalPayeePaymentType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.FavoritePaymentComparator;
import evertec.cibp.core.models.payment.TVAccountFavoritePayment;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.SendSmsGeneratedCodeRequest;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.BankingDaysUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.vs.beans.payments.Payee;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.form.bean.EPayrollsListBean;
import evertec.cibp.web.form.bean.MultiplePaymentBean;
import evertec.cibp.web.form.bean.PaymentBean;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.PaymentSortbyAccount;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.UserAccount;
import evertec.rsa.RSAConnector.Lang;

public class MakePayrollForm extends AbstractWizardFormController
		implements GlobalEntitlementAwareController, MessageSourceAware {

	/**
	  * finishCancel field set to false
	  */
	@SuppressWarnings("unused")
	private boolean finishCancel = false;
	
	/**
	  * recCodeBlockLength field
	  */
	private int recCodeBlockLength;
	
	/**
	  * recCodeMaxRetries field
	  */
	private int recCodeMaxRetries;
	
	/**
	  * recCodeRetryTimePeriod field
	  */
	private int recCodeRetryTimePeriod;
	
	/**
	  * cancelView field
	  */
	private String cancelView;
	
	protected final Log logger = LogFactory.getLog(getClass());
	protected CustomerServices customerService;
	
	/**
	  * messageSource field
	  */
	private MessageSource messageSource;
	
	/**
	  * telenominaID field
	  */
	private String telenominaID;
	
	/**
	  * productId field
	  */
	private String[] productId;
	
	/**
	  * evendorId field
	  */
	private String evendorId;
	
	/**
	  * userSessionLiteral field
	  */
    private final String userSessionLiteral = "userSession"; //The "userSession" literal for UserSession object

	public MakePayrollForm() {
		super();
		setCommandClass(EPayrollsListBean.class);
		setCommandName("payrolllist");
		setPages(new String[] { "makeEPayroll", "makeEPayroll", "makeEPayroll" });
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object payrolllist,
			BindException error) throws Exception {

		final UserSession userSession = KiuwanUtils.cast(UserSession.class,
				WebUtils.getSessionAttribute(request, userSessionLiteral)); //User session
		CustomerProfile customer = userSession.getCustomerProfile();
		List<TVPayment> confirmPayments = new LinkedList<>();
		List<TVPayment> errorPayments = new LinkedList<>();

		if (userSession.isOobRedirectToLogin()) {

			return new ModelAndView("redirect:login");

		} else if (userSession.isOobRedirectToBlock()) {

			return new ModelAndView("redirect:login?username=" + userSession.getUsername());
		}

		userSession.setOobAuthenticated(true);
		request.getSession().removeAttribute("oobCode");

		List<TVFrontEndPayee> payees = userSession.getPayessBasicEbpp(true);

		BigDecimal totalcomplete = BigDecimal.valueOf(0.0d);
		BigDecimal totalfail = BigDecimal.valueOf(0.0d);

		EPayrollsListBean payrollslist = (EPayrollsListBean) payrolllist;
		PaymentSortbyAccount paymentsort = new PaymentSortbyAccount();
		boolean isEPayrollDone = false;
		Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsPaymentFrom();
		Map<String, Object> model = new HashMap<>();
		model.put("telenominaID", telenominaID);
		model.put("productId", productId);
		boolean isEvendorActivated = StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR);
		model.put("isEvendorActivated", isEvendorActivated);
		model.put("statusPayrollApp", userSession.getPayrollAppStatus());
		model.put("OobEnroll", userSession.getOobEnroll());
		if (payrollslist.getPayrolls() != null)
			for (Iterator<PaymentBean> iterator = payrollslist.getPayrolls().iterator(); iterator.hasNext();) {
				PaymentBean name = iterator.next();
				if (name.getAmount() == null)
					continue;

				String accountfrom = "";
				if (!payrollslist.getSingleAccountPayrollFrom().equalsIgnoreCase("none")
						&& !payrollslist.getSingleAccountPayrollFrom().equalsIgnoreCase("multiple")) {
					accountfrom = payrollslist.getSingleAccountPayrollFrom();
				} else {
					accountfrom = name.getAccountFrom();
				}

				TVAccountTransferSource sou = null;

				// If it is a PIF account, create it
				if (accountfrom.startsWith("PIF")) {
					String externalAccountId = accountfrom.substring(3);
					PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
					if (externalAccount != null) {
						BankInformation bankInformation = new BankInformation();
						bankInformation.setBankId(externalAccount.getRoutingNumber());
						bankInformation.setBankName(externalAccount.getBankName());
						CustomerAccountPreferences pref = new CustomerAccountPreferences();
						TVAccountFactory accountFactory = new TVAccountFactory();
						Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
								externalAccount.getAccountNumber());
						sourceAccount.setAccountID(externalAccount.getAccountNumber());
						sourceAccount.setSubType(Character.toString(externalAccount.getAccountType())); // Checking/Savings/Reserve
						((DepositAccount) sourceAccount)
								.setSectionIndicator(Character.toString(externalAccount.getAccountType()));
						TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
						fromAccount.setCustomerAccountPreferences(pref);
						fromAccount.setNickname(externalAccount.getNickname());
						fromAccount.setHide(externalAccount.getShow() == '0');
						fromAccount.getTVAccount().setBankInformation(bankInformation);
						sou = new TVAccountTransferSource(fromAccount);
					}
				} else {
					sou = Utils.getAccountSource(accountfrom, accountsFrom);
				}

				TVFrontEndPayee tar = Utils.getPayeebyId(name.getPayeeid(), payees);
				if (sou == null || tar == null)
					continue;

				BigDecimal amount = name.getAmount();
				Calendar cal = Calendar.getInstance();
				cal.setTime(name.getEffectiveDate());

				TVPayment payment = null;

				String recurrent = name.getRecurrent();
				String infinite = name.getInfinite();

				if (recurrent != null && recurrent.equalsIgnoreCase("recurrent")) {
					Integer inst = name.getInstances();

					if (infinite != null && infinite.equalsIgnoreCase("infinite")) {
						inst = 999;
					}
					// else infinite == byinstances
					payment = new TVPayment(amount, name.getFrequency(), inst, cal, sou, tar);
				} else
					payment = new TVPayment(amount, Frequency.MANUALLY, 0, cal, sou, tar);

				// CIBP-2548 JOCAMPO
				if (payment.getPayee().getGlobalPayeeIdString() != null
						&& payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
					payment.getPayee().setTelenomina(true);
				} else {
					payment.getPayee().setTelenomina(false);
				}
				// mpicado, CIBP-1799
				PaymentResponse res = PaymentsServiceModule.getInstance().sendPayments(customer, userSession, payment,
						Utils.generateCustomerInteractionEvent(request), customerService);
				payment = res.getPayment();
				payment.getTVPayment().setEffectiveDate(cal);
				payment.setVendorSubject("");
				payment.setEmail("");
				payment.setComments("");
				payment.setInvoiceNumber("");
				payment.setEmployeeName(tar.getEmployeeName());
				if (isEvendorActivated) {
					payment.setReceivingDate(this.calculateReceivingDate(customerService.getHolidays(),
							payment.getTVPayment().getEffectiveDate(), tar.getGlobalPayee().getPaymentType()));
				} else {
					payment.setReceivingDate("");
				}
				if (tar.getGlobalPayee() != null && tar.getGlobalPayee().getPaymentType() != null) {
					if (tar.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO
							|| tar.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PUBLICO)
						payment.setEstimateDate("CHECK");
					else
						payment.setEstimateDate(Utils.getEstimateDate(customerService.getHolidays(), cal,
								tar.getGlobalPayee().getPaymentType()));
				} else
					payment.setEstimateDate(
							Utils.getEstimateDate(customerService.getHolidays(), cal, GlobalPayeePaymentType.INTERNO));
				if (res.getStatus() == ResultStatus.SUCCESS) {
					if (res.getPayment().getPayee().isHasNewEbill()) {
						customerService.updateAccountPayment(userSession.getEbpp2UserId(),
								res.getPayment().getPayee().getEbpp2UserAccount().getBillerAccountID());
						// Alizano hide new ebill message when payment is
						// success Fix Incident 1537231
						res.getPayment().getPayee().setHasNewEbill(false);
						if (res.getPayment().getPayee() != null
								&& res.getPayment().getPayee().getEbpp2UserAccount() != null) {
							res.getPayment().getPayee().getEbpp2UserAccount().setLastEbillPaid(true);
						}
						// End Fix
					}
					confirmPayments.add(payment);
					totalcomplete = totalcomplete.add(payment.getAmount());
					/*
					 * mpicado, CIBP-1799, esto se debe validar porque esto va a la base de datos de
					 * EBPP, y sin la condicion la aplicacion se va a caer si no tiene acceso a EBPP
					 */
					if (this.getEBPPMessageSourceStatus() && customerService
							.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {

						// The EBPP2 user id is not yet in session; add it
						if (!userSession.isEbpp2UserIdLookedUp()) {
							EbppEntitlement ent = null;
							if (customer.getEntitlementByType(EntitlementType.EBILLS) != null) {
								ent = new EbppEntitlement(customer.getEntitlementByType(EntitlementType.EBILLS));

								// If the username is blank, we need to look in
								// the current EBPP accounts ONLY
								if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
									userSession.setEbpp2UserId("");
								} else {
									userSession.setEbpp2UserId(ent.getUsername());
								}
								userSession.setEbpp2UserIdLookedUp(true);
							}
						}

						// The user has an EBPP2 ID
						if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

							// Find the account in the user accounts list
							List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

							// Loop through all the accounts and send the update
							// payment message if necessary
							for (Iterator<UserAccount> iterator2 = ebpp2UserAccounts.iterator(); iterator2.hasNext();) {
								UserAccount userAccount = (UserAccount) iterator2.next();

								if (userAccount.getAccountNumber().replaceFirst("^0+(?!$)", "")
										.equals(tar.getBillingAccount().replaceFirst("^0+(?!$)", ""))) {

									if (!userAccount.isLastEbillPaid()) {
										customerService.updateAccountPayment(userSession.getEbpp2UserId(),
												userAccount.getBillerAccountID());
										// Alizano hide new ebill message when
										// payment is success Fix Incident
										// 1537231
										res.getPayment().getPayee().setHasNewEbill(false);
										if (res.getPayment().getPayee() != null
												&& res.getPayment().getPayee().getEbpp2UserAccount() != null) {
											res.getPayment().getPayee().getEbpp2UserAccount().setLastEbillPaid(true);
										}
										// End Fix
									}
									break;
								}
							}
						}
					}
				} else {
					/*
					 * CIBP-1680 bgarcia Se agrega la logica para poner el detalle del mensaje
					 */
					String er = getTelepagoErrorDescriptor(res.getErrorEBA(),
							Utils.getRSALanguage(request).toLowerCase());
					if (er == null || er.equals("")) {
						payment.setPaymentMessage(res.getErrorMessage());
					} else {
						payment.setPaymentMessage(er + " - " + res.getErrorEBA());
					}
					errorPayments.add(payment);
					totalfail = totalfail.add(payment.getAmount());
				}
			}

		for (Iterator<TVPayment> iterator = confirmPayments.iterator(); iterator.hasNext();) {
			TVPayment payment = iterator.next();
			paymentsort.addPayment(payment);

		}
		for (Iterator<TVPayment> iterator = errorPayments.iterator(); iterator.hasNext();) {
			TVPayment payment = iterator.next();
			paymentsort.addPayment(payment);
		}

		boolean errorpay = false;
		model.put("paymentsort", paymentsort.getElementMap());

		if (confirmPayments != null && !confirmPayments.isEmpty()) {
			model.put("confirmPayments", confirmPayments);
			model.put("totalConfirm", totalcomplete);
		}
		if (errorPayments != null && !errorPayments.isEmpty()) {
			model.put("errorPayments", errorPayments);
			model.put("totalFail", totalfail);
			errorpay = true;
		}
		if (!errorpay) {
			if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				isEPayrollDone = true;
				userSession.setStatusmessage(
						"Thanks! You will receive a confirmation of these transactions via email and/or text message.");
			} else {
				isEPayrollDone = true;
				userSession.setStatusmessage(
						"&iexcl;Gracias! Recibir&aacute;s evidencia de estas transacciones por email y/o mensaje de texto.");
			}
		} else {
			if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				isEPayrollDone = true;
				userSession.setStatusmessage("Please verify errors in payments");
			} else {
				userSession.setStatusmessage("Tienes errores en pagos");
				isEPayrollDone = true;
			}
		}

		// Send Email Confirmation of e-Payroll regardless if alert has been
		// activated
		customerService.sendEPayrollPaymentConfirmation(customer, confirmPayments, errorPayments,
				userSession.getPreferredLanguage(),
				userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)));

		model.put("statusmessage", userSession.getStatusmessage());
		userSession.setStatusmessage("");
		model.put("isEPayrollDone", isEPayrollDone);
		model.put("page", "2");
		// Set RSA cookie
		Cookie cookie = WebUtils.getCookie(request, "PMData") != null ? WebUtils.getCookie(request, "PMData")
				: new Cookie("PMData", "");
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		ESAPI.httpUtilities().addCookie(response, cookie);
		
		return new ModelAndView("makeEPayroll", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		UserSession userSession = KiuwanUtils.cast(UserSession.class, 
				WebUtils.getSessionAttribute(request, userSessionLiteral)); //User session
		
		CustomerProfile customerProfile = userSession.getCustomerProfile();

		customerService.setEBPPStatus(this.getEBPPMessageSourceStatus());
		EPayrollsListBean payrollslist = (EPayrollsListBean) command;

		Map<String, Object> model = new HashMap<>();

		boolean isEvendorActivated = StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR);

		model.put("isEvendorActivated", String.valueOf(isEvendorActivated));
		final boolean realTimeNotificationsClaro = StaticMessageSource
				.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO); //Real Time Notifications iniciative flag
		model.put("realTimeNotificationsClaro", realTimeNotificationsClaro);
		model.put("telenominaID", telenominaID);
		model.put("productId", productId);
		model.put("statusPayrollApp", userSession.getPayrollAppStatus());
		model.put("OobEnroll", userSession.getOobEnroll());

		model.put("MBSFE_138", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138));

		model.put("page", page);

		Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsPaymentFrom();
		List<TVFrontEndPayee> payees = userSession.getPayessBasicEbpp(true);

		// Workaround to add ePayroll/Vendor to favoritePayments for lastpayment in
		// pmtHistoryInfo (Defect 3492 R33)
		List<TVAccountFavoritePayment> favorites = userSession.getFavoritePayments();
		List<TVAccountFavoritePayment> payrollPayments = new ArrayList<TVAccountFavoritePayment>();

		boolean isPayrollActivated = StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL);
		// End Workaround to add ePayroll/Vendor to favoritePayments for lastpayment in
		// pmtHistoryInfo (Defect 3492 R33)

		List<TVAccountFavoritePayment> payrolls = null;
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR)) {
			payrolls = userSession.getPayrollsPayments();
		} else {
			payrolls = userSession.getFavoritePayrolls(telenominaID);
		}

		if (page == 0 || page == 1) {
			// Set holidays
			Map<String, Object> h = new LinkedHashMap<>();
			List<Holiday> holidays = customerService.getHolidays();
			if (holidays != null)
				for (Holiday holiday : holidays) {
					h.put(holiday.getId(),
							new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(holiday.getDate().getTime()));
				}
			model.put("holidays", h);
			model.put("epayroll_ach_efectivedate", StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLLENHA));

			if (payrolls != null) {
				Iterator<TVAccountFavoritePayment> payrollIterator = payrolls.iterator();
				while (payrollIterator.hasNext()) {
					TVAccountFavoritePayment name = payrollIterator.next();
					if (name.getPendingPayments() != null) {
						Iterator<TVPayment> pendingPaymentsIter = name.getPendingPayments().iterator();
						while (pendingPaymentsIter.hasNext()) {
							TVPayment pmt = pendingPaymentsIter.next();
							TVFrontEndPayee py = null;
							if (pmt != null && pmt.getPayeeId() != null) {
								py = Utils.getPayeebyPayeeId(pmt.getPayeeId(), payees);
								pmt.setPayee(py);
							}
						}
					}
				}
			}
			AccountMaskUtil mask = new AccountMaskUtil();
			NumberTool numberTool = new NumberTool();

			Map<String, String> accountsFromMap = new LinkedHashMap<>();

			/*
			 * CIBP-2924 Si el cliente es comercial empezara a cargar informacion
			 */
			if (userSession.getCustomerProfile().getUserInterface() != null) {
				if (userSession.getCustomerProfile().getUserInterface().equals(UserInterface.COMMERCIAL)) {
					Set<TVAccountTransferSource> accountsPayrollFrom = userSession.getAccountsPaymentFrom();
					Map<String, String> accountsFromPayrollMap = new LinkedHashMap<String, String>();
					// Map<String, String> singleAccountPayrollFromMap = new
					// LinkedHashMap<String, String>(accountsFromPayrollMap);
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
						accountsFromPayrollMap.put("none", "Select an account");
					else
						accountsFromPayrollMap.put("none", "Selecciona una cuenta");

					for (Iterator<TVAccountTransferSource> iterator = accountsPayrollFrom.iterator(); iterator
							.hasNext();) {
						TVAccountTransferSource acctfrom = (TVAccountTransferSource) iterator.next();
						TVFrontendAccount accfront = acctfrom.getAccount();
						if (!accfront.getHide()) {
							if (accfront != null && accfront.getPortalBalance() != null && accfront.getSubtype() != null
									&& isCommercialAccount(accfront)) {
								/**
								 * TODO pasar este valor a un properties 021
								 **/
								accountsFromPayrollMap.put(acctfrom.getFrontEndId(),
										StringUtils.abbreviate(accfront.getNickname(), 15) + " "
												+ mask.mask(accfront.getAccountNumber()) + " "
												+ numberTool.format("currency", accfront.getPortalBalance())
												+ ((accfront.getAccountNumberSuffix() != null
														&& !accfront.getAccountNumberSuffix().equals(""))
																? " " + accfront.getAccountNumberSuffix() : ""));

							}
						}
					}
					model.put("singleAccountPayrollFrom", accountsFromPayrollMap);
					if (accountsFromPayrollMap.size() > 2) {
						model.put("hasFavoriteAccount", true);
					} else {
						model.put("hasFavoriteAccount", false);
					}
				}
			}

			Map<String, String> payeeMap = new LinkedHashMap<String, String>();
			Map<String, String> pifPayeeMap = new LinkedHashMap<String, String>();

			if (payees != null)
				for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {

					// Workaround to add ePayroll/Vendor to favoritePayments for lastpayment in
					// pmtHistoryInfo (Defect 3492 R33)
					TVFrontEndPayee pay = (TVFrontEndPayee) iterator.next();

					int tp = pay.getGlobalPayeeId();

					if (isPayrollActivated) {
						if (isEvendorActivated) {
							if (String.valueOf(tp).equalsIgnoreCase(telenominaID)
									|| String.valueOf(tp).equalsIgnoreCase(evendorId)) {
								TVAccountFavoritePayment payeeAux = new TVAccountFavoritePayment(pay);
								payeeAux.setFrontEndId(pay.getFrontEndId());
								payeeAux.getPayee().setTelenomina(true);
								payrollPayments.add(payeeAux);
							}
						} else {
							if (String.valueOf(tp).equalsIgnoreCase(telenominaID)) {
								TVAccountFavoritePayment payeeAux = new TVAccountFavoritePayment(pay);
								payeeAux.setFrontEndId(pay.getFrontEndId());
								payeeAux.getPayee().setTelenomina(true);
								payrollPayments.add(payeeAux);
							}
						}
					}
					// End Workaround to add ePayroll/Vendor to favoritePayments for lastpayment in
					// pmtHistoryInfo (Defect 3492 R33)

					if (tp == 288 || tp == 289 || tp == 4144 || tp == 290 || tp == 12682 || tp == 12453) {
						TVFrontendAccount reserveAct = Utils.getAccountByKey(pay.getBillingAccount().trim() + "|IDA|R",
								userSession.getAccounts());
						if (reserveAct != null && reserveAct.getPortalBalance() != null) {
							pay.setCurrentBalance(reserveAct.getPortalBalance());
						}
						if (pay.getCurrentBalance() != null) {
							payeeMap.put(pay.getFrontEndId(),
									StringUtils.abbreviate(pay.getNickname(), 15) + " "
											+ mask.mask(pay.getBillingAccount()) + "("
											+ numberTool.format("currency", pay.getCurrentBalance()) + ")");
							if (reserveAct != null) {
								if (customerService.accountValidForPIFTransfer(reserveAct))
									pifPayeeMap.put(pay.getFrontEndId(),
											StringUtils.abbreviate(pay.getNickname(), 15) + " "
													+ mask.mask(pay.getBillingAccount()) + "("
													+ numberTool.format("currency", pay.getCurrentBalance()) + ")");
							}
						} else {
							payeeMap.put(pay.getFrontEndId(), StringUtils.abbreviate(pay.getNickname(), 15) + " "
									+ mask.mask(pay.getBillingAccount()));
							if (reserveAct != null) {
								if (customerService.accountValidForPIFTransfer(reserveAct))
									pifPayeeMap.put(pay.getFrontEndId(), StringUtils.abbreviate(pay.getNickname(), 15)
											+ " " + mask.mask(pay.getBillingAccount()));
							}
						}
					} else {
						TVFrontendAccount reserveAct = Utils.getAccountByAccountNumber(pay.getBillingAccount().trim(),
								userSession.getAccounts());
						if (reserveAct != null && reserveAct.getPortalBalance() != null) {
							pay.setCurrentBalance(reserveAct.getPortalBalance());
						}
						if (pay.getCurrentBalance() != null) {
							payeeMap.put(pay.getFrontEndId(),
									StringUtils.abbreviate(pay.getNickname(), 15) + " "
											+ mask.mask(pay.getBillingAccount()) + "("
											+ numberTool.format("currency", pay.getCurrentBalance()) + ")");
							if (reserveAct != null) {
								if (customerService.accountValidForPIFTransfer(reserveAct))
									pifPayeeMap.put(pay.getFrontEndId(),
											StringUtils.abbreviate(pay.getNickname(), 15) + " "
													+ mask.mask(pay.getBillingAccount()) + "("
													+ numberTool.format("currency", pay.getCurrentBalance()) + ")");
							} else {
								Payee payee = (Payee) pay.getBackendObject();
								if (customerService.telepagoIdValidForPIFTransfer(payee.getBillerNumber())) {
									pifPayeeMap.put(pay.getFrontEndId(),
											StringUtils.abbreviate(pay.getNickname(), 15) + " "
													+ mask.mask(pay.getBillingAccount()) + "("
													+ numberTool.format("currency", pay.getCurrentBalance()) + ")");
								}
							}
						} else {
							payeeMap.put(pay.getFrontEndId(), StringUtils.abbreviate(pay.getNickname(), 15) + " "
									+ mask.mask(pay.getBillingAccount()));
							if (reserveAct != null) {
								if (customerService.accountValidForPIFTransfer(reserveAct))
									pifPayeeMap.put(pay.getFrontEndId(), StringUtils.abbreviate(pay.getNickname(), 15)
											+ " " + mask.mask(pay.getBillingAccount()));
							} else {
								Payee payee = (Payee) pay.getBackendObject();
								if (customerService.telepagoIdValidForPIFTransfer(payee.getBillerNumber())) {
									pifPayeeMap.put(pay.getFrontEndId(), StringUtils.abbreviate(pay.getNickname(), 15)
											+ " " + mask.mask(pay.getBillingAccount()));
								}
							}
						}
					}

				}
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				payeeMap.put("add", "Add a new payee");
				pifPayeeMap.put("add", "Add a new payee");
			} else {
				payeeMap.put("add", "A&ntilde;adir un comercio");
				pifPayeeMap.put("add", "A&ntilde;adir un comercio");
			}

			Map<String, String> frequencies = new LinkedHashMap<String, String>();
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				frequencies.put("variable", "one payment");
				frequencies.put("recurrent", "recurrent payment");
			} else {
				frequencies.put("variable", "una vez");
				frequencies.put("recurrent", "recurrente");
			}

			if (page == 0) {

				model.put("fromaccounts", accountsFromMap);
				model.put("payeesMap", payeeMap);
				model.put("pifPayeesMap", pifPayeeMap);

				if (payrolls != null && !payrolls.isEmpty())
					Collections.sort(payrolls, new FavoritePaymentComparator());

				// The EBPP2 user id is not yet in session; add it
				if (!userSession.isEbpp2UserIdLookedUp()) {
					EbppEntitlement ent = null;
					if (customerProfile.getEntitlementByType(EntitlementType.EBILLS) != null) {
						ent = new EbppEntitlement(customerProfile.getEntitlementByType(EntitlementType.EBILLS));

						// If the username is blank, we need to look in the
						// current EBPP accounts ONLY
						if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
							userSession.setEbpp2UserId("");
						} else {
							userSession.setEbpp2UserId(ent.getUsername());
						}
						userSession.setEbpp2UserIdLookedUp(true);
					}
				}

				// The user has an EBPP2 ID
				int tamFavorite = 0;
				if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

					List<UserAccount> ebpp2UsrAccounts = userSession.getEbpp2UserAccountsAndSummaries();
					if (payrolls != null) {
						tamFavorite = payrolls.size();
						for (int i = 0; i < tamFavorite; i++) {
							for (UserAccount userAccount : ebpp2UsrAccounts) {
								if (userAccount != null && !GenericValidator.isBlankOrNull(userAccount.getPayeeId())) {
									if (payrolls.get(i).getPayee().getGlobalPayeeId() == Integer
											.parseInt(userAccount.getPayeeId())
											&& payrolls.get(i).getPayee().getBillingAccount()
													.replaceFirst("^0+(?!$)", "").equals(userAccount.getAccountNumber()
															.replaceFirst("^0+(?!$)", ""))) {
										if (!userAccount.isLastEbillPaid()) {
											payrolls.get(i).getPayee().setHasNewEbill(true);
											payrolls.get(i).getPayee().setEbpp2UserAccount(userAccount);
										}
									}
								}
							}
						}
						model.put("valueTamPayrollList", payrolls.size());
					} else {
						model.put("valueTamPayrollList", 0);
					}
				}

				// Workaround to add ePayroll/Vendor to favoritePayments for lastpayment in
				// pmtHistoryInfo (Defect 3492 R33)
				if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

					if (isPayrollActivated) {
						if (favorites == null) {
						    final int favoritesArrayListInitialCapacity = 0; //ArrayList initial capacity
							favorites = new ArrayList<TVAccountFavoritePayment>(favoritesArrayListInitialCapacity);
						}

						if (favorites != null) {
							Iterator<TVAccountFavoritePayment> favoriteIterator = favorites.iterator();
							while (favoriteIterator.hasNext()) {
								TVAccountFavoritePayment favoritePay = favoriteIterator.next();
								if (isEvendorActivated) {
									if (favoritePay.getPayee().getGlobalPayee().getId().toString()
											.equalsIgnoreCase(telenominaID)
											|| favoritePay.getPayee().getGlobalPayee().getId().toString()
													.equalsIgnoreCase(evendorId)) {
										favoriteIterator.remove();
									}
								} else {
									if (favoritePay.getPayee().getGlobalPayee().getId().toString()
											.equalsIgnoreCase(telenominaID)) {
										favoriteIterator.remove();
									}
								}

							}
						}
						favorites.addAll(payrollPayments);
					}
				}
				// End Workaround to add ePayroll/Vendor to favoritePayments for lastpayment in
				// pmtHistoryInfo (Defect 3492 R33)

				model.put("favoritePayrolls", payrolls);
				model.put("frequencymap", frequencies);

				int size = 3;

				model.put("listSize", size);

				// Set up today's date
				SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				Calendar n = calculateEffDate(Calendar.getInstance());

				model.put("todayDate", df.format(n.getTime()));
				df = new SimpleDateFormat("MM");
				model.put("month", df.format(n.getTime()));
				df = new SimpleDateFormat("dd");
				model.put("day", df.format(n.getTime()));

				if (n.get(Calendar.YEAR) - Calendar.getInstance().get(Calendar.YEAR) >= 1) {
					model.put("dateMismatch", true);
				}

				// TODO verificar si existe una cuenta defatul para pagos y
				// ponerla como valor del singleAccountPayrollFrom
				if ((payrollslist.getSingleAccountPayrollFrom() == null
						|| payrollslist.getSingleAccountPayrollFrom().equals(""))
						&& customerProfile.hasEntitlement(EntitlementType.BILLPAY)) {
					BillPayEntitlement ent = new BillPayEntitlement(
							customerProfile.getEntitlementByType(EntitlementType.BILLPAY));
					if (ent.getDetailView())
						model.put("showDetail", true);
					else
						model.put("showDetail", false);

					if (ent.getAccountKeyPayroll() != null && !ent.getAccountKeyPayroll().equals("")) {
						String defaultaccountkey = ent.getAccountKeyPayroll();
						TVAccountTransferSource defaultAccount = Utils.getAccountSourceByActKey(defaultaccountkey,
								accountsFrom);
						if (defaultAccount != null) {
							payrollslist.setSingleAccountPayrollFrom(defaultAccount.getFrontEndId());
						}
					}
				}

			} else if (page == 1) {
				model.put("confirm", "true");
				model.put("fromaccounts", accountsFromMap);
				model.put("payeesMap", payeeMap);
				model.put("pifPayeesMap", pifPayeeMap);
				model.put("favoritePayrolls", payrolls);
				model.put("frequencymap", frequencies);

				int size = 3;
				model.put("listSize", size);
				model.put("todayDate", "");

			}

			// end of if page ==0 || page ==1
			if (page == 1) {
				boolean isMultipleAccountPayment = false;
				PaymentSortbyAccount paymentsort = new PaymentSortbyAccount();

				List<MultiplePaymentBean> multipleaccounts = new LinkedList<>();
				if (payrollslist.getSingleAccountPayrollFrom().equalsIgnoreCase("multiple")) {
					model.put("hasmultipleaccounts", "true");
					isMultipleAccountPayment = true;
				}

				BigDecimal totalverify = BigDecimal.valueOf(0.0d);

				for (Iterator<PaymentBean> iterator = payrollslist.getPayrolls().iterator(); iterator.hasNext();) {
					PaymentBean name = iterator.next();
					if (name.getAmount() == null)
						continue;
					String accountfrom = "";
					if (!payrollslist.getSingleAccountPayrollFrom().equalsIgnoreCase("none")
							&& !payrollslist.getSingleAccountPayrollFrom().equals("multiple")) {
						accountfrom = payrollslist.getSingleAccountPayrollFrom();
					} else {
						accountfrom = name.getAccountFrom();
					}

					TVAccountTransferSource sou = null;

					// If it is a PIF account, create it
					if (accountfrom.startsWith("PIF")) {
						String externalAccountId = accountfrom.substring(3);
						PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
						if (externalAccount != null) {
							BankInformation bankInformation = new BankInformation();
							bankInformation.setBankId(externalAccount.getRoutingNumber());
							bankInformation.setBankName(externalAccount.getBankName());
							CustomerAccountPreferences pref = new CustomerAccountPreferences();
							TVAccountFactory accountFactory = new TVAccountFactory();
							Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
									externalAccount.getAccountNumber());
							sourceAccount.setAccountID(externalAccount.getAccountNumber());
							sourceAccount.setSubType(Character.toString(externalAccount.getAccountType())); // Checking/Savings/Reserve
							((DepositAccount) sourceAccount)
									.setSectionIndicator(Character.toString(externalAccount.getAccountType()));
							TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
							fromAccount.setCustomerAccountPreferences(pref);
							fromAccount.setNickname(externalAccount.getNickname());
							fromAccount.setHide(externalAccount.getShow() == '0');
							fromAccount.getTVAccount().setBankInformation(bankInformation);
							sou = new TVAccountTransferSource(fromAccount);
						}
					} else {
						sou = Utils.getAccountSource(accountfrom, accountsFrom);
					}

					TVFrontEndPayee tar = Utils.getPayeebyId(name.getPayeeid().trim(), payees);
					if (sou == null || tar == null)
						continue;

					BigDecimal amount = name.getAmount();

					Calendar cal = Calendar.getInstance();
					cal.setTime(name.getEffectiveDate());
					cal = calculateEffDate(cal);
					name.setEffectiveDate(cal.getTime());

					TVPayment payment = null;
					String recurrent = name.getRecurrent();
					String infinite = name.getInfinite();

					if (recurrent != null && recurrent.equalsIgnoreCase("recurrent")) {

						Integer inst = name.getInstances();

						if (inst == null)
							inst = 0;

						if (infinite != null && infinite.equalsIgnoreCase("infinite")) {
							inst = 999;
						}
						// else infinite == byinstances
						payment = new TVPayment(amount, name.getFrequency(), inst, cal, sou, tar);
					} else
						payment = new TVPayment(amount, Frequency.MANUALLY, Integer.valueOf(0), cal, sou, tar);

					if (tar.getGlobalPayee() != null && tar.getGlobalPayee().getPaymentType() != null) {
						if (tar.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO
								|| tar.getGlobalPayee()
										.getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PUBLICO)
							payment.setEstimateDate("CHECK");
						else
							payment.setEstimateDate(Utils.getEstimateDate(customerService.getHolidays(), cal,
									tar.getGlobalPayee().getPaymentType()));
					} else
						payment.setEstimateDate(Utils.getEstimateDate(customerService.getHolidays(), cal,
								GlobalPayeePaymentType.INTERNO));

					totalverify = totalverify.add(payment.getAmount());

					paymentsort.addPayment(payment);

					if (isMultipleAccountPayment) {
						boolean found = false;
						int i = 0;
						for (Iterator<MultiplePaymentBean> multiiterator = multipleaccounts.iterator(); multiiterator
								.hasNext();) {
							MultiplePaymentBean multi = multiiterator.next();
							if (multi.getAccountNumber().equals(sou.getAccount().getAccountNumber())) {
								found = true;
								multi.setAmount(multi.getAmount().add(payment.getAmount()));
								multipleaccounts.set(i, multi);
							} else
								i++;
						}
						if (!found) {
							MultiplePaymentBean multi = new MultiplePaymentBean();
							multi.setAccountNumber(sou.getAccount().getAccountNumber());
							multi.setAmount(payment.getAmount());
							multipleaccounts.add(multi);
						}
					}
				}

				model.put("paymentsort", paymentsort.getElementMap());
				model.put("totalAmount", totalverify);
				model.put("multipleaccounts", multipleaccounts);

				if (payrollslist.getSingleAccountPayrollFrom() != null
						&& !"".equals(payrollslist.getSingleAccountPayrollFrom())
						&& !"none".equals(payrollslist.getSingleAccountPayrollFrom())
						&& !"multiple".equals(payrollslist.getSingleAccountPayrollFrom())) {
					TVAccountTransferSource sou = null;

					// If it is a PIF account, create it
					if (payrollslist.getSingleAccountPayrollFrom().startsWith("PIF")) {
						String externalAccountId = payrollslist.getSingleAccountPayrollFrom().substring(3);
						PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
						if (externalAccount != null) {
							BankInformation bankInformation = new BankInformation();
							bankInformation.setBankId(externalAccount.getRoutingNumber());
							bankInformation.setBankName(externalAccount.getBankName());
							CustomerAccountPreferences pref = new CustomerAccountPreferences();
							TVAccountFactory accountFactory = new TVAccountFactory();
							Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
									externalAccount.getAccountNumber());
							sourceAccount.setAccountID(externalAccount.getAccountNumber());
							sourceAccount.setSubType(Character.toString(externalAccount.getAccountType())); // Checking/Savings/Reserve
							((DepositAccount) sourceAccount)
									.setSectionIndicator(Character.toString(externalAccount.getAccountType()));
							TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
							fromAccount.setCustomerAccountPreferences(pref);
							fromAccount.setNickname(externalAccount.getNickname());
							fromAccount.setHide(externalAccount.getShow() == '0');
							fromAccount.getTVAccount().setBankInformation(bankInformation);
							sou = new TVAccountTransferSource(fromAccount);
						}
					} else {
						sou = Utils.getAccountSource(payrollslist.getSingleAccountPayrollFrom(), accountsFrom);
					}

					if (sou != null && sou.getAccount() != null && sou.getAccount().getPortalBalance() != null) {
						if (sou.getAccount().getSubtype().equals("CCA")) {
							if (sou.getAccount().getAvailableBalance().doubleValue() < totalverify.doubleValue())
								model.put("showAlertTotal", true);
						} else// IDA o PFC
						{
							if (sou.getAccount().getPortalBalance().doubleValue() < totalverify.doubleValue())
								model.put("showAlertTotal", true);
						}
					}
				}

			}
		}

		//////////////////// OOB
		EPayrollsListBean bean = (EPayrollsListBean) command;
		EPayrollsListBean.ACTION action = bean.getAction();
		if (page == 1) {
			// OOB challenge method

			CustomerProfile profile = userSession.getCustomerProfile();
			CustomerEntitlement rsaEnt = profile.getEntitlementByType(EntitlementType.RSA);

			String phone = null;
			String provider = null;
			DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
					userSession.getDeviceRequest().getDeviceTokenCookie(), request);
			RsaChallengeRequest rsaChallengeRequest = new RsaChallengeRequest(userSession.getUsername(), deviceRequest);

			CredentialType credentialType = customerService.rsaGetChallengeMethod(rsaChallengeRequest);
			userSession.setDeviceRequest(deviceRequest);

			if (credentialType.equals(CredentialType.OOBPHONE))
				model.put("challengeType", CustomerOOBChallengeType.OOBPHONE.toString());
			else
				model.put("challengeType", CustomerOOBChallengeType.OOBSMS.toString());

			if (action == EPayrollsListBean.ACTION.SEND_SMSCODE || action == EPayrollsListBean.ACTION.VALIDATE_SMSCODE
					|| action == EPayrollsListBean.ACTION.VALIDATE_CALLCODE) {
				// Retrieve primary phone number (MB Text)
				model.put("from", "phone");
				if (profile.getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
					SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
					if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
							&& !smsprofile.getCtns().isEmpty()) {
						phone = ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn();
						provider = ((SMSCTN) smsprofile.getCtns().iterator().next()).getProvider();
					}
				}
				String altPhone = rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE);
				if (altPhone != null && !altPhone.equalsIgnoreCase("N")) {
					model.put("hasAltPhone", true);
					model.put("altphone", RSAUtils.setPhoneMask(altPhone));
				} else
					model.put("hasAltPhone", false);
			} else if (action == EPayrollsListBean.ACTION.ALT_PHONE
					|| action == EPayrollsListBean.ACTION.VALIDATE_ALTSMSCODE
					|| action == EPayrollsListBean.ACTION.VALIDATE_ALTCALLCODE) {
				// Retrieve alternate phone number
				if (rsaEnt != null) {
					phone = rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE);
					provider = rsaEnt.getString(RSAEntitlement.ALTERNATEPROVIDER);
					model.put("from", "altphone");
				}
			}

			if (phone == null) {

				if (action == EPayrollsListBean.ACTION.VALIDATE_RECCODE) {
					model.put("from", "reccode");
				} else {
					if (credentialType.equals(CredentialType.OOBPHONE))
						errors.rejectValue("code", "login.oob.call.unable");
					else
						errors.rejectValue("code", "login.oob.codeError.message");

					return model;
				}

			} else {
				model.put("phone", RSAUtils.setPhoneMask(phone));
			}

			// SMS challenge
			if (credentialType.equals(CredentialType.USER_DEFINED)
					&& action != EPayrollsListBean.ACTION.VALIDATE_SMSCODE
					&& action != EPayrollsListBean.ACTION.VALIDATE_ALTSMSCODE) {

				if (action == EPayrollsListBean.ACTION.SEND_SMSCODE || action == EPayrollsListBean.ACTION.ALT_PHONE) {
					if (this.generateCode(request, phone, provider, errors, page))
						model.put("codeSent", true);
					else
						model.put("codeSent", false);

				} else {
					String code = "";
					if (page == 0)
						code = (String) WebUtils.getSessionAttribute(request, "oobCode");
					if (GenericValidator.isBlankOrNull(code)) {
						this.generateCode(request, phone, provider, errors, page);
					}
				}

				// Voice call challenge
			} else {
				if (action != EPayrollsListBean.ACTION.VALIDATE_SMSCODE
						&& action != EPayrollsListBean.ACTION.VALIDATE_ALTSMSCODE
						&& action != EPayrollsListBean.ACTION.VALIDATE_CALLCODE
						&& action != EPayrollsListBean.ACTION.VALIDATE_ALTCALLCODE) {
					if (action == EPayrollsListBean.ACTION.CALL_PHONE) {
						if (this.makeCall(request, phone, errors))
							model.put("callMade", true);
						else
							model.put("callMade", false);
					} else {
						this.makeCall(request, phone, errors);
					}
					bean.setCode((String) WebUtils.getSessionAttribute(request, "oobCode"));
				}
			}
		}

		model.put("redirectToBlock", userSession.isOobRedirectToBlock());
		model.put("username", userSession.getUsername());

		/////////////////// END OOB
		return model;

	}

	///////////////////////// OOB
	private boolean generateCode(HttpServletRequest request, String phone, String provider, Errors errors, int page) {

		UserSession userSession = KiuwanUtils.cast(UserSession.class, 
				WebUtils.getSessionAttribute(request, userSessionLiteral)); //User session
		
		CustomerProfile customer = userSession.getCustomerProfile();
		String user = userSession.getUsername();

		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);

		ChallengeOobRequest challengeOobRequest = new ChallengeOobRequest();
		challengeOobRequest.setCredentialType(CredentialType.USER_DEFINED);
		challengeOobRequest.setChallengephone(phone);
		challengeOobRequest.setDeviceRequest(deviceRequest);
		challengeOobRequest.setUsername(user);
		challengeOobRequest.setNewChallenge(true);

		try {
			ChallengeOobResponse challengeOobResponse =  customerService.getSignOnCustomerServiceModule()
					.challengeOob(challengeOobRequest);

			deviceRequest.setDeviceTokenCookie(challengeOobResponse.getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(challengeOobResponse.getDeviceTokenCookie());
			userSession.setDeviceRequest(deviceRequest);
			userSession.setRsaSessionId(challengeOobResponse.getRsaSessionId());
			userSession.setRsaTransactionId(challengeOobResponse.getRsaTransactionId());

			//
			if (ResultStatus.RSA_UNABLE.equals(challengeOobResponse.getResultStatus())) {
				errors.rejectValue("code", "sms.code.error", "Unable to send code at this time.");
				WebUtils.setSessionAttribute(request, "oobCode", null);
				return false;

			} else if (ResultStatus.RSA_BLOCKED.equals(challengeOobResponse.getResultStatus())) {
				userSession.setOobRedirectToBlock(true);
				WebUtils.setSessionAttribute(request, "oobCode", null);
				logger.error("RSA User " + userSession.getUsername() + " blocked by OOB SMS challenge");
				return false;
			}

			String code = challengeOobResponse.getOtp();
			SendSmsGeneratedCodeRequest codereq = new SendSmsGeneratedCodeRequest(
					Utils.generateCustomerInteractionEvent(request), customer, code, phone, provider);

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				customerService.sendGeneratedCode(codereq, PreferredLanguage.ENGLISH);
			else
				customerService.sendGeneratedCode(codereq, PreferredLanguage.SPANISH);

			WebUtils.setSessionAttribute(request, "oobCode", code);

			return true;

		} catch (Exception e) {
			errors.rejectValue("code", "sms.code.error", "Unable to send code at this time.");
			WebUtils.setSessionAttribute(request, "oobCode", null);
			return false;
		}
	}

	private boolean makeCall(HttpServletRequest request, String phone, Errors errors) {

		UserSession userSession = KiuwanUtils.cast(UserSession.class, 
				WebUtils.getSessionAttribute(request, userSessionLiteral)); //User session
		
		String user = userSession.getUsername();

		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);

		ChallengeOobRequest challengeOobRequest = new ChallengeOobRequest();
		challengeOobRequest.setCredentialType(CredentialType.OOBPHONE);
		challengeOobRequest.setChallengephone(phone);
		challengeOobRequest.setDeviceRequest(deviceRequest);
		challengeOobRequest.setUsername(user);
		challengeOobRequest.setLanguage("EN".equalsIgnoreCase(Utils.getRSALanguage(request)) ? Lang.en : Lang.es);
		challengeOobRequest.setNewChallenge(true);

		try {
			ChallengeOobResponse challengeOobResponse = customerService.getSignOnCustomerServiceModule()
					.challengeOob(challengeOobRequest);

			deviceRequest.setDeviceTokenCookie(challengeOobResponse.getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(challengeOobResponse.getDeviceTokenCookie());
			userSession.setDeviceRequest(deviceRequest);
			userSession.setRsaSessionId(challengeOobResponse.getRsaSessionId());
			userSession.setRsaTransactionId(challengeOobResponse.getRsaTransactionId());

			if (ResultStatus.RSA_UNABLE.equals(challengeOobResponse.getResultStatus())) {
				userSession.setOobRedirectToLogin(true);
				errors.rejectValue("code", "login.oob.call.unable", "Unable to generate call at this time.");
				WebUtils.setSessionAttribute(request, "oobCode", null);
				return false;
			} else if (ResultStatus.RSA_BLOCKED.equals(challengeOobResponse.getResultStatus())) {
				userSession.setOobRedirectToBlock(true);
				WebUtils.setSessionAttribute(request, "oobCode", null);
				logger.error("RSA User " + userSession.getUsername() + " blocked by OOB call challenge");
				return false;
			}

			WebUtils.setSessionAttribute(request, "oobCode", challengeOobResponse.getToken());
			return true;

		} catch (Exception e) {
			errors.rejectValue("code", "login.oob.call.unable", "Unable to generate call at this time.");
			WebUtils.setSessionAttribute(request, "oobCode", null);
			return false;
		}
	}
	///////////// END OOB

	@Override
	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		return new ModelAndView(cancelView);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		if (page == 0 || page == 1) {
			EPayrollsListBean plist = (EPayrollsListBean) command;

			if (plist.getSingleAccountPayrollFrom() != null) {
				if (plist.getSingleAccountPayrollFrom().equalsIgnoreCase("none")) {
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR)) {
						errors.rejectValue("singleAccountPayrollFrom", "commercial.business.select.required",
								"Please select the account registered in Business Payments.");
					} else {
						errors.rejectValue("singleAccountPayrollFrom", "epayrolls.select.required",
								"Select an account");
					}
				}
			}

			boolean isPifPayment = false;

			UserSession userSession = KiuwanUtils.cast(UserSession.class, 
					WebUtils.getSessionAttribute(request, userSessionLiteral)); //User session
			
			List<TVFrontEndPayee> payees = userSession.getPayessBasicEbpp(true);

			int i = 0;
			int m = 0;
			int noamount = 0;
			for (Iterator<PaymentBean> iter = plist.getPayrolls().iterator(); iter.hasNext();) {
				PaymentBean element = (PaymentBean) iter.next();
				if (element.getAmount() == null) {
					m++;
					noamount++;
					continue;
				}

				BigDecimal maxpayamount = new BigDecimal(999999.99);
				if (element.getAmount().compareTo(maxpayamount) > 0) {
					errors.rejectValue("payrolls[" + m + "].amount", "payments.amount.largermax",
							"Amount larger than max");

				} else if (element.getAmount().doubleValue() < new Double(0.01d)) {
					errors.rejectValue("payrolls[" + m + "].amount", "payments.amt.invalid", "Invalid");

				}

				if ("none".equalsIgnoreCase(plist.getSingleAccountPayrollFrom())
						|| "multiple".equalsIgnoreCase(plist.getSingleAccountPayrollFrom())) {
					if ("none".equalsIgnoreCase(element.getAccountFrom())) {
						errors.rejectValue("payrolls[" + m + "].accountFrom", "payments.acctfrom.required",
								"Account From is required");

					}
				}

				if ("none".equalsIgnoreCase(element.getPayeeid())) {
					errors.rejectValue("payrolls[" + m + "].payeeid", "payments.payee.invalid", "Payee is required");

				}

				if (element.getRecurrent() == null || "variable".equalsIgnoreCase(element.getRecurrent()))
					element.setFrequency(Frequency.MANUALLY);

				Date effectiveDate = element.getEffectiveDate();
				Calendar today = Calendar.getInstance();

				// Set the max date (one year in future)
				Calendar thresholdDate = Calendar.getInstance();
				thresholdDate.add(Calendar.YEAR, 1);

				if (effectiveDate != null) {
					// Validate no future dates
					Calendar n = Calendar.getInstance();
					n.setTime(effectiveDate);

					if ((n.get(Calendar.YEAR) < today.get(Calendar.YEAR))
							|| n.get(Calendar.YEAR) == today.get(Calendar.YEAR)
									&& n.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR)) {
						errors.rejectValue("payrolls[" + m + "].effectiveDate", "payments.date.past",
								"Invalid Date.<br>(Day in the past)");
					} else if (n.after(thresholdDate)) {
						errors.rejectValue("payrolls[" + m + "].effectiveDate", "payments.date.future",
								"Invalid Date.<br>(Date more than one year in future)");
					}

				} else {
					errors.rejectValue("payrolls[" + m + "].effectiveDate", "payments.date.required", "Required");

				}

				if ("recurrent".equalsIgnoreCase(element.getRecurrent())) {
					// Make sure that a frequency is selected
					if (element.getFrequency() == null) {
						errors.rejectValue("payrolls[" + m + "].frequency", "payments.frequency.required",
								"Frequency is required");

					}
					// Make sure that a frequency type is required
					if (GenericValidator.isBlankOrNull(element.getInfinite())) {
						errors.rejectValue("payrolls[" + m + "].infinite", "payments.freqtype.required",
								"Frequency type is required");

					} else if (!element.getInfinite().equalsIgnoreCase("infinite")) {
						if (element.getInstances() == null) {
							errors.rejectValue("payrolls[" + m + "].instances", "payments.instances.required",
									"Instances required");

						} else if (element.getInstances() < 2) {
							errors.rejectValue("payrolls[" + m + "].instances", "payments.amtinstances.required",
									"Instances must be 2 or more");

						}

					}
				}

				if (isPifPayment) {
					TVFrontEndPayee payee = Utils.getPayeebyId(element.getPayeeid(), payees);
					Payee p = (Payee) payee.getBackendObject();
					TVFrontendAccount tvPayee = Utils.getAccountByAccountNumber(p.getBillingAccount(),
							userSession.getAccounts());

					if (tvPayee == null) {
						if (!customerService.telepagoIdValidForPIFTransfer(p.getBillerNumber())) {
							errors.rejectValue("payrolls[" + m + "].effectiveDate", "payments.payee.pifinvalid",
									"Payment not allowed");
						}
					} else if (!customerService.accountValidForPIFTransfer(tvPayee)) {
						errors.rejectValue("payrolls[" + m + "].effectiveDate", "payments.payee.pifinvalid",
								"Payment not allowed");
					}
				}

				m++;
			}

			if ((m - noamount + i) == 0)
				errors.rejectValue("singleAccountPayrollFrom", "payments.pmtamount.required",
						"Please select a payment and amount");

			if (errors.hasErrors()) {
				UserSession session = KiuwanUtils.cast(UserSession.class, 
						WebUtils.getSessionAttribute(request, userSessionLiteral)); //User session
				
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
					session.setStatusmessage("Please verify the information below");
				else
					session.setStatusmessage("Verifica la informaci&oacute;n entrada");
			}

			EPayrollsListBean bean = (EPayrollsListBean) command;
			EPayrollsListBean.ACTION action = bean.getAction();

			bean.setTimeout(false);
			bean.setValidationError(false);

			if (userSession.isOobRedirectToBlock()) {
				return;
			}

			DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
					userSession.getDeviceRequest().getDeviceTokenCookie(), request);

			if (action == EPayrollsListBean.ACTION.CANCEL_OOB) {
				finishCancel = true;
			}

			// Validate SMS & voice call challenges

			if ((page == 1) && (action == EPayrollsListBean.ACTION.VALIDATE_SMSCODE
					|| action == EPayrollsListBean.ACTION.VALIDATE_CALLCODE
					|| action == EPayrollsListBean.ACTION.VALIDATE_ALTSMSCODE
					|| action == EPayrollsListBean.ACTION.VALIDATE_ALTCALLCODE)) {

				ValidateOobChallengeRequest validateRequest = new ValidateOobChallengeRequest();
				validateRequest.setUsername(userSession.getUsername());
				validateRequest.setDeviceRequest(deviceRequest);
				validateRequest.setRsaSessionId(userSession.getRsaSessionId());
				validateRequest.setRsaTransactionId(userSession.getRsaTransactionId());
				validateRequest.setDeviceTokenCookie(deviceRequest.getDeviceTokenCookie());
				validateRequest.setAnswer(bean.getCode());

				if (action == EPayrollsListBean.ACTION.VALIDATE_SMSCODE
						|| action == EPayrollsListBean.ACTION.VALIDATE_ALTSMSCODE) {
					validateRequest.setCredentialType(CredentialType.USER_DEFINED);
					validateRequest.setBindDevice(true);
				} else {
					validateRequest.setCredentialType(CredentialType.OOBPHONE);
					validateRequest.setBindDevice(false);
					validateRequest
							.setLanguage("EN".equalsIgnoreCase(Utils.getRSALanguage(request)) ? Lang.en : Lang.es);
				}

				final ResultStatus statusResult = userSession.getCustomerService().rsaValidateOobChallenge(validateRequest); //Validate OOB challenge

				if (statusResult != null) {

					deviceRequest.setDeviceTokenCookie(validateRequest.getDeviceTokenCookie());
					deviceRequest.setDeviceTokenFSO(validateRequest.getDeviceTokenCookie());
					userSession.setDeviceRequest(deviceRequest);
					userSession.setRsaSessionId(validateRequest.getRsaSessionId());
					userSession.setRsaTransactionId(validateRequest.getRsaTransactionId());

					if (ResultStatus.RSA_UNABLE.equals(statusResult)) {
						errors.rejectValue("code", "login.oob.codevalidation.error",
								"Unable to validate code at this time.");
						bean.setValidationError(true);
					} else if (ResultStatus.RSA_EXPIRED.equals(statusResult)) {
						errors.rejectValue("code", "login.oob.codeexpired.error", "Code has expired.");
						bean.setTimeout(true);
					} else if (ResultStatus.RSA_BLOCKED.equals(statusResult)) {
						userSession.setOobRedirectToBlock(true);
						logger.error("RSA User " + userSession.getUsername() + " blocked by OOB challenge tries");
					} else if (ResultStatus.RSA_FAILED.equals(statusResult)) {
						if (action == EPayrollsListBean.ACTION.VALIDATE_SMSCODE
								|| action == EPayrollsListBean.ACTION.VALIDATE_ALTSMSCODE) {
							errors.rejectValue("code", "sms.code.invalidRSA", "Incorrect code");
						} else
							errors.rejectValue("code", "login.oob.call.error",
									"Input the code on your phone, before clicking continue.");
					}
				}
			}
		}
	}

	public Calendar calculateEffDate(Calendar cal) {
		List<Holiday> holidays = customerService.getHolidays();
		BankingDaysUtils bankingDaysUtils = new BankingDaysUtils(holidays);
		return bankingDaysUtils.calculatePayDaysWithCutoff(cal, 1, 17, 0);
	}
	
	/**
	   * calculateReceivingDate
	   * @param holidays
	   * @param effectiveDate
	   * @param type
	   * @return string from method Utils.getEstimateDate
	   */
	private String calculateReceivingDate(List<Holiday> holidays, Calendar effectiveDate, GlobalPayeePaymentType type) {
		return Utils.getEstimateDate(holidays, effectiveDate, type);
	}

	public String getCancelView() {
		return cancelView;
	}

	public void setCancelView(String cancelView) {
		this.cancelView = cancelView;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.BILLPAY;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de EBPP
	 * en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

	/*
	 * CIBP-1680 bgarcia Obtiene el texto de error actualizado
	 */
	private String getTelepagoErrorDescriptor(String codigo, String len) {
		String status = "";
		try {
			if (!"en".equals(len))
				len = "es";
			status = this.messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
		} catch (Exception e) {
			status = "";
		}

		return status;
	}

	public String[] getProductId() {
		return productId;
	}

	public String getTelenominaID() {
		return telenominaID;
	}

	public void setTelenominaID(String telenominaID) {
		this.telenominaID = telenominaID;
	}

	public String getEvendorId() {
		return evendorId;
	}

	public void setEvendorId(String evendorId) {
		this.evendorId = evendorId;
	}

	public void setProductId(String[] productId) {
		this.productId = productId;
	}

	private boolean isCommercialAccount(TVFrontendAccount account) {
		for (int i = 0; i < productId.length; i++) {
			if (account.getProductDefinition().getProductId().equals(productId[i])) {
				return true;
			}
		}
		return false;
	}

	public int getRecCodeBlockLength() {
		return recCodeBlockLength;
	}

	public int getRecCodeMaxRetries() {
		return recCodeMaxRetries;
	}

	public int getRecCodeRetryTimePeriod() {
		return recCodeRetryTimePeriod;
	}

}
