package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.BinaryData.BinaryRecord;
import evertec.cibp.core.models.BinaryData.BinaryStatus;
import evertec.cibp.core.models.BinaryData.BinaryTypes;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.session.UserSession;

public class GetImage extends AbstractController {

	private CustomerServices customerService;

	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		Map<String, Object> model = new HashMap<>();

		String id = (String) request.getParameter("id");
		String t = (String) request.getParameter("type");
		String s = (String) request.getParameter("status");

		if (t != null && !t.trim().equals("") && s != null && !s.trim().equals("")) {
			BinaryTypes type = BinaryTypes.valueOf(t);
			BinaryStatus status = BinaryStatus.valueOf(s);

			UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			if (userSession != null && type != null && status != null && status == BinaryStatus.CURRENT
					&& userSession.getImageLogin() != null
					&& (type == BinaryTypes.LOGIN_IMAGE_PR || type == BinaryTypes.LOGIN_IMAGE_VI)) {
				model.put("imageRecord", userSession.getImageLogin());
				return new ModelAndView(new ImageView(), model);

			}

			BinaryRecord record = customerService.getImagebyTypeandStatus(type, status);
			model.put("imageRecord", record);
			return new ModelAndView(new ImageView(), model);

		} else if (id != null) {
			BinaryRecord record = customerService.getImagebyID(id);
			model.put("imageRecord", record);
			return new ModelAndView(new ImageView(), model);
		}

		return new ModelAndView(new ImageView(), model);
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

}
