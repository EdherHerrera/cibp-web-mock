package evertec.cibp.web.form;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import evertec.cibp.core.models.account.ATHCardTripNote;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.AccountsFacade;
import evertec.cibp.web.facade.CreditCardFacade;
import evertec.cibp.web.form.bean.CCPortalVacations;
import evertec.cibp.web.request.TravelNotificationRequest;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.prophit.service.bean.CustomerInquiryBean;
import evertec.prophit.service.bean.TravelNotificationBean;
import evertec.prophit.service.bean.TravelNotificationResponse;

public class CcVacationsController extends SimpleFormController implements MessageSourceAware {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;

	public CcVacationsController() {
		super();
		setCommandClass(CCPortalVacations.class);
		setCommandName("ccVacations");
		setFormView("ccVacations");
	}

	private Map<String, Object> getDomesticMap(UserSession userSession) {
		Map<String, Object> domesticMap = new LinkedHashMap<String, Object>();

		String locale = userSession.getLanguage();
		if(locale.equalsIgnoreCase("sp"))
		{
			locale = "es";
		}

		domesticMap.put(
				"D",
				messageSource.getMessage("credit.card.portal.vacations.domestic.yes", null,
						new Locale(locale)));
		domesticMap.put(
				"I",
				messageSource.getMessage("credit.card.portal.vacations.domestic.no", null,
						new Locale(locale)));

		return domesticMap;
	}

	// Added for CIBP-1767
	private List<String> getCCAAccounts(HttpServletRequest request, UserSession userSession) {

		List<TVFrontendAccount> accounts = userSession.getAccounts();
		
		List<String> accountsNumbers = new LinkedList<String>();

		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount frontFrom = (TVFrontendAccount) iterator.next();
			if (frontFrom != null && !frontFrom.getHide()) {
				if (frontFrom.getSubtype().equals("CCA") && frontFrom.getPortalBalance() != null)
					accountsNumbers.add(frontFrom.getAccountNumber());
				else if (frontFrom.getSubtype().equals("CCA"))
					accountsNumbers.add(frontFrom.getAccountNumber());
			}
		}

		return accountsNumbers;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		CCPortalVacations ccVacations = (CCPortalVacations) command;

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile customerProfile = userSession.getCustomerProfile();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		// CIBP-1767 - this obtain all the CCA Account of the user.
		List<String> ccaAccounts = this.getCCAAccounts(request, userSession);

		boolean hasWarning = false;
		boolean hasException = false;
		boolean hasSuccess = false;
		
		String trace = "";
		String ccaResult = "";
		String athResult = "";
		
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
		
		trace += "NOTE INFO:<br/>";
		trace += "FROM: " + dt.format(ccVacations.getDateFrom()) + "<br/>";
		trace += "TO: " + dt.format(ccVacations.getDateTo()) + "<br/>";
		trace += "CITIES: " + ccVacations.getCities() + "<br/>";
		trace += "PHONE: " + ccVacations.getPhone() + "<br/>";
		
		// CIBP-1767 - create a note to each CCA Account that the user have.
		for (int j = 0; j < ccaAccounts.size(); j++) {
			String ccaAccount = ccaAccounts.get(j);
			
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE398)){
				boolean found = false;
				//loop the selected accounts for the note
				for(int i = 0; i < ccVacations.getAccounts().length; i++){
					TVFrontendAccount plastic = Utils.getAccountById(ccVacations.getAccounts()[i], userSession.getCcaAccounts());
					if(plastic != null && plastic.getAccountNumber().equals(ccaAccount)){
						found = true;
						break;
					}
				}
				//if not checked skip it
				if(!found){
					continue;
				}
			}

			CustomerInquiryBean customerInq = customerService.prophItCustomerInquiry(ccaAccount);

			TravelNotificationBean travelInfo = new TravelNotificationBean();
			travelInfo.setCustInq(customerInq);
			travelInfo.setStartDate(dateFormat.format(ccVacations.getDateFrom()));
			travelInfo.setEndDate(dateFormat.format(ccVacations.getDateTo()));
			travelInfo.setNotes(ccVacations.getCities());
			travelInfo.setCustEmail(customerProfile.getEmail());
			travelInfo.setCustPhone(ccVacations.getPhone());
			travelInfo.setTravelFlag(ccVacations.getDomestic());

			List<TravelNotificationResponse> travelResponses = null;
			
			TravelNotificationRequest travelNotificationRequest = new TravelNotificationRequest(travelInfo,customerService);
			

			try {
				travelResponses = CreditCardFacade.getInstance().prophItTravelNotification(travelNotificationRequest);

				if (travelResponses != null && travelResponses.size() > 0) {

					// in case one plastic was not able to be notified in the
					// system.
					for (Iterator<TravelNotificationResponse> iterator2 = travelResponses.iterator(); iterator2
							.hasNext();) {
						TravelNotificationResponse plasticResponse = (TravelNotificationResponse) iterator2.next();
						
						ccaResult += "x" + ccaAccount.substring(ccaAccount.length() - 4, ccaAccount.length());
						ccaResult += " " + plasticResponse.getStatus() + " - ";
						String[] message = plasticResponse.getMessage().split("Account:");
						if(message != null && message.length == 2) {
							ccaResult += "Account:";
							if(message[1].startsWith("377")) {
								String cca = message[1].substring(0, 15);
								ccaResult += "x" + cca.substring(cca.length() - 4, cca.length());
								ccaResult += message[1].substring(16, message[1].length()) + ".";
								
							}else if(message[1].startsWith("4549") || message[1].startsWith("5310")) {
								String cca = message[1].substring(0, 16);
								ccaResult += "x" + cca.substring(cca.length() - 4, cca.length());
								ccaResult += message[1].substring(16, message[1].length()) + ".";
							}
						}
						ccaResult += " <br/>";

						if (!plasticResponse.isSuccess() && !hasWarning)
							hasWarning = true;

						if (plasticResponse.isSuccess() && !hasSuccess)
							hasSuccess = true;
					}

				} else {
					ccaResult += "x" + ccaAccount.substring(ccaAccount.length() - 4, ccaAccount.length());
					ccaResult += " TSYS FAULT.";
					ccaResult += "<br/>";
					if (!hasWarning)
						hasWarning = true;
				}
			} catch (Exception e) {
				if (!hasException)
					hasException = true;

				logger.error("ProphIT Travel Notification Error:");
				logger.error(e);
				
				ccaResult += "x" + ccaAccount.substring(ccaAccount.length() - 4, ccaAccount.length());
				ccaResult += " TSYS FAULT. <br/>";
			}

		}
		
		if(!ccaResult.equals("")){
			trace += "<br/>CCA NOTES: <br/>" + ccaResult;
		}
		
		//loop looking for ATH INT cards
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE398)){
			List<ATHCardTripNote> tripNotes = new ArrayList<ATHCardTripNote>();
			for(int i = 0; i < ccVacations.getAccounts().length; i++){
				TVFrontendAccount plastic = Utils.getAccountById(ccVacations.getAccounts()[i], userSession.getCardPlasticAccounts());
				if (plastic != null && plastic.getAccountNumber().startsWith("4549")) {
					tripNotes.add(new ATHCardTripNote(customerProfile.getProfileId(), plastic.getAccountNumber(), ccVacations.getDateFrom(), ccVacations.getDateTo()));
					athResult += "x" + plastic.getAccountNumber().substring(plastic.getAccountNumber().length() - 4, plastic.getAccountNumber().length()) + " Processed. <br/>";
				}
			}
			
			/**
			 * MBNA-1183
			 * Method addATHCardTripNotes : CIBP-WEB microservice integration
			 * 
			 * */
			AccountsFacade.getInstance().addATHCardTripNotes(tripNotes, request);
			
			//trace
			if(!athResult.equals("")){
				trace += "<br/>ATH INT NOTES: <br/>" + athResult + "<br/>";
			}
		}
		
		CustomerInteractionEvent interaction = Utils.generateCustomerInteractionEvent(request);
		interaction.setCustomStringData("TRACE", trace);
		
		customerService.logTripNotesResult(interaction);
		
		model.put("SHOWFORM", false);

		if (hasException && !hasSuccess)
			model.put("SUCCESS", false);
		else {
			model.put("SUCCESS", true);
			if (hasWarning || hasException)
				model.put("WARNING", true);
			else
				model.put("WARNING", false);

			// send email confirmation only if there was no exception from the
			// web service.
			customerService.sendCcVacationConfirmation(customerProfile, ccVacations.getCities(),
					dateFormat.format(ccVacations.getDateFrom()), dateFormat.format(ccVacations.getDateTo()),
					userSession.getPreferredLanguage());
		}

		return showForm(request, errors, "ccVacations", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		CCPortalVacations ccVacations = (CCPortalVacations) command;

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		ccVacations.setDomesticMap(this.getDomesticMap(userSession));
		
		Boolean MBSFE398 = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE398);
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE3482)) {
			model.put("travelnotification_down", true);
			return model;
		}
		if(MBSFE398){
			Map<String, TVFrontendAccount> filteraccounts = new LinkedHashMap<String, TVFrontendAccount>();
			List<TVFrontendAccount> ccas = userSession.getCcaAccounts();
			List<TVFrontendAccount> aths = userSession.getCardPlasticAccounts();
			List<String> nicknames = new ArrayList<String>();
			String account;
			for (Iterator<TVFrontendAccount> iterator = ccas.iterator(); iterator.hasNext();) {
				TVFrontendAccount frontendAccount = (TVFrontendAccount) iterator.next();
				if (filteraccounts.get(frontendAccount.getAccountNumber()) == null){
					account = frontendAccount.getAccountNumber().substring(frontendAccount.getAccountNumber().length() - 4, frontendAccount.getAccountNumber().length());
					nicknames.add(frontendAccount.getNickname() + "-x" + account);
					filteraccounts.put(frontendAccount.getAccountNumber(), frontendAccount);
				}				
			}
			for (Iterator<TVFrontendAccount> iterator = aths.iterator(); iterator.hasNext();) {
				TVFrontendAccount frontendAccount = (TVFrontendAccount) iterator.next();
				if (frontendAccount.getAccountNumber().startsWith("4549")) {
					if (filteraccounts.get(frontendAccount.getAccountNumber()) == null){
						CardPlasticAccountRecord record = (CardPlasticAccountRecord)frontendAccount.getTVAccount().getRecords();
						if(record != null){
							TVFrontendAccount depositAccount = Utils.getAccountByAccountNumber(record.getAccount().getAccountID(), userSession.getPortalAccounts());
							if(depositAccount != null){
								account = frontendAccount.getAccountNumber().substring(frontendAccount.getAccountNumber().length() - 4, frontendAccount.getAccountNumber().length());
								nicknames.add(depositAccount.getNickname() + "-x" + account);
								filteraccounts.put(frontendAccount.getAccountNumber(), frontendAccount);
							}
						}					
					}
				}
			}
			model.put("customerAccounts", filteraccounts.values());
			model.put("maskAccounts", nicknames);
			
			int tamPayrolls = 510;
			if(!filteraccounts.isEmpty()){
				tamPayrolls = (filteraccounts.size() * 21) + tamPayrolls;
				model.put("tamIframe", tamPayrolls + "px");
			}else{
				model.put("tamIframe", tamPayrolls + "px");
			}
		}

		model.put("SHOWFORM", true);
		
		model.put("MBSFE398", MBSFE398);

		return model;

	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		CCPortalVacations ccVacations = (CCPortalVacations) command;
		boolean allReq = false;
		boolean dateError = false;
		Calendar dtFrom = null;
		Calendar dtTo = null;

		if (ccVacations.getDateFrom() != null && !allReq) {
			Calendar today = Calendar.getInstance();

			// Set the max date (one year in future)
			Calendar thresholdDate = Calendar.getInstance();
			thresholdDate.add(Calendar.YEAR, 1);

			// Validate no future dates
			dtFrom = Calendar.getInstance();
			dtFrom.setTime(ccVacations.getDateFrom());

			if ((dtFrom.get(Calendar.YEAR) < today.get(Calendar.YEAR))
					|| dtFrom.get(Calendar.YEAR) == today.get(Calendar.YEAR)
					&& dtFrom.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR)
					|| (dtFrom.after(thresholdDate)) && !dateError) {
				dateError = true;
				allReq = true;
				errors.rejectValue("error", "credit.card.portal.vacations.date.error",
						"Invalid Date.<br>(No more than one year)");
				ccVacations.setError("true");
			}
		} else if (!allReq) {
			allReq = true;
			errors.rejectValue("error", "credit.card.portal.tab.cc.services.fields.required", "All Fields Are Required");
			ccVacations.setError("true");
		}

		if (ccVacations.getDateTo() != null && !allReq) {
			Calendar today = Calendar.getInstance();

			// Set the max date (one year in future)
			Calendar thresholdDate = Calendar.getInstance();
			thresholdDate.add(Calendar.YEAR, 1);

			// Validate no future dates
			dtTo = Calendar.getInstance();
			dtTo.setTime(ccVacations.getDateTo());

			if ((dtTo.get(Calendar.YEAR) < today.get(Calendar.YEAR))
					|| dtTo.get(Calendar.YEAR) == today.get(Calendar.YEAR)
					&& dtTo.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR) || (dtTo.after(thresholdDate))
					&& !dateError) {
				dateError = true;
				allReq = true;
				errors.rejectValue("error", "credit.card.portal.vacations.date.error",
						"Invalid Date.<br>(No more than one year)");
				ccVacations.setError("true");
			}
		} else if (!allReq) {
			allReq = true;
			errors.rejectValue("error", "credit.card.portal.tab.cc.services.fields.required", "All Fields Are Required");
			ccVacations.setError("true");
		}

		if (dtFrom != null && dtTo != null) {
			if (dtFrom.after(dtTo) && !allReq) {
				allReq = true;
				errors.rejectValue("error", "credit.card.portal.vacations.date.error.after",
						"From Date cannot be after To Date");
				ccVacations.setError("true");
			}
		}

		if ((ccVacations.getCities() == null || ccVacations.getCities().trim().equalsIgnoreCase("")) && !allReq) {
			errors.rejectValue("error", "credit.card.portal.tab.cc.services.fields.required", "All Fields Are Required");
			allReq = true;
			ccVacations.setError("true");
		}

		if ((ccVacations.getPhone() == null || (ccVacations.getPhone().equalsIgnoreCase(""))) && !allReq) {
			errors.rejectValue("error", "credit.card.portal.tab.cc.services.fields.required", "All Fields Are Required");
			allReq = true;
			ccVacations.setError("true");

		} // por defecto #1276
		else if (((ccVacations.getPhone().matches("\\d{10}") != true) && (ccVacations.getPhone().matches(
				"\\d{3}-\\d{3}-\\d{4}") != true))
				|| (ccVacations.getPhone().length() != 12 && ccVacations.getPhone().length() != 10) && !allReq) {
			errors.rejectValue("error", "credit.card.portal.vacations.phone.error", "Phone number format is invalid.");
			allReq = true;
			ccVacations.setError("true");
		}

		if ((ccVacations.getDomestic() == null || ccVacations.getDomestic().trim().equalsIgnoreCase("")) && !allReq) {
			errors.rejectValue("error", "credit.card.portal.tab.cc.services.fields.required", "All Fields Are Required");
			allReq = true;
			ccVacations.setError("true");
		}
		
		if(!allReq && StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE398)){
			if(ccVacations.getAccounts() == null || ccVacations.getAccounts().length == 0){
				errors.rejectValue("error", "credit.card.portal.vacations.plastics.error", "Select at least one plastic.");
				allReq = true;
				ccVacations.setError("true");
			}
		}

	}

	/**
	 * @param messageSource
	 *            the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}
}
