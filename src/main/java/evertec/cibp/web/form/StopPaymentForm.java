package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.StopPaymentRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.StopPaymentResponse;
import evertec.cibp.web.form.bean.AccountServiceBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class StopPaymentForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public StopPaymentForm() {
		super();
		setCommandClass(AccountServiceBean.class);
		setCommandName("accountservices");
		setFormView("stoppayment");
		setBindOnNewForm(true);
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		AccountServiceBean searchBean = (AccountServiceBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();

		TVFrontendAccount account = Utils.getAccountById(searchBean.getAccount(), userSession.getAccounts());
		String checkNumber = searchBean.getStopCheckNumber();

		StopPaymentRequest chkrequest = new StopPaymentRequest(Utils.generateCustomerInteractionEvent(request),
				customer, userSession.getBankingSession(), account, checkNumber);
		StopPaymentResponse chkres = customerService.requestStopPayment(chkrequest);
		if (chkres.getStatus() != ResultStatus.SUCCESS) {
			model.put("fail", true);
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				// userSession.setStatusmessage("Your stop payment request could not be processed at this time.");
				model.put("message", "Your stop payment request could not be processed at this time.");
			} else {
				// userSession.setStatusmessage("Su suspension de cheques no se pudo procesar en este momento.");
				model.put("message", "Su suspension de cheques no se pudo procesar en este momento.");
			}
		} else {
			model.put("success", true);
			String ref = "";
			if (chkres.getReferenceNumber() != null)
				ref = chkres.getReferenceNumber();

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				if (ref != null && !"".equals(ref))
					ref = "Reference: " + ref;
				// userSession.setStatusmessage("Your stop payment request was successfully sent. "
				// + ref);
				model.put("message", "Your stop payment request was successfully sent. " + ref);
			} else {
				if (ref != null && !"".equals(ref))
					ref = "Referencia: " + ref;
				// userSession.setStatusmessage("Su solicitud de detener el pago ha sido enviada exitosamente. "
				// + ref);
				model.put("message", "Su solicitud de detener el pago ha sido enviada exitosamente. " + ref);
			}
			model.put("ref", ref);
		}

		return showForm(request, error, "stoppayment", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		AccountMaskUtil mask = new AccountMaskUtil();

		/****** Reference Data for Stop Payments service *******/
		Map<String, String> stopCheckAccounts = new LinkedHashMap<String, String>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			stopCheckAccounts.put("none", "Select an account");
		else
			stopCheckAccounts.put("none", "Selecciona una cuenta");
		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount name = (TVFrontendAccount) iterator.next();
			if (name.getFeatures().isStopPayment() && !name.getHide())
				stopCheckAccounts
						.put(String.valueOf(name.getFrontEndId()),
								name.getNickname()
										+ " "
										+ mask.mask(name.getAccountNumber())
										+ ((name.getAccountNumberSuffix() != null && !name.getAccountNumberSuffix()
												.equals("")) ? " " + name.getAccountNumberSuffix() : ""));
		}
		model.put("stopCheckAccounts", stopCheckAccounts);

		Map<String, String> agree = new HashMap<String, String>();
		agree.put("agree", "");
		model.put("agree", agree);

		return model;

	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		AccountServiceBean searchBean = (AccountServiceBean) command;

		if (searchBean.getAccount() == null || searchBean.getAccount().equals("none"))
			errors.rejectValue("account", "accountservices.acct.invalid", "Invalid Account Selection");
		if (GenericValidator.isBlankOrNull(searchBean.getAgreement())
				|| !searchBean.getAgreement().equalsIgnoreCase("agree"))
			errors.rejectValue("agreement", "accountservices.agreement.error", "Please agree.");
		if (searchBean.getStopCheckNumber() == null || searchBean.getStopCheckNumber().equals(""))
			errors.rejectValue("stopCheckNumber", "accountservices.checknumber.invalid",
					"Please enter the check number.");

	}

}
