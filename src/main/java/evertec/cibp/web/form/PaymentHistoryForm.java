package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.factory.TVAccountFactory;
import bppr.tv.beans.accounts.types.DepositAccount;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerPayeePreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.GlobalPayeePaymentType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.CancelPayeeResponse;
import evertec.cibp.core.services.responses.PaymentHistoryResponse;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.FrequencyUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.vs.beans.payments.PaymentModify;
import evertec.cibp.vs.beans.payments.RecurringPayment;
import evertec.cibp.vs.models.PaymentFilterType;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.ModPaymentBean;
import evertec.cibp.web.form.bean.PaymentSearchBean;
import evertec.cibp.web.request.ModifyPaymentRequest;
import evertec.cibp.web.request.PaymentHistoryRequest;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.PaymentSort;
import evertec.cibp.web.utils.Utils;

public class PaymentHistoryForm extends CancellableFormController implements GlobalEntitlementAwareController,
		MessageSourceAware {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private int maxPayments = 15;

	private MessageSource messageSource;

	private String maxReverseableDays;
	private String telenominaID;
	private String vendorId;
	private String balanceDelay = "";

	public String getBalanceDelay() {
		return balanceDelay;
	}

	public void setBalanceDelay(String balanceDelay) {
		this.balanceDelay = balanceDelay;
	}

	public String getTelenominaID() {
		return telenominaID;
	}

	public void setTelenominaID(String telenominaID) {
		this.telenominaID = telenominaID;
	}
	
	public String getVendorId() {
		return vendorId;
	}
	
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public PaymentHistoryForm() {
		super();
		setCommandClass(PaymentSearchBean.class);
		setCommandName("paymenthistory");
		setFormView("paymenthistory");
		setBindOnNewForm(true);
	}


	@SuppressWarnings("unused")
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		PaymentSearchBean searchBean = (PaymentSearchBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();

		int action = searchBean.getAction() == 0 ? 1 : searchBean.getAction();
		Map<String, Object> model = new LinkedHashMap<String, Object>();
		model.put("telenominaID", telenominaID);
		if (action == PaymentSearchBean.SEARCH_BY_DATE && searchBean.getDate().equalsIgnoreCase("LAST_15"))
			action = PaymentSearchBean.SEARCH_LAST15;
		if (action == PaymentSearchBean.SEARCH_BY_PAYEE && searchBean.getPayeeId().equalsIgnoreCase("LAST_15"))
			action = PaymentSearchBean.SEARCH_LAST15;

		if (action == PaymentSearchBean.MOD_PAYMENT) {
			boolean hasalert = false;
			if (customer.hasEntitlement(EntitlementType.ALERTS)) {
				AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
				if (ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL)
						|| ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_SMS)
						|| ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH)) {
					hasalert = true;
				}
			}

			searchBean.setAction(1);
			PaymentResponse res = null;

			String tr = searchBean.getModPayment();
			TVPayment payment = null;
			ModPaymentBean modbean = null;
			for (Iterator<ModPaymentBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext();) {
				ModPaymentBean name = (ModPaymentBean) iterator.next();
				if (name != null)
					if (tr.equalsIgnoreCase(name.getModTransferId())) {
						payment = Utils.getPaymentById(tr, userSession.getPendingPayments());
						modbean = name;
						break;
					}
			}
			
			 // CIBP-1104 se utiliza para presentar en la alerta de correo el
			 // pago original previo a la modificacion
			TVPayment paymentOriginal = new TVPayment(new RecurringPayment());
			paymentOriginal.setEstimateDate(payment.getEstimateDate());
			paymentOriginal.setPayee(payment.getPayee());
			paymentOriginal.getTVPayment().setEffectiveDate(payment.getTVPayment().getEffectiveDate());
			paymentOriginal.getTVPayment().setAmount(payment.getAmount());
			paymentOriginal.setSource(payment.getSource());
			paymentOriginal.getTVPayment().setFrequency(payment.getFrequency());
			paymentOriginal.getTVPayment().setRemainingInstances(payment.getTVPayment().getRemainingInstances());
			paymentOriginal.getTVPayment().setReferenceNumber(payment.getTVPayment().getReferenceNumber());

			// IS A RECURRING PAYMENT
			String freq = modbean.getModFreq();
			String inst = modbean.getModInstances();
			String modInstancesType = modbean.getModInstancesType();
			Date modDate = modbean.getModDate();
			Calendar calModDate = Calendar.getInstance();
			int favId = searchBean.getFavId();
			boolean err = false;

			if (GenericValidator.isBlankOrNull(payment.getFrequency())
					|| GenericValidator.isBlankOrNull(freq)
					|| GenericValidator.isBlankOrNull(modInstancesType)
					|| (!"infinite".equalsIgnoreCase(modInstancesType) && (!GenericValidator.matchRegexp(inst,
							"^([1-9]|0[1-9]|[1-9][0-9]|[1-9][0-9]{1,2}|[0-9]{1,2}[0-9])$"))) || modDate == null
			// ||GenericValidator.isBlankOrNull(modDate)
			// ||!GenericValidator.matchRegexp(modDate,
			// "\\d{2}\\/\\d{2}\\/\\d{4}")
			) {

				err = true;

				if (GenericValidator.isBlankOrNull(payment.getFrequency()) || GenericValidator.isBlankOrNull(freq))
					error.rejectValue("favorites[" + favId + "].frequency", "", "Frequency is required");
				if (GenericValidator.isBlankOrNull(modInstancesType)
						|| (!"infinite".equalsIgnoreCase(modInstancesType) && (!GenericValidator.matchRegexp(inst,
								"^([1-9]|0[1-9]|[1-9][0-9]|[1-9][0-9]{1,2}|[0-9]{1,2}[0-9])$"))))
					error.rejectValue("favorites[" + favId + "].modInstances", "paymenthistory.instances.required",
							"Instances must be filled");
				if (modDate == null)
					error.rejectValue("favorites[" + favId + "].frequency", "paymenthistory.date.required",
							"Date is required");
				else {
					calModDate.setTime(modDate);
					calModDate.set(Calendar.HOUR, 0);
					calModDate.set(Calendar.MINUTE, 0);
					calModDate.set(Calendar.SECOND, 0);
					calModDate.set(Calendar.MILLISECOND, 0);

					Calendar now = Calendar.getInstance();
					now.set(Calendar.HOUR, 0);
					now.set(Calendar.MINUTE, 0);
					now.set(Calendar.SECOND, 0);
					now.set(Calendar.MILLISECOND, 0);

					if (!(calModDate.get(Calendar.MONTH) == now.get(Calendar.MONTH)
							&& calModDate.get(Calendar.YEAR) == now.get(Calendar.YEAR) && calModDate
								.get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH))) {
						err = calModDate.before(now);
						if (err)
							error.rejectValue("favorites[" + favId + "].modDate", "", "Date must be after today.");
					}

					now.add(Calendar.YEAR, 1);
					err = calModDate.after(now);
					if (err)
						error.rejectValue("favorites[" + favId + "].modDate", "", "Date must be before a year.");
				}

				model.put("modPaymentErr", true);
				model.put("modPaymentErrId", favId);

			} else {
				calModDate.setTime(modDate);
			}

			if (!err) {
				// CIBP-2548 JOCAMPO
				if (payment.getPayee().getGlobalPayeeIdString() != null
						&& payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
					payment.getPayee().setTelenomina(true);
				} else {
					payment.getPayee().setTelenomina(false);
				}
				int instances = 1;

				if ("infinite".equalsIgnoreCase(modInstancesType)
						|| (inst.matches("\\d+") && Integer.parseInt(inst) > 750))
					instances = 999;
				else if (!GenericValidator.isBlankOrNull(inst) && inst.matches("\\d+"))
					instances = Integer.parseInt(inst);

				String taxId = customer.getEntitlementByType(EntitlementType.BANKING).getString("SSN");
				
				PaymentModify pmtMod = new PaymentModify();
				pmtMod.setNewFrequency(Frequency.valueOf(freq).getVaultCode());
				pmtMod.setNewAmount(payment.getAmount());
				pmtMod.setNewEffectiveDate(calModDate);
				
				ModifyPaymentRequest modifyPaymentRequest = new ModifyPaymentRequest(taxId,payment,pmtMod);
				modifyPaymentRequest.setCustomerService(customerService);
				modifyPaymentRequest.setInstances(instances);
				modifyPaymentRequest.setInitialAmount(payment.getAmount());
				modifyPaymentRequest.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
				modifyPaymentRequest.setUserSession(userSession);
				
				res = PaymentsServiceModule.getInstance().modifyPayment(modifyPaymentRequest);

			}
			List<TVPayment> listp = new LinkedList<TVPayment>();
			listp.add(paymentOriginal);// CIBP-1104
			listp.add(res.getPayment());
			if (res != null && res.getStatus() == ResultStatus.SUCCESS) {
				userSession.resetPayees();
				userSession.setLoadedDelinquencyPmtInfo(false);
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
					userSession.setStatusmessage("Payment was modified");
				else
					userSession.setStatusmessage("Pago ha sido modificado");
				// CIBP-1104 se activa envio alerta de modificacion de pago
				if (hasalert) {
					customerService.sendPaymentModifyConfirmation(customer, listp, null,
							userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
							userSession.getPreferredLanguage());
				}
			} else if (res != null) {
				String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request).toLowerCase());
				String str_error = "";
				if (er == null || "".equals(er)) {
					userSession.setErrorDetail(res.getErrorMessage());
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						str_error = "Unable to modify payment" + "<a href='javascript:Show()' > Learn More</a>";
						userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
					} else {
						str_error = "Error modificando el pago" + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
						userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
					}
				} else {
					userSession.setErrorDetail(er);
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						str_error = "Unable to modify payment" + "<a href='javascript:Show()' > Learn More</a>";
						userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());

					} else {
						userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
						str_error = "Error modificando el pago" + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
					}
				}
				userSession.setStatusmessage(str_error);

			} else {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Unable to modify payment");
				else
					userSession.setStatusmessage("Error modificando el pago");
			}

		} else if (action == PaymentSearchBean.MOD_AMT) {
			boolean hasalert = false;
			if (customer.hasEntitlement(EntitlementType.ALERTS)) {
				AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
				if (ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL)
						|| ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_SMS)
						|| ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH)) {
					hasalert = true;
				}
			}

			searchBean.setAction(1);
			PaymentResponse res = null;

			String modPayment = searchBean.getModPayment();
			TVPayment payment = null;
			ModPaymentBean modbean = null;
			for (Iterator<ModPaymentBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext();) {
				ModPaymentBean name = (ModPaymentBean) iterator.next();
				if (name != null)
					if (modPayment.equalsIgnoreCase(name.getModTransferId())) {
						payment = Utils.getPaymentById(modPayment, userSession.getPendingPayments());
						modbean = name;
						break;
					}
			}
			
			 // CIBP-1104 se utiliza para presentar en la alerta de correo el
			 // pago original previo a la modificacion
			TVPayment paymentOriginal = new TVPayment(new RecurringPayment());
			paymentOriginal.setEstimateDate(payment.getEstimateDate());
			paymentOriginal.setPayee(payment.getPayee());
			paymentOriginal.getTVPayment().setEffectiveDate(payment.getTVPayment().getEffectiveDate());
			paymentOriginal.getTVPayment().setAmount(payment.getAmount());
			paymentOriginal.setSource(payment.getSource());
			paymentOriginal.getTVPayment().setFrequency(payment.getFrequency());
			paymentOriginal.getTVPayment().setRemainingInstances(payment.getTVPayment().getRemainingInstances());
			paymentOriginal.getTVPayment().setReferenceNumber(payment.getTVPayment().getReferenceNumber());

			// IS A SINGLE PAYMENT
			BigDecimal amt = modbean.getModAmount();

			if (amt != null && amt.doubleValue() >= new Double(0.01d)) {
				payment.getTVPayment().setProcessingDate(payment.getTVPayment().getEffectiveDate());
				String freq = payment.getFrequency();
				freq = FrequencyUtils.getStringFrequency(freq);
				
				Calendar eff = Calendar.getInstance();
				eff.setTime(payment.getEffectiveDate());
				// CIBP-2548 JOCAMPO
				if (payment.getPayee().getGlobalPayeeIdString() != null
						&& payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
					payment.getPayee().setTelenomina(true);
				} else {
					payment.getPayee().setTelenomina(false);
				}
				
				String taxId = customer.getEntitlementByType(EntitlementType.BANKING).getString("SSN");
				
				PaymentModify pmtMod = new PaymentModify();
				pmtMod.setNewFrequency(Frequency.valueOf(freq).getVaultCode());
				pmtMod.setNewAmount(amt);
				pmtMod.setNewEffectiveDate(eff);
				
				ModifyPaymentRequest modifyPaymentRequest = new ModifyPaymentRequest(taxId,payment,pmtMod);
				modifyPaymentRequest.setInitialAmount(payment.getAmount());
				modifyPaymentRequest.setCustomerService(customerService);
				modifyPaymentRequest.setInstances((int) payment.getTVPayment().getRemainingInstances());
				modifyPaymentRequest.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
				modifyPaymentRequest.setUserSession(userSession);
				
				res = PaymentsServiceModule.getInstance().modifyPayment(modifyPaymentRequest);

			}
			List<TVPayment> listp = new LinkedList<TVPayment>();
			listp.add(paymentOriginal);// CIBP-1104
			listp.add(res.getPayment());
			if (res != null && res.getStatus() == ResultStatus.SUCCESS) {
				userSession.resetPayees();
				userSession.setLoadedDelinquencyPmtInfo(false);
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
					userSession.setStatusmessage("Payment was modified");
				else
					userSession.setStatusmessage("Pago ha sido modificado");
				
				//Si es un pago tipo ePayroll el Email es obligatorio y el SMS es opcional
				if(payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase("4401"))
				{
					customerService.sendePayrollModifyConfirmation(customer, listp, null,
					userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),	userSession.getPreferredLanguage());
					
				} else {
					// Si es tipo Payee los dos son opcionales
					if (hasalert) {
						customerService.sendPaymentModifyConfirmation(customer, listp, null,
						userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),	userSession.getPreferredLanguage());
					}
				}
				
			} else if (res != null) {

				String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request).toLowerCase());
				String str_error = "";
				if (er == null || "".equals(er)) {
					userSession.setErrorDetail(res.getErrorMessage());
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						str_error = "Unable to modify payment" + "<a href='javascript:Show()'> Learn More</a>";
						userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
					} else {
						str_error = "Error modificando el pago" + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
						userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
					}
				} else {
					userSession.setErrorDetail(er);
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						str_error = "Unable to modify payment" + "<a href='javascript:Show()'> Learn More</a>";
						userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());

					} else {
						userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
						str_error = "Error modificando el pago" + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
					}
				}
				userSession.setStatusmessage(str_error);

			} else {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Unable to modify payment");
				else
					userSession.setStatusmessage("Error modificando el pago");
			}
		} else if (action == PaymentSearchBean.MOD_AMT_DATE) {
			boolean hasalert = false;
			if (customer.hasEntitlement(EntitlementType.ALERTS)) {
				AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
				if (ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL)
						|| ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_SMS)
						|| ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH)) {
					hasalert = true;
				}
			}

			searchBean.setAction(1);
			// IS A SINGLE PAYMENT

			PaymentResponse res = null;
			String modPayment = searchBean.getModPayment();
			TVPayment payment = null;
			ModPaymentBean modbean = null;
			for (Iterator<ModPaymentBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext();) {
				ModPaymentBean name = (ModPaymentBean) iterator.next();
				if (name != null)
					if (modPayment.equalsIgnoreCase(name.getModTransferId())) {
						payment = Utils.getPaymentById(modPayment, userSession.getPendingPayments());
						modbean = name;
						break;
					}
			}

			TVPayment paymentOriginal = new TVPayment(new RecurringPayment());
			paymentOriginal.setEstimateDate(payment.getEstimateDate());
			paymentOriginal.setPayee(payment.getPayee());
			paymentOriginal.getTVPayment().setEffectiveDate(payment.getTVPayment().getEffectiveDate());
			paymentOriginal.getTVPayment().setAmount(payment.getAmount());
			paymentOriginal.setSource(payment.getSource());
			paymentOriginal.getTVPayment().setFrequency(payment.getFrequency());
			paymentOriginal.getTVPayment().setRemainingInstances(payment.getTVPayment().getRemainingInstances());
			paymentOriginal.getTVPayment().setReferenceNumber(payment.getTVPayment().getReferenceNumber());

			BigDecimal amt = modbean.getModAmount();
			if (amt != null && amt.doubleValue() >= new Double(0.01d)) {
				payment.getTVPayment().setProcessingDate(payment.getTVPayment().getEffectiveDate());
				String freq = payment.getFrequency();
				freq = FrequencyUtils.getStringFrequency(freq);
				Date modDate = modbean.getModDate();
				Calendar calModDate = Calendar.getInstance();
				boolean err = false;
				err = (modDate == null);
				if (!err) {
					calModDate.setTime(modDate);

					calModDate.set(Calendar.HOUR, 0);
					calModDate.set(Calendar.MINUTE, 0);
					calModDate.set(Calendar.SECOND, 0);
					calModDate.set(Calendar.MILLISECOND, 0);

					Calendar now = Calendar.getInstance();
					now.set(Calendar.HOUR, 0);
					now.set(Calendar.MINUTE, 0);
					now.set(Calendar.SECOND, 0);
					now.set(Calendar.MILLISECOND, 0);

					if (!(calModDate.get(Calendar.MONTH) == now.get(Calendar.MONTH)
							&& calModDate.get(Calendar.YEAR) == now.get(Calendar.YEAR) && calModDate
								.get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH)))
						err = calModDate.before(now);
					now.add(Calendar.YEAR, 1);
					err = calModDate.after(now);
				}
				// Se valida si no hay error en la fecha
				if (!err) {
					// CIBP-2548 JOCAMPO
					if (payment.getPayee().getGlobalPayeeIdString() != null
							&& payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
						payment.getPayee().setTelenomina(true);
					} else {
						payment.getPayee().setTelenomina(false);
					}
					
					String taxId = customer.getEntitlementByType(EntitlementType.BANKING).getString("SSN");
					
					PaymentModify pmtMod = new PaymentModify();
					pmtMod.setNewFrequency(Frequency.valueOf(freq).getVaultCode());
					pmtMod.setNewAmount(amt);
					pmtMod.setNewEffectiveDate(calModDate);
					
					ModifyPaymentRequest modifyPaymentRequest = new ModifyPaymentRequest(taxId,payment,pmtMod);
					modifyPaymentRequest.setInitialAmount(payment.getAmount());
					modifyPaymentRequest.setCustomerService(customerService);
					modifyPaymentRequest.setInstances(1);
					modifyPaymentRequest.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
					modifyPaymentRequest.setUserSession(userSession);
					
					res = PaymentsServiceModule.getInstance().modifyPayment(modifyPaymentRequest);
				
				}
			}
			List<TVPayment> listp = new LinkedList<TVPayment>();
			listp.add(paymentOriginal);// CIBP-1104
			listp.add(res.getPayment());
			if (res != null && res.getStatus() == ResultStatus.SUCCESS) {
				userSession.resetPayees();
				userSession.setLoadedDelinquencyPmtInfo(false);
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
					userSession.setStatusmessage("Payment was modified");
				else
					userSession.setStatusmessage("Pago ha sido modificado");
				
				//Si es un pago tipo ePayroll el Email es obligatorio y el SMS es opcional
				if(payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase("4401")) {
					customerService.sendePayrollModifyConfirmation(customer, listp, null,
					userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),	userSession.getPreferredLanguage());
					
				} else {
					// Si es tipo Payee los Email y los SMS son opcionales
					if (hasalert) {
						customerService.sendPaymentModifyConfirmation(customer, listp, null,
						userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),	userSession.getPreferredLanguage());
					}
				}
			} else if (res != null) {

				String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request).toLowerCase());
				String str_error = "";
				if (er == null || "".equals(er)) {
					userSession.setErrorDetail(res.getErrorMessage());
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						str_error = "Unable to modify payment" + "<a href='javascript:Show()' > Learn More</a>";
						userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
					} else {
						str_error = "Error modificando el pago" + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
						userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
					}
				} else {
					userSession.setErrorDetail(er);
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						str_error = "Unable to modify payment" + "<a href='javascript:Show()' > Learn More</a>";
						userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
					} else {
						userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
						str_error = "Error modificando el pago" + "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
					}
				}
				userSession.setStatusmessage(str_error);
			}

		} else if (action == PaymentSearchBean.CAN_PAYMENT) {
			boolean hasalert = false;
			if (customer.hasEntitlement(EntitlementType.ALERTS)) {
				AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
				if (ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL)
						|| ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_SMS)
						|| ent.getAlerts().contains(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH)) {
					hasalert = true;
				}
			}

			searchBean.setAction(1);
			String tr = searchBean.getModPayment();

			TVPayment payment = null;
			if (tr != null && !tr.equalsIgnoreCase("") && userSession.getPendingPayments() != null)
				payment = Utils.getPaymentById(tr, userSession.getPendingPayments());

			CancelPayeeResponse can;
			if (payment != null) {
				// CIBP-2548 JOCAMPO
				if (payment.getPayee().getGlobalPayeeIdString() != null
						&& payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
					payment.getPayee().setTelenomina(true);
				} else {
					payment.getPayee().setTelenomina(false);
				}
				can = customerService.cancelPaymentWithErrorDescription(customer, userSession.getBankingSession(),
						payment, Utils.generateCustomerInteractionEvent(request));
			

				List<TVPayment> listp = new LinkedList<TVPayment>();
				listp.add(payment);
				if (can.getResult().equals(CancelPayeeResponse.Result.SUCCESS)) {
					userSession.setLoadedDelinquencyPmtInfo(false);
					//MBIM-10: Update balance
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBIM10)) {
						TVFrontendAccount frontendAccountToUpdate = Utils
								.getAccountByAccountNumber(payment.getSourceAccountNumber(), userSession.getAccounts());
						if (frontendAccountToUpdate != null && frontendAccountToUpdate.getSubtype() != null
								&& frontendAccountToUpdate.getSubtype().equals("IDA")) {
							List<TVFrontendAccount> tvFrontendAccounts = new LinkedList<>();
							tvFrontendAccounts.add(frontendAccountToUpdate);
							userSession.updateAccountBalance(tvFrontendAccounts, balanceDelay);
						}
					}

					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
						userSession.setStatusmessage("Payment was deleted");
					else
						userSession.setStatusmessage("Pago ha sido borrado");

					// Si es un pago tipo ePayroll el Email es obligatorio y el
					// SMS es opcional
					if (payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase("4401")) {
						
						customerService.sendePayrollCancelConfirmation(customer, listp, null, userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)), 
								userSession.getPreferredLanguage(), hasalert);

					} else {
						// Si es tipo Payee los dos son opcionales
						if (hasalert) {
							customerService.sendPaymentCancelConfirmation(customer, listp, null,
									userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
									userSession.getPreferredLanguage());
						}

						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
							if (payment.getPayee().getGlobalPayeeIdString().equals("359")
									|| payment.getPayee().getGlobalPayeeIdString().equals("1983")
									|| payment.getPayee().getGlobalPayeeIdString().equals("223")
									|| payment.getPayee().getGlobalPayeeIdString().equals("518")
									|| payment.getPayee().getGlobalPayeeIdString().equals("217")) {
								CustomerPayeePreferences cPP = payment.getPayee().getCustomerPayeePreferences();
								cPP.setLastPaymentDate("");
								customerService.modifyPayeeFrequentPreference(userSession.getCustomerProfile(),
										userSession.getBankingSession(), payment.getPayee(),
										payment.getPayee().getFavorite());
							}
						}

					}
				} else {

					String er = getTelepagoErrorDescriptor(can.getErrorEBA(), Utils.getRSALanguage(request)
							.toLowerCase());
					String str_error = "";
					if (er == null || "".equals(er)) {
						userSession.setErrorDetail(can.getErrorString());
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							str_error = "Unable to delete payment" + "<a href='javascript:Show()' > Learn More</a>";
							userSession.setErrorDetailCod("Error Code: " + can.getErrorEBA());
						} else {
							str_error = "Error borrando  el pago"
									+ "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
							userSession.setErrorDetailCod("C&oacute;digo de error: " + can.getErrorEBA());
						}
					} else {
						userSession.setErrorDetail(er);
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							str_error = "Unable to delete payment" + "<a href='javascript:Show()' > Learn More</a>";
							userSession.setErrorDetailCod("Error Code: " + can.getErrorEBA());

						} else {
							userSession.setErrorDetailCod("C&oacute;digo de error: " + can.getErrorEBA());
							str_error = "Error borrando  el pago"
									+ "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
						}
					}
					userSession.setStatusmessage(str_error);
			
					
				}
			}
			userSession.resetPayees();
		} else if (action == PaymentSearchBean.REVERSE_PAYMENT) {
			searchBean.setAction(1);

			String tr = searchBean.getModPayment();
			String payeeId = searchBean.getPayeeId();
			String referenceNum = searchBean.getReferenceNum();

			TVPayment payment = null;

			if (tr != null && payeeId != null && !tr.equalsIgnoreCase("") && !payeeId.equalsIgnoreCase("")
					&& userSession.getCustomerPayments() != null)
				payment = Utils.getProcessedPaymentById(tr, payeeId, referenceNum, userSession.getCustomerPayments());
			PaymentResponse res;
			if (payment != null) {
				// CIBP-2548 JOCAMPO
				if (payment.getPayee().getGlobalPayeeIdString() != null
						&& payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
					payment.getPayee().setTelenomina(true);
				} else {
					payment.getPayee().setTelenomina(false);
				}
				res = PaymentsServiceModule.getInstance().reversePayment(customer, userSession.getBankingSession(), payment,
						Utils.generateCustomerInteractionEvent(request), customerService);

				if (res != null && res.getStatus() == ResultStatus.SUCCESS) {
					userSession.setLoadedDelinquencyPmtInfo(false);
					if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
						userSession.setStatusmessage("Payment was reversed");
					else
						userSession.setStatusmessage("Pago ha sido revertido");
				} else {

					String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request)
							.toLowerCase());
					String str_error = "";
					if (er == null || "".equals(er)) {
						userSession.setErrorDetail(res.getErrorString());
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							str_error = "Unable to reverse payment" + "<a href='javascript:Show()' > Learn More</a>";
							userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());
						} else {
							str_error = "Error revirtiendo  el pago"
									+ "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
							userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
						}
					} else {
						userSession.setErrorDetail(er);
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							str_error = "Unable to reverse payment" + "<a href='javascript:Show()' > Learn More</a>";
							userSession.setErrorDetailCod("Error Code: " + res.getErrorEBA());

						} else {
							userSession.setErrorDetailCod("C&oacute;digo de error: " + res.getErrorEBA());
							str_error = "Error revirtiendo  el pago"
									+ "<a href='javascript:Show()'> Conoce M&aacute;s</a>";
						}
					}
					userSession.setStatusmessage(str_error);
				}
			}
			userSession.resetPayees();
		}
		
		return showForm(request, response, error, model);
				
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		AccountMaskUtil mask = new AccountMaskUtil();
		model.put("telenominaID", telenominaID);
		final boolean realTimeNotificationsClaro = StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO);//rtflag
		model.put("realTimeNotificationsClaro",realTimeNotificationsClaro);
		boolean isVendorActivated = StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR);
		List<TVFrontEndPayee> payees = userSession.getPayees(true);
		Map<String, String> payeeMap = new LinkedHashMap<String, String>();
		if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
			payeeMap.put("LAST_15", "All payees");
		else
			payeeMap.put("LAST_15", "Todos los comercios");
		
		if (payees != null)
			if(isVendorActivated){
				for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
					TVFrontEndPayee pay = (TVFrontEndPayee) iterator.next();
					if(pay.getGlobalPayeeId() != Integer.valueOf(telenominaID) && pay.getGlobalPayeeId() != Integer.valueOf(vendorId))
						payeeMap.put(pay.getFrontEndId(), pay.getNickname() + " " + mask.mask(pay.getBillingAccount()));
				}
			}
			else{
				for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
					TVFrontEndPayee pay = (TVFrontEndPayee) iterator.next();
					if(pay.getGlobalPayeeId() != Integer.valueOf(telenominaID))
						payeeMap.put(pay.getFrontEndId(), pay.getNickname() + " " + mask.mask(pay.getBillingAccount()));
				}
			}

		model.put("payeeMap", payeeMap);
		model.put("favorites", userSession.getFavoritePayments());

		Map<String, Object> datemap = new LinkedHashMap<String, Object>();
		List<Date> allDates = new LinkedList<Date>();
		// 12 meses
		Calendar now = Calendar.getInstance();
		SimpleDateFormat simp = new SimpleDateFormat("yyyyMM");
		SimpleDateFormat month = new SimpleDateFormat("MMMMM");

		if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
			datemap.put("LAST_15", "Recent Payments");
		else
			datemap.put("LAST_15", "Pagos Recientes");

		for(int i = 1; i < 11; i++) {
			now.add(Calendar.MONTH, -1);
			datemap.put(simp.format(now.getTime()), month.format(now.getTime()) + " " + now.get(Calendar.YEAR));
			allDates.add(new Date(now.getTimeInMillis()));
		}
		
		model.put("dateMap", datemap);
		model.put("alldates", allDates);
		model.put("allpayees", payees);

		// Set up today's date
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Calendar n = Calendar.getInstance();
		if (Calendar.SATURDAY == n.get(Calendar.DAY_OF_WEEK))
			n.add(Calendar.DAY_OF_WEEK, 2);
		else if (Calendar.SUNDAY == n.get(Calendar.DAY_OF_WEEK))
			n.add(Calendar.DAY_OF_WEEK, 1);

		model.put("todayDate", df.format(n.getTime()));
		df = new SimpleDateFormat("MM");
		model.put("month", df.format(n.getTime()));
		df = new SimpleDateFormat("dd");
		model.put("day", df.format(n.getTime()));

		PaymentSearchBean searchBean = (PaymentSearchBean) command;
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		int action = searchBean.getAction() == 0 ? 1 : searchBean.getAction();
		int paymentSize = maxPayments;
		int month2 = 0;
		Calendar startDate = Calendar.getInstance();
		String payeeId = "";
		List<Integer> payeeIds = new LinkedList<Integer>();

		int year = Calendar.getInstance().get(Calendar.YEAR);

		if (action == PaymentSearchBean.SEARCH_BY_DATE && searchBean.getDate().equalsIgnoreCase("LAST_15"))
			action = PaymentSearchBean.SEARCH_LAST15;
		if (action == PaymentSearchBean.SEARCH_BY_PAYEE && searchBean.getPayeeId().equalsIgnoreCase("LAST_15"))
			action = PaymentSearchBean.SEARCH_LAST15;

		PaymentFilterType searchType = PaymentFilterType.HISTORY;
		if (action == PaymentSearchBean.SEARCH_LAST15) {
			searchType = PaymentFilterType.HISTORY;
			paymentSize = 15;
		} else if (action == PaymentSearchBean.SEARCH_BY_DATE) {
			searchType = PaymentFilterType.HISTORY;
			String date = searchBean.getDate();
			if (date != null && !"none".equals(date) && !date.equalsIgnoreCase("LAST_15")) {
				year = Integer.parseInt(date.substring(0, 4));
				month2 = Integer.parseInt(date.substring(4, date.length()));
				startDate = new GregorianCalendar(year, month2 - 1, 1);
			} else {
				searchType = PaymentFilterType.HISTORY;
				paymentSize = 15;
				date = null;
			}
		} else if (action == PaymentSearchBean.SEARCH_BY_PAYEE) {
			searchType = PaymentFilterType.HISTORY;
			payeeId = searchBean.getPayeeId();
			if (payeeId != null && !"none".equals(payeeId)) {
				TVFrontEndPayee p = Utils.getPayeebyId(payeeId, payees);
				if (p != null && p.getCustomerPayeeCodes() != null)
					payeeIds = p.getCustomerPayeeCodes();
				if (payeeIds == null) {
					searchType = PaymentFilterType.HISTORY;
					paymentSize = 15;
				}
			} else {
				searchType = PaymentFilterType.HISTORY;
				paymentSize = 15;
			}
		}

		PaymentHistoryResponse transferResponse = null;
		List<TVPayment> payments = new LinkedList<TVPayment>();
		List<TVPayment> pending = new LinkedList<TVPayment>();

		List<TVPayment> customerPayments = new LinkedList<TVPayment>();
		List<TVPayment> customerPendingPayments = new LinkedList<TVPayment>();

		if (action == PaymentSearchBean.SEARCH_BY_PAYEE) {
			String[] ids = new String[payeeIds.size()];
			for (int l = 0; l < payeeIds.size(); l++) {
				ids[l] = payeeIds.get(l).toString();
			}
			transferResponse = customerService.getPaymentHistory(customer, userSession.getBankingSession(), searchType,
					paymentSize, ids, null);
			if (transferResponse.getPayments() != null)
				payments.addAll(transferResponse.getPayments());
			if (transferResponse.getInprocessPayments() != null)
				pending.addAll(transferResponse.getInprocessPayments());
		} else if (action == PaymentSearchBean.SEARCH_BY_DATE) {
			transferResponse = customerService.getPaymentHistory(customer, userSession.getBankingSession(), searchType,
					paymentSize, null, startDate);
			if (transferResponse.getPayments() != null)
				payments.addAll(transferResponse.getPayments());
			if (transferResponse.getInprocessPayments() != null)
				pending.addAll(transferResponse.getInprocessPayments());
		} else {
			transferResponse = customerService.getPaymentHistory(customer, userSession.getBankingSession(), searchType,
					paymentSize, null, null);
			if (transferResponse.getPayments() != null)
				payments.addAll(transferResponse.getPayments());
			if (transferResponse.getInprocessPayments() != null)
				pending.addAll(transferResponse.getInprocessPayments());
		}
		
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)){
			if(userSession.getUserAgentType() == UserAgentType.WEB){
				if(pending != null && pending.size() > 0){
					Iterator<TVPayment> iteratorPending = pending.iterator();
					if(isVendorActivated){
						while(iteratorPending.hasNext()){
							RecurringPayment payAux = (RecurringPayment)iteratorPending.next().getBackendObject();
							if(payAux.getPayeeNickname().equals("E-PAYROLL") || payAux.getPayeeNickname().contains("VENDOR"))
								iteratorPending.remove();
						}
					}
					else{
						while(iteratorPending.hasNext()){
							RecurringPayment payAux = (RecurringPayment)iteratorPending.next().getBackendObject();
							if(payAux.getPayeeNickname().equals("E-PAYROLL"))
								iteratorPending.remove();
						}
					}
					
				}
			}
		}
		
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)){
			if(userSession.getUserAgentType() == UserAgentType.WEB){
				if(payments != null && payments.size() > 0){
					Iterator<TVPayment> iteratorPayments = payments.iterator();
					if(isVendorActivated){
						while(iteratorPayments.hasNext()){
							RecurringPayment payAux = (RecurringPayment)iteratorPayments.next().getBackendObject();
							if(payAux.getPayeeNickname().equals("E-PAYROLL") || payAux.getPayeeNickname().contains("VENDOR"))
								iteratorPayments.remove();
						}
					}
					else{
						while(iteratorPayments.hasNext()){
							RecurringPayment payAux = (RecurringPayment)iteratorPayments.next().getBackendObject();
							if(payAux.getPayeeNickname().equals("E-PAYROLL"))
								iteratorPayments.remove();
						}
					}
				}
			}
		}
		
		if (payments != null)
			for (Iterator<TVPayment> iterator = payments.iterator(); iterator.hasNext();) {
				TVPayment name = (TVPayment) iterator.next();

				TVFrontendAccount s = null;
				if (name.getSource() != null) {
					s = Utils.getAccountSourceByKey(name.getSource().getAccount().getAccountKey(),
							userSession.getAccountsPaymentFrom());
					if (s == null)
						s = Utils.getAccountSourceByNumber(name.getSource().getAccount().getAccountNumber(),
								userSession.getAccountsPaymentFrom());
				} else {
					s = Utils.getAccountByAccountNumber(name.getSourceAccountNumber(), userSession.getAccounts());
				}

				if (s != null)
					name.setSource(new TVAccountTransferSource(s));

				TVFrontEndPayee p = Utils.getPayeebyPayeeId(name.getPayeeId(), payees);
				if (p == null)
					p = Utils.getPayeebyAccountNumber(name.getPayeeBillingAccount(), payees);
				name.setPayee(p);
				// CIBP-2548 JOCAMPO
				if (p != null && p.getGlobalPayeeIdString() != null
						&& p.getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)
						&& name.getTVPayment().getStatusCode().equalsIgnoreCase("OK")) {
					Calendar effectiveDate = name.getTVPayment().getEffectiveDate();
					Calendar temp = Calendar.getInstance();
					temp.set(Calendar.HOUR, 0);
					temp.set(Calendar.MINUTE, 0);
					temp.set(Calendar.SECOND, 0);
					temp.set(Calendar.MILLISECOND, 0);
					temp.set(Calendar.YEAR, effectiveDate.get(Calendar.YEAR));
					temp.set(Calendar.MONTH, effectiveDate.get(Calendar.MONTH));
					temp.set(Calendar.DAY_OF_MONTH, effectiveDate.get(Calendar.DAY_OF_MONTH));

					Calendar today = Calendar.getInstance();
					today.set(Calendar.HOUR, 0);
					today.set(Calendar.MINUTE, 0);
					today.set(Calendar.SECOND, 0);
					today.set(Calendar.MILLISECOND, 0);

					temp.add(Calendar.DAY_OF_MONTH, Integer.parseInt(maxReverseableDays));
					if (temp.before(today))
						name.setReverseable(false);
					else
						name.setReverseable(true);
				}

				customerPayments.add(name);
			}

		BigDecimal totalInProcess = new BigDecimal(0.0d);
		if (pending != null) {
			List<PIFExternalAccount> pifExternalAccounts = null;
			for (Iterator<TVPayment> iterator = pending.iterator(); iterator.hasNext();) {
				TVPayment name = (TVPayment) iterator.next();

				TVFrontendAccount s = null;
								
				if (name.getSource() != null) {
					s = Utils.getAccountSourceByKey(name.getSource().getAccount().getAccountKey(),
							userSession.getAccountsPaymentFrom());
					if (s == null)
						s = Utils.getAccountSourceByNumber(name.getSource().getAccount().getAccountNumber(),
								userSession.getAccountsPaymentFrom());
				} else {
					s = Utils.getAccountByAccountNumber(name.getSourceAccountNumber(), userSession.getAccounts());
					if (s == null) { // It's a PIF Account
						if (pifExternalAccounts == null) {
							pifExternalAccounts = customerService.getConfirmedPIFExternalAccounts(userSession
									.getCustomerProfile().getTaxId());
						}
						if (pifExternalAccounts != null) {
							for (Iterator<PIFExternalAccount> pifIterator = pifExternalAccounts.iterator(); pifIterator
									.hasNext();) {
								PIFExternalAccount pifSource = (PIFExternalAccount) pifIterator.next();
								if (pifSource.getAccountNumber().equals(name.getSourceAccountNumber())) {
									BankInformation bankInformation = new BankInformation();
									bankInformation.setBankId(pifSource.getRoutingNumber());
									bankInformation.setBankType("ABA");
									bankInformation.setBankName(pifSource.getBankName());
									CustomerAccountPreferences pref = new CustomerAccountPreferences();
									TVAccountFactory accountFactory = new TVAccountFactory();
									Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
											pifSource.getAccountNumber());
									sourceAccount.setAccountID(pifSource.getAccountNumber());
									sourceAccount.setSubType("IDA");
									((DepositAccount) sourceAccount).setSectionIndicator(Character.toString(pifSource
											.getAccountType()));
									TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
									fromAccount.setCustomerAccountPreferences(pref);
									fromAccount.setNickname(pifSource.getNickname());
									fromAccount.setHide(pifSource.getShow() == '0');
									fromAccount.getTVAccount().setBankInformation(bankInformation);
									s = fromAccount;
									break;
								}
							}
						}
					}
				}

				if (s != null)
					name.setSource(new TVAccountTransferSource(s));

				TVFrontEndPayee p = Utils.getPayeebyPayeeId(name.getPayeeId(), payees);
				if (p == null)
					p = Utils.getPayeebyAccountNumber(name.getPayeeBillingAccount(), payees);
				name.setPayee(p);

				if (name.getTVPayment().getRemainingInstances() <= 0) {
					name.getTVPayment().setRemainingInstances(999);
				}
				Calendar cal = Calendar.getInstance();
				cal.setTime(name.getEffectiveDate());

				if (p != null && p.getGlobalPayee() != null && p.getGlobalPayee().getPaymentType() != null)
					if (p.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO
							|| p.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PUBLICO)
						name.setEstimateDate("CHECK");
					else
						name.setEstimateDate(Utils.getEstimateDate(customerService.getHolidays(), cal, p
								.getGlobalPayee().getPaymentType()));
				else
					name.setEstimateDate(Utils.getEstimateDate(customerService.getHolidays(), cal,
							GlobalPayeePaymentType.INTERNO));

				// CIBP-2128 BGARCIA se valida el nickname , si aplica no se
				// muestra el boton de eliminar

				if (p != null) {
					if (p.getTelepagoShortName().equals("DIRPM/BPPR") || p.getTelepagoShortName().equals("DIRPM/LEASE")
							|| p.getTelepagoShortName().equals("DIRPM/AUTO"))
						name.setIsdirpm(true);
					else
						name.setIsdirpm(false);
				}
				customerPendingPayments.add(name);

				// The object "name" has an attribute called frequency which
				// does not have the same identifier
				// for minimum balance and total balance it has a prefix of mb
				// and tb, that is why if you
				// try to compare directly with Frequency.TOTAL_BALANCE or
				// Frequency.MINIMUM_BALANCE it will
				// not match. CIBP-2531
				if (!name.getFrequency().equalsIgnoreCase("mb") && !name.getFrequency().equalsIgnoreCase("tb")) {
					totalInProcess = totalInProcess.add(name.getAmount());
				}
			}
		}

		model.put("totalInProcess", totalInProcess);
		Collections.sort(customerPayments, new PaymentSort());
		model.put("processedPayments", customerPayments);
		Collections.sort(customerPendingPayments, new PaymentSort());
		model.put("pendingPayments", customerPendingPayments);
		
		// RealTimePayments
		model.put("MBSFE138", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138).toString());
		// RealTimePayments
		
		userSession.setPendingPayments(customerPendingPayments);
		userSession.setCustomerPayments(customerPayments);

		return model;

	}

	public int getMaxPayments() {
		return maxPayments;
	}

	public void setMaxPayments(int maxPayments) {
		this.maxPayments = maxPayments;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		PaymentSearchBean searchBean = (PaymentSearchBean) command;
		int action = searchBean.getAction() == 0 ? 1 : searchBean.getAction();

		if (action == PaymentSearchBean.MOD_PAYMENT) {
			String tr = searchBean.getModPayment();
			TVPayment payment = null;
			ModPaymentBean modbean = null;
			for (Iterator<ModPaymentBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext();) {
				ModPaymentBean name = (ModPaymentBean) iterator.next();
				if (name != null)
					if (tr.equalsIgnoreCase(name.getModTransferId())) {
						payment = Utils.getPaymentById(tr, userSession.getPendingPayments());
						modbean = name;
						break;
					}
			}

			int favId = searchBean.getFavId();
			if (payment == null) {
				errors.rejectValue("favorites[" + favId + "].modFreq", "", "Frequency is required");
				return;
			}

			// IS A RECURRING PAYMENT
			String freq = modbean.getModFreq();
			String inst = modbean.getModInstances();
			String modInstancesType = modbean.getModInstancesType();
			Date modDate = modbean.getModDate();
			Calendar calModDate = Calendar.getInstance();

			boolean err = false;

			if (GenericValidator.isBlankOrNull(payment.getFrequency())
					|| GenericValidator.isBlankOrNull(freq)
					|| GenericValidator.isBlankOrNull(modInstancesType)
					|| (!"infinite".equalsIgnoreCase(modInstancesType) && (!GenericValidator.matchRegexp(inst,
							"^([1-9]|0[1-9]|[1-9][0-9]|[1-9][0-9]{1,2}|[0-9]{1,2}[0-9])$"))) || modDate == null) {

				err = true;

				if (GenericValidator.isBlankOrNull(payment.getFrequency()) || GenericValidator.isBlankOrNull(freq))
					errors.rejectValue("favorites[" + favId + "].modFreq", "", "Frequency is required");
				if (GenericValidator.isBlankOrNull(modInstancesType)
						|| (!"infinite".equalsIgnoreCase(modInstancesType) && (!GenericValidator.matchRegexp(inst,
								"^([1-9]|0[1-9]|[1-9][0-9]|[1-9][0-9]{1,2}|[0-9]{1,2}[0-9])$"))))
					errors.rejectValue("favorites[" + favId + "].modInstances", "paymenthistory.instances.required",
							"Instances must be filled");
				if (modDate == null) {
					modbean.setPendingDate(modDate);
					errors.rejectValue("favorites[" + favId + "].modDate", "paymenthistory.date.required",
							"Date is required");

				} else {
					calModDate.setTime(modDate);
					calModDate.set(Calendar.HOUR, 0);
					calModDate.set(Calendar.MINUTE, 0);
					calModDate.set(Calendar.SECOND, 0);
					calModDate.set(Calendar.MILLISECOND, 0);
					Calendar now = Calendar.getInstance();
					now.set(Calendar.HOUR, 0);
					now.set(Calendar.MINUTE, 0);
					now.set(Calendar.SECOND, 0);
					now.set(Calendar.MILLISECOND, 0);
					if (!(calModDate.get(Calendar.MONTH) == now.get(Calendar.MONTH)
							&& calModDate.get(Calendar.YEAR) == now.get(Calendar.YEAR) && calModDate
								.get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH))) {
						err = calModDate.before(now);
						errors.rejectValue("favorites[" + favId + "].modDate", "", "Date must be after today.");
					} else if (err) {
						modbean.setPendingDate(modDate);
					} else {
						now.add(Calendar.YEAR, 1);
						err = calModDate.after(now);
						if (err) {
							modbean.setPendingDate(modDate);
							errors.rejectValue("favorites[" + favId + "].modDate", "", "Date must be after today.");
						}
					}
				}
			}
		} else if (action == PaymentSearchBean.MOD_AMT) {
			String modPayment = searchBean.getModPayment();
			ModPaymentBean modbean = null;
			int favId = searchBean.getFavId();

			for (Iterator<ModPaymentBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext();) {
				ModPaymentBean name = (ModPaymentBean) iterator.next();
				if (name != null)
					if (modPayment.equalsIgnoreCase(name.getModTransferId())) {
						modbean = name;
						break;
					}
			}
			if (modbean != null) {
				// IS A SINGLE PAYMENT
				modbean.setPendingAmount(modbean.getModAmount());
				BigDecimal amt = modbean.getModAmount();
				if (amt != null && amt.doubleValue() >= new Double(0.01d)) {
					modbean.setPendingAmount(modbean.getModAmount());
				} else
					errors.rejectValue("favorites[" + favId + "].modAmount", "paymentshistory.amount.required",
							"Amount is required");
			} else {
				errors.rejectValue("favorites[" + favId + "].modAmount", "paymentshistory.amount.required",
						"Amount is required");
			}

		} else if (action == PaymentSearchBean.MOD_AMT_DATE) {
			// IS A SINGLE PAYMENT

			int favId = searchBean.getFavId();

			boolean err = false;
			String modPayment = searchBean.getModPayment();
			ModPaymentBean modbean = null;
			for (Iterator<ModPaymentBean> iterator = searchBean.getFavorites().iterator(); iterator.hasNext();) {
				ModPaymentBean name = (ModPaymentBean) iterator.next();
				if (name != null)
					if (modPayment.equalsIgnoreCase(name.getModTransferId())) {
						modbean = name;
						break;
					}
			}

			if (modbean != null) {
				modbean.setPendingAmount(modbean.getModAmount());
				Date modDate = modbean.getModDate();
				Calendar calModDate = Calendar.getInstance();

				BigDecimal amt = modbean.getModAmount();
				if (amt == null || amt.doubleValue() < new Double(0.01d)) {
					errors.rejectValue("favorites[" + favId + "].modAmount", "paymentshistory.amount.required",
							"Amount is required");
				}

				if (modDate == null)
					errors.rejectValue("favorites[" + favId + "].modDate", "paymenthistory.date.required",
							"Date is required");
				else {
					calModDate.setTime(modDate);
					calModDate.setTime(modDate);
					calModDate.set(Calendar.HOUR, 0);
					calModDate.set(Calendar.MINUTE, 0);
					calModDate.set(Calendar.SECOND, 0);
					calModDate.set(Calendar.MILLISECOND, 0);

					Calendar now = Calendar.getInstance();
					now.set(Calendar.HOUR, 0);
					now.set(Calendar.MINUTE, 0);
					now.set(Calendar.SECOND, 0);
					now.set(Calendar.MILLISECOND, 0);

					if (!(calModDate.get(Calendar.MONTH) == now.get(Calendar.MONTH)
							&& calModDate.get(Calendar.YEAR) == now.get(Calendar.YEAR) && calModDate
								.get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH)))
						err = calModDate.before(now);
					if (err) {
						modbean.setPendingDate(modDate);
						errors.rejectValue("favorites[" + favId + "].modDate", "", "Date must be after today.");
					} else {
						now.add(Calendar.YEAR, 1);
						err = calModDate.after(now);
						if (err) {
							modbean.setPendingDate(modDate);
							errors.rejectValue("favorites[" + favId + "].modDate", "", "Date must be after today.");
						}
					}
				}
			} else {
				errors.rejectValue("favorites[" + favId + "].modAmount", "paymentshistory.amount.required",
						"Amount is required");
			}
		}
	}


	private String getTelepagoErrorDescriptor(String codigo, String len) {
		String status = "";
		try {
			if (!"en".equals(len))
				len = "es";
			status = this.messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
		} catch (Exception e) {
			status = "";
		}

		return status;
	}


	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.BILLPAY;
	}

	public String getMaxReverseableDays() {
		return maxReverseableDays;
	}

	public void setMaxReverseableDays(String maxReverseableDays) {
		this.maxReverseableDays = maxReverseableDays;
	}
}
