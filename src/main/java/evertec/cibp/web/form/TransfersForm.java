package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.factory.TVAccountFactory;
import bppr.tv.beans.accounts.types.DepositAccount;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.holiday.Holiday;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.transfer.TVAccountFavoriteTransfer;
import evertec.cibp.core.models.transfer.TVAccountTransfer;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.models.transfer.TVAccountTransferTarget;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.TransferResponse;
import evertec.cibp.core.utils.BankingDaysUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.form.bean.TransferBean;
import evertec.cibp.web.form.bean.TransfersListBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.TransferSort;
import evertec.cibp.web.utils.Utils;

//CIBP-1680 se agrega  implements MessageSourceAware para acceso a los archivos
public class TransfersForm extends AbstractWizardFormController implements GlobalEntitlementAwareController,
		MessageSourceAware {

	private static final String PIF_PREFIX = "PIF";

	private String cancelView;
	protected final Log logger = LogFactory.getLog(getClass());
	private CustomerServices customerService;
	/*
	 * CIBP-1680 bgarcia Iten para acceder los archivos
	 */
	private MessageSource messageSource;

	public TransfersForm() {
		super();
		setCommandClass(TransfersListBean.class);
		setCommandName("transferlist");
		setPages(new String[] { "makeTransfer", "makeTransfer", "makeTransfer" });
	}

	protected int getTargetPage(HttpServletRequest request, int currentPage) {
		return super.getTargetPage(request, currentPage);
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object transferlist,
			BindException error) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		boolean reloadTransfer = false;
		List<TVAccountTransfer> confTransfers = new LinkedList<TVAccountTransfer>();
		List<TVAccountTransfer> failTransfers = new LinkedList<TVAccountTransfer>();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		BigDecimal totalConfirm = new BigDecimal(0.0d);
		BigDecimal totalFail = new BigDecimal(0.0d);

		TransfersListBean transfersList = (TransfersListBean) transferlist;
		for (Iterator<Object> iterator = transfersList.getTransfers().iterator(); iterator.hasNext();) {
			TransferBean name = (TransferBean) iterator.next();
			if (name.getAmount() == null) {
				iterator.remove();
				continue;
			}

			TVAccountTransferSource sou = null;
			TVAccountTransferTarget tar = null;

			// If it is a PIF account, create it
			if (name.getAccountFrom().startsWith(PIF_PREFIX)) {
				String externalAccountId = name.getAccountFrom().substring(3);
				PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
				if (externalAccount != null) {
					BankInformation bankInformation = new BankInformation();
					bankInformation.setBankId(externalAccount.getRoutingNumber());
					bankInformation.setBankName(externalAccount.getBankName());
					CustomerAccountPreferences pref = new CustomerAccountPreferences();
					TVAccountFactory accountFactory = new TVAccountFactory();
					Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
							externalAccount.getAccountNumber());
					sourceAccount.setAccountID(externalAccount.getAccountNumber());
					sourceAccount.setSubType("IDA");
					((DepositAccount) sourceAccount).setSectionIndicator(Character.toString(externalAccount
							.getAccountType()));
					TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
					fromAccount.setCustomerAccountPreferences(pref);
					fromAccount.setNickname(externalAccount.getNickname());
					fromAccount.setHide(externalAccount.getShow() == '0');
					fromAccount.getTVAccount().setBankInformation(bankInformation);
					sou = new TVAccountTransferSource(fromAccount);
					tar = Utils.getAccountTarget(name.getAccountToPifFilter(), userSession.getAccountsTransferTo());

					// this is for the native apps
					// DO NOT DELETE
					if (tar == null && name.getAccountTo() != null && !name.getAccountTo().equals("none")) {
						// came from the native app
						tar = Utils.getAccountTarget(name.getAccountTo(), userSession.getAccountsTransferTo());
					}
				}
			} else {
				sou = Utils.getAccountSource(name.getAccountFrom(), userSession.getAccountsTransferFrom());
				tar = Utils.getAccountTarget(name.getAccountTo(), userSession.getAccountsTransferTo());
			}

			if (sou == null || tar == null)
				continue;

			BigDecimal amount = name.getAmount();
			Calendar cal = Calendar.getInstance();
			cal.setTime(name.getEffectiveDate());

			TVAccountTransfer transfer = null;
			Integer inst = name.getInstances();
			inst = inst == null ? 0 : inst;
			String recurrent = name.getRecurrent();
			String infinite = name.getInfinite();

			if (recurrent != null && recurrent.equalsIgnoreCase("recurrent")) {
				if (infinite != null && infinite.equalsIgnoreCase("infinite")) {
					inst = 999;
				}
				transfer = new TVAccountTransfer(amount, name.getFrequency(), inst, cal, sou, tar);
			} else
				transfer = new TVAccountTransfer(amount, Frequency.MANUALLY, inst, cal, sou, tar);

			boolean sendTransfer = true;
			boolean pifError = false;
			if (name.getAccountFrom().startsWith(PIF_PREFIX)) {
				if (transfer != null && transfer.getTarget() != null && transfer.getTarget().getAccount() != null) {
					if (!customerService.accountValidForPIFTransfer(transfer.getTarget().getAccount())) {
						sendTransfer = false;
						pifError = true;
					}
				} else {
					sendTransfer = false;
					pifError = true;
				}
			}

			String sourceNickname = sou.getAccount().getNickname();
			String targetNickname = tar.getAccount().getNickname();
			String sourceSuffix = sou.getAccount().getAccountNumberSuffix();
			String targetSuffix = tar.getAccount().getAccountNumberSuffix();
			boolean hide;
			boolean hide2;

			if (sou.getAccount().getCustomerAccountPreferences() != null)
				hide = sou.getAccount().getCustomerAccountPreferences().getHide();
			else
				hide = false;
			if (tar.getAccount().getCustomerAccountPreferences() != null)
				hide2 = tar.getAccount().getCustomerAccountPreferences().getHide();
			else
				hide2 = false;

			TransferResponse res = null;

			if (sendTransfer) {
				res = customerService.sendTransfer(customer, userSession.getBankingSession(), transfer,
						Utils.generateCustomerInteractionEvent(request));
				transfer = res.getTransfer();
			}

			if (!GenericValidator.isBlankOrNull(sourceSuffix)) {
				TVAccountTransferSource source2 = transfer.getSource();
				source2.setAccountNumberSuffix(sourceSuffix);
			}
			if (!GenericValidator.isBlankOrNull(targetSuffix)) {
				TVAccountTransferTarget target2 = transfer.getTarget();
				target2.setAccountNumberSuffix(targetSuffix);
			}
			if (!GenericValidator.isBlankOrNull(sourceNickname)) {
				TVAccountTransferSource source2 = transfer.getSource();
				CustomerAccountPreferences prefSource = new CustomerAccountPreferences();
				prefSource.setNickname(sourceNickname);
				prefSource.setHide(hide);
				source2.setAccountNickname(prefSource);
				transfer.setSource(source2);
			}
			if (!GenericValidator.isBlankOrNull(targetNickname)) {
				TVAccountTransferTarget target2 = transfer.getTarget();
				CustomerAccountPreferences prefTarget = new CustomerAccountPreferences();
				prefTarget.setNickname(targetNickname);
				prefTarget.setHide(hide2);
				target2.setAccountNickname(prefTarget);
				transfer.setTarget(target2);
			}

			if (res != null && res.getStatus() == ResultStatus.SUCCESS) {
				reloadTransfer = true;
				totalConfirm = totalConfirm.add(transfer.getAmount());
				confTransfers.add(transfer);
				model.put("delay", true);
				userSession.setDelay(true);
				userSession.resetAccounts();
				userSession.setMobileCashAccounts(null);
			} else {
				totalFail = totalFail.add(transfer.getAmount());
				/*
				 * CIBP-1680 bgarcia Se agrega la logica para poner el detalle
				 * del mensaje
				 */
				if (res != null) {
					String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request)
							.toLowerCase());
					if (er == null || er.equals("")) {
						transfer.setTransferMessage(res.getErrorMessage());
					} else {
						transfer.setTransferMessage(er + " - " + res.getErrorEBA());
					}
				} else {
					if (pifError) {
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE182)){
							transfer.setTransferMessage(this.messageSource.getMessage("mobile.pif.transfer.error", null,
								new Locale(!"en".equals(Utils.getRSALanguage(request).toLowerCase()) ? "es" : "en")));
						}
						else{
							transfer.setTransferMessage(this.messageSource.getMessage("mobile.pif.transfer.error", null,
									new Locale(!"en".equals(Utils.getRSALanguage(request).toLowerCase()) ? "es" : "en"))
									.toUpperCase());
						}
					}
				}
				failTransfers.add(transfer);
			}

		}

		for (Iterator<Object> iterator = transfersList.getFavorites().iterator(); iterator.hasNext();) {
			TransferBean name = (TransferBean) iterator.next();
			if (name.getAmount() == null) {
				iterator.remove();
				continue;
			}
			TVAccountTransferSource sou = Utils.getAccountSource(name.getAccountFrom(),
					userSession.getAccountsTransferFrom());
			TVAccountTransferTarget tar = Utils.getAccountTarget(name.getAccountTo(),
					userSession.getAccountsTransferTo());
			if (sou == null || tar == null)
				continue;

			BigDecimal amount = name.getAmount();
			Calendar cal = Calendar.getInstance();
			cal.setTime(name.getEffectiveDate());

			TVAccountTransfer transfer = null;
			Integer inst = name.getInstances();
			inst = inst == null ? 0 : inst;
			String recurrent = name.getRecurrent();
			String infinite = name.getInfinite();

			if (recurrent != null && recurrent.equalsIgnoreCase("recurrent")) {
				if (infinite != null && infinite.equalsIgnoreCase("infinite")) {
					inst = 999;
				}
				transfer = new TVAccountTransfer(amount, name.getFrequency(), inst, cal, sou, tar);
			} else
				transfer = new TVAccountTransfer(amount, Frequency.MANUALLY, inst, cal, sou, tar);

			String sourceNickname = sou.getAccount().getNickname();
			String targetNickname = tar.getAccount().getNickname();
			String sourceSuffix = sou.getAccount().getAccountNumberSuffix();
			String targetSuffix = tar.getAccount().getAccountNumberSuffix();
			boolean hide;
			boolean hide2;

			if (sou.getAccount().getCustomerAccountPreferences() != null)
				hide = sou.getAccount().getCustomerAccountPreferences().getHide();
			else
				hide = false;
			if (tar.getAccount().getCustomerAccountPreferences() != null)
				hide2 = tar.getAccount().getCustomerAccountPreferences().getHide();
			else
				hide2 = false;

			TransferResponse res = customerService.sendTransfer(customer, userSession.getBankingSession(), transfer,
					Utils.generateCustomerInteractionEvent(request));
			transfer = res.getTransfer();

			if (!GenericValidator.isBlankOrNull(sourceSuffix)) {
				TVAccountTransferSource source2 = transfer.getSource();
				source2.setAccountNumberSuffix(sourceSuffix);
			}
			if (!GenericValidator.isBlankOrNull(targetSuffix)) {
				TVAccountTransferTarget target2 = transfer.getTarget();
				target2.setAccountNumberSuffix(targetSuffix);
			}
			if (!GenericValidator.isBlankOrNull(sourceNickname)) {
				TVAccountTransferSource source2 = transfer.getSource();
				CustomerAccountPreferences prefSource = new CustomerAccountPreferences();
				prefSource.setNickname(sourceNickname);
				prefSource.setHide(hide);
				source2.setAccountNickname(prefSource);
				transfer.setSource(source2);
			}
			if (!GenericValidator.isBlankOrNull(targetNickname)) {
				TVAccountTransferTarget target2 = transfer.getTarget();
				CustomerAccountPreferences prefTarget = new CustomerAccountPreferences();
				prefTarget.setNickname(targetNickname);
				prefTarget.setHide(hide2);
				target2.setAccountNickname(prefTarget);
				transfer.setTarget(target2);
			}

			if (res.getStatus() == ResultStatus.SUCCESS) {
				reloadTransfer = true;
				totalConfirm = totalConfirm.add(transfer.getAmount());
				confTransfers.add(transfer);
				model.put("delay", true);
				userSession.setDelay(true);
				userSession.resetAccounts();
				userSession.setMobileCashAccounts(null);
			} else {
				totalFail = totalFail.add(transfer.getAmount());
				// transfer.setTransferMessage(res.getErrorMessage());
				/*
				 * CIBP-1680 bgarcia Se agrega la logica para poner el detalle
				 * del mensaje
				 */
				String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request).toLowerCase());
				if (er == null || er.equals("")) {
					transfer.setTransferMessage(res.getErrorMessage());
				} else {
					transfer.setTransferMessage(er + " - " + res.getErrorEBA());
				}
				failTransfers.add(transfer);
			}

		}

		boolean errortrans = false;
		model.put("page", "2");
		if (confTransfers != null && confTransfers.size() > 0) {
			model.put("confTransfers", confTransfers);
			model.put("totalConfirm", totalConfirm);
		}
		if (failTransfers != null && failTransfers.size() > 0) {
			model.put("failTransfers", failTransfers);
			model.put("totalFail", totalFail);
			errortrans = true;
		}
		/**
		 * bgarcia CIBP-1798
		 * 
		 * Se cambia la forma de validar la presencia de Alerts
		 * **/
		boolean hasalert = false;
		CustomerEntitlement e = customer.getEntitlementByType(EntitlementType.ALERTS);
		if (e != null) {
			AlertsEntitlement ent = new AlertsEntitlement(e);
			if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
				Set<AlertType> alerts = ent.getAlerts();
				if (alerts.contains(AlertType.TRANSFERS_EMAIL) || alerts.contains(AlertType.TRANSFERS_SMS)
						|| alerts.contains(AlertType.TRANSFERS_PUSH)) {
					hasalert = true;
				}
			}
		}
		if (!errortrans) {

			if (hasalert) {
				if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
					userSession
							.setStatusmessage("Thanks! You will receive a confirmation of these transactions via email, text message and/or push notification.");
				else
					userSession
							.setStatusmessage("&iexcl;Gracias! Recibir&aacute;s evidencia de estas transacciones por email, mensaje de texto y/o notificaci&oacuten.");

			}
			if (!hasalert) {
				if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
					userSession
							.setStatusmessage("Thanks! - To receive evidence of transfers by email, <a href=\"alerttypes\">click here</a>.");
				else
					userSession
							.setStatusmessage("&iexcl;Gracias! - Para recibir evidencia de transferencias por e-mail, <a href=\"alerttypes\">oprime aqu&iacute;</a>.");
			}

		} else {
			if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
				userSession.setStatusmessage("Please verify errors in transfers");
			else
				userSession.setStatusmessage("Tienes errores en transferencias");
		}

		if (hasalert) {
			customerService.sendTransferConfirmation(customer, confTransfers, failTransfers,
					userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
					userSession.getPreferredLanguage());
		}

		if (reloadTransfer) {
			userSession.resetTransfers();
		}
		return new ModelAndView("makeTransfer", model);
	}

	/**
	 * Separate fields into a map for use by various client views.
	 * 
	 * @param account
	 * @return
	 */
	private Map<String, String> accountDetailForAccount(TVFrontendAccount account) {
		Map<String, String> accountDetail = new LinkedHashMap<String, String>();
		if (account == null) {
			accountDetail.put("accountName", "");
			accountDetail.put("acctLast4Num", "");
			accountDetail.put("accountBalance", "");
			accountDetail.put("accountNumber", "");
			accountDetail.put("accountSubtype", "");
			accountDetail.put("accountSection", "");

			accountDetail.put("text", "");

		} else {
			AccountMaskUtil mask = new AccountMaskUtil();
			NumberTool numberTool = new NumberTool();

			StringBuilder sb = new StringBuilder();

			accountDetail.put("accountName", account.getNickname());
			accountDetail.put("accountLast4Num", mask.mask(account.getAccountNumber()));
			accountDetail.put("accountNumber", account.getAccountNumber());
			accountDetail.put("accountSection", account.getAccountSection());
			accountDetail.put("accountSubtype", account.getProductDefinition().getProductType().trim());
			accountDetail.put("accountNumberSuffix", account.getAccountNumberSuffix());

			/**
			 * lguzman, CIBP-1829 se incluye restriccion para tarjetas de
			 * credito, tipo CCA
			 */
			if (account != null && !account.getSubtype().equals("CCA") && account.getPortalBalance() != null) {
				accountDetail.put("accountBalance", numberTool.format("currency", account.getPortalBalance()));
				accountDetail.put("accountNumberSuffix", ((account.getAccountNumberSuffix() != null && !account
						.getAccountNumberSuffix().equals("")) ? " " + account.getAccountNumberSuffix() : ""));
			} else if (account != null && account.getSubtype().equals("CCA")) {
				if (account.getAvailableBalance() != null) {
					accountDetail.put("accountBalance", numberTool.format("currency", account.getAvailableBalance()));
				} else {
					accountDetail.put("accountBalance", "");
				}
				accountDetail.put("accountNumberSuffix", ((account.getAccountNumberSuffix() != null && !account
						.getAccountNumberSuffix().equals("")) ? " " + account.getAccountNumberSuffix() : ""));
			} else {
				accountDetail.put("accountBalance", "");
				accountDetail.put("accountNumberSuffix", ((account.getAccountNumberSuffix() != null && !account
						.getAccountNumberSuffix().equals("")) ? " " + account.getAccountNumberSuffix() : ""));
			}

			sb.append(StringUtils.abbreviate(accountDetail.get("accountName"), 15));
			sb.append(" ");
			sb.append(accountDetail.get("accountLast4Num"));
			if (!accountDetail.get("accountBalance").equals("")) {
				sb.append(" ");
				sb.append(accountDetail.get("accountBalance"));
			}
			if (!accountDetail.get("accountNumberSuffix").equals("")) {
				sb.append(" ");
				sb.append(accountDetail.get("accountNumberSuffix"));
			}

			accountDetail.put("text", sb.toString());
		}
		return accountDetail;
	}
	
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		model.put("page", page);

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		if (page == 0 || page == 1) {
			Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsTransferFrom();
			Set<TVAccountTransferTarget> accountsTo = userSession.getAccountsTransferTo();
			List<TVAccountFavoriteTransfer> favorites = userSession.getFavoritesTransfers();
			List<TVFrontendAccount> trfAccounts = userSession.getTrfAccounts();

			List<String> noAvailEccounts = userSession.getCustomerService().getDCINotAvailableEaccounts(
					userSession.getCustomerProfile());

			AccountMaskUtil mask = new AccountMaskUtil();
			NumberTool numberTool = new NumberTool();

			Map<String, String> accountsFromMap = new LinkedHashMap<String, String>();
			Map<String, Map<String, String>> detailedAccountsFromMap = new LinkedHashMap<String, Map<String, String>>();
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				accountsFromMap.put("none", "Select an account");
				detailedAccountsFromMap.put("none", null);
			} else {
				accountsFromMap.put("none", "Selecciona una cuenta");
				detailedAccountsFromMap.put("none", null);
			}

			if (accountsFrom != null) {
				boolean guardianAccFound = false; // CIBP-2542 (var to make sure
													// isGuardian is only set
													// once)
				for (Iterator<TVAccountTransferSource> iterator = accountsFrom.iterator(); iterator.hasNext();) {
					TVAccountTransferSource from = (TVAccountTransferSource) iterator.next();
					if (!noAvailEccounts.contains(from.getAccount().getAccountNumber())) {
						TVFrontendAccount frontFrom = Utils.getAccountByKey(from.getAccount().getAccountKey(),
								userSession.getAccounts());
						if (frontFrom != null)
							if (!frontFrom.getHide()) {
								Map<String, String> accountDetail = accountDetailForAccount(frontFrom);
								accountsFromMap.put(from.getFrontEndId(), accountDetail.get("text"));
								detailedAccountsFromMap.put(from.getFrontEndId(), accountDetail);
								if (!guardianAccFound && frontFrom.getIsGuardianOrRepPayeeAcc()) { // CIBP-2542
									model.put("isGuardian", true);
									guardianAccFound = true;
								}
							}
					}

				}
			}

			Map<String, String> accountsToMap = new LinkedHashMap<String, String>();
			Map<String, String> accountsToMapPIF = new LinkedHashMap<String, String>();
			Map<String, Map<String, String>> detailedAccountsToMap = new LinkedHashMap<String, Map<String, String>>();
			Map<String, Map<String, String>> detailedAccountsToMapPIF = new LinkedHashMap<String, Map<String, String>>();
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				accountsToMap.put("none", "Select an account");
				accountsToMapPIF.put("none", "Select an account");
				detailedAccountsToMap.put("none", accountDetailForAccount(null));
				detailedAccountsToMapPIF.put("none", accountDetailForAccount(null));
			} else {
				accountsToMap.put("none", "Selecciona una cuenta");
				accountsToMapPIF.put("none", "Selecciona una cuenta");
				detailedAccountsToMap.put("none", accountDetailForAccount(null));
				detailedAccountsToMapPIF.put("none", accountDetailForAccount(null));
			}
			if (accountsTo != null)
				for (Iterator<TVAccountTransferTarget> iterator = accountsTo.iterator(); iterator.hasNext();) {
					TVAccountTransferTarget to = (TVAccountTransferTarget) iterator.next();
					if (!noAvailEccounts.contains(to.getAccount().getAccountNumber())) {
						TVFrontendAccount frontTo = Utils.getAccountByKey(to.getAccount().getAccountKey(),
								userSession.getAccounts());
						if (frontTo == null)
							frontTo = Utils.getAccountByKey(to.getAccount().getAccountKey(),
									userSession.getTrfAccounts());

						if (frontTo != null)
							if (!frontTo.getHide()) {
								Map<String, String> accountDetail = accountDetailForAccount(frontTo);

								accountsToMap.put(to.getFrontEndId(), accountDetail.get("text"));
								detailedAccountsToMap.put(to.getFrontEndId(), accountDetail);

								/**
								 * lguzman, CIBP-1829 se incluye restriccion
								 * para tarjetas de credito, tipo CCA
								 */
								if (frontTo.getPortalBalance() != null) { // &&
																			// !"CCA".equals(frontTo.getProductDefinition().getProductType().trim()))
									if (customerService.accountValidForPIFTransfer(frontTo)) {
										if (accountNotTRF(to, trfAccounts)) {
											accountsToMapPIF
													.put(to.getFrontEndId(),
															StringUtils.abbreviate(frontTo.getNickname(), 15)
																	+ " "
																	+ mask.mask(frontTo.getAccountNumber())
																	+ ((frontTo.getAccountNumberSuffix() != null && !frontTo
																			.getAccountNumberSuffix().equals("")) ? " "
																			+ frontTo.getAccountNumberSuffix() : "")
																	+ " "
																	+ numberTool.format("currency",
																			frontTo.getPortalBalance()));
											detailedAccountsToMapPIF.put(to.getFrontEndId(), accountDetail);
										}
									}
								} else if ("CCA".equals(frontTo.getProductDefinition().getProductType().trim())) {
									if (frontTo.getAvailableBalance() != null) {
										if (customerService.accountValidForPIFTransfer(frontTo)) {
											if (accountNotTRF(to, trfAccounts)) {
												accountsToMapPIF
														.put(to.getFrontEndId(),
																StringUtils.abbreviate(frontTo.getNickname(), 15)
																		+ " "
																		+ mask.mask(frontTo.getAccountNumber())
																		+ ((frontTo.getAccountNumberSuffix() != null && !frontTo
																				.getAccountNumberSuffix().equals("")) ? " "
																				+ frontTo.getAccountNumberSuffix()
																				: ""));
												detailedAccountsToMapPIF.put(to.getFrontEndId(), accountDetail);
											}
										}
									} else {
										if (customerService.accountValidForPIFTransfer(frontTo)) {
											if (accountNotTRF(to, trfAccounts)) {
												accountsToMapPIF
														.put(to.getFrontEndId(),
																StringUtils.abbreviate(frontTo.getNickname(), 15)
																		+ " "
																		+ mask.mask(frontTo.getAccountNumber())
																		+ ((frontTo.getAccountNumberSuffix() != null && !frontTo
																				.getAccountNumberSuffix().equals("")) ? " "
																				+ frontTo.getAccountNumberSuffix()
																				: ""));
												detailedAccountsToMapPIF.put(to.getFrontEndId(), accountDetail);
											}
										}
									}
								} else {
									if (customerService.accountValidForPIFTransfer(frontTo)) {
										if (accountNotTRF(to, trfAccounts)) {
											accountsToMapPIF.put(
													to.getFrontEndId(),
													StringUtils.abbreviate(frontTo.getNickname(), 15)
															+ " "
															+ mask.mask(frontTo.getAccountNumber())
															+ ((frontTo.getAccountNumberSuffix() != null && !frontTo
																	.getAccountNumberSuffix().equals("")) ? " "
																	+ frontTo.getAccountNumberSuffix() : ""));
											detailedAccountsToMapPIF.put(to.getFrontEndId(), accountDetail);
										}
									}
								}
							}
					}
				}

			// CLIENT HAS DEPOSIT ACCOUNTS
			if (accountsToMap.size() > 1) {
				// Post-Initial Funding accounts
				List<PIFExternalAccount> pifExternalAccounts = customerService
						.getConfirmedPIFExternalAccounts(userSession.getCustomerProfile().getTaxId());
				if (pifExternalAccounts != null)
					for (Iterator<PIFExternalAccount> iterator = pifExternalAccounts.iterator(); iterator.hasNext();) {
						PIFExternalAccount from = (PIFExternalAccount) iterator.next();
						accountsFromMap.put(
								PIF_PREFIX + from.getAccountId(),
								StringUtils.abbreviate(from.getNickname(), 15) + " "
										+ mask.mask(from.getAccountNumber()));
						detailedAccountsFromMap.put(PIF_PREFIX + from.getAccountId(), Utils.detailedAccountFromPIF(from));
					}
				
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)){
					if(userSession.getInterbankFundingAllowed()){
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							accountsFromMap.put("add", "Add other bank account");
						else
							accountsFromMap.put("add", "A&ntilde;ade una cuenta de otro Banco");
					}
				}
				else{
					
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						accountsFromMap.put("add", "Add other bank account");
					else
						accountsFromMap.put("add", "A&ntilde;ade una cuenta de otro Banco");
				}
			}

			List<TVAccountFavoriteTransfer> favs = new LinkedList<TVAccountFavoriteTransfer>();
			if (favorites != null)
				for (Iterator<TVAccountFavoriteTransfer> iterator = favorites.iterator(); iterator.hasNext();) {
					TVAccountFavoriteTransfer fav = (TVAccountFavoriteTransfer) iterator.next();

					TVAccountTransferTarget tar = null;
					TVAccountTransferSource sou = null;
					if (fav != null && fav.getSource() != null && fav.getSource().getAccount() != null
							&& fav.getSource().getAccount().getAccountKey() != null && accountsFrom != null) {
						sou = Utils.getSourceByAccountKey(fav.getSource().getAccount().getAccountKey(), accountsFrom);
					}
					if (fav != null && fav.getTarget() != null && fav.getTarget().getAccount() != null
							&& fav.getTarget().getAccount().getAccountKey() != null && accountsTo != null)
						tar = Utils.getTargetByAccountKey(fav.getTarget().getAccount().getAccountKey(), accountsTo);
					if (sou != null && tar != null) {
						fav.setSource(sou);
						fav.setTarget(tar);
						favs.add(fav);
					}
				}

			if (favs != null)
				for (Iterator<TVAccountFavoriteTransfer> iterator = favs.iterator(); iterator.hasNext();) {
					TVAccountFavoriteTransfer name = (TVAccountFavoriteTransfer) iterator.next();
					if (name.getPendingTransfers() != null) {
						Collections.sort(name.getPendingTransfers(), new TransferSort());
						for (Iterator<TVAccountTransfer> iterator2 = name.getPendingTransfers().iterator(); iterator2
								.hasNext();) {
							TVAccountTransfer pmt = (TVAccountTransfer) iterator2.next();
							pmt.setSource(Utils.getSourceByAccountKey(pmt.getSource().getAccount().getAccountKey(),
									accountsFrom));
							pmt.setTarget(Utils.getTargetByAccountKey(pmt.getTarget().getAccount().getAccountKey(),
									accountsTo));
						}
					}
				}

			userSession.setAccountsTransferFrom(accountsFrom);
			userSession.setAccountsTransferTo(accountsTo);
			userSession.setFavoritesTransfers(favs);

			Map<String, String> frequencies = new LinkedHashMap<String, String>();
			if (Utils.getRSALanguage(request).equalsIgnoreCase("EN")) {
				frequencies.put("variable", "One time");
				frequencies.put("recurrent", "Recurring");
			} else {
				frequencies.put("variable", "Una vez");
				frequencies.put("recurrent", "Recurrente");
			}

			if (page == 0) {
				// Set up today's date
				SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				Calendar n = Calendar.getInstance();
				if (Calendar.SATURDAY == n.get(Calendar.DAY_OF_WEEK))
					n.add(Calendar.DAY_OF_WEEK, 2);
				else if (Calendar.SUNDAY == n.get(Calendar.DAY_OF_WEEK))
					n.add(Calendar.DAY_OF_WEEK, 1);

				model.put("todayDate", df.format(n.getTime()));
				df = new SimpleDateFormat("MM");
				model.put("month", df.format(n.getTime()));
				df = new SimpleDateFormat("dd");
				model.put("day", df.format(n.getTime()));
			}

			if (page == 0 || page == 1) {
				model.put("fromaccounts", accountsFromMap);
				model.put("detailedfromaccounts", detailedAccountsFromMap);
				model.put("toaccounts", accountsToMap);
				model.put("detailedtoaccounts", detailedAccountsToMap);
				model.put("toaccountspif", accountsToMapPIF);
				model.put("detailedtoaccountspif", detailedAccountsToMapPIF);
				model.put("favoritesTransfers", favs);
				model.put("frequencymap", frequencies);

				int size = 3;// accountsTo.size() > 10?10:accountsTo.size();
				model.put("listSize", size);

			}

		}

		if (page == 1) {
			model.put("todayDate", "");

			List<TVAccountTransfer> verifyTransfers = new LinkedList<TVAccountTransfer>();
			TransfersListBean transfersList = (TransfersListBean) command;
			BigDecimal totalAmount = new BigDecimal(0.0);
			for (Iterator<Object> iterator = transfersList.getTransfers().iterator(); iterator.hasNext();) {
				TransferBean name = (TransferBean) iterator.next();
				if (name.getAmount() == null) {
					iterator.remove();
					continue;
				}

				TVAccountTransferSource sou = null;
				TVAccountTransferTarget tar = null;

				// If it is a PIF account, create it
				if (name.getAccountFrom().startsWith(PIF_PREFIX)) {
					String externalAccountId = name.getAccountFrom().substring(3);
					PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
					if (externalAccount != null) {
						BankInformation bankInformation = new BankInformation();
						bankInformation.setBankId(externalAccount.getRoutingNumber());
						bankInformation.setBankName(externalAccount.getBankName());
						CustomerAccountPreferences pref = new CustomerAccountPreferences();
						TVAccountFactory accountFactory = new TVAccountFactory();
						Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA", externalAccountId);
						sourceAccount.setAccountID(externalAccountId);
						sourceAccount.setSubType("IDA");
						((DepositAccount) sourceAccount).setSectionIndicator(Character.toString(externalAccount
								.getAccountType()));
						TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
						fromAccount.setCustomerAccountPreferences(pref);
						fromAccount.setNickname(externalAccount.getNickname());
						fromAccount.setHide(externalAccount.getShow() == '0');
						fromAccount.getTVAccount().setBankInformation(bankInformation);
						sou = new TVAccountTransferSource(fromAccount);
						tar = Utils.getAccountTarget(name.getAccountToPifFilter(), userSession.getAccountsTransferTo());

						// this is for the native apps
						// DO NOT DELETE
						if (tar == null && name.getAccountTo() != null && !name.getAccountTo().equals("none")) {
							// came from the native app
							tar = Utils.getAccountTarget(name.getAccountTo(), userSession.getAccountsTransferTo());
						}
					}
				} else {
					sou = Utils.getAccountSource(name.getAccountFrom(), userSession.getAccountsTransferFrom());
					tar = Utils.getAccountTarget(name.getAccountTo(), userSession.getAccountsTransferTo());
				}

				if (sou == null || tar == null)
					continue;

				BigDecimal amount = name.getAmount();
				Calendar cal = Calendar.getInstance();
				cal.setTime(name.getEffectiveDate());
				cal = calculateEffDate(cal);
				name.setEffectiveDate(cal.getTime());

				TVAccountTransfer transfer = null;
				Integer inst = name.getInstances();
				inst = inst == null ? 0 : inst;
				String recurrent = name.getRecurrent();
				String infinite = name.getInfinite();

				if (recurrent != null && recurrent.equalsIgnoreCase("recurrent")) {
					if (infinite != null && infinite.equalsIgnoreCase("infinite")) {
						inst = 999;
					}
					transfer = new TVAccountTransfer(amount, name.getFrequency(), inst, cal, sou, tar);
				} else
					transfer = new TVAccountTransfer(amount, Frequency.MANUALLY, inst, cal, sou, tar);

				totalAmount = totalAmount.add(transfer.getAmount());
				verifyTransfers.add(transfer);

			}

			for (Iterator<Object> iterator = transfersList.getFavorites().iterator(); iterator.hasNext();) {
				TransferBean name = (TransferBean) iterator.next();
				if (name.getAmount() == null) {
					// iterator.remove();
					continue;
				}

				TVAccountTransferSource sou = null;
				TVAccountTransferTarget tar = null;

				// If it is a PIF account, create it
				if (name.getAccountFrom().startsWith(PIF_PREFIX)) {
					String externalAccountId = name.getAccountFrom().substring(3);
					PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
					if (externalAccount != null) {
						BankInformation bankInformation = new BankInformation();
						bankInformation.setBankId(externalAccount.getRoutingNumber());
						bankInformation.setBankName(externalAccount.getBankName());
						CustomerAccountPreferences pref = new CustomerAccountPreferences();
						TVAccountFactory accountFactory = new TVAccountFactory();
						Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA", externalAccountId);
						sourceAccount.setAccountID(externalAccountId);
						sourceAccount.setSubType("IDA");
						((DepositAccount) sourceAccount).setSectionIndicator(Character.toString(externalAccount
								.getAccountType()));
						TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
						fromAccount.setCustomerAccountPreferences(pref);
						fromAccount.setNickname(externalAccount.getNickname());
						fromAccount.setHide(externalAccount.getShow() == '0');
						fromAccount.getTVAccount().setBankInformation(bankInformation);
						sou = new TVAccountTransferSource(fromAccount);
						tar = Utils.getAccountTarget(name.getAccountToPifFilter(), userSession.getAccountsTransferTo());
					}
				} else {
					sou = Utils.getAccountSource(name.getAccountFrom(), userSession.getAccountsTransferFrom());
					tar = Utils.getAccountTarget(name.getAccountTo(), userSession.getAccountsTransferTo());
				}

				if (sou == null || tar == null)
					continue;

				BigDecimal amount = name.getAmount();

				Calendar cal = Calendar.getInstance();
				cal.setTime(name.getEffectiveDate());
				cal = calculateEffDate(cal);
				name.setEffectiveDate(cal.getTime());

				TVAccountTransfer transfer = null;
				Integer inst = name.getInstances();
				inst = inst == null ? 0 : inst;
				String recurrent = name.getRecurrent();
				String infinite = name.getInfinite();

				if (recurrent != null && recurrent.equalsIgnoreCase("recurrent")) {
					if (infinite != null && infinite.equalsIgnoreCase("infinite")) {
						inst = 999;
					}
					transfer = new TVAccountTransfer(amount, name.getFrequency(), inst, cal, sou, tar);
				} else
					transfer = new TVAccountTransfer(amount, Frequency.MANUALLY, inst, cal, sou, tar);

				totalAmount = totalAmount.add(transfer.getAmount());
				verifyTransfers.add(transfer);
			}
			model.put("verifyTransfers", verifyTransfers);
			model.put("totalAmount", totalAmount);
		}
		return model;
	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		return new ModelAndView(cancelView);
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		if (page == 0 || page == 1) {
			boolean err = false;
			TransfersListBean plist = (TransfersListBean) command;

			int i = 0;
			for (Iterator<Object> iter = plist.getTransfers().iterator(); iter.hasNext();) {
				TransferBean element = (TransferBean) iter.next();
				if (element.getAmount() == null) {
					iter.remove();
					continue;
				}

				BigDecimal maxpayamount = new BigDecimal(999999.99);
				if (element.getAmount().compareTo(maxpayamount) > 0) {
					errors.rejectValue("transfers[" + i + "].amount", "transfer.amount.largermax",
							"Amount larger than max");
					err = true;
				} else if (element.getAmount().doubleValue() < new Double(0.01d)) {
					errors.rejectValue("transfers[" + i + "].amount", "transfer.amount.invalid", "Invalid");
					err = true;
				}

				if ("none".equalsIgnoreCase(element.getAccountFrom())) {
					errors.rejectValue("transfers[" + i + "].accountFrom", "transfer.account.required",
							"Account From is required");
					err = true;
				}

				if (!element.getAccountFrom().startsWith(PIF_PREFIX) && "none".equalsIgnoreCase(element.getAccountTo())) {
					errors.rejectValue("transfers[" + i + "].accountTo", "transfer.account.required",
							"Account To is required");
					err = true;
				}

				if (element.getAccountFrom().startsWith(PIF_PREFIX)
						&& "none".equalsIgnoreCase(element.getAccountToPifFilter())) {
					errors.rejectValue("transfers[" + i + "].accountToPifFilter", "transfer.account.required",
							"Account To is required");
					err = true;
				}

				if (!"variable".equalsIgnoreCase(element.getRecurrent()) && element.getFrequency() == null) {
					errors.rejectValue("transfers[" + i + "].frequency", "transfer.frequency.required",
							"Please select the frequency");
					err = true;
				}

				if ("recurrent".equalsIgnoreCase(element.getRecurrent())) {
					if (GenericValidator.isBlankOrNull(element.getInfinite())) {
						errors.rejectValue("transfers[" + i + "].instances", "transfer.instance.required",
								"Please select an instance");
						err = true;
					}
					if ("byinstances".equalsIgnoreCase(element.getInfinite())
							&& (element.getInstances() == null || element.getInstances() < 2)) {
						errors.rejectValue("transfers[" + i + "].instances", "transfer.amtinstances.required",
								"Please select two or more instances");
						err = true;
					}
				}
				Date effectiveDate = element.getEffectiveDate();
				Calendar today = Calendar.getInstance();

				// Set the max date (one year in future)
				Calendar thresholdDate = Calendar.getInstance();
				thresholdDate.add(Calendar.YEAR, 1);

				if (effectiveDate != null) {
					// Validate no future dates
					Calendar n = Calendar.getInstance();
					n.setTime(effectiveDate);

					if ((n.get(Calendar.YEAR) < today.get(Calendar.YEAR))
							|| n.get(Calendar.YEAR) == today.get(Calendar.YEAR)
							&& n.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR)) {
						errors.rejectValue("transfers[" + i + "].effectiveDate", "transfer.date.past",
								"Invalid Date.<br>(Day in the past)");
						err = true;
					} else if (n.after(thresholdDate)) {
						errors.rejectValue("transfers[" + i + "].effectiveDate", "transfer.date.future",
								"Invalid Date.<br/>(Date more than one year in future)");
						err = true;
					}

				} else {
					errors.rejectValue("transfers[" + i + "].effectiveDate", "transfer.date.required", "Required");
					err = true;
				}
				i++;
			}
			int m = 0;
			int noamount = 0;
			for (Iterator<Object> iter = plist.getFavorites().iterator(); iter.hasNext();) {
				TransferBean element = (TransferBean) iter.next();
				if (element.getAmount() == null) {
					// iter.remove();
					m++;
					noamount++;
					continue;
				}

				BigDecimal maxpayamount = new BigDecimal(999999.99);
				if (element.getAmount().compareTo(maxpayamount) > 0) {
					errors.rejectValue("favorites[" + m + "].amount", "transfer.amount.largermax",
							"Amount larger than max");
					err = true;
				} else if (element.getAmount().doubleValue() < new Double(0.01d)) {
					errors.rejectValue("favorites[" + m + "].amount", "transfer.amount.invalid", "Invalid");
					err = true;
				}

				if ("none".equalsIgnoreCase(element.getAccountFrom())) {
					errors.rejectValue("favorites[" + m + "].accountFrom", "transfer.account.required",
							"Account From is required");
					err = true;
				}

				if (!element.getAccountFrom().startsWith(PIF_PREFIX) && "none".equalsIgnoreCase(element.getAccountTo())) {
					errors.rejectValue("favorites[" + m + "].accountTo", "transfer.account.required",
							"Accont To is required");
					err = true;
				}

				if (element.getAccountFrom().startsWith(PIF_PREFIX)
						&& "none".equalsIgnoreCase(element.getAccountToPifFilter())) {
					errors.rejectValue("transfers[" + i + "].accountToPifFilter", "transfer.account.required",
							"Account To is required");
					err = true;
				}

				if (!"variable".equalsIgnoreCase(element.getRecurrent()) && element.getFrequency() == null) {
					errors.rejectValue("favorites[" + m + "].frequency", "transfer.frequency.required",
							"Please select the frequency");
					err = true;
				}

				if ("recurrent".equalsIgnoreCase(element.getRecurrent())) {
					if (GenericValidator.isBlankOrNull(element.getInfinite())) {
						errors.rejectValue("favorites[" + m + "].instances", "transfer.instance.required",
								"Please select an instance");
						err = true;
					}
					if ("byinstances".equalsIgnoreCase(element.getInfinite())
							&& (element.getInstances() == null || element.getInstances() < 2)) {
						errors.rejectValue("favorites[" + m + "].instances", "transfer.amtinstances.required",
								"Please select two or more instances");
						err = true;
					}
				}
				Date effectiveDate = element.getEffectiveDate();
				Calendar today = Calendar.getInstance();

				// Set the max date (one year in future)
				Calendar thresholdDate = Calendar.getInstance();
				thresholdDate.add(Calendar.YEAR, 1);

				if (effectiveDate != null) {
					// Validate no future dates
					Calendar n = Calendar.getInstance();
					n.setTime(effectiveDate);

					if ((n.get(Calendar.YEAR) < today.get(Calendar.YEAR))
							|| n.get(Calendar.YEAR) == today.get(Calendar.YEAR)
							&& n.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR)) {
						errors.rejectValue("favorites[" + m + "].effectiveDate", "transfer.date.past",
								"Invalid Date.<br/>(Day in the past)");
						err = true;
					} else if (n.after(thresholdDate)) {
						errors.rejectValue("favorites[" + m + "].effectiveDate", "transfer.date.future",
								"Invalid Date.<br/>(Date more than one year in future)");
						err = true;
					}
				} else {
					errors.rejectValue("favorites[" + m + "].effectiveDate", "transfer.date.required", "Required");
					err = true;
				}
				m++;
			}

			if (m - noamount + i == 0)
				errors.reject("transfers.null", "Please select a transfer and amount");

			if (err) {
				UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
				if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
					session.setStatusmessage("Please verify information entered");
				else
					session.setStatusmessage("Verifica la informaci&oacute;n entrada");
			}
		}
	}

	private boolean accountNotTRF(TVAccountTransferTarget to, List<TVFrontendAccount> trfAccounts) {
		boolean nonTRFAccount = true;
		for (Iterator<TVFrontendAccount> iterator = trfAccounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount trf = (TVFrontendAccount) iterator.next();
			if (to.getAccount().getAccountKey().equals(trf.getAccountKey())) {
				nonTRFAccount = false;
				break;
			}
		}
		return nonTRFAccount;
	}

	public String getCancelView() {
		return cancelView;
	}

	public void setCancelView(String cancelView) {
		this.cancelView = cancelView;
	}

	public Calendar calculateEffDate(Calendar cal) {
		List<Holiday> holidays = customerService.getHolidays();
		BankingDaysUtils bankingDaysUtils = new BankingDaysUtils(holidays);
		return bankingDaysUtils.calculatePayDaysWithCutoff(cal, 1, 17, 0);
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerServices) {
		this.customerService = customerServices;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.TRANSFERS;
	}

	/*
	 * CIBP-1680 bgarcia Obtiene el texto de error actualizado
	 */
	private String getTelepagoErrorDescriptor(String codigo, String len) {
		String status = "";
		try {
			if (!len.equals("en"))
				len = "es";
			status = this.messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
		} catch (Exception e) {
			status = "";
		}

		return status;
	}

	/*
	 * CIBP-1680 bgarcia Accede al recurso para el file
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/*
	 * CIBP-1680 bgarcia Setea el recurso para el file
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
}
