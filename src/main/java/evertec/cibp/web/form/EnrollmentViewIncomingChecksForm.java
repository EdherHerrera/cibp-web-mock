package evertec.cibp.web.form;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import com.google.common.collect.ImmutableList;

import edu.emory.mathcs.backport.java.util.Collections;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.applications.ApplicationStatus;
import evertec.cibp.core.models.customer.applications.CustomerApplication;
import evertec.cibp.core.models.customer.applications.CustomerApplicationType;
import evertec.cibp.core.models.customer.applications.ViewIncomingChecksApplication;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.CibpValidationUtils;
import evertec.cibp.web.form.bean.AddressBean;
import evertec.cibp.web.form.bean.EnrollmentViewIncomingChecksBean;
import evertec.cibp.web.form.bean.EnrollmentViewIncomingCheksAccountListBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.HasSectionUtil;
import evertec.cibp.web.utils.Utils;

public class EnrollmentViewIncomingChecksForm extends CancellableFormController {

    protected final Log logger = LogFactory.getLog(getClass());
    private CustomerServices customerServices;
    private MessageSource messageSource;
    private String businessAccounts;

    private static final String USERSESSION = "userSession";

    public EnrollmentViewIncomingChecksForm() {
        super();
        setCommandClass(EnrollmentViewIncomingChecksBean.class);
        setCommandName("viewIncomingChecks");
        setFormView("enrollmentViewIncomingChecksService");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
                                    BindException error) throws Exception {
        Map<String, Object> model = new LinkedHashMap<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        CustomerProfile customer = userSession.getCustomerProfile();
        String lang = Utils.getRSALanguage(request);
        EnrollmentViewIncomingChecksBean bean = (EnrollmentViewIncomingChecksBean) command;
        String name = bean.getName();
        String email = customer.getEmail();
        String enrollAcct = customer.getEnrollmentAccountNumber();
        String phone1 = bean.getPhone1();
        String phone2 = bean.getPhone2();
        String phone3 = bean.getPhone3();
        String businessName = bean.getBusinessName();
        String businessPhone1 = bean.getBusinessPhone1();
        String businessPhone2 = bean.getBusinessPhone2();
        String businessPhone3 = bean.getBusinessPhone3();

        String phone = phone1 + phone2 + phone3;
        String businessPhone = businessPhone1 + businessPhone2 + businessPhone3;

        AddressBean mailing = bean.getMailing();
        AddressBean physical = bean.getResidential();

        List<EnrollmentViewIncomingCheksAccountListBean> acctList = bean.getListAccounts();
        StringBuilder accounts = new StringBuilder();
         
        boolean saveApplication = false;
        boolean accountExists = true;
        boolean accountApproved = true;
        accounts.append("[");
        for (int i = 0; i < acctList.size(); i++) {
            if (acctList.get(i).getIsAdded().equalsIgnoreCase("YES")) {

                if (accountExists) {
                    accountExists = this.customerServices.existsApplicationsByAtt(
                            CustomerApplicationType.VIEW_HISTORY_CHECKS_MODIFY, acctList.get(i).getAccount(),
                            ApplicationStatus.NEW, ApplicationStatus.APPROVED);
                }

                boolean accountApproved1 = this.customerServices.existsApplicationsByAtt(
                        CustomerApplicationType.VIEW_HISTORY_CHECKS_MODIFY, acctList.get(i).getAccount(),
                        ApplicationStatus.APPROVED);

                String smq = accountApproved1 ? "Y" : "N";

                accounts.append("{~account~:~" + acctList.get(i).getAccount() + "~, ~disposition~:~"
                + acctList.get(i).getAccountDisposition() + "~, ~smq~:~" + smq + "~},");
                


                if (accountApproved) accountApproved = accountApproved1;
            }


        }
        
      accounts = new StringBuilder(accounts.substring(0, accounts.length() - 2) + "}]");
             

       
        ViewIncomingChecksApplication application = new ViewIncomingChecksApplication();
        application.setName(name);
        application.setPhone(phone);
        application.setEnrollAcct(enrollAcct);
        application.setBusinessName(businessName);
        application.setBusinessPhone(businessPhone);
        application.setEmail(email);
        application.setMailingAddress1(mailing.getAddress1());
        application.setMailingAddress2(mailing.getAddress2());
        application.setMailingAddressCity(mailing.getCity());
        application.setMailingAddressState(mailing.getState());
        application.setMailingAddressZip(this.getZipCodeWellFormed(mailing.getZipCode1(), mailing.getZipCode2()));
        application.setPhysicalAddress1(physical.getAddress1());
        application.setPhysicalAddress2(physical.getAddress2());
        application.setPhysicalAddressCity(physical.getCity());
        application.setPhysicalAddressState(physical.getState());
        application.setSsne(customer.getTaxId());

        application.setPhysicalAddressZip(this.getZipCodeWellFormed(physical.getZipCode1(), physical.getZipCode2()));
       
        application.setAccountsList(accounts.toString());

        try {

            if (accountApproved) {
                Date today = new Date();
                DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                String dateOut = df.format(today);
                String logReason = "(" + dateOut + ") No Comment (APPROVED By System)";
                application.getApplication().setApplicationStatus(ApplicationStatus.APPROVED);
                application.getApplication().setLastModifiedDate(today);
                application.getApplication().setHistoryNote(logReason);
                application.getApplication().setCsrUsername("System");
            }
            this.getCustomerServices().saveViewIncomingChecksApplication(customer, application);
            saveApplication = true;
            boolean isFromAddNewAccount = WebUtils.getSessionAttribute(request, "isFpsAdd") != null
                    ? (boolean) WebUtils.getSessionAttribute(request, "isFpsAdd") : false;

            if (!accountApproved) {
                this.customerServices.sendViewIncomingChecksApplicationEnrollmentConfirmation(customer, application,
                        userSession.getPreferredLanguage(), isFromAddNewAccount);
               userSession.getCustomerApplications(true);
            }

        } catch (RuntimeException e) {
            saveApplication = false;
        }

        if (!saveApplication) {
            userSession.setStatusmessage(this.getMessageSource().getMessage("commercialservice.enrollment.fail", null,
                    new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang)));
        } else {

            if (accountExists) {
                userSession.setStatusmessage(this.getMessageSource().getMessage("fraud.service.enrollment.account.exists", null,
                        new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang)));
            } else {
                userSession.setStatusmessage(this.getMessageSource().getMessage("fraud.service.enrollment.successful", null,
                        new Locale(lang.equalsIgnoreCase("sp") ? "ES" : lang)));
            }


        }

        model.put("completed", true);
        model.put("saveApplication", saveApplication);
        return showForm(request, error, "enrollmentViewIncomingChecksService", model);
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
      
    	EnrollmentViewIncomingChecksBean bean = (EnrollmentViewIncomingChecksBean) super.formBackingObject(request);
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
    	boolean isNewEnrollment = true;
        String lang = Utils.getRSALanguage(request);
        ViewIncomingChecksApplication viewIncomingChecksApplication = null;

        if (bean.getName() == null) {
            bean.setName(userSession.getCustomerProfile().getFullName());
        }

        List<CustomerApplication> customerApplications = Utils.getCustomerAppbyType(userSession.getCustomerApplications(), CustomerApplicationType.VIEW_HISTORY_CHECKS_MODIFY, ApplicationStatus.All);
        Collections.sort(customerApplications, Collections.reverseOrder());

        for (CustomerApplication customerApplication : customerApplications) {
            if (customerApplication.getApplicationStatus() == ApplicationStatus.APPROVED
                    || customerApplication.getApplicationStatus() == ApplicationStatus.NEW) {
                ViewIncomingChecksApplication viewIncomingChecksApplication1 = new ViewIncomingChecksApplication(
                        customerApplication);
                bean.getAddedAccounts().add(viewIncomingChecksApplication1.getAccountsList());
                if (viewIncomingChecksApplication == null) {
                    viewIncomingChecksApplication = viewIncomingChecksApplication1;
                    isNewEnrollment = false;
                }

            }
        }

        if (request.getParameter("fpsAdd") != null) {
            boolean isFpsAdd = Boolean.valueOf(request.getParameter("fpsAdd")).booleanValue() && !isNewEnrollment;
            WebUtils.setSessionAttribute(request, "isFpsAdd", isFpsAdd);
        }

        if (((Boolean) WebUtils.getSessionAttribute(request, "isFpsAdd")).booleanValue()) {
            bean.fillFrom(viewIncomingChecksApplication);
        }

        return bean;
    }

    @Override
    protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
            throws Exception {

        EnrollmentViewIncomingChecksBean bean = (EnrollmentViewIncomingChecksBean) command;

        String phone1 = bean.getPhone1();
        String phone2 = bean.getPhone2();
        String phone3 = bean.getPhone3();

        String businessPhone1 = bean.getBusinessPhone1();
        String businessPhone2 = bean.getBusinessPhone2();
        String businessPhone3 = bean.getBusinessPhone3();

        String businessName = bean.getBusinessName();

        String terms = bean.getTermsConfirmation();

        AddressBean mailing = bean.getMailing();
        AddressBean physical = bean.getResidential();

        WebUtils.setSessionAttribute(request, "hasBeenValidated", true);

        List<EnrollmentViewIncomingCheksAccountListBean> acctList = bean.getListAccounts();

        if (GenericValidator.isBlankOrNull(businessName)) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                errors.rejectValue("businessName", "", "Please enter a valid business name.");
            else
                errors.rejectValue("businessName", "", "Entra un nombre de negocio v&aacute;lido.");
        }

        if (GenericValidator.isBlankOrNull(phone1) || GenericValidator.isBlankOrNull(phone2)
                || GenericValidator.isBlankOrNull(phone3))
            if (GenericValidator.isBlankOrNull(phone1) || !GenericValidator.matchRegexp(phone1, "^\\d{3}$")
                    || GenericValidator.isBlankOrNull(phone2) || !GenericValidator.matchRegexp(phone2, "^\\d{3}$")
                    || GenericValidator.isBlankOrNull(phone3) || !GenericValidator.matchRegexp(phone3, "^\\d{4}$")) {

                if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                    errors.rejectValue("phone3", "", "Please enter a home phone number.");
                else
                    errors.rejectValue("phone3", "", "Por favor entre un tel&eacute;fono residencial.");
            }

        if (GenericValidator.isBlankOrNull(businessPhone1) || GenericValidator.isBlankOrNull(businessPhone2)
                || GenericValidator.isBlankOrNull(businessPhone3))
            if (GenericValidator.isBlankOrNull(businessPhone1)
                    || !GenericValidator.matchRegexp(businessPhone1, "^\\d{3}$")
                    || GenericValidator.isBlankOrNull(businessPhone2)
                    || !GenericValidator.matchRegexp(businessPhone2, "^\\d{3}$")
                    || GenericValidator.isBlankOrNull(businessPhone3)
                    || !GenericValidator.matchRegexp(businessPhone3, "^\\d{4}$")) {

                if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                    errors.rejectValue("businessPhone3", "", "Please enter a Business phone number.");
                else
                    errors.rejectValue("businessPhone3", "", "Por favor entre un tel&eacute;fono del Negocio.");
            }

        ////////////////////////////////////////////////////////////

        if (GenericValidator.isBlankOrNull(mailing.getAddress1())
                || !CibpValidationUtils.isValidAddress(mailing.getAddress1())) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                errors.rejectValue("mailing.address1", "", "Please enter a valid address.");
            else
                errors.rejectValue("mailing.address1", "", "Favor entrar una direcci&oacute;n valida.");
        }

        if (!GenericValidator.isBlankOrNull(mailing.getAddress2())
                && !CibpValidationUtils.isValidAddress(mailing.getAddress2())) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                errors.rejectValue("mailing.address2", "", "Please enter a valid address.");
            else
                errors.rejectValue("mailing.address2", "", "Favor entrar una direcci&oacute;n valida.");
        }
        if (GenericValidator.isBlankOrNull(mailing.getCity())
                || !GenericValidator.matchRegexp(mailing.getCity(), "^[A-Za-z ]+$")) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                errors.rejectValue("mailing.city", "", "Please enter a<br/>valid city.");
            else
                errors.rejectValue("mailing.city", "", "Favor entrar una<br/>ciudad valida.");
        }

        if (GenericValidator.isBlankOrNull(mailing.getState())
                || !GenericValidator.matchRegexp(mailing.getState(), "^[A-Za-z ]+$")) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                errors.rejectValue("mailing.state", "", "Please enter a<br/>valid state.");
            else
                errors.rejectValue("mailing.state", "", "Favor entrar un<br/>estado valido.");
        }


        if (!this.isValideZipCode(mailing.getZipCode1(), mailing.getZipCode2())) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                errors.rejectValue("mailing.zipCode2", "", "Please enter a<br/>valid zipcode.");
            else
                errors.rejectValue("mailing.zipCode2", "", "Favor entrar un c&oacute;digo<br/>postal valido.");
        }

        ////////////////////////////////////////////////////////////

        if (GenericValidator.isBlankOrNull(physical.getAddress1())
                || !CibpValidationUtils.isValidAddress(physical.getAddress1())) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                errors.rejectValue("residential.address1", "", "Please enter a valid address.");
            else
                errors.rejectValue("residential.address1", "", "Favor entrar una direcci&oacute;n valida.");
        }

        if (!GenericValidator.isBlankOrNull(physical.getAddress2())
                && !CibpValidationUtils.isValidAddress(physical.getAddress2())) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                errors.rejectValue("residential.address2", "", "Please enter a valid address.");
            else
                errors.rejectValue("residential.address2", "", "Favor entrar una direcci&oacute;n valida.");
        }
        if (GenericValidator.isBlankOrNull(physical.getCity())
                || !GenericValidator.matchRegexp(physical.getCity(), "^[A-Za-z ]+$")) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                errors.rejectValue("residential.city", "", "Please enter a<br/>valid city.");
            else
                errors.rejectValue("residential.city", "", "Favor entrar una<br/>ciudad valida.");
        }

        if (GenericValidator.isBlankOrNull(physical.getState())
                || !GenericValidator.matchRegexp(physical.getState(), "^[A-Za-z ]+$")) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                errors.rejectValue("residential.state", "", "Please enter a<br/>valid state.");
            else
                errors.rejectValue("residential.state", "", "Favor entrar un<br/>estado valido.");
        }

        if (!this.isValideZipCode(physical.getZipCode1(), physical.getZipCode2())) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                errors.rejectValue("residential.zipCode2", "", "Please enter a<br/>valid zipcode.");
            else
                errors.rejectValue("residential.zipCode2", "", "Favor entrar un c&oacute;digo<br/>postal valido.");
        }

        if (GenericValidator.isBlankOrNull(terms) || !terms.equalsIgnoreCase("on")) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                errors.rejectValue("termsConfirmation", "", "Please agree.");
            else
                errors.rejectValue("termsConfirmation", "", "Por favor acepte los t&eacute;rminos y condiciones.");
        }

        int accounts = 0;

        for (int i = 0; i < acctList.size(); i++) {
            if (acctList.get(i).getIsAdded().equalsIgnoreCase("NO")) {
                accounts++;
            }
        }
        if (accounts == acctList.size()) {
            if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
                errors.rejectValue("isValid", "", "Please add at least one account");
            else
                errors.rejectValue("isValid", "", "Por favor ingrese al menos una cuenta");
        }

    }

    @Override
    protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
            throws Exception {

        Map<String, Object> model = new HashMap<>();

        List<String> businessAccounts = ImmutableList.copyOf(this.getBusinessAccounts().split(","));
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        Set<TVAccountTransferSource> accountsTransferSource = userSession.getAccountsPaymentFrom();
        EnrollmentViewIncomingChecksBean bean = (EnrollmentViewIncomingChecksBean) command;

        boolean hasBeenValidated = WebUtils.getSessionAttribute(request, "hasBeenValidated") != null
                ? (boolean) WebUtils.getSessionAttribute(request, "hasBeenValidated") : false;

        List<EnrollmentViewIncomingCheksAccountListBean> listAccounts = bean.getListAccounts();

        /*
         * String name = bean.getName(); if (bean.getName() == null) {
         * bean.setName(userSession.getCustomerProfile().getFullName()); }
         *
         * if (GenericValidator.isBlankOrNull(name) ||
         * !GenericValidator.matchRegexp(name, "^\\d+$")) { name = (String)
         * request.getParameter("name"); }
         */
        Map<String, String> accountsModel = new LinkedHashMap<>();
        if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
            accountsModel.put("none", "Select an account");
        else
            accountsModel.put("none", "Selecciona una cuenta");

        if (accountsTransferSource != null) {
            List<String> notAllowedAccts = HasSectionUtil.hasAccountsSectionNotAllowed(HasSectionUtil.FRAUD_PREVENTION_SERVICE_SECTION, userSession);

            List<String> noAvailEccounts = userSession.getCustomerService()
                    .getDCINotAvailableEaccounts(userSession.getCustomerProfile());
            EnrollmentViewIncomingCheksAccountListBean accountInfo = null;
            for (Iterator<TVAccountTransferSource> iterator = accountsTransferSource.iterator(); iterator.hasNext(); ) {
                TVAccountTransferSource from = (TVAccountTransferSource) iterator.next();
                String productId = from.getAccount().getProductDefinition().getProductId();
                String accountNumber = from.getAccount().getAccountNumber();
                if (businessAccounts.contains(productId)) {
                    if (!noAvailEccounts.contains(accountNumber) && !notAllowedAccts.contains(accountNumber)) {
                        TVFrontendAccount frontFrom = Utils.getAccountByKey(from.getAccount().getAccountKey(),
                                userSession.getAccounts());
                        if (frontFrom != null)
                            if (!frontFrom.getHide()) {
                                accountInfo = new EnrollmentViewIncomingCheksAccountListBean();
                                Map<String, String> accountDetail = accountDetailForAccount(frontFrom);
                                boolean skipAccount = false;
                                for (String account : bean.getAddedAccounts()) {
                                    skipAccount = account.contains(accountDetail.get("accountNumber"));
                                    if (skipAccount)
                                        break;
                                }

                                if (!hasBeenValidated) {
                                    accountInfo.setAccountId(from.getFrontEndId());
                                    accountInfo.setAccount(accountDetail.get("accountNumber") != null
                                            ? accountDetail.get("accountNumber").toString() : "");
                                    accountInfo.setAccountDisposition("PAY");
                                    accountInfo.setIsAdded("NO");
                                    listAccounts.add(accountInfo);
                                }
                                if (!skipAccount) {
                                    accountsModel.put(from.getFrontEndId(), accountDetail.get("text"));
                                }
                            }
                    }
                }
            }
            request.getSession().removeAttribute("hasBeenValidated");
        }

        model.put("accountDetails", listAccounts);
        model.put("amountAccountDetails", listAccounts.size());
        model.put("states", this.getStates());
        model.put("customer", userSession.getCustomerProfile());
        model.put("accounts", accountsModel);
        model.put("isFpsAdd", WebUtils.getSessionAttribute(request, "isFpsAdd"));
        return model;
    }

    private String getZipCodeWellFormed(String zipCode1, String zipCode2) {
        StringBuilder zipCode = new StringBuilder();
        if (!GenericValidator.isBlankOrNull(zipCode1) && !GenericValidator.isBlankOrNull(zipCode2)) {
            zipCode.append(zipCode1 + "-" + zipCode2);
        } else if (!GenericValidator.isBlankOrNull(zipCode1)) {
            zipCode.append(zipCode1);
        }
        if (!GenericValidator.isBlankOrNull(zipCode2) && GenericValidator.isBlankOrNull(zipCode1)) {
            zipCode.append(zipCode2);
        }
        return zipCode.toString();
    }

    private boolean isValideZipCode(String zipCode1, String zipCode2) {
        boolean validateZip2 = false;
        boolean isValid = true;
        if (GenericValidator.isBlankOrNull(zipCode1) && GenericValidator.isBlankOrNull(zipCode2)) {
            isValid = false;
        } else if (GenericValidator.isBlankOrNull(zipCode1) || !GenericValidator.matchRegexp(zipCode1, "^(?!([0123456789])\\1{4})\\d{5}")) {
            isValid = false;
            if (GenericValidator.isBlankOrNull(zipCode1)) {
                validateZip2 = true;
            }
        }
        if (validateZip2) {
            isValid = true;
            if (GenericValidator.isBlankOrNull(zipCode2) || !GenericValidator.matchRegexp(zipCode2, "^(?!([0123456789])\\1{3})\\d{4}")) {
                isValid = false;
            }
        }


        return isValid;
    }

    private Map<String, String> accountDetailForAccount(TVFrontendAccount account) {
        Map<String, String> accountDetail = new LinkedHashMap<String, String>();
        if (account == null) {
            accountDetail.put("accountName", "");
            accountDetail.put("acctLast4Num", "");
            accountDetail.put("accountBalance", "");
            accountDetail.put("accountNumber", "");
            accountDetail.put("accountSubtype", "");
            accountDetail.put("accountSection", "");

            accountDetail.put("text", "");

        } else {
            AccountMaskUtil mask = new AccountMaskUtil();
            NumberTool numberTool = new NumberTool();

            StringBuilder sb = new StringBuilder();

            accountDetail.put("accountName", account.getNickname());
            accountDetail.put("accountLast4Num", mask.mask(account.getAccountNumber()));
            accountDetail.put("accountNumber", account.getAccountNumber());
            accountDetail.put("accountSection", account.getAccountSection());
            accountDetail.put("accountSubtype", account.getProductDefinition().getProductType().trim());
            accountDetail.put("accountNumberSuffix", account.getAccountNumberSuffix());
            if (account != null && !account.getSubtype().equals("CCA") && account.getPortalBalance() != null) {
                accountDetail.put("accountBalance", numberTool.format("currency", account.getPortalBalance()));
                accountDetail
                        .put("accountNumberSuffix",
                                ((account.getAccountNumberSuffix() != null
                                        && !account.getAccountNumberSuffix().equals(""))
                                        ? " " + account.getAccountNumberSuffix() : ""));
            }
            sb.append(StringUtils.abbreviate(accountDetail.get("accountName"), 15));
            sb.append(" ");
            sb.append(accountDetail.get("accountLast4Num"));
            if (!accountDetail.get("accountBalance").equals("")) {
                sb.append(" ");
                sb.append(accountDetail.get("accountBalance"));
            }
            if (!accountDetail.get("accountNumberSuffix").equals("")) {
                sb.append(" ");
                sb.append(accountDetail.get("accountNumberSuffix"));
            }

            accountDetail.put("text", sb.toString());
        }
        return accountDetail;
    }

    private Map<Object, String> getStates() {
        Map<Object, String> states = new LinkedHashMap<Object, String>();
        states.put("PR", "PR");
        states.put("USVI", "USVI");

        return states;
    }

    public CustomerServices getCustomerServices() {
        return customerServices;
    }

    public void setCustomerServices(CustomerServices customerServices) {
        this.customerServices = customerServices;
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getBusinessAccounts() {
        if (businessAccounts == null) {
            businessAccounts = "";
        }
        return businessAccounts;
    }

    public void setBusinessAccounts(String businessAccounts) {
        this.businessAccounts = businessAccounts;
    }

}
