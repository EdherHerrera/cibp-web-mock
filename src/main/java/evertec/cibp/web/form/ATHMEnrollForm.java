package evertec.cibp.web.form;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import bppr.tv.beans.accounts.types.DepositAccount;
import edu.emory.mathcs.backport.java.util.Collections;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.entitlement.ATHMovilEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ATHMEnrollBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AthmUtils;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.AthmGenerateCallReq;
import evertec.otherservices.requests.AthmIsPhoneActiveReq;
import evertec.otherservices.requests.AthmLoginReq;
import evertec.otherservices.requests.AthmPhoneValidationReq;
import evertec.otherservices.requests.AthmStatusReq;
import evertec.otherservices.responses.AthmGenerateCallResp;
import evertec.otherservices.responses.AthmIsPhoneActiveResp;
import evertec.otherservices.responses.AthmLoginResp;
import evertec.otherservices.responses.AthmPhoneValidationResp;
import evertec.otherservices.responses.AthmStatusResp;

public class ATHMEnrollForm extends AbstractWizardFormController implements MessageSourceAware {
	protected final Log logger = LogFactory.getLog(getClass());

	protected CustomerServices customerService = null;
	private MessageSource messageSource = null;

	private String fiId = null;
	private String fiToken = null;
	private String enrollmentMaxTries = null;
	private String maxExpDateYears = null;
	private String allowedCardsPrefix = null;
	private String allowedAthmCardStatus = null;

	public ATHMEnrollForm() {
		super();
		setCommandClass(ATHMEnrollBean.class);
		setCommandName("athmenroll");
		setPages(new String[] { "athmenroll", "athmenroll", "athmenroll", "athmenroll", "athmenroll", "athmenroll" });
	}

	@Override
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		setAllowDirtyForward(false);

		String[] protectedFields = { "validationCount", "pinCount", "phoneInUse", "profileExists", "smsCodeSent",
				"step" };
		binder.setDisallowedFields(protectedFields);
	}
  
  
  @Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		ATHMEnrollBean athmEnrollBean = (ATHMEnrollBean) super.formBackingObject(request);
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		UserAgentType agentType = UserAgentType.WEB;
		
		// Use new Flag to present the View Type
		if (userSession != null && userSession.getUserAgentType() != null) {
			agentType = userSession.getUserAgentType();
			if (agentType == UserAgentType.JSON && athmEnrollBean != null) {
				athmEnrollBean.setMobileAppType(agentType.toString());
				athmEnrollBean.setMobileApp(Boolean.TRUE);
			}
		}
		
		return athmEnrollBean;
	}
  
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<String, Object> model = new HashMap<>();

		ATHMEnrollBean athmEnrollBean = (ATHMEnrollBean) command;
		GlobalEntitlement entitlement = customerService.getGlobalEntitlement(EntitlementType.ATHMOVIL);

		model.put("page", page);

		if (athmEnrollBean != null) {
			if (athmEnrollBean.getStep() <= 0) {
				athmEnrollBean.setStep(1);
			}

			model.put("step", athmEnrollBean.getStep());
		}

		if (entitlement == null || entitlement.getStatus() != EntitlementStatus.ENABLED) {
			model.put("down", "true");

			return model;
		}

		if (athmEnrollBean != null) {
			if (athmEnrollBean.isServiceDown()) {
				model.put("serviceDown", athmEnrollBean.isServiceDown());
				athmEnrollBean.setServiceDown(false);
				return model;
			} else if (athmEnrollBean.isSectionAutoBlock()) {
				model.put("athmBlock", athmEnrollBean.isSectionAutoBlock());
				athmEnrollBean.setSectionAutoBlock(false); // reset
			}
		}

		List<TVFrontendAccount> cardPlasticAccountList = userSession.getCardPlasticAccounts();
		switch (page) {
		case 0:

			if (cardPlasticAccountList != null && cardPlasticAccountList.size() > 0) {
				model.put("qualify", true);
			} else {
				model.put("qualify", false);
			}

			break;

		case 1:
			if (athmEnrollBean != null) {
				loadPhoneProviderData(request, command, model);
			}

			break;

		case 2: // Random code generation
			if (athmEnrollBean != null) {
				if (athmEnrollBean.isValidationCallError()) {
					this.generateCall(athmEnrollBean, model);

				}
				if (athmEnrollBean.isWaitCall()) {
					model.put("validated", athmEnrollBean.isWaitCall());
					model.put("code", athmEnrollBean.getRandomCode());

				} else {
					this.generateCall(athmEnrollBean, model);
				}
			}

			break;

		case 3:
			if (cardPlasticAccountList != null && cardPlasticAccountList.size() > 0) {
				model.put("qualify", true);
				loadAccountsData(request, command, model);
				loadExpirationDateData(request, command, model);
			} else {
				model.put("qualify", false);
			}

			break;

		case 4:
			loadTermsData(request, command, model);

			if (GenericValidator.isBlankOrNull(athmEnrollBean.getAction())) {
				AthmIsPhoneActiveReq validateProfilePhone = new AthmIsPhoneActiveReq();
				validateProfilePhone.setPhoneNumber(athmEnrollBean.getPhoneNumber().replaceAll("-", ""));
				validateProfilePhone.setFiID(fiId);
				validateProfilePhone.setFiToken(fiToken);

				AthmIsPhoneActiveResp response = userSession.getCustomerService().isPhoneActive(validateProfilePhone);

				if (response != null && response.getResponseStatus().equals(ResponseStatus.ERROR)) {
					logger.error("Phone validation failed. ATH Movil call generated an exception");
					model.put("serviceDown", true);
				} else if (response != null && response.getResponseStatus() == ResponseStatus.SUCCESS) {
					if (response.isActive()) {
						athmEnrollBean.setAction("login");
						model.put("action", "login");
					} else {
						athmEnrollBean.setAction("enroll");
						model.put("action", "enroll");
					}
				} else {
					logger.error("Phone Active validation is null");
				}
			} else {
				model.put("action", athmEnrollBean.getAction());
			}

			break;

		case 5:

		default:
			break;
		}

		return model;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		ATHMEnrollBean athmEnrollBean = (ATHMEnrollBean) command;
		
		String language = "sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "es" : Utils.getRSALanguage(request);
		Locale locale = new Locale(language);
		
		if (athmEnrollBean.isMobileApp()){
			athmEnrollBean.setError("");			
			if ("true".equalsIgnoreCase(athmEnrollBean.getMobileCancelForm())){
				page = 0;
				athmEnrollBean.setMobileCancelForm("false");
				athmEnrollBean.setWaitCall(false);
			}
		}

		athmEnrollBean.setErrorFound(false);
		if (page == 1) // Register
						// phone--------------------------------------------------
		{
			Long pNumber = AthmUtils.getNumericPhoneNumber(athmEnrollBean.getPhoneNumber());
			if (pNumber == null) {
				errors.rejectValue("phoneNumber", "", "*");
				athmEnrollBean.setErrorFound(true);
				athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.apps.enroll.step1.phone.invalid", null, locale));
			}
			if (athmEnrollBean.getPhoneProvider().equals("none")) {
				errors.rejectValue("phoneProvider", "", "*");
				athmEnrollBean.setErrorFound(true);
				athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.apps.enroll.step1.provider.error", null, locale));
			}
			if (!athmEnrollBean.isErrorFound()) {
				if (athmEnrollBean.getPhoneNumber().startsWith("0")) {
					errors.rejectValue("phoneNumber", "", "*");
					errors.rejectValue("error", "athmobile.portal.box.enroll.step1.phone.invalid", "*Invalid");
					
					athmEnrollBean.setErrorFound(true);
					athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.apps.enroll.step1.phone.invalid", null, locale));
					return;
				}
			}
			if (athmEnrollBean.isErrorFound()) {
				errors.rejectValue("error", "athmobile.portal.box.enroll.required2", "*Required");
			}
		} else if (page == 2) // Enter random
								// code---------------------------------------------------
		{
			// Verify if the user has entered the code
			AthmPhoneValidationReq phoneValReq = new AthmPhoneValidationReq();
			phoneValReq.setFiID(fiId);
			phoneValReq.setFiToken(fiToken);
			phoneValReq.setPhoneValidationID(athmEnrollBean.getPhoneValidationId());

			AthmPhoneValidationResp phoneValResp = customerService.verifyPhoneValidationStatus(phoneValReq);
			if (phoneValResp != null && phoneValResp.getResponseStatus().equals(ResponseStatus.SUCCESS)) {
				if (!("200".equals(phoneValResp.getStatusCode()))) // telephone not confirmed
				{
					if ("94".equals(phoneValResp.getStatusCode()))// User has not received call...WAIT
					{
						athmEnrollBean.setWaitCall(true);
						athmEnrollBean.setValidationCallError(false);
					} else if ("90".equals(phoneValResp.getStatusCode()))// User did not answer or 3 times wrong combination
					{
						athmEnrollBean.setValidationCallError(false);
						athmEnrollBean.setWaitCall(true);
					}
					errors.rejectValue("error", "athmobile.portal.box.enroll.step1.provider.error", "Required");
					athmEnrollBean.setErrorFound(true);
				} else {
					athmEnrollBean.setWaitCall(false);
					athmEnrollBean.setValidationCallError(false);
				}
			} else {
				athmEnrollBean.setServiceDown(true);
			}
		} else if (page == 3) // Select account
								// step------------------------------------------------------
		{
			if (athmEnrollBean.getAccount().equals("none")) {
				errors.rejectValue("account", "athmobile.portal.box.enroll.step3.error", "Required");
				athmEnrollBean.setErrorFound(true);
			}
			if (GenericValidator.isBlankOrNull(athmEnrollBean.getPlasticNum())) {
				errors.rejectValue("plasticNum", "athmobile.portal.box.enroll.step3.plasticnum.error", "*");
				athmEnrollBean.setErrorFound(true);
			} else if (athmEnrollBean.getPlasticNum().length() != 16) {
				errors.rejectValue("plasticNum", "athmobile.portal.box.enroll.step3.plasticnumlength.error", "*");
				athmEnrollBean.setErrorFound(true);
			}
			if ("0".equals(athmEnrollBean.getPlasticExpDateMonth())
					|| "0".equals(athmEnrollBean.getPlasticExpDateYear())) {
				errors.rejectValue("error", "athmobile.portal.box.enroll.step3.expdate.error", "*");
				athmEnrollBean.setErrorFound(true);
			}

			if (!athmEnrollBean.isErrorFound()) {

				List<TVFrontendAccount> cardPlasticAccountList = userSession.getCardPlasticAccounts();

				if (cardPlasticAccountList != null && cardPlasticAccountList.size() > 0) {
					TVFrontendAccount frontendAccount = Utils.getAccountTransferSourceById(athmEnrollBean.getAccount(),
							userSession.getAthmAccounts());

					if (frontendAccount == null) {
						errors.rejectValue("account", "athmobile.portal.box.enroll.setep3.accountError",
								"There was an error trying to get your account information.");
						athmEnrollBean.setErrorFound(true);
						athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.portal.box.enroll.setep3.accountError", null, locale));
						return;
					}

					AbstractAccount accountDetails = userSession.getCustomerService().getCardPlasticAccount(
							userSession.getCustomerProfile(), userSession.getBankingSession(), frontendAccount, true);

					if (accountDetails == null || accountDetails.getBackendObject() == null) {
						errors.rejectValue("account", "athmobile.portal.box.enroll.setep3.accountError",
								"There was an error trying to get your account information.");
						athmEnrollBean.setErrorFound(true);
						athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.portal.box.enroll.setep3.accountError", null, locale));
						return;
					}

					DepositAccount depositAccount = (DepositAccount) accountDetails.getBackendObject();

					if (depositAccount == null || depositAccount.getCardPlasticAccountRecords() == null) {
						errors.rejectValue("account", "athmobile.portal.box.enroll.setep3.accountError",
								"There was an error trying to get your account information.");
						athmEnrollBean.setErrorFound(true);
						athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.portal.box.enroll.setep3.accountError", null, locale));
						return;
					}

					CardPlasticAccountRecord[] cardPlasticAccountRecordList = depositAccount
							.getCardPlasticAccountRecords();

					if (cardPlasticAccountRecordList == null) {
						errors.rejectValue("account", "athmobile.portal.box.enroll.setep3.accountError",
								"There was an error trying to get your account information.");
						athmEnrollBean.setErrorFound(true);
						athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.portal.box.enroll.setep3.accountError", null, locale));
						return;
					}

					TVFrontendAccount frontendCardPlasticAccount = null;
					CardPlasticAccountRecord cardPlasticAccountRecord = null;
					boolean plasticMatch = false;
					if (cardPlasticAccountRecordList != null && cardPlasticAccountRecordList.length > 0) {

						for (int i = 0; i < cardPlasticAccountRecordList.length; i++) {
							cardPlasticAccountRecord = cardPlasticAccountRecordList[i];

							if (!GenericValidator.isBlankOrNull(cardPlasticAccountRecord.getCardPANId())
									&& cardPlasticAccountRecord.getCardPANId().equals(athmEnrollBean.getPlasticNum())) {
								
								String[] allowedCardsPrefixes = null;
								if(allowedCardsPrefix != null && allowedCardsPrefix.trim().length()>0){
									allowedCardsPrefixes = allowedCardsPrefix.split(",");
								}
								
								for (Iterator<TVFrontendAccount> iterator = cardPlasticAccountList.iterator(); iterator
										.hasNext();) {
									frontendCardPlasticAccount = iterator.next();
									// find the right Card Account
									if (cardPlasticAccountRecord.getCardPANId() != null
											&& AthmUtils.isAthmCardAllowed(cardPlasticAccountRecord.getCardPANId(), allowedCardsPrefixes)
											&& cardPlasticAccountRecord.getCardPANId().equals(frontendCardPlasticAccount.getAccountNumber())) {
										
										if (AthmUtils.isAthmCardStatusAllowed(cardPlasticAccountRecord.getStatus(), allowedAthmCardStatus)) {

											accountDetails = userSession.getCustomerService().getCardPlasticAccount(
													userSession.getCustomerProfile(), userSession.getBankingSession(),
													frontendCardPlasticAccount, true);

											if (accountDetails == null || accountDetails.getBackendObject() == null) {
												errors.rejectValue("account",
														"athmobile.portal.box.enroll.setep3.accountError",
														"There was an error trying to get your account information.");
												
												athmEnrollBean.setErrorFound(true);
												athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.portal.box.enroll.setep3.accountError", null, locale));
												break;
											}

											depositAccount = (DepositAccount) accountDetails.getBackendObject();

											if (depositAccount == null
													|| depositAccount.getCardPlasticAccountRecords() == null) {
												errors.rejectValue("account",
														"athmobile.portal.box.enroll.setep3.accountError",
														"There was an error trying to get your account information.");
												
												athmEnrollBean.setErrorFound(true);
												athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.portal.box.enroll.setep3.accountError", null, locale));
												break;
											}

											cardPlasticAccountRecordList = depositAccount
													.getCardPlasticAccountRecords();

											if (cardPlasticAccountRecordList == null) {
												errors.rejectValue("account",
														"athmobile.portal.box.enroll.setep3.accountError",
														"There was an error trying to get your account information.");
												
												athmEnrollBean.setErrorFound(true);
												athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.portal.box.enroll.setep3.accountError", null, locale));
												break;
											}

											for (int j = 0; j < cardPlasticAccountRecordList.length; j++) {
												cardPlasticAccountRecord = cardPlasticAccountRecordList[j];
												SimpleDateFormat yearMonthFormat = new SimpleDateFormat("yyyyMM");
												Calendar cardExpDate = cardPlasticAccountRecord.getExpirationDate();
												Calendar currentDate = Calendar.getInstance();
												String plasticExpDate = yearMonthFormat.format(cardExpDate.getTime());
												String userEnteredExpDate = athmEnrollBean.getPlasticExpDateYear()
														+ athmEnrollBean.getPlasticExpDateMonth();

												if (Integer.parseInt(yearMonthFormat.format(cardExpDate.getTime())) > Integer
														.parseInt(yearMonthFormat.format(currentDate.getTime()))
														&& frontendCardPlasticAccount.getAccountNumber().equals(
																athmEnrollBean.getPlasticNum())
														&& plasticExpDate.equals(userEnteredExpDate)) {

													athmEnrollBean.setPlasticExpDate(userEnteredExpDate);
													if (DepositAccount.CHECKING_SECTION.equals(frontendAccount
															.getAccountSection())) {
														athmEnrollBean.setPlasticAcctType("1");
													} else if (DepositAccount.SAVINGS_SECTION.equals(frontendAccount
															.getAccountSection())) {
														athmEnrollBean.setPlasticAcctType("2");
													}
													athmEnrollBean.setPlasticAccountNumber(frontendAccount
															.getAccountNumber());
													athmEnrollBean.setPlasticNumber(athmEnrollBean.getPlasticNum());
													plasticMatch = true;
													break;
												}
											}

										} else {// notActive
											break;
										}
									}
								}
								break;
							}
						}
					}

					if (!plasticMatch) {
						if (sectionBlockValidation(userSession, request))
							athmEnrollBean.setSectionAutoBlock(true);
						else {
							errors.rejectValue("error", "athmobile.portal.box.enroll.step3.expdate.invalid",
									"Invalid ATH card number or expiration date");
							
							athmEnrollBean.setErrorFound(true);
							athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.apps.enroll.step3.expdate.invalid", null, locale));
						}
						// Reset fields
						athmEnrollBean.setPlasticNum(null);
						athmEnrollBean.setPlasticExpDateMonth("0");
						athmEnrollBean.setPlasticExpDateYear("0");
					}
				}
			}

		} else if (page == 4) // Username and password
								// ----------------------------------------------
		{
			boolean makeLogin = false;
			boolean enrolled = false;
			String email = null;
			String dateOfBirth = null;
			String firstName = userSession.getCustomerProfile().getFirstName().trim();
			String lastName = userSession.getCustomerProfile().getLastName().trim();
			String secondLastName = (userSession.getCustomerProfile().getMaidenName() != null) ? userSession
					.getCustomerProfile().getMaidenName().trim() : "";

			if (athmEnrollBean.getAction().equals("login")) {
				if (GenericValidator.isBlankOrNull(athmEnrollBean.getUsernameLogin())) {
					errors.rejectValue("usernameLogin", "", "*");
					athmEnrollBean.setErrorFound(true);
				}
				if (GenericValidator.isBlankOrNull(athmEnrollBean.getPasswordLogin())) {
					errors.rejectValue("passwordLogin", "", "*");
					athmEnrollBean.setErrorFound(true);
				}

			} else {
				if (GenericValidator.isBlankOrNull(athmEnrollBean.getUsernameEnroll())) {
					errors.rejectValue("usernameEnroll", "", "*");
					athmEnrollBean.setErrorFound(true);
				}
				if (GenericValidator.isBlankOrNull(athmEnrollBean.getPasswordEnroll())) {
					errors.rejectValue("passwordEnroll", "", "*");
					athmEnrollBean.setErrorFound(true);
				}
				if (GenericValidator.isBlankOrNull(athmEnrollBean.getPasswordConfirm())) {
					errors.rejectValue("passwordConfirm", "", "*");
					athmEnrollBean.setErrorFound(true);
				}
				if (GenericValidator.isBlankOrNull(athmEnrollBean.getTermsConfirmation())
						|| !athmEnrollBean.getTermsConfirmation().equalsIgnoreCase("Y")) {
					errors.rejectValue("termsConfirmation", "", "*");
					athmEnrollBean.setErrorFound(true);
				}
				if (!athmEnrollBean.getPasswordConfirm().equals(athmEnrollBean.getPasswordEnroll()) && !athmEnrollBean.isErrorFound()) {
					errors.rejectValue("passwordEnroll", "", "*");
					errors.rejectValue("error", "athmobile.portal.box.enroll.step4.password.confirm.invalid",
							"*Required");
					athmEnrollBean.setErrorFound(true);
					return;
				}
			}
			// Enroll user --------
			if (athmEnrollBean.isErrorFound()) {
				errors.rejectValue("error", "athmobile.portal.box.enroll.required2", "*Required");
				return;
			} else {

				if (userSession.getCustomerProfile().getBirthDate() != null) {
					SimpleDateFormat yearMonthDayFormat = new SimpleDateFormat("yyyyMMdd");

					dateOfBirth = yearMonthDayFormat.format(userSession.getCustomerProfile().getBirthDate());
				}

				if (userSession.getCustomerProfile().getEmail() != null
						&& userSession.getCustomerProfile().getEmail().trim().length() > 0) {
					email = userSession.getCustomerProfile().getEmail().trim();
				}

			}

			if (athmEnrollBean.getAction().equals("enroll")) {
				AthmStatusReq enrollmentRequest = new AthmStatusReq();
				enrollmentRequest.setFiID(fiId);
				enrollmentRequest.setFiToken(fiToken);
				enrollmentRequest
						.setPhoneNumber(AthmUtils.getStringNumericPhoneNumber(athmEnrollBean.getPhoneNumber()));
				enrollmentRequest.setServiceProvider(getPhoneProviderId(athmEnrollBean.getPhoneProvider()));
				enrollmentRequest.setPlasticNumber(athmEnrollBean.getPlasticNumber());
				enrollmentRequest.setExpDate(athmEnrollBean.getPlasticExpDate());
				enrollmentRequest.setAccSection(athmEnrollBean.getPlasticAcctType());
				enrollmentRequest.setUserId(athmEnrollBean.getUsernameEnroll());
				enrollmentRequest.setPassword(athmEnrollBean.getPasswordEnroll());
				enrollmentRequest.setFirstName(firstName);
				enrollmentRequest.setLastName(lastName);
				enrollmentRequest.setSecondLastName(secondLastName);
				enrollmentRequest.setDateOfBirth(dateOfBirth);
				enrollmentRequest.setEmail(email);

				// to be used in Login
				athmEnrollBean.setUsernameLogin(athmEnrollBean.getUsernameEnroll());
				athmEnrollBean.setPasswordLogin(athmEnrollBean.getPasswordEnroll());

				enrollmentRequest.setLanguage(language);

				AthmStatusResp enrollmentResp = customerService.enrollUser(enrollmentRequest);

				if (enrollmentResp != null && enrollmentResp.getResponseStatus().equals(ResponseStatus.ERROR)) {
					athmEnrollBean.setServiceDown(true);
					logger.error("Enrollment failed. ATH Movil service generated an exception");
					makeLogin = false;
					enrolled = false;
				} else if (enrollmentResp != null && enrollmentResp.getResponseStatus().equals(ResponseStatus.SUCCESS)
						&& "200".equals(enrollmentResp.getStatusCode())) // success
				{
					makeLogin = true;
					enrolled = true;
				} else {
					if (enrollmentResp != null && enrollmentResp.getResponseStatus().equals(ResponseStatus.SUCCESS)
							&& ("89".equals(enrollmentResp.getStatusCode()))) {
						errors.rejectValue("error", "athmobile.portal.box.enroll.step4.username.unavailable",
								"This username is not available.");
						athmEnrollBean.setErrorFound(true);
						athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.portal.box.enroll.step4.username.unavailable", null, locale));
					} else if (enrollmentResp != null
							&& enrollmentResp.getResponseStatus().equals(ResponseStatus.SUCCESS)
							&& ("91".equals(enrollmentResp.getStatusCode())
									|| "08".equals(enrollmentResp.getStatusCode()) || "04".equals(enrollmentResp
									.getStatusCode()))) {
						errors.rejectValue("error",
								"athmobile.portal.box.enroll.step4." + enrollmentResp.getStatusCode(),
								"There was an error in the enrollment");
						athmEnrollBean.setErrorFound(true);
						athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.portal.box.enroll.step4." + enrollmentResp.getStatusCode(), null, locale));

					} else {
						errors.rejectValue("error", "athmovil.account.validation.failed", "Unable to validate account");
						athmEnrollBean.setErrorFound(true);
						athmEnrollBean.setErrorMessage(messageSource.getMessage("athmovil.account.validation.failed", null, locale));
					}
					makeLogin = false;
				}

			} else {
				makeLogin = true;
			}

			if (makeLogin) {
				AthmLoginReq athmLoginReq = new AthmLoginReq();
				athmLoginReq.setUserID(athmEnrollBean.getUsernameLogin());
				athmLoginReq.setPassword(athmEnrollBean.getPasswordLogin());
				athmLoginReq.setFiID(fiId);
				athmLoginReq.setFiToken(fiToken);
				athmLoginReq.setPhone(AthmUtils.getStringNumericPhoneNumber(athmEnrollBean.getPhoneNumber()));

				AthmLoginResp loginResponse = userSession.getCustomerService().login(athmLoginReq);

				if (athmEnrollBean.getAction().equals("enroll") && !enrolled) {
					errors.rejectValue("error", "athmobile.portal.box.enroll.step4.enrollmentLoginerror",
							"There was an error in the process.  Please, try again later.");
					athmEnrollBean.setErrorFound(true);
					athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.portal.box.enroll.step4.enrollmentLoginerror", null, locale));
					athmEnrollBean.setEnrollmentLoginError(true);
				} else if (loginResponse != null && loginResponse.getResponseStatus() == ResponseStatus.ERROR) {
					athmEnrollBean.setServiceDown(true);
					logger.error("Error in login process. ATH Movil service generated an exception");
				} else if (loginResponse != null && loginResponse.getResponseStatus().equals(ResponseStatus.SUCCESS)
						&& "200".equals(loginResponse.getStatusCode()) && loginResponse.getUserID() != null) {

					if (customerService.athmUserIdExists(loginResponse.getUserID())) {
						errors.rejectValue("error", "athmobile.portal.box.enroll.step4.duplicate",
								"These credentials have already been used with another Mi Banco profile.");
						athmEnrollBean.setErrorFound(true);
						athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.portal.box.enroll.step4.duplicate", null, locale));
					} else {
						if (!GenericValidator.isBlankOrNull(athmEnrollBean.getPlasticNumber())
								&& !GenericValidator.isBlankOrNull(athmEnrollBean.getPlasticExpDate())
								&& !GenericValidator.isBlankOrNull(athmEnrollBean.getPlasticAccountNumber())
								&& !GenericValidator.isBlankOrNull(athmEnrollBean.getPlasticAcctType())
								&& !GenericValidator.isBlankOrNull(loginResponse.getUserID())
								&& !GenericValidator.isBlankOrNull(athmEnrollBean.getPhoneNumber())) {
							// setEntitlement
							ATHMovilEntitlement entitlement = new ATHMovilEntitlement("", "",
									athmEnrollBean.getPlasticNumber(), athmEnrollBean.getPlasticExpDate(),
									athmEnrollBean.getPlasticAccountNumber(), athmEnrollBean.getPlasticAcctType(),
									loginResponse.getUserID(), AthmUtils.getStringNumericPhoneNumber(athmEnrollBean
											.getPhoneNumber()), EntitlementStatus.ENABLED);

							customerService.getCustomerProfileService().addEntitlement(
									userSession.getCustomerProfile(), entitlement.getCustomerEntitlement());
							
							// Update primary account
							AthmStatusReq updateAcctReq = new AthmStatusReq();
							updateAcctReq.setAccSection(entitlement.getPlasticAccountType());
							updateAcctReq.setExpDate(entitlement.getPlasticExpirationDate());
							updateAcctReq.setFiID(fiId);
							updateAcctReq.setFiToken(fiToken);
							updateAcctReq.setPlasticNumber(entitlement.getPlasticNumber());
							updateAcctReq.setUserId(entitlement.getUserId());
							customerService.changePrimaryCard(updateAcctReq);
							
							boolean userIdSaved = customerService.saveAthmUserId(userSession.getCustomerProfile()
									.getProfileId(), loginResponse.getUserID());
							if (!userIdSaved) {
								errors.rejectValue("error", "athmobile.portal.box.enroll.step4.enrollmentLoginerror",
										"There was an error in the process. Please, try again later.");
								athmEnrollBean.setErrorFound(true);
								athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.portal.box.enroll.step4.enrollmentLoginerror", null, locale));
							}
						} else {
							logger.error("Could not create entitlement ATHM to user: "
									+ userSession.getCustomerProfile().getUsername());
						}
					}
				} else if (loginResponse != null && ("04".equals(loginResponse.getStatusCode()) // Invalid
																								// Password
						|| "05".equals(loginResponse.getStatusCode()) // Inactive
																		// customer
				|| "07".equals(loginResponse.getStatusCode()))) {
					errors.rejectValue("error", "athmobile.portal.box.enroll.step4.invalid",
							"Invalid username or password"); // Invalid username
																// or password
					athmEnrollBean.setErrorFound(true);
					athmEnrollBean.setErrorMessage(messageSource.getMessage("athmobile.portal.box.enroll.step4.invalid", null, locale));
				} else if (loginResponse != null && "06".equals(loginResponse.getStatusCode())) {

					errors.rejectValue("error", "athmovil.account.blocked", "Access blocked");
					athmEnrollBean.setErrorMessage(messageSource.getMessage("athmovil.account.blocked", null, locale));
					athmEnrollBean.setErrorFound(true);
				} else {
					errors.rejectValue("error", "athmovil.account.validation.failed", "Unable to validate account");
					athmEnrollBean.setErrorMessage(messageSource.getMessage("athmovil.account.validation.failed", null, locale));
					athmEnrollBean.setErrorFound(true);
				}

			}
			if (errors.getErrorCount() > 0) {
				userSession.getCustomerService().logAthmEnrollmentError(
						Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));
			} else {
				userSession.getCustomerService().logAthmEnrollment(
						Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));
			}
		}
	}

	@Override
	protected int getTargetPage(HttpServletRequest request, Object command, Errors errors, int currentPage) {
		int target = super.getTargetPage(request, command, errors, currentPage);

		boolean updateStep = target > currentPage && !errors.hasErrors();

		ATHMEnrollBean athmEnrollBean = (ATHMEnrollBean) command;

		if (athmEnrollBean != null) {

			switch (currentPage) {
			case 0:
				if (AthmUtils.getNumericPhoneNumber(athmEnrollBean.getPhoneNumber()) != null) {
					if (athmEnrollBean.isProfileExists())
						target = 3;
				}

				break;

			case 1:
				if (athmEnrollBean.getPinCount() >= 3) {
					target = 4;
				}

				break;

			case 2:
				if (athmEnrollBean.getValidationCount() >= 3) {
					target = 4;
				}

				break;
			}
		}

		if (updateStep)
			athmEnrollBean.setStep(athmEnrollBean.getStep() + 1);

		return target;
	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		Map<String, Object> model = new HashMap<>();

		model.put("page", 5);

		return new ModelAndView("athmenroll", model);
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		return new ModelAndView("redirect:athmsendmoney");
	}

	private void loadTermsData(HttpServletRequest request, Object command, Map<String, Object> model) {
		Map<String, String> termsMap = new LinkedHashMap<>();

		termsMap.put("Y", "");

		model.put("termsconfirmation", termsMap);
	}

	private void loadPhoneProviderData(HttpServletRequest request, Object command, Map<String, Object> model) {
		Map<String, String> phoneProvidersMap = new LinkedHashMap<>();
    ATHMEnrollBean athmEnrollBean = (ATHMEnrollBean) command;

    if(athmEnrollBean.isMobileApp()){
      phoneProvidersMap.put("1", "Claro");
      phoneProvidersMap.put("2", "AT&T");
      phoneProvidersMap.put("3", "Sprint");
      phoneProvidersMap.put("4", "T-Mobile");
      phoneProvidersMap.put("5", "Open Mobile");
    }
    else {
    	
      if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
        phoneProvidersMap.put("none", "Select one");
      else
        phoneProvidersMap.put("none", "Selecciona uno");
        
      phoneProvidersMap.put("1", "Claro");
      phoneProvidersMap.put("2", "AT&amp;T");
      phoneProvidersMap.put("3", "Sprint");
      phoneProvidersMap.put("4", "T-Mobile");
      phoneProvidersMap.put("5", "Open Mobile");
    }
		model.put("phoneproviders", phoneProvidersMap);
    
	}

	private long getPhoneProviderId(String provider) {
		if (provider.equals("1"))
			return 1;

		else if (provider.equals("2"))
			return 2;

		else if (provider.equals("3"))
			return 4;

		else if (provider.equals("4"))
			return 5;

		else if (provider.equals("5"))
			return 6;
		return 0;
	}

	private void loadAccountsData(HttpServletRequest request, Object command, Map<String, Object> model) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		ATHMEnrollBean athmEnrollBean = (ATHMEnrollBean) command;
		List<TVAccountTransferSource> detailedAccounts = new LinkedList<TVAccountTransferSource>();

		String language = "sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "es" : Utils.getRSALanguage(request);

		Map<String, String> accountsMap = new LinkedHashMap<>();

		if (language.equalsIgnoreCase("en")) {
			accountsMap.put("none", "Select an account");
		} else {
			accountsMap.put("none", "Selecciona una cuenta");
		}

		List<TVAccountTransferSource> cardPlasticAccountList = userSession.getAthmAccounts();

		if (cardPlasticAccountList != null) {

			for (Iterator<TVAccountTransferSource> iterator = cardPlasticAccountList.iterator(); iterator.hasNext();) {
				TVAccountTransferSource frontendAccount = (TVAccountTransferSource) iterator.next();

				Map<String, String> accountText = AthmUtils.getAccountText(frontendAccount.getAccount());

				if (accountText != null){
					accountsMap.put(frontendAccount.getFrontEndId(), accountText.get("text"));
				}
		        if(athmEnrollBean.isMobileApp()) {
		        	detailedAccounts.add(frontendAccount);
		        }
        
			}
		}
    
    if(athmEnrollBean.isMobileApp())
      model.put("detailedaccounts", detailedAccounts);
		
    model.put("accounts", accountsMap);
	}

	private void loadExpirationDateData(HttpServletRequest request, Object command, Map<String, Object> model) {
		ATHMEnrollBean athmEnrollBean = (ATHMEnrollBean) command;
		Map<String, String> months = new LinkedHashMap<>();
		Map<String, String> years = new LinkedHashMap<>();

		int currentYear = Calendar.getInstance().get(Calendar.YEAR);

		String language = "sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "es" : Utils.getRSALanguage(request);
		Locale locale = new Locale(language);
		
		String monthOption;
		String yearOption;
		
		if (athmEnrollBean.isMobileApp()) {
			monthOption = messageSource.getMessage("athmobile.apps.enroll.month", null, locale);
			yearOption = messageSource.getMessage("athmobile.apps.enroll.year", null, locale);
		} else {
			monthOption = "Month";
			yearOption = "Year";
			if (!language.equalsIgnoreCase("en")) {
				monthOption = "Mes";
				yearOption = "A&ntilde;o";
			}
		}

		months.put("0", monthOption);
		years.put("0", yearOption);
		for (int i = 1; i <= 12; i++) {
			if (i < 10)
				months.put(String.valueOf("0" + i), ("0" + i));
			else
				months.put(String.valueOf(i), String.valueOf(i));
		}

		int maxYears = 10;
		if (!GenericValidator.isBlankOrNull(this.getMaxExpDateYears()))
			maxYears = Integer.parseInt(this.getMaxExpDateYears());

		for (int i = 0; i <= maxYears; i++)
			years.put(String.valueOf(currentYear + i), String.valueOf(currentYear + i));

		model.put("expDateMonths", months);
		model.put("expDateYears", years);
	}

	private void generateCall(ATHMEnrollBean bean, Map<String, Object> model) {
		AthmGenerateCallReq generateCallReq = new AthmGenerateCallReq();
		generateCallReq.setFiID(fiId);
		generateCallReq.setFiToken(fiToken);
		generateCallReq.setPhoneNumber(AthmUtils.getStringNumericPhoneNumber(bean.getPhoneNumber()));
		AthmGenerateCallResp generateCallResp = customerService.generateCall(generateCallReq);

		if (generateCallResp != null) {
			bean.setPhoneValidationId(generateCallResp.getPhoneValidationID());
			String code = generateCallResp.getRandomCode();
			if (bean.isMobileApp() && !GenericValidator.isBlankOrNull(code)){
				bean.setRandomCode(code.substring(0, 3)+ "-" +code.substring(3, code.length()));
			} else {
				bean.setRandomCode(code);
			}
		
		}

		if (generateCallResp != null && generateCallResp.getResponseStatus().equals(ResponseStatus.SUCCESS)
				&& "200".equals(generateCallResp.getStatusCode()))
			model.put("code", bean.getRandomCode());
		else {
			logger.error("Generate call failed. ATH Movil call generated an error");
			model.put("serviceDown", true);
		}
		model.put("validationType", bean.getValidationType());
	}

	public boolean sectionBlockValidation(UserSession userSession, HttpServletRequest request) {
		int maxTries = 3; // Default max tries
		String regex = "\\d+";
		if (getEnrollmentMaxTries() != null && getEnrollmentMaxTries().matches(regex))
			maxTries = Integer.parseInt(getEnrollmentMaxTries());

		return userSession.getCustomerService().evaluateAthmSectionBlock(userSession.getCustomerProfile(), maxTries);
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getFiId() {
		return fiId;
	}

	public void setFiId(String fiId) {
		this.fiId = fiId;
	}

	public String getFiToken() {
		return fiToken;
	}

	public void setFiToken(String fiToken) {
		this.fiToken = fiToken;
	}

	public String getEnrollmentMaxTries() {
		return enrollmentMaxTries;
	}

	public void setEnrollmentMaxTries(String enrollmentMaxTries) {
		this.enrollmentMaxTries = enrollmentMaxTries;
	}

	/**
	 * @return the maxExpDateYears
	 */
	public String getMaxExpDateYears() {
		return maxExpDateYears;
	}

	/**
	 * @param maxExpDateYears
	 *            the maxExpDateYears to set
	 */
	public void setMaxExpDateYears(String maxExpDateYears) {
		this.maxExpDateYears = maxExpDateYears;
	}
	
	
	/**
	 * @return the allowedCardsPrefix
	 */
	public String getAllowedCardsPrefix() {
		return allowedCardsPrefix;
	}

	/**
	 * @param allowedCardsPrefix the allowedCardsPrefix to set
	 */
	public void setAllowedCardsPrefix(String allowedCardsPrefix) {
		this.allowedCardsPrefix = allowedCardsPrefix;
	}
	
	/**
	 * @return the allowedAthmCardStatus
	 */
	public String getAllowedAthmCardStatus() {
		return allowedAthmCardStatus;
	}

	/**
	 * @param allowedAthmCardStatus the allowedAthmCardStatus to set
	 */
	public void setAllowedAthmCardStatus(String allowedAthmCardStatus) {
		this.allowedAthmCardStatus = allowedAthmCardStatus;
	}

}
