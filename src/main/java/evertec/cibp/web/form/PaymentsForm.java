package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.factory.TVAccountFactory;
import bppr.tv.beans.accounts.records.MortgageAccountRecord;
import bppr.tv.beans.accounts.types.DepositAccount;
import bppr.tv.beans.accounts.types.LoanAccount;
import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.base.AbstractStatement;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerPayeePreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.BillPayEntitlement;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.holiday.Holiday;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.FavoritePaymentComparator;
import evertec.cibp.core.models.payment.TVAccountFavoritePayment;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.BankingDaysUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.vs.beans.payments.Payee;
import evertec.cibp.web.GlobalEntitlementAwareController;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.MultiplePaymentBean;
import evertec.cibp.web.form.bean.PaymentBean;
import evertec.cibp.web.form.bean.PaymentsListBean;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.PaymentSortbyAccount;
import evertec.cibp.web.utils.Utils;
import evertec.dp.service.tvstatement.CreditCardStatement_Type;
import evertec.otherservices.beans.UserAccount;

/**
 * PaymentsForm
 * @author Evertec
 * @see PaymentsForm
 * @version 1.0
 */
public class PaymentsForm extends AbstractWizardFormController
		implements GlobalEntitlementAwareController, MessageSourceAware {

	/**
	 * cancel view
	 */
	private String cancelView; //cancel view
	/**
	 * Logget instance
	 */
	protected final Log logger = LogFactory.getLog(getClass()); // logger instance
	/**
	 * customer services core
	 */
	protected CustomerServices customerService; // customerService
	/**
	 * message Source user services
	 */
	private MessageSource messageSource; // messageSource

	/**
	 * telenominaID
	 */
	private String telenominaID; // telenominaID
	/**
	 * balanceDelay
	 */
	private String balanceDelay = ""; // balanceDelay
	/**
	 * evendorId
	 */
	private String evendorId; // evendorId
	
	public String getEvendorId() {
		return evendorId;
	}
	
	public void setEvendorId(String evendorId) {
		this.evendorId = evendorId;
	}

	public String getTelenominaID() {
		return telenominaID;
	}

	public void setTelenominaID(String telenominaID) {
		this.telenominaID = telenominaID;
	}

	public PaymentsForm() {
		super();
		setCommandClass(PaymentsListBean.class);
		setCommandName("paymentlist");
		setPages(new String[] { "makePayment", "makePayment", "makePayment" });
	}

	/**
	 * processFinish
	 * @param request user request
	 * @param response response
	 * @param paymentlist list of payments
	 * @param error error description
	 * @return ModelAndView
	 */
	protected ModelAndView processFinish (HttpServletRequest request, HttpServletResponse response, Object paymentlist,
			BindException error) throws Exception {

		UserSession userSession = KiuwanUtils.cast(UserSession.class, WebUtils.getSessionAttribute(request, "userSession")); // usersession
		final CustomerProfile customer = userSession.getCustomerProfile(); // customer Profile
		final CustomerServices customerService = userSession.getCustomerService(); // customer services
		List<TVPayment> confirmPayments = new LinkedList<TVPayment>();
		List<TVPayment> errorPayments = new LinkedList<TVPayment>();
		final List<TVFrontEndPayee> payees = userSession.getPayessBasicEbpp(true); // list payees
		BigDecimal totalcomplete = new BigDecimal(0.0d); // total complete
		BigDecimal totalfail = new BigDecimal(0.0d); // total fail

		final PaymentsListBean paymentsList = KiuwanUtils.cast(PaymentsListBean.class, paymentlist); // payment list
		PaymentSortbyAccount paymentsort = new PaymentSortbyAccount(); // sort of payments
		final Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsPaymentFrom(); // account from
		final List<PaymentBean> payments = paymentsList.getPayments(); // payments
		Map<String, Object> model = new HashMap<String, Object>(0); // return model view
		final boolean realTimeNotificationsClaro = StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO); //flag
		
		model.put("realTimeNotificationsClaro", realTimeNotificationsClaro);
		model.put("isEvendorActivated", StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR));
		model.put("telenominaID", telenominaID);
		if (payments != null)
			for (Iterator<PaymentBean> iterator = payments.iterator(); iterator.hasNext();) {
				PaymentBean name = (PaymentBean) iterator.next(); // name

				if (name.getAmount() == null)
					continue;
				String accountfrom = "";
				if (!paymentsList.getSingleAccountFrom().equalsIgnoreCase("none")
						&& !paymentsList.getSingleAccountFrom().equalsIgnoreCase("multiple")) {
					accountfrom = paymentsList.getSingleAccountFrom();
				} else {
					accountfrom = name.getAccountFrom();
				}

				TVAccountTransferSource sou = null;

				// If it is a PIF account, create it
				if (accountfrom.startsWith("PIF")) {
					String externalAccountId = accountfrom.substring(3);
					PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
					if (externalAccount != null) {
						BankInformation bankInformation = new BankInformation();
						bankInformation.setBankId(externalAccount.getRoutingNumber());
						bankInformation.setBankName(externalAccount.getBankName());
						CustomerAccountPreferences pref = new CustomerAccountPreferences();
						TVAccountFactory accountFactory = new TVAccountFactory();
						Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
								externalAccount.getAccountNumber());
						sourceAccount.setAccountID(externalAccount.getAccountNumber());
						sourceAccount.setSubType(Character.toString(externalAccount.getAccountType())); // Checking/Savings/Reserve
						((DepositAccount) sourceAccount)
								.setSectionIndicator(Character.toString(externalAccount.getAccountType()));
						TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
						fromAccount.setCustomerAccountPreferences(pref);
						fromAccount.setNickname(externalAccount.getNickname());
						fromAccount.setHide(externalAccount.getShow() == '0');
						fromAccount.getTVAccount().setBankInformation(bankInformation);
						sou = new TVAccountTransferSource(fromAccount);
					}
				} else {
					sou = Utils.getAccountSource(accountfrom, accountsFrom);
				}

				TVFrontEndPayee tar = Utils.getPayeebyId(name.getPayeeid(), payees);
				if (sou == null || tar == null)
					continue;

				BigDecimal amount = name.getAmount();
				Calendar cal = Calendar.getInstance();
				cal.setTime(name.getEffectiveDate());

				TVPayment payment = null;
				String recurrent = name.getRecurrent();
				String infinite = name.getInfinite();

				if (recurrent != null && recurrent.equalsIgnoreCase("recurrent")) {
					Integer inst = name.getInstances();

					if (infinite != null && infinite.equalsIgnoreCase("infinite")) {
						inst = 999;
					}
					// else infinite == byinstances
					payment = new TVPayment(amount, name.getFrequency(), inst, cal, sou, tar);
				} else
					payment = new TVPayment(amount, Frequency.MANUALLY, 0, cal, sou, tar);
				// CIBP-2548 JOCAMPO
				if (payment.getPayee().getGlobalPayeeIdString() != null
						&& payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
					payment.getPayee().setTelenomina(true);
				} else {
					payment.getPayee().setTelenomina(false);
				}
				// mpicado, CIBP-1799
				PaymentResponse res = PaymentsServiceModule.getInstance().sendPayments(customer, userSession, payment,
						Utils.generateCustomerInteractionEvent(request), customerService);
				payment = res.getPayment();
				payment.getTVPayment().setEffectiveDate(cal);

				String estimatedDate=Utils.getEstimateDateAccordingToPaymentType(tar,customerService,cal,recurrent,Utils.getRSALanguage(request));
				payment.setEstimateDate(estimatedDate);

				if (res.getStatus() == ResultStatus.SUCCESS) {
					confirmPayments.add(payment);
					totalcomplete = totalcomplete.add(payment.getAmount());

					// Incidente de actualizar balance en tiempo real
					// (MBSFE-137)
					// Se elimina validación de pagos que sean "SAME DAY"

					TVFrontendAccount frontendAccountToUpdate = Utils
							.getAccountByAccountNumber(payment.getSourceAccountNumber(), userSession.getAccounts());
					if (frontendAccountToUpdate.getSubtype() != null
							&& frontendAccountToUpdate.getSubtype().equals("IDA")) {
						List<TVFrontendAccount> frontEndAccounts2Update = new LinkedList<TVFrontendAccount>();
						frontEndAccounts2Update.add(frontendAccountToUpdate);
						userSession.updateAccountBalance(frontEndAccounts2Update, balanceDelay);
					}

					// The EBPP2 user id is not yet in session; add it
					if (!userSession.isEbpp2UserIdLookedUp()) {
						EbppEntitlement ent = null;
						if (customer.getEntitlementByType(EntitlementType.EBILLS) != null) {
							ent = new EbppEntitlement(customer.getEntitlementByType(EntitlementType.EBILLS));

							// If the username is blank, we need to look in the
							// current EBPP accounts ONLY
							if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
								userSession.setEbpp2UserId("");
							} else {
								userSession.setEbpp2UserId(ent.getUsername());
							}
							userSession.setEbpp2UserIdLookedUp(true);
						}
					}

					// The user has an EBPP2 ID
					if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

						// Find the account in the user accounts list
						List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

						// Loop through all the accounts and send the update
						// payment message if necessary
						for (Iterator<UserAccount> iterator2 = ebpp2UserAccounts.iterator(); iterator2.hasNext();) {
							UserAccount userAccount = (UserAccount) iterator2.next();

							if (userAccount.getAccountNumber().replaceFirst("^0+(?!$)", "")
									.equals(tar.getBillingAccount().replaceFirst("^0+(?!$)", ""))) {

								if (!userAccount.isLastEbillPaid()) {
									customerService.updateAccountPayment(userSession.getEbpp2UserId(),
											userAccount.getBillerAccountID());
									// Alizano hide new ebill message when
									// payment is success Fix Incident 1537231
									res.getPayment().getPayee().setHasNewEbill(false);
									if (res.getPayment().getPayee() != null
											&& res.getPayment().getPayee().getEbpp2UserAccount() != null) {
										res.getPayment().getPayee().getEbpp2UserAccount().setLastEbillPaid(true);
									}
									// End Fix
								}
								break;
							}
						}
					}
					
					// Mark is the Ebills PDF is not view
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
						if (tar.getBillingAccount() != null) {
							TVFrontendAccount cca = Utils.getAccountByAccountNumber(tar.getBillingAccount(),
									userSession.getAccounts());
							if(cca != null){
								List<AbstractStatement> ebills = Utils.statementsByAccount(cca, userSession, request);
								if (ebills != null && ebills.size() > 1) {
									Utils.markPdfViewed(ebills.get(1), cca, userSession);
									
									int pid = tar.getGlobalPayeeId();
									
									if(pid == 359 || pid == 517 || pid == 518 || pid == 1983 || pid == 223){
									
										CustomerPayeePreferences customerPayeePreferences = tar.getCustomerPayeePreferences();
										
										SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
										customerPayeePreferences.setLastPaymentDate(format.format(ebills.get(1).getEndDate()));
										
										tar.setCustomerPayeePreferences(customerPayeePreferences);
										customerService.modifyPayeeFrequentPreference(userSession.getCustomerProfile(), userSession.getBankingSession(), tar, tar.getFavorite());
								
									}
								}
							}					
							
						}
					} // End EBILLS_STATEMENTS					
				} else {
					String er = getTelepagoErrorDescriptor(res.getErrorEBA(),
							Utils.getRSALanguage(request).toLowerCase());
					if (er == null || er.equals("")) {
						payment.setPaymentMessage(res.getErrorMessage());
					} else {
						payment.setPaymentMessage(er + " - " + res.getErrorEBA());
					}
					errorPayments.add(payment);
					totalfail = totalfail.add(payment.getAmount());

				}
			}
		if (paymentsList.getFavorites() != null)
			for (Iterator<PaymentBean> iterator = paymentsList.getFavorites().iterator(); iterator.hasNext();) {
				PaymentBean name = iterator.next();
				if (name.getAmount() == null)
					continue;

				String accountfrom = "";
				if (!paymentsList.getSingleAccountFrom().equalsIgnoreCase("none")
						&& !paymentsList.getSingleAccountFrom().equalsIgnoreCase("multiple")) {
					accountfrom = paymentsList.getSingleAccountFrom();
				} else {
					accountfrom = name.getAccountFrom();
				}

				TVAccountTransferSource sou = null;

				// If it is a PIF account, create it
				if (accountfrom.startsWith("PIF")) {
					String externalAccountId = accountfrom.substring(3);
					PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
					if (externalAccount != null) {
						BankInformation bankInformation = new BankInformation();
						bankInformation.setBankId(externalAccount.getRoutingNumber());
						bankInformation.setBankName(externalAccount.getBankName());
						CustomerAccountPreferences pref = new CustomerAccountPreferences();
						TVAccountFactory accountFactory = new TVAccountFactory();
						Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
								externalAccount.getAccountNumber());
						sourceAccount.setAccountID(externalAccount.getAccountNumber());
						sourceAccount.setSubType(Character.toString(externalAccount.getAccountType())); // Checking/Savings/Reserve
						((DepositAccount) sourceAccount)
								.setSectionIndicator(Character.toString(externalAccount.getAccountType()));
						TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
						fromAccount.setCustomerAccountPreferences(pref);
						fromAccount.setNickname(externalAccount.getNickname());
						fromAccount.setHide(externalAccount.getShow() == '0');
						fromAccount.getTVAccount().setBankInformation(bankInformation);
						sou = new TVAccountTransferSource(fromAccount);
					}
				} else {
					sou = Utils.getAccountSource(accountfrom, accountsFrom);
				}

				TVFrontEndPayee tar = Utils.getPayeebyId(name.getPayeeid(), payees);
				if (sou == null || tar == null)
					continue;

				BigDecimal amount = name.getAmount();
				Calendar cal = Calendar.getInstance();
				cal.setTime(name.getEffectiveDate());

				TVPayment payment = null;

				String recurrent = name.getRecurrent();
				String infinite = name.getInfinite();

				if (recurrent != null && recurrent.equalsIgnoreCase("recurrent")) {
					Integer inst = name.getInstances();

					if (infinite != null && infinite.equalsIgnoreCase("infinite")) {
						inst = 999;
					}
					payment = new TVPayment(amount, name.getFrequency(), inst, cal, sou, tar);
				} else
					payment = new TVPayment(amount, Frequency.MANUALLY, 0, cal, sou, tar);

				// CIBP-2548 JOCAMPO
				if (payment.getPayee().getGlobalPayeeIdString() != null
						&& payment.getPayee().getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
					payment.getPayee().setTelenomina(true);
				} else {
					payment.getPayee().setTelenomina(false);
				}
				// mpicado, CIBP-1799
				PaymentResponse res = PaymentsServiceModule.getInstance().sendPayments(customer, userSession, payment,
						Utils.generateCustomerInteractionEvent(request), customerService);
				payment = res.getPayment();
				payment.getTVPayment().setEffectiveDate(cal);

				String estimatedDate=Utils.getEstimateDateAccordingToPaymentType(tar,customerService,cal,recurrent,Utils.getRSALanguage(request));
				payment.setEstimateDate(estimatedDate);

				if (res.getStatus() == ResultStatus.SUCCESS) {
					if (res.getPayment().getPayee().isHasNewEbill()) {
						res.getPayment().getPayee().setHasNewEbill(false);
						if (res.getPayment().getPayee() != null
								&& res.getPayment().getPayee().getEbpp2UserAccount() != null) {
							customerService.updateAccountPayment(userSession.getEbpp2UserId(),
									res.getPayment().getPayee().getEbpp2UserAccount().getBillerAccountID());							
							res.getPayment().getPayee().getEbpp2UserAccount().setLastEbillPaid(true);
						}
						// End Fix
					}

					confirmPayments.add(payment);
					totalcomplete = totalcomplete.add(payment.getAmount());

					// Se elimina validación de pagos que sean "SAME DAY"

					TVFrontendAccount frontendAccountToUpdate = Utils
							.getAccountByAccountNumber(payment.getSourceAccountNumber(), userSession.getAccounts());
					if (frontendAccountToUpdate.getSubtype() != null
							&& frontendAccountToUpdate.getSubtype().equals("IDA")) {
						List<TVFrontendAccount> frontEndAccounts2Update = new LinkedList<TVFrontendAccount>();
						frontEndAccounts2Update.add(frontendAccountToUpdate);
						userSession.updateAccountBalance(frontEndAccounts2Update, balanceDelay);
					}

					if (this.getEBPPMessageSourceStatus() && customerService
							.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {

						// The EBPP2 user id is not yet in session; add it
						if (!userSession.isEbpp2UserIdLookedUp()) {
							EbppEntitlement ent = null;
							if (customer.getEntitlementByType(EntitlementType.EBILLS) != null) {
								ent = new EbppEntitlement(customer.getEntitlementByType(EntitlementType.EBILLS));

								// If the username is blank, we need to look in
								// the current EBPP accounts ONLY
								if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
									userSession.setEbpp2UserId("");
								} else {
									userSession.setEbpp2UserId(ent.getUsername());
								}
								userSession.setEbpp2UserIdLookedUp(true);
							}
						}

						// The user has an EBPP2 ID
						if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

							// Find the account in the user accounts list
							List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

							// Loop through all the accounts and send the update
							// payment message if necessary
							for (Iterator<UserAccount> iterator2 = ebpp2UserAccounts.iterator(); iterator2.hasNext();) {
								UserAccount userAccount = (UserAccount) iterator2.next();

								if (userAccount.getAccountNumber().replaceFirst("^0+(?!$)", "")
										.equals(tar.getBillingAccount().replaceFirst("^0+(?!$)", ""))) {

									if (!userAccount.isLastEbillPaid()) {
										customerService.updateAccountPayment(userSession.getEbpp2UserId(),
												userAccount.getBillerAccountID());
										// Alizano hide new ebill message when
										// payment is success Fix Incident
										// 1537231
										res.getPayment().getPayee().setHasNewEbill(false);
										if (res.getPayment().getPayee() != null
												&& res.getPayment().getPayee().getEbpp2UserAccount() != null) {
											res.getPayment().getPayee().getEbpp2UserAccount().setLastEbillPaid(true);
										}
										// End Fix
									}
									break;
								}
							}
						}
						
						// Mark is the Ebills PDF is not view
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
							if (tar.getBillingAccount() != null) {
								TVFrontendAccount cca = Utils.getAccountByAccountNumber(tar.getBillingAccount(),
										userSession.getAccounts());
								List<AbstractStatement> ebills = Utils.statementsByAccount(cca, userSession, request);
								if (ebills != null && ebills.size() > 1) {
									Utils.markPdfViewed(ebills.get(1), cca, userSession);
									int pid = tar.getGlobalPayeeId();
									
									if(pid == 359 || pid == 517 || pid == 518 || pid == 1983 || pid == 223){
									
										CustomerPayeePreferences customerPayeePreferences = tar.getCustomerPayeePreferences();
																			
										SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
										customerPayeePreferences.setLastPaymentDate(format.format(ebills.get(1).getEndDate()));
										
										tar.setCustomerPayeePreferences(customerPayeePreferences);
										customerService.modifyPayeeFrequentPreference(userSession.getCustomerProfile(), userSession.getBankingSession(), tar, tar.getFavorite());
								
									}
								}
							}
						} // End EBILLS_STATEMENTS
					}
				} else {
					String er = getTelepagoErrorDescriptor(res.getErrorEBA(),
							Utils.getRSALanguage(request).toLowerCase());
					if (er == null || er.equals("")) {
						payment.setPaymentMessage(res.getErrorMessage());
					} else {
						payment.setPaymentMessage(er + " - " + res.getErrorEBA());
					}
					errorPayments.add(payment);
					totalfail = totalfail.add(payment.getAmount());
				}
			}

		for (Iterator<TVPayment> iterator = confirmPayments.iterator(); iterator.hasNext();) {
			TVPayment payment = (TVPayment) iterator.next();
			paymentsort.addPayment(payment);

		}
		for (Iterator<TVPayment> iterator = errorPayments.iterator(); iterator.hasNext();) {
			TVPayment payment = (TVPayment) iterator.next();
			paymentsort.addPayment(payment);

		}

		boolean hasalert = false;
		if (customer.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
			if (ent.getAlerts().contains(AlertType.PAYMENTS_EMAIL) || ent.getAlerts().contains(AlertType.PAYMENTS_SMS)
					|| ent.getAlerts().contains(AlertType.PAYMENTS_PUSH)) {
				hasalert = true;
			}

		}

		boolean errorpay = false;
		model.put("paymentsort", paymentsort.getElementMap());

		if (confirmPayments != null && confirmPayments.size() > 0) {
			model.put("confirmPayments", confirmPayments);
			model.put("totalConfirm", totalcomplete);
		}
		if (errorPayments != null && errorPayments.size() > 0) {
			model.put("errorPayments", errorPayments);
			model.put("totalFail", totalfail);
			errorpay = true;
		}
		if (!errorpay) {
			if (hasalert) {
				if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
					userSession.setStatusmessage(
							"Thanks! You will receive a confirmation of these transactions via email, text message and/or push notification.");
				else
					userSession.setStatusmessage(
							"&iexcl;Gracias! Recibir&aacute;s evidencia de estas transacciones por email, mensaje de texto y/o notificaci&oacuten.");
			}

			if (!hasalert) {
				if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
					userSession.setStatusmessage(
							"Thanks! - To receive evidence of payments by email, <a href=\"alerttypes\">click here</a>.");
				else
					userSession.setStatusmessage(
							"&iexcl;Gracias! - Para recibir evidencia de pagos por e-mail, <a href=\"alerttypes\">oprime aqu&iacute;</a>.");
			}
		} else {
			if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
				userSession.setStatusmessage("Please verify errors in payments");
			else
				userSession.setStatusmessage("Tienes errores en pagos");
		}

		if (hasalert) {
			customerService.sendPaymentConfirmation(customer, confirmPayments, errorPayments,
					userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
					userSession.getPreferredLanguage());
		}
		model.put("page", "2");
		userSession.resetPayees();
		return new ModelAndView("makePayment", model);
	}

	/**
	 * referenceData
	 * @param request user request type
	 * @param command user coomand
	 * @param errors erros result
	 * @param page page form
	 * @return Map<String, Object>
	 */
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		final UserSession userSession = KiuwanUtils.cast(UserSession.class, WebUtils.getSessionAttribute(request, "userSession")); //user
		
		if(userSession.getPendingResetPayerees() != null && userSession.getPendingResetPayerees()){
			userSession.resetPayerees();
			userSession.resetFavoritePayments();
			userSession.setPendingResetPayerees(Boolean.FALSE);
		}
		
		CustomerProfile customerProfile = userSession.getCustomerProfile(); // user customer services

		 // aqui establezco el valor de el MessageSource para que lo pueda usar
		 // el bean del core, me parece que podria hacerse accesando el context
		 // pero tuve algunos problemas al hacerlo.. mpicado CIBP-1799

		customerService.setEBPPStatus(this.getEBPPMessageSourceStatus());
		PaymentsListBean paymentsList = (PaymentsListBean) command; // user payment list

		Map<String, Object> model = new HashMap<String, Object>(0); // return model data for view
		final boolean realTimeNotificationsClaro = StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO); //flag
		final boolean isEvendorActivated = StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR); // Payment Evendor flag
		final boolean isPayrollActivated = StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL); // Payments Payroll flag
		
		model.put("telenominaID", telenominaID);
		model.put("realTimeNotificationsClaro", realTimeNotificationsClaro);
		model.put("isEvendorActivated", isEvendorActivated);
		
		model.put("page", page);

		Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsPaymentFrom();
		List<TVFrontEndPayee> payees = userSession.getPayessBasicEbpp(true);

		List<TVAccountFavoritePayment> favorites = userSession.getFavoritePayments();
		List<TVAccountFavoritePayment> favoritePayment = new ArrayList<TVAccountFavoritePayment>();
		List<TVAccountFavoritePayment> payrollPayments = new ArrayList<TVAccountFavoritePayment>();
		if (page == 0 || page == 1) {
			// Set holidays
			Map<String, Object> h = new LinkedHashMap<String, Object>();
			List<Holiday> holidays = customerService.getHolidays();
			if (holidays != null)
				for (Holiday holiday : holidays) {
					h.put(holiday.getId(),
							new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(holiday.getDate().getTime()));
				}
			model.put("holidays", h);

			if (favorites != null){
				Iterator<TVAccountFavoritePayment> iterator = favorites.iterator();
				while(iterator.hasNext()){
					TVAccountFavoritePayment name = (TVAccountFavoritePayment) iterator.next();
					if(name.getPendingPayments() != null){
						Iterator<TVPayment> iterator2 = name.getPendingPayments().iterator();
						while(iterator2.hasNext()){
							TVPayment pmt = (TVPayment) iterator2.next();
							TVFrontEndPayee py = null;
							if (pmt != null && pmt.getPayeeId() != null)
								py = Utils.getPayeebyPayeeId(pmt.getPayeeId(), payees);
							if (py != null && !py.getFavorite()) {
								iterator.remove();
								break;
							}
							pmt.setPayee(py);
						}
					}
				}
			}
			

			AccountMaskUtil mask = new AccountMaskUtil();
			NumberTool numberTool = new NumberTool();

			Map<String, String> accountsFromMap = new LinkedHashMap<String, String>();

			
			 // CIBP-2924 Si el cliente es comercial empezará a cargar información
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
				accountsFromMap.put("none", "Select an account");
			else
				accountsFromMap.put("none", "Selecciona una cuenta");
			for (Iterator<TVAccountTransferSource> iterator = accountsFrom.iterator(); iterator.hasNext();) {
				TVAccountTransferSource from = iterator.next();
				TVFrontendAccount frontFrom = from.getAccount();
				if (!frontFrom.getHide()) {
					if (frontFrom != null && frontFrom.getSubtype().equals("CCA")
							&& frontFrom.getAvailableBalance() != null)
						accountsFromMap.put(from.getFrontEndId(),
								StringUtils.abbreviate(frontFrom.getNickname(), 15) + " "
										+ mask.mask(frontFrom.getAccountNumber()) + " "
										+ numberTool.format("currency", frontFrom.getAvailableBalance())
										+ ((frontFrom.getAccountNumberSuffix() != null
												&& !frontFrom.getAccountNumberSuffix().equals(""))
														? " " + frontFrom.getAccountNumberSuffix() : ""));
					else if (frontFrom != null && frontFrom.getPortalBalance() != null)
						accountsFromMap.put(from.getFrontEndId(),
								StringUtils.abbreviate(frontFrom.getNickname(), 15) + " "
										+ mask.mask(frontFrom.getAccountNumber()) + " "
										+ numberTool.format("currency", frontFrom.getPortalBalance())
										+ ((frontFrom.getAccountNumberSuffix() != null
												&& !frontFrom.getAccountNumberSuffix().equals(""))
														? " " + frontFrom.getAccountNumberSuffix() : ""));
					else
						accountsFromMap.put(from.getFrontEndId(),
								StringUtils.abbreviate(frontFrom.getNickname(), 15) + " "
										+ mask.mask(frontFrom.getAccountNumber())
										+ ((frontFrom.getAccountNumberSuffix() != null
												&& !frontFrom.getAccountNumberSuffix().equals(""))
														? " " + frontFrom.getAccountNumberSuffix() : ""));
				}
			}

			// Post-Initial Funding accounts

			Map<String, String> payeeMap = new LinkedHashMap<>();
			Map<String, String> pifPayeeMap = new LinkedHashMap<>();

			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				payeeMap.put("none", "Select a payee");
				pifPayeeMap.put("none", "Select a payee");
			} else {
				payeeMap.put("none", "Selecciona un comercio");
				pifPayeeMap.put("none", "Selecciona un comercio");
			}

			if (payees != null)
				for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
					TVFrontEndPayee pay = (TVFrontEndPayee) iterator.next();

					int tp = pay.getGlobalPayeeId();

					if (isPayrollActivated) {
						if(isEvendorActivated){
							if (String.valueOf(tp).equalsIgnoreCase(telenominaID) ||
									String.valueOf(tp).equalsIgnoreCase(evendorId)) {
								TVAccountFavoritePayment payeeAux = new TVAccountFavoritePayment(pay);
								payeeAux.setFrontEndId(pay.getFrontEndId());
								payeeAux.getPayee().setTelenomina(true);
								payrollPayments.add(payeeAux);
							}
						}
						else{
							if (String.valueOf(tp).equalsIgnoreCase(telenominaID)) {
								TVAccountFavoritePayment payeeAux = new TVAccountFavoritePayment(pay);
								payeeAux.setFrontEndId(pay.getFrontEndId());
								payeeAux.getPayee().setTelenomina(true);
								payrollPayments.add(payeeAux);
							}
						}
					}

					if (tp == 288 || tp == 289 || tp == 4144 || tp == 290 || tp == 12682 || tp == 12453) {
						TVFrontendAccount reserveAct = Utils.getAccountByKey(pay.getBillingAccount().trim() + "|IDA|R",
								userSession.getAccounts());
						if (reserveAct != null && reserveAct.getPortalBalance() != null) {
							pay.setCurrentBalance(reserveAct.getPortalBalance());
						}
						if (pay.getCurrentBalance() != null) {
							payeeMap.put(pay.getFrontEndId(),
									StringUtils.abbreviate(pay.getNickname(), 15) + " "
											+ mask.mask(pay.getBillingAccount()) + "("
											+ numberTool.format("currency", pay.getCurrentBalance()) + ")");
							if (reserveAct != null) {
								if (customerService.accountValidForPIFTransfer(reserveAct))
									pifPayeeMap.put(pay.getFrontEndId(),
											StringUtils.abbreviate(pay.getNickname(), 15) + " "
													+ mask.mask(pay.getBillingAccount()) + "("
													+ numberTool.format("currency", pay.getCurrentBalance()) + ")");
							}
						} else {
							payeeMap.put(pay.getFrontEndId(), StringUtils.abbreviate(pay.getNickname(), 15) + " "
									+ mask.mask(pay.getBillingAccount()));
							if (reserveAct != null) {
								if (customerService.accountValidForPIFTransfer(reserveAct))
									pifPayeeMap.put(pay.getFrontEndId(), StringUtils.abbreviate(pay.getNickname(), 15)
											+ " " + mask.mask(pay.getBillingAccount()));
							}
						}
					} else {
						TVFrontendAccount reserveAct = Utils.getAccountByAccountNumber(pay.getBillingAccount().trim(),
								userSession.getAccounts());
						
						if (reserveAct != null) {
							
							pay.setCurrentBalance(getAmountDueInfo(reserveAct, userSession));
							
							final String MLA = "MLA"; //cadena con el account subtye
							if (reserveAct.getSubtype().equals(MLA)){
								pay.setShowAmountDueMessage(true);
							}else {
								pay.setShowAmountDueMessage(false);
							}
							
						}
						
						
						if (isPayrollActivated && (pay.getGlobalPayeeId() == Integer.valueOf(telenominaID) || pay.getPayeeName().contains("PAYROLL"))) {
							continue;
						}
						
						if (isEvendorActivated && (pay.getGlobalPayeeId() == Integer.valueOf(evendorId) || pay.getPayeeName().contains("VENDOR"))) {
							continue;
						}
						
						if (pay.getCurrentBalance() != null) {
							payeeMap.put(pay.getFrontEndId(),
									StringUtils.abbreviate(pay.getNickname(), 15) + " "
											+ mask.mask(pay.getBillingAccount()) + "("
											+ numberTool.format("currency", pay.getCurrentBalance()) + ")");
							if (reserveAct != null) {
								if (customerService.accountValidForPIFTransfer(reserveAct))
									pifPayeeMap.put(pay.getFrontEndId(),
											StringUtils.abbreviate(pay.getNickname(), 15) + " "
													+ mask.mask(pay.getBillingAccount()) + "("
													+ numberTool.format("currency", pay.getCurrentBalance()) + ")");
							} else {
								Payee payee = (Payee) pay.getBackendObject();
								if (customerService.telepagoIdValidForPIFTransfer(payee.getBillerNumber())) {
									pifPayeeMap.put(pay.getFrontEndId(),
											StringUtils.abbreviate(pay.getNickname(), 15) + " "
													+ mask.mask(pay.getBillingAccount()) + "("
													+ numberTool.format("currency", pay.getCurrentBalance()) + ")");
								}
							}
						} else {
							
							if(isPayrollActivated){
								if(userSession.getUserAgentType() == UserAgentType.WEB){
									if(isEvendorActivated){
										if(pay.getGlobalPayeeId() != Integer.valueOf(telenominaID) &&
												pay.getGlobalPayeeId() != Integer.valueOf(evendorId)){
											payeeMap.put(pay.getFrontEndId(), StringUtils.abbreviate(pay.getNickname(), 15) + " "
													+ mask.mask(pay.getBillingAccount()));
										}
									}
									else{
										if(pay.getGlobalPayeeId() != Integer.valueOf(telenominaID)){
											payeeMap.put(pay.getFrontEndId(), StringUtils.abbreviate(pay.getNickname(), 15) + " "
													+ mask.mask(pay.getBillingAccount()));
										}
									}
								}else{
									payeeMap.put(pay.getFrontEndId(), StringUtils.abbreviate(pay.getNickname(), 15) + " "
											+ mask.mask(pay.getBillingAccount()));
								}
							}else{
								payeeMap.put(pay.getFrontEndId(), StringUtils.abbreviate(pay.getNickname(), 15) + " "
										+ mask.mask(pay.getBillingAccount()));
							}
							
							
							if (reserveAct != null) {
								if (customerService.accountValidForPIFTransfer(reserveAct))
									pifPayeeMap.put(pay.getFrontEndId(), StringUtils.abbreviate(pay.getNickname(), 15)
											+ " " + mask.mask(pay.getBillingAccount()));
							} else {
								Payee payee = (Payee) pay.getBackendObject();
								if (customerService.telepagoIdValidForPIFTransfer(payee.getBillerNumber())) {
									pifPayeeMap.put(pay.getFrontEndId(), StringUtils.abbreviate(pay.getNickname(), 15)
											+ " " + mask.mask(pay.getBillingAccount()));
								}
							}
						}
					}

				}
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				payeeMap.put("add", "Add a new payee");
				pifPayeeMap.put("add", "Add a new payee");
			} else {
				payeeMap.put("add", "A&ntilde;adir un comercio");
				pifPayeeMap.put("add", "A&ntilde;adir un comercio");
			}

			Map<String, String> frequencies = new LinkedHashMap<String, String>();
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request))) {
				frequencies.put("variable", "one payment");
				frequencies.put("recurrent", "recurrent payment");
			} else {
				frequencies.put("variable", "una vez");
				frequencies.put("recurrent", "recurrente");
			}

			if (page == 0) {

				model.put("fromaccounts", accountsFromMap);
				model.put("payeesMap", payeeMap);
				model.put("pifPayeesMap", pifPayeeMap);

				if (favorites != null && favorites.size() > 0)
					Collections.sort(favorites, new FavoritePaymentComparator());

				// The EBPP2 user id is not yet in session; add it
				if (!userSession.isEbpp2UserIdLookedUp()) {
					EbppEntitlement ent = null;
					if (customerProfile.getEntitlementByType(EntitlementType.EBILLS) != null) {
						ent = new EbppEntitlement(customerProfile.getEntitlementByType(EntitlementType.EBILLS));

						// If the username is blank, we need to look in the
						// current EBPP accounts ONLY
						if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
							userSession.setEbpp2UserId("");
						} else {
							userSession.setEbpp2UserId(ent.getUsername());
						}
						userSession.setEbpp2UserIdLookedUp(true);
					}
				}
				
				List<String> ccaEbills = new ArrayList<>();
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
					model.put("ebillsFlag", true);
					List<CreditCardStatement_Type> ccaStatements = userSession.getCcaCurrentStatements();
					if (ccaStatements != null && !ccaStatements.isEmpty()) {
						for (Iterator<CreditCardStatement_Type> iterator = ccaStatements.iterator(); iterator
								.hasNext();) {
							CreditCardStatement_Type ccs = iterator.next();
							ccaEbills.add(ccs.getAccountId());
						}
					}
				
				}

				// The user has an EBPP2 ID
				int cantPayrolls = 0;
				int tamPayrolls = 0;
				int tamFavorite = 0;
				if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

					if (isPayrollActivated) {
						if(favorites == null) favorites = new ArrayList<TVAccountFavoritePayment>();
						if(favorites != null){
							Iterator<TVAccountFavoritePayment> favoriteIterator = favorites.iterator();
							while (favoriteIterator.hasNext()) {
								TVAccountFavoritePayment favoritePay = favoriteIterator.next();
								if(isEvendorActivated){
									if(favoritePay.getPayee().getGlobalPayee().getId().toString().equalsIgnoreCase(telenominaID)||
											favoritePay.getPayee().getGlobalPayee().getId().toString().equalsIgnoreCase(evendorId)){
										favoriteIterator.remove();
									}
								}
								else{								
									if (favoritePay.getPayee().getGlobalPayee().getId().toString()
											.equalsIgnoreCase(telenominaID)) {
										favoriteIterator.remove();
									}
								}
								
							}
						}
						favorites.addAll(payrollPayments);
					}
					List<UserAccount> ebpp2UsrAccounts = userSession.getEbpp2UserAccountsAndSummaries();
					if (favorites != null) {
						tamFavorite = favorites.size();
						for (int i = 0; i < tamFavorite; i++) {
							for (UserAccount userAccount : ebpp2UsrAccounts) {
								if (userAccount != null && !GenericValidator.isBlankOrNull(userAccount.getPayeeId())) {
									if (favorites.get(i).getPayee().getGlobalPayeeId() == Integer
											.parseInt(userAccount.getPayeeId())
											&& favorites.get(i).getPayee().getBillingAccount()
													.replaceFirst("^0+(?!$)", "").equals(userAccount.getAccountNumber()
															.replaceFirst("^0+(?!$)", ""))) {
										if (!userAccount.isLastEbillPaid()) {
											favorites.get(i).getPayee().setHasNewEbill(true);
											favorites.get(i).getPayee().setEbpp2UserAccount(userAccount);
										}
									}
								}
							}
							if (isPayrollActivated) {
								if (favorites.get(i).getPayee().getGlobalPayee() != null
										&& favorites.get(i).getPayee().getGlobalPayee().getIdString() != null) {
									if(isEvendorActivated){
										if (!favorites.get(i).getPayee().getGlobalPayee().getIdString()
												.equalsIgnoreCase(telenominaID) && 
												!favorites.get(i).getPayee().getGlobalPayee().getIdString()
												.equalsIgnoreCase(evendorId)) {
											favoritePayment.add(favorites.get(i));
											if(!ccaEbills.isEmpty()) {
												String acc = favorites.get(i).getPayee().getBillingAccount();
												if(ccaEbills.contains(acc)) {													
													favorites.get(i).getPayee().setHasNewEbill(true);	
												}
											}
										} else {
											cantPayrolls++;
										}
									}
									else{
									
										if (!favorites.get(i).getPayee().getGlobalPayee().getIdString()
												.equalsIgnoreCase(telenominaID)) {
											favoritePayment.add(favorites.get(i));
											if(!ccaEbills.isEmpty()) {
												String acc = favorites.get(i).getPayee().getBillingAccount();
												if(ccaEbills.contains(acc)) {													
													favorites.get(i).getPayee().setHasNewEbill(true);	
												}
											}
										} else {
											cantPayrolls++;
										}
									
									}
								}
							} else {
								favoritePayment.add(favorites.get(i));
							}
						}
					}
				}
				/*cantPayrolls = payrollPayments.size();
				if(cantPayrolls > 0 && cantPayrolls < 4)
				{
					tamPayrolls = (cantPayrolls * 159) + 660;
					model.put("tam", tamPayrolls + "px");				
				}else if (cantPayrolls > 3) {
					tamPayrolls = (cantPayrolls * 159) + 380;
					model.put("tam", tamPayrolls + "px");
				}*/
				if (realTimeNotificationsClaro){
                  Map<String, Boolean> paymentsHistory = new LinkedHashMap<>(); // payee with payments history
                  for(TVAccountFavoritePayment tvAccount : favoritePayment){
                        if (Utils.containsPayments(tvAccount.getPayee().getFrontEndId(),userSession)) {
                            paymentsHistory.put(tvAccount.getPayee().getFrontEndId(), Boolean.TRUE);
                        }
                  }
                  model.put("paymentHistory",paymentsHistory);

                }
				model.put("favoritePayments", favoritePayment);

				model.put("frequencymap", frequencies);

				int size = 3;

				model.put("listSize", size);

				// Set up today's date
				SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				final Calendar n = calculateEffDate(Calendar.getInstance()); // effective date
				final Calendar rtN = Calendar.getInstance(); // effective date for real Time
				final String todayDateKey = "todayDate"; // today date key
				final String todayDateRealTimeKey = "todayDateRealTime"; // real time today date key
				final String monthKey = "month"; // today month key
				final String monthRealTimeKey = "monthRealTime"; // real time today month date key
				final String dayKey = "day"; // today day key
				final String dayRealTimeKey = "dayRealTime"; // real time today month date key

				model.put(todayDateKey, df.format(n.getTime()));
				model.put(todayDateRealTimeKey, df.format(rtN.getTime()));
				df = new SimpleDateFormat("MM");
				model.put(monthKey, df.format(n.getTime()));
				model.put(monthRealTimeKey, df.format(rtN.getTime()));
				df = new SimpleDateFormat("dd");
				model.put(dayKey, df.format(n.getTime()));
				model.put(dayRealTimeKey, df.format(rtN.getTime()));

				if (n.get(Calendar.YEAR) - Calendar.getInstance().get(Calendar.YEAR) >= 1) {
					model.put("dateMismatch", true);
				}

				Map<String, String> singleAccountFromMap = new LinkedHashMap<String, String>(accountsFromMap);
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
					singleAccountFromMap.put("multiple", "Pay with multiple accounts");
				else
					singleAccountFromMap.put("multiple", "Pagar con m&uacute;ltiples cuentas");
				model.put("singleaccountfrom", singleAccountFromMap);

				// TODO verificar si existe una cuenta defatul para pagos y
				// ponerla como valor del singleaccountfrom
				if ((paymentsList.getSingleAccountFrom() == null || paymentsList.getSingleAccountFrom().equals(""))
						&& customerProfile.hasEntitlement(EntitlementType.BILLPAY)) {
					BillPayEntitlement ent = new BillPayEntitlement(
							customerProfile.getEntitlementByType(EntitlementType.BILLPAY));
					if (ent.getDetailView())
						model.put("showDetail", true);
					else
						model.put("showDetail", false);

					if (ent.getAccountKey() != null && !ent.getAccountKey().equals("")) {
						String defaultaccountkey = ent.getAccountKey();
						if (defaultaccountkey != null && defaultaccountkey.equalsIgnoreCase("multiple"))
							paymentsList.setSingleAccountFrom("multiple");
						else {
							TVAccountTransferSource defaultAccount = Utils.getAccountSourceByActKey(defaultaccountkey,
									accountsFrom);
							if (defaultAccount != null)
								paymentsList.setSingleAccountFrom(defaultAccount.getFrontEndId());
						}
					}
				}

			} else if (page == 1) {
				model.put("confirm", "true");
				model.put("fromaccounts", accountsFromMap);
				model.put("payeesMap", payeeMap);
				model.put("pifPayeesMap", pifPayeeMap);
				model.put("favoritePayments", favorites);
				model.put("frequencymap", frequencies);

				int size = 3;
				model.put("listSize", size);
				model.put("todayDate", "");
			}

			// end of if page ==0 || page ==1
			if (page == 1) {
				boolean isMultipleAccountPayment = false;
				PaymentSortbyAccount paymentsort = new PaymentSortbyAccount();
				// AccountMaskUtil mask = new AccountMaskUtil();

				List<MultiplePaymentBean> multipleaccounts = new LinkedList<MultiplePaymentBean>();
				if (paymentsList.getSingleAccountFrom().equalsIgnoreCase("multiple")) {
					model.put("hasmultipleaccounts", "true");
					isMultipleAccountPayment = true;
				}

				// List<TVPayment> verifyPayment = new LinkedList<TVPayment>();
				BigDecimal totalverify = new BigDecimal(0.0d);
				List<PaymentBean> payments = paymentsList.getPayments();

				for (Iterator<PaymentBean> iterator = payments.iterator(); iterator.hasNext();) {
					PaymentBean name = iterator.next();
					if (name.getAmount() == null)
						continue;
					String accountfrom = "";
					if (!paymentsList.getSingleAccountFrom().equalsIgnoreCase("none")
							&& !paymentsList.getSingleAccountFrom().equalsIgnoreCase("multiple")) {
						accountfrom = paymentsList.getSingleAccountFrom();
					} else {
						accountfrom = name.getAccountFrom();
					}

					TVAccountTransferSource sou = null;

					// If it is a PIF account, create it
					if (accountfrom.startsWith("PIF")) {
						String externalAccountId = accountfrom.substring(3);
						PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
						if (externalAccount != null) {
							BankInformation bankInformation = new BankInformation();
							bankInformation.setBankId(externalAccount.getRoutingNumber());
							bankInformation.setBankName(externalAccount.getBankName());
							CustomerAccountPreferences pref = new CustomerAccountPreferences();
							TVAccountFactory accountFactory = new TVAccountFactory();
							Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
									externalAccount.getAccountNumber());
							sourceAccount.setAccountID(externalAccount.getAccountNumber());
							sourceAccount.setSubType(Character.toString(externalAccount.getAccountType())); // Checking/Savings/Reserve
							((DepositAccount) sourceAccount)
									.setSectionIndicator(Character.toString(externalAccount.getAccountType()));
							TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
							fromAccount.setCustomerAccountPreferences(pref);
							fromAccount.setNickname(externalAccount.getNickname());
							fromAccount.setHide(externalAccount.getShow() == '0');
							fromAccount.getTVAccount().setBankInformation(bankInformation);
							sou = new TVAccountTransferSource(fromAccount);
						}
					} else {
						sou = Utils.getAccountSource(accountfrom, accountsFrom);
					}

					TVFrontEndPayee tar = Utils.getPayeebyId(name.getPayeeid(), payees);

					if (sou == null || tar == null)
						continue;

					BigDecimal amount = name.getAmount(); // payment amount

					Calendar cal = Calendar.getInstance(); // calendar
					final String rtNotificacionValue = tar.getGlobalPayee().getRtNotification(); // rt
					final String rtIndicator = tar.getGlobalPayee().getRtIndicator(); // real time
					
					cal.setTime(name.getEffectiveDate());
					if (realTimeNotificationsClaro
							&& !StringUtils.isEmpty(rtNotificacionValue)
							&& !StringUtils.isEmpty(rtIndicator)
							&& rtNotificacionValue.equals("Y")
							&& rtIndicator.equals("Y")) {
						name.setEffectiveDate(cal.getTime());
					} else {
						cal = calculateEffDate(cal);
						name.setEffectiveDate(cal.getTime());
					}


					TVPayment payment = null;
					String recurrent = name.getRecurrent();
					String infinite = name.getInfinite();

					if (recurrent != null && recurrent.equalsIgnoreCase("recurrent")) {

						Integer inst = name.getInstances();

						if (infinite != null && infinite.equalsIgnoreCase("infinite")) {
							inst = 999;
						}
						// else infinite == byinstances
						payment = new TVPayment(amount, name.getFrequency(), inst, cal, sou, tar);
					} else
						payment = new TVPayment(amount, Frequency.MANUALLY, 0, cal, sou, tar);

					String estimatedDate=Utils.getEstimateDateAccordingToPaymentType(tar,customerService,cal,recurrent,Utils.getRSALanguage(request));
					payment.setEstimateDate(estimatedDate);

					totalverify = totalverify.add(payment.getAmount());
					paymentsort.addPayment(payment);
					// verifyPayment.add(payment);

					if (isMultipleAccountPayment) {
						boolean found = false;
						int i = 0;
						for (Iterator<MultiplePaymentBean> multiiterator = multipleaccounts.iterator(); multiiterator
								.hasNext();) {
							MultiplePaymentBean multi = (MultiplePaymentBean) multiiterator.next();
							if (multi.getAccountNumber().equals(sou.getAccount().getAccountNumber())) {
								found = true;
								multi.setAmount(multi.getAmount().add(payment.getAmount()));
								multipleaccounts.set(i, multi);
							} else
								i++;
						}
						if (!found) {
							MultiplePaymentBean multi = new MultiplePaymentBean();
							multi.setAccountNumber(sou.getAccount().getAccountNumber());
							multi.setAmount(payment.getAmount());
							multipleaccounts.add(multi);
						}
					}
				}
				for (Iterator<PaymentBean> iterator = paymentsList.getFavorites().iterator(); iterator.hasNext();) {
					PaymentBean name = (PaymentBean) iterator.next();
					if (name.getAmount() == null)
						continue;
					String accountfrom = "";
					if (!paymentsList.getSingleAccountFrom().equalsIgnoreCase("none")
							&& !paymentsList.getSingleAccountFrom().equals("multiple")) {
						accountfrom = paymentsList.getSingleAccountFrom();
					} else {
						accountfrom = name.getAccountFrom();
					}

					TVAccountTransferSource sou = null;

					// If it is a PIF account, create it
					if (accountfrom.startsWith("PIF")) {
						String externalAccountId = accountfrom.substring(3);
						PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
						if (externalAccount != null) {
							BankInformation bankInformation = new BankInformation();
							bankInformation.setBankId(externalAccount.getRoutingNumber());
							bankInformation.setBankName(externalAccount.getBankName());
							CustomerAccountPreferences pref = new CustomerAccountPreferences();
							TVAccountFactory accountFactory = new TVAccountFactory();
							Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
									externalAccount.getAccountNumber());
							sourceAccount.setAccountID(externalAccount.getAccountNumber());
							sourceAccount.setSubType(Character.toString(externalAccount.getAccountType())); // Checking/Savings/Reserve
							((DepositAccount) sourceAccount)
									.setSectionIndicator(Character.toString(externalAccount.getAccountType()));
							TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
							fromAccount.setCustomerAccountPreferences(pref);
							fromAccount.setNickname(externalAccount.getNickname());
							fromAccount.setHide(externalAccount.getShow() == '0');
							fromAccount.getTVAccount().setBankInformation(bankInformation);
							sou = new TVAccountTransferSource(fromAccount);
						}
					} else {
						sou = Utils.getAccountSource(accountfrom, accountsFrom);
					}

					TVFrontEndPayee tar = Utils.getPayeebyId(name.getPayeeid().trim(), payees);
					if (sou == null || tar == null)
						continue;

					BigDecimal amount = name.getAmount();

					Calendar cal = Calendar.getInstance();
					cal.setTime(name.getEffectiveDate());
					if (realTimeNotificationsClaro && tar.getGlobalPayee().getRtNotification().equals("Y")
						&& tar.getGlobalPayee().getRtIndicator().equals("Y")) {
						name.setEffectiveDate(cal.getTime());
					} else {
						cal = calculateEffDate(cal);
						name.setEffectiveDate(cal.getTime());
					}

					TVPayment payment = null;
					String recurrent = name.getRecurrent();
					String infinite = name.getInfinite();

					if (recurrent != null && recurrent.equalsIgnoreCase("recurrent")) {

						Integer inst = name.getInstances();

						if (inst == null)
							inst = new Integer(0);

						if (infinite != null && infinite.equalsIgnoreCase("infinite")) {
							inst = 999;
						}
						// else infinite == byinstances
						payment = new TVPayment(amount, name.getFrequency(), inst, cal, sou, tar);
					} else
						payment = new TVPayment(amount, Frequency.MANUALLY, new Integer(0), cal, sou, tar);

					String estimatedDate=Utils.getEstimateDateAccordingToPaymentType(tar,customerService,cal,recurrent,Utils.getRSALanguage(request));
					payment.setEstimateDate(estimatedDate);

					totalverify = totalverify.add(payment.getAmount());

					paymentsort.addPayment(payment);
					// verifyPayment.add(payment);

					if (isMultipleAccountPayment) {
						boolean found = false;
						int i = 0;
						for (Iterator<MultiplePaymentBean> multiiterator = multipleaccounts.iterator(); multiiterator
								.hasNext();) {
							MultiplePaymentBean multi = (MultiplePaymentBean) multiiterator.next();
							if (multi.getAccountNumber().equals(sou.getAccount().getAccountNumber())) {
								found = true;
								multi.setAmount(multi.getAmount().add(payment.getAmount()));
								multipleaccounts.set(i, multi);
							} else
								i++;
						}
						if (!found) {
							MultiplePaymentBean multi = new MultiplePaymentBean();
							multi.setAccountNumber(sou.getAccount().getAccountNumber());
							multi.setAmount(payment.getAmount());
							multipleaccounts.add(multi);
						}
					}
				}

				model.put("paymentsort", paymentsort.getElementMap());
				// model.put("verifyPayment", verifyPayment);
				model.put("totalAmount", totalverify);
				model.put("multipleaccounts", multipleaccounts);

				if (paymentsList.getSingleAccountFrom() != null && !"".equals(paymentsList.getSingleAccountFrom())
						&& !"none".equals(paymentsList.getSingleAccountFrom())
						&& !"multiple".equals(paymentsList.getSingleAccountFrom())) {
					TVAccountTransferSource sou = null;

					// If it is a PIF account, create it
					if (paymentsList.getSingleAccountFrom().startsWith("PIF")) {
						String externalAccountId = paymentsList.getSingleAccountFrom().substring(3);
						PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
						if (externalAccount != null) {
							BankInformation bankInformation = new BankInformation();
							bankInformation.setBankId(externalAccount.getRoutingNumber());
							bankInformation.setBankName(externalAccount.getBankName());
							CustomerAccountPreferences pref = new CustomerAccountPreferences();
							TVAccountFactory accountFactory = new TVAccountFactory();
							Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
									externalAccount.getAccountNumber());
							sourceAccount.setAccountID(externalAccount.getAccountNumber());
							sourceAccount.setSubType(Character.toString(externalAccount.getAccountType())); // Checking/Savings/Reserve
							((DepositAccount) sourceAccount)
									.setSectionIndicator(Character.toString(externalAccount.getAccountType()));
							TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
							fromAccount.setCustomerAccountPreferences(pref);
							fromAccount.setNickname(externalAccount.getNickname());
							fromAccount.setHide(externalAccount.getShow() == '0');
							fromAccount.getTVAccount().setBankInformation(bankInformation);
							sou = new TVAccountTransferSource(fromAccount);
						}
					} else {
						sou = Utils.getAccountSource(paymentsList.getSingleAccountFrom(), accountsFrom);
					}

					if (sou != null && sou.getAccount() != null && sou.getAccount().getPortalBalance() != null) {
						if (sou.getAccount().getSubtype().equals("CCA")) {
							if (sou.getAccount().getAvailableBalance().doubleValue() < totalverify.doubleValue())
								model.put("showAlertTotal", true);
						} else// IDA o PFC
						{
							if (sou.getAccount().getPortalBalance().doubleValue() < totalverify.doubleValue())
								model.put("showAlertTotal", true);
						}
					}
				}
			}
		}
		
		model.put("MBSFE_138", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138));
		model.put("MBSFE738", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE738));
		model.put("MBCRTP", realTimeNotificationsClaro);
		return model;
	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		return new ModelAndView(cancelView);
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		if (page == 0 || page == 1) {
			PaymentsListBean plist = (PaymentsListBean) command;

			if (plist.getSingleAccountFrom().equalsIgnoreCase("none")) {
				errors.rejectValue("singleAccountFrom", "payments.select.required", "Select an account");
			}

			boolean isPifPayment = false;
			boolean multiple = (plist.getSingleAccountFrom().equalsIgnoreCase("multiple"));

			if (!multiple)
				isPifPayment = (plist.getSingleAccountFrom().startsWith("PIF"));

			UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			List<TVFrontEndPayee> payees = userSession.getPayessBasicEbpp(true);

			int i = 0;
			for (Iterator<PaymentBean> iter = plist.getPayments().iterator(); iter.hasNext();) {
				PaymentBean element = (PaymentBean) iter.next();
				if (element.getAmount() == null) {
					iter.remove();
					continue;
				}
				// TVFrontEndPayee frontendpayee;
				// if(element.getPayeeid()!=null &&
				// !element.getPayeeid().equals("")){
				// frontendpayee = Utils.getPayeebyId(element.getPayeeid(),
				// payees);
				// element.setGlobalPayeeId(""+frontendpayee.getGlobalPayeeId());
				// }

				BigDecimal maxpayamount = null;
				maxpayamount = new BigDecimal(999999999.99);
				
				if (element.getAmount().compareTo(maxpayamount) > 0) {
					errors.rejectValue("payments[" + i + "].amount", "payments.amount.largermax",
							"Amount larger than max");

				} else if (element.getAmount().doubleValue() < new Double(0.01d)) {
					errors.rejectValue("payments[" + i + "].amount", "payments.amt.invalid", "Invalid");

				}

				if ("none".equalsIgnoreCase(plist.getSingleAccountFrom())
						|| "multiple".equalsIgnoreCase(plist.getSingleAccountFrom())) {
					if ("none".equalsIgnoreCase(element.getAccountFrom())) {
						errors.rejectValue("payments[" + i + "].accountFrom", "payments.acctfrom.required",
								"Account From is required");

					}
				}

				if (multiple)
					isPifPayment = (element.getAccountFrom().startsWith("PIF"));

				if (isPifPayment)
					element.setPayeeid(element.getPifPayeeid());

				if ("none".equalsIgnoreCase(element.getPayeeid())) {
					errors.rejectValue("payments[" + i + "].payeeid", "payments.payee.invalid", "Payee is required");

				}

				if (GenericValidator.isBlankOrNull(element.getRecurrent())
						|| "variable".equalsIgnoreCase(element.getRecurrent())) {
					element.setFrequency(Frequency.MANUALLY);
				}

				Date effectiveDate = element.getEffectiveDate();
				Calendar today = Calendar.getInstance();

				// Set the max date (one year in future)
				Calendar thresholdDate = Calendar.getInstance();
				thresholdDate.add(Calendar.YEAR, 1);

				if (effectiveDate != null) {
					// Validate no future dates
					Calendar n = Calendar.getInstance();
					n.setTime(effectiveDate);

					if ((n.get(Calendar.YEAR) < today.get(Calendar.YEAR))
							|| n.get(Calendar.YEAR) == today.get(Calendar.YEAR)
									&& n.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR)) {
						errors.rejectValue("payments[" + i + "].effectiveDate", "payments.date.past",
								"Invalid Date.<br>(Day in the past)");
					} else if (n.after(thresholdDate)) {
						errors.rejectValue("payments[" + i + "].effectiveDate", "payments.date.future",
								"Invalid Date.<br>(Date more than one year in future)");
					}
				} else {
					errors.rejectValue("payments[" + i + "].effectiveDate", "payments.date.required", "Required");

				}

				if ("recurrent".equalsIgnoreCase(element.getRecurrent())) {
					// Make sure that a frequency is selected
					if (element.getFrequency() == null) {
						errors.rejectValue("payments[" + i + "].frequency", "payments.frequency.required",
								"Frequency is required");
					}
					// Make sure that a frequency type is required
					if (GenericValidator.isBlankOrNull(element.getInfinite())) {
						errors.rejectValue("payments[" + i + "].infinite", "payments.freqtype.required",
								"Frequency type is required");

					} else if (!element.getInfinite().equalsIgnoreCase("infinite")) {
						if (element.getInstances() == null) {
							errors.rejectValue("payments[" + i + "].instances", "payments.instances.required",
									"Instances required");
						} else if (element.getInstances() < 2) {
							errors.rejectValue("payments[" + i + "].instances", "payments.amtinstances.required",
									"Instances must be 2 or more");
						}
					}
				}

				// PIF
				if (isPifPayment && !"none".equalsIgnoreCase(element.getPayeeid())) {
					TVFrontEndPayee payee = Utils.getPayeebyId(element.getPayeeid(), payees);
					Payee p = (Payee) payee.getBackendObject();
					TVFrontendAccount tvPayee = Utils.getAccountByAccountNumber(p.getBillingAccount(),
							userSession.getAccounts());

					if (tvPayee == null) {
						if (!customerService.telepagoIdValidForPIFTransfer(p.getBillerNumber())) {
							errors.rejectValue("payments[" + i + "].payeeid", "payments.payee.pifinvalid",
									"Payment not allowed");
						}
					} else if (!customerService.accountValidForPIFTransfer(tvPayee)) {
						errors.rejectValue("payments[" + i + "].payeeid", "payments.payee.pifinvalid",
								"Payment not allowed");
					}
				}

				i++;
			}
			int m = 0;
			int noamount = 0;
			for (Iterator<PaymentBean> iter = plist.getFavorites().iterator(); iter.hasNext();) {
				PaymentBean element = (PaymentBean) iter.next();
				if (element.getAmount() == null) {
					// iter.remove();
					m++;
					noamount++;
					continue;
				}
				BigDecimal maxpayamount = null;
				maxpayamount = new BigDecimal(999999999.99);

				if (element.getAmount().compareTo(maxpayamount) > 0) {
					errors.rejectValue("favorites[" + m + "].amount", "payments.amount.largermax",
							"Amount larger than max");

				} else if (element.getAmount().doubleValue() < new Double(0.01d)) {
					errors.rejectValue("favorites[" + m + "].amount", "payments.amt.invalid", "Invalid");

				}

				if ("none".equalsIgnoreCase(plist.getSingleAccountFrom())
						|| "multiple".equalsIgnoreCase(plist.getSingleAccountFrom())) {
					if ("none".equalsIgnoreCase(element.getAccountFrom())) {
						errors.rejectValue("favorites[" + m + "].accountFrom", "payments.acctfrom.required",
								"Account From is required");

					}
				}

				if ("none".equalsIgnoreCase(element.getPayeeid())) {
					errors.rejectValue("favorites[" + m + "].payeeid", "payments.payee.invalid", "Payee is required");

				}

				if (element.getRecurrent() == null || "variable".equalsIgnoreCase(element.getRecurrent()))
					element.setFrequency(Frequency.MANUALLY);

				Date effectiveDate = element.getEffectiveDate();
				Calendar today = Calendar.getInstance();

				// Set the max date (one year in future)
				Calendar thresholdDate = Calendar.getInstance();
				thresholdDate.add(Calendar.YEAR, 1);

				if (effectiveDate != null) {
					// Validate no future dates
					Calendar n = Calendar.getInstance();
					n.setTime(effectiveDate);

					if ((n.get(Calendar.YEAR) < today.get(Calendar.YEAR))
							|| n.get(Calendar.YEAR) == today.get(Calendar.YEAR)
									&& n.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR)) {
						errors.rejectValue("favorites[" + m + "].effectiveDate", "payments.date.past",
								"Invalid Date.<br>(Day in the past)");
					} else if (n.after(thresholdDate)) {
						errors.rejectValue("favorites[" + m + "].effectiveDate", "payments.date.future",
								"Invalid Date.<br>(Date more than one year in future)");
					}

				} else {
					errors.rejectValue("favorites[" + m + "].effectiveDate", "payments.date.required", "Required");

				}

				if ("recurrent".equalsIgnoreCase(element.getRecurrent())) {
					// Make sure that a frequency is selected
					if (element.getFrequency() == null) {
						errors.rejectValue("favorites[" + m + "].frequency", "payments.frequency.required",
								"Frequency is required");

					}
					// Make sure that a frequency type is required
					if (GenericValidator.isBlankOrNull(element.getInfinite())) {
						errors.rejectValue("favorites[" + m + "].infinite", "payments.freqtype.required",
								"Frequency type is required");

					} else if (!element.getInfinite().equalsIgnoreCase("infinite")) {
						if (element.getInstances() == null) {
							errors.rejectValue("favorites[" + m + "].instances", "payments.instances.required",
									"Instances required");

						} else if (element.getInstances() < 2) {
							errors.rejectValue("favorites[" + m + "].instances", "payments.amtinstances.required",
									"Instances must be 2 or more");

						}

					}
				}

				// PIF
				if (multiple)
					isPifPayment = (element.getAccountFrom().startsWith("PIF"));

				if (isPifPayment) {
					TVFrontEndPayee payee = Utils.getPayeebyId(element.getPayeeid(), payees);
					Payee p = (Payee) payee.getBackendObject();
					TVFrontendAccount tvPayee = Utils.getAccountByAccountNumber(p.getBillingAccount(),
							userSession.getAccounts());

					if (tvPayee == null) {
						if (!customerService.telepagoIdValidForPIFTransfer(p.getBillerNumber())) {
							if (multiple)
								errors.rejectValue("favorites[" + m + "].accountFrom", "payments.payee.pifinvalid",
										"Payment not allowed");
							else
								errors.rejectValue("favorites[" + m + "].effectiveDate", "payments.payee.pifinvalid",
										"Payment not allowed");
						}
					} else if (!customerService.accountValidForPIFTransfer(tvPayee)) {
						if (multiple)
							errors.rejectValue("favorites[" + m + "].accountFrom", "payments.payee.pifinvalid",
									"Payment not allowed");
						else
							errors.rejectValue("favorites[" + m + "].effectiveDate", "payments.payee.pifinvalid",
									"Payment not allowed");
					}
				}

				m++;
			}

			if ((m - noamount + i) == 0)
				errors.rejectValue("singleAccountFrom", "payments.pmtamount.required",
						"Please select a payment and amount");

			if (errors.hasErrors()) {
				UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
				if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
					session.setStatusmessage("Please verify the information below");
				else
					session.setStatusmessage("Verifica la informaci&oacute;n entrada");
			}
		}
	}

	public Calendar calculateEffDate(Calendar cal) {
		List<Holiday> holidays = customerService.getHolidays();
		BankingDaysUtils bankingDaysUtils = new BankingDaysUtils(holidays);
		return bankingDaysUtils.calculatePayDaysWithCutoff(cal, 1, 17, 0);
	}

	public String getCancelView() {
		return cancelView;
	}

	public void setCancelView(String cancelView) {
		this.cancelView = cancelView;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.BILLPAY;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

	private String getTelepagoErrorDescriptor(String codigo, String len) {
		String status = "";
		try {
			if (!"en".equals(len))
				len = "es";
			status = this.messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
		} catch (Exception e) {
			status = "";
		}

		return status;
	}

	public String getBalanceDelay() {
		return balanceDelay;
	}

	public void setBalanceDelay(String balanceDelay) {
		this.balanceDelay = balanceDelay;
	}
	
	/**
	 * @param account
	 * @param userSession
	 */
	private BigDecimal getAmountDueInfo(TVFrontendAccount account, UserSession userSession) {
		
		final String mla = "MLA"; //string to avoid using literals
		
		LoanAccount loanAccount = null; //backend account info
		
		MortgageAccountRecord mortgageActRecord = null; // MLA Record
		
		BigDecimal amountDue = new BigDecimal(0);
		
		if (account.getSubtype().equals(mla) && account.getBackendObject() instanceof LoanAccount ) {
			
			 loanAccount = (LoanAccount) account.getBackendObject();
			 
			if (loanAccount.getRecords() instanceof MortgageAccountRecord) {
				mortgageActRecord = (MortgageAccountRecord) loanAccount.getRecords();
				amountDue = mortgageActRecord.getCurrentAmount();
				if (mortgageActRecord.getCurrentAmount()==null) {
					amountDue = getAmountDueFromAccountDetails(account, userSession);
				}else {
					amountDue = mortgageActRecord.getCurrentAmount();
				}
			}
			 
		}else if (account.getPortalBalance()!=null) {
			amountDue = account.getPortalBalance();
		}
		
		return amountDue;
            
	}
	
	/**
	 * @param account
	 * @param userSession
	 * @return
	 */
	private BigDecimal getAmountDueFromAccountDetails(TVFrontendAccount account, UserSession userSession) {
		
		LoanAccount loanAccount = null; //backend account info
		
		MortgageAccountRecord mortgageActRecord  = null; // MLA Record
		
		AbstractAccount accountDetails = null; // Account Details
		
		BigDecimal amountDue = new BigDecimal(0); //Amount Due
		
		accountDetails = userSession.getCustomerService().getAccountDetails(
				userSession.getCustomerProfile(), userSession.getBankingSession(), account, true);
		if (accountDetails.getBackendObject() instanceof LoanAccount) {
			loanAccount = (LoanAccount) accountDetails.getBackendObject();
			
			if (loanAccount.getRecords() instanceof MortgageAccountRecord) {
				mortgageActRecord = (MortgageAccountRecord) loanAccount.getRecords();
				amountDue = mortgageActRecord.getCurrentAmount();
			}
		}
		
		return amountDue;
	}
	

}
