package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.factory.TVAccountFactory;
import bppr.tv.beans.accounts.records.CreditCardAccountRecord;
import bppr.tv.beans.accounts.types.CardAccount;
import bppr.tv.beans.accounts.types.DepositAccount;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.holiday.Holiday;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.AutoPay;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.CancelPayeeResponse;
import evertec.cibp.core.services.responses.PaymentHistoryResponse;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.vs.models.PaymentFilterType;
import evertec.cibp.web.form.bean.CCPortalPayments;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class CcMakePaymentController extends CancellableFormController implements MessageSourceAware {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;
	private final static int maxPayments = 15;


	public CcMakePaymentController() {
		super();
		setCommandClass(CCPortalPayments.class);
		setCommandName("ccMakePayment");
		setFormView("ccMakePayment");
	}



	/**
	 * Added for CIBP-1767 Portal de Tarjetas de Credito
	 * @param account TVFrontendAccount
	 * @return Map
	 */
	private Map<String, String> getPaymentDays(TVFrontendAccount account) {

		// process to know which day the user can make the payment
		Map<String, String> paymentDays = new LinkedHashMap<String, String>();

		CardAccount ccaAccount = null;
		ccaAccount = (CardAccount) account.getBackendObject();
		CreditCardAccountRecord ccaAcctRecord = (CreditCardAccountRecord) ccaAccount.getRecords();

		Calendar pmtDueDate = Calendar.getInstance();
		Calendar pmtStartDate = Calendar.getInstance();

		pmtDueDate = ccaAcctRecord.getDueDate();
		if (pmtDueDate != null) {
			pmtStartDate.setTime(pmtDueDate.getTime());
			// dsalas cibp-2644 change from 7 to 13 days
			pmtStartDate.add(Calendar.DAY_OF_MONTH, -17);

			int startDay = pmtStartDate.get(Calendar.DAY_OF_MONTH);
			int endDay = pmtDueDate.get(Calendar.DAY_OF_MONTH);
			if (pmtDueDate.get(Calendar.MONTH) != pmtStartDate.get(Calendar.MONTH)) {
				int monthEndDay = pmtStartDate.getActualMaximum(Calendar.DAY_OF_MONTH);

				for (int e = 1; e <= endDay; e++) {
					paymentDays.put(String.valueOf(e), String.valueOf(e));
				}
				for (int e = startDay; e <= monthEndDay; e++) {
					paymentDays.put(String.valueOf(e), String.valueOf(e));
				}
			} else {
				for (int e = startDay; e <= endDay; e++) {
					paymentDays.put(String.valueOf(e), String.valueOf(e));
				}
			}
			return paymentDays;
		}
		return null;
	}

	// Added for CIBP-1767 Portal de Tarjetas de Credito
	private Map<String, String> getPaymentProcess(UserSession userSession) {

		Map<String, String> paymentProcessType = new LinkedHashMap<String, String>();

		if (userSession.getLanguage().equals("en")) {
			paymentProcessType.put("MINIMUM", messageSource.getMessage(
					"credit.card.portal.tab.make.payment.autopay.payamountmin", null, new Locale("en")));
			paymentProcessType.put("COMPLETE", messageSource.getMessage(
					"credit.card.portal.tab.make.payment.autopay.payamountcomplete", null, new Locale("en")));
		} else {
			paymentProcessType.put("MINIMUM", messageSource.getMessage(
					"credit.card.portal.tab.make.payment.autopay.payamountmin", null, new Locale("es")));
			paymentProcessType.put("COMPLETE", messageSource.getMessage(
					"credit.card.portal.tab.make.payment.autopay.payamountcomplete", null, new Locale("es")));

		}
		return paymentProcessType;
	}

	// Added for CIBP-1767
	private Map<String, String> getAccountFromPaymentMap(HttpServletRequest request, UserSession userSession,
			boolean includePIFAccounts) {

		Set<TVAccountTransferSource> accountsFrom = userSession.getAccountsPaymentFrom();
		CustomerServices customerService = userSession.getCustomerService();
		AccountMaskUtil mask = new AccountMaskUtil();
		NumberTool numberTool = new NumberTool();

		Map<String, String> accountsFromMap = new LinkedHashMap<String, String>();
		if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
			accountsFromMap.put("none", "Select an account");
		else
			accountsFromMap.put("none", "Selecciona una cuenta");

		for (Iterator<TVAccountTransferSource> iterator = accountsFrom.iterator(); iterator.hasNext();) {
			TVAccountTransferSource from = (TVAccountTransferSource) iterator.next();
			TVFrontendAccount frontFrom = from.getAccount();
			if (frontFrom != null && !frontFrom.getHide()) {
				if (!frontFrom.getSubtype().equals("CCA") && frontFrom.getPortalBalance() != null)
					accountsFromMap.put(
							from.getFrontEndId(),
							StringUtils.abbreviate(frontFrom.getNickname(), 15)
									+ " "
									+ mask.mask(frontFrom.getAccountNumber())
									+ " "
									+ numberTool.format("currency", frontFrom.getPortalBalance())
									+ ((frontFrom.getAccountNumberSuffix() != null && !frontFrom
											.getAccountNumberSuffix().equals("")) ? " "
											+ frontFrom.getAccountNumberSuffix() : ""));
				else if (!frontFrom.getSubtype().equals("CCA"))
					accountsFromMap.put(from.getFrontEndId(), StringUtils.abbreviate(frontFrom.getNickname(), 15)
							+ " "
							+ mask.mask(frontFrom.getAccountNumber())
							+ ((frontFrom.getAccountNumberSuffix() != null && !frontFrom.getAccountNumberSuffix()
									.equals("")) ? " " + frontFrom.getAccountNumberSuffix() : ""));
			}
		}

		if (includePIFAccounts) {
			List<PIFExternalAccount> pifExternalAccounts = customerService.getConfirmedPIFExternalAccounts(userSession
					.getCustomerProfile().getTaxId());
			if (pifExternalAccounts != null)
				for (Iterator<PIFExternalAccount> iterator = pifExternalAccounts.iterator(); iterator.hasNext();) {
					PIFExternalAccount from = (PIFExternalAccount) iterator.next();
					accountsFromMap.put("PIF" + from.getAccountId(), StringUtils.abbreviate(from.getNickname(), 15)
							+ " " + mask.mask(from.getAccountNumber()));
				}
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)){
				if(userSession.getInterbankFundingAllowed()){
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						accountsFromMap.put("add", "Add other bank account");
					else
						accountsFromMap.put("add", "A&ntilde;ade una cuenta de otro Banco");
				}
			}
			else{
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					accountsFromMap.put("add", "Add other bank account");
				else
					accountsFromMap.put("add", "A&ntilde;ade una cuenta de otro Banco");
			}
		}

		return accountsFromMap;
	}

	private String getAccountFromPaymentInfo(HttpServletRequest request, UserSession userSession, String accountId) {
		String acct = "";

		Map<String, String> accounts = this.getAccountFromPaymentMap(request, userSession, true);
		acct = accounts.get(accountId);

		return acct;
	}

	private CCPortalPayments getAutoPayActiveInfo(UserSession userSession, CCPortalPayments ccPayment) {
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		List<Integer> payeeIds = new LinkedList<Integer>();
		PaymentHistoryResponse transferResponse = null;
		List<TVPayment> pending = new LinkedList<TVPayment>();
		List<TVPayment> processed = new LinkedList<TVPayment>();

		if (ccPayment.getPayeeInfo() != null && ccPayment.getPayeeInfo().getCustomerPayeeCodes() != null)
			payeeIds = ccPayment.getPayeeInfo().getCustomerPayeeCodes();

		String[] ids = new String[payeeIds.size()];
		for (int l = 0; l < payeeIds.size(); l++) {
			ids[l] = payeeIds.get(l).toString();
		}
		transferResponse = customerService.getPaymentHistory(customer, userSession.getBankingSession(),
				PaymentFilterType.HISTORY, maxPayments, ids, null);
		if (transferResponse.getInprocessPayments() != null)
			pending.addAll(transferResponse.getInprocessPayments());
		if (transferResponse.getPayments() != null)
			processed.addAll(transferResponse.getPayments());

		if (pending != null) {
			for (Iterator<TVPayment> iterator = pending.iterator(); iterator.hasNext();) {
				TVPayment payment = (TVPayment) iterator.next();
				if (payment.getFrequency().equals("MB") || payment.getFrequency().equals("TB")) {
					ccPayment.setAutoPayNextProcessDate(payment.getEffectiveDate());
					if (payment.getSourceNickname() == null) {
						payment.setSource(new TVAccountTransferSource(Utils.getAccountByAccountNumber(
								payment.getSourceAccountNumber(), userSession.getAccounts())));
						payment.setPayee(ccPayment.getPayeeInfo());
					}
					ccPayment.setPaymentInfo(payment);
				}
			}
		}

		TVPayment lastPayment = null;
		if (processed != null) {
			for (Iterator<TVPayment> iterator = processed.iterator(); iterator.hasNext();) {
				TVPayment payment = (TVPayment) iterator.next();
				if (payment != null && payment.getSourceNickname() == null) {
					TVFrontendAccount sourceAccount = Utils.getAccountByAccountNumber(payment.getSourceAccountNumber(),
							userSession.getAccounts());
					if (sourceAccount != null) {
						payment.setSource(new TVAccountTransferSource(sourceAccount));
						payment.setPayee(ccPayment.getPayeeInfo());

						if (ccPayment.getPaymentInfo() != null && ccPayment.getPaymentInfo().getSource() != null
								&& ccPayment.getPaymentInfo().getSource().getAccount() != null && payment != null
								&& payment.getSource() != null && payment.getSource().getAccount() != null) {
							if (ccPayment.getPaymentInfo().getSource().getAccount().getAccountNumber()
									.equalsIgnoreCase(payment.getSource().getAccount().getAccountNumber())) {
								if (lastPayment == null) {
									lastPayment = payment;
								} else {
									if (payment.getEffectiveDate().after(lastPayment.getEffectiveDate())) {
										lastPayment = payment;
									}
								}
							}
						}
					}
				}
			}
		}
		if (lastPayment != null) {
			ccPayment.setPaymentLastProcessDate(lastPayment.getEffectiveDate());
			ccPayment.setPaymentLastProcessStatus(lastPayment.getTVPayment().getStatusCode());
		}

		return ccPayment;
	}

	// Added for CIBP-1767 - Used in Payment Tab
	private Map<String, Object> makeSinglePayment(CCPortalPayments ccPayment, UserSession userSession,
			CustomerProfile customerProfile, CustomerServices customerService, HttpServletRequest request,
			boolean hasAlert) throws RemoteException {
		Map<String, Object> model = new HashMap<>();

		TVFrontendAccount sou = null;

		// If it is a PIF account, create it the TVFrontendAccount Object.
		if (ccPayment.getSinglePayAccountFrom().startsWith("PIF")) {
			String externalAccountId = ccPayment.getSinglePayAccountFrom().substring(3);
			PIFExternalAccount externalAccount = customerService.getPifExternalAccount(externalAccountId);
			if (externalAccount != null) {
				BankInformation bankInformation = new BankInformation();
				bankInformation.setBankId(externalAccount.getRoutingNumber());
				bankInformation.setBankType("ABA");
				bankInformation.setBankName(externalAccount.getBankName());
				CustomerAccountPreferences pref = new CustomerAccountPreferences();
				TVAccountFactory accountFactory = new TVAccountFactory();
				Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
						externalAccount.getAccountNumber());
				sourceAccount.setAccountID(externalAccount.getAccountNumber());
				sourceAccount.setSubType("IDA");
				((DepositAccount) sourceAccount).setSectionIndicator(Character.toString(externalAccount
						.getAccountType()));
				TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
				fromAccount.setCustomerAccountPreferences(pref);
				fromAccount.setNickname(externalAccount.getNickname());
				fromAccount.setHide(externalAccount.getShow() == '0');
				fromAccount.getTVAccount().setBankInformation(bankInformation);
				sou = fromAccount;
			}
		} else {
			sou = Utils.getAccountById(ccPayment.getSinglePayAccountFrom(), userSession.getAccounts());
		}
		TVFrontEndPayee tar = Utils.getPayeebyId(ccPayment.getPayeeInfo().getFrontEndId(), userSession.getPayees(true));
		model.put("PAYMENTINPROCESS", true);
		if (tar != null) {

			BigDecimal amount = ccPayment.getSinglePayAmount();

			model.put("PAYMENTFROM",
					this.getAccountFromPaymentInfo(request, userSession, ccPayment.getSinglePayAccountFrom()));

			Calendar cal = Calendar.getInstance();
			if (ccPayment.getSinglePayDate() != null)
				cal.setTime(ccPayment.getSinglePayDate());

			TVPayment payment = null;
			payment = new TVPayment(amount, Frequency.MANUALLY, 0, cal, new TVAccountTransferSource(sou), tar);
			payment.getTVPayment().setEffectiveDate(cal);
			payment.getPayee().setTelenomina(false);
			PaymentResponse res = PaymentsServiceModule.getInstance().sendPayments(customerProfile, userSession,
					payment, Utils.generateCustomerInteractionEvent(request), customerService);
			List<TVPayment> successfullPayment = new LinkedList<TVPayment>();
			List<TVPayment> failPayment = new LinkedList<TVPayment>();

			if (res.getStatus() == ResultStatus.SUCCESS) {
				if (res.getPayment().getPayee().getEbpp2UserAccount() != null
						&& res.getPayment().getPayee().isHasNewEbill()) {
					customerService.updateAccountPayment(userSession.getEbpp2UserId(), res.getPayment().getPayee()
							.getEbpp2UserAccount().getBillerAccountID());
					// Alizano hide new ebill message when payment is success
					// Fix Incident 1537231
					res.getPayment().getPayee().setHasNewEbill(false);
					res.getPayment().getPayee().getEbpp2UserAccount().setLastEbillPaid(true);
					// End Fix
				}
				model.put("PAYMENTSEND", true);
				model.put("REFNUM", payment.getReferenceNumber());
				successfullPayment.add(payment);
			} else {
				model.put("PAYMENTSEND", false);
				model.put("PAYMENTERROR", "ERROR REASON: " + res.getErrorMessage());
				failPayment.add(payment);
			}

			if (hasAlert) {
				customerService.sendPaymentConfirmation(customerProfile, successfullPayment, failPayment,
						userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
						userSession.getPreferredLanguage());
			}
		}

		return model;

	}

	private Map<String, Object> activateAutoPay(HttpServletRequest request, CustomerProfile customerProfile,
			CCPortalPayments ccPayment, UserSession userSession, boolean hasAlert, CustomerServices customerService)
			throws RemoteException {
		Map<String, Object> modelAutoPay = new HashMap<>();

		// Activate AutoPay
		TVFrontendAccount sou = Utils.getAccountById(ccPayment.getAutoPayAccountFrom(), userSession.getAccounts());
		TVFrontEndPayee tar = Utils.getPayeebyId(ccPayment.getPayeeInfo().getFrontEndId(), userSession.getPayees(true));
		modelAutoPay.put("AUTOPAYINPROCESS", true);
		if (tar != null) {

			// Set Calendar as Today
			Calendar cal = Calendar.getInstance();
			Date dt = new Date();

			cal.setTime(dt);
			if (cal.get(Calendar.DATE) > Integer.parseInt(ccPayment.getAutoPayDay())) {
				cal.add(Calendar.MONTH, 1);
			}

			cal.set(Calendar.DATE, Integer.parseInt(ccPayment.getAutoPayDay()));

			TVPayment payment = null;
			if (ccPayment.getAutoPayProcessType().equalsIgnoreCase("COMPLETE")) {
				BigDecimal amount = null;

				if (ccPayment.getAutoPayAmountMax() != null
						&& ccPayment.getAutoPayAmountMax().compareTo(new BigDecimal(0)) > 0)
					amount = ccPayment.getAutoPayAmountMax();

				payment = new TVPayment(amount, Frequency.TOTAL_BALANCE, 0, cal, new TVAccountTransferSource(sou), tar);
			} else if (ccPayment.getAutoPayProcessType().equalsIgnoreCase("MINIMUM")) {
				// BigDecimal amount = new BigDecimal(1);
				payment = new TVPayment(null, Frequency.MINIMUM_BALANCE, 0, cal, new TVAccountTransferSource(sou), tar);
			}

			if (payment != null) {
				payment.getPayee().setTelenomina(false);
				PaymentResponse res = PaymentsServiceModule.getInstance().sendPayments(customerProfile, userSession,
						payment, Utils.generateCustomerInteractionEvent(request), customerService);

				if (res != null && res.getStatus() == ResultStatus.SUCCESS) {
					if (res.getPayment().getPayee().getEbpp2UserAccount() != null
							&& res.getPayment().getPayee().isHasNewEbill()) {
						customerService.updateAccountPayment(userSession.getEbpp2UserId(), res.getPayment().getPayee()
								.getEbpp2UserAccount().getBillerAccountID());
						// Alizano hide new ebill message when payment is
						// success
						// Fix Incident 1537231
						res.getPayment().getPayee().setHasNewEbill(false);
						res.getPayment().getPayee().getEbpp2UserAccount().setLastEbillPaid(true);
						// End Fix
					}
					modelAutoPay.put("AUTOPAYSEND", true);
					modelAutoPay.put("REFNUM", payment.getReferenceNumber());

					// set autopayactive flag as true
					modelAutoPay.put("AUTOPAYACTIVE", true);

					// Send Email Confirmation
					if (hasAlert) {
						AutoPay autoPay = new AutoPay();

						autoPay.setAcctToLast4Number(ccPayment.getAccountInfo().getAccountNumber()
								.substring(ccPayment.getAccountInfo().getAccountNumber().length() - 4));
						autoPay.setAcctToNick(ccPayment.getAccountInfo().getNickname());

						TVFrontendAccount autoPayAccountFrom = Utils.getAccountById(ccPayment.getAutoPayAccountFrom(),
								userSession.getAccounts());

						autoPay.setAcctFromLast4Number(autoPayAccountFrom.getAccountNumber().substring(
								autoPayAccountFrom.getAccountNumber().length() - 4));
						autoPay.setAcctFromNick(autoPayAccountFrom.getNickname());

						autoPay.setAutoPayDay(Integer.parseInt(ccPayment.getAutoPayDay()));
						autoPay.setAutoPayOption(ccPayment.getAutoPayProcessType());

						autoPay.setNextAutoPayDate(res.getPayment().getEffectiveDate());

						if (ccPayment.getAutoPayProcessType().equalsIgnoreCase("COMPLETE"))
							autoPay.setAutoPayAmt(ccPayment.getAutoPayAmountMax());

						// send email
						customerService.sendCcAutoPayConfirmation(customerProfile, autoPay, true,
								userSession.getPreferredLanguage());

					}
					userSession.resetPayees();
				} else {
					modelAutoPay.put("AUTOPAYSEND", false);
					modelAutoPay.put("AUTOPAYERROR", "ERROR REASON: " + res != null ? res.getErrorMessage()
							: "Null payment response");
				}
			} else {
				modelAutoPay.put("AUTOPAYSEND", false);
			}
		}

		return modelAutoPay;
	}

	private Map<String, Object> cancelAutoPay(HttpServletRequest request, CustomerProfile customerProfile,
			CCPortalPayments ccPayment, UserSession userSession, boolean hasAlert, CustomerServices customerService) {
		Map<String, Object> modelAutoPay = new HashMap<>();

		// Cancel AutoPay
		modelAutoPay.put("AUTOPAYINPROCESS", true);

		CCPortalPayments ccPaymentCancel = this.getAutoPayActiveInfo(userSession, ccPayment);

		TVPayment payment = ccPaymentCancel.getPaymentInfo();

		CancelPayeeResponse canres;
		if (payment != null) {
			payment.getPayee().setTelenomina(false);
			canres = customerService.cancelPaymentWithErrorDescription(customerProfile,
					userSession.getBankingSession(), payment, Utils.generateCustomerInteractionEvent(request));

			if (canres.getResult().equals(CancelPayeeResponse.Result.SUCCESS)) {
				modelAutoPay.put("AUTOPAYCAN", true);
				modelAutoPay.put("AUTOPAYACTIVE", false);

				// Send Email Confirmation
				if (hasAlert) {
					AutoPay autoPay = new AutoPay();

					autoPay.setAcctToLast4Number(ccPayment.getAccountInfo().getAccountNumber()
							.substring(ccPayment.getAccountInfo().getAccountNumber().length() - 4));
					autoPay.setAcctToNick(ccPayment.getAccountInfo().getNickname());

					if (!GenericValidator.isBlankOrNull(ccPayment.getPaymentInfo().getPmtDay1()))
						autoPay.setAutoPayDay(Integer.parseInt(ccPayment.getPaymentInfo().getPmtDay1()));
					else
						autoPay.setAutoPayDay(Integer.parseInt(ccPayment.getPaymentInfo().getPmtDay2()));

					autoPay.setAutoPayOption(ccPayment.getAutoPayProcessType());

					autoPay.setNextAutoPayDate(ccPayment.getAutoPayNextProcessDate());

					if (ccPayment.getPaymentInfo().getFrequency().equals(Frequency.TOTAL_BALANCE))
						autoPay.setAutoPayAmt(ccPayment.getAutoPayAmountMax());

					customerService.sendCcAutoPayConfirmation(customerProfile, autoPay, false,
							userSession.getPreferredLanguage());
				}
				userSession.resetPayees();

			} else {
				modelAutoPay.put("AUTOPAYCAN", false);
				modelAutoPay.put("AUTOPAYACTIVE", true);
			}
		}

		return modelAutoPay;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		CCPortalPayments ccMakePayment = (CCPortalPayments) command;

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile customerProfile = userSession.getCustomerProfile();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		String id = "";
		if (ccMakePayment.getAccountId() != null) {
			id = ccMakePayment.getAccountId();
		} else {
			id = request.getParameter("id");
			ccMakePayment.setAccountId(id);
		}

		TVFrontendAccount account = null;
		if (ccMakePayment.getAccountInfo() == null) {
			account = Utils.getAccountById(id, userSession.getAccounts());
			ccMakePayment.setAccountInfo(account);
		} else {
			account = ccMakePayment.getAccountInfo();
		}

		if (account != null) {
			ccMakePayment.setPayNickname(account.getNickname() + " x"
					+ account.getAccountNumber().substring(account.getAccountNumber().length() - 4));
			ccMakePayment.setPaymentDaysMap(this.getPaymentDays(account));

			TVFrontEndPayee payee = Utils.getPayeebyAccountNumber(account.getAccountNumber(),
					userSession.getPayees(true));

			if (payee != null) {
				ccMakePayment.setPayeeInfo(payee);
			}

			GlobalEntitlement entEbill = customerService.getGlobalEntitlement(EntitlementType.EBILLS);

			if (entEbill == null || entEbill.getStatus() != EntitlementStatus.ENABLED) {
				ccMakePayment.setEbillUp(false);
			} else {
				ccMakePayment.setEbillUp(true);
				// ccMakePayment.setEbppInfo(this.getEbillNewStatement(account.getAccountNumber(),
				// userSession));
			}
		}

		ccMakePayment.setAccountFromPaymentMapRegular(this.getAccountFromPaymentMap(request, userSession, true));
		ccMakePayment.setAccountFromPaymentMapAutoPay(this.getAccountFromPaymentMap(request, userSession, false));
		ccMakePayment.setAutoPayPaymentProcessMap(this.getPaymentProcess(userSession));

		String ccAction = request.getParameter("formAction");

		boolean hasalert = false;
		if (customerProfile.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement ent = new AlertsEntitlement(customerProfile.getEntitlementByType(EntitlementType.ALERTS));
			if (ent.getAlerts().contains(AlertType.PAYMENTS_EMAIL) || ent.getAlerts().contains(AlertType.PAYMENTS_SMS)
					|| ent.getAlerts().contains(AlertType.PAYMENTS_PUSH)) {
				hasalert = true;
			}

		}

		if (ccAction.equalsIgnoreCase("singlepay")) {
			Map<String, Object> modelSinglePayment = this.makeSinglePayment(ccMakePayment, userSession,
					customerProfile, customerService, request, hasalert);
			model.putAll(modelSinglePayment);
		} else if (ccAction.equalsIgnoreCase("autopay")) {
			Map<String, Object> modelAutoPay = this.activateAutoPay(request, customerProfile, ccMakePayment,
					userSession, hasalert, customerService);
			model.putAll(modelAutoPay);
		} else if (ccAction.equalsIgnoreCase("cancelAutopay")) {
			Map<String, Object> modelAutoPay = this.cancelAutoPay(request, customerProfile, ccMakePayment, userSession,
					hasalert, customerService);
			model.putAll(modelAutoPay);
		}

		return showForm(request, errors, "ccMakePayment", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		CCPortalPayments ccMakePayment = (CCPortalPayments) command;

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();

		GlobalEntitlement ent = customerService.getGlobalEntitlement(EntitlementType.BILLPAY);

		if (ent == null || ent.getStatus() != EntitlementStatus.ENABLED) {
			model.put("DOWNPAYMENT", true);
			// return model;
		}

		// Set holidays
		Map<String, Object> h = new LinkedHashMap<String, Object>();
		List<Holiday> holidays = customerService.getHolidays();
		if (holidays != null)
			for (Holiday holiday : holidays) {
				h.put(holiday.getId(),
						new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(holiday.getDate().getTime()));
			}
		model.put("holidays", h);

		String id = "";
		if (ccMakePayment.getAccountId() != null) {
			id = ccMakePayment.getAccountId();
		} else {
			id = request.getParameter("id");
			ccMakePayment.setAccountId(id);
		}

		if (id != null && !id.equalsIgnoreCase(""))
			model.put("accountId", id);

		TVFrontendAccount account = null;
		if (ccMakePayment.getAccountInfo() == null) {
			account = Utils.getAccountById(id, userSession.getAccounts());
			ccMakePayment.setAccountInfo(account);
		} else {
			account = ccMakePayment.getAccountInfo();
		}

		if (account != null) {
			ccMakePayment.setPayNickname(account.getNickname() + " x"
					+ account.getAccountNumber().substring(account.getAccountNumber().length() - 4));
			ccMakePayment.setPaymentDaysMap(this.getPaymentDays(account));

			TVFrontEndPayee payee = Utils.getPayeebyAccountNumber(account.getAccountNumber(),
					userSession.getPayessBasicEbpp(true));

			if (payee != null) {
				TVFrontendAccount reserveAct = Utils.getAccountByAccountNumber(payee.getBillingAccount().trim(),
						userSession.getAccounts());
				if (reserveAct != null && reserveAct.getPortalBalance() != null) {
					payee.setCurrentBalance(reserveAct.getPortalBalance());
				}
				ccMakePayment.setPayeeInfo(payee);
				ccMakePayment = this.getAutoPayActiveInfo(userSession, ccMakePayment);
				if (ccMakePayment.getAutoPayNextProcessDate() != null)
					model.put("AUTOPAYACTIVE", true);
				else
					model.put("AUTOPAYACTIVE", false);
			}

			GlobalEntitlement entEbill = customerService.getGlobalEntitlement(EntitlementType.EBILLS);

			if (entEbill.getStatus() != EntitlementStatus.ENABLED || !isEbppAvailable()) {
				ccMakePayment.setEbillUp(false);
			} else {
				ccMakePayment.setEbillUp(true);
				// ccMakePayment.setEbppInfo(this.getEbillNewStatement(account.getAccountNumber(),
				// userSession));
			}
		}

		ccMakePayment.setAccountFromPaymentMapRegular(this.getAccountFromPaymentMap(request, userSession, true));
		ccMakePayment.setAccountFromPaymentMapAutoPay(this.getAccountFromPaymentMap(request, userSession, false));
		ccMakePayment.setAutoPayPaymentProcessMap(this.getPaymentProcess(userSession));

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99


		return model;

	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		CCPortalPayments ccMakePayment = (CCPortalPayments) command;
		String ccAction = request.getParameter("formAction");
		boolean allFieldsReqSingle = false;
		boolean allFieldsReqAuto = false;

		if (ccAction.equalsIgnoreCase("singlepay")) {
			if (ccMakePayment.getSinglePayAccountFrom().equalsIgnoreCase("none") && !allFieldsReqSingle) {
				allFieldsReqSingle = true;
				errors.rejectValue("errorSinglePayment", "credit.card.portal.tab.cc.services.fields.required",
						"All Fields Are Required.");
			}
			if (ccMakePayment.getSinglePayDate() != null) {
				Calendar today = Calendar.getInstance();

				// Set the max date (one year in future)
				Calendar thresholdDate = Calendar.getInstance();
				thresholdDate.add(Calendar.YEAR, 1);

				// Validate no future dates
				Calendar n = Calendar.getInstance();
				n.setTime(ccMakePayment.getSinglePayDate());

				if ((n.get(Calendar.YEAR) < today.get(Calendar.YEAR))
						|| (n.get(Calendar.YEAR) == today.get(Calendar.YEAR))
						&& (n.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR))) {
					errors.rejectValue("errorSinglePayment", "credit.card.portal.tab.make.payment.single.past",
							"Invalid Date.<br>(Day in the past)");
				} else if (n.after(thresholdDate)) {
					errors.rejectValue("errorSinglePayment", "credit.card.portal.tab.make.payment.single.future",
							"Invalid Date.<br>(Date more than one year in future)");
				}
			} else if (!allFieldsReqSingle) {
				allFieldsReqSingle = true;
				errors.rejectValue("errorSinglePayment", "credit.card.portal.tab.cc.services.fields.required",
						"All Fields Are Required.");
			}
			if (ccMakePayment.getSinglePayAmount() != null) {
				// dsalas cibp-2608 se elimina la validacion de valor maximo
				// (10000)
				if (ccMakePayment.getSinglePayAmount().compareTo(new BigDecimal(0)) <= 0) {
					errors.rejectValue("errorSinglePayment", "payments.amt.invalid", "Amount Invalid");
				}
			} else if (!allFieldsReqSingle) {
				allFieldsReqSingle = true;
				errors.rejectValue("errorSinglePayment", "credit.card.portal.tab.cc.services.fields.required",
						"All Fields Are Required.");
			}
		} else if (ccAction.equalsIgnoreCase("autopay")) {
			if (ccMakePayment.getAutoPayAccountFrom().equalsIgnoreCase("none")) {
				if (!allFieldsReqAuto) {
					allFieldsReqAuto = true;
					errors.rejectValue("errorAutoPay", "credit.card.portal.tab.cc.services.fields.required",
							"All Fields Are Required.");
				}
			}
			if (ccMakePayment.getAutoPayProcessType() != null
					&& ccMakePayment.getAutoPayProcessType().equalsIgnoreCase("COMPLETE")) {
				if (ccMakePayment.getAutoPayAmountMax() != null) {
					BigDecimal maxpayamount = new BigDecimal(10000.00);
					if (ccMakePayment.getAutoPayAmountMax().compareTo(maxpayamount) > 0) {
						errors.rejectValue("errorAutoPay", "payments.amount.largermax", "Exceeds max amount");
					} else if (ccMakePayment.getAutoPayAmountMax().compareTo(new BigDecimal(1)) <= 0) {
						errors.rejectValue("errorAutoPay",
								"credit.card.portal.tab.make.payment.autopay.minimumamt.error",
								"Please, enter an amount higher than $1.00");
					}
				}
			}
		}
	}

	private boolean isEbppAvailable() {
		boolean ebppStatus = false;

		if (this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
				.equals(EntitlementStatus.ENABLED.toString())) {
			ebppStatus = true;
		} else {
			ebppStatus = false;
		}

		return ebppStatus;
	}

	/**
	 * @param messageSource
	 *            the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}
}
