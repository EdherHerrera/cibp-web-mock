package evertec.cibp.web.form;

import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.requests.adaptiveauth.EnrollmentInformationinRSARequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionSelectionResponse;
import evertec.cibp.web.form.bean.ValidateInfoBean;
import evertec.cibp.web.form.bean.ValidateInfoBean.ACTION;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class LoginRsaEnrollFromOAOForm extends AbstractWizardFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private MessageSource messageSource;

	public LoginRsaEnrollFromOAOForm() {
		super();
		setCommandClass(ValidateInfoBean.class);
		setCommandName("loginRsaEnrollFromOAO");
		setPages(new String[] { "loginRsaEnrollFromOAO", "loginRsaEnrollFromOAO" });
	}

	@Override
	protected int getTargetPage(HttpServletRequest request, Object command, Errors errors, int currentPage) {

		return super.getTargetPage(request, command, errors, currentPage);
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		ValidateInfoBean info = (ValidateInfoBean) command;
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		DeviceRequest deviceRequest = session.getDeviceRequest();

		// Add to RSA
		String q1 = info.getQuestion1();
		String q2 = info.getQuestion2();
		String q3 = info.getQuestion3();
		String a1 = info.getAnswer1();
		String a2 = info.getAnswer2();
		String a3 = info.getAnswer3();
		String computerType = info.getComputerType();
		boolean saveDevice = info.isSaveDevice();
		if ("private".equals(computerType))
			saveDevice = true;

		String username = session.getCustomerProfile() != null ? session.getCustomerProfile().getUsername()
				.toLowerCase() : session.getUsername().toLowerCase();

		EnrollmentInformationinRSARequest req = new EnrollmentInformationinRSARequest(
				Utils.generateCustomerInteractionEvent(request), username,
				session.getCustomerProfile() != null ? session.getCustomerProfile().getEmail() : "", saveDevice,deviceRequest);
		req.setQuestionsAndAnswer( q1,q2, q3, a1, a2, a3);

		ModifyCustomerProfileResponse enrollRSAresponse = session.getCustomerService().getSignOnCustomerServiceModule().enrollRSA(req);
		if (!ResultStatus.SUCCESS.equals(enrollRSAresponse.getStatus())) {
			error.reject("enrollment.invalid", "Please verify your input information");
			return showPage(request, error, 0);
		}

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
		cookie.setPath("/");

		ESAPI.httpUtilities().addCookie(response, cookie);
		info.actionsProcessed.add(ACTION.QUESTIONS);
		info.actionsProcessed.add(ACTION.CONTRACT);

		List<ASK_FOR> tasksList = session.getTasksList();
		if (tasksList.contains(ASK_FOR.RSA_ENROLL_FROM_OAO)) {
			List<ACTION> removetasks = info.getActionsProcessed();
			if (removetasks.contains(ACTION.QUESTIONS) && removetasks.contains(ACTION.CONTRACT)) {
				tasksList.remove(ASK_FOR.RSA_ENROLL_FROM_OAO);
			}
		}

		return new ModelAndView(SignonSequenceUtils.getNextView(session));
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		ValidateInfoBean info = (ValidateInfoBean) command;

		ValidateInfoBean.ACTION action = info.getAction();

		if (page == 0) {

			if (action == ValidateInfoBean.ACTION.QUESTIONS) {

				String q1 = info.getQuestion1();
				String q2 = info.getQuestion2();
				String q3 = info.getQuestion3();
				String a1 = info.getAnswer1();
				String a2 = info.getAnswer2();
				String a3 = info.getAnswer3();
				String a1c = info.getAnswer1confirm();
				String a2c = info.getAnswer2confirm();
				String a3c = info.getAnswer3confirm();
				String computerType = info.getComputerType();
				String re = "^\\w+(\\s{1}\\w+)*$";

				if (GenericValidator.isBlankOrNull(q1)) {
					errors.rejectValue("question1", "enrollment.question.required", "Required");
				}
				if (GenericValidator.isBlankOrNull(q2)) {
					errors.rejectValue("question2", "enrollment.question.required", "Required");
				}
				if (GenericValidator.isBlankOrNull(q3)) {
					errors.rejectValue("question3", "enrollment.question.required", "Required");
				}

				if (GenericValidator.isBlankOrNull(a1) || !GenericValidator.matchRegexp(a1, re)) {
					errors.rejectValue("answer1", "enrollment.answer.invalid", "Please enter a valid answer");

				} else if (!GenericValidator.isBlankOrNull(a1) && !a1.equals(a1c)) {
					errors.rejectValue("answer1confirm", "enrollment.answerconfirm.notmatch",
							"Answers values must match");

				}

				if (GenericValidator.isBlankOrNull(a2) || !GenericValidator.matchRegexp(a2, re)) {
					errors.rejectValue("answer2", "enrollment.answer.invalid", "Please enter a valid answer");

				} else if (!GenericValidator.isBlankOrNull(a2) && !a2.equals(a2c)) {
					errors.rejectValue("answer2confirm", "enrollment.answerconfirm.notmatch",
							"Answers values must match");

				}

				if (GenericValidator.isBlankOrNull(a3) || !GenericValidator.matchRegexp(a3, re)) {
					errors.rejectValue("answer3", "enrollment.answer.invalid", "Please enter a valid answer.");

				} else if (!GenericValidator.isBlankOrNull(a3) && !a3.equals(a3c)) {
					errors.rejectValue("answer3confirm", "enrollment.answerconfirm.notmatch",
							"Answers values must match");

				}
				if (!"private".equals(computerType) && !"public".equals(computerType)) {
					errors.rejectValue("computerType", "enrollment.computerType.required",
							"Please select a computer type");

				}
			}
		}
		
		else if (page == 1) {

			if (GenericValidator.isBlankOrNull(info.getAgreement()) || !info.getAgreement().equalsIgnoreCase("agree")) {
				errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree");
			}
		}
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();
		Map<String, Object> model = new HashMap<>();

		model.put("customername", profile != null ? profile.getFirstName() : session.getUsername().toLowerCase());

		List<ASK_FOR> tasksList = session.getTasksList();
		for (ASK_FOR thing : tasksList) {
			switch (thing) {
			case RSA_ENROLL_FROM_OAO:
				model.put("selectquestions", true);
				model.put("showcontract", true);
				break;
			default:
				break;
			}
		}

		model.put("page", Integer.toString(page));
		Map<String, String> agree = new HashMap<String, String>();
		agree.put("agree", "");
		model.put("agree", agree);

		// Questions
		if (page == 0) {

			Map<String, String> questions = new LinkedHashMap<String, String>();
			String selectquestiontext = "Select a question:";
			if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
				selectquestiontext = "Seleccione una pregunta:";
			questions.put("", selectquestiontext);
			
			if(session.getDeviceRequest() == null){
				session.setDeviceRequest(RSAUtils.getDeviceRequest(session.getPm_fp(), null, request));
			}
			
			CustomerInteractionEvent generateCustomerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
			RSAActionRequest actionrequest = new RSAActionRequest(generateCustomerInteractionEvent,
					profile != null ? profile.getUsername().toLowerCase() : session.getUsername().toLowerCase(), "",
					profile != null ? profile.getEmail() : "", 0, false, session.getDeviceRequest());
			actionrequest.setRsaLang("sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "ES" : "EN");
			QuestionSelectionResponse questionResponse = session.getCustomerService().rsaGetQuestionsSelection(
					actionrequest);
			
			session.getDeviceRequest().setDeviceTokenCookie(questionResponse.getRsaCookie());

			if (questionResponse.getStatusResult().equals(ResultStatus.SUCCESS)) {
				Map<Object, Object> q = new HashMap<Object, Object>();
				q.put("", selectquestiontext);
				q.putAll( questionResponse.getQuestionMap1());
				model.put("questions1", q);

				Map<Object, Object> q2 = new HashMap<Object, Object>();
				q2.put("", selectquestiontext);
				q2.putAll(questionResponse.getQuestionMap2());
				questions = new LinkedHashMap<String, String>();
				questions.put("", selectquestiontext);
				model.put("questions2", q2);

				Map<Object, Object> q3 = new HashMap<Object, Object>();
				q3.put("", selectquestiontext);
				q3.putAll(questionResponse.getQuestionMap3());
				questions = new LinkedHashMap<String, String>();
				questions.put("", selectquestiontext);
				model.put("questions3", q3);
			}

		}

		return model;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}
}
