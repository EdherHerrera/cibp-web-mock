package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyRSAEntitlementRequest;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.web.form.bean.ChangeOOBBean;
import evertec.cibp.web.form.bean.EditFrecuencyOOB;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

public class EditFrecuencyOOBController extends SimpleFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public EditFrecuencyOOBController() {
		super();
		setCommandClass(EditFrecuencyOOB.class);
		setCommandName("editFrecuencyOOB");
		setFormView("editFrecuencyOOB");
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		EditFrecuencyOOB infobean = (EditFrecuencyOOB) command;

		List<ASK_FOR_OOB> listNavOOB = userSession.getTasksListOOB();
		if(listNavOOB != null && listNavOOB.contains(ASK_FOR_OOB.EDIT_CHALLENGE_FREQUENCY)){
			userSession.getTasksListOOB().remove(ASK_FOR_OOB.EDIT_CHALLENGE_FREQUENCY);
			request.setAttribute("firstTime", false);
		}
		
		if (GenericValidator.isBlankOrNull(infobean.getFrecuency())) {
			if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.RSA) != null) {
				CustomerEntitlement rsaEntitlement = userSession.getCustomerProfile()
						.getEntitlementByType(EntitlementType.RSA);
				String frecuency = rsaEntitlement.getString("ALWAYSCHALLENGE");
				infobean.setFrecuency(frecuency.toUpperCase());
			} else {
				infobean.setFrecuency("FALSE");
			}
		}
		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {

		EditFrecuencyOOB infobean = (EditFrecuencyOOB) command;

		if (GenericValidator.isBlankOrNull(infobean.getFrecuency())) {
			errors.rejectValue("error", "enrollOOB.register.required=Requerido", "Requerido");
		}
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		EditFrecuencyOOB infobean = (EditFrecuencyOOB) command;
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		CustomerServices customerservice = userSession.getCustomerService();

		if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.RSA) != null) {
			CustomerEntitlement rsaEntitlement = userSession.getCustomerProfile()
					.getEntitlementByType(EntitlementType.RSA);
			Boolean alwaysChallenge = (infobean.getFrecuency().equalsIgnoreCase("FALSE")) ? false : true;
			CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request);
			try {
				//Modify only if different
				if(!rsaEntitlement.getString("ALWAYSCHALLENGE").equals(String.valueOf(alwaysChallenge))){
					
					ModifyRSAEntitlementRequest modifyRequest = new ModifyRSAEntitlementRequest(interactionEvent,
							customerProfile, rsaEntitlement.getString("ALTERNATE_PHONE"),
							rsaEntitlement.getString("ALTERNATE_PROVIDER"), alwaysChallenge, false, rsaEntitlement.getString("CODE_HASH"), rsaEntitlement.getString("CODE_SALT"));
					customerservice.modifyCustomerRSAEntitlement(modifyRequest);
				}				
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					userSession
							.setStatusmessage("Your verification frequency for 2 Step Verification has been modified.");
				} else {
					userSession
							.setStatusmessage("La frecuencia de Verificación de 2 Pasos ha sido modificada.");
				}
				
				boolean emailSent = customerservice.sendOobUpdateEmail(customerProfile,
						userSession.getPreferredLanguage(),ChangeOOBBean.ACTION.EDIT_CHALLENGE_FREQUENCY.name());
				if(!emailSent) {
					this.logger.error(EditFrecuencyOOBController.class.getName()+":"+"Could not send oob update frequency confirmation email to " + customerProfile.getUsername() + " ("
							+ customerProfile.getEmail() + ")");
				}
			} catch (Exception e) {
				return showForm(request, errors, this.getFormView(), new HashMap<Object, Object>());
			}			
		}
		Map<String, Object> model = new HashMap<>();
		model.put("reload", "true");
		return showForm(request, errors, this.getFormView(), model);
	}

}
