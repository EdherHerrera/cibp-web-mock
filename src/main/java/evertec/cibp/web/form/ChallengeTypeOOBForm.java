package evertec.cibp.web.form;

import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.core.models.customer.oob.CustomerOOBChallengeType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RsaChallengeRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.adaptiveauth.RsaOobResponse;
import evertec.cibp.web.form.bean.ChallengeTypeOOBBean;
import evertec.cibp.web.form.bean.ChangeOOBBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChallengeTypeOOBForm extends SimpleFormController {

	protected final Log log = LogFactory.getLog(getClass());

	public ChallengeTypeOOBForm() {
		super();
		setCommandClass(ChallengeTypeOOBBean.class);
		setCommandName("challengeTypeOOB");
		setFormView("challengeTypeOOB");
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		ChallengeTypeOOBBean infobean = (ChallengeTypeOOBBean) command;

		List<ASK_FOR_OOB> listNavOOB = userSession.getTasksListOOB();
		if(listNavOOB != null && listNavOOB.contains(ASK_FOR_OOB.EDIT_CHALLENGE_TYPE)){
			userSession.getTasksListOOB().remove(ASK_FOR_OOB.EDIT_CHALLENGE_TYPE);
			request.setAttribute("firstTime", false);
		}
		
		if (GenericValidator.isBlankOrNull(infobean.getType())) {
			CustomerServices customerService = userSession.getCustomerService();
			DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
					userSession.getDeviceRequest().getDeviceTokenCookie(), request);
			//Microservice implementation
			RsaChallengeRequest rsaChallengeRequest = new RsaChallengeRequest(userSession.getUsername(),deviceRequest);

			CredentialType credentialType = customerService.rsaGetChallengeMethod(rsaChallengeRequest);
			userSession.setDeviceRequest(deviceRequest);
			if (credentialType != null && credentialType.equals(CredentialType.OOBPHONE))
				infobean.setType(CustomerOOBChallengeType.OOBPHONE.toString());
			else
				infobean.setType(CustomerOOBChallengeType.OOBSMS.toString());
		}
		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
		ChallengeTypeOOBBean infobean = (ChallengeTypeOOBBean) command;
		if (GenericValidator.isBlankOrNull(infobean.getType())) {
			errors.rejectValue("error", "challengeTypeOOB.required", "Requerido");
		}
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		
		Map<String, Object> model = new HashMap<>();

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerservice = userSession.getCustomerService();
		ChallengeTypeOOBBean infobean = (ChallengeTypeOOBBean) command;

		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);
		RSAActionRequest actionRequest = new RSAActionRequest(
				Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
				userSession.getUsername(), "", userSession.getCustomerProfile().getEmail(), 0, userSession.isRemember(),
				userSession.getDeviceRequest());

		/**
		 * MBNA-2530
		 * Method updateRSAOOB : CIBP-WEB microservice integration
		 * This logic would pass through the orquestration.
		 */
		RsaOobResponse updResponse = userSession.getCustomerService().getSignOnCustomerServiceModule().enrollRSAOOB(actionRequest, infobean.getType());

		if (updResponse.getResultStatus().equals(ResultStatus.SUCCESS)) {
			deviceRequest.setDeviceTokenCookie(updResponse.getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(updResponse.getDeviceTokenCookie());
			userSession.setDeviceRequest(deviceRequest);
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				userSession
						.setStatusmessage("Your delivery method for 2 Step Verification has been modified.");
			} else {
				userSession
						.setStatusmessage("El m&eacute;todo de env&iacute;o de Verificaci&oacute;n de 2 Pasos ha sido modificado.");
			}
			model.put("reload", "true");
			boolean emailSent = customerservice.sendOobUpdateEmail(userSession.getCustomerProfile(),
					userSession.getPreferredLanguage(),ChangeOOBBean.ACTION.EDIT_CHALLENGE_TYPE.name());
			if(!emailSent) {
				this.log.error(ChallengeTypeOOBForm.class.getName()+":"+"Could not send oob update delivery method confirmation email to " + userSession.getCustomerProfile().getUsername() + " ("
						+ userSession.getCustomerProfile().getEmail() + ")");
			}
			if (updResponse.getDeviceTokenCookie() != null) {
				Cookie cookie = new Cookie("PMData", updResponse.getDeviceTokenCookie());
				cookie.setPath("/");
				cookie.setMaxAge(31536000); 
				ESAPI.httpUtilities().addCookie(response, cookie);
			}
		} else {
			// TO DO: Error management
		}
		
		return showForm(request, errors, this.getFormView(), model);
	}

}
