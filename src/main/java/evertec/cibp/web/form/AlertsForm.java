package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.sms.beans.SMSAccount;
import bppr.sms.beans.SMSAlertCondition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerEmailBalanceAlert;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.EmailAlertCondition;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.SMSEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.AddSMSAccountRequest;
import evertec.cibp.core.services.requests.ModifyCustomerAlertsRequest;
import evertec.cibp.web.facade.AlertsFacade;
import evertec.cibp.web.form.bean.AlertsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.Utils;

public class AlertsForm extends CancellableFormController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AlertsForm.class);
	public boolean isSubmit = false;
	// CIBP-1857 ISSUE BANCA MOBILE BGARCIA VARIABLE UTILIZADAS PARA GUARDAR LOS
	// ID DE LAS ALERTAS EN EL CASO DE QUE LA PAGINA NO SE RENDERIZA
	public String smsid1;
	public String smsid2;
	// CIBP-1104
	public String email_id1;
	public String email_id2;

	/** Logger */

	public AlertsForm() {
		super();
		setCommandClass(AlertsBean.class);
		setCommandName("alerts");
		setFormView("alerts");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();
		AlertsEntitlement entitlement = null;
		
		//bgarciaSe verifica si el profile tiene el Entitlement de ALERTS , en
		//caso de no tenerlo se le agrega REF JIRA CIBP-1777
		
		if (!profile.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement alert = new AlertsEntitlement();
			customerService.getCustomerProfileService().addEntitlement(profile, alert.getCustomerEntitlement());
		}
		
		entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		Set<AlertType> customeralerts = new HashSet<AlertType>();
		if (entitlement != null && entitlement.getAlerts() != null)
			customeralerts = entitlement.getAlerts();

		ModifyCustomerAlertsRequest req = new ModifyCustomerAlertsRequest();
		req.setCustomerProfile(userSession.getCustomerProfile());
		req.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));

		AlertsBean a = (AlertsBean) command;
		if (a.isPaymentEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_EMAIL);
			}

			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				
				return new ModelAndView("text", model);
			}
		} else if (a.isPaymentModifyEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_MODIFY_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_MODIFY_EMAIL);
			}

			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
		
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			
				return new ModelAndView("text", model);
			}
		} else if (a.isPaymentCancelEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_CANCEL_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				
				return new ModelAndView("text", model);
			}
		} else if (a.isTransferEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);

				return new ModelAndView("text", model);
			}
		} else if (a.isTransferModifyEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_MODIFY_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_MODIFY_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);

				return new ModelAndView("text", model);
			}
		} else if (a.isTransferCancelEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_CANCEL_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);

				return new ModelAndView("text", model);
			}
		} else if (a.isPaymentMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_SMS);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_SMS))
					customeralerts.remove(AlertType.PAYMENTS_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);

				return new ModelAndView("text", model);
			}
		} else if (a.isPaymentModifyMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_MODIFY_SMS);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_SMS))
					customeralerts.remove(AlertType.PAYMENTS_MODIFY_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);

				return new ModelAndView("text", model);
			}
		} else if (a.isPaymentCancelMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_CANCEL_SMS);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_SMS))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_SMS);
			}

			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);

				return new ModelAndView("text", model);
			}
		} else if (a.isTransferMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_SMS);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_SMS))
					customeralerts.remove(AlertType.TRANSFERS_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);

				return new ModelAndView("text", model);
			}
		} else if (a.isTransferModifyMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_MODIFY_SMS);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_SMS))
					customeralerts.remove(AlertType.TRANSFERS_MODIFY_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		} else if (a.isTransferCancelMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_CANCEL_SMS);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_SMS))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);

				return new ModelAndView("text", model);
			}
		} else if (a.isPasswordChangeMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.CHANGE_PASS_SMS);
			} else {
				if (customeralerts.contains(AlertType.CHANGE_PASS_SMS))
					customeralerts.remove(AlertType.CHANGE_PASS_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		}

		else if (a.isBlockedPasswordEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.ACCESS_BLOCK_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.ACCESS_BLOCK_EMAIL))
					customeralerts.remove(AlertType.ACCESS_BLOCK_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		} else if (a.isBlockedPasswordMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.ACCESS_BLOCK_SMS);
			} else {
				if (customeralerts.contains(AlertType.ACCESS_BLOCK_SMS))
					customeralerts.remove(AlertType.ACCESS_BLOCK_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		}

		else if (a.isBlockedSecretQuestionsEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.QUESTIONS_BLOCK_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL))
					customeralerts.remove(AlertType.QUESTIONS_BLOCK_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		} else if (a.isBlockedSecretQuestionsMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.QUESTIONS_BLOCK_SMS);
			} else {
				if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_SMS))
					customeralerts.remove(AlertType.QUESTIONS_BLOCK_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		} else if (a.isSecretQuestionsUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.QUESTIONS_EDIT_SMS);
			} else {
				if (customeralerts.contains(AlertType.QUESTIONS_EDIT_SMS))
					customeralerts.remove(AlertType.QUESTIONS_EDIT_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		}

		// FFEIC
		else if (a.isNewDeviceEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.NEW_DEVICE_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.NEW_DEVICE_EMAIL))
					customeralerts.remove(AlertType.NEW_DEVICE_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		} else if (a.isNewDeviceMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.NEW_DEVICE_SMS);
			} else {
				if (customeralerts.contains(AlertType.NEW_DEVICE_SMS))
					customeralerts.remove(AlertType.NEW_DEVICE_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		}
		// FIN FFEIC
		else if (a.isEmailUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.EMAIL_EDIT_SMS);
			} else {
				if (customeralerts.contains(AlertType.EMAIL_EDIT_SMS))
					customeralerts.remove(AlertType.EMAIL_EDIT_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		}

		else if (a.isPhoneNumberUpdatedEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PHONE_EDIT_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.PHONE_EDIT_EMAIL))
					customeralerts.remove(AlertType.PHONE_EDIT_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		} else if (a.isPhoneNumberUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PHONE_EDIT_SMS);
			} else {
				if (customeralerts.contains(AlertType.PHONE_EDIT_SMS))
					customeralerts.remove(AlertType.PHONE_EDIT_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		}

		else if (a.isAddressUpdatedEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.ADRESS_EDIT_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.ADRESS_EDIT_EMAIL))
					customeralerts.remove(AlertType.ADRESS_EDIT_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		} else if (a.isAddressUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.ADRESS_EDIT_SMS);
			} else {
				if (customeralerts.contains(AlertType.ADRESS_EDIT_SMS))
					customeralerts.remove(AlertType.ADRESS_EDIT_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		}

		else if (a.isMobileBankingPhoneNumberUpdatedEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.MOBILE_PHONE_EDIT_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_EMAIL))
					customeralerts.remove(AlertType.MOBILE_PHONE_EDIT_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		} else if (a.isMobileBankingPhoneNumberUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.MOBILE_PHONE_EDIT_SMS);
			} else {
				if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_SMS))
					customeralerts.remove(AlertType.MOBILE_PHONE_EDIT_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
				return new ModelAndView("text", model);

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
				return new ModelAndView("text", model);
			}
		} else if (a.isAccount1EmailConfirmation() || a.isAccount2EmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;

			CustomerInteractionEvent customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
			if (al) {
				try {
					boolean mod = a.getMod().equalsIgnoreCase("true") ? true : false;
					boolean existH = false;
					boolean existL = false;
					List<CustomerEmailBalanceAlert> emailaccounts = userSession.getEmailaccounts(Utils
							.generateCustomerInteractionEvent(request));

					if (!customeralerts.contains(AlertType.BALANCE_EMAIL)) {
						customeralerts.add(AlertType.BALANCE_EMAIL);
						req.setAlerts(customeralerts);
						profile = customerService.modifyCustomerAlerts(req);
						userSession.setCustomerProfile(profile);
					}

					if (a.isAccount1EmailConfirmation()) {
						if (email_id1.length() > 0)
							a.setEmail_id1(email_id1);

						TVFrontendAccount frontact = null;
						if (a.getAccount1() != null && !"none".equalsIgnoreCase(a.getAccount1()))
							frontact = Utils.getAccountById(a.getAccount1(), userSession.getAccounts());
						if (frontact == null) {
							model.put("text", false);
							return new ModelAndView("text", model);
						}

						CustomerEmailBalanceAlert beanAlert1 = new CustomerEmailBalanceAlert();

						beanAlert1.setAccountNumber(frontact.getAccountNumber());
						beanAlert1.setAccountProductId(frontact.getProductDefinition().getProductId());
						beanAlert1.setAccountSection(frontact.getAccountSection());
						beanAlert1.setAccountSubtype(frontact.getSubtype());
						EmailAlertCondition alertCond = new EmailAlertCondition();
						if (a.getAccount1operator().equalsIgnoreCase("GT"))
							alertCond.setAmountHigh(new BigDecimal(a.getAccount1amount()));
						else
							alertCond.setAmountLow(new BigDecimal(a.getAccount1amount()));
						alertCond.setOperator(a.getAccount1operator());
						beanAlert1.setAlertCondition(alertCond);
						beanAlert1.setCustomerProfile(userSession.getCustomerProfile());
						beanAlert1.setNickname(frontact.getNickname());
						beanAlert1.setPermId(userSession.getCustomerProfile().getPermId());
						beanAlert1.setAlertposicion("H");
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							beanAlert1.setAlertlanguage("I");
						else
							beanAlert1.setAlertlanguage("E");

						if (emailaccounts != null && emailaccounts.size() > 0) {
							for (Iterator<CustomerEmailBalanceAlert> iterator = emailaccounts.iterator(); iterator
									.hasNext();) {
								CustomerEmailBalanceAlert account2 = (CustomerEmailBalanceAlert) iterator.next();
								String posicion = account2.getAlertposicion();
								if ("H".equals(posicion)) {
									existH = true;
									beanAlert1.setId(account2.getId());
								}
							}
						}

						if (mod && existH) {// / va modificar
							if (a.getEmail_id1() != null && a.getEmail_id1().trim().length() > 0) {
								beanAlert1.setId(a.getEmail_id1());
								/**
								 * MBNA-3502
								 * Method UpdateCustomerEmailBalanceAlert : CIBP-WEB microservice integration
								 * 
								 */
								//Microservice implementation
								CustomerEmailBalanceAlert beanAlertConfirmation = AlertsFacade.getInstance().updateCustomerEmailBalanceAlert(beanAlert1, userSession);
							}
						} else {
							CustomerEmailBalanceAlert beanAlertConfirmation;
							if (!existH) {
								
								/**
								 * MBNA-3499
								 * Method InsertCustomerEmailBalanceAlert : CIBP-WEB microservice integration
								 * 
								 */
								//Microservice implementation
								beanAlertConfirmation = AlertsFacade.getInstance().insertCustomerEmailBalanceAlert(beanAlert1, userSession);
								
							} else {
								/**
								 * MBNA-3502
								 * Method UpdateCustomerEmailBalanceAlert : CIBP-WEB microservice integration
								 * 
								 */
								//Microservice implementation
								beanAlertConfirmation = AlertsFacade.getInstance().updateCustomerEmailBalanceAlert(beanAlert1, userSession);
							}
						}
						userSession.resetEmailAccounts();
					}
					if (a.isAccount2EmailConfirmation()) {
						if (email_id2.length() > 0)
							a.setEmail_id2(email_id2);

						TVFrontendAccount frontact = null;
						if (a.getAccount2() != null && !"none".equalsIgnoreCase(a.getAccount2()))
							frontact = Utils.getAccountById(a.getAccount2(), userSession.getAccounts());
						if (frontact == null) {
							model.put("text", false);
							return new ModelAndView("text", model);
						}

						CustomerEmailBalanceAlert beanAlert2 = new CustomerEmailBalanceAlert();

						beanAlert2.setAccountNumber(frontact.getAccountNumber());
						beanAlert2.setAccountProductId(frontact.getProductDefinition().getProductId());
						beanAlert2.setAccountSection(frontact.getAccountSection());
						beanAlert2.setAccountSubtype(frontact.getSubtype());
						EmailAlertCondition alertCond = new EmailAlertCondition();
						if (a.getAccount2operator().equalsIgnoreCase("GT"))
							alertCond.setAmountHigh(new BigDecimal(a.getAccount2amount()));
						else
							alertCond.setAmountLow(new BigDecimal(a.getAccount2amount()));
						alertCond.setOperator(a.getAccount2operator());
						beanAlert2.setAlertCondition(alertCond);
						beanAlert2.setCustomerProfile(userSession.getCustomerProfile());
						beanAlert2.setNickname(frontact.getNickname());
						beanAlert2.setPermId(userSession.getCustomerProfile().getPermId());
						beanAlert2.setAlertposicion("L");
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							beanAlert2.setAlertlanguage("I");
						else
							beanAlert2.setAlertlanguage("E");

						if (emailaccounts != null && emailaccounts.size() > 0) {
							for (Iterator<CustomerEmailBalanceAlert> iterator = emailaccounts.iterator(); iterator
									.hasNext();) {
								CustomerEmailBalanceAlert account2 = (CustomerEmailBalanceAlert) iterator.next();
								String posicion = account2.getAlertposicion();
								if ("L".equals(posicion)) {
									existL = true;
									beanAlert2.setId(account2.getId());
								}
							}
						}

						if (mod && existL) {// / va modificar
							if (a.getEmail_id2() != null && a.getEmail_id2().trim().length() > 0) {
								beanAlert2.setId(a.getEmail_id2());
								/**
								 * MBNA-3502
								 * Method UpdateCustomerEmailBalanceAlert : CIBP-WEB microservice integration
								 * 
								 */
								//Microservice implementation
								CustomerEmailBalanceAlert beanAlertConfirmation = AlertsFacade.getInstance().updateCustomerEmailBalanceAlert(beanAlert2, userSession);
							}
						} else {
							CustomerEmailBalanceAlert beanAlertConfirmation;
							if (!existL) {
								
								/**
								 * MBNA-3499
								 * Method InsertCustomerEmailBalanceAlert : CIBP-WEB microservice integration
								 * 
								 */
								//Microservice implementation
								beanAlertConfirmation = AlertsFacade.getInstance().insertCustomerEmailBalanceAlert(beanAlert2, userSession);
								
							} else {
								/**
								 * MBNA-3502
								 * Method UpdateCustomerEmailBalanceAlert : CIBP-WEB microservice integration
								 * 
								 */
								//Microservice implementation
								beanAlertConfirmation = AlertsFacade.getInstance().updateCustomerEmailBalanceAlert(beanAlert2, userSession);
							}
						}
						userSession.resetEmailAccounts();
					}
					model.put("text", a.getAction());
					return new ModelAndView("text", model);

				} catch (RuntimeException e) {
					logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
					model.put("text", false);
					return new ModelAndView("text", model);
				}
			} else {
				CustomerEmailBalanceAlert beanAlert = new CustomerEmailBalanceAlert();

				try {
					if (a.isAccount1EmailConfirmation()) {
						TVFrontendAccount frontact = null;
						if (a.getAccount1() != null && !"none".equalsIgnoreCase(a.getAccount1()))
							frontact = Utils.getAccountById(a.getAccount1(), userSession.getAccounts());
						if (frontact == null) {
							model.put("text", false);
							return new ModelAndView("text", model);
						}

						// beanAlert.setId(frontact.getAccountNumber());
						beanAlert.setAccountNumber(frontact.getAccountNumber());
						beanAlert.setAccountProductId(frontact.getProductDefinition().getProductId());
						beanAlert.setAccountSection(frontact.getAccountSection());
						beanAlert.setAccountSubtype(frontact.getSubtype());
						beanAlert.setCustomerProfile(userSession.getCustomerProfile());
						beanAlert.setNickname(frontact.getNickname());
						beanAlert.setPermId(userSession.getCustomerProfile().getPermId());
						if (frontact.getEmailAccountBalenceAlert().get(0).getId() != null
								&& frontact.getEmailAccountBalenceAlert().get(0).getId().trim().length() > 0) {// elimina
							beanAlert.setId(frontact.getEmailAccountBalenceAlert().get(0).getId());
							/**
							 * Remove CustomerEmailBalanceAlert bean alert
							 *
							 * */
							AlertsFacade.getInstance().deleteCustomerEmailBalanceAlert(beanAlert);
							userSession.resetEmailAccounts();
							if (a.isModAlert() && customeralerts.contains(AlertType.BALANCE_EMAIL)) {
								customeralerts.remove(AlertType.BALANCE_EMAIL);
								req.setAlerts(customeralerts);
								profile = customerService.modifyCustomerAlerts(req);
								userSession.setCustomerProfile(profile);
							}
						}
					}

					if (a.isAccount2EmailConfirmation()) {
						TVFrontendAccount frontact = null;
						if (a.getAccount2() != null && !"none".equalsIgnoreCase(a.getAccount2()))
							frontact = Utils.getAccountById(a.getAccount2(), userSession.getAccounts());
						if (frontact == null) {
							model.put("text", false);
							return new ModelAndView("text", model);
						}

						// beanAlert.setId(frontact.getAccountNumber());
						beanAlert.setAccountNumber(frontact.getAccountNumber());
						beanAlert.setAccountProductId(frontact.getProductDefinition().getProductId());
						beanAlert.setAccountSection(frontact.getAccountSection());
						beanAlert.setAccountSubtype(frontact.getSubtype());
						beanAlert.setCustomerProfile(userSession.getCustomerProfile());
						beanAlert.setNickname(frontact.getNickname());
						beanAlert.setPermId(userSession.getCustomerProfile().getPermId());
						if (frontact.getEmailAccountBalenceAlert().get(0).getId() != null
								&& frontact.getEmailAccountBalenceAlert().get(0).getId().trim().length() > 0) {// elimina
							beanAlert.setId(frontact.getEmailAccountBalenceAlert().get(0).getId());
							/**
							 * Remove CustomerEmailBalanceAlert bean alert
							 * */
							AlertsFacade.getInstance().deleteCustomerEmailBalanceAlert(beanAlert);
							userSession.resetEmailAccounts();
							if (a.isModAlert() && customeralerts.contains(AlertType.BALANCE_EMAIL)) {
								customeralerts.remove(AlertType.BALANCE_EMAIL);
								req.setAlerts(customeralerts);
								profile = customerService.modifyCustomerAlerts(req);
								userSession.setCustomerProfile(profile);
							}
						}
					}
				} catch (Exception e) {
					logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
					model.put("text", !al);
					return new ModelAndView("text", model);
				}
			}

			req.setAlerts(customeralerts);

		} else {
			CustomerInteractionEvent customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
			// CIBP-1104
			boolean existH = false;
			boolean existL = false;

			if (a.isAccount1MobileConfirmation()) {
				SMSEntitlement smsent = null;
				if (profile.hasEntitlement(EntitlementType.MOBILE_BANKING))
					smsent = new SMSEntitlement(profile.getEntitlementByType(EntitlementType.MOBILE_BANKING));
				// CIBP-1857 ISSUE BANCA MOBILE BGARCIA SETAN VALORES EN MOMERIA
				if (smsid1.length() > 0)
					a.setSmsid1(smsid1);
				TVFrontendAccount frontact = null;
				if (a.getAccount1() != null && !"none".equalsIgnoreCase(a.getAccount1()))
					frontact = Utils.getAccountById(a.getAccount1(), userSession.getAccounts());
				if (frontact == null) {
					model.put("text", false);
					return new ModelAndView("text", model);
				}
				boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
				boolean mod = a.getMod().equalsIgnoreCase("true") ? true : false;

				// CIBP-1104
				List<SMSAccount> smsaccounts = userSession.getSmsaccounts(customerInteractionEvent);
				if (smsaccounts != null && smsaccounts.size() > 0) {
					for (Iterator<SMSAccount> iterator = smsaccounts.iterator(); iterator.hasNext();) {
						SMSAccount account2 = (SMSAccount) iterator.next();
						String posicion = account2.getAlertposicion();
						if ("H".equals(posicion)) {
							existH = true;
							a.setSmsid1(account2.getId());
						}
					}
				}

				if (al) {
					if (mod && existH) {// / va modificar
						// Erase the current SMS 1
						if (a.getSmsid1() != null && a.getSmsid1().trim().length() > 0) {// elimina
							SMSAccount smsact = Utils.getSMSAccountById(a.getSmsid1(),
									userSession.getSmsaccounts(customerInteractionEvent));
							if (smsact != null) {
								try {
									customerService.DeleteSMSAccount(
											userSession.getSmsProfile(customerInteractionEvent), smsact.getId());// ,
																													// smsact.getShortName(),
																													// "",
																													// null,
																													// null);
									model.put("text", al);
									userSession.resetSmsAccounts();
								} catch (Exception e) {
									logger.error(
											"onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
									model.put("text", !al);
									return new ModelAndView("text", model);
								}
							}
						}
					}// luego de eliminar en caso de moficar
					AddSMSAccountRequest re = new AddSMSAccountRequest(customerInteractionEvent, profile,
							userSession.getSmsProfile(customerInteractionEvent), smsent, frontact.getAccountNumber(),
							frontact.getSubtype(), frontact.getAccountSection(), frontact.getNickname(), frontact
									.getProductDefinition().getProductId(), "H");
					re.setAddAlert(true);
					re.setCondition(a.getAccount1operator());
					if (a.getAccount1operator().equalsIgnoreCase("GT"))
						re.setConditionHigh(new BigDecimal(a.getAccount1amount()));
					else
						re.setConditionLow(new BigDecimal(a.getAccount1amount()));
					try {
						SMSAccount smsact = customerService.addSMSAccountWithAlertCondition(re);
						if (smsact == null) {
							model.put("text", false);
							return new ModelAndView("text", model);
						} else {
							// CIBP-1857 ISSUE BANCA MOBILE BGARCIA SETAN
							// VALORES EN MOMERIA
							a.setSmsid1(smsact.getId());
							userSession.resetSmsAccounts();
							model.put("text", true);
							model.put("text", true);
							model.put("smsactid1", smsact.getId());
							model.put("smsact1", smsact);
							smsid1 = smsact.getId();
							return new ModelAndView("text", model);
						}
					} catch (Exception e) {
						logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
						model.put("text", false);
						return new ModelAndView("text", model);
					}
					// }
				} else {// viene a eliminar
					if (a.getSmsid1() != null) {
						SMSAccount smsact = Utils.getSMSAccountById(a.getSmsid1(),
								userSession.getSmsaccounts(customerInteractionEvent));
						if (smsact != null) {
							try {
								customerService.DeleteSMSAccount(userSession.getSmsProfile(customerInteractionEvent),
										smsact.getId());// ,
														// smsact.getShortName(),
														// "", null,
														// null);
								model.put("text", al);
								userSession.resetSmsAccounts();
								smsid1 = "";

							} catch (Exception e) {
								logger.error(
										"onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
								model.put("text", !al);
								return new ModelAndView("text", model);
							}
						}
					} else {
						model.put("text", !al);
						return new ModelAndView("text", model);
					}
				}
			} else if (a.isAccount2MobileConfirmation()) {
				SMSEntitlement smsent = null;
				if (profile.hasEntitlement(EntitlementType.MOBILE_BANKING))
					smsent = new SMSEntitlement(profile.getEntitlementByType(EntitlementType.MOBILE_BANKING));
				TVFrontendAccount frontact = null;
				// CIBP-1857 ISSUE BANCA MOBILE BGARCIA SETAN VALORES EN MOMERIA
				if (smsid2.length() > 0)
					a.setSmsid2(smsid2);
				if (a.getAccount2() != null && !"none".equalsIgnoreCase(a.getAccount2()))
					frontact = Utils.getAccountById(a.getAccount2(), userSession.getAccounts());
				if (frontact == null) {
					model.put("text", false);
					return new ModelAndView("text", model);
				}
				boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
				boolean mod = a.getMod().equalsIgnoreCase("true") ? true : false;

				// CIBP-1104
				List<SMSAccount> smsaccounts = userSession.getSmsaccounts(customerInteractionEvent);
				if (smsaccounts != null && smsaccounts.size() > 0) {
					for (Iterator<SMSAccount> iterator = smsaccounts.iterator(); iterator.hasNext();) {
						SMSAccount account2 = (SMSAccount) iterator.next();
						String posicion = account2.getAlertposicion();
						if ("L".equals(posicion)) {
							existL = true;
							a.setSmsid2(account2.getId());
						}
					}
				}

				if (al) {// editar o insertar
					if (mod && existL) {
						// Erase the current SMS 1
						if (a.getSmsid2() != null && a.getSmsid2().trim().length() > 0) {// para
																							// editar
							SMSAccount smsact = Utils.getSMSAccountById(a.getSmsid2(),
									userSession.getSmsaccounts(customerInteractionEvent));
							if (smsact != null) {
								try {
									customerService.DeleteSMSAccount(
											userSession.getSmsProfile(customerInteractionEvent), smsact.getId());// smsact.getShortName(),
																													// "",
																													// null,
																													// null);
									model.put("text", al);
									userSession.resetSmsAccounts();
								} catch (Exception e) {
									logger.error(
											"onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
									model.put("text", !al);
									return new ModelAndView("text", model);
								}
							}
						}
					}
					AddSMSAccountRequest re = new AddSMSAccountRequest(customerInteractionEvent, profile,
							userSession.getSmsProfile(customerInteractionEvent), smsent, frontact.getAccountNumber(),
							frontact.getSubtype(), frontact.getAccountSection(), frontact.getNickname(), frontact
									.getProductDefinition().getProductId(), "L");
					re.setAddAlert(true);
					re.setCondition(a.getAccount2operator());
					if (a.getAccount2operator().equalsIgnoreCase("GT"))
						re.setConditionHigh(new BigDecimal(a.getAccount2amount()));
					else
						re.setConditionLow(new BigDecimal(a.getAccount2amount()));
					try {
						SMSAccount smsact = customerService.addSMSAccountWithAlertCondition(re);
						if (smsact == null) {
							model.put("text", false);
							return new ModelAndView("text", model);
						} else {
							// CIBP-1857 ISSUE BANCA MOBILE BGARCIA SETAN
							// VALORES EN MOMERIA
							a.setSmsid2(smsact.getId());
							model.put("text", true);
							model.put("smsactid2", smsact.getId());
							model.put("smsact2", smsact);
							smsid2 = smsact.getId();
							userSession.resetSmsAccounts();
							return new ModelAndView("text", model);
						}
					} catch (Exception e) {
						logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
						model.put("text", false);
						return new ModelAndView("text", model);
					}
					// }
				} else {// eliminar

					if (a.getSmsid2() != null) {
						SMSAccount smsact = Utils.getSMSAccountById(a.getSmsid2(),
								userSession.getSmsaccounts(customerInteractionEvent));
						if (smsact != null) {
							try {
								customerService.DeleteSMSAccount(userSession.getSmsProfile(customerInteractionEvent),
										smsact.getId());// ,
														// smsact.getShortName(),
														// "", null,
														// null);
								model.put("text", al);
								userSession.resetSmsAccounts();
								smsid2 = "";
								return new ModelAndView("text", model);
							} catch (Exception e) {
								logger.error(
										"onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
								model.put("text", !al);
								return new ModelAndView("text", model);
							}
						}
					} else {
						model.put("text", !al);
						return new ModelAndView("text", model);
					}
				}

			}
		}
		model.put("text", false);
		return new ModelAndView("text", model);

	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		AlertsBean bean = (AlertsBean) command;

		if (userSession.getGuidedSetupSession() != null && userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, true);
		} else {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, userSession.isInformationChallenge());
		}

		Boolean showUpdate = (Boolean) WebUtils.getSessionAttribute(request,
				ChallengeInformationUtils.SHOW_UPDATE_ALERT);

		if (showUpdate != null && showUpdate) {
			model.put("showUpdate", true);
		} else {
			model.put("showUpdate", false);
		}

		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.SHOW_UPDATE_ALERT, null);

		// CIBP-1857 ISSUE BANCA MOBILE BGARCIA INICIALIZAN VARIABLE UTILIZADAS
		// PARA GUARDAR LOS ID DE LAS ALERTAS EN EL CASO DE QUE LA PAGINA NO SE
		// RENDERIZA
		smsid1 = "";
		smsid2 = "";
		email_id1 = "";
		email_id2 = "";

		AccountMaskUtil maskutil = new AccountMaskUtil();
		Map<Object, Object> accountsMap = new LinkedHashMap<Object, Object>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			accountsMap.put("none", "Select an account");
		else
			accountsMap.put("none", "Seleccione una cuenta");

		List<TVFrontendAccount> accounts = userSession.getAccounts();
		for (TVFrontendAccount account : accounts) {
			if (!account.getHide()) { // CIBP-1104 para mostrar solo las activas
				if (account.getFeatures().isMobileBanking()) {
					String sux = "";
					if (account.getAccountNumberSuffix() != null)
						sux = account.getAccountNumberSuffix();
					accountsMap.put(account.getFrontEndId(),
							account.getNickname() + " " + maskutil.mask(account.getAccountNumber()) + " " + sux);
				}
			}
		}
		model.put("accounts", accountsMap);

		Map<Object, Object> operatorsMap = new LinkedHashMap<Object, Object>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			operatorsMap.put("GT", "More than");
			operatorsMap.put("LT", "Less than");
		} else {
			operatorsMap.put("GT", "Mayor de");
			operatorsMap.put("LT", "Menor de");
		}
		model.put("operators", operatorsMap);

		AlertsEntitlement entitlement = null;
		if (profile.hasEntitlement(EntitlementType.ALERTS))
			entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		Set<AlertType> customeralerts = new HashSet<AlertType>();
		if (entitlement != null && entitlement.getAlerts() != null)
			customeralerts = entitlement.getAlerts();

		if (customeralerts.contains(AlertType.PAYMENTS_EMAIL))
			model.put("payemail", true);

		if (customeralerts.contains(AlertType.TRANSFERS_EMAIL))
			model.put("trnemail", true);

		if (customeralerts.contains(AlertType.PAYMENTS_SMS))
			model.put("paysms", true);

		if (customeralerts.contains(AlertType.TRANSFERS_SMS))
			model.put("trnsms", true);

		if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_EMAIL))
			model.put("payModemail", true);

		if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_EMAIL))
			model.put("trnModemail", true);

		if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_SMS))
			model.put("payModsms", true);

		if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_SMS))
			model.put("trnModsms", true);

		if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_EMAIL))
			model.put("payCanemail", true);

		if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_EMAIL))
			model.put("trnCanemail", true);

		if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_SMS))
			model.put("payCansms", true);

		if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_SMS))
			model.put("trnCansms", true);

		if (customeralerts.contains(AlertType.CHANGE_PASS_EMAIL))
			model.put("chaPassemail", true);

		if (customeralerts.contains(AlertType.CHANGE_PASS_SMS))
			model.put("chaPasssms", true);

		if (customeralerts.contains(AlertType.ACCESS_BLOCK_EMAIL))
			model.put("accBlockemail", true);

		if (customeralerts.contains(AlertType.ACCESS_BLOCK_SMS))
			model.put("accBlocksms", true);

		if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL))
			model.put("qstBlockemail", true);

		if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_SMS))
			model.put("qstBlocksms", true);

		if (customeralerts.contains(AlertType.QUESTIONS_EDIT_EMAIL))
			model.put("qstEditemail", true);

		if (customeralerts.contains(AlertType.QUESTIONS_EDIT_SMS))
			model.put("qstEditsms", true);

		if (customeralerts.contains(AlertType.EMAIL_EDIT_EMAIL))
			model.put("emailEditemail", true);

		if (customeralerts.contains(AlertType.EMAIL_EDIT_SMS))
			model.put("emailEditsms", true);

		if (customeralerts.contains(AlertType.PHONE_EDIT_EMAIL))
			model.put("phoneEditemail", true);

		if (customeralerts.contains(AlertType.PHONE_EDIT_SMS))
			model.put("phoneEditsms", true);

		if (customeralerts.contains(AlertType.ADRESS_EDIT_EMAIL))
			model.put("adressEditemail", true);

		if (customeralerts.contains(AlertType.ADRESS_EDIT_SMS))
			model.put("adressEditsms", true);

		if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_EMAIL))
			model.put("mphoneEditemail", true);

		if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_SMS))
			model.put("mphoneEditsms", true);

		// FFEIC
		if (customeralerts.contains(AlertType.NEW_DEVICE_EMAIL))
			model.put("newDeviceemail", true);

		if (customeralerts.contains(AlertType.NEW_DEVICE_SMS))
			model.put("newDevicesms", true);
		// FIN FFEIC

		// //CIBP-1104 Balance correo electronico
		if (customeralerts.contains(AlertType.BALANCE_EMAIL)) {
			List<CustomerEmailBalanceAlert> emailaccounts = userSession.getEmailaccounts(Utils
					.generateCustomerInteractionEvent(request));
			int j = 1;
			if (emailaccounts != null && emailaccounts.size() > 0) {
				for (Iterator<CustomerEmailBalanceAlert> iterator = emailaccounts.iterator(); iterator.hasNext();) {
					CustomerEmailBalanceAlert account2 = (CustomerEmailBalanceAlert) iterator.next();
					EmailAlertCondition cond = account2.getAlertCondition();
					if (j <= 2 && cond != null) {
						TVFrontendAccount frontendAccount = new TVFrontendAccount(account2.getAccountSubtype(),
								account2.getAccountNumber(), account2.getAccountSection());
						TVFrontendAccount actid = Utils.getAccountByKey(frontendAccount.getAccountKey(), accounts);
						if (account2.getAlertCondition().getAmountHigh() != null
								|| account2.getAlertCondition().getAmountLow() != null) {
							if (account2.getAlertposicion() != null && account2.getAlertposicion().equals("H")) {
								bean.setAccount1(actid.getFrontEndId());
								bean.setAccount1operator(account2.getAlertCondition().getOperator());
								if (account2.getAlertCondition().getAmountHigh() != null)
									bean.setAccount1amount(account2.getAlertCondition().getAmountHigh().toString());
								else if (account2.getAlertCondition().getAmountLow() != null)
									bean.setAccount1amount(account2.getAlertCondition().getAmountLow().toString());
								model.put("emailbalanceact1", true);
								model.put("email_act1", account2);
								bean.setEmail_id1(account2.getId());
								email_id1 = account2.getId();
								// model.put("smsact1", account2);
								// bean.setSmsid1(account2.getId());
								// smsid1 = account2.getId();
							} else if (account2.getAlertposicion() != null && account2.getAlertposicion().equals("L")) {
								bean.setAccount2(actid.getFrontEndId());
								bean.setAccount2operator(account2.getAlertCondition().getOperator());
								if (account2.getAlertCondition().getAmountHigh() != null)
									bean.setAccount2amount(account2.getAlertCondition().getAmountHigh().toString());
								else if (account2.getAlertCondition().getAmountLow() != null)
									bean.setAccount2amount(account2.getAlertCondition().getAmountLow().toString());
								model.put("emailbalanceact2", true);
								model.put("email_act2", account2);
								bean.setEmail_id2(account2.getId());
								email_id2 = account2.getId();
								// model.put("smsact2", account2);
								// bean.setSmsid1(account2.getId());
								// smsid2 = account2.getId();
							}
							j = j + 1;
						}
					}
				}// for
				if ("".equals(email_id2)) {
					model.put("emailbalanceact2", false);
				}
				if ("".equals(email_id1)) {
					model.put("emailbalanceact1", false);
				}
			} else {
				model.put("emailbalanceact2", false);
				model.put("emailbalanceact1", false);
			}
		}

		if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)) {
			model.put("mobilealert", true);

			List<SMSAccount> smsaccounts = userSession.getSmsaccounts(Utils.generateCustomerInteractionEvent(request));
			int i = 1;
			if (smsaccounts != null && smsaccounts.size() > 0) { // CIBP-1104
				for (Iterator<SMSAccount> iterator = smsaccounts.iterator(); iterator.hasNext();) {
					SMSAccount account2 = (SMSAccount) iterator.next();
					SMSAlertCondition cond = account2.getAlertCondition();
					if (i <= 2 && cond != null) {

						TVFrontendAccount frontendAccount = new TVFrontendAccount(account2.getAccountSubtype(),
								account2.getAccountNumber(), account2.getAccountSection());

						TVFrontendAccount actid = Utils.getAccountByKey(frontendAccount.getAccountKey(), accounts);
						if (account2.getAlertCondition().getAmountHigh() != null
								|| account2.getAlertCondition().getAmountLow() != null) {
							if (account2.getAlertposicion() != null && account2.getAlertposicion().equals("H")) {
								bean.setAccount1(actid.getFrontEndId());
								bean.setAccount1operator(account2.getAlertCondition().getOperator());
								if (account2.getAlertCondition().getAmountHigh() != null)
									bean.setAccount1amount(account2.getAlertCondition().getAmountHigh().toString());
								else if (account2.getAlertCondition().getAmountLow() != null)
									bean.setAccount1amount(account2.getAlertCondition().getAmountLow().toString());
								model.put("smsactid1", actid.getFrontEndId());
								model.put("smsact1", account2);
								bean.setSmsid1(account2.getId());
								smsid1 = account2.getId();// CIBP-1857 ISSUE
															// BANCA MOBILE
															// BGARCIA SE CARGA
															// VALOR INICIAL
															// ALERTA 1
								i = i + 1;
							} else if (account2.getAlertposicion() != null && account2.getAlertposicion().equals("L")) {
								bean.setAccount2(actid.getFrontEndId());
								bean.setAccount2operator(account2.getAlertCondition().getOperator());
								if (account2.getAlertCondition().getAmountHigh() != null)
									bean.setAccount2amount(account2.getAlertCondition().getAmountHigh().toString());
								else if (account2.getAlertCondition().getAmountLow() != null)
									bean.setAccount2amount(account2.getAlertCondition().getAmountLow().toString());
								bean.setSmsid2(account2.getId());
								model.put("smsactid2", actid.getFrontEndId());
								model.put("smsact2", account2);
								smsid2 = account2.getId();// CIBP-1857 ISSUE
															// BANCA MOBILE
															// BGARCIA SE CARGA
															// VALOR INICIAL
															// ALERTA 2
								i = i + 1;
							}

						}
					}
				}// for
			} else {
				model.put("smsactid1", false);
				model.put("smsactid2", false);
			}
		}

		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Boolean informationChallenge = userSession.isInformationChallenge();

		if (!informationChallenge
				&& (userSession.getGuidedSetupSession() == null || !userSession.getGuidedSetupSession()
						.isGuidedSetupMode())) {
			errors.rejectValue("", "", "");
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ALERTS_FRAME, true);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
			Map<String, Object> parameters = new HashMap<String, Object>();
			AlertsBean a = (AlertsBean) command;
			AlertsBean a1 = a.clone();
			if (email_id1.length() > 0)
				a1.setEmail_id1(email_id1);
			if (email_id2.length() > 0)
				a1.setEmail_id2(email_id2);
			if (smsid1.length() > 0)
				a1.setSmsid1(smsid1);
			if (smsid2.length() > 0)
				a1.setSmsid2(smsid2);
			parameters.put(ChallengeInformationUtils.ALERTS_FRAME, a1);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
					ChallengeInformationUtils.ALERTS_FRAME);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, parameters);
		}
	}

}
