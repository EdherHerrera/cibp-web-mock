/**
* RegainAccessForm
* Project: CIBP
* Company: Evertec inc
*/
package evertec.cibp.web.form;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.enums.RegainTokenResource;
import evertec.cibp.core.enums.RegainTokenStatus;
import evertec.cibp.core.models.RegainAccessToken;
import evertec.cibp.core.models.csr.CustomerSearchFilter;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerProfileStatus;
import evertec.cibp.core.models.customer.MultipleCustomerProfileResponse;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;

import evertec.cibp.crypto.RandomUtils;
import evertec.cibp.web.form.bean.ResetPasswordBean;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.services.modules.RegainAccessServiceModule;
import evertec.cibp.web.session.UserSession;

import static evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType.RA_EMAIL_TOKEN_REQUESTED;

/**
 * Regain Access Form
 *
 * @author Eduardo Agosto
 * @since july-2019
 * @see evertec.cibp.core.models.RegainAccessToken
 * @version 1.0
 */
public class RegainAccessForm extends CancellableFormController {

	/**
	 * Logger instance.
	 */
	protected final Log log = LogFactory.getLog(getClass()); //

	/**
	 * Main view.
	 */
	private static final String REGAIN_ACCESS = "regainaccess"; //

	/**
	 * No profile found str error.
	 */
	private static final String NO_PROFILE_FOUND = "RegainAccessForm: No customer profile found. Do not send any email."; //

	/**
	 * Profile found str success.
	 */
	private static final String PROFILE_FOUND = "Customer profile found, email sent."; //

	/**
	 * Email sent view.
	 */
	private static final String EMAIL_SENT_VIEW = "regainaccessEmailSent"; //

	/**
	 * Email bean name.
	 */
	private static final String EMAIL = "email"; //

	/**
	 * Resource String.
	 */
	private static final String RESOURCE = "resource"; //

	/**
	 * Language String.
	 */
	private static final String LANGUAGE = "language"; //
	
	/**
	 * Email blank error text.
	 */
	private static final String EMAIL_BLANK_ERROR = "regain.access.email.required"; //
	
	/**
	 * Email blank error text default.
	 */
	private static final String EMAIL_BLANK_ERROR_DEF = "Required"; //
	
	/**
	 * Email invalid error text.
	 */
	private static final String EMAIL_INVALID_ERROR = "regain.access.emailvalidation.error"; //
	
	/**
	 * Email invalid error text default.
	 */
	private static final String EMAIL_INVALID_ERROR_DEF  = "Invalid email address"; //

	/**
	 * Email error request attribute.
	 */
	private static final String EMAIL_ERROR_ATTRIBUTE  = "errorEmail"; //
	
	/**
	 * Email parameter key.
	 */
	private static final String EMAIL_PARAMETER_KEY = "regainEmail"; //
	
	/**
	 * Email blocked view.
	 */
	private static final String EMAIL_BLOCKED_VIEW = "regainaccessEmailBlocked";//
	
	/**
	 * Email no match.
	 */
	private static final String EMAIL_NO_MATCH = "regain.access.email.address.noMatch";//
	
	/**
	 * EmailConfirm bean name.
	 */
	private static final String EMAIL_CONFIRM = "emailconfirm"; //
	
	/**
	 * ResetPassword endpoint.
	 */
	private String strResetPasswordEndpoint; //

	/**
	 * CustomerService instance.
	 */
	private CustomerServices customerService; //

	/**
	 * RegainAccessServiceModule instance.
	 */
	private RegainAccessServiceModule regainAccessServiceModule; //
	
	/**
	 * Email blakclist
	 */
	private String strEmailBlackList; //
	
	/**
	 * Email Schema 
	 */
	private String strSchemaRegainAccessMail; //
	

	/**
	 * Default constructor.
	 */
	public RegainAccessForm () {
		setCommandClass(ResetPasswordBean.class);
		setCommandName(REGAIN_ACCESS);
		setFormView(REGAIN_ACCESS);
		
		this.strResetPasswordEndpoint = StringUtils.EMPTY;
		this.customerService = null;
		this.regainAccessServiceModule = null;
	}
	
	/**
	 * RegainAccessForm
	 * @param strResetPasswordEndpoint
	 * @param strCancelView
	 * @param customerService
	 * @param regainAccessServiceModule
	 * @param intCacheSeconds
	 */
	public RegainAccessForm (String strResetPasswordEndpoint, String srtEmailsBlackList,  
			String strSchemaRegainAccessMail, String strCancelView, CustomerServices customerService,
			RegainAccessServiceModule regainAccessServiceModule, int intCacheSeconds) {
		this();
		this.strResetPasswordEndpoint = strResetPasswordEndpoint;
		this.strEmailBlackList = srtEmailsBlackList;
		this.strSchemaRegainAccessMail = strSchemaRegainAccessMail;
		this.setCancelView(strCancelView);
		this.customerService = customerService;
		this.regainAccessServiceModule = regainAccessServiceModule;
		this.setCacheSeconds(intCacheSeconds);
	}

	/**
	 * Create a reference data map for the given request and command.
	 *
	 * @param request
	 * @param command
	 * @param errors
	 * @return Map<String, Object>
	 */
	@Override
	protected Map<String, Object> referenceData (HttpServletRequest request, Object command, Errors errors)
			throws ModelAndViewDefiningException{
		Map<String, Object> model = new HashMap<>(1); //	
		final String resourceParam = request.getParameter(RESOURCE); //
		RegainTokenResource tokenResourceType = RegainTokenResource.OPTIONS; // Token Resource
		boolean isSecuritiesRegain = false;
		isSecuritiesRegain =  WebUtils.getSessionAttribute(request, "regainSecurities") != null ? 
				Boolean.class.cast(WebUtils.getSessionAttribute(request, "regainSecurities")) : false;
		
		WebUtils.setSessionAttribute(request, LANGUAGE, Utils.getRSALanguage(request));
			
		if (isSecuritiesRegain) {
			throw new ModelAndViewDefiningException(new ModelAndView("redirect:forgotpasswordsec", "forgot", null));
		}
		
		if (!GenericValidator.isBlankOrNull(resourceParam)
				&& EnumUtils.isValidEnum(RegainTokenResource.class, resourceParam.toUpperCase())) {
			tokenResourceType = RegainTokenResource.valueOf(resourceParam.toUpperCase());
		}
		
		model.put(RESOURCE, tokenResourceType); //

		return model;
	}

	/**
	 * Submit callback with all parameters.
	 *
	 * @param request
	 * @param response
	 * @param command
	 * @param errors
	 * @return ModelAndView containing the new view
	 * @throws Exception
	 */
	@Override
	protected ModelAndView onSubmit (HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		final String resourceParam = request.getParameter(RESOURCE); // resource param
		final ResetPasswordBean regainAccess = ResetPasswordBean.class.cast(command); //
		
		final Map<Object, Object> model = new HashMap<>(0); //
		final String email = regainAccess.getEmail(); //
		final String emailConfirm = regainAccess.getEmailconfirm();
		final String resource = regainAccess.getResourceRegainAccess(); //
		
		final String resourceInput = "resourceRegainAccess"; // input
		final String errorDescription = "regain.access.oldresetpassword.required"; // description
		final String errorValue = "Required"; //

		String view = this.getFormView(); //
		
		if (isValidEmailInputs(email, emailConfirm, request, errors)) {
			final String validEmail = email.trim();
			if (regainAccessServiceModule.isEmailBlocked(validEmail)) {
				view = EMAIL_BLOCKED_VIEW;
			} else if (resourceParam == RegainTokenResource.OPTIONS.getValue() && GenericValidator.isBlankOrNull(resource)) {
				errors.rejectValue(resourceInput, errorDescription, errorValue);
			} else {	
				RegainTokenResource tokenResourceType = getResourceTypeToken(resourceParam, resource);
				
				if (tokenResourceType != null) {
					manageProfileToken(request, validEmail, tokenResourceType);
					view = EMAIL_SENT_VIEW;
					
					String resourceUrl = request.getPathInfo().replace("/", "");
						
					model.put(EMAIL_PARAMETER_KEY,validEmail);
					model.put("resourceUrl",resourceUrl);
				}				
			}
		}
		 

		return showForm(request, errors, view, model);
	}
	
	/**
	 * getResourceTypeToken
	 * @param resourceParam
	 * @param resource
	 * @return RegainTokenResource
	 */
	private RegainTokenResource getResourceTypeToken (String resourceParam, String resource) {
		RegainTokenResource tokenResourceType = resourceParam != null ? 
				RegainTokenResource.valueOf(resourceParam.toUpperCase()) : RegainTokenResource.OPTIONS; // token Resource
		if (tokenResourceType == RegainTokenResource.OPTIONS) {
			final String usernameResourceValue = "username"; // input value
			
			tokenResourceType = resource.equals(usernameResourceValue) ? RegainTokenResource.USERID : RegainTokenResource.PSW;
		}
		
		return tokenResourceType;
	}
	

	/**
	 * validateEmailInputs
	 * @param email
	 * @param emailConfirm
	 * @param request
	 * @param errors
	 */
	private boolean isValidEmailInputs (String email, String emailConfirm, HttpServletRequest request, BindException errors) {
		boolean isValid = false;
		
		if (foundInEmailBlackList(email)) {
			errors.rejectValue(EMAIL, EMAIL_INVALID_ERROR, EMAIL_INVALID_ERROR_DEF);
			request.setAttribute(EMAIL_ERROR_ATTRIBUTE, true);
		} else if (GenericValidator.isBlankOrNull(email)) {
			errors.rejectValue(EMAIL, EMAIL_BLANK_ERROR, EMAIL_BLANK_ERROR_DEF);
			request.setAttribute(EMAIL_ERROR_ATTRIBUTE, true);
		} else if (!GenericValidator.isEmail(email)) {
			errors.rejectValue(EMAIL, EMAIL_INVALID_ERROR, EMAIL_INVALID_ERROR_DEF);
			request.setAttribute(EMAIL_ERROR_ATTRIBUTE, true);
		} else if (GenericValidator.isBlankOrNull(emailConfirm)) {
			errors.rejectValue(EMAIL_CONFIRM, EMAIL_BLANK_ERROR, EMAIL_BLANK_ERROR_DEF);
			request.setAttribute(EMAIL_ERROR_ATTRIBUTE, true);
		} else if (!GenericValidator.isEmail(emailConfirm)) {
			errors.rejectValue(EMAIL_CONFIRM, EMAIL_INVALID_ERROR, EMAIL_INVALID_ERROR_DEF);
			request.setAttribute(EMAIL_ERROR_ATTRIBUTE, true);
		} else if (!emailConfirm.equalsIgnoreCase(email)){
			errors.rejectValue(EMAIL_CONFIRM, EMAIL_NO_MATCH,EMAIL_INVALID_ERROR_DEF);
			request.setAttribute(EMAIL_ERROR_ATTRIBUTE, true);	
		} else {
			isValid = true;
		}
		
		return isValid;
	}
	
	/*
	 * validateEmailBlackList
	 * return boolean 
	 */
	private boolean foundInEmailBlackList(String strInputEmail) {
		boolean isFound = false;
		if (!GenericValidator.isBlankOrNull(strEmailBlackList)) {
			String[] emails = strEmailBlackList.split(",");
			if (emails.length > 0) {
				for (String email : emails) {
					if (email.toLowerCase().equalsIgnoreCase(strInputEmail)) {
						isFound = true;
						break;
					}
				}
			}
		}
		
		return isFound;
	}

	/**
	 * Manges profiles tokens.
	 *
	 * @param request
	 * @param strEmail
	 * @param tokenResourceType
	 */
	protected void manageProfileToken (HttpServletRequest request, String strEmail, RegainTokenResource tokenResourceType) {
		final String token = RandomUtils.generateRandomUniqueStringUUID(); //
		
		final CustomerProfileService profileService = customerService.getCustomerProfileService(); //
		final CustomerSearchFilter filter = new CustomerSearchFilter(); //
		filter.setEmail(strEmail);
		final MultipleCustomerProfileResponse customerProfileResponse = profileService.getCustomerProfileByFilter(filter); //

		if (customerProfileResponse != null && CustomerProfileStatus.FOUND.equals(customerProfileResponse.getStatus())
				&& !customerProfileResponse.getCustomerProfiles().isEmpty()
				&& customerProfileResponse.getCustomerProfiles().get(0) != null) {

			final CustomerProfile profile = getCustomerProfile(request, customerProfileResponse, tokenResourceType); //

			RegainAccessToken accessToken = new RegainAccessToken(strEmail, token, profile.getProfileId(), tokenResourceType); //
			accessToken.setCreationDate(new Date());
			accessToken.setStatus(RegainTokenStatus.VALID);

			final String language = String.class.cast(WebUtils.getSessionAttribute(request, LANGUAGE)); //
            final 	String baseUrl = request.getRequestURL().toString().split("cibp-web")[0]
            		.replaceAll("^(https|http)", strSchemaRegainAccessMail);
			final String endpoint = generateEndpoint(baseUrl, accessToken, language, tokenResourceType);//

			customerService.saveRegainAccessTokenAndSendEmail(endpoint, accessToken, profile,
					getPreferredLanguage(language));

			regainAccessServiceModule.logRegainAccessEvent(request, accessToken, null,
					RA_EMAIL_TOKEN_REQUESTED);

			log.debug(PROFILE_FOUND);
		} else {
			log.debug(NO_PROFILE_FOUND);
		}
	}

	/**
	 * Returns a CustomerProfile when user
	 * is requesting to Regain Access other than the username.
	 *
	 * @param request
	 * @param customerProfileResponse
	 * @param tokenResourceType
	 * @return CustomerProfile
	 */
	private CustomerProfile getCustomerProfile (HttpServletRequest request, MultipleCustomerProfileResponse customerProfileResponse,
			RegainTokenResource tokenResourceType) {
		CustomerProfile profile = null; //
		final UserSession userSession = UserSession.class.cast(WebUtils.getSessionAttribute(request, "userSession")); //
		final boolean isNotUserId = (tokenResourceType != RegainTokenResource.USERID);//
		final boolean isResourceUserId = isNotUserId && (userSession != null) && (userSession.getCustomerProfile() != null); //
		
		profile = isResourceUserId? userSession.getCustomerProfile() : customerProfileResponse.getCustomerProfiles().get(0);
		
		return profile;
	}
	
	/**
	 * Generate the endpoint.
	 *
	 * @param accessToken
	 * @param strLang
	 * @param tokenResourceType
	 * @return String
	 */
	private String generateEndpoint (String baseUrl, RegainAccessToken accessToken, String strLang, RegainTokenResource tokenResourceType) {

		final String labelResource = "resource=";//
		final String labelLang = "&lang=";//
		final String labelToken = "&token=";//
		final String language = "sp".equalsIgnoreCase(strLang) ? "es" : "en";
		final String isBlocked = tokenResourceType.equals(RegainTokenResource.BLOCKED) ?
				"&passwordBlockedMobile=true" : "";
		
		return baseUrl + strResetPasswordEndpoint + 
				labelResource + tokenResourceType +
				labelLang + language + 
				labelToken + accessToken.getToken() +
				isBlocked;
	}

    /**
    * @return PreferredLanguage
    * @param strLang
    */
	public PreferredLanguage getPreferredLanguage (String strLang) {
		PreferredLanguage lang = PreferredLanguage.SPANISH; //
		final String english = "en"; //

		if (!GenericValidator.isBlankOrNull(strLang) && english.equals(strLang)) {
			lang = PreferredLanguage.ENGLISH;
		}
		return lang;
	}

	/**
	 * @return the resetPasswordEndpoint
	 */
	public String getStrResetPasswordEndpoint () {
		return strResetPasswordEndpoint;
	}

	/**
	 * @param strResetPasswordEndpoint
	 */
	public void setStrResetPasswordEndpoint (String strResetPasswordEndpoint) {
		this.strResetPasswordEndpoint = strResetPasswordEndpoint;
	}

	/**
	 * @return the customerService
	 */
	public CustomerServices getCustomerService () {
		return customerService;
	}

	/**
	 * @param customerService
	 */
	public void setCustomerService (CustomerServices customerService) {
		this.customerService = customerService;
	}

	/**
	 * @return the regainAccessServiceModule
	 */
	public RegainAccessServiceModule getRegainAccessServiceModule () {
		return this.regainAccessServiceModule;
	}

	/**
	 * @param regainAccessServiceModule
	 */
	public void setRegainAccessServiceModule (RegainAccessServiceModule regainAccessServiceModule) {
		this.regainAccessServiceModule = regainAccessServiceModule;
	}

	/**
	 * getStrEmailBlackList
	 * @return String
	 */
	public String getStrEmailBlackList() {
		return strEmailBlackList;
	}

	/**
	 * setStrEmailBlackList
	 * @param strEmailBlackList
	 */
	public void setStrEmailBlackList(String strEmailBlackList) {
		this.strEmailBlackList = strEmailBlackList;
	}

	public String getStrSchemaRegainAccessMail() {
		return strSchemaRegainAccessMail;
	}

	public void setStrSchemaRegainAccessMail(String strSchemaRegainAccessMail) {
		this.strSchemaRegainAccessMail = strSchemaRegainAccessMail;
	}
	
	

}
