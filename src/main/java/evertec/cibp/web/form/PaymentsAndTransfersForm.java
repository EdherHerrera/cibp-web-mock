package evertec.cibp.web.form;

import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.EventType;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.PushNotificationInteraction;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyCustomerAlertsRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.PaymentsAndTransfersBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.owasp.esapi.ESAPI;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PaymentsAndTransfersForm extends CancellableFormController {

	private static final String USERSESSION = "userSession";
	
	private static final String DEFAULT_VIEW_NAME = "text";
	
	private static final String MOBILE_ALERTS_VIEW_NAME = "mobileAlertsJson";
	

	private String alertsGrayedOut = "";

	public PaymentsAndTransfersForm() {
		super();
		setCommandClass(PaymentsAndTransfersBean.class);
		setCommandName("paymentsandtransfers");
		setFormView("paymentsandtransfers");
	}

	public String getAlertsGrayedOut() {
		return alertsGrayedOut;
	}

	public void setAlertsGrayedOut(String alertsGrayedOut) {
		this.alertsGrayedOut = alertsGrayedOut;
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();
		AlertsEntitlement entitlement = null;
		AlertType pushCategory = null;
		
		if (!profile.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement alert = new AlertsEntitlement();
			customerService.getCustomerProfileService().addEntitlement(profile, alert.getCustomerEntitlement());
		}
		entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		Set<AlertType> customeralerts = new HashSet<>();
		if (entitlement != null && entitlement.getAlerts() != null)
			customeralerts = entitlement.getAlerts();

		ModifyCustomerAlertsRequest req = new ModifyCustomerAlertsRequest();
		req.setCustomerProfile(userSession.getCustomerProfile());
		req.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
		
		//OOB : verify if come from OOB Challenge
		if (!userSession.getUserAgentType().equals(UserAgentType.IPHONE) && !userSession.getUserAgentType().equals(UserAgentType.ANDROID)) {
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && userSession.getOobEnroll() && userSession.isOobAuthenticated()) {
				//get the information that changes if exists.
				@SuppressWarnings("unchecked")
				Map<String, Object> parameters = (Map<String, Object>) WebUtils.getSessionAttribute(request,
						ChallengeInformationUtils.INFORMATION_PARAMETERS);
				if(parameters != null && parameters.get(ChallengeInformationUtils.PAYMENTS_TRANSFERS_ACTION) != null) {
					PaymentsAndTransfersBean responseBean = (PaymentsAndTransfersBean) parameters.get(ChallengeInformationUtils.PAYMENTS_TRANSFERS_ACTION);
					org.apache.commons.beanutils.BeanUtils.copyProperties(command, responseBean);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.SHOW_UPDATE_ALERT, userSession.isOobAuthenticated());
				}
			}
		} else {
			model.put("success", false);
		}
		
		
		PaymentsAndTransfersBean a = (PaymentsAndTransfersBean) command;

		if (a.isPaymentEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ;
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_EMAIL);
			}

			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), userSession, model);
		} else if (a.isTransferCanEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_CANCEL_FUTURE_EMAIL);
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_EMAIL);
				if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_MODIFY_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_FUTURE_EMAIL);
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_EMAIL);
				if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_MODIFY_EMAIL);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), userSession, model);
		} else if (a.isTransferCanMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_CANCEL_FUTURE_SMS);
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_PUSH)) {
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_FUTURE_PUSH);
					pushCategory = AlertType.TRANSFERS_CANCEL_FUTURE_PUSH;
				}
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_SMS))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_SMS);
				if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_SMS))
					customeralerts.remove(AlertType.TRANSFERS_MODIFY_SMS);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_SMS))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_FUTURE_SMS);
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_SMS))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_SMS);
				if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_SMS))
					customeralerts.remove(AlertType.TRANSFERS_MODIFY_SMS);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), pushCategory, false, userSession, model);
		} else if (a.isPaymentCanEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL);
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_EMAIL);
				if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_MODIFY_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL);
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_EMAIL);
				if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_MODIFY_EMAIL);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), userSession, model);
		} else if (a.isPaymentCanMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_CANCEL_FUTURE_SMS);
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_SMS))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_SMS);
				if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_SMS))
					customeralerts.remove(AlertType.PAYMENTS_MODIFY_SMS);
				// Remove Pmt Mod/Can Push, only one of these may be active
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH)) {
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH);
					pushCategory = AlertType.PAYMENTS_CANCEL_FUTURE_PUSH;
				}
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_FUTURE_SMS))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_FUTURE_SMS);
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_SMS))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_SMS);
				if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_SMS))
					customeralerts.remove(AlertType.PAYMENTS_MODIFY_SMS);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), pushCategory, false, userSession, model);
		} 
		
		else if (a.isFpsCanEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			     if(al) {
			    	 customeralerts.add(AlertType.FRAUD_PREVENTION_SERVICE_EMAIL);
			     }else {
			    	  if(customeralerts.contains(AlertType.FRAUD_PREVENTION_SERVICE_EMAIL))
			    		  customeralerts.remove(AlertType.FRAUD_PREVENTION_SERVICE_EMAIL);
			     }
			 	 req.setAlerts(customeralerts);
			     return saveAlert(req, a.getAction(), userSession, model);
		} 
		
		else if (a.isFpsMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			     if (al) {
			    	 customeralerts.add(AlertType.FRAUD_PREVENTION_SERVICE_SMS);
			    	 if (customeralerts.contains(AlertType.FRAUD_PREVENTION_SERVICE_PUSH)) {
							customeralerts.remove(AlertType.FRAUD_PREVENTION_SERVICE_PUSH);
							pushCategory = AlertType.FRAUD_PREVENTION_SERVICE_PUSH;
						}
			    	 
			     } else {
			    	  if(customeralerts.contains(AlertType.FRAUD_PREVENTION_SERVICE_SMS))
			    		  customeralerts.remove(AlertType.FRAUD_PREVENTION_SERVICE_SMS);
			     }
			 	 req.setAlerts(customeralerts);
			 	return saveAlert(req, a.getAction(), pushCategory, false, userSession, model);
		} 
		
		else if (a.isTransferEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_EMAIL);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), userSession, model);
		} else if (a.isPaymentMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_SMS);
				if (customeralerts.contains(AlertType.PAYMENTS_PUSH)) {
					customeralerts.remove(AlertType.PAYMENTS_PUSH);
					pushCategory = AlertType.PAYMENTS_PUSH;
				}
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_SMS))
					customeralerts.remove(AlertType.PAYMENTS_SMS);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), pushCategory, false, userSession, model);
		} else if (a.isTransferMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_SMS);
				if (customeralerts.contains(AlertType.TRANSFERS_PUSH)) {
					customeralerts.remove(AlertType.TRANSFERS_PUSH);
					pushCategory = AlertType.TRANSFERS_PUSH;
				}
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_SMS))
					customeralerts.remove(AlertType.TRANSFERS_SMS);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), pushCategory, false, userSession, model);
		} else if (a.isPayrollMobileConfirmation()) {
				boolean al = a.getAction().equalsIgnoreCase("true");
				if (al) {
					customeralerts.add(AlertType.PAYROLL_SMS);
					if (customeralerts.contains(AlertType.PAYROLL_PUSH)) {
						customeralerts.remove(AlertType.PAYROLL_PUSH);
						pushCategory = AlertType.PAYROLL_PUSH;
					}
				} else {
					if (customeralerts.contains(AlertType.PAYROLL_SMS))
						customeralerts.remove(AlertType.PAYROLL_SMS);
				}
				req.setAlerts(customeralerts);
				return saveAlert(req, a.getAction(), pushCategory, false, userSession, model);
			} else if (a.isAddpayrollMobileConfirmation()) {
				boolean al = a.getAction().equalsIgnoreCase("true");
				if (al) {
					customeralerts.add(AlertType.ADDPAYROLL_SMS);
					if (customeralerts.contains(AlertType.ADDPAYROLL_PUSH)) {
						customeralerts.remove(AlertType.ADDPAYROLL_PUSH);
						pushCategory = AlertType.ADDPAYROLL_PUSH;
					}
				} else {
					if (customeralerts.contains(AlertType.ADDPAYROLL_SMS))
						customeralerts.remove(AlertType.ADDPAYROLL_SMS);
				}
				req.setAlerts(customeralerts);
				return saveAlert(req, a.getAction(), pushCategory, false, userSession, model);
			} else if (a.isEditpayrollMobileConfirmation()) {
				boolean al = a.getAction().equalsIgnoreCase("true");
				if (al) {
					customeralerts.add(AlertType.EDITPAYROLL_SMS);
					if (customeralerts.contains(AlertType.EDITPAYROLL_PUSH)) {
						customeralerts.remove(AlertType.EDITPAYROLL_PUSH);
						pushCategory = AlertType.EDITPAYROLL_PUSH;
					}
				} else {
					if (customeralerts.contains(AlertType.EDITPAYROLL_SMS))
						customeralerts.remove(AlertType.EDITPAYROLL_SMS);
				}
				req.setAlerts(customeralerts);
				return saveAlert(req, a.getAction(), pushCategory, false, userSession, model);
			}else if (a.isVendorPaymentConfirmation()) {
				boolean al = a.getAction().equalsIgnoreCase("true");
				if (al) {
					customeralerts.add(AlertType.VENDOR_PAYMENT_SMS);
					if (customeralerts.contains(AlertType.VENDOR_PAYMENT_PUSH)) {
						customeralerts.remove(AlertType.VENDOR_PAYMENT_PUSH);
						pushCategory = AlertType.VENDOR_PAYMENT_PUSH;
					}
				} else {
					if (customeralerts.contains(AlertType.VENDOR_PAYMENT_SMS))
						customeralerts.remove(AlertType.VENDOR_PAYMENT_SMS);
				}
				req.setAlerts(customeralerts);
				return saveAlert(req, a.getAction(), pushCategory, false, userSession, model);
			} else if (a.isAddVendorConfirmation()) {
				boolean al = a.getAction().equalsIgnoreCase("true");
				if (al) {
					customeralerts.add(AlertType.VENDOR_ADD_SMS);
					if (customeralerts.contains(AlertType.VENDOR_ADD_PUSH)) {
						customeralerts.remove(AlertType.VENDOR_ADD_PUSH);
						pushCategory = AlertType.VENDOR_ADD_PUSH;
					}
				} else {
					if (customeralerts.contains(AlertType.VENDOR_ADD_SMS))
						customeralerts.remove(AlertType.VENDOR_ADD_SMS);
				}
				req.setAlerts(customeralerts);
				return saveAlert(req, a.getAction(), pushCategory, false, userSession, model);
			} else if (a.isEditVendorConfirmation()) {
				boolean al = a.getAction().equalsIgnoreCase("true");
				if (al) {
					customeralerts.add(AlertType.VENDOR_EDIT_DELETE_SMS);
					if (customeralerts.contains(AlertType.VENDOR_EDIT_DELETE_PUSH)) {
						customeralerts.remove(AlertType.VENDOR_EDIT_DELETE_PUSH);
						pushCategory = AlertType.VENDOR_EDIT_DELETE_PUSH;
					}
				} else {
					if (customeralerts.contains(AlertType.VENDOR_EDIT_DELETE_SMS))
						customeralerts.remove(AlertType.VENDOR_EDIT_DELETE_SMS);
				}
				req.setAlerts(customeralerts);
				return saveAlert(req, a.getAction(), pushCategory, false, userSession, model);
			}
			
		
		
		// Push Notifications
		else if (a.isPaymentPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_PUSH);
				customeralerts.remove(AlertType.PAYMENTS_SMS);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_PUSH))
					customeralerts.remove(AlertType.PAYMENTS_PUSH);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), AlertType.PAYMENTS_PUSH, al
					, userSession, model);
		} else if (a.isPaymentCanPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH);
				customeralerts.remove(AlertType.PAYMENTS_CANCEL_FUTURE_SMS);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), AlertType.PAYMENTS_CANCEL_FUTURE_PUSH, al, userSession, model);
		} else if (a.isTransferPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_PUSH);
			    customeralerts.remove(AlertType.TRANSFERS_SMS);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_PUSH))
					customeralerts.remove(AlertType.TRANSFERS_PUSH);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), AlertType.TRANSFERS_PUSH, al, userSession, model);
		} else if (a.isTransferCanPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_CANCEL_FUTURE_PUSH);
				// Remove Xfer Mod/Can SMS, only one of these may be active
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_SMS))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_FUTURE_SMS);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_PUSH))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_FUTURE_PUSH);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), AlertType.TRANSFERS_CANCEL_FUTURE_PUSH, al, userSession, model);
		} else if (a.isPayrollPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.PAYROLL_PUSH);
				customeralerts.remove(AlertType.PAYROLL_SMS);
			} else {
				customeralerts.remove(AlertType.PAYROLL_PUSH);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), AlertType.PAYROLL_PUSH, al, userSession, model);
		} else if (a.isAddpayrollPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.ADDPAYROLL_PUSH);
				customeralerts.remove(AlertType.ADDPAYROLL_SMS);
			} else {
				customeralerts.remove(AlertType.ADDPAYROLL_PUSH);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), AlertType.ADDPAYROLL_PUSH, al, userSession, model);
		} else if (a.isEditpayrollPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.EDITPAYROLL_PUSH);
				customeralerts.remove(AlertType.EDITPAYROLL_SMS);
			} else {
				customeralerts.remove(AlertType.EDITPAYROLL_PUSH);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), AlertType.EDITPAYROLL_PUSH, al, userSession, model);
		} else if (a.isVendorPaymentPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.VENDOR_PAYMENT_PUSH);
				customeralerts.remove(AlertType.VENDOR_PAYMENT_SMS);
			} else {
				customeralerts.remove(AlertType.VENDOR_PAYMENT_PUSH);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), AlertType.VENDOR_PAYMENT_PUSH, al, userSession, model);
		} else if (a.isAddVendorPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.VENDOR_ADD_PUSH);
				customeralerts.remove(AlertType.VENDOR_ADD_SMS);
			} else {
				customeralerts.remove(AlertType.VENDOR_ADD_PUSH);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), AlertType.VENDOR_ADD_PUSH, al, userSession, model);
		} else if (a.isEditVendorPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true");
			if (al) {
				customeralerts.add(AlertType.VENDOR_EDIT_DELETE_PUSH);
				customeralerts.remove(AlertType.VENDOR_EDIT_DELETE_SMS);
			} else {
				customeralerts.remove(AlertType.VENDOR_EDIT_DELETE_PUSH);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), AlertType.VENDOR_EDIT_DELETE_PUSH, al, userSession, model);
		} else if (a.isFpsPushNotifConfirmation()) {
			boolean al = Boolean.parseBoolean(a.getAction());
			if (al) {
				customeralerts.add(AlertType.FRAUD_PREVENTION_SERVICE_PUSH);
				customeralerts.remove(AlertType.FRAUD_PREVENTION_SERVICE_SMS);
			} else {
				customeralerts.remove(AlertType.FRAUD_PREVENTION_SERVICE_PUSH);
			}
			req.setAlerts(customeralerts);
			return saveAlert(req, a.getAction(), AlertType.FRAUD_PREVENTION_SERVICE_PUSH, al, userSession, model);
		}
		
		model.put("text", false);
		
		// MBSD-2005 RSA Upgrade Recommendations - Internet Payments and Transfers (Update cookie)
		Cookie cookie = WebUtils.getCookie(request, "PMData") != null ? WebUtils.getCookie(request, "PMData") : new Cookie("PMData", "");
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		ESAPI.httpUtilities().addCookie(response, cookie);

		
		return new ModelAndView(getViewName(userSession), model);

	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		CustomerProfile profile = userSession.getCustomerProfile();
		
		model.put("isEvendorActivated", StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR));
		model.put("showFraudPreventionService", StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE));
		
		AlertsEntitlement entitlement = null;
		if (profile.hasEntitlement(EntitlementType.ALERTS))
			entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		Set<AlertType> customeralerts = new HashSet<>();
		if (entitlement != null && entitlement.getAlerts() != null)
			customeralerts = entitlement.getAlerts();

		if (profile.hasEntitlement(EntitlementType.ALERTS))
			entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		if (customeralerts.contains(AlertType.PAYMENTS_EMAIL))
			model.put("payemail", true);

		if (customeralerts.contains(AlertType.TRANSFERS_EMAIL))
			model.put("trnemail", true);

		if (customeralerts.contains(AlertType.PAYMENTS_SMS))
			model.put("paysms", true);
		
		if (customeralerts.contains(AlertType.PAYROLL_SMS))
			model.put("makpayrollsms", true);
		
		if (customeralerts.contains(AlertType.ADDPAYROLL_SMS))
			model.put("addpayrollsms", true);
		
		if (customeralerts.contains(AlertType.EDITPAYROLL_SMS))
			model.put("editpayrollsms", true);
		
		if (customeralerts.contains(AlertType.TRANSFERS_SMS))
			model.put("trnsms", true);

		if (customeralerts.contains(AlertType.PAYMENTS_PUSH))
			model.put("paypush", true);

		if (customeralerts.contains(AlertType.TRANSFERS_PUSH))
			model.put("trnpush", true);

		if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)) {
			model.put("mobilealert", true);
		}

		if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL))
			model.put("paycanemail", true);

		if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_FUTURE_SMS))
			model.put("paycansms", true);

		if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH))
			model.put("paycanpush", true);

		if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_EMAIL))
			model.put("trncanemail", true);

		if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_SMS))
			model.put("trncansms", true);

		if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_PUSH))
			model.put("trncanpush", true);
		
		if (customeralerts.contains(AlertType.VENDOR_PAYMENT_SMS))
			model.put("vendorpaymentsms", true);

		if (customeralerts.contains(AlertType.VENDOR_ADD_SMS))
			model.put("vendoraddsms", true);

		if (customeralerts.contains(AlertType.VENDOR_EDIT_DELETE_SMS))
			model.put("vendoreditsms", true);

		
		if (customeralerts.contains(AlertType.FRAUD_PREVENTION_SERVICE_EMAIL))
			model.put("fpsemail", true);
		
		if (customeralerts.contains(AlertType.FRAUD_PREVENTION_SERVICE_SMS))
			model.put("fpssms", true);
		
		if (customeralerts.contains(AlertType.FRAUD_PREVENTION_SERVICE_PUSH))
			model.put("fpspush", true);
		/**
		 * oob flag and oob enroll verification 
		 * */		
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && userSession.getOobEnroll()) {
			if(userSession.getTasksListOOB() != null) {
				if (!userSession.getTasksListOOB().contains(ASK_FOR_OOB.OOB_CHALLENGE)) 
					userSession.getTasksListOOB().add(0,ASK_FOR_OOB.OOB_CHALLENGE);
				 
				if(!userSession.getTasksListOOB().contains(ASK_FOR_OOB.PAYMENTS_TRANSFERS)) 
					userSession.getTasksListOOB().add(ASK_FOR_OOB.PAYMENTS_TRANSFERS);
			} else {
				List<ASK_FOR_OOB> listNavOOB = new ArrayList<>();
				listNavOOB.add(ASK_FOR_OOB.OOB_CHALLENGE);
				listNavOOB.add(ASK_FOR_OOB.ALERT_TYPES);
				listNavOOB.add(ASK_FOR_OOB.PAYMENTS_TRANSFERS);
				userSession.setTasksListOOB(listNavOOB);
			}
			model.put("oobEnrolled", true);
			model.put("oobChallenge", "loginoob");
			userSession.setInformationChallenge(userSession.isOobAuthenticated());
		}
		
		
		if (userSession.getGuidedSetupSession() != null && userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, true);
		} else {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, userSession.isInformationChallenge());
		}

		Boolean showUpdate = (Boolean) WebUtils.getSessionAttribute(request,
				ChallengeInformationUtils.SHOW_UPDATE_ALERT);

		if (showUpdate != null && showUpdate) {
			model.put("showUpdate", true);
		} else {
			model.put("showUpdate", false);
		}

		if (userSession.getInformationQuestion() != null) {
			String question = (String) userSession.getInformationQuestion().values().toArray()[0];
			model.put("question", question);
		}

		model.put("grayed", "TRUE".equalsIgnoreCase(alertsGrayedOut));

		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.SHOW_UPDATE_ALERT, null);

		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSESSION);
		Boolean informationChallenge = userSession.isInformationChallenge();
		
		if (userSession.getUserAgentType().equals(UserAgentType.IPHONE) || userSession.getUserAgentType().equals(UserAgentType.ANDROID))
			return;
		
		if (informationChallenge != null
				&& !informationChallenge
				&& (userSession.getGuidedSetupSession() == null || !userSession.getGuidedSetupSession()
						.isGuidedSetupMode())) {

			String language = Utils.getRSALanguage(request);
			String username = userSession.getUsername();

			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && userSession.getOobEnroll()) {
				informationChallenge = userSession.isOobAuthenticated();
				userSession.setOobChallenge(true);
			
			} else if (userSession.getInformationQuestion() == null) {

				if (informationChallenge != null && !informationChallenge) {

					DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), request);
					/**
					 * MBNA-1955
					 * Method rsaAnalyze : CIBP-WEB microservice integration
					 *
					 */
					//Microservice implementation
					RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
					AnalyzeResponse anResp = userSession.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

					deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
					deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

					userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
					userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

					userSession.setDeviceRequest(deviceRequest);
					
					if (!(ActionCode.CHALLENGE == anResp.getRiskResult().getTriggeredRule().getActionCode())) {
						userSession.setInformationChallenge(true);
					} else {
						String email = null;
						if (userSession.getCustomerProfile() != null)
							email = userSession.getCustomerProfile().getEmail();

						QuestionResponse personalQuestion;

						if (userSession.getInformationQuestion() == null) {
							RSAActionRequest actionRequest = new RSAActionRequest(
									Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
									userSession.getUsername(), "", email, 0, userSession.isRemember(),
									userSession.getDeviceRequest());

							if (userSession.getCustomerProfile() != null)
								actionRequest.setUsername(userSession.getCustomerProfile().getUsername());
							else if (userSession.getUsernameReservation() != null)
								actionRequest.setUsername(userSession.getUsernameReservation().getUsername());
							else
								actionRequest.setUsername(username);

							actionRequest.setDeviceRequest(userSession.getDeviceRequest());
							actionRequest.setRsaSessionId(userSession.getRsaSessionId());
							actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());

							actionRequest.setRsaLang(Utils.getRSALanguage(request));

							personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);
							
							// MBSD-2005: RSA Upgrade Recommendations - Internet Payments and Transfers (Update device request)
							deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request);
							deviceRequest.setDeviceTokenCookie(personalQuestion.getDeviceRequest().getDeviceTokenCookie());
							deviceRequest.setDeviceTokenFSO(personalQuestion.getDeviceRequest().getDeviceTokenFSO());
							userSession.setDeviceRequest(deviceRequest);
							userSession.setRsaSessionId(personalQuestion.getSessionId());
							userSession.setRsaTransactionId(personalQuestion.getRsaTransactionId());

						} else {
							personalQuestion = new QuestionResponse();
							personalQuestion.setStatusResult(ResultStatus.SUCCESS);
							personalQuestion.setQuestion(userSession.getInformationQuestion());
						}
						if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
							userSession.setInformationQuestion(personalQuestion.getQuestion());

						} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
							throw new ModelAndViewDefiningException(new ModelAndView("error"));
						} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
							throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
						}
					}
				}
			}
			errors.rejectValue("", "", "");
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.PAYMENTS_TRANSFERS_FRAME, true);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
			Map<String, Object> parameters = new HashMap<>();
			parameters.put(ChallengeInformationUtils.PAYMENTS_TRANSFERS_ACTION, command);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
					ChallengeInformationUtils.PAYMENTS_TRANSFERS_ACTION);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, parameters);
		}
	}
	
	private ModelAndView saveAlert(ModifyCustomerAlertsRequest request, String action, AlertType pushCategory, Boolean pushAdded, 
			UserSession session, Map<String, Object> model) {
		CustomerServices customerService = session.getCustomerService();
		CustomerProfile profile = session.getCustomerProfile();
		
		try {
			profile = customerService.modifyCustomerAlerts(request);
			session.setCustomerProfile(profile);
			model.put("text", action);
			model.put("success", true);
			this.logPushNotificationEvent(true, pushCategory, pushAdded, customerService, request.getInteractionEvent());

		} catch (RuntimeException e) {
			logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e);
			model.put("text", false);
			this.logPushNotificationEvent(false, pushCategory, pushAdded, customerService, request.getInteractionEvent());
		}
		return new ModelAndView(getViewName(session), model);
	}
	
	private ModelAndView saveAlert(ModifyCustomerAlertsRequest request, String action, UserSession session, Map<String, Object> model) {
		return saveAlert(request, action, null, false, session, model);
	}
	
	private void logPushNotificationEvent(Boolean success, AlertType pushCategory, Boolean pushAdded, CustomerServices service, 
			CustomerInteractionEvent event) {
		if (pushCategory != null) {
			event.setCustomStringData(PushNotificationInteraction.CATEGORY, pushCategory.toString());
			if (pushAdded)
				service.logPushNotificationAddEvent(event, success);
			else 
				service.logPushNotificationDeleteEvent(event, success);
		}
	}
	
	/**
	 * This method return the user agent of the device
	 * @param userSession
	 * @return Mobile Device UserAgent
	 */
	private boolean isMobileDeviceUserAgent(UserSession userSession) {
		return userSession.getUserAgentType().equals(UserAgentType.IPHONE) || userSession.getUserAgentType().equals(UserAgentType.ANDROID);
	}
	
	/**
	 * Return the vieName
	 * @param userSession
	 * @return
	 */
	private String getViewName(UserSession userSession) {
		return isMobileDeviceUserAgent(userSession) ? MOBILE_ALERTS_VIEW_NAME : DEFAULT_VIEW_NAME;
	}

}
