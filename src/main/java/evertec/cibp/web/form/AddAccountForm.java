package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;

import evertec.cibp.web.form.bean.AddAggregationAccountBean;

/**
 * Class that manages input for the Add account form functionality
 */
public class AddAccountForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	
	private static final String VIEW_NAME = "productselection";

	/**
	 * Constructor for the Add Account form
	 * View and bean definition
	 */
	public AddAccountForm() {
		super();
		setCommandClass(AddAggregationAccountBean.class);
		setCommandName(VIEW_NAME);
		setFormView(VIEW_NAME);
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		return showForm(request, errors, VIEW_NAME, model);
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();

		Map<Object, Object> bankProducts = new LinkedHashMap<>();
		bankProducts.put("12345", "AAFES Credit Card Bank");
		bankProducts.put("3707", "Addison Avenue Credit Union");
		bankProducts.put("8342", "Addison Avenue Credit Union Credit Card");
		bankProducts.put("3899", "American Airlines FCU");
		bankProducts.put("1482", "American Express Blue Card");
		bankProducts.put("1002", "American Express Credit Card");
		bankProducts.put("1480", "American Express Gold Card");
		bankProducts.put("1481", "American Express Platinum Card");
		bankProducts.put("1451", "AT&T Universal Card");
		bankProducts.put("4657", "Banana Republic Credit Card");
		bankProducts.put("13487", "Banco Popular N.A. - Popular Net Banking");
		bankProducts.put("3349", "Banco Santander, Puerto Rico");
		bankProducts.put("14007", "Bank of America");
		bankProducts.put("14008", "Bank of America (Credit Card)");
		bankProducts.put("1218", "Bank of America (FL)");
		bankProducts.put("1300", "Bank One Credit Card (Cardmember Services)");
		bankProducts.put("5845", "BBVA Puerto Rico - Integral");
		model.put("bankProducts", bankProducts);

		Map<Object, Object> retirement = new LinkedHashMap<>();
		retirement.put("5920", "Banco Popular 401k");
		model.put("institutions", retirement);

		Map<Object, Object> billers = new LinkedHashMap<>();
		billers.put("3458", "AutoExpreso");
		billers.put("3464", "Autoridad de Energia Electrica");
		billers.put("3465", "Autoridad de Acueductos y Alcantarillados");
		billers.put("3434", "AT&T Mobility (Cingular)");
		model.put("billers", billers);

		Map<Object, Object> programs = new LinkedHashMap<>();
		programs.put("2813", "American Airlines AAdvantage");
		programs.put("4031", "American Express Rewards");
		programs.put("6795", "jetBlue Airways trueBlue Rewards");
		model.put("programs", programs);

		Map<Object, Object> loans = new LinkedHashMap<>();
		loans.put("3261", "American Education Services");
		loans.put("3229", "ACS Education Services");
		model.put("loans", loans);

		return model;
	}

}
