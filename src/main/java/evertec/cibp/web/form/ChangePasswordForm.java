package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rsa.csd.ws.EventType;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;

import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerPassword;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.controller.validator.PasswordConstraintValidator;
import evertec.cibp.web.form.bean.ChangePasswordBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

public class ChangePasswordForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	
	private static final String ENROLLMENT_PASSWORD_CONTAINS_USERNAME = "enrollment.pwd.contains.username";
	private static final String PASSWORD_ERROR = "password";
	private static final String PASSWORD_CONTAINS_USERNAME = "Password must not contains username";

	// jocampo FFEIC
	private Integer maxPasswordRecords;
	private  String wordListDictionary; 
	
	// FIN FFEIC

	private void initForm(){
		setCommandClass(ChangePasswordBean.class);
		setCommandName("changepassword");
		setFormView("changepassword");
	}

	public ChangePasswordForm(PropertyEditorRegistrar propertyEditorRegistrar, Integer maxPasswordRecords , String wordListDictionary) {
		super();
		initForm();
		this.setPropertyEditorRegistrar(propertyEditorRegistrar);
		this.maxPasswordRecords = maxPasswordRecords;
		this.wordListDictionary = wordListDictionary;
	}

	public ChangePasswordForm() {
		super();
		initForm();
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		ChangePasswordBean dude = (ChangePasswordBean) command;
		
		/* CIBP-1104 */
		CustomerServices customerService = userSession.getCustomerService();

		CustomerProfile customer = userSession.getCustomerProfile();
		boolean hasalert = false;
		AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
		if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
			Set<AlertType> alerts = ent.getAlerts();
			if (alerts.contains(AlertType.CHANGE_PASS_EMAIL) || alerts.contains(AlertType.CHANGE_PASS_SMS)
					|| alerts.contains(AlertType.CHANGE_PASS_PUSH)) {
				hasalert = true;
			}
		}
		/* FIN--- CIBP-1104 */

		ModifyCustomerRequest modifyCustomerRequest = new ModifyCustomerRequest(
				Utils.generateCustomerInteractionEvent(request), profile.getProfileId(), profile.getPasswordHash(),
				profile.getPasswordSalt(), "", 0, "", profile.getStatus(), null, userSession.getDeviceRequest());
		Integer generatedRandomSalt = PasswordHashUtil.genereateRandomSalt();
		String newpass = dude.getPassword();
		modifyCustomerRequest.setNewPasswordHash(PasswordHashUtil.generateSaltedHash(newpass, generatedRandomSalt));
		modifyCustomerRequest.setNewPasswordSalt(generatedRandomSalt);
		modifyCustomerRequest.setProfileId(profile.getProfileId());
		modifyCustomerRequest.setDeviceRequest(userSession.getDeviceRequest());
		ModifyCustomerProfileResponse r = userSession.getCustomerService()
				.modifyCustomerPassword(modifyCustomerRequest);
		if (ResultStatus.SUCCESS.equals(r.getStatus())) {
			// jocampo FFEIC
			CustomerPassword cp = new CustomerPassword(profile.getProfileId(), profile.getPasswordHash(), new Date(),
					profile.getPasswordSalt());
			userSession.getPasswords().add(0, cp);
			List<CustomerPassword> passwordsDelete = null;
			if (userSession.getPasswords().size() > this.maxPasswordRecords) {
				passwordsDelete = userSession.getPasswords().subList(this.maxPasswordRecords,
						userSession.getPasswords().size());
				userSession.setPasswords(userSession.getPasswords().subList(0, this.maxPasswordRecords));
			}
			customerService.getCustomerProfileService().updateCustomerPasswords(userSession.getPasswords(),
					passwordsDelete);
			// FIN FFEIC

			userSession.setCustomerProfile(r.getCustomerProfile());
			model.put("confirm", true);
			profile.setPasswordSalt(generatedRandomSalt);
			profile.setPasswordHash(PasswordHashUtil.generateSaltedHash(dude.getPassword(), generatedRandomSalt));

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Password was changed");
			else
				userSession.setStatusmessage("Contrase&ntilde;a cambiada.");
			/* CIBP-1104 se activa envio alerta de cambio de contrasena */
			if (hasalert) {
				customerService.sendChangePasswordConfirmation(customer,
						userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
						userSession.getPreferredLanguage());
			}
		} else {
			model.put("confirm", true);
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Unable to change password");
			else
				userSession.setStatusmessage("Contrase&ntilde;a no pudo ser cambiada");
		}
		
		// Set RSA cookie
		Cookie cookie = WebUtils.getCookie(request, "PMData") != null ? WebUtils.getCookie(request, "PMData") : new Cookie("PMData", "");
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		ESAPI.httpUtilities().addCookie(response, cookie);


		return showForm(request, response, error, model);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		ChangePasswordBean dude = (ChangePasswordBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		
		if (GenericValidator.isBlankOrNull(dude.getOrigpassword())) {
			errors.rejectValue("origpassword", "changepassword.orig.required", "Required");
		}
		if (GenericValidator.isBlankOrNull(dude.getPassword())) {
			errors.rejectValue("password", "changepassword.password.required", "Required");
		}
		if (GenericValidator.isBlankOrNull(dude.getPasswordconfirm())) {
			errors.rejectValue("passwordconfirm", "changepassword.confirmpassword.required", "Required");
		}
		else if (!dude.getPassword().equals(dude.getPasswordconfirm())) {
			errors.rejectValue("passwordconfirm", "changepassword.confirmpassword.mustmatch", "Fields must match");
		}

		Map<String, String> paramsPass = new HashMap<String, String>();
		paramsPass.put(ChallengeInformationUtils.NEW_PASS,dude.getPassword());
		paramsPass.put(ChallengeInformationUtils.OLD_PASS,dude.getOrigpassword());
		paramsPass.put(ChallengeInformationUtils.WORD_LIST,wordListDictionary);
		paramsPass.put(ChallengeInformationUtils.LANGUAGE,Utils.getRSALanguage(request));
		PasswordConstraintValidator passwordConstraintValidator = new PasswordConstraintValidator();
		Map<String, String> paramsResult = passwordConstraintValidator.validateRules(paramsPass,profile,Boolean.FALSE);
		if (paramsResult.get("validate").equals("false")) {
			errors.rejectValue("password", paramsResult.get("message"), paramsResult.get("message"));
		}
		

		
		if (errors.getAllErrors().isEmpty()) {

			// jocampo FFEIC
			CustomerServices customerService = userSession.getCustomerService();

			if (userSession.getPasswords() == null) {
				userSession.setPasswords(customerService.getCustomerProfileService().getCustomerPasswords(profile));
			}
			// FIN FFEIC

			int originalSalt = profile.getPasswordSalt();
			String originalHash = profile.getPasswordHash();
			String newHash = PasswordHashUtil.generateSaltedHash(dude.getOrigpassword(), originalSalt);

			if (!newHash.equals(originalHash)) {
				errors.rejectValue("origpassword", "changepassword.orig.incorrect", "Incorrect Password");
			} else {
				// jocampo FFEIC
				// NO EQUALS ACTUAL PASS
				newHash = PasswordHashUtil.generateSaltedHash(dude.getPassword(), originalSalt);
				if (newHash.equals(originalHash)) {
					errors.rejectValue("password", "changepassword.password.previously.used", "Incorrect Password");
				}
				// NO EQUALS LAST PASS
				else {
					for (int i = 0; i < userSession.getPasswords().size(); i++) {
						String newPassOldHash = PasswordHashUtil.generateSaltedHash(dude.getPassword(), userSession
								.getPasswords().get(i).getPasswordSalt());
						if (userSession.getPasswords().get(i).getPasswordHash().equals(newPassOldHash)) {
							errors.rejectValue("password", "changepassword.password.previously.used",
									"Incorrect Password");
							break;
						}
					}
				}
				// FIN FFEIC
			}
		}
		
		if (errors.getAllErrors().isEmpty()) {
			Boolean informationChallenge = false;
			
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)) {
				if (!userSession.getOobEnroll()) {
					informationChallenge = userSession.isInformationChallenge();
				} else {
					if (!userSession.isOobAuthenticated())
						userSession.setOobChallenge(true);
					informationChallenge = userSession.isOobAuthenticated();
				}
			} else {
				informationChallenge = userSession.isInformationChallenge();
			}
			
			if (informationChallenge != null && !informationChallenge) {

				// ////////////////////

				String language = Utils.getRSALanguage(request);
				String username = userSession.getUsername();
				
				if (userSession.getInformationQuestion() == null && !userSession.getOobEnroll()) {

					if (informationChallenge != null && !informationChallenge) {

						DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), request);
						//Microservice implementation
						RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
						AnalyzeResponse anResp = userSession.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);
						deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData()
								.getDeviceTokenCookie());
						deviceRequest
								.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

						userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
						userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

						userSession.setDeviceRequest(deviceRequest);

						if (!(ActionCode.CHALLENGE == anResp.getRiskResult().getTriggeredRule().getActionCode())) {
							userSession.setInformationChallenge(true);
						} else {
							String email = null;
							if (userSession.getCustomerProfile() != null)
								email = userSession.getCustomerProfile().getEmail();

							QuestionResponse personalQuestion;

							if (userSession.getInformationQuestion() == null) {
								RSAActionRequest actionRequest = new RSAActionRequest(
										Utils.generateCustomerInteractionEvent(request, userSession.getUsername()),
										userSession.getUsername(), "", email, 0, userSession.isRemember(),
										userSession.getDeviceRequest());

								if (userSession.getCustomerProfile() != null)
									actionRequest.setUsername(userSession.getCustomerProfile().getUsername());
								else if (userSession.getUsernameReservation() != null)
									actionRequest.setUsername(userSession.getUsernameReservation().getUsername());
								else
									actionRequest.setUsername(username);

								actionRequest.setDeviceRequest(userSession.getDeviceRequest());
								actionRequest.setRsaSessionId(userSession.getRsaSessionId());
								actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());

								actionRequest.setRsaLang(Utils.getRSALanguage(request));

								personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(
										actionRequest);

							} else {
								personalQuestion = new QuestionResponse();
								personalQuestion.setStatusResult(ResultStatus.SUCCESS);
								personalQuestion.setQuestion(userSession.getInformationQuestion());
							}
							if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
								userSession.setInformationQuestion(personalQuestion.getQuestion());

							} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
								throw new ModelAndViewDefiningException(new ModelAndView("error"));
							} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
								throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
							}
						}
					}
				}
				
				errors.rejectValue("", "", "");
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
				Map<String, String> parameters = new HashMap<String, String>();
				parameters.put(ChallengeInformationUtils.PASSWORD_ACTION, dude.getPassword());
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
						ChallengeInformationUtils.PASSWORD_ACTION);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, parameters);
			}
		}
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, userSession.isInformationChallenge());

		// FFEIC + SMS
		model.put("username", userSession.getUsername());

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setPath("/");
		cookie.setValue(userSession.getDeviceRequest().getDeviceTokenCookie());
		if (cookie != null)
			model.put("devicetoken", cookie.getValue());

		

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)) {
			if (!userSession.getOobEnroll()) {
				if (userSession.getInformationQuestion() != null) {
					String question = (String) userSession.getInformationQuestion().values().toArray()[0];
					model.put("question", question);
				} 
			} else {
				if (!userSession.isOobAuthenticated() && userSession.getOobChallenge() != null && userSession.getOobChallenge()) {
					model.put("oobchallenge", true);
					List<ASK_FOR_OOB> listNavOOB = new ArrayList<ASK_FOR_OOB>();
					listNavOOB.add(ASK_FOR_OOB.OOB_CHALLENGE);
					listNavOOB.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
					userSession.setTasksListOOB(listNavOOB);
					userSession.setOobChallenge(false);
				}
			}
		}else{
			if (userSession.getInformationQuestion() != null) {
					String question = (String) userSession.getInformationQuestion().values().toArray()[0];
					model.put("question", question);
			}
		}
		
		
		return model;
	}

	public Integer getMaxPasswordRecords() {
		return maxPasswordRecords;
	}

	public void setMaxPasswordRecords(Integer maxPasswordRecords) {
		this.maxPasswordRecords = maxPasswordRecords;
	}

	public String getWordListDictionary() {
		return wordListDictionary;
	}

	public void setWordListDictionary(String wordListDictionary) {
		this.wordListDictionary = wordListDictionary;
	}
	
	

}
