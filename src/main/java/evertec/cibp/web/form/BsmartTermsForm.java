package evertec.cibp.web.form;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import edu.emory.mathcs.backport.java.util.Collections;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.PayrollEntitlement;
import evertec.cibp.core.models.epayroll.PayrollStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.AccountServiceBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class BsmartTermsForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	
	private final String HAS_ERRORS = "hasError"; 
	
	public BsmartTermsForm() {
		super();
		setCommandClass(AccountServiceBean.class);
		setCommandName("accountservices");
		setFormView("bsmartTerms");
		setBindOnNewForm(true);
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		Map<String, Object> model = new LinkedHashMap<>();
		AccountServiceBean bsmartBean = (AccountServiceBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		request.getSession().removeAttribute("hasBsmartAcct");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date); // Configuramos la fecha que se recibe
		calendar.add(Calendar.DAY_OF_YEAR, 90); 
		if(bsmartBean.getBsmartAccount() == null || bsmartBean.getBsmartAccount().equalsIgnoreCase("")){
			CustomerEntitlement ent = customerProfile.getEntitlementByType(EntitlementType.PAYROLL);
			PayrollEntitlement entt = null;
			if (ent != null) {
				entt = new PayrollEntitlement(ent);
			}
			date = calendar.getTime();
			entt.setAcceptTerms(PayrollStatus.ACEPT.toString());
			entt.setLimitedDate(format.format(date));
			try {
				customerService.aceptTermsBsmartAccount(customerProfile, entt.getCustomerEntitlement());
				WebUtils.setSessionAttribute(request, "hasBeenModified", true);
			} catch (IllegalArgumentException e) {
				System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
			} catch (SQLException e) {
				System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
			}
		}
		else{
			try {
				PayrollEntitlement entt = null;
				List<TVFrontendAccount> accounts = userSession.getAccounts();
				TVFrontendAccount newAccount = null;
				for(TVFrontendAccount account : accounts){
					if(account.getFrontEndId().equalsIgnoreCase(bsmartBean.getBsmartAccount())){
						newAccount = account;
						break;
					}
				}
				CustomerEntitlement ent = customerProfile.getEntitlementByType(EntitlementType.PAYROLL);
				date = calendar.getTime();
				if(ent == null){
					entt = new PayrollEntitlement(String.valueOf(userSession.getAccounts().size()), String.valueOf(true), PayrollStatus.ACEPT.toString(), 
							format.format(date),EntitlementStatus.ENABLED);
					userSession.getCustomerService().addEntitlement(userSession.getCustomerProfile(), entt.getCustomerEntitlement());
				}
				else{
					entt = new PayrollEntitlement(ent);
					entt.setAcceptTerms(PayrollStatus.ACEPT.toString());
					entt.setAmountaccounts(String.valueOf(userSession.getAccounts().size()));
					entt.setHasbsmart(String.valueOf(true));
					entt.setLimitedDate(format.format(date));
					CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(entt.getCustomerEntitlement()));
					
				}
				customerService.modifyEnrollmentAccountInformation(customerProfile, newAccount.getAccountNumber(), newAccount.getProductDefinition().getProductType(), newAccount.getProductDefinition().getProductId());
				WebUtils.setSessionAttribute(request, "hasBeenModified", true);
			} catch (IllegalArgumentException e) {
				System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
			} 
		}
		userSession.setCustomerProfile(customerProfile);
		userSession.setEpayrollBulletNull();
		WebUtils.setSessionAttribute(request, "userSession", userSession);
		return showForm(request, error, "bsmartTerms", model);

	}

	protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		boolean hasBeenModified = WebUtils.getSessionAttribute(request, "hasBeenModified") != null ? (boolean)WebUtils.getSessionAttribute(request, "hasBeenModified") : false;
		request.getSession().removeAttribute("hasBeenModified");
		model.put("hasBeenModified", hasBeenModified);
		AccountMaskUtil mask = new AccountMaskUtil();
		Map<String, String> agree = new HashMap<>();
		agree.put("agree", "");
		model.put("agree", agree);
		
		boolean hasBsmartAcct = WebUtils.getSessionAttribute(request, "hasBsmartAcct") != null ? (boolean)WebUtils.getSessionAttribute(request, "hasBsmartAcct") : false;
		Map<String, String> accountsFromMap = new LinkedHashMap<>();
		if(hasBsmartAcct){
			if ("en".equalsIgnoreCase(Utils.getRSALanguage(request)))
				accountsFromMap.put("none", "Select an account");
			else
				accountsFromMap.put("none", "Selecciona una cuenta");
			model.put("hasBsmartAccount", true);
			List<TVFrontendAccount> accounts = userSession.getAccounts();
			for(TVFrontendAccount account : accounts){
				if(account.getAccountProductId().equalsIgnoreCase("086") && account.getAccountNumberSuffix() == null){
					accountsFromMap.put(account.getFrontEndId(),
							StringUtils.abbreviate(account.getNickname(), 15) + " "
									+ mask.mask(account.getAccountNumber())
									+ ((account.getAccountNumberSuffix() != null
											&& !account.getAccountNumberSuffix().equals(""))
													? " " + account.getAccountNumberSuffix() : ""));
				}
			}
			model.put("avalaibleBSmart", accountsFromMap);
		}
		
		String hasError = WebUtils.getSessionAttribute(request, HAS_ERRORS) != null ? WebUtils.getSessionAttribute(request, HAS_ERRORS).toString() : "false";
		request.getSession().removeAttribute(HAS_ERRORS);
		model.put(HAS_ERRORS, hasError);
		
		return model;

	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		AccountServiceBean bsmartBean = (AccountServiceBean) command;
		
		boolean hasBsmartAcct = WebUtils.getSessionAttribute(request, "hasBsmartAcct") != null ? (boolean)WebUtils.getSessionAttribute(request, "hasBsmartAcct") : false;
		
		if (GenericValidator.isBlankOrNull(bsmartBean.getAgreement())
				|| !bsmartBean.getAgreement().equalsIgnoreCase("agree")) {
			errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
		}
		
		if(hasBsmartAcct){
			if(GenericValidator.isBlankOrNull(bsmartBean.getBsmartAccount()) ||
					bsmartBean.getBsmartAccount().equalsIgnoreCase("none")){
				errors.rejectValue("bsmartAccount", "payroll.bsmart.account.nonselect", "Please select an account");
			}
		}
		
		if(bsmartBean.getFormError()){
			WebUtils.setSessionAttribute(request, HAS_ERRORS, "true");
		}
	}

}
