package evertec.cibp.web.form;

import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import evertec.cibp.core.models.BinaryData.BinaryRecord;

public class ImageView implements org.springframework.web.servlet.View {

	@SuppressWarnings("unchecked")
	public void render(Map map, HttpServletRequest request, HttpServletResponse response) throws Exception {

		BinaryRecord record = (BinaryRecord) map.get("imageRecord");
		if (record != null) {

			int length = (int) record.getData().length();
			byte[] imageByte = record.getData().getBytes(1, length);

			String cont = record.getContentType().replaceAll("\\.", "");
			response.setContentType(cont);

			ServletOutputStream out = response.getOutputStream();
			out.write(imageByte);
			IOUtils.closeQuietly(out);
			// "image/jpeg", "image/jpg", "image/pjpeg" , "image/gif"

		}
	}

	public String getContentType() {
		return "image";
	}

}
