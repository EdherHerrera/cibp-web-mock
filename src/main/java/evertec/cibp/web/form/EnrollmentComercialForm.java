package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerAgreements;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.EnrollmentComercialBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

public class EnrollmentComercialForm extends CancellableFormController {
	
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	private CustomerServices customerService;
	
	public EnrollmentComercialForm() {
		super();
		setCommandClass(EnrollmentComercialBean.class);
		setCommandName("enrollmentComercial");
		setFormView("enrollmentComercial");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		if(userSession == null){
			return new ModelAndView("redirect:login", "noSessionError", null);
		}
		
		Boolean result = this.customerService.addBusinessEnrollCustomer(userSession.getCustomerProfile());
		if(result){
		    CustomerFacade.getInstance().removeCustomerFlag(userSession.getCustomerProfile(), CustomerFlagType.PENDING_COMERCIAL_ACCEPTANCE, userSession);
			CustomerInteractionEvent in = Utils.generateCustomerInteractionEvent(request);
			List<CustomerAgreements> agreements = customerService.customerAgreementsListByProfileId(userSession.getCustomerProfile().getProfileId());
			CustomerAgreements agreement = null;
			if(agreements.isEmpty()){
				agreement = new CustomerAgreements();
				agreement.setProfileId(userSession.getCustomerProfile().getProfileId());
				agreement.setAttributeDataValue(CustomerAgreements.MIBANCO_COMERCIAL, "TRUE");
			}
			else{
				agreement = agreements.get(0);
				agreement.setAttributeDataValue(CustomerAgreements.MIBANCO_COMERCIAL, "TRUE");
			}
			customerService.updateCustomerAgreement(agreement);
			customerService.logMiBancoComercialAcceptance(in);
			return new ModelAndView("forward:welcome");
		}
		
		return new ModelAndView("redirect:login");
	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		return new ModelAndView("login");
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		

		EnrollmentComercialBean enrollmentBean = (EnrollmentComercialBean) command;

		if (GenericValidator.isBlankOrNull(enrollmentBean.getAgreement())
				|| !enrollmentBean.getAgreement().equalsIgnoreCase("agree")) {
			errors.rejectValue("agreement", "enrollment.agreement.error", "Please agree.");
		}
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		GlobalEntitlement enrollentEntitlement = customerService.getGlobalEntitlement(EntitlementType.ENROLLMENT);
		
		if (enrollentEntitlement == null || enrollentEntitlement.getStatus().equals(EntitlementStatus.DISABLED)) {
			model.put("enrollmentAvailable", "false");
			return model;
		} else {
			model.put("enrollmentAvailable", "true");
		}
		
		Map<String, String> agree = new HashMap<String, String>();
		agree.put("agree", "");
		model.put("agree", agree);
		
		return model;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}
	
}
