package evertec.cibp.web.form;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.enums.RegainEmailStatus;
import evertec.cibp.core.models.RegainAccessEmail;

import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.DeviceRequest;

import edu.emory.mathcs.backport.java.util.Arrays;

import evertec.cibp.core.enums.RegainTokenStatus;
import evertec.cibp.core.enums.RegainTokenResource;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerPassword;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerSignonStats;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.CustomerProfileStatus;
import evertec.cibp.core.models.csr.CustomerSearchFilter;
import evertec.cibp.core.models.RegainAccessToken;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.models.customer.MultipleCustomerProfileResponse;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.BankingSessionRequest;
import evertec.cibp.core.services.requests.CardCVVAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.CardPINAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.DepositAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.requests.NonTransAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.PayrollCardAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.PersonalInformationValidationRequest;
import evertec.cibp.core.services.responses.BankingSessionResponse;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse.AccountValidationStatus;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.encryptedPin.PinEncryptionTool;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.CaptchaSettings;
import evertec.cibp.web.form.bean.ResetPasswordBean;
import evertec.cibp.web.session.CaptchaEnabledForm;
import evertec.cibp.web.session.CaptchaSession;
import evertec.cibp.web.session.PasswordResetSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.services.modules.RegainAccessServiceModule;
import evertec.cibp.web.utils.Utils;

import static evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType.REGAIN_ACCESS_ACCT_SUCCESS;
import static evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType.REGAIN_ACCESS_ACCT_FAIL;
import static evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType.REGAIN_ACCESS_PERSONAL_SUCCESS;
import static evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType.REGAIN_ACCESS_PERSONAL_FAIL;
import static evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType.RA_EMAIL_TOKEN_EXPIRED;
import static evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType.REGAIN_ACCESS_EMAIL_BLOCK;
import static evertec.cibp.core.utils.StaticMessageSource.isFlagEnabled;
import static evertec.cibp.core.utils.StaticMessageSource.MBFC224;

/**
 * ResetPasswordForm
 * @author et55596
 * @since 1.2
 * @version 1.0 
 */

public class ResetPasswordForm extends AbstractWizardFormController implements CaptchaEnabledForm {
	
	/** Logger */
	protected final Log log = LogFactory.getLog(getClass());//

	/** RESETPASSWORD_ERROR_NOCUSTOMERPROFILE */
	private static final String RESETPASSWORD_ERROR_NOCUSTOMERPROFILE = "resetpassword.error.nocustomerprofile";//

	/** INVALID_ACCOUNT_NUMBER */
	private static final String INVALID_ACCOUNT_NUMBER = "Invalid account Number";//

	/** ERROR_ACCOUNT_NUMBER */
	private static final String ERROR_ACCOUNT_NUMBER = "erroraccountnumber";//

	/** RESET_PASSWORD_ACCOUNTNUMBER_INVALID */
	private static final String RESET_PASSWORD_ACCOUNTNUMBER_INVALID = "resetpassword.accountnumber.invalid";//

	/** ACCOUNT_NUMBER */
	private static final String ACCOUNT_NUMBER = "accountnumber";//

	/** RESETPASSWORD_PIN_INVALID */
	private static final String RESETPASSWORD_PIN_INVALID = "resetpassword.pin.invalid";//

	/** EXP_MONTH */
	private static final String EXP_MONTH = "expMonth";//

	/** EXP_YEAR */
	private static final String EXP_YEAR = "expYear";//

	/** SESSION_ENROLL_UPDATE */
	private static final String SESSION_ENROLL_UPDATE = "sessionEnrollUpdate";//

	/** NON_TRAN_ACC */
	private static final String NON_TRAN_ACC = "NonTranAcc";//
	
	/** PASSWORD */
	private static final String PASSWORD = "password";//

	/** PASSWORD_CONFIRM */
	private static final String PASSWORD_CONFIRM = "passwordconfirm";//

	/** UPDATE_ACCOUNT */
	private static final String UPDATE_ACCOUNT = "updateAccount";//

	/** PASSWORD_BLOCKED_MOBILE */
	private static final String PASSWORD_BLOCKED_MOBILE = "passwordBlockedMobile";//

	/** ERRORS_TIPS */
	private static final String ERRORS_TIPS = "errorsTips";//

	/** PLEASE_VERIFY_INPUT */
	private static final String PLEASE_VERIFY_INPUT = "Please verify input";//

	/** CAPTCHA_SESSION */
	private static final String CAPTCHA_SESSION = "captchaSession";//

	/** D4 */
	private static final String D4 = "\\d{4}";//

	/** DOB_MONTH */
	private static final String DOB_MONTH = "dobmonth";//

	/** DOB_DAY */
	private static final String DOB_DAY = "dobday";//

	/** DOB_YEAR */
	private static final String DOB_YEAR = "dobyear";//

	/** TAX_ID3 */
	private static final String TAX_ID3 = "taxId3";//

	/** RESETPASSWORD_FIELD_REQUIRED */
	private static final String RESETPASSWORD_FIELD_REQUIRED = "resetpassword.field.required";//

	/** REQUIRED */
	private static final String REQUIRED = "Required";//

	/** MOBILE_APP_TYPE */
	private static final String MOBILE_APP_TYPE = "mobileAppType";//

	/** RESETPASSWORD_NON_TRANSACTIONAL_INVALID */
	private static final String RESETPASSWORD_NON_TRANSACTIONAL_INVALID = "resetpassword.nonTransactional.invalid";//

	/** RESETPASSWORD */
	private static final String RESETPASSWORD = "resetpassword";//

	/** PASSWORD_RESET_SESSION */
	private static final String PASSWORD_RESET_SESSION = "passwordResetSession";//

	/** MOBILE_APP */
	private static final String MOBILE_APP = "mobileApp";//

	/** CUSTOMER_PASSWORD */
	private static final String CUSTOMER_PASSWORD = "passwords";//

	/** ENROLLMENT_PASSWORD_CONTAINS_USERNAME */
	private static final String ENROLLMENT_PASSWORD_CONTAINS_USERNAME = "enrollment.pwd.contains.username";//

	/** PASSWORD_ERROR */
	private static final String PASSWORD_ERROR = "password";//

	/** PASSWORD_CONTAINS_USERNAME */
	private static final String PASSWORD_CONTAINS_USERNAME = "Password must not contains username";//

	/**
	 * Regain Access resource.
	 */
	private static final String REGAIN_ACCESS_RESOURCE = "resource"; //

	/**
	 * Regain Access token.
	 */
	private static final String REGAIN_ACCESS_TOKEN = "token"; //

	/**
	 * Regain Access token view.
	 */
	private static final String REGAIN_ACCESS_TOKEN_REF = "regainAccessToken"; //

	/**
	 * Email blocked view.
	 */
	private static final String EMAIL_BLOCKED_VIEW = "regainaccessEmailBlocked";//

	/** cancelView */
	private String cancelView;//

	/** customerService */
	private CustomerServices customerService;//

	/** pinTool */
	private PinEncryptionTool pinTool;//

	/** captchaSettings */
	private CaptchaSettings captchaSettings;//

	/** paymentvalidationrange */
	private String paymentvalidationrange;//

	/** nonTransEnrollAccs */
	private String nonTransEnrollAccs;//

	/** nonTransProductFlow */
	private String nonTransProductFlow;//

	/**
	 * Regain Access CSR-tools token log.
	 */
	private String strRegainTokenTrace; //

	/**
	 * RegainAccessServiceModule instance.
	 */
	private RegainAccessServiceModule regainAccessServiceModule; //

	/** maxPasswordRecords */
	private Integer maxPasswordRecords;//

	// FIN FFEIC

	public PinEncryptionTool getPinTool() {
		return pinTool;
	}

	public void setPinTool(PinEncryptionTool pinTool) {
		this.pinTool = pinTool;
	}

	/**
	 * ResetPasswordForm
	 */
	public ResetPasswordForm () {
		setCommandClass(ResetPasswordBean.class);
		setCommandName(RESETPASSWORD);
		setPages(new String[] { RESETPASSWORD, RESETPASSWORD, RESETPASSWORD });

		this.cancelView = StringUtils.EMPTY;
		this.customerService = null;
		this.pinTool = null;
		this.captchaSettings = null;
		this.regainAccessServiceModule = null;

		this.maxPasswordRecords = null;
		this.paymentvalidationrange = StringUtils.EMPTY;
		this.nonTransEnrollAccs = StringUtils.EMPTY;
		this.nonTransProductFlow = StringUtils.EMPTY;
		this.setPropertyEditorRegistrar(null);
	}

	/**
	 * ResetPasswordForm
	 * @param strCancelView
	 * @param customerService
	 * @param pinTool
	 * @param captchaSettings
	 * @param regainAccessServiceModule
	 * @param maxPasswordRecords
	 * @param strPaymentvalidationrange
	 * @param strNonTransEnrollAccs
	 * @param strNonTransProductFlow
	 * @param propertyEditorRegistrar
	 */
	public ResetPasswordForm (String strCancelView, CustomerServices customerService, PinEncryptionTool pinTool,
			CaptchaSettings captchaSettings, RegainAccessServiceModule regainAccessServiceModule,
			Integer maxPasswordRecords, String strPaymentvalidationrange, String strNonTransEnrollAccs,
			String strNonTransProductFlow, PropertyEditorRegistrar propertyEditorRegistrar) {
		this();
		this.cancelView = strCancelView;
		this.customerService = customerService;
		this.pinTool = pinTool;
		this.captchaSettings = captchaSettings;
		this.regainAccessServiceModule = regainAccessServiceModule;

		this.maxPasswordRecords = maxPasswordRecords;
		this.paymentvalidationrange = strPaymentvalidationrange;
		this.nonTransEnrollAccs = strNonTransEnrollAccs;
		this.nonTransProductFlow = strNonTransProductFlow;
		this.setPropertyEditorRegistrar(propertyEditorRegistrar);
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		strRegainTokenTrace = ServletRequestUtils.getStringParameter(request, REGAIN_ACCESS_TOKEN);

		ResetPasswordBean resetPasswordBean = ResetPasswordBean.class.cast(super.formBackingObject(request));//
		final String type = ServletRequestUtils.getStringParameter(request, "agentType", "web");//

		if ("android".equalsIgnoreCase(type) || "ios".equalsIgnoreCase(type)) {
			resetPasswordBean.setMobileAppType(type);
			resetPasswordBean.setMobileApp(Boolean.TRUE);

		}
		return resetPasswordBean;
	}

	/**
	 * Sets Regain Access Token session attribute.
	 *
	 * @param request
	 */
	private void setRegainAccessTokenAttr (HttpServletRequest request) {
		final String regainSessionAttribute = String.class.cast(WebUtils.getSessionAttribute(request, REGAIN_ACCESS_TOKEN_REF)); //
		final String regainAccessToken = ServletRequestUtils.getStringParameter(request, REGAIN_ACCESS_TOKEN, ""); //
		
		if (!GenericValidator.isBlankOrNull(regainAccessToken) && 
				(GenericValidator.isBlankOrNull(regainSessionAttribute) 
						|| !regainSessionAttribute.equals(regainAccessToken))) {
			WebUtils.setSessionAttribute(request, REGAIN_ACCESS_TOKEN_REF, regainAccessToken);
		}
	}

	/**
	 * Get customer profiles by RegainAccessToken and Tax ID.
	 *
	 * @param regainAccessToken RegainAccessToken object.
	 * @param strTaxId customer profile Tax ID (SSN).
	 * @return customer profiles in MultipleCustomerProfileResponse object.
	 */
	private MultipleCustomerProfileResponse getRegainAccessCustomerProfiles (RegainAccessToken regainAccessToken,
			String strTaxId) {
		MultipleCustomerProfileResponse multipleCustomerProfileResponse = new MultipleCustomerProfileResponse(); //
		final String email = regainAccessToken.getEmail(); //

		if (email == null) {
			multipleCustomerProfileResponse = new MultipleCustomerProfileResponse();
			multipleCustomerProfileResponse.setStatus(CustomerProfileStatus.NOT_FOUND);
			multipleCustomerProfileResponse.setCustomerProfiles(new ArrayList<>(0));
		} else {
			final CustomerSearchFilter filter = new CustomerSearchFilter(); //

			filter.setEmail(email);
			filter.setSsn(strTaxId);

			final CustomerProfileService profileService = customerService.getCustomerProfileService(); //

			multipleCustomerProfileResponse = profileService.getCustomerProfileByExactFilter(filter);
		}

		if (!isFlagEnabled(MBFC224)) {
			multipleCustomerProfileResponse =  customerService.getCustomerProfiles(strTaxId);
		}
		return multipleCustomerProfileResponse;
	}

	/**
	 * Expires Regain Access Token on Data Base.
	 *
	 * @param regainAccessToken
	 * @param request
	 */
	private void setRegainAccessTokenToExpired (RegainAccessToken regainAccessToken, HttpServletRequest request) {
		if (!isFlagEnabled(MBFC224)) {
			return;
		}

		final RegainTokenStatus status = regainAccessToken.getStatus(); //

		if ((status != null) && (status != RegainTokenStatus.EXPIRED)) {
			regainAccessToken.setStatus(RegainTokenStatus.EXPIRED);
			customerService.updateRegainAccessToken(regainAccessToken);

			regainAccessServiceModule.logRegainAccessEvent(request, regainAccessToken, null,
					RA_EMAIL_TOKEN_EXPIRED);
		}
	}

	/**
	 * @param passwordSession
	 * @param strUserAgent
	 */
	private void generateRecoverUsernameEvent (PasswordResetSession passwordSession, String strUserAgent) {
		final UserSession uSession = new UserSession(null, passwordSession.getCustomerProfile(), null); //
		final CustomerInteractionEvent regainEvent = passwordSession.getInteractionEvent(); //

		regainEvent.setCustomStringData(DeviceUtils.CONNECTION_INFO,
				Utils.generateRaEventTraceInformation(strUserAgent, strRegainTokenTrace, uSession));
		customerService.customerResetUsernameSuccess(passwordSession.getCustomerProfile(), regainEvent);
	}

	/**
	 * Blocks Regain Access email after certain fail attempts.
	 * Logs an email block event.
	 *
	 * @param request HttpServletRequest.
	 * @throws ModelAndViewDefiningException throw to redirect to Email block view.
	 */
	private void regainAccessBlockEmail (HttpServletRequest request) throws ModelAndViewDefiningException {
		if (!isFlagEnabled(MBFC224)) {
			return;
		}

		final String token = String.class.cast(WebUtils.getSessionAttribute(request, REGAIN_ACCESS_TOKEN_REF)); //
		final RegainAccessToken regainAccessToken = customerService.getRegainAccessToken(token); //

		if (!GenericValidator.isBlankOrNull(token) &&
				(regainAccessToken != null) &&
				!GenericValidator.isBlankOrNull(regainAccessToken.getEmail())) {

			final RegainAccessEmail regainAccessEmail = customerService.getRegainAccessEmail(regainAccessToken.getEmail()); //

			regainAccessEmail.setEmail(regainAccessToken.getEmail());
			regainAccessEmail.setStatus(RegainEmailStatus.BLOCKED);
			regainAccessEmail.setBlockedDate(new Date());
			customerService.updateRegainAccessEmail(regainAccessEmail);

			regainAccessServiceModule.logRegainAccessEvent(request, regainAccessToken, null,
					REGAIN_ACCESS_EMAIL_BLOCK);

			final Map<String, Boolean> model = new HashMap<>(1); //
			final String userAgentMobileApp = "userAgentMobileApp"; //

			model.put(userAgentMobileApp, DeviceUtils.isUserAgentMobileApp(request));
			throw new ModelAndViewDefiningException(new ModelAndView(EMAIL_BLOCKED_VIEW, model));
		}
	}

	@SuppressWarnings("unchecked")
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		ResetPasswordBean dude = (ResetPasswordBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		PasswordResetSession passwordResetSession = (PasswordResetSession) WebUtils.getSessionAttribute(request,
				PASSWORD_RESET_SESSION);
		Map<String, Object> model = new HashMap<>();
		Map<Object, CustomerProfile> customerProfiles = passwordResetSession.getCustomerProfileAccountMap();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		// FFEIC
		List<CustomerPassword> passwords = (List<CustomerPassword>) WebUtils.getSessionAttribute(request, CUSTOMER_PASSWORD);
		// FIN FFEIC

		CustomerProfile p = customerProfiles.get(passwordResetSession.getValidatedAcct());

		int newpasswordsalt = PasswordHashUtil.genereateRandomSalt();
		String newpasswordhash = PasswordHashUtil.generateSaltedHash(dude.getPassword(), newpasswordsalt);

		ModifyCustomerRequest modifyCustomerRequest = new ModifyCustomerRequest(new CustomerInteractionEvent(
				request.getRemoteAddr(), request.getRequestedSessionId(), "-", "", Utils.getServerHostname()),
				p.getProfileId(), p.getPasswordHash(), p.getPasswordSalt(), p.getUsername(), newpasswordsalt,
				newpasswordhash, p.getStatus(), CustomerFlagType.PASSWORD_BLOCK, null);
		ModifyCustomerProfileResponse modifyCustomerResponse = customerService
				.modifyCustomerPassword(modifyCustomerRequest);
		if (modifyCustomerResponse.getStatus() == ResultStatus.SUCCESS) {
			try {
				// jocampo FFEIC
				CustomerPassword cp = new CustomerPassword(passwordResetSession.getCustomerProfile().getProfileId(),
						passwordResetSession.getCustomerProfile().getPasswordHash(), new Date(), passwordResetSession.getCustomerProfile().getPasswordSalt());
				passwords.add(0, cp);
				List<CustomerPassword> passwordsDelete = null;
				if (passwords.size() > this.maxPasswordRecords) {
					passwordsDelete = passwords.subList(this.maxPasswordRecords, passwords.size());
					passwords = passwords.subList(0, this.maxPasswordRecords);
				}
				this.customerService.getCustomerProfileService().updateCustomerPasswords(passwords, passwordsDelete);
				// FIN FFEIC
				final ModifyCustomerProfileResponse resetCustomerFlag = customerService
						.resetCustomerFlag(modifyCustomerRequest);
				if (resetCustomerFlag.getStatus() == ResultStatus.SUCCESS) {
					final int numberPage = 2;//

					model.put("passwordchangeSuccess", true);
					model.put("username", p.getUsername());
					model.put("page", numberPage);
					model.put(MOBILE_APP, dude.isMobileApp());
					model.put(MOBILE_APP_TYPE, dude.getMobileAppType());
				}

				//updating last password change by regain access flow
				CustomerSignonStats css = modifyCustomerResponse.getCustomerProfile().getSignonStats();
				css.setLastRegainAccessPassChange(Calendar.getInstance().getTime());
				CustomerFacade.getInstance().updateCustomerSignonStats(css, userSession);

			} catch (Exception e) {
				log.error("Error on finishing the reset password", e);
			}
			// return new ModelAndView("redirect:login");
		}

		request.removeAttribute(REGAIN_ACCESS_RESOURCE);
		request.removeAttribute(REGAIN_ACCESS_TOKEN);

		return showForm(request, errors, RESETPASSWORD, model);
	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		request.removeAttribute(REGAIN_ACCESS_RESOURCE);
		request.removeAttribute(REGAIN_ACCESS_TOKEN);

		return new ModelAndView(cancelView);
	}

	@SuppressWarnings("unchecked")
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		PasswordResetSession passwordResetSession = (PasswordResetSession) WebUtils.getSessionAttribute(request,
				PASSWORD_RESET_SESSION);
		ResetPasswordBean dude = (ResetPasswordBean) command;
		if (page == 0) {

			// cibp-1886
			WebUtils.setSessionAttribute(request, "userSession", null);

			String taxId1 = dude.getTaxId1();
			String taxId2 = dude.getTaxId2();
			String taxId3 = dude.getTaxId3();
			Integer dobyear = dude.getDobyear();
			Integer dobmonth = dude.getDobmonth();
			Integer dobday = dude.getDobday();
			boolean error = false;

			if (GenericValidator.isBlankOrNull(taxId1) || GenericValidator.isBlankOrNull(taxId2)
					|| GenericValidator.isBlankOrNull(taxId3)) {
				errors.rejectValue(TAX_ID3, RESETPASSWORD_FIELD_REQUIRED, REQUIRED);
				error = true;
			} else {
				if (!GenericValidator.matchRegexp(taxId1, "\\d{3}") || !GenericValidator.matchRegexp(taxId2, "\\d{2}")
						|| !GenericValidator.matchRegexp(taxId3, D4)) {
					errors.rejectValue(TAX_ID3, "enrollment.taxId.invalid", "Invalid SSN");
					error = true;
				}
			}

			if (dobyear == null || !GenericValidator.matchRegexp(dobyear.toString(), D4)) {
				errors.rejectValue(DOB_YEAR, RESETPASSWORD_FIELD_REQUIRED, REQUIRED);
				error = true;
			}

			if (dobday == null || dobday.equals(0) || !GenericValidator.matchRegexp(dobday.toString(), "\\d{1,2}")) {
				errors.rejectValue(DOB_DAY, RESETPASSWORD_FIELD_REQUIRED, REQUIRED);
				error = true;
			}

			if (dobmonth == null || !GenericValidator.matchRegexp(dobmonth.toString(), "\\d{1,2}")) {
				errors.rejectValue(DOB_MONTH, RESETPASSWORD_FIELD_REQUIRED, REQUIRED);
				error = true;
			}

			if (!(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC224))) {
				final CaptchaSession captchaSession = CaptchaSession.class.cast(WebUtils.getSessionAttribute(request, CAPTCHA_SESSION)); //

				// We dispose of the Captcha. A new one must always be used.
				WebUtils.setSessionAttribute(request, CAPTCHA_SESSION, null);
				if (!captchaSession.isCaptchaValid(dude.getCaptchaAnswer())) {
					errors.rejectValue("captchaAnswer", "resetpassword.captcha.invalid", PLEASE_VERIFY_INPUT);
					return;
				}
			}

			if (!error) {

				final String taxId = taxId1 + taxId2 + taxId3;//

				//UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
				//(MBNA) MBNA-1099
		        List<CustomerProfile> customerProfiles;

				final String strRegainToken = String.class.cast(WebUtils.getSessionAttribute(request, REGAIN_ACCESS_TOKEN_REF));//
				final RegainAccessToken regainAccessToken = customerService.getRegainAccessToken(strRegainToken);//
				final MultipleCustomerProfileResponse losdude = getRegainAccessCustomerProfiles(regainAccessToken, taxId);//

				setRegainAccessTokenToExpired(regainAccessToken, request);
				customerProfiles = losdude.getCustomerProfiles();

				Map<Object, CustomerProfile> a = new HashMap<Object, CustomerProfile>();

				if (customerProfiles != null && customerProfiles.size() == 1) {
					// Only exist one customer Profile, I can block
					final CustomerProfile customerProfile = customerProfiles.get(0);//
					passwordResetSession.setCustomerProfile(customerProfile);
					//MBFIS-141 - ET57991
					if (CustomerUtils.isSecurities(customerProfile) && AccountUtils.getIsTransactional(customerProfile).equalsIgnoreCase("N")) { 
						throw new ModelAndViewDefiningException(new ModelAndView("redirect:/actions/forgotpasswordsec", "forgot", null));
					}
					//END MBFIS-141
					if (customerProfile.hasFlag(CustomerFlagType.PASSWORD_RESET_BLOCK)) {
						passwordResetSession.setBlocked(true);
						errors.rejectValue(TAX_ID3, "resetpassword.taxId.invalid", "Invalid SSN");
						final int counterNumber = 3;//
						passwordResetSession.setActCounter(counterNumber);
						passwordResetSession.addRegainAccessInfoCounter();
						return;
					} else if (customerProfile.getStatus() == CustomerStatus.PENDING_CSR_ACTIVATION
							|| customerProfile.hasFlag(CustomerFlagType.VALIDATE_DOB)
							|| customerProfile.hasFlag(CustomerFlagType.MUST_VALIDATE_ENROLLACCOUNT)) {
						passwordResetSession.setIncompleteEnroll(true);
						return;
					}

					passwordResetSession.getInteractionEvent().setUsername(customerProfile.getUsername());
					passwordResetSession.getInteractionEvent().setProfileId(customerProfile.getProfileId());
					Calendar cal = Calendar.getInstance();
					cal.setTime(customerProfile.getBirthDate());
					if (!(dobyear != cal.get(Calendar.YEAR) || (dobmonth != cal.get(Calendar.MONTH)) || dobday != cal
							.get(Calendar.DAY_OF_MONTH))) {
						a.put(customerProfile.getEnrollmentAccountNumber(), customerProfile);
					}
					if (a.size() == 0) {
						passwordResetSession.addRegainAccessInfoCounter();
						errors.rejectValue(TAX_ID3, RESETPASSWORD_ERROR_NOCUSTOMERPROFILE, PLEASE_VERIFY_INPUT);

						regainAccessServiceModule.logRegainAccessEvent(request, regainAccessToken, null,
								REGAIN_ACCESS_PERSONAL_FAIL);
					}else {
						passwordResetSession.setCustomerProfileAccountMap(a);
						passwordResetSession.setValidatedTaxId(taxId);

						regainAccessServiceModule.logRegainAccessEvent(request, regainAccessToken, customerProfile,
								REGAIN_ACCESS_PERSONAL_SUCCESS);
					}
				} else if (customerProfiles != null && customerProfiles.size() > 0) {
					List<CustomerProfile> listProfilesSEC = new ArrayList<>(); // MBFIS-141 
					for (CustomerProfile cp : customerProfiles) {
						if (CustomerUtils.isSecurities(cp) && AccountUtils.getIsTransactional(cp).equalsIgnoreCase("N")) { //MBFIS-141
							listProfilesSEC.add(cp);
							customerProfiles.remove(cp);
							continue;
						} // END
						Calendar cal = Calendar.getInstance();
						cal.setTime(cp.getBirthDate());
						if (dobyear != cal.get(Calendar.YEAR) || (dobmonth != cal.get(Calendar.MONTH))
								|| dobday != cal.get(Calendar.DAY_OF_MONTH))
							continue;
						if (cp != null && cp.getEnrollmentAccountNumber() != null)
							a.put(cp.getEnrollmentAccountNumber(), cp);
					}
					if (listProfilesSEC.size() == customerProfiles.size()) { //MBFIS-141
						throw new ModelAndViewDefiningException(new ModelAndView("redirect:/actions/forgotpasswordsec", "forgot", null));
					} else {
						for (CustomerProfile profile : listProfilesSEC) {
							customerProfiles.remove(profile);
						}
					} //END
					if (a.size() == 0) {
						passwordResetSession.addRegainAccessInfoCounter();
						errors.rejectValue(TAX_ID3, RESETPASSWORD_ERROR_NOCUSTOMERPROFILE, PLEASE_VERIFY_INPUT);

						regainAccessServiceModule.logRegainAccessEvent(request, regainAccessToken, null,
								REGAIN_ACCESS_PERSONAL_FAIL);
					}
					else {
						passwordResetSession.setCustomerProfileAccountMap(a);
						passwordResetSession.setValidatedTaxId(taxId);

						final List<CustomerProfile> successProfiles = losdude.getCustomerProfiles();
						
						for(CustomerProfile p : successProfiles) {
							regainAccessServiceModule.logRegainAccessEvent(request, regainAccessToken, p,
									REGAIN_ACCESS_PERSONAL_SUCCESS);
						}

					}
				} else {
					passwordResetSession.addRegainAccessInfoCounter();
					errors.rejectValue(TAX_ID3, RESETPASSWORD_ERROR_NOCUSTOMERPROFILE, PLEASE_VERIFY_INPUT);

					regainAccessServiceModule.logRegainAccessEvent(request, regainAccessToken, null,
							REGAIN_ACCESS_PERSONAL_FAIL);
				}
			}
		}

		else if (page == 1) {

			boolean error = false;//
			String accountnumber = dude.getAccountnumber();//
			final String taxId = passwordResetSession.getValidatedTaxId();//
			final String pin = dude.getPin();//
			final String cvv = dude.getCvv();//
			final String expMonth = dude.getExpMonth();//
			final String expYear = dude.getExpYear();//
			final String zipCode = dude.getZipCode();//
			final Integer originationYear = dude.getOriginationYear();//
			final BigDecimal monthlyPayment = dude.getMonthlyPayment();//
			ValidateAccountStatusResponse validateAccountInfo = null;//
			String accountType = dude.getAccountInfoType();//

			dude.setAccountInfoType(accountType);

			//MBCA17 & 190 BY:ET56509
			boolean MBCA17 = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA17);
			final String valRegExp = "(\\$)?([1-9]{1}[0-9]{0,2})(\\,\\d{3})*$|^(\\$)?([1-9]{1}[0-9]{0,2})(\\d{3})*$";//


			final Iterator<Map.Entry<Object, CustomerProfile>> iterator = passwordResetSession.getCustomerProfileAccountMap()
					.entrySet().iterator();//
			while (iterator.hasNext()) {
				Map.Entry<Object, CustomerProfile> entry = (Map.Entry<Object, CustomerProfile>) iterator.next();
				CustomerProfile cp = (CustomerProfile) entry.getValue();
				if (cp.getEnrollmentAccountNumber().equals(accountnumber)) {
					CustomerEntitlement ent = cp.getEntitlementByType(EntitlementType.BANKING);
					if (ent != null) {
						evertec.cibp.core.models.entitlement.BankingEntitlement entt = new BankingEntitlement(ent);
						BankingSessionRequest sessionRequest = new BankingSessionRequest(
								Utils.generateCustomerInteractionEvent(request), cp, null, entt, false);
						BankingSessionResponse bankingresponse = this.customerService.getBankingSession(sessionRequest);

						if (bankingresponse.getErrorCode() == 1740 && cp != null) {
							Calendar custBirthDate = Calendar.getInstance();
							custBirthDate.setTime(cp.getBirthDate());
							PersonalInformationValidationRequest personalRequest = new PersonalInformationValidationRequest(
									null, cp.getTaxId(), custBirthDate, false, false);
							personalRequest.setValidateMultipleProfiles(true);
							PersonalInformationValidationResponse customerEnrollment = this.customerService
									.isEnrollableCustomer(personalRequest);
							WebUtils.setSessionAttribute(request, "customerEnrollment", customerEnrollment);

							boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);

							UserSession sessionEnrollUpdate = null;
							if(oobFlag){
								//							add listOOB null
								sessionEnrollUpdate = new UserSession(null, cp, null, null);
							}else{
								sessionEnrollUpdate = new UserSession(null, cp, null);
							}
							sessionEnrollUpdate.setLanguage(Utils.getRSALanguage(request));
							sessionEnrollUpdate.setCustomerService(this.customerService);
							WebUtils.setSessionAttribute(request, SESSION_ENROLL_UPDATE, sessionEnrollUpdate);
							return;
						} else {
							BankingSession bankingSession = null;
							CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
							BankingEntitlement bankingEntitlement = new BankingEntitlement(
									cp.getEntitlementByType(EntitlementType.BANKING));
							BankingSessionRequest req = new BankingSessionRequest(event, cp, null, bankingEntitlement,
									false);
							bankingSession = customerService.getBankingSession(req).getBankingSession();
							List<TVFrontendAccount> accounts = null;
							accounts = customerService.getCustomerAccounts(cp, bankingSession);
							if (!Utils.enrollmentAccountExists(accounts, cp)) {
								Calendar custBirthDate = Calendar.getInstance();
								custBirthDate.setTime(cp.getBirthDate());

								PersonalInformationValidationRequest personalRequest = new PersonalInformationValidationRequest(
										null, cp.getTaxId(), custBirthDate, false, false);
								personalRequest.setValidateMultipleProfiles(true);
								PersonalInformationValidationResponse customerEnrollment = customerService
										.isEnrollableCustomer(personalRequest);
								WebUtils.setSessionAttribute(request, "customerEnrollment", customerEnrollment);

								boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);

								UserSession sessionEnrollUpdate = null;
								if(oobFlag){
									//								add listOOB null
									sessionEnrollUpdate = new UserSession(null, cp, null, null);
								}else{
									sessionEnrollUpdate = new UserSession(null, cp, null);
								}
								sessionEnrollUpdate.setLanguage(Utils.getRSALanguage(request));
								sessionEnrollUpdate.setCustomerService(this.customerService);
								WebUtils.setSessionAttribute(request, SESSION_ENROLL_UPDATE, sessionEnrollUpdate);
								return;
							}
						}
					}
				}
			}

			if ("IDA".equalsIgnoreCase(accountType)) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{9}")) {

					// MBSFE-306
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
						errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					} else {
						errors.rejectValue(ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					}
					// END MBSFE-306
					error = true;
				}

				if (GenericValidator.isBlankOrNull(pin) || !GenericValidator.matchRegexp(pin, D4)) {
					errors.rejectValue("pin", RESETPASSWORD_PIN_INVALID, "Invalid PIN");
					error = true;
				}
				if (!error) {
					String encryptedPin = pinTool.encrypt(pin);
					DepositAccountInformationValidationRequest depReq = new DepositAccountInformationValidationRequest(
							null, accountnumber, "", taxId, encryptedPin, true, false);

					validateAccountInfo = customerService.validateDepositAccountInfo(depReq);
				}

			} else if ("CCA".equalsIgnoreCase(accountType)) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{16}")) {

					// MBSFE-306
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
						errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					} else {
						errors.rejectValue(ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					}
					// END MBSFE-306
					error = true;
				}
				if (GenericValidator.isBlankOrNull(cvv) || !GenericValidator.matchRegexp(cvv, "\\d{3}")) {
					errors.rejectValue("cvv", "resetpassword.cvv.invalid", "Invalid CVV code");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(zipCode) || !GenericValidator.matchRegexp(zipCode, "\\d{5}")) {
					errors.rejectValue("zipCode", "resetpassword.zipCode.invalid", "Invalid zip code");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(expMonth) || expMonth.equals("0")) {
					errors.rejectValue(EXP_MONTH, "enrollment.expMonth.invalid", "Invalid exp month");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(expYear) || !GenericValidator.matchRegexp(expYear, D4)) {
					errors.rejectValue(EXP_YEAR, "enrollment.expYear.invalid", "Invalid exp year");
					error = true;
				}
				if (!error) {
					CardCVVAccountInformationValidationRequest cvvReq = new CardCVVAccountInformationValidationRequest(
							null, accountnumber, taxId, cvv, expMonth, expYear, zipCode, true);
					validateAccountInfo = customerService.validateCrediCardCVVAccountInfo(cvvReq);
				}

			} else if ("AMEX".equalsIgnoreCase(accountType)) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{15}") || !accountnumber.startsWith("377")) {

					// MBSFE-306
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
						errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					} else {
						errors.rejectValue(ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					}
					// END MBSFE-306
					error = true;
				}
				if (GenericValidator.isBlankOrNull(pin) || !GenericValidator.matchRegexp(pin, D4)) {
					errors.rejectValue("pin", RESETPASSWORD_PIN_INVALID, "Invalid PIN code");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(zipCode) || !GenericValidator.matchRegexp(zipCode, "\\d{5}")) {
					errors.rejectValue("zipCode", "resetpassword.zipCode.invalid", "Invalid zip code");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(expMonth) || expMonth.equals("0")) {
					errors.rejectValue(EXP_MONTH, "enrollment.expMonth.invalid", "Invalid exp month");
					error = true;
				}
				if (GenericValidator.isBlankOrNull(expYear) || !GenericValidator.matchRegexp(expYear, D4)) {
					errors.rejectValue(EXP_YEAR, "enrollment.expYear.invalid", "Invalid exp year");
					error = true;
				}
				if (!error) {
					accountnumber = "0" + accountnumber;
					String encryptedPin = pinTool.encrypt(pin);
					CardPINAccountInformationValidationRequest cardPinReq = new CardPINAccountInformationValidationRequest(
							null, accountnumber, taxId, encryptedPin, expMonth, expYear, zipCode, true, true, true);
					validateAccountInfo = customerService.validateCrediCardPINAccountInfo(cardPinReq);
				}

			} else if ("PFC".equalsIgnoreCase(accountType)) {

				if (GenericValidator.isBlankOrNull(accountnumber)
						|| !GenericValidator.matchRegexp(accountnumber, "\\d{16}")) {

					// MBSFE-306
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
						errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					} else {
						errors.rejectValue(ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					}
					// END MBSFE-306
					error = true;
				}
				if (GenericValidator.isBlankOrNull(pin) || !GenericValidator.matchRegexp(pin, D4)) {
					errors.rejectValue("pin", RESETPASSWORD_PIN_INVALID, "Invalid PIN code");
					error = true;
				}
				if (!error) {
					String encryptedPin = pinTool.encrypt(pin);
					PayrollCardAccountInformationValidationRequest payrollReq = new PayrollCardAccountInformationValidationRequest(
							null, accountnumber, taxId, encryptedPin, true);
					validateAccountInfo = customerService.validatePayrollCardAccountInfo(payrollReq);
				}
			}else if((MBCA17 && NON_TRAN_ACC.equalsIgnoreCase(accountType)) || (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA640) && NON_TRAN_ACC.equalsIgnoreCase(accountType))){

				final String digitLength = "\\d+";//

				if (GenericValidator.isBlankOrNull(accountnumber)|| !GenericValidator.matchRegexp(accountnumber, digitLength)){

					// MBSFE-306
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
						errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					} else {
						errors.rejectValue(ACCOUNT_NUMBER, RESETPASSWORD_NON_TRANSACTIONAL_INVALID, INVALID_ACCOUNT_NUMBER);
					}

					// END MBSFE-306
					error = true;
				}

				if ((originationYear == null || !originationYear.toString().matches(D4)) && !error){
					// MBSFE-306
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
						errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					} else {
						errors.rejectValue(ACCOUNT_NUMBER, RESETPASSWORD_NON_TRANSACTIONAL_INVALID, INVALID_ACCOUNT_NUMBER);
					}
					// END MBSFE-306
					error = true;
				}

				if( (monthlyPayment == null || !monthlyPayment.toPlainString().matches(valRegExp)) && !error){
					// MBSFE-306
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
						errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					} else {
						errors.rejectValue(ACCOUNT_NUMBER, RESETPASSWORD_NON_TRANSACTIONAL_INVALID, INVALID_ACCOUNT_NUMBER);
					}
					// END MBSFE-306
					error = true;
				}

				String nNonTransAccType = null;
				Iterator<Map.Entry<Object, CustomerProfile>> nonTraniterator = passwordResetSession.getCustomerProfileAccountMap()
						.entrySet().iterator();			
				while (nonTraniterator.hasNext()) {
					Map.Entry<Object, CustomerProfile> entry = (Map.Entry<Object, CustomerProfile>) nonTraniterator.next();
					CustomerProfile cp = (CustomerProfile) entry.getValue();
					if (cp.getEnrollmentAccountNumber().equals(accountnumber) && cp.getTaxId().equals(taxId)) {
						nNonTransAccType = cp.getEnrollmentAccountSubtype();
						accountType = cp.getEnrollmentAccountSubtype();
					}
				}
				
				if(nNonTransAccType == null) {
					//Hack: Set default value to force trip to vault and force counter
					nNonTransAccType = "MLA";
				}


				if(!error){
					NonTransAccountInformationValidationRequest ntReq = new NonTransAccountInformationValidationRequest(
							null, accountnumber,taxId,true, originationYear, monthlyPayment,paymentvalidationrange, true, nNonTransAccType);
					validateAccountInfo = customerService.validateNonTransAccountInfo(ntReq);
				}
			}
				

			if (validateAccountInfo == null || validateAccountInfo.getStatus() == null)
				error = true;

			boolean accountmatch = false;
			if (!error) {
				// Verificar si la cuenta entrada parea con alguna en los
				// profiles
				Set<Entry<Object, CustomerProfile>> customerProfilesSet = null;
				if (passwordResetSession.getCustomerProfileAccountMap() != null)
					customerProfilesSet = passwordResetSession.getCustomerProfileAccountMap().entrySet();
				if (customerProfilesSet != null && customerProfilesSet.size() > 0)
					for (Entry<Object, CustomerProfile> entry : customerProfilesSet) {
						CustomerProfile prof = entry.getValue();
						if (prof != null && prof.getEnrollmentAccountNumber() != null
								&& accountnumber.equalsIgnoreCase(prof.getEnrollmentAccountNumber())) {
							CustomerProfile customerProfile = prof;
							if (customerProfile.hasFlag(CustomerFlagType.PASSWORD_RESET_BLOCK)) {
								passwordResetSession.setBlocked(true);
								passwordResetSession.setActCounter(3);

								// MBSFE-306
								if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
									errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
											INVALID_ACCOUNT_NUMBER);
								} else {
									errors.rejectValue(ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
											INVALID_ACCOUNT_NUMBER);
								}
								// END MBSFE-306
								return;
							}
							passwordResetSession.getInteractionEvent().setUsername(customerProfile.getUsername());
							passwordResetSession.getInteractionEvent().setProfileId(customerProfile.getProfileId());
							passwordResetSession.setCustomerProfile(customerProfile);
							accountmatch = true;
							break;
						}
					}
			}
			if (!error) {
				final String strToken = (WebUtils.getSessionAttribute(request, REGAIN_ACCESS_TOKEN_REF) != null)
						? WebUtils.getSessionAttribute(request,"regainAccessToken").toString() : ""; //

				String userAgent = request.getHeader("user-agent");
				String customerDevices = "";
				if(userAgent != null) {
					customerDevices = DeviceUtils.getCustomerDevice(userAgent).toString();
				}

				// ValidateInfo status... validation...
				if (validateAccountInfo.getStatus() == AccountValidationStatus.VALID) {
					passwordResetSession.setValidatedAcct(accountnumber);
					passwordResetSession.setValidatedAcctType(accountType);
					passwordResetSession.setValidatedAcctProductId(validateAccountInfo.getAccountProductId());

					DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(dude.getPm_fp(),null, request);

					if (accountmatch) {
						regainAccessServiceModule.logRegainAccessEvent(request, new RegainAccessToken(null, strToken, null, null),
								passwordResetSession.getCustomerProfile(), REGAIN_ACCESS_ACCT_SUCCESS);

						String strRegainResource = String.class.cast(WebUtils.getSessionAttribute(request, REGAIN_ACCESS_RESOURCE));//

						if (strRegainResource == null) {
							strRegainResource = RegainTokenResource.PSW.toString();
						}

						if (strRegainResource.equals(RegainTokenResource.USERID.toString())) {
							generateRecoverUsernameEvent(passwordResetSession, userAgent);

						} else {
							customerService.customerResetPasswordSuccess(passwordResetSession.getCustomerProfile(), deviceRequest,
						         passwordResetSession.getInteractionEvent());
						}
					}
					else {

						// MBSFE-306
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
							errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
									INVALID_ACCOUNT_NUMBER);
						} else {
							errors.rejectValue(ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
									INVALID_ACCOUNT_NUMBER);
						}
						// END MBSFE-306
						passwordResetSession.addRegainAccessActCounter();
						failCustomer(accountmatch, passwordResetSession);

						regainAccessServiceModule.logRegainAccessEvent(request, new RegainAccessToken(null, strToken, null, null),
								passwordResetSession.getCustomerProfile(), REGAIN_ACCESS_ACCT_FAIL);
					}
					return;
				} else if (validateAccountInfo.getStatus() == AccountValidationStatus.ACCOUNT_BLOCKED) {
					passwordResetSession.setBlocked(true);
					passwordResetSession.setActCounter(3);

					// MBSFE-306
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
						errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					} else {
						errors.rejectValue(ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					}
					// END MBSFE-306
					blockCustomer(accountmatch, passwordResetSession,
							passwordResetSession.getCustomerProfileAccountMap(), customerDevices);
					return;

				} else if (validateAccountInfo.getStatus() == AccountValidationStatus.ATH_NOT_MATCH
						|| validateAccountInfo.getStatus() == AccountValidationStatus.ACCOUNT_INVALID
						|| validateAccountInfo.getStatus() == AccountValidationStatus.INVALID
						|| validateAccountInfo.getStatus() == AccountValidationStatus.INVALID_ZIPCODE
						|| validateAccountInfo.getStatus() == AccountValidationStatus.INVALID_EXPDATE) {

					passwordResetSession.addRegainAccessActCounter();

					regainAccessServiceModule.logRegainAccessEvent(request, new RegainAccessToken(null, strToken, null, null),
							passwordResetSession.getCustomerProfile(), REGAIN_ACCESS_ACCT_FAIL);

					if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD2479)) {
						if(accountmatch) {//Agregar flag para quitar el true
							passwordResetSession.addActCounter();
							if (passwordResetSession.getActCounter() >= 3) {
								passwordResetSession.setBlocked(true);
								errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID, INVALID_ACCOUNT_NUMBER);
								blockCustomer(accountmatch, passwordResetSession, passwordResetSession.getCustomerProfileAccountMap(), customerDevices);
								return;
							} else {
								failCustomer(accountmatch, passwordResetSession);
							}

						}else {

							passwordResetSession.addActNotFoundCounter();
							if (passwordResetSession.getActNotFoundCounter() >= 3) {
								errors.rejectValue(ERROR_ACCOUNT_NUMBER, "resetpassword.accountnumber.notfound",
											"Account Number Not Found");
								return;
							} else {
								failCustomer(accountmatch, passwordResetSession);
							}

						}
					}else {

						//MBSD2479 Borrar este Codigo al Limpiar el flag
						passwordResetSession.addActCounter();
						if (passwordResetSession.getActCounter() >= 3) {
							passwordResetSession.setBlocked(true);

							// MBSFE-306
							if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
								errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
										INVALID_ACCOUNT_NUMBER);
							} else {
								errors.rejectValue(ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
										INVALID_ACCOUNT_NUMBER);
							}
							// END MBSFE-306
							blockCustomer(accountmatch, passwordResetSession,
									passwordResetSession.getCustomerProfileAccountMap(), customerDevices);
							return;
						} else {
							failCustomer(accountmatch, passwordResetSession);
						}
						//END Deleted MBSD2479
					}

					// MBSFE-306
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
						errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					} else {
						errors.rejectValue(ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					}
					// END MBSFE-306
					return;

				}else {
					passwordResetSession.addRegainAccessActCounter();
					passwordResetSession.addActCounter();

					regainAccessServiceModule.logRegainAccessEvent(request, new RegainAccessToken(null, strToken, null, null),
							passwordResetSession.getCustomerProfile(), REGAIN_ACCESS_ACCT_FAIL);

					if (passwordResetSession.getActCounter() >= 3) {
						passwordResetSession.setBlocked(true);

						// MBSFE-306
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
							errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
									INVALID_ACCOUNT_NUMBER);
						} else {
							errors.rejectValue(ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
									INVALID_ACCOUNT_NUMBER);
						}
						// END MBSFE-306
						blockCustomer(accountmatch, passwordResetSession,
								passwordResetSession.getCustomerProfileAccountMap(),customerDevices);
						return;
					} else {
						failCustomer(accountmatch, passwordResetSession);
					}

					// MBSFE-306
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306)) {
						errors.rejectValue(ERROR_ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					} else {
						errors.rejectValue(ACCOUNT_NUMBER, RESET_PASSWORD_ACCOUNTNUMBER_INVALID,
								INVALID_ACCOUNT_NUMBER);
					}
					// END MBSFE-306

					return;
				}
			}

		} else if (page == 2) {

			String password = dude.getPassword();
			String passwordconfirm = dude.getPasswordconfirm();

			if (GenericValidator.isBlankOrNull(password)) {
				errors.rejectValue(PASSWORD, "resetpassword.pwd.required", REQUIRED);
			}
			else if (!(GenericValidator.maxLength(password, 15) && GenericValidator.minLength(password, 8))) {
				errors.rejectValue(PASSWORD, "resetpassword.pwd.invalid", "Invalid Password");
			}
			else if (!Utils.isValidPassword(dude.getPassword())) {
				errors.rejectValue(PASSWORD, "resetpassword.pwd.invalid", "Invalid Password");
			}
			else if (!password.equals(passwordconfirm)) {
				errors.rejectValue(PASSWORD_CONFIRM, "resetpassword.pwd.mustmatch", "Password must match");
			} 
			else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989)
					&& password.toLowerCase().contains(passwordResetSession.getCustomerProfile().getUsername().toLowerCase())) {
				errors.rejectValue(PASSWORD_ERROR, ENROLLMENT_PASSWORD_CONTAINS_USERNAME, PASSWORD_CONTAINS_USERNAME);
			}
			else {
				// FFEIC
				List<CustomerPassword> passwords = (List<CustomerPassword>) WebUtils.getSessionAttribute(request,
						CUSTOMER_PASSWORD);
				if (passwords == null) {
					passwords = customerService.getCustomerProfileService().getCustomerPasswords(passwordResetSession.getCustomerProfile());
					WebUtils.setSessionAttribute(request, CUSTOMER_PASSWORD, passwords);
				}

				// NO EQUALS ACTUAL PASS
				int originalSalt = passwordResetSession.getCustomerProfile().getPasswordSalt();
				String originalHash = passwordResetSession.getCustomerProfile().getPasswordHash();
				String newHash = PasswordHashUtil.generateSaltedHash(dude.getPassword(), originalSalt);
				if (newHash.equals(originalHash)) {
					errors.rejectValue(PASSWORD, "changepassword.password.previously.used", "Incorrect Password");
				}
				// NO EQUALS LAST PASS
				else {
					for (int i = 0; i < passwords.size(); i++) {
						String newPassOldHash = PasswordHashUtil.generateSaltedHash(dude.getPassword(), passwords
								.get(i).getPasswordSalt());
						if (passwords.get(i).getPasswordHash().equals(newPassOldHash)) {
							errors.rejectValue(PASSWORD, "changepassword.password.previously.used",
									"Incorrect Password");
							break;
						}
					}
				}
				// FIN FFEIC
			}
		}
	}

	private void blockCustomer(boolean accountmatch, PasswordResetSession passwordResetSession,
			Map<Object, CustomerProfile> profiles,  String customerDevices) {
		if (accountmatch) {

			passwordResetSession.getInteractionEvent().setUsername(passwordResetSession.getCustomerProfile().getUsername());
			passwordResetSession.getInteractionEvent().setProfileId(passwordResetSession.getCustomerProfile().getProfileId());
			customerService.blockCustomerResetPassword(passwordResetSession.getCustomerProfile(), passwordResetSession.getInteractionEvent(), customerDevices);

		} else {
			Set<Entry<Object, CustomerProfile>> customerProfilesSet = profiles.entrySet();
			if (!customerProfilesSet.isEmpty())
				for (Entry<Object, CustomerProfile> entry : customerProfilesSet) {
					CustomerProfile prof = entry.getValue();
					passwordResetSession.getInteractionEvent().setUsername(prof.getUsername());
					passwordResetSession.getInteractionEvent().setProfileId(prof.getProfileId());
					customerService.blockCustomerResetPassword(prof, passwordResetSession.getInteractionEvent(), customerDevices);
				}
		}
	}

	private void failCustomer(boolean accountmatch, PasswordResetSession passwordResetSession) {
		if (accountmatch)
			customerService.customerFailResetPassword(passwordResetSession.getCustomerProfile(), passwordResetSession.getInteractionEvent(), null);
	}

	/**
	 *@param passwordResetSession
	 *@return boolean
	 */
	private boolean verifyBlockEmailReferenceData(PasswordResetSession passwordResetSession) {
		boolean isBlocked = false;//

		isBlocked = passwordResetSession != null && (passwordResetSession.getRegainAccessInfoCounter() >= 3
				|| passwordResetSession.getRegainAccessActCounter() >= 3);

		return isBlocked;
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {
		Map<String, Object> model = new HashMap<>(0);//
		ResetPasswordBean enrollment = ResetPasswordBean.class.cast(command);//

		PasswordResetSession passwordResetSession = PasswordResetSession.class.cast(WebUtils.getSessionAttribute(request,
				PASSWORD_RESET_SESSION)); //

		if (verifyBlockEmailReferenceData(passwordResetSession)) {
			regainAccessBlockEmail(request);
		}

		boolean MBCA17 = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA17);

		model.put("mbim506-option1", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBIM506_OPTION1));
		model.put("mbim506-option2", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBIM506_OPTION2));
		model.put("mbim506-option3", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBIM506_OPTION3));

		model.put("page", page);
		model.put("formname", request.getPathInfo().replace("/old/", "").replace("/", ""));

		final boolean regainFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC224); //

		if (!regainFlag) {
			model.put("urlCaptcha", "/actions/captcha");
		}
		
		model.put("regainFlag",regainFlag);

		//MBFIS-141 - ET57991
		final UserSession uSession = UserSession.class.cast(WebUtils.getSessionAttribute(request, "userSession"));//
		final String nWord = "N";//

		if (!enrollment.isMobileApp() && (uSession != null && CustomerUtils.isSecurities(uSession.getCustomerProfile()) && AccountUtils.getIsTransactional(uSession.getCustomerProfile()).equalsIgnoreCase(nWord))) {
			throw new ModelAndViewDefiningException(new ModelAndView("redirect:/actions/forgotpasswordsec", "forgot", null));
		}
		//END MBFIS-141

		WebUtils.setSessionAttribute(request, Utils.DEEP_LINK, null);

		//MBSFE-306
		model.put("MBSFE306", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE306));
		//END MBSFE-306

		// 1886
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, SESSION_ENROLL_UPDATE);
		if (userSession != null) {
			model.put(UPDATE_ACCOUNT, true);
			WebUtils.setSessionAttribute(request, UPDATE_ACCOUNT, true);
			return model;
		}

		model.put(UPDATE_ACCOUNT, false);

		if (page != 0 && passwordResetSession == null) {
			errors.reject("", "Password Session is null.  Please retry.");
			model.put(MOBILE_APP, enrollment.isMobileApp());
			model.put(MOBILE_APP_TYPE, enrollment.getMobileAppType());
			return null;
		} else if (page == 0) {

			if (isFlagEnabled(MBFC224) && passwordResetSession != null) {
				final int emailBlockCounter = passwordResetSession.getRegainAccessInfoCounter(); //

				passwordResetSession = new PasswordResetSession();
				passwordResetSession.setRegainAccessInfoCounter(emailBlockCounter);
			} else {
				passwordResetSession = new PasswordResetSession();
			}

			WebUtils.setSessionAttribute(request, CUSTOMER_PASSWORD, null);

			setRegainAccessTokenAttr(request);
		}

		if (passwordResetSession.getInteractionEvent() == null)
			passwordResetSession.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));

		if (!isFlagEnabled(MBFC224)) {
			final CaptchaSession captchaSession = new CaptchaSession(captchaSettings); //

			WebUtils.setSessionAttribute(request, CAPTCHA_SESSION, captchaSession);
		}

		if (passwordResetSession.getActNotFoundCounter() >= 3) {
			passwordResetSession.setActNotFoundCounter(0);
			model.put("MBSD2479", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD2479));
			model.put("actnotfound", true);
			return model;
		}

		if (passwordResetSession.isBlocked() || passwordResetSession.getActCounter() >= 3
				|| passwordResetSession.getCounterBlock() >= 3 || passwordResetSession.getDobCounter() >= 3
				|| passwordResetSession.getCounterFailed() >= 3) {
			passwordResetSession.setBlocked(true);
			model.put("blocked", passwordResetSession.isBlocked());
			model.put(MOBILE_APP, enrollment.isMobileApp());
			model.put(MOBILE_APP_TYPE, enrollment.getMobileAppType());

			return model;
		}

		if (passwordResetSession.isIncompleteEnroll()) {
			model.put("incompleteEnroll", true);
			model.put(MOBILE_APP, enrollment.isMobileApp());
			model.put(MOBILE_APP_TYPE, enrollment.getMobileAppType());

			return model;
		}
		model.put("passblocked", (String) WebUtils.getSessionAttribute(request, "passblocked"));
		WebUtils.setSessionAttribute(request, PASSWORD_RESET_SESSION, passwordResetSession);

		// months
		Map<Object, String> months = new LinkedHashMap<Object, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			months.put("", "Mes:");
		else
			months.put("", "Month:");
		SimpleDateFormat df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.MONTH, 0);
		for (int i = 0; i <= 11; i++) {
			String month = df.format(cal.getTime());
			months.put(i, month);
			cal.add(Calendar.MONTH, 1);
		}
		model.put("months", months);

		// days
		Map<Object, String> days = new LinkedHashMap<Object, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			days.put(0, "D&iacute;a:");
		else
			days.put(0, "Day:");

		df = new SimpleDateFormat("d");
		cal.set(Calendar.DAY_OF_MONTH, 1);
		for (int i = 1; i <= 31; i++) {
			String day = df.format(cal.getTime());
			days.put(i, day);
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		model.put("days", days);

		// years
		Map<Object, String> years = new LinkedHashMap<Object, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			years.put(0, "A&ntilde;o:");
		else
			years.put(0, "Year:");
		df = new SimpleDateFormat("yyyy");
		cal = GregorianCalendar.getInstance();
		cal.add(Calendar.YEAR, -12);
		for (int i = 0; i <= 100; i++) {
			String year = df.format(cal.getTime());
			years.put(Integer.parseInt(year), year);
			cal.add(Calendar.YEAR, -1);
		}
		model.put("years", years);

		// expYears
		Map<Object, String> expYears = new LinkedHashMap<Object, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			expYears.put("", "A&ntilde;o:");
		else
			expYears.put("", "Year:");
		df = new SimpleDateFormat("yyyy");
		cal = GregorianCalendar.getInstance();
		for (int i = 1; i <= 9; i++) {
			String year = df.format(cal.getTime());
			expYears.put(Integer.parseInt(year), year);
			cal.add(Calendar.YEAR, 1);
		}
		model.put("expYears", expYears);

		// expMonths
		Map<Object, String> expMonths = new LinkedHashMap<Object, String>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			expMonths.put("", "Mes:");
		else
			expMonths.put("", "Month:");
		df = new SimpleDateFormat("MMMM", RequestContextUtils.getLocale(request));
		cal = GregorianCalendar.getInstance();
		cal.set(Calendar.MONTH, 0);
		for (int i = 1; i <= 12; i++) {
			String month = df.format(cal.getTime());
			expMonths.put(i, month);
			cal.add(Calendar.MONTH, 1);
		}
		model.put("expMonths", expMonths);

		final String accountInfoType = enrollment.getAccountInfoType();//
		boolean hasIDA = false;//
		boolean hasCCA = false;//
		boolean hasAMEX = false;//
		boolean hasPFC = false;//
		boolean hasREDI_HELOCS = false;//
		boolean hasNonTrans = false;//


		Map<Object, CustomerProfile> customerProfiles = passwordResetSession.getCustomerProfileAccountMap();
		if (page >= 1 && customerProfiles != null && customerProfiles.entrySet() != null) {
			Set<Entry<Object, CustomerProfile>> customerProfilesSet = customerProfiles.entrySet();
			if (customerProfilesSet.size() > 0) {
				for (Entry<Object, CustomerProfile> entry : customerProfilesSet) {
					String accountType = entry.getValue().getEnrollmentAccountSubtype();
					String actnum = entry.getValue().getEnrollmentAccountNumber();
					if (accountType.equalsIgnoreCase("IDA")) {
						model.put("accountIDA", "TRUE");
						hasIDA = true;
					} else if (accountType.equalsIgnoreCase("CCA")) {
						if (actnum != null
								&& ((actnum.length() == 15 && actnum.startsWith("377")) || actnum.length() == 16
								&& actnum.startsWith("0377"))) {
							model.put("accountAMEX", "TRUE");
							accountType = "AMEX";
							enrollment.setAccountInfoType("AMEX");
							hasAMEX = true;
						} else {
							model.put("accountCCA", "TRUE");
							hasCCA = true;
							if ((entry.getValue().getEnrollmentAccountProductId() != null)
									&& (entry.getValue().getEnrollmentAccountProductId().equalsIgnoreCase("EVHLC") || entry
											.getValue().getEnrollmentAccountProductId().equalsIgnoreCase("EVHLP"))) {
								model.put("accountREDI_HELOCS", true);
								hasREDI_HELOCS = true;
								enrollment.setProductDescription("Redi Equity");
							} else {
								model.put("CCAwithoutREDI_HELOCS", true);
							}
						}
					} else if (accountType.equalsIgnoreCase("AMEX")) {
						model.put("accountAMEX", "TRUE");
						hasAMEX = true;
					} else if (accountType.equalsIgnoreCase("PFC")) {
						model.put("accountPFC", "TRUE");
						hasPFC = true;
					} else if((MBCA17 && getNonTransactionalProductSet().contains(accountType) || (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA640) && getNonTransactionalProductSet().contains(accountType)))){

						//MBCA17 & 190 BY:ET56509
						if(!hasIDA && !hasCCA && !hasAMEX && !hasPFC && !hasREDI_HELOCS) {
							model.put("nontransaccount", "TRUE");
						}
						hasNonTrans = true;

						if(accountType.equalsIgnoreCase("MLA")) {
							model.put("mbca17", MBCA17);
						}
						else if (accountType.equalsIgnoreCase("RVA") || accountType.equalsIgnoreCase("LEA")) {
							model.put("mbca640", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA640));
						}
					}
				}

				if (GenericValidator.isBlankOrNull(accountInfoType)) {
					if (hasIDA)
						enrollment.setAccountInfoType("IDA");
					else if (hasCCA) {
						enrollment.setAccountInfoType("CCA");
						if (hasREDI_HELOCS)
							enrollment.setProductDescription("Redi Equity");
					} else if (hasAMEX)
						enrollment.setAccountInfoType("AMEX");
					else if (hasPFC)
						enrollment.setAccountInfoType("PFC");
					else if(hasNonTrans) {
						enrollment.setAccountInfoType(NON_TRAN_ACC);
					}

				}
			}
		}

		if (page >= 2) {
			CustomerProfile customerProfileAcct = customerProfiles.get(passwordResetSession.getValidatedAcct());
			if (customerProfileAcct != null && customerProfileAcct.getUsername() != null)
				model.put("username", customerProfileAcct.getUsername());

			final String resource = String.class.cast(WebUtils.getSessionAttribute(request, REGAIN_ACCESS_RESOURCE));//

			model.put(REGAIN_ACCESS_RESOURCE, resource);
		}

		model.put(MOBILE_APP, enrollment.isMobileApp());
		model.put(MOBILE_APP_TYPE, enrollment.getMobileAppType());
		model.put("MBSFE303", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE303));
		model.put("errors", errors.getAllErrors());
		model.put("MBSFE291", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE291).toString());

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE291)) {
			String passwordBlockedMobile = ServletRequestUtils.getStringParameter(request, PASSWORD_BLOCKED_MOBILE, "web");
			if ("true".equalsIgnoreCase(passwordBlockedMobile)) {
				model.put(PASSWORD_BLOCKED_MOBILE, passwordBlockedMobile);
			} else {
				model.put(PASSWORD_BLOCKED_MOBILE, "");
			}
		}
		//MBSFE-303
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE303)) {

			List<FieldError> fieldErrors = new ArrayList<>();

			for (int i = 0; i < errors.getErrorCount(); i++) {
				fieldErrors.add((FieldError) errors.getFieldErrors().get(i));
			}

			int errorCount = errors.getErrorCount();

			if (page == 0) {

				if (errorCount == 1) {
					FieldError fieldError = (FieldError) errors.getAllErrors().get(0);
					if (fieldError.getField().equals(DOB_MONTH) || fieldError.getField().equals(DOB_DAY)
							|| fieldError.getField().equals(DOB_YEAR)) {
						model.put(ERRORS_TIPS, "date");
					} else {
						FieldError field = (FieldError) errors.getAllErrors().get(0);
						model.put(ERRORS_TIPS, field.getField());
					}
				} else if (errorCount > 1) {
					for (FieldError fieldError : fieldErrors) {
						if (fieldError.getField().equals(DOB_MONTH) || fieldError.getField().equals(DOB_DAY)
								|| fieldError.getField().equals(DOB_YEAR)) {
							errorCount--;
						}
					}
					if (errorCount > 0) {
						model.put(ERRORS_TIPS, "none");
					} else {
						model.put(ERRORS_TIPS, "date");
					}
				}

			} else if (page == 1) {
				if (errorCount == 1) {
					FieldError fieldError = (FieldError) errors.getAllErrors().get(0);
					if (fieldError.getField().equals(EXP_MONTH) || fieldError.getField().equals(EXP_YEAR)) {
						model.put(ERRORS_TIPS, "expDateCCA");
					} else {
						FieldError field = (FieldError) errors.getAllErrors().get(0);
						model.put(ERRORS_TIPS, field.getField());
					}
				} else if (errorCount > 1) {
					for (FieldError fieldError : fieldErrors) {
						if (fieldError.getField().equals(EXP_MONTH) || fieldError.getField().equals(EXP_YEAR)) {
							errorCount--;
						}
					}
					if (errorCount > 0) {
						model.put(ERRORS_TIPS, "none");
					} else {
						model.put(ERRORS_TIPS, "expDateCCA");
					}
				}

			} else {
				if (errorCount == 1) {
					FieldError fieldError = (FieldError) errors.getAllErrors().get(0);
					model.put(ERRORS_TIPS, fieldError.getField());
				} else {
					model.put(ERRORS_TIPS, "none");

				}
			}
		}

		//MBSFE-303 end

		model.put("MBSFE1590", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1590));

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, uSession != null && uSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return model;
	}

	@SuppressWarnings("unchecked")
	private List<String> getNonTransactionalProductSet() {
		String[] ntAccountsArray = getNonTransProductFlow().split(",");
		return Arrays.asList(ntAccountsArray);
	}

	public String getCancelView() {
		return cancelView;
	}

	public void setCancelView(String cancelView) {
		this.cancelView = cancelView;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public void setCaptchaSettings(CaptchaSettings captchaSettings) {
		this.captchaSettings = captchaSettings;
	}

	public Integer getMaxPasswordRecords() {
		return maxPasswordRecords;
	}

	public void setMaxPasswordRecords(Integer maxPasswordRecords) {
		this.maxPasswordRecords = maxPasswordRecords;
	}

	public String getPaymentvalidationrange() {
		return paymentvalidationrange;
	}

	public void setPaymentvalidationrange(String paymentvalidationrange) {
		this.paymentvalidationrange = paymentvalidationrange;
	}

	public String getNonTransEnrollAccs() {
		return nonTransEnrollAccs;
	}

	public void setNonTransEnrollAccs(String nonTransEnrollAccs) {
		this.nonTransEnrollAccs = nonTransEnrollAccs;
	}

	public String getNonTransProductFlow() {
		return nonTransProductFlow;
	}

	public void setNonTransProductFlow(String nonTransProductFlow) {
		this.nonTransProductFlow = nonTransProductFlow;
	}

	/**
	 * @return the regainAccessServiceModule
	 */
	public RegainAccessServiceModule getRegainAccessServiceModule () {
		return this.regainAccessServiceModule;
	}

	/**
	 * @param regainAccessServiceModule
	 */
	public void setRegainAccessServiceModule (RegainAccessServiceModule regainAccessServiceModule) {
		this.regainAccessServiceModule = regainAccessServiceModule;
	}

}
