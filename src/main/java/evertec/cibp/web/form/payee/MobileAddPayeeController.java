package evertec.cibp.web.form.payee;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.chemistry.opencmis.commons.impl.json.JSONArray;
import org.apache.chemistry.opencmis.commons.impl.json.JSONObject;
import org.apache.chemistry.opencmis.commons.impl.json.parser.JSONParser;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import com.evertecinc.www.PremiaPointsActivation.PointsCostCenter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.EStatementInteraction;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.GlobalPayeePreferences;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyAllCustomerEmailsRequest;
import evertec.cibp.core.services.requests.PremiaPointsRequest;
import evertec.cibp.core.services.responses.AddPayeeResponse;
import evertec.cibp.core.services.responses.AddPayeeResponse.Result;
import evertec.cibp.core.services.responses.DeleteCustomerPayeeResponse;
import evertec.cibp.core.utils.CibpValidationUtils;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.index.globalpayee.GlobalPayeeIndexBean;
import evertec.cibp.index.globalpayee.SearchResult;
import evertec.cibp.index.globalpayee.SearchResultImpl;
import evertec.cibp.mobilecash.core.template.utils.AccountMaskUtil;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.AddExistingPayeeBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.view.GlobalPayeeSearchResultView;
import evertec.otherservices.beans.UserAccount;
import evertec.prophit.service.bean.CustomerInquiryBean;
import evertec.prophit.service.bean.EbillIndType;
import evertec.prophit.service.bean.MaintenceEbillBean;
import evertec.prophit.service.bean.MaintenceEbillResponse;

public class MobileAddPayeeController extends MultiActionController {

	/** Logger */
	public final Log log = LogFactory.getLog(getClass());

	private static final String SCREEN = "Screen";
	private static final String EN_ESTE_MOMENTO = " en este momento.";
	private static final String AT_THIS_TIME = " at this time.";
	private static final String ERROR_ATENDIENDO_COMERCIO = "Error a&ntilde;adiendo su comercio ";
	private static final String UNABLE_TO_ADD_PAYEE = "Unable to add your payee ";
	private static final String ADDPAYEE_INVALIDPAYEE_ERROR = "addpayee.payee.invalid";
	private static final String DELETE_PAYEE_ERROR = "editpayee.verification.error";
	private static final String PAYEE_INVALID = "addpayee.payee.invalid";
	private static final String USER_SESSION = "userSession";
	private static final String SUCCESS_ADDED = "successAdded";
	private static final String ERROR_CODE = "errorCode";
	private static final String ERROR_MESSAGE = "errorMessage";
	private static final String PAYEE = "payee";
	private static final String PAYEE_ID = "payeeId";
	private static final String MOBILE_ADD_PAYEE_INFO = "mobileAddPayeeInfo";
	private static final String MOBILE_EDIT_PAYEE = "mobileEditPayee";
	private static final String LANGUAGE = "language";
	private static final int PAYEE_VISA = 359;
	private static final int PAYEE_MCARD = 223;
	private static final int PAYEE_AMEX = 1983;

	private CustomerServices customerService;
	private GlobalPayeeIndexBean globalPayeeIndex;
	private String hidePayees;
	private MessageSource messageSource;
	private String invalidEmailDomains;
	
	private String telenominaID;
	private String vendorID;
	private static AccountMaskUtil accountMask = new AccountMaskUtil();
	private static JsonSerializer<TVFrontEndPayee> payeeSerializer = new JsonSerializer<TVFrontEndPayee>() {  
	    @Override
	    public JsonElement serialize(TVFrontEndPayee src, Type typeOfSrc, JsonSerializationContext context) {
	        JsonObject jsonPayee = new JsonObject();
	       
	        jsonPayee.addProperty("frontEndId", src.getFrontEndId());
	        jsonPayee.addProperty("globalPayeeId", src.getGlobalPayeeId());
	        jsonPayee.addProperty("nickname", src.getNickname());
	        jsonPayee.addProperty("billingAccount", accountMask.mask(src.getBillingAccount()));

	        return jsonPayee;
	    }
	};
	private static Gson gson = new GsonBuilder().registerTypeAdapter(TVFrontEndPayee.class, payeeSerializer).create();
	
	public ModelAndView mobileAddPayee(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION);
		model.put(LANGUAGE, userSession.getLanguage());
		
		String sessionLanguage = userSession.getLanguage().equals("en")?"en":"es";
		String requestLanguage = Utils.getRSALanguage(request).equals("en")?"en":"es";
		if (!sessionLanguage.equals(requestLanguage)) {
			return new ModelAndView("redirect:mobileAddPayee?lang="+sessionLanguage);
		}
		String commonPayeeTreeRaw = "";

		// Load/Store common payee tree (first time retrieved from DB then from
		// userSession)
		if (userSession.getCommonGlobalPayeesTree() == null) {
			commonPayeeTreeRaw = customerService.getCommonPayeeTree();
			log.info("Get Commons Payees");
			userSession.setCommonGlobalPayeesTree(commonPayeeTreeRaw);
		} else {
			commonPayeeTreeRaw = userSession.getCommonGlobalPayeesTree();
		}

		if (!commonPayeeTreeRaw.isEmpty()) {
			if ((userSession.getCommonGlobalPaymentTypeMap() == null || userSession.getRtMap() == null)) {
				JSONParser jsonParser = new JSONParser();
				JSONArray commonPayeesTreeJSON = (JSONArray) jsonParser.parse(commonPayeeTreeRaw);

				if (commonPayeesTreeJSON != null) {
					List<Integer> commonPayeeIds = getCommonPayeeIds(commonPayeesTreeJSON);
					Map<Integer, GlobalPayee> ps = PaymentsFacade.getInstance().getGlobalPayeeById(commonPayeeIds, userSession);
					Map<String, String> globalMap = new HashMap<>();

					// RT Payment Logo
					Map<String, String> rtMap = new HashMap<>();

					Set<Entry<Integer, GlobalPayee>> entrymap = ps.entrySet();
					for (Entry<Integer, GlobalPayee> entry : entrymap) {
						if (entry != null && entry.getKey() != null && entry.getValue() != null
								&& entry.getValue().getPaymentType() != null) {
							globalMap.put(String.valueOf(entry.getKey()), entry.getValue().getPaymentType().name());
							rtMap.put(String.valueOf(entry.getKey()), entry.getValue().getRtIndicator());
						}
					}

					userSession.setCommonGlobalpayees(ps);
					userSession.setCommonGlobalPaymentTypeMap(globalMap);
					userSession.setRtMap(rtMap);
					model.put("ps", ps);
					model.put("globalMap", globalMap);
					model.put("rtMap", rtMap);
				}
			} else {
				model.put("ps", userSession.getCommonGlobalpayees());
				model.put("globalMap", userSession.getCommonGlobalPaymentTypeMap());
				model.put("rtMap", userSession.getRtMap());
			}
						
			model.put("commonPayeeTree", commonPayeeTreeRaw);
		}

		return new ModelAndView("mobileAddPayee", model);
	}

	private List<Integer> getCommonPayeeIds(JSONArray commonPayees) {
		List<Integer> ids = new LinkedList<Integer>();

		for (int i = 0; i < commonPayees.size(); i++) {
			JSONObject node = (JSONObject) commonPayees.get(i);

			if (node.containsKey("children")) {
				ids.addAll(getCommonPayeeIds((JSONArray) node.get("children")));
			} else {
				if (node.containsKey("id")) {
					ids.add(new Integer(node.get("id").toString()));
				}
			}
		}

		return ids;
	}

	public ModelAndView newPayeesFrame(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();

		return new ModelAndView("newPayeesFrame", model);
	}

	public ModelAndView mobileAddPayeeAjax(HttpServletRequest request, HttpServletResponse response) throws Exception {

		if (!globalPayeeIndex.isInitialized())
			return new ModelAndView("initializing");

		String s = ServletRequestUtils.getStringParameter(request, "s");
		Integer page = ServletRequestUtils.getIntParameter(request, "page");
		Integer maxResults = 15;
		if (ServletRequestUtils.getIntParameter(request, "maxResults") != null)
			maxResults = ServletRequestUtils.getIntParameter(request, "maxResults");
		if (page == null)
			page = 0;

		if (s == null) {
			GlobalPayeeSearchResultView paymentSearchResultView = new GlobalPayeeSearchResultView();
			SearchResult searchResult = new SearchResultImpl(new ArrayList<>());
			return new ModelAndView(paymentSearchResultView, "searchResult", searchResult);
		} else {
			SearchResult result = globalPayeeIndex.search(s, hidePayees);
			result.setPageSize(maxResults);

			Map<String, Object> m = new HashMap<>();
			m.put("searchResult", result);
			m.put("page", page);

			int totalPages = result.getNumberOfMatches() / maxResults;
			if ((result.getNumberOfMatches() % maxResults) > 0)
				totalPages++;

			LinkedHashMap<String, String> pagesMap = new LinkedHashMap<>();
			for (int i = 1; i <= totalPages && totalPages > 1; i++) {
				pagesMap.put(String.valueOf(i), String.valueOf(i));

			}
			m.put("pagesMap", pagesMap);

			if (result.getNumberOfMatches() <= 0) {
				if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
					m.put("noMatchMsg", "No Matches Found.");
				else
					m.put("noMatchMsg", "No opciones encontradas.");
			}
			return new ModelAndView("mobileAddPayeeAjax", m);
		}
	}

	private Locale getLanguage(UserSession userSession) {
		return new Locale(userSession.getLanguage());		
	}
	
	/** Get images and help texts */
	public ModelAndView mobileAddPayeeGetImgSrc(HttpServletRequest request, HttpServletResponse response) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION);
		Locale lang= getLanguage(userSession);
		Map<String, Object> model = new HashMap<>();
		
		String payeeId = ServletRequestUtils.getStringParameter(request, PAYEE_ID);
		model.put("imgSrc", this.messageSource.getMessage("addexistingpayee." + payeeId, null, "", lang));		
		model.put("customerEmail", userSession.getCustomerProfile().getEmail() != null ? userSession.getCustomerProfile().getEmail() : "");
		GlobalPayee global = customerService.getGlobalPayeeById(new Integer(payeeId));
		if (global == null)
			return new ModelAndView(MOBILE_ADD_PAYEE_INFO, model);
		model.put("globalPayee", global.getName());
		PaymentsFacade.getInstance().getGlobalPayeePreference(global, userSession);
		
		if (global.getPreference() != null) {			
			String language = Utils.getRSALanguage(request).equals("sp") ? "es" : "en";
			model.put("haspref", true);
			GlobalPayeePreferences pref = global.getPreference();
			if (pref.isSet(pref.EBILL) && pref.getEbill() && !new Integer(payeeId).equals(37))
				model.put("askforebill", true);
			if (pref.isSet(pref.VALIDATION_CODE) && pref.getValidationCode())
				model.put("askforzipcode", true);
			
			if (language.equalsIgnoreCase("en") && pref.isSet(pref.HELP_EN)) {				
				model.put("help", pref.getHelpEn().replace("\"", "\\\""));
			}
			else if (language.equalsIgnoreCase("es") && pref.isSet(pref.HELP_ES)) {				
				model.put("help", pref.getHelpEs().replace("\"", "\\\""));
			}						
			
		}
		model.put("eBillsFlag", StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS));
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)){
			// MBSFE-7 Eliminar opcion de ebill de AEE
			model.put("isCCA", (new Integer(payeeId).equals(359) ||new Integer(payeeId).equals(1983) || new Integer(payeeId).equals(223)));
		}else {
			model.put("isCCA", false);
		}
		
		return new ModelAndView(MOBILE_ADD_PAYEE_INFO, model);
	}

	public ModelAndView mobileSavePayee(HttpServletRequest request, HttpServletResponse response) throws Exception {

		AddExistingPayeeBean payeeBean = new AddExistingPayeeBean();

		if (GenericValidator.isBlankOrNull(payeeBean.getEmployeeRoutingNumber())) {
			payeeBean.setEmployeeRoutingNumber("021502011");
		}

		if (GenericValidator.isBlankOrNull(payeeBean.getRoutingNumber())) {
			payeeBean.setRoutingNumber("021502011");
		}

		payeeBean.setBillingNumber(ServletRequestUtils.getStringParameter(request, "billingNumber"));
		payeeBean.setBillingNumberConf(ServletRequestUtils.getStringParameter(request, "confirmBillingNumber"));
		payeeBean.setNickname(ServletRequestUtils.getStringParameter(request, "nickname"));
		payeeBean.setPayee(ServletRequestUtils.getStringParameter(request, PAYEE_ID));
		payeeBean.setActivateEbill(Boolean.valueOf(ServletRequestUtils.getStringParameter(request, "ebill")));
		payeeBean.setZipcode(ServletRequestUtils.getStringParameter(request, "zipCode"));
		payeeBean.setEditEmail(Boolean.valueOf(ServletRequestUtils.getStringParameter(request, "editEmail")));
		payeeBean.setEmail(ServletRequestUtils.getStringParameter(request, "email"));
		payeeBean.setEmailConf(ServletRequestUtils.getStringParameter(request, "emailConfirm"));		

		Map<String, Object> model = validate(request, payeeBean);
		if (!model.isEmpty()) {
			return new ModelAndView(MOBILE_ADD_PAYEE_INFO, model);
		} else {
			model = savePayee(request, payeeBean);
		}

		model.put("billingNumber", payeeBean.getBillingNumber());
		model.put("confirmBillingNumber", payeeBean.getBillingNumberConf());
		model.put("nickname", payeeBean.getNickname());
		model.put("eBill", payeeBean.getActivateEbill());
		
		
		return new ModelAndView(MOBILE_ADD_PAYEE_INFO, model);
	}
	
	public ModelAndView mobileDeletePayeeForm(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		
		String payeeId = ServletRequestUtils.getStringParameter(request, PAYEE_ID);
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION);
		deletePayee(request, model, payeeId, userSession);
		
		this.getUserPayeesToEdit(model, userSession, false);
		return new ModelAndView(MOBILE_EDIT_PAYEE, model);
	}
	
	public ModelAndView mobileDeletePayee(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		
		String payeeId = ServletRequestUtils.getStringParameter(request, PAYEE_ID);
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION);
		deletePayee(request, model, payeeId, userSession);
		
		this.getUserPayeesToEdit(model, userSession, true);
		return new ModelAndView(MOBILE_ADD_PAYEE_INFO, model);
	}

	private void deletePayee(HttpServletRequest request, Map<String, Object> model, String payeeId,
			UserSession userSession) {
		BankingSession bankingSession = userSession.getBankingSession();
		CustomerProfile profile = userSession.getCustomerProfile();
		Locale language= getLanguage(userSession);
		List<TVFrontEndPayee> payees = userSession.getPayees(true);
		TVFrontEndPayee pay = Utils.getPayeebyId(payeeId, payees);
		DeleteCustomerPayeeResponse deleteok = null;
		//AccountMaskUtil util = new AccountMaskUtil();
		String payeeInf = "";
		if (pay != null) {
			// CIBP-2548 JOCAMPO
			if (pay.getGlobalPayeeIdString() != null && pay.getGlobalPayeeIdString().equalsIgnoreCase(telenominaID)) {
				pay.setTelenomina(true);
			} else {
				pay.setTelenomina(false);
			}

			payeeInf = pay.getNickname() + " " + accountMask.mask(pay.getBillingAccount());

			boolean isHighRiskPayee = false;
			if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1701) && pay.getGlobalPayee() != null ) {
				PaymentsFacade.getInstance().getGlobalPayeePreference(pay.getGlobalPayee(), userSession);

				if (pay.getGlobalPayee().getPreference() != null)
					isHighRiskPayee = pay.getGlobalPayee().getPreference().getHighRisk();
			}
			// The user has an EBPP2 ID
			if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

				// Get the list of user accounts
				List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

				// Loop through all the accounts and match them with the
				// proper userAccount
				if (ebpp2UserAccounts != null && !ebpp2UserAccounts.isEmpty()) {
					pay.setEbpp2UserAccount(Utils.getEbpp2UserAccountsByAccount(pay.getBillingAccount(),
							userSession));
				}

				// If we found the account, delete it as well
				if (pay.getEbpp2UserAccount() != null) {

					deleteok = customerService.deleteCustomerPayeeWithErrorDescriptionForEbpp2(profile,
							bankingSession, pay, userSession.getEbpp2UserId(), pay.getEbpp2UserAccount()
									.getBillerAccountID(), Utils.generateCustomerInteractionEvent(request), isHighRiskPayee);
				} else {

					deleteok = customerService.deleteCustomerPayeeWithErrorDescription(profile, bankingSession,
							pay, Utils.generateCustomerInteractionEvent(request), isHighRiskPayee);
				}
			}
			// Use the original EBPP implementation
			else {

				// mpicado CIBP-1799, se cambio el llamado al metodo
				// getEbppCustomerModel() por el que se creo sobrecargado
				// getEbppCustomerModel(boolean ebppStatus)
				deleteok = customerService.deleteCustomerPayeeWithErrorDescription(profile, bankingSession, pay,
						Utils.generateCustomerInteractionEvent(request),
						isHighRiskPayee);
			}

			if (deleteok.getResult().equals(DeleteCustomerPayeeResponse.Result.SUCCESS)) {
				userSession.resetEbpp2Accounts();
				userSession.resetPayees();
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					userSession.setStatusmessage("Payee " + payeeInf + " was deleted.");
					model.put("statusMessage", "Payee " + payeeInf + " was deleted.");
				}
				else {
					userSession.setStatusmessage("Ha borrado el comercio: " + payeeInf);
					model.put("statusMessage", "Ha borrado el comercio: " + payeeInf);
				}
				
			} else {
				/*
				 * CIBP-1680 bgarcia Se agrega la logica para poner el
				 * detalle del mensaje
				 */
				String er = getTelepagoErrorDescriptor(deleteok.getErrorEBA(), language, deleteok.getErrorString());
				String error = "";
				if (er == null || er.equals("")) {
					userSession.setErrorDetail(deleteok.getErrorString());
					model.put(ERROR_CODE, deleteok.getErrorEBA().replaceAll("\\s+", " "));
					model.put(ERROR_MESSAGE, this.messageSource.getMessage(DELETE_PAYEE_ERROR, null, language) + payeeInf);
					model.put("statusMessage", deleteok.getErrorString().replaceAll("\\s+", " "));
				} else {
					userSession.setErrorDetail(er);
					model.put(ERROR_CODE, deleteok.getErrorEBA().replaceAll("\\s+", " "));
					model.put(ERROR_MESSAGE, this.messageSource.getMessage(DELETE_PAYEE_ERROR, null, language) + payeeInf);
					model.put("statusMessage", er.replaceAll("\\s+", " "));
				}
				userSession.setStatusmessage(error);
			}
		} else {
			model.put(ERROR_CODE, this.messageSource.getMessage(PAYEE_INVALID, null, language));
			model.put(ERROR_MESSAGE, this.messageSource.getMessage(PAYEE_INVALID, null, language));
			model.put("statusMessage", this.messageSource.getMessage(PAYEE_INVALID, null, language));
		}
	}
	
	public ModelAndView mobileEditPayee(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION);
		model.put(LANGUAGE, userSession.getLanguage());
		String sessionLanguage = userSession.getLanguage().equals("en")?"en":"es";
		String requestLanguage = Utils.getRSALanguage(request).equals("en")?"en":"es";
		if (!sessionLanguage.equals(requestLanguage)) {
			return new ModelAndView("redirect:mobileEditPayee?lang="+sessionLanguage);
		}
		
		this.getUserPayeesToEdit(model, userSession, false);
		return new ModelAndView(MOBILE_EDIT_PAYEE, model);
	}

	private Map<String, Object> savePayee(HttpServletRequest request, AddExistingPayeeBean payeeBean) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION);
		CustomerProfile profile = userSession.getCustomerProfile();
		Locale language= getLanguage(userSession);
		
		BankingSession bankingSession = userSession.getBankingSession();
		
		if (payeeBean.getPayee() == null) {
			model.put(PAYEE, this.messageSource.getMessage(ADDPAYEE_INVALIDPAYEE_ERROR, null, language));
			return model;
		}

		GlobalPayee global = customerService.getGlobalPayeeById(new Integer(payeeBean.getPayee()));
		if (global == null) {
			model.put(PAYEE, this.messageSource.getMessage(ADDPAYEE_INVALIDPAYEE_ERROR, null, language));
			return model;
		}

		PaymentsFacade.getInstance().getGlobalPayeePreference(global, userSession);

		model.put("globalPayee", global);

		if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			userSession.getGuidedSetupSession().setResubmitParent(true);
		}

		// The EBPP2 user id is not yet in session; add it
		if (!userSession.isEbpp2UserIdLookedUp()) {
			EbppEntitlement ent = null;
			if (profile.getEntitlementByType(EntitlementType.EBILLS) != null) {
				ent = new EbppEntitlement(profile.getEntitlementByType(EntitlementType.EBILLS));

				// If the username is blank, we need to look in the current EBPP
				// accounts ONLY
				if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
					userSession.setEbpp2UserId("");
				} else {
					userSession.setEbpp2UserId(ent.getUsername());
				}
				userSession.setEbpp2UserIdLookedUp(true);
			}
		}

		String nick = payeeBean.getNickname();
		if (nick == null || nick.equals(""))
			nick = global.getName();

		String billingAct = payeeBean.getBillingNumber();

		String email = payeeBean.getEmail();
		if (StringUtils.isNotEmpty(email) && GenericValidator.isEmail(email)) {
			// The user has an EBPP2 ID
			ModifyAllCustomerEmailsRequest macer = new ModifyAllCustomerEmailsRequest(
					Utils.generateCustomerInteractionEvent(request), profile, customerService,
					userSession.getAccounts(), bankingSession, userSession.getEbpp2UserId(),
					userSession.getPreferredLanguage(), payeeBean.getEmail());
			profile = CustomerUtils.modifyAllEmails(macer);
			userSession.setCustomerProfile(profile);
		}

		
		AddPayeeResponse res = null;
		boolean isHighRiskPayee = false;
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1701) && global.getPreference() != null) {
			isHighRiskPayee = global.getPreference().getHighRisk();
		}
		try {
			if (payeeBean.getActivateEbill()) {
				
				String zipcode = payeeBean.getZipcode();
				if (zipcode == null || zipcode.equals(""))
					zipcode = "00000";
				
				// Universal credit cards fix
				if (payeeBean.getPayee().equals("359") || payeeBean.getPayee().equals("517")
						|| payeeBean.getPayee().equals("518") || payeeBean.getPayee().equals("1983")
						|| payeeBean.getPayee().equals("223")) {

					TVFrontendAccount account = Utils.getAccountByAccountNumber(payeeBean.getBillingNumber(),
							userSession.getAccounts());

					if (account != null) {
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)
								&& account.getSubtype().equals("CCA")) {

							res = customerService.addCustomerPayeeWithEbill(profile, userSession.getBankingSession(),
									global, billingAct, nick, false, userSession.getEbpp2UserId(), zipcode,
									Utils.generateCustomerInteractionEvent(request), payeeBean.getPayee(), isHighRiskPayee);

							// activate the cca ebills on ida
							String acctNum = account.getAccountNumber();
							CustomerInquiryBean responseTsys;
							try {
								responseTsys = customerService.prophItService().makeCustomerInquiry(acctNum);
							} catch (Exception e) {
								logger.error("Tsys Error:" + e.getMessage());
								responseTsys = null;
							}
							customerService.prophItService().makeCustomerInquiry(acctNum);

							boolean mant = false;
							if (responseTsys != null && responseTsys.getEbillInd() != null) {
								String ebillInd = "";
								if (res.getErrorCode() == 0) {
									if (responseTsys.getEbillInd().equals(EbillIndType.ACTIVE.toString())) {
										ebillInd = EbillIndType.DEACTIVE.toString();
										mant = false;// Already Active
									} else if (responseTsys.getEbillInd().equals(EbillIndType.ELIGIBLE.toString())) {
										ebillInd = EbillIndType.ENROLL.toString();
										mant = true;
									}
								}
								if (mant) {
									MaintenceEbillBean bean = new MaintenceEbillBean();
									bean.setAcctNum(acctNum);
									bean.setCaseNumber(responseTsys.getCaseNumber());
									bean.setChannelId(responseTsys.getChannelId());
									bean.setEbillInd(ebillInd);
									bean.setImpersonateId(responseTsys.getImpersonateId());
									bean.setKeepCaseOpen(responseTsys.getKeepCaseOpen());
									bean.setNewContact(responseTsys.getNewContact());
									bean.setOrgId(responseTsys.getOrgId());
									bean.setOrgLevel(responseTsys.getOrgLevel());
									MaintenceEbillResponse responseMant = customerService.prophItService()
											.maintenceEBill(bean);
									if (responseMant != null) {
										if (responseMant.getMessage().indexOf("Successfull") != -1) {
											// Premia Report
											PremiaPointsRequest premiaRequest = new PremiaPointsRequest(null,
													String.valueOf(profile.getPermId()), acctNum, profile.getTaxId(),
													"Anadir Factura Electronica", "098", PointsCostCenter.INSFE,
													profile.getUsername(), "MiBanco", true);
											customerService.premiaPointsActivation(premiaRequest);
											CustomerInteractionEvent customerInteractionEvent = Utils
													.generateCustomerInteractionEvent(request);
											customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
													"Account: " + acctNum + " activate.");
											customerService.logUpdateStatementStatus(customerInteractionEvent,
													InteractionSeverity.INFO);
											res.setResult(Result.SUCCESS);
											customerService.sendEbillsCCAEmailActivationSuccess(
													userSession.getCustomerProfile(),
													userSession.getPreferredLanguage(), account.getNickname());
										} else {
											res.setResult(Result.ERROR);
											customerService.sendEbillsCCAEmailActivationFailure(
													userSession.getCustomerProfile(),
													userSession.getPreferredLanguage(), account.getNickname());
											CustomerInteractionEvent customerInteractionEvent = Utils
													.generateCustomerInteractionEvent(request);
											customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
													"unable to activate, reason: " + responseMant.getMessage());
											customerService.logUpdateStatementStatus(customerInteractionEvent,
													InteractionSeverity.INFO);

										}
									} else {
										res.setResult(Result.ERROR);
										customerService.sendEbillsCCAEmailActivationFailure(
												userSession.getCustomerProfile(), userSession.getPreferredLanguage(),
												account.getNickname());
										CustomerInteractionEvent customerInteractionEvent = Utils
												.generateCustomerInteractionEvent(request);
										customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
												"unable to activate, reason: " + "tsys response null");
										customerService.logUpdateStatementStatus(customerInteractionEvent,
												InteractionSeverity.INFO);
									}
								}
							} else {// Set error por ebill activation
								if (res.getResult().equals(Result.SUCCESS)) {
									res.setResult(Result.SUCCESS_NOEBILL);
									res.setErrorCode(Long.parseLong("99999"));
									if (res.getErrorEBA() == null) {
										res.setErrorEBA("Error TSYS");
									}
									customerService.sendEbillsCCAEmailActivationFailure(
											userSession.getCustomerProfile(), userSession.getPreferredLanguage(),
											account.getNickname());
								}
							}
						} else {
							res = customerService.addCustomerPayeeWithEbill(profile, userSession.getBankingSession(),
									global, billingAct, nick, true, userSession.getEbpp2UserId(), zipcode,
									Utils.generateCustomerInteractionEvent(request), payeeBean.getPayee(), isHighRiskPayee);
							userSession.resetEbpp2Accounts();
						}
					} else
						res = customerService.addCustomerPayeeWithEbill(profile, userSession.getBankingSession(),
								global, billingAct, nick, true, userSession.getEbpp2UserId(), zipcode,
								Utils.generateCustomerInteractionEvent(request), payeeBean.getPayee(), isHighRiskPayee);
					userSession.resetEbpp2Accounts();
				} else {
					res = customerService.addCustomerPayeeWithEbill(profile, userSession.getBankingSession(), global,
							billingAct, nick, true, userSession.getEbpp2UserId(), zipcode,
							Utils.generateCustomerInteractionEvent(request), payeeBean.getPayee(), isHighRiskPayee);
				}
				userSession.resetEbpp2Accounts();

				if (res.getErrorCode() != 0) {
					model.put(ERROR_CODE, res.getErrorEBA());
					String er = getTelepagoErrorDescriptor(res.getErrorEBA(), language, res.getErrorString());
					String error = "";
					if (er == null || er.equals("")) {if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							error = "Payee " + nick + " - " + billingAct + " was added but e-bill could not be activated.";
							model.put(ERROR_MESSAGE, error);
							model.put("statusMessage", res.getErrorString());
						} else {
							error = "A&ntilde;adimos su comercio " + nick + " - " + billingAct
									+ " pero no se pudo activar el e-bill.";
							model.put(ERROR_MESSAGE, error);
							model.put("statusMessage", res.getErrorString());
						}
					} else {
						model.put("statusMessage", er);
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							error = UNABLE_TO_ADD_PAYEE + nick + " - " + billingAct
									+ " at this time nor could the e-bill be activated.";
							model.put(ERROR_MESSAGE, error);
						} else {
							error = "No se pudo a&ntilde;adir su comercio " + nick + " - " + billingAct
									+ " en este momento ni se pudo activar el e-bill.";
							model.put(ERROR_MESSAGE, error);
						}
					}

				} else if (res.getErrorCode() == 0) {

					// Make it a frequent payee
					List<TVFrontEndPayee> payees = userSession.getPayees(true);
					TVFrontEndPayee payee = Utils.getPayeebyAccountNumber(payeeBean.getBillingNumber(), payees);

					if (payee != null) {
						customerService.modifyPayeeFrequentPreference(profile, bankingSession, payee, true);
						payee.getCustomerPayeePreferences().setFavorite(true);
						userSession.resetFavoritePayments();
					}

					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put(SUCCESS_ADDED, "Your payee " + nick + " - " + billingAct + " was added.");
					}
					else {
						model.put(SUCCESS_ADDED, "Su comercio " + nick + " - " + billingAct + " fue a&ntilde;adido exitosamente.");
					}
					//Send Email
					PreferredLanguage preferredLanguage = PreferredLanguage.SPANISH;
					if(Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						preferredLanguage = PreferredLanguage.ENGLISH;
					}					
					CustomerProfile customerProfile = userSession.getCustomerProfile();
					AccountMaskUtil mask = new AccountMaskUtil();
					Map<Object, Object> models = new HashMap<>();
					models.put("name", customerProfile.getFirstName());
					models.put("can", mask.mask(customerProfile.getEnrollmentAccountNumber()));
					models.put("payeeName", global.getName());
					models.put("nickName", nick);
					models.put("payeeNumber", mask.mask(billingAct));
					models.put("sucessful", true);
					customerService.sendEmailPayee(customerProfile, preferredLanguage, models );
					userSession.resetAccounts();
				} else {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put(ERROR_MESSAGE, UNABLE_TO_ADD_PAYEE + nick + " - " + billingAct + AT_THIS_TIME);
					}
					else {						
						model.put(ERROR_MESSAGE, ERROR_ATENDIENDO_COMERCIO + nick + " - " + billingAct + EN_ESTE_MOMENTO);
					}
					return model;
				}
			}			
			else {
				boolean telenomina = false;
				String employeeName = null;
				String employeeId = null;
				String employeeRoutingNumber = null;
				String accounttype = null;
				
				res = customerService.addCustomerPayeeWithoutEbillWithErrorDescription(profile, userSession.getBankingSession(), global, billingAct, nick, Utils.generateCustomerInteractionEvent(request), employeeName, employeeId, employeeRoutingNumber, accounttype, telenomina, isHighRiskPayee);
				
				if (res.getErrorCode() == 0 ) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						model.put(SUCCESS_ADDED,"Your payee " + nick + " - " + billingAct + " was added.");
					} else {
						model.put(SUCCESS_ADDED,"Su comercio " + nick + " - " + billingAct + " fue a&ntilde;adido exitosamente.");
					}
					//Send Email
					PreferredLanguage preferredLanguage = PreferredLanguage.SPANISH;
					if(Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						preferredLanguage = PreferredLanguage.ENGLISH;
					}					
					CustomerProfile customerProfile = userSession.getCustomerProfile();
					AccountMaskUtil mask = new AccountMaskUtil();
					Map<Object, Object> models = new HashMap<>();
					models.put("name", customerProfile.getFirstName());
					models.put("can", mask.mask(customerProfile.getEnrollmentAccountNumber()));
					models.put("payeeName", global.getName());
					models.put("nickName", nick);
					models.put("payeeNumber", mask.mask(billingAct));
					models.put("sucessful", true);
					customerService.sendEmailPayee(customerProfile, preferredLanguage, models );
					userSession.resetPayees();
				} else {
					/*
					 * CIBP-1680 bgarcia Se agrega la logica para poner el detalle del mensaje
					 */
					String er = getTelepagoErrorDescriptor(res.getErrorEBA(), language, res.getErrorString());
					String error = "";
					if (er == null || er.equals("")) {
						model.put("statusMessage", res.getErrorString());
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							error = UNABLE_TO_ADD_PAYEE + nick + " - " + billingAct + AT_THIS_TIME
									;
							model.put(ERROR_CODE, res.getErrorEBA());
						} else {
							error = ERROR_ATENDIENDO_COMERCIO + nick + " - " + billingAct
									+ EN_ESTE_MOMENTO ;
							model.put(ERROR_CODE, res.getErrorEBA());
						}
					} else {
						model.put("statusMessage", er);
						model.put(ERROR_CODE, res.getErrorEBA());
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							error = UNABLE_TO_ADD_PAYEE + nick + " - " + billingAct + AT_THIS_TIME;
						} else {
							error = ERROR_ATENDIENDO_COMERCIO + nick + " - " + billingAct + EN_ESTE_MOMENTO ;
							
						}
					}					
					model.put(ERROR_MESSAGE, error);
					return model;				
				}
			}
			model.put(SUCCESS_ADDED, true);

		} catch (RuntimeException e) {
			String error = "";
			model.put(ERROR_CODE, SCREEN);
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				error = UNABLE_TO_ADD_PAYEE + nick + " - " + billingAct + AT_THIS_TIME;
				model.put(ERROR_MESSAGE, error);
			} else {
				error = ERROR_ATENDIENDO_COMERCIO + nick + " - " + billingAct + EN_ESTE_MOMENTO;
				model.put(ERROR_MESSAGE, error);
			}
			return model;
		}
		return model;
	}

	private Map<String, Object> validate(HttpServletRequest request, AddExistingPayeeBean payeeBean) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USER_SESSION);
		Map<String, Object> model = new HashMap<>();
		payeeBean.setEmailError(false);
		Locale language= getLanguage(userSession);

		if (GenericValidator.isBlankOrNull(payeeBean.getPayee())) {
			model.put(PAYEE, this.messageSource.getMessage(ADDPAYEE_INVALIDPAYEE_ERROR, null, language));
			model.put(ERROR_CODE, SCREEN);
		}

		String id = payeeBean.getPayee();
		if (GenericValidator.isBlankOrNull(id) || !GenericValidator.matchRegexp(id, "^\\d+$")) {
			model.put(PAYEE, this.messageSource.getMessage(ADDPAYEE_INVALIDPAYEE_ERROR, null, language));
			model.put(ERROR_CODE, SCREEN);
			return model;
		}

		GlobalPayee global = customerService.getGlobalPayeeById(new Integer(payeeBean.getPayee()));
		if (global == null) {
			model.put(PAYEE, this.messageSource.getMessage(ADDPAYEE_INVALIDPAYEE_ERROR, null, language));
			model.put(ERROR_CODE, SCREEN);
		}

		String accountNumber = payeeBean.getBillingNumber();
		String nickname = payeeBean.getNickname();

		if (GenericValidator.isBlankOrNull(accountNumber) || !CibpValidationUtils.isValidPayeeAccountNumber(accountNumber)) {
			model.put(ERROR_MESSAGE,this.messageSource.getMessage("addpayee.billernumber.invalid", null, language));
			model.put(ERROR_CODE, SCREEN);
		}
		else if (GenericValidator.isBlankOrNull(payeeBean.getBillingNumberConf()) || 
				(!GenericValidator.isBlankOrNull(accountNumber) && !GenericValidator.isBlankOrNull(payeeBean.getBillingNumberConf()) && !payeeBean.getBillingNumber().equals(payeeBean.getBillingNumberConf()))) {
			model.put(ERROR_MESSAGE,this.messageSource.getMessage("addpayee.billerConfirmation.notmatch", null, language));
			model.put(ERROR_CODE, SCREEN);
		}
		else if (GenericValidator.isBlankOrNull(nickname) || !CibpValidationUtils.isValidPayeeNickname(nickname)) {
			model.put(ERROR_MESSAGE,this.messageSource.getMessage("addpayee.nickname.invalid", null, language).trim());
			model.put(ERROR_CODE, SCREEN);
		}
		PaymentsFacade.getInstance().getGlobalPayeePreference(global, userSession);

		if (global != null && global.getPreference() != null) {
			GlobalPayeePreferences pref = global.getPreference();

			if (payeeBean.getActivateEbill() && pref.isSet(pref.EBILL) && pref.getEbill()
					&& payeeBean.isEditEmail()) {

				String email = payeeBean.getEmail();
				String emailconfirm = payeeBean.getEmailConf();

				if (GenericValidator.isBlankOrNull(email)) {
					model.put(ERROR_MESSAGE, this.messageSource.getMessage("addpayee.email.requiered", null, language));
					model.put(ERROR_CODE, SCREEN);
				}
				if (!GenericValidator.isBlankOrNull(email) && !GenericValidator.isEmail(email)) {
					model.put(ERROR_MESSAGE, this.messageSource.getMessage("addpayee.email.invalid", null, language));
					model.put(ERROR_CODE, SCREEN);
				} else {

					if (!GenericValidator.isBlankOrNull(email) && !email.equals(emailconfirm)) {
						model.put(ERROR_MESSAGE, this.messageSource.getMessage("addpayee.confirmationEmail.notmatch", null, language));
						model.put(ERROR_CODE, SCREEN);
					}
				}
				if (email != null && !payeeBean.getEmailError()) {
					String[] domain = email.split("@");
					if (domain.length > 1 && Utils.haveType(this.invalidEmailDomains, domain[1])) {
						model.put(ERROR_MESSAGE, this.messageSource.getMessage("email.invaliddomain", null, language));
						model.put(ERROR_CODE, SCREEN);
					}
				}
			}
			if (payeeBean.getActivateEbill() && pref.isSet(pref.VALIDATION_CODE) && pref.getValidationCode()
					&& (GenericValidator.isBlankOrNull(payeeBean.getZipcode())
							|| !GenericValidator.matchRegexp(payeeBean.getZipcode(), "\\d{5}"))) {
				model.put(ERROR_MESSAGE, this.messageSource.getMessage("addpayee.zipcode.requiered", null, language));
				model.put(ERROR_CODE, SCREEN);
			}
		}
		return model;

	}

	/*
	 * CIBP-1680 bgarcia Obtiene el texto de error actualizado de los archivo
	 */
	private String getTelepagoErrorDescriptor(String codigo, Locale len, String errorCode) {
		String status = "";
		try {
			if (len.getLanguage() != "en")
				len = new Locale("es");

			status = this.messageSource.getMessage(codigo.trim(), null, len).toUpperCase();
		} catch (Exception e) {
			status = errorCode;
		}

		return status;
	}
	
	private void getUserPayeesToEdit(Map<String, Object> model, UserSession userSession, boolean toJSON) {
		boolean isVendorActive = StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR);
		if (!userSession.isLoadebpp())
			userSession.resetPayees();

		/*
		 * aqui establezco el valor de el MessageSource para que lo pueda usar
		 * el bean del core, me parece que podria hacerse accesando el context
		 * pero tuve algunos problemas al hacerlo.. mpicado CIBP-1799
		 */
		userSession.getCustomerService().setEBPPStatus(this.getEBPPMessageSourceStatus());

		// mpicado CIBP-1799 se cambia el llamado por el metodo sobrecargado que
		// se creo, para poder enviarle el status de EBPP
		List<TVFrontEndPayee> payees = userSession.getPayees(true);
		if(isVendorActive && payees != null && !payees.isEmpty()){
			for (Iterator<TVFrontEndPayee> it = payees.iterator(); it.hasNext();){
				TVFrontEndPayee payee = it.next();
				if(String.valueOf(payee.getGlobalPayeeId()).equalsIgnoreCase(telenominaID) || 
						String.valueOf(payee.getGlobalPayeeId()).equalsIgnoreCase(vendorID) || 
						payee.getPayeeName().contains("PAYROLL") || payee.getPayeeName().contains("VENDOR")){
					it.remove();
				}
			}
		}
		
		model.put("telenominaID", telenominaID);
		model.put("vendorID", vendorID);
		model.put("isVendorEnabled", String.valueOf(isVendorActive));
		// Get the list of user accounts
		List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccounts();

		// The user has an EBPP2 ID
		if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {
			// Loop through all the payees and match them with the proper
			// userAccount
			if (payees != null && !payees.isEmpty()) {
				for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
					TVFrontEndPayee payee = iterator.next();
					// Loop through all the accounts and match them with the
					// proper userAccount
					if (ebpp2UserAccounts != null && !ebpp2UserAccounts.isEmpty()) {
						payee.setEbpp2UserAccount(Utils.getEbpp2UserAccountsByAccount(payee.getBillingAccount(),
								userSession));
					}
					//new tsys check					
					if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)){
						if(Utils.getPayeeisCCA(payee.getBillingAccount(), userSession)){
							UserAccount userAcc = new UserAccount();
							model.put("eBillsFlag", true);
							/*Use Ajax Call tsysStatusPayees */							
							userAcc.setAccountLabel("CCA_TSYS_AJAX");
							payee.setEbpp2UserAccount(userAcc);
						}//End if is cca
						else{
							//*Hacer la validacion por global payee id*/	
							//359 Visa, 223 MC y 1983 Amex
							if(payee.getGlobalPayeeId() == PAYEE_VISA 
									|| payee.getGlobalPayeeId() == PAYEE_MCARD
									|| payee.getGlobalPayeeId() == PAYEE_AMEX){
								UserAccount userAcc = new UserAccount();
								model.put("eBillsFlag", true);					
								userAcc.setAccountLabel("CCA_NOT_FROM_USER");
								payee.setEbpp2UserAccount(userAcc);
							}
						}
					}//End if ebills flag	
				}
			}

			// Get the algaro e-Bills
			//List<UserAccount> ebpp2AlgaroEbills = userSession.getEbpp2UserAccountsAlgaroEbills();
			//model.put("ebpp2AlgaroEbills", ebpp2AlgaroEbills);

			// Get the algaro e-Statements
			//List<UserAccount> ebpp2AlgaroEstatements = userSession.getEbpp2UserAccountsAlgaroEstatements();
			//model.put("ebpp2AlgaroEstatements", ebpp2AlgaroEstatements);
		}
		if(toJSON)
			model.put("payees", gson.toJson(payees));
		else model.put("payees", payees);
		
		
	}
	
	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}


	/**
	 * @return the customerService
	 */
	public CustomerServices getCustomerService() {
		return customerService;
	}

	/**
	 * @param customerService
	 *            the customerService to set
	 */
	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	/**
	 * @return the globalPayeeIndex
	 */
	public GlobalPayeeIndexBean getGlobalPayeeIndex() {
		return globalPayeeIndex;
	}

	/**
	 * @param globalPayeeIndex
	 *            the globalPayeeIndex to set
	 */
	public void setGlobalPayeeIndex(GlobalPayeeIndexBean globalPayeeIndex) {
		this.globalPayeeIndex = globalPayeeIndex;
	}

	/**
	 * @return the hidePayees
	 */
	public String getHidePayees() {
		return hidePayees;
	}

	/**
	 * @param hidePayees
	 *            the hidePayees to set
	 */
	public void setHidePayees(String hidePayees) {
		this.hidePayees = hidePayees;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getInvalidEmailDomains() {
		return invalidEmailDomains;
	}

	public void setInvalidEmailDomains(String invalidEmailDomains) {
		this.invalidEmailDomains = invalidEmailDomains;
	}

	public String getTelenominaID() {
		return telenominaID;
	}

	public void setTelenominaID(String telenominaID) {
		this.telenominaID = telenominaID;
	}
	
	public String getVendorID() {
		return vendorID;
	}

	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}
}
