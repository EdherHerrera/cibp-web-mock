package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.web.enums.WebResponseStatus;
import evertec.cibp.web.form.bean.RemoteDepositEnrollInRDCBean;
import evertec.cibp.web.request.RDCEnrollmentInfoRequest;
import evertec.cibp.web.resources.StringResources;
import evertec.cibp.web.resources.ViewResources;
import evertec.cibp.web.response.RDCEnrollmentInfoResponse;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.StringResourcesUtils;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.enums.ResponseStatus;

/**
 * 
 * @author Evertec CIBP
 * @version 1.0
 *
 */
public class RemoteDepositEnrollInRDCForm extends BaseRemoteDepositForm {
	
	protected static final String STATUS = "status";
	protected static final String ERROR= "error";
	protected static final String ERROR_RESPONSE = "ERROR";
	
	

	public RemoteDepositEnrollInRDCForm() {
		super();
		setCommandClass(RemoteDepositEnrollInRDCBean.class);
		setFormView(ViewResources.REMOTE_DEPOSIT_ENROLL);
	}
	
	/**
	 * onSubmit
	 * @param request
	 * @param response
	 * @param submitDeposit
	 * @param error
	 * @throws Exception
	 * @return ModelAndView
	 */
	@Override
	protected ModelAndView onSubmit (HttpServletRequest request, HttpServletResponse response, Object submitDeposit,
			BindException error) throws Exception {

		final String maintenanceConstant = "maintenance";
		
		ResponseStatus responseStatusValue= null; // response code status
		String statusValue = ERROR_RESPONSE; // Status request
		String errorValue = ""; // Error value title
		String errorDescription = StringResources.REMOTE_DEPOSIT_ERROR_GENERIC; // text error description
		boolean isError = true; // if error exist
		
		Map<String, Object> model = new HashMap<>(0);
		final UserSession userSession = Utils.getUserSession(request);
		final RDCEnrollmentInfoRequest rdcEnrollmentReq = generateEnrollmentInfoRequest(userSession);
		final RDCEnrollmentInfoResponse rdcEnrollmentResp = Utils.enrollUserInRDC(rdcEnrollmentReq, userSession, request);
		
		if (rdcEnrollmentResp != null) {
			if (rdcEnrollmentResp.getResponseStatus() == WebResponseStatus.SUCCESS) {
				isError = false;
				responseStatusValue = rdcEnrollmentResp.getServiceResponseStatus();
			} else if (rdcEnrollmentResp.getResponseStatus() == WebResponseStatus.MAINTENANCE) {
				statusValue = maintenanceConstant;
				errorDescription = StringResources.REMOTE_DEPOSIT_ERROR_MAINTENANCE;
			} else {
				responseStatusValue = rdcEnrollmentResp.getServiceResponseStatus();
			}
		}
		
		if (isError) {
			errorValue = StringResourcesUtils.getStringResource(messageSource, request, errorDescription);
		}
		
		model.put(STATUS, responseStatusValue != null ? responseStatusValue : statusValue);
		model.put(ERROR, errorValue);
		
		return showForm(request, error, ViewResources.REMOTE_DEPOSIT_ENROLL, model);
	}
	
	/**
	 * getGlobalEntitlement
	 * @return EntitlementType
	 */
	public EntitlementType getGlobalEntitlement() {
		return EntitlementType.RDC;
	}

}
