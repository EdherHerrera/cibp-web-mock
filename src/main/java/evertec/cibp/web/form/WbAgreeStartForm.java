package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.signup.SignupCampaign;
import evertec.cibp.web.form.bean.CampaignConcurrencyBean;
import evertec.cibp.web.session.UserSession;

public class WbAgreeStartForm extends CancellableFormController {

	private CampaignConcurrencyBean concurrencyBean;

	public WbAgreeStartForm() {
		setCommandClass(SignupCampaign.class);
		setCommandName("wbaStart");
		setFormView("wbaStart");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String hidden = ServletRequestUtils.getStringParameter(request, "actiontype");

		if ("cancel".equals(hidden)) {
			WebUtils.setSessionAttribute(request, "skipwba", true);
		} else {
			List<SignupCampaign> signupCampaignInfoByProfileIdA = userSession.getCustomerSignupCampaigns();
			for (Iterator<SignupCampaign> iter = signupCampaignInfoByProfileIdA.iterator(); iter.hasNext();) {
				SignupCampaign camp = (SignupCampaign) iter.next();
				if (camp.getCampaignType().equals(CampaignType.WBA.toString())) {
					camp.setCampaignResult(obtenerResultado(hidden));
					userSession.getCustomerService().updateSignupCampInfo(camp);
					break;
				}
			}
			concurrencyBean.subConcurrency();
		}

		return new ModelAndView("redirect:portal");
	}

	private char obtenerResultado(String hidden) {
		if ("yes".equals(hidden)) {
			return 'Y';
		} else {
			return 'N';
		}
	}

	@Override
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<String, Object> modelo = new HashMap<String, Object>();

		List<SignupCampaign> listaCampannas = userSession.getCustomerSignupCampaigns();
		for (SignupCampaign signupCampaign : listaCampannas) {
			if (signupCampaign.getCampaignType().equals(CampaignType.WBA.toString())) {
				modelo.put("cuentas", signupCampaign.getCuentas());
				if (signupCampaign.getDdaOnly().trim().equals("Y")) {
					modelo.put("ddaonly", signupCampaign.getDdaOnly());
				}
			}
		}
		return modelo;
	}

	public void setConcurrencyBean(CampaignConcurrencyBean concurrencyBean) {
		this.concurrencyBean = concurrencyBean;
	}

	public CampaignConcurrencyBean getConcurrencyBean() {
		return concurrencyBean;
	}

	// public void enviarResultados(UserSession userSession){
	// CustomerServices servicios = userSession.getCustomerService();
	// servicios.sendWbaResponsesEmail(PreferredLanguage.SPANISH,
	// "dsalas@evertec-la.com", "Lista de respuestas de usuario");
	//
	// }

}
