package evertec.cibp.web.form.fis.enroll;

import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.requests.adaptiveauth.EnrollmentInformationinRSARequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.form.fis.enroll.bean.TermsAndCondsSECBean;
import evertec.cibp.web.session.EnrollmentSECSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class TermsAndCondsSECForm extends CancellableFormController {

	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	private static final String ENROLL_SEC_CUSTOMER_SESSION = "enrollSECCustSession";

	public TermsAndCondsSECForm() {
		super();
		setCommandClass(TermsAndCondsSECBean.class);
		setCommandName("termsandcondssec");
		setFormView("termsandcondssec");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		TermsAndCondsSECBean bean = (TermsAndCondsSECBean) command;
		EnrollmentSECSession enrollmentSECBean = (EnrollmentSECSession) WebUtils.getSessionAttribute(request,
				ENROLL_SEC_CUSTOMER_SESSION);
		enrollmentSECBean.setAgreement(bean.isAcceptTerms());

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customerProfile = session.getCustomerProfile();
		DeviceRequest deviceRequest = session.getDeviceRequest();

		// Add to RSA
		String q1 = enrollmentSECBean.getQuestion1();
		String q2 = enrollmentSECBean.getQuestion2();
		String q3 = enrollmentSECBean.getQuestion3();
		String a1 = enrollmentSECBean.getAnswer1();
		String a2 = enrollmentSECBean.getAnswer2();
		String a3 = enrollmentSECBean.getAnswer3();
		String computerType = enrollmentSECBean.getComputerType();
		boolean saveDevice = false; // REVISAR
		if ("private".equals(computerType))
			saveDevice = true;

		String username = customerProfile.getUsername().toLowerCase();
		EnrollmentInformationinRSARequest req = new EnrollmentInformationinRSARequest(
				Utils.generateCustomerInteractionEvent(request), username,
				customerProfile.getEmail(), saveDevice, deviceRequest);
		req.setQuestionsAndAnswer( q1,q2, q3, a1, a2, a3);

		/**
		 * MBNA-2518 Method enrollRSA : CIBP-WEB microservice integration
		 * 
		 */
		ModifyCustomerProfileResponse enrollRSAresponse =session.getCustomerService().getSignOnCustomerServiceModule().enrollRSA(req);
		if (!ResultStatus.SUCCESS.equals(enrollRSAresponse.getStatus())) {
			error.reject("acceptTerms", "mbfisTerms.errorMessage");
		}

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
		cookie.setPath("/");
		
		customerProfile.setStatus(CustomerStatus.ACTIVE);

		int newpasswordsalt = PasswordHashUtil.genereateRandomSalt();
		String newpasswordhash = PasswordHashUtil.generateSaltedHash(enrollmentSECBean.getPassword(), newpasswordsalt);
		
		CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request);
		CustomerServices customerService = session.getCustomerService();
		ModifyCustomerRequest modifyCustomer = new ModifyCustomerRequest();
		modifyCustomer.setInteractionEvent(interactionEvent);
		modifyCustomer.setProfileId(session.getCustomerProfile().getProfileId());
		modifyCustomer.setNewPasswordHash(newpasswordhash);
		modifyCustomer.setNewPasswordSalt(newpasswordsalt);
		customerService.resetPassword(modifyCustomer);
		
		WebUtils.setSessionAttribute(request, "userSession", session);
		WebUtils.setSessionAttribute(request, "showOOBSEC", "T");
		WebUtils.setSessionAttribute(request, ENROLL_SEC_CUSTOMER_SESSION, null);
		return new ModelAndView("redirect:welcome");
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {
		Map<String, Object> map = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		userSession.getUsername();
		map.put("url", "mbfisTerms.url");
		return map;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
		TermsAndCondsSECBean bean = (TermsAndCondsSECBean) command;

		if (!bean.isAcceptTerms()) {
			errors.rejectValue("acceptTerms", "mbfisTerms.requiredMessage");
		}
	}
}
