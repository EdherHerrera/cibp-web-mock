package evertec.cibp.web.form.fis.enroll;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.web.form.bean.PasswordBean;
import evertec.cibp.web.form.bean.ValidateInfoBean;
import evertec.cibp.web.form.fis.enroll.bean.AccountInformationSECBean;
import evertec.cibp.web.session.EnrollmentSECSession;
import evertec.cibp.web.session.UserSession;

public class AccountInformationSECForm extends CancellableFormController {
	
	private static final String ENROLL_SEC_CUSTOMER_SESSION = "enrollSECCustSession";

	public AccountInformationSECForm() {
		super();
		setCommandClass(AccountInformationSECBean.class);
		setCommandName("accountinformationsec");
		setFormView("accountinformationsec");
	}
	
	@Override

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		
	AccountInformationSECBean info = (AccountInformationSECBean) super.formBackingObject(request);
	String type = ServletRequestUtils.getStringParameter(request, "agentType", "web");
	if ("android".equalsIgnoreCase(type) || "ios".equalsIgnoreCase(type)) {
			info.setMobileAppType(type);
			info.setMobileApp(Boolean.TRUE);
		}
			return info;
	}
	
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		AccountInformationSECBean bean = (AccountInformationSECBean)command;
		EnrollmentSECSession enrollmentSECSession = (EnrollmentSECSession) WebUtils.getSessionAttribute(request,
				ENROLL_SEC_CUSTOMER_SESSION);
		if (enrollmentSECSession==null) {
			enrollmentSECSession = new EnrollmentSECSession();
		}else
			enrollmentSECSession.setAccountnumber(bean.getAccountnumber());
		WebUtils.setSessionAttribute(request, ENROLL_SEC_CUSTOMER_SESSION, enrollmentSECSession);
		
		return new ModelAndView("redirect:resetpasswordsec");
	}
	
	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
		Map<String, Object> map = new HashMap<>();
		AccountInformationSECBean info = (AccountInformationSECBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		userSession.getUsername();
		map.put("mobileApp", info.isMobileApp());
		return map;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		AccountInformationSECBean bean = (AccountInformationSECBean)command;

		if (bean.getAccountnumber() == null || bean.getAccountnumber().equals("")) {
			errors.rejectValue("accountnumber", "mbfisEnrollment.account.validation.empty", "Please enter account information");
			return;
		}
		
		if (userSession == null || !matchAccounts(userSession.getAccounts(), bean.getAccountnumber())) {
			errors.rejectValue("accountnumber", "mbfisEnrollment.account.validation.error", "Please verify your account information");
		}
	}
	
	
	private boolean matchAccounts(List<TVFrontendAccount> listAccount, String accountNumber) {
		for (Iterator<TVFrontendAccount> iterator = listAccount.iterator(); iterator.hasNext();) {
			TVFrontendAccount userAccount = iterator.next();
			if (userAccount.getAccountNumber().equalsIgnoreCase(accountNumber) && userAccount.getSubtype().equalsIgnoreCase("SEC")) {
				return true;
			}
		}	
		return false;
	}

}
