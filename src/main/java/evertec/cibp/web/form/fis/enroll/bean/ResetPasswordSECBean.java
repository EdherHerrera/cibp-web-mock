package evertec.cibp.web.form.fis.enroll.bean;

public class ResetPasswordSECBean {
	private String password;
	private String passwordconfirm;
	private String mobileAppType;
	private boolean mobileApp;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordconfirm() {
		return passwordconfirm;
	}
	public void setPasswordconfirm(String passwordconfirm) {
		this.passwordconfirm = passwordconfirm;
	}
	public String getMobileAppType() {
		return mobileAppType;
	}
	public void setMobileAppType(String mobileAppType) {
		this.mobileAppType = mobileAppType;
	}
	public boolean isMobileApp() {
		return mobileApp;
	}
	public void setMobileApp(boolean mobileApp) {
		this.mobileApp = mobileApp;
	}
	
}
