package evertec.cibp.web.form.fis.enroll;

import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.requests.adaptiveauth.EnrollmentInformationinRSARequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionSelectionResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.fis.enroll.bean.SecurityInformationSECBean;
import evertec.cibp.web.session.EnrollmentSECSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class SecurityInformationSECForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	private MessageSource messageSource;
	private static final String ENROLL_SEC_CUSTOMER_SESSION = "enrollSECCustSession";
	


	public SecurityInformationSECForm() {
		super();
		setCommandClass(SecurityInformationSECBean.class);
		setCommandName("securityinformationsec");
		setFormView("securityinformationsec");
	}
	
	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		SecurityInformationSECBean info = (SecurityInformationSECBean) super.formBackingObject(request);
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.RSA_MOBILE)) {
			String type = ServletRequestUtils.getStringParameter(request, "agentType", "web");
			if ("android".equalsIgnoreCase(type) || "ios".equalsIgnoreCase(type)) {
				info.setMobileAppType(type);
				info.setMobileApp(Boolean.TRUE);
			}
		}
		return info;
	}

	
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		SecurityInformationSECBean info = (SecurityInformationSECBean) command;
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		EnrollmentSECSession enrollmentSECSession = (EnrollmentSECSession) WebUtils.getSessionAttribute(request,
				ENROLL_SEC_CUSTOMER_SESSION);
		
		if (enrollmentSECSession==null) {
			enrollmentSECSession = new EnrollmentSECSession();
		}
				
		// Add to RSA
		String q1 = info.getQuestion1();
		String q2 = info.getQuestion2();
		String q3 = info.getQuestion3();
		String a1 = info.getAnswer1();
		String a2 = info.getAnswer2();
		String a3 = info.getAnswer3();
		String computerType = info.getComputerType();		
		enrollmentSECSession.setQuestion1(q1);
		enrollmentSECSession.setAnswer1(a1);
		enrollmentSECSession.setQuestion2(q2);
		enrollmentSECSession.setAnswer2(a2);
		enrollmentSECSession.setQuestion3(q3);
		enrollmentSECSession.setAnswer3(a3);
		enrollmentSECSession.setComputerType(computerType);
		
		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
		cookie.setPath("/");
 		
		ESAPI.httpUtilities().addCookie(response, cookie);

		
		if (session.getCustomerProfile().getStatus().equals(CustomerStatus.ENROLLED)) {
			WebUtils.setSessionAttribute(request, ENROLL_SEC_CUSTOMER_SESSION, enrollmentSECSession);
			return new ModelAndView("redirect:termsandcondssec");
		}else { //Save the RSA configuration
			
			boolean saveDevice = false; 
			if ("private".equals(computerType))
				saveDevice = true;
			
			DeviceRequest deviceRequest = session.getDeviceRequest();
			String username = session.getCustomerProfile().getUsername().toLowerCase();
			EnrollmentInformationinRSARequest req = new EnrollmentInformationinRSARequest(
					Utils.generateCustomerInteractionEvent(request), username,
					session.getCustomerProfile().getEmail(), saveDevice,deviceRequest);
			req.setQuestionsAndAnswer( q1,q2, q3, a1, a2, a3);

			ModifyCustomerProfileResponse enrollRSAresponse = session.getCustomerService().getSignOnCustomerServiceModule()
					.enrollRSA(req);
			if (!ResultStatus.SUCCESS.equals(enrollRSAresponse.getStatus())) {
				error.rejectValue("computerType", "mbfis.general.errrorMessage", "An error has ocurred");
			}
			//Delete RSA_ENROLL Task
			session.getTasksList().remove(ASK_FOR.RSA_ENROLL);
			return new ModelAndView("redirect:welcome");
		}
		
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		SecurityInformationSECBean info = (SecurityInformationSECBean) command;
		
				String q1 = info.getQuestion1();
				String q2 = info.getQuestion2();
				String q3 = info.getQuestion3();
				String a1 = info.getAnswer1();
				String a2 = info.getAnswer2();
				String a3 = info.getAnswer3();
				String a1c = info.getAnswer1confirm();
				String a2c = info.getAnswer2confirm();
				String a3c = info.getAnswer3confirm();
				String computerType = info.getComputerType();
				String re = "^\\w+(\\s{1}\\w+)*$";
				
				if (GenericValidator.isBlankOrNull(q1)) {
					errors.rejectValue("question1", "enrollment.sec.question.required", "Required");
				}
				if (GenericValidator.isBlankOrNull(q2)) {
					errors.rejectValue("question2", "enrollment.sec.question.required", "Required");
				}
				if (GenericValidator.isBlankOrNull(q3)) {
					errors.rejectValue("question3", "enrollment.sec.question.required", "Required");
				}

				if (GenericValidator.isBlankOrNull(a1) || !GenericValidator.matchRegexp(a1, re)) {
					errors.rejectValue("answer1", "enrollment.sec.answer.invalid", "Please enter a valid answer");

				} else if (!GenericValidator.isBlankOrNull(a1) && !a1.equals(a1c)) {
					errors.rejectValue("answer1confirm", "enrollment.sec.answerconfirm.notmatch",
							"Answers values must match");
				}

				if (GenericValidator.isBlankOrNull(a2) || !GenericValidator.matchRegexp(a2, re)) {
					errors.rejectValue("answer2", "enrollment.sec.answer.invalid", "Please enter a valid answer");

				} else if (!GenericValidator.isBlankOrNull(a2) && !a2.equals(a2c)) {
					errors.rejectValue("answer2confirm", "enrollment.sec.answerconfirm.notmatch",
							"Answers values must match");

				}

				if (GenericValidator.isBlankOrNull(a3) || !GenericValidator.matchRegexp(a3, re)) {
					errors.rejectValue("answer3", "enrollment.sec.answer.invalid", "Please enter a valid answer.");

				} else if (!GenericValidator.isBlankOrNull(a3) && !a3.equals(a3c)) {
					errors.rejectValue("answer3confirm", "enrollment.sec.answerconfirm.notmatch",
							"Answers values must match");

				}
				if (!"private".equals(computerType) && !"public".equals(computerType)) {
					errors.rejectValue("computerType", "enrollment.sec.computerType.required",
							"Please select a computer type");

				}
			
	}
	
	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = session.getCustomerProfile();
		Map<String, Object> model = new HashMap<>();

		// Questions
		

			Map<String, String> questions = new LinkedHashMap<String, String>();
			String selectquestiontext = "Select a question:";
			if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
				selectquestiontext = "Seleccione una pregunta:";
			questions.put("", selectquestiontext);

			CustomerInteractionEvent generateCustomerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
			RSAActionRequest actionrequest = new RSAActionRequest(generateCustomerInteractionEvent, profile
					.getUsername().toLowerCase(), "", profile.getEmail(), 0, false, session.getDeviceRequest());
			actionrequest.setRsaLang("sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "ES" : "EN");
			
			/**
			 * MBNA-2536
			 * Method getQuestionsSelection : CIBP-WEB microservice integration
			 */
			QuestionSelectionResponse questionResponse = session.getCustomerService().rsaGetQuestionsSelection(actionrequest);

			Cookie cookie = WebUtils.getCookie(request, "PMData");
			if (cookie == null)
				cookie = new Cookie("PMData", "");
			cookie.setPath("/");
			cookie.setValue(questionResponse.getRsaCookie());

			if (questionResponse.getStatusResult().equals(ResultStatus.SUCCESS)) {
				Map<Object, Object> q = new HashMap<Object, Object>();
				q.put("", selectquestiontext);
				q.putAll(questionResponse.getQuestionMap1());
				model.put("questions1", q);

				Map<Object, Object> q2 = new HashMap<Object, Object>();
				q2.put("", selectquestiontext);
				q2.putAll(questionResponse.getQuestionMap2());
				questions = new LinkedHashMap<String, String>();
				questions.put("", selectquestiontext);
				model.put("questions2", q2);
				
				Map<Object, Object> q3 = new HashMap<Object, Object>();
				q3.put("", selectquestiontext);
				q3.putAll(questionResponse.getQuestionMap3());
				questions = new LinkedHashMap<String, String>();
				questions.put("", selectquestiontext);
				model.put("questions3", q3);
			}

		return model;
	}
	
	
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	
}
