package evertec.cibp.web.form.fis.enroll.bean;

public class TermsAndCondsSECBean {

	private boolean acceptTerms;

	public boolean isAcceptTerms() {
		return acceptTerms;
	}

	public void setAcceptTerms(boolean acceptTerms) {
		this.acceptTerms = acceptTerms;
	}
	
}
