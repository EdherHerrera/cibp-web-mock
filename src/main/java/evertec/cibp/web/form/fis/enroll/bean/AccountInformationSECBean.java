/**
 * 
 */
package evertec.cibp.web.form.fis.enroll.bean;

/**
 * @author ET55084
 *
 */
public class AccountInformationSECBean {

	private String accountnumber;
	private String mobileAppType;	
	private boolean mobileApp;
	
	public boolean isMobileApp() {
		return mobileApp;
	}

	public void setMobileApp(boolean mobileApp) {
		this.mobileApp = mobileApp;
	}

	public String getMobileAppType() {
		return mobileAppType;
	}

	public void setMobileAppType(String mobileAppType) {
		this.mobileAppType = mobileAppType;
	}
	

	public String getAccountnumber() {
		return accountnumber;
	}

	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}
	
}
