package evertec.cibp.web.form.fis.enroll;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.form.fis.enroll.bean.ResetPasswordSECBean;
import evertec.cibp.web.session.EnrollmentSECSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class ResetPasswordSECForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	
	private Integer maxPasswordRecords;
	private static final String ENROLL_SEC_CUSTOMER_SESSION = "enrollSECCustSession";
	private static final String PASSWORD = "password";
	private static final String PASSWORD_CONFIRM = "passwordconfirm";
	private static final String REQUIRED = "Required";
	private CustomerProfile customerProfile;

	private static final String ENROLLMENT_PASSWORD_CONTAINS_USERNAME = "enrollment.pwd.contains.username";
	private static final String PASSWORD_ERROR = "password";
	private static final String PASSWORD_CONTAINS_USERNAME = "Password must not contains username";

	public ResetPasswordSECForm() {
		super();
		setCommandClass(ResetPasswordSECBean.class);
		setCommandName("resetpasswordsec");
		setFormView("resetpasswordsec");
	}
	
	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		ResetPasswordSECBean resetPasswordSecBean = (ResetPasswordSECBean) super.formBackingObject(request);

		String type = ServletRequestUtils.getStringParameter(request, "agentType", "web");
		if ("android".equalsIgnoreCase(type) || "ios".equalsIgnoreCase(type)) {
			resetPasswordSecBean.setMobileAppType(type);
			resetPasswordSecBean.setMobileApp(Boolean.TRUE);

		}
		return resetPasswordSecBean;
	}


	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		
		
		ResetPasswordSECBean dude = (ResetPasswordSECBean) command; 

		EnrollmentSECSession enrollmentSECSession = (EnrollmentSECSession)
		WebUtils.getSessionAttribute(request, ENROLL_SEC_CUSTOMER_SESSION);
		enrollmentSECSession.setPassword(dude.getPassword());
		enrollmentSECSession.setPasswordconfirm(dude.getPasswordconfirm());
		WebUtils.setSessionAttribute(request, ENROLL_SEC_CUSTOMER_SESSION, enrollmentSECSession);
		
		return new ModelAndView("redirect:securityinformationsec");
	}
	
	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException error)
			throws Exception {
		ResetPasswordSECBean dude = (ResetPasswordSECBean) command; 
		String password = dude.getPassword();
		String passwordconfirm = dude.getPasswordconfirm();
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		if (GenericValidator.isBlankOrNull(password)) {
			error.rejectValue(PASSWORD, "enrollment.sec.resetpassword.pwd.required", REQUIRED);
		}
		else if (!(GenericValidator.maxLength(password, 15) && GenericValidator.minLength(password, 8))) {
			error.rejectValue(PASSWORD, "enrollment.sec.resetpassword.pwd.invalid", "Invalid Password");
		}
		else if (!Utils.isValidPassword(dude.getPassword())) {
			error.rejectValue(PASSWORD, "enrollment.sec.resetpassword.pwd.invalid", "Invalid Password");
		}
		else if (!password.equals(passwordconfirm)) {
			error.rejectValue(PASSWORD_CONFIRM, "enrollment.sec.resetpassword.pwd.mustmatch", "Password must match");
		} 
		else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989)
				&& password.toLowerCase().contains(session.getCustomerProfile().getUsername().toLowerCase())) {
			error.rejectValue(PASSWORD_ERROR, ENROLLMENT_PASSWORD_CONTAINS_USERNAME, PASSWORD_CONTAINS_USERNAME);
		}
		else {
			// NO EQUALS ACTUAL PASS
			int passwordSalt = 0;
			String passwordHash = "";
			customerProfile = session.getCustomerProfile();
			if (customerProfile!= null && customerProfile.getPasswordSalt() != 0) {
				passwordSalt = customerProfile.getPasswordSalt();
				passwordHash = customerProfile.getPasswordHash();
			}			
			int originalSalt = passwordSalt;
			String originalHash = passwordHash;
			String newHash = PasswordHashUtil.generateSaltedHash(password, originalSalt);
			if (newHash.equals(originalHash)) {
				error.rejectValue(PASSWORD, "enrollment.sec.resetpassword.pwd.used", "Incorrect Password");
			}
		}
	}
	
	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession session = UserSession.class.cast(WebUtils.getSessionAttribute(request, "userSession"));
		CustomerProfile profile = session.getCustomerProfile();
		CustomerInteractionEvent generateCustomerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
		RSAActionRequest actionrequest = new RSAActionRequest(generateCustomerInteractionEvent, profile
				.getUsername().toLowerCase(), "", profile.getEmail(), 0, false, session.getDeviceRequest());
		actionrequest.setRsaLang("sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "ES" : "EN");
		
		
		return model;
	}
	


	public Integer getMaxPasswordRecords() {
		return maxPasswordRecords;
	}

	public void setMaxPasswordRecords(Integer maxPasswordRecords) {
		this.maxPasswordRecords = maxPasswordRecords;
	}

}
