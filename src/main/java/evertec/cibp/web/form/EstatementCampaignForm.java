package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.ebill.Ebpp2BillerProductIdResponse;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.signup.SignupCampaign;
import evertec.cibp.core.models.signup.SignupCampaignEstatement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.EstatementCampaignBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.requests.CreateUserReq;
import evertec.otherservices.requests.RegisterUserAccountReq;
import evertec.otherservices.responses.RegisterUserAccountResp;

public class EstatementCampaignForm extends CancellableFormController {
	private MessageSource messageSource;

	public EstatementCampaignForm() {
		super();
		setCommandClass(EstatementCampaignBean.class);
		setCommandName("estatementcampaign");
		setFormView("estatementcampaign");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		EstatementCampaignBean bean = new EstatementCampaignBean();//command

		if(command instanceof EstatementCampaignBean) {
			bean = (EstatementCampaignBean) command;
		}

		UserSession userSession = new UserSession();//user session
		final String strUserSession = "userSession";//user session string

		if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
			userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
		}
		BankingSession bankingSession = userSession.getBankingSession(); //banking session

		List<SignupCampaign> signupCampaignInfoByProfileId = userSession.getCustomerSignupCampaigns();
		SignupCampaign signupCampaign = null;

		for (Iterator<SignupCampaign> iter = signupCampaignInfoByProfileId.iterator(); iter.hasNext();) {
			SignupCampaign camp = (SignupCampaign) iter.next();
			if (camp.getCampaignType().equals(CampaignType.ESTMT.toString())) {
				signupCampaign = camp;
				break;
			}
		}

		if (signupCampaign != null) {
			SignupCampaign originalCampaign = signupCampaign;
			CustomerServices customerService = userSession.getCustomerService();
			CustomerProfile profile = userSession.getCustomerProfile();

			char signupStatus = bean.getSignupStatus();
			String[] activeAccountsTemp = signupCampaign.getActiveAccounts();
			String[] eligibleAccounts = signupCampaign.getElegibleAccounts();

			// String[] activeAccounts = new String[activeAccountsTemp.length +
			// eligibleAccounts.length];
			ArrayList activeAccounts = new ArrayList();

			// int index = 0;
			for (int index = 0; index < activeAccountsTemp.length; index++)
				activeAccounts.add(activeAccountsTemp[index]);

			signupCampaign.setCampaignResult(signupStatus);

			if (signupStatus == 'Y') {

				// Keep track of the accounts that were successfully added
				List<TVFrontendAccount> addedAccounts = new LinkedList<TVFrontendAccount>();

				// Get all the accounts
				List<TVFrontendAccount> accounts = userSession.getAccounts();

				// Get the eligible accounts to update
				String[] eligible = request.getParameterValues("eligible");

				if (accounts != null && accounts.size() > 0 && eligible != null) {

					for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {

						// Get this account number
						TVFrontendAccount name = (TVFrontendAccount) iterator.next();
						String accountNumber = name.getAccountNumber();

						// Loop through the eligible accounts and set their
						// e-Bill
						if (eligible.length > 0) {

							// The EBPP2 user id is not yet in session; add it
							if (!userSession.isEbpp2UserIdLookedUp()) {
								EbppEntitlement ent = null;

								// If the EBPP2 entitlement is not created,
								// create it
								if (profile.getEntitlementByType(EntitlementType.EBILLS) == null) {
									// Create new ebpp2 user
									CreateUserReq userReq = new CreateUserReq();
									userReq.setEmail(userSession.getCustomerProfile().getEmail());
									userReq.setFirstName(userSession.getCustomerProfile().getFirstName());
									userReq.setLastName(userSession.getCustomerProfile().getLastName());
									userReq.setUserName(userSession.getCustomerProfile().getUsername());
									int userId = userSession.getCustomerService().createEbpp2User(userReq);
									String ebpp2UserId = (userId > 0) ? Integer.toString(userId) : "";
									ent = new EbppEntitlement(profile.getTaxId(), profile.getEnrollmentAccountNumber(),
											ebpp2UserId, EntitlementStatus.ENABLED);
								}

								if (profile.getEntitlementByType(EntitlementType.EBILLS) != null) {
									ent = new EbppEntitlement(profile.getEntitlementByType(EntitlementType.EBILLS));

									// If the username is blank, create one
									if ("".equals(ent.getUsername()) || null == ent.getUsername()) {

										// Create new ebpp2 user
										CreateUserReq userReq = new CreateUserReq();
										userReq.setEmail(userSession.getCustomerProfile().getEmail());
										userReq.setFirstName(userSession.getCustomerProfile().getFirstName());
										userReq.setLastName(userSession.getCustomerProfile().getLastName());
										userReq.setUserName(userSession.getCustomerProfile().getUsername());
										int userId = userSession.getCustomerService().createEbpp2User(userReq);
										String ebpp2UserId = (userId > 0) ? Integer.toString(userId) : "";
										ent.setUsername(ebpp2UserId);

										CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(ent.getCustomerEntitlement()));
										userSession.setEbpp2UserId(ebpp2UserId);
									} else {
										userSession.setEbpp2UserId(ent.getUsername());
									}
									userSession.setEbpp2UserIdLookedUp(true);
								}
							}

							for (int i = 0; i < eligible.length; i++) {

								if (accountNumber.equals(eligible[i])) {

									boolean updated = false;

									// CCA or account?
									if (accountNumber.length() == 9) {

										int billerProductId = 0;
										if (name.getAccountProductId() != null) {
											Ebpp2BillerProductIdResponse ebpp2BillerProductIdResponse = customerService
													.getEbpp2BillerProductIdInformationDepositAccount(name);

											if (ebpp2BillerProductIdResponse != null)
												billerProductId = ebpp2BillerProductIdResponse.getBillerProductId();
										}

										// Do we have a valid biller product ID?
										if (billerProductId > 0) {

											// If this is an estatement,
											// activate it in IDA
											boolean continueProcess = customerService.activateEbpp2AccountInIDA(
													bankingSession, name);

											if (continueProcess) {
												RegisterUserAccountReq req = new RegisterUserAccountReq();
												req.setAccountNumber(name.getAccountNumber());
												req.setBillerId(1);
												req.setBillerProductId(billerProductId);
												req.setLabel(name.getNickname());
												req.setNss(profile.getTaxId());
												req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
												req.setZip("00000");

												// Register the account
												RegisterUserAccountResp resp = customerService
														.registerEbpp2UserAccount(req);

												// SUCCESS
												if (resp.getRegisterResponse() != null
														&& resp.getRegisterResponse().equals("0"))
													updated = true;
											}
										}
									} else {
										List<TVFrontEndPayee> payees = userSession.getPayees(true);
										TVFrontEndPayee payee = Utils.getPayeebyAccountNumber(name.getAccountNumber(),
												payees);
										if (payee != null) {

											RegisterUserAccountReq req = new RegisterUserAccountReq();
											int billerProductId = 0;
											int billerId = 0;

											Ebpp2BillerProductIdResponse ebpp2BillerProductIdResponse = customerService
													.getEbpp2BillerProductIdInformation(Integer.toString(payee
															.getGlobalPayeeId()));

											if (ebpp2BillerProductIdResponse != null) {

												billerProductId = ebpp2BillerProductIdResponse.getBillerProductId();
												billerId = ebpp2BillerProductIdResponse.getBillerId();

												// Do we have a valid biller
												// product ID?
												if (billerProductId > 0 && billerId > 0) {

													req.setAccountNumber(payee.getBillingAccount());
													req.setBillerId(billerId);
													req.setBillerProductId(billerProductId);
													req.setLabel(payee.getNickname());
													req.setNss(profile.getTaxId());
													req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
													req.setZip("00000");

													// Register the account
													RegisterUserAccountResp resp = customerService
															.registerEbpp2UserAccount(req);

													// SUCCESS
													if (resp.getRegisterResponse() != null
															&& resp.getRegisterResponse().equals("0"))
														updated = true;
												}
											}
										}
									}

									if (updated) {

										// activeAccounts[index] =
										// accountNumber;
										// index++;
										boolean found = false;
										for (int e = 0; e < activeAccounts.size(); e++) {
											if (activeAccounts.get(e).toString().equals(accountNumber))
												found = true;
										}
										if (!found) {
											activeAccounts.add(accountNumber);
											addedAccounts.add(name);
											userSession.resetEbpp2Accounts();
										}

										// Add the account
									} else {
										// A false return sends the user to the
										// fail
										// screen
										return new ModelAndView("redirect:estatementcampaignfail");
									}
								}
							}
						}
						String[] strActiveAccts = new String[activeAccounts.size()];
						for (int i = 0; i < activeAccounts.size(); i++)
							strActiveAccts[i] = activeAccounts.get(i).toString();

						// Set the active accounts
						signupCampaign.setActiveAccounts(strActiveAccts);
						signupCampaign.setActiveQty(strActiveAccts.length);

					}
					// Do the update
					signupCampaign.setAddedAccounts(addedAccounts);
					signupCampaign.setCampaignDate(new Date());
					profile.getSignupCampaign().remove(originalCampaign);
					profile.getSignupCampaign().add(signupCampaign);
					customerService.updateSignupCampInfo(signupCampaign);

					return new ModelAndView("redirect:estatementcampaignsuccess");
				}
			} else if (signupStatus == 'N') {

				// Do the update
				signupCampaign.setCampaignDate(new Date());
				profile.getSignupCampaign().remove(originalCampaign);
				profile.getSignupCampaign().add(signupCampaign);
				customerService.updateSignupCampInfo(signupCampaign);
				return new ModelAndView("redirect:portal");
			}
		}
		return new ModelAndView("redirect:portal");
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>(); //model result
		UserSession userSession = new UserSession();//user session
		final String strUserSession = "userSession";//user session string
		CustomerServices customerService = userSession.getCustomerService();

		if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
			userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
		}

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		List<SignupCampaign> signupCampaignInfoByProfileId = userSession.getCustomerSignupCampaigns();
		SignupCampaign signupCampaign = null;

		for (Iterator<SignupCampaign> iter = signupCampaignInfoByProfileId.iterator(); iter.hasNext();) {
			SignupCampaign camp = (SignupCampaign) iter.next();
			if (camp.getCampaignType().equals(CampaignType.ESTMT.toString())) {
				signupCampaign = camp;
				break;
			}
		}

		if (signupCampaign != null) {
			// Get the list of enrolled and enrollable accounts
			String[] activeAccountNumbers = signupCampaign.getActiveAccounts();
			String[] eligibleAccountNumbers = signupCampaign.getElegibleAccounts();

			// Go through the user accounts, find a match, and add
			List<TVFrontendAccount> accounts = userSession.getAccounts();
			List<TVFrontendAccount> activeAccounts = new LinkedList<TVFrontendAccount>();
			List<TVFrontendAccount> eligibleAccounts = new LinkedList<TVFrontendAccount>();

			int realActiveAcctsQty = 0;

			if (accounts != null && accounts.size() > 0) {
				for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
					// Get this account number
					TVFrontendAccount name = (TVFrontendAccount) iterator.next();
					String accountNumber = name.getAccountNumber();

					// Have we added this account? If so, skip
					if (!accountExistsInArray(activeAccounts, accountNumber)
							&& !accountExistsInArray(eligibleAccounts, accountNumber)) {

						// Is this part of the active accounts?
						boolean found = false;
						for (int i = 0; i < activeAccountNumbers.length; i++) {
							if (accountNumber.equals(activeAccountNumbers[i])) {
								activeAccounts.add(name);
								found = true;
								realActiveAcctsQty += 1;
							}
						}

						// Is this part of the eligible accounts?
						if (!found) {
							for (int j = 0; j < eligibleAccountNumbers.length; j++) {
								if (accountNumber.equals(eligibleAccountNumbers[j])) {
									eligibleAccounts.add(name);
								}
							}
						}
					}
				}
			}

			int forestSaved = signupCampaign.getSignupCampEstmt().getForests();
			// int activeQty = signupCampaign.getActiveQty();
			int premiaPoints = 0;
			int carbonSaved = signupCampaign.getSignupCampEstmt().getWarming();
			String currentEmail = userSession.getCustomerProfile().getEmail();

			// Get all the possible estatement values
			List<SignupCampaignEstatement> possibleValues = customerService.getSignupCampEstmtInfo();

			model.put("currentEmail", currentEmail);
			model.put("activeAccounts", activeAccounts);
			model.put("eligibleAccounts", eligibleAccounts);
			model.put("forestSaved", forestSaved);
			model.put("activeQty", realActiveAcctsQty);
			model.put("premiaPoints", premiaPoints);
			model.put("carbonSaved", carbonSaved);
			model.put("possibleValues", possibleValues);
		}
		return model;
	}

	boolean accountExistsInArray(List<TVFrontendAccount> accountList, String accountNumberToFind) {
		boolean found = false;

		for (Iterator<TVFrontendAccount> iterator = accountList.iterator(); iterator.hasNext();) {
			TVFrontendAccount name = (TVFrontendAccount) iterator.next();
			if (name.getAccountNumber().equals(accountNumberToFind)) {
				found = true;
				break;
			}
		}

		return found;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	private boolean isEbppAvailable() {
		boolean ebppStatus = false;

		if (this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
				.equals(EntitlementStatus.ENABLED.toString())) {
			ebppStatus = true;
		} else {
			ebppStatus = false;
		}

		return ebppStatus;
	}

}
