package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import com.evertecinc.www.PremiaPointsActivation.PointsCostCenter;

import bppr.tv.beans.Owner;
import bppr.tv.beans.accounts.Account;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TVFrontendCreditAccount;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.customer.CustomerPayeePreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.EStatementInteraction;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.guided.setup.AddPayeeFailureLog;
import evertec.cibp.core.models.guided.setup.AddPayeeProcess;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.PremiaPointsRequest;
import evertec.cibp.core.services.responses.AddPayeeResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.GuidedSetupAddPayeeBean;
import evertec.cibp.web.form.bean.GuidedSetupAddPayeeListBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.requests.CreateUserReq;
import evertec.otherservices.requests.RegisterUserAccountReq;
import evertec.otherservices.responses.RegisterUserAccountResp;
import evertec.prophit.service.bean.CustomerInquiryBean;
import evertec.prophit.service.bean.EbillIndType;
import evertec.prophit.service.bean.MaintenceEbillBean;
import evertec.prophit.service.bean.MaintenceEbillResponse;

/**
 * Clase evertec.cibp.web.form.GuidedSetupAddEbills Sistema: Mi Banco
 * Descripcion: Clase que controla las acciones de la capa de presentacion para
 * el proceso de comercios en la campana guided setup
 * 
 * @author Ing. Blas Alonso GR Creada el Abril 15, 2011 cibp-2218
 */

public class GuidedSetupAddPayee extends AbstractWizardFormController implements MessageSourceAware {

	private static final Logger logger = Logger.getLogger(GuidedSetupAddPayee.class);
	private MessageSource messageSource;

	public GuidedSetupAddPayee() {
		super();
		setCommandClass(GuidedSetupAddPayeeListBean.class);
		setCommandName("gsAddPayee");
		setPages(new String[] { "gsAddPayee" });
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {
		GuidedSetupAddPayeeListBean gsAddPayeeList = (GuidedSetupAddPayeeListBean) command;
		boolean registrar = false;
		for (int i = 0; i < gsAddPayeeList.getListAddPayeeBeans().size(); i++) {
			if (gsAddPayeeList.getListAddPayeeBeans().get(i).isChecked()) {
				registrar = true;
				break;
			}
		}
		if (!registrar) {
			errors.reject("guidedSetup.addpayee.agregar");
		}
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		model.put("realizoAccionPayments", true);
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		GuidedSetupAddPayeeListBean gsAddPayeeList = (GuidedSetupAddPayeeListBean) command;
		List<GuidedSetupAddPayeeBean> gsAddPayeeListErrorsAndUncheck = new ArrayList<GuidedSetupAddPayeeBean>();
		boolean ebppStatus = userSession.getCustomerService().getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED
				&& this.getEBPPMessageSourceStatus();
		request.removeAttribute("erroresAddpayee");

		String nick = "";
		String billingAct = "";
		String subtype = "";
		EbppEntitlement ent = null;

		CustomerProfile profile = userSession.getCustomerProfile();
		boolean errores = false;
		for (Iterator<GuidedSetupAddPayeeBean> iter = gsAddPayeeList.getListAddPayeeBeans().iterator(); iter.hasNext();) {
			GuidedSetupAddPayeeBean name = (GuidedSetupAddPayeeBean) iter.next();
			if (profile.getEntitlementByType(EntitlementType.EBILLS) == null) {
				// Create new ebpp2 user
				CreateUserReq userReq = new CreateUserReq();
				userReq.setEmail(userSession.getCustomerProfile().getEmail());
				userReq.setFirstName(userSession.getCustomerProfile().getFirstName());
				userReq.setLastName(userSession.getCustomerProfile().getLastName());
				userReq.setUserName(userSession.getCustomerProfile().getUsername());
				int userId = userSession.getCustomerService().createEbpp2User(userReq);
				String ebpp2UserId = (userId > 0) ? Integer.toString(userId) : "";
				ent = new EbppEntitlement(profile.getTaxId(), profile.getEnrollmentAccountNumber(), ebpp2UserId,
						EntitlementStatus.ENABLED);
			} else
				ent = new EbppEntitlement(profile.getEntitlementByType(EntitlementType.EBILLS));

			if (name.isChecked()) {
				if (name.getTvfeAccount() != null) {
					nick = name.getTvfeAccount().getNickname();
					billingAct = name.getTvfeAccount().getAccountNumber();
					subtype = name.getSubType();
				}
				AddPayeeResponse res = new AddPayeeResponse();
				boolean isHighRiskPayee = false;
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1701) && name.getGlobalPayee() != null) {

					PaymentsFacade.getInstance().getGlobalPayeePreference(name.getGlobalPayee(), userSession);

					if (name.getGlobalPayee().getPreference() != null)
						isHighRiskPayee = name.getGlobalPayee().getPreference().getHighRisk();
				}
				try {
					res = userSession.getCustomerService().addCustomerPayeeWithoutEbillWithErrorDescription(
							userSession.getCustomerProfile(), userSession.getBankingSession(), name.getGlobalPayee(),
							billingAct, nick, Utils.generateCustomerInteractionEvent(request), null, null, null, null,
							false, isHighRiskPayee);

				} catch (Exception e) {
					res.setErrorCode(1);
				}
				// aca se agrega el comercio

				if (res.getErrorCode() != 0) {
					AddPayeeFailureLog log = new AddPayeeFailureLog();
					log.setAccount(billingAct);
					log.setCan(userSession.getCustomerProfile().getEnrollmentAccountNumber());
					String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request)
							.toLowerCase());
					if (er == null || er.trim().length() == 0)
						log.setError(res.getErrorString());
					else
						log.setError(er);
					Calendar cal = Calendar.getInstance();
					log.setFecha(cal.getTime());
					log.setName(name.getGlobalPayee().getShortName());
					log.setSsn(userSession.getCustomerProfile().getTaxId());
					userSession.getCustomerService().saveAddPayeeFailureLog(log);
					name.setSuccess(-1);
					gsAddPayeeListErrorsAndUncheck.add(name);
					errores = true;
				} else {
					model.put("successAdded", true);
					userSession.resetPayees();
					// aca se agrega el ebill en la tabla de ebills pendientes
					if (name.isHasEbiil() && ebppStatus) {
						if (subtype.equalsIgnoreCase("CCA")) {
							if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
								try {
									CustomerInquiryBean responseTsys = userSession.getCustomerService().prophItService()
											.makeCustomerInquiry(name.getTvfeAccount().getAccountNumber());
									if (responseTsys.getEbillInd().equalsIgnoreCase(EbillIndType.ELIGIBLE.toString())) {
										MaintenceEbillBean maintenceEBillBean = new MaintenceEbillBean();
										maintenceEBillBean.setAcctNum(name.getTvfeAccount().getAccountNumber());
										maintenceEBillBean.setCaseNumber(responseTsys.getCaseNumber());
										maintenceEBillBean.setChannelId(responseTsys.getChannelId());
										maintenceEBillBean.setEbillInd(EbillIndType.ENROLL.toString());
										maintenceEBillBean.setImpersonateId(responseTsys.getImpersonateId());
										maintenceEBillBean.setKeepCaseOpen(responseTsys.getKeepCaseOpen());
										maintenceEBillBean.setNewContact(responseTsys.getNewContact());
										maintenceEBillBean.setOrgId(responseTsys.getOrgId());
										maintenceEBillBean.setOrgLevel(responseTsys.getOrgLevel());
										MaintenceEbillResponse responseMaintananceTsys = userSession
												.getCustomerService().prophItService()
												.maintenceEBill(maintenceEBillBean);
										if (responseMaintananceTsys != null
												&& responseMaintananceTsys.getMessage().indexOf("Successfull") != -1) {
											userSession.getCustomerService().sendEbillsCCAEmailActivationSuccess(
													userSession.getCustomerProfile(), PreferredLanguage.SPANISH,
													name.getTvfeAccount().getNickname());
											name.setSuccess(1);
											// Premia Report
											PremiaPointsRequest premiaRequest = new PremiaPointsRequest(null,
													String.valueOf(profile.getPermId()),
													name.getTvfeAccount().getAccountNumber(), profile.getTaxId(),
													"Anadir Factura Electronica", "098", PointsCostCenter.INSFE,
													profile.getUsername(), "MiBanco", true);
											userSession.getCustomerService().premiaPointsActivation(premiaRequest);
											CustomerInteractionEvent customerInteractionEvent = Utils
													.generateCustomerInteractionEvent(request);
											customerInteractionEvent.setCustomStringData(EStatementInteraction.STATUS,
													"Account: " + name.getTvfeAccount().getAccountNumber()
															+ " activate.");
											userSession.getCustomerService().logUpdateStatementStatus(
													customerInteractionEvent, InteractionSeverity.INFO);

										} else {
											model.put("erroresAddpayee", gsAddPayeeList.getListAddPayeeBeans());
											gsAddPayeeListErrorsAndUncheck.add(name);
											name.setSuccess(0);
										}

									} else if (responseTsys.getEbillInd()
											.equalsIgnoreCase(EbillIndType.ACTIVE.toString())) {
										name.setSuccess(1);
									} else {
										model.put("erroresAddpayee", gsAddPayeeList.getListAddPayeeBeans());
										gsAddPayeeListErrorsAndUncheck.add(name);
										name.setSuccess(0);
									}
								} catch (Exception e) {
									// error with catch
									logger.info("Error getting information from TSYS for account "
											+ name.getTvfeAccount().getAccountNumber());
									model.put("erroresAddpayee", gsAddPayeeList.getListAddPayeeBeans());
									gsAddPayeeListErrorsAndUncheck.add(name);
									name.setSuccess(0);
								}

							} else {
								// The EBPP2 user id is not yet in session; add it
								if (!userSession.isEbpp2UserIdLookedUp()) {
									if (profile.getEntitlementByType(EntitlementType.EBILLS) != null) {
										ent = new EbppEntitlement(profile.getEntitlementByType(EntitlementType.EBILLS));

										// If the username is blank, we need to look
										// in the current EBPP accounts ONLY
										if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
											userSession.setEbpp2UserId("");
										} else {
											userSession.setEbpp2UserId(ent.getUsername());
										}
										userSession.setEbpp2UserIdLookedUp(true);
									}
								}

								// The user has an EBPP2 ID
								if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

									RegisterUserAccountReq req = new RegisterUserAccountReq();
									int billerProductId = 0;

									TVFrontendAccount tvp = Utils.getAccountByAccountNumber(billingAct,
											userSession.getAccounts());
									AbstractAccount accountDetails = userSession.getCustomerService()
											.getAccountDetails(profile, userSession.getBankingSession(), tvp, true);
									TVFrontendCreditAccount frontendCreditAccount = (TVFrontendCreditAccount) accountDetails;
									if (frontendCreditAccount.getProductIdBCO() != null
											&& !frontendCreditAccount.getProductIdBCO().equals(""))
										billerProductId = Integer.parseInt(frontendCreditAccount.getProductIdBCO());
									else {
										// Corporate cards start with 454903
										if (billingAct.startsWith("454903"))
											billerProductId = 59;
										// Business cards start with 454925
										else if (billingAct.startsWith("454925"))
											billerProductId = 60;
										// VISA Advantage Business = 61
										else if (tvp.getProductDefinition().getProductId().equals("VBABS"))
											billerProductId = 61;
										// All others are individual cards
										else
											billerProductId = 55;
									}

									// Do we have a valid biller product ID?
									if (billerProductId > 0) {

										req.setAccountNumber(billingAct);
										req.setBillerId(1);
										req.setBillerProductId(billerProductId);
										req.setLabel(nick);
										req.setNss(profile.getTaxId());
										req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
										req.setZip("00000");

										// Register the account
										RegisterUserAccountResp resp = userSession.getCustomerService()
												.registerEbpp2UserAccount(req);

										if (resp == null || resp.getRegisterResponse() == null
												|| !resp.getRegisterResponse().equals("0")) {
											model.put("erroresAddpayee", gsAddPayeeList.getListAddPayeeBeans());
											gsAddPayeeListErrorsAndUncheck.add(name);
											name.setSuccess(0);
										} else
											name.setSuccess(1);
									} else {
										model.put("erroresAddpayee", gsAddPayeeList.getListAddPayeeBeans());
										gsAddPayeeListErrorsAndUncheck.add(name);
										name.setSuccess(0);
									}
								}
							}
						}
					}
				}

			} else {
				gsAddPayeeListErrorsAndUncheck.add(name);
				name.setSuccess(0);
			}
		}
		if (errores) {
			gsAddPayeeList.setListAddPayeeBeans(gsAddPayeeListErrorsAndUncheck);
			model.put("erroresAddpayee", gsAddPayeeList.getListAddPayeeBeans());
		}

		userSession.getGuidedSetupSession().setResubmitParent(false);
		return new ModelAndView("forward:gsAddPayee", model);
	}

	@SuppressWarnings("unchecked")
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		GuidedSetupAddPayeeListBean gsAddPayeeList = (GuidedSetupAddPayeeListBean) command;
		if (userSession.getGuidedSetupSession().isResubmitParent()) {
			model.put("submitgs", true);
			userSession.getGuidedSetupSession().setResubmitParent(false);
			return model;
		} else {
			if (request.getAttribute("erroresAddpayee") == null) {
				ProcesoCreaComercios(userSession, gsAddPayeeList);
			} else {
				gsAddPayeeList.setListAddPayeeBeans((List<GuidedSetupAddPayeeBean>) request
						.getAttribute("erroresAddpayee"));
			}
		}
		boolean ebppStatus = userSession.getCustomerService().getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED
				&& this.getEBPPMessageSourceStatus();
		if (ebppStatus)
			model.put("ebpp", 0);// pagina principal
		else
			model.put("ebpp", 1);// pagina principal

		// if (gsAddPayeeList != null && gsAddPayeeList.getListAddPayeeBeans()
		// != null && gsAddPayeeList.getListAddPayeeBeans().size() > 0)
		model.put("page", 0);// pagina principal
		model.put("bto_hidden", false);
		return model;
	}

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

	/** @return regresa el messageSource */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource
	 *            el messageSource a establecer
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public GlobalPayee validoAddPayee(List<AddPayeeProcess> list, TVFrontendAccount name) {
		for (Iterator<AddPayeeProcess> iter = list.iterator(); iter.hasNext();) {
			AddPayeeProcess val = (AddPayeeProcess) iter.next();
			if (val.getBank().equalsIgnoreCase(name.getBankId())
					&& val.getId().getProduct_code().equalsIgnoreCase(name.getProductDefinition().getProductId())) {
				return val.getId().getGlobalPayee();
			}

		}
		return null;
	}

	public void ProcesoCreaComercios(UserSession userSession, GuidedSetupAddPayeeListBean beanFinal) {
		try {
			CustomerServices customerService = userSession.getCustomerService();
			List<AddPayeeProcess> list = customerService.getAllAddPayeeProcess();
			Map<String, CustomerPayeePreferences> loadPayeePreferences = userSession.getCustomerService()
					.getPayeePreferences(userSession.getCustomerProfile());
			List<GuidedSetupAddPayeeBean> listfinal = new java.util.LinkedList<GuidedSetupAddPayeeBean>();

			// PARA AGREGAR LOS COMERCIOS INTERNOS DEPENDIENDO DEL PRODUCTO QUE
			// BAJE DE LA BOVEDA
			boolean relacion = false;
			GlobalPayee g = null;

			List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>();
			// mpicado, esto lo hago para crear una lista nueva que apunte a
			// distintas posiciones de memoria en los objetos TVFrontendAccount

			for (int z = 0; z < userSession.getAccounts().size(); z++) {
				accounts.add(userSession.getAccounts().get(z).clone());

			}

			for (Iterator<TVFrontendAccount> iter = accounts.iterator(); iter.hasNext();) {
				TVFrontendAccount name = (TVFrontendAccount) iter.next();
				Account a = name.getTVAccount();

				Owner owners[] = a.getOwners();
				relacion = false;
				g = null;

				if (owners != null && owners.length > 0) {
					for (int i = 0; i < owners.length && relacion == false; i++) {
						Owner owner2 = owners[i];
						if (owner2 != null
								&& owner2.getOwnershipType() != null
								&& (owner2.getOwnershipType().equals("PRIMARY") || owner2.getOwnershipType().equals(
										"SECONDARY")))
							relacion = true;
					}
				}

				if (relacion)
					g = validoAddPayee(list, name);

				if (g != null) {
					CustomerPayeePreferences customerPayeePreferences = loadPayeePreferences.get(name
							.getAccountNumber() + "|" + g.getId().toString());
					if (customerPayeePreferences == null && isValidByAccounNumberSuffix(name)) {
						GuidedSetupAddPayeeBean bean = new GuidedSetupAddPayeeBean();
						bean.setTvfeAccount(name);
						bean.setGlobalPayee(g);
						bean.setChecked(false);
						bean.setHasEbiil(false);
						bean.setSubType(name.getSubtype());
						bean.setSuccess(0);
						listfinal.add(bean);
					}
				}
			}

			beanFinal.setListAddPayeeBeans(listfinal);
		} catch (Exception e) {
			logger.error("Error al correr el proceso de comercios de guided setup: " + e.getMessage());
		}
	}

	/*
	 * CIBP-1680 bgarcia Obtiene el texto de error actualizado de los archivo
	 */
	private String getTelepagoErrorDescriptor(String codigo, String len) {
		String status = "";
		try {
			if (!"".equals(len))
				len = "es";
			status = this.messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
		} catch (Exception e) {
			status = "";
		}

		return status;
	}

	private boolean isValidByAccounNumberSuffix(TVFrontendAccount account) {
		// All CCA's
		if (account.getSubtype().equalsIgnoreCase("CCA")) {
			return true;
		}
		// IDA's with active RES
		if (account.getSubtype().equalsIgnoreCase("IDA") && account.getAccountNumberSuffix() != null
				&& account.getAccountNumberSuffix().equals("RES")) {
			return true;
		}
		// LOANS
		if (!account.getSubtype().equalsIgnoreCase("IDA")
				&& (account.getAccountNumberSuffix() == null || account.getAccountNumberSuffix().equals(""))) {
			return true;
		}
		return false;
	}

}
