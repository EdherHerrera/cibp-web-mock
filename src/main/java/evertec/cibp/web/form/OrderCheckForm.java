package evertec.cibp.web.form;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.AccountsFacade;
import evertec.cibp.web.form.bean.AccountServiceBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class OrderCheckForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public OrderCheckForm() {
		super();
		setCommandClass(AccountServiceBean.class);
		setCommandName("accountservices");
		setFormView("ordercheck");
		setBindOnNewForm(true);
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		AccountServiceBean searchBean = (AccountServiceBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();

		String url = null;
		TVFrontendAccount account = Utils.getAccountById(searchBean.getAccount(), userSession.getAccounts());

		// All e-accounts (and GoFree accounts) should not be able to order
		// checks
		if (account != null && account.getProductDefinition() != null
				&& !GenericValidator.isBlankOrNull(account.getProductDefinition().getProductId())
				&& account.getProductDefinition().getProductId().equals("081")) {

			model.put("newaccount", true);

		} else {
			if (account != null && account.getProductDefinition() != null
					&& account.getProductDefinition().getProductId() != null
					&& !account.getProductDefinition().getProductId().equals("")) {
				try {
					
					/**
					 * MBNA-1171
					 * Method getHarlandUrlByAccount : CIBP-WEB microservice integration
					 * 
					 */
					url = AccountsFacade.getInstance().getHarlandUrlByAccount(customer, account, "https://www.bancopopular.com/cibp-web/actions/accountservices", userSession);
				
				} catch (Exception e) {
					model.put("urlerror", true);
					userSession.setStatusmessage("Unable to get the Order Check Url.");
				}

			}
			if (url != null)
				model.put("url", url);
		}
		return showForm(request, error, "ordercheck", model);

	}

	protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		AccountMaskUtil mask = new AccountMaskUtil();

		/****** Reference Data for Order check Request service *******/
		Map<String, String> checkOrderAccounts = new LinkedHashMap<String, String>();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			checkOrderAccounts.put("none", "Select an account");
		else
			checkOrderAccounts.put("none", "Selecciona una cuenta");
		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount name = (TVFrontendAccount) iterator.next();
			if (name.getFeatures().isCheckOrder() && !name.getHide()) {
				checkOrderAccounts
						.put(String.valueOf(name.getFrontEndId()),
								name.getNickname()
										+ " "
										+ mask.mask(name.getAccountNumber())
										+ ((name.getAccountNumberSuffix() != null && !name.getAccountNumberSuffix()
												.equals("")) ? " " + name.getAccountNumberSuffix() : ""));
			}
		}

		if (checkOrderAccounts != null && checkOrderAccounts.size() > 0)
			model.put("checkOrderAccounts", checkOrderAccounts);
		else
			model.put("nocheckaccount", true);

		model.put("checksNotification", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE14));
		return model;

	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		AccountServiceBean searchBean = (AccountServiceBean) command;

		if (searchBean.getAccount() == null || searchBean.getAccount().equals("none"))
			errors.rejectValue("account", "ordercheck.account.invalid", "Invalid Account Selection");
	}

}
