package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.InternationalCharacterUtils;
import evertec.cibp.web.form.bean.PIFSmallDepositsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

public class PIFSmallDepositsForm extends SimpleFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public PIFSmallDepositsForm() {
		super();
		setCommandClass(PIFSmallDepositsBean.class);
		setCommandName("pifsmalldeposits");
		setFormView("pifsmalldeposits");
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();

		PIFSmallDepositsBean bean = (PIFSmallDepositsBean) command;
		String deposit1 = bean.getDeposit1();
		String deposit2 = bean.getDeposit2();

		if (deposit1.length() == 4)
			deposit1 = deposit1.substring(1);

		if (deposit2.length() == 4)
			deposit2 = deposit2.substring(1);

		// Verify the two small deposits
		boolean validSmallDeposits = customerService.verifyPIFSmallDeposits(bean.getAccountId(), deposit1, deposit2);

		if (validSmallDeposits) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession
						.setStatusmessage("Your other Bank account was confirmed. You can start transferring funds.");
			else
				userSession
						.setStatusmessage("Su cuenta de otro Banco fue confirmada. Ahora puede comenzar a transferir fondos.");

			model.put("completeprocess", true);
			return showForm(request, response, errors, model);
		} else {

			// First, verify if the max wrong limit (overall) was reached
			if (customerService.pifSmallDepositsIncorrectLimitReached(bean.getAccountId())) {

				boolean updated = customerService.addBlock4PIFAccount(bean.getAccountId());

				if (updated) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Your other Bank account was blocked.");
					else
						userSession.setStatusmessage("Su cuenta de otro Banco fue bloqueada.");

					// Send the error email
					PIFExternalAccount account = customerService.getPifExternalAccount(bean.getAccountId());
					String bankAccount = "";
					if (account.getAccountNumber().length() > 4)
						bankAccount = "x"
								+ account.getAccountNumber().substring(account.getAccountNumber().length() - 4);
					else
						bankAccount = "x" + account.getAccountNumber();
					customerService.sendPIFBlock4Confirmation(profile, bankAccount, account.getBankName(),
							userSession.getPreferredLanguage());

					model.put("completeprocess", true);
					return showForm(request, response, errors, model);
				} else {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("There was a problem with your deposits. Please try again.");
					else
						userSession.setStatusmessage("Hubo un problema con sus depositos. Favor intente de nuevo.");

					model.put("completeprocess", true);
					return showForm(request, response, errors, model);
				}
			} else {

				int failedAttempts = customerService.getPIFFailCount(bean.getAccountId());
				if (failedAttempts >= 3) {

					boolean updated = customerService.addBlock3PIFAccount(bean.getAccountId());

					if (updated) {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession.setStatusmessage("Your other Bank account was blocked.");
						else
							userSession.setStatusmessage("Su cuenta de otro Banco fue bloqueada.");

						// Send the error email
						PIFExternalAccount account = customerService.getPifExternalAccount(bean.getAccountId());
						String bankAccount = "";
						if (account.getAccountNumber().length() > 4)
							bankAccount = "x"
									+ account.getAccountNumber().substring(account.getAccountNumber().length() - 4);
						else
							bankAccount = "x" + account.getAccountNumber();
						customerService.sendVerificationErrorConfirmation(profile, bankAccount, account.getBankName(),
								userSession.getPreferredLanguage());

						model.put("completeprocess", true);
						return showForm(request, response, errors, model);
					} else {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							userSession.setStatusmessage("There was a problem with your deposits. Please try again.");
						else
							userSession.setStatusmessage("Hubo un problema con sus depositos. Favor intente de nuevo.");

						model.put("completeprocess", true);
						return showForm(request, response, errors, model);
					}
				} else {

					model.put("accountId", bean.getAccountId());
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						userSession.setStatusmessage("Incorrect deposit amount. Please try again");
						errors.rejectValue("deposit2", "", "Incorrect deposit amount.");
					} else {
						userSession
								.setStatusmessage("Los dep&oacute;sitos est&aacute;n incorrectos. Intente de nuevo.");
						errors.rejectValue("deposit2", "", "Los dep&oacute;sitos est&aacute;n incorrectos.");
					}
				}
			}
		}

		return showForm(request, response, errors, model);

	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		PIFSmallDepositsBean bean = (PIFSmallDepositsBean) command;

		String id = (String) request.getParameter("id");
		id = InternationalCharacterUtils.sanatizeInputs(id, false);
		if (id.equals(""))
			id = bean.getAccountId();

		model.put("accountId", id);
		return model;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		boolean err = false;
		PIFSmallDepositsBean bean = (PIFSmallDepositsBean) command;
		String deposit1 = bean.getDeposit1();
		String deposit2 = bean.getDeposit2();
		String confirmation1 = bean.getConfirmation1();
		String confirmation2 = bean.getConfirmation2();

		if (GenericValidator.isBlankOrNull(deposit1)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("deposit1", "", "Required");
				err = true;
			} else {
				errors.rejectValue("deposit1", "", "Requerido");
				err = true;
			}
		}

		if (GenericValidator.isBlankOrNull(deposit2)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("deposit2", "", "Required");
				err = true;
			} else {
				errors.rejectValue("deposit2", "", "Requerido");
				err = true;
			}
		}

		if (GenericValidator.isBlankOrNull(confirmation1)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("confirmation1", "", "Required");
				err = true;
			} else {
				errors.rejectValue("confirmation1", "", "Requerido");
				err = true;
			}
		}

		if (GenericValidator.isBlankOrNull(confirmation2)) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				errors.rejectValue("confirmation2", "", "Required");
				err = true;
			} else {
				errors.rejectValue("confirmation2", "", "Requerido");
				err = true;
			}
		}

		if (!err) {

			// Format has to be in cents
			if (deposit1.length() == 4) {
				if (!deposit1.startsWith("0.") || !GenericValidator.isInt(deposit1.substring(2))) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("deposit1", "", "Use format .xx or 0.xx");
						err = true;
					} else {
						errors.rejectValue("deposit1", "", "Use el formato .xx o 0.xx");
						err = true;
					}
				}
			} else if (deposit1.length() == 3) {
				if (deposit1.charAt(0) != '.' || !GenericValidator.isInt(deposit1.substring(1))) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("deposit1", "", "Use format .xx or 0.xx");
						err = true;
					} else {
						errors.rejectValue("deposit1", "", "Use el formato .xx o 0.xx");
						err = true;
					}
				}
			} else {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					errors.rejectValue("deposit1", "", "Use format .xx or 0.xx");
					err = true;
				} else {
					errors.rejectValue("deposit1", "", "Use el formato .xx o 0.xx");
					err = true;
				}
			}

			if (deposit2.length() == 4) {
				if (!deposit2.startsWith("0.") || !GenericValidator.isInt(deposit2.substring(2))) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("deposit2", "", "Use format .xx or 0.xx");
						err = true;
					} else {
						errors.rejectValue("deposit2", "", "Use el formato .xx o 0.xx");
						err = true;
					}
				}
			} else if (deposit2.length() == 3) {
				if (deposit2.charAt(0) != '.' || !GenericValidator.isInt(deposit2.substring(1))) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("deposit2", "", "Use format .xx or 0.xx");
						err = true;
					} else {
						errors.rejectValue("deposit2", "", "Use el formato .xx o 0.xx");
						err = true;
					}
				}
			} else {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					errors.rejectValue("deposit2", "", "Use format .xx or 0.xx");
					err = true;
				} else {
					errors.rejectValue("deposit2", "", "Use el formato .xx o 0.xx");
					err = true;
				}
			}

			if (confirmation1.length() == 4) {
				if (!confirmation1.startsWith("0.") || !GenericValidator.isInt(confirmation1.substring(2))) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("confirmation1", "", "Use format .xx or 0.xx");
						err = true;
					} else {
						errors.rejectValue("confirmation1", "", "Use el formato .xx o 0.xx");
						err = true;
					}
				}
			} else if (confirmation1.length() == 3) {
				if (confirmation1.charAt(0) != '.' || !GenericValidator.isInt(confirmation1.substring(1))) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("confirmation1", "", "Use format .xx or 0.xx");
						err = true;
					} else {
						errors.rejectValue("confirmation1", "", "Use el formato .xx o 0.xx");
						err = true;
					}
				}
			} else {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					errors.rejectValue("confirmation1", "", "Use format .xx or 0.xx");
					err = true;
				} else {
					errors.rejectValue("confirmation1", "", "Use el formato .xx o 0.xx");
					err = true;
				}
			}

			if (confirmation2.length() == 4) {
				if (!confirmation2.startsWith("0.") || !GenericValidator.isInt(confirmation2.substring(2))) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("confirmation2", "", "Use format .xx or 0.xx");
						err = true;
					} else {
						errors.rejectValue("confirmation2", "", "Use el formato .xx o 0.xx");
						err = true;
					}
				}
			} else if (confirmation2.length() == 3) {
				if (confirmation2.charAt(0) != '.' || !GenericValidator.isInt(confirmation2.substring(1))) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						errors.rejectValue("confirmation2", "", "Use format .xx or 0.xx");
						err = true;
					} else {
						errors.rejectValue("confirmation2", "", "Use el formato .xx o 0.xx");
						err = true;
					}
				}
			} else {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					errors.rejectValue("confirmation2", "", "Use format .xx or 0.xx");
					err = true;
				} else {
					errors.rejectValue("confirmation2", "", "Use el formato .xx o 0.xx");
					err = true;
				}
			}
		}

		if (!err) {

			// Values have to pair up
			if (!deposit1.equals(confirmation1)) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					errors.rejectValue("confirmation1", "", "The values do not match");
					err = true;
				} else {
					errors.rejectValue("confirmation1", "", "Los valores no parean");
					err = true;
				}
			}

			if (!deposit2.equals(confirmation2)) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					errors.rejectValue("confirmation2", "", "The values do not match");
					err = true;
				} else {
					errors.rejectValue("confirmation2", "", "Los valores no parean");
					err = true;
				}
			}
		}
	}
}
