package evertec.cibp.web.form;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.form.bean.ODLimitBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

public class ODLimitForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public ODLimitForm() {
		super();
		setCommandClass(ODLimitBean.class);
		setCommandName("odlimit");
		setFormView("odlimit");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		// CIBP-1767
		String showInsideIframe = request.getParameter("showInsideIframe");
		model.put("showInsideIframe", showInsideIframe);
		ODLimitBean bean = (ODLimitBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		// CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		// List<TVFrontendAccount> accounts = userSession.getAccounts();
		List<TVFrontendAccount> successAddedList = new LinkedList<TVFrontendAccount>();
		List<TVFrontendAccount> successWithdrawnList = new LinkedList<TVFrontendAccount>();
		List<TVFrontendAccount> failedList = new LinkedList<TVFrontendAccount>();
		String detail;

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99
		
		boolean success = false;
		
		if (request.getParameter("detail") != null) {
			detail = request.getParameter("detail");
			model.put("detail", detail);
		}
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			model.put("Lang", "sp");
		else
			model.put("Lang", "en");
		for (Iterator<TVFrontendAccount> iter = bean.getAccountList().iterator(); iter.hasNext();) {
			TVFrontendAccount element = (TVFrontendAccount) iter.next();

			// String productId=element.getAccountProductId();
			String odlimit = element.getOdLimit();
			try {
				if (odlimit.equalsIgnoreCase("A")) {
					success = customerService.completePendingRegE(element, odlimit);
					if (success) {
						successAddedList.add(element);
					} else {
						failedList.add(element);
					}
				} else {
					success = customerService.completePendingRegE(element, odlimit);
					if (success) {
						successWithdrawnList.add(element);
					} else {
						failedList.add(element);
					}
				}
			} catch (Exception e) {
				logger.error("Error: Exception is " + e.getMessage());
				logger.error("Error: while sending overdraft limit value " + element.getOdLimit() + " for account "
						+ element.getAccountNumber());
			}
		}
		
		//Se envia email de confirmacion
		if(successAddedList.size() > 0) {
			customerService.sendOverdraftAcceptanceEmail(userSession.getCustomerProfile(),
					userSession.getPreferredLanguage(),successAddedList);
		}
		
		model.put("successAddedList", successAddedList);
		model.put("successWithdrawnList", successWithdrawnList);
		if (failedList.size() > 0)
			model.put("failedList", failedList);
		model.put("complete", true);

		return showForm(request, error, "odlimit", model);
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		ODLimitBean bean = new ODLimitBean();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		List<TVFrontendAccount> accountList = new LinkedList<TVFrontendAccount>();

		for (Iterator<TVFrontendAccount> iter = accounts.iterator(); iter.hasNext();) {
			TVFrontendAccount element = (TVFrontendAccount) iter.next();
			String suffix;
			if (element.getAccountNumberSuffix() != null)
				suffix = element.getAccountNumberSuffix();
			else
				suffix = "";
			if ((!suffix.equalsIgnoreCase("")) && suffix.equalsIgnoreCase("RES") || suffix.equalsIgnoreCase("SAV"))
				continue;
			String productId = element.getAccountProductId();
			if (productId.equalsIgnoreCase("009")// Acceso Popular
					|| productId.equalsIgnoreCase("015")// Cuenta Popular
					|| productId.equalsIgnoreCase("017")// Chequera Popular
					|| productId.equalsIgnoreCase("020")// Avance 50
					|| productId.equalsIgnoreCase("023")// Multicuenta Staff
					|| productId.equalsIgnoreCase("024")// Popular Plus
					|| productId.equalsIgnoreCase("025")// Multicuenta Popular
					|| productId.equalsIgnoreCase("026")// Ideal
					|| productId.equalsIgnoreCase("028")// MaxiPremium
					|| productId.equalsIgnoreCase("029")// Private Management
														// Account
					|| productId.equalsIgnoreCase("049")// Ahorro a Toda Hora
														// Staff
					|| productId.equalsIgnoreCase("051")// Ahorro 24
					|| productId.equalsIgnoreCase("052")// Ahorro a Toda Hora
					|| productId.equalsIgnoreCase("069")// Multicuenta
														// Subsidiary Staff
					|| productId.equalsIgnoreCase("072")// Ahorro a Toda Hora
														// Subsidiary Staff
					|| productId.equalsIgnoreCase("081")// e-account
					|| productId.equalsIgnoreCase("087"))// CIMA
			{
				accountList.add(element);
			}
		}
		bean.setAccountList(accountList);
		return bean;
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		// CIBP-1767
		String showInsideIframe = request.getParameter("showInsideIframe");
		model.put("showInsideIframe", showInsideIframe);
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<TVFrontendAccount> accounts = userSession.getAccounts();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		// ODLimitBean bean = (ODLimitBean) command;
		List<TVFrontendAccount> accountList = new LinkedList<TVFrontendAccount>();
		if ("sp".equalsIgnoreCase(Utils.getRSALanguage(request)))
			model.put("Lang", "sp");
		else
			model.put("Lang", "en");

		String detail;
		if (request.getParameter("detail") != null) {
			detail = request.getParameter("detail");
			model.put("detail", detail);
		}

		for (Iterator<TVFrontendAccount> iter = accounts.iterator(); iter.hasNext();) {
			TVFrontendAccount element = (TVFrontendAccount) iter.next();
			String suffix;
			if (element.getAccountNumberSuffix() != null)
				suffix = element.getAccountNumberSuffix();
			else
				suffix = "";
			if ((!suffix.equalsIgnoreCase("")) && suffix.equalsIgnoreCase("RES") || suffix.equalsIgnoreCase("SAV"))
				continue;
			String productId = element.getAccountProductId();
			if (productId.equalsIgnoreCase("009")// Acceso Popular
					|| productId.equalsIgnoreCase("020")// Avance 50
					|| productId.equalsIgnoreCase("023")// Multicuenta Staff
					|| productId.equalsIgnoreCase("024")// Popular Plus
					|| productId.equalsIgnoreCase("025")// Multicuenta Popular
					|| productId.equalsIgnoreCase("026")// Ideal
					|| productId.equalsIgnoreCase("028")// MaxiPremium
					|| productId.equalsIgnoreCase("029")// Private Management
														// Account
					|| productId.equalsIgnoreCase("049")// Ahorro a Toda Hora
														// Staff
					|| productId.equalsIgnoreCase("051")// Ahorro 24
					|| productId.equalsIgnoreCase("052")// Ahorro a Toda Hora
					|| productId.equalsIgnoreCase("069")// Multicuenta
														// Subsidiary Staff
					|| productId.equalsIgnoreCase("072")// Ahorro a Toda Hora
														// Subsidiary Staff
					|| productId.equalsIgnoreCase("081")// e-account
					|| productId.equalsIgnoreCase("087"))// CIMA
			{
				accountList.add(element);
			}
		}
		model.put("accountList", accountList);
		return model;
	}
}