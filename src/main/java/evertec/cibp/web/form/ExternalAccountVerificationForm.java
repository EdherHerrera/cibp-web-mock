package evertec.cibp.web.form;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.transfer.CustomerExternalTransferSource;
import evertec.cibp.core.models.transfer.ExternalTransferSourceStatus;
import evertec.cibp.web.form.bean.ExternalAccountVerificationBean;
import evertec.cibp.web.form.validator.ExternalAccountVerificationValidator;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class ExternalAccountVerificationForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public ExternalAccountVerificationForm() {
		super();
		setCommandClass(ExternalAccountVerificationBean.class);
		setCommandName("externalAccount");
		setFormView("externalAccountVerification");
		setValidator(new ExternalAccountVerificationValidator());

	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		ExternalAccountVerificationBean bean = (ExternalAccountVerificationBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<String, Object> model = new LinkedHashMap<>();
		CustomerExternalTransferSource ts = getTransferSource(userSession, bean.getAccountId());

		if (ts == null) {
			showForm(request, errors, "externalAccountVerification");
		}

		ExternalTransferSourceStatus validateExternalAccount;
		try {
			validateExternalAccount = userSession.validateExternalAccount(ts,
					Utils.generateCustomerInteractionEvent(request), bean.getAmount1(), bean.getAmount2());

			if (validateExternalAccount.equals(ExternalTransferSourceStatus.VALIDATED)) {
				userSession.resetExternalAccounts();
				model.put("validated", true);
			} else if (validateExternalAccount.equals(ExternalTransferSourceStatus.BLOCKED)) {
				model.put("blocked", true);
			} else {
				model.put("notvalidated", true);
			}
			model.put("account", ts);
			return showForm(request, errors, "externalAccountVerification", model);

		} catch (Exception e) {
			logger.error(e);

			errors.reject("nerds.account.verification.error", "Could not verify account at this time");
			return showForm(request, response, errors);

		}

	}

	private CustomerExternalTransferSource getTransferSource(UserSession userSession, String frontEndId) {

		List<CustomerExternalTransferSource> sources = userSession.getPendingExternalAccountsValidation();

		for (CustomerExternalTransferSource customerExternalTransferSource : sources) {
			if (customerExternalTransferSource.getFrontEndId().equalsIgnoreCase(frontEndId))
				return customerExternalTransferSource;
		}
		return null;
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {

		ExternalAccountVerificationBean bean = (ExternalAccountVerificationBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		/*
		 * if (bean.getAmount1()==null || bean.getAmount1Confirmation() == null)
		 * { errors.rejectValue("amount1", "external.amtError", "Required"); }
		 * else if (!bean.getAmount1().equals(bean.getAmount1Confirmation())) {
		 * errors.rejectValue("amount1Confirmation", "external.amtDiscrepancy",
		 * "Must be equal"); }
		 * 
		 * if (bean.getAmount2()==null || bean.getAmount2Confirmation() == null)
		 * { errors.rejectValue("amount2", "external.amtError", "Required"); }
		 * else if (!bean.getAmount2().equals(bean.getAmount2Confirmation())) {
		 * errors.rejectValue("amount2Confirmation", "external.amtDiscrepancy",
		 * "Must be equal"); }
		 */

		CustomerExternalTransferSource ts = getTransferSource(userSession, bean.getAccountId());
		if (ts == null) {
			errors.rejectValue("accountId", "", "External Account not valid");
		}
	}

	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		String stringParameter = ServletRequestUtils.getStringParameter(request, "accountId");
		ExternalAccountVerificationBean externalAccountVerificationBean = new ExternalAccountVerificationBean();

		if (!StringUtils.isEmpty(stringParameter))
			externalAccountVerificationBean.setAccountId(stringParameter);

		return externalAccountVerificationBean;
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<String, String> m = new HashMap<String, String>();

		List<CustomerExternalTransferSource> pendingExternalAccountsValidation = userSession
				.getPendingExternalAccountsValidation();
		AccountMaskUtil accountMaskUtil = new AccountMaskUtil();

		for (CustomerExternalTransferSource customerExternalTransferSource : pendingExternalAccountsValidation) {
			m.put(customerExternalTransferSource.getFrontEndId(), customerExternalTransferSource.getBankName() + " "
					+ accountMaskUtil.mask(customerExternalTransferSource.getAccountNumber()));

		}

		model.put("unverifiedExternalAccountMap", m);
		model.put("pendingAccounts", pendingExternalAccountsValidation);

		ExternalAccountVerificationBean bean = (ExternalAccountVerificationBean) command;
		String actid = bean.getAccountId();
		if (actid == null || actid.equals("")) {
			actid = (String) request.getParameter("accountId");
			if (actid != null && !actid.equals(""))
				bean.setAccountId(actid);
		}

		CustomerExternalTransferSource ts = null;
		if (actid != null && !actid.equals("")) {
			ts = getTransferSource(userSession, actid);
		}
		if (ts != null) {
			ExternalTransferSourceStatus validateExternalAccount = ts.getStatus();
			if (validateExternalAccount.equals(ExternalTransferSourceStatus.BLOCKED)) {
				model.put("blocked", true);
			}
			model.put("account", ts);
		}

		return model;
	}
}
