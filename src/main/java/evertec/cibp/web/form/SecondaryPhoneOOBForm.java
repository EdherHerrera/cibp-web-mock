package evertec.cibp.web.form;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerActions;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyRSAEntitlementRequest;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.web.form.bean.ChangeOOBBean;
import evertec.cibp.web.form.bean.SecondaryPhoneOOBBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

public class SecondaryPhoneOOBForm extends AbstractWizardFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public SecondaryPhoneOOBForm() {
		super();
		setAllowDirtyBack(true);
		setCommandClass(SecondaryPhoneOOBBean.class);
		setCommandName("secondaryPhoneOOB");
		setPages(new String[] { "secondaryPhoneOOB", "secondaryPhoneOOB" });
	}

	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		SecondaryPhoneOOBBean secPhoneBean = (SecondaryPhoneOOBBean) command;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		request.getSession().removeAttribute("pageSecondaryPhoneOOB");

		SecondaryPhoneOOBBean.ACTION action = secPhoneBean.getAction();

		if (action == SecondaryPhoneOOBBean.ACTION.SPHONE_SAVE || 
			action == SecondaryPhoneOOBBean.ACTION.SPHONE_DELETE) {
			
			String phone = "";
			String provider = "";
			//Set alternate phone and carrier to "N" if action is delete
			if (action == SecondaryPhoneOOBBean.ACTION.SPHONE_DELETE) {
				phone = "N";
				provider = "N";
		    //Otherwise set them to the values entered by the user
			} else {	
				phone = (String) WebUtils.getSessionAttribute(request, "oobSecondaryPhone");
				provider = (String) WebUtils.getSessionAttribute(request, "oobSecondaryProvider");
			}
				
			CustomerEntitlement ent = customer.getEntitlementByType(EntitlementType.RSA);
			if (ent != null) {
				CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request);
				try {

					if (!ent.getString("ALTERNATE_PHONE").equals(phone) || 
							!ent.getString("ALTERNATE_PROVIDER").equals(provider)) {
						Boolean alwaysChallenge = (ent.getString("ALWAYSCHALLENGE").equalsIgnoreCase("FALSE")) ? false : true;
						ModifyRSAEntitlementRequest modifyRequest = new ModifyRSAEntitlementRequest(interactionEvent, 
								customer, phone, provider, alwaysChallenge, false, ent.getString("CODE_HASH"), ent.getString("CODE_SALT"));
						customerService.modifyCustomerRSAEntitlement(modifyRequest);
						
						// Increment alternate phone counter for Monthly Report
						if (ent.getString("ALTERNATE_PHONE").equalsIgnoreCase("N"))
							customerService.logCustomerUsageData(customer, CustomerActions.OOB_ALT_PHONE);
					}

					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession.setStatusmessage("Your alternate phone number has been modified.");
					else
						userSession
						.setStatusmessage("Tu n&uacute;mero de tel&eacute;fono alterno ha sido modificado.");

					model.put("phoneUpdated", true);
					boolean emailSent = customerService.sendOobUpdateEmail(userSession.getCustomerProfile(),
							userSession.getPreferredLanguage(),ChangeOOBBean.ACTION.EDIT_BACKUP_PHONE.name());
					if(!emailSent) {
						this.logger.error(SecondaryPhoneOOBForm.class.getName()+":"+"Could not send oob update alternate phone confirmation email to " + userSession.getCustomerProfile().getUsername() + " ("
								+ userSession.getCustomerProfile().getEmail() + ")");
					}
					return new ModelAndView("secondaryPhoneOOB", model);

				} catch (Exception e) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
						userSession
						.setStatusmessage("Unable to edit your alternate phone number at this time, please try later.");
					else
						userSession
						.setStatusmessage("Problemas al cambiar su n&uacute;mero de tel&eacute;fono secundario en este momento, favor intentar luego.");
					model.put("unableUpdate", true);
					return new ModelAndView("secondaryPhoneOOB", model);
				}
			}
		} else {
			request.getSession().setAttribute("pageSecondaryPhoneOOB", 1);
		}

		return new ModelAndView("secondaryPhoneOOB", model);
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		List<ASK_FOR_OOB> listNavOOB = userSession.getTasksListOOB();
		if(listNavOOB != null && listNavOOB.contains(ASK_FOR_OOB.EDIT_BACKUP_PHONE)){
			userSession.getTasksListOOB().remove(ASK_FOR_OOB.EDIT_BACKUP_PHONE);
			request.setAttribute("firstTime", false);
		}
		
		if (request.getSession().getAttribute("pageSecondaryPhoneOOB") != null) {
			page = Integer.parseInt(request.getSession().getAttribute("pageSecondaryPhoneOOB").toString());
			request.getSession().setAttribute("secondaryPhoneOOB", command);
		}
		request.getSession().setAttribute("pageSecondaryPhoneOOB", page);

		model.put("page", Integer.toString(page));
		model.put("customer", userSession.getCustomerProfile());
		if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.RSA) != null) {
			CustomerEntitlement rsaEntitlement = userSession.getCustomerProfile()
					.getEntitlementByType(EntitlementType.RSA);
			String secondaryPhone = rsaEntitlement.getString("ALTERNATE_PHONE");
			if (secondaryPhone == null || secondaryPhone.length() != 10)
				model.put("EnrollSMS", false);
			else
				model.put("EnrollSMS", true);

		} else
			model.put("EnrollSMS", false);

		if (page == 0) {
			Map<String, Object> smsproviders = new LinkedHashMap<String, Object>();
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				smsproviders.put("none", "Select a provider");
			else
				smsproviders.put("none", "Selecciona un proveedor");

			smsproviders.put("PR1", "OPEN");
			smsproviders.put("PR7", "CLARO");
			smsproviders.put("PR6", "CENTENNIAL");
			smsproviders.put("US3", "AT&T");
			smsproviders.put("US4", "T-MOBILE");
			smsproviders.put("US5", "SPRINT");

			model.put("smsproviders", smsproviders);
		}

		String msgerr = (String) WebUtils.getSessionAttribute(request, "msgerr");
		if (msgerr != null) {
			errors.rejectValue("provider", "", msgerr);
			return model;
		}

		return model;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {

		SecondaryPhoneOOBBean secPhoneBean = (SecondaryPhoneOOBBean) command;
		SecondaryPhoneOOBBean.ACTION action = secPhoneBean.getAction();

		if (request.getSession().getAttribute("pageSecondaryPhoneOOB") != null) {
			page = Integer.parseInt(request.getSession().getAttribute("pageSecondaryPhoneOOB").toString());
			request.getSession().setAttribute("secondaryPhoneOOB", command);
		}

		if (page == 0) {
			if (action == SecondaryPhoneOOBBean.ACTION.SPHONE_SAVE) {
				if (secPhoneBean.getProvider() == null || secPhoneBean.getProvider().equals("none")) {
					errors.rejectValue("provider", "sms.provider.invalid", "Please select a provider");
				}
				if (secPhoneBean.getPhone1() == null || secPhoneBean.getPhone1().equals("")
						|| !secPhoneBean.getPhone1().matches("\\d{3}")) {
					errors.rejectValue("phone3", "sms.phone.invalid", "Please enter a correct phone number");
				} else if (secPhoneBean.getPhone2() == null || secPhoneBean.getPhone2().equals("")
						|| !secPhoneBean.getPhone2().matches("\\d{3}")) {
					errors.rejectValue("phone3", "sms.phone.invalid", "Please enter a correct phone number");
				} else if (secPhoneBean.getPhone3() == null || secPhoneBean.getPhone3().equals("")
						|| !secPhoneBean.getPhone3().matches("\\d{4}")) {
					errors.rejectValue("phone3", "sms.phone.invalid", "Please enter a correct phone number");
				}
				String phone = secPhoneBean.getPhone1() + secPhoneBean.getPhone2() + secPhoneBean.getPhone3();
				request.getSession().removeAttribute("pageSecondaryPhoneOOB");

				if (errors.getAllErrors().size() == 0) {
					WebUtils.setSessionAttribute(request, "oobSecondaryPhone", phone);
					WebUtils.setSessionAttribute(request, "oobSecondaryProvider", secPhoneBean.getProvider());

				}
			}
		}// end if page == 0
		
	}

}
