package evertec.cibp.web.form.mobilecash;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.mobilecash.core.models.MobileCashHistoryStatus;
import evertec.cibp.mobilecash.core.models.MobileCashSearchStatus;
import evertec.cibp.mobilecash.core.models.MobileCashTransaction;
import evertec.cibp.mobilecash.core.models.MobileCashTransactionStatus;
import evertec.cibp.mobilecash.core.models.MobileCashTrxType;
import evertec.cibp.mobilecash.core.services.MobileCashServices;
import evertec.cibp.web.form.mobilecash.bean.MobileCashTrxAssignAtmBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

public class MobileCashTrxAssignAtmForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	private MobileCashServices mobileCashService;
	
	private String processingExpirationMinutes;
	private String delayParameter;

	public MobileCashTrxAssignAtmForm() {
		super();
		setCommandClass(MobileCashTrxAssignAtmBean.class);
		setCommandName("mobileCashTrxAssignAtm");
		setFormView("mobileCashTrxAssignAtm");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_DROP)) {
			Boolean cancelTrx = (request.getParameter("cancelTrx") == null || !request.getParameter("cancelTrx").equals("1")) ? false : true;
			if(cancelTrx){
				return this.cancelPendingTransaction(request, response, command, error);
			}
			return this.assignPendingAtmTransaction(request, response, command, error);
		} else {
			Map<String, Object> model = new LinkedHashMap<String, Object>();
			UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			CustomerProfile profile = userSession.getCustomerProfile();
			String trxID = userSession.getMobileCashTransaction();
			String QRCode = request.getParameter("atmToken");
			Boolean cancelTrx = (request.getParameter("cancelTrx") == null || !request.getParameter("cancelTrx").equals("1")) ? false : true;
			if(cancelTrx){
				if(GenericValidator.isBlankOrNull(trxID)){
					logger.error("Cancel parameter null.");
					model.put("status", MobileCashSearchStatus.ERROR);
					return showForm(request, error, "mobileCashTrxAssignAtm", model);
				}
				else{
					MobileCashTransaction transaction = this.mobileCashService.getMobileCashTrx(trxID);
					if(transaction == null){
						logger.error("Cannot find transaction. Username " + profile.getUsername());
						model.put("status", MobileCashSearchStatus.ERROR);
						return showForm(request, error, "mobileCashTrxAssignAtm", model);
					}
					if(transaction.getTrxExpDate().compareTo(Calendar.getInstance().getTime()) < 0 ){
						logger.error("Transaction is expired. Username " + profile.getUsername());
						model.put("status", MobileCashSearchStatus.ERROR);
						return showForm(request, error, "mobileCashTrxAssignAtm", model);
					}
					transaction.setTrxExpDate(Calendar.getInstance().getTime());
					this.mobileCashService.saveMobileCashTrx(transaction);
					CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request, profile.getUsername());
					interactionEvent.resetProperties();
					interactionEvent.putString("trx", transaction.getMobileCashTrxId());
					interactionEvent.putString("account", transaction.getAccount());
					interactionEvent.putString("section", transaction.getAccountSection());
					interactionEvent.putString("atmCard", transaction.getaTMCard());
					interactionEvent.putString("amount", String.valueOf(transaction.getAmount()));
					interactionEvent.putString("lang", transaction.getLanguage());
					interactionEvent.putString("type", transaction.getTrxType().toString());
					userSession.getCustomerService().logMobileCashTrxCancel(interactionEvent, InteractionSeverity.INFO);
					model.put("status", MobileCashSearchStatus.SUCCESS);
					model.put("trxReceipt", transaction.getTrxReceipt());
					return showForm(request, error, "mobileCashTrxAssignAtm", model);
				}
			}
			if(GenericValidator.isBlankOrNull(trxID) || GenericValidator.isBlankOrNull(QRCode) || QRCode.length() <= 20){
				logger.error("Post parameter null.");
				model.put("status", MobileCashSearchStatus.ERROR);
				return showForm(request, error, "mobileCashTrxAssignAtm", model);
			}
			else{
				//cleanQRCode
				logger.info("QR code received '"+QRCode+"'");
				QRCode = QRCode.replaceAll("\n", "");
				logger.info("QR code cleaned '"+QRCode+"'");

				MobileCashTransaction transaction = this.mobileCashService.getMobileCashTrx(trxID);
				if(transaction == null){
					logger.error("Cannot find transaction. Username " + profile.getUsername());
					model.put("status", MobileCashSearchStatus.ERROR);
					return showForm(request, error, "mobileCashTrxAssignAtm", model);
				}
				if(transaction.getTrxExpDate().compareTo(Calendar.getInstance().getTime()) < 0 ){
					logger.error("Transaction is expired. Username " + profile.getUsername());
					model.put("status", MobileCashSearchStatus.ERROR);
					return showForm(request, error, "mobileCashTrxAssignAtm", model);
				}
				transaction.setAtmToken(QRCode);
				transaction.setAtmId(QRCode.replace("242762", "").substring(0, QRCode.replace("242762", "").length() - 14));
				this.mobileCashService.saveMobileCashTrx(transaction);
				CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request, profile.getUsername());
				interactionEvent.resetProperties();
				interactionEvent.putString("trx", transaction.getMobileCashTrxId());
				interactionEvent.putString("account", transaction.getAccount());
				interactionEvent.putString("section", transaction.getAccountSection());
				interactionEvent.putString("atmCard", transaction.getaTMCard());
				interactionEvent.putString("amount", String.valueOf(transaction.getAmount()));
				interactionEvent.putString("lang", transaction.getLanguage());
				interactionEvent.putString("type", transaction.getTrxType().toString());
				userSession.getCustomerService().logMobileCashTrxAssignAtm(interactionEvent, InteractionSeverity.INFO);
				model.put("status", MobileCashSearchStatus.SUCCESS);
				model.put("trxReceipt", transaction.getTrxReceipt());
				return showForm(request, error, "mobileCashTrxAssignAtm", model);
			}
		}
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		return model;
	}
	
	private ModelAndView assignPendingAtmTransaction(HttpServletRequest request, HttpServletResponse response, Object command, 
			BindException error) throws Exception {
		Map<String, Object> model = new LinkedHashMap<String, Object>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		String pendingTranId = request.getParameter("pendingTranId");
		String QRCode = request.getParameter("atmToken");
		
		if(GenericValidator.isBlankOrNull(pendingTranId) || GenericValidator.isBlankOrNull(QRCode) || QRCode.length() <= 20){
			logger.error("Post parameter null.");
			model.put("status", MobileCashSearchStatus.MISSING_PARAMETERS);
			return showForm(request, error, "mobileCashTrxAssignAtm", model);
		} else{
			//cleanQRCode
			logger.info("QR code received '"+QRCode+"'");
			QRCode = QRCode.replaceAll("\n", "");
			logger.info("QR code cleaned '"+QRCode+"'");
			
			MobileCashTransaction transaction = this.mobileCashService.getMobileCashActiveTrx(pendingTranId);
			if (transaction == null) {
				logger.error("Cannot find transaction. Username " + profile.getUsername());
				model.put("status", MobileCashSearchStatus.ERROR);
				return showForm(request, error, "mobileCashTrxAssignAtm", model);
			}
			if (transaction.getTrxExpDate().compareTo(Calendar.getInstance().getTime()) < 0 ) {
				logger.error("Transaction is expired. Username " + profile.getUsername());
				model.put("status", MobileCashSearchStatus.ERROR);
				return showForm(request, error, "mobileCashTrxAssignAtm", model);
			}
			
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT861)) { 
				Date newExpirationDate = Utils.getExpirationMinutes(processingExpirationMinutes, Calendar.getInstance());
				transaction.setTrxExpDate(newExpirationDate);
			}
			transaction.setStatus(MobileCashTransactionStatus.PROCESSING.toString());
			transaction.setAtmTrxStatus(null);
			transaction.setAtmLocationId(null);
			transaction.setAtmLocation(null);
			transaction.setAtmTrxId(null);
			transaction.setAtmTrxStatusCode(null);
			transaction.setTrxProcessedDate(null);
			transaction.setAtmToken(QRCode);
			transaction.setAtmId(QRCode.replace("242762", "").substring(0, QRCode.replace("242762", "").length() - 14));
			this.mobileCashService.saveMobileCashTrx(transaction);
			
			userSession.setMobileCashTransactions(null);
			userSession.setMobileCashHistory(null);
			CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request, profile.getUsername());
			interactionEvent.resetProperties();
			interactionEvent.putString("trx", transaction.getMobileCashTrxId());
			interactionEvent.putString("account", transaction.getAccount());
			interactionEvent.putString("section", transaction.getAccountSection());
			interactionEvent.putString("atmCard", transaction.getaTMCard());
			interactionEvent.putString("amount", String.valueOf(transaction.getAmount()));
			interactionEvent.putString("lang", transaction.getLanguage());
			interactionEvent.putString("type", transaction.getTrxType().toString());
			userSession.getCustomerService().logMobileCashTrxAssignAtm(interactionEvent, InteractionSeverity.INFO);			
			model.put("status", MobileCashSearchStatus.SUCCESS);
			model.put("trxReceipt", transaction.getTrxReceipt());
			return showForm(request, error, "mobileCashTrxAssignAtm", model);
		}
	}

	private ModelAndView cancelPendingTransaction(HttpServletRequest request, HttpServletResponse response, Object command, 
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		String pendingTranId = request.getParameter("pendingTranId");

		if(GenericValidator.isBlankOrNull(pendingTranId)){
			logger.error("PendingTranId parameter null.");
			model.put("status", MobileCashSearchStatus.MISSING_PARAMETERS);
			return showForm(request, error, "mobileCashTrxAssignAtm", model);
		}
		else{
			MobileCashTransaction tran = this.getTransaction(request, userSession, pendingTranId);

			if(tran == null){
				logger.error("Cannot find transaction. Username " + profile.getUsername());
				model.put("status", MobileCashSearchStatus.TRX_NOT_FOUND);
				return showForm(request, error, "mobileCashTrxAssignAtm", model);
			}

			if(tran.getTrxExpDate().compareTo(Calendar.getInstance().getTime()) < 0 ){
				logger.error("Transaction is expired. Username " + profile.getUsername());
				model.put("status", MobileCashSearchStatus.ERROR);
				return showForm(request, error, "mobileCashTrxAssignAtm", model);
			}

			tran.setTrxExpDate(Calendar.getInstance().getTime());
			this.mobileCashService.saveMobileCashTrx(tran);
			userSession.getMobileCashTransactions().remove(tran);
			tran.setTrxStatus(MobileCashHistoryStatus.EXPIRED.toString());
			
			List<MobileCashTransaction> mobileCashHistory = userSession.getMobileCashHistory();
			if (mobileCashHistory == null)
				mobileCashHistory = new LinkedList<MobileCashTransaction>();
			mobileCashHistory.add(0,tran);
			userSession.setMobileCashHistory(mobileCashHistory);

			CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request, profile.getUsername());
			interactionEvent.resetProperties();
			interactionEvent.putString("trx", tran.getMobileCashTrxId());
			interactionEvent.putString("account",tran.getAccount());
			interactionEvent.putString("section", tran.getAccountSection());
			interactionEvent.putString("atmCard", tran.getaTMCard());
			interactionEvent.putString("amount", String.valueOf(tran.getAmount()));
			interactionEvent.putString("lang", tran.getLanguage());
			interactionEvent.putString("type", tran.getTrxType().toString());

			if(tran.getTrxType().equals(MobileCashTrxType.DEPOSIT)) {
				interactionEvent.putString("senderPhone", tran.getSenderPhone());
				interactionEvent.putString("receiverPhone", tran.getReceiverPhone());
				if(!GenericValidator.isBlankOrNull(tran.getMemo()))
					interactionEvent.putString("memo", tran.getMemo());
			}

			userSession.getCustomerService().logMobileCashTrxCancel(interactionEvent, InteractionSeverity.INFO);
			model.put("status", MobileCashSearchStatus.SUCCESS);
			model.put("trxReceipt", tran.getTrxReceipt());
			return showForm(request, error, "mobileCashTrxAssignAtm", model);
		}
	}

	public MobileCashTransaction getTransaction(HttpServletRequest request, UserSession userSession, String pendingTranId) {
		List<MobileCashTransaction> transactions = userSession.getMobileCashTransactions();

		for (Iterator<MobileCashTransaction> iterator = transactions.iterator(); iterator.hasNext();) {
			MobileCashTransaction tran = (MobileCashTransaction) iterator.next();
			if(pendingTranId.equals(tran.getTrxReceipt())) {
				return tran;
			}
		}

		return null;
	}

	public void setMobileCashService(MobileCashServices mobileCashService) {
		this.mobileCashService = mobileCashService;
	}

	public String getDelayParameter() {
		return delayParameter;
	}

	public void setDelayParameter(String delayParameter) {
		this.delayParameter = delayParameter;
	}

	public String getProcessingExpirationMinutes() {
		return processingExpirationMinutes;
	}

	public void setProcessingExpirationMinutes(String processedExpirationMinutes) {
		this.processingExpirationMinutes = processedExpirationMinutes;
	}

}
