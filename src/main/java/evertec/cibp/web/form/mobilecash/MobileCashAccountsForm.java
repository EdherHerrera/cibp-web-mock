
package evertec.cibp.web.form.mobilecash;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.models.configuration.ConfParameters;
import evertec.cibp.core.services.CustomerServices;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerPhone;
import evertec.cibp.core.models.enrollment.EnrollmentLiteStatus;
import evertec.cibp.core.services.responses.FingerprintRegistrationResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.mobilecash.core.models.MobileCashHistoryStatus;
import evertec.cibp.mobilecash.core.models.MobileCashTransaction;
import evertec.cibp.mobilecash.core.models.MobileCashTrxType;
import evertec.cibp.mobilecash.core.services.MobileCashServices;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.mobilecash.bean.MobileCashAccountsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.session.UserSessionType;
import evertec.cibp.web.utils.Utils;

public class MobileCashAccountsForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	
	private MobileCashServices mobileCashService;
	private String maxAmount;
	private String maxAmountCashDrop;
	private String delayParameter;

	public MobileCashAccountsForm() {
		super();
		setCommandClass(MobileCashAccountsBean.class);
		setCommandName("mobileCashAccounts");
		setFormView("mobileCashAccounts");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		return showForm(request, error, "mobileCashAccounts", model);
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		if (ServletRequestUtils.getBooleanParameter(request, "refresh", false)) {
			userSession.setMobileCashTransactions(null);
			userSession.setMobileCashHistory(null);
		}
		
		FingerprintRegistrationResponse fpResponse = Utils.doesUserHasFingerprintRegistered(userSession, delayParameter);

		userSession = getCustomerPhone (userSession);

		if (fpResponse.getStatus().equals(EnrollmentLiteStatus.FINGERPRINT_DELAY_FAILED))
			model.put("minutesLeft", fpResponse.getMinutesLeft());

		model.put("status", fpResponse.getStatus().getCode());
		model.put("accounts", userSession.getMobileCashAccounts());
		//MBNA-2678
		CustomerServices customerServices = userSession.getCustomerService();
		ConfParameters confParameters_forme = customerServices.getConfParametersByShortNameAndApp("MOBILECASH_TRX_MAXIMUM_AMOUNT", "MOBILE_CASH");
		ConfParameters confParameters_forother = customerServices.getConfParametersByShortNameAndApp("MOBILECASH_CASHDROP_TRX_MAXIMUM_AMOUNT", "MOBILE_CASH");
		String maxAmount = ((confParameters_forme != null && !confParameters_forme.getValue().isEmpty())) ? confParameters_forme.getValue() : "";
		model.put("maxAmount", maxAmount);
		String maxAmountCashDrop = ((confParameters_forother != null && !confParameters_forother.getValue().isEmpty())) ? confParameters_forother.getValue() : "";
		model.put("maxAmountCashDrop",maxAmountCashDrop);
		model.putAll(getTransactions(request));

		return model;
	}

	private UserSession getCustomerPhone (UserSession userSession){

		if(userSession.getCustomerPhone() == null ||
				GenericValidator.isBlankOrNull(userSession.getCustomerPhone().getPhoneNumber())) {
			CustomerPhone phone = new CustomerPhone();
			phone.setProfileId(userSession.getCustomerProfile().getProfileId());

			/**
			 * MBNA-2986
			 * Method getCustomerPhone : CIBP-WEB microservice integration
			 *
			 */
			phone = CustomerFacade.getInstance().getCustomerPhone(phone, userSession);

			if (phone != null) {
				userSession.getCustomerProfile().setCustomerPhone(phone);
			}
		}
		return userSession;
	}

	public Map<String, Object> getTransactions(HttpServletRequest request) {
		
		Map<String, Object> model = new HashMap<>();
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		List<MobileCashTransaction> pendingTrx = session.getMobileCashTransactions();
		List<MobileCashTransaction> historyTrx = session.getMobileCashHistory();
		int pendingCount = 0;
		
		if (pendingTrx == null && historyTrx == null) {
			pendingTrx = mobileCashService.getAllCustomerTrx(session.getCustomerPhone().getPhoneNumber());
			historyTrx = new LinkedList<MobileCashTransaction>();
		}
			
		for (MobileCashTransaction trx : pendingTrx) {
			
			if (!trx.getProfileId().equals(session.getCustomerProfile().getProfileId())) {
				trx.setReceived(true);
			}
			
			if (!UserSessionType.CUSTOMER_PROFILE_LITE.equals(session.getUserSessionType())) {
				if (trx.getAccount() != null && session.getAccounts() != null &&
						trx.getAccountSection() != null && trx.getAccountSection().length() > 0) {
					TVFrontendAccount frontendAccount = Utils.getAccountByKey(trx.getAccount() + "|IDA|" + 
						trx.getAccountSection().substring(0,1), session.getAccounts());
					if (frontendAccount != null) {
						trx.setAccountFrontEndId(frontendAccount.getFrontEndId());
						trx.setNickname(frontendAccount.getNickname());
					}
				}
			}
			
			if (!GenericValidator.isBlankOrNull(trx.getTrxStatus())
					|| trx.getTrxExpDate().before(new Date())) {
				if (trx.getTrxExpDate().before(new Date())) {
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT861)) {
						if (MobileCashHistoryStatus.PROCESSING.toString().equals(trx.getTrxStatus()))
							trx.setTrxStatus(MobileCashHistoryStatus.PENDING_ATM.toString());
					} else {
						if (MobileCashHistoryStatus.PROCESSING.toString().equals(trx.getTrxStatus()))
							trx.setTrxStatus(MobileCashHistoryStatus.EXPIRED.toString());
					}
				}
				
				historyTrx.add(trx);
			} else {
				if (trx.getTrxType().equals(MobileCashTrxType.WITHDRAW)) {
					model.put("pendingForMe", trx);
					pendingCount++;
				} else {
					if(!trx.getProfileId().equals(session.getCustomerProfile().getProfileId()))
						pendingCount++;
				}
			}
		}
		
		if (historyTrx != null)
			pendingTrx.removeAll(historyTrx);
		session.setMobileCashTransactions(pendingTrx);
		session.setMobileCashHistory(historyTrx);
	
		model.put("pendingCount", pendingCount);
		
		return model;
	}
	
	public void setMobileCashService(MobileCashServices mobileCashService) {
		this.mobileCashService = mobileCashService;
	}
	
	public void setMaxAmount(String maxAmount) {
		this.maxAmount = maxAmount;
	}
	
	public void setMaxAmountCashDrop(String maxAmountCashDrop) {
		this.maxAmountCashDrop = maxAmountCashDrop;
	}

	public String getDelayParameter() {
		return delayParameter;
	}

	public void setDelayParameter(String delayParameter) {
		this.delayParameter = delayParameter;
	}

}
