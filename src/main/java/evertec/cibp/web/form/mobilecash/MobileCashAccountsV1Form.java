package evertec.cibp.web.form.mobilecash;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.web.form.mobilecash.bean.MobileCashAccountsBean;
import evertec.cibp.web.session.UserSession;

public class MobileCashAccountsV1Form extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	
	private String maxAmount;

	public MobileCashAccountsV1Form() {
		super();
		setCommandClass(MobileCashAccountsBean.class);
		setCommandName("mobileCashAccountsV1");
		setFormView("mobileCashAccountsV1");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		return showForm(request, error, "mobileCashAccountsV1", model);
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		model.put("accounts", userSession.getMobileCashAccounts());
		model.put("maxAmount", this.maxAmount);
		return model;
	}

	public void setMaxAmount(String maxAmount) {
		this.maxAmount = maxAmount;
	}

}
