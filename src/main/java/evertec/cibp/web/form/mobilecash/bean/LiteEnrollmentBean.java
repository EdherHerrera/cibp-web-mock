package evertec.cibp.web.form.mobilecash.bean;

public class LiteEnrollmentBean {
	
	private String username;
	private String phoneNumber;
	private String phoneProvider;
	private String email;
	private String deviceId;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPhoneProvider() {
		return phoneProvider;
	}
	public void setPhoneProvider(String phoneProvider) {
		this.phoneProvider = phoneProvider;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
}