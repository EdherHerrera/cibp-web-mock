package evertec.cibp.web.form.mobilecash;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CibpPushTokens;
import evertec.cibp.core.models.customer.CustomerPhone;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.EasyOfacCustomer;
import evertec.cibp.core.models.customer.EasyOfacCustomerStatus;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.responses.CustomerDeviceResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.mobilecash.core.models.MobileCashAppId;
import evertec.cibp.mobilecash.core.models.MobileCashHistoryStatus;
import evertec.cibp.mobilecash.core.models.MobileCashSearchStatus;
import evertec.cibp.mobilecash.core.models.MobileCashTransaction;
import evertec.cibp.mobilecash.core.models.MobileCashTransactionStatus;
import evertec.cibp.mobilecash.core.models.MobileCashTrxType;
import evertec.cibp.mobilecash.core.services.MobileCashServices;
import evertec.cibp.mobilecash.core.services.responses.MobileCashResponse;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.mobilecash.bean.MobileCashTrxSubmitBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.session.UserSessionType;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;
import evertec.easyofac.models.CustomerSearch;
import evertec.easyofac.models.ResponseOfac;
import evertec.easyofac.models.ResponseStatus;

public class MobileCashTrxSubmitForm extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());
	
	private MobileCashServices mobileCashService;
	
	private String withdrawExpirationMinutes;
	private String depositExpirationMinutes;
	private String easyOfacEndpoint;
//	private String tokenId = "cJpsoJsToyg:APA91bENbf86AtOFkVr2xbMW5T5yCWU8K1eh6SmUJ1vOAmPiuI4cOnZZ-Ud306g37AvM1zWQlXsfUWShlU_1HD1HoZbQ_0gOH6nLcjQnkpBlAbVHvXBpz-z5qNckRWBI9vJOV3cjMkHd";
	
	//formatter takes the same value every time date is sent
	private SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	 
	public synchronized String synchronizedFormatter(Date date) {
		    return df.format(date);
		  }
	public MobileCashTrxSubmitForm() {
		super();
		setCommandClass(MobileCashTrxSubmitBean.class);
		setCommandName("mobileCashTrxSubmit");
		setFormView("mobileCashTrxSubmit");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		
		Map<String, Object> model = new LinkedHashMap<>();
		
		boolean cashdrop = StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_DROP);
		model.put("cashdrop", cashdrop);
		
		
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		String frontId = request.getParameter("accountFrontendId");
		String amount = request.getParameter("amount");
		String lang = request.getParameter("language");
		String atmLast4Num = request.getParameter("atmLast4Num");
		String atmType = request.getParameter("atmType");
		String transType = request.getParameter("tranType");
		
		if (GenericValidator.isBlankOrNull(frontId) || GenericValidator.isBlankOrNull(amount) || GenericValidator.isBlankOrNull(lang)
				|| GenericValidator.isBlankOrNull(atmLast4Num) || GenericValidator.isBlankOrNull(atmType) || GenericValidator.isBlankOrNull(transType)){
			logger.error("Pre-Stage parameters null.");
			model.put("status", MobileCashSearchStatus.MISSING_PARAMETERS.toString());
			return showForm(request, error, "mobileCashTrxSubmit", model);
		} else{
			String receiverPhone = request.getParameter("receiverPhone");
			String memo = request.getParameter("memo");
			boolean isCashDrop = false;
			
			if(transType.equalsIgnoreCase(MobileCashTrxType.DEPOSIT.toString())){
				isCashDrop = true;
				
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MOBILE_CASH_FOR_OTHERS)) {
					// Checks if Global Entitlement (MOBILE_CASH_FOR_OTHERS) is disabled.
					try {
						
						if (userSession.getCustomerService().
								getGlobalEntitlement(EntitlementType.MOBILE_CASH_FOR_OTHERS).getStatus() == EntitlementStatus.DISABLED) {
							model.put("transactions", new ArrayList<MobileCashTransaction>());
							model.put("status", MobileCashSearchStatus.ERROR);
							logger.info(EntitlementType.MOBILE_CASH_FOR_OTHERS.toString() + " entitlement not enabled. Blocking transaction.");
							return showForm(request, error, "mobileCashTrxSubmit", model);	
						}

					} catch (Exception e) {
						logger.error("Error getting the " + EntitlementType.MOBILE_CASH_FOR_OTHERS.toString() + " entitlement from the database.");
					}
				}
				
				if (userSession.getCustomerPhone() == null || GenericValidator.isBlankOrNull(userSession.getCustomerPhone().getPhoneNumber()) || GenericValidator.isBlankOrNull(receiverPhone)) {
					logger.error("Customer Phone number is null.");
					model.put("status", MobileCashSearchStatus.MISSING_PARAMETERS);
					model.put("transactions", new ArrayList<MobileCashTransaction>());
					return showForm(request, error, "mobileCashTrxSubmit", model);
				}
				
				CustomerDeviceResponse phoneResponse = userSession.getCustomerService().isPhoneInBlacklist(profile.getCustomerPhone().getPhoneNumber(), receiverPhone);
				if (phoneResponse == null) {
					model.put("blackListStatus", ResultStatus.ERROR);
					model.put("transactions", new ArrayList<MobileCashTransaction>());
					return showForm(request, error, "mobileCashTrxSubmit", model);
				}
				
				if(ResultStatus.SENDERPHONE_IN_BLACKLIST.equals(phoneResponse.getResponseStatus())) {
					model.put("blackListStatus", ResultStatus.SENDERPHONE_IN_BLACKLIST);
					model.put("transactions", new ArrayList<MobileCashTransaction>());
					return showForm(request, error, "mobileCashTrxSubmit", model);
				} else if (ResultStatus.RECEIVERPHONE_IN_BLACKLIST.equals(phoneResponse.getResponseStatus())){
					model.put("blackListStatus", ResultStatus.RECEIVERPHONE_IN_BLACKLIST);
					model.put("transactions", new ArrayList<MobileCashTransaction>());
					return showForm(request, error, "mobileCashTrxSubmit", model);
				}
			}
			
			lang = lang.toUpperCase(Locale.ENGLISH);
			TVFrontendAccount frontendAccount = Utils.getAccountById(frontId, userSession.getAccounts());
			if(frontendAccount == null){
				logger.error("Cannot find account. Username " + profile.getUsername());
				model.put("status", MobileCashSearchStatus.ERROR);
				model.put("transactions", new ArrayList<MobileCashTransaction>());
				return showForm(request, error, "mobileCashTrxSubmit", model);
			}
			CardPlasticAccountRecord ath = this.getCardPlastic(atmLast4Num, frontId, atmType, userSession);
						
			if(ath == null){
				logger.error("Cannot find ath plastic. Username " + profile.getUsername());
				model.put("status", MobileCashSearchStatus.ERROR);
				model.put("transactions", new ArrayList<MobileCashTransaction>());
				return showForm(request, error, "mobileCashTrxSubmit", model);
			}
			Calendar calendar = Calendar.getInstance();
			int permIndex = profile.getPermId().toString().length() - 6;
			String receipt = String.valueOf(calendar.getTimeInMillis() + profile.getPermId().toString().substring(permIndex));
			
			List<MobileCashTransaction> transactions = new ArrayList<MobileCashTransaction>();
			MobileCashTransaction transaction = new MobileCashTransaction();
			transaction.setAccount(frontendAccount.getAccountNumber());
			transaction.setAccountSection(frontendAccount.getAccountSection().equals("C") ? "CHK" : "SAV");
			try{
				transaction.setAmount(Double.valueOf(amount));
	    	}catch(Exception e){
	    		logger.error("Cannot cast amount sent. " + e.getMessage());
	    		model.put("transactions", new ArrayList<MobileCashTransaction>());
				model.put("status", MobileCashSearchStatus.ERROR);
				return showForm(request, error, "mobileCashTrxSubmit", model);
	    	}
			
			transaction.setAppId(MobileCashAppId.CIBP);
			transaction.setaTMCard(ath.getCardPANId());
			transaction.setaTMCardExpDate(ath.getExpirationDate().getTime());
			transaction.setProfileId(profile.getProfileId());
			transaction.setUserName(profile.getUsername());
			if (isCashDrop) {
				transaction.setReceiverPhone(receiverPhone);
				transaction.setMemo(memo);
				transaction.setTrxExpDate(this.getExpirationMinutes(depositExpirationMinutes, calendar));
			} else {
				transaction.setTrxExpDate(this.getExpirationMinutes(withdrawExpirationMinutes, calendar));
			}

			transaction.setSenderPhone(userSession.getCustomerPhone().getPhoneNumber());
			transaction.setTrxType(MobileCashTrxType.valueOf(transType));
			transaction.setAccountFrontEndId(frontendAccount.getFrontEndId());
			transaction.setNickname(frontendAccount.getNickname());
			transaction.setTrxReceipt(receipt);
			transaction.setCreationDate(Calendar.getInstance().getTime());
			transaction.setLanguage(lang);
			transaction.setStatus(MobileCashTransactionStatus.ACTIVE.toString());
			if(this.mobileCashService.saveMobileCashTrx(transaction)){
				CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request, profile.getUsername());
				interactionEvent.resetProperties();
				interactionEvent.putString("trx", transaction.getMobileCashTrxId());
				interactionEvent.putString("account", transaction.getAccount());
				interactionEvent.putString("section", transaction.getAccountSection());
				interactionEvent.putString("atmCard", transaction.getaTMCard());
				interactionEvent.putString("amount", String.valueOf(transaction.getAmount()));
				interactionEvent.putString("lang", transaction.getLanguage());
				interactionEvent.putString("type", transaction.getTrxType().toString());
				interactionEvent.putString("senderPhone", transaction.getSenderPhone());

				if (isCashDrop) {
					interactionEvent.putString("receiverPhone", transaction.getReceiverPhone());
					interactionEvent.putString("memo", transaction.getMemo());
					
					//userSession.getCustomerService().sendPush(tokenId, String.valueOf(transaction.getAmount()));
					//this.sendPush(userSession, transaction);
				}

				transactions.add(transaction);
				List<MobileCashTransaction> mobileCashTransactions = userSession.getMobileCashTransactions();
				if (mobileCashTransactions == null)
					mobileCashTransactions = new LinkedList<MobileCashTransaction>();
				mobileCashTransactions.add(transaction);
				userSession.setMobileCashTransactions(mobileCashTransactions);
				model.put("transactions", transactions);
				model.put("status", MobileCashSearchStatus.TRX_FOUND.toString());
				userSession.getCustomerService().logMobileCashTrxSubmit(interactionEvent, InteractionSeverity.INFO, transaction.getTrxType());
				return showForm(request, error, "mobileCashTrxSubmit", model);
			}
			else{
				model.put("status", MobileCashSearchStatus.ERROR.toString());
				model.put("transactions", new ArrayList<MobileCashTransaction>());
				return showForm(request, error, "mobileCashTrxSubmit", model);
			}
			
		}
	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		boolean cashdrop = StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_DROP);
		Map<String, Object> model = new HashMap<>();
		model.put("cashdrop", cashdrop);
		
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_DROP)) {

			if (ServletRequestUtils.getBooleanParameter(request, "refresh", false)) {
				userSession.setMobileCashTransactions(null);
				userSession.setMobileCashHistory(null);
			}
			
			CustomerDeviceResponse phoneResponse = userSession.getCustomerService().isPhoneNumberInBlacklist(userSession.getCustomerPhone().getPhoneNumber());
			
			if (phoneResponse != null && phoneResponse.getResponseStatus().equals(ResultStatus.PHONE_IN_BLACKLIST)) { 
				model.put("blackListStatus", MobileCashSearchStatus.PHONE_IN_BLACKLIST);
			}

			if(userSession.getUserSessionType().equals(UserSessionType.CUSTOMER_PROFILE_LITE) && StaticMessageSource.isFlagEnabled(StaticMessageSource.EASY_OFAC) && 
					!GenericValidator.isBlankOrNull(userSession.getCustomerProfile().getOfacId())) {
				CustomerSearch search = new CustomerSearch();
				search.setId(userSession.getCustomerProfile().getOfacId());
				search.setCountry(userSession.getCustomerProfile().getCountry());
				ResponseOfac ofacResponse = userSession.getCustomerService().inspectCustomerEasyOfac(search);
				
				if (ofacResponse != null && ofacResponse.getStatus() != null && (ofacResponse.getStatus().equals(ResponseStatus.UNSAFE) || 
						ofacResponse.getStatus().equals(ResponseStatus.COUNTRY_MATCH))) {
					EasyOfacCustomerStatus ofacStatus = Utils.convertEasyOfacStatus(ofacResponse.getStatus()); 
			        
					EasyOfacCustomer cust = new EasyOfacCustomer();
					cust.setValidatedDate(new Date());
					cust.setEasyOfacId(userSession.getCustomerProfile().getOfacId());
					cust.setStatus(ofacStatus);
					
					if (userSession.getCustomerService().updateEasyOfacCustomerInDB(cust)) {
						userSession.setEasyOfacMatch(true);
					}
				}
			}
			
			model.put("easyOfacStatus", userSession.isEasyOfacMatch());
			model.putAll(setTransactionsInModel(request));
			
			return model;
		} else {
			MobileCashResponse mobileCashResponse = this.mobileCashService.getCustomerCurrentPendingTrx(profile.getProfileId());
			model.put("status", mobileCashResponse.getStatus());
			if(mobileCashResponse.getStatus().equals(MobileCashSearchStatus.TRX_FOUND.toString())){
				if(!GenericValidator.isBlankOrNull(mobileCashResponse.getAtmToken()) 
						|| !GenericValidator.isBlankOrNull(mobileCashResponse.getAtmTrxStatus())
						|| !GenericValidator.isBlankOrNull(mobileCashResponse.getAtmTrxStatusCode())){
					logger.error("ATM data already assigned to mobile cash transaction.");
					MobileCashTransaction transaction = this.mobileCashService.getMobileCashTrx(mobileCashResponse.getMobileCashTrxId());
					transaction.setTrxExpDate(Calendar.getInstance().getTime());
					this.mobileCashService.saveMobileCashTrx(transaction);
					model.put("status", MobileCashSearchStatus.TRX_NOT_FOUND);
					return model;
				}
				userSession.setMobileCashTransaction(mobileCashResponse.getMobileCashTrxId());
				try {
					TVFrontendAccount account = Utils.getAccountByKey(mobileCashResponse.getAccount() + 
							"|IDA|" + mobileCashResponse.getAcctSection().substring(0,1), userSession.getAccounts());
					
					model.put("nickname", account.getNickname());
					model.put("account", mobileCashResponse.getAccount());
					model.put("amount", mobileCashResponse.getAmount());
					model.put("accountSection", mobileCashResponse.getAcctSection());
					model.put("trxExpDate", this.synchronizedFormatter(mobileCashResponse.getTrxExpDate()));
					model.put("trxReceipt", mobileCashResponse.getReceipt());
					model.put("atmCard", mobileCashResponse.getAtmCardNumber());
					
				} catch (Exception e) {
					logger.error("Account assigned to mobile cash transaction is null. " + e.getMessage());
					MobileCashTransaction transaction = this.mobileCashService.getMobileCashTrx(mobileCashResponse.getMobileCashTrxId());
					transaction.setTrxExpDate(Calendar.getInstance().getTime());
					this.mobileCashService.saveMobileCashTrx(transaction);
					model.put("status", MobileCashSearchStatus.TRX_NOT_FOUND);
					return model;
				}
			}
			return model;
		}
	}
	
	@SuppressWarnings("unused")
	private void sendPush(UserSession userSession, MobileCashTransaction transaction) {
		//Validate if CustomerLite has a phone registered for push.
		CustomerPhone phone = new CustomerPhone();
		phone.setPhoneNumber(transaction.getReceiverPhone());
		
		/**
		* MBNA-2986
		* Method getCustomerPhone : CIBP-WEB microservice integration
		* 
		*/
		phone = CustomerFacade.getInstance().getCustomerPhone(phone, userSession);
		
		if (phone != null && !GenericValidator.isBlankOrNull(phone.getPhoneNumber())) {
			CibpPushTokens pushDevice = new CibpPushTokens();
			pushDevice.setProfileId(phone.getProfileId());
			
			CustomerDeviceResponse response = userSession.getCustomerService().getPushTokenByProfileId(pushDevice);
			if(response != null && response.getResponseStatus().equals(ResultStatus.SUCCESS)) {
				userSession.getCustomerService().sendPush(response.getCustomerPushDevice().getPushToken(), String.valueOf(transaction.getAmount()));
			}
		}
	}
	
	private Map<String, Object> setTransactionsInModel(HttpServletRequest request) {
		
		Map<String, Object> model = new HashMap<>();
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		List<MobileCashTransaction> pendingTrx = session.getMobileCashTransactions();
		List<MobileCashTransaction> historyTrx = session.getMobileCashHistory();
		
		if (pendingTrx == null && historyTrx == null) {
		
			pendingTrx = mobileCashService.getAllCustomerTrx(session.getCustomerPhone().getPhoneNumber());
			historyTrx = new LinkedList<MobileCashTransaction>();
			
			for (MobileCashTransaction trx : pendingTrx) {
				
				if (!trx.getProfileId().equals(session.getCustomerProfile().getProfileId())) {
					trx.setReceived(true);
				}
				
				if (!UserSessionType.CUSTOMER_PROFILE_LITE.equals(session.getUserSessionType())) {
					if (trx.getAccount() != null && session.getAccounts() != null && 
							trx.getAccountSection() != null && trx.getAccountSection().length() > 0) {
						TVFrontendAccount frontendAccount = Utils.getAccountByKey(trx.getAccount() + 
								"|IDA|" + trx.getAccountSection().substring(0,1), session.getAccounts());
						if (frontendAccount != null) {
							trx.setAccountFrontEndId(frontendAccount.getFrontEndId());
							trx.setNickname(frontendAccount.getNickname());
						}
					}	
				}
				
				if (!GenericValidator.isBlankOrNull(trx.getTrxStatus()) 
						|| trx.getTrxExpDate().before(new Date())) {
					if (trx.getTrxExpDate().before(new Date())) {
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT861)) {
							if (MobileCashHistoryStatus.PROCESSING.toString().equals(trx.getTrxStatus()))
								trx.setTrxStatus(MobileCashHistoryStatus.PENDING_ATM.toString());
						} else {
							if (MobileCashHistoryStatus.PROCESSING.toString().equals(trx.getTrxStatus()))
								trx.setTrxStatus(MobileCashHistoryStatus.EXPIRED.toString());
						}
					}
					
					historyTrx.add(trx);	
				}
	
			}
			
			pendingTrx.removeAll(historyTrx);
			session.setMobileCashTransactions(pendingTrx);
			session.setMobileCashHistory(historyTrx);
		}
		
		if ((pendingTrx != null && pendingTrx.size() > 0) || 
				(historyTrx != null && historyTrx.size() > 0)) {
			model.put("transactions", pendingTrx);
			model.put("history", historyTrx);
			model.put("status", MobileCashSearchStatus.TRX_FOUND);
		} else {
			model.put("status", MobileCashSearchStatus.TRX_NOT_FOUND);
		}
		
		return model;
	}
	
	private Date getExpirationMinutes(String expirationMinutes, Calendar calendar) {
		try{
			calendar.add(Calendar.MINUTE, Integer.valueOf(expirationMinutes));
			return calendar.getTime();
		}
		catch(Exception e){
			logger.error("Cannot add expiration minutes to date. " + e.getMessage());
			return calendar.getTime();
		}
	}
	
	private CardPlasticAccountRecord getCardPlastic(String atmLast4Num, String frontId, String atmType, UserSession userSession) {
		CardPlasticAccountRecord ath = null;
		for(int i = 0; i < userSession.getMobileCashAccounts().size(); i++){
			if(userSession.getMobileCashAccounts().get(i).getAccount().getFrontEndId().equals(frontId)){
				List<CardPlasticAccountRecord> plasticsAccount = userSession.getMobileCashAccounts().get(i).getAtmCards();
				for(int j = 0; j < plasticsAccount.size(); j++){
					boolean isReg = plasticsAccount.get(j).getCardPANId().startsWith("0215") ? true : false;
					String accountmask = "x" + plasticsAccount.get(j).getCardPANId().
											substring(plasticsAccount.get(j).getCardPANId().length() - 4, 
													plasticsAccount.get(j).getCardPANId().length());
					if(atmType.equals(AccountMaskUtil.ATH_REGULAR) && isReg 
							&& accountmask.equals(atmLast4Num)){
						ath = plasticsAccount.get(j);
						break;
					}
					if(atmType.equals(AccountMaskUtil.ATH_INTERNATIONAL) && !isReg 
							&& accountmask.equals(atmLast4Num)){
						ath = plasticsAccount.get(j);
						break;
					}
				}
			}
			if(ath != null){
				break;
			}
		}
		return ath;
	}
	
	public void setMobileCashService(MobileCashServices mobileCashService) {
		this.mobileCashService = mobileCashService;
	}
	
	public String getWithdrawExpirationMinutes() {
		return withdrawExpirationMinutes;
	}

	public void setWithdrawExpirationMinutes(String withdrawExpirationMinutes) {
		this.withdrawExpirationMinutes = withdrawExpirationMinutes;
	}

	public String getDepositExpirationMinutes() {
		return depositExpirationMinutes;
	}

	public void setDepositExpirationMinutes(String depositExpirationMinutes) {
		this.depositExpirationMinutes = depositExpirationMinutes;
	}
	
	public String getEasyOfacEndpoint() {
		return easyOfacEndpoint;
	}

	public void setEasyOfacEndpoint(String easyOfacEndpoint) {
		this.easyOfacEndpoint = easyOfacEndpoint;
	}
	
}
