package evertec.cibp.web.form.mobilecash;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.mobilecash.core.models.MobileCashAppId;
import evertec.cibp.mobilecash.core.models.MobileCashSearchStatus;
import evertec.cibp.mobilecash.core.models.MobileCashTransaction;
import evertec.cibp.mobilecash.core.models.MobileCashTrxType;
import evertec.cibp.mobilecash.core.services.MobileCashServices;
import evertec.cibp.mobilecash.core.services.responses.MobileCashResponse;
import evertec.cibp.web.form.mobilecash.bean.MobileCashTrxSubmitBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;

public class MobileCashTrxSubmitV1Form extends CancellableFormController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	private MobileCashServices mobileCashService;

	private String expirationMinutes;

	//formatter takes the same value every time date is sent
	private SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	private static final String VIEW_NAME = "mobileCashTrxSubmitV1";

	  public synchronized String synchronizedFormatter(Date date) {
		    return df.format(date);
		  }
	  
	public MobileCashTrxSubmitV1Form() {
		super();
		setCommandClass(MobileCashTrxSubmitBean.class);
		setCommandName(VIEW_NAME);
		setFormView(VIEW_NAME);
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {

		Map<String, Object> model = new LinkedHashMap<String, Object>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		String frontId = request.getParameter("accountFrontendId");
		String amount = request.getParameter("amount");
		String lang = request.getParameter("language");
		String atmLast4Num = request.getParameter("atmLast4Num");
		String atmType = request.getParameter("atmType");
		if(GenericValidator.isBlankOrNull(frontId) || GenericValidator.isBlankOrNull(amount) || GenericValidator.isBlankOrNull(lang)
				|| GenericValidator.isBlankOrNull(atmLast4Num) || GenericValidator.isBlankOrNull(atmType)){
			logger.error("Pre-stage parameter null.");
			model.put("status", MobileCashSearchStatus.ERROR);
			return showForm(request, error, VIEW_NAME, model);
		}
		else{
			lang = lang.toUpperCase(Locale.ENGLISH);
			TVFrontendAccount frontendAccount = Utils.getAccountById(frontId, userSession.getAccounts());
			if(frontendAccount == null){
				logger.error("Cannot find account. Username " + profile.getUsername());
				model.put("status", MobileCashSearchStatus.ERROR);
				return showForm(request, error, VIEW_NAME, model);
			}
			CardPlasticAccountRecord ath = null;
			for(int i = 0; i < userSession.getMobileCashAccounts().size(); i++){
				if(userSession.getMobileCashAccounts().get(i).getAccount().getFrontEndId().equals(frontId)){
					List<CardPlasticAccountRecord> plasticsAccount = userSession.getMobileCashAccounts().get(i).getAtmCards();
					for(int j = 0; j < plasticsAccount.size(); j++){
						boolean isReg = plasticsAccount.get(j).getCardPANId().startsWith("0215") ? true : false;
						String accountmask = "x" + plasticsAccount.get(j).getCardPANId().
								substring(plasticsAccount.get(j).getCardPANId().length() - 4, 
										plasticsAccount.get(j).getCardPANId().length());
						if(atmType.equals(AccountMaskUtil.ATH_REGULAR) && isReg 
								&& accountmask.equals(atmLast4Num)){
							ath = plasticsAccount.get(j);
							break;
						}
						if(atmType.equals(AccountMaskUtil.ATH_INTERNATIONAL) && !isReg 
								&& accountmask.equals(atmLast4Num)){
							ath = plasticsAccount.get(j);
							break;
						}
					}
				}
				if(ath != null){
					break;
				}
			}

			if(ath == null){
				logger.error("Cannot find ath plastic. Username " + profile.getUsername());
				model.put("status", MobileCashSearchStatus.ERROR);
				return showForm(request, error, VIEW_NAME, model);
			}
			Calendar calendar = Calendar.getInstance();
			int permIndex = profile.getPermId().toString().length() - 6;
			String receipt = String.valueOf(calendar.getTimeInMillis() + profile.getPermId().toString().substring(permIndex));
			try{
				calendar.add(Calendar.MINUTE, Integer.valueOf(this.expirationMinutes));
			}
			catch(Exception e){
				logger.error("Cannot add expiration minutes to date. " + e.getMessage());
				model.put("status", MobileCashSearchStatus.ERROR);
				return showForm(request, error, VIEW_NAME, model);
			}

			MobileCashTransaction transaction = new MobileCashTransaction();
			transaction.setAccount(frontendAccount.getAccountNumber());
			transaction.setAccountSection(frontendAccount.getAccountSection().equals("C") ? "CHK" : "SAV");
			try{
				transaction.setAmount(Double.valueOf(amount));
			}catch(Exception e){
				logger.error("Cannot cast amount sent. " + e.getMessage());
				model.put("status", MobileCashSearchStatus.ERROR);
				return showForm(request, error, VIEW_NAME, model);
			}
			transaction.setAppId(MobileCashAppId.CIBP);
			transaction.setaTMCard(ath.getCardPANId());
			transaction.setaTMCardExpDate(ath.getExpirationDate().getTime());
			transaction.setProfileId(profile.getProfileId());
			transaction.setUserName(profile.getUsername());
			transaction.setTrxType(MobileCashTrxType.WITHDRAW);
			transaction.setTrxExpDate(calendar.getTime());
			transaction.setTrxReceipt(receipt);
			transaction.setCreationDate(Calendar.getInstance().getTime());
			transaction.setLanguage(lang);
			if(this.mobileCashService.saveMobileCashTrx(transaction)){
				CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request, profile.getUsername());
				interactionEvent.resetProperties();
				interactionEvent.putString("trx", transaction.getMobileCashTrxId());
				interactionEvent.putString("account", transaction.getAccount());
				interactionEvent.putString("section", transaction.getAccountSection());
				interactionEvent.putString("atmCard", transaction.getaTMCard());
				interactionEvent.putString("amount", String.valueOf(transaction.getAmount()));
				interactionEvent.putString("lang", transaction.getLanguage());
				interactionEvent.putString("type", transaction.getTrxType().toString());
				userSession.getCustomerService().logMobileCashTrxSubmit(interactionEvent, InteractionSeverity.INFO, transaction.getTrxType());
				model.put("status", MobileCashSearchStatus.TRX_FOUND);
				model.put("account", transaction.getAccount());
				model.put("amount", transaction.getAmount());
				model.put("accountSection", transaction.getAccountSection());
				model.put("trxExpDate", this.synchronizedFormatter(transaction.getTrxExpDate()));
				model.put("trxReceipt", transaction.getTrxReceipt());
				model.put("atmCard", transaction.getaTMCard());
				model.put("nickname", frontendAccount.getNickname());
				return showForm(request, error, VIEW_NAME, model);
			}
			else{
				model.put("status", MobileCashSearchStatus.ERROR);
				return showForm(request, error, VIEW_NAME, model);
			}
		}

	}

	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		MobileCashResponse mobileCashResponse = this.mobileCashService.getCustomerCurrentPendingTrx(profile.getProfileId());
		model.put("status", mobileCashResponse.getStatus());
		
		if(mobileCashResponse.getStatus().equals(MobileCashSearchStatus.TRX_FOUND.toString())){
			if(!GenericValidator.isBlankOrNull(mobileCashResponse.getAtmToken()) 
					|| !GenericValidator.isBlankOrNull(mobileCashResponse.getAtmTrxStatus())
					|| !GenericValidator.isBlankOrNull(mobileCashResponse.getAtmTrxStatusCode())){
				logger.error("ATM data already assigned to mobile cash transaction.");
				MobileCashTransaction transaction = this.mobileCashService.getMobileCashTrx(mobileCashResponse.getMobileCashTrxId());
				transaction.setTrxExpDate(Calendar.getInstance().getTime());
				this.mobileCashService.saveMobileCashTrx(transaction);
				model.put("status", MobileCashSearchStatus.TRX_NOT_FOUND);
				return model;
			}
			userSession.setMobileCashTransaction(mobileCashResponse.getMobileCashTrxId());
			TVFrontendAccount account = Utils.getAccountByAccountNumber(mobileCashResponse.getAccount(), userSession.getAccounts());
			if(account == null){
				logger.error("Account assigned to mobile cash transaction is null.");
				MobileCashTransaction transaction = this.mobileCashService.getMobileCashTrx(mobileCashResponse.getMobileCashTrxId());
				transaction.setTrxExpDate(Calendar.getInstance().getTime());
				this.mobileCashService.saveMobileCashTrx(transaction);
				model.put("status", MobileCashSearchStatus.TRX_NOT_FOUND);
				return model;
			}
			model.put("nickname", account.getNickname());
			model.put("account", mobileCashResponse.getAccount());
			model.put("amount", mobileCashResponse.getAmount());
			model.put("accountSection", mobileCashResponse.getAcctSection());
			model.put("trxExpDate", this.synchronizedFormatter(mobileCashResponse.getTrxExpDate()));
			model.put("trxReceipt", mobileCashResponse.getReceipt());
			model.put("atmCard", mobileCashResponse.getAtmCardNumber());
		}
		return model;
	}

	public void setMobileCashService(MobileCashServices mobileCashService) {
		this.mobileCashService = mobileCashService;
	}

	public void setExpirationMinutes(String expirationMinutes) {
		this.expirationMinutes = expirationMinutes;
	}

}
