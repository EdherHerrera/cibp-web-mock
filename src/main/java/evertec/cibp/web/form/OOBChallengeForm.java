package evertec.cibp.web.form;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import java.nio.charset.StandardCharsets;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.services.requests.adaptiveauth.ChallengeOobRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RsaChallengeRequest;
import evertec.cibp.core.services.requests.adaptiveauth.ValidateOobChallengeRequest;
import evertec.cibp.core.services.responses.adaptiveauth.ChallengeOobResponse;
import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.util.WebUtils;
import org.springframework.web.util.UriUtils;

import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceRequest;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import evertec.cibp.campaign.loader.CampaignPaths;
import evertec.cibp.core.facades.AdaptiveAuthFacade;
import evertec.cibp.core.models.customer.CustomerActions;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.interaction.CustomerInteraction;
import evertec.cibp.core.models.customer.oob.CustomerOOBChallengeType;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.RSAEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.DeleteUserRSARequest;
import evertec.cibp.core.services.requests.SendSmsGeneratedCodeRequest;
import evertec.cibp.core.services.responses.OobRecoveryCodeStatusResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.utils.ApplicationsDefinition;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.OOBChallengeBean;
import evertec.cibp.web.form.mobile.bean.MobileAlert;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.NavigationSequenceUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import evertec.rsa.RSAConnector.Lang;

public class OOBChallengeForm extends AbstractWizardFormController {
	
	/** Logger */
	protected final Log log = LogFactory.getLog(getClass());
	
	private static final String RSA_USER = "RSA User: ";
	private static final String OOB_CODE = "oobCode";
	private static final String LOGINOOB = "loginoob";
	private static final String USERSSESION = "userSession";
	private static final String LOGIN_OOB_CALL_UNABLE = "login.oob.call.unable";
	private static final String HAS_ALT_PHONE = "hasAltPhone";
	
	private CampaignPaths paths;
	private MessageSource messageSource = null;
	private boolean redirectToLogin = false;
	private boolean redirectToBlock = false;
	private boolean finishCancel = false;
	private int recCodeBlockLength;
	private int recCodeMaxRetries;
	private int recCodeRetryTimePeriod;
	private List<ASK_FOR_OOB> alertTypeSubSection;
	private String sourceView;
	
	
	public OOBChallengeForm() {
		super();
		setCommandClass(OOBChallengeBean.class);
		setCommandName(LOGINOOB);
		setPages(new String[] {LOGINOOB, LOGINOOB, LOGINOOB, LOGINOOB, LOGINOOB });
		alertTypeSubSection = new ArrayList<> ();
		alertTypeSubSection.add(ASK_FOR_OOB.BALANCE);
		alertTypeSubSection.add(ASK_FOR_OOB.PERSONAL_INFORMATION_CHANGES);
		alertTypeSubSection.add(ASK_FOR_OOB.PAYMENTS_TRANSFERS);
		alertTypeSubSection.add(ASK_FOR_OOB.ATH_ALERT_TYPES);
		alertTypeSubSection.add(ASK_FOR_OOB.CCA_ALERT_TYPES);
		alertTypeSubSection.add(ASK_FOR_OOB.EDIT_EMAIL);
		alertTypeSubSection.add(ASK_FOR_OOB.PUSH_DEVICE_REMOVAL);
		
	}
	
	@Override
	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors, int page)
			throws Exception {

		Map<String, Object> model = new LinkedHashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSSESION);
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99
		
		//Used when you need to know specifically who called OOB without checking the OOBTaskList
		sourceView = request.getParameter("source");
		if (StringUtils.isNotEmpty(sourceView) && StringUtils.equals(sourceView, MobileAlert.DEVICE_MANAGEMENT_VIEW)) {
			setDeviceManagementOOB(userSession, false);
		}
		
		String serverHostname = Utils.getServerHostname();
		model.put("serverName", serverHostname);
		
		if (errors.hasFieldErrors()) {
			String language = "sp".equalsIgnoreCase(Utils.getRSALanguage(request)) ? "es" : Utils
					.getRSALanguage(request);
			Locale locale = new Locale(language);
			model.put("error_message", messageSource.getMessage(errors.getFieldError().getCode(), null, locale));
		}
		
		File file = new File(this.paths.getFilePath() + "fixedEnCampaign.vm");
		if (file != null && file.exists()) {
			model.put("fixedCampaign", "true");
		}
		file = new File(this.paths.getFilePath() + "divEnWantToDo.vm");
		if (file != null && file.exists()) {
			model.put("WantToDo", "true");
		}
		
		OOBChallengeBean bean = (OOBChallengeBean) command;
		OOBChallengeBean.ACTION action = bean.getAction();
		if (action != null)
			model.put("action", action.toString());
		else
			model.put("action", "");
		
		CustomerEntitlement rsaEnt = profile.getEntitlementByType(EntitlementType.RSA);
		
		boolean showHelp = false;
		if(userSession.getTasksListOOB()!= null && (userSession.getTasksListOOB().contains(ASK_FOR_OOB.ALERT_TYPES) 
				|| userSession.getTasksListOOB().contains(ASK_FOR_OOB.PERSONAL_INFORMATION))
				&& Collections.disjoint(userSession.getTasksListOOB(), alertTypeSubSection)) {
			model.put("isPopUp", true);
			if(userSession.getTasksListOOB().contains(ASK_FOR_OOB.ALERT_TYPES)){
				showHelp = true;
				model.put("showHelp", showHelp);
			}
		}
		
		if (userSession.getTasksList().contains(ASK_FOR.RECOVERY_CODE))
			page = 3;
		
		
		model.put("page", Integer.toString(page));
		model.put("username", userSession.getUsername());
		model.put("loginSuccess", userSession.isLoginCompleteSuccess());
		
		// Phone challenge
		if (page == 0 || page == 2) {
			
			// Get OOB challenge method
			String phone = null;
			String provider = null;
			String mobileSdkData = userSession.getMobileSdkData();
			
			final boolean isSDKMobileRequest = !GenericValidator.isBlankOrNull(mobileSdkData);
			DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), 
					mobileSdkData, request, isSDKMobileRequest);
			RsaChallengeRequest rsaChallengeRequest = new RsaChallengeRequest(userSession.getUsername(),deviceRequest);
			CredentialType credentialType = userSession.getCustomerService().rsaGetChallengeMethod(rsaChallengeRequest);
			userSession.setDeviceRequest(deviceRequest);
			if (credentialType == null) {
				return model;
			}
			
			if (credentialType.equals(CredentialType.OOBPHONE))
				model.put("challengeType", CustomerOOBChallengeType.OOBPHONE.toString());
			else 
				model.put("challengeType", CustomerOOBChallengeType.OOBSMS.toString());
			
			
			if (page == 0) {
				// Retrieve primary phone number (MB Text)
				if (profile.getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
					SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
					if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
							&& !smsprofile.getCtns().isEmpty()) {
						phone = ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn();
						provider = ((SMSCTN) smsprofile.getCtns().iterator().next()).getProvider();
					}
				} 
				String altPhone = rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE);
				if (altPhone != null && !altPhone.equalsIgnoreCase("N"))
					model.put(HAS_ALT_PHONE, true);
				else
					model.put(HAS_ALT_PHONE, false);
			} else {
				// Retrieve alternate phone number
				if (rsaEnt != null) {
					phone = rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE);
					provider = rsaEnt.getString(RSAEntitlement.ALTERNATEPROVIDER);
				}
			}
			
			if (phone == null) {
				if (credentialType.equals(CredentialType.OOBPHONE))
					errors.rejectValue("code", LOGIN_OOB_CALL_UNABLE);
				else
					errors.rejectValue("code", "login.oob.codeError.message");
				return model;
			} 
			model.put("phone", RSAUtils.setPhoneMask(phone));
			
			if (action != OOBChallengeBean.ACTION.VALIDATE_CALLCODE && action != OOBChallengeBean.ACTION.VALIDATE_SMSCODE) {
				bean.setCode(null);
			
				// SMS challenge
				if (credentialType.equals(CredentialType.USER_DEFINED)) {
					
					if (action == OOBChallengeBean.ACTION.SEND_SMSCODE) {
						if (this.generateCode(request, phone, provider, true, errors))
							model.put("codeSent", true);
						else
							model.put("codeSent", false);
						
					} else {
						// Don't make another challenge if language is changed
						String code = "";
						if (page == 0)
							 code = (String) WebUtils.getSessionAttribute(request, OOB_CODE);
						if(GenericValidator.isBlankOrNull(code))
							// Call RSA analyze for alternate number (page 2)
							this.generateCode(request, phone, provider, (page == 2 ? true : false), errors);
					}
					
				// Voice call challenge
				} else {
					if (action == OOBChallengeBean.ACTION.CALL_PHONE) {
						if (this.makeCall(request, phone, true, errors))
							model.put("callMade", true);
						else
							model.put("callMade", false);
					} else {
						// Call RSA analyze for alternate number (page 2)
						this.makeCall(request, phone, (page == 2 ? true : false), errors);
					}
					bean.setCode((String) WebUtils.getSessionAttribute(request, OOB_CODE));
				}
				if (redirectToBlock) {
					redirectToBlock = false;
					model.put("rsablocked", true);
				} else {
					model.put("rsablocked", false);
				}
			}
	
		// Other challenges
		} else if (page == 1) {
			
			String altPhone = rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE);
			if (altPhone != null && !altPhone.equalsIgnoreCase("N"))
				model.put(HAS_ALT_PHONE, true);
			else
				model.put(HAS_ALT_PHONE, false);
		}
		
		// Send device token for PMData cookie
		model.put("devicetoken", userSession.getDeviceRequest().getDeviceTokenCookie());
		model.put("rsa_cookie", userSession.getDeviceRequest().getDeviceTokenCookie());
		
		return model;
		
	}
	
	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors, int page)
			throws Exception {
		
		String userAgent = request.getHeader("user-agent");
		String customerDevices = "";
		if(userAgent != null) {			
			customerDevices = DeviceUtils.getCustomerDevice(userAgent).toString();
		}
				
		OOBChallengeBean bean = (OOBChallengeBean) command;
		bean.setDevice_info_rsa(request.getParameter("device_info_rsa"));
		bean.setTimeout(false);
		bean.setValidationError(false);
		
		if (redirectToBlock)
			return;

		OOBChallengeBean.ACTION action = bean.getAction();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSSESION);
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		
		if (action == OOBChallengeBean.ACTION.CANCEL_OOB) {
			finishCancel = true;
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, false);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ATHM_ACCT_MOD, null);
		}
		
		if (userSession.getTasksList().contains(ASK_FOR.RECOVERY_CODE)) {
			page = 3;
		}
		
		// Validate SMS & voice call challenges
		if ((page == 0 || page == 2) && 
				(action == OOBChallengeBean.ACTION.VALIDATE_SMSCODE || action == OOBChallengeBean.ACTION.VALIDATE_CALLCODE)) {
			
			ValidateOobChallengeRequest validateRequest = new ValidateOobChallengeRequest();
			validateRequest.setUsername(userSession.getUsername());
			validateRequest.setDeviceRequest(userSession.getDeviceRequest());
			validateRequest.setRsaSessionId(userSession.getRsaSessionId());
			validateRequest.setRsaTransactionId(userSession.getRsaTransactionId());
			String cookie = userSession.getDeviceRequest().getDeviceTokenCookie();
			if (!GenericValidator.isBlankOrNull(bean.getRsa_cookie())) {
				cookie = bean.getRsa_cookie();
			}
			validateRequest.setDeviceTokenCookie(cookie);
			validateRequest.setAnswer(bean.getCode());
			
			if (action == OOBChallengeBean.ACTION.VALIDATE_SMSCODE){
				validateRequest.setCredentialType(CredentialType.USER_DEFINED);
				validateRequest.setBindDevice(true);
			}else {
				validateRequest.setCredentialType(CredentialType.OOBPHONE);
				validateRequest.setBindDevice(false);
				validateRequest.setLanguage("EN".equalsIgnoreCase(Utils.getRSALanguage(request)) ? Lang.en : Lang.es);
			}
			
			ResultStatus statusResult = AdaptiveAuthFacade.getInstance().validateOobChallenge(validateRequest);
			userSession.getDeviceRequest().setDeviceTokenCookie(validateRequest.getDeviceTokenCookie());
			userSession.getDeviceRequest().setDeviceTokenFSO(validateRequest.getDeviceTokenCookie());
			if (statusResult != null) {
				
				if (!ResultStatus.RSA_ALLOW.equals(statusResult)) {	
					CustomerProfileService customerProfileService = customerService.getCustomerProfileService();
					if (ResultStatus.RSA_UNABLE.equals(statusResult)) {
						errors.rejectValue("code", "login.oob.codevalidation.error", "Unable to validate code at this time.");
						bean.setValidationError(true);
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754) && userSession.isOobLogin()) {
							customerProfileService.failSignon(customerProfile, CustomerInteraction.OBB_ERROR, customerDevices);
						}
					} else if (ResultStatus.RSA_EXPIRED.equals(statusResult)) {
						errors.rejectValue("code", "login.oob.codeexpired.error", "Code has expired.");
						bean.setTimeout(true);
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754) && userSession.isOobLogin()) {
							customerProfileService.failSignon(customerProfile, CustomerInteraction.OBB_ERROR, customerDevices);
						}
					} else if (ResultStatus.RSA_BLOCKED.equals(statusResult)) {
						redirectToBlock = true;
						log.error(RSA_USER + userSession.getUsername() + " blocked by OOB challenge tries");
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754) && userSession.isOobLogin()) {
							customerProfileService.failSignon(customerProfile, CustomerInteraction.RSA_BLOCKED, customerDevices);
						}
					} else if (ResultStatus.RSA_FAILED.equals(statusResult)) {
						if (action == OOBChallengeBean.ACTION.VALIDATE_SMSCODE) {
							errors.rejectValue("code", "sms.code.invalidRSA", "Incorrect code");
							if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754) && userSession.isOobLogin()) {
								customerProfileService.failSignon(customerProfile, CustomerInteraction.OBB_ERROR, customerDevices);
							}
						}
						else
							errors.rejectValue("code", "login.oob.call.error", "Input the code on your phone, before clicking continue.");
					} 
				}
			}
		
		// Validate recovery code
		} else if (page == 3 && action == OOBChallengeBean.ACTION.VALIDATE_RECCODE) {

			CustomerProfile profile = userSession.getCustomerProfile();

			if (profile.hasEntitlement(EntitlementType.RSA)) {

				CustomerEntitlement rsaEnt = profile.getEntitlementByType(EntitlementType.RSA);
				customerService = userSession.getCustomerService();

				OobRecoveryCodeStatusResponse recCodeResponse = customerService.getOOBRecoveryCodeStatus(profile,
						getRecCodeBlockLength());

				if (recCodeResponse != null && recCodeResponse.isBlocked())
					errors.rejectValue("recoveryCode", "login.oob.recCodeBlock.message",
							new Object[] { recCodeResponse.getRemainingMinutes() },
							"You have exceeded the maximum number of incorrect attempts.");

				else if (validateRecoveryCode(bean, rsaEnt)) {
					DeleteUserRSARequest deleteUserRSARequest = new DeleteUserRSARequest(
							userSession.getCustomerProfile().getUsername(), ApplicationsDefinition.CIBP);
					deleteUserRSARequest.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
					deleteUserRSARequest.setRemovedRecoveryCode(true);

					// Delete RSA user
					boolean rsaDeleted = customerService.deleteUserRSA(deleteUserRSARequest);

					// Reset RSA entitlement
					if (rsaDeleted) {
						customerService.getCustomerProfileService().removeEntitlement(profile, rsaEnt);
						RSAUtils.createRSAEntitlement(customerService, profile);

						// Increment Removal By Recovery Code count for Monthly
						// Report
						customerService.logCustomerUsageData(profile, CustomerActions.OOB_RECCODE_DEL);

						// Redirect to login if it's in session
						if (userSession.isLoginCompleteSuccess()) {
							redirectToLogin = true;

							// Otherwise add RSA enrollment to sign on flow
						} else {
							List<ASK_FOR> tasks = userSession.getTasksList();
							if (profile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)
									|| profile.hasFlag(CustomerFlagType.MUST_CHANGE_PASSWORD))
								tasks.add(tasks.indexOf(ASK_FOR.SESSION_INIT), ASK_FOR.RSA_ENROLL_USERNAME);
							else
								tasks.add(tasks.indexOf(ASK_FOR.SESSION_INIT), ASK_FOR.RSA_ENROLL);
						}

					} else {
						redirectToLogin = true;
						log.error(
								"Error deleting RSA user " + userSession.getUsername() + " after recovery code entry.");
					}

				} else {
					customerService.logFailOOBRecoveryCode(profile, getRecCodeMaxRetries(),
							getRecCodeRetryTimePeriod());
					errors.rejectValue("recoveryCode", "login.oob.recCodeError.message", "Incorrect recovery code");
					if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754) && userSession.isOobLogin()) {
						CustomerProfileService customerProfileService = customerService.getCustomerProfileService();
						customerProfileService.failSignon(customerProfile, CustomerInteraction.OBB_ERROR, customerDevices);
					}
				}

			} else {
				redirectToLogin = true;
				log.error("RSA Entitlement missing for user " + userSession.getUsername()
						+ " after recovery code entry.");
			}

		}
		
	}

	public boolean validateRecoveryCode(OOBChallengeBean bean, CustomerEntitlement rsaEnt) {
		
			String entCodeHash = rsaEnt.getString(RSAEntitlement.CODEHASH);
			String entCodeSalt = rsaEnt.getString(RSAEntitlement.CODESALT);
			
			if (!entCodeHash.equals("N") && !entCodeSalt.equals("N")){							
				String codeHashVerify = (PasswordHashUtil.generateSaltedHash(bean.getRecoveryCode(), Integer.parseInt(entCodeSalt)));
				return codeHashVerify.equals(entCodeHash);
				
			}else
				return false;
		
	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSSESION);
		final String mobileSdkData = userSession.getMobileSdkData();
		final boolean isSDKMobileRequest = !GenericValidator.isBlankOrNull(mobileSdkData);
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), 
				mobileSdkData, request, isSDKMobileRequest);
		Cookie cookie = new Cookie("PMData", deviceRequest.getDeviceTokenCookie());
		cookie.setSecure(true);
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		ESAPI.httpUtilities().addCookie(response, cookie);

		
//		if cancel, don't remove the oobCode
		if (finishCancel) {
			finishCancel = false;
			userSession.getTasksListOOB().remove(ASK_FOR_OOB.OOB_CHALLENGE);
			if (userSession.getTasksListOOB() != null && (userSession.getTasksListOOB().contains(ASK_FOR_OOB.MY_DOCUMENTS)||userSession.getTasksListOOB().contains(ASK_FOR_OOB.ATH_MOVIL)))
				return new ModelAndView("text", "text", "<script>"+NavigationSequenceUtils.getNextView(userSession.getTasksListOOB())+"</script>");
			else
				return (navigationCancel(userSession, request));
			
		}
		request.getSession().removeAttribute(OOB_CODE);
		
		final String rsa_cookie = UriUtils.encodeQueryParam(deviceRequest.getDeviceTokenCookie(), StandardCharsets.UTF_8.name());
		
		if (redirectToLogin) {
			redirectToLogin = false;
			return new ModelAndView("redirect:login" + "?rsa_cookie=" + rsa_cookie);
			
		} else if (redirectToBlock) {
			redirectToBlock = false;
			return new ModelAndView("redirect:login?username=" + userSession.getUsername() + "&rsa_cookie=" + rsa_cookie);
		}
		//If the user did the OOB challenge and it was success, set flag to true 
		userSession.setOobAuthenticated(true);
		log.info("USER OOB AUTHENTICATED:" + userSession.isOobAuthenticated());
		
		//Bypass navigation for apps in session since they manage it
		if (userSession.isLoginCompleteSuccess() && userSession.getUserAgentType() == UserAgentType.JSON) {
			Map<String, Object> model = new LinkedHashMap<>();
			model.put("page", 0);
			model.put("success", true);
			return new ModelAndView(LOGINOOB, model);
		}

        //Remove in case that navigation OOB 
		if (userSession.getTasksListOOB() != null && !userSession.getTasksListOOB().isEmpty() && userSession.getTasksListOOB().contains(ASK_FOR_OOB.OOB_CHALLENGE)) {
			userSession.getTasksListOOB().remove(ASK_FOR_OOB.OOB_CHALLENGE);
			if (userSession.getTasksListOOB().contains(ASK_FOR_OOB.ATH_MOVIL) || userSession.getTasksListOOB().contains(ASK_FOR_OOB.PERSONAL_INFORMATION)) { 
				return new ModelAndView("text", "text", "<script>parent.oobValidation();</script>");
			} else if (userSession.getTasksListOOB().contains(ASK_FOR_OOB.MY_DOCUMENTS)) {
				return new ModelAndView("text", "text", "<script>parent.challengeClose();</script>");
			} else if (!Collections.disjoint(userSession.getTasksListOOB(), alertTypeSubSection)) { //Verify if the petition comes from alert type subsection.
				userSession.setInformationChallenge(true);
				//Get the action that it could be go
				String action = getActionAlertTypeSubSection(userSession.getTasksListOOB());
				if (userSession.getTasksListOOB().contains(ASK_FOR_OOB.PUSH_DEVICE_REMOVAL) && 
						MobileAlert.DEVICE_MANAGEMENT_VIEW.equals(action)) {
					WebUtils.setSessionAttribute(request, MobileAlert.DEVICE_MANAGEMENT_AUTH, false);
				}
				//Remove the element in the navigation task
				WebUtils.setSessionAttribute(request, MobileAlert.DEVICE_MANAGEMENT_AUTH, false);

				userSession.getTasksListOOB().remove(getPopUpElementFromTaskList(userSession.getTasksListOOB()));
				return new ModelAndView("text", "text", "<script>parent.oobValidation('"+action+"');</script>");
			} else
				return new ModelAndView(NavigationSequenceUtils.getNextView(userSession.getTasksListOOB()));
		}
		
		if (userSession.getTasksList() != null && userSession.getTasksList().contains(ASK_FOR.OOB_CHALLENGE))
			userSession.getTasksList().remove(ASK_FOR.OOB_CHALLENGE);
		else
			userSession.getTasksList().remove(ASK_FOR.RECOVERY_CODE);
		
		return new ModelAndView(SignonSequenceUtils.getNextView(userSession));
	}
	
	private ModelAndView navigationCancel(UserSession userSession, HttpServletRequest request) {

		userSession.getTasksListOOB().remove(0);
		//Set the view that was called
		String view = NavigationSequenceUtils.getNextView(userSession.getTasksListOOB());
		boolean isDeviceManagement = MobileAlert.DEVICE_MANAGEMENT_VIEW.equals(view);
		if(!userSession.getTasksListOOB().isEmpty()) {
			userSession.getTasksListOOB().remove(0);
		}
		userSession.setOobChallenge(false);
		//Verify if the cancel call comes from alertType subsection.
		if(!Collections.disjoint(userSession.getTasksListOOB(), alertTypeSubSection) || isDeviceManagement) {
			//if the page should reload on BackOOB
			boolean isReload = true;
			if (isDeviceManagement) {
				setDeviceManagementOOB(userSession, true);
				sourceView = null;
				isReload = false;
			} 
			userSession.getTasksListOOB().removeAll(alertTypeSubSection);
			return new ModelAndView("text", "text", "<script>parent.gobackOOB(" + isReload + ");</script>");
		} else {
			return new ModelAndView(view);
		}
	}
		

	@Override
	protected boolean isFinishRequest(HttpServletRequest request) {
		
		if (redirectToLogin || redirectToBlock || finishCancel)
			return true;
		else
			return super.isFinishRequest(request);
	}
	
	private boolean generateCode(HttpServletRequest request, String phone, String provider, boolean newChallenge, Errors errors) {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSSESION);
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		String user = userSession.getUsername();
		String mobileSdkData = userSession.getMobileSdkData();
		
		final boolean isSDKMobileRequest = !GenericValidator.isBlankOrNull(mobileSdkData);
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), 
				mobileSdkData, request, isSDKMobileRequest);
	
		ChallengeOobRequest challengeOobRequest = new ChallengeOobRequest();
		challengeOobRequest.setCredentialType(CredentialType.USER_DEFINED);
		challengeOobRequest.setChallengephone(phone);
		challengeOobRequest.setDeviceRequest(deviceRequest);
		challengeOobRequest.setUsername(user);
		challengeOobRequest.setRsaSessionId(userSession.getRsaSessionId());
		challengeOobRequest.setRsaTransactionId(userSession.getRsaTransactionId());
		challengeOobRequest.setLanguage("EN".equalsIgnoreCase(Utils.getRSALanguage(request)) ? Lang.en : Lang.es);
		if (userSession.isLoginCompleteSuccess())
			challengeOobRequest.setNewChallenge(true);
		else
			challengeOobRequest.setNewChallenge(newChallenge);
		
		try {
			ChallengeOobResponse challengeOobResponse = customerService.getSignOnCustomerServiceModule().challengeOob(challengeOobRequest);
			
			deviceRequest.setDeviceTokenCookie(challengeOobResponse.getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(challengeOobResponse.getDeviceTokenCookie());
			userSession.setDeviceRequest(deviceRequest);
			userSession.setRsaSessionId(challengeOobResponse.getRsaSessionId());
			userSession.setRsaTransactionId(challengeOobResponse.getRsaTransactionId());
			
			if (ResultStatus.RSA_UNABLE.equals(challengeOobResponse.getResultStatus())) {
				errors.rejectValue("code", "sms.code.error", "Unable to send code at this time.");
				WebUtils.setSessionAttribute(request, OOB_CODE, null);
				return false;
				
			} else if (ResultStatus.RSA_BLOCKED.equals(challengeOobResponse.getResultStatus())) {
				redirectToBlock = true;
				WebUtils.setSessionAttribute(request, OOB_CODE, null);
				log.error(RSA_USER + userSession.getUsername() + " blocked by OOB SMS challenge");
				return false;
			}
				
			String code = challengeOobResponse.getOtp();
			
			SendSmsGeneratedCodeRequest codereq = new SendSmsGeneratedCodeRequest(
					Utils.generateCustomerInteractionEvent(request), customer, code, phone, provider);
			
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				customerService.sendGeneratedCode(codereq, PreferredLanguage.ENGLISH);
			else
				customerService.sendGeneratedCode(codereq, PreferredLanguage.SPANISH);
			
			WebUtils.setSessionAttribute(request, OOB_CODE, code);
			
			return true;
			
		} catch (Exception e) {
			errors.rejectValue("code", "sms.code.error", "Unable to send code at this time.");
			WebUtils.setSessionAttribute(request, OOB_CODE, null);
			return false;
		}
	}
	
	private boolean makeCall(HttpServletRequest request, String phone, boolean newChallenge, Errors errors) {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, USERSSESION);
		CustomerServices customerService = userSession.getCustomerService();
		String user = userSession.getUsername();
		final String mobileSdkData = userSession.getMobileSdkData();
		
		final boolean isSDKMobileRequest = !GenericValidator.isBlankOrNull(mobileSdkData);
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), 
				mobileSdkData, request, isSDKMobileRequest);
	
		ChallengeOobRequest challengeOobRequest = new ChallengeOobRequest();
		challengeOobRequest.setCredentialType(CredentialType.OOBPHONE);
		challengeOobRequest.setChallengephone(phone);
		challengeOobRequest.setDeviceRequest(deviceRequest);
		challengeOobRequest.setUsername(user);
		challengeOobRequest.setRsaSessionId(userSession.getRsaSessionId());
		challengeOobRequest.setRsaTransactionId(userSession.getRsaTransactionId());
		challengeOobRequest.setLanguage("EN".equalsIgnoreCase(Utils.getRSALanguage(request)) ? Lang.en : Lang.es);
		if (userSession.isLoginCompleteSuccess())
			challengeOobRequest.setNewChallenge(true);
		else
			challengeOobRequest.setNewChallenge(newChallenge);
		
		
		try {
			ChallengeOobResponse challengeOobResponse = customerService.getSignOnCustomerServiceModule().challengeOob(challengeOobRequest);
			
			deviceRequest.setDeviceTokenCookie(challengeOobResponse.getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(challengeOobResponse.getDeviceTokenCookie());
			userSession.setDeviceRequest(deviceRequest);
			userSession.setRsaSessionId(challengeOobResponse.getRsaSessionId());
			userSession.setRsaTransactionId(challengeOobResponse.getRsaTransactionId());
			
			if (ResultStatus.RSA_UNABLE.equals(challengeOobResponse.getResultStatus())) {
				redirectToLogin = true;
				errors.rejectValue("code", LOGIN_OOB_CALL_UNABLE, "Unable to generate call at this time.");
				WebUtils.setSessionAttribute(request, OOB_CODE, null);
				return false;
			} else if (ResultStatus.RSA_BLOCKED.equals(challengeOobResponse.getResultStatus())) {
				redirectToBlock = true;
				WebUtils.setSessionAttribute(request, OOB_CODE, null);
				log.error(RSA_USER + userSession.getUsername() + " blocked by OOB call challenge");
				return false;
			}
			
			WebUtils.setSessionAttribute(request, OOB_CODE, challengeOobResponse.getToken());
			return true;
			
		} catch (Exception e) {
			errors.rejectValue("code", LOGIN_OOB_CALL_UNABLE, "Unable to generate call at this time.");
			WebUtils.setSessionAttribute(request, OOB_CODE, null);
			return false;
		}
	}
	
	/**
	 * Method to get the action after oob challenge is completed on popups
	 * @param oobNav
	 * @return
	 */
	private String getActionAlertTypeSubSection(List<ASK_FOR_OOB> oobNav) {
		String action = "" ;
		if(oobNav == null) {
			action = "";
		} else {
			action = NavigationSequenceUtils.getAskView(getPopUpElementFromTaskList(oobNav));
		}
		return action;
	}
	
	/**
	 * Method for get the popUp from oob's navigation task list
	 * @param oobNav
	 * @return ASK_FOR_OOB Element or null if doesn't exists
	 */
	private ASK_FOR_OOB getPopUpElementFromTaskList(List<ASK_FOR_OOB> oobNav) {
		for (ASK_FOR_OOB ask_FOR_OOB : oobNav) {
			if(alertTypeSubSection.contains(ask_FOR_OOB))
				return ask_FOR_OOB;
		}
		return null;
	}
	
    //OOB Management for Device Management
	private void setDeviceManagementOOB (UserSession session, boolean isCancel) {
		List<ASK_FOR_OOB> listNavOOB = new ArrayList<>();
		if (isCancel) {
			listNavOOB.add(NumberUtils.INTEGER_ZERO, ASK_FOR_OOB.ALERT_TYPES);
			listNavOOB.add(NumberUtils.INTEGER_ZERO, ASK_FOR_OOB.EDIT_SMS);
			listNavOOB.add(NumberUtils.INTEGER_ZERO, ASK_FOR_OOB.OOB_CHALLENGE);
		} else {
			if (!session.isOobAuthenticated())
				listNavOOB.add(ASK_FOR_OOB.OOB_CHALLENGE);
			
			listNavOOB.add(ASK_FOR_OOB.ALERT_TYPES);
			listNavOOB.add(ASK_FOR_OOB.PUSH_DEVICE_REMOVAL);
		}
		session.setTasksListOOB(listNavOOB);
	}
	
	public CampaignPaths getPaths() {
		return paths;
	}

	public void setPaths(CampaignPaths paths) {
		this.paths = paths;
	}
	
	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public int getRecCodeBlockLength() {
		return recCodeBlockLength;
	}

	public void setRecCodeBlockLength(int recCodeBlockLength) {
		this.recCodeBlockLength = recCodeBlockLength;
	}

	public int getRecCodeMaxRetries() {
		return recCodeMaxRetries;
	}

	public void setRecCodeMaxRetries(int recCodeMaxRetries) {
		this.recCodeMaxRetries = recCodeMaxRetries;
	}

	public int getRecCodeRetryTimePeriod() {
		return recCodeRetryTimePeriod;
	}

	public void setRecCodeRetryTimePeriod(int recCodeRetryTimePeriod) {
		this.recCodeRetryTimePeriod = recCodeRetryTimePeriod;
	}

}
