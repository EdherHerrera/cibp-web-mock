package evertec.cibp.web.form;

import bppr.sms.beans.SMSAccount;
import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSEbppAccount;
import bppr.sms.beans.SMSProfile;
import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.EventType;
import edu.emory.mathcs.backport.java.util.Collections;
import evertec.cibp.core.dto.PushTokensDto;
import evertec.cibp.core.enums.PushTokenStatus;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerEmailBalanceAlert;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.EmailAlertCondition;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.SMSEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.AddSMSAccountRequest;
import evertec.cibp.core.services.requests.DeleteSMSProfileRequest;
import evertec.cibp.core.services.requests.ModifyCustomerAlertsRequest;
import evertec.cibp.core.services.requests.ModifyCustomerEmailRequest;
import evertec.cibp.core.services.requests.PushTokensRequest;
import evertec.cibp.core.services.requests.SendSmsGeneratedCodeRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.SmsAlertsResponse;
import evertec.cibp.core.services.responses.SmsAlertsResponse.Result;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.ValidationQuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.services.smsAlerts.SmsBean;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.enums.MobileResponseStatus;
import evertec.cibp.web.facade.AlertsFacade;
import evertec.cibp.web.facade.PushTokensFacade;
import evertec.cibp.web.form.bean.AlertTypesBean;
import evertec.cibp.web.form.bean.BalancesBean;
import evertec.cibp.web.form.bean.EditEmailBean;
import evertec.cibp.web.form.bean.PaymentsAndTransfersBean;
import evertec.cibp.web.form.bean.PersonalInformationChangesBean;
import evertec.cibp.web.form.bean.PurchasesAndWithdrawalsBean;
import evertec.cibp.web.form.mobile.bean.MobileAlert;
import evertec.cibp.web.response.PushTokensResponse;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.AlertTypesUtils;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.CookieUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.CancellableFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * @author jocampo
 * 
 */
public class AlertTypesForm extends CancellableFormController implements MessageSourceAware {

	private MessageSource messageSource;
	
	private static final Logger logger = Logger.getLogger(AlertTypesForm.class);

	// jocampo FFEIC
	private Integer maxPasswordRecords;

	// FIN FFEIC
	private String alertsAccountTypes;

	public AlertTypesForm() {
		super();
		setCommandClass(AlertTypesBean.class);
		setCommandName("alert");
		setFormView("alerttypes");
		this.setCancelView(StringUtils.EMPTY);
		this.alertsAccountTypes = null;
	}

	/**
	 * Constructor parameter.
	 * @param alertsAccountTypes account types
	 */
	public AlertTypesForm(String cancelView, String alertsAccountTypes){
		this();
		this.setCancelView(cancelView);
		this.alertsAccountTypes = alertsAccountTypes;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		Map<String, Object> model = new LinkedHashMap<>(); //model result
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		boolean informationChallenge = session.isInformationChallenge(); //information challenge

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, session != null && session.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		if (!informationChallenge) {

			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);

			CustomerProfile profile = session.getCustomerProfile();

			boolean dummyQuestions = (profile == null);

			String answer = request.getParameter("answer");

			if (GenericValidator.isBlankOrNull(answer)) {
				model.put("text", "empty-");
			}

			else {

				// PersonalInformationBean personal = (PersonalInformationBean)
				// command;

				// String answer = personal.getAnswer();

				RSAActionRequest rsaActionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(
						request, session.getUsername()), session.getUsername(), answer, dummyQuestions ? null
								: profile.getEmail(), 0, session.isRemember(), session.getDeviceRequest());
				String username = session.getUsername();
				if (session.getCustomerProfile() != null)
					rsaActionRequest.setUsername(session.getCustomerProfile().getUsername());
				else if (session.getUsernameReservation() != null)
					rsaActionRequest.setUsername(session.getUsernameReservation().getUsername());
				else
					rsaActionRequest.setUsername(username);

				rsaActionRequest.setRsaSessionId(session.getRsaSessionId());
				rsaActionRequest.setRsaTransactionId(session.getRsaTransactionId());
				rsaActionRequest.setQuestion(session.getInformationQuestion());
				rsaActionRequest.setRsaLang(null);
				ValidationQuestionResponse responseAnswer = session.getCustomerService().getSignOnCustomerServiceModule().validateRSAQuestion(
						rsaActionRequest);
				session.setQuestion(null);
				session.setRsaSessionId(responseAnswer.getRsaSessionId());
				session.setRsaTransactionId(responseAnswer.getRsaTransactionId());

				Cookie cookie = WebUtils.getCookie(request, "PMData");
				if (cookie == null)
					cookie = new Cookie("PMData", "");

				cookie.setPath("/");
				cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
				ESAPI.httpUtilities().addCookie(response, cookie);

				if (ResultStatus.SUCCESS.equals(responseAnswer.getStatusResult())) {
					cookie.setPath("/");
					cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
					ESAPI.httpUtilities().addCookie(response, cookie);
					excecuteAction(request, response, model);
					session.setInformationChallenge(true);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
					model.put("text", "success-");
				} else if (ResultStatus.BLOCKED.equals(responseAnswer.getStatusResult())) {
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
					/* CIBP-1104 */
					boolean hasalert = false;
					AlertsEntitlement ent = new AlertsEntitlement(session.getCustomerProfile().getEntitlementByType(
							EntitlementType.ALERTS));
					if (ent != null && ent.getAlerts() != null) {
						Set<AlertType> alerts = ent.getAlerts();
						if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
								|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
							hasalert = true;
						}
					}
					/*
					 * CIBP-1104 se activa envio alerta de bloqueo de preguntas
					 * secretas
					 */
					if (hasalert) {
						session.getCustomerService().sendQuestionsBlockedConfirmation(session.getCustomerProfile(),
								session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
								session.getPreferredLanguage());
					}
					/* FIN--- CIBP-1104 */

					// errors.rejectValue("answer", "account.block.message",
					// "Account Blocked");
					CookieUtils.addCookie(response, 60 * 60 * 24 * 365, "username", username);

					model.put("text", "blocked-");
					// return new ModelAndView("forward:login", model);
					// return showForm(request, response, errors);
				} else if (ResultStatus.EXCEPTION.equals(responseAnswer.getStatusResult())) {
					cookie.setPath("/");
					cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
					ESAPI.httpUtilities().addCookie(response, cookie);
					model.put("text", "exception-");
					// return new ModelAndView("forward:login", model);
				} else if (ResultStatus.ERROR.equals(responseAnswer.getStatusResult())) {
					errors.rejectValue("answer", "login.question.error.message", "Invalid answer");
					cookie.setPath("/");
					cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
					ESAPI.httpUtilities().addCookie(response, cookie);

					String email = null;
					if (session.getCustomerProfile() != null)
						email = session.getCustomerProfile().getEmail();

					QuestionResponse personalQuestion;

					RSAActionRequest actionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(
							request, session.getUsername()), session.getUsername(), "", email, 0, session.isRemember(),
							session.getDeviceRequest());

					if (session.getCustomerProfile() != null)
						actionRequest.setUsername(session.getCustomerProfile().getUsername());
					else if (session.getUsernameReservation() != null)
						actionRequest.setUsername(session.getUsernameReservation().getUsername());
					else
						actionRequest.setUsername(username);

					// session.setRsaSessionId(anResp.getIdentificationData().getSessionId());
					// session.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

					actionRequest.setDeviceRequest(session.getDeviceRequest());
					actionRequest.setRsaSessionId(session.getRsaSessionId());
					actionRequest.setRsaTransactionId(session.getRsaTransactionId());

					actionRequest.setRsaLang(Utils.getRSALanguage(request));

					personalQuestion = session.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

					if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
						String question = (String) personalQuestion.getQuestion().values().toArray()[0];
						session.setInformationQuestion(personalQuestion.getQuestion());
						model.put("text", "error-" + question);

					} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
						// throw new ModelAndViewDefiningException(new
						// ModelAndView("error"));
						model.put("text", "exception-");
					} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
						WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
						/* CIBP-1104 */
						boolean hasalert = false;
						AlertsEntitlement ent = new AlertsEntitlement(session.getCustomerProfile()
								.getEntitlementByType(EntitlementType.ALERTS));
						if (ent != null && ent.getAlerts() != null) {
							Set<AlertType> alerts = ent.getAlerts();
							if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
									|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
								hasalert = true;
							}
						}
						/*
						 * CIBP-1104 se activa envio alerta de bloqueo de
						 * preguntas secretas
						 */
						if (hasalert) {
							session.getCustomerService().sendQuestionsBlockedConfirmation(session.getCustomerProfile(),
									session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
									session.getPreferredLanguage());
						}
						/* FIN--- CIBP-1104 */

						// errors.rejectValue("answer", "account.block.message",
						// "Account Blocked");
						CookieUtils.addCookie(response, 60 * 60 * 24 * 365, "username", username);

						model.put("text", "blocked-");
					}
				}
			}
			return new ModelAndView("text", model);
		}
		return new ModelAndView("redirect:alerttypes");
	}

	protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<ASK_FOR_OOB> listNavOOB = new ArrayList<>();
		userSession.setOobChallenge(false);

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, session != null && session.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99
		
		boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);
		boolean ccaALertsFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.CCA_ALERTS);
		model.put("ccaALertsFlag", ccaALertsFlag);
		model.put("ACH_PAYMENT_NT", StaticMessageSource.isFlagEnabled(StaticMessageSource.ACH_PAYMENT_NT));
		WebUtils.setSessionAttribute(request, MobileAlert.DEVICE_MANAGEMENT_AUTH, false);
		/**
		 * oob flag and oob enroll verification 
		 * */
		String iframeSMS = "sms";
		if (oobFlag && userSession.getOobEnroll()) {
			//verify if the form comes from Personal Information, clean all flags used by email
			if (userSession.getTasksListOOB() != null && userSession.getTasksListOOB().contains(ASK_FOR_OOB.PERSONAL_INFORMATION)) {
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
				
			}
			// add the navigation to CHANGE_OOB in cancel case
			listNavOOB.add(0, ASK_FOR_OOB.ALERT_TYPES);
			
			// add the authenticated oob to the view vm
			if (!userSession.isOobAuthenticated()) {
				// select the next step after OOB Challenge
				listNavOOB.add(NumberUtils.INTEGER_ZERO, ASK_FOR_OOB.EDIT_SMS);

				//ask for OOB in Push Device Removal
				model.put("showOOB", true);
				WebUtils.setSessionAttribute(request, MobileAlert.DEVICE_MANAGEMENT_AUTH, true);
				
				// add the challenge OOB steep navigation is optional
				listNavOOB.add(NumberUtils.INTEGER_ZERO, ASK_FOR_OOB.OOB_CHALLENGE);
				iframeSMS = "loginoob";
			}
		}
		
		model.put("iframeSMS", iframeSMS);
			
		userSession.setTasksListOOB(listNavOOB);
		WebUtils.setSessionAttribute(request, "firstTimePopUp", true);
		WebUtils.setSessionAttribute(request, "userSession", userSession);
		
		Boolean deleteMobile = (Boolean) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.MOBILE_DELETE);
		Boolean showBalance = (Boolean) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.BALANCE_FRAME);
		Boolean showPersonal = (Boolean) WebUtils.getSessionAttribute(request,
				ChallengeInformationUtils.PERSONAL_INFORMATION_CHANGES_FRAME);
		Boolean showPaymentsTransfers = (Boolean) WebUtils.getSessionAttribute(request,
				ChallengeInformationUtils.PAYMENTS_TRANSFERS_FRAME);
		Boolean showMobile = (Boolean) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.MOBILE_FRAME);
		Boolean showATH = (Boolean) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.ATH_FRAME);
		Boolean showCCA = (Boolean) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.CCA_FRAME);
		Boolean showServices = (Boolean) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.SERVICES_FRAME);
		
		model.put("balanceFrame", BooleanUtils.isTrue(showBalance));
		model.put("personalFrame", BooleanUtils.isTrue(showPersonal));
		model.put("paymentsTranfersFrame", BooleanUtils.isTrue(showPaymentsTransfers));
		model.put("deleteMobile", BooleanUtils.isTrue(deleteMobile));
		model.put("mobileFrame", BooleanUtils.isTrue(showMobile));
		model.put("showATH", BooleanUtils.isTrue(showATH));
		model.put("showServices", BooleanUtils.isTrue(showServices));
		if (ccaALertsFlag)
			model.put("showCCA", BooleanUtils.isTrue(showCCA));
		
		// CIBP-2804
		if (session.getGuidedSetupSession() != null && session.getGuidedSetupSession().isGuidedSetupMode()) {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, true);
		} else {
			model.put(ChallengeInformationUtils.INFORMATION_CHALLENGE, session.isInformationChallenge());
		}
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.PERSONAL_INFORMATION_CHANGES_FRAME, null);
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.PAYMENTS_TRANSFERS_FRAME, null);

		String alerts = request.getParameter("showAlerts");	
		model.put("showAlerts", !GenericValidator.isBlankOrNull(alerts));
		model.put("customer", userSession.getCustomerProfile());

		if (userSession.getCustomerProfile() != null) {
			if (userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
				SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
				if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
						&& !smsprofile.getCtns().isEmpty()) {
					model.put("EnrollSMS", true);
					model.put("smsPhone", ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn());
				} else {
					model.put("EnrollSMS", false);
				}
			} else {
				model.put("EnrollSMS", false);
			}
		}
		
		if (Utils.isPushEnabled()) {
			
			PushTokensRequest pushReq = new PushTokensRequest(userSession.getCustomerProfile().getProfileId(), PushTokenStatus.ACTIVE);
			PushTokensResponse pushResp = PushTokensFacade.getInstance().getTokens(pushReq, userSession);
			
			if (pushResp != null) {
				if (MobileResponseStatus.SUCCESS.equals(pushResp.getStatus())) {
					List<PushTokensDto> dtos = pushResp.getData();
					if (CollectionUtils.isNotEmpty(dtos)) {
						//Get RSA Question if user has devices
						if (oobFlag && !userSession.getOobEnroll() 
								&& !userSession.isInformationChallenge()) {
							if (userSession.getInformationQuestion() == null) {
								getRSAChallenge(request, userSession, MobileAlert.DEVICE_MANAGEMENT_AUTH);
							}
							if (session.getInformationQuestion() != null) {
								WebUtils.setSessionAttribute(request, MobileAlert.DEVICE_MANAGEMENT_AUTH, true);
								model.put("RSAQuestion", (String) session.getInformationQuestion().values().toArray()[0]);
							}
						}
						Collections.sort(dtos);
						WebUtils.setSessionAttribute(request, MobileAlert.PUSH_DEVICES, dtos);
						model.put("pushDevices", dtos);
					}
				} else if (MobileResponseStatus.ERROR.equals(pushResp.getStatus()))  {
					for (com.evertec.cibp.api.client.model.Error error : pushResp.getErrors()) {
						if (StringUtils.equals(error.getCode(), String.valueOf(HttpServletResponse.SC_NOT_FOUND))) {
							logger.error("Error getting push devices: " + error.getTitle());
						}
					}
				}
			} else {
				logger.error("There was an error getting push devices");
			}
		}
			
		int accountTotal = 0;
		List<TVFrontendAccount> accts = userSession.getAccounts();
		Map<String, TVFrontendAccount> filteraccounts = new LinkedHashMap<>();
		for (Iterator<TVFrontendAccount> iterator = accts.iterator(); iterator.hasNext();) {
			TVFrontendAccount frontendAccount = iterator.next();
			if (filteraccounts.get(frontendAccount.getAccountNumber()) == null)
				accountTotal++;
		}

		model.put("account_total", accountTotal);

		if (session.getInformationQuestion() != null) {
			String question = (String) session.getInformationQuestion().values().toArray()[0];
			model.put("question", question);
		}
		
		model.put("hasOnOffEligibleAccount", StaticMessageSource.isFlagEnabled(StaticMessageSource.ON_OFF_ALERTS) 
				&& session.isHasOnOffEligibleAccount());
		

		return model;

	}
	
	@SuppressWarnings("unchecked")
	private void excecuteAction(HttpServletRequest request, HttpServletResponse respone, Map<String, Object> model)
			throws Exception {
		String action = (String) WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION);
		Map<String, Object> parameters = (Map<String, Object>) WebUtils.getSessionAttribute(request,
				ChallengeInformationUtils.INFORMATION_PARAMETERS);
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile customer = userSession.getCustomerProfile();
		
		
		if (action != null) {
			
			switch (action) {
			
			case ChallengeInformationUtils.BALANCE_ACTION:
				BalancesBean dude = (BalancesBean) parameters.get(ChallengeInformationUtils.BALANCE_ACTION);
				updateBalance(request, customerService, model, dude, userSession, customer);
				break;
			
			case ChallengeInformationUtils.EMAIL_ACTION:
				updateEmail(request, model, parameters);
				break;
				
			case ChallengeInformationUtils.PERSONAL_INFORMATION_CHANGES_ACTION:
				updatePersonalInformationChanges(request, model, parameters);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.PERSONAL_INFORMATION_CHANGES_FRAME, true);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.SHOW_UPDATE_ALERT, true);
				break;
			
			case ChallengeInformationUtils.PAYMENTS_TRANSFERS_ACTION:
				updatePaymentsTranfers(request, model, parameters);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.PAYMENTS_TRANSFERS_FRAME, true);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.SHOW_UPDATE_ALERT, true);
				break;
				
			case ChallengeInformationUtils.MOBILE_FRAME:
				smsEnrollmentCode(request, model, parameters, userSession);
				break;
			
			case ChallengeInformationUtils.MOBILE_DELETE:
				model.put("smsDeleted", true);
				deleteSmsProfile(request, userSession, model);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.MOBILE_DELETE, null);
				break;
				
			case ChallengeInformationUtils.ATH_ACTION:
				this.updateATHPurchasesAndWithdrawals(request, model, parameters);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ATH_FRAME, true);
				break;
						
			default:
				break;
			}
		}
		//Set authentication needed for device management as false, since the user completed the challenge
		WebUtils.setSessionAttribute(request, MobileAlert.DEVICE_MANAGEMENT_AUTH, false);

	}
	
	private void getRSAChallenge(HttpServletRequest request, UserSession userSession, String viewSource) throws Exception {
		
		String language = Utils.getRSALanguage(request);
		String username = userSession.getUsername();

		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), request);
		
		/**
		 * MBNA-1955
		 * Method rsaAnalyze : CIBP-WEB microservice integration
		 * 
		 */
		RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
		AnalyzeResponse anResp = userSession.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);
		
		if (HttpServletResponse.SC_CONFLICT != anResp.getStatusHeader().getStatusCode()) {
			deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
			userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
			userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());
			userSession.setDeviceRequest(deviceRequest);
		} else {
			throw new ModelAndViewDefiningException(new ModelAndView("error"));
		}
		
		if (ActionCode.CHALLENGE != anResp.getRiskResult().getTriggeredRule().getActionCode()) {
			userSession.setInformationChallenge(!StringUtils.equals(MobileAlert.DEVICE_MANAGEMENT_AUTH, viewSource));
		} else {
			String email = null;
			if (userSession.getCustomerProfile() != null)
				email = userSession.getCustomerProfile().getEmail();

			QuestionResponse personalQuestion;

			if (userSession.getInformationQuestion() == null) {
				RSAActionRequest actionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(request,
						userSession.getUsername()), userSession.getUsername(), "", email, 0, userSession.isRemember(),
						userSession.getDeviceRequest());

				if (userSession.getCustomerProfile() != null)
					actionRequest.setUsername(userSession.getCustomerProfile().getUsername());
				else if (userSession.getUsernameReservation() != null)
					actionRequest.setUsername(userSession.getUsernameReservation().getUsername());
				else
					actionRequest.setUsername(username);

				actionRequest.setDeviceRequest(userSession.getDeviceRequest());
				actionRequest.setRsaSessionId(userSession.getRsaSessionId());
				actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());
				actionRequest.setRsaLang(Utils.getRSALanguage(request));

				personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

			} else {
				personalQuestion = new QuestionResponse();
				personalQuestion.setStatusResult(ResultStatus.SUCCESS);
				personalQuestion.setQuestion(userSession.getInformationQuestion());
			}
			
			if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
				userSession.setInformationQuestion(personalQuestion.getQuestion());
			} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
				throw new ModelAndViewDefiningException(new ModelAndView("error"));
			} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
				throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
			}
		}

	}

	/**
	 * Metodo getEBPPMessageSourceStatus Metodo que pregunta por el status de
	 * EBPP en el archivo MessageSource
	 * 
	 * @return true si el status es ENABLED, false de lo contrario mpicado
	 */
	// CIBP-2089 bgarcia
	private boolean getEBPPMessageSourceStatus() {
		String status = this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase();
		if (status.equals(EntitlementStatus.ENABLED.toString()))
			return true;
		else
			return false;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@SuppressWarnings("unused")
	private void updateBalance(HttpServletRequest request, CustomerServices customerService, Map<String, Object> model,
			Object dude, UserSession userSession, CustomerProfile customer) {
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.SHOW_UPDATE_ALERT, true);
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.BALANCE_FRAME, true);
		AlertsEntitlement entitlement = null;
		if (!customer.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement alert = new AlertsEntitlement();
			customerService.getCustomerProfileService().addEntitlement(customer, alert.getCustomerEntitlement());
		}

		entitlement = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));

		Set<AlertType> customeralerts = new HashSet<AlertType>();

		if (entitlement != null && entitlement.getAlerts() != null)
			customeralerts = entitlement.getAlerts();

		ModifyCustomerAlertsRequest req = new ModifyCustomerAlertsRequest();
		req.setCustomerProfile(userSession.getCustomerProfile());
		req.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));

		BalancesBean a = (BalancesBean) dude;
		String emailalertgt = a.isEmailalertgt() ? "true" : "false";
		String smsalertgt = a.isSmsalertgt() ? "true" : "false";
		String emailalertlt = a.isEmailalertlt() ? "true" : "false";
		String smsalertlt = a.isSmsalertlt() ? "true" : "false";
		// String emailalertnewst = a.isEmailalertnewst() ?"true":"false";
		// String smsalertnewst = a.isSmsalertnewst() ?"true":"false";
		// String emailalertnewstmod = a.isEmailalertnewstmod() ?"true":"false";
		// String smsalertnewstmod = a.isSmsalertnewstmod() ?"true":"false";
		String amountgt = a.getAmountgt() == null ? "" : a.getAmountgt();
		String amountlt = a.getAmountlt() == null ? "" : a.getAmountlt();

		model.put(ChallengeInformationUtils.BALANCE_ACTION, a);
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.BALANCE_MODEL, model);

		CustomerInteractionEvent customerInteractionEvent = Utils.generateCustomerInteractionEvent(request);
		try {
			// retrives account list that user have with any alert setted
			List<SMSEbppAccount> smsebppaccounts = userSession.getSmsEbppaccounts(customerInteractionEvent);
			List<CustomerEmailBalanceAlert> emailaccounts = userSession.getEmailaccounts(customerInteractionEvent);
			List<SMSAccount> smsaccounts = userSession.getSmsaccounts(customerInteractionEvent);
			CustomerEmailBalanceAlert beanAlert = new CustomerEmailBalanceAlert();
			TVFrontendAccount frontact = null;

			if (a.getAccount() != null && !"none".equalsIgnoreCase(a.getAccount()))
				frontact = Utils.getAccountById(a.getAccount(), userSession.getAccounts());

			if (frontact == null) {
				model.put("text", false);
				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.SHOW_UPDATE_ALERT, false);
			} else {
				// Sets common data
				beanAlert.setAccountNumber(frontact.getAccountNumber());
				beanAlert.setAccountProductId(frontact.getProductDefinition().getProductId());
				beanAlert.setAccountSection(frontact.getAccountSection());
				beanAlert.setAccountSubtype(frontact.getSubtype());
				beanAlert.setCustomerProfile(userSession.getCustomerProfile());
				beanAlert.setNickname(frontact.getNickname());
				beanAlert.setPermId(userSession.getCustomerProfile().getPermId());
				beanAlert.setAlertposicion("X");
				String language = "";
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					language = "I";
				else
					language = "E";

				beanAlert.setAlertlanguage(language);

				SMSEntitlement smsent = null;
				if (a.isSearching()) { // if selects any account from dropdown
					// list,
					// search for all existing email alerts
					if (customeralerts.contains(AlertType.BALANCE_EMAIL)) {
						for (Iterator<CustomerEmailBalanceAlert> iterator = emailaccounts.iterator(); iterator
								.hasNext();) {
							CustomerEmailBalanceAlert account2 = (CustomerEmailBalanceAlert) iterator.next();
							if (account2.getAccountNumber().equals(frontact.getAccountNumber())
									&& account2.getAccountSection().equalsIgnoreCase(frontact.getAccountSection())
									&& account2.getAccountSubtype().equalsIgnoreCase(
											frontact.getProductDefinition().getProductType())
											&& account2.getAccountProductId().equalsIgnoreCase(
													frontact.getProductDefinition().getProductId())) { // Search
								// for
								// email
								// alerts
								if (account2.getAlertCondition().getOperator().equals("GT")) {
									amountgt = account2.getAlertCondition().getAmountHigh().toString();
									emailalertgt = "true";
									a.setEmailalertgt(true);
								} else if (account2.getAlertCondition().getOperator().equals("LT")) {
									amountlt = account2.getAlertCondition().getAmountLow().toString();
									emailalertlt = "true";
									a.setEmailalertlt(true);
								}
							}
						}
					}
					int iAmountgt;
					int iAmountlt;
					try {
						iAmountgt = Integer.parseInt(amountgt);
					} catch (NumberFormatException nfEx) {
						iAmountgt = 0;
					} catch (Exception Ex) {
						iAmountgt = 0;
					}
					try {
						iAmountlt = Integer.parseInt(amountlt);
					} catch (NumberFormatException nfEx) {
						iAmountlt = 0;
					} catch (Exception Ex) {
						iAmountlt = 0;
					}
					if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)) {
						for (Iterator<SMSAccount> iterator = smsaccounts.iterator(); iterator.hasNext();) {
							SMSAccount account2 = (SMSAccount) iterator.next();
							if (account2.getAccountNumber().equals(frontact.getAccountNumber())
									&& account2.getAccountSection().equalsIgnoreCase(frontact.getAccountSection())
									&& account2.getAccountSubtype().equalsIgnoreCase(
											frontact.getProductDefinition().getProductType())
											&& account2.getAccountProductId().equalsIgnoreCase(
													frontact.getProductDefinition().getProductId())) { // Search
								// for
								// email
								// alerts
								if(account2.getAlertCondition()!= null 
										&& account2.getAlertCondition().getOperator() != null){ //IM 2080104
									if (account2.getAlertCondition().getOperator().equals("GT")) {
										if (iAmountgt == 0)
											amountgt = account2.getAlertCondition().getAmountHigh().toString();
										smsalertgt = "true";
										a.setSmsalertgt(true);
									} else if (account2.getAlertCondition().getOperator().equals("LT")) {
										if (iAmountlt == 0)
											amountlt = account2.getAlertCondition().getAmountLow().toString();
										smsalertlt = "true";
										a.setSmsalertlt(true);
									}
								}								
							}
						}
						// for(Iterator<SMSEbppAccount> iterator =
						// smsebppaccounts.iterator(); iterator.hasNext();) {
						// SMSEbppAccount account2 = (SMSEbppAccount)
						// iterator.next();
						// if(account2.getAccountNumber().equals(frontact.getAccountNumber())){
						// smsalertnewst = "true";
						// smsalertnewstmod = "true";
						// a.setSmsalertgt(true);
						// }
						// }

					}
					model.put("account", frontact);
				} else {
					// if(a.isEmailalertgt()||a.isEmailalertlt()||a.isEmailalertnewst()){
					// // if any email alert is checked then
					// if(!customeralerts.contains(AlertType.BALANCE_EMAIL)){ //
					// verify if have "balance email" alert type
					// customeralerts.add (AlertType.BALANCE_EMAIL);
					// req.setAlerts (customeralerts);
					// profile = customerService.modifyCustomerAlerts(req);
					// userSession.setCustomerProfile(profile);
					// }
					// }

					if (a.isEmailalertgt() || a.isEmailalertlt()) { // if any
						// email
						// alert is
						// checked
						// then
						if (!customeralerts.contains(AlertType.BALANCE_EMAIL)) { // verify
							// if
							// have
							// "balance email"
							// alert
							// type
							customeralerts.add(AlertType.BALANCE_EMAIL);
							req.setAlerts(customeralerts);
							customer = customerService.modifyCustomerAlerts(req);
							userSession.setCustomerProfile(customer);
						}
					}

					// if(!a.isEmailalertgt()||!a.isEmailalertlt()||!a.isEmailalertnewst()){
					// //if any email alert is unchecked
					// if(!a.isEmailalertgt()&&a.isEmailalertgtmod()){ // if
					// alert
					// were exists, delete
					// beanAlert.setId(findEmailAlert(emailaccounts,
					// frontact.getAccountNumber(),"GT"));
					// boolean isAlertDeleted =
					// customerService.DeleteCustomerEmailBalanceAlert(beanAlert);
					// if(isAlertDeleted){
					// emailalertgt = "false";
					// a.setEmailalertgt(false);
					// }
					// }
					// if(!a.isEmailalertlt()&&a.isEmailalertltmod()){ // if
					// alert
					// were exists, delete
					// beanAlert.setId(findEmailAlert(emailaccounts,
					// frontact.getAccountNumber(),"LT"));
					// boolean isAlertDeleted =
					// customerService.DeleteCustomerEmailBalanceAlert(beanAlert);
					// if(isAlertDeleted){
					// emailalertlt = "false";
					// a.setEmailalertlt(false);
					// }
					// }
					// if(!a.isEmailalertnewst()&&a.isEmailalertnewstmod()){/**
					// Aqui
					// poner para la opcion "when a new e-Statment is received
					// **/
					// emailalertnewstmod = "false";
					// }
					// }

					if (!a.isEmailalertgt() || !a.isEmailalertlt()) { // if any
						// email
						// alert
						// is
						// unchecked
						if (!a.isEmailalertgt()) { // if alert were exists,
							// delete
							String alertId = findEmailAlert(emailaccounts, frontact.getAccountNumber(),
									frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
									frontact.getProductDefinition().getProductId(), "GT");
							boolean isAlertDeleted = false;
							if (!alertId.equals("")) {
								beanAlert.setId(alertId);
								/**
								 * MBNA-3493
								 * Method DeleteCustomerEmailBalanceAlert :CIBP-WEB microservice integration
								 * 
								 * */
								isAlertDeleted = AlertsFacade.getInstance().deleteCustomerEmailBalanceAlert(beanAlert);
							}
							if (isAlertDeleted) {
								emailalertgt = "false";
								a.setEmailalertgt(false);
								a.setEmailalertgtmod(false);
							}
						}
						if (!a.isEmailalertlt()) { // if alert were exists,
							// delete
							String alertId = findEmailAlert(emailaccounts, frontact.getAccountNumber(),
									frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
									frontact.getProductDefinition().getProductId(), "LT");
							boolean isAlertDeleted = false;
							if (!alertId.equals("")) {
								beanAlert.setId(alertId);
								/**
								 * MBNA-3493
								 * Method DeleteCustomerEmailBalanceAlert :CIBP-WEB microservice integration
								 * 
								 * */
								isAlertDeleted = AlertsFacade.getInstance().deleteCustomerEmailBalanceAlert(beanAlert);
							}
							if (isAlertDeleted) {
								emailalertlt = "false";
								a.setEmailalertlt(false);
								a.setEmailalertltmod(false);
							}
						}
					}

					// if any email alert is checked, firts time or any amount
					// is
					// modified
					if (a.isEmailalertgt()) { // if email checkmark is
						// checked...
						// ALERT_CONDITION_OPERATOR = GT
						EmailAlertCondition alertCond = new EmailAlertCondition();
						alertCond.setAmountHigh(new BigDecimal(a.getAmountgt()));
						alertCond.setOperator("GT");
						beanAlert.setAlertCondition(alertCond);
						/** Insert, Update or Delete **/
						String alertId = findEmailAlert(emailaccounts, frontact.getAccountNumber(),
								frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
								frontact.getProductDefinition().getProductId(), "GT");
						if (!alertId.equals(""))
							beanAlert.setId(alertId);
						if (!alertId.equals("")) {
							/**
							 * MBNA-3502
							 * Method UpdateCustomerEmailBalanceAlert : CIBP-WEB microservice integration
							 * 
							 */
							//Microservice implementation
							CustomerEmailBalanceAlert beanAlertConfirmation = AlertsFacade.getInstance().updateCustomerEmailBalanceAlert(beanAlert, userSession);
							emailalertgt = "true";
							a.setEmailalertgt(true);
							a.setEmailalertgtmod(true);
						} else if (alertId.equals("")) { // if alert not exists
							// then
							// insert
							
							/**
							 * MBNA-3499
							 * Method InsertCustomerEmailBalanceAlert : CIBP-WEB microservice integration
							 * 
							 */
							//Microservice implementation
							CustomerEmailBalanceAlert beanAlertConfirmation = AlertsFacade.getInstance().insertCustomerEmailBalanceAlert(beanAlert, userSession);

							emailalertgt = "true";
						}
					}
					if (a.isEmailalertlt()) { // if email checkmark is
						// checked...
						// ALERT_CONDITION_OPERATOR = LT
						EmailAlertCondition alertCond = new EmailAlertCondition();
						alertCond.setAmountLow(new BigDecimal(a.getAmountlt()));
						alertCond.setOperator("LT");
						beanAlert.setAlertCondition(alertCond);
						/** Insert, Update or Delete **/
						String alertId = findEmailAlert(emailaccounts, frontact.getAccountNumber(),
								frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
								frontact.getProductDefinition().getProductId(), "LT");
						if (!alertId.equals(""))
							beanAlert.setId(alertId);
						if (!alertId.equals("")) {
							/**
							 * MBNA-3502
							 * Method UpdateCustomerEmailBalanceAlert : CIBP-WEB microservice integration
							 * 
							 */
							//Microservice implementation
							CustomerEmailBalanceAlert beanAlertConfirmation = AlertsFacade.getInstance().updateCustomerEmailBalanceAlert(beanAlert, userSession);
							emailalertlt = "true";
							a.setEmailalertlt(true);
							a.setEmailalertltmod(true);
						} else if (alertId.equals("")) { // if alert not exists
							// then
							// insert
							
							/**
							 * MBNA-3499
							 * Method InsertCustomerEmailBalanceAlert : CIBP-WEB microservice integration
							 * 
							 */
							//Microservice implementation
							CustomerEmailBalanceAlert beanAlertConfirmation = AlertsFacade.getInstance().insertCustomerEmailBalanceAlert(beanAlert, userSession);
							
							emailalertlt = "true";
						}
					}
					/* Now, all concerning to SMS's alerts */
					// if(a.isSmsalertgt()||a.isSmsalertlt()||a.isSmsalertnewst()){
					// // if any sms alert is checked then
					// if(profile.hasEntitlement(EntitlementType.MOBILE_BANKING))
					// smsent = new
					// SMSEntitlement(profile.getEntitlementByType(EntitlementType.MOBILE_BANKING));
					// }
					if (a.isSmsalertgt() || a.isSmsalertlt()) { // if any sms
						// alert
						// is checked
						// then
						if (customer.hasEntitlement(EntitlementType.MOBILE_BANKING))
							smsent = new SMSEntitlement(customer.getEntitlementByType(EntitlementType.MOBILE_BANKING));
					}

					String smsAlertId = "";
					SMSProfile SmsProfile = userSession.getSmsProfile(customerInteractionEvent);

					// if(!a.isSmsalertgt()||!a.isSmsalertlt()||!a.isSmsalertnewst()){
					// //if any sms alert is unchecked
					// if(!a.isSmsalertgt()&&a.isSmsalertgtmod()){ // if alert
					// were
					// exists, delete
					// smsAlertId = findSmsAlert(smsaccounts,
					// frontact.getAccountNumber(),"GT");
					// customerService.DeleteSMSAccount(SmsProfile, smsAlertId);
					// smsalertgt = "false";
					// a.setSmsalertgt(false);
					// }
					// if(!a.isSmsalertlt()&&a.isSmsalertltmod()){ // if alert
					// were
					// exists, delete
					// smsAlertId = findSmsAlert(smsaccounts,
					// frontact.getAccountNumber(),"LT");
					// customerService.DeleteSMSAccount(SmsProfile, smsAlertId);
					// smsalertlt = "false";
					// a.setSmsalertlt(false);
					// }
					// if(!a.isSmsalertnewst()&&a.isSmsalertnewstmod()){/** Aqui
					// poner para la opcion "when a new e-Statment is received
					// **/
					// smsAlertId = findSmsEbppAlert(smsebppaccounts,
					// frontact.getAccountNumber());
					// customerService.DeleteSMSEbppAccount(SmsProfile,
					// smsAlertId);
					// smsalertnewstmod = "false";
					// smsalertnewst = "false";
					// a.setSmsalertnewstmod(false);
					// a.setSmsalertnewst(false);
					// }
					// }

					if (!a.isSmsalertgt() || !a.isSmsalertlt()) { // if any sms
						// alert is
						// unchecked
						if (!a.isSmsalertgt()) { // if alert were exists, delete
							smsAlertId = findSmsAlert(smsaccounts, frontact.getAccountNumber(),
									frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
									frontact.getProductDefinition().getProductId(), "GT");
							if (!smsAlertId.equals("")) {
								customerService.DeleteSMSAccount(SmsProfile, smsAlertId);
								smsalertgt = "false";
								a.setSmsalertgt(false);
								a.setSmsalertgtmod(false);
							}
						}
						if (!a.isSmsalertlt()) { // if alert were exists, delete
							smsAlertId = findSmsAlert(smsaccounts, frontact.getAccountNumber(),
									frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
									frontact.getProductDefinition().getProductId(), "LT");
							if (!smsAlertId.equals("")) {
								customerService.DeleteSMSAccount(SmsProfile, smsAlertId);
								smsalertlt = "false";
								a.setSmsalertlt(false);
								a.setSmsalertltmod(false);
							}
						}
						// if(!a.isSmsalertnewst()&&a.isSmsalertnewstmod()){/**
						// Aqui
						// poner para la opcion "when a new e-Statment is
						// received
						// **/
						// smsAlertId = findSmsEbppAlert(smsebppaccounts,
						// frontact.getAccountNumber());
						// customerService.DeleteSMSEbppAccount(SmsProfile,
						// smsAlertId);
						// smsalertnewstmod = "false";
						// smsalertnewst = "false";
						// a.setSmsalertnewstmod(false);
						// a.setSmsalertnewst(false);
						// }
					}

					SMSAccount smsact = new SMSAccount();
					// SMSEbppAccount smsebppact = new SMSEbppAccount();
					if (a.isSmsalertgt()) { // if sms checkmark is checked...
						// ALERT_CONDITION_OPERATOR = GT
						AddSMSAccountRequest re = new AddSMSAccountRequest(customerInteractionEvent, customer,
								SmsProfile, smsent, frontact.getAccountNumber(), frontact.getSubtype(),
								frontact.getAccountSection(), frontact.getNickname(), frontact.getProductDefinition()
								.getProductId(), "X");
						re.setConditionHigh(new BigDecimal(a.getAmountgt()));
						re.setCondition("GT");
						/** Insert, Update or Delete **/
						String alertId = findSmsAlert(smsaccounts, frontact.getAccountNumber(),
								frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
								frontact.getProductDefinition().getProductId(), "GT");

						if (!alertId.equals("")) { // if exists alert and
							// smsalertgt
							// is unchecked then delete
							customerService.DeleteSMSAccount(SmsProfile, alertId);
							smsalertgt = "true";
							a.setSmsalertgt(true);
							a.setSmsalertgtmod(true);
							smsact = customerService.addSMSAccountWithAlertCondition(re);
						} else if (alertId.equals("")) { // if alert does not
							// exists
							// and smsalertgt is
							// checked then
							// insert
							re.setAddAlert(true);
							smsact = customerService.addSMSAccountWithAlertCondition(re);
							smsalertgt = "true";
						}
					}
					if (a.isSmsalertlt()) { // if sms checkmark is checked...
						// ALERT_CONDITION_OPERATOR = GT
						AddSMSAccountRequest re = new AddSMSAccountRequest(customerInteractionEvent, customer,
								SmsProfile, smsent, frontact.getAccountNumber(), frontact.getSubtype(),
								frontact.getAccountSection(), frontact.getNickname(), frontact.getProductDefinition()
								.getProductId(), "X");
						re.setConditionLow(new BigDecimal(a.getAmountlt()));
						re.setCondition("LT");
						/** Insert, Update or Delete **/
						String alertId = findSmsAlert(smsaccounts, frontact.getAccountNumber(),
								frontact.getAccountSection(), frontact.getProductDefinition().getProductType(),
								frontact.getProductDefinition().getProductId(), "LT");

						if (!alertId.equals("")) { // if exists alert and
							// smsalertlt
							// is unchecked then delete
							customerService.DeleteSMSAccount(SmsProfile, alertId);
							smsalertlt = "true";
							a.setSmsalertlt(true);
							a.setSmsalertltmod(true);
							smsact = customerService.addSMSAccountWithAlertCondition(re);
						} else if (alertId.equals("")) { // if alert not exists
							// and
							// smsalertlt is
							// checked
							// then insert
							re.setAddAlert(true);
							smsact = customerService.addSMSAccountWithAlertCondition(re);
							smsalertlt = "true";
						}
					}
					BalancesForm.updateCustomerBalanceAlert(userSession, a, request, frontact, language);
				}
			}
		} catch (RuntimeException e) {
			logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
			model.put("text", false);
			WebUtils.setSessionAttribute(request, ChallengeInformationUtils.SHOW_UPDATE_ALERT, false);
		}
		if (!a.isEmailalertgt() && !a.isSmsalertgt()) { // if both sms and email
			// alert are unchecked,
			// clean amount value.
			amountgt = "";
		}
		if (!a.isEmailalertlt() && !a.isSmsalertlt()) { // if both sms and email
			// alert are unchecked,
			// clean amount value.
			amountlt = "";
		}
		userSession.resetEmailAccounts();
		userSession.resetSmsAccounts();
		userSession.resetSmsEbppAccounts();

		model.put("emailalertgt", emailalertgt);
		model.put("smsalertgt", smsalertgt);
		model.put("emailalertlt", emailalertlt);
		model.put("smsalertlt", smsalertlt);
		model.put("amountgt", amountgt);
		model.put("amountlt", amountlt);
	}

	private void updatePersonalInformationChanges(HttpServletRequest request, Map<String, Object> model,
			Map<String, Object> parameters) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();
		AlertsEntitlement entitlement = null;
		/*
		 * Se verifica si el profile tiene el Entitlement de ALERTS , en caso de
		 * no tenerlo se le agrega REF JIRA CIBP-1777
		 */
		if (!profile.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement alert = new AlertsEntitlement();
			customerService.getCustomerProfileService().addEntitlement(profile, alert.getCustomerEntitlement());
		}
		// if(profile.hasEntitlement(EntitlementType.ALERTS))
		entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		Set<AlertType> customeralerts = new HashSet<AlertType>();
		if (entitlement != null && entitlement.getAlerts() != null)
			customeralerts = entitlement.getAlerts();

		ModifyCustomerAlertsRequest req = new ModifyCustomerAlertsRequest();
		req.setCustomerProfile(userSession.getCustomerProfile());
		req.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));

		PersonalInformationChangesBean a = (PersonalInformationChangesBean) parameters
				.get(ChallengeInformationUtils.PERSONAL_INFORMATION_CHANGES_ACTION);
		/* CIBP-1104 */
		if (a.isPasswordChangeMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.CHANGE_PASS_SMS);
				customeralerts.remove(AlertType.CHANGE_PASS_PUSH);
			} else {
				if (customeralerts.contains(AlertType.CHANGE_PASS_SMS))
					customeralerts.remove(AlertType.CHANGE_PASS_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		}

		else if (a.isBlockedPasswordEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.ACCESS_BLOCK_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.ACCESS_BLOCK_EMAIL))
					customeralerts.remove(AlertType.ACCESS_BLOCK_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isBlockedPasswordMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.ACCESS_BLOCK_SMS);
				customeralerts.remove(AlertType.ACCESS_BLOCK_PUSH);
			} else {
				if (customeralerts.contains(AlertType.ACCESS_BLOCK_SMS))
					customeralerts.remove(AlertType.ACCESS_BLOCK_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		}

		else if (a.isBlockedSecretQuestionsEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.QUESTIONS_BLOCK_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL))
					customeralerts.remove(AlertType.QUESTIONS_BLOCK_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isBlockedSecretQuestionsMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.QUESTIONS_BLOCK_SMS);
				customeralerts.remove(AlertType.QUESTIONS_BLOCK_PUSH);
			} else {
				if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_SMS))
					customeralerts.remove(AlertType.QUESTIONS_BLOCK_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isSecretQuestionsUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.QUESTIONS_EDIT_SMS);
				customeralerts.remove(AlertType.QUESTIONS_EDIT_PUSH);
			} else {
				if (customeralerts.contains(AlertType.QUESTIONS_EDIT_SMS))
					customeralerts.remove(AlertType.QUESTIONS_EDIT_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isEmailUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.EMAIL_EDIT_SMS);
				customeralerts.remove(AlertType.EMAIL_EDIT_PUSH);
			} else {
				if (customeralerts.contains(AlertType.EMAIL_EDIT_SMS))
					customeralerts.remove(AlertType.EMAIL_EDIT_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		}

		else if (a.isPhoneNumberUpdatedEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PHONE_EDIT_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.PHONE_EDIT_EMAIL))
					customeralerts.remove(AlertType.PHONE_EDIT_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isPhoneNumberUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PHONE_EDIT_SMS);
				customeralerts.remove(AlertType.PHONE_EDIT_PUSH);
			} else {
				if (customeralerts.contains(AlertType.PHONE_EDIT_SMS))
					customeralerts.remove(AlertType.PHONE_EDIT_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		}

		else if (a.isAddressUpdatedEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.ADRESS_EDIT_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.ADRESS_EDIT_EMAIL))
					customeralerts.remove(AlertType.ADRESS_EDIT_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isAddressUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.ADRESS_EDIT_SMS);
				customeralerts.remove(AlertType.ADRESS_EDIT_PUSH);
			} else {
				if (customeralerts.contains(AlertType.ADRESS_EDIT_SMS))
					customeralerts.remove(AlertType.ADRESS_EDIT_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		}

		else if (a.isMobileBankingPhoneNumberUpdatedEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.MOBILE_PHONE_EDIT_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_EMAIL))
					customeralerts.remove(AlertType.MOBILE_PHONE_EDIT_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isMobileBankingPhoneNumberUpdatedMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.MOBILE_PHONE_EDIT_SMS);
				customeralerts.remove(AlertType.MOBILE_PHONE_EDIT_PUSH);
			} else {
				if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_SMS))
					customeralerts.remove(AlertType.MOBILE_PHONE_EDIT_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		}
		// FFEIC
		else if (a.isNewDeviceEmail()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.NEW_DEVICE_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.NEW_DEVICE_EMAIL))
					customeralerts.remove(AlertType.NEW_DEVICE_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isNewDeviceMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.NEW_DEVICE_SMS);
				customeralerts.remove(AlertType.NEW_DEVICE_PUSH);
			} else {
				if (customeralerts.contains(AlertType.NEW_DEVICE_SMS))
					customeralerts.remove(AlertType.NEW_DEVICE_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isNewFingerprintMobile()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.NEW_FINGERPRINT_SMS);
				customeralerts.remove(AlertType.NEW_FINGERPRINT_PUSH);
			} else {
				if (customeralerts.contains(AlertType.NEW_FINGERPRINT_SMS))
					customeralerts.remove(AlertType.NEW_FINGERPRINT_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e);
				model.put("text", false);
			}
		}
		// FIN FFEIC
		model.put("text", false);
	}

	private void updatePaymentsTranfers(HttpServletRequest request, Map<String, Object> model,
			Map<String, Object> parameters) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile profile = userSession.getCustomerProfile();
		AlertsEntitlement entitlement = null;
		if (!profile.hasEntitlement(EntitlementType.ALERTS)) {
			AlertsEntitlement alert = new AlertsEntitlement();
			customerService.getCustomerProfileService().addEntitlement(profile, alert.getCustomerEntitlement());
		}
		entitlement = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));

		Set<AlertType> customeralerts = new HashSet<AlertType>();
		if (entitlement != null && entitlement.getAlerts() != null)
			customeralerts = entitlement.getAlerts();

		ModifyCustomerAlertsRequest req = new ModifyCustomerAlertsRequest();
		req.setCustomerProfile(userSession.getCustomerProfile());
		req.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));

		PaymentsAndTransfersBean a = (PaymentsAndTransfersBean) parameters
				.get(ChallengeInformationUtils.PAYMENTS_TRANSFERS_ACTION);

		if (a.isPaymentEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_EMAIL);
			}

			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isTransferCanEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_CANCEL_FUTURE_EMAIL);
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_EMAIL);
				if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_MODIFY_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_FUTURE_EMAIL);
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_EMAIL);
				if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_MODIFY_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isTransferCanMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_CANCEL_FUTURE_SMS);
				customeralerts.remove(AlertType.TRANSFERS_CANCEL_FUTURE_PUSH);
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_SMS))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_SMS);
				if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_SMS))
					customeralerts.remove(AlertType.TRANSFERS_MODIFY_SMS);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_SMS))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_FUTURE_SMS);
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_SMS))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_SMS);
				if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_SMS))
					customeralerts.remove(AlertType.TRANSFERS_MODIFY_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isPaymentCanEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL);
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_EMAIL);
				if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_MODIFY_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_FUTURE_EMAIL);
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_EMAIL);
				if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_EMAIL))
					customeralerts.remove(AlertType.PAYMENTS_MODIFY_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isPaymentCanMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_CANCEL_FUTURE_SMS);
				customeralerts.remove(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH);
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_SMS))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_SMS);
				if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_SMS))
					customeralerts.remove(AlertType.PAYMENTS_MODIFY_SMS);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_FUTURE_SMS))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_FUTURE_SMS);
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_SMS))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_SMS);
				if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_SMS))
					customeralerts.remove(AlertType.PAYMENTS_MODIFY_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isTransferEmailConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_EMAIL);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_EMAIL))
					customeralerts.remove(AlertType.TRANSFERS_EMAIL);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isPaymentMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_SMS);
				customeralerts.remove(AlertType.PAYMENTS_PUSH);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_SMS))
					customeralerts.remove(AlertType.PAYMENTS_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isTransferMobileConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_SMS);
				customeralerts.remove(AlertType.TRANSFERS_PUSH);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_SMS))
					customeralerts.remove(AlertType.TRANSFERS_SMS);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());

			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isPaymentPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_PUSH);
				customeralerts.remove(AlertType.PAYMENTS_SMS);
			  
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_PUSH))
					customeralerts.remove(AlertType.PAYMENTS_PUSH);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isPaymentCanPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH);
				customeralerts.remove(AlertType.PAYMENTS_CANCEL_FUTURE_SMS);
			} else {
				if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH))
					customeralerts.remove(AlertType.PAYMENTS_CANCEL_FUTURE_PUSH);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isTransferPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_PUSH);
				customeralerts.remove(AlertType.TRANSFERS_SMS);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_PUSH))
					customeralerts.remove(AlertType.TRANSFERS_PUSH);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		} else if (a.isTransferCanPushNotifConfirmation()) {
			boolean al = a.getAction().equalsIgnoreCase("true") ? true : false;
			if (al) {
				customeralerts.add(AlertType.TRANSFERS_CANCEL_FUTURE_PUSH);
				customeralerts.remove(AlertType.TRANSFERS_CANCEL_FUTURE_SMS);
			} else {
				if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_PUSH))
					customeralerts.remove(AlertType.TRANSFERS_CANCEL_FUTURE_PUSH);
			}
			req.setAlerts(customeralerts);
			try {
				profile = customerService.modifyCustomerAlerts(req);
				userSession.setCustomerProfile(profile);
				model.put("text", a.getAction());
			} catch (RuntimeException e) {
				logger.error("onSubmit(HttpServletRequest, HttpServletResponse, Object, BindException)", e); //$NON-NLS-1$
				model.put("text", false);
			}
		}
	}

	private void deleteSmsProfile(HttpServletRequest request, UserSession userSession, Map<String, Object> model) {
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		model.put("smsDeleted", true);
		try {
			DeleteSMSProfileRequest delreq = new DeleteSMSProfileRequest(
					Utils.generateCustomerInteractionEvent(request), customer, new SMSEntitlement(
							customer.getEntitlementByType(EntitlementType.MOBILE_BANKING)),
							userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)));
			customerService.deleteSMSProfile(delreq);
			userSession.setSmsProfile(null);
			userSession.resetMobileBanking();
			// CIBP-1857 ISSUE BANCA MOBILE BGARCIA
			// SE LIMPIAN LAS ALERTAS POR CUENTA
			userSession.setSmsaccounts(null);// blas
			// CIBP-1857 ISSUE BANCA MOBILE BGARCIA se eliminan las alertas sms
			// de mi banco
			AlertsEntitlement entitlement = null;
			entitlement = new AlertsEntitlement(userSession.getCustomerProfile().getEntitlementByType(
					EntitlementType.ALERTS));
			Set<AlertType> customeralerts = new HashSet<AlertType>();
			if (entitlement != null && entitlement.getAlerts() != null)
				customeralerts = entitlement.getAlerts();
			if (customeralerts.contains(AlertType.PAYMENTS_SMS))
				customeralerts.remove(AlertType.PAYMENTS_SMS);
			if (customeralerts.contains(AlertType.TRANSFERS_SMS))
				customeralerts.remove(AlertType.TRANSFERS_SMS);
			if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_SMS))
				customeralerts.remove(AlertType.PAYMENTS_CANCEL_SMS);
			if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_SMS))
				customeralerts.remove(AlertType.TRANSFERS_CANCEL_SMS);
			if (customeralerts.contains(AlertType.PASSWORD_BLOCK_SMS))
				customeralerts.remove(AlertType.PASSWORD_BLOCK_SMS);
			if (customeralerts.contains(AlertType.RSA_BLOCK_SMS))
				customeralerts.remove(AlertType.RSA_BLOCK_SMS);
			if (customeralerts.contains(AlertType.PAYMENTS_CANCEL_FUTURE_SMS))
				customeralerts.remove(AlertType.PAYMENTS_CANCEL_FUTURE_SMS);
			if (customeralerts.contains(AlertType.TRANSFERS_CANCEL_FUTURE_SMS))
				customeralerts.remove(AlertType.TRANSFERS_CANCEL_FUTURE_SMS);
			/* CIBP-1104 */
			if (customeralerts.contains(AlertType.PAYMENTS_MODIFY_SMS))
				customeralerts.remove(AlertType.PAYMENTS_MODIFY_SMS);
			if (customeralerts.contains(AlertType.TRANSFERS_MODIFY_SMS))
				customeralerts.remove(AlertType.TRANSFERS_MODIFY_SMS);
			if (customeralerts.contains(AlertType.CHANGE_PASS_SMS))
				customeralerts.remove(AlertType.CHANGE_PASS_SMS);
			if (customeralerts.contains(AlertType.ACCESS_BLOCK_SMS))
				customeralerts.remove(AlertType.ACCESS_BLOCK_SMS);
			if (customeralerts.contains(AlertType.QUESTIONS_BLOCK_SMS))
				customeralerts.remove(AlertType.QUESTIONS_BLOCK_SMS);
			if (customeralerts.contains(AlertType.QUESTIONS_EDIT_SMS))
				customeralerts.remove(AlertType.QUESTIONS_EDIT_SMS);
			if (customeralerts.contains(AlertType.EMAIL_EDIT_SMS))
				customeralerts.remove(AlertType.EMAIL_EDIT_SMS);
			if (customeralerts.contains(AlertType.PHONE_EDIT_SMS))
				customeralerts.remove(AlertType.PHONE_EDIT_SMS);
			if (customeralerts.contains(AlertType.ADRESS_EDIT_SMS))
				customeralerts.remove(AlertType.ADRESS_EDIT_SMS);
			if (customeralerts.contains(AlertType.MOBILE_PHONE_EDIT_SMS))
				customeralerts.remove(AlertType.MOBILE_PHONE_EDIT_SMS);
			if (customeralerts.contains(AlertType.NEW_DEVICE_SMS))
				customeralerts.remove(AlertType.NEW_DEVICE_SMS);
			/* FIN---- CIBP-1104 */

			// Search and Remove SMS OPTION for ATH SMS ALERTS
			// PurchasesAndWithdrawalsBean bean = (PurchasesAndWithdrawalsBean)
			// command;

			AccountMaskUtil maskutil = new AccountMaskUtil();

			List<TVFrontendAccount> accounts = userSession.getAccounts();
			List<SmsBean> smsBeans = new LinkedList<SmsBean>();
			for (TVFrontendAccount account : accounts) {
				String suffix;
				if (account != null && account.getAccountNumberSuffix() != null)
					suffix = account.getAccountNumberSuffix();
				else
					suffix = "";
				if ((!suffix.equalsIgnoreCase("")) && suffix.equalsIgnoreCase("RES") || suffix.equalsIgnoreCase("SAV"))
					continue;
				boolean cont = false;
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE231)){
					cont = Utils.haveType(this.alertsAccountTypes, account.getAccountProductId());
				}else{
					cont = this.alertsAccountTypes.contains(account.getAccountProductId());
				}
				if (account != null && !account.getHide() && cont) {
					try {
						CardPlasticFrontend[] plastics = userSession.getCardPlastics(account);

						if (plastics.length > 0
								&& !(account.getCurrentAthSmsAlerts() != null && account.getCurrentAthSmsAlerts()
								.size() > 0)) {
							HashMap<Object, Object> smsInfo = new HashMap<>();
							for (int i = 0; i < plastics.length; i++) {
								SmsBean smsBeanRequest = new SmsBean();
								smsBeanRequest.setAcctNum(account.getAccountNumber());
								smsBeanRequest.setTaxId(customer.getTaxId());
								smsBeanRequest.setCardNum(plastics[i].getCardPlasticAccountRecord().getCardPANId());

								SmsAlertsResponse smsAlertsResponse = customerService.getSmsAlertInfo(
										smsBeanRequest, Utils.generateCustomerInteractionEvent(request));
								if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
										&& smsAlertsResponse.getResponseSmsBean() != null) {
									smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
											smsAlertsResponse.getResponseSmsBean());
									smsBeans.add(smsAlertsResponse.getResponseSmsBean());
								} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
										&& smsAlertsResponse.getResponseSmsBean() == null) {
									smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
									if (!smsAlertsResponse.isAuditable())
										logger.error("Error Removing SMS Option while Unregistering Cel #: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
												+ maskutil.mask(account.getAccountNumber())
												+ "\ncard: "
												+ maskutil.mask(smsBeanRequest.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ smsAlertsResponse.getAuditString());
									else
										logger.error("Error Removing SMS Option while Unregistering Cel #: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
												+ maskutil.mask(account.getAccountNumber())
												+ "\ncard: "
												+ maskutil.mask(smsBeanRequest.getCardNum())
												+ "\n"
												+ customer.getFullName() + "\nProfileID: " + customer.getProfileId());
								} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
										&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
									smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
									if (!smsAlertsResponse.isAuditable())
										logger.error("Error Removing SMS Option while Unregistering Cel #: Unable to send ATH SmsInfo request for \nacct: "
												+ maskutil.mask(account.getAccountNumber())
												+ "\ncard: "
												+ maskutil.mask(smsBeanRequest.getCardNum())
												+ "\n"
												+ customer.getFullName() + "\nProfileID: " + customer.getProfileId());
									else
										logger.error("Error Removing SMS Option while Unregistering Cel #: Unable to send ATH SmsInfo request for \nacct: "
												+ maskutil.mask(account.getAccountNumber())
												+ "\ncard: "
												+ maskutil.mask(smsBeanRequest.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n" + smsAlertsResponse.getAuditString());
								} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
										&& (smsAlertsResponse.getException() != null || !GenericValidator
										.isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
									smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
									if (!smsAlertsResponse.isAuditable()
											&& !GenericValidator.isBlankOrNull(smsAlertsResponse
													.getSmsAlertsException())
													&& smsAlertsResponse.getException() != null)
										logger.error("Error Removing SMS Option while Unregistering Cel #: calling ATH SmsInfo \nacct: "
												+ maskutil.mask(account.getAccountNumber())
												+ "\ncard: "
												+ maskutil.mask(smsBeanRequest.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n"
												+ smsAlertsResponse.getSmsAlertsException()
												+ "\n"
												+ smsAlertsResponse.getException());
									else if (!smsAlertsResponse.isAuditable()
											&& !GenericValidator.isBlankOrNull(smsAlertsResponse
													.getSmsAlertsException())
													&& smsAlertsResponse.getException() == null) {
										logger.error("Error Removing SMS Option while Unregistering Cel #: calling ATH SmsInfo \nacct: "
												+ maskutil.mask(account.getAccountNumber())
												+ "\ncard: "
												+ maskutil.mask(smsBeanRequest.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n" + smsAlertsResponse.getSmsAlertsException());
									} else if (!smsAlertsResponse.isAuditable()
											&& GenericValidator
											.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
											&& smsAlertsResponse.getException() != null) {
										logger.error("Error Removing SMS Option while Unregistering Cel #: calling ATH SmsInfo \nacct: "
												+ maskutil.mask(account.getAccountNumber())
												+ "\ncard: "
												+ maskutil.mask(smsBeanRequest.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n" + smsAlertsResponse.getException());
									} else if (smsAlertsResponse.isAuditable()
											&& !GenericValidator.isBlankOrNull(smsAlertsResponse
													.getSmsAlertsException())
													&& smsAlertsResponse.getException() != null)
										logger.error("Error Removing SMS Option while Unregistering Cel #: calling ATH SmsInfo \nacct: "
												+ maskutil.mask(account.getAccountNumber())
												+ "\ncard: "
												+ maskutil.mask(smsBeanRequest.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n"
												+ smsAlertsResponse.getSmsAlertsException()
												+ "\n"
												+ smsAlertsResponse.getAuditString());
									else {
										logger.error("Error Removing SMS Option while Unregistering Cel #: calling ATH SmsInfo \nacct: "
												+ maskutil.mask(account.getAccountNumber())
												+ "\ncard: "
												+ maskutil.mask(smsBeanRequest.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n" + smsAlertsResponse.getAuditString());
									}
								} else {
									smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
									logger.error("Error Removing SMS Option while Unregistering Cel #: calling ATH SmsInfo \nacct: "
											+ maskutil.mask(account.getAccountNumber())
											+ "\ncard: "
											+ maskutil.mask(smsBeanRequest.getCardNum())
											+ "\n"
											+ customer.getFullName() + "\nProfileID: " + customer.getProfileId() + "\n");
								}
							}
							if (smsInfo.size() > 0)
								account.setCurrentAthSmsAlerts(smsInfo);
						}

						if (plastics.length > 0 && account.getCurrentAthSmsAlerts() != null
								&& account.getCurrentAthSmsAlerts().size() > 0) {
							// CardPlasticAccountRecord[]
							// plastics=account.getCardPlasticAccountRecords();
							for (int i = 0; i < plastics.length; i++) {
								if (account.getCurrentAthSmsAlerts().containsKey(
										plastics[i].getCardPlasticAccountRecord().getCardPANId())
										&& (account.getCurrentAthSmsAlerts().get(
												plastics[i].getCardPlasticAccountRecord().getCardPANId()) != null)) {
									smsBeans.add((SmsBean) account.getCurrentAthSmsAlerts().get(
											plastics[i].getCardPlasticAccountRecord().getCardPANId()));
								}
							}
						}

						List<SmsBean> forUpdating = new LinkedList<SmsBean>();
						List<SmsBean> forDeleting = new LinkedList<SmsBean>();

						if (smsBeans != null && smsBeans.size() > 0) {
							for (SmsBean sms : smsBeans) {

								sms.setPurchase_thr_chksms("0");
								if (GenericValidator.isBlankOrNull(sms.getPurchase_thr_chkemail())
										|| sms.getPurchase_thr_chkemail().equalsIgnoreCase("0"))
									sms.setAlertsPurchaseminamt("");
								sms.setWithdraw_thr_chksms("0");
								if (GenericValidator.isBlankOrNull(sms.getWithdraw_thr_chkemail())
										|| sms.getWithdraw_thr_chkemail().equalsIgnoreCase("0"))
									sms.setAlertsWithdrawminamt("");

								sms.setPurchase_all_chksms("0");
								sms.setWithdraw_all_chksms("0");

								sms.setDlyPurchasetotlschksms("0");
								if (GenericValidator.isBlankOrNull(sms.getDlyPurchasetotlschkemail())
										|| sms.getDlyPurchasetotlschkemail().equalsIgnoreCase("0"))
									sms.setDlyPurchasetotlsminamt("");

								sms.setDlyWithdrawtotlschksms("0");
								if (GenericValidator.isBlankOrNull(sms.getDlyWithdrawtotlschkemail())
										|| sms.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0"))
									sms.setDlyWithdrawtotlsminamt("");

								// org.apache.commons.beanutils.BeanUtils.copyProperties(smsBeanRequest,
								// a);

								if ((GenericValidator.isBlankOrNull(sms.getPurchase_thr_chkemail()) || sms
										.getPurchase_thr_chkemail().equalsIgnoreCase("0"))
										&& (GenericValidator.isBlankOrNull(sms.getPurchase_thr_chksms()) || sms
												.getPurchase_thr_chksms().equalsIgnoreCase("0"))
												&& (GenericValidator.isBlankOrNull(sms.getWithdraw_thr_chkemail()) || sms
														.getWithdraw_thr_chkemail().equalsIgnoreCase("0"))
														&& (GenericValidator.isBlankOrNull(sms.getWithdraw_thr_chksms()) || sms
																.getWithdraw_thr_chksms().equalsIgnoreCase("0"))
																&& (GenericValidator.isBlankOrNull(sms.getDlyPurchasetotlschkemail()) || sms
																		.getDlyPurchasetotlschkemail().equalsIgnoreCase("0"))
																		&& (GenericValidator.isBlankOrNull(sms.getDlyPurchasetotlschksms()) || sms
																				.getDlyPurchasetotlschksms().equalsIgnoreCase("0"))
																				&& (GenericValidator.isBlankOrNull(sms.getDlyWithdrawtotlschkemail()) || sms
																						.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0"))
																						&& (GenericValidator.isBlankOrNull(sms.getDlyWithdrawtotlschksms()) || sms
																								.getDlyWithdrawtotlschksms().equalsIgnoreCase("0"))
																								&& (GenericValidator.isBlankOrNull(sms.getPurchase_all_chkemail()) || sms
																										.getPurchase_all_chkemail().equalsIgnoreCase("0"))
																										&& (GenericValidator.isBlankOrNull(sms.getPurchase_all_chksms()) || sms
																												.getPurchase_all_chksms().equalsIgnoreCase("0"))
																												&& (GenericValidator.isBlankOrNull(sms.getWithdraw_all_chkemail()) || sms
																														.getWithdraw_all_chkemail().equalsIgnoreCase("0"))
																														&& (GenericValidator.isBlankOrNull(sms.getWithdraw_all_chksms()) || sms
																																.getWithdraw_all_chksms().equalsIgnoreCase("0"))) {
									forDeleting.add(sms);
								} else
									forUpdating.add(sms);
							}
						}

						if (forDeleting != null && forDeleting.size() > 0) {

							for (SmsBean sms : forDeleting) {
								SmsBean newBean = new SmsBean();

								newBean.setAcctNum(sms.getAcctNum());
								newBean.setTaxId(sms.getTaxId());
								newBean.setCardNum(sms.getCardNum());
								SmsAlertsResponse smsAlertsResponse = customerService.deleteSmsAlert(newBean,
										Utils.generateCustomerInteractionEvent(request));
								if (smsAlertsResponse.getResult().equals(Result.SUCCESS)) {
									account.getCurrentAthSmsAlerts().remove(sms.getCardNum());

									// Delete record from database here
									try {
										boolean removed = customerService.removeATHSmsAlertProfile(
												customer.getProfileId(), sms.getAcctNum(), sms.getCardNum());
										if (!removed) {
											logger.error("Removed from PAXUS but ERROR eliminating ATH SMS ALERTS PROFILE from cibp database for profileID: "
													+ maskutil.mask(sms.getAcctNum())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\n" + customer.getProfileId() + "\nRecord might not exist...");
										}
									} catch (Exception e) {
										logger.error("Error eliminating ATH SMS ALERTS PROFILE from cibp database for profileID: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\n"
												+ customer.getProfileId()
												+ "\n"
												+ ExceptionUtils.getFullStackTrace(e));
									}

									AlertTypesUtils.removeAlertEveryTransactionProfileById(customerService, customer, sms, logger);

								} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
										&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
									logger.error("Error Deleting SMS Profile while Unregistering Cel #: Unable to send ATH SmsDelete request for \nacct: "
											+ maskutil.mask(sms.getAcctNum())
											+ "\ncard: "
											+ maskutil.mask(sms.getCardNum())
											+ "\n"
											+ customer.getFullName()
											+ "\nProfileID: " + customer.getProfileId());
								} else {

									logger.error("Error Deleting SMS Profile while Unregistering Cel #: (Profile not found) for \nacct: "
											+ maskutil.mask(sms.getAcctNum())
											+ "\ncard: "
											+ maskutil.mask(sms.getCardNum())
											+ "\n"
											+ customer.getFullName()
											+ "\nProfileID: " + customer.getProfileId());
								}

							}

						}

						if (forUpdating != null && forUpdating.size() > 0) {

							for (SmsBean sms : forUpdating) {
								SmsBean newBean = new SmsBean();

								org.apache.commons.beanutils.BeanUtils.copyProperties(newBean, sms);

								SmsAlertsResponse smsAlertsResponse = customerService.updateSmsAlert(newBean,
										Utils.generateCustomerInteractionEvent(request));
								if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
										&& smsAlertsResponse.getResponseSmsBean() != null) {
									account.getCurrentAthSmsAlerts().put(sms.getCardNum(),
											smsAlertsResponse.getResponseSmsBean());
									try {
										boolean removed = customerService.removeATHSmsAlertProfile(
												customer.getProfileId(), sms.getAcctNum(), sms.getCardNum());
										if (!removed) {
											logger.error("UPDATED IN PAXUS, Removing sms options but ERROR eliminating ATH SMS ALERTS PROFILE from cibp database for profileID: "
													+ maskutil.mask(sms.getAcctNum())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\n" + customer.getProfileId() + "\nRecord might not exist...");
										}
									} catch (Exception e) {
										logger.error("Error eliminating ATH SMS ALERTS PROFILE from cibp database for profileID: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\n"
												+ customer.getProfileId()
												+ "\n"
												+ ExceptionUtils.getFullStackTrace(e));
									}

									if ((GenericValidator.isBlankOrNull(sms.getPurchase_all_chksms()) || sms
											.getPurchase_all_chksms().equalsIgnoreCase("0"))
											&& (GenericValidator.isBlankOrNull(sms.getWithdraw_all_chksms()) || sms
													.getWithdraw_all_chksms().equalsIgnoreCase("0"))
													&& (GenericValidator.isBlankOrNull(sms.getPurchase_all_chkemail()) || sms
															.getPurchase_all_chkemail().equalsIgnoreCase("0"))
															&& (GenericValidator.isBlankOrNull(sms.getWithdraw_all_chkemail()) || sms
																	.getWithdraw_all_chkemail().equalsIgnoreCase("0"))) {

										AlertTypesUtils.removeAlertEveryTransactionProfileById(customerService, customer, sms, logger);
											
									}

								} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
										&& smsAlertsResponse.getResponseSmsBean() == null) {
									try {
										boolean removed = customerService.removeATHSmsAlertProfile(
												customer.getProfileId(), sms.getAcctNum(), sms.getCardNum());
										if (!removed) {
											logger.error("UPDATED IN PAXUS, Removing sms options but ERROR eliminating ATH SMS ALERTS PROFILE from cibp database for profileID: "
													+ maskutil.mask(sms.getAcctNum())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\n" + customer.getProfileId() + "\nRecord might not exist...");
										}
									} catch (Exception e) {
										logger.error("Error eliminating ATH SMS ALERTS PROFILE from cibp database for profileID: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\n"
												+ customer.getProfileId()
												+ "\n"
												+ ExceptionUtils.getFullStackTrace(e));
									}

									if ((GenericValidator.isBlankOrNull(sms.getPurchase_all_chksms()) || sms
											.getPurchase_all_chksms().equalsIgnoreCase("0"))
											&& (GenericValidator.isBlankOrNull(sms.getWithdraw_all_chksms()) || sms
													.getWithdraw_all_chksms().equalsIgnoreCase("0"))
													&& (GenericValidator.isBlankOrNull(sms.getPurchase_all_chkemail()) || sms
															.getPurchase_all_chkemail().equalsIgnoreCase("0"))
															&& (GenericValidator.isBlankOrNull(sms.getWithdraw_all_chkemail()) || sms
																	.getWithdraw_all_chkemail().equalsIgnoreCase("0"))) {

										AlertTypesUtils.removeAlertEveryTransactionProfileById(customerService, customer, sms, logger);
									}

									if (!smsAlertsResponse.isAuditable())
										logger.error("Error Updating SMS Profile while Unregistering Cel #: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ smsAlertsResponse.getAuditString());
									else
										logger.error("Error Updating SMS Profile while Unregistering Cel #: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: " + customer.getProfileId());
								} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
										&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
									if (!smsAlertsResponse.isAuditable())
										logger.error("Error Updating SMS Profile while Unregistering Cel #: Unable to send ATH smsUpdate request for \nacct: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: " + customer.getProfileId());
									else
										logger.error("Error Updating SMS Profile while Unregistering Cel #: Unable to send ATH smsUpdate request for \nacct: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n"
												+ smsAlertsResponse.getAuditString());
								} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
										&& (smsAlertsResponse.getException() != null || !GenericValidator
										.isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
									if (!smsAlertsResponse.isAuditable()
											&& !GenericValidator.isBlankOrNull(smsAlertsResponse
													.getSmsAlertsException())
													&& smsAlertsResponse.getException() != null)
										logger.error("Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n"
												+ smsAlertsResponse.getSmsAlertsException()
												+ "\n"
												+ smsAlertsResponse.getException());
									else if (!smsAlertsResponse.isAuditable()
											&& !GenericValidator.isBlankOrNull(smsAlertsResponse
													.getSmsAlertsException())
													&& smsAlertsResponse.getException() == null) {
										logger.error("Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n"
												+ smsAlertsResponse.getSmsAlertsException());
									} else if (!smsAlertsResponse.isAuditable()
											&& GenericValidator
											.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
											&& smsAlertsResponse.getException() != null) {
										logger.error("Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n"
												+ smsAlertsResponse.getException());
									} else if (smsAlertsResponse.isAuditable()
											&& !GenericValidator.isBlankOrNull(smsAlertsResponse
													.getSmsAlertsException())
													&& smsAlertsResponse.getException() != null)
										logger.error("Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n"
												+ smsAlertsResponse.getSmsAlertsException()
												+ "\n"
												+ smsAlertsResponse.getAuditString());
									else {
										logger.error("Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n"
												+ smsAlertsResponse.getAuditString());
									}
								} else {

									if (!smsAlertsResponse.isAuditable())
										logger.error("Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: " + customer.getProfileId() + "\n");
									else {
										logger.error("Error Updating SMS Profile while Unregistering Cel #: calling ATH smsUpdate \nacct: "
												+ maskutil.mask(sms.getAcctNum())
												+ "\ncard: "
												+ maskutil.mask(sms.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n"
												+ smsAlertsResponse.getAuditString());
									}
								}

							}

						}
						smsBeans.clear();
					} catch (Exception e) {
						smsBeans.clear();
						logger.error(
								"Error Removing SMS Option while Unregistering Cel #: \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\n" + "\nProfileID: "
										+ customer.getProfileId(), e);
					}

				}
			}

			ModifyCustomerAlertsRequest req = new ModifyCustomerAlertsRequest();
			req.setCustomerProfile(userSession.getCustomerProfile());
			req.setInteractionEvent(Utils.generateCustomerInteractionEvent(request));
			req.setAlerts(customeralerts);
			userSession.setCustomerProfile(customerService.modifyCustomerAlerts(req));

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Your mobile banking phone number has been deleted.");
			else
				userSession.setStatusmessage("Su n&uacute;mero de tel&eacute;fono ha sido borrado.");

		} catch (Exception e) {

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession
				.setStatusmessage("Unable to delete your phone number at this time, please try later.");
			else
				userSession
				.setStatusmessage("No se pudo borrar su n&uacute;mero de tel&eacute;fono. Favor intentar luego.");

		}
	}

	private void smsEnrollmentCode(HttpServletRequest request, Map<String, Object> model,
			Map<String, Object> parameters, UserSession userSession) {
		CustomerServices customerService = userSession.getCustomerService();
		CustomerProfile customer = userSession.getCustomerProfile();
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.MOBILE_FRAME, true);
		Calendar cal = Calendar.getInstance();
		long mil = cal.getTimeInMillis();
		String code = String.valueOf(mil);
		String phone = (String) parameters.get(ChallengeInformationUtils.PHONE);
		String provider = (String) parameters.get(ChallengeInformationUtils.PROVIDER);
		code = code.substring(code.length() - 4, code.length());
		WebUtils.setSessionAttribute(request, "smsGeneratedCode", code);
		WebUtils.setSessionAttribute(request, "smsPhonereceived", phone);
		WebUtils.setSessionAttribute(request, "smsProvider", provider);
		String msgerr = "";
		try {
			SendSmsGeneratedCodeRequest codereq = new SendSmsGeneratedCodeRequest(
					Utils.generateCustomerInteractionEvent(request), customer, code, phone, provider);
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				customerService.sendGeneratedCode(codereq, PreferredLanguage.ENGLISH);
			else
				customerService.sendGeneratedCode(codereq, PreferredLanguage.SPANISH);
			return;
		} catch (Exception e) {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				msgerr = "Unable to enroll your phone number at this time. Please try later.";
			} else {
				msgerr = "Problemas al inscribir tu n&uacute;mero de tel&eacute;fono en este momento. Favor intentar luego.";
			}
			WebUtils.setSessionAttribute(request, "msgerr", msgerr);
		}
	}

	private void updateEmail(HttpServletRequest request, Map<String, Object> model, Map<String, Object> parameters) {
		EditEmailBean dude = (EditEmailBean) parameters.get(ChallengeInformationUtils.EMAIL_ACTION);
		String email = dude.getEmail();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		try {

			/* CIBP-1104 */
			CustomerProfile customer = userSession.getCustomerProfile();
			// FFEIC
			String oldEmail = customer.getEmail();
			// FIN FFEIC
			boolean hasalert = false;
			AlertsEntitlement ent = new AlertsEntitlement(customer.getEntitlementByType(EntitlementType.ALERTS));
			if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
				Set<AlertType> alerts = ent.getAlerts();
				if (alerts.contains(AlertType.EMAIL_EDIT_EMAIL) || alerts.contains(AlertType.EMAIL_EDIT_SMS) 
						|| alerts.contains(AlertType.EMAIL_EDIT_PUSH)) {
					hasalert = true;
				}
			}
			/* FIN--- CIBP-1104 */

			// CIBP-2089 bgarcia
			if (getEBPPMessageSourceStatus()
					&& customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {
				// The user has an EBPP2 ID
				if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {
					// Update the customer email
					customerService.updateEbpp2User(userSession.getEbpp2UserId(), email, customer);
					ModifyCustomerEmailRequest emailmod = new ModifyCustomerEmailRequest(
							Utils.generateCustomerInteractionEvent(request), customer, email);
					customer = customerService.modifyCustomerEmail(emailmod);
					userSession.setCustomerProfile(customer);
				} else {
					ModifyCustomerEmailRequest emailmod = new ModifyCustomerEmailRequest(
							Utils.generateCustomerInteractionEvent(request), userSession.getCustomerProfile(), email);
					userSession.setCustomerProfile(userSession.getCustomerService().modifyCustomerEmail(emailmod,
							getEBPPMessageSourceStatus()));
				}

				model.put("confirm", true);
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("Email has been changed.");
				else
					userSession.setStatusmessage("E-mail ha sido cambiado.");
				/* CIBP-1104 se activa envio alerta de cambio de email */
				if (hasalert) {
					customerService.sendEmailEditConfirmation(customer,
							userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
							userSession.getPreferredLanguage(), oldEmail);
				}

				AccountMaskUtil maskutil = new AccountMaskUtil();
				List<TVFrontendAccount> accounts = userSession.getAccounts();
				List<evertec.cibp.core.services.smsAlerts.SmsBean> smsBeans = new LinkedList<evertec.cibp.core.services.smsAlerts.SmsBean>();
				for (TVFrontendAccount account : accounts) {
					String suffix;
					if (account != null && account.getAccountNumberSuffix() != null)
						suffix = account.getAccountNumberSuffix();
					else
						suffix = "";
					if ((!suffix.equalsIgnoreCase("")) && suffix.equalsIgnoreCase("RES")
							|| suffix.equalsIgnoreCase("SAV"))
						continue;
					boolean cont = false;
					if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE231)){
						cont = Utils.haveType(this.alertsAccountTypes, account.getAccountProductId());
					}else{
						cont = this.alertsAccountTypes.contains(account.getAccountProductId());
					}
					if (account != null && !account.getHide() && cont) {
						try {
							// if(!account.isCardPlasticsLoaded()){}
							CardPlasticFrontend[] plastics = userSession.getCardPlastics(account);

							if (plastics.length > 0
									&& !(account.getCurrentAthSmsAlerts() != null && account.getCurrentAthSmsAlerts()
									.size() > 0)) {
								HashMap<Object, Object> smsInfo = new HashMap<>();
								for (int i = 0; i < plastics.length; i++) {
									evertec.cibp.core.services.smsAlerts.SmsBean smsBeanRequest = new evertec.cibp.core.services.smsAlerts.SmsBean();
									smsBeanRequest.setAcctNum(account.getAccountNumber());
									smsBeanRequest.setTaxId(customer.getTaxId());
									smsBeanRequest.setCardNum(plastics[i].getCardPlasticAccountRecord().getCardPANId());

									SmsAlertsResponse smsAlertsResponse = customerService.getSmsAlertInfo(
											smsBeanRequest, Utils.generateCustomerInteractionEvent(request));
									if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
											&& smsAlertsResponse.getResponseSmsBean() != null) {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(),
												smsAlertsResponse.getResponseSmsBean());
										smsBeans.add(smsAlertsResponse.getResponseSmsBean());
									} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
											&& smsAlertsResponse.getResponseSmsBean() == null) {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
										if (!smsAlertsResponse.isAuditable())
											logger.error("Error Updating SMS email while editing email: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId() + smsAlertsResponse.getAuditString());
										else
											logger.error("Error Updating SMS email while editing email: get ATH SmsInfo record found, BUT error retrieving details for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId());
									} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
											&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
										if (!smsAlertsResponse.isAuditable())
											logger.error("Error Updating SMS email while editing email: Unable to send ATH SmsInfo request for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId());
										else
											logger.error("Error Updating SMS email while editing email: Unable to send ATH SmsInfo request for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
									} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
											&& (smsAlertsResponse.getException() != null || !GenericValidator
											.isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
										if (!smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null)
											logger.error("Error Updating SMS email while editing email: calling ATH SmsInfo \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getSmsAlertsException()
													+ "\n"
													+ smsAlertsResponse.getException());
										else if (!smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
														&& smsAlertsResponse.getException() == null) {
											logger.error("Error Updating SMS email while editing email: calling ATH SmsInfo \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getSmsAlertsException());
										} else if (!smsAlertsResponse.isAuditable()
												&& GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null) {
											logger.error("Error Updating SMS email while editing email: calling ATH SmsInfo \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId() + "\n" + smsAlertsResponse.getException());
										} else if (smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null)
											logger.error("Error Updating SMS email while editing email: calling ATH SmsInfo \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getSmsAlertsException()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
										else {
											logger.error("Error Updating SMS email while editing email: calling ATH SmsInfo \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(smsBeanRequest.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
										}
									} else {
										smsInfo.put(plastics[i].getCardPlasticAccountRecord().getCardPANId(), null);
										logger.error("Error Updating SMS email while editing email: calling ATH SmsInfo \nacct: "
												+ maskutil.mask(account.getAccountNumber())
												+ "\ncard: "
												+ maskutil.mask(smsBeanRequest.getCardNum())
												+ "\n"
												+ customer.getFullName()
												+ "\nProfileID: "
												+ customer.getProfileId()
												+ "\n");
									}
								}
								if (smsInfo.size() > 0)
									account.setCurrentAthSmsAlerts(smsInfo);
							}

							if (plastics.length > 0 && account.getCurrentAthSmsAlerts() != null
									&& account.getCurrentAthSmsAlerts().size() > 0) {
								// CardPlasticAccountRecord[]
								// plastics=account.getCardPlasticAccountRecords();
								for (int i = 0; i < plastics.length; i++) {
									if (account.getCurrentAthSmsAlerts().containsKey(
											plastics[i].getCardPlasticAccountRecord().getCardPANId())
											&& (account.getCurrentAthSmsAlerts().get(
													plastics[i].getCardPlasticAccountRecord().getCardPANId()) != null)) {
										smsBeans.add((evertec.cibp.core.services.smsAlerts.SmsBean) account
												.getCurrentAthSmsAlerts().get(
														plastics[i].getCardPlasticAccountRecord().getCardPANId()));
									}
								}
							}

							List<evertec.cibp.core.services.smsAlerts.SmsBean> forUpdating = new LinkedList<evertec.cibp.core.services.smsAlerts.SmsBean>();

							if (smsBeans != null && smsBeans.size() > 0) {
								for (evertec.cibp.core.services.smsAlerts.SmsBean sms : smsBeans) {

									// org.apache.commons.beanutils.BeanUtils.copyProperties(smsBeanRequest,
									// a);
									sms.setPrimaryemail(email);
									forUpdating.add(sms);
								}
							}

							if (forUpdating != null && forUpdating.size() > 0) {

								for (evertec.cibp.core.services.smsAlerts.SmsBean sms : forUpdating) {
									evertec.cibp.core.services.smsAlerts.SmsBean newBean = new evertec.cibp.core.services.smsAlerts.SmsBean();

									org.apache.commons.beanutils.BeanUtils.copyProperties(newBean, sms);

									SmsAlertsResponse smsAlertsResponse = customerService.updateSmsAlert(
											newBean, Utils.generateCustomerInteractionEvent(request));
									if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
											&& smsAlertsResponse.getResponseSmsBean() != null) {
										account.getCurrentAthSmsAlerts().put(sms.getCardNum(),
												smsAlertsResponse.getResponseSmsBean());
									} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
											&& smsAlertsResponse.getResponseSmsBean() == null) {
										if (!smsAlertsResponse.isAuditable())
											logger.error("Error Updating SMS email while editing email: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ smsAlertsResponse.getAuditString());
										else
											logger.error("Error Updating SMS email while editing email: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: " + customer.getProfileId());
									} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
											&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
										if (!smsAlertsResponse.isAuditable())
											logger.error("Error Updating SMS email while editing email: Unable to send ATH smsUpdate request for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: " + customer.getProfileId());
										else
											logger.error("Error Updating SMS email while editing email: Unable to send ATH smsUpdate request for \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
									} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
											&& (smsAlertsResponse.getException() != null || !GenericValidator
											.isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
										if (!smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null)
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getSmsAlertsException()
													+ "\n"
													+ smsAlertsResponse.getException());
										else if (!smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
														&& smsAlertsResponse.getException() == null) {
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getSmsAlertsException());
										} else if (!smsAlertsResponse.isAuditable()
												&& GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null) {
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getException());
										} else if (smsAlertsResponse.isAuditable()
												&& !GenericValidator.isBlankOrNull(smsAlertsResponse
														.getSmsAlertsException())
														&& smsAlertsResponse.getException() != null)
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getSmsAlertsException()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
										else {
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
										}
									} else {

										if (!smsAlertsResponse.isAuditable())
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: " + customer.getProfileId() + "\n");
										else {
											logger.error("Error Updating SMS email while editing email: calling ATH smsUpdate \nacct: "
													+ maskutil.mask(account.getAccountNumber())
													+ "\ncard: "
													+ maskutil.mask(sms.getCardNum())
													+ "\n"
													+ customer.getFullName()
													+ "\nProfileID: "
													+ customer.getProfileId()
													+ "\n"
													+ smsAlertsResponse.getAuditString());
										}
									}

								}

							}

						} catch (Exception e) {
							logger.error(
									"Error Updating SMS email while editing email: \nacct: "
											+ maskutil.mask(account.getAccountNumber()) + "\n" + "\nProfileID: "
											+ customer.getProfileId(), e);
						}

					}
				}

			} else {// CIBP-2089 bgarcia
				model.put("confirm", true);
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					userSession.setStatusmessage("We are sorry, the system isn&acute;t available. Please try later.");
				else
					userSession
					.setStatusmessage("Lo sentimos, el sistema no est&aacute; disponible. Por favor intenta mas tarde.");
			}

		} catch (Exception e) {
			model.put("confirm", true);
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				userSession.setStatusmessage("Unable to change email.");
			else
				userSession.setStatusmessage("Email no pudo ser cambiado.");
		}
	}

	public Integer getMaxPasswordRecords() {
		return maxPasswordRecords;
	}

	public void setMaxPasswordRecords(Integer maxPasswordRecords) {
		this.maxPasswordRecords = maxPasswordRecords;
	}

	private String findEmailAlert(List<CustomerEmailBalanceAlert> emailsAlertsAccount, String account,
			String accountSection, String productType, String productId, String pos) {
		if (emailsAlertsAccount != null && emailsAlertsAccount.size() > 0) {
			for (Iterator<CustomerEmailBalanceAlert> iterator = emailsAlertsAccount.iterator(); iterator.hasNext();) {
				CustomerEmailBalanceAlert account2 = (CustomerEmailBalanceAlert) iterator.next();
				if (account2.getAlertCondition().getOperator().equalsIgnoreCase(pos)
						&& account2.getAccountNumber().equals(account)
						&& account2.getAccountSection().equalsIgnoreCase(accountSection)
						&& account2.getAccountSubtype().equalsIgnoreCase(productType)
						&& account2.getAccountProductId().equalsIgnoreCase(productId))
					return account2.getId();
			}
		}
		return "";
	}

	private String findSmsAlert(List<SMSAccount> smsAlertsAccount, String account, String accountSection,
			String productType, String productId, String pos) {
		if (smsAlertsAccount != null && smsAlertsAccount.size() > 0) {
			for (Iterator<SMSAccount> iterator = smsAlertsAccount.iterator(); iterator.hasNext();) {
				SMSAccount account2 = (SMSAccount) iterator.next();
				if (account2.getAlertCondition()!= null && 
						account2.getAlertCondition().getOperator() != null &&
						account2.getAlertCondition().getOperator().equalsIgnoreCase(pos)
						&& account2.getAccountNumber().equals(account)
						&& account2.getAccountSection().equalsIgnoreCase(accountSection)
						&& account2.getAccountSubtype().equalsIgnoreCase(productType)
						&& account2.getAccountProductId().equalsIgnoreCase(productId))
					return account2.getId();
			}
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	private void updateATHPurchasesAndWithdrawals(HttpServletRequest request, Map<String, Object> model,
			Map<String, Object> parameters) throws Exception {
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = session.getCustomerService();
		PurchasesAndWithdrawalsBean a = (PurchasesAndWithdrawalsBean) parameters
				.get(ChallengeInformationUtils.ATH_ACTION);
		model.put(ChallengeInformationUtils.ATH_ACTION, a);
		TVFrontendAccount account = Utils.getAccountById(a.getAccount1(), session.getAccounts());
		AccountMaskUtil maskutil = new AccountMaskUtil();
		CustomerProfile profile = session.getCustomerProfile();
		boolean mobile_banking;
		boolean errors = false;
		if (session.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)) {
			mobile_banking = true;
		} else
			mobile_banking = false;

		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ATH_FRAME, true);
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ATH_MODEL, model);

		String cardNumber = "";
		if (!GenericValidator.isBlankOrNull(a.getAthplastic1())) {

			CardPlasticFrontend[] plasticFrontendArray = session.getCardPlastics(account);

			if (plasticFrontendArray != null && plasticFrontendArray.length > 0) {
				for (int i = 0; i < plasticFrontendArray.length; i++) {
					if (plasticFrontendArray[i].getFrontEndId().equalsIgnoreCase(a.getAthplastic1())) {
						cardNumber = plasticFrontendArray[i].getCardPlasticAccountRecord().getCardPANId();
						break;
					}
				}
			}

		}

		if (!mobile_banking && !GenericValidator.isBlankOrNull(a.getWithdraw_thr_chksms2())) {
			a.setWithdraw_thr_chksms(a.getWithdraw_thr_chksms2());
		}
		if (!mobile_banking && !GenericValidator.isBlankOrNull(a.getPurchase_thr_chksms2())) {
			a.setPurchase_thr_chksms(a.getPurchase_thr_chksms2());
		}

		if (!mobile_banking && !GenericValidator.isBlankOrNull(a.getWithdraw_all_chksms2())) {
			a.setWithdraw_all_chksms(a.getWithdraw_all_chksms2());
		}
		if (!mobile_banking && !GenericValidator.isBlankOrNull(a.getPurchase_all_chksms2())) {
			a.setPurchase_all_chksms(a.getPurchase_all_chksms2());
		}

		if (!mobile_banking && !GenericValidator.isBlankOrNull(a.getDlyPurchasetotlschksms2())) {
			a.setDlyPurchasetotlschksms(a.getDlyPurchasetotlschksms2());
		}
		if (!mobile_banking && !GenericValidator.isBlankOrNull(a.getDlyWithdrawtotlschksms2())) {
			a.setDlyWithdrawtotlschksms(a.getDlyWithdrawtotlschksms2());
		}

		String phone = null;
		String provider = null;
		SMSProfile smsProfile = null;
		if (mobile_banking) {
			if (session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)) != null)
				smsProfile = session.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
			if (smsProfile != null && smsProfile.getCtns() != null) {
				Set<SMSCTN> set = smsProfile.getCtns();
				for (SMSCTN ctnBean : set) {
					provider = ctnBean.getProvider();
					phone = ctnBean.getCtn();
				}
			}
		}

		String statusMessage;

		if (GenericValidator.isBlankOrNull(a.getPurchase_thr_chkemail()))
			a.setPurchase_thr_chkemail("0");
		if (GenericValidator.isBlankOrNull(a.getPurchase_thr_chksms()))
			a.setPurchase_thr_chksms("0");
		if (GenericValidator.isBlankOrNull(a.getWithdraw_thr_chkemail()))
			a.setWithdraw_thr_chkemail("0");
		if (GenericValidator.isBlankOrNull(a.getWithdraw_thr_chksms()))
			a.setWithdraw_thr_chksms("0");
		if (GenericValidator.isBlankOrNull(a.getDlyPurchasetotlschkemail()))
			a.setDlyPurchasetotlschkemail("0");
		if (GenericValidator.isBlankOrNull(a.getDlyPurchasetotlschksms()))
			a.setDlyPurchasetotlschksms("0");
		if (GenericValidator.isBlankOrNull(a.getDlyWithdrawtotlschkemail()))
			a.setDlyWithdrawtotlschkemail("0");
		if (GenericValidator.isBlankOrNull(a.getDlyWithdrawtotlschksms()))
			a.setDlyWithdrawtotlschksms("0");
		if (GenericValidator.isBlankOrNull(a.getPurchase_all_chkemail()))
			a.setPurchase_all_chkemail("0");
		if (GenericValidator.isBlankOrNull(a.getPurchase_all_chksms()))
			a.setPurchase_all_chksms("0");
		if (GenericValidator.isBlankOrNull(a.getWithdraw_all_chkemail()))
			a.setWithdraw_all_chkemail("0");
		if (GenericValidator.isBlankOrNull(a.getWithdraw_all_chksms()))
			a.setWithdraw_all_chksms("0");

		String alertsPurchaseError = "";

		String alertsEveryPurchaseError = "";
		String alertsEveryWithdrawError = "";
		String alertsDailyTotalPurchaseError = "";
		String alertsDailytotalWithdrawError = "";

		String alertsWithdrawError = "";
		String alertsStartEndError = "";
		String onlyDigits;
		String leaveBlank;
		String amountRequired;
		String register;
		String alertTimes;

		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			onlyDigits = "Only digits greater than zero allowed.";
			leaveBlank = "Leave amount blank or select email/sms option(s).";
			amountRequired = "Amount required if selecting email/sms option(s).";
			register = "Must register cell phone number to receive sms alerts.";
			alertTimes = "Verify alert start and end times.";
		} else {
			onlyDigits = "Solo d\u00EDgitos mayor que cero son v\u00E1lidos.";
			leaveBlank = "Dejar cantidad en blanco o marcar opcion(es) email/sms.";
			amountRequired = "Cantidad requerida si marcas opcion(es) email/sms.";
			register = "Requerido registrar # celular para recibir alertas sms.";
			alertTimes = "Verificar los tiempos para env\u00EDos de alertas.";
		}

		if (!GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
				&& !GenericValidator.matchRegexp(a.getAlertsPurchaseminamt(), "^[1-9][0-9]*$")) {
			alertsPurchaseError = onlyDigits;
			errors = true;
		}
		if (!GenericValidator.isBlankOrNull(a.getAlertsWithdrawminamt())
				&& !GenericValidator.matchRegexp(a.getAlertsWithdrawminamt(), "^[1-9][0-9]*$")) {
			alertsWithdrawError = onlyDigits;
			errors = true;
		}

		if (!GenericValidator.isBlankOrNull(a.getDlyPurchasetotlsminamt())
				&& !GenericValidator.matchRegexp(a.getDlyPurchasetotlsminamt(), "^[1-9][0-9]*$")) {
			alertsDailyTotalPurchaseError = onlyDigits;
			errors = true;
		}
		if (!GenericValidator.isBlankOrNull(a.getDlyWithdrawtotlsminamt())
				&& !GenericValidator.matchRegexp(a.getDlyWithdrawtotlsminamt(), "^[1-9][0-9]*$")) {
			alertsDailytotalWithdrawError = onlyDigits;
			errors = true;
		}

		if (!GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
				&& GenericValidator.matchRegexp(a.getAlertsPurchaseminamt(), "^[1-9][0-9]*$")
				&& a.getPurchase_thr_chkemail().equalsIgnoreCase("0")
				&& a.getPurchase_thr_chksms().equalsIgnoreCase("0")) {
			if (!alertsPurchaseError.equals(""))
				alertsPurchaseError += "<br>";

			alertsPurchaseError += leaveBlank;
			errors = true;
		}
		if (!GenericValidator.isBlankOrNull(a.getAlertsWithdrawminamt())
				&& GenericValidator.matchRegexp(a.getAlertsWithdrawminamt(), "^[1-9][0-9]*$")
				&& a.getWithdraw_thr_chkemail().equalsIgnoreCase("0")
				&& a.getWithdraw_thr_chksms().equalsIgnoreCase("0")) {
			if (!alertsWithdrawError.equals(""))
				alertsWithdrawError += "<br>";

			alertsWithdrawError += leaveBlank;
			errors = true;
		}
		if (!GenericValidator.isBlankOrNull(a.getDlyPurchasetotlsminamt())
				&& GenericValidator.matchRegexp(a.getDlyPurchasetotlsminamt(), "^[1-9][0-9]*$")
				&& a.getDlyPurchasetotlschkemail().equalsIgnoreCase("0")
				&& a.getDlyPurchasetotlschksms().equalsIgnoreCase("0")) {
			if (!alertsDailyTotalPurchaseError.equals(""))
				alertsDailyTotalPurchaseError += "<br>";

			alertsDailyTotalPurchaseError += leaveBlank;
			errors = true;
		}
		if (!GenericValidator.isBlankOrNull(a.getDlyWithdrawtotlsminamt())
				&& GenericValidator.matchRegexp(a.getDlyWithdrawtotlsminamt(), "^[1-9][0-9]*$")
				&& a.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0")
				&& a.getDlyWithdrawtotlschksms().equalsIgnoreCase("0")) {
			if (!alertsDailytotalWithdrawError.equals(""))
				alertsDailytotalWithdrawError += "<br>";

			alertsDailytotalWithdrawError += leaveBlank;
			errors = true;
		}
		if (GenericValidator.isBlankOrNull(a.getDlyPurchasetotlsminamt())
				&& (a.getDlyPurchasetotlschkemail().equalsIgnoreCase("1") || a.getDlyPurchasetotlschksms()
						.equalsIgnoreCase("1"))) {
			if (!alertsDailyTotalPurchaseError.equals(""))
				alertsDailyTotalPurchaseError += "<br>";

			alertsDailyTotalPurchaseError += amountRequired;
			errors = true;
		}
		if (GenericValidator.isBlankOrNull(a.getDlyWithdrawtotlsminamt())
				&& (a.getDlyWithdrawtotlschkemail().equalsIgnoreCase("1") || a.getDlyWithdrawtotlschksms()
						.equalsIgnoreCase("1"))) {
			if (!alertsDailytotalWithdrawError.equals(""))
				alertsDailytotalWithdrawError += "<br>";

			alertsDailytotalWithdrawError += amountRequired;
			errors = true;
		}

		if (GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
				&& (a.getPurchase_thr_chkemail().equalsIgnoreCase("1") || a.getPurchase_thr_chksms().equalsIgnoreCase(
						"1"))) {
			if (!alertsPurchaseError.equals(""))
				alertsPurchaseError += "<br>";

			alertsPurchaseError += amountRequired;
			errors = true;
		}
		if (GenericValidator.isBlankOrNull(a.getAlertsWithdrawminamt())
				&& (a.getWithdraw_thr_chkemail().equalsIgnoreCase("1") || a.getWithdraw_thr_chksms().equalsIgnoreCase(
						"1"))) {
			if (!alertsWithdrawError.equals(""))
				alertsWithdrawError += "<br>";

			alertsWithdrawError += amountRequired;
			errors = true;
		}

		if (a.getDlyPurchasetotlschksms().equalsIgnoreCase("1") && !mobile_banking) {
			if (!alertsDailyTotalPurchaseError.equals(""))
				alertsDailyTotalPurchaseError += "<br>";

			alertsDailyTotalPurchaseError += register;
			errors = true;
		}
		if (a.getDlyWithdrawtotlschksms().equalsIgnoreCase("1") && !mobile_banking) {
			if (!alertsDailytotalWithdrawError.equals(""))
				alertsDailytotalWithdrawError += "<br>";

			alertsDailytotalWithdrawError += register;
			errors = true;
		}

		if (a.getPurchase_thr_chksms().equalsIgnoreCase("1") && !mobile_banking) {
			if (!alertsPurchaseError.equals(""))
				alertsPurchaseError += "<br>";

			alertsPurchaseError += register;
			errors = true;
		}
		if (a.getWithdraw_thr_chksms().equalsIgnoreCase("1") && !mobile_banking) {
			if (!alertsWithdrawError.equals(""))
				alertsWithdrawError += "<br>";

			alertsWithdrawError += register;
			errors = true;
		}

		if (a.getPurchase_all_chksms().equalsIgnoreCase("1") && !mobile_banking) {
			if (!alertsEveryPurchaseError.equals(""))
				alertsEveryPurchaseError += "<br>";

			alertsEveryPurchaseError += register;
			errors = true;
		}
		if (a.getWithdraw_all_chksms().equalsIgnoreCase("1") && !mobile_banking) {
			if (!alertsEveryWithdrawError.equals(""))
				alertsEveryWithdrawError += "<br>";

			alertsEveryWithdrawError += register;
			errors = true;
		}
		if (a.getProfileNtfyallhour().equalsIgnoreCase("0")
				&& (GenericValidator.isBlankOrNull(a.getProfileNtfystrhour()) || GenericValidator.isBlankOrNull(a
						.getProfileNtfyendhour()))) {
			alertsStartEndError = alertTimes;
			errors = true;
		}

		if (!alertsPurchaseError.equals(""))
			model.put("alertsPurchaseError", alertsPurchaseError);
		if (!alertsWithdrawError.equals(""))
			model.put("alertsWithdrawError", alertsWithdrawError);
		if (!alertsStartEndError.equals(""))
			model.put("alertsStartEndError", alertsStartEndError);

		if (!alertsEveryPurchaseError.equals(""))
			model.put("alertsEveryPurchaseError", alertsEveryPurchaseError);
		if (!alertsEveryWithdrawError.equals(""))
			model.put("alertsEveryWithdrawError", alertsEveryWithdrawError);
		if (!alertsDailyTotalPurchaseError.equals(""))
			model.put("alertsDailyTotalPurchaseError", alertsDailyTotalPurchaseError);
		if (!alertsDailytotalWithdrawError.equals(""))
			model.put("alertsDailytotalWithdrawError", alertsDailytotalWithdrawError);

		if (errors) {
			return;
		}

		// smsDelete
		if (a.getPurchase_thr_chkemail().equalsIgnoreCase("0") && a.getPurchase_thr_chksms().equalsIgnoreCase("0")
				&& a.getWithdraw_thr_chkemail().equalsIgnoreCase("0")
				&& a.getWithdraw_thr_chksms().equalsIgnoreCase("0")
				&& a.getDlyPurchasetotlschkemail().equalsIgnoreCase("0")
				&& a.getDlyPurchasetotlschksms().equalsIgnoreCase("0")
				&& a.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0")
				&& a.getDlyWithdrawtotlschksms().equalsIgnoreCase("0")
				&& a.getPurchase_all_chkemail().equalsIgnoreCase("0")
				&& a.getPurchase_all_chksms().equalsIgnoreCase("0")
				&& a.getWithdraw_all_chkemail().equalsIgnoreCase("0")
				&& a.getWithdraw_all_chksms().equalsIgnoreCase("0")) {

			SmsBean smsBeanRequest = new SmsBean();
			smsBeanRequest.setAcctNum(account.getAccountNumber());
			smsBeanRequest.setTaxId(profile.getTaxId());
			smsBeanRequest.setCardNum(cardNumber);
			try {
				if (account.getCurrentAthSmsAlerts() != null && account.getCurrentAthSmsAlerts().size() > 0) {
					HashMap<Object, Object> smsInfo = account.getCurrentAthSmsAlerts();
					if (smsInfo.containsKey(cardNumber) && smsInfo.get(cardNumber) != null) {
						SmsAlertsResponse smsAlertsResponse = customerService.deleteSmsAlert(smsBeanRequest,
								Utils.generateCustomerInteractionEvent(request));
						if (smsAlertsResponse.getResult().equals(Result.SUCCESS)) {
							changeRecordInDB(profile, smsBeanRequest, customerService);
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								statusMessage = "Profile Deleted";
							} else {
								statusMessage = "Perfil Borrado";
							}
							model.put("statusMessage", statusMessage);
							account.getCurrentAthSmsAlerts().remove(cardNumber);
						}

						else if (smsAlertsResponse.getResult().equals(Result.ERROR)
								&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								statusMessage = "Error deleting.";
							} else {
								statusMessage = "Error borrando.";
							}
							model.put("statusMessage", statusMessage);
							logger.error("Error: Unable to send ATH SmsDelete request for \nacct: "
									+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
									+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
									+ "\nProfileID: " + profile.getProfileId());
						} else {
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								statusMessage = "Unable to delete. Profile not found.";
							} else {
								statusMessage = "No se pudo Borrar. Perfil no encontrado.";
							}
							model.put("statusMessage", statusMessage);
							logger.error("Error: Unable to delete ATH Alert (Profile not found) for \nacct: "
									+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
									+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
									+ "\nProfileID: " + profile.getProfileId());
						}
					} else {
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
							statusMessage = "Unable to delete. Profile not found.";
						} else {
							statusMessage = "No se pudo Borrar. Perfil no encontrado.";
						}
						model.put("statusMessage", statusMessage);
						logger.error("Error: Unable to delete ATH Alert (Profile not found) for \nacct: "
								+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
								+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
								+ "\nProfileID: " + profile.getProfileId());
					}
				} else {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						statusMessage = "Unable to delete. Profile not found.";
					} else {
						statusMessage = "No se pudo Borrar. Perfil no encontrado.";
					}
					model.put("statusMessage", statusMessage);
				}
			} catch (Exception e) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					statusMessage = "Error deleting.";
				} else {
					statusMessage = "Error borrando.";
				}
				model.put("statusMessage", statusMessage);
				logger.error(
						"Error: customerService.deleteSmsAlert(...) \nacct: "
								+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
								+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + "\nProfileID: "
								+ profile.getProfileId(), e);
			}
			return;
		}

		// update or add sms profile alerts
		if (!(a.getPurchase_thr_chkemail().equalsIgnoreCase("0") && a.getPurchase_thr_chksms().equalsIgnoreCase("0")
				&& a.getWithdraw_thr_chkemail().equalsIgnoreCase("0")
				&& a.getWithdraw_thr_chksms().equalsIgnoreCase("0")
				&& a.getDlyPurchasetotlschkemail().equalsIgnoreCase("0")
				&& a.getDlyPurchasetotlschksms().equalsIgnoreCase("0")
				&& a.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0")
				&& a.getDlyWithdrawtotlschksms().equalsIgnoreCase("0")
				&& a.getPurchase_all_chkemail().equalsIgnoreCase("0")
				&& a.getPurchase_all_chksms().equalsIgnoreCase("0")
				&& a.getWithdraw_all_chkemail().equalsIgnoreCase("0") && a.getWithdraw_all_chksms().equalsIgnoreCase(
						"0"))) {

			SmsBean smsBeanRequest = new SmsBean();

			org.apache.commons.beanutils.BeanUtils.copyProperties(smsBeanRequest, a);
			// if(!mobile_banking &&
			// !GenericValidator.isBlankOrNull(a.getAlertsPurchasechksms()))
			// smsBeanRequest.setAlertsPurchasechksms("");
			// if(!mobile_banking &&
			// !GenericValidator.isBlankOrNull(a.getAlertsWithdrawchksms()))
			// smsBeanRequest.setAlertsWithdrawchksms("");

			if (!GenericValidator.isBlankOrNull(phone))
				smsBeanRequest.setPrimaryphone(phone);
			if (!GenericValidator.isBlankOrNull(provider))
				smsBeanRequest.setPrimaryPhoneCarrier(provider);
			smsBeanRequest.setAcctNum(account.getAccountNumber());
			smsBeanRequest.setCardNum(cardNumber);
			smsBeanRequest.setProfileOnOffFlag("1");
			smsBeanRequest.setTaxId(profile.getTaxId());
			smsBeanRequest.setRealName(profile.getFullName());
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				smsBeanRequest.setLanguagecode("EN");
			} else {
				smsBeanRequest.setLanguagecode("ES");
			}
			smsBeanRequest.setPrimaryemail(profile.getEmail());

			// Attempt update profile
			if (account.getCurrentAthSmsAlerts() != null && account.getCurrentAthSmsAlerts().size() > 0) {
				try {
					HashMap<Object, Object> smsInfo = account.getCurrentAthSmsAlerts();
					if (smsInfo.containsKey(cardNumber) && smsInfo.get(cardNumber) != null) {
						SmsAlertsResponse smsAlertsResponse = customerService.updateSmsAlert(smsBeanRequest,
								Utils.generateCustomerInteractionEvent(request));
						if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
								&& smsAlertsResponse.getResponseSmsBean() != null) {
							account.getCurrentAthSmsAlerts().put(cardNumber, smsAlertsResponse.getResponseSmsBean());
							changeRecordInDB(profile, smsBeanRequest, customerService);
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								statusMessage = "Profile Updated";
							} else {
								statusMessage = "Perfil actualizado";
							}
							model.put("statusMessage", statusMessage);
						} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
								&& smsAlertsResponse.getResponseSmsBean() == null) {
							changeRecordInDB(profile, smsBeanRequest, customerService);
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								statusMessage = "Profile Updated.\nError retrieving updated info.";
							} else {
								statusMessage = "Perfil actualizado.\nError devolviendo info actualizada.";
							}
							model.put("statusMessage", statusMessage);
							if (!smsAlertsResponse.isAuditable())
								logger.error("Error: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
										+ maskutil.mask(account.getAccountNumber())
										+ "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum())
										+ "\n"
										+ profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + smsAlertsResponse.getAuditString());
							else
								logger.error("Error: ATH smsUpdate successful, BUT error retrieving updated details for \nacct: "
										+ maskutil.mask(account.getAccountNumber())
										+ "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum())
										+ "\n"
										+ profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId());
						} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
								&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								statusMessage = "Unable to Update Profile.";
							} else {
								statusMessage = "No se pudo actualizar.";
							}
							model.put("statusMessage", statusMessage);
							if (!smsAlertsResponse.isAuditable())
								logger.error("Error: Unable to send ATH smsUpdate request for \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId());
							else
								logger.error("Error: Unable to send ATH smsUpdate request for \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n"
										+ smsAlertsResponse.getAuditString());

						} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
								&& (smsAlertsResponse.getException() != null || !GenericValidator
								.isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								statusMessage = "Unable to Update Profile.";
							} else {
								statusMessage = "No se pudo actualizar.";
							}
							model.put("statusMessage", statusMessage);
							if (!smsAlertsResponse.isAuditable()
									&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
									&& smsAlertsResponse.getException() != null)
								logger.error("Error: calling ATH smsUpdate \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n"
										+ smsAlertsResponse.getSmsAlertsException() + "\n"
										+ smsAlertsResponse.getException());
							else if (!smsAlertsResponse.isAuditable()
									&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
									&& smsAlertsResponse.getException() == null) {
								logger.error("Error: calling ATH smsUpdate \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n"
										+ smsAlertsResponse.getSmsAlertsException());
							} else if (!smsAlertsResponse.isAuditable()
									&& GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
									&& smsAlertsResponse.getException() != null) {
								logger.error("Error: calling ATH smsUpdate \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n"
										+ smsAlertsResponse.getException());
							} else if (smsAlertsResponse.isAuditable()
									&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
									&& smsAlertsResponse.getException() != null)
								logger.error("Error: calling ATH smsUpdate \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n"
										+ smsAlertsResponse.getSmsAlertsException() + "\n"
										+ smsAlertsResponse.getAuditString());
							else {
								logger.error("Error: calling ATH smsUpdate \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n"
										+ smsAlertsResponse.getAuditString());
							}
						} else {
							if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
								statusMessage = "Unable to Update Profile.";
							} else {
								statusMessage = "No se pudo actualizar.";
							}
							model.put("statusMessage", statusMessage);
							if (!smsAlertsResponse.isAuditable())
								logger.error("Error: calling ATH smsUpdate \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n");
							else {
								logger.error("Error: calling ATH smsUpdate \nacct: "
										+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
										+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
										+ "\nProfileID: " + profile.getProfileId() + "\n"
										+ smsAlertsResponse.getAuditString());
							}
						}
						return;
					}
				} catch (Exception e) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						statusMessage = "Unable to Update Profile.";
					} else {
						statusMessage = "No se pudo actualizar.";
					}
					model.put("statusMessage", statusMessage);
					logger.error(
							"Error: customerService.updateSmsAlert(...) \nacct: "
									+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
									+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + "\nProfileID: "
									+ profile.getProfileId(), e);
					return;
				}
			}

			// Add smsProfile alerts
			try {
				SmsAlertsResponse smsAlertsResponse = customerService.addSmsAlert(smsBeanRequest,
						Utils.generateCustomerInteractionEvent(request));
				if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
						&& smsAlertsResponse.getResponseSmsBean() != null) {
					changeRecordInDB(profile, smsBeanRequest, customerService);

					if (account.getCurrentAthSmsAlerts() != null)
						account.getCurrentAthSmsAlerts().put(cardNumber, smsAlertsResponse.getResponseSmsBean());
					else {
						HashMap<Object, Object> smsInfo = new HashMap<Object, Object>();
						smsInfo.put(cardNumber, smsAlertsResponse.getResponseSmsBean());
						account.setCurrentAthSmsAlerts(smsInfo);
					}

					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						statusMessage = "Profile Added";
					} else {
						statusMessage = "Perfil agregado";
					}
					model.put("statusMessage", statusMessage);
				} else if (smsAlertsResponse.getResult().equals(Result.SUCCESS)
						&& smsAlertsResponse.getResponseSmsBean() == null) {
					changeRecordInDB(profile, smsBeanRequest, customerService);
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						statusMessage = "Profile Added.\nError retrieving added info.";
					} else {
						statusMessage = "Perfil agregado.\nError devolviendo info agregada.";
					}
					model.put("statusMessage", statusMessage);
					if (!smsAlertsResponse.isAuditable())
						logger.error("Error: ATH smsAdd successful, BUT error retrieving added details for \nacct: "
								+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
								+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
								+ "\nProfileID: " + profile.getProfileId() + smsAlertsResponse.getAuditString());
					else
						logger.error("Error: ATH smsAdd successful, BUT error retrieving added details for \nacct: "
								+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
								+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
								+ "\nProfileID: " + profile.getProfileId());
				} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
						&& smsAlertsResponse.getErrorString().contains("ERROR: SmsInfo NOT SENT.")) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						statusMessage = "Unable to Add Profile.";
					} else {
						statusMessage = "No se pudo Agregar.";
					}
					model.put("statusMessage", statusMessage);
					if (!smsAlertsResponse.isAuditable())
						logger.error("Error: Unable to send ATH smsAdd request for \nacct: "
								+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
								+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
								+ "\nProfileID: " + profile.getProfileId());
					else
						logger.error("Error: Unable to send ATH smsAdd request for \nacct: "
								+ maskutil.mask(account.getAccountNumber()) + "\ncard: "
								+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName()
								+ "\nProfileID: " + profile.getProfileId() + "\n" + smsAlertsResponse.getAuditString());

				} else if (smsAlertsResponse.getResult().equals(Result.ERROR)
						&& (smsAlertsResponse.getException() != null || !GenericValidator
						.isBlankOrNull(smsAlertsResponse.getSmsAlertsException()))) {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						statusMessage = "Unable to Add Profile.";
					} else {
						statusMessage = "No se pudo Agregar.";
					}
					model.put("statusMessage", statusMessage);
					if (!smsAlertsResponse.isAuditable()
							&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
							&& smsAlertsResponse.getException() != null)
						logger.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
								+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
								+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n"
								+ smsAlertsResponse.getSmsAlertsException() + "\n" + smsAlertsResponse.getException());
					else if (!smsAlertsResponse.isAuditable()
							&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
							&& smsAlertsResponse.getException() == null) {
						logger.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
								+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
								+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n"
								+ smsAlertsResponse.getSmsAlertsException());
					} else if (!smsAlertsResponse.isAuditable()
							&& GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
							&& smsAlertsResponse.getException() != null) {
						logger.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
								+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
								+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n"
								+ smsAlertsResponse.getException());
					} else if (smsAlertsResponse.isAuditable()
							&& !GenericValidator.isBlankOrNull(smsAlertsResponse.getSmsAlertsException())
							&& smsAlertsResponse.getException() != null)
						logger.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
								+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
								+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n"
								+ smsAlertsResponse.getSmsAlertsException() + "\n" + smsAlertsResponse.getAuditString());
					else {
						logger.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
								+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
								+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n"
								+ smsAlertsResponse.getAuditString());
					}
				} else {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						statusMessage = "Unable to Add Profile.";
					} else {
						statusMessage = "No se pudo Agregar.";
					}
					model.put("statusMessage", statusMessage);
					if (!smsAlertsResponse.isAuditable())
						logger.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
								+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
								+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n");
					else {
						logger.error("Error: calling ATH smsAdd \nacct: " + maskutil.mask(account.getAccountNumber())
								+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n"
								+ profile.getFullName() + "\nProfileID: " + profile.getProfileId() + "\n"
								+ smsAlertsResponse.getAuditString());
					}
				}
			} catch (Exception e) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					statusMessage = "Unable to Add Profile.";
				} else {
					statusMessage = "No se pudo agregar.";
				}
				model.put("statusMessage", statusMessage);
				logger.error(
						"Error: customerService.addSmsAlert(...) \nacct: " + maskutil.mask(account.getAccountNumber())
						+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + "\nProfileID: "
						+ profile.getProfileId(), e);

			}

		}
	}

	private void changeRecordInDB(CustomerProfile profile, SmsBean smsBeanRequest, CustomerServices customerService) {
		AccountMaskUtil maskutil = new AccountMaskUtil();
		try {

			if ((!GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chksms()) && smsBeanRequest
					.getPurchase_all_chksms().equalsIgnoreCase("1"))
					|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chksms()) && smsBeanRequest
							.getWithdraw_all_chksms().equalsIgnoreCase("1"))
							|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chkemail()) && smsBeanRequest
									.getPurchase_all_chkemail().equalsIgnoreCase("1"))
									|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chkemail()) && smsBeanRequest
											.getWithdraw_all_chkemail().equalsIgnoreCase("1"))) {

				/**
				 * MBNA-3445
				 * Method addATHAlertEveryTransactionProfile : CIBP-WEB microservice integration
				 * 
				 * */
				boolean added = AlertsFacade.getInstance().addATHAlertEveryTransactionProfile(profile.getProfileId(),
						smsBeanRequest.getAcctNum(), smsBeanRequest.getCardNum());
				
				if (!added) {
					logger.error("ATHEveryTransaction Record already exists in cibp database for profileID: "
							+ profile.getProfileId() + "\nacct: " + maskutil.mask(smsBeanRequest.getAcctNum())
							+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName());
				}
			}

			if ((GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chksms()) || smsBeanRequest
					.getPurchase_all_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chksms()) || smsBeanRequest
							.getWithdraw_all_chksms().equalsIgnoreCase("0"))
							&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chkemail()) || smsBeanRequest
									.getPurchase_all_chkemail().equalsIgnoreCase("0"))
									&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chkemail()) || smsBeanRequest
											.getWithdraw_all_chkemail().equalsIgnoreCase("0"))) {

				AlertTypesUtils.removeAlertEveryTransactionProfileById(customerService, profile, smsBeanRequest, logger);
			
			}

			if ((!GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_thr_chksms()) && smsBeanRequest
					.getPurchase_thr_chksms().equalsIgnoreCase("1"))
					|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_thr_chksms()) && smsBeanRequest
							.getWithdraw_thr_chksms().equalsIgnoreCase("1"))
							|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getDlyPurchasetotlschksms()) && smsBeanRequest
									.getDlyPurchasetotlschksms().equalsIgnoreCase("1"))
									|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getDlyWithdrawtotlschksms()) && smsBeanRequest
											.getDlyWithdrawtotlschksms().equalsIgnoreCase("1"))
											|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chksms()) && smsBeanRequest
													.getPurchase_all_chksms().equalsIgnoreCase("1"))
													|| (!GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chksms()) && smsBeanRequest
															.getWithdraw_all_chksms().equalsIgnoreCase("1"))) {

				boolean added = customerService.addATHSmsAlertProfile(profile.getProfileId(),
						smsBeanRequest.getAcctNum(), smsBeanRequest.getCardNum());
				if (!added) {
					logger.error("ATHSMSAlert Record already exists in cibp database for profileID: "
							+ profile.getProfileId() + "\nacct: " + maskutil.mask(smsBeanRequest.getAcctNum())
							+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName());
				}
				// return;
			}
			if ((GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_thr_chksms()) || smsBeanRequest
					.getPurchase_thr_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_thr_chksms()) || smsBeanRequest
							.getWithdraw_thr_chksms().equalsIgnoreCase("0"))
							&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyPurchasetotlschksms()) || smsBeanRequest
									.getDlyPurchasetotlschksms().equalsIgnoreCase("0"))
									&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyWithdrawtotlschksms()) || smsBeanRequest
											.getDlyWithdrawtotlschksms().equalsIgnoreCase("0"))
											&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chksms()) || smsBeanRequest
													.getPurchase_all_chksms().equalsIgnoreCase("0"))
													&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chksms()) || smsBeanRequest
															.getWithdraw_all_chksms().equalsIgnoreCase("0"))) {

				boolean removed = customerService.removeATHSmsAlertProfile(profile.getProfileId(),
						smsBeanRequest.getAcctNum(), smsBeanRequest.getCardNum());
				if (!removed) {
					logger.error("Could not remove ATHSMSAlert Record. DOES NOT exist in cibp database for profileID: "
							+ profile.getProfileId() + "\nacct: " + maskutil.mask(smsBeanRequest.getAcctNum())
							+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName());
				}
				// return;
			}

			// Paxus profiles regardless of what type alerts are configured.
			if (!((GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_thr_chksms()) || smsBeanRequest
					.getPurchase_thr_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_thr_chksms()) || smsBeanRequest
							.getWithdraw_thr_chksms().equalsIgnoreCase("0"))
							&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyPurchasetotlschksms()) || smsBeanRequest
									.getDlyPurchasetotlschksms().equalsIgnoreCase("0"))
									&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyWithdrawtotlschksms()) || smsBeanRequest
											.getDlyWithdrawtotlschksms().equalsIgnoreCase("0"))
											&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chksms()) || smsBeanRequest
													.getPurchase_all_chksms().equalsIgnoreCase("0"))
													&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chksms()) || smsBeanRequest
															.getWithdraw_all_chksms().equalsIgnoreCase("0"))

															&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_thr_chkemail()) || smsBeanRequest
																	.getPurchase_thr_chkemail().equalsIgnoreCase("0"))
																	&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_thr_chkemail()) || smsBeanRequest
																			.getWithdraw_thr_chkemail().equalsIgnoreCase("0"))
																			&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyPurchasetotlschkemail()) || smsBeanRequest
																					.getDlyPurchasetotlschkemail().equalsIgnoreCase("0"))
																					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyWithdrawtotlschkemail()) || smsBeanRequest
																							.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0"))
																							&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chkemail()) || smsBeanRequest
																									.getPurchase_all_chkemail().equalsIgnoreCase("0")) && (GenericValidator
																											.isBlankOrNull(smsBeanRequest.getWithdraw_all_chkemail()) || smsBeanRequest
																											.getWithdraw_all_chkemail().equalsIgnoreCase("0")))) {

				boolean added = customerService.addATHAnyAlertProfile(profile.getProfileId(),
						smsBeanRequest.getAcctNum(), smsBeanRequest.getCardNum());
				if (!added) {
					logger.error("ATHAnyAlert Record already exists in cibp database for profileID: "
							+ profile.getProfileId() + "\nacct: " + maskutil.mask(smsBeanRequest.getAcctNum())
							+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName());
				}
				// return;
			}
			// Paxus profiles regardless of what type alerts are configured.
			if ((GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_thr_chksms()) || smsBeanRequest
					.getPurchase_thr_chksms().equalsIgnoreCase("0"))
					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_thr_chksms()) || smsBeanRequest
							.getWithdraw_thr_chksms().equalsIgnoreCase("0"))
							&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyPurchasetotlschksms()) || smsBeanRequest
									.getDlyPurchasetotlschksms().equalsIgnoreCase("0"))
									&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyWithdrawtotlschksms()) || smsBeanRequest
											.getDlyWithdrawtotlschksms().equalsIgnoreCase("0"))
											&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chksms()) || smsBeanRequest
													.getPurchase_all_chksms().equalsIgnoreCase("0"))
													&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chksms()) || smsBeanRequest
															.getWithdraw_all_chksms().equalsIgnoreCase("0"))

															&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_thr_chkemail()) || smsBeanRequest
																	.getPurchase_thr_chkemail().equalsIgnoreCase("0"))
																	&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_thr_chkemail()) || smsBeanRequest
																			.getWithdraw_thr_chkemail().equalsIgnoreCase("0"))
																			&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyPurchasetotlschkemail()) || smsBeanRequest
																					.getDlyPurchasetotlschkemail().equalsIgnoreCase("0"))
																					&& (GenericValidator.isBlankOrNull(smsBeanRequest.getDlyWithdrawtotlschkemail()) || smsBeanRequest
																							.getDlyWithdrawtotlschkemail().equalsIgnoreCase("0"))
																							&& (GenericValidator.isBlankOrNull(smsBeanRequest.getPurchase_all_chkemail()) || smsBeanRequest
																									.getPurchase_all_chkemail().equalsIgnoreCase("0"))
																									&& (GenericValidator.isBlankOrNull(smsBeanRequest.getWithdraw_all_chkemail()) || smsBeanRequest
																											.getWithdraw_all_chkemail().equalsIgnoreCase("0"))) {

				boolean removed = customerService.removeATHAnyAlertProfile(profile.getProfileId(),
						smsBeanRequest.getAcctNum(), smsBeanRequest.getCardNum());
				if (!removed) {
					logger.error("Could not remove ATHAnyAlert Record. DOES NOT exist in cibp database for profileID: "
							+ profile.getProfileId() + "\nacct: " + maskutil.mask(smsBeanRequest.getAcctNum())
							+ "\ncard: " + maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName());
				}
				// return;
			}

		} catch (Exception e) {
			logger.error("Error adding/removing ATH SMS ALERTS PROFILE to/from cibp database for profileID: "
					+ profile.getProfileId() + "\nacct: " + maskutil.mask(smsBeanRequest.getAcctNum()) + "\ncard: "
					+ maskutil.mask(smsBeanRequest.getCardNum()) + "\n" + profile.getFullName() + "\n"
					+ ExceptionUtils.getFullStackTrace(e));
		}

	}

	public String getAlertsAccountTypes() {
		return alertsAccountTypes;
	}

	public void setAlertsAccountTypes(String alertsAccountTypes) {
		this.alertsAccountTypes = alertsAccountTypes;
	}
}