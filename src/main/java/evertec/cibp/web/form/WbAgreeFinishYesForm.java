package evertec.cibp.web.form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.CancellableFormController;

import evertec.cibp.core.models.signup.SignupCampaign;

public class WbAgreeFinishYesForm extends CancellableFormController {

	public WbAgreeFinishYesForm() {
		setCommandClass(SignupCampaign.class);
		setCommandName("wbaFinishyes");
		setFormView("wbaFinishyes");
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		return new ModelAndView("redirect:portal");
	}

}
