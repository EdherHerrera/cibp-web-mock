package evertec.cibp.web.form;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;
import org.springframework.web.servlet.mvc.CancellableFormController;

import com.google.common.collect.ImmutableList;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.models.base.RunningBalanceTransaction;
import evertec.cibp.core.models.configuration.ConfParameters;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.applications.ApplicationStatus;
import evertec.cibp.core.models.customer.applications.CustomerApplication;
import evertec.cibp.core.models.customer.applications.CustomerApplicationType;
import evertec.cibp.core.models.fraud.prevention.IncomingChecks;
import evertec.cibp.core.models.fraud.prevention.IncomingChecksDecision;
import evertec.cibp.core.models.fraud.prevention.IncomingChecksCustDecision;
import evertec.cibp.core.models.fraud.prevention.IncomingChecksStatus;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.TransactionRequest;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.core.utils.FraudPreventionUtils;
import evertec.cibp.web.form.bean.IncomingCheksBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.HasSectionUtil;
import evertec.cibp.web.utils.Utils;


public class IncomingChecksForm extends CancellableFormController {

	protected final Log myLogger = LogFactory.getLog(getClass());
	private CustomerServices customerServices;
	private MessageSource messageSource;
	private static final String incominCheksLbl = "incomingchecks";
	private String businessAccounts;
	
	public IncomingChecksForm() {
		super();
		setCommandClass(IncomingCheksBean.class);
		setCommandName(incominCheksLbl);
		setFormView(incominCheksLbl);
	}
	
	
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException error) throws Exception {
		Map<String, Object> model = new HashMap<>();
		IncomingChecks objIncochecks = new IncomingChecks();
		IncomingChecksCustDecision objIncoChecksDecisions = new IncomingChecksCustDecision();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<String> businessAccounts = ImmutableList.copyOf(this.getBusinessAccounts().split(","));
		List<String> noAvailEccounts = userSession.getCustomerService().getDCINotAvailableEaccounts(userSession.getCustomerProfile());
		CustomerProfile customer = userSession.getCustomerProfile();
		
		for (Iterator<TVAccountTransferSource> iterator = userSession.getAccountsPaymentFrom().iterator(); iterator.hasNext();) {
			
			TVAccountTransferSource account = iterator.next();
			List<IncomingChecks> incomingChecks = customerServices.getIncomingChecksByAcct(account.getAccount().getAccountNumber(), IncomingChecksStatus.NEW);
			List<CustomerApplication> listCustApp = Utils.getCustomerAppbyType(userSession.getCustomerApplications(), CustomerApplicationType.VIEW_HISTORY_CHECKS_MODIFY, ApplicationStatus.APPROVED);
			List<String> acctArray  = FraudPreventionUtils.getAccountsbyAppType(listCustApp);
			// If the current account is not a business account type nor in the enrolled account list, then skip search
			if (!(businessAccounts.contains(account.getAccount().getProductDefinition().getProductId()) && acctArray.contains(account.getAccount().getAccountNumber()))) {
				continue;
			}
			
			if(!incomingChecks.isEmpty() && !noAvailEccounts.contains(account.getAccount().getAccountNumber()))
			{
				for (Iterator<IncomingChecks> iteraCheks = incomingChecks.iterator(); iteraCheks.hasNext();) {
					boolean isModify = false;
					IncomingChecks incomingC = iteraCheks.next();
					String parameterName = "decision"+incomingC.getIncomingCheckId();
					String decision = request.getParameter(parameterName);
					IncomingChecksDecision decisionF = IncomingChecksDecision.PAY;
					String action="N/A";
					if(decision.equalsIgnoreCase("PAY") && incomingC.getDecision() != IncomingChecksDecision.PAY)
					{
						decisionF = IncomingChecksDecision.PAY;
						action = "Return to Pay";
						isModify = true;
					}else if(decision.equalsIgnoreCase("RETURN") && incomingC.getDecision() != IncomingChecksDecision.RETURN){
						decisionF = IncomingChecksDecision.RETURN;
						action = "Pay to Return";
						isModify = true;
					}
					if(isModify){
						objIncochecks.setDecision(decisionF);
						objIncochecks.setDecisionDate(new Date());
						objIncochecks.setAccountNumber(incomingC.getAccountNumber());
						objIncochecks.setCheckId(incomingC.getCheckId());
						objIncochecks.setAmount(incomingC.getAmount());
						objIncochecks.setIssueType(incomingC.getIssueType());
						objIncochecks.setIssueDate(incomingC.getIssueDate());
						objIncochecks.setLine(incomingC.getLine());
						objIncochecks.setUploadDate(incomingC.getUploadDate());
						objIncochecks.setIncomingCheckId(incomingC.getIncomingCheckId());
						objIncochecks.setProfileIdDecision(userSession.getCustomerProfile().getProfileId());
						objIncochecks.setUserName(userSession.getCustomerProfile().getUsername());

						customerServices.updateIncomingChecksDesicion(objIncochecks);
						
						//Save Decision History
						objIncoChecksDecisions.setAccountName(account.getAccount().getNickname());
						objIncoChecksDecisions.setAccountNumber(incomingC.getAccountNumber());
						objIncoChecksDecisions.setAction(action);
						objIncoChecksDecisions.setAmount(incomingC.getAmount());
						objIncoChecksDecisions.setBankId(account.getAccount().getBankId());
						objIncoChecksDecisions.setCheckId(incomingC.getCheckId());
						objIncoChecksDecisions.setDecision(decisionF);
						objIncoChecksDecisions.setDecisionDate(new Date());
						objIncoChecksDecisions.setIncomingCheckId(incomingC.getIncomingCheckId());
						objIncoChecksDecisions.setProfileIdDecision(userSession.getCustomerProfile().getProfileId());
						objIncoChecksDecisions.setDecisionUsername(userSession.getCustomerProfile().getUsername());
						customerServices.addIncomingCheckDecision(objIncoChecksDecisions);
						if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
							userSession.setStatusmessage(messageSource.getMessage("fraud.service.incoming.checks.decision.sent", null, new Locale("EN")));
						 else 
							userSession.setStatusmessage(messageSource.getMessage("fraud.service.incoming.checks.decision.sent", null, new Locale("ES")));
						
						model.put("sentDecision", true);
					
					}
				}
				
				
			}		
			
		}
		if (model.get("sentDecision") != null && ((Boolean) model.get("sentDecision")).booleanValue()) {
			customerServices.sendViewIncomingChecksAccountsDecisionsEmail(customer, userSession.getPreferredLanguage());
		}
		
		return showForm(request, response, error, model);
	}

    @Override
    protected Map<String, Object> referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception
    {
        Map<String, Object> model = new HashMap<>();
        List<IncomingCheksBean> list = new ArrayList<>();
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        List<String> businessAccounts = ImmutableList.copyOf(this.getBusinessAccounts().split(","));
        List<String> noAvailEccounts = userSession.getCustomerService().getDCINotAvailableEaccounts(userSession.getCustomerProfile());

        //Parameter: 1=Fraud Prevention Service Section(Table: PROFILE_TYPE_SECTION)
        List<String> notAllowedAccts = HasSectionUtil.hasAccountsSectionNotAllowed(HasSectionUtil.FRAUD_PREVENTION_SERVICE_SECTION, userSession);

        List<CustomerApplication> listCustApp = Utils.getCustomerAppbyType(userSession.getCustomerApplications(), CustomerApplicationType.VIEW_HISTORY_CHECKS_MODIFY, ApplicationStatus.APPROVED);
        List<String> acctArray  = FraudPreventionUtils.getAccountsbyAppType(listCustApp);
        for (Iterator<TVAccountTransferSource> iterator = userSession.getAccountsPaymentFrom().iterator(); iterator.hasNext();) {
            TVAccountTransferSource account = iterator.next();

            // If the current account is not a business account type nor in the enrolled account list, then skip search
            if (!(businessAccounts.contains(account.getAccount().getProductDefinition().getProductId()) && acctArray.contains(account.getAccount().getAccountNumber()))) {
                continue;
            }

			Map<Object, Object> searchCriteria = new HashMap<>();
			searchCriteria.put("accountNumber", account.getAccount().getAccountNumber());
			searchCriteria.put("IncomingChecksStatus", IncomingChecksStatus.NEW);
            List<IncomingChecks> incomingChecks = customerServices.getIncomingChecksByFilter(searchCriteria, 1000000);
            List<AbstractTransaction> transactions =getTransactionsImages(account.getAccount(),userSession,request);

            if(!incomingChecks.isEmpty() && !noAvailEccounts.contains(account.getAccount().getAccountNumber()) && !notAllowedAccts.contains(account.getAccount().getAccountNumber())) {
                Boolean parent = true;
                for (Iterator<IncomingChecks> iteraCheks = incomingChecks.iterator(); iteraCheks.hasNext(); ) {
                    IncomingCheksBean bean = new IncomingCheksBean();
                    IncomingChecks incomingC = iteraCheks.next();
                    for (Iterator<AbstractTransaction> itTransaction = transactions.iterator(); itTransaction.hasNext(); ) {
                        AbstractTransaction abstractTransaction = itTransaction.next();

                        if (StringUtils.containsIgnoreCase(abstractTransaction.getDescription(), incomingC.getCheckId())) {
                            RunningBalanceTransaction runningBalanceTransaction = (RunningBalanceTransaction) abstractTransaction;
                            bean.setImageKey(runningBalanceTransaction.getImageKey());
                            bean.setHasImage(runningBalanceTransaction.isHasImage());
                          
                            String frontEndImageKey = evertec.cibp.core.utils.Utils.stringToSha256(runningBalanceTransaction.getImageKey());
                            bean.setFrontEndImageKey(frontEndImageKey);
                            
                            if (userSession.getCheckImageMap() == null) {
								userSession.setCheckImageMap(new HashMap<>());
							}
                            
                            userSession.getCheckImageMap().put(frontEndImageKey, runningBalanceTransaction.getImageKey());
                        }
                    }
                    
                    AccountMaskUtil maskutil = new AccountMaskUtil();
                    
                    bean.setParent(parent);
                    bean.setIncomingCheckId(incomingC.getIncomingCheckId());
                    bean.setAccountNumber(maskutil.mask(account.getAccount().getAccountNumber()));
                    bean.setFrontEndId(account.getAccount().getFrontEndId());
                    bean.setDecision(incomingC.getDecision());
                    bean.setCheckId(incomingC.getCheckId());
                    bean.setAmount(incomingC.getAmount());
                    bean.setAccountNickName(account.getAccount().getNickname());
                    String newDate = new SimpleDateFormat("dMMMY").format(incomingC.getIssueDate());

                    model.put("issueDate", newDate);
                    list.add(bean);
                    parent = false;

                }
            }
        }
        ConfParameters confParameters = customerServices.getConfParametersByShortNameAndApp("CUT_OFF_TIME", "FRAUD_PREVENTION");
        String cutOffTimeEnd = confParameters.getValue(); //Esta hora de corte viene de la DB en la table CONF_PARAMETERS
        String inputFormat = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.US);


        model.put("cutOffTimeEnd", customerServices.isCutOffTimeEnd(cutOffTimeEnd));
        model.put("cutOffTime", sdf.format(inputParser.parse(cutOffTimeEnd)));
        model.put("acconutList", list);
        model.put("statusFraudPrevApp", userSession.getFraudPrevAppStatus());
        model.put("hasChecks", list.size() > 0);
        model.put("sentDecision", false);
        return model;
    }
	
	
	private List<AbstractTransaction> getTransactionsImages(TVFrontendAccount account, UserSession userSession,
			HttpServletRequest request) {
		List<AbstractTransaction> transactions = new ArrayList<>();
		int maxTransactions = 50;
		try {
			TransactionRequest depreq2 = new TransactionRequest(Utils.generateCustomerInteractionEvent(request), null,
					userSession.getCustomerProfile(), userSession.getBankingSession(), account, maxTransactions, 1,
					true, null, null, null);
			TransactionsResponse transactionResponse = userSession.getCustomerService()
					.getDepositCurrentTransactions(depreq2);
			transactions.addAll(transactionResponse.getTransactions());

		} catch (Exception e) {
			logger.error("Error getting check image url: ", e);
		}

		return transactions;
	}

	
	public CustomerServices getCustomerServices() {
		return customerServices;
	}
	
	public void setCustomerServices(CustomerServices customerServices) {
		this.customerServices = customerServices;
	}
	
	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	public String getBusinessAccounts() {
		if (businessAccounts == null) {
			businessAccounts = "";
		}
		return businessAccounts;
	}

	public void setBusinessAccounts(String businessAccounts) {
		this.businessAccounts = businessAccounts;
	}
}
