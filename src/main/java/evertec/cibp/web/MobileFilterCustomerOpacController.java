package evertec.cibp.web;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.accountopening.dci.DCICustomer;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.requests.opac.CISOpacCustomerInfoRequest;
import evertec.cibp.core.services.responses.opac.CISOpacCustomerInfoResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.MobileApiSettingsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * Used by iOS app to determine if profile isnt from Tortola..
 * 
 * @author et56169
 * 
 */
@SuppressWarnings("unused")
public class MobileFilterCustomerOpacController extends MultiActionController implements MessageSourceAware {

	
	private static final Logger logger = Logger.getLogger(WebAppController.class);
	private MessageSource messageSource;
	private CustomerServices customerService;
	private String opacUrl;//CIBPServices

	public void initialize() {
		// TODO Auto-generated method stub
	}

	protected CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	
	public String getOpacUrl() {
		return opacUrl;
	}

	public void setOpacUrl(String opacUrl) {
		this.opacUrl = opacUrl;
	}
	
	public ModelAndView customerCisOpac(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//TODO mover al usersession el llamado. Cambiar para que devuelva el pais
		Map<String, Object> model = new HashMap<>();
		String country = "";
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC)){
			country = userSession.getCountry();	
		}
		model.put("isCommercial", (userSession.getCustomerProfile().getUserInterface() == UserInterface.COMMERCIAL));
		model.put("country", country);
		model.put("flagOpac", StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC));
		return new ModelAndView("customerCisOpac", model);
	}
}
