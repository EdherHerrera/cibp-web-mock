package evertec.cibp.web.enums;

/**
 * Unica Authentication Code
 * @author Raul Mora
 *
 */
public enum UnicaAuthCode { 
	NO_AUTHENTICATION_PRESENTED("000"),
	OOB_AUTHENTICATION_SUCCESS("103"),
	EMAIL_AUTHENTICATION_SUCCESS("105"),
	EMAIL_AUTHENTICATION_FAIL("005");
	
	private String code;
	
	private UnicaAuthCode(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	
}
