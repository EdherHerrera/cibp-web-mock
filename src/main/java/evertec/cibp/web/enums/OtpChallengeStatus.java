package evertec.cibp.web.enums;

/**
 * Otp Challenge Status
 * @author jnieves
 *
 */
public enum OtpChallengeStatus { 
	OTP_SERVICE_SUCCESS, 
	OTP_SERVICE_FAILED, 
	RESEND_LIMIT_REACHED,
	RESEND_LIMIT_EXCEEDED,
	VALIDATION_SUCCESS,
	VALIDATION_FAILED,
	VALIDATION_LIMIT,
	CODE_EXPIRED,
	MISSING_INFO;
}
