package evertec.cibp.web.enums;

public enum UserAgentType {
	WEB, MOBILE_DESKTOP, MOBILE, IPHONE, IPAD, ANDROID, JSON;
}
