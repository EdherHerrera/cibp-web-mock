package evertec.cibp.web.enums;

public enum MobileResponseStatus {

	SUCCESS, ERROR, NOT_FOUND, ERROR_VTS, DISABLE;
}
