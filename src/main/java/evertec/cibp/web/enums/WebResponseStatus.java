package evertec.cibp.web.enums;

public enum WebResponseStatus {
	SUCCESS, FAILED, MAINTENANCE;
}
