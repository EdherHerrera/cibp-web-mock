/**
 * 
 */
package evertec.cibp.web.enums;

/**
 * The type of statement credit configured for the account.
 * Threshold credits allow customers to configure a minimum amount to redeem for.
 * Monthly credits allow customers to redeem for a set amount each month. Values: THRESHOLD, MONTHLY
 */
public enum RecurringCashBackType {
    MONTHLY, THRESHOLD
}