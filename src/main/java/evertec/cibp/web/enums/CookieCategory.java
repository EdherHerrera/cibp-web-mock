package evertec.cibp.web.enums;

public enum CookieCategory {
	ANALYTICS, ADVERTISING, FUNCTIONAL, STRICTLY;
}
