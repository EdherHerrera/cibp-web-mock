package evertec.cibp.web.resources;

/**
 * Interface for get class variables.
 * @author ET58344, Evertec Inc.
 * @since 06-11-2021
 * @version 1.0
 */
public interface GDPRResources {

    String INFORMATION_TITLE = "informationTitle"; //title
    String INFORMATION_TEXT = "informationText"; //text
    String STRICTLY_COOKIES_TITLE = "strictlyTitle"; //title
    String STRICTLY_COOKIES_TEXT = "strictlyText"; //text
    String ANALYTICS_TITLE = "analyticsTitle"; //title
    String ANALYTICS_TEXT = "analyticsText"; //text
    String FUNCTIONAL_TITLE = "functionalTitle"; //title
    String FUNCTIONAL_TEXT = "functionalText"; //text
    String ADVERTISING_TITLE = "advertisingTitle"; //title
    String ADVERTISING_TEXT = "advertisingText"; //text
    String STR_JSON_INFORMATION = "Information"; //json object
    String STR_JSON_CATEGORIES = "CookieCategories"; //json array
    String STR_CATEGORY = "category"; //str category
    String STR_STRICTLY = "strictly"; //str strictly
    String STR_ESTRICTAMENTE = "estrictamente"; //str estrictamente
    String STR_TITLE = "title"; //str title
    String STR_TEXT = "text"; //str text
    String STR_ANALYTICS = "analytics"; //str analytics
    String STR_RENDIMIENTO = "rendimiento"; //str rendimiento
    String STR_FUNCTIONAL = "functional"; //str functional
    String STR_FUNCIONAL = "funcional"; //str funcional
    String STR_ADVERTISING = "advertising"; //str advertising
    String STR_PUBLICIDAD = "publicidad"; //str publicidadS
    String EXCEPTION_MSG = "Error ocurred in getJSONFromUrl"; //error message
    String ENABLE = "ENABLE"; //enable string
}
