package evertec.cibp.web.resources;

/**
 * 
 * @author Evertec CIBP
 * @since
 * @version 1.0
 *
 */
public interface StringResources {

	/**
	 * Remote Deposit Strings
	 */
	String REMOTE_DEPOSIT_ERROR_GENERIC = "remotedeposit.errors.generic"; // string resource error
	String REMOTE_DEPOSIT_ERROR_MAINTENANCE = "remoteDeposit.errors.maintenance"; // string resource error
	String REMOTE_DEPOSIT_ERROR_AMOUNT_LARGE_MAX = "remoteDeposit.errors.amount.largermax"; // Error validation
	String REMOTE_ERROR_AMOUNT = "remoteDeposit.errors.amount"; // Error validation
	String REMOTE_DEPOSIT_ERROR_FRONT_IMAGE = "remoteDeposit.errors.frontImage"; // Error validation
	String REMOTE_DEPOSIT_ERROR_BACK_IMAGE = "remoteDeposit.errors.backImage"; // Error validation
        
}
