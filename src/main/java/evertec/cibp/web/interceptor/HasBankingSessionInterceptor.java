package evertec.cibp.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.web.session.UserSession;

public class HasBankingSessionInterceptor extends HandlerInterceptorAdapter {

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		if (null != userSession && userSession.getBankingSession() != null) {
			return true;
		} else {
			if (userSession != null)
				userSession.setStatusmessage("This Section is not available for your profile");
			throw new ModelAndViewDefiningException(new ModelAndView("forward:portal"));

		}
	}

}
