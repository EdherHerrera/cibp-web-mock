package evertec.cibp.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

public class RewardsInterceptor extends HandlerInterceptorAdapter implements MessageSourceAware {

	private MessageSource messageSource;

	public RewardsInterceptor() {
		super();
	}
	@Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
        String frontEndId = "frontEndId";
        TVFrontendAccount ccaAccountByFrontEndId = null;

        ccaAccountByFrontEndId = Utils.getAccountById(request.getParameter(frontEndId),
                userSession.getCcaAccounts());
        
        if(ccaAccountByFrontEndId == null) {
            response.sendRedirect(request.getContextPath() + "/actions/portal");
            return false;
        }

        if ((StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS) 
                && Utils.cashRewardsEligible(userSession,ccaAccountByFrontEndId))) {
            return true;
        } else {
            response.sendRedirect(request.getContextPath() + "/actions/portal");
            return false;
        }
    }
	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

}
