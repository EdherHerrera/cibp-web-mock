/**
* RegainAccessFlagInterceptor
* Project: CIBP
* Company: Evertec inc
*/
package evertec.cibp.web.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;

/**
 * Regain Access Interceptor
 *
 * @author Eduardo Agosto
 * @since august-2019
 * @see evertec.cibp.web.form.RegainAccessForm
 * @version 1.0
 */
public class RegainAccessFlagInterceptor extends HandlerInterceptorAdapter {

	/**
	 * Old ResetPassword view.
	 */
	private static final String RESET_PASSWORD_OLD = "old/resetpassword?resource="; //to do resetpassword old url
	
	/**
	 * Resource String.
	 */
	private static final String RESOURCE = "resource"; //

	/**
	 * CustomerService instance.
	 */
	private CustomerServices customerServices; //
	
	/**
	 * RegainAccessFlagInterceptor
	 */
	public RegainAccessFlagInterceptor () {
		this.customerServices = null;
	}
	
	/**
	 * RegainAccessFlagInterceptor
	 * @param customerServices
	 */
	public RegainAccessFlagInterceptor (CustomerServices customerServices) {
		this();
		this.customerServices = customerServices;
	}

	/**
	 * @param request
	 * @param response
	 * @param handler
	 * @return boolean
	 * @throws ServletRequestBindingException 
	 * @throws IOException 
	 * @throws AccessControlException 
	 */
	@Override
	public boolean preHandle (HttpServletRequest request, HttpServletResponse response, Object handler) 
			throws ServletRequestBindingException, IOException, AccessControlException {
		final boolean allowed = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC224); //
		
		if (!allowed) {
			final String resource = ServletRequestUtils.getStringParameter(request, RESOURCE); //
			
			ESAPI.httpUtilities().sendRedirect(response, request.getContextPath() + "/actions/" + RESET_PASSWORD_OLD + resource);
		}
		
		return allowed;
	}
	
	/**
	 * @return the customerServices
	 */
	public CustomerServices getCustomerServices () {
		return customerServices;
	}

	/**
	 * @param customerServices the customerServices to set
	 */
	public void setCustomerServices (CustomerServices customerServices) {
		this.customerServices = customerServices;
	}

}

