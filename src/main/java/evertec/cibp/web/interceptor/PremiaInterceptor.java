/**
 * 
 */
package evertec.cibp.web.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.utils.Utils;

/**
 * Intercepts all PREMIA accounts.
 * 
 * @author Ariel D. Salazar H.
 * @created 2020-04-07 00:00
 * @since 1.0
 * @version 1.8
 * @see HandlerInterceptorAdapter
 *
 */
public class PremiaInterceptor extends HandlerInterceptorAdapter {
	
	/**
	 * {@inheritDoc}
	 * 
	 * @throws AccessControlException
	 * @throws IOException 
	 */
	@Override
	public void postHandle (HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws AccessControlException, IOException {
		
		final boolean isPremEnabled = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559);

		if (isPremEnabled) {
			
			Utils.removePremiaOptions(modelAndView);
			urlAccessCheck(request, response, modelAndView);
		}
	}
	
	/**
	 * Check access to PREMIA if you write the url directly
	 * @param request
	 * @param response
	 * @param modelAndView
	 * @throws IOException 
	 * @throws AccessControlException 
	 */
	private void urlAccessCheck (HttpServletRequest request, HttpServletResponse response,
			ModelAndView modelAndView) throws IOException, AccessControlException {
		
		final String portalLocation = "/actions/portal"; // portal location
		
		final boolean hasRedirect = Utils.urlAccessCheck(modelAndView); // redirect flag
		
		if (hasRedirect) {
			
			// redirect to portal
			ESAPI.httpUtilities().sendRedirect(response, request.getContextPath() + portalLocation);
		}
	}
	
}
