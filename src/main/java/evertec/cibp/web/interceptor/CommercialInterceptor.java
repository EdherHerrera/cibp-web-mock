package evertec.cibp.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.applications.ApplicationStatus;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.UserSession;

public class CommercialInterceptor extends HandlerInterceptorAdapter implements MessageSourceAware {
	 
	private MessageSource messageSource;	
	private String vendorId;
	private String payrollId;
	
	
	
	public CommercialInterceptor() {
		super();
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR)){
			//Este Interceptor es solo para funcionalidades de clientes Comerciales
			if (profile.getUserInterface() != UserInterface.COMMERCIAL) {
				response.sendRedirect(request.getContextPath() + "/actions/welcome");	
			}else{				
				//Validamos Path especificos para validaciones puntuales en cada Path
				if(modelAndView.getViewName().equalsIgnoreCase("epayrollservice")){
					if(userSession.getPayrollAppStatus() != null && (userSession.getPayrollAppStatus().equals(ApplicationStatus.NEW) || userSession.getPayrollAppStatus().equals(ApplicationStatus.APPROVED)))
						response.sendRedirect(request.getContextPath() + "/actions/business");				
				}
			}
		}
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE)){
			if (profile.getUserInterface() != UserInterface.COMMERCIAL) {
				response.sendRedirect(request.getContextPath() + "/actions/welcome");	
			}else{				
				//Validamos Path especificos para validaciones puntuales en cada Path
				
			}
		}
	}	
	
	
	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	public String getPayrollId() {
		return payrollId;
	}
	
	public void setPayrollId(String payrollId) {
		this.payrollId = payrollId;
	}
	
	public String getVendorId() {
		return vendorId;
	}
	
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

}
