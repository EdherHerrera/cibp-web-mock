package evertec.cibp.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import evertec.cibp.web.utils.CookieUtils;

public class LocationInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		if (request.getParameter("vi") != null) {
			CookieUtils.addCookie(response, -1, "vi", "vi");
		} else if (request.getParameter("pr") != null) {
			CookieUtils.removeCookie(response, "vi");
		}
		return true;

	}

}
