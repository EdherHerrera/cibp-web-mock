package evertec.cibp.web.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.SessionInitializationController;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.NavigationSequenceUtils;

public class SignonTasksInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = Logger.getLogger(SignonTasksInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		// If the userSession does not exist or if a logged in customer tries to
		// go through any of the login steps we redirect him to the first login
		// page

		String referer = request.getHeader("REFERER");

		if (userSession == null) {
			logger.warn("No userSession object found for requested session id: " + request.getRequestedSessionId()
					+ " REFERER:" + referer + "  CLIENT IP: " + request.getRemoteAddr() + " USER-AGENT:"
					+ request.getHeader("User-Agent") + " URI:" + request.getRequestURI());
			throw new ModelAndViewDefiningException(new ModelAndView("redirect:login", "noSessionError", null));
		} else {

			boolean wasContinue = validateRedirects(userSession, request, response, handler);
			if (!wasContinue) {
				return wasContinue;
			}
			
			boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);
			//verify if user is login, the oob flag is true, the user is oob enrolled and the tasks list oob is not empty and null 
			if(userSession.isLoginCompleteSuccess() && oobFlag && userSession.getOobEnroll() && userSession.getTasksListOOB() != null && !userSession.getTasksListOOB().isEmpty()){
				//if navigation is oob, then validate oob classes  
				ASK_FOR_OOB thingOOB = userSession.getTasksListOOB().get(0);
				if (handler.getClass() == NavigationSequenceUtils.getAskClass(thingOOB))
					return true;
				else
					throw new ModelAndViewDefiningException(new ModelAndView(NavigationSequenceUtils.getAskView(thingOOB)));
			}
			// We get the next task in order
			ASK_FOR thing = userSession.getTasksList().get(0);
			if (handler.getClass() == SignonSequenceUtils.getAskClass(thing))
				return true;
			else
				throw new ModelAndViewDefiningException(new ModelAndView(SignonSequenceUtils.getAskView(thing)));

			// response.sendRedirect(SignonSequenceUtils.getAskView(thing));
			// return false;
			// inside the app
		
		}

	}
	
	protected boolean validateRedirects(UserSession userSession, HttpServletRequest request, HttpServletResponse response, Object handler) 
			throws IOException, AccessControlException {
		boolean result = true;
		
		if (userSession.getTasksList().size() == 0) {
			if (handler.getClass() != SessionInitializationController.class) {

				logger.warn("No More tasks Found. Handler Class is not Session Initialization Controller. ("
						+ handler.getClass() + ")  Username:" + userSession.getUsername());

				ESAPI.httpUtilities().sendRedirect(response, request.getContextPath() + "/actions/login");
			} else {
				ESAPI.httpUtilities().sendRedirect(response, request.getContextPath() + "/actions/portal");
			}

			result = false;
		} else if (userSession.getIsLoginInterruptionEnable()) {
			ESAPI.httpUtilities().sendRedirect(response, request.getContextPath() + "/actions/oobInterruptionPage");
			result = false;
		}
		
		return result;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		if (modelAndView != null && modelAndView.getViewName() != null
				&& !modelAndView.getViewName().startsWith("redirect:"))
			if (WebUtils.getCookie(request, "vi") != null)
				modelAndView.getModelMap().put("vi", true);
	}

}
