package evertec.cibp.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.web.form.interruptions.OOBInterruptionForm;
import evertec.cibp.web.session.UserSession;

public class LoggedinInterceptor extends HandlerInterceptorAdapter {

	public Logger logger = Logger.getLogger(LoggedinInterceptor.class);

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		String referer = request.getHeader("REFERER");

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		// If the userSession does not exist or if a logged in customer tries to
		// go through any of the login steps we redirect him to the first login
		// page

		if (null == userSession || !userSession.isLoginCompleteSuccess()) {

			if (request.getRequestedSessionId() != null && userSession == null) {

				logger.warn("No Session for requested session id: " + request.getRequestedSessionId() + " REFERER:"
						+ referer + "  CLIENT IP: " + request.getRemoteAddr() + " USER-AGENT:"
						+ request.getHeader("User-Agent") + " URI:" + request.getRequestURI());

			} else
				logger.warn("No Session found. REFERER:" + referer + "  CLIENT IP: " + request.getRemoteAddr()
						+ " USER-AGENT:" + request.getHeader("User-Agent") + " URI:" + request.getRequestURI());

			if (request.getPathInfo().startsWith("/pfm")) {
				response.sendRedirect("pfmRedirectLogin");
				return false;
			} else {
				throw new ModelAndViewDefiningException(new ModelAndView("redirect:login", "noSessionError", null));
			}
		} else {
			boolean canContinue = true;
			if (!OOBInterruptionForm.canContinue(userSession, request)) {
				ESAPI.httpUtilities().sendRedirect(response, request.getContextPath() + "/actions/loginInterruptionPage");
				canContinue = false;
			}
 			return canContinue;
		}

	}

}
