package evertec.cibp.web.interceptor;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.session.EnrollmentSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * UserAgentInterceptor
 * @author et55596
 * @since 1.2
 * @version 1.0 
 */
public class UserAgentInterceptor extends HandlerInterceptorAdapter {

    /**constants logger */
	private static final Logger logger = Logger.getLogger(UserAgentInterceptor.class);
	
	/**constants views */
	private List<String> views = new LinkedList<String>();
	
	/**constants jsonUserAgent */
	private static final String jsonUserAgent = "JSON Client";
	
	private static final String athMovilApp = "ATH Movil";

	/**
	 * Mover al xml la lista de views
	 */
	public UserAgentInterceptor() {
		super();
		final String resetpassword = "resetpassword"; //
		final String regainaccess = "regainaccess"; //
		final String regainaccessEmailSent = "regainaccessEmailSent"; //
		final String regainaccessEmailBlocked = "regainaccessEmailBlocked"; //
		final String bsmartEnrollAlertview = "bsmartEnrollAlert";//
		final String cookiePreferenceView = "cookiePreferenceView";//Element to the list
		final String transAddScreen = "transAddScreen"; //transAddScreen
		
		views.add("login");
		views.add("question");
		views.add("loginoob");
		views.add("password");
		views.add("portal");
		views.add("quickpayment");
		views.add("addexistingpayee");
		views.add("makeTransfer");
		views.add("transaction");
		views.add("billpaydowntime");
		views.add("error");
		views.add("signondowntime");
		views.add("transfersdowntime");
		views.add("terms-en");
		views.add("terms-sp");
		views.add("contactus");
		views.add(resetpassword);
		views.add("mobileGetBalanceInfo");
		views.add("mobileRefreshBalances");
		views.add("mobileTransactionInfo");
		views.add("mobileMakeTransfer");
		views.add("mobileCheckRsaChallenge");
		views.add("detail");
		views.add("globalStatus");
		views.add("ebppInbox");
		views.add("ebpp2Inbox");
		views.add("ebpp");
		views.add("ebppPortalInbox");
		views.add("ebpp2PortalInbox");
		views.add("ebppLatestHistory");
		views.add("ebppnostatement");
		views.add("ebppPreviousHistoryInfoEbills");
		views.add("ebppPreviousHistoryInfoEstatement");
		views.add("ebppsorry");
		views.add("ebppStatusUpdate");
		views.add("ebppViewBiller");
		views.add("transferhistory");
		views.add("paymenthistory");
		views.add("enrollment");
		views.add("enrollmentComercial");
		views.add("welcome");
		views.add("customerEntitlements");
		views.add("remoteDepositEasyAcceptTerms");
		views.add("getRemoteDepositEasyAcceptTerms");
		views.add("remotedeposithistory");
		views.add("remotedeposithistoryimages");
		views.add("remotedepositrdcenroll");
		views.add("remotedepositsubmitdeposit");
		views.add("eSignForm");
		views.add("athmenroll");
		views.add("athmsendmoney");
		views.add("athmssotoken");
		views.add("athmssounbind");
		views.add("mobileCashAccounts");
		views.add("mobileCashTrxSubmit");
		views.add("mobileCashTrxAssignAtm");
		views.add("liteEnrollmentVerification");
		// Token OPAC
		views.add("tokenOpac");
		views.add("customerCisOpac");
		// Down-times
		views.add("downtime");
		views.add("athmovildowntime");
		views.add("sosdowntime");
		views.add("oaodowntime");
		views.add("transactionsdowntime");
		views.add("pfmdowntime");
		views.add("ezstmtdowntime");
		views.add("campaignsdowntime");
		views.add("bankingdowntime");
		views.add("mobile_bankingdowntime");
		views.add("ebillsdowntime");
		views.add("aggregationdowntime");
		views.add("alertsdowntime");
		views.add("enrollmentdowntime");
		views.add("account_openingdowntime");
		views.add("guided_setupdowntime");
		views.add("rdcdowntime");
		views.add("rsadowntime");
		views.add("mobile_cashdowntime");
		views.add("mobileTokenization");
		views.add("outreach");
		views.add("hasMobilePhoneAlerts");
		views.add("mobilePhoneProviders");
		views.add("generateSmsCode");
		views.add("createLiteProfile");
		views.add("validateSmsCode");
		views.add("isCustomerLiteEnrolled");
		views.add("liteCustomerEnrollmentInfoSubmit");
		views.add("addCustomerDevice");
		views.add("mobileCashTrxSubmitV1");
		views.add("mobileCashAccountsV1");
		views.add("mobileCashTrxAssignAtmV1");
		views.add("mobileAthOnOff");
		views.add("mobileAddPayee");
		views.add("mobileEditPayee");
		views.add("mobileAddPayeeAjax");
		views.add("mobileAddPayeeInfo");
		views.add("mobileAlerts");
		views.add("mobileAlertsJson");
		views.add("ccServices");

		views.add("loginSsdsForced");

		views.add("pushTokens");
		views.add("pushTokenResponse");

		views.add("rsaForm");
		views.add("rsaMessage");
		views.add("json");

		views.add("notificationCenter");
		//MBFIS
	    views.add("accountinformationsec");
	    views.add("resetpasswordsec");

		views.add("rewardsRedemption");
		views.add("redemptionConfiguration");
		views.add("redemptionConfirmation.vm");

		views.add(regainaccess);
		views.add(regainaccessEmailSent);
		views.add(regainaccessEmailBlocked);
		views.add(transAddScreen);
		views.add("doubleprofileValidationScreen");
		
		views.add(bsmartEnrollAlertview);

		views.add("acceptMarketplaceTerms");
		views.add("generateOtpCode");
		views.add("validateOtpCode");
		views.add("determineAuthenticationChallenge");
		
		views.add("tsysLoyaltyRewardsRedirection");
		views.add("tsysLoyaltyRewardsAccountInfo");
		views.add("getUnicaUrl");
		views.add("unicainformation");

		views.add("changeEmailMobile");
		views.add("editemail");

		views.add(cookiePreferenceView);
		views.add("gdprPrivacySettings");
		
		views.add("loginInterruptionPage");
		views.add("oobInterruptionPage");
	}

	/**
	 * preHandle
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @param handler Object
	 * @return true 
	 * @throws Exception
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		this.analyzeUserAgentType(request);

		return true;
	}

	/**
	 * postHandle
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @param handler Object
	 * @param modelAndView ModelAndView
	 * @throws Exception
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.RSA_MOBILE) && !views.contains("loginRsaEnroll")) {
	    		views.add("loginRsaEnroll");
	    }

		if (!StaticMessageSource.isFlagEnabled(StaticMessageSource.RSA_MOBILE) && views.contains("loginRsaEnroll")) {
			views.remove("loginRsaEnroll");
		}

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1531) && !views.contains("loginSsdsForced")) {
    		views.add("loginSsdsForced");
	    }

		if (!StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1531) && views.contains("loginSsdsForced")) {
			views.remove("loginSsdsForced");
		}

		String viewName = null;
		UserAgentType agentType = UserAgentType.WEB;

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		EnrollmentSession enrollmentSession = (EnrollmentSession) WebUtils.getSessionAttribute(request,
				"enrollmentSession");

		if (modelAndView != null)
			viewName = modelAndView.getViewName();

		if (viewName == null || viewName.startsWith("redirect:") || viewName.startsWith("forward:")
				|| viewName.indexOf("pfm") > -1 || viewName.endsWith("text"))
			return;

		boolean viewAvailable = views.contains(viewName);
		logger.info("view: " + viewName + " is in mobile: " + viewAvailable);

		// Use new Flag to present the View Type
		if (userSession != null && userSession.getUserAgentType() != null) {
			agentType = userSession.getUserAgentType();
		} else if (enrollmentSession != null && enrollmentSession.getUserAgentType() != null) {
			agentType = enrollmentSession.getUserAgentType();
		} else {
			agentType = this.analyzeUserAgentType(request);
		}

		//MBFIS-331
		String appMobile = ServletRequestUtils.getStringParameter(request, "appMobile", "false");

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP) && viewName.contains("accountinformationsec") &&
				(agentType != UserAgentType.WEB && agentType != UserAgentType.MOBILE_DESKTOP && agentType != UserAgentType.JSON && appMobile.equalsIgnoreCase("false")) ) {

				modelAndView.setViewName("mobile" + "/notAvailableSec");

		}
		else if (agentType == UserAgentType.JSON) {
			modelAndView.getModel().put("version", "json");
			if (viewAvailable)
				modelAndView.setViewName("json" + "/" + viewName);
			else
				modelAndView.setViewName("json" + "/notAvailable");

			response.setContentType("application/json");

		} else if (agentType == UserAgentType.IPHONE || agentType == UserAgentType.ANDROID) {
			modelAndView.getModel().put("version", "iphone");


			if (viewAvailable)
				modelAndView.setViewName("iphone" + "/" + viewName);
			else
				modelAndView.setViewName("iphone" + "/notAvailable");
		} else if (agentType == UserAgentType.IPAD) {
			modelAndView.getModel().put("version", "tablet");
		} else if (agentType == UserAgentType.MOBILE) {
			modelAndView.getModel().put("version", "mobile");
			if (viewAvailable)
				modelAndView.setViewName("mobile" + "/" + viewName);
			else
				modelAndView.setViewName("mobile" + "/notAvailable");
		}

		super.postHandle(request, response, handler, modelAndView);

		Map<String, Object> model = modelAndView.getModel();

		// For campaing in login page - Esta aqui para no crear un Interceptor
		// solo para esto.
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			model.put("isenglish", true);
		}

		if (userSession != null && userSession.getLogincampaigns() != null
				&& userSession.getLogincampaigns().size() > 0)
			model.put("campaigns", userSession.getLogincampaigns());

		if (isUserAgentMobileApp(request)) {
			final String userAgentMobileApp = "userAgentMobileApp"; //

			model.put(userAgentMobileApp, true);
		}
		
	}

	/** Validate user-agent
	 * to verify if the request is been made from MiBanco.app.
	 *
	 * @param request HttpServletRequest
	 * @return boolean
	 */
	private boolean isUserAgentMobileApp(HttpServletRequest request) {
		final String userAgent = request.getHeader("user-agent");
		return !GenericValidator.isBlankOrNull(userAgent) && (
				userAgent.matches(".*MiBanco\\.app.*") ||
				userAgent.matches(".*WebView*"));
	}
	
	/**
	 * analyzeUserAgentType
	 * @param request HttpServletRequest
	 * @return UserAgentType
	 */
	private UserAgentType analyzeUserAgentType(HttpServletRequest request) {
		UserAgentType currentUserAgentType = UserAgentType.WEB;

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		EnrollmentSession enrollmentSession = (EnrollmentSession) WebUtils.getSessionAttribute(request,
				"enrollmentSession");

		String header = request.getHeader("user-agent");

		// Se buscan los cookies para encontrar si tiene el client-version
		Cookie versionCookie = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = cookies[i];
				if (cookie.getName().equalsIgnoreCase("client-version")) {
					versionCookie = cookie;
					break;
				}
			}
		}

		currentUserAgentType = this.setUserAgentType(header, userSession, enrollmentSession);

		// Assign flag depending of cookie first, then by UserAgent
		if ((versionCookie != null && "mobile".equalsIgnoreCase(versionCookie.getValue()))) {
			if (currentUserAgentType == UserAgentType.WEB) {
				currentUserAgentType = UserAgentType.IPHONE;
			}
		} else if ((versionCookie != null && "classic".equalsIgnoreCase(versionCookie.getValue()))) {
			if (currentUserAgentType == UserAgentType.IPHONE || currentUserAgentType == UserAgentType.ANDROID
					|| currentUserAgentType == UserAgentType.MOBILE) {
				currentUserAgentType = UserAgentType.MOBILE_DESKTOP;
			}
		}

		if (userSession != null) {
			userSession.setUserAgentType(currentUserAgentType);
		} else if (enrollmentSession != null) {
			enrollmentSession.setUserAgentType(currentUserAgentType);
		}

		return currentUserAgentType;

	}

	/**
	 * setUserAgentType
	 * @param header String
	 * @param userSession UserSession
	 * @param enrollmentSession EnrollmentSession
	 * @return UserAgentType
	 */
	private UserAgentType setUserAgentType(String header, UserSession userSession,
			EnrollmentSession enrollmentSession) {
		UserAgentType userAgentTypeResutl = UserAgentType.WEB;

		if (header.indexOf(jsonUserAgent) != -1 && header.indexOf(athMovilApp) == -1) {
			userAgentTypeResutl = UserAgentType.JSON;
		} else if (header.indexOf("iPhone") != -1 || header.indexOf("iPod") != -1) {
			userAgentTypeResutl = UserAgentType.IPHONE;
		} else if (header.indexOf("iPad") != -1) {
			userAgentTypeResutl = UserAgentType.IPAD;
		} else if (header.indexOf("Android") != -1) {
			userAgentTypeResutl = UserAgentType.ANDROID;
		} else if (isMobile(header)) {
			userAgentTypeResutl = UserAgentType.MOBILE;
		} else {
			userAgentTypeResutl = UserAgentType.WEB;
		}

		return userAgentTypeResutl;
	}
	
	private boolean isMobile(String header) {
		return header.indexOf("BlackBerry") != -1 || header.indexOf("Motorola-Q") != -1
				|| header.indexOf("IEMobile") != -1 || header.indexOf("Windows CE") != -1;
	}
}
