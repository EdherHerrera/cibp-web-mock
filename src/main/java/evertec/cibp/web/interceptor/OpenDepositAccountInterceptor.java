/**
 * 
 */
package evertec.cibp.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.web.session.UserSession;

/**
 * @author jocampo
 * 
 */
public class OpenDepositAccountInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		if (userSession != null) {
			if (userSession.getDciCustomer() == null) {
				response.sendRedirect("productselection");
				return false;
			}
		}
		return true;
	}

}
