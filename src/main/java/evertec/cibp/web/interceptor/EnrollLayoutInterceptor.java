package evertec.cibp.web.interceptor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.EnrollmentSession;
import evertec.cibp.web.session.UserSession;

public class EnrollLayoutInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = Logger.getLogger(EnrollLayoutInterceptor.class);
	private String comercialProducts;

	public String getComercialProducts() {
		return comercialProducts;
	}

	public void setComercialProducts(String comercialProducts) {
		this.comercialProducts = comercialProducts;
	}

	// Mover al xml la lista de views
	public EnrollLayoutInterceptor() {
		super();
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		if (modelAndView.getViewName() == null)
			return;

		@SuppressWarnings("unchecked")
		Map<String, Object> model = modelAndView.getModel();
		EnrollmentSession enrollmentSession = (EnrollmentSession) WebUtils.getSessionAttribute(request,
				"enrollmentSession");		
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		if (enrollmentSession != null) {
			String validatedAcctProdId = "";
			validatedAcctProdId = enrollmentSession.getValidatedAcctProductId();
			if ((!GenericValidator.isBlankOrNull(validatedAcctProdId))
					&& (!GenericValidator.isBlankOrNull(comercialProducts))
					&& (comercialProducts.indexOf(validatedAcctProdId) != -1))
				model.put("useComercialInterface", "T");
			else if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL) && !GenericValidator.isBlankOrNull(validatedAcctProdId) 
					&& "086".equalsIgnoreCase(validatedAcctProdId)){				
				model.put("bsmartEnrollment", "T");
				addBsmartEnrollVar(model, request);
			}
			else{
				model.put("useComercialInterface", "F");
			}

		} else if (session != null) {
			CustomerProfile profile = session.getCustomerProfile();
			if (profile.getUserInterface() != null && profile.getUserInterface() == UserInterface.COMMERCIAL) {
				model.put("useComercialInterface", "T");
			} else
				model.put("useComercialInterface", "F");
		} else
			model.put("useComercialInterface", "F");

	}
	
	/**
	 * 
	 * @param model
	 * @param request
	 */
	private void addBsmartEnrollVar(Map<String, Object> model, HttpServletRequest request) {
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC458)) {
			final String bsmartEnrollVar = "comBsmartEnrollment";//
			final String blnVar = "bcomWasDisplayed";//
			final boolean comBsmartEnroll = Boolean.class.cast(WebUtils.getSessionAttribute(request,
					bsmartEnrollVar));//
			
			final boolean blnBcomWasDisplayed = WebUtils.getSessionAttribute(request,
					blnVar) == null? false : Boolean.class.cast(WebUtils.getSessionAttribute(request,
					blnVar));//
			
			model.put(bsmartEnrollVar, comBsmartEnroll);
			model.put(blnVar,blnBcomWasDisplayed);
		}
	}
	
}
