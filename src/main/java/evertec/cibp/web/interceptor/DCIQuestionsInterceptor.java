package evertec.cibp.web.interceptor;

import java.math.BigInteger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.web.session.UserSession;

public class DCIQuestionsInterceptor extends HandlerInterceptorAdapter {

	private String wealthCodes;
	private String preferredCodes;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		String accountTytpe = request.getParameter("accountType");
		
		if (userSession != null) {
			// VALIDATION IF THE USER CAN OPEN A DEPOSIT ACCOUNT
			// IF IS NULL VALIDATION HAS NOT TAKEN PLACE
			if (userSession.getCanOpenDepositAccount() == null) {
				if (userSession.getBankingSession() != null) {
					BigInteger permId = userSession.getBankingSession().getPermId();

					if (permId == null) {
						permId = new BigInteger("0");
					}

					userSession.setCanOpenDepositAccount(userSession.getCustomerService().canOpenDepositAccount(
							userSession.getCustomerProfile(), String.valueOf(permId),
							userSession.getCustomerWealthPreferred(this.wealthCodes, this.preferredCodes)));
				}
			}
			if (!userSession.getCanOpenDepositAccount() && (accountTytpe != null && !accountTytpe.equals("e-account"))) {
				response.sendRedirect("productselection");
				WebUtils.setSessionAttribute(request, "noopen", true);
				return false;
			}
		}
		return true;
	}

	public void setWealthCodes(String wealthCodes) {
		this.wealthCodes = wealthCodes;
	}

	public String getWealthCodes() {
		return wealthCodes;
	}

	public void setPreferredCodes(String preferredCodes) {
		this.preferredCodes = preferredCodes;
	}

	public String getPreferredCodes() {
		return preferredCodes;
	}
}
