package evertec.cibp.web.interceptor;

import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.util.ClassUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.view.RedirectView;

import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.GlobalEntitlementAwareController;

public class GlobalEntitlementInterceptor extends HandlerInterceptorAdapter implements MessageSourceAware {

	// private static final Logger logger =
	// Logger.getLogger(GlobalEntitlementInterceptor.class);
	private CustomerServices customerServices;
	private MessageSource messageSource;

	public void setMessageSource(MessageSource messagesource) {
		this.messageSource = messagesource;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public CustomerServices getCustomerServices() {
		return customerServices;
	}

	public void setCustomerServices(CustomerServices customerServices) {
		this.customerServices = customerServices;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		String isdown = messageSource.getMessage("downPage", null, "UP", Locale.ENGLISH);
		if (isdown != null && isdown.equalsIgnoreCase("DOWN")) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("SIGNON".toLowerCase()
					+ "downtime", true)));
		}

		Set allInterfacesForClassAsSet = ClassUtils.getAllInterfacesForClassAsSet(handler.getClass());

		if (allInterfacesForClassAsSet.contains(GlobalEntitlementAwareController.class)) {

			GlobalEntitlementAwareController globalDep = (GlobalEntitlementAwareController) handler;

			EntitlementType globalEntitlement = globalDep.getGlobalEntitlement();

			GlobalEntitlement globalEntitlement2 = null;

			try {

				globalEntitlement2 = customerServices.getGlobalEntitlement(globalEntitlement);

			} catch (Exception e) {

				throw new ModelAndViewDefiningException(new ModelAndView(
						((globalEntitlement2 != null && globalEntitlement2.getType() != null) ? globalEntitlement2
								.getType().name().toLowerCase() : "")
								+ "downtime"));
			}

			if (globalEntitlement2 != null && globalEntitlement2.getStatus().equals(EntitlementStatus.ENABLED))
				return true;
			else {

				throw new ModelAndViewDefiningException(new ModelAndView(
						((globalEntitlement2 != null && globalEntitlement2.getType() != null) ? globalEntitlement2
								.getType().name().toLowerCase() : "")
								+ "downtime"));
			}

		} else
			return true;

	}

}
