/**
 * 
 */
package evertec.cibp.web.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.web.form.bean.UnicaApplication;
import evertec.cibp.web.session.UserSession;

/**
 * Control to display the Solicitud Unica detail only if allowed
 * 
 * @author et55498
 */
public class UnicaDetailInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		if (userSession != null && userSession.getUnicaApplications() != null
				&& userSession.getUnicaApplications().size() > 0) {
			String applicationId = request.getParameter("account");

			if (!GenericValidator.isBlankOrNull(applicationId)) {
				List<UnicaApplication> unicaAppList = userSession.getUnicaApplications();

				// Maximum applications are 10. Not an impact on performance
				for (UnicaApplication unicaApp : unicaAppList) {
					if (unicaApp.getUnicaApplicationInfo() != null
							&& !GenericValidator.isBlankOrNull(unicaApp.getUnicaApplicationInfo().getApplicationId())
							&& unicaApp.getUnicaApplicationInfo().getApplicationId().equals(applicationId)
							&& !GenericValidator.isBlankOrNull(unicaApp.getUnicaApplicationInfo().getApplStatusDesc())
							&& unicaApp.getUnicaApplicationInfo().getApplStatusDesc().equals("approved")) {

						return true;
					}
				}
			}
		}
		response.sendRedirect("portal");
		return false;
	}

}
