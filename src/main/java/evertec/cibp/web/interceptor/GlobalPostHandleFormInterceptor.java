package evertec.cibp.web.interceptor;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.session.UserSessionType;

/**
 * GlobalPostHandleFormInterceptor
 * @author et55596
 * @since 1.2
 * @version 1.0 
 */
public class GlobalPostHandleFormInterceptor extends HandlerInterceptorAdapter implements MessageSourceAware {

    /**
     * messageSource
     */
	private MessageSource messageSource; // messageSource
	
	/**
	 * MBCA1311
	 */
	private static final String MBCA1311= "mbca1311"; // MBCA1311	

	/**
	 * getMessageSource
	 * @return messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * setMessageSource
	 * @param messageSource MessageSource
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param obj
	 * @param modelandview
	 * @throws Exception
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object obj,
			ModelAndView modelandview) throws Exception {

		if (modelandview != null && modelandview.getViewName() != null
				&& !modelandview.getViewName().startsWith("redirect:")) {
			ModelMap model = modelandview.getModelMap();
			UserSession usersession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

			if (usersession != null) {
				model.put("userSession", usersession != null ? usersession : null);
				String statusmessage = usersession.getStatusmessage();
				if (!GenericValidator.isBlankOrNull(statusmessage)) {
					// usersession.setStatusmessage("");
					model.put("statusmessage", statusmessage);
					/*
					 * CIBP-1680 bgarcia Se ponen el modelo el cdoigo de error
					 * EBA y la descripcion actulizada , para ser desplegada en
					 * la pagina layount.vm
					 */
					model.put("errorDetail", usersession.getErrorDetail());// descripcion
																			// actualizada
					model.put("errorDetailCod", usersession.getErrorDetailCod());// codigo
																					// EBA
																					// de
																					// Telepago
				}
				// request.getSession().getAttribute("userSession").setStatusmessage("");
				model.put("customername", usersession.getCustomerProfile() != null ? usersession.getCustomerProfile()
						.getFirstName() : null);
				// FFEIC
				if (usersession.getLastLoginDate() != null) {
					model.put("lastLoginDate",
							usersession.getCustomerProfile() != null ? usersession.getLastLoginDate() : null);
					model.put("lastLoginIp", usersession.getCustomerProfile() != null ? usersession.getLastLoginIp()
							: null);
				}
				// FIN FFEIC
				if (usersession.hasVirginIslandAccount())
					model.put("hasVIAccount", true);
				if (usersession.hasBritishVirginIslandAccount())
					model.put("hasBVIAccount", true);
				if (usersession.getUserSessionType() == UserSessionType.CUSTOMER_PROFILE && !usersession.isDelay()
						&& usersession.isComercialCustomer())
					model.put("isComercial", true);

				if (CustomerFacade.getInstance().getGlobalEntitlement(usersession, EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED
						// mpicado, CIBP-1799, condicion para el downtime de
						// ebpp
						&& this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
								.equals(EntitlementStatus.ENABLED.toString()))
					model.put("ebillUp", true);
				// mpicado, CIBP-1799, Logica para el manejo del downtime de OAO
				/*
				 * if(usersession.getCustomerService().getGlobalEntitlement(
				 * EntitlementType
				 * .ACCOUNT_OPENING).getStatus()==EntitlementStatus.ENABLED &&
				 * this.messageSource.getMessage("oao.module",null,new
				 * Locale("es"
				 * )).toUpperCase().equals(EntitlementStatus.ENABLED.toString
				 * ())) model.put("oaoUp", true); //mpicado, CIBP-1799, Logica
				 * para el manejo del downtime de Imagenes de cheque
				 * if(usersession
				 * .getCustomerService().getGlobalEntitlement(EntitlementType
				 * .CHECKS).getStatus()==EntitlementStatus.ENABLED &&
				 * this.messageSource.getMessage("checks.module",null,new
				 * Locale(
				 * "es")).toUpperCase().equals(EntitlementStatus.ENABLED.
				 * toString ())) model.put("checksUp", true); //mpicado,
				 * CIBP-1799, Logica para el manejo del downtime de Harland
				 * if(usersession.getCustomerService
				 * ().getGlobalEntitlement(EntitlementType
				 * .HARLAND).getStatus()==EntitlementStatus.ENABLED &&
				 * this.messageSource.getMessage("harland.module",null,new
				 * Locale
				 * ("es")).toUpperCase().equals(EntitlementStatus.ENABLED.
				 * toString ())) model.put("harlandUp", true);
				 */

				if (usersession != null && usersession.getUserSessionType() != null) {
					model.put("fullyEnrolledCustomer",
							usersession.getUserSessionType().equals(UserSessionType.CUSTOMER_PROFILE));
				}

				// CIBP-2700 No campaigns for mobile version
				if (usersession.getUserAgentType() == UserAgentType.WEB
						|| usersession.getUserAgentType() == UserAgentType.IPAD) {

					// mpicado, cibp-2218
					model.put("guidedSetupMode", usersession.getGuidedSetupSession().isGuidedSetupMode());

					// bgarcia, CIBP-2218
					if (usersession.getGuidedSetupSession().isGuidedSetupMode()) {
						model.put("individualguidedSetup", usersession.getGuidedSetupSession()
								.isIndividualguidedSetup());

					}
				}

				// P2P
				if (usersession.getCustomerService().getGlobalEntitlement(EntitlementType.ATHMOVIL) != null
						&& usersession.getCustomerService().getGlobalEntitlement(EntitlementType.ATHMOVIL).getStatus() == EntitlementStatus.ENABLED) {
					model.put("showATHM", true);

					if (usersession.getCustomerProfile().hasEntitlement(EntitlementType.ATHMOVIL)) {
						model.put("showATHMmenu", true);
					}

					if (usersession.getCustomerProfile().hasFlag(CustomerFlagType.ATHM_BLOCK)) {
						
						String message = ""; // message
						
						model.put("athmBlock", usersession.getCustomerProfile().hasFlag(CustomerFlagType.ATHM_BLOCK));
						message = "athmobile.portal.box.automatic.blocked";
						if(StaticMessageSource.isFlagEnabled(StaticMessageSource.ATHM_UNBLOCK_INCID))
							message = "athmobile.portal.box.blocked.new";
						if (!"SYSTEM".equals(usersession.getCustomerProfile().getSignonStats().getAthmUpdatedBy()))
							message = "athmobile.portal.box.blocked";
						model.put("athmBlockMessage", message);
					}
				}

				if (usersession.getCustomerProfile().hasFlag(CustomerFlagType.DOCUMENTS_AVAILABLE)) {
					// Desembolso Asistido (In case the user doesn't click OK in
					// the block acknowledgment)
					if (usersession.isBlocked()) {
						response.sendRedirect("login");
					} else if (usersession.getDisbursementList() != null) {
						model.put("displayMyDocuments", usersession.getDisbursementList().size() > 0);
					}
				}
				
				//MBCA
				model.put("isTransactional", AccountUtils.getIsTransactional(usersession.getCustomerProfile()).equalsIgnoreCase("Y") ? true : false);
		
				model.put("mbdp", StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC));
				model.put("MBCA109", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA109));
				model.put("MBCA501", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA501));
				model.put("MBCA105", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA105));
				model.put("MBCA39", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA39));	
				model.put("MBCA98", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA98));
				model.put("MBCA817", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA817));
				model.put("MBCA209", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA209));
				model.put("MBCA210", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA210));
				model.put("MBCA110", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA110));
				model.put("MBCA102", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA102));
				model.put("updatedTerms", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA295));
				model.put("athmSso", StaticMessageSource.isFlagEnabled(StaticMessageSource.ATHM_SSO));
				model.put("MBIM271", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBIM271));
				model.put(MBCA1311, usersession.isRemoveAccountStatement());
			}
		}
	}
}