package evertec.cibp.web.interceptor;

import java.math.BigInteger;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RedirectUtils;

public class MbdpFormInterceptor extends HandlerInterceptorAdapter {

	private String allowNonTransactional;
	private String wealthCodes;
	private String preferredCodes;

	public MbdpFormInterceptor() {
		this.allowNonTransactional = StringUtils.EMPTY;
		this.wealthCodes = StringUtils.EMPTY;
		this.preferredCodes = StringUtils.EMPTY;
	}

	public MbdpFormInterceptor(String allowNonTransactional, String wealthCodes, String preferredCodes) {
		this();
		this.allowNonTransactional = allowNonTransactional;
		this.wealthCodes = wealthCodes;
		this.preferredCodes = preferredCodes;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		final String requestPath = request.getContextPath() + "/actions/portal"; // portal path

		if (userSession != null
				&& Optional.ofNullable(allowNonTransactional).filter(flag -> !flag.isEmpty()).isPresent()) {
			if (userSession.getCanOpenDepositAccount() == null && userSession.getBankingSession() != null) {
				BankingSession bankingSession = userSession.getBankingSession();
				BigInteger permId = Optional.ofNullable(bankingSession.getPermId()).orElse(new BigInteger("0"));

				userSession.setCanOpenDepositAccount(userSession.getCustomerService().canOpenDepositAccount(
						userSession.getCustomerProfile(), String.valueOf(permId),
						userSession.getCustomerWealthPreferred(this.wealthCodes, this.preferredCodes)));
			}

			if (userSession.getCanOpenDepositAccount() != null && userSession.getCanOpenDepositAccount()
					&& Optional.ofNullable(allowNonTransactional).filter(flag -> !flag.isEmpty()).isPresent()) {
				if (allowNonTransactional.equalsIgnoreCase("true")) {
					if (userSession.isComercialCustomer()
							|| userSession.getCustomerProfile().getIsTransactional().equals("N")) {
						RedirectUtils.redirectToPath(response, requestPath);
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * @return the allowNonTransactional
	 */
	public String getAllowNonTransactional() {
		return allowNonTransactional;
	}

	/**
	 * @param allowNonTransactional the allowNonTransactional to set
	 */
	public void setAllowNonTransactional(String allowNonTransactional) {
		this.allowNonTransactional = allowNonTransactional;
	}

	/**
	 * @return the wealthCodes
	 */
	public String getWealthCodes() {
		return wealthCodes;
	}

	/**
	 * @param wealthCodes the wealthCodes to set
	 */
	public void setWealthCodes(String wealthCodes) {
		this.wealthCodes = wealthCodes;
	}

	/**
	 * @return the preferredCodes
	 */
	public String getPreferredCodes() {
		return preferredCodes;
	}

	/**
	 * @param preferredCodes the preferredCodes to set
	 */
	public void setPreferredCodes(String preferredCodes) {
		this.preferredCodes = preferredCodes;
	}
}