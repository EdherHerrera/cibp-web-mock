/**
* RegainAccessTokenInterceptor
* Project: CIBP
* Company: Evertec inc
*/
package evertec.cibp.web.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.web.services.modules.RegainAccessServiceModule;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.errors.AccessControlException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import org.owasp.esapi.ESAPI;

import evertec.cibp.core.enums.RegainTokenStatus;
import evertec.cibp.core.models.RegainAccessToken;
import evertec.cibp.core.services.CustomerServices;

import static evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType.RA_EMAIL_TOKEN_EXPIRED;
import static evertec.cibp.core.utils.StaticMessageSource.isFlagEnabled;
import static evertec.cibp.core.utils.StaticMessageSource.MBFC224;

/**
 * Regain Access Interceptor
 *
 * @author Eduardo Agosto
 * @since august-2019
 * @see evertec.cibp.web.form.RegainAccessForm
 * @version 1.0
 */
public class RegainAccessTokenInterceptor extends HandlerInterceptorAdapter {

	/**
	 * Logger instance.
	 */
	protected final Log log = LogFactory.getLog(getClass()); //

	/**
	 * Expired token view.
	 */
	private static final String EXPIRED_TOKEN_VIEW_FORMAT ="regainaccess?resource=%s&expired=true"; //

	/**
	 * Token String.
	 */
	private static final String TOKEN = "token"; //

	/**
	 * Resource String.
	 */
	private static final String RESOURCE = "resource"; //
	
	/**
	 * Email blocked view.
	 */
	private static final String EMAIL_BLOCKED_VIEW = "regainaccessEmailBlocked";//

	/**
	 * CustomerService instance.
	 */
	private CustomerServices customerServices; //

	/**
	 * RegainAccessServiceModule instance.
	 */
	private RegainAccessServiceModule regainAccessServiceModule; //
	
	/**
	 * RegainAccessTokenInterceptor
	 */
	public RegainAccessTokenInterceptor () {
		this.customerServices = null;
		this.regainAccessServiceModule = null; 
	}
	
	/**
	 * RegainAccessTokenInterceptor
	 * @param customerServices
	 * @param regainAccessServiceModule
	 */
	public RegainAccessTokenInterceptor (CustomerServices customerServices, RegainAccessServiceModule regainAccessServiceModule) {
		this();
		this.customerServices = customerServices;
		this.regainAccessServiceModule = regainAccessServiceModule;
	}
	
	/**
	 * @param request
	 * @param response
	 * @param handler
	 * @return
	 * @throws ServletRequestBindingException
	 * @throws IOException
	 * @throws AccessControlException 
	 */
	@Override
	public boolean preHandle (HttpServletRequest request, HttpServletResponse response, Object handler)
			throws ServletRequestBindingException, IOException, AccessControlException {
		
		final String REGAIN_ACCESS_TOKEN = "regainAccessToken";
		boolean isFlagActive = isFlagEnabled(MBFC224);
		boolean validateToken = false;
		String tokenParameter = null; //
		String tokenSession = null;
		
		if (isFlagActive) {
			validateToken = true;
			tokenParameter = ServletRequestUtils.getStringParameter(request, TOKEN); //
			tokenSession = String.class.cast(WebUtils.getSessionAttribute(request, REGAIN_ACCESS_TOKEN)); //
			
			if (GenericValidator.isBlankOrNull(tokenParameter) && !GenericValidator.isBlankOrNull(tokenSession)) {
				validateToken = false;
			}
		}

		return validateToken ? manageAccess(request, response, tokenParameter) : true;
	}

	/**
	 * Manges token.
	 *
	 * @param request
	 * @param response
	 * @param strToken
	 * @param blnIsFlagInactive
	 * @return
	 * @throws ServletRequestBindingException
	 * @throws IOException
	 * @throws AccessControlException 
	 */
	private boolean manageAccess (HttpServletRequest request, HttpServletResponse response, String strToken)
			throws ServletRequestBindingException, IOException, AccessControlException {
		
		final RegainAccessToken accessToken = customerServices.getRegainAccessToken(strToken); //
		final boolean emailTokenvalid = isEmailTokenValid(accessToken);//
		final boolean emailBlocked =  emailTokenvalid && getRegainAccessServiceModule().isEmailBlocked(accessToken.getEmail()); //
		boolean manageAccess = true;//
				
		if (emailBlocked) {
			manageAccess = false;
			ESAPI.httpUtilities().sendRedirect(response, request.getContextPath() + "/actions/" + EMAIL_BLOCKED_VIEW);
		} else if (!validateTokenAndLogExpiredEvent(accessToken, request)) {
			final String blockParam = "passwordBlockedMobile"; //
			final String resource = ServletRequestUtils.getStringParameter(request, RESOURCE); //
			final String passwordBlockedMobile = ServletRequestUtils.getStringParameter(request, blockParam); //
			String lang = ServletRequestUtils.getStringParameter(request, "lang"); //
			
			if (accessToken.getEmail() == null && resource == null) {
				final String regainAccessForm = passwordBlockedMobile != null && passwordBlockedMobile.equals("true") 
						? "regainaccess?resource=blocked&lang=" + lang : "regainaccess?resource=options&lang=" + lang; // old version resetPassword
				
				ESAPI.httpUtilities().sendRedirect(response, request.getContextPath() + "/actions/" + regainAccessForm);
			} else {
				final String expiredView = EXPIRED_TOKEN_VIEW_FORMAT + "&lang=" + lang;
				manageAccess = false;
				ESAPI.httpUtilities().sendRedirect(response, request.getContextPath() + 
						"/actions/" + String.format(expiredView, resource));
			}
		}

		return manageAccess;
	}

	/**
	 * @param accessToken
	 * @param request
	 * @return
	 */
	private boolean validateTokenAndLogExpiredEvent (RegainAccessToken accessToken, HttpServletRequest request) {
		final boolean tokenNotEmpty = (accessToken != null && !GenericValidator.isBlankOrNull(accessToken.getToken()));//
		boolean tokenValid = tokenNotEmpty && RegainTokenStatus.VALID.equals(accessToken.getStatus()); //

		if (tokenValid && accessToken.getStatus().equals(RegainTokenStatus.EXPIRED)) {
			tokenValid = false;
		} else if (tokenValid && !accessToken.getStatus().equals(RegainTokenStatus.EXPIRED)) {
			if (getRegainAccessServiceModule().isTokenExpired(accessToken)) {
				tokenValid = false;
				regainAccessServiceModule.logRegainAccessEvent(request, accessToken, null, RA_EMAIL_TOKEN_EXPIRED);
			}
			WebUtils.setSessionAttribute(request, RESOURCE, accessToken.getResource().toString());
		}

		return tokenValid;
	}
	
	/**
	 * @param accessToken
	 * @return boolean
	 */
	private boolean isEmailTokenValid (RegainAccessToken accessToken) {
		return accessToken != null && !GenericValidator.isBlankOrNull(accessToken.getEmail());
	}

	/**
	 * @return the customerServices
	 */
	public CustomerServices getCustomerServices () {
		return customerServices;
	}

	/**
	 * @param customerServices the customerServices to set
	 */
	public void setCustomerServices (CustomerServices customerServices) {
		this.customerServices = customerServices;
	}

	/**
	 * @return the regainAccessServiceModule
	 */
	public RegainAccessServiceModule getRegainAccessServiceModule () {
		return this.regainAccessServiceModule;
	}

	/**
	 * @param regainAccessServiceModule
	 */
	public void setRegainAccessServiceModule (RegainAccessServiceModule regainAccessServiceModule) {
		this.regainAccessServiceModule = regainAccessServiceModule;
	}

}
