/**
 * 
 */
package evertec.cibp.web.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.errors.AccessControlException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.InterceptorUtils;
import evertec.cibp.web.utils.RedirectUtils;
import evertec.cibp.web.utils.Utils;

/**
 * Restricts the Non-Transactional customer in selected pages and redirects to
 * portal.
 * 
 * When Non-Transactional customers with only RVA/LEA cannot enter the
 * transaction page
 * 
 * @author et56509
 * @since sept 11, 2019
 * @version 2.0
 * @return portal
 * @see OtherClass
 */

public class UrlRestrictIntercept extends HandlerInterceptorAdapter {

	/**
	 * Restricts the Non-Transactionals in specific pages and redirects to portal
	 * and customers with only RVA/LEA cannot enter the transaction page
	 * 
	 * @author et56509
	 * @since sept 11, 2019
	 * @version 2.0
	 *
	 * @param HttpServletRequest  request
	 * @param HttpServletResponse response
	 * @param Object              handler
	 * @param ModelAndView        modelAndView
	 * @throws IOException
	 * @throws AccessControlException
	 */
	@Override
	public void postHandle (HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws IOException, AccessControlException {

		final UserSession userSession = Utils.getUserSession(request); // userSession
		
		final String requestPath = request.getContextPath() + "/actions/portal"; // portal path
		
		if (userSession != null 
				&& userSession.getCustomerProfile() != null) {

			if (InterceptorUtils.checkAllowedViews(modelAndView.getViewName(), userSession.getCustomerProfile()) 
					|| userSession.isRemoveAccountStatement()) {
				
				RedirectUtils.redirectToPath(response, requestPath);
			}

		} else {
			
			RedirectUtils.redirectToPath(response, requestPath);
		}
	}
}
