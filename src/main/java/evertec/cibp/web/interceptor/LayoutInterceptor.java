package evertec.cibp.web.interceptor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.StaticScriptUtil;

public class LayoutInterceptor extends HandlerInterceptorAdapter implements MessageSourceAware {

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(LayoutInterceptor.class);
	private String wealthCodes;
	private String preferredCodes;
	private MessageSource messageSource;

	// Mover al xml la lista de views
	public LayoutInterceptor() {
		super();
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		if (modelAndView == null || modelAndView.getViewName() == null)
			return;
		
		if (modelAndView.getModel().toString().contains(StaticScriptUtil.MINOR) &&
				modelAndView.getModel().toString().contains(StaticScriptUtil.MAJOR)&&
				modelAndView.getModel().toString().toUpperCase().contains(StaticScriptUtil.SCRIPT)){
			final String view = "portal";
			ModelAndView newModel = new ModelAndView();
			newModel.setViewName(view);
			modelAndView = newModel;
			final String path = request.getContextPath() + StaticScriptUtil.URL_REDIRECT;
			ESAPI.httpUtilities().sendRedirect(response, path);
		}

		Map<String, Object> model = modelAndView.getModel();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		boolean isWealth = false;
		
		boolean useComercialInterface = false;

		if (modelAndView.getViewName().contains("updEnrollAcct")) {
			return;
		}

		if (userSession != null) {
			isWealth = userSession.isWealth(wealthCodes, preferredCodes);
			useComercialInterface = userSession.getCustomerProfile().useCommercialInterface();
		}
		
		/*
		 * Se usará para que se verifique si la Bandera del hidesections se
		 * encuentra deshabilitada para que esconda los estados
		 */
		String showMenuPayroll = "false";
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)) {
			
			if (userSession != null) 
			{
				model.put("epayrollNewAlertBadge", userSession.hasePayrollBullet());
			}			
			showMenuPayroll = "true";			
		}
		model.put("showMenuPayroll", showMenuPayroll);
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)){
			if(userSession != null && userSession.getInterbankFundingAllowed() != null){
				model.put("interbankFundingAllowed", userSession.getInterbankFundingAllowed());
			}
			else{
				model.put("interbankFundingAllowed", Boolean.FALSE);
			}		
		}
		else{
			model.put("interbankFundingAllowed", Boolean.TRUE);
		}
		
		model.put("evendor", StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR));
		
		
		model.put("MBSFE398", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE398));
		
		model.put("MBSFE624", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE624));
		
		model.put("fraudPreventionService", StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE));
		model.put("fpsShowNewBubbleAlert",isFpsShowNewBubbleAlert((Boolean)WebUtils.getSessionAttribute(request, "fpsShowNewBubbleAlert")));
			
		if (isWealth)
			model.put("isWealth", "T");
		else
			model.put("isWealth", "F");
		if (useComercialInterface)
			model.put("useComercialInterface", "T");
		else
			model.put("useComercialInterface", "F");
		
		//MBFIS-21
		if (userSession!=null) {
			if (CustomerUtils.isSecurities(userSession.getCustomerProfile())) {
				model.put("isSecurities", Boolean.TRUE);
			}else {
				model.put("isSecurities", Boolean.FALSE);
			}
		}
		
		//END MBFIS-21

		if (!modelAndView.getViewName().startsWith("redirect:"))
			model.put("vmName", modelAndView.getViewName());

		if (!modelAndView.getViewName().startsWith("redirect:")
				&& (modelAndView.getViewName().equalsIgnoreCase("forward:portal")))
			model.put("vmName", "portal");

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513) 
				&& userSession != null
				&& userSession.getUserAgentType() != null
				&& !userSession.getUserAgentType().equals(UserAgentType.JSON)) {
			final String accountMLA = userSession.getAccountMLA();
			final String urlRedirect = "/actions/detail?account="; //redirect part
			final String urlTabNum = "&tabNum=0&trxTabNum=0"; //tabNum part
			final String path = request.getContextPath() + urlRedirect + accountMLA + urlTabNum;
			if (accountMLA != null) {
				userSession.setAccountMLA(null);
				ESAPI.httpUtilities().sendRedirect(response, path);
			}
		}
		
	}

	public void setWealthCodes(String wealthCodes) {
		this.wealthCodes = wealthCodes;
	}

	public String getWealthCodes() {
		return wealthCodes;
	}

	public void setPreferredCodes(String preferredCodes) {
		this.preferredCodes = preferredCodes;
	}

	public String getPreferredCodes() {
		return preferredCodes;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	private boolean isFpsShowNewBubbleAlert(Boolean value){
		 return value == null ? Boolean.FALSE: value.booleanValue();
		
	}
}
