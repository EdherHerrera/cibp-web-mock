package evertec.cibp.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.CookieUtils;

public class LogoutInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();
		}

		/*
		 * UserSession userSession =
		 * (UserSession)WebUtils.getSessionAttribute(request, "userSession"); if
		 * (userSession!=null) { WebUtils.setSessionAttribute(request,
		 * "userSession", null); }
		 */

		return true;

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		// Remove the sessionError cookie when the userSession does not exist.

		if (null == userSession || !userSession.isLoginCompleteSuccess()) {
			CookieUtils.removeCookie(response, "sessionError");
		}
	}
}
