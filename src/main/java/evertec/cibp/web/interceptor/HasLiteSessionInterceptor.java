package evertec.cibp.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.session.UserSessionLite;

public class HasLiteSessionInterceptor extends HandlerInterceptorAdapter {

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		UserSessionLite userSessionLite = (UserSessionLite) WebUtils.getSessionAttribute(request, "userSessionLite");
		if (userSessionLite != null) {
			return true;
		}
		//TODO: Add next step (no session error?)
		return false;

	}

}