package evertec.cibp.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.HasSectionUtil;

public class SectionManagerInterceptor extends HandlerInterceptorAdapter implements MessageSourceAware {
	 
	private MessageSource messageSource;		
	
	public SectionManagerInterceptor() {
		super();
	}

	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.SECTION_MANAGEMENT)){			
			UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");	
			if(userSession != null && !modelAndView.getViewName().isEmpty())
			{
				if(!HasSectionUtil.hasSectionPath(userSession, modelAndView.getViewName())) 
					response.sendRedirect(request.getContextPath() + "/actions/portal");
			}
		}
	}	
	
	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

}
