package evertec.cibp.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import evertec.cibp.web.session.UserSession;

public class SessionInterceptor extends HandlerInterceptorAdapter {

	public Logger logger = Logger.getLogger(SessionInterceptor.class);

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String referer = request.getHeader("REFERER");

		if (request.getRequestedSessionId() != null && userSession == null) {

			logger.warn("No Session for requested session id: " + request.getRequestedSessionId() + " REFERER:"
					+ referer + "  CLIENT IP: " + request.getRemoteAddr() + " USER-AGENT:"
					+ request.getHeader("User-Agent") + " URI:" + request.getRequestURI());

		}

		if (null == userSession) {
			logger.warn("No Session found. REFERER:" + referer + "  CLIENT IP: " + request.getRemoteAddr()
					+ " USER-AGENT:" + request.getHeader("User-Agent") + " URI:" + request.getRequestURI());
			throw new ModelAndViewDefiningException(new ModelAndView("redirect:login", "noSessionError", null));

		} else {
			return true;
		}
	}

}
