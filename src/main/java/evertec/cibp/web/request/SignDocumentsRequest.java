package evertec.cibp.web.request;

import evertec.cibp.core.models.documents.CustomerDocumentsPackage;
import evertec.cibp.web.form.dto.CustomerDocPackageDto;

/**
 * This class provides a wrapper for the request of document signature
 */
public class SignDocumentsRequest {
	
	/** The customer name*/
	private String fullCustomerName;

	/** The disbursement */
	private CustomerDocPackageDto disbursement;
	
	/**
	 * The default constructor
	 * @param fullCustomerName The full customer name
	 * @param disbursement The disbursement
	 */
	public SignDocumentsRequest(String fullCustomerName, CustomerDocPackageDto disbursement)
	{
		this.fullCustomerName = fullCustomerName;
		this.disbursement = disbursement;
	}

	/**
	 * @return the full Customer Name
	 */
	public String getFullCustomerName() {
		return fullCustomerName;
	}

	/**
	 * @param fullCustomerName the full Customer Name to set
	 */
	public void setFullCustomerName(String fullCustomerName) {
		this.fullCustomerName = fullCustomerName;
	}
	
	/**
	 * @return the disbursement
	 */
	public CustomerDocPackageDto getDisbursement() {
		return disbursement;
	}

	/**
	 * @param disbursement the disbursement to set
	 */
	public void setDisbursement(CustomerDocPackageDto disbursement) {
		this.disbursement = disbursement;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SignDocumentsRequest [fullCustomerName=" + fullCustomerName + ", disbursement=" + disbursement + "]";
	}
	
}
