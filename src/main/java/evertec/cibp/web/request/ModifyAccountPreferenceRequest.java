package evertec.cibp.web.request;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.web.session.UserSession;

/**
 * This class provides a wrapper for the request of an account preference modification
 */
public class ModifyAccountPreferenceRequest {
	
	private UserSession userSession;
	private CustomerProfile customerProfile;
	private TVFrontendAccount account;
	private String nickname;
	private boolean hide;
	
	/**
	 * Default constructor
	 */
	public ModifyAccountPreferenceRequest() {
		
	}
	
	/**
	 * @return The user session
	 */
	public UserSession getUserSession() {
		return userSession;
	}
	
	/**
	 * @param userSession The user session
	 */
	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}
	
	/**
	 * @return The customer profile
	 */
	public CustomerProfile getCustomerProfile() {
		return customerProfile;
	}
	
	/**
	 * @param customerProfile The customer profile
	 */
	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}
	
	/**
	 * @return The account
	 */
	public TVFrontendAccount getAccount() {
		return account;
	}
	
	/**
	 * @param account The customer account
	 */
	public void setAccount(TVFrontendAccount account) {
		this.account = account;
	}
	
	/**
	 * @return The account nickname
	 */
	public String getNickname() {
		return nickname;
	}
	
	/**
	 * @param nickname The account nickname
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	/**
	 * @return account hide true or false
	 */
	public boolean isHide() {
		return hide;
	}
	
	/**
	 * @param hide The hide parameter to set
	 */
	public void setHide(boolean hide) {
		this.hide = hide;
	}
	
}
