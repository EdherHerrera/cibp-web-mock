package evertec.cibp.web.request;

import java.util.Date;

import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.mbop.MbopPremiaCustomerInfo;
import evertec.cibp.core.models.mbop.MbopStatus;

/**
 * Copyright 2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * SaveMbopCustomerInfoRequest class.
 * <p>
 * The class SaveMbopCustomerInfoRequest allows to transfer data from the service to the client.
 *
 * @author Joel Contreras
 * @version 1.0
 * @since 1.0
 */
public class SaveMbopCustomerInfoRequest {
	
    /**
     *  profileId
     */
    private String profileId;

    /**
     *  status
     */
    private MbopStatus status;

    /**
     *  firstSignonOk
     */
    private Date firstSignonOk;

    /**
     *  lastSignonOk
     */
    private Date lastSignonOk;

    /**
     *  firstSignonFail
     */
    private Date firstSignonFail;

    /**
     *  lastSignonFail
     */
    private Date lastSignonFail;

    /**
     *  signonOkQty
     */
    private Long signonOkQty;

    /**
     *  signonFailQty
     */
    private Long signonFailQty;

    /**
     *  mbopPremiaInfo
     */
    private MbopPremiaCustomerInfo mbopPremiaInfo;

    /**
     *  enrollmentSource
     */
    private CustomerEnrollmentSource enrollmentSource;


    /**
     * Gets the profile id.
     *
     * @return the profileId
     */
    public String getProfileId() {
        return profileId;
    }


    /**
     * Sets the tool tip profileId.
     *
     * @param profileId  the profileId of the tool tip
     */
    public void setProfileId(String profileId) { this.profileId = profileId; }

    /**
     * Gets the status
     *
     * @return the status
     */
    public MbopStatus getStatus() {
        return status;
    }


    /**
     * Sets the tool tip status.
     *
     * @param status  the status of the tool tip
     */
    public void setStatus(MbopStatus status) {
        this.status = status;
    }

    /**
     * Gets the firstSignonOk
     *
     * @return the firstSignonOk
     */
    public Date getFirstSignonOk() {
        return firstSignonOk;
    }


    /**
     * Sets the tool tip firstSignonOk.
     *
     * @param firstSignonOk  the firstSignonOk of the tool tip
     */
    public void setFirstSignonOk(Date firstSignonOk) {
        this.firstSignonOk = firstSignonOk;
    }

    /**
     * Gets the lastSignonOk
     *
     * @return the lastSignonOk
     */
    public Date getLastSignonOk() {
        return lastSignonOk;
    }


    /**
     * Sets the tool tip lastSignonOk.
     *
     * @param lastSignonOk  the lastSignonOk of the tool tip
     */
    public void setLastSignonOk(Date lastSignonOk) {
        this.lastSignonOk = lastSignonOk;
    }

    /**
     * Gets the firstSignonFail
     *
     * @return the firstSignonFail
     */
    public Date getFirstSignonFail() {
        return firstSignonFail;
    }


    /**
     * Sets the tool tip firstSignonFail.
     *
     * @param firstSignonFail  the enrollmentSource of the tool tip
     */
    public void setFirstSignonFail(Date firstSignonFail) {
        this.firstSignonFail = firstSignonFail;
    }

    /**
     * Gets the lastSignonFail
     *
     * @return the lastSignonFail
     */
    public Date getLastSignonFail() {
        return lastSignonFail;
    }


    /**
     * Sets the tool tip lastSignonFail.
     *
     * @param lastSignonFail  the lastSignonFail of the tool tip
     */
    public void setLastSignonFail(Date lastSignonFail) {
        this.lastSignonFail = lastSignonFail;
    }

    /**
     * Gets the signonOkQty
     *
     * @return the signonOkQty
     */
    public Long getSignonOkQty() {
        return signonOkQty;
    }


    /**
     * Sets the tool tip signonOkQty.
     *
     * @param signonOkQty  the signonOkQty of the tool tip
     */
    public void setSignonOkQty(Long signonOkQty) {
        this.signonOkQty = signonOkQty;
    }

    /**
     * Gets the signonOkQty
     *
     * @return the signonOkQty
     */
    public Long getSignonFailQty() {
        return signonFailQty;
    }


    /**
     * Sets the tool tip signonFailQty.
     *
     * @param signonFailQty  the enrollmentSource of the tool tip
     */
    public void setSignonFailQty(Long signonFailQty) {
        this.signonFailQty = signonFailQty;
    }

    /**
     * Gets the mbopPremiaInfo
     *
     * @return the mbopPremiaInfo
     */
    public MbopPremiaCustomerInfo getMbopPremiaInfo() {
        return mbopPremiaInfo;
    }


    /**
     * Sets the tool tip mbopPremiaInfo.
     *
     * @param mbopPremiaInfo  the mbopPremiaInfo of the tool tip
     */
    public void setMbopPremiaInfo(MbopPremiaCustomerInfo mbopPremiaInfo) {
        this.mbopPremiaInfo = mbopPremiaInfo;
    }

    /**
     * Gets the enrollmentSource
     *
     * @return the enrollmentSource
     */
    public CustomerEnrollmentSource getEnrollmentSource() {
        return enrollmentSource;
    }

    /**
     * Sets the tool tip enrollmentSource.
     *
     * @param enrollmentSource  the enrollmentSource of the tool tip
     */
    public void setEnrollmentSource(CustomerEnrollmentSource enrollmentSource) {
        this.enrollmentSource = enrollmentSource;
    }


}
