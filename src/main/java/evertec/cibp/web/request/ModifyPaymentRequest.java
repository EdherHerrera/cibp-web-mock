package evertec.cibp.web.request;

import java.math.BigDecimal;

import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.vs.beans.payments.PaymentModify;
import evertec.cibp.web.session.UserSession;

public class ModifyPaymentRequest {
	
	private String taxId;
	private int instances;
	
	private TVPayment payment; 
	private BigDecimal initialAmount;
	private UserSession userSession;
	private PaymentModify paymentModify;
	private CustomerServices customerService;
	CustomerInteractionEvent interactionEvent;
	
	public ModifyPaymentRequest() {}
	public ModifyPaymentRequest(String taxId, TVPayment payment, PaymentModify paymentModify) {
		this.taxId = taxId;
		this.payment = payment;
		this.paymentModify = paymentModify;
	}
	
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public CustomerServices getCustomerService() {
		return customerService;
	}
	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}
	public TVPayment getPayment() {
		return payment;
	}
	public void setPayment(TVPayment payment) {
		this.payment = payment;
	}
	public PaymentModify getPaymentModify() {
		return paymentModify;
	}
	public void setPaymentModify(PaymentModify paymentModify) {
		this.paymentModify = paymentModify;
	}
	public int getInstances() {
		return instances;
	}
	public void setInstances(int instances) {
		this.instances = instances;
	}
	public CustomerInteractionEvent getInteractionEvent() {
		return interactionEvent;
	}
	public void setInteractionEvent(CustomerInteractionEvent interactionEvent) {
		this.interactionEvent = interactionEvent;
	}
	
	public UserSession getUserSession() {
		return userSession;
	}
	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}
	
	public BigDecimal getInitialAmount() {
		return initialAmount;
	}
	public void setInitialAmount(BigDecimal initialAmount) {
		this.initialAmount = initialAmount;
	}

}
