package evertec.cibp.web.request;

import java.util.Map;

import evertec.cibp.core.models.customer.PreferredLanguage;

/**
 * This class provides a wrapper for the request for sending a notification
 */
public class AddNotificationRequest {
	private String fromName;
	private String fromEmailAddress;
	private String toName;
	private String toEmailAddress;
	private String customerProfileId;
	private String alertType;
	private PreferredLanguage preferredLanguage;
	private Map<String,Object> templateContext;
	private int event;

	/**
	 * Default Constructor
	 */
	public AddNotificationRequest() {
		super();
	}

	/**
	 * Constructor with all fields
	 * @param fromName
	 * @param fromEmailAddress
	 * @param toName
	 * @param toEmailAddress
	 * @param customerProfileId
	 * @param alertType
	 * @param preferredLanguage
	 * @param templateContext
	 * @param event
	 */
	public AddNotificationRequest(String fromName, String fromEmailAddress, String toName, String toEmailAddress,
			String customerProfileId, String alertType, PreferredLanguage preferredLanguage, Map<String,Object> templateContext,
			int event) {
		super();
		this.fromName = fromName;
		this.fromEmailAddress = fromEmailAddress;
		this.toName = toName;
		this.toEmailAddress = toEmailAddress;
		this.customerProfileId = customerProfileId;
		this.alertType = alertType;
		this.preferredLanguage = preferredLanguage;
		this.templateContext = templateContext;
		this.event = event;
	}

	/**
	 * @return the fromName
	 */
	public String getFromName() {
		return fromName;
	}

	/**
	 * @param fromName the fromName to set
	 */
	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	/**
	 * @return the fromEmailAddress
	 */
	public String getFromEmailAddress() {
		return fromEmailAddress;
	}

	/**
	 * @param fromEmailAddress the fromEmailAddress to set
	 */
	public void setFromEmailAddress(String fromEmailAddress) {
		this.fromEmailAddress = fromEmailAddress;
	}

	/**
	 * @return the toName
	 */
	public String getToName() {
		return toName;
	}

	/**
	 * @param toName the toName to set
	 */
	public void setToName(String toName) {
		this.toName = toName;
	}

	/**
	 * @return the toEmailAddress
	 */
	public String getToEmailAddress() {
		return toEmailAddress;
	}

	/**
	 * @param toEmailAddress the toEmailAddress to set
	 */
	public void setToEmailAddress(String toEmailAddress) {
		this.toEmailAddress = toEmailAddress;
	}

	/**
	 * @return the customerProfileId
	 */
	public String getCustomerProfileId() {
		return customerProfileId;
	}

	/**
	 * @param customerProfileId the customerProfileId to set
	 */
	public void setCustomerProfileId(String customerProfileId) {
		this.customerProfileId = customerProfileId;
	}

	/**
	 * @return the alertType
	 */
	public String getAlertType() {
		return alertType;
	}

	/**
	 * @param alertType the alertType to set
	 */
	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	/**
	 * @return the preferredLanguage
	 */
	public PreferredLanguage getPreferredLanguage() {
		if (preferredLanguage == null) { 
			preferredLanguage = PreferredLanguage.ENGLISH;
		}
		return preferredLanguage;
	}

	/**
	 * @param preferredLanguage the preferredLanguage to set
	 */
	public void setPreferredLanguage(PreferredLanguage preferredLanguage) {
		if (preferredLanguage == null){
			this.preferredLanguage = PreferredLanguage.ENGLISH;
		}else {
			this.preferredLanguage = preferredLanguage;
		}
	}

	/**
	 * @return the templateContext
	 */
	public Map<String, Object> getTemplateContext() {
		return templateContext;
	}

	/**
	 * @param templateContext the templateContext to set
	 */
	public void setTemplateContext(Map<String, Object> templateContext) {
		this.templateContext = templateContext;
	}

	/**
	 * @return the event
	 */
	public int getEvent() {
		return event;
	}

	/**
	 * @param event the event to set
	 */
	public void setEvent(int event) {
		this.event = event;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AddNotificationRequest [fromName=" + fromName + ", fromEmailAddress=" + fromEmailAddress + ", toName="
				+ toName + ", toEmailAddress=" + toEmailAddress + ", customerProfileId=" + customerProfileId
				+ ", alertType=" + alertType + ", preferredLanguage=" + preferredLanguage + ", templateContext="
				+ templateContext + ", event=" + event + "]";
	}
	
	

}
