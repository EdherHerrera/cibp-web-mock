package evertec.cibp.web.request;

import java.io.Serializable;

import evertec.cibp.core.models.customer.applications.CustomerApplication;

/**
 * Copyright 2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * CustomerApplicationRequest class.
 * <p>
 * The class CustomerApplicationRequest to send customerApplication details.
 *
 * @author Jelvin Ferreras
 * @version 1.0
 * @since 1.0
 */
public class CustomerApplicationRequest implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * applicationType
     * Type String
     */
    private String applicationType;

    /**
     * attributeDate
     * Type String
     */
    private String attributeData;

    /**
     * secureAttributeDate
     * Type String
     */
    private String secureAttributeData;

    /**
     * csrUsername
     * Type String
     */
    private String csrUsername;

    /**
     * customerFirstName
     * Type String
     */
    private String customerFirstName;

    /**
     * customerLastName
     * Type String
     */
    private String customerLastName;
    
    /**
     * Constructor from Customer Application
     * */
    public CustomerApplicationRequest(CustomerApplication customerApplication) {
        this.applicationType = customerApplication.getApplicationType().toString();
        this.attributeData = customerApplication.getAttributeData();
        this.secureAttributeData = customerApplication.getSecureAttributeData();
        this.csrUsername = customerApplication.getCsrUsername();
        this.customerFirstName = customerApplication.getCustomerProfile().getFirstName();
        this.customerLastName = customerApplication.getCustomerProfile().getLastName();
    }

    /**
     * Gets the Application type.
     *
     * @return the Application type
     */
    public String getApplicationType() {
        return applicationType;
    }

    /**
     * Sets the Application type.
     *
     * @param applicationType, the applicationType
     */
    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    /**
     * Gets the Attribute data.
     *
     * @return the Attribute data
     */
    public String getAttributeData() {
        return attributeData;
    }

    /**
     * Sets the Attribute data.
     *
     * @param attributeData, the attributeData
     */
    public void setAttributeData(String attributeData) {
        this.attributeData = attributeData;
    }

    /**
     * Gets the Secure attribute data.
     *
     * @return the Secure attribute data
     */
    public String getSecureAttributeData() {
        return secureAttributeData;
    }

    /**
     * Sets the Secure attribute data.
     *
     * @param secureAttributeData, the secureAttributeData
     */
    public void setSecureAttributeData(String secureAttributeData) {
        this.secureAttributeData = secureAttributeData;
    }

    /**
     * Gets the CSR username.
     *
     * @return the CSR username
     */
    public String getCsrUsername() {
        return csrUsername;
    }

    /**
     * Sets the CSR username.
     *
     * @param csrUsername, the csrUsername
     */
    public void setCsrUsername(String csrUsername) {
        this.csrUsername = csrUsername;
    }

    /**
     * Gets the Customer firstname.
     *
     * @return the Customer firstname
     */
    public String getCustomerFirstName() {
        return customerFirstName;
    }

    /**
     * Sets the Customer firstname.
     *
     * @param customerFirstName, the customerFirstName
     */
    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    /**
     * Gets the Customer lastname.
     *
     * @return the Customer lastname
     */
    public String getCustomerLastName() {
        return customerLastName;
    }

    /**
     * Sets the Customer lastname.
     *
     * @param customerLastName, the customerLastName
     */
    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    @Override
    public String toString() {
        return "CustomerApplicationRequest{" +
                "applicationType='" + applicationType + '\'' +
                ", attributeData='" + attributeData + '\'' +
                ", secureAttributeData='" + secureAttributeData + '\'' +
                ", csrUsername='" + csrUsername + '\'' +
                ", customerFirstName='" + customerFirstName + '\'' +
                ", customerLastName='" + customerLastName + '\'' +
                '}';
    }
}
