package evertec.cibp.web.request;

import javax.servlet.http.HttpServletRequest;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.web.session.UserSession;

/**
 * This class provides a wrapper for the request of vault accounts service
 */
public class VaultAccountsRequest {
	
	private UserSession userSession;
	private CustomerProfile customerProfile;
	private HttpServletRequest request;
	private BankingEntitlement bankingEntitlement;
	
	/**
	 * Default constructor
	 */
	public VaultAccountsRequest() {
		
	}
	
	/**
	 * @return The user session
	 */
	public UserSession getUserSession() {
		return userSession;
	}
	
	/**
	 * @param userSession The user session
	 */
	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}
	
	/**
	 * @return The customer profile
	 */
	public CustomerProfile getCustomerProfile() {
		return customerProfile;
	}
	
	/**
	 * @param customerProfile The customer profile
	 */
	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}

	/**
	 * @return the request
	 */
	public HttpServletRequest getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	/**
	 * @return the bankingEntitlement
	 */
	public BankingEntitlement getBankingEntitlement() {
		return bankingEntitlement;
	}

	/**
	 * @param bankingEntitlement the bankingEntitlement to set
	 */
	public void setBankingEntitlement(BankingEntitlement bankingEntitlement) {
		this.bankingEntitlement = bankingEntitlement;
	}
	
}
