package evertec.cibp.web.request;

import java.util.Calendar;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.vs.models.PaymentFilterType;
import evertec.cibp.web.session.UserSession;


/**
 * Copyright 2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * PaymentHistoryRequest class.
 * <p>
 *
 * @author Jorge De Los Santos
 * @version 1.0
 * @since 1.0
 */
public class PaymentHistoryRequest {
	
	private CustomerProfile customerProfile;
	private UserSession userSession;
	private PaymentFilterType paymentFilter;
	private int paymentSize;
	private String[] payeeId;
	private Calendar startDate;
	
	/**
	 * Constructor that creates a PaymentHistoryRequest object
	 */
	public PaymentHistoryRequest() {
		super();
	}


	/**
	 * Constructor that creates a PaymentHistoryRequest object
	 * 
	 * @param customerProfile The Customer Profile
	 * @param userSession The User Session
	 * @param paymentFilter The payment Filter
	 * @param paymentSize The payment Size
	 * @param payeeId The payeeId List
	 * @param startDate The Start Date
	 */
	public PaymentHistoryRequest(CustomerProfile customerProfile, UserSession userSession,
			PaymentFilterType paymentFilter, int paymentSize, String[] payeeId, Calendar startDate) {
		super();
		this.customerProfile = customerProfile;
		this.userSession = userSession;
		this.paymentFilter = paymentFilter;
		this.paymentSize = paymentSize;
		this.payeeId = payeeId;
		this.startDate = startDate;
	}


	public CustomerProfile getCustomerProfile() {
		return customerProfile;
	}


	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}


	public UserSession getUserSession() {
		return userSession;
	}


	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}


	public PaymentFilterType getPaymentFilter() {
		return paymentFilter;
	}


	public void setPaymentFilter(PaymentFilterType paymentFilter) {
		this.paymentFilter = paymentFilter;
	}


	public int getPaymentSize() {
		return paymentSize;
	}


	public void setPaymentSize(int paymentSize) {
		this.paymentSize = paymentSize;
	}


	public String[] getPayeeId() {
		return payeeId;
	}


	public void setPayeeId(String[] payeeId) {
		this.payeeId = payeeId;
	}


	public Calendar getStartDate() {
		return startDate;
	}


	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}

}
