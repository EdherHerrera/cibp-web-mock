package evertec.cibp.web.request;

import com.rsa.csd.ws.UpdateUserRequest;

/**
 * <h1>UpdateUserRSAOOBRequest</h1>
 * This POJO is used to encapsulate the user update RSA OOB request.
 * <p>
 * Copyright 2019 Evertec, Inc. All Rights Reserved.
 *
 * @author Henry Terrero
 * @version 1.0
 * @since 2019-03-29
 */
public class UpdateUserRSAOOBRequest {
	
	 /**
     * This is the RSA UpdateUserRequest object.
     */
    private UpdateUserRequest updateUserRequest;
    
    /**
     * This is the user credential type.
     */
    private String credentialType;
    
    /**
     * This is the optional language request.
     */
    private String language;
    
    /**
     * This is the default class constructor.
     */
    public UpdateUserRSAOOBRequest() {
    }

    /**
     * This is the parameterized class constructor.
     *
     * @param updateUserRequest This is the update request object.
     * @param credentialType    This is the user credential type.
     * @param language          This is the optional language request.
     */
    public UpdateUserRSAOOBRequest(UpdateUserRequest updateUserRequest, String credentialType, String language) {
        this.updateUserRequest = updateUserRequest;
        this.credentialType = credentialType;
        this.language = language;
    }
    
    /**
     * This method returns the RSA request object.
     *
     * @return This returns the RSA request object.
     */
    public UpdateUserRequest getUpdateUserRequest() {
        return updateUserRequest;
    }

    /**
     * This method sets the RSA request object.
     *
     * @param updateUserRequest This is the RSA request object.
     */
    public void setUpdateUserRequest(UpdateUserRequest updateUserRequest) {
        this.updateUserRequest = updateUserRequest;
    }

    /**
     * This method returns the user credential type.
     *
     * @return This returns the user credential type.
     */
    public String getCredentialType() {
        return credentialType;
    }

    /**
     * This method sets the user credential type.
     *
     * @param credentialType This is the user credential type.
     */
    public void setCredentialType(String credentialType) {
        this.credentialType = credentialType;
    }

    /**
     * This method sets the optional request language.
     *
     * @return This returns the optional request language.
     */
    public String getLanguage() {
        return language;
    }

    /**
     * This method sets the optional request language.
     *
     * @param language This is the optional request language.
     */
    public void setLanguage(String language) {
        this.language = language;
    }

}
