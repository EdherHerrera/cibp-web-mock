package evertec.cibp.web.request;

import evertec.cibp.web.session.UserSession;

/**
 * This class provides a wrapper for the request of alerts service
 */
public class AlertsHistoryRequest {
	private UserSession userSession;
	private String profileId;
	private String taxId;
	
	
	/**
	 * Default constructor
	 */
	public AlertsHistoryRequest() {
		
	}


	/**
	 * @return The user session
	 */
	public UserSession getUserSession() {
		return userSession;
	}


	/**
	 * @param userSession The user session
	 */
	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}


	/**
	 * @return The profile id
	 */
	public String getProfileId() {
		return profileId;
	}


	/**
	 * @param profileId The profile id
	 */
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}


	/**
	 * @return The profile id
	 */
	public String getTaxId() {
		return taxId;
	}


	/**
	 * @param taxId The tax id
	 */
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	
	

}
