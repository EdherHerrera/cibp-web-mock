package evertec.cibp.web.request;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Copyright 2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * CheckCopyRequest class.
 * <p>
 * The class CheckCopyRequest to request CheckCopy.
 *
 * @author Jelvin Ferreras
 * @version 1.0
 * @since 1.0
 */
public class CheckCopyRequest implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * accountNumber
     * Type String
     */
    private String accountNumber;

    /**
     * amount
     * Type String
     */
    private String amount;

    /**
     * checkNumber
     * Type String
     */
    private String checkNumber;

    /**
     * checkDate
     * Type String
     */
    private String checkDate;

    /**
     * bankId
     * Type String
     */
    private String bankId;
   
   /** Default constructor. */
   public CheckCopyRequest() {
	   
   }
   
   /** Service call constructor */
   public CheckCopyRequest(String accountNumber, BigDecimal amt, String checkNumber, Calendar checkDate, String bankId) {
	   this.accountNumber = accountNumber;
	   this.amount = amt.toString();
	   this.checkNumber = checkNumber;
	   this.checkDate = new SimpleDateFormat("yyyy-MM-dd").format(checkDate.getTime());
	   this.bankId = bankId;
   }
   
   /**
    * Gets the accountNumber.
    *
    * @return the account number
    */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the accountNumber.
     *
     * @param accountNumber, the account number
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * Gets the amount.
     *
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Sets the amount.
     *
     * @param amount, the amount
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * Gets the checkNumber.
     *
     * @return the check number
     */
    public String getCheckNumber() {
        return checkNumber;
    }

    /**
     * Sets the check number.
     *
     * @param checkNumber, the check number
     */
    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    /**
     * Gets the checkDate.
     *
     * @return the check date
     */
    public String getCheckDate() {
        return checkDate;
    }

    /**
     * Sets the check date.
     *
     * @param checkDate, the check date
     */
    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    /**
     * Gets the bankId.
     *
     * @return the bank ID
     */
    public String getBankId() {
        return bankId;
    }

    /**
     * Sets the bank ID.
     *
     * @param bankId, the bank id
     */
    public void setBankId(String bankId) {
        this.bankId = bankId;
    }
}
