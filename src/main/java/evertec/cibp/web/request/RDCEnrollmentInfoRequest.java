package evertec.cibp.web.request;

import java.math.BigDecimal;

public class RDCEnrollmentInfoRequest {

	private String rdcBid;
	private String settlementGroupIdChk;
	private String settlementGroupIdSav;
	private BigDecimal userDailyAmount;
	private int userDailyItem;
	private BigDecimal userMultidayAmount;
	private int userMultidayItem;

	public String getRdcBid() {
		return rdcBid;
	}

	public void setRdcBid(String rdcBid) {
		this.rdcBid = rdcBid;
	}

	public String getSettlementGroupIdChk() {
		return settlementGroupIdChk;
	}

	public void setSettlementGroupIdChk(String settlementGroupIdChk) {
		this.settlementGroupIdChk = settlementGroupIdChk;
	}

	public String getSettlementGroupIdSav() {
		return settlementGroupIdSav;
	}

	public void setSettlementGroupIdSav(String settlementGroupIdSav) {
		this.settlementGroupIdSav = settlementGroupIdSav;
	}

	public BigDecimal getUserDailyAmount() {
		return userDailyAmount;
	}

	public void setUserDailyAmount(BigDecimal userDailyAmount) {
		this.userDailyAmount = userDailyAmount;
	}

	public int getUserDailyItem() {
		return userDailyItem;
	}

	public void setUserDailyItem(int userDailyItem) {
		this.userDailyItem = userDailyItem;
	}

	public BigDecimal getUserMultidayAmount() {
		return userMultidayAmount;
	}

	public void setUserMultidayAmount(BigDecimal userMultidayAmount) {
		this.userMultidayAmount = userMultidayAmount;
	}

	public int getUserMultidayItem() {
		return userMultidayItem;
	}

	public void setUserMultidayItem(int userMultidayItem) {
		this.userMultidayItem = userMultidayItem;
	}
}
