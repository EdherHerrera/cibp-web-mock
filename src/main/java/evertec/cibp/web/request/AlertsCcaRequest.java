package evertec.cibp.web.request;

/**
 * Copyright 2018 Evertec, Inc.  All Rights Reserved.
 *
 * AlertsCcaRequest model.
 *
 */
public class AlertsCcaRequest {
	
	 /**
     * This is a property for being mapping
     * profile id
     */
	private String profileId;
	
	  /**
     * This is a property for being mapping
     * plastic owner id or plastic number
     */
	private String plastic;
	
	  /**
     * This is a property for being mapping
     * phone number
     */
	private String phoneNumber;
	
	 /**
     * This is a property for being mapping
     * email
     */
	private String email;
	
	 /**
     * This is a property for being mapping
     * card hash
     */
	private String cardHash;

	  /**
     * Gets the profile id.
     *
     * @return the profile id
     */
	public String getProfileId() {
		return profileId;
	}

	  /**
     * Sets the profile id.
     *
     * @param profileId the profile id to set
     */
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	  /**
     * Gets the plastic owner id or plastic number.
     *
     * @return the plastic
     */
	public String getPlastic() {
		return plastic;
	}

	  /**
     * Sets the plastic owner id or plastic number.
     *
     * @param plastic the plastic to set
     */
	public void setPlastic(String plastic) {
		this.plastic = plastic;
	}

	 /**
     * Gets the phone number.
     *
     * @return the phone number
     */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	  /**
     * Sets the phone number.
     *
     * @param phoneNumber the phone number to set
     */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	  /**
     * Gets the email.
     *
     * @return the email
     */
	public String getEmail() {
		return email;
	}

	 /**
     * Sets the email.
     *
     * @param email the email to set
     */
	public void setEmail(String email) {
		this.email = email;
	}

	  /**
     * Gets the card hash.
     *
     * @return the card hash
     */
	public String getCardHash() {
		return cardHash;
	}

	  /**
     * Sets the card hash.
     *
     * @param cardHash the card hash to set
     */
	public void setCardHash(String cardHash) {
		this.cardHash = cardHash;
	}
	
	
}
