package evertec.cibp.web.request;

import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.core.services.requests.adaptiveauth.ChallengeOobRequest;
import evertec.rsa.RSAConnector;

import java.io.Serializable;

public class ChallengeOobMsRequest implements Serializable {
	
	private static final long serialVersionUID = 3013179671795316957L;
	
	/**
    *
    * The credential type
    *
    * */
   private String credentialType;

   /**
    *
    * The username
    *
    * */
   private String username;

   /**
    * 
    * The challenge phone
    *
    * */
   private String challengephone;

   /**
    * 
    * The Device Request
    *
    * */
   private DeviceRequest deviceRequest;

   /**
    *
    * The language
    *
    * */
   private RSAConnector.Lang language;

   /**
    *
    * The RSA Transaction ID
    *
    * */
   private String rsaTransactionId;

   /**
    *
    * The RSA Session ID
    *
    * */
   private String rsaSessionId;

   /**
    * The boolean for new challenge
    */
   private boolean newChallenge;
	
	public ChallengeOobMsRequest(ChallengeOobRequest request) {
		
		this.credentialType = request.getCredentialType().toString();
		this.username = request.getUsername();
		this.challengephone = request.getChallengephone();
		this.deviceRequest = request.getDeviceRequest();
		this.language = request.getLanguage();
		this.rsaTransactionId = request.getRsaTransactionId();
		this.rsaSessionId = request.getRsaSessionId();
		this.newChallenge = request.isNewChallenge();
		
	}
	
	/**
     * Gets the credentialType.
     *
     * @return the credentialType
     */
    public String getCredentialType() {
        return credentialType;
    }

    /**
     * Sets the credentialType.
     *
     * @param credentialType the credentialType to set
     */
    public void setCredentialType(String credentialType) {
        this.credentialType = credentialType;
    }

    /**
     * Gets the username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username.
     *
     * @param username the credentialType to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the challengePhone.
     *
     * @return the challengePhone
     */
    public String getChallengephone() {
        return challengephone;
    }

    /**
     * Sets the challengephone.
     *
     * @param challengephone the credentialType to set
     */
    public void setChallengephone(String challengephone) {
        this.challengephone = challengephone;
    }

    /**
     * Gets the deviceRequest
     *
     * @return the deviceRequest
     */
    public DeviceRequest getDeviceRequest() {
        return deviceRequest;
    }

    /**
     * Sets the deviceRequest.
     *
     * @param deviceRequest the credentialType to set
     */
    public void setDeviceRequest(DeviceRequest deviceRequest) {
        this.deviceRequest = deviceRequest;
    }

    /**
     * Gets the language
     *
     * @return the language
     */
    public RSAConnector.Lang getLanguage() {
        return language;
    }

    /**
     * Sets the language.
     *
     * @param language the credentialType to set
     */
    public void setLanguage(RSAConnector.Lang language) {
        this.language = language;
    }

    /**
     * Gets the rsaTransactionId
     *
     * @return the rsaTransactionId
     */
    public String getRsaTransactionId() {
        return rsaTransactionId;
    }

    /**
     * Sets the rsaTransactionId.
     *
     * @param rsaTransactionId the credentialType to set
     */
    public void setRsaTransactionId(String rsaTransactionId) {
        this.rsaTransactionId = rsaTransactionId;
    }

    /**
     * Gets the rsaSessionId
     *
     * @return the rsaSessionId
     */
    public String getRsaSessionId() {
        return rsaSessionId;
    }

    /**
     * Sets the rsaSessionId.
     *
     * @param rsaSessionId the credentialType to set
     */
    public void setRsaSessionId(String rsaSessionId) {
        this.rsaSessionId = rsaSessionId;
    }

    /**
     * Gets the value of newChallenge
     *
     * @return newChallenge value
     */
    public boolean isNewChallenge() {
        return newChallenge;
    }

    /**
     * Sets the newChallenge.
     *
     * @param newChallenge the credentialType to set
     */
    public void setNewChallenge(boolean newChallenge) {
        this.newChallenge = newChallenge;
    }

}
