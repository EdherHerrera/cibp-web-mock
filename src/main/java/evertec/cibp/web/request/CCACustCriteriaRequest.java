package evertec.cibp.web.request;

import evertec.otherservices.alertsDataTypes.AddAlertsCustCriteriaReqTypeHierarchyInfoEnrollHierarchy;
import evertec.otherservices.alertsDataTypes.CCAAlertsContactsDataType;
import evertec.otherservices.alertsDataTypes.CCAAlertsCriteriaDataType;
import evertec.otherservices.alertsDataTypes.CCAAlertsCriteriaHierarchyDataType;
import evertec.otherservices.alertsEnums.AddAlertsCustCriteriaRequestTypeHierarchyInfoEnrollHierarchyAlertOwner;
import evertec.otherservices.alertsEnums.CCAAlertsDataTypeStatus;
import evertec.otherservices.alertsEnums.KeyType;
import evertec.otherservices.alertsRequests.MntAlertsCustCriteriaReqType;

import java.io.Serializable;

/**
 * Copyright 2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * CCACustCriteriaRequest class.
 * <p>
 * The class CCACustCriteriaRequest for mntCCACustCriteria controller method.
 *
 * @author Jelvin Ferreras
 * @version 1.0
 * @since 1.0
 */
public class CCACustCriteriaRequest implements Serializable {

    /**
     * MntAlertsCustCriteriaReqType class fields
     * */
    private String status;
    private String desc;
    private CCAAlertsContactsDataType contacts;
    private CCAAlertsCriteriaDataType criteria;
    private Boolean enrolledNbrIsUSID;
    private CCAAlertsCriteriaHierarchyDataType hierarchyInfo;
    private String enrolledNbr;
    private String alertID;
    private String version;

    /**
     * TSYSAcctMaintenanceReqType parent class fields
     * */
    private String key;
    private String keyType;
    private String cardNbr;
    private String acctID;
    private String correlationID;
    private String giftCardNbr;
    private String usid;
    private String alertsMemberID;

    /**
     * TSYSMsgType parent class fields
     * */
    private String reqID;

    /**
     * AddAlertsCustCriteriaReqTypeHierarchyInfoEnrollHierarchy helper class fields
     * */
    private String alertOwner;

    /**
     * Default constructor
     * */
    public CCACustCriteriaRequest() {

    }
    
    /**
     * MntAlertsCustCriteriaReqType constructor
     * */
    public CCACustCriteriaRequest(MntAlertsCustCriteriaReqType request) {

    	this.status = request.getStatus().getValue();
    	this.desc = request.getDesc();
    	this.contacts = request.getContacts();
    	this.criteria = request.getCriteria();
    	this.enrolledNbrIsUSID = request.getEnrolledNbrIsUSID();
        this.hierarchyInfo = request.getHierarchyInfo();
        this.hierarchyInfo.setEnrollHierarchy(null);
        this.enrolledNbr = request.getEnrolledNbr();
        this.alertID = request.getAlertID();
        this.version = request.getVersion();
        this.key = request.getKey();
        this.keyType = request.getKeyType().getValue();
        this.cardNbr = request.getCardNbr();
        this.acctID = request.getAcctID();
        this.correlationID = request.getCorrelationID();
        this.giftCardNbr = request.getGiftCardNbr();
        this.usid = request.getUSID();
        this.alertsMemberID = request.getAlertsMemberID();
        this.reqID = request.getReqID();
        this.alertOwner = request.getHierarchyInfo().getEnrollHierarchy().getAlertOwner().getValue();
    
    }

    public MntAlertsCustCriteriaReqType toMntAlertsCustCriteriaReqType() {

        AddAlertsCustCriteriaReqTypeHierarchyInfoEnrollHierarchy item = new AddAlertsCustCriteriaReqTypeHierarchyInfoEnrollHierarchy();
        item.setAlertOwner(AddAlertsCustCriteriaRequestTypeHierarchyInfoEnrollHierarchyAlertOwner.fromString(alertOwner));

        this.hierarchyInfo.setEnrollHierarchy(item);

        MntAlertsCustCriteriaReqType request =  new MntAlertsCustCriteriaReqType();
        request.setStatus(CCAAlertsDataTypeStatus.fromValue(status));
        request.setDesc(desc);
        request.setContacts(contacts);
        request.setCriteria(criteria);
        request.setEnrolledNbrIsUSID(enrolledNbrIsUSID);
        request.setHierarchyInfo(hierarchyInfo);
        request.setEnrolledNbr(enrolledNbr);
        request.setAlertID(alertID);
        request.setVersion(version);
        request.setKey(key);
        request.setKeyType(KeyType.fromString(keyType));
        request.setCardNbr(cardNbr);
        request.setAcctID(acctID);
        request.setCorrelationID(correlationID);
        request.setGiftCardNbr(giftCardNbr);
        request.setUSID(usid);
        request.setAlertsMemberID(alertsMemberID);
        request.setReqID(reqID);
        return request;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public CCAAlertsContactsDataType getContacts() {
        return contacts;
    }

    public void setContacts(CCAAlertsContactsDataType contacts) {
        this.contacts = contacts;
    }

    public CCAAlertsCriteriaDataType getCriteria() {
        return criteria;
    }

    public void setCriteria(CCAAlertsCriteriaDataType criteria) {
        this.criteria = criteria;
    }

    public Boolean getEnrolledNbrIsUSID() {
        return enrolledNbrIsUSID;
    }

    public void setEnrolledNbrIsUSID(Boolean enrolledNbrIsUSID) {
        this.enrolledNbrIsUSID = enrolledNbrIsUSID;
    }

    public CCAAlertsCriteriaHierarchyDataType getHierarchyInfo() {
        return hierarchyInfo;
    }

    public void setHierarchyInfo(CCAAlertsCriteriaHierarchyDataType hierarchyInfo) {
        this.hierarchyInfo = hierarchyInfo;
    }

    public String getEnrolledNbr() {
        return enrolledNbr;
    }

    public void setEnrolledNbr(String enrolledNbr) {
        this.enrolledNbr = enrolledNbr;
    }

    public String getAlertID() {
        return alertID;
    }

    public void setAlertID(String alertID) {
        this.alertID = alertID;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKeyType() {
        return keyType;
    }

    public void setKeyType(String keyType) {
        this.keyType = keyType;
    }

    public String getCardNbr() {
        return cardNbr;
    }

    public void setCardNbr(String cardNbr) {
        this.cardNbr = cardNbr;
    }

    public String getAcctID() {
        return acctID;
    }

    public void setAcctID(String acctID) {
        this.acctID = acctID;
    }

    public String getCorrelationID() {
        return correlationID;
    }

    public void setCorrelationID(String correlationID) {
        this.correlationID = correlationID;
    }

    public String getGiftCardNbr() {
        return giftCardNbr;
    }

    public void setGiftCardNbr(String giftCardNbr) {
        this.giftCardNbr = giftCardNbr;
    }

    public String getUsid() {
        return usid;
    }

    public void setUsid(String usid) {
        this.usid = usid;
    }

    public String getAlertsMemberID() {
        return alertsMemberID;
    }

    public void setAlertsMemberID(String alertsMemberID) {
        this.alertsMemberID = alertsMemberID;
    }

    public String getReqID() {
        return reqID;
    }

    public String getAlertOwner() {
        return alertOwner;
    }

    public void setAlertOwner(String alertOwner) {
        this.alertOwner = alertOwner;
    }

    public void setReqID(String reqID) {
        this.reqID = reqID;
    }

    @Override
    public String toString() {
        return "CCACustCriteriaRequest{" +
                "status='" + status + '\'' +
                ", desc='" + desc + '\'' +
                ", contacts=" + contacts +
                ", criteria=" + criteria +
                ", enrolledNbrIsUSID=" + enrolledNbrIsUSID +
                ", hierarchyInfo=" + hierarchyInfo +
                ", enrolledNbr='" + enrolledNbr + '\'' +
                ", alertID='" + alertID + '\'' +
                ", version='" + version + '\'' +
                '}';
    }
}

