package evertec.cibp.web.request;

import java.util.Objects;

import evertec.cibp.web.utils.Utils;

public class TsysLoyaltyRewardsRedemptionRequest {
	
	public enum RedemptionType {
		STATEMENT_CREDIT("STATEMENT_CREDIT"), 
		DIRECT_DEPOSIT("DIRECT_DEPOSIT");
		
		private RedemptionType(String value) {
			this.string = value;
		}
		
		private final String string;
	
		public String toString() { return string; }
	}
	
	/**
	 * Used only by cibp-web to specify to cibp-creditcards-service
	 * how the customer desires to redeem: statement credit or direct 
	 * deposit.
	 */
	private RedemptionType redemptionType;
	
	private String rewardsValue;

	private DirectDepositAccountInfo transferAccountInfo;

	
	public boolean isValid() {
		
		boolean redemptionTypeIsValid = redemptionType != null;
		
		boolean rewardsValueIsValid = rewardsValue != null && 
				rewardsValue.matches(Utils.MONETARY_VALUE);
		
		/**
		 * null is for statement credit, direct deposit requires this variable not be null and be valid
		 */
		boolean transferAccountInfoIsValid = transferAccountInfo == null || 
				transferAccountInfo.isValid();
		
		return redemptionTypeIsValid && rewardsValueIsValid && transferAccountInfoIsValid;
	}

	public RedemptionType getRedemptionType() {
		return redemptionType;
	}

	public void setRedemptionType(RedemptionType redemptionType) {
		this.redemptionType = redemptionType;
	}

	/**
	 * Get transferAccountInfo
	 * 
	 * @return transferAccountInfo
	 **/
	public DirectDepositAccountInfo getTransferAccountInfo() {
		return transferAccountInfo;
	}

	public void setTransferAccountInfo(DirectDepositAccountInfo transferAccountInfo) {
		this.transferAccountInfo = transferAccountInfo;
	}

	/**
	 * Value of rewards the customer wishes to redeem.
	 * 
	 * @return rewardsValue
	 **/
	public String getRewardsValue() {
		return rewardsValue;
	}

	public void setRewardsValue(String rewardsValue) {
		this.rewardsValue = rewardsValue;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TsysLoyaltyRewardsRedemptionRequest directDepositRequest = (TsysLoyaltyRewardsRedemptionRequest) o;
		return Objects.equals(this.transferAccountInfo, directDepositRequest.transferAccountInfo)
				&& Objects.equals(this.rewardsValue, directDepositRequest.rewardsValue);
	}

	@Override
	public int hashCode() {
		return Objects.hash(transferAccountInfo, rewardsValue);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class DirectDepositRequest {\n");
		sb.append("    transferAccountInfo: ").append(toIndentedString(transferAccountInfo)).append("\n");
		sb.append("    rewardsValue: ").append(toIndentedString(rewardsValue)).append("\n");
		sb.append("    redemptionType: ").append(toIndentedString(redemptionType)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	public static class DirectDepositAccountInfo {

		private String routingNumber = null;

		/**
		 * Optional value
		 */
		private String accountType = null;

		private String accountNumber = null;
		
		public boolean isValid( ) {
			return accountNumber != null && accountNumber.matches("\\d+") &&
					routingNumber != null && routingNumber.matches("\\d+");
		}

		/**
		 * routing number associated to account user wishes to transfer funds to
		 * 
		 * @return routingNumber
		 **/
		public String getRoutingNumber() {
			return routingNumber;
		}

		public void setRoutingNumber(String routingNumber) {
			this.routingNumber = routingNumber;
		}

		/**
		 * optional field allowing users to specify account type
		 * 
		 * @return accountType
		 **/
		public String getAccountType() {
			return accountType;
		}

		public void setAccountType(String accountType) {
			this.accountType = accountType;
		}

		/**
		 * account number associated to account user wishes to transfer funds to
		 * 
		 * @return accountNumber
		 **/
		public String getAccountNumber() {
			return accountNumber;
		}

		public void setAccountNumber(String accountNumber) {
			this.accountNumber = accountNumber;
		}

		@Override
		public boolean equals(java.lang.Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			DirectDepositAccountInfo directDepositAccountInfo = (DirectDepositAccountInfo) o;
			return Objects.equals(this.routingNumber, directDepositAccountInfo.routingNumber)
					&& Objects.equals(this.accountType, directDepositAccountInfo.accountType)
					&& Objects.equals(this.accountNumber, directDepositAccountInfo.accountNumber);
		}

		@Override
		public int hashCode() {
			return Objects.hash(routingNumber, accountType, accountNumber);
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("class DirectDepositAccountInfo {\n");

			sb.append("    routingNumber: ").append(toIndentedString(routingNumber)).append("\n");
			sb.append("    accountType: ").append(toIndentedString(accountType)).append("\n");
			sb.append("    accountNumber: ").append(toIndentedString(accountNumber)).append("\n");
			sb.append("}");
			return sb.toString();
		}

		/**
		 * Convert the given object to string with each line indented by 4 spaces
		 * (except the first line).
		 */
		private String toIndentedString(java.lang.Object o) {
			if (o == null) {
				return "null";
			}
			return o.toString().replace("\n", "\n    ");
		}
	}
}
