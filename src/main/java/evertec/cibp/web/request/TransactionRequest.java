package evertec.cibp.web.request;

import java.io.Serializable;
import java.util.Calendar;



import bppr.tv.beans.accounts.types.CardAccount;
import bppr.tv.beans.statements.Statement;
import evertec.cibp.vs.beans.BankingSession;

/**
 * This class is used to get the data from client that request credit card
 * transactions
 * 
 * @author Faustino Delgado Camacho
 *
 */
public class TransactionRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5906019952108510004L;

	/**
	 * Number of transactions per page
	 */
	private int transactionsPerPage;

	/**
	 * Number of pages requested
	 */
	private int pageRequested;

	/**
	 * Sorting order ascending or descending
	 */
	private int sortOrder;

	/**
	 * Statement of the credit card transactions. This field is used in the getCreditCardStatementTransactions(...)
	 */
	private Statement statementRequested;
	
	/**
	 * Account to be requested. In this case needs CardAccount Object to be consumed by the TvConnector.
	 */
	private CardAccount accountRequested;
	
	/**
	 * Banking session object needs by TVConnector.
	 */
	private BankingSession bankingSession;
	
	/**
	 * Start date
	 */
	private Calendar startDate;

	/**
	 * @return the transactionsPerPage
	 */
	public int getTransactionsPerPage() {
		return transactionsPerPage;
	}

	/**
	 * @param transactionsPerPage the transactionsPerPage to set
	 */
	public void setTransactionsPerPage(int transactionsPerPage) {
		this.transactionsPerPage = transactionsPerPage;
	}

	/**
	 * @return the pageRequested
	 */
	public int getPageRequested() {
		return pageRequested;
	}

	/**
	 * @param pageRequested the pageRequested to set
	 */
	public void setPageRequested(int pageRequested) {
		this.pageRequested = pageRequested;
	}

	/**
	 * @return the sortOrder
	 */
	public int getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder the sortOrder to set
	 */
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * @return the statementRequested
	 */
	public Statement getStatementRequested() {
		return statementRequested;
	}

	/**
	 * @param statementRequested the statementRequested to set
	 */
	public void setStatementRequested(Statement statementRequested) {
		this.statementRequested = statementRequested;
	}

	/**
	 * @return the accountRequested
	 */
	public CardAccount getAccountRequested() {
		return accountRequested;
	}

	/**
	 * @param accountRequested the accountRequested to set
	 */
	public void setAccountRequested(CardAccount accountRequested) {
		this.accountRequested = accountRequested;
	}

	/**
	 * @return the bankingSession
	 */
	public BankingSession getBankingSession() {
		return bankingSession;
	}

	/**
	 * @param bankingSession the bankingSession to set
	 */
	public void setBankingSession(BankingSession bankingSession) {
		this.bankingSession = bankingSession;
	}

	/**
	 * 
	 * @return
	 */
	public Calendar getStartDate() {
		return startDate;
	}

	/**
	 * 
	 * @param startDate
	 */
	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}
}
