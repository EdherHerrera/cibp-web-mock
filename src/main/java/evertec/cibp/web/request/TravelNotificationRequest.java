package evertec.cibp.web.request;

import evertec.cibp.core.services.CustomerServices;
import evertec.prophit.service.bean.TravelNotificationBean;

/**
 * This class provides a wrapper for the request of credit cards service
 */
public class TravelNotificationRequest {
	
	 /**
     * travelInfo
     * Type TravelNotificationBean
     */
	private TravelNotificationBean travelInfo;
	
	 /**
     * customerService
     * Type CustomerServices
     */
	private CustomerServices customerService;
	
	/**
	 * Default constructor
	 */
	public TravelNotificationRequest() {
		// Do nothing because of travelInfo and customerService.
		
	}
	
	/**
	 * Constructor
	 * @param travelInfo, the TravelNotificationBean
	 * @param customerService, the CustomerServices
	 */
	public TravelNotificationRequest(TravelNotificationBean travelInfo, CustomerServices customerService) {
		this.travelInfo = travelInfo;
		this.customerService = customerService;
		
	}

	  /**
     * Gets the TravelNotificationBean.
     *
     * @return the travelInfo
     */
	public TravelNotificationBean getTravelInfo() {
		return travelInfo;
	}

	 /**
     * Sets the TravelNotificationBean.
     *
     * @param travelInfo, the TravelNotificationBean
     */
	public void setTravelInfo(TravelNotificationBean travelInfo) {
		this.travelInfo = travelInfo;
	}

	  /**
     * Gets the CustomerServices.
     *
     * @return the customerService
     */
	public CustomerServices getCustomerService() {
		return customerService;
	}

	 /**
     * Sets the CustomerServices.
     *
     * @param customerService, the CustomerServices
     */
	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}
	
	
	

}
