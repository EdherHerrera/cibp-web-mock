package evertec.cibp.web.request;

/**
 * Copyright 2019 Evertec, Inc.  All Rights Reserved.
 * 
 * @author Jorge De Los Santos
 * @version 1.1
 * @since Jan 18, 2019 3:14:56 PM
 *
 */
public class CustomAlertsCcaRequest {
	
	/**
     * This is a property for being mapping
     * plastic owner id
     */
	private String plasticOwnerId;
    
    /**
     * This is a property for being mapping
     * plastic number
     */
	private String plasticNumber;
    
    /**
     * This is a property for being mapping
     * phone number
     */
	private String phoneNumber;
    
    /**
     * This is a property for being mapping
     * card hash
     */
	private String cardHash;
    
    /**
     * This is a property for being mapping
     * email
     */
	private String email;

	/**
	 * @return the plasticOwnerId
	 */
	public String getPlasticOwnerId() {
		return plasticOwnerId;
	}

	/**
	 * @param plasticOwnerId the plasticOwnerId to set
	 */
	public void setPlasticOwnerId(String plasticOwnerId) {
		this.plasticOwnerId = plasticOwnerId;
	}

	/**
	 * @return the plasticNumber
	 */
	public String getPlasticNumber() {
		return plasticNumber;
	}

	/**
	 * @param plasticNumber the plasticNumber to set
	 */
	public void setPlasticNumber(String plasticNumber) {
		this.plasticNumber = plasticNumber;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the cardHash
	 */
	public String getCardHash() {
		return cardHash;
	}

	/**
	 * @param cardHash the cardHash to set
	 */
	public void setCardHash(String cardHash) {
		this.cardHash = cardHash;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Override toString method
	 * */
	@Override
	public String toString() {
		return "AlertsCcaRequest [plasticOwnerId=".concat(plasticOwnerId).concat(", plasticNumber=").concat(plasticNumber)
				.concat(", phoneNumber=").concat(phoneNumber).concat(", cardHash=").concat(cardHash).concat(", email=").concat(email).concat("]");
	}
	
	

}
