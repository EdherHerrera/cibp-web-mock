package evertec.cibp.web.request;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import evertec.cibp.web.constants.RequestParams;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;

import evertec.cibp.core.enums.PushTokenStatus;
import evertec.cibp.web.utils.PushTokenConstants;

/**
 * PushTokensMobileRequest
 * @author et55596
 * @since 1.2
 * @version 1.0 
 */
public class PushTokensMobileRequest implements Serializable {

	private static final long serialVersionUID = 8670540710390513301L;
	
	private String token;
	private String deviceType;
	private String deviceModel;
	private String deviceName;
	private PushTokenStatus status;
	private String invalidToken;
	private boolean hasPermissions = true;
	private boolean isActive;
	
	
	/**
	 * Default Constructor
	 */
	public PushTokensMobileRequest() {
		super();
	}
	

	/**
	 * Custom Constructor
	 * Custom
	 *
	 * @param request HttpServletRequest
	 */
	public PushTokensMobileRequest(HttpServletRequest request) {
		super();

		if (!GenericValidator.isBlankOrNull(request.getParameter(RequestParams.PUSH_TOKEN))) {
			this.setToken(request.getParameter(RequestParams.PUSH_TOKEN));
		}

		if (!GenericValidator.isBlankOrNull(request.getParameter(RequestParams.INVALID_PUSH_TOKEN))) {
			this.setInvalidToken(request.getParameter(RequestParams.INVALID_PUSH_TOKEN));
		}

		if (!GenericValidator.isBlankOrNull(request.getParameter(RequestParams.DEVICE_TYPE))) {
			this.setDeviceType(request.getParameter(RequestParams.DEVICE_TYPE));
		}

		if (!GenericValidator.isBlankOrNull(request.getParameter(RequestParams.DEVICE_MODEL))) {
			this.setDeviceModel(request.getParameter(RequestParams.DEVICE_MODEL));
		}

		if (!GenericValidator.isBlankOrNull(request.getParameter(RequestParams.DEVICE_NAME))) {
			this.setDeviceName(request.getParameter(RequestParams.DEVICE_NAME));
		}

		if (!GenericValidator.isBlankOrNull(request.getParameter(RequestParams.HAS_PERMISSIONS))) {
			this.setHasPermissions(Boolean.parseBoolean(request.getParameter(RequestParams.HAS_PERMISSIONS)));
			this.status = hasPermissions ? PushTokenStatus.ACTIVE : PushTokenStatus.INACTIVE;
		}

		if (!GenericValidator.isBlankOrNull(request.getParameter(RequestParams.IS_ACTIVE))) {
			this.isActive = Boolean.parseBoolean(request.getParameter(RequestParams.IS_ACTIVE));
			if (this.hasPermissions)
				this.status = this.isActive ? PushTokenStatus.ACTIVE : PushTokenStatus.INITIAL;
		}
	}


	/**
	 * @return the invalidToken
	 */
	public String getInvalidToken() {
		return invalidToken;
	}
	
	/**
	 * @param invalidToken the invalidToken to set
	 */
	public void setInvalidToken(String invalidToken) {
		this.invalidToken = invalidToken;
	}
	
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	
	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}
	
	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	/**
	 * @return the deviceModel
	 */
	public String getDeviceModel() {
		return deviceModel;
	}
	
	/**
	 * @param deviceModel the deviceModel to set
	 */
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = StringUtils.left(deviceModel, PushTokenConstants.DEVICE_MODEL_LENGTH);
	}
	
	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}
	
	/**
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = StringUtils.left(deviceName, PushTokenConstants.DEVICE_NAME_LENGTH);
	}
	
	/**
	 * @return the hasPermissions
	 */
	public Boolean hasPermissions() {
		return hasPermissions;
	}
	
	/**
	 * @param hasPermissions the hasPermissions to set
	 */
	public void setHasPermissions(Boolean hasPermissions) {
		this.hasPermissions = hasPermissions;
	}
	
	/**
	 * @return the status
	 */
	public PushTokenStatus getStatus() {
		return status;
	}
	
	/**
	 * @param status the status to set
	 */
	public void setStatus(PushTokenStatus status) {
		this.status = status;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	

	
	
	

}
