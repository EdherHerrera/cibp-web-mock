package evertec.cibp.web.request;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.web.session.UserSession;

/**
 * This class provides a wrapper for the request of accounts service
 */
public class PendingApplicationRequest {
	
	private UserSession userSession;
	private CustomerProfile customerProfile;
	private boolean validateEloanApplications;
	private String userName;
	
	/**
	 * Default constructor
	 */
	public PendingApplicationRequest() {
		
	}

	
	/**
	 * @return The user session
	 */
	public UserSession getUserSession() {
		return userSession;
	}

	
	/**
	 * @param userSession The user session
	 */
	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}

	
	/**
	 * @return The customer profile
	 */
	public CustomerProfile getCustomerProfile() {
		return customerProfile;
	}

	
	/**
	 * @param customerProfile The customer profile
	 */
	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}

	
	/**
	 * @return The validate Eloan applications
	 */
	public boolean isValidateEloanApplications() {
		return validateEloanApplications;
	}

	/**
	 * @param validateEloanApplications The validate Eloan applications
	 */
	public void setValidateEloanApplications(boolean validateEloanApplications) {
		this.validateEloanApplications = validateEloanApplications;
	}

	/**
	 * @return The user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName The user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
	
	

}
