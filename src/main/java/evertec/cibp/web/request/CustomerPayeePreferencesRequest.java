package evertec.cibp.web.request;

import java.io.Serializable;

/**
 * Copyright 2019 Evertec, Inc. All Rights Reserved.
 * <p>
 * CustomerPayeePreferencesRequest class.
 * <p>
 *
 * @author Henry Terrero
 * @version 1.0
 * @since 1.0
 */
public class CustomerPayeePreferencesRequest implements Serializable {
	
	  /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5019437617687227948L;
    
    /**
     * The customer payee preference id
     */
	private String customerPayeePreferenceId;
	
	/**
     * The  profile id
     */
	private String profileId;
	
	/**
     * The payee key
     */
	private String payeeKey;
	
	/**
     * The attribute data
     */
	private String attributeData;
	
	

	/**
     * The constructor default
     */
	public CustomerPayeePreferencesRequest() {
		super();
	}
	
	
	/**
     * The constructor using fields
     */
	  public CustomerPayeePreferencesRequest(String customerPayeePreferenceId, String profileId, String payeeKey,
			String attributeData) {

		this.customerPayeePreferenceId = customerPayeePreferenceId;
		this.profileId = profileId;
		this.payeeKey = payeeKey;
		this.attributeData = attributeData;
	}



	/**
     * Gets the customer payee preference id.
     *
     * @return the customerPayeePreferenceId
     */
	public String getCustomerPayeePreferenceId() {
		return customerPayeePreferenceId;
	}

	/**
     * Sets the customer payee preference id.
     *
     * @param customerPayeePreferenceId the customerPayeePreferenceId to set
     */
	public void setCustomerPayeePreferenceId(String customerPayeePreferenceId) {
		this.customerPayeePreferenceId = customerPayeePreferenceId;
	}

	  /**
     * Gets the profile id.
     *
     * @return the profileId
     */
	public String getProfileId() {
		return profileId;
	}

	/**
     * Sets the profile id.
     *
     * @param profileId the profileId to set
     */
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	 /**
     * Gets the payee key.
     *
     * @return the payeeKey
     */
	public String getPayeeKey() {
		return payeeKey;
	}

	/**
     * Sets the payee key.
     *
     * @param payeeKey the payeeKey to set
     */
	public void setPayeeKey(String payeeKey) {
		this.payeeKey = payeeKey;
	}

	 /**
     * Gets the attribute data.
     *
     * @return the attributeData
     */
	public String getAttributeData() {
		return attributeData;
	}

	/**
     * Sets the attribute data.
     *
     * @param attributeData the attributeData to set
     */
	public void setAttributeData(String attributeData) {
		this.attributeData = attributeData;
	}
	
	

}
