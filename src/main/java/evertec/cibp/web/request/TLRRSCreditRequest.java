/**
 * 
 */
package evertec.cibp.web.request;

import evertec.cibp.web.enums.RecurringCashBackType;

//TODO this DTO is a copy of creditcards-service request
// make sure there are not fields that shouldn't be expose to cibp-web
// when copying request to this DTO
/**
 *  Request object for creditcards-service jar
 *  @see OtherClass
 *  @since 1.0
 *  @version 1.0
 *  @author Stephanie Diaz Ocasio
 */
public class TLRRSCreditRequest {

    /**
     * Rewards Value amount
     */
    private String rewardsValue; //

    /**
     * The item the customer has chosen to spend their rewards on
     */
    private String itemCode; //

    /**
     * Recurring CashBack Type
     * @see RecurringCashBackType
     */
    private RecurringCashBackType recurringCashBackType; //

    /**
     * @return current rewardsValue
     */
    public String getRewardsValue () {
        return rewardsValue;
    }

    /**
     * @param strRewardsValue rewardsValue to set
     */
    public void setRewardsValue (String strRewardsValue) {
        this.rewardsValue = strRewardsValue;
    }

    /**
     * @return current itemCode
     */
    public String getItemCode () {
        return itemCode;
    }

    /**
     * @param strItemCode itemCode to set
     */
    public void setItemCode (String strItemCode) {
        this.itemCode = strItemCode;
    }

    /**
     * @return current recurringCashBackType
     */
    public RecurringCashBackType getRecurringCashBackType () {
        return recurringCashBackType;
    }

    /**
     * @param recurringCashBackType recurringCashBackType to set
     */
    public void setRecurringCashBackType (RecurringCashBackType recurringCashBackType) {
        this.recurringCashBackType = recurringCashBackType;
    }
}