package evertec.cibp.web.request;


/**
 * This class provides a wrapper for the request of accounts service
 */
public class AccountApplicationRequest {
	
	private String username;

    private String accountNumber;

	public AccountApplicationRequest() {
		
	}
	
	

	public AccountApplicationRequest(String username) {
		super();
		this.username = username;
	}



	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	
    
    

}
