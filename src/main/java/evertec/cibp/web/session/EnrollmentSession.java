package evertec.cibp.web.session;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import evertec.cibp.core.models.accountopening.EaccountMessage;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.enrollment.EnrollableAccount;
import evertec.cibp.web.enums.UserAgentType;

public class EnrollmentSession implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4302804962944831849L;
	private String validatedTaxId, validatedAcct, validatedAcctType, validatedAcctProductId, validatedUsername;
	private Calendar submitedDob;
	private int counterBlock;
	private int counterFailed;
	private boolean blocked;
	private boolean failed;
	private String remoteToken;
	private List<EnrollableAccount> enrollExistingAccounts;
	private List<EnrollableAccount> accounts;

	private int dobCounter;
	private int actCounter;
	private int actInfCounter;
	private int zipCounter;
	private int expdateCounter;
	private boolean incompleteEnroll;
	private UserAgentType userAgentType;

	private CustomerInteractionEvent interactionEvent;
	
	private EaccountMessage eaccountMessage;

	public CustomerInteractionEvent getInteractionEvent() {
		return interactionEvent;
	}

	public void setInteractionEvent(CustomerInteractionEvent interactionEvent) {
		this.interactionEvent = interactionEvent;
	}

	public String getValidatedTaxId() {
		return validatedTaxId;
	}

	public void setValidatedTaxId(String validatedTaxId) {
		this.validatedTaxId = validatedTaxId;
	}

	public String getValidatedAcct() {
		return validatedAcct;
	}

	public void setValidatedAcct(String validatedAcct) {
		this.validatedAcct = validatedAcct;
	}

	public String getValidatedAcctType() {
		return validatedAcctType;
	}

	public void setValidatedAcctType(String validatedAcctType) {
		this.validatedAcctType = validatedAcctType;
	}

	public String getValidatedAcctProductId() {
		return validatedAcctProductId;
	}

	public void setValidatedAcctProductId(String validatedAcctProductId) {
		this.validatedAcctProductId = validatedAcctProductId;
	}

	public String getValidatedUsername() {
		return validatedUsername;
	}

	public void setValidatedUsername(String validatedUsername) {
		this.validatedUsername = validatedUsername;
	}

	public Calendar getSubmitedDob() {
		return submitedDob;
	}

	public void setSubmitedDob(Calendar submitedDob) {
		this.submitedDob = submitedDob;
	}

	public int getCounterBlock() {
		return counterBlock;
	}

	public void setCounterBlock(int counterBlock) {
		this.counterBlock = counterBlock;
	}

	public int getCounterFailed() {
		return counterFailed;
	}

	public void setCounterFailed(int counterFailed) {
		this.counterFailed = counterFailed;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public boolean isFailed() {
		return failed;
	}

	public void setFailed(boolean failed) {
		this.failed = failed;
	}

	public void addCounterBlock() {
		if (++counterBlock >= 3) {
			blocked = true;
		}
	}

	public void addCounterFailed() {
		if (++counterFailed >= 2) {
			failed = true;
		}
	}

	public int getDobCounter() {
		return dobCounter;
	}

	public void setDobCounter(int dobCounter) {
		this.dobCounter = dobCounter;
	}

	public void addDobCounter() {
		dobCounter++;
	}

	public int getActCounter() {
		return actCounter;
	}

	public void setActCounter(int actCounter) {
		this.actCounter = actCounter;
	}

	public void addActCounter() {
		actCounter++;
	}

	public int getActInfCounter() {
		return actInfCounter;
	}

	public void setActInfCounter(int actInfCounter) {
		this.actInfCounter = actInfCounter;
	}

	public void addActInfCounter() {
		actInfCounter++;
	}

	public int getZipCounter() {
		return zipCounter;
	}

	public void setZipCounter(int zipCounter) {
		this.zipCounter = zipCounter;
	}

	public void addZipCounter() {
		zipCounter++;
	}

	public int getExpdateCounter() {
		return expdateCounter;
	}

	public void setExpdateCounter(int expdateCounter) {
		this.expdateCounter = expdateCounter;
	}

	public void addExpdateCounter() {
		expdateCounter++;
	}

	public String getRemoteToken() {
		return remoteToken;
	}

	public void setRemoteToken(String remoteToken) {
		this.remoteToken = remoteToken;
	}

	public List<EnrollableAccount> getEnrollExistingAccounts() {
		return enrollExistingAccounts;
	}

	public void setEnrollExistingAccounts(List<EnrollableAccount> enrollExistingAccounts) {
		this.enrollExistingAccounts = enrollExistingAccounts;
	}

	public List<EnrollableAccount> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<EnrollableAccount> accounts) {
		this.accounts = accounts;
	}

	public boolean isIncompleteEnroll() {
		return incompleteEnroll;
	}

	public void setIncompleteEnroll(boolean incompleteEnroll) {
		this.incompleteEnroll = incompleteEnroll;
	}

	public UserAgentType getUserAgentType() {
		return userAgentType;
	}

	public void setUserAgentType(UserAgentType userAgentType) {
		this.userAgentType = userAgentType;
	}

	public EaccountMessage getEaccountMessage() {
		return eaccountMessage;
	}

	public void setEaccountMessage(EaccountMessage eaccountMessage) {
		this.eaccountMessage = eaccountMessage;
	}

}
