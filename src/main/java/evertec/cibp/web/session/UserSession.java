
package evertec.cibp.web.session;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import evertec.cibp.web.enums.CookieCategory;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;

import com.evertec.cibp.api.client.RestClient;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.MobileDevice;

import bppr.sms.beans.SMSAccount;
import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSEbppAccount;
import bppr.sms.beans.SMSProfile;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.AccountSubtypes;
import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import bppr.tv.beans.accounts.records.CreditCardAccountRecord;
import bppr.tv.beans.accounts.types.CardAccount;
import bppr.tv.beans.accounts.types.DepositAccount;
import evertec.cibp.core.enums.OnOffEnums;
import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.core.models.BinaryData.BinaryRecord;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontEndAccountCardPlastic;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TVFrontendCreditAccount;
import evertec.cibp.core.models.account.TVFrontendDepositAccount;
import evertec.cibp.core.models.accountopening.AccountApplication;
import evertec.cibp.core.models.accountopening.AccountApplications;
import evertec.cibp.core.models.accountopening.EaccountMessage;
import evertec.cibp.core.models.accountopening.dci.DCICustomer;
import evertec.cibp.core.models.accountopening.dci.DCICustomerAnswer;
import evertec.cibp.core.models.accountopening.dci.DCIQuestion;
import evertec.cibp.core.models.accountopening.dci.DCIQuestionAnswer;
import evertec.cibp.core.models.aggregation.AggregatedAccount;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.base.AbstractStatement;
import evertec.cibp.core.models.campaign.CampaignGenericBean;
import evertec.cibp.core.models.campaign.LoginCampaign;
import evertec.cibp.core.models.customer.AlertsHistory;
import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.core.models.customer.CustomerBalanceAlert;
import evertec.cibp.core.models.customer.CustomerEmailBalanceAlert;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerPassword;
import evertec.cibp.core.models.customer.CustomerPhone;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerViewedNotification;
import evertec.cibp.core.models.customer.CustomerWealthPreferred;
import evertec.cibp.core.models.customer.MassCompromiseFileAccounts;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.applications.ApplicationStatus;
import evertec.cibp.core.models.customer.applications.CustomerApplication;
import evertec.cibp.core.models.customer.devices.CustomerDevice;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.documents.CIBPConfigurableTexts;
import evertec.cibp.core.models.documents.CIBPConfigurableTextsTypes;
import evertec.cibp.core.models.documents.ConfigurableTextSubSections;
import evertec.cibp.core.models.documents.CustomerDocuments;
import evertec.cibp.core.models.documents.CustomerDocumentsPackage;
import evertec.cibp.core.models.ebill.EbppPreviousHistoryInfo;
import evertec.cibp.core.models.enrollment.EnrollableAccount;
import evertec.cibp.core.models.enrollment.UsernameReservation;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.entitlement.PayrollEntitlement;
import evertec.cibp.core.models.entitlement.RDCEntitlement;
import evertec.cibp.core.models.entitlement.RetirementPlanEntt;
import evertec.cibp.core.models.entitlement.SMSEntitlement;
import evertec.cibp.core.models.epayroll.PayrollStatus;
import evertec.cibp.core.models.onoff.OnOffAccounts;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.GlobalPayeePaymentType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVAccountFavoritePayment;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.pfm.PFMAccountWrapper;
import evertec.cibp.core.models.pfm.PFMCategoryWrapper;
import evertec.cibp.core.models.pfm.PFMChartCategoryRecordWrapper;
import evertec.cibp.core.models.pfm.PFMChartNetIncomeItemWrapper;
import evertec.cibp.core.models.pfm.PFMCustomRuleWrapper;
import evertec.cibp.core.models.pfm.PFMTagWrapper;
import evertec.cibp.core.models.pfm.PFMTransactionRecordWrapper;
import evertec.cibp.core.models.pfm.PFMTransactionWrapper;
import evertec.cibp.core.models.postinitialfunding.PIFAccountTypes;
import evertec.cibp.core.models.profile.ProfileTypeSection;
import evertec.cibp.core.models.remotedeposit.RemoteDepositDisplay;
import evertec.cibp.core.models.remotedeposit.RemoteDepositDisplayId;
import evertec.cibp.core.models.remotedeposit.RemoteDepositStatus;
import evertec.cibp.core.models.signup.SignupCampaign;
import evertec.cibp.core.models.sos.SosClaimsInfo;
import evertec.cibp.core.models.statements.AccountPDFStmtInfo;
import evertec.cibp.core.models.statements.AccountStmtInfo;
import evertec.cibp.core.models.statements.InterestAcctStmt;
import evertec.cibp.core.models.transfer.CustomerExternalTransferSource;
import evertec.cibp.core.models.transfer.ExternalTransferSourceStatus;
import evertec.cibp.core.models.transfer.FavoriteTransfer;
import evertec.cibp.core.models.transfer.TVAccountFavoriteTransfer;
import evertec.cibp.core.models.transfer.TVAccountTransfer;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.models.transfer.TVAccountTransferTarget;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.AccountsClasificationRequest;
import evertec.cibp.core.services.requests.CustomerEmailBalanceAlertAccountsGetterRequest;
import evertec.cibp.core.services.requests.GetSMSProfileRequest;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.requests.SmsAccountsGetterRequest;
import evertec.cibp.core.services.requests.opac.CISOpacCustomerInfoRequest;
import evertec.cibp.core.services.responses.AccountsClasificationResponse;
import evertec.cibp.core.services.responses.CustomerDeviceResponse;
import evertec.cibp.core.services.responses.CustomerPaymentInfoResponse;
import evertec.cibp.core.services.responses.CustomerTransferInfoResponse;
import evertec.cibp.core.services.responses.CustomerProfileVault;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.opac.CISOpacCustomerInfoResponse;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.DisposableIdentifier;
import evertec.cibp.core.utils.OnOffUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.mobilecash.core.models.MobileCashTransaction;
import evertec.cibp.mobilecash.core.services.MobileCashServices;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.athm.TransferInfo;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.AccountsFacade;
import evertec.cibp.web.facade.AlertsBalanceFacade;
import evertec.cibp.web.facade.AlertsFacade;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.facade.DocumentsFacade;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.facade.VaultAccountsFacade;
import evertec.cibp.web.form.bean.AlerthistoryBean;
import evertec.cibp.web.form.bean.CCPlasticsCustInfo;
import evertec.cibp.web.form.bean.UnicaApplication;
import evertec.cibp.web.form.dto.CustomerEmailBalanceAlertAccountsReqDto;
import evertec.cibp.web.models.customer.CustomerMessageContent;
import evertec.cibp.web.models.devices.DeviceInfo;
import evertec.cibp.web.models.vault.AccountStatement;
import evertec.cibp.web.premia.control.PremiaUtil;
import evertec.cibp.web.request.AlertsHistoryRequest;
import evertec.cibp.web.request.ModifyAccountPreferenceRequest;
import evertec.cibp.web.request.PendingApplicationRequest;
import evertec.cibp.web.request.VaultAccountsRequest;
import evertec.cibp.web.response.MobileCashAccountResponse;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.Utils;
import evertec.creditcards.model.CustomerIdentifiers;
import evertec.dp.service.tvstatement.CreditCardStatement_Type;
import evertec.dp.service.tvstatement.CurrentStatementsRs_Type;
import evertec.dp.service.tvstatement.CurrentStatements_Type;
import evertec.dp.service.tvstatement.DepositStatement_Type;
import evertec.onoff.models.PlasticOnOffModel;
import evertec.otherservices.beans.AccountSummary;
import evertec.otherservices.beans.UserAccount;
import evertec.otherservices.beans.athm.OsPhoneInfo;
import evertec.otherservices.beans.stmt.OSStmtPDFList;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.AthmTargetPhoneReq;
import evertec.otherservices.responses.AthmGetServiceChargeResp;
import evertec.otherservices.responses.AthmTargetPhoneResp;
import evertec.prophit.service.bean.AuthorizationBean;
import evertec.prophit.service.bean.AuthorizationResponse;
import evertec.prophit.service.bean.CustomerInquiryBean;
import evertec.prophit.service.ws.AuthorizationsWS.Authorizations;
import evertec.relius.retirementplan.ws.wsdl.RetirementPlanBalanceResponse;
import evertec.zap.service.bean.EmployeeInfoResponse;

@SuppressWarnings("unused")
public class UserSession implements Serializable {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(UserSession.class);
    public static final long serialVersionUID = 1L;

    private String vaultSessionKey;
    private UserSessionType userSessionType;
    private CustomerServices customerService;
    private MobileCashServices mobileCashService;
    private List<EnrollableAccount> enrollableAccounts;
    private DeviceRequest deviceRequest;
    private String rsaTransactionId;
    private String rsaSessionId;
    private Map<Object, Object> question;
    private String username;
    private List<ASK_FOR> tasksList;
    private int passwordSalt;
    private List<TVFrontendAccount> accounts = null;
    private List<MobileCashAccountResponse> mobileCashAccounts = null;
    private List<TVFrontendAccount> cardPlasticAccounts = null;
    private List<TVFrontendAccount> trfAccounts;
    private BankingSession bankingSession;
    private CustomerProfile customerProfile;
    private UsernameReservation usernameReservation;
    private CustomerTransferInfoResponse transferResponse;
    private List<TVAccountFavoriteTransfer> favoritesTransfers;
    private List<TVAccountTransfer> pendingTransfers;
    private AccountApplications accountApplications;
    private List<TVFrontendAccount> portalAccounts = null;
    private List<TVFrontendAccount> ccaAccounts = null;
    private List<TVFrontendAccount> secins = null;
    private List<TVFrontendAccount> programs = null;
    private List<TVFrontendAccount> guardianRepPayeeAccts = null;
    private List<TVAccountTransferSource> athmAccounts = null;
    private List<TVFrontendAccount> pendingCloseAccounts = null;
    
    private List<TVAccountFavoritePayment> favoritePayments;
    private List<TVAccountFavoritePayment> favoritePayrolls;
    private List<TVAccountFavoritePayment> favoriteVendors;
    private List<TVPayment> pendingPayments;
    private List<TVPayment> customerPayments;
    private ApplicationStatus statusPayrollApp;
    private String ePayrollBullet = null;
    private ApplicationStatus statusFraudPrevApp;
    private List<CustomerApplication> customerApplications;

    private List<TVFrontEndPayee> payees;
    private String remoteToken;
    private SMSProfile smsProfile;
    private String language;
    private List<CustomerExternalTransferSource> customerExternalTransferSources;
    private List<EbppPreviousHistoryInfo> ebppPreviousHistoryBillers;
    private List<SosClaimsInfo> sosClaims;

    // Interest PDF Statements
    private List<AccountPDFStmtInfo> intStmtLastStmts = null;
    private List<AccountPDFStmtInfo> intStmtPrevStmts = null;
    private List<AccountPDFStmtInfo> intStmtsNewAvailables = null;
    private List<AccountPDFStmtInfo> intStmts = null;
    private List<InterestAcctStmt> intStmtAccts = null;
    private Hashtable<String, AccountPDFStmtInfo> intPdfStmtHistInfoById = null;
    private Hashtable<String, List<AccountPDFStmtInfo>> intPrevPdfStmtHistInfoTbl = null;
    private Hashtable<String, String> acctPdfStmtHistInfoByAcct = null;
    private Hashtable<String, List<String>> ezStmtApplicationFilterTbl = null;

    // Account PDF Statements
    private List<AccountPDFStmtInfo> acctsStmtNewAvailable = null;
    private List<AccountPDFStmtInfo> acctLastPdfStmtHistInfo = null;
    private List<AccountPDFStmtInfo> acctPrevPdfStmtHistInfo = null;
    private Hashtable<String, AccountPDFStmtInfo> acctPdfStmtHistInfoById = null;
    private Hashtable<String, List<AccountPDFStmtInfo>> acctPrevPdfStmtHistInfoTbl = null;

    // private
    // evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent
    // customerInteractionEvent;
    private boolean loadebpp = false;
    private boolean loginCompleteSuccess = false;
    private List<AggregatedAccount> aggregatedAccounts;

    private boolean canOpenAdditionalAccounts = true;
    private Integer incompleteApplicationCount;
    private boolean canCompleteAdditionalAccounts = true;
    private boolean hasViAccount = false;
    private boolean hasBViAccount = false;
    private String country = null;

    private boolean comercialCustomer = false;

    private String statusmessage;
    private List<SMSAccount> smsaccounts;
    private List<SMSEbppAccount> smsebppaccounts;
    private boolean delay;
    private boolean loadedPaymentHistory = false;
    private boolean loadedfrequentPayees = false;
    private boolean loadedbasicebpp = false;
    private boolean fromvi;
    private String promotion;
    private HashMap<String, String> checkImageMap;
    private Map<Integer, GlobalPayee> commonGlobalpayees;
    private Map<String, String> commonGlobalPaymentTypeMap;
    private String commonGlobalPayeesTree;

    private boolean loadedDelinquencyPmtInfo = false;
    private BinaryRecord imageLogin;
    private List<LoginCampaign> logincampaigns;

    private Map<String, String> rtMap;

    // ATH Movil
    private OsPhoneInfo[] athmovilFriendsList;
    private HashMap<String, List<TransferInfo>> athmTransfersInfoHash = null;
    private HashMap<String, CardPlasticFrontend[]> cardPlastics;
    private boolean athmUnregistered = false;
    private AthmGetServiceChargeResp athmServiceCharge;
    private CustomerInquiryBean prophItCustomerInfo;// CIBP-1767 - used to make only one customer inquiry when using the service tab.
    private String cryptoKey = null;
    private String errorDetail;// Descripcion del error actualizado
    private String errorDetailCod;// Obtiene el codigo del error EBA de

    private boolean annualSummaryAccountsLoaded = false;

    private List<SignupCampaign> customerSignupCampaigns = null;

    private CustomerWealthPreferred custWealthPreferred = null;
    private boolean wealthPreferredLoaded = false;
    private EmployeeInfoResponse privateBankerInfo = null;
    private boolean privateBankerImgLoaded = false;

    private CustomerPaymentInfoResponse payeeres = null;

    private boolean displayTransactionDowntimeLoaded = false;
    private boolean displayTransactionDowntime = true;
    private String[] transactionDowntimeMessages;

    // PFM
    private String pfmId;
    private boolean inPfmUpdatedLookedUp = false;
    private boolean inPfmUpdate = false;
    private int pfmCategoryId;
    private int pfmSubcategoryId;
    private int pfmCategoryIdToHighlight;
    private int pfmTagId;
    private String pfmTransactionIdToEdit;
    private String pfmPaging;
    private String pfmCustomRulePaging;
    private boolean pfmSpanish = true;
    private boolean pfmCategoriesSortedSpanish = true;
    private boolean pfmTagsSortedSpanish = true;
    private int pfmCurrentRuleId = 0;
    private boolean pfmUpdateCurrentRule = false;
    private boolean pfmDeleteCurrentRule = false;
    private String pfmFirstTime;

    private List<PFMAccountWrapper> pfmAccounts;
    private List<PFMCategoryWrapper> pfmCategories;
    private List<PFMTagWrapper> pfmTags;
    private List<PFMCustomRuleWrapper> pfmCustomRules;
    private PFMChartCategoryRecordWrapper pfmOriginalChartInfo;
    private PFMChartCategoryRecordWrapper pfmPreviousChartInfo;
    private PFMChartCategoryRecordWrapper pfmChartInfo;
    private List<PFMChartNetIncomeItemWrapper> pfmChartNetIncomeInfo;

    private PFMTransactionRecordWrapper pfmTransactionRecord;
    private PFMTransactionRecordWrapper pfmTransactionSubcategoryRecord;
    private PFMTransactionWrapper pfmTransactionToUseAsCustomRule;
    private int pfmTransactionToUseAsCustomRuleOriginalSubcategory;

    // Keep track of whether this is a new user or not (for automatic PFM
    // entitlement creation)
    private boolean newUserForAutomaticPFMEntitlementCreation = false;
    
    private String accountMLA;

    /**
     * atributo
     * <code>guidedSetupSession del tipo GuidedSetupSession, mpicado cibp-2218, instancia de objeto
     *  para manejar las variables relacionadas con el guided setup campaign</code>
     */
    private GuidedSetupSession guidedSetupSession = new GuidedSetupSession();

    private boolean hasCCA = false;
    private boolean hasVisibleCCA = false;
    private boolean hasDDA = false;
    private boolean hasVisibleDDA = false;
    private boolean hasCD = false;
    private boolean hasAuto = false;
    private boolean hasPremia = false;
    private boolean hasVisiblePremia = false;
    private boolean hasMortgage = false;
    private boolean hasEbill = false;
    private boolean hasEstatment = false;
    private boolean hasEaccount = false;
    private boolean hasIRA = false;
    private boolean hasInsurance = false;
    private boolean hasPayee = false;
    private boolean hasLeasing = false;
    private boolean hasSecurities = false;
    private boolean hasPersonalLoans = false;

    private boolean pushMigrationSuccesful = false; // instance of push migration boolean variable
    
    // jocampo FFEIC
    private List<CustomerPassword> passwords;
    private String lastLoginDate;
    private String lastLoginIp;
    private boolean deviceChecked = false;
    private boolean skipRSA = false;
    // FIN FFEIC

    // cibp-2786
    // check once if the entitlement and the non-editable alerts exist
    private boolean alertsChecked = false;
    // fin cibp-2786

    private boolean informationChallenge = false;
    private boolean remember;
    private String pm_fp;
    private Map<Object, Object> informationQuestion;

    // EZStatement FIX
    // All the possible interest letter and mortgages product IDs from
    // EZStatement
    private String ezStatementInformativaProductIds[];
    private String ezStatementMortgageProductIds[];

    // CIBP-2700 Track if using mobile version to not load PFM nor campaigns
    private UserAgentType userAgentType;

    // cibp-2561
    private List<CCPlasticsCustInfo> plastics;

    // EBPP2
    private String ebpp2UserId;
    private boolean ebpp2UserIdLookedUp = false;
    private List<UserAccount> ebpp2UserAccounts = null;
    private List<UserAccount> ebpp2UserAccountsAndSummaries = null;
    private List<UserAccount> ebpp2UserAccountsAlgaroEbills = null;
    private List<UserAccount> ebpp2UserAccountsAlgaroEstatements = null;
    private HashMap<String, String> ebpp2BillerProducts;
    private boolean hasEAccounts = false;

    // CIBP-2621
    private Boolean canOpenDepositAccount = null;

    // CIBP-2761
    private HashMap<String, Authorizations> authorizations;

    // bgarcia CIBP-1104 ALERTAS DE BALANCE DE EMAIL
    private List<CustomerEmailBalanceAlert> emailaccounts;

    // ALERTAS DE BALANCE
    private List<CustomerBalanceAlert> balanceAlerts;

    private List<AlertsHistory> alertsHistory;
    private AlerthistoryBean alertHistoryBean;

    // Remote Deposit
    private RDCEntitlement fisInfo = null;
    private boolean fisEnrolled = false;
    private List<TVFrontendAccount> fisFilterAccounts = null;
    private Hashtable<String, Long> fisAccounts = null;

    // DCI
    private List<DCIQuestion> dciQuestions;
    private List<DCIQuestionAnswer> dciQuestionAnswers;
    private DCICustomer dciCustomer;
    private List<DCICustomerAnswer> dciCustomerAnswers;

    // Desembolso Asistido
    private List<CustomerDocumentsPackage> disbursementsList;
    private List<CustomerDocumentsPackage> disbursementsAfterAcceptance;
    private HashMap<String, CIBPConfigurableTexts> configurableTextMap;
    private List<CIBPConfigurableTexts> disbTextList;
    private boolean blocked = false;

    // Solicitud Unica
    private List<UnicaApplication> unicaApplications = null;
    private List<String> unicaAcceptedOffers;
    private HashMap<String, List<CIBPConfigurableTexts>> configurableTextListMap;

    private HashMap<String, Integer> pinRequestAttempts;

    // Esign
    private Boolean eSignLookUp = false;

    // Google analytics
    private Boolean portalAnalytics = false;

    // OOB
    private Boolean oobEnroll = false;

    private Boolean checkedOobEnroll = false;
    // OOB Navigation
    private List<ASK_FOR_OOB> tasksListOOB;

    private Boolean oobChallenge = false;

    /**constants oobRedirectToLogin */
    private boolean oobRedirectToLogin = false; //OOB redirect to login
    
    /**constants oobRedirectToBlock */
    private boolean oobRedirectToBlock = false; //OOB redirect to regain access

    // New ebills implementation
    private List<AccountPDFStmtInfo> accountsCurrentStatements = null;
    private String iraProductIds = "";
    private CampaignGenericBean campaignGenericBean = null;
    private Boolean renewPermId = true;
    private Boolean activateEAccounts = true;
    private List<TVFrontendDepositAccount> idaAccounts = null;
    /**constants cca Card Plastics */
    private List<TVFrontendAccount> ccaCardPlastics = null; // User CCA Card Plastics
    private CurrentStatementsRs_Type currentVaultStatements = null;	
    private List<CreditCardStatement_Type> ccaCurrentStatements = null;
    private List<DepositStatement_Type> ddaCurrentStatements = null;
    private CurrentStatementsRs_Type historyVaultStatements = null;
    private List<CreditCardStatement_Type> ccaHistoryStatements = null;
    private List<DepositStatement_Type> ddaHistoryStatements = null;

    private List<TVFrontendAccount> closedAccounts = null;
    private List<TVFrontendAccount> controlHAccounts = null;
    private List<TVFrontendAccount> ccaAvailbleToEbill = null;
    private List<AccountStmtInfo> ccaEbills = null;
    private List<AccountStmtInfo> ccaPortalEbills = null;
    private HashMap<String, AccountPDFStmtInfo> pdfStatementAccount = null;
    private List<AccountPDFStmtInfo> accountsStatements = null;
    private int maxStatements = 18;

    private String mobileCashTransaction;
    private List<MobileCashTransaction> mobileCashTransactions;
    private List<MobileCashTransaction> mobileCashHistory;

    private CustomerPhone customerPhone;

    private boolean newOfacCustomer = false;

    // OOB
    private boolean oobAuthenticated = false;

    //OUTREACH
    private String outReachConfirmation = "";

    // AddEmployeeOOB
    private boolean oobAddEmployeeAut = false;

    // OAO interrup message
    private EaccountMessage eaccountMessage = null;

    private boolean enrollAcctPendingClose = false;
    private Map<String, CardPlasticFrontend> tsysCCACustomerAlerts;

    private List<CustomerDevice> customerDevices;

    private Boolean interbankFundingAllowed = null;

    // MBSFE-454
    private Integer codeCounter = 0;// MBSFE-454

    // OUTREACH
    private boolean questionAnswered = false;
    private boolean elegibleToOutreach = false;
    private boolean alreadyShownOutreach = false;
    private boolean myInfoCallOutreach = false;

    // CashDrop SMS Retries
    private int smsGlobalRetries = 0;
    private int smsSendRetries = 0;
    private String smsGeneratedCode;
    
    /**constants otpLastChallengeDate */
    private Date otpLastChallengeDate;
    
    /**constants otpValidateRetries */
    private int otpValidateRetries = 0;
    
    /**constants otpSendRetries */
    private int otpSendRetries = 0;
    
    /** the one time pin challenge code */
    private String otpChallengeCode;
    
    private boolean validatedEasyOfac = false;
    private boolean easyOfacMatch = false;

    private String deviceId;
    private String fingerprintId;

    // ON/OFF
    private String[] allowedAthCardStatuses;
    private String[] allowedOnOffAthBins;
    private String[] excludeOnOffCcaBins;
    private OnOffAccounts onOffAccounts = null;
    private boolean hasOnOffEligibleAccount;

    private Boolean canPerformPostInitialFunding;

    //MBSE 1635
    private List<CustomerMessageContent> messagesList;
    private List<AccountApplication> completedApplications;
    private boolean showAlertText;
    private Boolean newNotifications = null;
    private CustomerViewedNotification viewedNotification;

    /**Flag to know if oob Challenge is used in the login  */
    private boolean oobLogin = false;
    
    //Begin MBCA
    
    /**
     * Set containing enrollable non transactional accounts.
     * Enrollment Account Types filter must be available to be modified without a Release.
     */
    private Set<String> nonTransEnrollAccsSet;
    
    //End MBCA

    /**MBFIS-515 Retirement Plan Accounts  */
    
    private RetirementPlanBalanceResponse retirementPlan = null; //RetirementPlanBalanceResponse   

    /**MBFIS-752 updateLoginCount */
    private boolean updateLoginCount = true; //update or not the loginCount in Entitlement

    //end MBFIS-515

	/**Customer has or not Retirement Plan */
    private boolean hasRetirementPlan = false; //retirement plan flag

   
    //end MBFIS-515

   
    private Boolean pendingResetPayerees; //MBSFE-703
    
 
    /**
     *  Microservice Architecture 
     */

    /** Client configured to consume only authentication resources */
    private RestClient restClientAuthentication = new RestClient();
    /** Client configured to consume no authentication resources, 
     * requeries customer user and password to be instantiated*/
    private RestClient restClientResourceOwner = null;

    private Boolean logEbppStatus = false;

    /**
     * MBSFE-665
     */
    private List<MassCompromiseFileAccounts> massCompromiseFileAccounts;

    //Section Management
    private Map<Integer, String> sectionsPath;
    private Map<Integer, ProfileTypeSection> profileTypeSections;

    //MBSFE-1341
    private HashMap<String, List<List<AccountStatement>>> accountsChargesStatements;

    private Map<String, String> paymentFrequencyMap = null;
    private String epayrollId;
    private String evendorId;

    //MBDP-2226
    private Boolean foreignCustomer = false;

    private DeviceInfo deviceInfo;
    
    /**MBCA-1311 Remove Account Statement Option */
    private Boolean removeAccountStatement = false; // Flag MBCA-1311

    /**MBFM-99 cookie preferences for each profile */
    private List<CookiePreferenceStatus> cookiePreferenceStatusList;
    
    /**Param in url */
    private boolean rewardPoints;
    
    /** List Reward Points */
    private List<TVFrontendAccount> listRewardPoints;
    
    /** Consult rewards points */
    private boolean consultRewardsPoints;
    
    /**nonPremiaState */
    private Boolean nonPremiaState;
    
    /**MBCA-1963 Save Ghost Account*/
    private List<TVFrontendAccount> premiaAccounts = null;
    
    /**MBCI-688 Premia Accounts*/
    private List<TVFrontendAccount> premiaAccountList = null;

    /** MBSE retPlanCallback */
    private boolean retPlanCallback = false;
    
    /**MBFC interruptionPage*/
    private CustomerProfileVault customerProfileVault = new CustomerProfileVault();
    ///** has Interruption Active */
    private boolean isLoginInterruptionEnabled = false;

    private HashMap<String, CustomerIdentifiers> ccaCustomerIdentifierMap;

    public boolean getRetPlanCallback() {
		return retPlanCallback;
	}

	public void setRetPlanCallback(boolean retPlanCallback) {
		this.retPlanCallback = retPlanCallback;
	}

    /**
     * getSmsGlobalRetries
     * @return smsGlobalRetries
     */
    public int getSmsGlobalRetries() {
        return smsGlobalRetries;
    }

    public void setSmsGlobalRetries(int smsGlobalRetries) {
        this.smsGlobalRetries = smsGlobalRetries;
    }

    public int getSmsSendRetries() {
        return smsSendRetries;
    }

    public void setSmsSendRetries(int smsSendRetries) {
        this.smsSendRetries = smsSendRetries;
    }
    
    /**
     * getOtpValidateRetries
     * @return otpSendRetries
     */
    public int getOtpValidateRetries() {
        return otpValidateRetries;
    }

    /**
     * setOtpValidateRetries
     * @param otpValidateRetries int
     */
    public void setOtpValidateRetries(int otpValidateRetries) {
        this.otpValidateRetries = otpValidateRetries;
    }
    
    /**
     * getOtpSendRetries
     * @return otpSendRetries
     */
    public int getOtpSendRetries() {
        return otpSendRetries;
    }

    /**
     * setOtpSendRetries
     * @param otpSendRetries int
     */
    public void setOtpSendRetries(int otpSendRetries) {
        this.otpSendRetries = otpSendRetries;
    }

    private SMSCTN alertsPhoneNumber = null;

    public Map<String, CardPlasticFrontend> getTsysCCACustomerAlerts() {
        return tsysCCACustomerAlerts;
    }

    public void updateTsysCCACustomerAlerts(String plastic) {
        if (tsysCCACustomerAlerts != null) {
            tsysCCACustomerAlerts.remove(plastic);
        }
    }

    public void setCustomerDevices(CustomerDevice device){
        if (customerDevices == null)
            customerDevices = new LinkedList<>();

        customerDevices.add(device);
    }

    public void updateCustomerDevices(CustomerDevice device){
        customerDevices.remove(device);
    }

    public void setTsysCCACustomerAlerts(CardPlasticFrontend plasticAlerts, String plastic) {
        if (tsysCCACustomerAlerts == null) {
            tsysCCACustomerAlerts = new HashMap<>();
            tsysCCACustomerAlerts.put(plastic, plasticAlerts);
        } else {
            if (plasticAlerts != null && !GenericValidator.isBlankOrNull(plastic)) {
                tsysCCACustomerAlerts.put(plastic, plasticAlerts);
            }
        }
    }

    public CardPlasticFrontend[] getCardPlastics(TVFrontendAccount account) {
        AccountMaskUtil maskutil = new AccountMaskUtil();
        try {
            if (cardPlastics != null && cardPlastics.containsKey(account.getFrontEndId())) {
                return cardPlastics.get(account.getFrontEndId());
            } else if (cardPlastics != null && !cardPlastics.containsKey(account.getFrontEndId())) {
                return this.getPlasticsAccountRecords(account);
            } else {
                if (cardPlastics == null)
                    cardPlastics = new HashMap<>();

                return this.getPlasticsAccountRecords(account);
            }
        } catch (RuntimeException e) {
            logger.error("Could not get card plastics for account: " + maskutil.mask(account.getAccountNumber()), e);
            return new CardPlasticFrontend[0];
        }
    }

    //MBSFE-624 recover full detailed ATH card
    public CardPlasticAccountRecord getCardPlastic(String accountFrontEndId, String cardFrontEndId) {
        try {
            if(this.cardPlastics != null && this.cardPlastics.containsKey(accountFrontEndId)){
                CardPlasticFrontend[] plastics = this.cardPlastics.get(accountFrontEndId);
                CardPlasticFrontend plastic = null;
                for(int i = 0; i < plastics.length; i++){
                    if(plastics[i].getFrontEndId().equals(cardFrontEndId)){
                        plastic = plastics[i];
                        break;
                    }
                }
                if(plastic != null){
                    AbstractAccount accountDetails = null;
                    DepositAccount depositAccount = null;
                    for (int j = 0; j < this.cardPlasticAccounts.size(); j++) {
                        TVFrontendAccount plasticAccount = this.cardPlasticAccounts.get(j);
                        // PLASTIC BELONGS TO THE USER
                        CardPlasticAccountRecord accRecord = plastic.getCardPlasticAccountRecord();
                        if (accRecord != null && accRecord.getCardPANId().equals(plasticAccount.getAccountNumber())) {
                            accountDetails = this.getCustomerService().getCardPlasticAccount(
                                    this.getCustomerProfile(), this.getBankingSession(), plasticAccount, true);
                            if (accountDetails != null && accountDetails.getBackendObject() != null) {
                                depositAccount = (DepositAccount) accountDetails.getBackendObject();
                                if (depositAccount.getCardPlasticAccountRecords() != null
                                        && depositAccount.getCardPlasticAccountRecords().length != 0) {
                                    return depositAccount.getCardPlasticAccountRecords()[0];
                                }
                            }
                        }
                    }
                }
                return null;
            }else{
                return null;
            }
        } catch (RuntimeException e) {
            logger.error("Could not get card plastics", e);
            return null;
        }
    }

    /**
     * MBFIS-638
     * Method that validates if customer has retirement plan and should see or not a new feature budge in the accounts list
     * @return retirementPlanBudge
     */
    public Boolean hasRetirementPlanBudge() {      	
    	        
         boolean retirementPlanBudge = true; // show or hide new alert in retirement plan account
         
    	 final CustomerProfile profile = getCustomerProfile(); //CustomerProfile info
    	
            if (profile != null ) {               
  
                        CustomerEntitlement ent = getCustomerProfile().getEntitlementByType(EntitlementType.RETIREMENT_PLAN); //Customer entitlement
                       
                            if (ent != null) {
                            	int count = 0; //login count
                            			
                            	count = ent.getInt(RetirementPlanEntt.LOGIN_COUNT); 
                            	if (count >= 3 && updateLoginCount) { //if count highter than 3 and it's first login -> hide badge 
                            		updateLoginCount = false;
                            	}
                            	
                            	if (count >= 3) {
                                    retirementPlanBudge = false;
                                }
                            	
                            	if(updateLoginCount) {
                            		count++;
                            		ent.putInt(RetirementPlanEntt.LOGIN_COUNT, count);
                            		getCustomerService().updateEntitlement(profile, ent);
                            		updateLoginCount = false;
                            	}
                            }
                
            }else{
            	retirementPlanBudge=false;
            }
    	
        
        return retirementPlanBudge;
    }
    
    private CardPlasticFrontend[] getPlasticsAccountRecords(TVFrontendAccount account) {
        List<CardPlasticFrontend> tempActives = new LinkedList<>();
        DisposableIdentifier id = new DisposableIdentifier();

        CardPlasticFrontend[] cardPlasticsArrayResponse = customerService.getPlasticAccountRecords(customerProfile, bankingSession,
                account);

        if (cardPlasticsArrayResponse != null && cardPlasticsArrayResponse.length > 0) {
            for (int i = 0; i < cardPlasticsArrayResponse.length; i++) {
                CardPlasticFrontend cardPlasticFrontend = new CardPlasticFrontend(
                        cardPlasticsArrayResponse[i].getCardPlasticAccountRecord());
                cardPlasticFrontend.setFrontEndId(id.getNextDisposableId());

                String status = cardPlasticFrontend.getCardPlasticAccountRecord().getStatus();
                if (GenericValidator.isBlankOrNull(status)) {
                    continue;
                }

                status = status.trim();
                if (status.startsWith("O") || status.startsWith("1") || status.startsWith("2") || status.contains("ACTIVE"))
                    tempActives.add(cardPlasticFrontend);
            }

            if (!tempActives.isEmpty()) {
                CardPlasticFrontend[] cardPlasticsArray = tempActives.toArray(new CardPlasticFrontend[0]);
                cardPlastics.put(account.getFrontEndId(), cardPlasticsArray);
                return cardPlasticsArray;
            } else {
                cardPlastics.put(null, null);
                return new CardPlasticFrontend[0];
            }

        } else {
            cardPlastics.put(account.getFrontEndId(), null);
            return new CardPlasticFrontend[0];
        }
    }

    public OsPhoneInfo[] getAthmContacts(String fiID, String fiToken, String userId) {
        if (athmovilFriendsList == null) {
            AthmTargetPhoneReq phoneListReq = new AthmTargetPhoneReq();
            phoneListReq.setFiID(fiID);
            phoneListReq.setFiToken(fiToken);
            phoneListReq.setUserID(userId);
            AthmTargetPhoneResp phoneListResp = customerService.getCustomerTargetPhones(phoneListReq);
            if (phoneListResp != null)
                athmovilFriendsList = phoneListResp.getTargetPhones();

            this.setAthmUnregistered(false);
            if (phoneListResp != null && phoneListResp.getResponseStatus() != null
                    && phoneListResp.getResponseStatus().equals(ResponseStatus.SUCCESS)
                    && "05".equals(phoneListResp.getStatusCode())) // unregistered
            {
                // remove entitlement to enforce enrollment
                customerService.deleteAthmHandshake(this.getCustomerProfile());
                logger.error("User is unregistered in ATHM. Enrollment will be enforced.");
                this.setAthmUnregistered(true);
            }
        }

        return athmovilFriendsList;
    }

    public void resetAthMovilContacts() {
        athmovilFriendsList = null;
    }

    public Map<Object, Object> getInformationQuestion() {
        return informationQuestion;
    }

    public void setInformationQuestion(Map<Object, Object> informationQuestion) {
        this.informationQuestion = informationQuestion;
    }

    public boolean isDelay() {
        return delay;
    }

    public void setDelay(boolean delay) {
        this.delay = delay;
    }

    public AlerthistoryBean getAlertHistoryBean() {
        return alertHistoryBean;
    }

    public void setAlertHistoryBean(AlerthistoryBean alertHistoryBean) {
        this.alertHistoryBean = alertHistoryBean;
    }

    public void resetAlertHistoryBean() {
        alertHistoryBean = null;
    }

    public List<AlertsHistory> getAlertsHistoryBySSN(String taxId, String alertsHistoryDays) {
        if (alertsHistory == null || alertsHistory.size() == 0) {
            AlertsHistoryRequest alertsHistoryRequest = new AlertsHistoryRequest();
            alertsHistoryRequest.setUserSession(this);
            alertsHistoryRequest.setTaxId(taxId);

            alertsHistory =  AlertsFacade.getInstance().getAlertsHistory(alertsHistoryRequest, alertsHistoryDays);

        }

        return alertsHistory;
    }

    public List<AlertsHistory> getAlertsHistoryByProfileId(String profileId, String alertsHistoryDays) {
        if (alertsHistory == null || alertsHistory.size() == 0) {
            AlertsHistoryRequest alertsHistoryRequest = new AlertsHistoryRequest();
            alertsHistoryRequest.setUserSession(this);
            alertsHistoryRequest.setProfileId(profileId);

            alertsHistory =  AlertsFacade.getInstance().getAlertsHistory(alertsHistoryRequest, alertsHistoryDays);

        }
        return alertsHistory;
    }

    public List<AlertsHistory> getAlertsHistory() {
        return alertsHistory;
    }

    public void setAlertsHistory(List<AlertsHistory> alertsHistory) {
        this.alertsHistory = alertsHistory;
    }

    public void resetAlertsHistory() {
        alertsHistory = null;
    }

    // bgarcia CIBP-1104 ALERTAS DE BALANCE DE EMAIL
    public List<CustomerEmailBalanceAlert> getEmailaccounts(CustomerInteractionEvent customerInteractionEvent) {
        if (emailaccounts == null || emailaccounts.size() == 0) {
            if (customerProfile.hasEntitlement(EntitlementType.ALERTS)) {
                AlertsEntitlement emialent = new AlertsEntitlement(
                        customerProfile.getEntitlementByType(EntitlementType.ALERTS));
                emailaccounts = new LinkedList<CustomerEmailBalanceAlert>();
                CustomerEmailBalanceAlertAccountsGetterRequest emailreq = new CustomerEmailBalanceAlertAccountsGetterRequest(
                        customerInteractionEvent, customerProfile, emialent, getAccounts());

                CustomerEmailBalanceAlertAccountsReqDto request = new CustomerEmailBalanceAlertAccountsReqDto
                        (customerService, this, customerProfile.getProfileId(), accounts, emailreq);

                emailaccounts = AlertsFacade.getInstance().getCustomerEmailBalanceAlertAccounts(request);


            }
        }
        return emailaccounts;
    }

    // CIBP-1857 ISSUE BANCA MOBILE BGARCIA SE TOMA EN CUENTA Q UE UNA CUENTA
    // PUEDE TENER MAS DE UNA ALERTA
    public List<SMSAccount> getSmsaccounts(CustomerInteractionEvent customerInteractionEvent) {

        if (smsaccounts == null || smsaccounts.size() == 0) {
            if (customerProfile.hasEntitlement(EntitlementType.MOBILE_BANKING)) {

                SMSEntitlement smsent = new SMSEntitlement(
                        customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING));
                smsaccounts = new LinkedList<SMSAccount>();
                SmsAccountsGetterRequest smsreq = new SmsAccountsGetterRequest(customerInteractionEvent,
                        customerProfile, smsent, getSmsProfile(customerInteractionEvent), getAccounts());
                customerService.getSMSAccounts(smsreq);
                for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
                    TVFrontendAccount act = (TVFrontendAccount) iterator.next();
                    if (act.getSmsAccount() != null && act.getSmsAccount().size() > 0) {
                        for (Iterator<SMSAccount> iterator1 = act.getSmsAccount().iterator(); iterator1.hasNext();)
                            smsaccounts.add((SMSAccount) iterator1.next());
                    }
                }

            }
        }

        return smsaccounts;
    }

    public void setSmsaccounts(List<SMSAccount> smsaccounts) {
        this.smsaccounts = smsaccounts;
    }

    // bgarcia CIBP-1104 ALERTAS DE BALANCE DE EMAIL
    public List<CustomerBalanceAlert> getBalanceAlerts() {
        if (balanceAlerts == null || balanceAlerts.isEmpty()) {
            balanceAlerts = AlertsBalanceFacade.getInstance().getCustomerBalanceAlert
                    (this, this.getCustomerProfile().getProfileId());

        }
        return balanceAlerts;
    }

    public void setBalanceAlerts(List<CustomerBalanceAlert> balanceAlerts) {
        this.balanceAlerts = balanceAlerts;
    }

    /********* S679931 *****/
    public List<SMSEbppAccount> getSmsEbppaccounts(CustomerInteractionEvent customerInteractionEvent) {
        if (smsebppaccounts == null || smsebppaccounts.size() == 0) {
            if (customerProfile.hasEntitlement(EntitlementType.MOBILE_BANKING)) {

                SMSEntitlement smsent = new SMSEntitlement(
                        customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING));
                smsebppaccounts = new LinkedList<SMSEbppAccount>();
                SmsAccountsGetterRequest smsreq = new SmsAccountsGetterRequest(customerInteractionEvent,
                        customerProfile, smsent, getSmsProfile(customerInteractionEvent), getAccounts());
                customerService.getSMSEbppAccounts(smsreq);
                for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
                    TVFrontendAccount act = (TVFrontendAccount) iterator.next();
                    if (act.getSmsEbppAccount() != null && act.getSmsEbppAccount().size() > 0) {
                        for (Iterator<SMSEbppAccount> iterator1 = act.getSmsEbppAccount().iterator(); iterator1
                                .hasNext();)
                            smsebppaccounts.add((SMSEbppAccount) iterator1.next());
                    }
                }

            }
        }

        return smsebppaccounts;
    }

    public void setSmsebppaccounts(List<SMSEbppAccount> smsebppaccounts) {
        this.smsebppaccounts = smsebppaccounts;
    }

    public UserSession() {
        super();
    }

    public UserSession(CustomerServices customerService, CustomerProfile customerProfile, List<ASK_FOR> tasksList,
            List<ASK_FOR_OOB> tasksListOOB) {
        this.customerService = customerService;
        this.customerProfile = customerProfile;
        this.tasksList = tasksList;
        this.tasksListOOB = tasksListOOB;

    }

    public UserSession(CustomerServices customerService, CustomerProfile customerProfile, List<ASK_FOR> tasksList) {
        this.customerService = customerService;
        this.customerProfile = customerProfile;
        this.tasksList = tasksList;

    }

    public CustomerServices getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerServices customerService) {
        this.customerService = customerService;
    }

    public List<TVFrontendAccount> getCardPlasticAccounts() {
        if (cardPlasticAccounts == null)
            cardPlasticAccounts = new LinkedList<>();

        return cardPlasticAccounts;
    }

    public List<MobileCashAccountResponse> getMobileCashAccounts() {
        if (this.mobileCashAccounts == null) {
            this.mobileCashAccounts = new LinkedList<>();
            for (TVFrontendAccount account : this.getAccounts()) {
                // FILTER ACCOUNTS WITH ATH CARDS
                if (account != null && account.getSubtype() != null && account.getSubtype().equalsIgnoreCase("IDA")
                        && account.getAccountSection() != null && !account.getAccountSection().equalsIgnoreCase("R")) {
                    // AT LEAST ONE ATH VALID
                    AbstractAccount accountDetails = null;
                    DepositAccount depositAccount = null;
                    CardPlasticAccountRecord[] cardPlasticAccountRecordList = null;
                    List<CardPlasticAccountRecord> plastics = new ArrayList<>();
                    CardPlasticFrontend[] plasticsAccount = this.getCardPlastics(account);
                    if (plasticsAccount != null) {
                        for (int i = 0; i < plasticsAccount.length; i++) {
                            for (int j = 0; j < this.cardPlasticAccounts.size(); j++) {
                                TVFrontendAccount plasticAccount = this.cardPlasticAccounts.get(j);
                                // PLASTIC BELONGS TO THE USER
                                CardPlasticAccountRecord accRecord = plasticsAccount[i].getCardPlasticAccountRecord();
                                if (accRecord != null && accRecord.getCardPANId().equals(plasticAccount.getAccountNumber())) {
                                    accountDetails = this.getCustomerService().getCardPlasticAccount(
                                            this.getCustomerProfile(), this.getBankingSession(), plasticAccount, true);
                                    if (accountDetails != null && accountDetails.getBackendObject() != null) {
                                        depositAccount = (DepositAccount) accountDetails.getBackendObject();
                                        cardPlasticAccountRecordList = depositAccount.getCardPlasticAccountRecords();
                                        if (cardPlasticAccountRecordList != null
                                                && cardPlasticAccountRecordList.length != 0) {
                                            plastics.add(cardPlasticAccountRecordList[0]);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        if (!plastics.isEmpty()) {
                            this.mobileCashAccounts.add(new MobileCashAccountResponse(account, plastics));
                        }
                    }
                }
            }
        }
        return this.mobileCashAccounts;
    }

    public void setMobileCashAccounts(List<MobileCashAccountResponse> mobileCashAccounts) {
        this.mobileCashAccounts = mobileCashAccounts;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<ASK_FOR> getTasksList() {
        return tasksList;
    }

    public void setTasksList(List<ASK_FOR> tasksList) {
        this.tasksList = tasksList;
    }

    public void setAccounts(List<TVFrontendAccount> accounts) {
        this.accounts = accounts;
    }

    @SuppressWarnings("unchecked")
    public List<SosClaimsInfo> getSosClaims(MessageSource messageSource, String sosResolvedConditions,
            String sosCallToActionStatus) {
        if (sosClaims == null) {

			boolean isDowntime = false;
			if (this.getCustomerService() != null
					&& this.getCustomerService().getGlobalEntitlement(EntitlementType.SOS) != null) {
				isDowntime = this.getCustomerService().getGlobalEntitlement(EntitlementType.SOS)
						.getStatus() != EntitlementStatus.ENABLED;
			}

            if (isDowntime) {
                sosClaims = new LinkedList<SosClaimsInfo>();
                return sosClaims;
            }

            sosClaims = customerService.getSosClaims(bankingSession.getPermId().toString());
            // Condition to add descriptions to be displayed
            if (sosClaims != null && sosClaims.size() > 0) {
                Date currentDate = new Date();
                List<SosClaimsInfo> inProgressClaims = new LinkedList<SosClaimsInfo>();
                List<SosClaimsInfo> resolvedClaims = new LinkedList<SosClaimsInfo>();
                boolean claimAdded = false;
                boolean claimResolved = false;
                String[] resolvedConditions = sosResolvedConditions.split(",");

                for (SosClaimsInfo claimsInfo : sosClaims) {
                    claimAdded = false;
                    claimResolved = false;

                    for (String resolvedCondition : resolvedConditions) {
                        if (claimsInfo.getClaimStatus().toUpperCase().contains(resolvedCondition)) {
                            claimResolved = true;
                            break;
                        }
                    }

                    if (claimResolved) {

                        long dateDiff = currentDate.getTime() - claimsInfo.getClaimResolvedDate().getTime();
                        long dateDiffInDays = TimeUnit.DAYS.convert(dateDiff, TimeUnit.MILLISECONDS);

                        if (dateDiffInDays >= 0 && dateDiffInDays <= 7) {
                            claimsInfo.setClaimStatusMessage("sos.claimStatus.resolved");
                            resolvedClaims.add(claimsInfo);
                            claimAdded = true;
                        }
                    } else // In progress claims
                    {
                        claimsInfo.setClaimStatusMessage("sos.claimStatus.inprogress");
                        if (Arrays.asList(sosCallToActionStatus.split(","))
                                .contains(claimsInfo.getClaimStatus().toUpperCase())) {
                            claimsInfo.setClaimStatusMessage("sos.claimStatus.inprogresscall");
                        }
                        inProgressClaims.add(claimsInfo);
                        claimAdded = true;
                    }

                    if (claimAdded) {
                        // Set claim type to show to user (creating
                        // text.properties
                        // variable dynamically)
                        String claimArea = claimsInfo.getClaimArea().trim().toUpperCase();
                        String claimMessageVar = "sos.claimType." + claimArea.toLowerCase().trim();
                        if (claimArea.equals("DDA") || (claimArea.equals("CBR")
                                && claimsInfo.getClaimType().toUpperCase().contains("CALLBACK FROM CLAIMS"))) {
                            claimMessageVar = claimMessageVar + "."
                                    + claimsInfo.getClaimType().replace(" ", "").toLowerCase();
                        } else if (claimArea.equals("CBR")// solution to defect
                                // 1431
                                && claimsInfo.getClaimSubType().toUpperCase().contains("CALL CUSTOMER")) {
                            claimMessageVar = claimMessageVar + "."
                                    + claimsInfo.getClaimSubType().replace(" ", "").toLowerCase();
                        }

                        // To verify if the dinamically generated description
                        // variable is defined in the property file
                        Locale locale = "es".equalsIgnoreCase(this.getLanguage()) ? new Locale("es") : Locale.ENGLISH;

                        claimsInfo.setClaimTypeMessage(claimMessageVar);
                        if (messageSource.getMessage(claimMessageVar, null, "", locale).equals(""))
                            claimsInfo.setClaimTypeMessage("sos.claimStatus.undefined");
                    }
                }
                sosClaims = ListUtils.union(inProgressClaims, resolvedClaims);

            } else {
                sosClaims = new LinkedList<SosClaimsInfo>();
            }

        }
        return sosClaims;
    }

    public List<TVFrontendAccount> getFisFilterAccounts() {
        if (fisFilterAccounts == null || fisFilterAccounts.size() == 0) {
            fisFilterAccounts = new LinkedList<TVFrontendAccount>();
            List<TVFrontendAccount> userAccounts = getAccounts();
            List<String> filterAccountStatus = customerService.getRemoteDepositFilterAccountStatus();			
            // Remote Deposit Accounts Filter
            for (Iterator<TVFrontendAccount> iterator = userAccounts.iterator(); iterator.hasNext();) {
                TVFrontendAccount name = (TVFrontendAccount) iterator.next();
                RemoteDepositDisplayId id = new RemoteDepositDisplayId();
                id.setBankId(name.getBankId());
                id.setProductId(name.getProductDefinition().getProductId());
                id.setSubTypeId(name.getSubtype());
                id.setSuffix(name.getAccountNumberSuffix());
                RemoteDepositDisplay fisAccountType = customerService.isRemoteDepositAvailable(id);
                boolean fisAccountAvailable = ((fisAccountType != null
                        && fisAccountType.getActivateRemoteDeposit() == RemoteDepositStatus.ENABLE) ? true : false);
                // TODO Validar si sera overwrite o enabled

                if (fisAccountAvailable &&
                        StaticMessageSource.isFlagEnabled(StaticMessageSource.FILTER_RDC_ACCOUNTS_MOBILE) &&
                        filterAccountStatus.contains(name.getProductDefinition().getProductStatus())) {
                    fisAccountAvailable = false;
                }

                if (fisAccountAvailable || (name.getCustomerAccountPreferences() != null
                        && name.getCustomerAccountPreferences().getRdcStatus() != null
                        && name.getCustomerAccountPreferences().getRdcStatus().equals("OVERWRITE"))) {

                    // Accounts facade implementation
                    ModifyAccountPreferenceRequest modifyAccountPreferenceRequest = new ModifyAccountPreferenceRequest();
                    modifyAccountPreferenceRequest.setAccount(name);
                    modifyAccountPreferenceRequest.setCustomerProfile(customerProfile);
                    modifyAccountPreferenceRequest.setHide(name.getHide());
                    modifyAccountPreferenceRequest.setNickname(name.getNickname());
                    modifyAccountPreferenceRequest.setUserSession(this);
                    AccountsFacade.getInstance().modifyAccountPreference(modifyAccountPreferenceRequest);

                    fisFilterAccounts.add(name);
                }
            }
        }
        return fisFilterAccounts;
    }

    public boolean updateAccountBalance(List<TVFrontendAccount> frontendAccountsToUpdate, String balanceUpdateTime) {
        try {
            boolean accountUpdated = false;
            boolean threadSleepExecuted = false;
            int updateTime = 7000;
            String regex = "[0-9]+";
            if (!GenericValidator.isBlankOrNull(balanceUpdateTime) && balanceUpdateTime.matches(regex)) {
                updateTime = Integer.valueOf(balanceUpdateTime);
            }

            for (TVFrontendAccount accountToUpdate : frontendAccountsToUpdate) {
                for (TVFrontendAccount account : accounts) {
                    if (account.getAccountNumber().equals(accountToUpdate.getAccountNumber())
                            && ((GenericValidator.isBlankOrNull(account.getAccountNumberSuffix())
                                    && GenericValidator.isBlankOrNull(accountToUpdate.getAccountNumberSuffix()))
                                    || (account.getAccountNumberSuffix()
                                            .equals(accountToUpdate.getAccountNumberSuffix())))
                            && ((GenericValidator.isBlankOrNull(account.getAccountProductId())
                                    && GenericValidator.isBlankOrNull(accountToUpdate.getAccountProductId()))
                                    || (account.getAccountProductId().equals(accountToUpdate.getAccountProductId())))
                            && ((GenericValidator.isBlankOrNull(account.getAccountSection())
                                    && GenericValidator.isBlankOrNull(accountToUpdate.getAccountSection()))
                                    || (account.getAccountSection().equals(accountToUpdate.getAccountSection())))) {
                        logger.debug("-- Found matching account");
                        logger.debug("-- Old object balance: " + account.getPortalBalance().doubleValue());

                        if (!threadSleepExecuted) {
                            Thread.sleep(updateTime);
                            threadSleepExecuted = true;
                        }

                        AbstractAccount accountDetails = getCustomerService().getAccountDetails(getCustomerProfile(),
                                getBankingSession(), account, true);

                        Account accountDetail = (Account) accountDetails.getBackendObject();
                        if (accountDetail.getAccountListBalance() != null) {
                            // Update balance in the same account object

                            ((Account) account.getBackendObject())
                            .setAccountListBalance(accountDetail.getAccountListBalance());
                            accountUpdated = true;
                            logger.debug("-- New object balance: " + account.getPortalBalance().doubleValue());
                        }
                        break;
                    }
                }
            }
            return accountUpdated;
        } catch (Exception e) {
            logger.error("Exception in updateAccountBalance (A resetAccounts will be made): " + e.getMessage());
            resetAccounts();
            this.getAccounts();
            return false;
        }
    }

    public void clasifyAccounts(List<TVFrontendAccount> userAccounts, boolean serviceAcctInq) {

        portalAccounts = new LinkedList<>();
        secins = new LinkedList<>();
        programs = new LinkedList<>();
        guardianRepPayeeAccts = new LinkedList<>();
        athmAccounts = new LinkedList<>();
        accounts = new LinkedList<>();
        pendingCloseAccounts = new LinkedList<>();
        ccaAccounts = new LinkedList<>();
        premiaAccountList = new LinkedList<>();

        List<PIFAccountTypes> allowed = customerService.getCreditableAccountTypes();


        // MBSFE-665
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE665)) {
            customerProfile.setMassCompromiseFileAccounts(getMassCompromiseFileAccounts());
        }

        // Ebills closed accounts
        if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
            AccountsClasificationRequest clasifyAccountsRequest = new AccountsClasificationRequest();
            clasifyAccountsRequest.setCampaignGenericBean(campaignGenericBean);
            clasifyAccountsRequest.setCustomerProfile(customerProfile);
            clasifyAccountsRequest.setCustomerService(customerService);
            clasifyAccountsRequest.setIraProductIds(iraProductIds);
            clasifyAccountsRequest.setServiceAcctInq(true);
            clasifyAccountsRequest.setUserAccounts(userAccounts);
            clasifyAccountsRequest.setAllowed(allowed);
            clasifyAccountsRequest.setUserInterface(customerProfile.getUserInterface());
            AccountsClasificationResponse resp = AccountUtils.clasifyAccounts(clasifyAccountsRequest);
            this.closedAccounts = resp.getClosedAccounts();
            this.controlHAccounts = resp.getControlHAccounts();
            accounts = resp.getAccounts();
            accounts.removeAll(closedAccounts);
            accounts.removeAll(controlHAccounts);
            cardPlasticAccounts = resp.getCardPlasticAccounts();
            accounts.removeAll(cardPlasticAccounts);
            pendingCloseAccounts = resp.getPendingCloseAccounts();
            accounts.removeAll(pendingCloseAccounts);
            portalAccounts.addAll(accounts);
            portalAccounts.removeAll(resp.getHiddenAccounts());
            secins = resp.getSecins();
            portalAccounts.removeAll(secins);
            programs = resp.getPrograms();
            portalAccounts.removeAll(programs);
            guardianRepPayeeAccts = resp.getGuardianRepPayeeAccts();
            athmAccounts = resp.getAthmAccounts();		
            hasViAccount = resp.isHasViAccount();
            hasBViAccount = resp.isHasBViAccount();
            comercialCustomer = resp.isComercialCustomer();
            hasEAccounts = resp.isHasEAccounts();			
            this.interbankFundingAllowed = resp.getInterbankFundingAllowed();			
            ccaAccounts = resp.getCcaAccounts();
            premiaAccountList = resp.getPremiaAccounts();

        }else{
            if (serviceAcctInq)
                cardPlasticAccounts = new LinkedList<>();

            if (userAccounts != null) {
                for (Iterator<TVFrontendAccount> iterator = userAccounts.iterator(); iterator.hasNext();) {
                    TVFrontendAccount name =  iterator.next();

                    if (serviceAcctInq && name.getSubtype().equals(AccountSubtypes.ATH_CARD_PLASTIC)) {
                        cardPlasticAccounts.add(name);
                    } else if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE213)
                            && name.getStatusCode().equals("N")) {
                        pendingCloseAccounts.add(name);
                    } else {
                        accounts.add(name);

                        if (!name.getHide() && name.getFeatures() != null && name.getFeatures().isPayFrom()
                                && AccountSubtypes.DEPOSIT.equals(name.getSubtype())) {
                            TVAccountTransferSource s = new TVAccountTransferSource(name);

                            s.setFrontEndId(name.getFrontEndId());

                            athmAccounts.add(s);
                        }

                        if (name.getIsGuardianOrRepPayeeAcc()) { // CIBP-2542
                            if (name.getCustomerAccountPreferences() == null
                                    || name.getCustomerAccountPreferences().getId() == null) {
                                String nickname = "Representative Payee Account";
                                if (name.isGuardianAcc())
                                    nickname = "Guardian Account";

                                CustomerProfile prof = this.getCustomerProfile();
                                if (this.getCustomerService() != null && prof != null)
                                    this.getCustomerService().modifyDefaultAccountPreference(prof, name, nickname);
                            }
                        }

                        if (!name.getHide()) {
                            if (name.getSubtype().equals("INS") || name.getSubtype().equals("SEC"))
                                secins.add(name);
                            else if (name.getSubtype().equals("PRM"))
                                programs.add(name);
                            else if (name.getIsGuardianOrRepPayeeAcc()) { // CIBP-2542
                                guardianRepPayeeAccts.add(name);
                            } else
                                portalAccounts.add(name);
                        }

                        if (!GenericValidator.isBlankOrNull(name.getSubtype()) && name.getSubtype().equals("CCA")) {

                            // MBSFE-665
                            if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE665)) {
                                Map<Object, Object> massCompromise = AccountUtils.setFlagsMassCompromiseAccounts(name,
                                        customerProfile.getMassCompromiseFileAccounts());
                                name.setMessageAlertMassCompromise((String) massCompromise.get("messageAlert"));
                                name.setMassCompromise((boolean) massCompromise.get("statusFlag"));
                            }
                            ccaAccounts.add(name);

                        }

                        if (name.getBankId().equals("002"))
                            hasViAccount = true;
                        if (name.getBankId().equals("004"))
                            hasBViAccount = true;

                        if (name.getAccountNumber()
                                .equalsIgnoreCase(customerProfile.getEnrollmentAccountNumber())
                                && (AccountUtils.isComercialPrimaryAccount(name.getTVAccount())
                                        || (customerProfile.getUserInterface() != null
                                        && customerProfile.getUserInterface() == UserInterface.COMMERCIAL)))
                            comercialCustomer = true;

                        if (name.getAccountProductId() != null && name.getAccountProductId().equals("081"))
                            hasEAccounts = true;

                        name.setAvailableElecStmt(customerService.isEZStatementAccount(name.getBankId(),
                                name.getProductDefinition().getProductId(), name.getSubtype(),
                                name.getAccountNumberSuffix())); // CIBP-2691
                    }

                    // MBSD-1750 Verify if there's an eligible account to add other bank accounts
                    // MLA products will be also defined in PIF CSR-tool option, so MBSD-792 will be handle as any other product  defined in DB
                    if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)
                            && this.interbankFundingAllowed == null) {
                        for (PIFAccountTypes productTypesAllowed : allowed) {
                            if (productTypesAllowed.getAddOption() != null
                                    && productTypesAllowed.getAddOption().equals(PIFAccountTypes.ADD_OPTION)
                                    && productTypesAllowed.getAccountSubtype() != null && name != null
                                    && name.getProductDefinition() != null
                                    && name.getProductDefinition().getProductId() != null
                                    && name.getProductDefinition().getProductType() != null
                                    && productTypesAllowed.getAccountId()
                                    .equalsIgnoreCase(name.getProductDefinition().getProductId())
                                    && productTypesAllowed.getAccountSubtype()
                                    .equalsIgnoreCase(name.getProductDefinition().getProductType())) {
                                this.interbankFundingAllowed = Boolean.TRUE;
                                break;
                            } else {
                                if (productTypesAllowed.getAddOption() != null
                                        && productTypesAllowed.getAddOption().equals(PIFAccountTypes.ADD_OPTION)
                                        && name != null && name.getProductDefinition() != null
                                        && name.getProductDefinition().getProductId() != null
                                        && productTypesAllowed.getAccountId()
                                        .equalsIgnoreCase(name.getProductDefinition().getProductId())) {
                                    this.interbankFundingAllowed = Boolean.TRUE;
                                    break;
                                }
                            }
                        }
                    }
                }				
            }
        }//End if ebills
        if (this.interbankFundingAllowed == null) {
            this.interbankFundingAllowed = Boolean.FALSE;
        }

        if ((StaticMessageSource.isFlagEnabled(StaticMessageSource.ATH_ONOFF) || StaticMessageSource.isFlagEnabled(StaticMessageSource.CCA_ONOFF))) {
            filterOnOffAccounts();
        }
    }

    public List<TVFrontendAccount> getAccounts() {

        if (accounts == null || accounts.isEmpty()) {
            delay = false;
            if (customerProfile != null && bankingSession != null) {
                VaultAccountsRequest vaultRequest = new VaultAccountsRequest();
                vaultRequest.setUserSession(this);
                return VaultAccountsFacade.getInstance().getAccounts(vaultRequest);
            } else {
                throw new ProgrammingError("UserSession cannot exist without a CustomerProfile");
            }
        } else {
            return accounts;
        }
    }
    
    /**
     * getAccounts
     * @return TVFrontendAccount
     */
    public List<TVFrontendAccount> getCcaCardPlastics() {
        if (ccaCardPlastics == null || ccaCardPlastics.isEmpty()) {
            delay = false;
            if (customerProfile != null && bankingSession != null) {
                VaultAccountsRequest vaultRequest = new VaultAccountsRequest();
                vaultRequest.setUserSession(this);
                return VaultAccountsFacade.getInstance().getCcaCardPlastics(vaultRequest);
            } else {
                throw new ProgrammingError("UserSession cannot exist without a CustomerProfile");
            }
        } else {
            return ccaCardPlastics;
        }
    }

    public List<TVFrontendAccount> updateGetAccounts() {
        List<TVFrontendAccount> userAccounts = null;

        boolean validateEloanApplications = customerProfile.isPendingAccountApplication();

        accountApplications = getAccountApplications(validateEloanApplications);
        if (accountApplications != null)
            userAccounts = customerService.getCustomerAccounts(customerProfile, accountApplications,
                    bankingSession, true);
        else
            userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, true);

        clasifyAccounts(userAccounts, true);
        return accounts;
    }
    
    /**
     * updateGetAccounts
     * @return TVFrontendAccount
     */
    public List<TVFrontendAccount> updateCcaCardPlastics() {     
    	ccaCardPlastics = customerService.getCardPlastics(customerProfile, bankingSession, true);
        return ccaCardPlastics;
    }

    public List<TVFrontendAccount> getPortalSecinsAccounts() {
        return secins;
    }

    public List<TVFrontendAccount> getPortalAccounts() {
        return portalAccounts;
    }

    public List<TVFrontendAccount> getPortalProgramAccounts() {
        return programs;
    }

    public List<TVFrontendAccount> getPortalGuardianAccounts() {
        return guardianRepPayeeAccts;
    }

    public BankingSession getBankingSession() {

        return bankingSession;
    }

    public CustomerProfile getCustomerProfile() {
        return customerProfile;
    }

    public void setCustomerProfile(CustomerProfile customerProfile) {
        if (customerProfile != null && this.customerProfile != null){
            customerProfile.setIfxError(this.customerProfile.isIfxError());
        }
        this.customerProfile = customerProfile;
    }

    public void resetAccounts() {
        accounts = null;
    }

    public CustomerTransferInfoResponse getTransferResponse() {
        return transferResponse;
    }

    public void setTransferResponse(CustomerTransferInfoResponse transferResponse) {
        this.transferResponse = transferResponse;
    }

    public Set<TVAccountTransferSource> getAccountsTransferFrom() {
        if (this.transferResponse == null)
            getCustomerTransferInfo();

        return this.transferResponse.getSources();
    }

    public void setAccountsTransferFrom(Set<TVAccountTransferSource> accountsTransferFrom) {
        this.transferResponse.setSources(accountsTransferFrom);
    }

    public Set<TVAccountTransferTarget> getAccountsTransferTo() {
        if (this.transferResponse == null)
            getCustomerTransferInfo();

        return this.transferResponse.getTargets();
    }

    public void setAccountsTransferTo(Set<TVAccountTransferTarget> accountsTransferTo) {
        this.transferResponse.setTargets(accountsTransferTo);
    }

    public List<TVAccountFavoriteTransfer> getFavoritesTransfers() {
        if (this.favoritesTransfers == null)
            getCustomerTransferInfo();

        return this.favoritesTransfers;
    }

    public void setFavoritesTransfers(List<TVAccountFavoriteTransfer> favoritesTransfers) {
        this.favoritesTransfers = favoritesTransfers;
    }

    public List<TVAccountTransfer> getPendingTransfers() {
        return pendingTransfers;
    }

    public void setPendingTransfers(List<TVAccountTransfer> pendingTransfers) {
        this.pendingTransfers = pendingTransfers;
    }

    public Set<TVAccountTransferSource> getAccountsWithFrontEndId() {
        List<TVFrontendAccount> accountsFrom = getAccounts();
        LinkedHashSet<TVAccountTransferSource> accountsPaymentFrom = new LinkedHashSet<>();

        for (Iterator<TVFrontendAccount> iterator = accountsFrom.iterator(); iterator.hasNext();) {
            TVFrontendAccount frontFrom = iterator.next();
            TVAccountTransferSource s = new TVAccountTransferSource(frontFrom);
            s.setFrontEndId(frontFrom.getFrontEndId());
            accountsPaymentFrom.add(s);
        }

        return accountsPaymentFrom;
    }

    public Set<TVAccountTransferSource> getAccountsPaymentFrom() {
        List<TVFrontendAccount> accountsFrom = getAccounts();
        LinkedHashSet<TVAccountTransferSource> accountsPaymentFrom = new LinkedHashSet<>();

        for (Iterator<TVFrontendAccount> iterator = accountsFrom.iterator(); iterator.hasNext();) {
            TVFrontendAccount frontFrom = (TVFrontendAccount) iterator.next();
            if (!frontFrom.getHide() && frontFrom.getFeatures().isPayFrom()) {
                TVAccountTransferSource s = new TVAccountTransferSource(frontFrom);
                s.setFrontEndId(frontFrom.getFrontEndId());
                accountsPaymentFrom.add(s);
            }
        }

        return accountsPaymentFrom;
    }

    public List<TVAccountFavoritePayment> getFavoritePayments() {
        if (!loadedfrequentPayees
                | (favoritePayments == null | (favoritePayments != null && favoritePayments.size() < 1))) {
            getQuickPayees(true);
            this.loadedfrequentPayees = true;
        }

        return favoritePayments;
    }

    public List<TVAccountFavoritePayment> getVendorPayments(){
        if(favoriteVendors == null){
            this.resetPayees();
            getQuickPayees(true);
        }
        return favoriteVendors;
    }

    public List<TVAccountFavoritePayment> getPayrollsPayments(){
        if(favoritePayrolls == null){
            this.resetPayees();
            getQuickPayees(true);
        }
        return favoritePayrolls;
    }

    public List<TVAccountFavoritePayment> getFavoritePayrolls(String telenominaId) {

        getQuickPayees(true);
        this.loadedfrequentPayees = true;
        favoritePayrolls = new ArrayList<>();

        if (favoritePayments != null) {
            for (int i = 0; i < favoritePayments.size(); i++) {
                if (favoritePayments.get(i).getPayee().getGlobalPayee() != null
                        && favoritePayments.get(i).getPayee().getGlobalPayee().getIdString() != null) {
                    if (favoritePayments.get(i).getPayee().getGlobalPayee().getIdString()
                            .equalsIgnoreCase(telenominaId)) {
                        TVAccountFavoritePayment tempPayment = favoritePayments.get(i);
                        favoritePayrolls.add(tempPayment);
                    }
                }
            }
        }

        return favoritePayrolls;
    }

    public List<TVPayment> getPendingPayments() {
        return pendingPayments;
    }

    public void setPendingPayments(List<TVPayment> pendingPayments) {
        this.pendingPayments = pendingPayments;
    }

    public List<TVFrontEndPayee> getPayees(boolean includeFrequent) {
        if (payees == null || (includeFrequent && !this.loadedfrequentPayees)) {
            getQuickPayees(includeFrequent);
        }

        return payees;
    }

    public List<TVFrontendAccount> getTrfAccounts() {

		GlobalEntitlement ent = customerService.getGlobalEntitlement(EntitlementType.TRANSFERS);
		CustomerEntitlement bankent = this.customerProfile.getEntitlementByType(EntitlementType.BANKING);
		if (ent != null && ent.getStatus() == EntitlementStatus.ENABLED) {
			if (bankent != null) {
				evertec.cibp.core.models.entitlement.BankingEntitlement entt = new BankingEntitlement(bankent);
				if (trfAccounts == null)
					trfAccounts = customerService.getThirdPartyAccounts(customerProfile, entt, accounts);
			}
		}
		return trfAccounts;
	}

	public void resetAccountPreferences() {
		accounts = customerService.reloadAccountPreferences(customerProfile, accounts);

	}

    public boolean deletePayee(TVFrontEndPayee tvp, CustomerInteractionEvent customerInteractionEvent,
            boolean ebppStatus,boolean isHighRiskPayee) {

        return customerService.deleteCustomerPayee(customerProfile, bankingSession, tvp, customerInteractionEvent, isHighRiskPayee);
    }

    public List<TVFrontEndPayee> getPayessBasicEbpp(boolean includeFrequents) {
        try {
            if (!loadedbasicebpp || payees == null) {
                CustomerPaymentInfoResponse res = customerService.getPayeesBasicEbppInformation(customerProfile,
                        bankingSession, getPayees(includeFrequents));

                if (res.getPayees() != null && res.getPayees().size() > 0) {
                    this.payees = res.getPayees();
                    setGlobalPayeebyId();
                    this.loadedbasicebpp = true;
                    this.loadebpp = false;
                }

                return this.payees;
            } else
                return this.payees;
        } catch (RuntimeException e) {
            this.payees = null;
            return this.payees;
        }

    }

    private void setGlobalPayeebyId() {
        if (payees != null && payees.size() > 0)
            for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
                TVFrontEndPayee type = (TVFrontEndPayee) iterator.next();
                if (type.getGlobalPayee() == null) {
                    GlobalPayee global = PaymentsFacade.getInstance().getGlobalPayeeById(new Integer(type.getGlobalPayeeId()), this);
                    if (global != null)
                        type.setGlobalPayee(global);
                    else if (global == null && type.getTelepagoShortName() != null
                            && type.getTelepagoShortName().trim().endsWith("#")) {
                        global = new GlobalPayee();
                        global.setId(type.getGlobalPayeeId());
                        global.setPaymentType(GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO);
                        type.setGlobalPayee(global);
                    } else if (global == null) {
                        global = new GlobalPayee();
                        global.setId(type.getGlobalPayeeId());
                        global.setPaymentType(GlobalPayeePaymentType.DIRECT_DEBIT);
                        type.setGlobalPayee(global);
                    }
                }
            }

    }

    public List<TVFrontEndPayee> getPayeesPaymentHistoryInfo() {
        try {
            // if (!loadedPaymentHistory || payees == null) {

            CustomerPaymentInfoResponse res = customerService.getPayeesAndPaymentHistory(customerProfile,
                    bankingSession, getPayees(true), this.favoritePayments);

            if (res.getPayees() != null && res.getPayees().size() > 0) {
                this.payees = res.getPayees();
                setGlobalPayeebyId();
                this.loadedPaymentHistory = res.isLoadhistory();
            }

            // }
        } catch (RuntimeException e) {
            // TODO Auto-generated catch block
            this.payees = null;
        }
        return this.payees;

    }

    public boolean addCheckPayee(CustomerProfile profile, BankingSession bankingSession2,
            CustomerInteractionEvent customerInteractionEvent, String address1, String address2, String city,
            String postalCode, String state, String phone, String billinAct, String nick, boolean ebppStatus, boolean isHighRiskPayee) {

        boolean add = customerService.addCheckPayee(profile, bankingSession, address1, address2, city, postalCode,
                state, phone, billinAct, nick, customerInteractionEvent, isHighRiskPayee);
        if (add) {
            resetPayees();

        }
        return add;

    }

    public String getRemoteToken() {
        return remoteToken;
    }

    public void setRemoteToken(String remoteToken) {
        this.remoteToken = remoteToken;
    }

    @SuppressWarnings("deprecation")
    public SMSProfile getSmsProfile(CustomerInteractionEvent customerInteractionEvent) {

        if (customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
            SMSEntitlement ent = new SMSEntitlement(
                    customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING));
            GetSMSProfileRequest getreq = new GetSMSProfileRequest(customerInteractionEvent, customerProfile, ent);
            this.smsProfile = customerService.getSMSProfile(getreq);
        }

        return this.smsProfile;

    }

    public void setSmsProfile(SMSProfile smsProfile) {
        this.smsProfile = smsProfile;
    }

    public void resetMobileBanking() {
        this.smsaccounts = null;
        this.smsProfile = null;
    }

    public int getPasswordSalt() {
        return passwordSalt;
    }

    public void setPasswordSalt(int passwordSalt) {
        this.passwordSalt = passwordSalt;
    }

    public UsernameReservation getUsernameReservation() {
        return usernameReservation;
    }

    public void setUsernameReservation(UsernameReservation usernameReservation) {
        this.usernameReservation = usernameReservation;
    }

    public AccountApplications getAccountApplications(boolean validateEloanApplications) {
        int incompleteCount = 0;
        PendingApplicationRequest pendingApplicationRequest = new PendingApplicationRequest();
        pendingApplicationRequest.setUserSession(this);
        pendingApplicationRequest.setValidateEloanApplications(validateEloanApplications);

        if (this.accountApplications == null) {
            try {
                if (customerProfile != null) {
                    pendingApplicationRequest.setCustomerProfile(customerProfile);
                    this.accountApplications = AccountsFacade.getInstance().getPendingApplications(pendingApplicationRequest);

                }else {
                    pendingApplicationRequest.setUserName(this.getUsernameReservation().getUsername());
                    this.accountApplications = AccountsFacade.getInstance().getPendingApplications(pendingApplicationRequest);
                }		

                for (AccountApplication accountApplication : accountApplications.getCompletedApplications()) {
                    if (!accountApplication.isAccountAvailableInBackend()) {
                        canOpenAdditionalAccounts = false;
                        canCompleteAdditionalAccounts = false;
                        break;
                    }

                }

                for (AccountApplication accountApplication : accountApplications.getPendingApplications()) {
                    if (!accountApplication.isIncomplete()) {
                        canOpenAdditionalAccounts = false;
                        canCompleteAdditionalAccounts = false;
                        break;
                    } else
                        incompleteCount++;

                }

                this.incompleteApplicationCount = incompleteCount;

            } catch (Exception e) {
                // If for some reason we cannot get the accounts from the nerds
                // web service.. we just return an empty list.

                logger.error("getAccountApplications() - Could not obtain pending applications from nerds service", e); //$NON-NLS-1$
                return new AccountApplications();
            }
        }

        return this.accountApplications;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {

        this.language = language;
    }

    public PreferredLanguage getPreferredLanguage() {

        if (language != null && language.equals("en"))
            return PreferredLanguage.ENGLISH;
        else
            return PreferredLanguage.SPANISH;

    }

    public List<CustomerExternalTransferSource> getPendingExternalAccountsValidation() {

        if (customerExternalTransferSources == null) {
            customerExternalTransferSources = customerService.getPendingExternalAccountsValidation(customerProfile);
            DisposableIdentifier id = new DisposableIdentifier();
            for (CustomerExternalTransferSource transferSource : customerExternalTransferSources) {
                transferSource.setFrontEndId(id.getNextDisposableId());
            }
        }
        return customerExternalTransferSources;
    }

    public ExternalTransferSourceStatus validateExternalAccount(CustomerExternalTransferSource externalTransferSource,
            CustomerInteractionEvent customerInteractionEvent, BigDecimal amount1, BigDecimal amount2) {
        ExternalTransferSourceStatus validateExternalAccount = customerService.validateExternalAccount(customerProfile,
                externalTransferSource, amount1, amount2);
        if (ExternalTransferSourceStatus.VALIDATED.equals(validateExternalAccount)) {
            customerExternalTransferSources = null;
            List<CustomerExternalTransferSource> pendingExternalAccountsValidation = getPendingExternalAccountsValidation();
            ModifyCustomerRequest modifycustomerrequest = new ModifyCustomerRequest(customerInteractionEvent,
                    customerProfile.getProfileId(), CustomerFlagType.PENDING_EXT_ACCT_VALIDATION);

            if (pendingExternalAccountsValidation.size() == 0) {

                customerService.resetCustomerFlag(modifycustomerrequest);
            }
            resetExternalAccounts();
        }
        return validateExternalAccount;
    }

    public AccountPDFStmtInfo getPDFInfoById(String id) {
        this.getAcctPDFStatementsPrevStmt(true);

        if (this.intPdfStmtHistInfoById != null && this.intPdfStmtHistInfoById.containsKey(id)) {
            return this.intPdfStmtHistInfoById.get(id);
        } else if (this.acctPdfStmtHistInfoById != null && this.acctPdfStmtHistInfoById.containsKey(id)) {
            return this.acctPdfStmtHistInfoById.get(id);
        }
        return null;
    }

	// Interest Letters - Get all accounts of the customer
	private List<InterestAcctStmt> getInterestStmtAccts() {
		if (this.intStmtAccts == null && customerProfile.hasEntitlement(EntitlementType.EZSTMT) && customerService
				.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {
			this.intStmtAccts = customerService.getInterestStmtAccts(customerProfile.getTaxId());
		}

        return this.intStmtAccts;
    }

    // Interest Letters - All Letters
    private List<AccountPDFStmtInfo> getInterestStatements() {

		if (this.intStmts == null && customerProfile.hasEntitlement(EntitlementType.EZSTMT) && customerService
				.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {

            List<InterestAcctStmt> intStmtAccts = getInterestStmtAccts();

            // Create a hashtable to hold all the filters for this application
            if (ezStmtApplicationFilterTbl == null)
                ezStmtApplicationFilterTbl = new Hashtable<String, List<String>>();

            List<AccountPDFStmtInfo> historyInterest = new LinkedList<AccountPDFStmtInfo>();
            this.intPdfStmtHistInfoById = new Hashtable<String, AccountPDFStmtInfo>();

            for (Iterator<InterestAcctStmt> iterator = intStmtAccts.iterator(); iterator.hasNext();) {

                InterestAcctStmt account = (InterestAcctStmt) iterator.next();

                List<OSStmtPDFList> stmtPDFInfoLst = customerService.getStatementPDFList(account.getAccount_number());

                if (stmtPDFInfoLst != null) {

                    int qty = stmtPDFInfoLst.size() + 1;

                    for (Iterator<OSStmtPDFList> iterator2 = stmtPDFInfoLst.iterator(); iterator2.hasNext();) {

                        OSStmtPDFList stmtPDFInfo = (OSStmtPDFList) iterator2.next();

                        if (stmtPDFInfo != null) {

                            AccountPDFStmtInfo accountPDFStmtInfo = new AccountPDFStmtInfo();

                            // For this to be an interest letter or mla
                            // statement, it must have a
                            // product id in the following ids
                            if (stmtPDFInfo.getProductId() != null && ezStatementInformativaProductIds != null
                                    && ezStatementMortgageProductIds != null
                                    && Arrays.asList(ezStatementInformativaProductIds) != null
                                    && Arrays.asList(ezStatementMortgageProductIds) != null
                                    && (Arrays.asList(ezStatementMortgageProductIds)
                                            .contains(stmtPDFInfo.getProductId())
                                            || Arrays.asList(ezStatementInformativaProductIds)
                                            .contains(stmtPDFInfo.getProductId()))) {

                                boolean mlaStatement = Arrays.asList(ezStatementMortgageProductIds)
                                        .contains(stmtPDFInfo.getProductId());

                                if (mlaStatement) {
                                    TVFrontendAccount acc = Utils.getAccountByAccountNumber(account.getAccount_number(),
                                            this.getAccounts());
                                    if (acc != null) {
                                        if (!this.getAccounts().contains(acc)) {
                                            break;
                                        }
                                    } else
                                        break;
                                }

                                // One final verification - if the hashtable has
                                // the value, use that; if not look it up
                                List<String> ezStmtApplicationFilters = new LinkedList<String>();
                                if (ezStmtApplicationFilterTbl != null
                                        && ezStmtApplicationFilterTbl.get(account.getApplication()) != null) {
                                    ezStmtApplicationFilters = ezStmtApplicationFilterTbl.get(account.getApplication());
                                } else {
                                    ezStmtApplicationFilters = customerService
                                            .getEzStmtApplicationFilter(account.getApplication());
                                    ezStmtApplicationFilterTbl.put(account.getApplication(), ezStmtApplicationFilters);
                                }

                                if (ezStmtApplicationFilters.contains(stmtPDFInfo.getProductId())) {

                                    // Try to pair the account to find its bank
                                    // id
                                    accountPDFStmtInfo.setBankId("001");
                                    TVFrontendAccount frontendAccount = Utils.getAccountByAccountNumberForIRACAE(
                                            account.getAccount_number().replaceFirst("^0+(?!$)", ""),
                                            this.getAccounts());
                                    if (frontendAccount != null && frontendAccount.getBankId() != null)
                                        accountPDFStmtInfo.setBankId(frontendAccount.getBankId());

                                    qty -= 1;

                                    // This is to create unique keys to the PDF
                                    // to
                                    // prevent access of other people statements
                                    Long time = System.currentTimeMillis();

                                    // as the file uploaded has the account type
                                    // used by mainframe I change them with the
                                    // account type used in CIBP
                                    if (account.getAccount_number().length() <= 10)
                                        accountPDFStmtInfo.setId(account.getAccount_number()
                                                .substring(account.getAccount_number().length() / 2)
                                                + customerProfile.getTaxId().substring(
                                                        customerProfile.getTaxId().length() - 4)
                                                + time + qty);
                                    else
                                        accountPDFStmtInfo.setId(account.getAccount_number()
                                                .substring(account.getAccount_number().length() - 10)
                                                + customerProfile.getTaxId().substring(
                                                        customerProfile.getTaxId().length() - 4)
                                                + time + qty);

                                    accountPDFStmtInfo.setAccount_type(
                                            account.getApplication().replace("LVA", "LEA").replace("RES", "MLA"));
                                    accountPDFStmtInfo.setAccount_name("x" + account.getAccount_number()
                                    .substring(account.getAccount_number().length() - 4));

                                    if (mlaStatement && frontendAccount.getCustomerAccountPreferences() != null) {
                                        accountPDFStmtInfo.setAccount_nickname(
                                                frontendAccount.getCustomerAccountPreferences().getNickname());
                                    } else {
                                        accountPDFStmtInfo.setAccount_nickname("x" + account.getAccount_number()
                                        .substring(account.getAccount_number().length() - 4));

                                    }
                                    accountPDFStmtInfo.setInvoice_date(stmtPDFInfo.getStmtDate());
                                    accountPDFStmtInfo.setAccountNumber(account.getAccount_number());
                                    accountPDFStmtInfo.setDocId(stmtPDFInfo.getPdfId());

                                    accountPDFStmtInfo.setEzStmtProductId(stmtPDFInfo.getProductId());

                                    if (!GenericValidator.isBlankOrNull(stmtPDFInfo.getProductId()))
                                        accountPDFStmtInfo.setProductDescription(customerService
                                                .getEzStmtProductDescription(stmtPDFInfo.getProductId()));
                                    accountPDFStmtInfo.setViewed(stmtPDFInfo.isViewed());

                                    historyInterest.add(accountPDFStmtInfo);
                                    this.intPdfStmtHistInfoById.put(accountPDFStmtInfo.getId(), accountPDFStmtInfo);
                                }
                            }
                        }
                    }
                }
            }
            this.intStmts = historyInterest;
        }

        return this.intStmts;
    }

    // Interest Letter Recent Statement by Account
    public List<AccountPDFStmtInfo> getInterestStatementsLastStmt() {

		if (intStmtLastStmts == null && customerProfile.hasEntitlement(EntitlementType.EZSTMT) && customerService
				.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {

            if (this.intStmts == null)
                getInterestStatements();

            if (this.intStmts != null) {
                List<AccountPDFStmtInfo> lastHistoryInterest = new LinkedList<AccountPDFStmtInfo>();
                Hashtable<String, AccountPDFStmtInfo> intStmtData = new Hashtable<String, AccountPDFStmtInfo>();

                for (int i = 0; i < this.intStmts.size(); i++) {
                    AccountPDFStmtInfo account = (AccountPDFStmtInfo) this.intStmts.get(i);
                    AccountPDFStmtInfo lastInfo = null;

                    for (int j = 0; j < this.intStmts.size(); j++) {
                        AccountPDFStmtInfo account2 = (AccountPDFStmtInfo) this.intStmts.get(j);

                        if (account.getAccountNumber().equalsIgnoreCase(account2.getAccountNumber())
                                && account.getEzStmtProductId().equals(account2.getEzStmtProductId())) {
                            if (Double.parseDouble(account2.getInvoice_date().replaceAll("-", "")) > Double
                                    .parseDouble(account.getInvoice_date().replaceAll("-", ""))) {
                                lastInfo = account2;
                            } else {
                                lastInfo = account;
                            }
                        }

                    }
                    if (lastInfo != null && !intStmtData
                            .containsKey(lastInfo.getAccountNumber() + "|" + lastInfo.getEzStmtProductId())) {
                        lastHistoryInterest.add(lastInfo);
                        intStmtData.put(lastInfo.getAccountNumber() + "|" + lastInfo.getEzStmtProductId(), lastInfo);
                    }
                }

                this.intStmtLastStmts = lastHistoryInterest;
            }
        }
        return this.intStmtLastStmts;
    }

    // Interest Letter History excluding the most recent Statement
    public List<AccountPDFStmtInfo> getInterestStatementPrevStmt() {

		if (intStmtPrevStmts == null && customerProfile.hasEntitlement(EntitlementType.EZSTMT) && customerService
				.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {

            if (this.intStmtLastStmts == null)
                getInterestStatementsLastStmt();

            if (this.intStmtLastStmts != null) {
                List<AccountPDFStmtInfo> prevHistoryInterest = new LinkedList<AccountPDFStmtInfo>();
                Hashtable<String, AccountPDFStmtInfo> intLastStmtData = new Hashtable<String, AccountPDFStmtInfo>();

                for (int i = 0; i < this.intStmtLastStmts.size(); i++) {
                    AccountPDFStmtInfo account = (AccountPDFStmtInfo) this.intStmtLastStmts.get(i);
                    intLastStmtData.put(
                            account.getAccountNumber() + account.getEzStmtProductId() + account.getInvoice_date(),
                            account);
                }

                for (int j = 0; j < this.intStmts.size(); j++) {
                    AccountPDFStmtInfo account2 = (AccountPDFStmtInfo) this.intStmts.get(j);

                    if (!intLastStmtData.containsKey(
                            account2.getAccountNumber() + account2.getEzStmtProductId() + account2.getInvoice_date())) {
                        prevHistoryInterest.add(account2);
                    }
                }

                if (intLastStmtData.size() > 0) {

                    if (this.intPrevPdfStmtHistInfoTbl == null)
                        this.intPrevPdfStmtHistInfoTbl = new Hashtable<String, List<AccountPDFStmtInfo>>();

                    for (AccountPDFStmtInfo acctLastStmt : this.intStmtLastStmts) {

                        List<AccountPDFStmtInfo> prevAcctStmt = new LinkedList<AccountPDFStmtInfo>();

                        for (AccountPDFStmtInfo acctPrevStmt : prevHistoryInterest) {
                            if (acctLastStmt.getAccountNumber().equals(acctPrevStmt.getAccountNumber())) {
                                prevAcctStmt.add(acctPrevStmt);
                            }
                        }

                        prevAcctStmt = this.sortPrevHist(prevAcctStmt);

                        this.intPrevPdfStmtHistInfoTbl.put(acctLastStmt.getAccountNumber(), prevAcctStmt);

                        prevAcctStmt = null;
                    }
                }

                prevHistoryInterest = this.sortPrevHist(prevHistoryInterest);

                this.intStmtPrevStmts = prevHistoryInterest;
            }
        }
        return this.intStmtPrevStmts;
    }

    private List<AccountPDFStmtInfo> sortPrevHist(List<AccountPDFStmtInfo> list) {
        if (list != null && list.size() > 0) {
            boolean flag = true;
            AccountPDFStmtInfo tmp = null;

            while (flag) {
                flag = false;
                for (int counter = 0; counter < list.size() - 1; counter++) {
                    int date1 = Integer.valueOf(list.get(counter).getInvoice_date().replaceAll("-", ""));
                    int date2 = Integer.valueOf(list.get(counter + 1).getInvoice_date().replaceAll("-", ""));
                    if (date1 < date2) {
                        tmp = list.get(counter);
                        list.set(counter, list.get(counter + 1));
                        list.set(counter + 1, tmp);
                        flag = true;
                    }
                }
            }
        }

        return list;
    }

    // Interest Letter Green Box Notification
    public List<AccountPDFStmtInfo> getInterestStatementNewInboxNotification() {

		// Ez Statement - get New Statements Notification
		if (this.intStmtsNewAvailables == null && customerProfile.hasEntitlement(EntitlementType.EZSTMT)
				&& customerService.getGlobalEntitlement(EntitlementType.EBILLS)
						.getStatus() == EntitlementStatus.ENABLED) {

            if (this.intStmtLastStmts == null)
                getInterestStatementsLastStmt();

            List<AccountPDFStmtInfo> intStmtInbox = new LinkedList<AccountPDFStmtInfo>();

            for (Iterator<AccountPDFStmtInfo> iterator = this.intStmtLastStmts.iterator(); iterator.hasNext();) {
                AccountPDFStmtInfo account = iterator.next();

                List<String> list = customerService.getNewStatements(account.getAccountNumber(),
                        account.getEzStmtProductId());

                if (list != null && list.size() > 0) {
                    intStmtInbox.add(account);
                }

            }

            this.intStmtsNewAvailables = intStmtInbox;
        }
        // Ez Statement - end New Statements Notification

        return this.intStmtsNewAvailables;
    }

    private List<AccountPDFStmtInfo> getAcctAllPDFStmt(List<OSStmtPDFList> list, TVFrontendAccount acct) {
        List<AccountPDFStmtInfo> pdfList = new LinkedList<AccountPDFStmtInfo>();
        if (list != null) {
            int qty = 0;
            for (int j = 0; j < list.size(); j++) {
                OSStmtPDFList pdf = list.get(j);

                if(ezStatementInformativaProductIds != null)
                {
                    // Make sure that only non-informativas are loaded here
                    if (!Arrays.asList(ezStatementInformativaProductIds).contains(pdf.getProductId()))
                    {
                        AccountPDFStmtInfo info = new AccountPDFStmtInfo();

                        Long time = null;
                        info = new AccountPDFStmtInfo();

                        time = System.currentTimeMillis();
                        qty += 1;

                        info.setId(qty + acct.getAccountNumber().substring(acct.getAccountNumber().length() - 4) + time);
                        info.setAccount_type("PDFSTMT");
                        info.setAccount_name(acct.getNickname());
                        info.setAccount_nickname(acct.getNickname());
                        info.setAccountNumber(acct.getAccountNumber());
                        info.setDocId(pdf.getPdfId());
                        info.setInvoice_date(pdf.getStmtDate());
                        info.setEzStmtProductId(pdf.getProductId());
                        pdfList.add(info);
                    }
                }
            }
        }
        return pdfList;
    }

    private AccountPDFStmtInfo getAcctPDFLastStmt(List<OSStmtPDFList> list, TVFrontendAccount acct) {
        AccountPDFStmtInfo lastStmt = null;
        if (list != null && list.size() > 0) {
            OSStmtPDFList lastInfo = (OSStmtPDFList) list.get(0);
            if (list.size() > 1) {
                for (int j = 0; j < list.size(); j++) {
                    OSStmtPDFList account2 = (OSStmtPDFList) list.get(j);

                    if (Double.parseDouble(account2.getStmtDate().replaceAll("-", "")) > Double
                            .parseDouble(lastInfo.getStmtDate().replaceAll("-", ""))) {
                        lastInfo = account2;
                    }
                }
            }
            logger.info(acct.getAccountNumber() + " last:" + lastInfo != null ? lastInfo.getStmtDate() : "null");
            if (lastInfo != null) {
                int qty = 0;
                Long time = null;
                lastStmt = new AccountPDFStmtInfo();

                time = System.currentTimeMillis();
                qty += 1;

                lastStmt.setId(qty + acct.getAccountNumber().substring(acct.getAccountNumber().length() - 4) + time);
                lastStmt.setAccount_type("PDFSTMT");
                lastStmt.setAccount_name(acct.getNickname());
                lastStmt.setAccount_nickname(acct.getNickname());
                lastStmt.setAccountNumber(acct.getAccountNumber());
                lastStmt.setDocId(lastInfo.getPdfId());
                lastStmt.setInvoice_date(lastInfo.getStmtDate());
                lastStmt.setEzStmtProductId(lastInfo.getProductId());
            }
        }
        return lastStmt;
    }

    // Accounts PDF Statements
    // Make a list with the recent PDF Statement by Account
    public List<AccountPDFStmtInfo> getAcctPDFStatementsLastStmt(boolean ebppStatus) {

		if (customerProfile.hasEntitlement(EntitlementType.EBILLS) && customerService
				.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {

            List<AccountPDFStmtInfo> acctStmtInbox = new LinkedList<AccountPDFStmtInfo>();

            // INCID-325
            // Do not look up the same account number more than once
            List<String> accountsLookedUp = new LinkedList<String>();

            if (this.acctLastPdfStmtHistInfo == null) {

                int ebppIndex = -1;
                for (TVFrontendAccount acct : this.getAccounts()) {

                	if (acct != null)      	
                        logger.info(acct.getAccountNumber());
                    // INCID-325
                    // Only add the account if it has not been added yet
                    if (acct != null && !accountsLookedUp.contains(acct.getAccountNumber())) {
                        logger.info(acct.getAccountNumber() + " found in accts");
                        // INCID-325
                        // Add the account to the list
                        accountsLookedUp.add(acct.getAccountNumber());

                        List<OSStmtPDFList> pdfList = customerService.getStatementPDFList(acct);
                        if (pdfList != null && pdfList.size() > 0) {
                            AccountPDFStmtInfo lastPdf = this.getAcctPDFLastStmt(pdfList, acct);

                            // if it is a CCA, and the invoice dates are
                            // similar,
                            // then add the PDF id to the EBPP object set the EZ
                            // Statement flag to NOT SHOW
                            if (acct.getSubtype() != null) {
                                if (acct.getSubtype().equalsIgnoreCase("CCA")) {
                                    lastPdf.setDisplay(false);
                                    acctStmtInbox.add(lastPdf);
                                } else if (acct.getSubtype().equalsIgnoreCase("IDA")) {
                                    acctStmtInbox.add(lastPdf);
                                }
                            }
                        }
                    }
                }
                this.acctLastPdfStmtHistInfo = acctStmtInbox;
            }
        }
        return this.acctLastPdfStmtHistInfo;
    }

    // Accounts PDF Statements
    // Make a list with the PDF Statement History without the recent Statement
    public List<AccountPDFStmtInfo> getAcctPDFStatementsPrevStmt(boolean ebppStatus) {

		if (customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {

            if (acctPrevPdfStmtHistInfo == null) {
                getAcctPDFStatementsLastStmt(ebppStatus);

                if (this.acctLastPdfStmtHistInfo != null) {
                    if (this.acctPdfStmtHistInfoById == null)
                        this.acctPdfStmtHistInfoById = new Hashtable<String, AccountPDFStmtInfo>();

                    List<AccountPDFStmtInfo> prevHistoryStatement = new LinkedList<AccountPDFStmtInfo>();
                    List<AccountPDFStmtInfo> acctPdfList = new LinkedList<AccountPDFStmtInfo>();
                    Hashtable<String, AccountPDFStmtInfo> acctLastStmtData = new Hashtable<String, AccountPDFStmtInfo>();

                    for (int i = 0; i < this.acctLastPdfStmtHistInfo.size(); i++) {
                        AccountPDFStmtInfo account = (AccountPDFStmtInfo) this.acctLastPdfStmtHistInfo.get(i);
                        this.acctPdfStmtHistInfoById.put(account.getId(), account);

                        acctLastStmtData.put(account.getAccountNumber() + account.getInvoice_date(), account);

                        // get all PDF Statements
                        TVFrontendAccount acct = Utils.getAccountByAccountNumber(account.getAccountNumber(),
                                this.getAccounts());
                        List<OSStmtPDFList> pdfList = customerService.getStatementPDFList(acct);
                        List<AccountPDFStmtInfo> tmpPdfList = this.getAcctAllPDFStmt(pdfList, acct);
                        if (tmpPdfList != null) {
                            acctPdfList.addAll(tmpPdfList);
                        }
                    }

                    for (int j = 0; j < acctPdfList.size(); j++) {
                        AccountPDFStmtInfo account2 = (AccountPDFStmtInfo) acctPdfList.get(j);

                        if (!acctLastStmtData.containsKey(account2.getAccountNumber() + account2.getInvoice_date())) {
                            this.acctPdfStmtHistInfoById.put(account2.getId(), account2);
                            prevHistoryStatement.add(account2);
                        }
                    }

                    if (acctLastStmtData.size() > 0) {

                        if (this.acctPrevPdfStmtHistInfoTbl == null)
                            this.acctPrevPdfStmtHistInfoTbl = new Hashtable<String, List<AccountPDFStmtInfo>>();

                        for (AccountPDFStmtInfo acctLastStmt : this.acctLastPdfStmtHistInfo) {

                            List<AccountPDFStmtInfo> prevAcctStmt = new LinkedList<AccountPDFStmtInfo>();

                            for (AccountPDFStmtInfo acctPrevStmt : prevHistoryStatement) {
                                if (acctLastStmt.getAccountNumber().equals(acctPrevStmt.getAccountNumber())) {
                                    prevAcctStmt.add(acctPrevStmt);
                                }
                            }

                            prevAcctStmt = this.sortPrevHist(prevAcctStmt);

                            this.acctPrevPdfStmtHistInfoTbl.put(acctLastStmt.getAccountNumber(), prevAcctStmt);

                            prevAcctStmt = null;
                        }
                    }

                    prevHistoryStatement = this.sortPrevHist(prevHistoryStatement);

                    this.acctPrevPdfStmtHistInfo = prevHistoryStatement;
                }
            }
        }
        return this.acctPrevPdfStmtHistInfo;
    }

    public List<AccountPDFStmtInfo> getAcctPrevPDFStmtsByAccount(String accountNumber, boolean ebppStatus) {
        if (this.acctPrevPdfStmtHistInfoTbl == null)
            this.getAcctPDFStatementsPrevStmt(ebppStatus);

        if (this.acctPrevPdfStmtHistInfoTbl != null)
            return this.acctPrevPdfStmtHistInfoTbl.get(accountNumber);

        return null;
    }

    public List<AccountPDFStmtInfo> getIntPrevPDFStmtsByAccount(String accountNumber) {
        if (this.intPrevPdfStmtHistInfoTbl == null)
            this.getInterestStatementPrevStmt();

        if (this.intPrevPdfStmtHistInfoTbl == null)
            return null;

        return this.intPrevPdfStmtHistInfoTbl.get(accountNumber);
    }

    public List<AccountPDFStmtInfo> getIntPrevPDFStmtsByAccountAndProductId(AccountPDFStmtInfo account) {

        List<AccountPDFStmtInfo> completeList = getIntPrevPDFStmtsByAccount(account.getAccountNumber());
        List<AccountPDFStmtInfo> filteredList = new LinkedList<AccountPDFStmtInfo>();

        for (AccountPDFStmtInfo statement : completeList) {
            if (statement.getEzStmtProductId().equals(account.getEzStmtProductId())) {
                filteredList.add(statement);
            }
        }

        return filteredList;
    }

    public List<EnrollableAccount> getEnrollableAccounts() {
        return enrollableAccounts;
    }

    public void setEnrollableAccounts(List<EnrollableAccount> enrollableAccounts) {
        this.enrollableAccounts = enrollableAccounts;
    }

    public boolean isLoadebpp() {
        return loadebpp;
    }

    public void setLoadebpp(boolean loadebpp) {
        this.loadebpp = loadebpp;
    }

    public boolean isLoginCompleteSuccess() {
        return loginCompleteSuccess;
    }

    public void setLoginCompleteSuccess(boolean loginCompleteSuccess) {
        this.loginCompleteSuccess = loginCompleteSuccess;
    }

    public String toString() {

        StringBuffer sb = new StringBuffer();
        sb.append(username);
        sb.append(" Status: ");
        sb.append(loginCompleteSuccess ? "Logged In" : "Not Logged IN");
        return sb.toString();
    }

    public List<AggregatedAccount> getAggregatedAccounts() {
        if (this.aggregatedAccounts != null)
            return aggregatedAccounts;
        else {
            try {
                Integer aggcustomerid = customerService
                        .getAggregationCustomerIdByUsername(customerProfile.getUsername());
                aggregatedAccounts = customerService.getAggregationAccountsByCustomerId(aggcustomerid);
                return aggregatedAccounts;
            } catch (Exception e) {
                return null;
            }

        }
    }

    public void resetTransfers() {
        this.transferResponse = null;
    }

    public void getCustomerTransferInfo() {

        if (customerProfile.hasFlag(CustomerFlagType.ENROLL_TELEPAGO)
                || !customerProfile.hasEntitlement(EntitlementType.BILLPAY))
            customerService.enrollInTelepago(customerProfile,
                    Utils.getAccountByAccountNumber(customerProfile.getEnrollmentAccountNumber(), getAccounts()));

        if (this.transferResponse == null) {
            this.transferResponse = customerService.getCustomerTransferInfo(customerProfile, bankingSession,
                    getAccounts());
            List<FavoriteTransfer> favorites = this.transferResponse.getFavoriteTransfers();
            List<TVAccountFavoriteTransfer> favs = new LinkedList<TVAccountFavoriteTransfer>();

            for (Iterator<FavoriteTransfer> iterator = favorites.iterator(); iterator.hasNext();) {
                TVAccountFavoriteTransfer fav = (TVAccountFavoriteTransfer) iterator.next();
                favs.add(fav);
            }
            this.favoritesTransfers = favs;
        } else
            return;

    }

    public String getStatusmessage() {
        return statusmessage;
    }

    public void setStatusmessage(String statusmessage) {
        this.statusmessage = statusmessage;
    }

    public void setBankingSession(BankingSession bankingSession) {
        this.bankingSession = bankingSession;
    }

    public void resetPayees() {
        payeeres = null;
        payees = null;
        favoritePayments = null;
        favoriteVendors = null;
        favoritePayrolls = null;
        loadedfrequentPayees = false;
        loadedPaymentHistory = false;
        loadebpp = false;
        loadedbasicebpp = false;
    }

    public void resetSmsAccounts() {
        smsaccounts = null;
    }

    public void resetBalanceAlerts() {
        balanceAlerts = null;
    }

    public void resetSmsEbppAccounts() {
        smsebppaccounts = null;
    }

    public void resetPayerees() {
        payeeres = null;
    }

    public void resetFavoritePayments() {
        favoritePayments = null;
        favoriteVendors = null;
        favoritePayrolls = null;
        loadedfrequentPayees = false;
    }

    public void resetAccountApplications() {

        accountApplications = null;

    }

    // CIBP-1104
    public void resetEmailAccounts() {
        emailaccounts = null;
    }

    public boolean isCanOpenAdditionalAccounts() {
        if (checkApplications())
            getAccountApplications(true);
        return canOpenAdditionalAccounts;
    }

    private boolean checkApplications() {

        if (this.accountApplications == null)
            return ((userSessionType.equals(UserSessionType.CUSTOMER_PROFILE)
                    && customerProfile.isPendingAccountApplication())
                    || userSessionType.equals(UserSessionType.USERNAME_RESERVATION));
        else
            return false;

    }

    public boolean isCanCompleteAdditionalAccounts() {
        if (checkApplications())
            getAccountApplications(true);

        return canCompleteAdditionalAccounts;
    }

    public boolean hasVirginIslandAccount() {
        return hasViAccount;
    }

    public boolean hasBritishVirginIslandAccount() {
        return hasBViAccount;
    }

    public boolean isComercialCustomer() {
        if (userSessionType == UserSessionType.CUSTOMER_PROFILE && this.accounts == null && !delay) {
            this.getAccounts();
        }
        return comercialCustomer;
    }

    public UserSessionType getUserSessionType() {
        return userSessionType;
    }

    public void setUserSessionType(UserSessionType userSessionType) {
        this.userSessionType = userSessionType;
    }

    public Integer getIncompleteApplicationCount() {
        return incompleteApplicationCount;
    }

    public void setIncompleteApplications(Integer incompleteApplications) {
        if (this.accountApplications == null)
            getAccountApplications(true);

        this.incompleteApplicationCount = incompleteApplications;
    }

    private void getQuickPayees(boolean includeFrequents) {

        try {
            if (payeeres == null)
                payeeres = PaymentsServiceModule.getInstance().getFastPayees(this, bankingSession, includeFrequents);
            if (payees == null | (payees != null && payees.size() < 1) | favoritePayments == null
                    | (favoritePayments != null && favoritePayments.size() < 1)) {
                if (payeeres.getPayees() != null && payeeres.getPayees().size() > 0) {
                    this.payees = payeeres.getPayees();
                    setGlobalPayeebyId();
                    if (includeFrequents) {
                        this.favoritePayments = payeeres.getFrequentPayments();
                        this.favoritePayrolls = payeeres.getPayrollPayments();
                        this.favoriteVendors = payeeres.getVendorPayments();
                        this.loadedfrequentPayees = true;
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error getting  Quick Payees", e);
            this.payees = null;
        }
    }

    public void resetExternalAccounts() {
        customerExternalTransferSources = null;

    }

    public boolean isLoadedPaymentHistory() {
        return loadedPaymentHistory;
    }

    public void setLoadedPaymentHistory(boolean loadedPaymentHistory) {
        this.loadedPaymentHistory = loadedPaymentHistory;
    }

    public boolean isLoadedfrequentPayees() {
        return loadedfrequentPayees;
    }

    public void setLoadedfrequentPayees(boolean loadedfrequentPayees) {
        this.loadedfrequentPayees = loadedfrequentPayees;
    }

    // mpicado, cibp-1842
    public void resetInterestStmtsNewAvailables() {
        this.intStmtsNewAvailables = null;
    }

    public boolean isFromvi() {
        return fromvi;
    }

    public void setFromvi(boolean fromvi) {
        this.fromvi = fromvi;
    }

    public HashMap<String, String> getCheckImageMap() {
        return checkImageMap;
    }

    public void setCheckImageMap(HashMap<String, String> checkImageMap) {
        this.checkImageMap = checkImageMap;
    }

    public Map<Integer, GlobalPayee> getCommonGlobalpayees() {
        return commonGlobalpayees;
    }

    public void setCommonGlobalpayees(Map<Integer, GlobalPayee> commonGlobalpayees) {
        this.commonGlobalpayees = commonGlobalpayees;
    }

    public Map<String, String> getCommonGlobalPaymentTypeMap() {
        return commonGlobalPaymentTypeMap;
    }

    public void setCommonGlobalPaymentTypeMap(Map<String, String> commonGlobalPaymentTypeMap) {
        this.commonGlobalPaymentTypeMap = commonGlobalPaymentTypeMap;
    }

    public boolean isLoadedDelinquencyPmtInfo() {
        return loadedDelinquencyPmtInfo;
    }

    public void setLoadedDelinquencyPmtInfo(boolean loadedDelinquencyPmtInfo) {
        this.loadedDelinquencyPmtInfo = loadedDelinquencyPmtInfo;
    }

    public BinaryRecord getImageLogin() {
        return imageLogin;
    }

    public void setImageLogin(BinaryRecord imageLogin) {
        this.imageLogin = imageLogin;
    }

    public List<LoginCampaign> getLogincampaigns() {
        return logincampaigns;
    }

    public void setLogincampaigns(List<LoginCampaign> logincampaigns) {
        this.logincampaigns = logincampaigns;
    }

    public void setEbppPreviousHistoryBillers(List<EbppPreviousHistoryInfo> ebppPreviousHistoryBillers) {
        this.ebppPreviousHistoryBillers = ebppPreviousHistoryBillers;
    }

    public List<EbppPreviousHistoryInfo> getEbppPreviousHistoryBillers() {
        if (ebppPreviousHistoryBillers == null)
            ebppPreviousHistoryBillers = new LinkedList<EbppPreviousHistoryInfo>();

        return ebppPreviousHistoryBillers;
    }

    public void setRtMap(Map<String, String> rtMap) {
        this.rtMap = rtMap;
    }

    public Map<String, String> getRtMap() {
        return rtMap;
    }

    public String getErrorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }

    public String getErrorDetailCod() {
        return errorDetailCod;
    }

    public void setErrorDetailCod(String errorDetaiCod) {
        this.errorDetailCod = errorDetaiCod;
    }

    /**
     * @param acctNumber
     *            the prophItCustomerInfo to set
     */
    public void setProphItCustomerInfo(String acctNumber) {
        this.prophItCustomerInfo = customerService.prophItCustomerInquiry(acctNumber);
    }

    /**
     * @return the prophItCustomerInfo
     */
    public CustomerInquiryBean getProphItCustomerInfo() {
        return prophItCustomerInfo;
    }

    /**
     * @return the cryptoKey
     */
    public String getCryptoKey() {

        return this.cryptoKey;
    }

    /**
     * @param cryptoKey
     *            the cryptoKey to set
     */
    public void setCryptoKey(String cryptoKey) {
        this.cryptoKey = cryptoKey;
    }

    /**
     * Metodo retornaEdad metodo que devuelve la edad basado en un date, la idea
     * es pasarle por parametro el birthDate del profile para saber la edad de
     * un customer
     * 
     * @param dateEdad
     * @return edad basada en la fecha de nacimiento
     * @author mpicado cibp-2137
     */
    public int retornaEdad(Date dateEdad) {
        Calendar c = Calendar.getInstance();
        int actualYear = c.get(Calendar.YEAR);
        int actualMonth = c.get(Calendar.MONTH);
        int actualDay = c.get(Calendar.DAY_OF_MONTH);
        if (actualDay == 0)// el dia 0 es domingo
            actualDay = 7;

        Calendar cEdad = Calendar.getInstance();
        cEdad.setTime(dateEdad);
        int dia = cEdad.get(Calendar.DAY_OF_MONTH);
        int mes = cEdad.get(Calendar.MONTH);
        int anno = cEdad.get(Calendar.YEAR);

        if (anno < 100)
            anno += 1900;
        else {
            if (anno > actualYear)
                anno -= 100;
        }

        int edad = actualYear - anno;
        if (mes > actualMonth)
            edad--;
        else {
            if (actualMonth == mes) {
                if (dia > actualDay)
                    edad--;
            }
        }

        return edad;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public List<SignupCampaign> getCustomerSignupCampaigns() {
        if (customerSignupCampaigns == null) {
            customerSignupCampaigns = this.getCustomerService()
                    .getSignupCampaignInfoByProfileId(this.getCustomerProfile());
        }
        return customerSignupCampaigns;
    }
    
	public boolean isPremiumBanking(String premiumBanking, String wealthCodes, String preferredCodes) {
		boolean isPremiumBanking = false;

		if (!GenericValidator.isBlankOrNull(premiumBanking) && !GenericValidator.isBlankOrNull(wealthCodes)
				&& !GenericValidator.isBlankOrNull(preferredCodes)) {
			final String[] pmCodes = premiumBanking.split(":");

			CustomerWealthPreferred prefferedCodes = getCustomerWealthPreferred(wealthCodes, preferredCodes);

			if (prefferedCodes != null && prefferedCodes.getCodes() != null && !prefferedCodes.getCodes().isEmpty()) {
				isPremiumBanking = Utils.hasCustomerCode(prefferedCodes.getCodes(), pmCodes);
			}
		}
		return isPremiumBanking;
	}


    public boolean isWealth(String wealthCodes, String preferredCodes) {
        if (!wealthPreferredLoaded) {

            if (this.getBankingSession() != null) {
                BigInteger permId = this.getBankingSession().getPermId();

                if (permId == null)
                    permId = new BigInteger("0");

                custWealthPreferred = customerService.getWealthPreferredCustomerInfo(this.bankingSession, wealthCodes,
                        preferredCodes, customerProfileVault);

                wealthPreferredLoaded = true;
            }
        }

        return (custWealthPreferred == null) ? false : custWealthPreferred.isWealth();
    }

    public boolean isPreferred(String wealthCodes, String preferredCodes) {
        if (!wealthPreferredLoaded) {
            if (this.getBankingSession() != null) {
                BigInteger permId = this.getBankingSession().getPermId();

                if (permId == null)
                    permId = new BigInteger("0");

                custWealthPreferred = customerService.getWealthPreferredCustomerInfo(this.bankingSession, wealthCodes,
                        preferredCodes);
                wealthPreferredLoaded = true;
            }
        }

        return custWealthPreferred.isPreferred();
    }

    public CustomerWealthPreferred getCustomerWealthPreferred(String wealthCodes, String preferredCodes) {
        if (!wealthPreferredLoaded) {
            if (this.getBankingSession() != null) {
                BigInteger permId = this.getBankingSession().getPermId();

                if (permId == null)
                    permId = new BigInteger("0");

                custWealthPreferred = customerService.getWealthPreferredCustomerInfo(this.bankingSession, wealthCodes,
                        preferredCodes);

                wealthPreferredLoaded = true;
            }
        }

        return custWealthPreferred;
    }

    /**
     * Method to get the customer codes.
     * 
     */
    public List<String> getCustomerCodes(String wealthCodes, String preferredCodes) {
        // wealthCode and preferredCodes is for call iswealth() and get the
        // customer codes if doesnt exist in custWealthPreferred
        List<String> customerCodes = new LinkedList<String>();
        if (this.custWealthPreferred == null) {
            // we call it to get the customer codes.
            this.isWealth(wealthCodes, preferredCodes);
            customerCodes = custWealthPreferred.getCodes();
        } else if (custWealthPreferred.getCodes() != null) {
            // If already called the wealth.utils, custWealthPreferred have the
            // customer codes
            customerCodes = custWealthPreferred.getCodes();
        }
        return customerCodes;
    }

    public EmployeeInfoResponse getPrivateBankerInfo(String employeeNum) {

        if (employeeNum != null && !employeeNum.trim().equals("") && privateBankerInfo == null) {
            privateBankerInfo = customerService.getEmployeeInfo(employeeNum);
        }

        return privateBankerInfo;
    }

    public void setPrivateBankerImgLoaded(boolean privateBankerImgLoaded) {
        this.privateBankerImgLoaded = privateBankerImgLoaded;
    }

    public boolean isPrivateBankerImgLoaded() {
        return privateBankerImgLoaded;
    }

    public void setCustomerPayments(List<TVPayment> customerPayments) {
        this.customerPayments = customerPayments;

    }

    public List<TVPayment> getCustomerPayments() {
        return customerPayments;
    }

	public boolean getDisplayTransactionDowntime() {
		if (!displayTransactionDowntimeLoaded) {
			displayTransactionDowntimeLoaded = true;
			displayTransactionDowntime = !(customerService.getGlobalEntitlement(EntitlementType.TRANSACTIONS)
					.getStatus() == EntitlementStatus.ENABLED);

            if (displayTransactionDowntime) {
                transactionDowntimeMessages = customerService.getTransactionsDowntimeMessages();
            }
        }

        return displayTransactionDowntime;
    }

    public String[] getDisplayTransactionDowntimeMessages() {
        return transactionDowntimeMessages;
    }

    /** @return regresa el guidedSetupSession, mpicado cibp-2218 */
    public GuidedSetupSession getGuidedSetupSession() {
        return guidedSetupSession;
    }

    /**
     * @param guidedSetupSession
     *            el guidedSetupSession a establecer, mpicado cibp-2218
     */
    public void setGuidedSetupSession(GuidedSetupSession guidedSetupSession) {
        this.guidedSetupSession = guidedSetupSession;
    }

    public void setPfmId(String thisPfmId) {
        pfmId = thisPfmId;
    }

    public String getPfmId() {
        return pfmId;
    }

	public boolean isInPfmUpdatedLookedUp() {
		if (!this.inPfmUpdatedLookedUp) {
			this.inPfmUpdatedLookedUp = true;
			if (customerService.getGlobalEntitlement(EntitlementType.PFM).getStatus() == EntitlementStatus.ENABLED) {
				this.inPfmUpdate = customerService.inPFMUpdate();
			} else
				this.inPfmUpdate = false;
		}
		return inPfmUpdatedLookedUp;
	}

    public void setInPfmUpdatedLookedUp(boolean inPfmUpdatedLookedUp) {
        this.inPfmUpdatedLookedUp = inPfmUpdatedLookedUp;
    }

    public boolean isInPfmUpdate() {
        return inPfmUpdate;
    }

    public void setInPfmUpdate(boolean inPfmUpdate) {
        this.inPfmUpdate = inPfmUpdate;
    }

    public int getPfmCategoryId() {
        return pfmCategoryId;
    }

    public void setPfmCategoryId(int pfmCategoryId) {
        this.pfmCategoryId = pfmCategoryId;
    }

    public int getPfmSubcategoryId() {
        return pfmSubcategoryId;
    }

    public void setPfmSubcategoryId(int pfmSubcategoryId) {
        this.pfmSubcategoryId = pfmSubcategoryId;
    }

    public int getPfmCategoryIdToHighlight() {
        return pfmCategoryIdToHighlight;
    }

    public void setPfmCategoryIdToHighlight(int pfmCategoryIdToHighlight) {
        this.pfmCategoryIdToHighlight = pfmCategoryIdToHighlight;
    }

    public int getPfmTagId() {
        return pfmTagId;
    }

    public void setPfmTagId(int pfmTagId) {
        this.pfmTagId = pfmTagId;
    }

    public String getPfmTransactionIdToEdit() {
        return pfmTransactionIdToEdit;
    }

    public void setPfmTransactionIdToEdit(String pfmTransactionIdToEdit) {
        this.pfmTransactionIdToEdit = pfmTransactionIdToEdit;
    }

    public List<PFMCustomRuleWrapper> getPfmCustomRules() {
        return pfmCustomRules;
    }

    public void setPfmCustomRules(List<PFMCustomRuleWrapper> pfmCustomRules) {
        this.pfmCustomRules = pfmCustomRules;
    }

    public String getPfmPaging() {
        return pfmPaging;
    }

    public void setPfmPaging(String pfmPaging) {
        this.pfmPaging = pfmPaging;
    }

    public String getPfmCustomRulePaging() {
        return pfmCustomRulePaging;
    }

    public void setPfmCustomRulePaging(String pfmCustomRulePaging) {
        this.pfmCustomRulePaging = pfmCustomRulePaging;
    }

    public boolean isPfmSpanish() {
        return pfmSpanish;
    }

    public void setPfmSpanish(boolean pfmSpanish) {
        this.pfmSpanish = pfmSpanish;
    }

    public boolean isPfmCategoriesSortedSpanish() {
        return pfmCategoriesSortedSpanish;
    }

    public void setPfmCategoriesSortedSpanish(boolean pfmCategoriesSortedSpanish) {
        this.pfmCategoriesSortedSpanish = pfmCategoriesSortedSpanish;
    }

    public boolean isPfmTagsSortedSpanish() {
        return pfmTagsSortedSpanish;
    }

    public void setPfmTagsSortedSpanish(boolean pfmTagsSortedSpanish) {
        this.pfmTagsSortedSpanish = pfmTagsSortedSpanish;
    }

    public int getPfmCurrentRuleId() {
        return pfmCurrentRuleId;
    }

    public void setPfmCurrentRuleId(int pfmCurrentRuleId) {
        this.pfmCurrentRuleId = pfmCurrentRuleId;
    }

    public boolean isPfmUpdateCurrentRule() {
        return pfmUpdateCurrentRule;
    }

    public boolean isPfmDeleteCurrentRule() {
        return pfmDeleteCurrentRule;
    }

    public void setPfmDeleteCurrentRule(boolean pfmDeleteCurrentRule) {
        this.pfmDeleteCurrentRule = pfmDeleteCurrentRule;
    }

    public void setPfmUpdateCurrentRule(boolean pfmUpdateCurrentRule) {
        this.pfmUpdateCurrentRule = pfmUpdateCurrentRule;
    }

    public void setPfmAccounts(List<PFMAccountWrapper> thesePfmAccounts) {
        pfmAccounts = thesePfmAccounts;
    }

    public List<PFMAccountWrapper> getPfmAccounts() {
        return pfmAccounts;
    }

    public void setPfmCategories(List<PFMCategoryWrapper> thesePfmCategories) {
        pfmCategories = thesePfmCategories;
    }

    public List<PFMCategoryWrapper> getPfmCategories() {
        return pfmCategories;
    }

    public void setPfmTags(List<PFMTagWrapper> thesePfmTags) {
        pfmTags = thesePfmTags;
    }

    public List<PFMTagWrapper> getPfmTags() {
        return pfmTags;
    }

    public void setPfmOriginalChartInfo(PFMChartCategoryRecordWrapper thisPfmOriginalChartInfo) {
        pfmOriginalChartInfo = thisPfmOriginalChartInfo;
    }

    public PFMChartCategoryRecordWrapper getPfmOriginalChartInfo() {
        return pfmOriginalChartInfo;
    }

    public void setPfmPreviousChartInfo(PFMChartCategoryRecordWrapper thisPfmPreviousChartInfo) {
        pfmPreviousChartInfo = thisPfmPreviousChartInfo;
    }

    public PFMChartCategoryRecordWrapper getPfmPreviousChartInfo() {
        return pfmPreviousChartInfo;
    }

    public void setPfmChartInfo(PFMChartCategoryRecordWrapper thisPfmChartInfo) {
        pfmChartInfo = thisPfmChartInfo;
    }

    public PFMChartCategoryRecordWrapper getPfmChartInfo() {
        return pfmChartInfo;
    }

    public void setPfmChartNetIncomeInfo(List<PFMChartNetIncomeItemWrapper> thisPfmChartNetIncomeInfo) {
        pfmChartNetIncomeInfo = thisPfmChartNetIncomeInfo;
    }

    public List<PFMChartNetIncomeItemWrapper> getPfmChartNetIncomeInfo() {
        return pfmChartNetIncomeInfo;
    }

    public PFMTransactionRecordWrapper getPfmTransactionRecord() {
        return pfmTransactionRecord;
    }

    public void setPfmTransactionRecord(PFMTransactionRecordWrapper pfmTransactionRecord) {
        this.pfmTransactionRecord = pfmTransactionRecord;
    }

    public PFMTransactionRecordWrapper getPfmTransactionSubcategoryRecord() {
        return pfmTransactionSubcategoryRecord;
    }

    public void setPfmTransactionSubcategoryRecord(PFMTransactionRecordWrapper pfmTransactionSubcategoryRecord) {
        this.pfmTransactionSubcategoryRecord = pfmTransactionSubcategoryRecord;
    }

    public void setPfmFirstTime(String pfmFirstTime) {
        this.pfmFirstTime = pfmFirstTime;
    }

    public String getPfmFirstTime() {
        return this.pfmFirstTime;
    }

    public PFMTransactionWrapper getPfmTransactionToUseAsCustomRule() {
        return pfmTransactionToUseAsCustomRule;
    }

    public void setPfmTransactionToUseAsCustomRule(PFMTransactionWrapper pfmTransactionToUseAsCustomRule) {
        this.pfmTransactionToUseAsCustomRule = pfmTransactionToUseAsCustomRule;
    }

    public int getPfmTransactionToUseAsCustomRuleOriginalSubcategory() {
        return pfmTransactionToUseAsCustomRuleOriginalSubcategory;
    }

    public void setPfmTransactionToUseAsCustomRuleOriginalSubcategory(
            int pfmTransactionToUseAsCustomRuleOriginalSubcategory) {
        this.pfmTransactionToUseAsCustomRuleOriginalSubcategory = pfmTransactionToUseAsCustomRuleOriginalSubcategory;
    }

    public boolean isNewUserForAutomaticPFMEntitlementCreation() {
        return newUserForAutomaticPFMEntitlementCreation;
    }

    public void setNewUserForAutomaticPFMEntitlementCreation(boolean newUserForAutomaticPFMEntitlementCreation) {
        this.newUserForAutomaticPFMEntitlementCreation = newUserForAutomaticPFMEntitlementCreation;
    }

    public List<CustomerPassword> getPasswords() {
        return passwords;
    }

    public void setPasswords(List<CustomerPassword> passwords) {
        this.passwords = passwords;
    }

    public String getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(String lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public boolean isDeviceChecked() {
        return deviceChecked;
    }

    public void setDeviceChecked(boolean deviceChecked) {
        this.deviceChecked = deviceChecked;
    }

    public boolean isInformationChallenge() {
        return informationChallenge;
    }

    public void setInformationChallenge(boolean informationChallenge) {
        this.informationChallenge = informationChallenge;
    }

    public DeviceRequest getDeviceRequest() {
        return deviceRequest;
    }

    public void setDeviceRequest(DeviceRequest deviceRequest) {
        this.deviceRequest = deviceRequest;
    }

    public String getRsaTransactionId() {
        return rsaTransactionId;
    }

    public void setRsaTransactionId(String rsaTransactionId) {
        this.rsaTransactionId = rsaTransactionId;
    }

    public String getRsaSessionId() {
        return rsaSessionId;
    }

    public void setRsaSessionId(String rsaSessionId) {
        this.rsaSessionId = rsaSessionId;
    }

    public Map<Object, Object> getQuestion() {
        return question;
    }

    public void setQuestion(Map<Object, Object> question) {
        this.question = question;
    }

    public boolean isRemember() {
        return remember;
    }

    public void setRemember(boolean remember) {
        this.remember = remember;
    }

    public String getPm_fp() {
        return pm_fp;
    }

    public void setPm_fp(String pm_fp) {
        this.pm_fp = pm_fp;
    }

    public boolean isSkipRSA() {
        return skipRSA;
    }

    public void setSkipRSA(boolean skipRSA) {
        this.skipRSA = skipRSA;
    }

    public boolean isAlertsChecked() {
        return alertsChecked;
    }

    public void setAlertsChecked(boolean alertsChecked) {
        this.alertsChecked = alertsChecked;
    }

    public List<CCPlasticsCustInfo> getPlastics() {
        return plastics;
    }

    public void setPlastics(List<CCPlasticsCustInfo> plastics) {
        this.plastics = plastics;
    }

    public String getEbpp2UserId() {
        return ebpp2UserId;
    }

    public void setEbpp2UserId(String ebpp2UserId) {
        this.ebpp2UserId = ebpp2UserId;
    }

    public void setEbpp2BillerProducts(HashMap<String, String> ebpp2BillerProducts) {
        this.ebpp2BillerProducts = ebpp2BillerProducts;
    }

    public List<UserAccount> getEbpp2UserAccounts() {
        if (this.ebpp2UserAccounts == null) {
            this.ebpp2UserAccounts = new LinkedList<UserAccount>();
            try {
                this.ebpp2UserAccounts = customerService.getEbpp2UserAccounts(this.ebpp2UserId);
            } catch (Exception e) {
                System.out.println("Error obtaining EBPP2 User Accounts: " + e);
                return null;
            }
        }
        return ebpp2UserAccounts;
    }

    public void setEbpp2UserAccounts(List<UserAccount> ebpp2UserAccounts) {
        this.ebpp2UserAccounts = ebpp2UserAccounts;
    }

    public List<UserAccount> getEbpp2UserAccountsAndSummaries() {
        if (this.ebpp2UserAccountsAndSummaries == null) {

            this.ebpp2UserAccountsAndSummaries = new LinkedList<UserAccount>();

            if (this.ebpp2UserAccounts == null) {
                getEbpp2UserAccounts();
            }

            // Get the list of account summaries
            try {
                List<AccountSummary> ebpp2AccountSummaries = customerService.getEbpp2AccountSummary(this.ebpp2UserId);

                // Now loop through all the accounts...
                for (UserAccount userAccount : ebpp2UserAccounts) {

                    // And loop through all the summaries...
                    for (AccountSummary accountSummary : ebpp2AccountSummaries) {

                        // If we match the account and its summary, add it in
                        // the appropriate spot, based on invoice date
                        if (userAccount.getBillerAccountID() == accountSummary.getBillerAccountID()) {
                            if (userAccount.getAccountStatements() == null)
                                userAccount.setAccountStatements(new LinkedList<AccountSummary>());
                            if (userAccount.getAccountStatements().size() == 0)
                                userAccount.getAccountStatements().add(accountSummary);
                            else {
                                boolean insertedBefore = false;
                                for (int i = 0; i < userAccount.getAccountStatements().size(); i++) {
                                    if (userAccount.getAccountStatements().get(i).getInvoiceDate()
                                            .before(accountSummary.getInvoiceDate())) {
                                        userAccount.getAccountStatements().add(i, accountSummary);
                                        insertedBefore = true;
                                        break;
                                    }
                                }
                                if (!insertedBefore)
                                    userAccount.getAccountStatements().add(accountSummary);
                            }
                        }
                    }

                    // Add this account and its summaries
                    this.ebpp2UserAccountsAndSummaries.add(userAccount);
                }
            } catch (Exception e) {
                System.out.println("Error obtaining EBPP2 User Account Summaries: " + e);
                return null;
            }
        }
        return ebpp2UserAccountsAndSummaries;
    }

    public void setEbpp2UserAccountsAndSummaries(List<UserAccount> ebpp2UserAccountsAndSummaries) {
        this.ebpp2UserAccountsAndSummaries = ebpp2UserAccountsAndSummaries;
    }

    public boolean isEbpp2UserIdLookedUp() {
        return ebpp2UserIdLookedUp;
    }

    public void setEbpp2UserIdLookedUp(boolean ebpp2UserIdLookedUp) {
        this.ebpp2UserIdLookedUp = ebpp2UserIdLookedUp;
    }

    public List<UserAccount> getEbpp2UserAccountsAlgaroEbills() {
        if (this.ebpp2UserAccountsAlgaroEbills == null) {

            this.ebpp2UserAccountsAlgaroEbills = new LinkedList<UserAccount>();

            if (this.ebpp2UserAccounts == null) {
                getEbpp2UserAccounts();
            }

            // Get the list of payees and match their accounts
            try {
                List<TVFrontEndPayee> payees = getPayees(false);

                // Loop through all the EBPP2 accounts...
                for (UserAccount userAccount : ebpp2UserAccounts) {

                    // For this to be an e-Bill, the biller ID must be greater
                    // than 1 OR
                    // the product ID must be 55, 59 or 60 (credit card)
                    if (userAccount.getBillerId() > 1
                            || (userAccount.getBillerId() == 1 && userAccount.getBillerProductId() == 55
                            || userAccount.getBillerProductId() == 59 || userAccount.getBillerProductId() == 60
                            || userAccount.getBillerProductId() == 61)) {

                        // ...and through all the payees
                        boolean found = false;
                        for (TVFrontEndPayee payee : payees) {
                            if (userAccount.getAccountNumber().replaceFirst("^0+(?!$)", "")
                                    .equals(payee.getBillingAccount().replaceFirst("^0+(?!$)", ""))) {

                                found = true;
                                break;
                            }
                        }

                        // If we did not match the payee and account, it is an
                        // algaro e-Bill
                        if (!found)
                            this.ebpp2UserAccountsAlgaroEbills.add(userAccount);
                    }
                }
            } catch (Exception e) {
                System.out.println("Error obtaining EBPP2 User Account Algaro e-Bills: " + e);
                return null;
            }
        }
        return ebpp2UserAccountsAlgaroEbills;
    }

    public void setEbpp2UserAccountsAlgaroEbills(List<UserAccount> ebpp2UserAccountsAlgaroEbills) {
        this.ebpp2UserAccountsAlgaroEbills = ebpp2UserAccountsAlgaroEbills;
    }

    public List<UserAccount> getEbpp2UserAccountsAlgaroEstatements() {
        if (this.ebpp2UserAccountsAlgaroEstatements == null) {

            this.ebpp2UserAccountsAlgaroEstatements = new LinkedList<UserAccount>();

            if (this.ebpp2UserAccounts == null) {
                getEbpp2UserAccounts();
            }

            // Get the list of accounts and match their numbers
            try {
                List<TVFrontendAccount> accounts = getAccounts();

                // Loop through all the EBPP2 accounts...
                for (UserAccount userAccount : ebpp2UserAccounts) {

                    // For this to be an e-Statement, the biller ID must be 1
                    // and
                    // the product ID CANNOT be 55, 59, 60 or 61 (credit card)
                    if (userAccount.getBillerId() == 1
                            && !(userAccount.getBillerProductId() == 55 || userAccount.getBillerProductId() == 59
                            || userAccount.getBillerProductId() == 60)
                            || userAccount.getBillerProductId() == 61) {

                        // ...and through all the accounts
                        boolean found = false;
                        for (TVFrontendAccount account : accounts) {
                            if (userAccount.getAccountNumber().replaceFirst("^0+(?!$)", "")
                                    .equals(account.getAccountNumber().replaceFirst("^0+(?!$)", ""))) {

                                found = true;
                                break;
                            }
                        }

                        // If we did not match the account numbers, it is an
                        // algaro e-Statement
                        if (!found)
                            this.ebpp2UserAccountsAlgaroEstatements.add(userAccount);
                    }
                }
            } catch (Exception e) {
                System.out.println("Error obtaining EBPP2 User Account Algaro e-Statements: " + e);
                return null;
            }
        }
        return ebpp2UserAccountsAlgaroEstatements;
    }

    public void setEbpp2UserAccountsAlgaroEstatements(List<UserAccount> ebpp2UserAccountsAlgaroEstatements) {
        this.ebpp2UserAccountsAlgaroEstatements = ebpp2UserAccountsAlgaroEstatements;
    }

    public void resetEbpp2Accounts() {
        setEbpp2UserAccounts(null);
        setEbpp2UserAccountsAndSummaries(null);
        setEbpp2UserAccountsAlgaroEbills(null);
        setEbpp2UserAccountsAlgaroEstatements(null);
    }

    public boolean isHasEAccounts() {
        return hasEAccounts;
    }

    public void setHasEAccounts(boolean hasEAccounts) {
        this.hasEAccounts = hasEAccounts;
    }

    public boolean isOobAuthenticated() {
        return oobAuthenticated;
    }

    public void setOobAuthenticated(boolean oobAuthenticated) {
        this.oobAuthenticated = oobAuthenticated;
    }

    public Boolean getCanOpenDepositAccount() {
        return canOpenDepositAccount;
    }

    public void setCanOpenDepositAccount(Boolean canOpenDepositAccount) {
        this.canOpenDepositAccount = canOpenDepositAccount;
    }

    public boolean hasCCA() {
        for (TVFrontendAccount account : this.getAccounts()) {
            if (account.getProductDefinition().getProductType().equals("CCA")) {
                hasCCA = true;
                break;
            }
        }
        return hasCCA;
    }

    public void setHasCCA(boolean b) {
        this.hasCCA = b;
    }

    public boolean hasVisibleCCA() {
        return this.hasVisibleCCA;
    }

    public void setHasVisibleCCA(boolean hasVisibleCCA) {
        this.hasVisibleCCA = hasVisibleCCA;
    }

    // CIBP-2796
    // PMA accounts will bypass the deposit account check
    public boolean hasPifBypassAccount(String bypassAccounts) {
        boolean addPifAccountsEnabled = false;
        String[] accountsList = bypassAccounts.split(",");
        for (String bypassAccount : accountsList) {
            for (TVFrontendAccount account : this.getAccounts()) {
                if (account.getProductDefinition().getProductId().equals(bypassAccount)) {
                    addPifAccountsEnabled = true;
                    break;
                }
            }
        }
        return addPifAccountsEnabled;
    }

    // Remote Deposit
    public RDCEntitlement getFisInfo() {
        // The FIS user id is not yet in session; add it
        if (fisInfo == null) {
            if (this.getCustomerProfile().getEntitlementByType(EntitlementType.RDC) != null) {
                RDCEntitlement ent = new RDCEntitlement(
                        this.getCustomerProfile().getEntitlementByType(EntitlementType.RDC));
                // If the user id is blank, we need to set it to a new one
                if (GenericValidator.isBlankOrNull(ent.getRdcCustomerId())) {
                    logger.debug("FIS USER ID BEFORE UPDATE IS " + ent.getRdcCustomerId());
                    logger.debug("FIS ACCT CHK GRP ID BEFORE UPDATE IS " + ent.getRdcAccountGroupChk());
                    logger.debug("FIS SSO ID BEFORE UPDATE IS " + ent.getRdcSsoUserId());
                    customerService.updateRDCEntitlementCustomerId(this.getCustomerProfile(), ent);
                    CustomerEntitlement newEnt = this.getCustomerProfile().getEntitlementByType(EntitlementType.RDC);
                    RDCEntitlement ent2 = new RDCEntitlement(newEnt);
                    logger.debug("FIS USER ID AFTER UPDATE IS " + ent2.getRdcCustomerId());
                    logger.debug("FIS ACCT CHK GRP ID AFTER UPDATE IS " + ent2.getRdcAccountGroupChk());
                    logger.debug("FIS SSO ID AFTER UPDATE IS " + ent2.getRdcSsoUserId());
                    this.fisInfo = ent2;
                } else {
                    this.fisInfo = ent;
                }
            } else {
                fisInfo = null;
            }
        }

        return fisInfo;
    }

    public Hashtable<String, Long> getFisAccounts() {
        return fisAccounts;
    }

    public void setFisAccounts(Hashtable<String, Long> fisAccounts) {
        this.fisAccounts = fisAccounts;
    }

    public boolean isFisEnrolled() {
        return fisEnrolled;
    }

    public void setFisEnrolled(boolean fisEnrolled) {
        this.fisEnrolled = fisEnrolled;
    }

    public HashMap<String, List<TransferInfo>> getAthmTransfersInfoHash() {
        return athmTransfersInfoHash;
    }

    public void setAthmTransfersInfoHash(HashMap<String, List<TransferInfo>> athmTransfersInfoHash) {
        this.athmTransfersInfoHash = athmTransfersInfoHash;
    }

    public void resetATHMTransferInfoHash() {
        this.athmTransfersInfoHash = null;
    }

    public List<TVAccountTransferSource> getAthmAccounts() {
        return athmAccounts;
    }

    public boolean isAthmUnregistered() {
        return athmUnregistered;
    }

    public void setAthmUnregistered(boolean athmUnregistered) {
        this.athmUnregistered = athmUnregistered;
    }

    public void setAuthorizations(HashMap<String, Authorizations> authorizations) {
        this.authorizations = authorizations;
    }

    public HashMap<String, Authorizations> getAuthorizations() {
        if (this.authorizations == null) {
            this.authorizations = new HashMap<String, Authorizations>();
        }
        return authorizations;
    }

    public void getAuthorizations(String key, TVFrontendAccount account) {
        this.setProphItCustomerInfo(account.getAccountNumber());
        if (this.getProphItCustomerInfo() != null) {
            AuthorizationBean authorization = new AuthorizationBean(this.getProphItCustomerInfo().getAccountNumber(),
                    this.getProphItCustomerInfo().getOrgLevel(), this.getProphItCustomerInfo().getOrgId(),
                    this.getProphItCustomerInfo().getCaseNumber(), this.getProphItCustomerInfo().getNewContact(),
                    this.getProphItCustomerInfo().getKeepCaseOpen(), this.getProphItCustomerInfo().getChannelId(),
                    this.getProphItCustomerInfo().getImpersonateId());
            AuthorizationResponse authorizations = this.customerService.prophitAuthorizations(authorization);
            if (authorizations != null) {
                this.getAuthorizations().put(key, authorizations.getAuthorizations());
            } else {
                this.getAuthorizations().put(key, null);
            }
            this.prophItCustomerInfo = null;
        } else {
            this.getAuthorizations().put(key, null);
        }
    }

    public void setEzStatementInformativaProductIds(String[] ezStatementInformativaProductIds) {
        this.ezStatementInformativaProductIds = ezStatementInformativaProductIds;
    }

    public String[] getEzStatementInformativaProductIds() {
        return this.ezStatementInformativaProductIds;
    }

    public String[] getEzStatementMortgageProductIds() {
        return this.ezStatementMortgageProductIds;
    }

    public void setEzStatementMortgageProductIds(String[] ezStatementMortgageProductIds) {
        this.ezStatementMortgageProductIds = ezStatementMortgageProductIds;
    }

    public AthmGetServiceChargeResp getAthmServiceCharge(String step) {
        if (athmServiceCharge == null || step.equals("DISPLAY")) {
            athmServiceCharge = customerService.getServiceCharge();
        }
        return athmServiceCharge;
    }

    public List<DCIQuestion> getDciQuestions() {
        return dciQuestions;
    }

    public void setDciQuestions(List<DCIQuestion> dciQuestions) {
        this.dciQuestions = dciQuestions;
    }

    public List<DCIQuestionAnswer> getDciQuestionAnswers() {
        return dciQuestionAnswers;
    }

    public void setDciQuestionAnswers(List<DCIQuestionAnswer> dciQuestionAnswers) {
        this.dciQuestionAnswers = dciQuestionAnswers;
    }

    public DCICustomer getDciCustomer() {
        return dciCustomer;
    }

    public void setDciCustomer(DCICustomer dciCustomer) {
        this.dciCustomer = dciCustomer;
    }

    public List<DCICustomerAnswer> getDciCustomerAnswers() {
        return dciCustomerAnswers;
    }

    public void setDciCustomerAnswers(List<DCICustomerAnswer> dciCustomerAnswers) {
        this.dciCustomerAnswers = dciCustomerAnswers;
    }

    public boolean isHasDDA() {
        return hasDDA;
    }

    public void setHasDDA(boolean hasDDA) {
        this.hasDDA = hasDDA;
    }

    public boolean hasVisibleDDA() {
        return hasVisibleDDA;
    }

    public void setHasVisibleDDA(boolean hasVisibleDDA) {
        this.hasVisibleDDA = hasVisibleDDA;
    }

    public boolean isHasCD() {
        return hasCD;
    }

    public void setHasCD(boolean hasCD) {
        this.hasCD = hasCD;
    }

    public boolean isHasPremia() {
        return hasPremia;
    }

    public void setHasPremia(boolean hasPremia) {
        this.hasPremia = hasPremia;
    }

    public boolean hasVisiblePremia() {
        return hasVisiblePremia;
    }

    public void setHasVisiblePremia(boolean hasVisiblePremia) {
        this.hasVisiblePremia = hasVisiblePremia;
    }

    public boolean isHasMortgage() {
        return hasMortgage;
    }

    public void setHasMortgage(boolean hasMortgage) {
        this.hasMortgage = hasMortgage;
    }

    public boolean isHasEbill() {
        return hasEbill;
    }

    public void setHasEbill(boolean hasEbill) {
        this.hasEbill = hasEbill;
    }

    public boolean isHasEstatment() {
        return hasEstatment;
    }

    public void setHasEstatment(boolean hasEstatment) {
        this.hasEstatment = hasEstatment;
    }

    public boolean isHasEaccount() {
        return hasEaccount;
    }

    public void setHasEaccount(boolean hasEaccount) {
        this.hasEaccount = hasEaccount;
    }

    public boolean isHasAuto() {
        return hasAuto;
    }

    public void setHasAuto(boolean hasAuto) {
        this.hasAuto = hasAuto;
    }

    public boolean isHasIRA() {
        return hasIRA;
    }

    public void setHasIRA(boolean hasIRA) {
        this.hasIRA = hasIRA;
    }

    public boolean isHasInsurance() {
        return hasInsurance;
    }

    public void setHasInsurance(boolean hasInsurance) {
        this.hasInsurance = hasInsurance;
    }

    public boolean isHasLeasing() {
        return hasLeasing;
    }

    public void setHasLeasing(boolean hasLeasing) {
        this.hasLeasing = hasLeasing;
    }

    public boolean isHasPayee() {
        return hasPayee;
    }

    public void setHasPayee(boolean hasPayee) {
        this.hasPayee = hasPayee;
    }

    public boolean isHasSecurities() {
        return hasSecurities;
    }

    public void setHasSecurities(boolean hasSecurities) {
        this.hasSecurities = hasSecurities;
    }

    public boolean isHasPersonalLoans() {
        return hasPersonalLoans;
    }

    public void setHasPersonalLoans(boolean personalLoans) {
        this.hasPersonalLoans = personalLoans;
    }

    /**
     * @return the disbursementsList
     */
    public List<CustomerDocumentsPackage> getDisbursementsList(boolean refresh) {

        DocumentsFacade documents = DocumentsFacade.getInstance();
        return documents.getDisbursementsList( refresh, this );
    }

    public List<CustomerDocuments> getOrderedDocsList(CustomerDocumentsPackage disb) {

        int count = 0;
        List<CustomerDocuments> orderedDocsList = new LinkedList<CustomerDocuments>();
        List<CIBPConfigurableTexts> orderedDbText = this.getOrderedDisbursementList();

        for (Iterator<CIBPConfigurableTexts> iterator = orderedDbText.iterator(); iterator.hasNext();) {
            CIBPConfigurableTexts orderedText = (CIBPConfigurableTexts) iterator.next();
            if (disb.getCustomerDocumentsMap().containsKey(orderedText.getTextCode())) {
                count++;

                String deadlineTime = new SimpleDateFormat("h:mm a").format(disb.getSignatureDeadlineDate());
                if (this.getPreferredLanguage() == PreferredLanguage.ENGLISH) {
                    disb.setFrontendDeadline(deadlineTime + " on "
                            + new SimpleDateFormat("MM/dd/yyyy").format(disb.getSignatureDeadlineDate()));
                } else {
                    disb.setFrontendDeadline(deadlineTime + " de "
                            + new SimpleDateFormat("dd/MM/yyyy").format(disb.getSignatureDeadlineDate()));
                }

                for (CustomerDocuments doc : disb.getCustomerDocuments()) {
                    String text = this.getDBConfiguredText(CIBPConfigurableTextsTypes.DESEMBOLSO_ASISTIDO,
                            ConfigurableTextSubSections.DOCUMENT_NAMES, doc.getId().getDocInfoId());
                    doc.setFrontEndText(text);
                }

                orderedDocsList.add(disb.getCustomerDocumentsMap().get(orderedText.getTextCode()));
                if (count == disb.getCustomerDocuments().size()) {
                    break;
                }
            }
        }
        return orderedDocsList;
    }

    public List<CustomerDocumentsPackage> getDisbursementList() {
        return disbursementsList;
    }

    public List<CustomerDocumentsPackage> getDisbursementsAfterAcceptance() {
        return disbursementsAfterAcceptance;
    }

    public List<CustomerDocuments> getDocumentsAfterAcceptance(String loanNumber) {
        if (disbursementsAfterAcceptance == null) {
            disbursementsAfterAcceptance = DocumentsFacade.getInstance().getAcceptedDisbursements(this);
        }
        if (disbursementsAfterAcceptance != null) {
            for (CustomerDocumentsPackage pack : disbursementsAfterAcceptance) {
                List<CustomerDocuments> docsList = new LinkedList<CustomerDocuments>();
                if (pack.getDocuments() != null && pack.getDocuments().size() > 0) {
                    LinkedHashMap<String, CustomerDocuments> docsMap = new LinkedHashMap<String, CustomerDocuments>();
                    for (CustomerDocuments docs : pack.getDocuments()) {
                        if ((docs.getHideDoc() == null || "N".equals(docs.getHideDoc()))
                                && "Y".equals(docs.getDisplayAfter())) {
                            docsMap.put(docs.getId().getDocInfoId(), docs);
                            docsList.add(docs);
                        }
                        pack.setCustomerDocuments(docsList);
                        pack.setCustomerDocumentsMap(docsMap);
                        // set up ordered list
                        pack.setCustomerDocuments(getOrderedDocsList(pack));

                    }
                }
            }

            for (CustomerDocumentsPackage disbursements : disbursementsAfterAcceptance) {
                if (disbursements.getPackSearchId().equals(loanNumber)) {
                    if (disbursements.getCustomerDocuments() != null
                            && disbursements.getCustomerDocuments().size() > 0) {

                        for (CustomerDocuments docs : disbursements.getCustomerDocuments()) {
                            String text = this.getDBConfiguredText(CIBPConfigurableTextsTypes.DESEMBOLSO_ASISTIDO,
                                    ConfigurableTextSubSections.DOCUMENT_NAMES, docs.getId().getDocInfoId());
                            docs.setFrontEndText(text);
                        }
                        return disbursements.getCustomerDocuments();
                    }
                    break;
                }
            }
        }
        return new LinkedList<CustomerDocuments>();
    }

    /**
     * @param disbursementsList
     *            the disbursementsList to set
     */
    public void setDisbursementsList(List<CustomerDocumentsPackage> disbursementsList) {
        this.disbursementsList = disbursementsList;
    }

    /**
     * Gets lists from CIBP_CONFIGURABLE_TXT table in the database Useful when
     * the data to get is to populate a dropdown from the database To get a
     * specific text, please use getDBConfiguredText(...)
     * 
     * @param section
     *            - The project ID
     * @param subSection
     *            - The subsection ID
     * @return A list of the required configured text
     */
    public List<CIBPConfigurableTexts> getDBConfiguredTextList(CIBPConfigurableTextsTypes section,
            ConfigurableTextSubSections subSection) {
        if (configurableTextListMap == null) {
            configurableTextListMap = new HashMap<String, List<CIBPConfigurableTexts>>();
        }

        if (!configurableTextListMap.containsKey(section + "|" + subSection)) {
            CIBPConfigurableTexts filter = new CIBPConfigurableTexts();
            filter.setProjectId(section);
            filter.setSubSectionId(subSection);
            List<CIBPConfigurableTexts> textList = customerService.getFilteredConfigurableText(filter);
            configurableTextListMap.put(section + "|" + subSection, textList);
        }
        return configurableTextListMap.get(section + "|" + subSection);
    }

    public String getDBConfiguredText(CIBPConfigurableTextsTypes section, ConfigurableTextSubSections subSection,
            String textCode) {
        String SEPARATOR = "|";
        String textToDisplay = "";
        String textKey = section + SEPARATOR + subSection + SEPARATOR + textCode;

        if (configurableTextMap == null)
            configurableTextMap = new LinkedHashMap<String, CIBPConfigurableTexts>();

        String generalSectionKey = section + SEPARATOR + subSection;
        if (!configurableTextMap.containsKey(generalSectionKey)) {
            // Query by section
            CIBPConfigurableTexts filter = new CIBPConfigurableTexts();
            filter.setProjectId(section);
            filter.setSubSectionId(subSection);
            List<CIBPConfigurableTexts> textList = customerService.getFilteredConfigurableText(filter);
            if (textList != null && textList.size() > 0) {
                // Add element with section as key
                configurableTextMap.put(generalSectionKey, null);
                // Fillout hashmap with composite key
                for (CIBPConfigurableTexts configuredText : textList) {
                    String varKey = configuredText.getProjectId() + SEPARATOR + configuredText.getSubSectionId()
                    + SEPARATOR + configuredText.getTextCode();
                    configurableTextMap.put(varKey, configuredText);
                }
            }
        }

        if (configurableTextMap.containsKey(textKey)) {

            CIBPConfigurableTexts textObject = configurableTextMap.get(textKey);
            if (this.getPreferredLanguage() == PreferredLanguage.ENGLISH)
                textToDisplay = textObject.getEnglishDescription();
            else
                textToDisplay = textObject.getSpanishDescription();
        }
        return textToDisplay;
    }

    public List<CIBPConfigurableTexts> getOrderedDisbursementList() {
        if (disbTextList == null) {
            CIBPConfigurableTexts filter = new CIBPConfigurableTexts();
            filter.setProjectId(CIBPConfigurableTextsTypes.DESEMBOLSO_ASISTIDO);
            filter.setSubSectionId(ConfigurableTextSubSections.DOCUMENT_NAMES);
            disbTextList = customerService.getFilteredConfigurableText(filter);
        }
        return disbTextList;
    }

    /**
     * Method to get the Solicitud Unica application list
     * 
     * @param resetApps
     *            Variable to reset the list
     * @return The Solicitud Unica applications list
     */
    public List<UnicaApplication> getUnicaApplications() {
        return unicaApplications;
    }

    /**
     * Method to set the Unica applications from the AjaxActionsController class
     * 
     * @param applications
     *            The applications list
     */
    public void setUnicaApplications(List<UnicaApplication> applications) {
        unicaApplications = applications;
    }

    /**
     * Method to verify if the offer for a specific application has been
     * accepted The call to the database is made only 1 time
     * 
     * @param applicationId
     *            The application ID
     * @return true if the application offer was accepted, false otherwise
     */
    public boolean isUnicaOfferAccepted(String applicationId) {
        if (unicaAcceptedOffers == null)
            unicaAcceptedOffers = customerService.unicaOfferAccepted(this.getCustomerProfile().getTaxId());

        if (unicaAcceptedOffers.contains(applicationId))
            return true;
        return false;
    }

    /**
     * Method to update the list locally (after the first call) This to avoid a
     * call to the database updating the list everytime an offer is accepted.
     * 
     * @param applicationId
     */
    public void setOfferAcceptance(String applicationId) {
        unicaAcceptedOffers.add(applicationId);
    }

    /**
     * @return the blocked
     */
    public boolean isBlocked() {
        return blocked;
    }

    /**
     * @param blocked
     *            the blocked to set
     */
    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    /**
     * By Michael Ortiz - ET56883
     * 
     * This method will setup the OnOff eligible accounts list that will be sent to ECCS to fetch the account status.
     * The list will be stored in the OnOff model in UserSession and will be only used if an asynchronous call is made.
     * 
     */

    public void filterOnOffAccounts() {

        OnOffAccounts onOffAcct = new OnOffAccounts();
        onOffAcct.setFrontEndCards(new ArrayList<>());
        onOffAcct.setOnOffCardList(new ArrayList<>());
        onOffAcct.setTmpPlasticList(new HashMap<>());
        onOffAcct.setTvFrontEndCardPlasticMap(new HashMap<>());

        for (TVFrontendAccount acc : getAccounts()) {

            if (GenericValidator.isBlankOrNull(acc.getAccountNumber()))
                continue;

            if(StaticMessageSource.isFlagEnabled(StaticMessageSource.ATH_ONOFF) 
                    && acc.getSubtype().equals(OnOffEnums.IDA.getValue())) {

                onOffAcct = getOnOffIDAPlastics(onOffAcct, acc);
            }

            if(StaticMessageSource.isFlagEnabled(StaticMessageSource.CCA_ONOFF) 
                    && acc.getSubtype().equals(OnOffEnums.CCA.getValue())) {

                onOffAcct = getOnOffCCAPlastics(onOffAcct, acc);
            }

            if (StaticMessageSource.isFlagEnabled(StaticMessageSource.ON_OFF_ALERTS) 
                    && !onOffAcct.getOnOffCardList().isEmpty()) {
                setHasOnOffEligibleAccount(true);
            }

            acc.setOnOffPlasticList(onOffAcct.getFrontEndCards());

            onOffAcct.setFrontEndCards(new ArrayList<>());

        }

        onOffAcct.getTmpPlasticList().clear();
        setOnOffAccounts(onOffAcct);
    }

    public OnOffAccounts getOnOffIDAPlastics(OnOffAccounts onOffAcct, TVFrontendAccount acc) {

        for (TVFrontendAccount plastic : getCardPlasticAccounts()) {

            DepositAccount depAcc = (DepositAccount) plastic.getBackendObject();
            if (depAcc == null || depAcc.getRecords() == null || GenericValidator.isBlankOrNull(depAcc.getAccountID()))
                continue;

            TVFrontEndAccountCardPlastic accountCard = new TVFrontEndAccountCardPlastic();
            CardPlasticAccountRecord rec = (CardPlasticAccountRecord) depAcc.getRecords();

            if (rec.getAccount() == null || GenericValidator.isBlankOrNull(rec.getAccount().getAccountID()))
                continue;

            String cardAccountNumber = rec.getAccount().getAccountID();
            String carPANId = depAcc.getAccountID();
            String cardStatus = rec.getStatus();

            if (cardAccountNumber.equalsIgnoreCase(acc.getAccountNumber()) 
                    && OnOffUtils.isCardStatusValid(cardStatus, getAllowedAthCardStatuses()) 
                    && OnOffUtils.containsCardBin(carPANId, getAllowedOnOffAthBins())) {

                acc.setElegibleForOnOff(true);
                rec.setCardPANId(carPANId);

                DisposableIdentifier id = new DisposableIdentifier();
                CardPlasticFrontend recFrontEnd = new CardPlasticFrontend(rec);
                recFrontEnd.setFrontEndId(id.getNextDisposableId());
                recFrontEnd.setPlasticType(Utils.getPlasticType(rec.getCardPANId(), acc.getAccountProductId()));

                PlasticOnOffModel onOffModel = OnOffUtils.createOnOffModel(recFrontEnd);

                //Validate if the account is already in map, if so, add the other product of the same account to the list.
                if (onOffAcct.getTvFrontEndCardPlasticMap().containsKey(acc.getAccountNumber())) {
                    TVFrontEndAccountCardPlastic endCardPlastic = onOffAcct.getTvFrontEndCardPlasticMap().get(acc.getAccountNumber());
                    endCardPlastic.getAccount().add(acc);
                } else {
                    List<TVFrontendAccount> accs = new LinkedList<>();
                    accs.add(acc);

                    accountCard.setAccount(accs);
                    accountCard.setCard(recFrontEnd);
                    onOffAcct.getTvFrontEndCardPlasticMap().put(acc.getAccountNumber(), accountCard);
                }

                onOffAcct.getFrontEndCards().add(recFrontEnd);

                if (!onOffAcct.getTmpPlasticList().containsKey(carPANId)) {
                    onOffAcct.getOnOffCardList().add(onOffModel);
                    onOffAcct.getTmpPlasticList().put(carPANId, carPANId);

                }
            }
        }

        return onOffAcct;
    }

    public OnOffAccounts getOnOffCCAPlastics(OnOffAccounts onOffAcct, TVFrontendAccount acc) {

        /* Getting Credit Card Account */
        CardPlasticFrontend[] ccaPlastics = Utils.getCCACardPlastics(getCustomerService(), getCustomerProfile(), getBankingSession(), acc);

        for (CardPlasticFrontend plastic : ccaPlastics) {

            CardPlasticAccountRecord rec =  plastic.getCardPlasticAccountRecord();

            if (plastic == null || rec == null || GenericValidator.isBlankOrNull(rec.getAccount().getAccountID()))
                continue;

            TVFrontEndAccountCardPlastic accountCard = new TVFrontEndAccountCardPlastic();

            if (rec.getAccount() == null || GenericValidator.isBlankOrNull(rec.getAccount().getAccountID()))
                continue;

            String cardAccountNumber = rec.getAccount().getAccountID();
            String carPANId = rec.getCardPANId();
            // TODO Check why record does not contain status. Bypassing for now.
            //String cardStatus = rec.getStatus();

            if (cardAccountNumber.equalsIgnoreCase(acc.getAccountNumber())
                    && !OnOffUtils.containsCardBin(cardAccountNumber, getExcludeOnOffCcaBins()) // Exclude main account bin's
                    && !OnOffUtils.containsCardBin(carPANId, getExcludeOnOffCcaBins())) { // Exclude main account sub bin's

                acc.setElegibleForOnOff(true);
                rec.setCardPANId(carPANId);

                DisposableIdentifier id = new DisposableIdentifier();
                CardPlasticFrontend recFrontEnd = new CardPlasticFrontend(rec);
                recFrontEnd.setFrontEndId(id.getNextDisposableId());
                recFrontEnd.setPlasticType(Utils.getPlasticType(rec.getCardPANId(), acc.getAccountProductId()));

                PlasticOnOffModel onOffModel = OnOffUtils.createOnOffModel(recFrontEnd);

                //Validate if the account is already in map, if so, add the other product of the same account to the list.
                if (onOffAcct.getTvFrontEndCardPlasticMap().containsKey(acc.getAccountNumber())) {
                    TVFrontEndAccountCardPlastic endCardPlastic = onOffAcct.getTvFrontEndCardPlasticMap().get(acc.getAccountNumber());
                    endCardPlastic.getAccount().add(acc);
                } else {
                    List<TVFrontendAccount> accs = new LinkedList<>();
                    accs.add(acc);

                    accountCard.setAccount(accs);
                    accountCard.setCard(recFrontEnd);
                    onOffAcct.getTvFrontEndCardPlasticMap().put(acc.getAccountNumber(), accountCard);
                }

                onOffAcct.getFrontEndCards().add(recFrontEnd);

                if (!onOffAcct.getTmpPlasticList().containsKey(carPANId)) {
                    onOffAcct.getOnOffCardList().add(onOffModel);
                    onOffAcct.getTmpPlasticList().put(carPANId, carPANId);
                }
            }
        }

        return onOffAcct;

    }

    public HashMap<String, Integer> getPinRequestAttempts() {
        return pinRequestAttempts;
    }

    public void setPinRequestAttempts(HashMap<String, Integer> pinRequestAttempts) {
        this.pinRequestAttempts = pinRequestAttempts;
    }

    public Boolean geteSignLookUp() {
        return eSignLookUp;
    }

    public void seteSignLookUp(Boolean eSignLookUp) {
        this.eSignLookUp = eSignLookUp;
    }

    public UserAgentType getUserAgentType() {
        return userAgentType;
    }

    public void setUserAgentType(UserAgentType userAgentType) {
        this.userAgentType = userAgentType;
    }

    public Boolean getPortalAnalytics() {
        return portalAnalytics;
    }

    public void setPortalAnalytics(Boolean portalAnalytics) {
        this.portalAnalytics = portalAnalytics;
    }

    public Boolean getOobEnroll() {
        return oobEnroll;
    }

    public void setOobEnroll(Boolean oobEnroll) {
        this.oobEnroll = oobEnroll;
    }

    public List<AccountPDFStmtInfo> getAccountsStatements(HttpServletRequest request) {

        if (this.accountsStatements == null) {
            this.accountsStatements = new LinkedList<AccountPDFStmtInfo>();
            List<TVFrontendAccount> accounts = new LinkedList<TVFrontendAccount>();
            if (closedAccounts != null)
                accounts.addAll(closedAccounts);
            if (controlHAccounts != null) {
                accounts.addAll(controlHAccounts);
            }
            accounts.addAll(this.getAccounts());
            accounts = Utils.filteredEbillsAccounts(accounts);
            for (Iterator<TVFrontendAccount> iterator2 = accounts.iterator(); iterator2.hasNext();) {
                TVFrontendAccount account = (TVFrontendAccount) iterator2.next();
                if (account.getSubtype().equals("IDA")) {
                    List<AbstractStatement> statements = Utils.statementsByAccount(account, this, request);
                    if (statements != null) {
                        for (Iterator<AbstractStatement> iterator3 = statements.iterator(); iterator3.hasNext();) {
                            AbstractStatement statement = (AbstractStatement) iterator3.next();
                            if (statement.getFrontEndId().equalsIgnoreCase("1")) {
                                continue;
                            }
                            if (GenericValidator.isBlankOrNull(statement.getPdfID())) {
                                continue;// Remove PDF ID NULL
                            }
                            AccountPDFStmtInfo info = new AccountPDFStmtInfo();
                            info = Utils.abstractToPdfStmt(statement, account);
                            info.setId(String.valueOf(account.hashCode()));
                            // Remove from greenbox if is a closed account
                            if (closedAccounts != null && closedAccounts.contains(account))
                                info.setViewed(true);
                            if (controlHAccounts != null && controlHAccounts.contains(account))
                                info.setViewed(true);
                            accountsStatements.add(info);
                        }
                    }
                }
            }

        }
        return accountsStatements;
    }

    public List<AccountPDFStmtInfo> getAccountsCurrentStatements(HttpServletRequest request) {
        if (this.accountsCurrentStatements == null) {
            this.accountsCurrentStatements = new LinkedList<AccountPDFStmtInfo>();
            List<TVFrontendAccount> accounts = new LinkedList<TVFrontendAccount>();
            if (closedAccounts != null)
                accounts.addAll(closedAccounts);
            if (controlHAccounts != null) {
                accounts.addAll(controlHAccounts);
            }
            accounts.addAll(this.getAccounts());
            accounts = Utils.filteredEbillsAccounts(accounts);
            for (Iterator<TVFrontendAccount> iterator2 = accounts.iterator(); iterator2.hasNext();) {
                TVFrontendAccount account = (TVFrontendAccount) iterator2.next();
                if (account.getSubtype().equals("IDA")) {
                    List<AbstractStatement> statements = Utils.statementsByAccount(account, this, request);
                    if (statements != null) {
                        for (Iterator<AbstractStatement> iterator3 = statements.iterator(); iterator3.hasNext();) {
                            AbstractStatement statement = (AbstractStatement) iterator3.next();
                            if (statement.getFrontEndId().equalsIgnoreCase("1")) {
                                continue;
                            }
                            if (GenericValidator.isBlankOrNull(statement.getPdfID())) {
                                continue;// Remove PDF ID NULL
                            }
                            AccountPDFStmtInfo info = new AccountPDFStmtInfo();
                            info = Utils.abstractToPdfStmt(statement, account);
                            info.setId(String.valueOf(account.hashCode()));
                            // Remove from greenbox if is a closed account
                            if (closedAccounts != null && closedAccounts.contains(account))
                                info.setViewed(true);
                            if (controlHAccounts != null && controlHAccounts.contains(account))
                                info.setViewed(true);
                            accountsCurrentStatements.add(info);
                            break;// Only load last statement
                        }
                    }
                }
            }

        }
        return accountsCurrentStatements;
    }

    public void setAccountsCurrentStatements(List<AccountPDFStmtInfo> accountCurrentStatements) {
        this.accountsCurrentStatements = accountCurrentStatements;
    }

    public String getIraProductIds() {
        return iraProductIds;
    }

    public void setIraProductIds(String iraProductIds) {
        this.iraProductIds = iraProductIds;
    }

    public CampaignGenericBean getCampaignGenericBean() {
        return campaignGenericBean;
    }

    public void setCampaignGenericBean(CampaignGenericBean campaignGenericBean) {
        this.campaignGenericBean = campaignGenericBean;
    }

    public List<TVFrontendAccount> getClosedAccounts() {
        if(closedAccounts == null) {
            closedAccounts = new ArrayList<>();
        }
        return closedAccounts;
    }

    public void setClosedAccounts(List<TVFrontendAccount> closedAccounts) {
        this.closedAccounts = closedAccounts;
    }

    public List<TVFrontendAccount> getControlHAccounts() {
        if(controlHAccounts == null)
            controlHAccounts =  new LinkedList<>();
        return controlHAccounts;
    }

    public void setControlHAccounts(List<TVFrontendAccount> controlHAccounts) {
        this.controlHAccounts = controlHAccounts;
    }

    public void resetAccountsCurrentStatements() {
        accountsCurrentStatements = null;
    }

    public Boolean getRenewPermId() {
        return renewPermId;
    }

    public void setRenewPermId(Boolean renewPermId) {
        this.renewPermId = renewPermId;
    }

    public Boolean getActivateEAccounts() {
        return activateEAccounts;
    }

    public void setActivateEAccounts(Boolean activateEAccounts) {
        this.activateEAccounts = activateEAccounts;
    }

    public List<TVFrontendDepositAccount> getIdaAccounts() {
        if (this.idaAccounts == null) {
            List<TVFrontendDepositAccount> idaAccountsRecs = new LinkedList<TVFrontendDepositAccount>();
            List<TVFrontendAccount> userAccounts = this.getAccounts();
            for (Iterator<TVFrontendAccount> iterator = userAccounts.iterator(); iterator.hasNext();) {
                TVFrontendAccount account = (TVFrontendAccount) iterator.next();
                if (account.getSubtype().equals("IDA")) {
                    TVFrontendDepositAccount idaAccount = new TVFrontendDepositAccount(account.getTVAccount());
                    customerService.inquiryIdaAccount(idaAccount);
                    idaAccount.setFrontEndId(account.getFrontEndId());
                    idaAccountsRecs.add(idaAccount);
                    account.setStmtDisposition(idaAccount.getStmtDisposition());
                }
            }
            this.idaAccounts = idaAccountsRecs;
        }
        return this.idaAccounts;
    }

    public void setIdaAccounts(List<TVFrontendDepositAccount> idaAccounts) {
        this.idaAccounts = idaAccounts;
    }

    public void resetIdaAccount() {
        idaAccounts = null;
    }

    public String getMobileCashTransaction() {
        return mobileCashTransaction;
    }

    public void setMobileCashTransaction(String mobileCashTransaction) {
        this.mobileCashTransaction = mobileCashTransaction;
    }

    public List<MobileCashTransaction> getMobileCashTransactions() {
        if (!StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_DROP)) {
            if (mobileCashTransactions == null) {
                mobileCashTransactions = new LinkedList<MobileCashTransaction>();
            }
        }
        return mobileCashTransactions;
    }

    public void setMobileCashTransactions(List<MobileCashTransaction> mobileCashTransactions) {
        this.mobileCashTransactions = mobileCashTransactions;
    }

    public List<MobileCashTransaction> getMobileCashHistory() {
        return mobileCashHistory;
    }

    public void setMobileCashHistory(List<MobileCashTransaction> mobileCashHistory) {
        this.mobileCashHistory = mobileCashHistory;
    }

    public List<TVFrontendAccount> getCcaAvailbleToEbill() {
        if (ccaAvailbleToEbill == null) {
            ccaAvailbleToEbill = new LinkedList<TVFrontendAccount>();
            List<TVFrontendAccount> checkAcc = new ArrayList<>();
            checkAcc.addAll(getAccounts());
            checkAcc.addAll(getClosedAccounts());
            checkAcc.addAll(getControlHAccounts());
            for (TVFrontendAccount acc : checkAcc) {
                if (acc.getSubtype() != null && (acc.getSubtype().equals("CCA") || acc.getSubtype().equals("AMEX"))) {
                    //if (acc.getIsAvailableElecStmt()) {
                    ccaAvailbleToEbill.add(acc);
                    //}Remove filter to show pdf and greenbox to all cca account.
                }
            }
        }
        return ccaAvailbleToEbill;
    }

    public void setCcaAvailbleToEbill(List<TVFrontendAccount> ccaAvailbleToEbill) {
        this.ccaAvailbleToEbill = ccaAvailbleToEbill;
    }

    public List<AccountStmtInfo> getCcaEbills(HttpServletRequest request) {
        if (this.ccaEbills == null) {
            ccaEbills = new ArrayList<AccountStmtInfo>();
            for (TVFrontendAccount cca : this.getCcaAvailbleToEbill()) {
                AccountStmtInfo accStmtInfo = new AccountStmtInfo();
                accStmtInfo.setAccountLabel(cca.getNickname());
                accStmtInfo.setAccountNumber(cca.getAccountNumber());
                // accStmtInfo.setActive(cca.getStmtDisposition());

                // TODO complete object

                List<AbstractStatement> statements = Utils.statementsByAccount(cca, this, request);
                List<AccountPDFStmtInfo> accountStatements = new LinkedList<AccountPDFStmtInfo>();
                if (statements != null) {
                    for (Iterator<AbstractStatement> iterator = statements.iterator(); iterator.hasNext();) {
                        AbstractStatement statement = (AbstractStatement) iterator.next();
                        if (GenericValidator.isBlankOrNull(statement.getPdfID())) {
                            continue;// Remove PDF ID NULL
                        }
                        AccountPDFStmtInfo pdfStmt = Utils.abstractToPdfStmt(statement, cca);
                        pdfStmt.setAccountFrontendID(cca.getFrontEndId());
                        accountStatements.add(pdfStmt);
                    }

                }
                AbstractAccount response = customerService.getAccountDetails(getCustomerProfile(), getBankingSession(),
                        cca, true);
                CardAccount cuenta = (CardAccount) response.getBackendObject();
                CreditCardAccountRecord ccaRecord = (CreditCardAccountRecord) cuenta.getRecords();
                TVFrontendCreditAccount tvCca = (TVFrontendCreditAccount) response;
                if (ccaRecord != null) {
                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                    if (ccaRecord.getLastPaymentDate() != null) {
                        accStmtInfo.setLastPaymentDate(format.format(ccaRecord.getLastPaymentDate().getTime()));
                    }
                    if (ccaRecord.getMinimumPaymentAmount() == null)
                        ccaRecord.setMinimumPaymentAmount(new BigDecimal(0));
                    accStmtInfo.setMinimumAmountDue(ccaRecord.getMinimumPaymentAmount());
                    accStmtInfo.setLastPaymentAmount(ccaRecord.getLastPaymentAmount());
                    // accStmtInfo.setAmountDue(tvCca.getTotalAmountDue());
                    // to do validate which one stays
                    accStmtInfo.setAmountDue(tvCca.getCurrentBalance());
                    if (ccaRecord.getDueDate() != null) {
                        accStmtInfo.setDueDate(format.format(ccaRecord.getDueDate().getTime()));
                    }
                    if(ccaRecord.getLastStatementDate() != null){						
                        accStmtInfo.setLastStatementDate(format.format(ccaRecord.getLastStatementDate().getTime()));	
                    }else{
                        accStmtInfo.setLastStatementDate("");
                    }

                }
                accStmtInfo.setAccountStatements(accountStatements);
                accStmtInfo.setBillerAccountID(cca.hashCode());
                ccaEbills.add(accStmtInfo);
            }
        }
        return ccaEbills;
    }

    public void setCcaEbills(List<AccountStmtInfo> ccaEbills) {
        this.ccaEbills = ccaEbills;
    }

    public HashMap<String, AccountPDFStmtInfo> getPdfStatementAccount(List<AccountStmtInfo> ccaEbills,
            List<AccountPDFStmtInfo> accountsCurrentStatements) {
        List<AccountStmtInfo> ccaAccountsStatements = ccaEbills;
        List<AccountPDFStmtInfo> idaAccountStatment = accountsCurrentStatements;
        pdfStatementAccount = new HashMap<String, AccountPDFStmtInfo>();

        for (Iterator<AccountStmtInfo> iteratorAcc = ccaAccountsStatements.iterator(); iteratorAcc.hasNext();) {
            AccountStmtInfo ccaAccountStatements = (AccountStmtInfo) iteratorAcc.next();
            List<AccountPDFStmtInfo> ccaStatements = ccaAccountStatements.getAccountStatements();
            for (Iterator<AccountPDFStmtInfo> iteratorStmt = ccaStatements.iterator(); iteratorStmt.hasNext();) {
                AccountPDFStmtInfo ccaStmt = (AccountPDFStmtInfo) iteratorStmt.next();
                pdfStatementAccount.put(ccaStmt.getDocId(), ccaStmt);
            }
        }

        for (Iterator<AccountPDFStmtInfo> iteratorStmt = idaAccountStatment.iterator(); iteratorStmt.hasNext();) {
            AccountPDFStmtInfo idaStmt = (AccountPDFStmtInfo) iteratorStmt.next();
            pdfStatementAccount.put(idaStmt.getDocId(), idaStmt);
        }

        return pdfStatementAccount;
    }

    public void setPdfStatementAccount(HashMap<String, AccountPDFStmtInfo> pdfStatementAccount) {
        this.pdfStatementAccount = pdfStatementAccount;
    }

    public void resetCcaEbills() {
        this.ccaEbills = null;
    }

    public List<ASK_FOR_OOB> getTasksListOOB() {
        return tasksListOOB;
    }

    public void setTasksListOOB(List<ASK_FOR_OOB> tasksListOOB) {
        this.tasksListOOB = tasksListOOB;
    }

    public Boolean getOobChallenge() {
        return oobChallenge;
    }

    public void setOobChallenge(Boolean oobChallenge) {
        this.oobChallenge = oobChallenge;
    }

    public int getMaxStatements() {
        return maxStatements;
    }

    public void setMaxStatements(int maxStatements) {
        this.maxStatements = maxStatements;
    }

    public EaccountMessage getEaccountMessage() {
        return eaccountMessage;
    }

    public void setEaccountMessage(EaccountMessage eaccountMessage) {
        this.eaccountMessage = eaccountMessage;
    }

    public ApplicationStatus getPayrollAppStatus() {
    	if (statusPayrollApp == null) {
    		statusPayrollApp = Utils.getCustomerAppStatus(this.getCustomerApplications()).get("epayrollAppStatus");
    	}
        return statusPayrollApp;
    }

    public void setPayrollAppStatus(ApplicationStatus statusPayrollApp) {
        this.statusPayrollApp = statusPayrollApp;
    }

    public List<TVFrontendAccount> getPendingCloseAccounts() {
        return pendingCloseAccounts;
    }

    public boolean isEnrollAcctPendingClose() {
        return enrollAcctPendingClose;
    }

    public void setEnrollAcctPendingClose(boolean enrollAcctPendingClose) {
        this.enrollAcctPendingClose = enrollAcctPendingClose;
    }

    public List<TVFrontendAccount> getCcaAccounts() {
        return ccaAccounts;
    }

    public void setCcaAccounts(List<TVFrontendAccount> ccaAccounts) {
        this.ccaAccounts = ccaAccounts;
    }

    public void setOobAddEmployeeAut(boolean oobAddEmployeeAut) {
        this.oobAddEmployeeAut = oobAddEmployeeAut;
    }

    public boolean isAddEmployeeOobAut() {
        return oobAddEmployeeAut;
    }

    public Boolean getInterbankFundingAllowed() {
        return interbankFundingAllowed;
    }

    public void setInterbankFundingAllowed(boolean interbankFundingAllowed) {
        this.interbankFundingAllowed = interbankFundingAllowed;
    }

    // MBSFE-454
    public Integer getCodeCounter() {
        return codeCounter;
    }

    public void setCodeCounter(Integer codeCounter) {
        this.codeCounter = codeCounter;
    }
    // END MBSFE-454

    public boolean isQuestionAnswered() {
        return questionAnswered;
    }

    public void setQuestionAnswered(boolean questionAnswered) {
        this.questionAnswered = questionAnswered;
    }

    public boolean isElegibleToOutreach() {
        return elegibleToOutreach;
    }

    public void setElegibleToOutreach(boolean elegibleToOutreach) {
        this.elegibleToOutreach = elegibleToOutreach;
    }

    public boolean isAlreadyShownOutreach() {
        return alreadyShownOutreach;
    }

    public void setAlreadyShownOutreach(boolean alreadyShownOutreach) {
        this.alreadyShownOutreach = alreadyShownOutreach;
    }

    public CustomerPhone getCustomerPhone() {

        if (customerPhone == null || GenericValidator.isBlankOrNull(customerPhone.getPhoneNumber())) {
            customerPhone = new CustomerPhone();
            customerPhone.setProfileId(getCustomerProfile().getProfileId());

            /**
             * MBNA-2986
             * Method getCustomerPhone : CIBP-WEB microservice integration
             * 
             */
            customerPhone = CustomerFacade.getInstance().getCustomerPhone(customerPhone, this);

            if (customerPhone != null) {
                getCustomerProfile().setCustomerPhone(customerPhone);
            }
        }
        return customerPhone;
    }

    public void setEpayrollBulletNull(){
        ePayrollBullet = null;
    }

    public String hasePayrollBullet() throws Exception {

        if (ePayrollBullet == null) {
            ePayrollBullet = "F";
            if (accounts != null) {
                if (accounts.size() > 0) {
                    CustomerProfile profile = getCustomerProfile();
                    if (profile != null) {
                        CustomerEntitlement ent = getCustomerProfile().getEntitlementByType(EntitlementType.PAYROLL);
                        evertec.cibp.core.models.entitlement.PayrollEntitlement entt = null;
                        boolean isModify = false;
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        boolean hasBsmart = false;
                        if (profile.getUserInterface() == UserInterface.COMMERCIAL) {
                            if (ent == null) {
                                hasBsmart = profile.getEnrollmentAccountProductId().equalsIgnoreCase("086");
                                Date date = new Date();
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(date); // Configuramos la fecha
                                // que se recibe
                                calendar.add(Calendar.DAY_OF_YEAR, 90); // numer
                                date = calendar.getTime();
                                entt = new PayrollEntitlement(String.valueOf(accounts.size()),
                                        String.valueOf(hasBsmart), PayrollStatus.ACEPT.toString(), format.format(date),
                                        EntitlementStatus.ENABLED);
                                isModify = true;
                                ePayrollBullet = "T";
                            } else {
                                entt = new PayrollEntitlement(ent);
                                if (!entt.getLimitedDate().equalsIgnoreCase("")) {
                                    Date date = format.parse(entt.getLimitedDate());
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.set(Calendar.HOUR, 0);
                                    calendar.set(Calendar.MINUTE, 0);
                                    calendar.set(Calendar.SECOND, 0);
                                    calendar.set(Calendar.MILLISECOND, 0);
                                    Date actualDate = calendar.getTime();
                                    if (date.after(actualDate) || date.equals(actualDate)) {
                                        // setear la variable
                                        // alert badge.
                                        ePayrollBullet = "T";
                                    }
                                }
                            }
                        }
                        if (isModify) {
                            getCustomerService().addEntitlement(profile, entt.getCustomerEntitlement());
                            setCustomerProfile(profile);
                        }
                    }
                }
            }
        }
        return ePayrollBullet;
    }

    public SMSCTN getAlertsPhoneNumber(HttpServletRequest request) {
        if (alertsPhoneNumber == null) {
            alertsPhoneNumber = new SMSCTN();
            SMSProfile smsProf = getSmsProfile(Utils.generateCustomerInteractionEvent(request));
            if (smsProf != null) {
                @SuppressWarnings("unchecked")
                Set<SMSCTN> phones = smsProf.getCtns();
                if (phones != null && phones.size() > 0) {
                    Iterator<SMSCTN> iter = phones.iterator();
                    while (iter.hasNext()) {

                        alertsPhoneNumber = iter.next();
                        break;
                    }
                }
            }
        }
        return alertsPhoneNumber;
    }

    public String getSmsGeneratedCode() {
        return smsGeneratedCode;
    }

    public void setSmsGeneratedCode(String smsGeneratedCode) {
        this.smsGeneratedCode = smsGeneratedCode;
    }

    /**
	 * @return the otpChallengeCode
	 */
	public String getOtpChallengeCode() {
		return otpChallengeCode;
	}

	/**
	 * @param otpChallengeCode the one time pin to set
	 */
	public void setOtpChallengeCode(String otpChallengeCode) {
		this.otpChallengeCode = otpChallengeCode;
	}
	
    /**
     * getOtpLastChallengeDate
     * @return otpLastChallengeDate
     */
    public Date getOtpLastChallengeDate() {
        return otpLastChallengeDate;
    }

    /**
     * setOtpLastChallengeDate
     * @param otpLastChallengeDate Date
     */
    public void setOtpLastChallengeDate(Date otpLastChallengeDate) {
        this.otpLastChallengeDate = otpLastChallengeDate;
    }
    
    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public List<CustomerDevice> getCustomerDevices() {
        if (customerDevices == null) {
            customerDevices = new LinkedList<CustomerDevice>();
            CustomerDeviceResponse resp = getCustomerService().getCustomerDevicesByProfileId(this.getCustomerProfile().getProfileId());
            if (resp != null && resp.getAllDevices() != null && resp.getAllDevices().size() > 0) {
                customerDevices = resp.getAllDevices();
            }
        }
        return customerDevices;
    }

    public void setCustomerDevices(List<CustomerDevice> customerDevices) {
        this.customerDevices = customerDevices;
    }

    public String getFingerprintId() {
        return fingerprintId;
    }

    public void setFingerprintId(String fingerprintId) {
        this.fingerprintId = fingerprintId;
    }

    public MobileCashServices getMobileCashService() {
        return mobileCashService;
    }

    public void setMobileCashService(MobileCashServices mobileCashService) {
        this.mobileCashService = mobileCashService;
    }

    public boolean isEasyOfacMatch() {
        return easyOfacMatch;
    }

    public void setEasyOfacMatch(boolean easyOfacMatch) {
        this.easyOfacMatch = easyOfacMatch;
    }

    public boolean isNewOfacCustomer() {
        return newOfacCustomer;
    }

    public void setNewOfacCustomer(boolean newOfacCustomer) {
        this.newOfacCustomer = newOfacCustomer;
    }

    public String getCommonGlobalPayeesTree() {
        return commonGlobalPayeesTree;
    }

    public void setCommonGlobalPayeesTree(String commonGlobalPayeesTree) {
        this.commonGlobalPayeesTree = commonGlobalPayeesTree;
    }

    public String getOutReachConfirmation() {
        return outReachConfirmation;
    }

    public void setOutReachConfirmation(String outReachConfirmation) {
        this.outReachConfirmation = outReachConfirmation;
    }

    public String getCountry() {
        if (country == null) {
            if (this.getBankingSession() != null && this.getBankingSession().getPermId() != null) {				
                CISOpacCustomerInfoRequest CISrequest = new CISOpacCustomerInfoRequest();
                CISrequest.setCustomerPermId(this.getBankingSession().getPermId().toString());
                CISOpacCustomerInfoResponse cisResponse = this.getCustomerService().getCISCustomerInfo(CISrequest);
                country = (cisResponse == null) ? StringUtils.EMPTY : cisResponse.getCountry();
            } else {
                country = StringUtils.EMPTY;
            }
        }
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return Returns a set of enrollable non transactional accounts.
     * @author Hector Ortiz ET56802
     */
    public Set<String> getNonTransEnrollAccsSet() {
        return nonTransEnrollAccsSet;
    }

    /**
     * Takes a csv of non transactional accounts an places them in a set.
     * @param nonTransEnrollAccs csv string with the enrollable non transactional accounts.
     * @author Hector Ortiz ET56802
     */
    public void setNonTransEnrollAccsSet(String nonTransEnrollAccs) {
        if(nonTransEnrollAccsSet == null){
            nonTransEnrollAccsSet = new HashSet<>();
        }
        else{
            nonTransEnrollAccsSet.clear();
        }

        String[] nonTransEnrollAccsArr = nonTransEnrollAccs.split(",");

        for(int i = 0; i < nonTransEnrollAccsArr.length; ++i){
            this.nonTransEnrollAccsSet.add(nonTransEnrollAccsArr[i]); 
        }
    }

    public Boolean getPendingResetPayerees() {
        return pendingResetPayerees;
    }

    public void setPendingResetPayerees(Boolean pendingResetPayerees) {
        this.pendingResetPayerees = pendingResetPayerees;
    }

    public Boolean getCheckedOobEnroll() {
        return checkedOobEnroll;
    }

    public void setCheckedOobEnroll(Boolean checkedOobEnroll) {
        this.checkedOobEnroll = checkedOobEnroll;
    }

    public List<CustomerDocumentsPackage> getDisbursementsList() {
        return this.disbursementsList;
    }

    public RestClient getRestClientResourceOwner() {
        return restClientResourceOwner;
    }

    public void setRestClientResourceOwner(RestClient restClientResourceOwner) {
        this.restClientResourceOwner = restClientResourceOwner;
    }

    public Boolean getLogEbppStatus() {
        return logEbppStatus;
    }

    public void setLogEbppStatus(Boolean logEbppStatus) {
        this.logEbppStatus = logEbppStatus;
    }

    public boolean isOobLogin() {
        return oobLogin;
    }

    public void setOobLogin(boolean oobLogin) {
        this.oobLogin = oobLogin;
    }

    /**
     * isOobRedirectToLogin
     * @return true o false
     */
    public boolean isOobRedirectToLogin() {
		return oobRedirectToLogin;
	}

    /**
     * setOobRedirectToLogin
     * @param oobRedirectToLogin true o false
     */
	public void setOobRedirectToLogin(boolean oobRedirectToLogin) {
		this.oobRedirectToLogin = oobRedirectToLogin;
	}

	/**
     * isOobRedirectToBlock
     * @return true o false
     */
	public boolean isOobRedirectToBlock() {
		return oobRedirectToBlock;
	}

	/**
     * setOobRedirectToBlock
     * @param oobRedirectToBlock true o false
     */
	public void setOobRedirectToBlock(boolean oobRedirectToBlock) {
		this.oobRedirectToBlock = oobRedirectToBlock;
	}

    /**
     * MBSFE-665 INI
     */
    public List<MassCompromiseFileAccounts> getMassCompromiseFileAccounts() {
        return massCompromiseFileAccounts;
    }

    public void setMassCompromiseFileAccounts(List<MassCompromiseFileAccounts> massCompromiseFileAccounts) {
        this.massCompromiseFileAccounts = massCompromiseFileAccounts;
    }
    /**
     * MBSFE-665 FIN
     */


    public void setFavoritePayments(List<TVAccountFavoritePayment> favoritePayments) {
        this.favoritePayments = favoritePayments;
    }



    public ApplicationStatus getFraudPrevAppStatus() {
        return statusFraudPrevApp;
    }

    public void setFraudPrevAppStatus(ApplicationStatus statusFraudPrevApp) {
        this.statusFraudPrevApp = statusFraudPrevApp;
    }

    public List<AccountStmtInfo> getCcaPortalEbills(HttpServletRequest request) {
        if (this.ccaPortalEbills == null) {
            ccaPortalEbills = new ArrayList<>();
            for (TVFrontendAccount cca : this.getCcaAvailbleToEbill()) {
                AccountStmtInfo accStmtInfo = new AccountStmtInfo();
                accStmtInfo.setAccountLabel(cca.getNickname());
                accStmtInfo.setAccountNumber(cca.getAccountNumber());
                List<AbstractStatement> statements = Utils.statementsByAccount(cca, this, request);
                List<AccountPDFStmtInfo> accountStatements = new LinkedList<>();
                if (statements != null) {
                    for (Iterator<AbstractStatement> iterator = statements.iterator(); iterator.hasNext();) {
                        AbstractStatement statement =  iterator.next();
                        if (GenericValidator.isBlankOrNull(statement.getPdfID())) {
                            continue;// Remove PDF ID NULL
                        }
                        AccountPDFStmtInfo pdfStmt = Utils.abstractToPdfStmt(statement, cca);
                        pdfStmt.setAccountFrontendID(cca.getFrontEndId());
                        accountStatements.add(pdfStmt);
                    }
                }
                accStmtInfo.setAccountStatements(accountStatements);
                accStmtInfo.setBillerAccountID(cca.hashCode());
                ccaPortalEbills.add(accStmtInfo);
            }
        }
        return ccaPortalEbills;	
    }

    public void setCcaPortalEbills(List<AccountStmtInfo> ccaPortalEbills) {
        this.ccaPortalEbills = ccaPortalEbills;
    }

    public CurrentStatementsRs_Type getCurrentVaultStatements() {
        if(currentVaultStatements == null) {
            currentVaultStatements = customerService.getCurrentStatements(this.getBankingSession(),"Electronic");			
        }
        return currentVaultStatements;
    }

    public void setCurrentVaultStatements(CurrentStatementsRs_Type currentVaultStatements) {
        this.currentVaultStatements = currentVaultStatements;
    }

    public List<CreditCardStatement_Type> getCcaCurrentStatements() {

        if(ccaCurrentStatements == null) {
            CurrentStatementsRs_Type resp = this.getCurrentVaultStatements();
            if(resp.getStatus() != null && resp.getStatus().getStatusCode() == 0 && resp.getCurrentStatements() != null) {
                CurrentStatements_Type statements = resp.getCurrentStatements();
                CreditCardStatement_Type[] ccaStatements = statements.getCreditCardStatements();
                if(ccaStatements != null) {
                    ccaCurrentStatements = new ArrayList<>(Arrays.asList(ccaStatements));
                }					
            }
        }
        return ccaCurrentStatements;
    }

    public void setCcaCurrentStatements(List<CreditCardStatement_Type> ccaCurrentStatements) {
        this.ccaCurrentStatements = ccaCurrentStatements;
    }

    public List<DepositStatement_Type> getDdaCurrentStatements() {
        if(ddaCurrentStatements == null) {
            CurrentStatementsRs_Type resp = this.getCurrentVaultStatements();
            if(resp.getStatus().getStatusCode() == 0 && resp.getCurrentStatements() != null) {
                CurrentStatements_Type statements = resp.getCurrentStatements();
                DepositStatement_Type[] ccaStatements = statements.getDepositStatements();
                if(ccaStatements != null) {
                    ddaCurrentStatements = new ArrayList<>(Arrays.asList(ccaStatements));
                }					
            }
        }
        return ddaCurrentStatements;
    }

    public void setDdaCurrentStatements(List<DepositStatement_Type> ddaCurrentStatements) {
        this.ddaCurrentStatements = ddaCurrentStatements;
    }

    public CurrentStatementsRs_Type getHistoryVaultStatements() {
        if(historyVaultStatements == null) {
            historyVaultStatements = customerService.getCurrentStatements(this.getBankingSession(),"Electronic",null,true);
        }
        return historyVaultStatements;
    }

    public void setHistoryVaultStatements(CurrentStatementsRs_Type historyVaultStatements) {
        this.historyVaultStatements = historyVaultStatements;
    }

    public List<CreditCardStatement_Type> getCcaHistoryStatements() {
        if(ccaHistoryStatements == null) {
            CurrentStatementsRs_Type resp = this.getHistoryVaultStatements();
            if(resp != null && resp.getStatus().getStatusCode() == 0 && resp.getCurrentStatements() != null) {
                CurrentStatements_Type statements = resp.getCurrentStatements();
                CreditCardStatement_Type[] ccaStatements = statements.getCreditCardStatements();
                if(ccaStatements != null) {
                    ccaHistoryStatements = new ArrayList<>(Arrays.asList(ccaStatements));
                }		
            }
        }
        return ccaHistoryStatements;
    }

    public void setCcaHistoryStatements(List<CreditCardStatement_Type> ccaHistoryStatements) {		
        this.ccaHistoryStatements = ccaHistoryStatements;
    }

    public List<DepositStatement_Type> getDdaHistoryStatements() {
        if(ddaHistoryStatements == null) {
            CurrentStatementsRs_Type resp = this.getHistoryVaultStatements();
            if(resp != null && resp.getStatus().getStatusCode() == 0 && resp.getCurrentStatements() != null) {
                CurrentStatements_Type statements = resp.getCurrentStatements();
                DepositStatement_Type[] ccaStatements = statements.getDepositStatements();
                if(ccaStatements != null) {
                    ddaHistoryStatements = new ArrayList<>(Arrays.asList(ccaStatements));
                }					
            }
        }
        return ddaHistoryStatements;
    }

    public void setDdaHistoryStatements(List<DepositStatement_Type> ddaHistoryStatements) {
        this.ddaHistoryStatements = ddaHistoryStatements;
    }

    /**
     * MBNA-5900
     * Method getCustomerApplications : CIBP-WEB microservice integration
     * @param refresh refresh customer applications from database
     * @return the customer applications
     */
    public List<CustomerApplication> getCustomerApplications(boolean refresh) {
        if (customerApplications == null || refresh)
            customerApplications = customerService.getCustomerApplications(customerProfile);

        return customerApplications;
    }

    public List<CustomerApplication> getCustomerApplications() {
        return this.getCustomerApplications(false);
    }

    public Map<Integer, ProfileTypeSection> getProfileTypeSections() {
        if (this.profileTypeSections == null)
            profileTypeSections = new HashMap<>();

        return this.profileTypeSections;
    }

    public void setSectionPath(Map<Integer, String> sectionsPath) {
        this.sectionsPath = sectionsPath;		
    }

    public Map<Integer, String> getSectionPath() {
        if (this.sectionsPath == null)
            sectionsPath = new HashMap<>();
        return sectionsPath;
    }

    public HashMap<String, List<List<AccountStatement>>> getAccountsChargesStatements() {
        return accountsChargesStatements;
    }

    public void setAccountsChargesStatements(HashMap<String, List<List<AccountStatement>>> accountsChargesStatements) {
        this.accountsChargesStatements = accountsChargesStatements;
    }

    public List<CustomerMessageContent> getMessagesList() { 
        return messagesList;
    }
    
    public void setMessagesList(List<CustomerMessageContent> messagesList) {
        this.messagesList = messagesList;
    }

    public List<AccountApplication> getCompletedApplications() {
        return completedApplications;
    }

    public void setCompletedApplications(List<AccountApplication> completedApplications) {
        this.completedApplications = completedApplications;
    }

    public boolean isShowAlertText() {
        return showAlertText;
    }

    public void setShowAlertText(boolean showAlertText) {
        this.showAlertText = showAlertText;
    }

    public OnOffAccounts getOnOffAccounts() {
        return onOffAccounts;
    }

    public void setOnOffAccounts(OnOffAccounts onOffAccounts) {
        this.onOffAccounts = onOffAccounts;
    }

    public boolean isHasOnOffEligibleAccount() {
        return hasOnOffEligibleAccount;
    }

    public void setHasOnOffEligibleAccount(boolean hasOnOffEligibleAccount) {
        this.hasOnOffEligibleAccount = hasOnOffEligibleAccount;
    }

    public String getPaymentFrequencyCode(String frequency) {
        String freq = frequency.toUpperCase();
        if(paymentFrequencyMap == null) {
            paymentFrequencyMap = new HashMap<>();
            paymentFrequencyMap.put("WEEKLY", "WW");
            paymentFrequencyMap.put("BIWEEKLY", "BW");
            paymentFrequencyMap.put("QUARTERLY", "QQ");
            paymentFrequencyMap.put("QUATERLY", "QQ");
            paymentFrequencyMap.put("SEMIANNUALLY", "SA");
            paymentFrequencyMap.put("MONTHLY", "MM");
            paymentFrequencyMap.put("ANNUALLY", "AA");
        }

        if(paymentFrequencyMap.containsKey(freq)) {
            return paymentFrequencyMap.get(freq);
        }
        return "";

    }

    public String getEpayrollId() {
        return epayrollId;
    }

    public void setEpayrollId(String epayrollId) {
        this.epayrollId = epayrollId;
    }

    public String getEvendorId() {
        return evendorId;
    }

    public void setEvendorId(String evendorId) {
        this.evendorId = evendorId;
    }

    public String[] getAllowedAthCardStatuses() {
        return allowedAthCardStatuses;
    }

    public void setAllowedAthCardStatuses(String[] allowedAthCardStatuses) {
        this.allowedAthCardStatuses = allowedAthCardStatuses;
    }

    public String[] getAllowedOnOffAthBins() {
        return allowedOnOffAthBins;
    }

    public void setAllowedOnOffAthBins(String[] allowedOnOffAthBins) {
        this.allowedOnOffAthBins = allowedOnOffAthBins;
    }

    public String[] getExcludeOnOffCcaBins() {
        return excludeOnOffCcaBins;
    }

    public void setExcludeOnOffCcaBins(String[] excludeOnOffCcaBins) {
        this.excludeOnOffCcaBins = excludeOnOffCcaBins;
    }

    public Boolean getNewNotifications() {
        return newNotifications;
    }

    public void setNewNotifications(Boolean newNotifications) {
        this.newNotifications = newNotifications;
    }

    public CustomerViewedNotification getViewedNotification() {
        return viewedNotification;
    }

    public void setViewedNotification(CustomerViewedNotification viewedNotification) {
        this.viewedNotification = viewedNotification;
    }

    public Boolean getForeignCustomer() {
        return foreignCustomer;
    }

    public void setForeignCustomer(Boolean foreignCustomer) {
        this.foreignCustomer = foreignCustomer;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

	 /**
     * @return retirementPlan response variable
     */
	public RetirementPlanBalanceResponse getRetirementPlan() {
		return retirementPlan;
	}

	public void setRetirementPlan(RetirementPlanBalanceResponse retirementPlan) {
		this.retirementPlan = retirementPlan;
	}
	
	 /**
     * @return hasRetirementPlan boolean variable
     */
	public boolean isHasRetirementPlan() {
		return hasRetirementPlan;
	}

	public void setHasRetirementPlan(boolean hasRetirementPlan) {
		this.hasRetirementPlan = hasRetirementPlan;
	}

	/** @return updateLoginCount */
	public boolean isUpdateLoginCount() {
		return updateLoginCount;
	}

	/**
	 * setUpdateLoginCount
	 * @param updateLoginCount
	 */
	public void setUpdateLoginCount(boolean updateLoginCount) {
		this.updateLoginCount = updateLoginCount;
	}

	/**
	 * @return the removeAccountStatement
	 */
	public boolean isRemoveAccountStatement() {
		return removeAccountStatement;
	}

	/**
	 * setRemoveAccountStatement
	 * @param removeAccountStatement
	 */
	public void setRemoveAccountStatement(boolean removeAccountStatement) {
		this.removeAccountStatement = removeAccountStatement;
	}
	
    /**
     * getAccounts
     * @return TVFrontendAccount
     */
    public List<TVFrontendAccount> getPremiaAccounts() {

        if (premiaAccounts == null || premiaAccounts.isEmpty()) {
            delay = false;
            if (customerProfile != null && bankingSession != null) {
                VaultAccountsRequest vaultRequest = new VaultAccountsRequest();
                vaultRequest.setUserSession(this);
                premiaAccounts = VaultAccountsFacade.getInstance().getAccountsFilteredByProductsId(vaultRequest, customerService.getPremiaProducts());
                premiaAccounts = PremiaUtil.filterPremiaAccounts(premiaAccounts, customerService.getPremiaGhostProductId());
                return premiaAccounts;
            } else {
                throw new ProgrammingError("UserSession cannot exist without a CustomerProfile");
            }
        } else {
            return premiaAccounts;
        }
    }
    
    public String getMobileSdkData () {
    	String mobileSdkData = "";
		
		if (getDeviceRequest().getDeviceIdentifier() != null && getDeviceRequest().getDeviceIdentifier().length > 0) {
			MobileDevice mobileDevice = KiuwanUtils.cast(MobileDevice.class, getDeviceRequest().getDeviceIdentifier()[0]);
			mobileSdkData = mobileDevice.getMobileSdkData();
		}
		
		return mobileSdkData;
    }

    
    /**
     * updateGetAccounts
     * @return TVFrontendAccount
     */
    public List<TVFrontendAccount> getFilteredAccounts(List<String> lstProducsId) {
        return customerService.getFilteredAccountsByProductId(customerProfile, bankingSession, lstProducsId);
    }

        /**
     * Get cookie preference status list
     * @author ET58344
     * @since 08-20-2020
     * @return List<CookiePreferenceStatus> result
     */
    public List<CookiePreferenceStatus> getCookiePreferenceStatusList() {
        return cookiePreferenceStatusList;
    }

    /**
     * Set cookie preference status lis
     * @author ET58344
     * @since 08-20-2020
     * @param cookiePreferenceStatusList value
     */
    public void setCookiePreferenceStatusList(List<CookiePreferenceStatus> cookiePreferenceStatusList) {
        this.cookiePreferenceStatusList = cookiePreferenceStatusList;
    }

    /**
     * Is cookie category enabled method
     *
     * @author ET58344
     * @since 08-21-2020
     * @param cookieCategory enum value
     * @return boolean result
     */
    public boolean isCookieCategoryEnabled (CookieCategory cookieCategory) {
        boolean result = false; //boolean result
        final String strEnabled = "ENABLED"; //enabled string

        if (getCookiePreferenceStatusList() != null) {

            for (CookiePreferenceStatus eachCategory : getCookiePreferenceStatusList()) {

                if (cookieCategory.toString().equals(eachCategory.getCategory())
                        && strEnabled.equals(eachCategory.getStatus())) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }
    
	/**
	 * @return the rewardPoints
	 */
	public boolean isRewardPoints() {
		return rewardPoints;
	}

	/**
	 * @param rewardPoints the rewardPoints to set
	 */
	public void setRewardPoints(boolean rewardPoints) {
		this.rewardPoints = rewardPoints;
	}

	/**
	 * @return the nonPremiaState
	 */
	public Boolean getNonPremiaState() {
		return nonPremiaState;
	}

	/**
	 * @param nonPremiaState the nonPremiaState to set
	 */
	public void setNonPremiaState(Boolean nonPremiaState) {
		this.nonPremiaState = nonPremiaState;
	}

	/**
	 * @return the listRewardPoints
	 */
	public List<TVFrontendAccount> getListRewardPoints() {
		return listRewardPoints;
	}

	/**
	 * @param listRewardPoints the listRewardPoints to set
	 */
	public void setListRewardPoints(List<TVFrontendAccount> listRewardPoints) {
		this.listRewardPoints = listRewardPoints;
	}

	/**
	 * @return the consultRewardsPoints
	 */
	public boolean isConsultRewardsPoints() {
		return consultRewardsPoints;
	}

	/**
	 * @param consultRewardsPoints the consultRewardsPoints to set
	 */
	public void setConsultRewardsPoints(boolean consultRewardsPoints) {
		this.consultRewardsPoints = consultRewardsPoints;
	}

	/**
	 * @return the custWealthPreferred
	 */	
	public CustomerWealthPreferred getCustWealthPreferred() {
		return custWealthPreferred;
	}

	/**
	 * @return the premiaAccountList
	 */	
	public List<TVFrontendAccount> getPremiaAccountList() {
		return premiaAccountList;
	}

	public String getAccountMLA() {
		return accountMLA;
	}

	public void setAccountMLA(String accountMLA) {
		this.accountMLA = accountMLA;
	}
	
	public CustomerProfileVault getCustomerProfileVault() {
		return customerProfileVault;
	}

	public void setCustomerProfileVault(CustomerProfileVault customerProfileVault) {
		this.customerProfileVault = customerProfileVault;
    }

	public boolean getIsLoginInterruptionEnable() {
		return isLoginInterruptionEnabled;
	}

	public void setIsLoginInterruptionEnabled(boolean isLoginInterruptionEnabled) {
		this.isLoginInterruptionEnabled = isLoginInterruptionEnabled;
	}

	/**
	 * @return the ccaCustomerIdentifierMap
	 */
	public HashMap<String, CustomerIdentifiers> getCcaCustomerIdentifierMap() {
		return ccaCustomerIdentifierMap;
	}

	/**
	 * @param cardNumber the CCA number
	 * @param custIdentifier the account id and customer id info from TSYS GeneralInformation
	 */	
	public void setCcaCustomerIdentifierInfo(String cardNumber, CustomerIdentifiers custIdentifier) {
		
		if (ccaCustomerIdentifierMap == null) {
			ccaCustomerIdentifierMap = new HashMap<String, CustomerIdentifiers>();
        }
		ccaCustomerIdentifierMap.put(cardNumber, custIdentifier);
		
	}
	
}
