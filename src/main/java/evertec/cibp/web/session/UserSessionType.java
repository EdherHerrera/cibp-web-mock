package evertec.cibp.web.session;

public enum UserSessionType {

	CUSTOMER_PROFILE, USERNAME_RESERVATION, CUSTOMER_PROFILE_LITE

}
