package evertec.cibp.web.session;

import java.util.List;
import java.util.Map;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.enrollment.EnrollableAccount;

public class UpdateEnrollAcctSession {
	private String validatedAcct, validatedAcctType, validatedAcctProductId, validatedUsername;
	private int counterBlock;
	private int counterFailed;
	private boolean blocked;
	private boolean failed;
	private String remoteToken;
	private List<EnrollableAccount> enrollExistingAccounts;
	private List<EnrollableAccount> accounts;

	private Map<Object, CustomerProfile> customerProfileAccountMap;

	private int actCounter;
	private int actInfCounter;
	private int zipCounter;
	private int expdateCounter;
	private boolean incompleteEnroll = false;
	private boolean alreadyEnrolled = false;

	private CustomerInteractionEvent interactionEvent;
	private boolean confirm = false;
	private boolean error = false;

	public CustomerInteractionEvent getInteractionEvent() {
		return interactionEvent;
	}

	public void setInteractionEvent(CustomerInteractionEvent interactionEvent) {
		this.interactionEvent = interactionEvent;
	}

	public String getValidatedAcct() {
		return validatedAcct;
	}

	public void setValidatedAcct(String validatedAcct) {
		this.validatedAcct = validatedAcct;
	}

	public String getValidatedAcctType() {
		return validatedAcctType;
	}

	public void setValidatedAcctType(String validatedAcctType) {
		this.validatedAcctType = validatedAcctType;
	}

	public String getValidatedAcctProductId() {
		return validatedAcctProductId;
	}

	public void setValidatedAcctProductId(String validatedAcctProductId) {
		this.validatedAcctProductId = validatedAcctProductId;
	}

	public String getValidatedUsername() {
		return validatedUsername;
	}

	public void setValidatedUsername(String validatedUsername) {
		this.validatedUsername = validatedUsername;
	}

	public int getCounterBlock() {
		return counterBlock;
	}

	public void setCounterBlock(int counterBlock) {
		this.counterBlock = counterBlock;
	}

	public int getCounterFailed() {
		return counterFailed;
	}

	public void setCounterFailed(int counterFailed) {
		this.counterFailed = counterFailed;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public boolean isFailed() {
		return failed;
	}

	public void setFailed(boolean failed) {
		this.failed = failed;
	}

	public void addCounterBlock() {
		if (++counterBlock >= 3) {
			blocked = true;
		}
	}

	public void addCounterFailed() {
		if (++counterFailed >= 2) {
			failed = true;
		}
	}

	public int getActCounter() {
		return actCounter;
	}

	public void setActCounter(int actCounter) {
		this.actCounter = actCounter;
	}

	public void addActCounter() {
		actCounter++;
	}

	public int getActInfCounter() {
		return actInfCounter;
	}

	public void setActInfCounter(int actInfCounter) {
		this.actInfCounter = actInfCounter;
	}

	public void addActInfCounter() {
		actInfCounter++;
	}

	public int getZipCounter() {
		return zipCounter;
	}

	public void setZipCounter(int zipCounter) {
		this.zipCounter = zipCounter;
	}

	public void addZipCounter() {
		zipCounter++;
	}

	public int getExpdateCounter() {
		return expdateCounter;
	}

	public void setExpdateCounter(int expdateCounter) {
		this.expdateCounter = expdateCounter;
	}

	public void addExpdateCounter() {
		expdateCounter++;
	}

	public String getRemoteToken() {
		return remoteToken;
	}

	public void setRemoteToken(String remoteToken) {
		this.remoteToken = remoteToken;
	}

	public List<EnrollableAccount> getEnrollExistingAccounts() {
		return enrollExistingAccounts;
	}

	public void setEnrollExistingAccounts(List<EnrollableAccount> enrollExistingAccounts) {
		this.enrollExistingAccounts = enrollExistingAccounts;
	}

	public List<EnrollableAccount> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<EnrollableAccount> accounts) {
		this.accounts = accounts;
	}

	public boolean isIncompleteEnroll() {
		return incompleteEnroll;
	}

	public void setIncompleteEnroll(boolean incompleteEnroll) {
		this.incompleteEnroll = incompleteEnroll;
	}

	public void setAlreadyEnrolled(boolean alreadyEnrolled) {
		this.alreadyEnrolled = alreadyEnrolled;
	}

	public boolean isAlreadyEnrolled() {
		return alreadyEnrolled;
	}

	public void setCustomerProfileAccountMap(Map<Object, CustomerProfile> customerProfileAccountMap) {
		this.customerProfileAccountMap = customerProfileAccountMap;
	}

	public Map<Object, CustomerProfile> getCustomerProfileAccountMap() {
		return customerProfileAccountMap;
	}

	public boolean isConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}
}
