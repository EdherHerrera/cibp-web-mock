package evertec.cibp.web.session;

import evertec.cibp.web.form.bean.CaptchaSettings;

public interface CaptchaEnabledForm {

	public void setCaptchaSettings(CaptchaSettings settings);

}
