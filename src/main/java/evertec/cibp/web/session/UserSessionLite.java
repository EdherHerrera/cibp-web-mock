package evertec.cibp.web.session;

import java.io.Serializable;
import org.apache.log4j.Logger;
import evertec.cibp.core.models.customer.CustomerProfileLite;
import evertec.cibp.core.services.CustomerLiteServices;


/**
 * User session object for lite profiles
 * @author et55498
 *
 */
public class UserSessionLite implements Serializable {

	public static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(UserSessionLite.class);
	private CustomerProfileLite customerProfileLite;
	private CustomerLiteServices customerLiteServices;
	//Cash Drop SMS Retries
	private int smsGlobalRetries = 0;
	private int smsSendRetries = 0;
	private String smsGeneratedCode;

	public UserSessionLite() {
		super();
	}

	public UserSessionLite(CustomerLiteServices customerLiteService, CustomerProfileLite customerProfileLite) {
		this.customerLiteServices = customerLiteService;
		this.customerProfileLite = customerProfileLite;
	}
	
	public CustomerProfileLite getCustomerProfileLite() {
		return customerProfileLite;
	}

	public void setCustomerProfileLite(CustomerProfileLite customerProfileLite) {
		this.customerProfileLite = customerProfileLite;
	}
	
	public CustomerLiteServices getCustomerLiteServices() {
		return customerLiteServices;
	}

	public void setCustomerLiteServices(CustomerLiteServices customerLiteServices) {
		this.customerLiteServices = customerLiteServices;
	}

	public String getSmsGeneratedCode() {
		return smsGeneratedCode;
	}

	public void setSmsGeneratedCode(String smsGeneratedCode) {
		this.smsGeneratedCode = smsGeneratedCode;
	}

	public int getSmsGlobalRetries() {
		return smsGlobalRetries;
	}

	public void setSmsGlobalRetries(int smsGlobalRetries) {
		this.smsGlobalRetries = smsGlobalRetries;
	}

	public int getSmsSendRetries() {
		return smsSendRetries;
	}

	public void setSmsSendRetries(int smsSendRetries) {
		this.smsSendRetries = smsSendRetries;
	}
	
}