package evertec.cibp.web.session;

import java.util.List;
import java.util.Map;

import com.rsa.csd.ws.DeviceRequest;

import evertec.cibp.core.models.enrollment.EnrollableAccount;

public class EnrollmentSECSession {
	
	
	private String username;
	private String password;
	private String passwordconfirm;
	private String accountnumber;
	private boolean agreement;
	private List<EnrollableAccount> accounts;
	private Map<Object, Object> questions1;
	private Map<Object, Object> questions2;
	private Map<Object, Object> questions3;
	private String question1;
	private String question2;
	private String question3;
	private String answer1;
	private String answer2;
	private String answer3;
	private String answer1confirm;
	private String answer2confirm;
	private String answer3confirm;
	private DeviceRequest deviceRequest;	
	private String computerType;
	private String mobileAppType;
	private boolean mobileApp;

	public String getMobileAppType() {
		return mobileAppType;
	}

	public void setMobileAppType(String mobileAppType) {
		this.mobileAppType = mobileAppType;
	}

	public boolean isMobileApp() {
		return mobileApp;
	}

	public void setMobileApp(boolean mobileApp) {
		this.mobileApp = mobileApp;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordconfirm() {
		return passwordconfirm;
	}

	public void setPasswordconfirm(String passwordconfirm) {
		this.passwordconfirm = passwordconfirm;
	}

	public String getAccountnumber() {
		return accountnumber;
	}

	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}

	public boolean getAgreement() {
		return agreement;
	}

	public void setAgreement(boolean agreement) {
		this.agreement = agreement;
	}

	public List<EnrollableAccount> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<EnrollableAccount> accounts) {
		this.accounts = accounts;
	}

	public Map<Object, Object> getQuestions1() {
		return questions1;
	}

	public void setQuestions1(Map<Object, Object> questions1) {
		this.questions1 = questions1;
	}

	public Map<Object, Object> getQuestions2() {
		return questions2;
	}

	public void setQuestions2(Map<Object, Object> questions2) {
		this.questions2 = questions2;
	}

	public Map<Object, Object> getQuestions3() {
		return questions3;
	}

	public void setQuestions3(Map<Object, Object> questions3) {
		this.questions3 = questions3;
	}

	public String getQuestion1() {
		return question1;
	}

	public void setQuestion1(String question1) {
		this.question1 = question1;
	}

	public String getQuestion2() {
		return question2;
	}

	public void setQuestion2(String question2) {
		this.question2 = question2;
	}

	public String getQuestion3() {
		return question3;
	}

	public void setQuestion3(String question3) {
		this.question3 = question3;
	}

	public String getAnswer1() {
		return answer1;
	}

	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	public String getAnswer2() {
		return answer2;
	}

	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	public String getAnswer3() {
		return answer3;
	}

	public void setAnswer3(String answer3) {
		this.answer3 = answer3;
	}

	public String getAnswer1confirm() {
		return answer1confirm;
	}

	public void setAnswer1confirm(String answer1confirm) {
		this.answer1confirm = answer1confirm;
	}

	public String getAnswer2confirm() {
		return answer2confirm;
	}

	public void setAnswer2confirm(String answer2confirm) {
		this.answer2confirm = answer2confirm;
	}

	public String getAnswer3confirm() {
		return answer3confirm;
	}

	public void setAnswer3confirm(String answer3confirm) {
		this.answer3confirm = answer3confirm;
	}

	public DeviceRequest getDeviceRequest() {
		return deviceRequest;
	}

	public void setDeviceRequest(DeviceRequest deviceRequest) {
		this.deviceRequest = deviceRequest;
	}

	public String getComputerType() {
		return computerType;
	}

	public void setComputerType(String computerType) {
		this.computerType = computerType;
	}

}
