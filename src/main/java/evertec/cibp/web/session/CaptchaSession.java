package evertec.cibp.web.session;

import java.io.Serializable;
import java.util.Locale;

import org.apache.commons.validator.GenericValidator;

import com.eloan.util.captcha.ImageCaptcha;
import com.eloan.util.captcha.ImageCaptchaFactory;
import com.eloan.util.captcha.ImageCaptchaUtil;

import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.CaptchaSettings;

public class CaptchaSession implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7485775280473697887L;
	private String captchaText;
	private CaptchaSettings settings;

	public CaptchaSession(CaptchaSettings settings) {
		this.settings = settings;
		try {
			// Static text verification for test automation.
			captchaText = StaticMessageSource.getMessageSource().getMessage(StaticMessageSource.CAPTCHA_TEXT_QA, null, Locale.ROOT);
			if(GenericValidator.isBlankOrNull(captchaText)) {
				captchaText = ImageCaptchaUtil.generateRandomText();
			}
			
		}catch(Exception e) {
			captchaText = ImageCaptchaUtil.generateRandomText();
		}

	}

	public boolean isCaptchaValid(String captchaInput) {

		return settings.isDisabled() || captchaText.equalsIgnoreCase(captchaInput);

	}

	public ImageCaptcha getImageCaptcha() {

		ImageCaptchaFactory factory = new ImageCaptchaFactory();
		factory.setDistortionFactor(settings.getDistortionFactor());
		ImageCaptcha captcha = factory.getImageCaptcha(captchaText);

		return captcha;

	}

}
