/**
 * 
 */
package evertec.cibp.web.session;

import java.io.Serializable;

/**
 * Clase evertec.cibp.web.session.GuidedSetupSession Sistema: Mi Banco
 * Descripcion: Clase que maneja los atributos necesarios para manejar el modo
 * campana tipo guided setup
 * 
 * @author Ing. Michael Picado Fuentes Creada el Mar 14, 2011 10:14:03 AM
 *         CIBP-2218
 */

public class GuidedSetupSession implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * atributo
	 * <code>guidedSetupMode del tipo boolean, mpicado variable que me indica si se encuentra en modo campana guided setup</code>
	 */
	private boolean guidedSetupMode;
	/**
	 * atributo
	 * <code>hasPIFguidedSetup del tipo boolean, mpicado, variable que indica si el usuairo puede ver la seccion de PIF</code>
	 */
	private boolean hasPIFguidedSetup;
	// bgarcia, relacionado al guided setup campaign, pero para clientes
	// existentes
	private boolean individualguidedSetup;
	// bgarcia, relacionado al guided setup campaign, pero para clientes
	// existentes
	private String individualguidedSetupCampaignId;

	private String individualGuidedSetupCampaignType;
	/**
	 * atributo
	 * <code>resubmitParent del tipo boolean, mpicado, atributo que me indica si debo
	 * recargar la pagina padre contenedora del iframe</code>
	 */
	private boolean resubmitParent;
	/**
	 * atributo <code>guidedSetupMostrado del tipo boolean</code>,mpicado,
	 * atributo que me dice si ya paso por la logica del guided setup, para
	 * cuando siga entrando en el portal no vuelva a pasar por esta logica
	 */
	private boolean guidedSetupMostrado;
	private boolean hasEBPPGuidedSetup;
	private boolean hasPaymentsGuidedSetup;

	/* Constructor de la clase GuidedSetupSession */
	public boolean isHasPaymentsGuidedSetup() {
		return hasPaymentsGuidedSetup;
	}

	public void setHasPaymentsGuidedSetup(boolean hasPaymentsGuidedSetup) {
		this.hasPaymentsGuidedSetup = hasPaymentsGuidedSetup;
	}

	public GuidedSetupSession() {
		this.guidedSetupMode = false;
		this.hasPIFguidedSetup = false;
		this.individualguidedSetup = false;
		this.individualguidedSetupCampaignId = "";
		this.resubmitParent = false;
		this.guidedSetupMostrado = false;
		this.hasEBPPGuidedSetup = false;
		this.hasPaymentsGuidedSetup = false;
	}

	/** @return regresa el guidedSetupMode */
	public boolean isGuidedSetupMode() {
		return guidedSetupMode;
	}

	/**
	 * @param guidedSetupMode
	 *            el guidedSetupMode a establecer
	 */
	public void setGuidedSetupMode(boolean guidedSetupMode) {
		this.guidedSetupMode = guidedSetupMode;
	}

	/** @return regresa el hasPIFguidedSetup */
	public boolean isHasPIFguidedSetup() {
		return hasPIFguidedSetup;
	}

	/**
	 * @param hasPIFguidedSetup
	 *            el hasPIFguidedSetup a establecer
	 */
	public void setHasPIFguidedSetup(boolean hasPIFguidedSetup) {
		this.hasPIFguidedSetup = hasPIFguidedSetup;
	}

	// bgarcia, CIBP-2218
	public boolean isIndividualguidedSetup() {
		return individualguidedSetup;
	}

	// bgarcia, CIBP-2218
	public void setIndividualguidedSetup(boolean individualguidedSetup) {
		this.individualguidedSetup = individualguidedSetup;
	}

	// bgarcia, CIBP-2218
	public String getIndividualguidedSetupCampaignId() {
		return individualguidedSetupCampaignId;
	}

	// bgarcia, CIBP-2218
	public void setIndividualguidedSetupCampaignId(String individualguidedSetupCampaignId) {
		this.individualguidedSetupCampaignId = individualguidedSetupCampaignId;
	}

	/** @return regresa el resubmitParent */
	public boolean isResubmitParent() {
		return resubmitParent;
	}

	/**
	 * @param resubmitParent
	 *            el resubmitParent a establecer
	 */
	public void setResubmitParent(boolean resubmitParent) {
		this.resubmitParent = resubmitParent;
	}

	/** @return regresa el guidedSetupMostrado */
	public boolean isGuidedSetupMostrado() {
		return guidedSetupMostrado;
	}

	/**
	 * @param guidedSetupMostrado
	 *            el guidedSetupMostrado a establecer
	 */
	public void setGuidedSetupMostrado(boolean guidedSetupMostrado) {
		this.guidedSetupMostrado = guidedSetupMostrado;
	}

	/** @return regresa el hasEBPPGuidedSetup */
	public boolean isHasEBPPGuidedSetup() {
		return hasEBPPGuidedSetup;
	}

	/**
	 * @param hasEBPPGuidedSetup
	 *            el hasEBPPGuidedSetup a establecer
	 */
	public void setHasEBPPGuidedSetup(boolean hasEBPPGuidedSetup) {
		this.hasEBPPGuidedSetup = hasEBPPGuidedSetup;
	}

	public String getIndividualGuidedSetupCampaignType() {
		return individualGuidedSetupCampaignType;
	}

	public void setIndividualGuidedSetupCampaignType(String individualGuidedSetupCampaignType) {
		this.individualGuidedSetupCampaignType = individualGuidedSetupCampaignType;
	}

}
