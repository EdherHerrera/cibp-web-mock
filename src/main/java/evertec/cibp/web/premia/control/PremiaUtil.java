/**
 *
 */
package evertec.cibp.web.premia.control;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.evertec.cibp.api.client.model.MicroServiceResponse;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TsysLoyaltyRewardsInfo;
import evertec.cibp.web.facade.CreditCardFacade;
import evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse;
import evertec.cibp.web.session.UserSession;

/**
*
* @author S681718
* @version 1.0
* @since 1.0
* @see PremiaUtil
*/
public final class PremiaUtil {

	/**constants logger */
	private static final Log LOGGER = LogFactory.getLog(PremiaUtil.class);//

	/**
	 * Premia identifier for mobiles
	 */
	public static final String PREMIA_MOBILE_SUBTYPE = "PRM";//

	/**
	 * Premia product identifier for mobiles
	 */
	public static final String PREMIA_MOBILE_PRODID = "1";//

	/**
	 * Premia status active
	 */
	public static final String PREMIA_CCA_ACTIVE = "ACTIVE";//

	/**
	 * Premia status Undefined
	 */
	public static final String PREMIA_CCA_UNDEFINED = "UNDEFINED";//

	/**
	 * Premia type account
	 */
	public static final String PREMIA_CCA_TYPE = "CCA";//

	/**
	 * Default Constructor
	 */
	private PremiaUtil () {
		throw new IllegalStateException("Premia Utility class");
	}


	/**
	 * Method for get premia account for create token.
	 * @param userSession
	 * @param premiaPriority
	 * @return
	 */
	public static List<TVFrontendAccount> premiaSelectFrontEndAccount (UserSession userSession, TVFrontendAccount premiaPriority, List<String> lstProducsId) {

		List<TVFrontendAccount> correctPrograms = new LinkedList<>();//premia rewards

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse = null; // Object Response
		final float minimumCashRewardsRedemptionAmount = 0;//
		final String logCreditCardMSResponseNullable = "The creditcards micro service response is null"; //
		final String logCreditCardsMSErrorDetail = "Creditcards micro service error detail = %s";//

			if (premiaPriority != null && premiaPriority.getTsysLoyaltyRewardsInfo() == null) {
				
			//This call is made using ccaAccountByFrontEndId.getAccountNumber() which really holds
					//the 16 digits of the "main plastic" and not the unique 11 digit account id in Tsys
					//thus its passed in the cardNumber parameter.
					microServiceTsysLoyaltyRewardsResponse = CreditCardFacade.getInstance()
							.consumeTsysLoyaltyRewardsAccountInfo(userSession,
									null, removeZeroToAccountNumber(premiaPriority.getAccountNumber(), premiaPriority.getAccountProductId(), lstProducsId), premiaPriority.isPremiaCommercialAccount());

				if (microServiceTsysLoyaltyRewardsResponse == null) {
					LOGGER.error(logCreditCardMSResponseNullable);
				} else if (microServiceTsysLoyaltyRewardsResponse.getErrors().isEmpty()) {
					setPremiaAccountRewardsPoints(premiaPriority, microServiceTsysLoyaltyRewardsResponse, minimumCashRewardsRedemptionAmount);
				} else {
					LOGGER.error(StringEscapeUtils.escapeJava(logCreditCardsMSErrorDetail + microServiceTsysLoyaltyRewardsResponse.getErrors()
							.get(0).getDetail()));
				}
			}
		correctPrograms.add(premiaPriority);
		return correctPrograms;
	}
	
	/**
	 * 
	 * @param plastic
	 * @return
	 */
	public static String removeZeroToAccountNumber(String plastic, String accountProductId, List<String> lstProducsId) {
		boolean cardHaveZero = lstProducsId.contains(accountProductId);
		if (plastic != null && plastic.startsWith("0") && plastic.length() > 15 && cardHaveZero) {
			return plastic.substring(1, plastic.length());
		}
		return plastic;
	}

	/**
	 *
	 * @param premiaPriority
	 * @param microServiceTsysLoyaltyRewardsResponse
	 * @param minimumCashRewardsRedemptionAmount
	 */
	private static void setPremiaAccountRewardsPoints (TVFrontendAccount premiaPriority,
													   MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse,
													   float minimumCashRewardsRedemptionAmount) {
		TsysLoyaltyRewardsInfo tsysLoyaltyRewardsInfo = new TsysLoyaltyRewardsInfo();//

		final TsysLoyaltyRewardsAccountInfoResponse tsysLoyaltyRewardsAccountInformation = microServiceTsysLoyaltyRewardsResponse
				.getData().get(0);//

		if (tsysLoyaltyRewardsAccountInformation.getPooledRewardsBalance() != null
					&& !tsysLoyaltyRewardsAccountInformation.getPooledRewardsBalance().getAvailable().getValue().isEmpty()) {
			tsysLoyaltyRewardsInfo.setAvailableRewardsBalance(tsysLoyaltyRewardsAccountInformation
					.getPooledRewardsBalance().getAvailable().getValue());
		} else if (tsysLoyaltyRewardsAccountInformation.getRewardsBalance() != null) {
			tsysLoyaltyRewardsInfo.setAvailableRewardsBalance(tsysLoyaltyRewardsAccountInformation
					.getRewardsBalance().getAvailable().getValue());
		}

		tsysLoyaltyRewardsInfo.setCanRedeemRewards(tsysLoyaltyRewardsAccountInformation.getCanRedeemRewards());
		tsysLoyaltyRewardsInfo.setRewardsAccountStatus(tsysLoyaltyRewardsAccountInformation.getRewardsAccountStatus());
		tsysLoyaltyRewardsInfo.setMinimumRewardsBalance(minimumCashRewardsRedemptionAmount);
		premiaPriority.setTsysLoyaltyRewardsInfo(tsysLoyaltyRewardsInfo);
	}
	
	/**
	 * 
	 * @param customerCodes from profile
	 * @param premiaCodes which identify the profile has premia products
	 * @return
	 */
	public static boolean hasPremiaProductsByCustomerCodes (List<String> customerCodes, List<String> premiaCodes) {
		boolean hasPremiaProduct = false;
		
		if (customerCodes == null && premiaCodes == null) {
			hasPremiaProduct = false;
		} else if (customerCodes != null && customerCodes.isEmpty() || premiaCodes != null && premiaCodes.isEmpty()) {
			hasPremiaProduct = false;
		} else {
			List<String> intersectElements = customerCodes.stream()
					.map(customercode -> normaliceCustomerCode(customercode))
					.filter(premiaCodes :: contains)
					.collect(Collectors.toList());
			hasPremiaProduct = !intersectElements.isEmpty();
		}
		
		return hasPremiaProduct;
	}
	
	/**
	 * Method to normalice code to type XXX (three digits) 
	 * eg. ( 9 -> 009)
	 * @param codeVCustomer
	 * @return String with code normalized
	 */
	private static String normaliceCustomerCode (String codeVCustomer) {
		return ("000" + codeVCustomer).substring(codeVCustomer.length());
	}

	/**
	 * Method for Filter Accounts by Active Status and special ghost account.
	 * @param premiaAccounts
	 * @param ghostAccount
	 * @return
	 */
	public static List<TVFrontendAccount> filterPremiaAccounts (List<TVFrontendAccount> premiaAccounts, String ghostAccount) {
		if (ghostAccount != null && !ghostAccount.isEmpty() && premiaAccounts != null) {
		return premiaAccounts.stream()
				.filter(premiaAccount -> isValidPremiaAccount(premiaAccount, ghostAccount))
				.collect(Collectors.toList());
		} else {
			return premiaAccounts;
		}
	}

	/**
	 *
	 * @param premiaAccount
	 * @param ghostAccount
	 * @return
	 */
	private static boolean isValidPremiaAccount (TVFrontendAccount premiaAccount, String ghostAccount) {
		return premiaAccount.getProductDefinition().getProductType().equals(PREMIA_CCA_TYPE) &&
				(premiaAccount.getProductDefinition().getProductStatus().equals(PREMIA_CCA_ACTIVE) ||
				premiaAccount.getProductDefinition().getProductStatus().equals(PREMIA_CCA_UNDEFINED) ||
				premiaAccount.getProductDefinition().getProductId().equals(ghostAccount));
	}
}
