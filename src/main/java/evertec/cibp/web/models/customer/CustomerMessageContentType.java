package evertec.cibp.web.models.customer;

public enum CustomerMessageContentType {
	EVENT, INFORMATION, WARNING, CAMPAIGN
}
