package evertec.cibp.web.models.customer;
import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import evertec.cibp.core.models.base.BaseObject;
import evertec.cibp.web.utils.Utils;

public class CustomerMessageContent extends BaseObject implements Serializable {

	private static final long serialVersionUID = -1615327290571030299L;
	
	private String id;
	private CustomerMessageContentType type;
	private String message;
	private String iconUrl;
	private String viewedTime;
	private String messageCSS;
	private String date;
	private Date campaignDate;

	public CustomerMessageContent(String id, CustomerMessageContentType type, String message, String iconUrl) {
		this.id = id;
		this.type = type;
		this.message = message;
		this.iconUrl = iconUrl;
	}
	
	public CustomerMessageContent(String id, String message, String date, String iconUrl) {
		this.id = id;
		this.message = message;
		this.date = date;
		this.iconUrl = iconUrl;
	}
	
	public CustomerMessageContent(String id, CustomerMessageContentType type, String message, String iconUrl,
			Date campaignDate) {
		super();
		this.id = id;
		this.type = type;
		this.message = message;
		this.iconUrl = iconUrl;
		this.campaignDate = campaignDate;
	}

	public CustomerMessageContentType getType() {
		return type;
	}

	public void setType(CustomerMessageContentType type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getViewedTime() {
		return viewedTime;
	}

	public void setViewedTime(String viewedTime) {
		this.viewedTime = viewedTime;
	}
	
	public String getMessageCSS() {
		return messageCSS;
	}

	public void setMessageCSS(String messageCSS) {
		this.messageCSS = messageCSS;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Date getCampaignDate() {
		return campaignDate;
	}

	public void setCampaignDate(Date campaignDate) {
		this.campaignDate = campaignDate;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object objeto) {
		boolean resultado;
		if (!(objeto instanceof CustomerMessageContent))
			resultado = false;
		else {
			CustomerMessageContent customerMessageContent = (CustomerMessageContent) objeto;
			resultado = new EqualsBuilder().append(this.id, customerMessageContent.getId()).isEquals();
		}
		return resultado;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(this.id).append(this.message).append(this.type).append(this.iconUrl).append(this.messageCSS).append(this.date).toHashCode();
	}
	
}
