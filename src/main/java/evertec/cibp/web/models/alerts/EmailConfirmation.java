package evertec.cibp.web.models.alerts;

import java.io.Serializable;
import java.util.Date;

/**
 * Copyright 2017 Evertec, Inc.  All Rights Reserved.
 * 
 * Email Confirmation model, that is the entity of the table "EMAIL_CONFIRMATIONS".
 * 
 * The class is annotated with {@code @Entity}, which marks this class as an 
 * entity bean, so it must have a no-argument constructor that is visible 
 * with at least protected scope.
 */

public class EmailConfirmation implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 377879037066090845L;
	
	private String id;
	private String fromName;
	private String fromAddress;
	private String toName;
	private String toAddress;
	private String messageBody;
	private String subject;
	private Long status;
	private String error;
	private Date creationDate;
	private int event;
	private String profileId;
	
	/**
	 * Constructor
	 */
	public EmailConfirmation() {
		super();
	}

	/**
	 * Constructor
	 * @param fromName
	 * @param fromAddress
	 * @param toName
	 * @param toAddress
	 * @param messageBody
	 * @param subject
	 * @param event
	 * @param profileId
	 */
	public EmailConfirmation(String fromName, String fromAddress, String toName, String toAddress,
			String messageBody, String subject, int event, String profileId) {
		super();
		this.fromName = fromName;
		this.fromAddress = fromAddress;
		this.toName = toName;
		this.toAddress = toAddress;
		this.messageBody = messageBody;
		this.subject = subject;
		this.event = event;
		this.profileId = profileId;
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the fromName
	 */
	public String getFromName() {
		return fromName;
	}

	/**
	 * @param fromName the fromName to set
	 */
	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	/**
	 * @return the fromAddress
	 */
	public String getFromAddress() {
		return fromAddress;
	}

	/**
	 * @param fromAddress the fromAddress to set
	 */
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	/**
	 * @return the toName
	 */
	public String getToName() {
		return toName;
	}

	/**
	 * @param toName the toName to set
	 */
	public void setToName(String toName) {
		this.toName = toName;
	}

	/**
	 * @return the toAddress
	 */
	public String getToAddress() {
		return toAddress;
	}

	/**
	 * @param toAddress the toAddress to set
	 */
	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	/**
	 * @return the messageBody
	 */
	public String getMessageBody() {
		return messageBody;
	}

	/**
	 * @param messageBody the messageBody to set
	 */
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the status
	 */
	public Long getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Long status) {
		this.status = status;
	}

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the event
	 */
	public int getEvent() {
		return event;
	}

	/**
	 * @param event the event to set
	 */
	public void setEvent(int event) {
		this.event = event;
	}

	/**
	 * @return the profileId
	 */
	public String getProfileId() {
		return profileId;
	}

	/**
	 * @param profileId the profileId to set
	 */
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmailConfirmation [id=" + id + ", fromName=" + fromName + ", fromAddress=" + fromAddress + ", toName="
				+ toName + ", toAddress=" + toAddress + ", messageBody=" + messageBody + ", subject=" + subject
				+ ", status=" + status + ", error=" + error + ", creationDate=" + creationDate + ", event=" + event
				+ ", profileId=" + profileId + "]";
	}

	
}
