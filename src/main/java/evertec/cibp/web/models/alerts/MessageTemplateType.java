package evertec.cibp.web.models.alerts;

import java.io.Serializable;

/**
 * The ENUM MessageTemplate provides the valid Templates_Key
 * @author Luis Alicea
 * @since 1.0
 * @version 1.0
 */
public enum MessageTemplateType implements Serializable {
	
	//DOCUMENTS 
	DOCUMENTS_ACCEPTANCE_EMAIL, DOCUMENTS_ACCEPTANCE_CSR_EMAIL, ATH_CARD_ON_ACTION_EMAIL, ATH_CARD_OFF_ACTION_EMAIL
}
