package evertec.cibp.web.models.vault;

public class VaultSignIn{
	
    private String permId;
	private String sessionKey;
	
	/**
	 * @return the permId
	 */
	public String getPermId() {
		return permId;
	}

	/**
	 * @param permId the permId to set
	 */
	public void setPermId(String permId) {
		this.permId = permId;
	}

	/**
	 * @return the sessionKey
	 */
	public String getSessionKey() {
		return sessionKey;
	}

	/**
	 * @param sessionKey the sessionKey to set
	 */
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}


	@Override
	public String toString() {
		return "VaultSignIn [permId=" + permId + ", sessionKey=" + sessionKey + "]";
	}

}
