package evertec.cibp.web.models.vault;

public class AccountStatement {
	
	private int year;
	private String month;
	private String totalChargeMonthly;
	private String totalChargeYearly;

	public AccountStatement(int year, String month, String totalChargeMonthly, String totalChargeYearly) {
		this.year = year;
		this.month = month;
		this.totalChargeMonthly = totalChargeMonthly;
		this.totalChargeYearly = totalChargeYearly;
	}
	
	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getTotalChargeMonthly() {
		return totalChargeMonthly;
	}

	public void setTotalChargeMonthly(String totalChargeMonthly) {
		this.totalChargeMonthly = totalChargeMonthly;
	}

	public String getTotalChargeYearly() {
		return totalChargeYearly;
	}

	public void setTotalChargeYearly(String totalChargeYearly) {
		this.totalChargeYearly = totalChargeYearly;
	}
	
}
