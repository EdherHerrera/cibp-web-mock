package evertec.cibp.web.models.adaptiveauth;

import java.io.Serializable;

/**
 * <h1>QuestionAndAnswer</h1>
 * This class provides the fields for the questions and answers
 *
 * Copyright 2019 Evertec, Inc. All Rights Reserved.
 * 
 * @author Ramón Cruz
 *
 */
public class QuestionAndAnswer implements Serializable {
	
	/**
	 * 
	 * The serialVersionUID
	 * 
	 */
	private static final long serialVersionUID = -5934806696413508871L;

	/**
	 * 
	 * The question
	 * 
	 */
	private String question;
	
	/**
	 * 
	 * The answer
	 * 
	 */
	private String answer;
	
	/**
	 * 
	 * Empty constructor
	 *
	 */
	public QuestionAndAnswer() {

	}

	/**
	 * 
	 * Constructor for setting the question and answer
	 * @param question
	 * @param answer
	 */
	public QuestionAndAnswer(String question, String answer) {
		this.question = question;
		this.answer = answer;
	}

	/**
	 * 
	 * Gets the question
	 * @return question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * 
	 * Sets the question
	 * @param question
	 * 
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * 
	 * Gets the answer
	 * @return answer
	 * 
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * 
	 * Sets the answer
	 * @param answer
	 * 
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/**
	 * 
	 * Gets the serialVersionUID
	 * @return serialVersionUID
	 * 
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}