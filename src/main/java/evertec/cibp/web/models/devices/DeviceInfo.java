package evertec.cibp.web.models.devices;

public class DeviceInfo {

    private static final String UNAVAILABLE = "Not Available";

    private String appVersion;
    private String osVersion;
    private String networkProvider;
    private String deviceModel;

    public DeviceInfo(boolean isNull) {
        super();
        if(isNull) {
            this.appVersion = UNAVAILABLE;
            this.osVersion = UNAVAILABLE;
            this.networkProvider = UNAVAILABLE;
            this.deviceModel = UNAVAILABLE;
        }else {
            new DeviceInfo();
        }
    }
    
    
    public DeviceInfo() {
        super();
    }

    public String getAppVersion() {
        return appVersion;
    }
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
    public String getOsVersion() {
        return osVersion;
    }
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }
    public String getNetworkProvider() {
        return networkProvider;
    }
    public void setNetworkProvider(String networkProvider) {
        this.networkProvider = networkProvider;
    }
    public String getDeviceModel() {
        return deviceModel;
    }
    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((appVersion == null) ? 0 : appVersion.hashCode());
        result = prime * result + ((deviceModel == null) ? 0 : deviceModel.hashCode());
        result = prime * result + ((networkProvider == null) ? 0 : networkProvider.hashCode());
        result = prime * result + ((osVersion == null) ? 0 : osVersion.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DeviceInfo other = (DeviceInfo) obj;
        if (appVersion == null) {
            if (other.appVersion != null)
                return false;
        } else if (!appVersion.equals(other.appVersion))
            return false;
        if (deviceModel == null) {
            if (other.deviceModel != null)
                return false;
        } else if (!deviceModel.equals(other.deviceModel))
            return false;
        if (networkProvider == null) {
            if (other.networkProvider != null)
                return false;
        } else if (!networkProvider.equals(other.networkProvider))
            return false;
        if (osVersion == null) {
            if (other.osVersion != null)
                return false;
        } else if (!osVersion.equals(other.osVersion))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "DeviceInfo [appVersion=" + appVersion + ", osVersion=" + osVersion + ", networkProvider="
                + networkProvider + ", deviceModel=" + deviceModel + "]";
    }



}
