/**
 * Config package to get all the spring configs
 * <p>
 * Project: CIBP Web
 * Company: Evertec
 */
package evertec.cibp.web.config;

import evertec.cibp.web.interceptor.GlobalEntitlementInterceptor;
import evertec.cibp.web.interceptor.GlobalPostHandleFormInterceptor;
import evertec.cibp.web.interceptor.HasBankingSessionInterceptor;
import evertec.cibp.web.interceptor.LoggedinInterceptor;
import evertec.cibp.web.interceptor.UrlRestrictIntercept;
import evertec.cibp.web.interceptor.UserAgentInterceptor;
import evertec.cibp.core.interceptor.HostnameIdentifierInterceptor;



import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.theme.ThemeChangeInterceptor;


@EnableWebMvc
@Configuration
@ComponentScan({"evertec.cibp.web.controller"})

/**
 * The WebConfig annotation class on spring framework
 *
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @created 2020-02-26 10:35
 * @since 1.0
 * @version 1.8
 *
 */
public class WebConfig extends WebMvcConfigurerAdapter {

    /**
     * The required Spring constructor
     */
    public WebConfig() {
        super();
    }

    /**
     * Get the globalEntitlementInterceptor from context
     *
     * @return GlobalEntitlementInterceptor
     */
    @Bean
    GlobalEntitlementInterceptor globalEntitlementInterceptor() {
        return new GlobalEntitlementInterceptor();
    }

    /**
     * Get the localeChangeInterceptor from context
     *
     * @return LocaleChangeInterceptor
     */
    @Bean
    LocaleChangeInterceptor localeChangeInterceptor() {
        return new LocaleChangeInterceptor();
    }

    /**
     * Get the themeChangeInterceptor from context
     *
     * @return ThemeChangeInterceptor
     */
    @Bean
    ThemeChangeInterceptor themeChangeInterceptor() {
        return new ThemeChangeInterceptor();
    }

    /**
     * Get the loggedinInterceptor from context
     *
     * @return LoggedinInterceptor
     */
    @Bean
    LoggedinInterceptor loggedinInterceptor() {
        return new LoggedinInterceptor();
    }

    /**
     * Get the HasBankingSessionInterceptor from context
     *
     * @return HasBankingSessionInterceptor
     */
    @Bean
    HasBankingSessionInterceptor hasBankingSessionInterceptor() {
        return new HasBankingSessionInterceptor();
    }

    /**
     * Get the GlobalPostHandleFormInterceptor from context
     *
     * @return GlobalPostHandleFormInterceptor
     */
    @Bean
    GlobalPostHandleFormInterceptor globalPostHandleFormInterceptor() {
        return new GlobalPostHandleFormInterceptor();
    }

    /**
     * Get the UserAgentInterceptor from context
     *
     * @return UserAgentInterceptor
     */
    @Bean
    UserAgentInterceptor userAgentInterceptor() {
        return new UserAgentInterceptor();
    }
    
    /**
     * Get the HosNameIdentifierInterceptor from context
     *
     * @return HostnameIdentifierInterceptor
     */
    @Bean
    HostnameIdentifierInterceptor hostnameIdentifierInterceptor() {
        return new HostnameIdentifierInterceptor();
    }

    /**
     * Get the urlRestrictInterceptor from context
     *
     * @return UrlRestrictInterceptor
     */
    @Bean
    UrlRestrictIntercept urlRestrictInterceptor() {
        return new UrlRestrictIntercept();
    }

    /**
     * Create the messageSource on the framework
     *
     * @return MessageSource
     */
    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = null; // The Message source to return

        messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames("messages", "texts");
        messageSource.setUseCodeAsDefaultMessage(true);

        messageSource.setDefaultEncoding("UTF-8");

        return messageSource;
    }

    /**
     * Get the factory to get the validators
     *
     * @return LocalValidatorFactoryBean
     */
    @Bean
    public LocalValidatorFactoryBean getValidator() {
        LocalValidatorFactoryBean bean = null; // The Factory validator bean

        bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());

        return bean;
    }

    /**
     * Add all the interceptors to the spring context
     *
     * @param registry the object that contains the interceptors
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
    	 registry.addInterceptor(globalEntitlementInterceptor());//
         registry.addInterceptor(localeChangeInterceptor());
         registry.addInterceptor(themeChangeInterceptor());
         registry.addInterceptor(loggedinInterceptor());//
         registry.addInterceptor(hasBankingSessionInterceptor());//
         registry.addInterceptor(globalPostHandleFormInterceptor());
         registry.addInterceptor(userAgentInterceptor());
         registry.addInterceptor(hostnameIdentifierInterceptor());//
         registry.addInterceptor(urlRestrictInterceptor());//
    }
}
