package evertec.cibp.web.athm;

import java.io.Serializable;

public class TransferInfo implements Serializable {
	private static final long serialVersionUID = 3796241765331108280L;

	private String _date = null;
	private String _source = null;
	private String _target = null;
	private String _amount = null;
	private String _card = null;
	private String _message = null;
	private String _referenceNumber = null;
	private String _status = null;

	public void setDate(String date) {
		this._date = date;
	}

	public String getDate() {
		return this._date;
	}

	public void setSource(String source) {
		this._source = source;
	}

	public String getSource() {
		return this._source;
	}

	public void setTarget(String target) {
		this._target = target;
	}

	public String getTarget() {
		return this._target;
	}

	public void setAmount(String amount) {
		this._amount = amount;
	}

	public String getAmount() {
		return this._amount;
	}

	public void setCard(String card) {
		this._card = card;
	}

	public String getCard() {
		return this._card;
	}

	public void setMessage(String message) {
		this._message = message;
	}

	public String getMessage() {
		return this._message;
	}

	public void setReferenceNumber(String referenceNumber) {
		this._referenceNumber = referenceNumber;
	}

	public String getReferenceNumber() {
		return this._referenceNumber;
	}

	public void setStatus(String status) {
		this._status = status;
	}

	public String getStatus() {
		return this._status;
	}
}