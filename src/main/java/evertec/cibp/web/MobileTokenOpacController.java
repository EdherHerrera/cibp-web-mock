package evertec.cibp.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.MobileApiSettingsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * Used by apps to determine if profile is ready to make a account opening.
 * 
 * @author et56169
 * 
 */
@SuppressWarnings("unused")
public class MobileTokenOpacController extends MultiActionController implements MessageSourceAware {

	
	private static final Logger logger = Logger.getLogger(WebAppController.class);
	private MessageSource messageSource;
	private CustomerServices customerService;
	private MobileApiSettingsBean mobileAppGatewaySettings;
	private String opacUrl;
	private String[] locatorActiveLayers;

	public void initialize() {
		String layersString = mobileAppGatewaySettings.getLocatorActiveLayers();
		locatorActiveLayers = layersString.split(",");
	}
	
	public ModelAndView tokenOpac(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		String token = "";
		String url = "";
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC)){
			int owners = 0;
			String accountType = "checking";
			String ownershipType = "Individual";
			UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			token = userSession.getCustomerService().tokenGenerateForProductSelectionUUID(
					userSession.getCustomerProfile(), accountType, ownershipType, owners);
		
		url = opacUrl + "?mobile=true&&rtoken=" + token+"&&language=";
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			url = url+ "en";
		else
			url = url + "es";
		}
		model.put("tokenOpac", token);
		model.put("opacURL", opacUrl);
		model.put("URL", url);
		model.put("flagOpac", StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC));
		
		return new ModelAndView("tokenOpac", model);
	}
	
	protected MobileApiSettingsBean getMobileAppGatewaySettings() {
		return mobileAppGatewaySettings;
	}

	public void setMobileAppGatewaySettings(MobileApiSettingsBean mobileAppGatewaySettings) {
		this.mobileAppGatewaySettings = mobileAppGatewaySettings;
	}

	protected CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	
	public String getOpacUrl() {
		return opacUrl;
	}

	public void setOpacUrl(String opacUrl) {
		this.opacUrl = opacUrl;
	}
}
