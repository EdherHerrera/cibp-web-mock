package evertec.cibp.web;

import evertec.cibp.core.models.entitlement.EntitlementType;

public interface GlobalEntitlementAwareController {

	public EntitlementType getGlobalEntitlement();

}
