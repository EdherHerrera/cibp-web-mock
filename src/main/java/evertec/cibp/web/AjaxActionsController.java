package evertec.cibp.web;

import bppr.sms.beans.SMSEVENTS;
import bppr.tv.beans.Owner;
import bppr.tv.beans.accounts.AccountRecord;
import bppr.tv.beans.accounts.types.DepositAccount;
import bppr.tv.beans.statements.credit.CreditStatement;
import com.evertec.cibp.api.client.model.Error;
import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.google.gson.Gson;
import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.EventType;
import com.rsa.csd.ws.UserStatus;
import edu.emory.mathcs.backport.java.util.Arrays;
import edu.emory.mathcs.backport.java.util.Collections;
import evertec.cibp.core.dto.PushTokensDto;
import evertec.cibp.core.enums.OnOffEnums;
import evertec.cibp.core.enums.PushTokenStatus;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TVFrontendDepositAccount;
import evertec.cibp.core.models.account.TsysLoyaltyRewardsInfo;
import evertec.cibp.core.models.accountopening.AccountApplication;
import evertec.cibp.core.models.accountopening.AccountApplications;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.base.AbstractStatement;
import evertec.cibp.core.models.campaign.ActiveCampaignCustomerView;
import evertec.cibp.core.models.campaign.ActiveCampaignView;
import evertec.cibp.core.models.campaign.Campaign;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.campaign.PortalCampaign;
import evertec.cibp.core.models.campaign.ViewCampaign;
import evertec.cibp.core.models.customer.AlertsHistory;
import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.core.models.customer.CustomerPayeePreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerWealthPreferred;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.VendorReceiptHistory;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.customer.interaction.PushTokenInteraction;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.documents.CIBPConfigurableTextsTypes;
import evertec.cibp.core.models.documents.ConfigurableTextSubSections;
import evertec.cibp.core.models.documents.CustomerDocumentsPackage;
import evertec.cibp.core.models.ebill.Ebpp2BillerProductIdResponse;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EbppEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.holiday.Holiday;
import evertec.cibp.core.models.messaging.PrivateBankerEmail;
import evertec.cibp.core.models.onoff.OnOffCoreModel;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.sos.SosClaimsInfo;
import evertec.cibp.core.models.statements.AccountPDFStmtInfo;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.CustomerServices.TsysApiAction;
import evertec.cibp.core.services.interfaces.CookiePreferenceStatusService;
import evertec.cibp.core.services.requests.PushTokensRequest;
import evertec.cibp.core.services.requests.SendSmsGeneratedCodeRequest;
import evertec.cibp.core.services.requests.StatementRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.AddPayeeResponse;
import evertec.cibp.core.services.responses.AddPayeeResponse.Result;
import evertec.cibp.core.services.responses.EditVendorPayeeResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.ValidationQuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.utils.OnOffUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.CryptoUtils;
import evertec.cibp.crypto.RandomUtils;
import evertec.cibp.index.globalpayee.GlobalPayeeIndexBean;
import evertec.cibp.index.globalpayee.SearchResult;
import evertec.cibp.index.globalpayee.SearchResultImpl;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.commercial.AjaxActionsCommercial;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.enums.MobileResponseStatus;
import evertec.cibp.web.enums.RecurringCashBackType;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.AccountsFacade;
import evertec.cibp.web.facade.CreditCardFacade;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.facade.DocumentsFacade;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.facade.PushTokensFacade;
import evertec.cibp.web.form.bean.AddExistingPayeeBean;
import evertec.cibp.web.form.bean.EditAccountBean;
import evertec.cibp.web.form.bean.EditPayeeBean;
import evertec.cibp.web.form.bean.UnicaApplication;
import evertec.cibp.web.form.mobile.bean.MobileAlert;
import evertec.cibp.web.request.ModifyAccountPreferenceRequest;
import evertec.cibp.web.request.TLRRSCreditRequest;
import evertec.cibp.web.resolver.types.AjaxError;
import evertec.cibp.web.response.PushTokensResponse;
import evertec.cibp.web.response.RetirementPlanInfoResponse;
import evertec.cibp.web.response.TLRRSCreditResponse;
import evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse;
import evertec.cibp.web.services.modules.CookiePreferenceServiceModule;
import evertec.cibp.web.retirementPlan.control.RetirementPlanUtil;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.session.UserSessionType;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.CookieUtils;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.ListUtils;
import evertec.cibp.web.utils.MicroServiceUtils;
import evertec.cibp.web.utils.PushTokenConstants;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.UnicaUtils;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.view.GlobalPayeeSearchResultView;
import evertec.onoff.models.OnOffPortalResponse;
import evertec.otherservices.beans.UserAccount;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.RegisterUserAccountReq;
import evertec.otherservices.responses.RegisterUserAccountResp;
import evertec.otherservices.responses.UnicaClientConfirmationResponse;
import evertec.prophit.service.bean.CustomerInfoBean;
import evertec.prophit.service.bean.CustomerInquiryBean;
import evertec.prophit.service.bean.EbillIndType;
import evertec.zap.service.bean.EmployeeInfoResponse;
import evertec.relius.retirementplan.configuration.RetirementPlanClient;
import evertec.relius.retirementplan.ws.GetRetirementPlanBalanceFilterParameters;
import evertec.relius.retirementplan.ws.wsdl.RetirementPlanBalanceResponse;
import feign.RetryableException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class AjaxActionsController extends MultiActionController implements AjaxError {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	private GlobalPayeeIndexBean globalPayeeIndex;
	private MessageSource messageSource;
	protected CustomerServices customerService;
	protected CookiePreferenceStatusService cookiePreferenceStatusService;
	private String aadvantageProducts;
	private String wealthCodes;
	private String preferredCodes;
	private String sosResolvedConditions;
	private String sosCallToActionStatus;

	private String ezStatementInformativaIRAQuarterlyProductId;
	private String ezStatementInformativaCAEQuarterlyProductId;
	private String ezStatementInformativaIRAYearlyProductId;
	private String ezStatementInformativaCAEYearlyProductId;
	private String[] ezStatementInformativaIRACAEProductIDs;

	private String unicaLoanProducts;
	private String unicaCreditProducts;
	private String marketplaceCreditProducts;
	private String unicaRefresh;
	private String unicaAllowedStatus;
	private String marketplaceAllowedStatus;
	private String unicaBaseUrl;

	private String eSignValidDepositAccounts;
	private String[] relationshipType;
	private String hidePayees;
	private String epayrollId;
	private String evendorId;
	private String alertsHistoryDays;
	
	private float minimumCashRewardsRedemptionAmount;
	
	private final String jsonValue = "json"; // String value
	private final String frontEndIdValue = "frontEndId"; // String value
	private final String strUnused = "unused"; //unused string

	
    private RetirementPlanClient retirementPlanClient = new RetirementPlanClient();// crear
    
    
	public ModelAndView alerthistorymessage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String id;
		String message = "";
		String source = "";
		String channel = "";
		String phone = "";
		Date eventDate;
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		List<AlertsHistory> historylist = userSession.getAlertsHistory();
		if (!GenericValidator.isBlankOrNull(request.getParameter("alerthistoryid"))) {
			id = request.getParameter("alerthistoryid");
			for (AlertsHistory history : historylist) {
				if (history.getId().equalsIgnoreCase(id)) {
					message = history.getMessage();
					source = history.getSource();
					channel = history.getChannel();
					if (channel.equalsIgnoreCase("SMS") && source.equalsIgnoreCase("Mibanco")) {
						// Mask code for OOB SMS challenge
						if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && 
								history.getEventType() == SMSEVENTS.SMS_SENT_GENERATED_CODE_MESSAGE && message != null)
							message = message.replaceAll("\\d{6}", "XXXXXX");
						eventDate = history.getDate();
						String eventDateString = df.format(eventDate);
						phone = history.getPhone();
						model.put("text", eventDateString + "<BR>Cel #: " + phone + "<BR><BR>" + message);
						return new ModelAndView("textpopup", model);
					}
					if (channel.equalsIgnoreCase("EMAIL") && source.equalsIgnoreCase("Mibanco")) {
						message = message.replaceAll("<a", "<a target=\"_blank\" ");
						message = message.replaceAll("<A", "<a target=\"_blank\" ");
						model.put("text", message);
						return new ModelAndView("text", model);
					}
					if (source.equalsIgnoreCase("Paxus")) {
						model.put("text", message);
						return new ModelAndView("textpopup", model);
					}
				}
			}
		}
		return new ModelAndView("textpopup", model);
	}

	@SuppressWarnings("unchecked")
	public ModelAndView portalUnicaApplications(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<UnicaApplication> unicaApplicationsList = new LinkedList<UnicaApplication>();
		List<UnicaApplication> marketplaceApplications = new LinkedList<UnicaApplication>();
		if (userSession != null) {

			addApplications(model, userSession); // Pending apps (e-accounts,
													// etc)
			unicaApplicationsList = UnicaUtils.getApplications(userSession, unicaRefresh, unicaAllowedStatus, 
					unicaLoanProducts, unicaCreditProducts, marketplaceAllowedStatus, marketplaceCreditProducts);
			
			Map<Boolean, List<UnicaApplication>> partitionedApplications = unicaApplicationsList.stream().collect(Collectors.partitioningBy(UnicaApplication::isFromMarketplace));
			
			marketplaceApplications = partitionedApplications.get(true);
			unicaApplicationsList = partitionedApplications.get(false);		
		}
		
		model.put("portalUnicaApplications", unicaApplicationsList);
		
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)) {
			model.put("marketplaceApplications", marketplaceApplications);
		}
			
		return new ModelAndView("portalUnicaApplications", model);
	}

	private void addApplications(Map<String, Object> model, UserSession userSession) {

		if (userSession != null && userSession.getUserSessionType().equals(UserSessionType.USERNAME_RESERVATION)) {
			model.put("cannotOpenAdditionalAccounts", true);
			model.put("applicationCancelationTriggersErase", userSession.getIncompleteApplicationCount() == 1);
		}
		AccountApplications accountApplications = userSession.getAccountApplications(true);
		List<AccountApplication> completedApplications2 = accountApplications.getCompletedApplications();

		for (AccountApplication accountApplication : completedApplications2) {
			if (!accountApplication.isAccountAvailableInBackend()) {
				if (!completedApplications2.isEmpty()) {
					userSession.setCompletedApplications(completedApplications2);
				}
				model.put("completedNotAvailableApplications", true);
				break;
			}
		}

		model.put("canOpenAdditionalAccounts", userSession.isCanOpenAdditionalAccounts());
		model.put("canCompleteAdditionalAccounts", userSession.isCanCompleteAdditionalAccounts());

		model.put("completedApplications", completedApplications2);
		List<AccountApplication> aps = accountApplications.getPendingApplications();
		for (Iterator<AccountApplication> iterator = aps.iterator(); iterator.hasNext();) {
			AccountApplication accountApplication2 = (AccountApplication) iterator.next();

			if (!accountApplication2.isIncomplete()) {
				model.put("showAlertText", true);
				model.put("showNotice", true);
			}

		}

		model.put("pendingApplications", accountApplications.getPendingApplications());
	}

	@SuppressWarnings("rawtypes")
	public ModelAndView searchGlobalPayee(HttpServletRequest request, HttpServletResponse response) throws Exception {

		if (!globalPayeeIndex.isInitialized()) {
			return new ModelAndView("initializing");
		}

		String s = ServletRequestUtils.getStringParameter(request, "s");
		Integer page = ServletRequestUtils.getIntParameter(request, "page");

		if (page == null)
			page = new Integer(0);

		if (s == null) {
			GlobalPayeeSearchResultView paymentSearchResultView = new GlobalPayeeSearchResultView();
			SearchResult searchResult = new SearchResultImpl(new ArrayList());
			return new ModelAndView(paymentSearchResultView, "searchResult", searchResult);
		} else {
			SearchResult result = globalPayeeIndex.search(s, hidePayees);
			result.setPageSize(15);

			Map<String, Object> m = new HashMap<>();
			m.put("searchResult", result);
			m.put("page", page);

			int totalPages = result.getNumberOfMatches() / 15;
			if ((result.getNumberOfMatches() % 15) > 0)
				totalPages++;

			LinkedHashMap<String, String> pagesMap = new LinkedHashMap<>();
			for (int i = 1; i <= totalPages && totalPages > 1; i++) {
				pagesMap.put(String.valueOf(i), String.valueOf(i));

			}
			m.put("pagesMap", pagesMap);

			if (result.getNumberOfMatches() <= 0) {
				if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
					m.put("noMatchMsg", "No Matches Found.");
				else
					m.put("noMatchMsg", "No opciones encontradas.");
			}
			
			
			m.put("MBSFE_138", StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138));
			return new ModelAndView("ajaxGlobalPayeeSearchResult", m);
		}
	}
	
	public ModelAndView getPayeePreferencesByPayeeId(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		StringBuilder sb = new StringBuilder();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String payeeId = request.getParameter("payee") != null ? request.getParameter("payee") : "";
		if (userSession != null) {
			TVFrontEndPayee payee = Utils.getPayeebyId(payeeId, userSession.getPayees(true));
			if (payee != null) {
				CustomerPayeePreferences preferences = payee.getCustomerPayeePreferences();
				if (preferences != null) {
					sb.append("nickname~" + preferences.getNickname() + "|");
					sb.append("address~" + preferences.getAddress() + "|");
					sb.append("zipcode~" + preferences.getZipcode() + "|");
					sb.append("state~" + preferences.getState() + "|");
					sb.append("city~" + preferences.getCity() + "|");
					sb.append("email~" + preferences.getEmail() + "|");
					sb.append("comments~" + preferences.getComments() + "|");
				}
			}
		}
		model.put("text", sb.toString());
		return new ModelAndView("text", model);
	}
	
	
	public ModelAndView editVendorPayeePreference(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		StringBuilder sb = new StringBuilder();
		StringBuilder errors = new StringBuilder();
		EditPayeeBean editPayee = new EditPayeeBean();
		boolean hasErrors = false;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		if(userSession != null){
			CustomerProfile customerProfile = userSession.getCustomerProfile();
			BankingSession bankingSession = userSession.getBankingSession();
			CustomerPayeePreferences preferences = new CustomerPayeePreferences();
			
			String confirmEmail = request.getParameter("confirm-email") != null ? request.getParameter("confirm-email")
				: "";
		
			editPayee.setPayee(request.getParameter("payee") != null ? request.getParameter("payee") : "");
			preferences.setAddress(request.getParameter("address") != null ? request.getParameter("address") : "");
			preferences.setCity(request.getParameter("city") != null ? request.getParameter("city") : "");
			preferences.setComments(request.getParameter("comments") != null ? request.getParameter("comments") : "");
			preferences.setEmail(request.getParameter("email") != null ? request.getParameter("email") : "");
			preferences.setNickname(request.getParameter("nickname") != null ? request.getParameter("nickname") : "");
			preferences.setState(request.getParameter("state") != null ? request.getParameter("state") : "");
			preferences.setZipcode(request.getParameter("zipcode") != null ? request.getParameter("zipcode") : "");
			
			TVFrontEndPayee payee = Utils.getPayeebyId(editPayee.getPayee(), userSession.getPayees(true));
			
			Locale lang;
			
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				lang = new Locale("EN");
			else
				lang = new Locale("ES");
			
			if(payee != null){
				if (!confirmEmail.equals(preferences.getEmail())) {
					hasErrors = true;
					errors.append("#" + this.messageSource.getMessage("commercial.business.editvendor.mailequals", null, lang));
				}
				if (GenericValidator.isBlankOrNull(confirmEmail)) {
					hasErrors = true;
					errors.append("#" + this.messageSource.getMessage("commercial.business.editvendor.mailempty", null, lang));
				}
				if (!GenericValidator.isEmail(preferences.getEmail())) {
					hasErrors = true;
					errors.append("#" + this.messageSource.getMessage("commercial.business.editvendor.mailformat", null, lang));
				}
				if (!preferences.getZipcode().matches("(?!([0123456789])\\1{4})\\d{5}")) {
					hasErrors = true;
					errors.append("#" + this.messageSource.getMessage("commercial.business.editvendor.nozip", null, lang));
				}
				
				if (hasErrors) {
					sb.append("hasErrors~" + hasErrors + "|" + "errorList~" + errors.toString() + "|");
				} else {
					EditVendorPayeeResponse responseEdit = customerService.modifyVendorPayeePreferences(customerProfile,
						bankingSession, payee, preferences);
					
					if(responseEdit.getResult().toString() == "SUCCESS"){
						if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
							sb.append("successMessage~Vendor information was edited.|");
						else
							sb.append("successMessage~Ha editado la informaci&oacute;n del comercio.|");
					}
					
					sb.append("status~" + responseEdit.getResult() + "|");
					sb.append("newNickname~" + preferences.getNickname() + "|");
				}
				
			}
			
		}
		model.put("text", sb.toString());
		return new ModelAndView("text", model);
	}
	
	public ModelAndView addVendorPayee(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		BankingSession bankingSession = userSession.getBankingSession();
		StringBuilder sb = new StringBuilder();
		AddExistingPayeeBean payeeBean = new AddExistingPayeeBean();
		String payeeId = request.getParameter("payee");
		payeeBean.setPayee(payeeId);

		/**
		 * MBNA-2662
		 * */
		GlobalPayee global = PaymentsFacade.getInstance().getGlobalPayeeById(new Integer(payeeBean.getPayee()), userSession);
		
		
		PaymentsFacade.getInstance().getGlobalPayeePreference(global, userSession);

		AddPayeeResponse res;
		if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			userSession.getGuidedSetupSession().setResubmitParent(true);
		}

		if (!userSession.isEbpp2UserIdLookedUp()) {
			EbppEntitlement ent = null;
			if (customerProfile.getEntitlementByType(EntitlementType.EBILLS) != null) {
				ent = new EbppEntitlement(customerProfile.getEntitlementByType(EntitlementType.EBILLS));

				// If the username is blank, we need to look in the current EBPP
				// accounts ONLY
				if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
					userSession.setEbpp2UserId("");
				} else {
					userSession.setEbpp2UserId(ent.getUsername());
				}
				userSession.setEbpp2UserIdLookedUp(true);
			}
		}
		
		
		String nick = request.getParameter("nickname");
		if (nick == null || nick.equals(""))
			payeeBean.setNickname(global.getName());
		else
			payeeBean.setNickname(nick);

		String billingAct = request.getParameter("billingNumber");
		if (billingAct != null){
			payeeBean.setBillingNumber(billingAct);
			payeeBean.setBillingNumberConf(billingAct);
		}
		String vendorName = request.getParameter("vendorName");	
		
		if (vendorName != null)
			payeeBean.setVendorName(vendorName);

		String vendorId = request.getParameter("vendorId");
		if (vendorId != null){
			payeeBean.setVendorId(vendorId);
			payeeBean.setVendorIdConf(vendorId);
		}

		String routingNumber = request.getParameter("routingNumber");
		if (routingNumber != null)
			payeeBean.setEmployeeRoutingNumber(routingNumber);

		String bankName = request.getParameter("bank_Name");
		if (bankName != null)
			payeeBean.setBank(bankName);

		String accountType = request.getParameter("accounttype");
		if (accountType != null)
			payeeBean.setAccounttype(accountType);
		
		String city = request.getParameter("city");
		if (city != null)
			payeeBean.setCity(city);
		
		String address = request.getParameter("address");
		if (address != null)
			payeeBean.setAddress(address);
		
		String state = request.getParameter("state");
		if (state != null)
			payeeBean.setState(state);
		
		String zipCode = request.getParameter("zipCode");
		if (zipCode != null)
			payeeBean.setZipcode(zipCode);
		
		String comments = request.getParameter("comments");
		if (comments != null)
			payeeBean.setComments(comments);
		
		String email = request.getParameter("email");
		if (email != null){
			payeeBean.setEmail(email);
			payeeBean.setEmailConf(email);
		}
		
		Map<Object, Object> vendorPayee = new HashMap<>();
		vendorPayee.put("vendorName", vendorName);
		vendorPayee.put("vendorId", vendorId);			
		vendorPayee.put("accountType", accountType);
		vendorPayee.put("billingAct", billingAct);
		vendorPayee.put("routingNumber", routingNumber);
		vendorPayee.put("bankName", bankName);
		vendorPayee.put("nick", nick);
		vendorPayee.put("city", city);
		vendorPayee.put("address", address);
		vendorPayee.put("state", state);
		vendorPayee.put("zipCode", zipCode);
		vendorPayee.put("comments", comments);
		vendorPayee.put("email", email);
		
		res = customerService.addCustomerVendorWithoutEbillWithErrorDescription(customerProfile, bankingSession, global, Utils.generateCustomerInteractionEvent(request), vendorPayee, isHighRiskPayee(global));
		if(res.getResult() == Result.SUCCESS) {	
			customerService.sendVendorConfirmation(customerProfile, userSession.getPreferredLanguage(), vendorPayee, 
					userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)), customerProfile.hasEntitlement(EntitlementType.ALERTS));
			
			
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")){
				sb.append("successMessage~The vendor was successfully added! You may add another vendor on this screen.|");
			} else {
				sb.append("successMessage~El suplidor fue a&ntilde;adido exitosamente! Puede a&ntilde;adir otro suplidor desde esta pantalla|");
			}
			sb.append("status~successful|");
			userSession.resetAccounts();
			userSession.resetPayees();
		}
		else if(res.getResult() == Result.ERROR){
			sb.append("status~failed|");
			String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request).toLowerCase(),
					res.getErrorString());
			sb.append("errorString~" + er + "|");
			String error = "";
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				sb.append("errorMessage~Payee " + nick + " - " + billingAct + " wasn't added.|");
			} else {
				sb.append("errorMessage~No A&ntilde;adimos su comercio " + nick + " - " + billingAct + "|");
			}
			if (er == null || er.equals("")) {
				userSession.setErrorDetail(res.getErrorString());
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					error = "Unable to add your payee " + nick + " - " + billingAct + " at this time."
							+ "<a  href='javascript:Show()'> Learn More</a>";
					sb.append("errorMessage~" + error + "|");
					sb.append("errorCode~Error Code: " + res.getErrorEBA() + "|");
				} else {
					error = "Error a&ntilde;adiendo su comercio " + nick + " - " + billingAct + " en este momento."
							+ "<a  href='javascript:Show()'> Conoce M&aacute;s</a>";
					sb.append("errorMessage~" + error + "|");
					sb.append("errorCode~C&oacute;digo de error: " + res.getErrorEBA() + "|");
				}
			} else {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
					error = "Unable to add your payee " + nick + " - " + billingAct + " at this time."
							+ "<a  href='javascript:Show()'> Learn More</a>";
					sb.append("errorMessage~" + error + "|");
					sb.append("errorCode~Error Code: " + res.getErrorEBA() + "|");
				} else {
					error = "Error a&ntilde;adiendo su comercio " + nick + " - " + billingAct + " en este momento."
							+ "<a  href='javascript:Show()'> Conoce M&aacute;s</a>";
					sb.append("errorMessage~" + error + "|");
					sb.append("errorCode~C&oacute;digo de error: " + res.getErrorEBA() + "|");
				}
			}
		}
		WebUtils.setSessionAttribute(request, "userSession", userSession);
		model.put("text", sb.toString());
		return new ModelAndView("text", model);
	}
	

	public ModelAndView addEpayrollPayee(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		BankingSession bankingSession = userSession.getBankingSession();
		StringBuilder sb = new StringBuilder();
		AddExistingPayeeBean payeeBean = new AddExistingPayeeBean();
		String payeeId = request.getParameter("payee");
		payeeBean.setPayee(payeeId);

		GlobalPayee global = PaymentsFacade.getInstance().getGlobalPayeeById(new Integer(payeeBean.getPayee()), userSession);
		

		PaymentsFacade.getInstance().getGlobalPayeePreference(global, userSession);

		AddPayeeResponse res;
		if (userSession.getGuidedSetupSession().isGuidedSetupMode()) {
			userSession.getGuidedSetupSession().setResubmitParent(true);
		}

		if (!userSession.isEbpp2UserIdLookedUp()) {
			EbppEntitlement ent = null;
			if (customerProfile.getEntitlementByType(EntitlementType.EBILLS) != null) {
				ent = new EbppEntitlement(customerProfile.getEntitlementByType(EntitlementType.EBILLS));

				// If the username is blank, we need to look in the current EBPP
				// accounts ONLY
				if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
					userSession.setEbpp2UserId("");
				} else {
					userSession.setEbpp2UserId(ent.getUsername());
				}
				userSession.setEbpp2UserIdLookedUp(true);
			}
		}

		String nick = request.getParameter("nickname");
		if (nick == null || nick.equals(""))
			payeeBean.setNickname(global.getName());
		else
			payeeBean.setNickname(nick);

		String billingAct = request.getParameter("billingNumber");
		if (billingAct != null){
			payeeBean.setBillingNumber(billingAct);
			payeeBean.setBillingNumberConf(billingAct);
		}
		
		String employeeName = request.getParameter("employeeName");
		if (employeeName != null)
			payeeBean.setEmployeeName(employeeName);

		String employeeId =  request.getParameter("employeeId");
		if (employeeId != null){
			payeeBean.setEmployeeId(employeeId);
			payeeBean.setEmployeeIdConf(employeeId);
		}

		String employeeRoutingNumber = request.getParameter("employeeRoutingNumber");
		if (employeeRoutingNumber != null)
			payeeBean.setEmployeeRoutingNumber(employeeRoutingNumber);

		String bankName = request.getParameter("bank_Name");
		if (bankName != null)
			payeeBean.setBank(bankName);

		String accountType = request.getParameter("accounttype");
		if (accountType != null)
			payeeBean.setAccounttype(accountType);

		boolean verifyPass = true;
		if (!payeeId.equals(epayrollId)
				|| (payeeId.equals(epayrollId)
						&& (!userSession.getCustomerProfile().useCommercialInterface()))
				|| (payeeId.equals(epayrollId)
						&& (userSession.getCustomerProfile().useCommercialInterface()))) {
			res = customerService.addCustomerPayeeWithoutEbillWithErrorDescription(customerProfile,	bankingSession, global, billingAct, nick, Utils.generateCustomerInteractionEvent(request), employeeName, 
					employeeId, employeeRoutingNumber, accountType, true, isHighRiskPayee(global));
		} else {
			res = new AddPayeeResponse();
			verifyPass = false;
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
				sb.append("errorMessage~Payee " + nick + " - " + billingAct + " wasn't added.|");
			} else {
				sb.append("errorMessage~No A&ntilde;adimos su comercio " + nick + " - " + billingAct + "|");
			}
		}
		if (res.getErrorCode() == 0 && verifyPass) {
			if (payeeId.equals(epayrollId))
				if (userSession.getCustomerProfile().useCommercialInterface() == true) {
					if (userSession.getCustomerProfile().useCommercialInterface() == true) {
						
						Map<Object, Object> models = new HashMap<>();
						models.put("idEmployee", payeeBean.getEmployeeId());
						models.put("employeeName", payeeBean.getEmployeeName());
						models.put("nickName", payeeBean.getNickname());
						models.put("rt", payeeBean.getEmployeeRoutingNumber());
						models.put("employeeCan", payeeBean.getBillingNumber());
						if (request.getParameter("bank_Name") != null) {
							bankName = request.getParameter("bank_Name");
						}	
						models.put("rtName", bankName);

						userSession.resetPayees();
						//SMS logic moved inside this generic method
						customerService.sendEPayrollConfirmation(customerProfile, userSession.getPreferredLanguage(),
								models, userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)));
					}
				}
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				//sb.append("successMessage~The employee " + nick + " was successfully added! You may add another employee on this screen.|");
				sb.append("successMessage~The employee was successfully added! You may add another employee on this screen.|");
			else
				//sb.append("successMessage~El empleado fue " + nick + " a&ntilde;adido exitosamente! Puede a&ntilde;adir otro empleado desde esta pantalla|");
				sb.append("successMessage~El empleado fue a&ntilde;adido exitosamente! Puede a&ntilde;adir otro empleado desde esta pantalla|");
			sb.append("status~successful|");
			userSession.resetAccounts();
		} else {
			sb.append("status~failed|");
			String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request).toLowerCase(),
					res.getErrorString());
			sb.append("errorString~" + er + "|");
			String error = "";
			if (verifyPass) {
				if (er == null || er.equals("")) {
					userSession.setErrorDetail(res.getErrorString());
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						error = "Unable to add your payee " + nick + " - " + billingAct + " at this time."
								+ "<a  href='javascript:Show()'> Learn More</a>";
						sb.append("errorMessage~" + error + "|");
						sb.append("errorCode~Error Code: " + res.getErrorEBA() + "|");
					} else {
						error = "Error a&ntilde;adiendo su comercio " + nick + " - " + billingAct + " en este momento."
								+ "<a  href='javascript:Show()'> Conoce M&aacute;s</a>";
						sb.append("errorMessage~" + error + "|");
						sb.append("errorCode~C&oacute;digo de error: " + res.getErrorEBA() + "|");
					}
				} else {
					if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
						error = "Unable to add your payee " + nick + " - " + billingAct + " at this time."
								+ "<a  href='javascript:Show()'> Learn More</a>";
						sb.append("errorMessage~" + error + "|");
						sb.append("errorCode~Error Code: " + res.getErrorEBA() + "|");
					} else {
						error = "Error a&ntilde;adiendo su comercio " + nick + " - " + billingAct + " en este momento."
								+ "<a  href='javascript:Show()'> Conoce M&aacute;s</a>";
						sb.append("errorMessage~" + error + "|");
						sb.append("errorCode~C&oacute;digo de error: " + res.getErrorEBA() + "|");
					}
				}
			}
		}
		WebUtils.setSessionAttribute(request, "userSession", userSession);
		model.put("text", sb.toString());
		return new ModelAndView("text", model);
	}

	private boolean isHighRiskPayee(GlobalPayee global) {
		boolean isHighRiskPayee = false;
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1701) && global.getPreference() != null) {
			isHighRiskPayee = global.getPreference().getHighRisk();
		}
		return isHighRiskPayee;
	}
	
	private String getTelepagoErrorDescriptor(String codigo, String len, String errorCode) {
		String status = "";
		try {
			if (!"en".equals(len)) 
				len = "es";

			status = this.messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
		} catch (Exception e) {
			status = errorCode;
		}

		return status;
	}
	
	public ModelAndView loadAccountInformation(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Map<String, Object> model = new HashMap<>();
		StringBuilder accountInfo = new StringBuilder();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String accountNumber = request.getParameter("account");
		TVFrontendAccount account = Utils.getAccountById(accountNumber, userSession.getAccounts());
		AbstractAccount accountDetails = userSession.getCustomerService().getAccountDetails(userSession.getCustomerProfile(),
				userSession.getBankingSession(), account, true);
		DepositAccount accountAux = (DepositAccount)accountDetails.getBackendObject();
		AccountRecord record = accountAux.getRecords(); 
		if(record != null){
			accountInfo.append("accountNumber~" + accountAux.getAccountID() + "|");
			Owner []owners = record.getOwners();
			if(owners != null && owners.length > 0){
				for(Owner ow : owners){
					if(ow.getTinInfo().getTaxID().equalsIgnoreCase(userSession.getCustomerProfile().getTaxId())){
						for (String relationType : relationshipType) {
							String []relationSplit = relationType.split("-");
							String relation = relationSplit[0];
							if (ow.getRelationshipType().equalsIgnoreCase(relation)) 
								accountInfo.append("description~" + record.getProductDescription().toUpperCase());
						}
					}
				}
			}
		}
 		model.put("text", accountInfo.toString());
		return new ModelAndView("text", model);
	}
	
	public ModelAndView loadAthPlastics(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		StringBuilder cards = new StringBuilder();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String accountNumber = request.getParameter("account1");
		TVFrontendAccount account = Utils.getAccountById(accountNumber, userSession.getAccounts());
		AccountMaskUtil maskutil = new AccountMaskUtil();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			cards.append("<option value='none'>Select an ATH\u00AE Card</option>");
		else
			cards.append("<option value='none'>Seleccione una Tarjeta ATH\u00AE</option>");
		
		CardPlasticFrontend[] plastics = userSession.getCardPlastics(account);
		
		if (plastics != null && plastics.length > 0){
			for (int i = 0; i < plastics.length; i++) {
				String cardType;
				if (plastics[i].getCardPlasticAccountRecord().getCardPANId().startsWith("0215")) {
					cardType = "ATH\u00AE Regular";
				} else if (plastics[i].getCardPlasticAccountRecord().getCardPANId().startsWith("4549")) {
					cardType = "ATH\u00AE Internacional";
				} else if (plastics[i].getCardPlasticAccountRecord().getCardPANId().startsWith("60119052")) {																					// Discover
					cardType = "ATH \u002D Discover";
				} else {
					cardType = "";
				}
				cards.append("<option value='" + plastics[i].getFrontEndId() + "'>" + cardType + " " + maskutil.mask(plastics[i].getCardPlasticAccountRecord().getCardPANId())+ "</option>");
			}
		}
		model.put("text", cards.toString());
		return new ModelAndView("text", model);
	}
	
	public ModelAndView loadActiveAthPlastics(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		StringBuilder cards = new StringBuilder();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String accountNumber = request.getParameter("account1");
		TVFrontendAccount account = Utils.getAccountById(accountNumber, userSession.getAccounts());
		AccountMaskUtil maskutil = new AccountMaskUtil();
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
			cards.append("<option value='none'>Select an ATH\u00AE Card</option>");
		else
			cards.append("<option value='none'>Seleccione una Tarjeta ATH\u00AE</option>");
		
		CardPlasticFrontend[] plastics = userSession.getCardPlastics(account);
		
		if (plastics != null && plastics.length > 0){
			for (int i = 0; i < plastics.length; i++) {
				String cardType;
				if (plastics[i].getCardPlasticAccountRecord().getCardPANId().startsWith("0215")) {
					cardType = "ATH\u00AE Regular";
				} else if (plastics[i].getCardPlasticAccountRecord().getCardPANId().startsWith("4549")) {
					if(plastics[i].getCardPlasticAccountRecord().getStatus().trim().equals("INACTIVE")){
						continue;
					}
					cardType = "ATH\u00AE Internacional";
				} else if (plastics[i].getCardPlasticAccountRecord().getCardPANId().startsWith("60119052")) {																					// Discover
					cardType = "ATH \u002D Discover";
				} else {
					cardType = "";
				}
				cards.append("<option value='" + plastics[i].getFrontEndId() + "'>" + cardType + " " + maskutil.mask(plastics[i].getCardPlasticAccountRecord().getCardPANId())+ "</option>");
			}
		}
		model.put("text", cards.toString());
		return new ModelAndView("text", model);
	}
	
	public ModelAndView sendBankerEmail(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String emailSent = "F";

		if (userSession.isWealth(this.wealthCodes, this.preferredCodes)) {
			CustomerProfile customerProfile = userSession.getCustomerProfile();
			CustomerWealthPreferred custWealthPreferred = userSession.getCustomerWealthPreferred(this.wealthCodes,
					this.preferredCodes);

			if (custWealthPreferred.getPrivateBankerNum() != null
					&& !custWealthPreferred.getPrivateBankerNum().trim().equals("")) {
				String subject = (String) request.getParameter("privateBankerEmailSubject");
				String message = (String) request.getParameter("privateBankerEmailMessage");

				if (!GenericValidator.isBlankOrNull(subject) & !GenericValidator.isBlankOrNull(message)) {
					PrivateBankerEmail email = new PrivateBankerEmail();
					email.setMessage(message);
					email.setSubject(subject.replaceAll("\n", ""));

					EmployeeInfoResponse employeeInfo = userSession.getPrivateBankerInfo(custWealthPreferred
							.getPrivateBankerNum());
					if (employeeInfo != null && !employeeInfo.getEmail().trim().equals("")) {
						if (customerService.sendPrivateBankerEmail(customerProfile, email, employeeInfo,
								userSession.getPreferredLanguage())) {
							emailSent = "T";
						}
					}
				} else {
					emailSent = "M";
				}
			}
		}

		model.put("text", emailSent);

		return new ModelAndView("text", model);
	}

	// FFEIC
	public ModelAndView newDevice(HttpServletRequest request, HttpServletResponse response) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		Map<String, Object> model = new HashMap<>();

		String header = request.getHeader("user-agent");

		if (!userSession.isDeviceChecked()) {

			boolean hasalert = false;
			AlertsEntitlement ent = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));
			if (ent != null && ent.getCustomerEntitlement() != null) {
				Set<AlertType> alerts = ent.getAlerts();
				if (alerts.contains(AlertType.NEW_DEVICE_EMAIL) || alerts.contains(AlertType.NEW_DEVICE_SMS)
						|| alerts.contains(AlertType.NEW_DEVICE_PUSH)) {
					hasalert = true;
				}
			}

			if (hasalert) {

				Cookie deviceCookie = null;
				Cookie[] cookies = request.getCookies();
				if (cookies != null) {
					for (int i = 0; i < cookies.length; i++) {
						if (cookies[i].getName().equalsIgnoreCase(DeviceUtils.DEVICE_TOKEN_COOKIE)) {
							deviceCookie = cookies[i];
							break;
						}
					}
				}

				String userAgent = null;
				try {
					userAgent = profile.getSignonStats().getString(deviceCookie.getValue());
					if (!userAgent.equals(header)) {
						throw new Exception("NEW USER-AGENT");
					}
				} catch (Exception e) {

					this.customerService.sendNewDeviceConfirmation(profile,
							userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
							userSession.getPreferredLanguage());

					if (profile.getSignonStats().getAttributeData() == null) {
						profile.getSignonStats().resetProperties();
					}
					String token;
					if (deviceCookie != null && deviceCookie.getValue() != null) {
						token = deviceCookie.getValue();
					} else {
						token = RandomUtils.generateRandomUniqueString();
						CookieUtils.addCookie(response, 60 * 60 * 24 * 365, DeviceUtils.DEVICE_TOKEN_COOKIE, token);
					}
					if (userAgent != null) {
						profile.getSignonStats().removeProperty(token);
					}
					profile.getSignonStats().putString(token, header);
					/*** MBNA-3124  */
					CustomerFacade.getInstance().updateCustomerSignonStats(profile.getSignonStats(), userSession);
				}
			}
			userSession.setDeviceChecked(true);
		}

		return new ModelAndView("text", model);
	}

	public ModelAndView getCreditCardUrl(HttpServletRequest request, HttpServletResponse response) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<String, Object> model = new HashMap<>();

		String profileCountry = "PR";
		String actionSelected = request.getParameter("action");
		String textIdent = request.getParameter("selectionId").replace("PROD", actionSelected);

		if (userSession.hasVirginIslandAccount())
			profileCountry = "VI";
		else if (userSession.hasBritishVirginIslandAccount())
			profileCountry = "TORT";

		ConfigurableTextSubSections subSection = ConfigurableTextSubSections.valueOf("CREDIT_" + actionSelected
				+ "_LINK_" + profileCountry);

		String urlValue = userSession.getDBConfiguredText(CIBPConfigurableTextsTypes.UNICA, subSection, textIdent);
		model.put("text", urlValue);
		return new ModelAndView("text", model);
	}

	public void acceptUnicaOffer(HttpServletRequest request, HttpServletResponse response) throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String applicationId = request.getParameter("applicationId");
		UnicaClientConfirmationResponse unicaResponse = customerService.acceptCreditOffer(applicationId);
		if (unicaResponse != null && "00".equals(unicaResponse.getDPStatus().getStatusCode())
				&& unicaResponse.getResponseStatus().equals(ResponseStatus.SUCCESS)
				&& "0".equals(unicaResponse.getAcapsResponse().getResponseCode().toString())) {
			customerService.acceptOffer(userSession.getCustomerProfile().getProfileId(),userSession.getCustomerProfile().getTaxId(), applicationId);
			userSession.setOfferAcceptance(applicationId);

			if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
				userSession.setStatusmessage("The offer was accepted.");
			else
				userSession.setStatusmessage("La oferta fue aceptada.");

			userSession.getCustomerService().logAcceptUnicaOffer(
					Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));
		} else {
			if ("EN".equalsIgnoreCase(Utils.getRSALanguage(request)))
				userSession.setStatusmessage("There was an error in the acceptance process. Please try again later.");
			else
				userSession.setStatusmessage("Hubo un error en el proceso. Favor intentar m&aacute;s tarde.");

			userSession.getCustomerService().logAcceptUnicaOfferError(
					Utils.generateCustomerInteractionEvent(request, userSession.getUsername()));
		}
	}

	// FIN FFEIC

	// CIBP-2786
	// check once if the entitlement and the non-editable alerts exist
	public ModelAndView ffeicAlertsCheck(HttpServletRequest request, HttpServletResponse response) throws Exception {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();
		Map<String, Object> model = null;
		AlertsEntitlement alert = null;

		if (!userSession.isAlertsChecked()) {
			// entitlement doesnt exist
			if (!profile.hasEntitlement(EntitlementType.ALERTS)) {
				alert = new AlertsEntitlement();
				Set<AlertType> types = new HashSet<AlertType>();
				types.add(AlertType.CHANGE_PASS_EMAIL);
				types.add(AlertType.QUESTIONS_EDIT_EMAIL);
				types.add(AlertType.EMAIL_EDIT_EMAIL);
				types.add(AlertType.REGAIN_ACCESS_EMAIL);
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT315))
					types.add(AlertType.NEW_FINGERPRINT_EMAIL);
				alert.setAlerts(types);
				this.customerService.addEntitlement(profile, alert.getCustomerEntitlement());
			} else {
				alert = new AlertsEntitlement(profile.getEntitlementByType(EntitlementType.ALERTS));
				// entitlement exists but alerts are null
				if (alert.getAlerts() == null) {
					Set<AlertType> types = new HashSet<AlertType>();
					types.add(AlertType.CHANGE_PASS_EMAIL);
					types.add(AlertType.QUESTIONS_EDIT_EMAIL);
					types.add(AlertType.EMAIL_EDIT_EMAIL);
					types.add(AlertType.REGAIN_ACCESS_EMAIL);
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT315))
						types.add(AlertType.NEW_FINGERPRINT_EMAIL);
					alert.setAlerts(types);
					CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(alert.getCustomerEntitlement()));
				}
				// entitlement exist, alerts data are not null
				// check if non-editable alerts are already checked
				else {
					boolean update = false;
					Set<AlertType> types = alert.getAlerts();
					if (!types.contains(AlertType.CHANGE_PASS_EMAIL)) {
						types.add(AlertType.CHANGE_PASS_EMAIL);
						update = true;
					}
					if (!types.contains(AlertType.QUESTIONS_EDIT_EMAIL)) {
						types.add(AlertType.QUESTIONS_EDIT_EMAIL);
						update = true;
					}
					if (!types.contains(AlertType.EMAIL_EDIT_EMAIL)) {
						types.add(AlertType.EMAIL_EDIT_EMAIL);
						update = true;
					}
					if (!types.contains(AlertType.REGAIN_ACCESS_EMAIL)) {
						types.add(AlertType.REGAIN_ACCESS_EMAIL);
						update = true;
					}
					if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT315)) {
						if (!types.contains(AlertType.NEW_FINGERPRINT_EMAIL)) {
							types.add(AlertType.NEW_FINGERPRINT_EMAIL);
							update = true;
						}
					}
					if (update) {
						alert.setAlerts(types);
						CustomerFacade.getInstance().updateEntitlement(userSession, Collections.singletonList(alert.getCustomerEntitlement()));
					}
				}
			}
			userSession.setAlertsChecked(true);
		}
		return new ModelAndView("text", model);
	}

	// FIN CIBP-2786

	public ModelAndView verifyRTPayment(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// Call from portal ebppinbox, with database information
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		List<TVFrontEndPayee> payees = userSession.getPayees(false);

		String id = (String) request.getParameter("id");
		String isRTPayment = "N";

		TVFrontEndPayee payee = Utils.getPayeebyId(id, payees);
		if (payee != null && payee.getGlobalPayee() != null && payee.getGlobalPayee().getRtIndicator() != null
				&& payee.getGlobalPayee().getRtIndicator().equalsIgnoreCase("Y"))
			isRTPayment = "Y";
		else
			isRTPayment = "N";

		model.put("text", isRTPayment);

		return new ModelAndView("text", model);
	}

	public ModelAndView ebpp2PreviousHistoryInfoEbills(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		// Call from portal ebppinbox, with database information
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String id = (String) request.getParameter("id");

		if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

			// Get the EBPP2 account and summary information
			List<UserAccount> ebpp2AccountsAndSummaries = userSession.getEbpp2UserAccountsAndSummaries();

			// Loop through until we find the correct account
			if (ebpp2AccountsAndSummaries != null && ebpp2AccountsAndSummaries.size() > 0) {
				for (int i = 0; i < ebpp2AccountsAndSummaries.size(); i++) {

					// When we find the account, verify if we have set up the
					// history information
					if (ebpp2AccountsAndSummaries.get(i).getBillerAccountID() == Integer.parseInt(id)) {
						model.put("ebppPreviousHistory", ebpp2AccountsAndSummaries.get(i).getAccountStatements());
						model.put("last4AcctNumber", ebpp2AccountsAndSummaries.get(i).getLast4Digits());
						model.put("ebpp2UserAccountId", ebpp2AccountsAndSummaries.get(i).getBillerAccountID());
						break;
					}
				}
			}
		}
		/*
		 * 11/11/2013 Alizano Fix EBPP2 Mobile error if
		 * (userSession.isClienMobileVersion()) return new
		 * ModelAndView("ebppPreviousHistoryInfoEbills", model); else
		 */
		return new ModelAndView("ebpp2PreviousHistoryInfoEbills", model);
	}

	public ModelAndView detailCCPortal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();

		String id = (String) request.getParameter("id");
		TVFrontendAccount account = Utils.getAccountById(id, userSession.getAccounts());

		if (aadvantageProducts.indexOf(account.getAccountProductId()) != -1)
			model.put("aaMiles", true);
		else
			model.put("aaMiles", false);

		if (userSession.hasVirginIslandAccount())
			model.put("profileCountry", "vi");
		else if (userSession.hasBritishVirginIslandAccount())
			model.put("profileCountry", "bvi");
		else
			model.put("profileCountry", "pr");

		// The EBPP2 user id is not yet in session; add it
		if (!userSession.isEbpp2UserIdLookedUp()) {
			EbppEntitlement ent = null;
			if (profile.getEntitlementByType(EntitlementType.EBILLS) != null) {
				ent = new EbppEntitlement(profile.getEntitlementByType(EntitlementType.EBILLS));

				// If the username is blank, we need to look in the current EBPP
				// accounts ONLY
				if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
					userSession.setEbpp2UserId("");
				} else {
					userSession.setEbpp2UserId(ent.getUsername());
				}
				userSession.setEbpp2UserIdLookedUp(true);
			}
		}
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)){
			try{
				if(account.getIsAvailableElecStmt()) {//fix only inquiry if IsAvailableElecStmt
					CustomerInquiryBean responseTsys =  customerService.prophItService()
							.makeCustomerInquiry(account.getAccountNumber());
					if (responseTsys != null) {
						if (responseTsys.getEbillInd().equals(EbillIndType.ACTIVE.toString())) {
							account.setStmtDisposition("E");
							account.setAvailableElecStmt(true);
						}else if(responseTsys.getEbillInd().equals(EbillIndType.ELIGIBLE.toString())){
							account.setStmtDisposition("M");
							account.setAvailableElecStmt(true);
						}else if(responseTsys.getEbillInd().equals(EbillIndType.NOELIGIBLE.toString())){							
							account.setAvailableElecStmt(false);
						}
					}
				}
				if(account.getStatements().size() > 1){
					model.put("statementID", account.getStatements().get(1).getPdfID());
				}
				model.put("account", account);
			} catch (Exception e) {
				logger.error("Tsys Inq Error");
				logger.error(e);
			}
		}else 
		// The user has an EBPP2 ID
		if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

			if (customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED
					&& this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
							.equals(EntitlementStatus.ENABLED.toString())) {

				boolean foundAccount = false;

				// Loop through all the accounts and match them with the proper
				// userAccount
				account.setEbpp2UserAccount(Utils.getEbpp2UserAccountsByAccount(account.getAccountNumber(), userSession));
				if (account.getEbpp2UserAccount() != null)
					foundAccount = true;

				model.put("account", account);

				// Do we have a statement to show?
				if (foundAccount) {
					if (account.getEbpp2UserAccount().getAccountStatements() != null
							&& account.getEbpp2UserAccount().getAccountStatements().size() > 0)
						model.put("statementID", account.getEbpp2UserAccount().getAccountStatements().get(0)
								.getStatementID());
				}

			}
		}
		
		
		
		model.put("eBillsFlag", StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS));
		
		AbstractAccount accountDetails = userSession.getCustomerService().getAccountDetails(
				userSession.getCustomerProfile(), userSession.getBankingSession(), account, false);
		model.put("accountDetails", accountDetails);

		if (!userSession.getAuthorizations().containsKey(id)) {
			userSession.getAuthorizations(id, account);
		}
		if (userSession.getAuthorizations().get(id) != null
				&& userSession.getAuthorizations().get(id).getCurrentBalance() != null) {
			model.put("currentBalance", userSession.getAuthorizations().get(id).getCurrentBalance());
		}
		
		model.put("unicaBaseUrl", unicaBaseUrl);
		
		return new ModelAndView("detailCCPortal", model);
	}

	/**
	 * Method to update PermId with information requested from NTIP, used to keep PermID up to date and usable for nickname process in email notification.
	 * @param request
	 * @param response
	 * @author et55305
	 */
	public void renewPermID(HttpServletRequest request, HttpServletResponse response){
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		if (userSession.getCustomerProfile() != null && userSession.getRenewPermId()) {			
			userSession.getCustomerService().updateCustomerPermId(userSession.getCustomerProfile(),userSession.getBankingSession().getPermId());
			userSession.setRenewPermId(false);			
		}
	}
	
	
	public ModelAndView evaluateCCAOwner(HttpServletRequest request, HttpServletResponse response) throws Exception{
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String ccaNotofUser = "F";
		String billerNumber = (String) request.getParameter("billerNumber"); 
		List<TVFrontendAccount> ccaAvailbleToEbill = userSession.getCcaAccounts();
		
		if(!ccaAvailbleToEbill.isEmpty()){
			for (Iterator<TVFrontendAccount> iter = userSession.getCcaAccounts().iterator(); iter.hasNext();){
				TVFrontendAccount ccaAccount = (TVFrontendAccount) iter.next();
				if(billerNumber.equals(ccaAccount.getAccountNumber()) && ccaAccount.getIsAvailableElecStmt()){
					ccaNotofUser = "T";
					break;
				}
			}
		}
		
		return new ModelAndView("text", "text", ccaNotofUser); 
	}

	
	public ModelAndView detailDEPPortal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile profile = userSession.getCustomerProfile();

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession != null && userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		String id = (String) request.getParameter("id");
		TVFrontendAccount account = Utils.getAccountById(id, userSession.getAccounts());
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS))
		{
			if (customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED
					&& this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
							.equals(EntitlementStatus.ENABLED.toString())) {
					
					TVFrontendDepositAccount idaAccount = Utils.getIdaAccountById(id, userSession.getIdaAccounts());
					model.put("idaAccount", idaAccount);
					model.put("account", account);

					if(account.getStatements().size() > 1 && !account.getStatements().get(1).getPdfID().isEmpty()){
						model.put("statementID", account.getStatements().get(1).getPdfID());
					}				
				
					model.put("eBillsFlag", StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS));
			}
		}
		else{
		// The EBPP2 user id is not yet in session; add it
			if (!userSession.isEbpp2UserIdLookedUp()) {
				EbppEntitlement ent = null;
				if (profile.getEntitlementByType(EntitlementType.EBILLS) != null) {
					ent = new EbppEntitlement(profile.getEntitlementByType(EntitlementType.EBILLS));
	
					// If the username is blank, we need to look in the current EBPP
					// accounts ONLY
					if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
						userSession.setEbpp2UserId("");
					} else {
						userSession.setEbpp2UserId(ent.getUsername());
					}
					userSession.setEbpp2UserIdLookedUp(true);
				}
			}
	
			// The user has an EBPP2 ID
			if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {
	
				if (customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED
						&& this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
								.equals(EntitlementStatus.ENABLED.toString())) {
	
					boolean foundAccount = false;
	
					// Loop through all the accounts and match them with the proper
					// userAccount
					account.setEbpp2UserAccount(Utils.getEbpp2UserAccountsByAccount(account.getAccountNumber(), userSession));
					if (account.getEbpp2UserAccount() != null)
						foundAccount = true;
	
					model.put("account", account);
	
					// Do we have a statement to show?
					if (foundAccount) {
						if (account.getEbpp2UserAccount().getAccountStatements() != null
								&& account.getEbpp2UserAccount().getAccountStatements().size() > 0)
							model.put("statementID", account.getEbpp2UserAccount().getAccountStatements().get(0)
									.getStatementID());
					}
				}
			}
		}
		AbstractAccount accountDetails = userSession.getCustomerService().getAccountDetails(
				userSession.getCustomerProfile(), userSession.getBankingSession(), account, false);
		model.put("accountDetails", accountDetails);

		return new ModelAndView("detailDEPPortal", model);
	}

	public ModelAndView ebpp2PreviousHistoryInfoEstatements(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// Call from portal ebppinbox, with database information
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String id = (String) request.getParameter("id");

		if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {

			// Get the EBPP2 account and summary information
			List<UserAccount> ebpp2AccountsAndSummaries = userSession.getEbpp2UserAccountsAndSummaries();

			// Loop through until we find the correct account
			if (ebpp2AccountsAndSummaries != null && ebpp2AccountsAndSummaries.size() > 0) {
				for (int i = 0; i < ebpp2AccountsAndSummaries.size(); i++) {

					// When we find the account, verify if we have set up the
					// history information
					if (ebpp2AccountsAndSummaries.get(i).getBillerAccountID() == Integer.parseInt(id)) {
						model.put("ebppPreviousHistory", ebpp2AccountsAndSummaries.get(i).getAccountStatements());
						model.put("last4AcctNumber", ebpp2AccountsAndSummaries.get(i).getLast4Digits());
						model.put("ebpp2ProductBillerProductId", ebpp2AccountsAndSummaries.get(i).getBillerProductId());
						model.put("ebpp2UserAccountId", ebpp2AccountsAndSummaries.get(i).getBillerAccountID());
						break;
					}
				}
			}
		}
		/*
		 * 11/11/2013 Alizano Fix EBPP2 Mobile error if
		 * (userSession.isClienMobileVersion()) return new
		 * ModelAndView("ebppPreviousHistoryInfoEstatements", model); else
		 */
		return new ModelAndView("ebpp2PreviousHistoryInfoEstatements", model);
	}

	public ModelAndView acctPDFStmtPreviousHistoryInfo(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// Call from portal ebppinbox, with database information
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String id = (String) request.getParameter("id");

		List<AccountPDFStmtInfo> stmtPrevHist = null;

		AccountPDFStmtInfo acctInfo = userSession.getPDFInfoById(id);

		if (acctInfo != null)
			model.put("stmtType", acctInfo.getAccount_type());

		if (acctInfo != null) {
			if ("PDFSTMT".equals(acctInfo.getAccount_type())) {
				stmtPrevHist = userSession.getAcctPrevPDFStmtsByAccount(acctInfo.getAccountNumber(),
						this.isEbppAvailable());

				// only 18 month of history will be shown to user
				if (stmtPrevHist != null && stmtPrevHist.size() < 18) {
					String oldestPdf = null;
					// Get Oldest PDF Stmt
					if (stmtPrevHist != null && stmtPrevHist.size() > 0) {
						oldestPdf = stmtPrevHist.get(0).getInvoice_date();
						for (AccountPDFStmtInfo pdfPrevInfo : stmtPrevHist) {
							if (Double.parseDouble(pdfPrevInfo.getInvoice_date().replaceAll("-", "")) < Double
									.parseDouble(oldestPdf.replaceAll("-", ""))) {
								oldestPdf = pdfPrevInfo.getInvoice_date();
							}
						}
					} else {
						oldestPdf = acctInfo.getInvoice_date();
					}
				}
			} else {
				// For IRA/CAE, make sure to include the product ID
				if (ezStatementInformativaIRAQuarterlyProductId.equals(acctInfo.getEzStmtProductId())
						|| ezStatementInformativaCAEQuarterlyProductId.equals(acctInfo.getEzStmtProductId())
						|| ezStatementInformativaIRAYearlyProductId.equals(acctInfo.getEzStmtProductId())
						|| ezStatementInformativaCAEYearlyProductId.equals(acctInfo.getEzStmtProductId())) {

					stmtPrevHist = userSession.getIntPrevPDFStmtsByAccountAndProductId(acctInfo);
					model.put("IRACAE", "true");
				} else {
					if (acctInfo.getEzStmtProductId() != null && acctInfo.getEzStmtProductId() != ""){
						stmtPrevHist = userSession.getIntPrevPDFStmtsByAccountAndProductId(acctInfo);
					} else{
						stmtPrevHist = userSession.getIntPrevPDFStmtsByAccount(acctInfo.getAccountNumber());
					}
					if (Arrays.asList(userSession.getEzStatementMortgageProductIds())
							.contains(acctInfo.getEzStmtProductId())) {
						stmtPrevHist = this.filterMortgageStatements(stmtPrevHist);
					}
				}
					///code was eliminated because other changes took care of situation.
			}
		}

		if (stmtPrevHist != null && !stmtPrevHist.isEmpty()) {
			stmtPrevHist = this.filterAcctStatements(stmtPrevHist);
		}
		// If null add new ebills statements
		else {
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {
				stmtPrevHist = new LinkedList<>();
				//	validate closed and control h accounts				
				List<TVFrontendAccount> accs = new LinkedList<>();
				accs.addAll(userSession.getAccounts());
				if(userSession.getClosedAccounts() != null){
					accs.addAll(userSession.getClosedAccounts());
				}
				if(userSession.getControlHAccounts() != null){
					accs.addAll(userSession.getControlHAccounts());
				}
				TVFrontendAccount account = Utils.accountByPDFID(accs, id);
				if(account == null)
					account= Utils.accountByHash(accs, id);
				//
				model.put("stmtType", "PDFSTMT");
				if (account != null) {
					List<AbstractStatement> statements = Utils.statementsByAccount(account, userSession, request);
					if (statements != null) {
						boolean firstStatement = true;			
						for (Iterator<AbstractStatement> iterator2 = statements.iterator(); iterator2.hasNext();) {
							AbstractStatement statement = iterator2.next();
							if (GenericValidator.isBlankOrNull(statement.getPdfID()) || statement.getPdfID().trim().equals("0")) {
								continue;
							}else if(firstStatement){
								firstStatement = false;//Hide current statement with pdf.
								continue;
							}
							AccountPDFStmtInfo info = new AccountPDFStmtInfo();
							info.setId(statement.getPdfID());
							info.setAccount_type("PDFSTMT");
							info.setAccount_name(account.getNickname());
							info.setAccount_nickname(account.getNickname());
							info.setAccountNumber(account.getAccountNumber());
							info.setAccountFrontendID(account.getFrontEndId());
							info.setDocId(statement.getPdfID());
							if(account.getSubtype() != null && account.getSubtype().equals("CCA")) {
								CreditStatement ccaStmt =  (CreditStatement) statement.getBackendObject();
								if(ccaStmt != null) {
									if(ccaStmt.getDueDt() != null)
										info.setDueDate(ccaStmt.getDueDt().getTime());
									if(ccaStmt.getStmtMinPmt() != null)
										info.setStmtMinPmt(ccaStmt.getStmtMinPmt().toString());
									if(ccaStmt.getClosingBalance() != null)
										info.setClosingBalance(ccaStmt.getClosingBalance().toString());
								}
							}
							if (statement.getEndDate() != null) {
								SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
								info.setInvoice_date(format.format(statement.getEndDate()));
							} else {
								info.setInvoice_date("Fecha");
							}
							info.setEzStmtProductId("N/A");
							stmtPrevHist.add(info);
						}
					}

				}
			}
		}

		if (stmtPrevHist != null && stmtPrevHist.size() > 0)
			model.put("acctPDFStmtPreviousHistory", stmtPrevHist);

		model.put("iraQuarterlyEzStmtProductId", ezStatementInformativaIRAQuarterlyProductId);
		model.put("caeQuarterlyEzStmtProductId", ezStatementInformativaCAEQuarterlyProductId);
		model.put("iraYearlyEzStmtProductId", ezStatementInformativaIRAYearlyProductId);
		model.put("caeYearlyEzStmtProductId", ezStatementInformativaCAEYearlyProductId);

		return new ModelAndView("acctPDFStmtPreviousHistoryInfo", model);
	}

	private List<AccountPDFStmtInfo> filterAcctStatements(List<AccountPDFStmtInfo> stmtList) {
		List<AccountPDFStmtInfo> newList = new LinkedList<AccountPDFStmtInfo>();
		if (stmtList != null && stmtList.size() > 0) {
			boolean found = false;
			for (AccountPDFStmtInfo info : stmtList) {
				found = false;
				for (AccountPDFStmtInfo newInfo : newList) {
					if (info.getDocId().equals(newInfo.getDocId())) {
						found = true;
					}
				}
				if (!found) {
					newList.add(info);
				}
			}
		}
		return newList;
	}
	
	private List<AccountPDFStmtInfo> filterMortgageStatements(List<AccountPDFStmtInfo> stmtList){
		List<AccountPDFStmtInfo> newList = new LinkedList<AccountPDFStmtInfo>();
		if (stmtList != null && stmtList.size() > 0) {
			int count = 0;
			for (Iterator<AccountPDFStmtInfo> iterator = stmtList.iterator(); iterator.hasNext();) {
				AccountPDFStmtInfo intStmtInfo = (AccountPDFStmtInfo) iterator.next();
				if (count < 12){
					newList.add(intStmtInfo);
					count++;
				} else
					return newList;
			}
		}
		return newList;
	}

	public ModelAndView searchAccountStatements(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		LinkedHashMap<String, String> statementsMap = new LinkedHashMap<String, String>();
		String act = (String) request.getParameter("account");
		boolean isNotStatement = false;
		TVFrontendAccount account = null;
		if (GenericValidator.isBlankOrNull(act) || act.equals("none")) {
			isNotStatement = true;
		} else {
			account = Utils.getAccountById(act, userSession.getAccounts());
			model.put("account", account);
			if (account == null || !account.getFeatures().isShowStatement()) {
				isNotStatement = true;
			}
		}
		if (isNotStatement) {
			/*
			 * BGARCIA CIBP-1465 SETEA LA DATE DE CONSULTA POR MEDIO DEL FILTRO
			 * DE FECHAS EN LA PAGINA A LA FECHA ACTUAL
			 */
			Date dl = Calendar.getInstance().getTime();
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			df = new SimpleDateFormat("yyyy");
			model.put("year1", df.format(dl));
			df = new SimpleDateFormat("MM");
			model.put("monthl", df.format(dl));
			df = new SimpleDateFormat("dd");
			model.put("dayl", df.format(dl));
			return new ModelAndView("ajaxAccountStatementsList", model);
		}
		/**
		 * Here is the IDA, CCA statements implementation
		 */
		try {
			List<AbstractStatement> availableStatements = null;
			if (account.getStatements() == null) {
				StatementRequest stmtrq = new StatementRequest(Utils.generateCustomerInteractionEvent(request),
						userSession.getCustomerProfile(), userSession.getBankingSession(), account, 16);
				availableStatements = userSession.getCustomerService().getAvailableStatements(stmtrq)
						.getStatementList();
				account.setStatements(availableStatements);
			} else
				availableStatements = account.getStatements();
			Date dl = Calendar.getInstance().getTime();
			if (availableStatements.size() > 1)
				dl = availableStatements.get(availableStatements.size() - 1).getStartDate();// OBTINE
																							// LA
																							// FECHA
																							// INFERIOR
																							// PARA
																							// REGULAR
																							// LOS
																							// REPORTES
																							// POR
																							// MEDIO
																							// DEL
																							// RANGO
																							// DE
																							// FECHAS
			else {
				/*
				 * BGARCIA CIBP-1465 EN CASO DE SOLO TENER EL CICLO DE RECIENTES
				 * SETEA LA DATE DE CONSULTA POR MEDIO DEL FILTRO DE FECHAS EN
				 * LA PAGINA DESDE EL A�O PASADO Y HASTA LA FECHA ACTUAL
				 */
				Calendar d = Calendar.getInstance();
				d.add(Calendar.YEAR, -1);
				dl = d.getTime();
			}
			/*
			 * CIBP-1465 BGARCIA Dia limite PONE LA FECHA EN EL CONTEXTO
			 */
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			df = new SimpleDateFormat("yyyy");
			model.put("year1", df.format(dl));
			df = new SimpleDateFormat("MM");
			model.put("monthl", df.format(dl));
			df = new SimpleDateFormat("dd");
			model.put("dayl", df.format(dl));

			LinkedHashMap<String, AbstractStatement> accountStatementMap = new LinkedHashMap<String, AbstractStatement>();

			statementsMap = Utils.getStatements(availableStatements, accountStatementMap,
					Utils.getRSALanguage(request), account.getSubtype().equals("PRM"));
			model.put("statementsMap", statementsMap.entrySet());
			return new ModelAndView("ajaxAccountStatementsList", model);

		} catch (Exception e) {
			model.put("statementsMap", statementsMap);
			return new ModelAndView("ajaxAccountStatementsList", model);
		}

	}

	/**
	 * Edit Account Ajaxy
	 */
	public ModelAndView editAccountNickname(HttpServletRequest request, HttpServletResponse response,
			EditAccountBean actbean) throws Exception {

		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		// TODO poner un if para cuando solo queramos actulaizar las cuentas
		CustomerProfile profile = userSession.getCustomerProfile();
		List<TVFrontendAccount> accounts = userSession.getAccounts();
				
		TVFrontendAccount accountById = Utils.getAccountById(actbean.getId(), accounts);
		boolean isvalid = false;
		if (!GenericValidator.isBlankOrNull(actbean.getNickname())
				&& GenericValidator.maxLength(actbean.getNickname(), 30)
				&& GenericValidator.matchRegexp(actbean.getNickname(), "^[A-Za-z\\d-]+(\\s[A-Za-z\\d-]+)*$"))
			isvalid = true;

		if (isvalid) {
			// Accounts facade implementation
			ModifyAccountPreferenceRequest modifyAccountPreferenceRequest = new ModifyAccountPreferenceRequest();
			modifyAccountPreferenceRequest.setAccount(accountById);
			modifyAccountPreferenceRequest.setCustomerProfile(profile);
			modifyAccountPreferenceRequest.setHide(false);
			modifyAccountPreferenceRequest.setNickname(actbean.getNickname());
			modifyAccountPreferenceRequest.setUserSession(userSession);
			AccountsFacade.getInstance().modifyAccountPreference(modifyAccountPreferenceRequest);			
			userSession.resetAccountPreferences();
			model.put("text", actbean.getNickname());
		} else
			model.put("text", accountById.getNickname());

		return new ModelAndView("text", model);
	}

	public ModelAndView holidays(HttpServletRequest request, HttpServletResponse response) throws Exception {

		UserSession s = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = s.getCustomerService();
		Map<String, Object> model = new HashMap<>();
		String hA = "";
		List<Holiday> holidays = customerService.getHolidays();
		if (holidays != null)
			for (Holiday holiday : holidays) {
				hA += "'" + new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(holiday.getDate().getTime()) + "',";
			}
		model.put("text", hA);

		return new ModelAndView("text", model);
	}

	public ModelAndView portalCampaign(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();

		if (customerService.getGlobalEntitlement(EntitlementType.CAMPAIGNS).getStatus() == EntitlementStatus.ENABLED) {

			UserSession s = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

			CustomerProfile customer = null;
			List<ActiveCampaignCustomerView> lsCustCamp = null;
			List<PortalCampaign> lc = new java.util.LinkedList<PortalCampaign>();
			if (s != null) {
				Utils.setHasStatementOrEbill(s);
				// lookup for a customer campaign
				if (!s.getGuidedSetupSession().isGuidedSetupMode()) {
					customer = s.getCustomerProfile();
					if (customer != null && customer.getTaxId() != null
							&& customer.getEnrollmentAccountNumber() != null) {
						try {
							lsCustCamp = customerService.getActiveCustCampaign(customer.getTaxId().trim());
						} catch (Exception e) {
							logger.error("Error getCampaignCustomer", e);
						}
					}
					List<Campaign> list = customerService.getRunnigCampaignByType(CampaignType.PORTAL);

					// List<PortalCampaign> custCam = new
					// java.util.LinkedList<PortalCampaign>();
					cicloGeneral: for (Iterator<Campaign> iterator = list.iterator(); iterator.hasNext();) {

						PortalCampaign p = new PortalCampaign(iterator.next());
						// jocampo
						// IF CUST IS IN EXCLUDE SEGMENT CAMP IS NOT SHOWN
						boolean isExcludeSegment = false;
						if (p.getCampaign().getExcludeSegments() != null
								&& !p.getCampaign().getExcludeSegments().equals("")) {
							if (Utils.hasExcludeSegments(p, s, this.wealthCodes, this.preferredCodes)) {
								return new ModelAndView("portalCampaign", model);
							}
						}
						boolean isInclude = false;
						boolean isIncludeSegment = false;
						if (p.getCampaign().getIncludeSegments() != null
								&& !p.getCampaign().getIncludeSegments().equals("<properties/>")) {
							isInclude = Utils.hasIncludeSegments(p, s, this.wealthCodes, this.preferredCodes);
							isIncludeSegment = true;
						}

						if (lsCustCamp != null && lsCustCamp.size() > 0 && p.getCampaign().getFileCampaign() != null) {
							ActiveCampaignCustomerView campcust;
							for (int i = 0; i < lsCustCamp.size(); i++) {
								campcust = lsCustCamp.get(i);
								if (campcust.getId().getCampaignId().equalsIgnoreCase(p.getCampaign().getId())) {
									p = Utils.replaceCampaignVariables(p, campcust, logger);
									lc.add(p);
									break cicloGeneral;
								}
							}
						} else if (isIncludeSegment
								&& GenericValidator.isBlankOrNull(p.getCampaign().getFileCampaign())) {
							if (isInclude) {
								lc.add(p);
							} else {
								return new ModelAndView("portalCampaign", model);
							}
						} else if (!isIncludeSegment && !isExcludeSegment
								&& GenericValidator.isBlankOrNull(p.getCampaign().getFileCampaign())) {
							lc.add(p);
						} else if (p.getCampaign().getFileCampaign() == null)
							lc.add(p);
					}
				}
			}

			// if no customer campaign was found, then lookup a general campaign
			if (lc == null || lc.size() == 0) {
				List<ActiveCampaignView> lsPortalCamp = customerService.getActiveCampaignByType(CampaignType.PORTAL);
				for (Iterator<ActiveCampaignView> iterator = lsPortalCamp.iterator(); iterator.hasNext();) {
					PortalCampaign p = new PortalCampaign(iterator.next());
					if (GenericValidator.isBlankOrNull(p.getCampaign().getFileCampaign())) {
						lc = new java.util.LinkedList<PortalCampaign>();
						lc.add(p);
						break;
					}
				}
			}
			boolean isEnglish = Utils.getRSALanguage(request).equalsIgnoreCase("en");
			model.put("isenglish", isEnglish);
			if (lc != null && lc.size() > 1) {
				PortalCampaign c = lc.get(0);
				lc = new java.util.LinkedList<PortalCampaign>();
				lc.add(c);
			}
			model.put("campaignCustomer", lc);

			/*
			 * mpicado, CIBP-1807, aqui se procede a verificar si el cliente ya
			 * vio la campana que se le va a mostrar, si es que hay, para saber
			 * si se muestra de forma grande o de forma pequena, si el cliente
			 * no la ha visto se procede a mostrarla en grande pero se guarda
			 * que el cliente ya vio la campana, y la proxima vez que la vaya a
			 * ver le va salir en pequeno
			 */
			if (lc != null && lc.size() > 0) {
				if (this.customerService.isViewedCampaign(s.getCustomerProfile(), lc.get(0).getCampaign()))
					model.put("viewedCampaign", true);
				else {
					model.put("viewedCampaign", false);
					ViewCampaign vc = new ViewCampaign();

					if (s.getCustomerProfile().getProfileId() == null)
						vc.getId().setProfileId(s.getCustomerProfile().getUsername());
					else
						vc.getId().setProfileId(s.getCustomerProfile().getProfileId());

					vc.getId().setIdCampaign(lc.get(0).getCampaign().getId());
					vc.setViewDate(Calendar.getInstance().getTime());
					vc.setType(CampaignType.PORTAL);
					vc.setStatus(ViewCampaign.VISTA);
					/*
					 * el problema con esto es que se puede guardar una campana
					 * como ya vista aqui y que por alguna razon ocurra un error
					 * despues de esto y no se pueda mostrar la pagina, entonces
					 * la campana se guardo como vista y la proxima vez que el
					 * usuario entre al portal, va a ver la version teaser de la
					 * campana
					 */
					this.customerService.saveViewCampaign(vc);
					customerService.campaignStatistics(lc.get(0).getCampaign().getId(), "VIEW",
							CampaignType.PORTAL.toString());
				}

			}
		} else {
			model.put("campaignCustomer", null);
		}
		model.put("campaignType", CampaignType.PORTAL);
		return new ModelAndView("portalCampaign", model);
	}

	public ModelAndView inProcessTransactions(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<String, Object> model = new HashMap<>();
		String key = request.getParameter("frontendid");
		TVFrontendAccount account = Utils.getAccountById(key, userSession.getAccounts());
		if (!userSession.getAuthorizations().containsKey(key)) {
			userSession.getAuthorizations(key, account);
		}
		if (userSession.getAuthorizations().get(key) == null
				|| userSession.getAuthorizations().get(key).getAuthorizations() == null) {
			model.put("tryLater", true);
		} else {
			if (userSession.getAuthorizations().get(key).getAuthorizations().isEmpty()) {
				model.put("showAuth", false);
			} else {
				model.put("inprocess", userSession.getAuthorizations().get(key).getAuthorizations());
			}
		}
		if (userSession.getAuthorizations().get(key) != null
				&& userSession.getAuthorizations().get(key).getCurrentBalance() != null) {
			model.put("currentBalance", userSession.getAuthorizations().get(key).getCurrentBalance());
		} else {
			model.put("currentBalance", account.getAvailableBalance());
		}
		return new ModelAndView("inProcessTransactions", model);
	}

	/**
	 * Method to get the SOS claims and asign the correct descriptions variables
	 * 
	 * @return The claim list with status and description variable assignment
	 * @author et55498
	 */
	public ModelAndView sosClaims(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		List<SosClaimsInfo> sosClaims = new LinkedList<>();
		model.put("sosClaims", sosClaims);
		return new ModelAndView("sosClaims", model);
	}

	public ModelAndView statementSummaryCampaign(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		CustomerProfile customer = null;
		List<ActiveCampaignCustomerView> cust = null;
		UserSession s = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<String, Object> model = new HashMap<>();
		if (s != null) {
			Utils.setHasStatementOrEbill(s);
			customer = s.getCustomerProfile();
			if (customer != null && customer.getTaxId() != null && customer.getEnrollmentAccountNumber() != null)
				try {
					cust = customerService.getActiveCustCampaign(customer.getTaxId().trim());
				} catch (Exception e) {
					logger.error("Error getCampaignCustomer", e);
					cust = null;
				}
		}
		List<Campaign> list = customerService.getRunnigCampaignByType(CampaignType.STATEMENT_SUMMARY);
		List<PortalCampaign> lc = new java.util.LinkedList<PortalCampaign>();
		// List<PortalCampaign> custCam = new
		// java.util.LinkedList<PortalCampaign>();
		cicloGeneral: for (Iterator<Campaign> iterator = list.iterator(); iterator.hasNext();) {
			PortalCampaign p = new PortalCampaign(iterator.next());
			// IF CUST IS IN EXCLUDE SEGMENT CAMP IS NOT SHOWN
			boolean isExcludeSegment = false;
			if (p.getCampaign().getExcludeSegments() != null && !p.getCampaign().getExcludeSegments().equals("")) {
				if (Utils.hasExcludeSegments(p, s, this.wealthCodes, this.preferredCodes)) {
					return new ModelAndView("portalCampaign", model);
				}
			}
			boolean isInclude = false;
			boolean isIncludeSegment = false;
			if (p.getCampaign().getIncludeSegments() != null
					&& !p.getCampaign().getIncludeSegments().equals("<properties/>")) {
				isInclude = Utils.hasIncludeSegments(p, s, this.wealthCodes, this.preferredCodes);
				isIncludeSegment = true;
			}

			if (cust != null && cust.size() > 0 && p.getCampaign().getFileCampaign() != null) {
				ActiveCampaignCustomerView campcust;
				for (int i = 0; i < cust.size(); i++) {
					campcust = cust.get(i);
					if (campcust.getId().getCampaignId().equalsIgnoreCase(p.getCampaign().getId())) {
						p = Utils.replaceCampaignVariables(p, campcust, logger);
						lc.add(p);
						break cicloGeneral;
					}
				}
			} else if (isIncludeSegment && GenericValidator.isBlankOrNull(p.getCampaign().getFileCampaign())) {
				if (isInclude) {
					lc.add(p);
				} else {
					return new ModelAndView("portalCampaign", model);
				}
			} else if (!isIncludeSegment && !isExcludeSegment
					&& GenericValidator.isBlankOrNull(p.getCampaign().getFileCampaign())) {
				lc.add(p);
			} else if (p.getCampaign().getFileCampaign() == null)
				lc.add(p);
		}
		boolean isEnglish = Utils.getRSALanguage(request).equalsIgnoreCase("en");
		model.put("isenglish", isEnglish);
		if (lc != null && lc.size() > 1) {
			PortalCampaign c = lc.get(0);
			lc = new java.util.LinkedList<PortalCampaign>();
			lc.add(c);
		}
		model.put("campaignCustomer", lc);

		/*
		 * mpicado, CIBP-1807, aqui se procede a verificar si el cliente ya vio
		 * la campana que se le va a mostrar, si es que hay, para saber si se
		 * muestra de forma grande o de forma pequena, si el cliente no la ha
		 * visto se procede a mostrarla en grande pero se guarda que el cliente
		 * ya vio la campana, y la proxima vez que la vaya a ver le va salir en
		 * pequeno
		 */
		if (lc != null && lc.size() > 0) {
			if (this.customerService.isViewedCampaign(s.getCustomerProfile(), lc.get(0).getCampaign()))
				model.put("viewedCampaign", true);
			else {
				model.put("viewedCampaign", false);
				ViewCampaign vc = new ViewCampaign();
				if (s.getCustomerProfile().getProfileId() == null)
					vc.getId().setProfileId(s.getCustomerProfile().getUsername());
				else
					vc.getId().setProfileId(s.getCustomerProfile().getProfileId());

				vc.getId().setIdCampaign(lc.get(0).getCampaign().getId());
				vc.setViewDate(Calendar.getInstance().getTime());
				vc.setType(CampaignType.STATEMENT_SUMMARY);
				vc.setStatus(ViewCampaign.VISTA);
				/*
				 * el problema con esto es que se puede guardar una campana como
				 * ya vista aqui y que por alguna razon ocurra un error despues
				 * de esto y no se pueda mostrar la pagina, entonces la campana
				 * se guardo como vista y la proxima vez que el usuario entre al
				 * portal, va a ver la version teaser de la campana
				 */
				this.customerService.saveViewCampaign(vc);
				this.customerService.campaignStatistics(lc.get(0).getCampaign().getId(), "VIEW",
						CampaignType.STATEMENT_SUMMARY.toString());
			}
		}
		model.put("campaignType", CampaignType.STATEMENT_SUMMARY);
		return new ModelAndView("portalCampaign", model);
	}

	public ModelAndView campaignStatistic(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String campaignId = (String) request.getParameter("cmid");
		String clickorview = (String) request.getParameter("cov");
		String campaignarea = (String) request.getParameter("cma");

		if (!GenericValidator.isBlankOrNull(campaignId) && !GenericValidator.isBlankOrNull(clickorview)
				&& !GenericValidator.isBlankOrNull(campaignarea)) {

			customerService.campaignStatistics(campaignId, clickorview, campaignarea);
		}

		Map<String, Object> model = new HashMap<>();
		return new ModelAndView("text", model);
	}

	private boolean isEbppAvailable() {
		boolean ebppStatus = false;

		if (this.messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()
				.equals(EntitlementStatus.ENABLED.toString())) {
			ebppStatus = true;
		} else {
			ebppStatus = false;
		}

		return ebppStatus;
	}

	public GlobalPayeeIndexBean getGlobalPayeeIndex() {
		return globalPayeeIndex;
	}

	public void setGlobalPayeeIndex(GlobalPayeeIndexBean globalPayeeIndex) {
		this.globalPayeeIndex = globalPayeeIndex;
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getAadvantageProducts() {
		return aadvantageProducts;
	}

	public void setAadvantageProducts(String aadvantageProducts) {
		this.aadvantageProducts = aadvantageProducts;
	}

	public void setWealthCodes(String wealthCodes) {
		this.wealthCodes = wealthCodes;
	}

	public String getWealthCodes() {
		return wealthCodes;
	}

	public void setPreferredCodes(String preferredCodes) {
		this.preferredCodes = preferredCodes;
	}

	public String getPreferredCodes() {
		return preferredCodes;
	}

	public ModelAndView cancelInformationChallenge(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, false);
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.MOBILE_FRAME, null);
		WebUtils.setSessionAttribute(request, ChallengeInformationUtils.ATHM_ACCT_MOD, null);
		Map<String, Object> model = new HashMap<>();

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String language = Utils.getRSALanguage(request);

		String username = session.getUsername();
		model.put("username", username);

		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");

		cookie.setPath("/");
		cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
		if (cookie != null)
			model.put("devicetoken", cookie.getValue());

		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(session.getPm_fp(), session.getDeviceRequest().getDeviceTokenCookie(), request);
		/**
		 * MBNA-1955
		 * Method rsaAnalyze : CIBP-WEB microservice integration
		 *
		 */
		//Microservice implementation
		RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
		AnalyzeResponse anResp = customerService.rsaAnalyze(rsaAnalyzeRequest);

		deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
		deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

		session.setRsaSessionId(anResp.getIdentificationData().getSessionId());
		session.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

		session.setDeviceRequest(deviceRequest);

		if (deviceRequest != null && cookie != null) {
			cookie.setValue(deviceRequest.getDeviceTokenCookie());
			cookie.setPath("/");
			ESAPI.httpUtilities().addCookie(response, cookie);
		}

		if (ActionCode.REVIEW == anResp.getRiskResult().getTriggeredRule().getActionCode()
				|| ActionCode.DENY == anResp.getRiskResult().getTriggeredRule().getActionCode()) {
			model.put("text", "exception-");
		} else if (anResp.getIdentificationData().getUserStatus() == UserStatus.LOCKOUT) {
			boolean hasalert = false;
			AlertsEntitlement ent = new AlertsEntitlement(session.getCustomerProfile().getEntitlementByType(
					EntitlementType.ALERTS));
			if (ent != null && ent.getAlerts() != null) {
				Set<AlertType> alerts = ent.getAlerts();
				if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
						|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
					hasalert = true;
				}
			}
			/* CIBP-1104 se activa envio alerta de bloqueo de preguntas secretas */
			if (hasalert) {
				session.getCustomerService().sendQuestionsBlockedConfirmation(session.getCustomerProfile(),
						session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
						session.getPreferredLanguage());
			}
			/* FIN--- CIBP-1104 */

			// errors.rejectValue("answer", "account.block.message",
			// "Account Blocked");
			CookieUtils.addCookie(response, 60 * 60 * 24 * 365, "username", username);

			model.put("text", "blocked-");
		} else if (ActionCode.CHALLENGE == anResp.getRiskResult().getTriggeredRule().getActionCode()) {
			String email = null;
			if (session.getCustomerProfile() != null)
				email = session.getCustomerProfile().getEmail();

			QuestionResponse personalQuestion;

			RSAActionRequest actionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(request,
					session.getUsername()), session.getUsername(), "", email, 0, session.isRemember(),
					session.getDeviceRequest());

			if (session.getCustomerProfile() != null)
				actionRequest.setUsername(session.getCustomerProfile().getUsername());
			else if (session.getUsernameReservation() != null)
				actionRequest.setUsername(session.getUsernameReservation().getUsername());
			else
				actionRequest.setUsername(username);

			// session.setRsaSessionId(anResp.getIdentificationData().getSessionId());
			// session.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

			actionRequest.setDeviceRequest(session.getDeviceRequest());
			actionRequest.setRsaSessionId(session.getRsaSessionId());
			actionRequest.setRsaTransactionId(session.getRsaTransactionId());

			actionRequest.setRsaLang(Utils.getRSALanguage(request));

			personalQuestion = session.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

			if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
				session.setInformationQuestion(personalQuestion.getQuestion());

			} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
				model.put("text", "exception-");
			} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
				boolean hasalert = false;
				AlertsEntitlement ent = new AlertsEntitlement(session.getCustomerProfile().getEntitlementByType(
						EntitlementType.ALERTS));
				if (ent != null && ent.getAlerts() != null) {
					Set<AlertType> alerts = ent.getAlerts();
					if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
							|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
						hasalert = true;
					}
				}
				/*
				 * CIBP-1104 se activa envio alerta de bloqueo de preguntas
				 * secretas
				 */
				if (hasalert) {
					session.getCustomerService().sendQuestionsBlockedConfirmation(session.getCustomerProfile(),
							session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
							session.getPreferredLanguage());
				}
				/* FIN--- CIBP-1104 */

				// errors.rejectValue("answer", "account.block.message",
				// "Account Blocked");
				CookieUtils.addCookie(response, 60 * 60 * 24 * 365, "username", username);

				model.put("text", "blocked-");
			}
		}

		return new ModelAndView("text", model);
	}

	public String getSosResolvedConditions() {
		return sosResolvedConditions;
	}

	public void setSosResolvedConditions(String sosResolvedConditions) {
		this.sosResolvedConditions = sosResolvedConditions;
	}

	public String getSosCallToActionStatus() {
		return sosCallToActionStatus;
	}

	public void setSosCallToActionStatus(String sosCallToActionStatus) {
		this.sosCallToActionStatus = sosCallToActionStatus;
	}

	public String getEzStatementInformativaIRAQuarterlyProductId() {
		return ezStatementInformativaIRAQuarterlyProductId;
	}

	public void setEzStatementInformativaIRAQuarterlyProductId(String ezStatementInformativaIRAQuarterlyProductId) {
		this.ezStatementInformativaIRAQuarterlyProductId = ezStatementInformativaIRAQuarterlyProductId;
	}

	public String getEzStatementInformativaIRAYearlyProductId() {
		return ezStatementInformativaIRAYearlyProductId;
	}

	public void setEzStatementInformativaIRAYearlyProductId(String ezStatementInformativaIRAYearlyProductId) {
		this.ezStatementInformativaIRAYearlyProductId = ezStatementInformativaIRAYearlyProductId;
	}

	public String getEzStatementInformativaCAEQuarterlyProductId() {
		return ezStatementInformativaCAEQuarterlyProductId;
	}

	public void setEzStatementInformativaCAEQuarterlyProductId(String ezStatementInformativaCAEQuarterlyProductId) {
		this.ezStatementInformativaCAEQuarterlyProductId = ezStatementInformativaCAEQuarterlyProductId;
	}

	public String getEzStatementInformativaCAEYearlyProductId() {
		return ezStatementInformativaCAEYearlyProductId;
	}

	public void setEzStatementInformativaCAEYearlyProductId(String ezStatementInformativaCAEYearlyProductId) {
		this.ezStatementInformativaCAEYearlyProductId = ezStatementInformativaCAEYearlyProductId;
	}

	public String[] getEzStatementInformativaIRACAEProductIDs() {
		return ezStatementInformativaIRACAEProductIDs;
	}

	public void setEzStatementInformativaIRACAEProductIDs(String[] ezStatementInformativaIRACAEProductIDs) {
		this.ezStatementInformativaIRACAEProductIDs = ezStatementInformativaIRACAEProductIDs;
	}

	public ModelAndView skipDCIQuestions(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> model = new HashMap<>();

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		session.setLoginCompleteSuccess(true);

		model.put("text", "success");

		return new ModelAndView("text", model);
	}

	public ModelAndView documentsMenuRedirect(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(), request);
		if (userSession.isInformationChallenge()) {
			boolean oobFlag = StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB);
			if (oobFlag && userSession.getOobEnroll()) {
				if (userSession.getOobEnroll() && userSession.isOobAuthenticated()) {
					model.put("text", "direct");
					return new ModelAndView("text", model);
				} else {
					model.put("oobChallengeOpen", true);
					model.put("text", "challenge-");
				}
				userSession.setDeviceRequest(deviceRequest);
				List<ASK_FOR_OOB> listOOB = new ArrayList<>();
				listOOB.add(ASK_FOR_OOB.MY_DOCUMENTS);
				listOOB.add(ASK_FOR_OOB.OOB_CHALLENGE);
				userSession.setTasksListOOB(listOOB);
			} else {
				model.put("text", "direct");
			}
		} else {
			String username = userSession.getUsername();
			String language = Utils.getRSALanguage(request);


			/**
			 * MBNA-1955
			 * Method rsaAnalyze : CIBP-WEB microservice integration
			 *
			 */
			//Microservice implementation
			RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
			AnalyzeResponse anResp = customerService.rsaAnalyze(rsaAnalyzeRequest);

			deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

			userSession.setRsaSessionId(anResp.getIdentificationData().getSessionId());
			userSession.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

			userSession.setDeviceRequest(deviceRequest);

			if (ActionCode.CHALLENGE != anResp.getRiskResult().getTriggeredRule().getActionCode()) {
				userSession.setInformationChallenge(true);
				model.put("text", "direct");
			} else {
				String email = null;
				if (userSession.getCustomerProfile() != null)
					email = userSession.getCustomerProfile().getEmail();

				QuestionResponse personalQuestion;

				if (userSession.getInformationQuestion() == null) {
					RSAActionRequest actionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(
							request, userSession.getUsername()), userSession.getUsername(), "", email, 0,
							userSession.isRemember(), userSession.getDeviceRequest());

					if (userSession.getCustomerProfile() != null)
						actionRequest.setUsername(userSession.getCustomerProfile().getUsername());
					else if (userSession.getUsernameReservation() != null)
						actionRequest.setUsername(userSession.getUsernameReservation().getUsername());
					else
						actionRequest.setUsername(username);

					actionRequest.setDeviceRequest(userSession.getDeviceRequest());
					actionRequest.setRsaSessionId(userSession.getRsaSessionId());
					actionRequest.setRsaTransactionId(userSession.getRsaTransactionId());

					actionRequest.setRsaLang(Utils.getRSALanguage(request));

					personalQuestion = userSession.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

				} else {
					personalQuestion = new QuestionResponse();
					personalQuestion.setStatusResult(ResultStatus.SUCCESS);
					personalQuestion.setQuestion(userSession.getInformationQuestion());
				}
				if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
					userSession.setInformationQuestion(personalQuestion.getQuestion());
					model.put("text", "redirect-" + personalQuestion.getQuestion().values().toArray()[0]);

				} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
					throw new ModelAndViewDefiningException(new ModelAndView("error"));
				} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
					throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
				}
			}
		}
		Cookie cookie = new Cookie("PMData", deviceRequest.getDeviceTokenCookie());
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		ESAPI.httpUtilities().addCookie(response, cookie);
		return new ModelAndView("text", model);
	}

	public ModelAndView portalInformationChallenge(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String answer = ServletRequestUtils.getStringParameter(request, "answer");
		if (GenericValidator.isBlankOrNull(answer)) {
			model.put("text", "empty-");
		} else {
			CustomerProfile profile = session.getCustomerProfile();
			boolean dummyQuestions = (profile == null);
			RSAActionRequest rsaActionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(request,
					session.getUsername()), session.getUsername(), answer, dummyQuestions ? null : profile.getEmail(),
					0, session.isRemember(), session.getDeviceRequest());
			String username = session.getUsername();
			if (session.getCustomerProfile() != null)
				rsaActionRequest.setUsername(session.getCustomerProfile().getUsername());
			else if (session.getUsernameReservation() != null)
				rsaActionRequest.setUsername(session.getUsernameReservation().getUsername());
			else
				rsaActionRequest.setUsername(username);

			rsaActionRequest.setRsaSessionId(session.getRsaSessionId());
			rsaActionRequest.setRsaTransactionId(session.getRsaTransactionId());
			rsaActionRequest.setQuestion(session.getInformationQuestion());
			rsaActionRequest.setRsaLang(null);
			ValidationQuestionResponse responseAnswer = session.getCustomerService().getSignOnCustomerServiceModule().validateRSAQuestion(
					rsaActionRequest);
			session.setQuestion(null);
			session.setRsaSessionId(responseAnswer.getRsaSessionId());
			session.setRsaTransactionId(responseAnswer.getRsaTransactionId());

			Cookie cookie = WebUtils.getCookie(request, "PMData");
			if (cookie == null)
				cookie = new Cookie("PMData", "");

			cookie.setPath("/");
			cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
			ESAPI.httpUtilities().addCookie(response, cookie);

			if (ResultStatus.SUCCESS.equals(responseAnswer.getStatusResult())) {
				cookie.setPath("/");
				cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
				ESAPI.httpUtilities().addCookie(response, cookie);
				session.setInformationChallenge(true);
				model.put("text", "success-");
			} else if (ResultStatus.BLOCKED.equals(responseAnswer.getStatusResult())) {
				boolean hasalert = false;
				AlertsEntitlement ent = new AlertsEntitlement(session.getCustomerProfile().getEntitlementByType(
						EntitlementType.ALERTS));
				if (ent != null && ent.getAlerts() != null) {
					Set<AlertType> alerts = ent.getAlerts();
					if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
							|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
						hasalert = true;
					}
				}
				if (hasalert) {
					session.getCustomerService().sendQuestionsBlockedConfirmation(session.getCustomerProfile(),
							session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
							session.getPreferredLanguage());
				}
				CookieUtils.addCookie(response, 60 * 60 * 24 * 365, "username", username);
				session.setBlocked(true);

				// Action block Assisted disbursement documents
				if (session.isBlocked() && session.getCustomerProfile().hasFlag(CustomerFlagType.DOCUMENTS_AVAILABLE)) {

					List<CustomerDocumentsPackage> list = session.getDisbursementList();
					for (CustomerDocumentsPackage pack : list) {
						pack.setHide("Y");
						pack.setLastHiddenBy("SYSTEM");						
						// Documents facade implementation
						DocumentsFacade.getInstance().updateDocumentPackage(pack, session);
					}
				}
				model.put("text", "blocked-");
			} else if (ResultStatus.EXCEPTION.equals(responseAnswer.getStatusResult())) {
				cookie.setPath("/");
				cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
				ESAPI.httpUtilities().addCookie(response, cookie);
				model.put("text", "exception-");
				// return new ModelAndView("forward:login", model);
			} else if (ResultStatus.ERROR.equals(responseAnswer.getStatusResult())) {
				cookie.setPath("/");
				cookie.setValue(session.getDeviceRequest().getDeviceTokenCookie());
				ESAPI.httpUtilities().addCookie(response, cookie);

				String email = null;
				if (session.getCustomerProfile() != null)
					email = session.getCustomerProfile().getEmail();

				QuestionResponse personalQuestion;

				RSAActionRequest actionRequest = new RSAActionRequest(Utils.generateCustomerInteractionEvent(request,
						session.getUsername()), session.getUsername(), "", email, 0, session.isRemember(),
						session.getDeviceRequest());

				if (session.getCustomerProfile() != null)
					actionRequest.setUsername(session.getCustomerProfile().getUsername());
				else if (session.getUsernameReservation() != null)
					actionRequest.setUsername(session.getUsernameReservation().getUsername());
				else
					actionRequest.setUsername(username);

				actionRequest.setDeviceRequest(session.getDeviceRequest());
				actionRequest.setRsaSessionId(session.getRsaSessionId());
				actionRequest.setRsaTransactionId(session.getRsaTransactionId());

				actionRequest.setRsaLang(Utils.getRSALanguage(request));

				personalQuestion = session.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

				if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
					String question = (String) personalQuestion.getQuestion().values().toArray()[0];
					session.setInformationQuestion(personalQuestion.getQuestion());
					model.put("text", "error-" + question);

				} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
					model.put("text", "exception-");
				} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION, null);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, null);
					/* CIBP-1104 */
					boolean hasalert = false;
					AlertsEntitlement ent = new AlertsEntitlement(session.getCustomerProfile().getEntitlementByType(
							EntitlementType.ALERTS));
					if (ent != null && ent.getAlerts() != null) {
						Set<AlertType> alerts = ent.getAlerts();
						if (alerts.contains(AlertType.QUESTIONS_BLOCK_EMAIL) || alerts.contains(AlertType.QUESTIONS_BLOCK_SMS)
								|| alerts.contains(AlertType.QUESTIONS_BLOCK_PUSH)) {
							hasalert = true;
						}
					}
					if (hasalert) {
						session.getCustomerService().sendQuestionsBlockedConfirmation(session.getCustomerProfile(),
								session.getSmsProfile(Utils.generateCustomerInteractionEvent(request)),
								session.getPreferredLanguage());
					}
					CookieUtils.addCookie(response, 60 * 60 * 24 * 365, "username", username);

					model.put("text", "blocked-");
				}
			}
		}
		return new ModelAndView("text", model);
	}

	public ModelAndView deleteCustomerProfile(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map<String, Object> model = new HashMap<>();
		String username = (String) WebUtils.getSessionAttribute(request, "userToDelete");
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		if (username != null) {
			//there are profiles that are being deleted
			logger.debug(Utils.neutralizeMessage("Info: deleteCustomerProfile, starting to delete customer profile : " + username));
			logger.error(Utils.neutralizeMessage("Info: deleteCustomerProfile, starting to delete customer profile : " + username));
			//Find acccount to deactive stmt
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {				
				List<TVFrontendAccount> accs = userSession.getAccounts();
				boolean success = this.customerService.deactiveEbills(accs , userSession.getCustomerProfile(), userSession.getBankingSession());
				model.put("text", success);
				if(!success){
					return new ModelAndView("text", model);
				}
			}
			this.customerService.deleteCustomerProfile(username);
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)) {	
				WebUtils.setSessionAttribute(request, "customerEnrollment", null);
				WebUtils.setSessionAttribute(request, "userSession", null);
				WebUtils.setSessionAttribute(request, "sessionEnrollUpdate", null);
			}
		}
		return new ModelAndView("text", model);
	}

	public ModelAndView eSignEnforcement(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = userSession.getCustomerService();
		Boolean removeFlag = true;
		Boolean activationSuccess = false;
		Boolean activationError = false;
		StringBuilder successData = new StringBuilder();
		StringBuilder errorData = new StringBuilder();

		CustomerProfile profile = userSession.getCustomerProfile();

		if (profile.hasFlag(CustomerFlagType.ESIGN_ENFORCEMENT)) {

			List<TVFrontendAccount> accounts = userSession.getAccounts();

			userSession.seteSignLookUp(true);

			if (customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() == EntitlementStatus.ENABLED) {
				customerService.setEBPPStatus(true);
			} else {
				customerService.setEBPPStatus(false);
				removeFlag = false;
			}

			// The EBPP2 user id is not yet in session; add it
			if (!userSession.isEbpp2UserIdLookedUp()) {
				EbppEntitlement ent = null;
				if (profile.getEntitlementByType(EntitlementType.EBILLS) != null) {
					ent = new EbppEntitlement(profile.getEntitlementByType(EntitlementType.EBILLS));

					// If the username is blank, we need to look in the current
					// EBPP
					// accounts ONLY
					if ("".equals(ent.getUsername()) || null == ent.getUsername()) {
						userSession.setEbpp2UserId("");
					} else {
						userSession.setEbpp2UserId(ent.getUsername());
					}
					userSession.setEbpp2UserIdLookedUp(true);
				}
			}
			if (userSession.isEbpp2UserIdLookedUp() && !userSession.getEbpp2UserId().equals("")) {
				boolean eStatement = false;
				RegisterUserAccountReq req;
				int billerProductId = 0;
				String valid = "";
				BankingSession bankingSession = userSession.getBankingSession();
				// Cuentas
				for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
					TVFrontendAccount account = (TVFrontendAccount) iterator.next();
					valid = account.getBankId() + "-" + account.getAccountProductId();

					if (eSignValidDepositAccounts.contains(valid)) {
						if (account.getIsAvailableElecStmt()) {
							// Cuenta Valida
							eStatement = true;
							if (account.getAccountProductId() != null) {
								Ebpp2BillerProductIdResponse ebpp2BillerProductIdResponse = customerService
										.getEbpp2BillerProductIdInformationDepositAccount(account);

								if (ebpp2BillerProductIdResponse != null)
									billerProductId = ebpp2BillerProductIdResponse.getBillerProductId();
							}
							// Do we have a valid biller product ID?
							if (billerProductId > 0) {
								// If this is an estatement, activate it in IDA
								boolean continueProcess = true;
								if (eStatement) {
									continueProcess = customerService
											.activateEbpp2AccountInIDA(bankingSession, account);
								}
								if (continueProcess) {
									req = new RegisterUserAccountReq();
									req.setAccountNumber(account.getAccountNumber());
									req.setBillerId(1);
									req.setBillerProductId(billerProductId);
									req.setLabel(account.getNickname());
									req.setNss(profile.getTaxId());
									req.setUserId(Integer.parseInt(userSession.getEbpp2UserId()));
									req.setZip("00000");

									// Register the account
									RegisterUserAccountResp resp = customerService.registerEbpp2UserAccount(req);

									// SUCCESS
									if (resp.getRegisterResponse() != null && resp.getRegisterResponse().equals("0")) {
										activationSuccess = true;
										successData.append(account.getAccountNumber() + " ");
										// Reset the user account and user
										// account
										// summary session variabled
										userSession.resetEbpp2Accounts();
									}
									// FAILURE
									else {
										activationError = true;
										errorData.append("Fail register account in Ebpp2 : "
												+ account.getAccountNumber());
										logger.debug(Utils.neutralizeMessage("Fail register account in Ebpp2 : " + account));
									}
								} else {
									activationError = true;
									errorData.append("Fail register account in IDA: " + account.getAccountNumber());
									logger.debug("Fail register account in IDA: " + account);
								}
							} else {
								activationError = true;
								errorData.append("No billerProductId:" + account.getAccountNumber());
								logger.debug("No billerProductId: " + account);
							}
						}// End if IsAvailableElecStmt
					}// End if valid accounts
				}
				// Remove Flag
				if (removeFlag) {
					CustomerFacade.getInstance().removeCustomerFlag(profile, CustomerFlagType.ESIGN_ENFORCEMENT, userSession);
					customerService.logEsignFlagRemoved(Utils.generateCustomerInteractionEvent(request,
							userSession.getUsername()));
				}
				if (activationSuccess) {
					CustomerInteractionEvent customerInteractionEvent = Utils.generateCustomerInteractionEvent(request,
							userSession.getUsername());
					customerInteractionEvent.putString("ESIGNINFO", successData.toString());
					customerService.logEsignAccountRegistration(customerInteractionEvent);
				}
				if (activationError) {
					CustomerInteractionEvent customerInteractionEvent = Utils.generateCustomerInteractionEvent(request,
							userSession.getUsername());
					customerInteractionEvent.putString("ESIGNINFO", errorData.toString());
					customerService.logEsignAccountFailRegistration(customerInteractionEvent);
				}
			} else {
				logger.debug("Error not EBPP2 user");
			}

		}
		return new ModelAndView("forward:welcome", model);
	}

	public ModelAndView eSignForm(HttpServletRequest request, HttpServletResponse response) {

		UserSession userSession = new UserSession();//user session
		final String strUserSession = "userSession";//user session string

		if(WebUtils.getSessionAttribute(request, strUserSession) instanceof UserSession) {
			userSession = (UserSession) WebUtils.getSessionAttribute(request, strUserSession);
		}
		Map<String, Object> model = new HashMap<>(); //model result

		//MBFM-99
		final String cookieAnalyticEnabled = "cookieAnalyticEnabled"; //cookie analytic enabled
		model.put(cookieAnalyticEnabled, userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS));
		//END MBFM-99

		return new ModelAndView("eSignForm", model);
	}

	/**
	 * tsysLoyaltyRewardsBalanceInformation method to get User Accounts CashRewards amounts 
	 * @param request
	 * @param response
	 * @return ModelAndView
	 */
	public ModelAndView tsysLoyaltyRewardsBalanceInformation(HttpServletRequest request, 
			HttpServletResponse response) {

		final String logEnteredMethod = "Entered tsysLoyaltyRewardsBalanceInformation in %s with username = %s"; // String value
		final String logExtractedParams = "Exctracted from params front end id = %s"; // String value
		final String logCouldNotExtractFrontEndId = "Could not extract frontEndId from params: %s"; // String value
		final String logAccountCouldNotBeFound = "Account could not be found using frontEndId = %s"; // String value
		final String logAccountToBeVPCBKAndUserPrimary = "Account has to be VPCBK and the user must be primary"; // String value
		final String logCreditCardMSResponseNullable = "The creditcards micro service response is null"; // String value
		final String logSendingResponse = "Sending response = "; // String value
		final String logCreditCardsMSErrorDetail = "Creditcards micro service error detail = %s";
		final String badRequestValue = "Bad Request"; // String value
		final String parameterFrontEndIdValue = "Query parameter frontEndId was not provided"; // String value
		final String balanceInformationValue = "tsysLoyaltyRewardsBalanceInformation"; // String value
		final String notFoundValue = "Not Found"; // String value
		final String requestedAccountNotFound = "The requested account was not found"; // String value
		final String internalServerErrorValue = "Internal Server Error"; // String value
		final String vpcbkValue = "VPCBK"; // String value
		

		final UserSession userSession = Utils.getUserSession(request); //User existence is verified by intercepter

		logger.info(Utils.neutralizeMessage(String.format(logEnteredMethod, this.getClass().getSimpleName(), userSession.getCustomerProfile().getUsername()))); 

		Map<String, Object> model = new HashMap<String, Object>(0); // object model-view return
		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse = null; // Object Response
		String frontEndId = null; // Cash Rewards Account
		final Gson json = new Gson(); //

		try {
			frontEndId = ServletRequestUtils.getStringParameter(request, frontEndIdValue);
			logger.info(String.format(logExtractedParams, frontEndId));
		} catch (ServletRequestBindingException e) {

			logger.error(String.format(logCouldNotExtractFrontEndId + e.getMessage()));

			microServiceTsysLoyaltyRewardsResponse = new MicroServiceResponse<>();

			final Error microServiceError = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_BAD_REQUEST, badRequestValue,
					parameterFrontEndIdValue, balanceInformationValue);

			microServiceTsysLoyaltyRewardsResponse.getErrors().add(microServiceError);  

			final String responseJson = json.toJson(microServiceTsysLoyaltyRewardsResponse); //

			model.put(jsonValue, responseJson);
			return new ModelAndView(jsonValue, model);
		}


		TVFrontendAccount ccaAccountByFrontEndId = Utils.getAccountById(frontEndId, 
				userSession.getCcaAccounts());


		if(ccaAccountByFrontEndId == null) {

			logger.error(String.format(logAccountCouldNotBeFound, frontEndId));

			microServiceTsysLoyaltyRewardsResponse = new MicroServiceResponse<>();

			final Error microServiceError = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_NOT_FOUND, notFoundValue,
					requestedAccountNotFound, balanceInformationValue);

			microServiceTsysLoyaltyRewardsResponse.getErrors().add(microServiceError);

			final String responseJson = json.toJson(microServiceTsysLoyaltyRewardsResponse); //
			model.put(jsonValue, responseJson);
			return new ModelAndView(jsonValue, model);
		}

		if(!ccaAccountByFrontEndId.getAccountProductId().equalsIgnoreCase(vpcbkValue) 
				|| !ccaAccountByFrontEndId.isPrimaryAccount()) {

			logger.error(logAccountToBeVPCBKAndUserPrimary);

			microServiceTsysLoyaltyRewardsResponse = new MicroServiceResponse<>();

			final Error microServiceError = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_BAD_REQUEST, badRequestValue,
					"The requested must be made with a VPCBK and a primary owner", balanceInformationValue);

			microServiceTsysLoyaltyRewardsResponse.getErrors().add(microServiceError);

			final String responseJson = json.toJson(microServiceTsysLoyaltyRewardsResponse); //

			model.put(jsonValue, responseJson);
			return new ModelAndView(jsonValue, model);
		}


		//This call is made using ccaAccountByFrontEndId.getAccountNumber() which really holds 
		//the 16 digits of the "main plastic" and not the unique 11 digit account id in Tsys
		//thus its passed in the cardNumber parameter.
		microServiceTsysLoyaltyRewardsResponse = CreditCardFacade.getInstance()
				.consumeTsysLoyaltyRewardsAccountInfo(userSession, 
						null, ccaAccountByFrontEndId.getAccountNumber());

		if(microServiceTsysLoyaltyRewardsResponse == null) {
			logger.error(logCreditCardMSResponseNullable);

			microServiceTsysLoyaltyRewardsResponse = new MicroServiceResponse<>();

			final Error microServiceError = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_INTERNAL_SERVER_ERROR, internalServerErrorValue,
					"The response was null", balanceInformationValue);

			microServiceTsysLoyaltyRewardsResponse.getErrors().add(microServiceError);

			final String responseJson = json.toJson(microServiceTsysLoyaltyRewardsResponse); //

			model.put(jsonValue, responseJson);
			return new ModelAndView(jsonValue, model);
		}

		if(microServiceTsysLoyaltyRewardsResponse.getErrors().isEmpty()) {
			final TsysLoyaltyRewardsAccountInfoResponse tsysLoyaltyRewardsAccountInformation = 
					microServiceTsysLoyaltyRewardsResponse.getData().get(0);

			TsysLoyaltyRewardsInfo tsysLoyaltyRewardsInfo = new TsysLoyaltyRewardsInfo();

			if(tsysLoyaltyRewardsAccountInformation.getRewardsBalance() != null) {
				tsysLoyaltyRewardsInfo.setAvailableRewardsBalance(tsysLoyaltyRewardsAccountInformation
						.getRewardsBalance().getAvailable().getValue());
			}

			tsysLoyaltyRewardsInfo.setCanRedeemRewards(tsysLoyaltyRewardsAccountInformation.getCanRedeemRewards());
			tsysLoyaltyRewardsInfo.setRewardsAccountStatus(tsysLoyaltyRewardsAccountInformation.getRewardsAccountStatus());
			tsysLoyaltyRewardsInfo.setMinimumRewardsBalance(minimumCashRewardsRedemptionAmount);


			ccaAccountByFrontEndId.setTsysLoyaltyRewardsInfo(tsysLoyaltyRewardsInfo);

			logger.info(String.format(logSendingResponse + tsysLoyaltyRewardsAccountInformation.toString()));
			
			MicroServiceResponse<TsysLoyaltyRewardsInfo> convertedResponse = new MicroServiceResponse<>(); //
			convertedResponse.getData().add(tsysLoyaltyRewardsInfo);
			convertedResponse.setStatus(microServiceTsysLoyaltyRewardsResponse.getStatus());

			final String responseJson = json.toJson(convertedResponse); //

			model.put(jsonValue, responseJson);
			return new ModelAndView(jsonValue, model);
		}
		else {
			logger.error(String.format(logCreditCardsMSErrorDetail, microServiceTsysLoyaltyRewardsResponse.getErrors().get(0).getDetail()));

			final String responseJson = json.toJson(microServiceTsysLoyaltyRewardsResponse); //

			model.put(jsonValue, responseJson);
			return new ModelAndView(jsonValue, model);
		}
	}

	public String getUnicaLoanProducts() {
		return unicaLoanProducts;
	}

	public void setUnicaLoanProducts(String unicaLoanProducts) {
		this.unicaLoanProducts = unicaLoanProducts;
	}

	public String getUnicaCreditProducts() {
		return unicaCreditProducts;
	}

	public void setUnicaCreditProducts(String unicaCreditProducts) {
		this.unicaCreditProducts = unicaCreditProducts;
	}

	/**
	 * @param marketplaceCreditProducts the marketplaceCreditProducts to set
	 */
	public void setMarketplaceCreditProducts(String marketplaceCreditProducts) {
		this.marketplaceCreditProducts = marketplaceCreditProducts;
	}

	/**
	 * @return the unicaRefresh
	 */
	public String getUnicaRefresh() {
		return unicaRefresh;
	}

	/**
	 * @param unicaRefresh
	 *            the unicaRefresh to set
	 */
	public void setUnicaRefresh(String unicaRefresh) {
		this.unicaRefresh = unicaRefresh;
	}

	/**
	 * @return the unicaAllowedStatus
	 */
	public String getUnicaAllowedStatus() {
		return unicaAllowedStatus;
	}

	/**
	 * @param unicaAllowedStatus
	 *            the unicaAllowedStatus to set
	 */
	public void setUnicaAllowedStatus(String unicaAllowedStatus) {
		this.unicaAllowedStatus = unicaAllowedStatus;
	}
	
	/**
	 * @param marketplaceAllowedStatus
	 *            the marketplaceAllowedStatus to set
	 */
	public void setMarketplaceAllowedStatus(String marketplaceAllowedStatus) {
		this.marketplaceAllowedStatus = marketplaceAllowedStatus;
	}

	/**
	 * @param unicaBaseUrl the unicaBaseUrl to set
	 */
	public void setUnicaBaseUrl(String unicaBaseUrl) {
		this.unicaBaseUrl = unicaBaseUrl;
	}

	// CIBP-2761 PIN REQUEST
	public ModelAndView validatePinPassword(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> model = new HashMap<>();

		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		String password = ServletRequestUtils.getStringParameter(request, "pass");
		String id = ServletRequestUtils.getStringParameter(request, "id");
		String plasticNumEncrypted = ServletRequestUtils.getStringParameter(request, "info");

		if (id == null || plasticNumEncrypted == null) {
			model.put("text", "exception");
			return new ModelAndView("text", model);
		}

		if (password == null || password.trim().equals("")) {
			model.put("text", "empty");
			return new ModelAndView("text", model);
		}

		TVFrontendAccount account = Utils.getAccountById(id, session.getAccounts());
		String cardNumber = this.getCardPlasticNumberDecrypted(plasticNumEncrypted, session);
		CustomerInfoBean plasticInfo = this.getPlasticCustomerInfo(cardNumber, session, account, request);
		if (plasticInfo.getReturnCode().equalsIgnoreCase("C") || plasticInfo.getReturnCode().equalsIgnoreCase("G")) {
			if (plasticInfo.getVerificationId() != null && !plasticInfo.getVerificationId().isEmpty()) {
				if (session.getPinRequestAttempts() == null) {
					session.setPinRequestAttempts(new HashMap<String, Integer>());
				}
				Integer attempts = null;
				if (session.getPinRequestAttempts().containsKey(cardNumber)) {
					attempts = session.getPinRequestAttempts().get(cardNumber);
				} else {
					attempts = Integer.valueOf(0);
					session.getPinRequestAttempts().put(cardNumber, attempts);
				}
				if (plasticInfo.getVerificationId().equals(password)) {
					WebUtils.setSessionAttribute(request, "cardNumber", plasticNumEncrypted);
					model.put("text", "success");
					attempts = Integer.valueOf(3);
					session.getPinRequestAttempts().put(cardNumber, attempts);
					return new ModelAndView("text", model);
				} else {
					attempts++;
					session.getPinRequestAttempts().put(cardNumber, attempts);
					if (attempts == 2) {
						WebUtils.setSessionAttribute(request, "cardNumber", plasticNumEncrypted);
						model.put("text", "success");
					} else {
						model.put("text", "error");
					}
				}
			}
			return new ModelAndView("text", model);
		} else {
			model.put("text", "exception");
			return new ModelAndView("text", model);
		}
	}

	private String getCardPlasticNumberDecrypted(String encryptedNumber, UserSession userSession) {
		CryptoUtils crypto = new CryptoUtils();
		String num = "";

		try {
			if (userSession.getCryptoKey() == null || userSession.getCryptoKey().equals("")) {
				logger.error("crypto key not set in usersession");
			} else {
				num = crypto.decrypt(encryptedNumber, userSession.getCryptoKey());
			}
		} catch (Exception e) {
			logger.error("Error decrypting Credit Card Number");
			logger.error(e);
		}
		return num;
	}

	@SuppressWarnings("unchecked")
	private CustomerInfoBean getPlasticCustomerInfo(String plasticNumber, UserSession userSession,
			TVFrontendAccount account, HttpServletRequest request) {
		CustomerInfoBean custInfo = null;
		List<CustomerInfoBean> customerInfoLst = new ArrayList<CustomerInfoBean>();

		if ("VBVBR".equals(account.getProductDefinition().getProductId())) {
			customerInfoLst = (List<CustomerInfoBean>) WebUtils.getSessionAttribute(request, "ListInfoBean");
		} else {
			customerInfoLst = userSession.getProphItCustomerInfo().getCustomerInfoGroupAsList();
		}

		for (Iterator<CustomerInfoBean> iterator = customerInfoLst.iterator(); iterator.hasNext();) {
			CustomerInfoBean info = (CustomerInfoBean) iterator.next();
			if (plasticNumber.indexOf(info.getPlasticNumber()) >= 0) {
				custInfo = info;
				break;
			}
		}

		return custInfo;
	}
	
	
	// Mejora Disclaimer Check

	public ModelAndView OrderCheck(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();

		String accountNumber = request.getParameter("accountNumber");

		String url = null;
		TVFrontendAccount account = Utils.getAccountById(accountNumber, userSession.getAccounts());

		if (account != null && account.getProductDefinition() != null
				&& !GenericValidator.isBlankOrNull(account.getProductDefinition().getProductId())
				&& account.getProductDefinition().getProductId().equals("081")) {
			model.put("text", "newaccount");
			//model.put("newaccount", true);
		} else {
			if (account != null && account.getProductDefinition() != null
					&& account.getProductDefinition().getProductId() != null
					&& !account.getProductDefinition().getProductId().equals("")) {
				try {
					
					/**
					 * MBNA-1171
					 * Method getHarlandUrlByAccount : CIBP-WEB microservice integration
					 * 
					 */
					url = AccountsFacade.getInstance().getHarlandUrlByAccount(customer, account, "https://www.bancopopular.com/cibp-web/actions/accountservices", userSession);
					
				} catch (Exception e) {
					model.put("text", "urlerror");
					//model.put("urlerror", true);
					userSession.setStatusmessage("Unable to get the Order Check Url.");
				}

			}
			if (url != null)
				model.put("text", url);
				//model.put("url", url);
		}

		return new ModelAndView("text", model);
	}



	// FIN CIBP-2761 PIN REQUEST

	public String geteSignValidDepositAccounts() {
		return eSignValidDepositAccounts;
	}

	public void seteSignValidDepositAccounts(String eSignValidDepositAccounts) {
		this.eSignValidDepositAccounts = eSignValidDepositAccounts;
	}
	
	public ModelAndView tsysStatus(HttpServletRequest request, HttpServletResponse response) {
		String link = "";
		String frontEndId = request.getParameter("frontEndId");
		String index = request.getParameter("index");
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		// TODO Change to message source
		String lblActive = "Activo";
		String lblnoRegister = "No registrado";
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			lblActive = "Active";
			lblnoRegister = "Not Registered";
		}
		try {
			TVFrontendAccount acc = Utils.getAccountById(frontEndId, userSession.getAccounts());
			if (acc != null && acc.getIsAvailableElecStmt()) {
				CustomerInquiryBean responseTsys = customerService.prophItService()
						.makeCustomerInquiry(acc.getAccountNumber());
				if (responseTsys != null) {
					if (responseTsys.getEbillInd().equals(EbillIndType.ACTIVE.toString())) {
						link = "<a onclick=\"createIframe('ebppStatusUpdate?action=UPDATE_ESTMT_STATUS&amp;id="
								+ frontEndId + "', " + "'ebppIframe" + index + "', 'ebppPlaceholder" + index
								+ "', '400px', '300px');" + "destroyIframes('ebppPlaceholder" + index + "');\">"
								+ lblActive + "</a>";
					} else if (responseTsys.getEbillInd().equals(EbillIndType.ELIGIBLE.toString())) {
						link = "<a onclick=\"createIframe('ebppStatusUpdate?action=UPDATE_ESTMT_STATUS&amp;id="
								+ frontEndId + "'," + " 'ebppIframe" + index + "', 'ebppPlaceholder" + index
								+ "', '400px', '300px');" + "destroyIframes('ebppPlaceholder" + index + "');\">"
								+ lblnoRegister + "</a>";
					} else if (responseTsys.getEbillInd().equals(EbillIndType.NOELIGIBLE.toString())) {

					}
				}
			}
		} catch (Exception e) {
			logger.error("Tsys Inq Error");
		}
		model.put("text", link);
		return new ModelAndView("text", model);
	}
	
	
	public ModelAndView tsysStatusPayees(HttpServletRequest request, HttpServletResponse response) {
		String link = "";
		String frontEndId = request.getParameter("frontEndId");
		String index = request.getParameter("index");
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		// TODO Change to message source
		String lblActive = "Activo";
		String lblnoRegister = "No registrado";
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			lblActive = "Active";
			lblnoRegister = "Not Registered";
		}
		try {
			TVFrontEndPayee payee = Utils.getPayeebyId(frontEndId, userSession.getPayees(true));
			TVFrontendAccount acc = Utils.getAccountByAccountNumber(payee.getBillingAccount(), userSession.getAccounts());
			if(acc.getIsAvailableElecStmt()) {//fix only inquiry if IsAvailableElecStmt
				if (acc != null) {
					CustomerInquiryBean responseTsys = customerService.prophItService()
							.makeCustomerInquiry(acc.getAccountNumber());
					if (responseTsys != null) {
						if (responseTsys.getEbillInd().equals(EbillIndType.ACTIVE.toString())) {
							link = "<a onclick=\"createIframe('ebppStatusUpdate?action=UPDATE_EBILL_STATUS&amp;id="
									+ frontEndId + "', " + "'ebppIframe" + index + "', 'ebppPlaceholder" + index
									+ "', '400px', '300px');" + "destroyIframes('ebppPlaceholder" + index + "');\">"
									+ lblActive + "</a>";
						} else if (responseTsys.getEbillInd().equals(EbillIndType.ELIGIBLE.toString())) {
							link = "<a onclick=\"createIframe('ebppStatusUpdate?action=UPDATE_EBILL_STATUS&amp;id="
									+ frontEndId + "'," + " 'ebppIframe" + index + "', 'ebppPlaceholder" + index
									+ "', '400px', '300px');" + "destroyIframes('ebppPlaceholder" + index + "');\">"
									+ lblnoRegister + "</a>";
						} else if (responseTsys.getEbillInd().equals(EbillIndType.NOELIGIBLE.toString())) {
	
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Tsys Inq Error");
		}
		model.put("text", link);
		return new ModelAndView("text", model);
	}
	
	//MBSFE-454
	public ModelAndView generateNewCode(HttpServletRequest request, HttpServletResponse response) {
		
		String msg = "";	
		Map<String, Object> model = new HashMap<>();
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Integer counter = userSession.getCodeCounter();
		
		if(counter != 10){	
			
			CustomerProfile customer = userSession.getCustomerProfile();
			CustomerServices customerService = userSession.getCustomerService();
			Calendar cal = Calendar.getInstance();
			
			long mil = cal.getTimeInMillis(); 
			String code = String.valueOf(mil);
			code = code.substring(code.length() - 4, code.length());
			WebUtils.setSessionAttribute(request, "smsGeneratedCode", code);
			
			String phone = (String) WebUtils.getSessionAttribute(request, "smsPhonereceived");
			 String provider = (String) WebUtils.getSessionAttribute(request, "smsProvider");			
			
			try {
				SendSmsGeneratedCodeRequest codereq = new SendSmsGeneratedCodeRequest(
						Utils.generateCustomerInteractionEvent(request), customer, code, phone,
						provider);
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					customerService.sendGeneratedCode(codereq, PreferredLanguage.ENGLISH);
				else
					customerService.sendGeneratedCode(codereq, PreferredLanguage.SPANISH);
				
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					msg = "The code was sent again.";
				else
					msg = "El c&oacute;digo se envi&oacute; nuevamente.";
			} catch (Exception e) {
				if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
					msg = "Unable to enroll in Mi Banco Text at this time, please try later.";
				else
					msg = "Problemas al inscribir en Mi Banco Text en este momento, favor intentar luego.";
			}			
			userSession.setCodeCounter(counter+1);
		}
		else {
			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				msg = "You reached the maximum number of possible attempts.";
			else
				msg = "Alcanz&oacute; el m&aacute;ximo de intentos posibles.";
		}
		model.put("text", msg);
		return new ModelAndView("text", model);	
	}
	
	public ModelAndView vendorReceiptFrame(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		if(!GenericValidator.isBlankOrNull(request.getParameter("referenceNumber"))){
			VendorReceiptHistory vendorReceipt = customerService.getVendorReceiptHistory(request.getParameter("referenceNumber"));
			String message = vendorReceipt.getMessage();
			message = message.replaceAll("<a", "<a target=\"_blank\" ");
			message = message.replaceAll("<A", "<a target=\"_blank\" ");
			model.put("text", message);
		}

		return new ModelAndView("text", model);
	}
	
	public ModelAndView activateEAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<>();
		//Remove EAccount Activation
		return new ModelAndView("text", model);
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView deletePushDevice(HttpServletRequest request, HttpServletResponse response) {
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		Map<String, Object> model = new HashMap<>(NumberUtils.INTEGER_ONE);
		boolean success = false;
		int index = NumberUtils.toInt(request.getParameter("index"), NumberUtils.INTEGER_MINUS_ONE);
		//Since user can edit JS on client-side, a server-side validation is made to ensure the user passed RSA/OOB if shown
		boolean needsAuthentication =  (boolean) WebUtils.getSessionAttribute(request, "DeviceManagementAuth");
		
		if (index != NumberUtils.INTEGER_MINUS_ONE && !needsAuthentication) {
			List<PushTokensDto> dtos = (List<PushTokensDto>) WebUtils.getSessionAttribute(request, MobileAlert.PUSH_DEVICES);
			PushTokensDto dto = dtos.get(index);
			PushTokensRequest pushTokensRequest = new PushTokensRequest(dto);
			pushTokensRequest.setStatus(PushTokenStatus.INITIAL);
			List<PushTokensRequest> pushTokensRequestList = new ArrayList<>();
			pushTokensRequestList.add(pushTokensRequest);
			PushTokensResponse resp = PushTokensFacade.getInstance().putTokens(pushTokensRequestList, userSession);
			
			CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request, userSession.getUsername());
			event.setCustomStringData(PushTokenInteraction.TOKEN, pushTokensRequest.getToken());
			event.setCustomStringData(PushTokenInteraction.DEVICE_TYPE, pushTokensRequest.getDeviceType());
			event.setCustomStringData(PushTokenInteraction.DEVICE_MODEL, pushTokensRequest.getDeviceModel());
			event.setCustomStringData(PushTokenInteraction.DEVICE_NAME, pushTokensRequest.getDeviceName());
			event.setCustomStringData(PushTokenInteraction.STATUS, pushTokensRequest.getStatus().toString());
			event.setCustomStringData(PushTokenInteraction.STATUS_CHANGE_REASON, PushTokenConstants.DEVICE_REMOVAL);
			
			
			if (resp != null) {
				
				if (MobileResponseStatus.SUCCESS.equals(resp.getStatus())) {
					success = true;
					Map<Object, Object> alertParams = new HashMap<>();
					alertParams.put("deviceName", pushTokensRequest.getDeviceName());
					alertParams.put("name", userSession.getCustomerProfile().getFirstName());
					
					new Thread(() -> { 
						//Send alert in a new Thread, since customer does not need to wait for it to be sent
						customerService.sendPushDeviceAlert(userSession.getCustomerProfile(), alertParams, 
								userSession.getPreferredLanguage(), false);
						//Log push event
						userSession.getCustomerService().logPushTokenEvent(event, false, true);
						
					}).start();
					
					
				} else {
					logger.error("There was an error updating push device: " + resp.getError());
					userSession.getCustomerService().logPushTokenEvent(event, false, false);
				}
			}
		} else {    
			logger.error("Index received is out of array bounds or is not a number for push device list");
		}
		
		model.put("text", success);
		return new ModelAndView("text", model);
	}
	
	public String[] getRelationshipType() {
		return relationshipType;
	}

	public void setRelationshipType(String[] relationshipType) {
		this.relationshipType = relationshipType;
	}

	public String getHidePayees() {
		return hidePayees;
	}

	public void setHidePayees(String hidePayees) {
		this.hidePayees = hidePayees;
	}
	
	
	//END MBSFE-454
	
	public String getEpayrollId() {
		return epayrollId;
	}
	
	public String getEvendorId() {
		return evendorId;
	}
	
	public void setEpayrollId(String epayrollId) {
		this.epayrollId = epayrollId;
	}
	
	public void setEvendorId(String evendorId) {
		this.evendorId = evendorId;
	}
	
	public void setAlertsHistoryDays(String alertsHistoryDays) {
		this.alertsHistoryDays = alertsHistoryDays;
	}
	
	public ModelAndView getOnOffCardPlastics(HttpServletRequest request, 
			HttpServletResponse response) {
		Map<String, Object> model = new HashMap<>();
		
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
	
		if (userSession.getOnOffAccounts() == null 
				|| userSession.getOnOffAccounts().getTvFrontEndCardPlasticMap().isEmpty()
				|| userSession.getOnOffAccounts().getOnOffCardList().isEmpty()) {
			
			model.put("json", "[]");
			return new ModelAndView("json", model);			
		}
		
		OnOffCoreModel onOffCoreModel = new OnOffCoreModel();
		onOffCoreModel.setCustomerServices(userSession.getCustomerService());
		onOffCoreModel.setCustomerProfile(userSession.getCustomerProfile());
		onOffCoreModel.setClientId(OnOffEnums.CIBP_WEB.getValue());
		onOffCoreModel.setIp(userSession.getLastLoginIp());
		
		// ECCS Request
		OnOffUtils.manageOnOffStatus(userSession.getOnOffAccounts().getOnOffCardList(),
				userSession.getOnOffAccounts().getTvFrontEndCardPlasticMap(), onOffCoreModel);
		
		ArrayList<OnOffPortalResponse> onOffAccountList = new ArrayList<>();
		
		for (TVFrontendAccount account : userSession.getAccounts()) {
			
			if (account.isElegibleForOnOff()) {
				
				String frontEndId = account.getFrontEndId();
				int onOffCounter = account.getAthOnOffCount();
				boolean onOffElegible = account.isElegibleForOnOff();
				
				OnOffPortalResponse onOffAccount = new OnOffPortalResponse();
				onOffAccount.setFrontEndId(frontEndId);
				onOffAccount.setOnOffCount(onOffCounter);
				onOffAccount.setOnOffElegible(onOffElegible);
			
				onOffAccountList.add(onOffAccount);

			}
		}
		
		Gson gson = new Gson();
		String json = gson.toJson(onOffAccountList);
		
		model.put("json", json);
		return new ModelAndView("json", model);
			
	}

	public float getmMinimumCashRewardsRedemptionAmount() {
		return minimumCashRewardsRedemptionAmount;
	}

	public void setMinimumCashRewardsRedemptionAmount(float minimumCashRewardsRedemptionAmount) {
		this.minimumCashRewardsRedemptionAmount = minimumCashRewardsRedemptionAmount;
	}

	/**
	 * verifyRTPaymentAndRTNotification validate real time of Payee
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws Exception
	 */
	public ModelAndView verifyRTPaymentAndRTNotification(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return AjaxActionsCommercial.verifyRTPaymentAndRTNotification(request);
	}
	
	/**
	 * tsysLoyaltyRewardsAutomaticRedemption method 
	 *  requests information of the Cash Rewards user in order to obtain
	 *  information, create or remove the Automatic Redemption 
	 * remove the user from Automatic Redemption  
	 * @param request
	 * @param response
	 * @return ModelAndView
	 */
	public ModelAndView tsysLoyaltyRewardsAutomaticRedemption(HttpServletRequest request, 
			HttpServletResponse response) { 

		final String logEnteredMethod = "Entered tsysLoyaltyRewardsAutomaticRedemption in %s with username = %s"; // String value
		final String logExtractedParams = "Exctracted from params front end id = %s"; // String value
		final String logCouldNotExtractFrontEndId = "Could not extract frontEndId from params: %s"; // String value
		final String logAccountCouldNotBeFound = "Account could not be found using frontEndId = %s"; // String value
		final String logAccountToBeVPCBKAndUserPrimary = "Account has to be VPCBK and the user must be primary"; // String value
		final String logCreditCardMSResponseNull = "The creditcards micro service response is null"; // String value
		final String logSendingResponse = "Sending response = "; // String value
		final String logCreditCardsMSErrorDetail = "Creditcards micro service error detail = %s"; //
		final String badRequestValue = "Bad Request"; // String value
		final String parameterFrontEndIdValue = "Query parameter frontEndId was not provided"; // String value
		final String automaticRedemptionInformationValue = "tsysLoyaltyRewardsAutomaticRedemption"; // String value
		final String notFoundValue = "Not Found"; // String value
		final String requestedAccountNotFound = "The requested frontEndId was not found"; // String value
		final String internalServerErrorValue = "Internal Server Error"; // String value
		final String vpcbkValue = "VPCBK"; // String value
		final String methodActionError = "Method Action Error"; //String value
		

		final UserSession userSession = Utils.getUserSession(request); //User existence is verified by intercepter

		logger.info(Utils.neutralizeMessage(String.format(logEnteredMethod, this.getClass().getSimpleName(), userSession.getCustomerProfile().getUsername()))); 
		Map<String, Object> model = new HashMap<>(0); // object model-view return
		MicroServiceResponse<TLRRSCreditResponse> microServiceAutomaticRedemptionResponse = null; // Object Response
		TLRRSCreditRequest automaticRequest = new TLRRSCreditRequest(); //
		automaticRequest.setRecurringCashBackType(RecurringCashBackType.MONTHLY);
		String frontEndId = null; // Cash Rewards Account
		final Gson json = new Gson(); //

		try {
			frontEndId = ServletRequestUtils.getStringParameter(request, frontEndIdValue);
			logger.info(String.format(logExtractedParams, frontEndId));
		} catch (ServletRequestBindingException e) {

			logger.error(String.format(logCouldNotExtractFrontEndId + e.getMessage()));

			microServiceAutomaticRedemptionResponse = new MicroServiceResponse<>();

			final Error microServiceError = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_BAD_REQUEST, 
					badRequestValue, parameterFrontEndIdValue, automaticRedemptionInformationValue); //

			microServiceAutomaticRedemptionResponse.getErrors().add(microServiceError);  
			final String responseJson = json.toJson(microServiceAutomaticRedemptionResponse); //

			model.put(jsonValue, responseJson);
			return new ModelAndView(jsonValue, model);
		}

		final TVFrontendAccount ccaAccountByFrontEndId = Utils.getAccountById(frontEndId, 
				userSession.getCcaAccounts()); //


		if(ccaAccountByFrontEndId == null) {

			logger.error(String.format(logAccountCouldNotBeFound, frontEndId));

			microServiceAutomaticRedemptionResponse = new MicroServiceResponse<>();

			final Error microServiceError = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_NOT_FOUND,
					notFoundValue, requestedAccountNotFound, automaticRedemptionInformationValue); //

			microServiceAutomaticRedemptionResponse.getErrors().add(microServiceError);

			final String responseJson = json.toJson(microServiceAutomaticRedemptionResponse); //

			model.put(jsonValue, responseJson);
			return new ModelAndView(jsonValue, model);
		}

		if(!ccaAccountByFrontEndId.getAccountProductId().equalsIgnoreCase(vpcbkValue) 
				|| !ccaAccountByFrontEndId.isPrimaryAccount()) {

			logger.error(logAccountToBeVPCBKAndUserPrimary);

			microServiceAutomaticRedemptionResponse = new MicroServiceResponse<>();

			final Error microServiceError = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_BAD_REQUEST, 
					badRequestValue, "The requested must be made with a VPCBK and a primary owner",
					automaticRedemptionInformationValue); //

			microServiceAutomaticRedemptionResponse.getErrors().add(microServiceError);

			final String responseJson = json.toJson(microServiceAutomaticRedemptionResponse); //

			model.put(jsonValue, responseJson);
			return new ModelAndView(jsonValue, model);
		}

		String method = ""; //
		try {
			method = ServletRequestUtils.getStringParameter(request, "methodAction");
		} catch (ServletRequestBindingException e) {
			logger.error(methodActionError +" " + e.getMessage());
		}
		
		//Depending on the Method Sent,it will execute the TsysAPIAction
		//The default will be a GET
		TsysApiAction action = TsysApiAction.GET; //
		switch (method) {
		case "GET":
			action = TsysApiAction.GET;
			break;
		case "POST":
			action = TsysApiAction.CREATE;
			break;
		case "PUT":
			action = TsysApiAction.UPDATE;
			break;
		case "DELETE":
			action = TsysApiAction.DELETE;
			break;
		default: 
			action = TsysApiAction.GET;
			break;
		}
		
		//This call is made using ccaAccountByFrontEndId.getAccountNumber() which really holds 
		//the 16 digits of the "main plastic" and not the unique 11 digit account id in Tsys
		//thus its passed in the cardNumber parameter.
		//This will obtain the values
		try {
			microServiceAutomaticRedemptionResponse = CreditCardFacade.getInstance()
					.tLRRecurringStatementCredit(userSession, action, null, 
							ccaAccountByFrontEndId.getAccountNumber(), automaticRequest);
		}
		catch(RetryableException exception) {
			logger.info("Error consuming automatic redemption.");
			microServiceAutomaticRedemptionResponse = null;
		}
		logger.info(microServiceAutomaticRedemptionResponse);
		
		if(microServiceAutomaticRedemptionResponse == null) {
			logger.error(logCreditCardMSResponseNull);

			microServiceAutomaticRedemptionResponse = new MicroServiceResponse<>();

			final Error microServiceError = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_INTERNAL_SERVER_ERROR, 
					internalServerErrorValue, "The response was null", automaticRedemptionInformationValue); //

			microServiceAutomaticRedemptionResponse.getErrors().add(microServiceError);

			final String responseJson = json.toJson(microServiceAutomaticRedemptionResponse); //

			model.put(jsonValue, responseJson);
			return new ModelAndView(jsonValue, model);
		}

		if(microServiceAutomaticRedemptionResponse.getErrors().isEmpty()) {
			final TLRRSCreditResponse tsysLoyaltyRewardsAccountInformation = 
					microServiceAutomaticRedemptionResponse.getData().get(0); //
			
			TsysLoyaltyRewardsInfo tsysLoyaltyRewardsInfo = ccaAccountByFrontEndId.getTsysLoyaltyRewardsInfo(); //
			
			//tsysLoyaltyRewardsInfo.setRecurringCashBackType(tsysLoyaltyRewardsAccountInformation.getRecurringCashBackType().toString());
        	final evertec.creditcards.request.TsysLoyaltyRewardsRecurringStatementCreditRequest.RecurringCashBackType type = evertec.creditcards.request.TsysLoyaltyRewardsRecurringStatementCreditRequest.RecurringCashBackType.valueOf("MONTHLY"); //
			tsysLoyaltyRewardsInfo.setRecurringCashBackType(type);

			
			logger.info(String.format(logSendingResponse + tsysLoyaltyRewardsAccountInformation.toString()));

			//Retrieves the response status of the method. This is important since
			//we need to verify later on if the user is enrolled on Automatic Redemption (status 404)
			//The default status is 200
			microServiceAutomaticRedemptionResponse.getStatus();
			final String responseJson = json.toJson(microServiceAutomaticRedemptionResponse); //
			final String automaticRedemtionStatus = (action.equals(TsysApiAction.CREATE)) ? "activate": "deactivate"; //automatic redemption status
			final String nameValue = "name"; // Value String
			
			model.put(jsonValue, responseJson);
			
			if (action.equals(TsysApiAction.CREATE) || action.equals(TsysApiAction.DELETE)) {
				if (!GenericValidator.isBlankOrNull(userSession.getCustomerProfile().getEmail())) {
					
					model.put(nameValue, userSession.getCustomerProfile().getFirstName());
					
					userSession.getCustomerService().sendCashRewardsAutomaticRedemptionEmail(userSession.getCustomerProfile(), model,
							userSession.getPreferredLanguage(), automaticRedemtionStatus);
				}	
			}
			return new ModelAndView(jsonValue, model);
		}
		else {
			logger.error(String.format(logCreditCardsMSErrorDetail, microServiceAutomaticRedemptionResponse
					.getErrors().get(0).getDetail()));
			final String responseJson = json.toJson(microServiceAutomaticRedemptionResponse); //

			model.put(jsonValue, responseJson);
			return new ModelAndView(jsonValue, model);
		}
	}

	/**
     * This method update in database the user preferences of privacy settings cookies.
     * @author ET58344, Evertec Inc.
     * @since 01-28-2020
     * 
     * @param request request http
     * @param response response http
     * @throws Exception error exception
     */
	public void updateCookiePrivacySettings(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Object session = WebUtils.getSessionAttribute(request, "userSession"); //session obj
		UserSession userSession = new UserSession(); //user session
		
		if(session instanceof UserSession) {
			userSession = (UserSession) session; //user session
		}
		

		String cookieCategory = request.getParameter("cookieCategory"); //category
		CookiePreferenceServiceModule cpsm = new CookiePreferenceServiceModule(); //service module
		String status = request.getParameter("status"); //status
		CookiePreferenceStatus cookiePreferenceToUpdate = new CookiePreferenceStatus(); //row to update
		final String inputAnalytic= "swAnalytic"; //input analytic
		final String inputFunctional= "swFunctional"; //input functional
		final String inputAdvertising= "swAdvertising"; //input advertising
		final String statusDisabled = "DISABLED"; //status disabled
		String cookieChanged = StringUtils.EMPTY; //cookie changed
		
		switch (cookieCategory) {
		case inputAnalytic:
			
			cookiePreferenceToUpdate = ListUtils.getCookiePreferenceFromList(
					userSession.getCookiePreferenceStatusList(), CookieCategory.ANALYTICS);
			cookiePreferenceToUpdate.setStatus(status);
			registerEvent(request,userSession,status,cookieCategory);
			cookieChanged = CookieCategory.ANALYTICS.toString();
			break;
		case inputFunctional:
			
			cookiePreferenceToUpdate = ListUtils.getCookiePreferenceFromList(
					userSession.getCookiePreferenceStatusList(), CookieCategory.FUNCTIONAL);
			cookiePreferenceToUpdate.setStatus(status);
			registerEvent(request,userSession,status,cookieCategory);
			cookieChanged = CookieCategory.FUNCTIONAL.toString();
			break;
		case inputAdvertising:
			
			cookiePreferenceToUpdate = ListUtils.getCookiePreferenceFromList(
					userSession.getCookiePreferenceStatusList(), CookieCategory.ADVERTISING);	
			cookiePreferenceToUpdate.setStatus(status);
			registerEvent(request,userSession,status,cookieCategory);
			cookieChanged = CookieCategory.ADVERTISING.toString();
			break;		
		default:
			break;
		}
		
		customerService.getCookiePreferenceStatusService().updateCookiePreferenceStatus(
				cookiePreferenceToUpdate);
		
		/*Si se deshabilita la(s) cookie(s) se deben borrar del navegador*/
		if(status.equals(statusDisabled)) {
			cpsm.deleteSelectedCookies(cookieChanged, response);
		}
	}

	private void registerEvent(HttpServletRequest request,UserSession userSession,String status,String category){
		CustomerInteractionEvent customerInteractionEvent=Utils.generateCustomerInteractionEvent(request);
		String header = request.getHeader("user-agent");
		if(userSession.getUserAgentType() == UserAgentType.WEB ||  userSession.getUserAgentType() == UserAgentType.IPHONE ||  userSession.getUserAgentType() == UserAgentType.ANDROID) {
			customerInteractionEvent.setCustomStringData(DeviceUtils.CONNECTION_INFO, DeviceUtils.getBrowserInfo(header, userSession));
		} else if (userSession.getUserAgentType() == UserAgentType.JSON) {
			customerInteractionEvent.setCustomStringData(DeviceUtils.CONNECTION_INFO, DeviceUtils.getMobileInfo(header, userSession.getDeviceInfo()));
		}
		final String inputAnalytic= "swAnalytic"; //input analytic
		final String inputFunctional= "swFunctional"; //input functional
		final String inputAdvertising= "swAdvertising"; //input advertising
		switch(category){
			case inputAnalytic:
				customerService.getInteractionService().logAnalyticsCookiesActivation(customerInteractionEvent, InteractionSeverity.INFO,status);
				break;
			case inputFunctional:
				customerService.getInteractionService().logFunctionalCookiesActivation(customerInteractionEvent,InteractionSeverity.INFO,status);
				break;
			case inputAdvertising:
				customerService.getInteractionService().logAdvertisingCookiesActivation(customerInteractionEvent,InteractionSeverity.INFO,status);
				break;
			default:
				break;
		}

	}

	/**
     * fisRetirementPlanInformation method to get the User Retirement Plan Information
     * @param request
     * @param response
     * @return ModelAndView
     */
    public ModelAndView fisRetirementPlanInformation(HttpServletRequest request, 
    		HttpServletResponse response) {

    	final UserSession userSession = Utils.getUserSession(request); //User existence is verified by intercepter

    	Map<String, Object> model = new HashMap<String, Object>(0); // object model-view return

    	CustomerProfile profile = userSession.getCustomerProfile();

    	final Gson json = new Gson();

    	/**
    	 * @author jortiz
    	 * @mbfis462
    	 * Get the retirement plan of the user if the user have one retirement plan.
    	 */

    	boolean globalRetirementPlanEnable = false; //GlobalEntitlement RETIREMENT_PLAN

    	if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_CIBP)) {

    		globalRetirementPlanEnable = CustomerFacade.getInstance().getGlobalEntitlement(userSession, EntitlementType.RETIREMENT_PLAN).getStatus() == EntitlementStatus.ENABLED; // check if global entitlement retirement plan is enabled

    		//globalRetirementPlanEnable = true;

    		if (userSession.getRetirementPlan() == null) {
    			getRetirementPlans(userSession);
    		}
    		if (userSession.isHasRetirementPlan()) {

    			final String retPlanInfo = "retplan";//var to put in model

				RetirementPlanInfoResponse  retplan = RetirementPlanUtil.generateFilteredResponse(userSession.getRetirementPlan());
    			
    			model.put(retPlanInfo, retplan);
    		}
    		
    		model =	globalEntRetPlan(userSession, profile, model, globalRetirementPlanEnable);
    		
    	}

    	final String hasRetPlan = "hasRetPlan"; // bool that says if the client has a Retirement plan

    	model.put(hasRetPlan, userSession.isHasRetirementPlan());

    	//MBFIS-638 MB Web New Alert for the Retirement Plan Relation newRetirementPlanAccoutSectBadge
    	if (userSession.isHasRetirementPlan() && globalRetirementPlanEnable) 
    	{
    		final String rpBadge = "retirementPlanAccountNewBadge"; //Retirement plan new badge key

    		model.put(rpBadge, userSession.hasRetirementPlanBudge());

    	}
    	
    	userSession.setRetPlanCallback(true);

    	final String responseJson = json.toJson(model); 
    	ModelAndView newModel = new ModelAndView();

    	newModel.setViewName(jsonValue);
    	newModel.getModel().put(jsonValue, responseJson);

    	return newModel;

    }

/**
 * @author jortiz
 * @mbfis462
 * This method return the retirement plan of the user if the user already have one retirement plan if not will return null.
 * @param userSession extract the ssn 
 * @return RetirementPlanBalanceResponse
 */
    protected RetirementPlanBalanceResponse getRetirementPlans(UserSession userSession){
		RetirementPlanBalanceResponse retirementPlanResponse = new RetirementPlanBalanceResponse();//retirement plan response
    	try {

    		GetRetirementPlanBalanceFilterParameters filter = new GetRetirementPlanBalanceFilterParameters();// request filter

    		filter.setSsNum(userSession.getCustomerProfile().getTaxId());

    		retirementPlanResponse = retirementPlanClient.getRetirementPlanBalance(filter);

    		if(retirementPlanResponse.getPlans() != null && retirementPlanResponse.getPlans().size() > 0 ) {
    			userSession.setRetirementPlan(retirementPlanResponse);
    			userSession.setHasRetirementPlan(true);
    		}else {
    			userSession.setRetirementPlan(retirementPlanResponse);
    		}
    	}catch(Exception e) {
    		logger.debug(e);
    	}

    	return retirementPlanResponse;

}
   

/**
 * @author jortiz
 * @mbfm176
 * This method call the create Entitlement method for the users that have a retirement plan 
 * @param userSession 
 * @param profile 
 * @param model 
 * @param globalRetirementPlanEnable 
 */
    protected Map<String, Object> globalEntRetPlan(UserSession userSession,CustomerProfile profile,Map<String, Object>  model,boolean globalRetirementPlanEnable){

    	final String retplanDowntimeMessage = "retplanDowntimeMessage"; //Retirement plan downtime message

    	/**
		 * MBFIS-809 Show a message when CSR Downtime is
		 **/
		if (!globalRetirementPlanEnable) {
			model.put(retplanDowntimeMessage, Boolean.TRUE);
		}else {
			model.put(retplanDowntimeMessage, Boolean.FALSE);
		}
		
		/**
		 * MBFIS-515 Create Retirement Plan entitlement for customers with Retirement Plan Accounts if the customer does not already have it
		 **/
		if (userSession.isHasRetirementPlan() && !profile.hasEntitlement(EntitlementType.RETIREMENT_PLAN)) {
			this.createRetirementPlanEntt(userSession);
		}
		
		return model;
    	
}
    
/**
 * MBFIS-515 Create Retirement Plan Entitlement
 *
 * @param userSession
 * @author iahumada
 */
    private void createRetirementPlanEntt (UserSession userSession) {
    	CustomerEntitlement retirementPlanEntt = new evertec.cibp.core.models.entitlement.RetirementPlanEntt(
    			userSession.getRetirementPlan().plansInfoToStringList(),
    			0);//entitlement to save on db

    	retirementPlanEntt.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
    	retirementPlanEntt.setGlobalEntitlement(CustomerFacade.getInstance().getGlobalEntitlement(userSession, EntitlementType.RETIREMENT_PLAN));
    	retirementPlanEntt.setType(EntitlementType.RETIREMENT_PLAN);
    	retirementPlanEntt.setCustomerProfile(userSession.getCustomerProfile());

    	customerService.addEntitlement(userSession.getCustomerProfile(), retirementPlanEntt);
	}

	public RetirementPlanClient getRetirementPlanClient() {
		return retirementPlanClient;
	}

	public void setRetirementPlanClient(RetirementPlanClient retirementPlanClient) {
		this.retirementPlanClient = retirementPlanClient;
	}
	



}
