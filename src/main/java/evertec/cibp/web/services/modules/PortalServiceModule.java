package evertec.cibp.web.services.modules;

import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import evertec.cibp.core.models.account.AccountNumbers;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.accountopening.AccountApplication;
import evertec.cibp.core.models.accountopening.AccountApplicationStatus;
import evertec.cibp.core.models.accountopening.AccountApplications;
import evertec.cibp.core.models.vault.VaultAccount;
import evertec.cibp.core.utils.DisposableIdentifier;

public class PortalServiceModule {

	private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PortalServiceModule.class);

	public List<TVFrontendAccount> createTVFrontendAccountOD(List<VaultAccount> accounts, AccountApplications applications) {

		List<TVFrontendAccount> genericAccounts = new LinkedList<>();

		for (Iterator<VaultAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			VaultAccount act = iterator.next();
			
			TVFrontendAccount genAct = new TVFrontendAccount(act);

			DisposableIdentifier id = new DisposableIdentifier();
			genAct.setFrontEndId(id.getNextDisposableId());
			genAct.setProductDefinition(act.getProductDefinition());
			genAct.setFeatures(act.getFeatures());

//			if (commonService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus() != EntitlementStatus.ENABLED)
//				genAct.getFeatures().setEStatement(false);
			
			manageOwners(act, genAct);
//			manageAccountPreferences(customerProfile, genAct);
//			manageNicknames(customerProfile, genAct);

			AccountNumbers accNumbers = manageNewAccountNumbers(applications);
			if (accNumbers.getFilternewAccountNumbers().contains(act.getId()))
				manageAccountApplication(applications, genAct);

			genericAccounts.add(genAct);

		}
		return genericAccounts;
	}
	
	public AccountNumbers manageNewAccountNumbers(AccountApplications applications) {
		AccountNumbers accNumbers = new AccountNumbers();
		List<String> newAccountNumbers = new LinkedList<>();
		List<String> filternewAccountNumbers = new LinkedList<>();
		
		if (applications == null) {
			accNumbers.setNewAccountNumbers(newAccountNumbers);
			accNumbers.setFilternewAccountNumbers(filternewAccountNumbers);
			return accNumbers;
		}
		
		if (applications.getCompletedApplications() != null)
			for (AccountApplication object : applications.getCompletedApplications()) {
				if (object.getStatus() == AccountApplicationStatus.COMPLETE && object.isAccountHasBalanceInBackend())
					filternewAccountNumbers.add(object.getAccountNumber());
				else
					newAccountNumbers.add(object.getAccountNumber());
			}
		
		if (applications.getPendingApplications() != null)
			for (AccountApplication object : applications.getPendingApplications())
				newAccountNumbers.add(object.getAccountNumber());
		
		accNumbers.setNewAccountNumbers(newAccountNumbers);
		accNumbers.setFilternewAccountNumbers(filternewAccountNumbers);
		
		return accNumbers;
	}
	
	private void manageOwners(VaultAccount act, TVFrontendAccount account) {
		String relationshipType = act.getRelationshipType();
		
		if ("062".equalsIgnoreCase(relationshipType)) {
			account.setGuardianAcc(true);
			account.setGuardianOrRepPayeeAcc(true);
		} else if ("090".equalsIgnoreCase(relationshipType)) {
			account.setGuardianOrRepPayeeAcc(true);
		}
	}

	
	
	private void manageAccountApplication(AccountApplications applications, TVFrontendAccount account) {
		account.setNewAccount(true);
		AccountApplication accountApplicationByAccountNumber = getAccountApplicationByAccountNumber(
				account.getAccountNumber(), applications.getCompletedApplications());

		if (accountApplicationByAccountNumber == null)
			return;
		
		Calendar cal60 = Calendar.getInstance();
		cal60.setTime(accountApplicationByAccountNumber.getCreationDate());
		cal60.add(Calendar.DAY_OF_MONTH, 60);

		account.setShowFullAccount(accountApplicationByAccountNumber.getStatus().equals(
				AccountApplicationStatus.COMPLETE)
				&& accountApplicationByAccountNumber.isSignatureCardComplete()
				&& cal60.after(Calendar.getInstance()));

		if (account.getNickname().equals(""))
			account.setNickname(accountApplicationByAccountNumber.getAccountProductDescription());

		if (!accountApplicationByAccountNumber.isAccountAvailableInBackend()) {
			accountApplicationByAccountNumber.setAccountAvailableInBackend(true);
			//TODO: pendingAccountOpeningService.notifyAccountAvailableInBackend(accountApplicationByAccountNumber);
		}

		if (!accountApplicationByAccountNumber.isAccountHasBalanceInBackend() && account.getPortalBalance() != null) {
			accountApplicationByAccountNumber.setAccountHasBalanceInBackend(true);
			//TODO: pendingAccountOpeningService.notifyAccountBalanceAvailableInBackend(accountApplicationByAccountNumber);
		}
	}
	
	/*
	private void manageAccountPreferences(CustomerProfile customerProfile, TVFrontendAccount account) {
		Map<String, CustomerAccountPreferences> map = accountPreferencesService.loadAccountPreferences(customerProfile);
		CustomerAccountPreferences customerAccountPreferences = map.get(account.getAccountKey());

		if (customerAccountPreferences == null) {
			CustomerAccountPreferences cap = new CustomerAccountPreferences();
			cap.setKey(account.getAccountKey());
			account.setCustomerAccountPreferences(cap);
			cap.setNickname(account.getDescription());
			cap.setHide(false);
			cap.setDepositLimit("");
			cap.setRdcStatus("");
		} else
			account.setCustomerAccountPreferences(customerAccountPreferences);
		
		account.setHide(account.getCustomerAccountPreferences().getHide());
	}
	/*
	
	/*
	private void manageNicknames(CustomerProfile customerProfile, TVFrontendAccount account) {
		if (account.getCustomerAccountPreferences() == null || GenericValidator.isBlankOrNull(account.getCustomerAccountPreferences().getNickname()))
			return;
		
		if (!account.getCustomerAccountPreferences().getNickname().trim().equals("")) {
			account.setNickname(account.getCustomerAccountPreferences().getNickname());
			return;
		}
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBIM125)) {
			if (!account.getDescription().trim().equals("")) {
				account.setNickname(account.getDescription());
				account.getCustomerAccountPreferences().setNickname(account.getDescription());
				accountPreferencesService.updateAccountPreferences(account, customerProfile);
				return;
			}
			
			String nickname = account.getSubtype()+" Account";
			account.setNickname(nickname);
			account.getCustomerAccountPreferences().setNickname(nickname);
			accountPreferencesService.updateAccountPreferences(account, customerProfile);
		}
	}
	 */
	private AccountApplication getAccountApplicationByAccountNumber(String accountNumber,
			List<AccountApplication> applications) {

		for (AccountApplication accountApplication : applications) {
			if (accountApplication.getAccountNumber().equals(accountNumber))
				return accountApplication;
		}

		return null;

	}

	/*
	public List<TVFrontendAccount> reloadAccountPreferences(CustomerProfile customerProfile,
			List<TVFrontendAccount> accounts) {
		List<TVFrontendAccount> reloadAccounts = new LinkedList<>();

		if (customerProfile == null)
			throw new ProgrammingError("Customer Profile must be defined.");

		if (accounts.isEmpty())
			throw new RuntimeException("No accounts received for customer.");

		Map<String, CustomerAccountPreferences> map = accountPreferencesService.loadAccountPreferences(customerProfile);

		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount act = iterator.next();

			CustomerAccountPreferences customerAccountPreferences = map.get(act.getAccountKey());
			if (customerAccountPreferences == null) {

				CustomerAccountPreferences cap = new CustomerAccountPreferences();
				cap.setKey(act.getAccountKey());
				act.setCustomerAccountPreferences(cap);
				cap.setNickname(act.getDescription());
				cap.setHide(false);
				cap.setRdcStatus("");
				cap.setDepositLimit("");
			} else
				act.setCustomerAccountPreferences(customerAccountPreferences);

			if (act.getCustomerAccountPreferences() != null
					&& act.getCustomerAccountPreferences().getNickname() != null
					&& !act.getCustomerAccountPreferences().getNickname().trim().equals(""))
				act.setNickname(act.getCustomerAccountPreferences().getNickname());

			if (act.getCustomerAccountPreferences() != null)
				act.setHide(act.getCustomerAccountPreferences().getHide());

			reloadAccounts.add(act);
		}
		return reloadAccounts;
	}
	*/


//	public List<String> getPortalMessages(String taxId, String lang) {
//		return interactionService.getPortalMessages(taxId, lang);
//	}

//	public CommonServicesModule getCommonService() {
//		return commonService;
//	}
//
//	public void setCommonService(CommonServicesModule commonService) {
//		this.commonService = commonService;
//	}

}