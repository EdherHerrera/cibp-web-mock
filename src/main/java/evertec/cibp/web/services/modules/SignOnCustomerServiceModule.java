package evertec.cibp.web.services.modules;

import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.services.requests.DeleteUserRSARequest;
import evertec.cibp.core.utils.ApplicationsDefinition;
import evertec.cibp.web.facade.EnrollmentFacade;
import evertec.cibp.web.session.UserSession;
import org.apache.commons.lang.StringEscapeUtils;

import java.rmi.RemoteException;

/**
 * As part of the microservices architecture movement, core Business logic is being refactored and moved to the web for microservice consumption.
 * This class contains signon service operations orchestration for microservice consumption
 * @author et55498
 *
 */
public class SignOnCustomerServiceModule {

	private final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SignOnCustomerServiceModule.class);



	/** The instance. */
	private static SignOnCustomerServiceModule instance = null;

	/** Instantiates a new SignOnCustomerServiceModule. */
	protected SignOnCustomerServiceModule() {}

	/**
	 * Gets the single instance of SignOnCustomerServiceModule.
	 * 
	 * @return single instance of SignOnCustomerServiceModule
	 */
	public static SignOnCustomerServiceModule getInstance() {
		if ( instance == null ) {
			instance = new SignOnCustomerServiceModule();
		}
		return instance;
	}

	/**
	 * Orchestration method for deleting a user reservation
	 * @param userSession The userSession
	 */
	public void deleteUserReservation(UserSession userSession) {
		if (userSession == null)
			throw new ProgrammingError("UserSession cannot be null");
		if(userSession.getUsernameReservation() == null)
			throw new ProgrammingError("usernameReservation cannot be null");


		/** MBNA-3046 **/
		boolean deleteSuccess = EnrollmentFacade.getInstance().deleteUserReservationFromDatabase(userSession);

		if(!deleteSuccess){
			logger.error(StringEscapeUtils.escapeJava("Unable to delete username from database: " + userSession.getUsernameReservation().getUsername()));
		}
		try{
			/** MBNA-1967 **/
			DeleteUserRSARequest deleteUserRSARequest = new DeleteUserRSARequest(userSession.getUsernameReservation().getUsername(), ApplicationsDefinition.CIBP);
			boolean deleteSuccessRSA =  userSession.getCustomerService().getEnrollmentService()
					.getCommonService().deleteUserRSA(deleteUserRSARequest);

		} catch (
				RemoteException e) {
			logger.error("Unable to delete username from RSA: " + userSession.getUsernameReservation().getUsername(), e);
		}

	}

}
