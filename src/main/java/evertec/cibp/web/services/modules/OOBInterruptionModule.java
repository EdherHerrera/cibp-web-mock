/**
 * 
 */
package evertec.cibp.web.services.modules;

import java.util.HashMap;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.GenericValidator;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.Utils;

/**
 * @author S685758
 *
 */
public class OOBInterruptionModule {

	/**
	 * CustomerService instance.
	 */
	private CustomerServices customerServices;
	
	/**
	 * HashMap events.
	 */	
	private static final HashMap<String, CustomerInteractionEventType> events = setEvents();
	
	private static HashMap<String, CustomerInteractionEventType> setEvents() {
		HashMap<String, CustomerInteractionEventType> events = new HashMap<>();
		events.put("oob_intpg_display", CustomerInteractionEventType.OOB_INTERRUPTION_PAGE_DISPLAY);
		events.put("oob_intpg_skip", CustomerInteractionEventType.OOB_INTERRUPTION_PAGE_SKIP);
		events.put("oob_intpg_continue", CustomerInteractionEventType.OOB_INTERRUPTION_PAGE_CONTINUE);
		
		events.put("intpg_change_sms_phone_success", CustomerInteractionEventType.INTPG_CHANGE_SMS_PHONE_SUCCESS);
		events.put("intpg_change_email_fail", CustomerInteractionEventType.INTPG_CHANGE_EMAIL_FAIL);
		events.put("intpg_change_email_success", CustomerInteractionEventType.INTPG_CHANGE_EMAIL_SUCCESS);
		
		events.put("intpg_enrollment_oob", CustomerInteractionEventType.INTPG_ENROLLMENT_OOB);
		events.put("intpg_enrollment_sms_oob", CustomerInteractionEventType.INTPG_ENROLLMENT_SMS_OOB);
		events.put("intpg_enrollment_voice_oob", CustomerInteractionEventType.INTPG_ENROLLMENT_VOICE_OOB);
		events.put("intpg_frecuency_as_required_oob", CustomerInteractionEventType.INTPG_FREQUENCY_AS_REQUIRED_OOB);
		events.put("intpg_info_sms_alerts", CustomerInteractionEventType.INTPG_INFO_SMS_ALERTS);
		
		return events;
	}
	
	

	public OOBInterruptionModule() {
		super();
	}

	public OOBInterruptionModule(CustomerServices customerServices) {
		super();
		this.customerServices = customerServices;
	}

	public void logOOBInterruptionEvents(HttpServletRequest request, UserSession userSession, String eventType) {

		CustomerInteractionEvent customerInteractionEvent = Utils.generateCustomerInteractionEvent(request); //

		CustomerProfile customerProfile = Optional.ofNullable(userSession.getCustomerProfile())
				.orElse(new CustomerProfile());

		String strUserAgent = request.getHeader("user-agent");
		String stringTrace = Utils.generateOOBInterruptionEventTraceInformation(strUserAgent, userSession); //
		
		if (!GenericValidator.isBlankOrNull(stringTrace)) {
			customerInteractionEvent.setCustomStringData(DeviceUtils.CONNECTION_INFO, stringTrace);
		}

		customerInteractionEvent.setProfileId(customerProfile.getProfileId());
		customerInteractionEvent.setUsername(customerProfile.getUsername());

		getCustomerServices().logOOBInterruptionPage(customerInteractionEvent, events.get(eventType));
	}

	/**
	 * @return the customerServices
	 */
	public CustomerServices getCustomerServices() {
		return customerServices;
	}

	/**
	 * @param customerServices the customerServices to set
	 */
	public void setCustomerServices(CustomerServices customerServices) {
		this.customerServices = customerServices;
	}

	/**
	 * @return the events
	 */
	public static HashMap<String, CustomerInteractionEventType> getEvents() {
		return events;
	}	
}
