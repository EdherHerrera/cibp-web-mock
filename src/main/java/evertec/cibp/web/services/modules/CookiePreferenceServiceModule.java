package evertec.cibp.web.services.modules;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.utils.CookieUtils;

/**
 * Service Module Cookie Preference Controller
 * 
 * @author ET58344, Evertec Inc.
 * 
 * @since 02-22-2021
 * @version 1.0
 */
public class CookiePreferenceServiceModule {

	private CustomerServices customerService; // customer service
	private static final String ANALYTICS_COOKIE_STR = "ANALYTICS"; // analytics cookie
	private static final String ADVERTISING_COOKIE_STR = "ADVERTISING"; // advertising cookie
	private static final String FUNCTIONAL_COOKIE_STR = "FUNCTIONAL"; // functional cookie
	private static final String COOKIE_STATUS_ENABLED = "ENABLED"; // final str status enabled
	private static final String COOKIE_STATUS_DISABLED = "DISABLED"; // final str status disabled
	private static final String[] ANALYTIC_COOKIES = { "_ga", "_gat", "_gat_legacyProperty", "_gid" }; // analytic
																										// cookies
	private static final String[] COOKIE_CATEGORIES = { ANALYTICS_COOKIE_STR, ADVERTISING_COOKIE_STR,
			FUNCTIONAL_COOKIE_STR }; // list analytic cookies

	/**
	 * Default constructor
	 * @author ET58344, Evertec Inc.
	 * @since 03-03-2021
	 */
	public CookiePreferenceServiceModule() {
		this.customerService = null;
	}
	
	/**
	 * Injection parameter contructor
	 * @author ET58344, Evertec Inc.
	 * @since 03-02-2021
	 * @param customerService service
	*/
	public CookiePreferenceServiceModule(CustomerServices customerService) {
		this.customerService = customerService;
	}	
	
	/**
	 * This method create initial preference to profile.
	 * 
	 * @author ET58344, Evertec Inc.
	 * 
	 * @since 02-22-2021
	 * @param profileID parameter
	 * @return List<CookiePreferenceStatus> result
	 */
	public List<CookiePreferenceStatus> createProfileCookiePreference(String profileID) {

		CookiePreferenceStatus preferenceToSave = new CookiePreferenceStatus(); // preference object

		for (String eachCookie : COOKIE_CATEGORIES) {

			preferenceToSave = new CookiePreferenceStatus();
			preferenceToSave.setCategory(eachCookie);
			preferenceToSave.setProfileId(profileID);
			preferenceToSave.setStatus(COOKIE_STATUS_ENABLED);

			customerService.getCookiePreferenceStatusService().saveCookiePreferenceStatus(preferenceToSave);
		}

		return customerService.getCookiePreferenceStatusService().getCookiePreferenceStatusByProfileId(profileID);
	}

	/**
	 * This method delete all browser cookies in the selected category.
	 * 
	 * @author ET58344, Evertec Inc.
	 * @since 02-22-2021
	 * @param category category selected
	 * @return boolean result of proccess
	 */
	public boolean deleteSelectedCookies(String category, HttpServletResponse response) {

		boolean result = Boolean.FALSE; // result of proccess

		switch (category) {
		case ANALYTICS_COOKIE_STR:
			for (String eachCookie : ANALYTIC_COOKIES) {
				CookieUtils.removeCookie(response, eachCookie);
			}
			result = Boolean.TRUE;
			break;
		case ADVERTISING_COOKIE_STR:
			result = Boolean.TRUE;
			break;
		case FUNCTIONAL_COOKIE_STR:
			result = Boolean.TRUE;
			break;
		default:
			break;
		}

		return result;
	}

	/**
	 * This method delete disabled cookies.
	 * 
	 * @author ET58344, Evertec Inc.
	 * @since 02-23-2021
	 * @param preferences list
	 * @param response    result
	 */
	public void deleteAllCookiePreference(List<CookiePreferenceStatus> preferences, HttpServletResponse response) {

		for (CookiePreferenceStatus eachPreference : preferences) {

			if (eachPreference.getStatus().equals(COOKIE_STATUS_DISABLED)) {
				deleteSelectedCookies(eachPreference.getCategory(), response);
			}
		}
	}

	/**
	 * Get customer service
	 * 
	 * @author ET58344, Evertec Inc.
	 * @since 02-22-2021
	 * @return CustomerServices the customerService object
	 */
	public CustomerServices getCustomerService() {
		return customerService;
	}

	/**
	 * Set customer service
	 * 
	 * @author ET58344, Evertec Inc.
	 * @since 02-22-2021
	 * @param customerService the customerService to set
	 */
	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}
}
