package evertec.cibp.web.services.modules;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.validator.GenericValidator;

import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerPayeePreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.customer.interaction.PaymentTransferAddDeleteInteraction;
import evertec.cibp.core.models.customer.interaction.PaymentTransferModifyInteraction;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.BillPayEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.GlobalPayeePreferences;
import evertec.cibp.core.models.payee.PayeeComparator;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.FavoritePaymentComparator;
import evertec.cibp.core.models.payment.PaymentHistoryTally;
import evertec.cibp.core.models.payment.TVAccountFavoritePayment;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.CustomerPaymentInfoResponse;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.DisposableIdentifier;
import evertec.cibp.core.utils.FrequencyUtils;
import evertec.cibp.core.utils.PaymentUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.beans.payments.Payee;
import evertec.cibp.vs.beans.payments.Payment;
import evertec.cibp.vs.beans.payments.PaymentModify;
import evertec.cibp.vs.beans.payments.RecurringPayment;
import evertec.cibp.vs.models.PaymentFilterType;
import evertec.cibp.vs.models.requests.CustomerPayeeListActionRequest;
import evertec.cibp.vs.models.requests.ModifyPaymentActionRequest;
import evertec.cibp.vs.models.requests.PaymentActionRequest;
import evertec.cibp.vs.models.requests.PaymentListActionRequest;
import evertec.cibp.vs.models.requests.RecurringPaymentActionRequest;
import evertec.cibp.vs.models.results.CustomerPayeeListActionResult;
import evertec.cibp.vs.models.results.ModifyPaymentActionResult;
import evertec.cibp.vs.models.results.PaymentActionResult;
import evertec.cibp.vs.models.results.PaymentListActionResult;
import evertec.cibp.vs.models.results.RecurringPaymentActionResult;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.request.ModifyPaymentRequest;
import evertec.cibp.web.session.UserSession;

/**
 * As part of the microservices architecture movement, core Business logic is being refactored and moved to the web for microservice consumption.
 * This class contains all payments service operations orchestration for microservice consumption
 * @author et55498
 *
 */
public class PaymentsServiceModule {

	private final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PaymentsServiceModule.class);
	private static final int PAYMENT_SERVICES_SUCCESS_CODE = 0;
	private static final int MODIFY_PAYMENT_INSTANCE_LOWER = 0;
	private static final int MODIFY_PAYMENT_INSTANCE_UPPER = 750;


	/** The instance. */
	private static PaymentsServiceModule instance = null;
	
	/** Instantiates a new payment facade. */
	protected PaymentsServiceModule() {}

	/**
	 * Gets the single instance of PaymentsFacade.
	 * 
	 * @return single instance of PaymentsFacade
	 */
	public static PaymentsServiceModule getInstance() {
		if ( instance == null ) {
			instance = new PaymentsServiceModule();
		}
		return instance;
	}


	public static PaymentResponse sendPayments(CustomerProfile customerProfile, UserSession session,
			TVPayment payment, CustomerInteractionEvent interactionEvent, CustomerServices customerService) {
		PaymentResponse response = new PaymentResponse();
		response.setPayment(payment);
		response.setStatus(ResultStatus.ERROR);

		if (payment == null)
			throw new IllegalArgumentException("Received Null Payment");
		if (customerProfile == null)
			throw new IllegalArgumentException("Received Null Customer Profile");

		// boolean telenomina;
		// if(payment.getPayee().getGlobalPayeeId()==5961)
		// telenomina=true;
		// else
		// telenomina=false;

		PaymentTransferAddDeleteInteraction in = new PaymentTransferAddDeleteInteraction(interactionEvent);
		if (payment.getAmount() != null)
			in.setAmount(payment.getAmount().toString());
		Calendar effDt = Calendar.getInstance();
		effDt.setTime(payment.getEffectiveDate());
		in.setEffectiveDate(effDt);
		in.setCreditAccount(payment.getPayeeBillingAccount());
		if (payment.getSource() != null) {
			in.setDebitAccount(payment.getSource().getAccount().getAccountNumber());
			in.setDebitBank(payment.getSource().getAccount().getTVAccount().getBankInformation().getBankId());
			in.setDebitType(payment.getSource().getAccount().getSubtype() + " "
					+ payment.getSource().getAccount().getAccountSection());
		} else {
			in.setDebitAccount(payment.getSourceAccountNumber());
			in.setDebitBank(payment.getSourceBankId());
			in.setDebitType(payment.getSourceAcctType() + " " + payment.getSourceAcctSectionInd());
		}
		in.setTelepagoCode(payment.getPayeeId());
		in.setFrequency(payment.getFrequency());

		if (payment.getFrequency() == null || payment.getFrequency().equals("")) {
			if (payment.getSource().getAccount().getBankId().length() == 9
					&& !payment.getSource().getAccount().getBankId().equals("021502011")) {
				payment.getTVPayment().getAccountFrom().getBankInfo().setBankIdType("ABA");
			}

			PaymentActionRequest request = new PaymentActionRequest(true,
					PaymentUtils.convertTVPaymentToVSPayment(payment), customerProfile.getEntitlementByType(
							EntitlementType.BANKING).getString("SSN"), "");
			if (payment.getPayee().isTelenomina())
				request.setTelenomina(true);
			else
				request.setTelenomina(false);
			PaymentActionResult varPayment = PaymentsFacade.getInstance().sendRegularPayments(request, customerService, session);
			if (varPayment.getErrorCode() != 0) {
				response.setPayment(payment);
				response.setErrorCode(String.valueOf(varPayment.getErrorCode()));
				if (varPayment.getErrorDescription() != null) {
					response.setErrorMessage(varPayment.getErrorDescription());
					response.setErrorEBA(varPayment.getErrorDescription().split("-")[0].trim());// CIBP-1680
					in.setReferenceNumber(varPayment.getErrorDescription());
				}
				customerService.getInteractionService().logPaymentSendInteraction(in.getCustomerInteractionEvent(),
						InteractionSeverity.WARNING);
				return response;
			} else {

				Payment tvpayment = varPayment.getPayment();
				// Payment was success
				if (tvpayment.getEffectiveDate() == null)
					tvpayment.setEffectiveDate(effDt);

				in.setReferenceNumber(payment.getReferenceNumber());
				customerService.getInteractionService().logPaymentSendInteraction(in.getCustomerInteractionEvent(), InteractionSeverity.INFO);
				response.getPayment().setBackendObject(tvpayment);
			}

			response.setStatus(ResultStatus.SUCCESS);
			return response;
		} else {
			RecurringPayment recurring = payment.getTVPayment();
			long instances = recurring.getRemainingInstances();
			String freq = recurring.getFrequency();
			recurring.setRemainingInstances(instances);
			recurring.setFrequency(freq);

			Calendar endDate = (Calendar) recurring.getProcessingDate().clone();
			if (instances == 0 || instances >= 750) {
				recurring.setFinalProcessingDate(null);
				recurring.setRemainingInstances(0);
			} else {
				if (instances > 1){
					GregorianCalendar gregoriancalendar = new GregorianCalendar(recurring.getProcessingDate().get(Calendar.YEAR),
							recurring.getProcessingDate().get(Calendar.MONTH), recurring.getProcessingDate().get(Calendar.DAY_OF_MONTH));
					endDate = FrequencyUtils.getEndDate(gregoriancalendar, (int) instances, freq);
				}
				recurring.setFinalProcessingDate(endDate);
			}

			in.setEndDate(endDate);

			RecurringPaymentActionRequest request = new RecurringPaymentActionRequest(true,
					PaymentUtils.convertTVPaymentToVSPayment(payment), customerProfile.getEntitlementByType(
							EntitlementType.BANKING).getString("SSN"), "");
			if (payment.getPayee().isTelenomina())
				request.setTelenomina(true);
			else
				request.setTelenomina(false);
			RecurringPaymentActionResult addRecurringPayment = customerService.getPaymentService().getVaultService().addRecurringPayment(request);
			if (addRecurringPayment.getErrorCode() != 0) {
				response.setPayment(payment);
				response.setErrorCode(String.valueOf(addRecurringPayment.getErrorCode()));
				if (addRecurringPayment.getErrorDescription() != null) {
					response.setErrorMessage(addRecurringPayment.getErrorDescription());
					in.setReferenceNumber(addRecurringPayment.getErrorDescription());
					response.setErrorEBA(addRecurringPayment.getErrorDescription().split("-")[0].trim());// CIBP-1680
				}
				customerService.getInteractionService().logPaymentSendInteraction(in.getCustomerInteractionEvent(),
						InteractionSeverity.WARNING);
				return response;
			} else {
				// Payment was success
				RecurringPayment tvpayment = addRecurringPayment.getRecurringPayment();
				if (tvpayment.getEffectiveDate() == null)
					tvpayment.setEffectiveDate(effDt);
				if (tvpayment.getReferenceNumber() != null)
					in.setReferenceNumber(tvpayment.getReferenceNumber());

				tvpayment.setRemainingInstances(instances);
				customerService.getInteractionService()
						.logPaymentSendInteraction(in.getCustomerInteractionEvent(), InteractionSeverity.INFO);

				response.getPayment().setBackendObject(tvpayment);
			}
			response.setStatus(ResultStatus.SUCCESS);
			return response;
		}
	}
	

	
	public CustomerPaymentInfoResponse getFastPayees(UserSession userSession, BankingSession bankingSession,
			boolean includeFavorites) {
		
		CustomerPaymentInfoResponse response = new CustomerPaymentInfoResponse();
		response.setPayrollPayments(new ArrayList<TVAccountFavoritePayment>());
		response.setVendorPayments(new ArrayList<TVAccountFavoritePayment>());
		List<TVFrontEndPayee> customertvPayees = new LinkedList<TVFrontEndPayee>();
		DisposableIdentifier id = new DisposableIdentifier();
		CustomerServices customerService = userSession.getCustomerService();
		

		if (userSession.getCustomerProfile() == null)
			throw new RuntimeException("Customer profile is null");
		if (bankingSession == null)
			throw new RuntimeException("Banking Session is null");

		BankingEntitlement bank = new BankingEntitlement(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.BANKING));
		final String customerSSN = bank.getSsn();
		CustomerPayeeListActionRequest payeeListRequest = new CustomerPayeeListActionRequest(false, bankingSession,
				customerSSN);
		CustomerPayeeListActionResult payeeListresult = PaymentsFacade.getInstance().getCustomerPayees(payeeListRequest, customerService);

		List<Payee> customerPayees = new LinkedList<Payee>();
		if (payeeListresult != null && payeeListresult.getPayees() != null)
			customerPayees = payeeListresult.getPayees();
	

		Map<String, TVFrontEndPayee> tvpayees = new LinkedHashMap<String, TVFrontEndPayee>();
		for (Iterator<Payee> iterator = customerPayees.iterator(); iterator.hasNext();) {
			Payee name = (Payee) iterator.next();

			TVFrontEndPayee tvp = new TVFrontEndPayee(name);
			String uniqueId = tvp.getPayeeKey();

			if (tvpayees.containsKey(uniqueId)) {
				tvp = tvpayees.get(uniqueId);
				String id2 = name.getPayeeId();
				if (id2.matches("\\d+")) {
					if (!tvp.getCustomerPayeeCodes().contains(Integer.parseInt(name.getPayeeId()))) {
						tvp.getCustomerPayeeCodes().add(Integer.parseInt(name.getPayeeId()));
						tvpayees.put(uniqueId, tvp);
					}
				}
			} else {
				String id2 = name.getPayeeId();
				if (id2.matches("\\d+")) {
					tvp.getCustomerPayeeCodes().add(Integer.parseInt(name.getPayeeId()));
					tvpayees.put(uniqueId, tvp);
				}
			}

		}

		Collection<TVFrontEndPayee> colPayees = tvpayees.values();
		customertvPayees.addAll(colPayees);

		Map<String, CustomerPayeePreferences> loadPayeePreferences =  customerService.getPaymentService().loadPayeePreferences(userSession.getCustomerProfile());

		for (Iterator<TVFrontEndPayee> iterator = customertvPayees.iterator(); iterator.hasNext();) {
			TVFrontEndPayee name = (TVFrontEndPayee) iterator.next();
			name.setFrontEndId(id.getNextDisposableId());

			CustomerPayeePreferences customerPayeePreferences = getCustomerPayeePreferences(loadPayeePreferences, name);
			name.setCustomerPayeePreferences(customerPayeePreferences);

			GlobalPayeePreferences pref = null;
			if (name.getGlobalPayeeId() != 0)
				pref = customerService.getPaymentService().getPayeePreferenceByGlobalId(String.valueOf(name.getGlobalPayeeId()));
			if (pref != null && pref.isSet(pref.EBILL))
				name.setAvailabletoEbill(pref.getEbill());
			else
				name.setAvailabletoEbill(false);

		}

		if (includeFavorites) {
			BillPayEntitlement ent = null;
			if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.BILLPAY))
				ent = new BillPayEntitlement(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.BILLPAY));

			if (ent != null && ent.getHasPayeePreferences()) {
				if(StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR)){
					response.setFrequentPayments(getPayeeCustomerFrequentPreferences(userSession, bankingSession,
							customertvPayees, response));
				}
				else{
					response.setFrequentPayments(getPayeeCustomerFrequentPreferences(userSession, bankingSession,
						customertvPayees));
				}
			} else {
				CustomerPaymentInfoResponse r = getPayeesAndPaymentHistory(userSession, bankingSession,
						customertvPayees, null);
				response.setFrequentPayments(r.getFrequentPayments());
				response.setLoadhistory(true);
			}
		}
				
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)){
			validateIfHasProcessedPaymentByPayees(userSession, customertvPayees);
		}
		
		// Sort la lista de payees por el nombre del payee.
		Collections.sort(customertvPayees, new PayeeComparator());
		response.setPayees(customertvPayees);
		return response;

	}
	
		/**
	 * validateIfHasProcessedPaymentByPayees method that validate if payee has processed real time payment
	 * @param customerProfile
	 * @param payees
	 * @param favorites
	 */
	private void validateIfHasProcessedPaymentByPayees (UserSession userSession, 
			List<TVFrontEndPayee> payees) {
		
		List<RecurringPayment> processedPayments = new ArrayList<RecurringPayment>(0); // payment history
		final PaymentListActionRequest processedPaymentListActionRequest = new PaymentListActionRequest(false,
				PaymentFilterType.HISTORY, 20, null, null, userSession.getCustomerProfile().getEntitlementByType(EntitlementType.BANKING)
						.getString("SSN")); // request to vault services
		
		final PaymentListActionResult processedPaymentHistory = userSession.getCustomerService().getVaultService().getPaymentHistory(processedPaymentListActionRequest); // vt response
		
		if (processedPaymentHistory.getPayments() != null) {
			processedPayments.addAll(processedPaymentHistory.getPayments());
		}
		
		final String statusCodeNotitificationPayment = "IA"; // status code notification payment
		final String statusCodeOkPayment = "OK"; // status code ok payment
		TVFrontEndPayee payee = null; // iterator item
		
		if (processedPayments != null) {
			for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
				payee = (TVFrontEndPayee) iterator.next(); // iterator item
				
				for (RecurringPayment processedPayment : processedPayments) {
					if (payee.getFrontEndId().equalsIgnoreCase(getTPID(processedPayment.getPayeeId(), payees))) {
						if (processedPayment.getStatusCode().equals(statusCodeNotitificationPayment) || processedPayment.getStatusCode().equals(statusCodeOkPayment)) {
							payee.setHasPreviousRealTimePayment(true);
							break;
						}
					}
				}
			}
		}
	}
	
	private List<TVAccountFavoritePayment> getPayeeCustomerFrequentPreferences(UserSession userSession,
			BankingSession bankingSession, List<TVFrontEndPayee> payees) {

		if (payees == null || payees.size() == 0)
			payees = getFastPayees(userSession, bankingSession, false).getPayees();

		if (payees == null)
			return null;

		DisposableIdentifier id = new DisposableIdentifier();
		List<TVAccountFavoritePayment> favorites = new java.util.LinkedList<TVAccountFavoritePayment>();
		BillPayEntitlement ent = null;
		if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.BILLPAY))
			ent = new BillPayEntitlement(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.BILLPAY));

		if (ent != null && ent.getHasPayeePreferences()) {
			for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
				TVFrontEndPayee name = (TVFrontEndPayee) iterator.next();
				if (name.getFavorite()) {
					TVAccountFavoritePayment favpayment = new TVAccountFavoritePayment(name);
					favpayment.setFrontEndId(id.getNextDisposableId());
					favorites.add(favpayment);
				}
			}
		}
		return favorites;

	}
	
	private List<TVAccountFavoritePayment> getPayeeCustomerFrequentPreferences(UserSession userSession,
			BankingSession bankingSession, List<TVFrontEndPayee> payees, CustomerPaymentInfoResponse response) {

		if (payees == null || payees.size() == 0)
			payees = getFastPayees(userSession, bankingSession, false).getPayees();

		if (payees == null)
			return null;

		DisposableIdentifier id = new DisposableIdentifier();
		List<TVAccountFavoritePayment> favorites = new java.util.LinkedList<TVAccountFavoritePayment>();
		BillPayEntitlement ent = null;
		if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.BILLPAY))
			ent = new BillPayEntitlement(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.BILLPAY));

		if (ent != null && ent.getHasPayeePreferences()) {
			for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
				TVFrontEndPayee name = (TVFrontEndPayee) iterator.next();
				
				if(!String.valueOf(name.getGlobalPayeeId()).equalsIgnoreCase(userSession.getEpayrollId()) && 
						!String.valueOf(name.getGlobalPayeeId()).equalsIgnoreCase(userSession.getEvendorId())){
					if (name.getFavorite()) {
						TVAccountFavoritePayment favpayment = new TVAccountFavoritePayment(name);
						favpayment.setFrontEndId(id.getNextDisposableId());
						favorites.add(favpayment);
					}
				}
				else if(String.valueOf(name.getGlobalPayeeId()).equalsIgnoreCase(userSession.getEpayrollId())){
					TVAccountFavoritePayment favpayment = new TVAccountFavoritePayment(name);
					favpayment.setFrontEndId(id.getNextDisposableId());
					response.getPayrollPayments().add(favpayment);
				}
				else if(String.valueOf(name.getGlobalPayeeId()).equalsIgnoreCase(userSession.getEvendorId())){
					TVAccountFavoritePayment favpayment = new TVAccountFavoritePayment(name);
					favpayment.setFrontEndId(id.getNextDisposableId());
					response.getVendorPayments().add(favpayment);
				}
			}
		}
		return favorites;

	}
	
	public CustomerPaymentInfoResponse getPayeesAndPaymentHistory(UserSession userSession,
			BankingSession bankingSession, List<TVFrontEndPayee> payees, List<TVAccountFavoritePayment> favorites) {

		CustomerPaymentInfoResponse response = new CustomerPaymentInfoResponse();
		CustomerServices customerService = userSession.getCustomerService();
		
		BillPayEntitlement ent = null;
		if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.BILLPAY)) {
			ent = new BillPayEntitlement(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.BILLPAY));
		} else
			throw new ProgrammingError("No BILLPAY Entitlement Found");

		if (payees == null || payees.size() == 0)
			payees = getFastPayees(userSession, bankingSession, false).getPayees();

		response.setPayees(payees);

		// SimpleDateFormat datef = new SimpleDateFormat("yyyyMMdd");
		Calendar now = Calendar.getInstance();
		now.set(Calendar.DAY_OF_MONTH, 1);

		PaymentListActionRequest paymentListActionRequest = new PaymentListActionRequest(false,
				PaymentFilterType.HISTORY, 20, null, now, userSession.getCustomerProfile().getEntitlementByType(EntitlementType.BANKING)
						.getString("SSN"));
		PaymentHistoryTally tally = new PaymentHistoryTally();
		List<RecurringPayment> payments = new ArrayList<RecurringPayment>();
		List<RecurringPayment> pendingpayments = new ArrayList<RecurringPayment>();

		PaymentListActionResult paymentHistory = customerService.getPaymentService().getVaultService().getPaymentHistory(paymentListActionRequest);
		if (paymentHistory.getPayments() != null) {
			payments.addAll(paymentHistory.getPayments());
		}

		now.add(Calendar.MONTH, -1);
		paymentListActionRequest.setStartDate(now);
		paymentHistory = customerService.getPaymentService().getVaultService().getPaymentHistory(paymentListActionRequest);
		if (paymentHistory.getPayments() != null)
			payments.addAll(paymentHistory.getPayments());

		if (payments != null && payments.size() > 0)
			for (RecurringPayment recurringPayment : payments) {
				tally.addPayment(recurringPayment, getTPID(recurringPayment.getPayeeId(), payees));
			}

		paymentListActionRequest = new PaymentListActionRequest(false, PaymentFilterType.PENDING, 20, null, null,
				userSession.getCustomerProfile().getEntitlementByType(EntitlementType.BANKING).getString("SSN"));
		paymentHistory = customerService.getPaymentService().getVaultService().getPaymentHistory(paymentListActionRequest);
		pendingpayments = paymentHistory.getPayments();

		if (favorites == null || favorites.size() == 0) {
			favorites = new LinkedList<TVAccountFavoritePayment>();
			if (ent != null && ent.getHasPayeePreferences()) {
				favorites = getPayeeCustomerFrequentPreferences(userSession, bankingSession, payees);
				Set<List<RecurringPayment>> countBiggerThanX = tally.getCountBiggerThanX(0);
				for (Iterator<TVAccountFavoritePayment> iterator = favorites.iterator(); iterator.hasNext();) {
					TVAccountFavoritePayment name = (TVAccountFavoritePayment) iterator.next();
					for (List<RecurringPayment> list : countBiggerThanX) {
						for (Iterator<RecurringPayment> iterator2 = list.iterator(); iterator2.hasNext();) {
							RecurringPayment payment = (RecurringPayment) iterator2.next();
							if (name.getFrontEndId().equalsIgnoreCase(getTPID(payment.getPayeeId(), payees))) {
								if (!payment.getStatusCode().equals("OK"))
									continue;
								if (name.getLastPayment() == null)
									name.setLastPayment(new TVPayment(payment));
								break;
							} else
								break;
						}
					}

				}
			} else {
				List<String> tpids = new LinkedList<String>();
				DisposableIdentifier id = new DisposableIdentifier();
				Set<List<RecurringPayment>> countBiggerThanX = tally.getCountBiggerThanX(0);
				boolean found = false;
				for (List<RecurringPayment> list : countBiggerThanX) {
					for (Iterator<RecurringPayment> iterator2 = list.iterator(); iterator2.hasNext();) {
						RecurringPayment payment = (RecurringPayment) iterator2.next();

						if (!payment.getStatusCode().equals("OK"))
							continue;

						for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
							TVFrontEndPayee name = (TVFrontEndPayee) iterator.next();
							if (name.getFrontEndId().equalsIgnoreCase(getTPID(payment.getPayeeId(), payees))) {
								if (tpids.contains(name.getFrontEndId()))
									continue;

								tpids.add(name.getFrontEndId());
								TVAccountFavoritePayment favpayment = new TVAccountFavoritePayment(name);
								favpayment.setFrontEndId(id.getNextDisposableId());
								favpayment.setLastPayment(new TVPayment(payment));
								favorites.add(favpayment);
								name.getCustomerPayeePreferences().setFavorite(true);
								modifyPayeeFrequentPreference(userSession, bankingSession, name, true);
								found = true;
								break;
							}
						}
						break;
					}

				}
				if (!found) {
					// is the first time that customer get to payment service
					// and payee frequents are set automatically
					ent.setHasPayeePreferences(true);
					List<CustomerEntitlement> request = new ArrayList<>();
					request.add(ent.getCustomerEntitlement());
					CustomerFacade.getInstance().updateEntitlement(userSession, request);
				}

			}
		} else {
			Set<List<RecurringPayment>> countBiggerThanX = tally.getCountBiggerThanX(0);
			for (Iterator<TVAccountFavoritePayment> iterator = favorites.iterator(); iterator.hasNext();) {
				TVAccountFavoritePayment name = (TVAccountFavoritePayment) iterator.next();
				for (List<RecurringPayment> list : countBiggerThanX) {
					for (Iterator<RecurringPayment> iterator2 = list.iterator(); iterator2.hasNext();) {
						RecurringPayment payment = (RecurringPayment) iterator2.next();
						if (name.getPayee().getFrontEndId().equalsIgnoreCase(getTPID(payment.getPayeeId(), payees))) {
							if (!payment.getStatusCode().equals("OK"))
								continue;
							if (name.getLastPayment() == null)
								name.setLastPayment(new TVPayment(payment));
							break;
						} else
							break;
					}
				}

			}
		}

		// Add at favoriteList the list of pending payments to each payee

		for (Iterator<TVAccountFavoritePayment> iterator = favorites.iterator(); iterator.hasNext();) {
			TVAccountFavoritePayment name = (TVAccountFavoritePayment) iterator.next();
			name.setPendingPayments(new LinkedList<TVPayment>());
		}

		if (pendingpayments != null) {
			for (RecurringPayment recurringPayment : pendingpayments) {
				for (Iterator<TVAccountFavoritePayment> iterator = favorites.iterator(); iterator.hasNext();) {
					TVAccountFavoritePayment name = (TVAccountFavoritePayment) iterator.next();
					if (name.getPayee().getFrontEndId()
							.equalsIgnoreCase(getTPID(recurringPayment.getPayeeId(), payees))) {
						TVPayment ipaym = new TVPayment(recurringPayment);
						if (recurringPayment.getAccountFrom().getAccountType() != null
								&& recurringPayment.getAccountFrom().getAccountType().equals("SAV")) {
							ipaym.setSource(new TVAccountTransferSource(new TVFrontendAccount("IDA", recurringPayment
									.getAccountFrom().getAccountNumber(), "S")));
						} else {
							ipaym.setSource(new TVAccountTransferSource(new TVFrontendAccount(recurringPayment
									.getAccountFrom().getAccountType(), recurringPayment.getAccountFrom()
									.getAccountNumber(), recurringPayment.getAccountFrom().getSectionInd())));
						}
						if (name.getPendingPayments() == null) {
							name.setPendingPayments(new LinkedList<TVPayment>());
							name.getPendingPayments().add(ipaym);
						} else {
							name.getPendingPayments().add(ipaym);
						}
						break;
					}
				}
			}
		}

		Collections.sort(favorites, new FavoritePaymentComparator());
		response.setFrequentPayments(favorites);
		response.setPayees(payees);
		response.setLoadhistory(true);
		return response;
	}
	
	private CustomerPayeePreferences getCustomerPayeePreferences(
			Map<String, CustomerPayeePreferences> loadPayeePreferences, TVFrontEndPayee name) {
		CustomerPayeePreferences customerPayeePreferences = loadPayeePreferences.get(name.getPayeeKey());

		if (customerPayeePreferences == null) {

			customerPayeePreferences = new CustomerPayeePreferences();
			customerPayeePreferences.setKey(name.getPayeeKey());
			customerPayeePreferences.setFavorite(false);
			customerPayeePreferences.setNickname(name.getPayeeName());

		}
		return customerPayeePreferences;
	}
	
	private String getTPID(String p, List<TVFrontEndPayee> payees) {
		for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
			TVFrontEndPayee name = (TVFrontEndPayee) iterator.next();
			if (name.getCustomerPayeeCodes() != null && name.getCustomerPayeeCodes().contains(Integer.parseInt(p))) {
				String id = name.getFrontEndId();
				return id;
			}
		}
		return null;
	}
	
	public void modifyPayeeFrequentPreference(UserSession userSession, BankingSession bankingSession,
			TVFrontEndPayee payee, boolean isFrequent) {

		if (userSession.getCustomerProfile() == null)
			throw new ProgrammingError("Customer Profile must be defined.");

		if (bankingSession == null || bankingSession.getSessionKey() == null || bankingSession.getPermId() == null) {
			throw new ProgrammingError(
					"No authentication data for customer received. (Entitlement and BankingSession not exist)");
		}

		if (payee == null || !(payee instanceof TVFrontEndPayee)) {
			throw new ProgrammingError("Payee must be defined");
		}

		if (payee.getCustomerPayeePreferences() == null) {
			CustomerPayeePreferences pref = new CustomerPayeePreferences();
			payee.setCustomerPayeePreferences(pref);
		}

		payee.getCustomerPayeePreferences().setFavorite(isFrequent);
		userSession.getCustomerService().getPaymentService().updatePayeePreferences(payee, userSession.getCustomerProfile());

		if (userSession.getCustomerProfile().hasEntitlement(EntitlementType.BILLPAY)) {
			BillPayEntitlement ent = new BillPayEntitlement(
					userSession.getCustomerProfile().getEntitlementByType(EntitlementType.BILLPAY));
			if (!ent.getHasPayeePreferences()) {
				ent.setHasPayeePreferences(true);
				List<CustomerEntitlement> request = new ArrayList<>();
				request.add(ent.getCustomerEntitlement());
				CustomerFacade.getInstance().updateEntitlement(userSession, request);
			}
		}

	}

	// --------------------------------------------------------------------------------------------------------
	// ------------------------------- REFACTORED CODE BELOW --------------------------------------------------
	// --------------------------------------------------------------------------------------------------------
	
	
	/**
	 * Method in charge of modifying a regular or recurring payment
	 * @param modifyPaymentRequest
	 * @return paymentResponse
	 */
	public PaymentResponse modifyPayment(ModifyPaymentRequest modifyPaymentRequest) {

		logger.debug("Entered modifyPayment method");

		PaymentResponse response = new PaymentResponse();
		response.setPayment(modifyPaymentRequest.getPayment());
		response.setStatus(ResultStatus.ERROR);

		if (modifyPaymentRequest.getPayment() == null) {
			logger.error("The payment object is null");
			return response;
		}

		evertec.cibp.vs.beans.payments.RecurringPayment recurring = PaymentUtils.convertTVPaymentToVSPayment(modifyPaymentRequest.getPayment());

		PaymentModify pmtMod = modifyPaymentRequest.getPaymentModify();
		boolean isRecurringPayment = !GenericValidator.isBlankOrNull(pmtMod.getNewFrequency());
		if(isRecurringPayment) {
			recurring = recurringPaymentInit(pmtMod, recurring, modifyPaymentRequest.getInstances());
		}

		pmtMod.setRecurringPmt(recurring);
		ModifyPaymentActionRequest request = new ModifyPaymentActionRequest(true, pmtMod, modifyPaymentRequest.getTaxId(), null);
		request.setTelenomina(modifyPaymentRequest.getPayment().getPayee().isTelenomina());
		ModifyPaymentActionResult result = null;

	
		result = PaymentsFacade.getInstance().modifyRegularPayment(request, modifyPaymentRequest.getCustomerService());
		
		
		response = createResponseObjectFromResult(result, modifyPaymentRequest, recurring);
		sendAuditLogInteraction(modifyPaymentRequest, recurring, response);
		return response;
	}
	
	public PaymentResponse reversePayment(CustomerProfile customerProfile,
			BankingSession bankingSession, TVPayment payment, CustomerInteractionEvent interactionEvent, CustomerServices customerService) {
		if (payment == null)
			throw new IllegalArgumentException("Received Null Payment");
		if (customerProfile == null)
			throw new IllegalArgumentException("Received Null Customer Profile");
		if (bankingSession == null)
			throw new IllegalArgumentException("Received Null Banking Session");

		PaymentResponse res = new PaymentResponse();
		res.setPayment(payment);
		res.setPaymentReversal(true);
	
		PaymentTransferAddDeleteInteraction auditLogContent = new PaymentTransferAddDeleteInteraction(interactionEvent, payment, 1);
	
		PaymentActionRequest request = new PaymentActionRequest(true, payment.getTVPayment(), customerProfile
				.getEntitlementByType(EntitlementType.BANKING).getString("SSN"),
				customerProfile.getEnrollmentAccountNumber(), true);

		request.setTelenomina(payment.getPayee().isTelenomina());
		PaymentActionResult result = PaymentsFacade.getInstance().reversePayment(request, customerService);
		
		if(result.getErrorCode() != PAYMENT_SERVICES_SUCCESS_CODE) {
			res.setErrorCode(result.getErrorCode());
			res.setErrorString(result.getErrorDescription());
			res.setStatus(ResultStatus.ERROR);
			if (result.getErrorDescription() != null && result.getErrorDescription().indexOf("-") > -1)
				res.setErrorEBA(result.getErrorDescription().split("-")[0].toString());
			
		} else {
			res.setErrorCode(result.getErrorCode());
			res.setErrorString("");
			res.setStatus(ResultStatus.SUCCESS);
		}
		
		customerService.getInteractionService().logPaymentCancelInteraction(auditLogContent.getCustomerInteractionEvent(),
					res.getStatus()== ResultStatus.ERROR?InteractionSeverity.WARNING :InteractionSeverity.INFO);
		return res;
	}
	

	private PaymentResponse createResponseObjectFromResult(ModifyPaymentActionResult result, ModifyPaymentRequest modifyPaymentRequest, RecurringPayment recurring) {
		PaymentResponse response = new PaymentResponse();
		
		if (PAYMENT_SERVICES_SUCCESS_CODE == result.getErrorCode()) {

			logger.debug("The call to the modifyPayment was successful");
			response = createSuccessResponseObjectFromResult(result, modifyPaymentRequest, recurring);
			
		} else {
			response.setPayment(modifyPaymentRequest.getPayment());
			response.setErrorCode(String.valueOf(result.getErrorCode()));
			response.setErrorMessage(result.getErrorDescription());
			response.setStatus(ResultStatus.ERROR);
			if (result.getErrorDescription() != null) {
			
				response.setReferenceNumber(result.getErrorDescription());
				response.setErrorEBA(result.getErrorDescription().split("-")[0].trim());
			}
			logger.error("The call to the modifyPayment returned error: "+ response.getErrorMessage());

		}

		return response;

	}
	
	private PaymentResponse createSuccessResponseObjectFromResult (ModifyPaymentActionResult result, ModifyPaymentRequest modifyPaymentRequest, RecurringPayment recurring) {
		PaymentResponse response = new PaymentResponse();
		response.setPayment(modifyPaymentRequest.getPayment());
		
		boolean isRecurringPayment = !GenericValidator.isBlankOrNull(modifyPaymentRequest.getPaymentModify().getNewFrequency());
		
		response.getPayment().setBackendObject(result.getRecurrPmt());
		response.getPayment().getTVPayment().setRemainingInstances(modifyPaymentRequest.getInstances());
		response.setStatus(ResultStatus.SUCCESS);
		response.getPayment().getTVPayment().setAmount(modifyPaymentRequest.getPaymentModify().getNewAmount());
		response.getPayment().getTVPayment().setEffectiveDate(modifyPaymentRequest.getPaymentModify().getNewEffectiveDate());

		if (recurring.getReferenceNumber() != null && !isRecurringPayment) {
			response.setReferenceNumber(recurring.getReferenceNumber());
		}

		if(isRecurringPayment) {
			String frequencyCode = modifyPaymentRequest.getUserSession().getPaymentFrequencyCode(modifyPaymentRequest.getPaymentModify().getNewFrequency());
			if(!GenericValidator.isBlankOrNull(frequencyCode)) {
				response.getPayment().getTVPayment().setFrequency(frequencyCode);
			}
		}
		return response;
	}

	private PaymentTransferModifyInteraction sendAuditLogInteraction(ModifyPaymentRequest modifyPaymentRequest, RecurringPayment recurring, PaymentResponse response) 
	{
		PaymentTransferModifyInteraction auditLogInteraction = new PaymentTransferModifyInteraction(modifyPaymentRequest.getInteractionEvent());

		if (modifyPaymentRequest.getPayment().getAmount() != null) {
			auditLogInteraction.setAmount(modifyPaymentRequest.getInitialAmount().toString());
		}

		Calendar effectiveDate = Calendar.getInstance();
		effectiveDate.setTime(modifyPaymentRequest.getPayment().getEffectiveDate());
		auditLogInteraction.setEffectiveDate(effectiveDate);

		auditLogInteraction.setCreditAccount(modifyPaymentRequest.getPayment().getPayeeBillingAccount());
		if (modifyPaymentRequest.getPayment().getSource() != null) {
			auditLogInteraction.setDebitAccount(modifyPaymentRequest.getPayment().getSource().getAccount().getAccountNumber());
			auditLogInteraction.setDebitBank(modifyPaymentRequest.getPayment().getSource().getAccount().getTVAccount().getBankInformation().getBankId());
			auditLogInteraction.setDebitType(modifyPaymentRequest.getPayment().getSource().getAccount().getSubtype() + " "
					+ modifyPaymentRequest.getPayment().getSource().getAccount().getAccountSection());
		} else {
			auditLogInteraction.setDebitAccount(modifyPaymentRequest.getPayment().getSourceAccountNumber());
			auditLogInteraction.setDebitBank(modifyPaymentRequest.getPayment().getSourceBankId());
			auditLogInteraction.setDebitType(modifyPaymentRequest.getPayment().getSourceAcctType() + " " + modifyPaymentRequest.getPayment().getSourceAcctSectionInd());
		}

		auditLogInteraction.setTelepagoCode(modifyPaymentRequest.getPayment().getPayeeId());
		auditLogInteraction.setFrequency(modifyPaymentRequest.getPayment().getFrequency());
		auditLogInteraction.setNewAmount(modifyPaymentRequest.getPaymentModify().getNewAmount().toString());
		auditLogInteraction.setNewEffectiveDate(modifyPaymentRequest.getPaymentModify().getNewEffectiveDate());
		auditLogInteraction.setNewFrequency(modifyPaymentRequest.getPaymentModify().getNewFrequency());
		
		if(!GenericValidator.isBlankOrNull(modifyPaymentRequest.getPaymentModify().getNewFrequency())) { // if not recurring
			auditLogInteraction.setNewEndDate(recurring.getFinalProcessingDate());
		}
		
		if(!GenericValidator.isBlankOrNull(response.getReferenceNumber())) {
			auditLogInteraction.setReferenceNumber(response.getReferenceNumber());
		}
		
		InteractionSeverity interactionSeverity = InteractionSeverity.INFO;
		if(response.getStatus() == ResultStatus.ERROR) {
			interactionSeverity = InteractionSeverity.WARNING;
		}
		
		modifyPaymentRequest.getCustomerService().getInteractionService().logPaymentModifyInteraction(auditLogInteraction.getCustomerInteractionEvent(),
				interactionSeverity);
		
		return auditLogInteraction;
	}


	private RecurringPayment recurringPaymentInit(PaymentModify paymentModify, RecurringPayment recurring, int instances) {
		Calendar startDate = (Calendar) paymentModify.getNewEffectiveDate().clone();

		recurring.setRemainingInstances(instances);
		recurring.setEffectiveDate(startDate);

		Calendar endDate = (Calendar) startDate.clone();
		if ( instances == MODIFY_PAYMENT_INSTANCE_LOWER || instances >= MODIFY_PAYMENT_INSTANCE_UPPER) {
			recurring.setFinalProcessingDate(null);
			recurring.setRemainingInstances(0);
		} else {
			if (instances > MODIFY_PAYMENT_INSTANCE_LOWER){
				GregorianCalendar gregoriancalendar = new GregorianCalendar(startDate.get(Calendar.YEAR),
						startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
				endDate = FrequencyUtils.getEndDate(gregoriancalendar, (int) instances, paymentModify.getNewFrequency());
			}
			recurring.setFinalProcessingDate(endDate);
		}

		return recurring;
	}
}
