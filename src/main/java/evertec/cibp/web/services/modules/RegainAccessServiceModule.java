/**
 *  RegainAccessServiceModule
 *  Project: CIBP
 *  Company: Evertec inc
 */
package evertec.cibp.web.services.modules;

import evertec.cibp.core.enums.RegainEmailStatus;
import evertec.cibp.core.enums.RegainTokenStatus;
import evertec.cibp.core.models.RegainAccessEmail;
import evertec.cibp.core.models.RegainAccessToken;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.Utils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.servlet.http.HttpServletRequest;

import static evertec.cibp.core.utils.StaticMessageSource.isFlagEnabled;

/**
 * Regain Access Utils.
 *
 * @author Hector Martinez (s683215)
 * @since 2020-04-21
 * @see evertec.cibp.web.form.RegainAccessForm
 * @version 1.0
 */
public class RegainAccessServiceModule {

    /**
     * Logger instance.
     */
    protected final Log log = LogFactory.getLog(getClass()); //

    /**
     * Profile found str success.
     */
    private static final String EVENT_LOG_FAIL = "Fail to log Regain Access Customer Event."; //

    /**
     * Time transformation factor.
     */
    private static final long ONE_MINUTE_IN_MILLIS = 60000; //
    
    /**
     * Token expiration minutes.
     */
    private String strTokenExpirationMinutes; //

    /**
     * Email expiration minutes.
     */
    private String strEmailExpirationMinutes; //

    /**
     * CustomerService instance.
     */
    private CustomerServices customerServices; //
    
    /**
     * RegainAccessServiceModule
     */
    public RegainAccessServiceModule () {
    	this.strTokenExpirationMinutes = StringUtils.EMPTY;
    	this.strEmailExpirationMinutes = StringUtils.EMPTY;
    	this.customerServices = null;
    }
    
    /**
     * RegainAccessServiceModule
     * @param strTokenExpirationMinutes
     * @param strEmailExpirationMinutes
     * @param customerServices
     */
    public RegainAccessServiceModule (String strTokenExpirationMinutes, String strEmailExpirationMinutes,
    		CustomerServices customerServices) {
    	this();
    	this.strTokenExpirationMinutes = strTokenExpirationMinutes;
    	this.strEmailExpirationMinutes = strEmailExpirationMinutes;
    	this.customerServices = customerServices;
    }

    /**
     * Manages token expiration.
     *
     * @param accessToken
     * @return
     */
    public boolean isTokenExpired (RegainAccessToken accessToken) {
        boolean isExpired = false; //
        final long creationDate = accessToken.getCreationDate().getTime(); //
        final long timeAfterExpMinutes = creationDate + (Integer.parseInt(strTokenExpirationMinutes) * ONE_MINUTE_IN_MILLIS); //
        
        final Instant instant = Instant.ofEpochMilli(timeAfterExpMinutes); // aux create local date time
        final LocalDateTime nuevaDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()); // finish token date
        final LocalDateTime currentDateTime = LocalDateTime.now(); //

        if (nuevaDate.isBefore(currentDateTime)) {
            isExpired = true;
            if (!accessToken.getStatus().equals(RegainTokenStatus.EXPIRED)) {
                accessToken.setStatus(RegainTokenStatus.EXPIRED);
                getCustomerServices().updateRegainAccessToken(accessToken);
            }
        }
        return isExpired;
    }

    /**
     * Return whether the email is block by Regain Access or not.
     *
     * @param strEmail String email.
     * @return true if email is blocked.
     */
    public boolean isEmailBlocked (String strEmail) {
        RegainAccessEmail accessEmail = getCustomerServices().getRegainAccessEmail(strEmail); //

        boolean block = accessEmail.getStatus() != null && accessEmail.getStatus().equals(RegainEmailStatus.BLOCKED); //

        if (block) {
            final long creationDate = accessEmail.getBlockedDate().getTime(); //
            final long expirationTime = (Integer.parseInt(strEmailExpirationMinutes) * ONE_MINUTE_IN_MILLIS);//
            final Instant instant = Instant.ofEpochMilli(creationDate + expirationTime); // aux create local date time
            final LocalDateTime blockFinishDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()); // finish token date
            final LocalDateTime currentDate = LocalDateTime.now(); //

            if (blockFinishDate.isBefore(currentDate)) {
                accessEmail.setStatus(RegainEmailStatus.UNBLOCKED);
                getCustomerServices().updateRegainAccessEmail(accessEmail);
                block = false;
            }
        }

        return block;
    }

    /**
     * @param request
     * @param regainAccessToken
     * @param customerProfile
     * @param eventType
     */
    public void logRegainAccessEvent (HttpServletRequest request, RegainAccessToken regainAccessToken,
            CustomerProfile customerProfile, CustomerInteractionEventType eventType) {
        if (!isFlagEnabled(StaticMessageSource.MBFC224)) {
        	return;
        }
            
        final String strUserAgent = request.getHeader("user-agent"); //
        final UserSession uSession = new UserSession(getCustomerServices(),null,null); //
        final String strTrace = Utils.generateRaEventTraceInformation(strUserAgent, regainAccessToken.getToken(), uSession); //
        final CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request); //
        
        event.setCustomStringData(DeviceUtils.CONNECTION_INFO, strTrace);

        if (customerProfile != null) {
            event.setProfileId(customerProfile.getProfileId());
            event.setUsername(customerProfile.getUsername());
        }

		saveEvent(eventType, event, regainAccessToken);
    }
    
    /**
     * @param eventType
     * @param event
     * @param regainAccessToken
     */
    private void saveEvent (CustomerInteractionEventType eventType, CustomerInteractionEvent event
    		, RegainAccessToken regainAccessToken) {
    	
        switch (eventType) {
        case RA_EMAIL_TOKEN_REQUESTED:
        	getCustomerServices().customerEmailTokenSentSuccess(event, regainAccessToken.getEmail());
            break;
        case RA_EMAIL_TOKEN_EXPIRED:
        	getCustomerServices().customerTokenExpiredInteraction(event, regainAccessToken.getEmail());
            break;
        case REGAIN_ACCESS_EMAIL_BLOCK:
        	getCustomerServices().customersRegainAccessEmailBlockedInteraction(event, regainAccessToken.getEmail());
            break;
        case REGAIN_ACCESS_PERSONAL_SUCCESS:
        	getCustomerServices().customerMatchInformationEvent(event, true, null);
            break;
        case REGAIN_ACCESS_PERSONAL_FAIL:
        	getCustomerServices().customerMatchInformationEvent(event, false, regainAccessToken.getEmail());
            break;
        case REGAIN_ACCESS_ACCT_SUCCESS:
        	getCustomerServices().customerMatchAccountEvent(event, true);
            break;
        case REGAIN_ACCESS_ACCT_FAIL:
        	getCustomerServices().customerMatchAccountEvent(event, false);
            break;
        default:
            log.debug(EVENT_LOG_FAIL);
            break;
        }
    }

    /**
     * @return the email expiration date.
     */
    public String getStrEmailExpirationMinutes () {
        return strEmailExpirationMinutes;
    }

    /**
     * @param strEmailExpirationMinutes the text property to be set.
     */
    public void setStrEmailExpirationMinutes (String strEmailExpirationMinutes) {
        this.strEmailExpirationMinutes = strEmailExpirationMinutes;
    }

    /**
     * @return CustomerServices.
     */
    public CustomerServices getCustomerServices () {
        return customerServices;
    }

    /**
     * @param customerServices  the CustomerServices be set.
     */
    public void setCustomerServices (CustomerServices customerServices) {
        this.customerServices = customerServices;
    }

    /**
     * @return the tokenExpirationMinutes
     */
    public String getStrTokenExpirationMinutes () {
        return strTokenExpirationMinutes;
    }

    /**
     * @param strTokenExpirationMinutes the tokenExpirationMinutes to set
     */
    public void setStrTokenExpirationMinutes (String strTokenExpirationMinutes) {
        this.strTokenExpirationMinutes = strTokenExpirationMinutes;
    }

}
