/**
 *
 */
package evertec.cibp.web.constants;

/**
 * @author armandojimenez
 *
 * Class used for common Request Parameters
 *
 */
public class RequestParams {

    /**constant Params for Push Controllers **/
    public static final String AMOUNT = "amount";

    /**constant ID **/
    public static final String ID = "id";

    /**constant PLASTIC_ID **/
    public static final String PLASTIC_ID = "plasticId";

    /**constant OWNER_ID **/
    public static final String OWNER_ID = "ownerId";

    /**constant IS_PUSH_ACTIVE **/
    public static final String IS_PUSH_ACTIVE = "isPushActive";

    /**constant IS_EMAIL_ACTIVE **/
    public static final String IS_EMAIL_ACTIVE = "isEmailActive";

    /**constant DECISION **/
    public static final String DECISION = "decision";

    /**constant UPDATE_EMAIL **/
    public static final String UPDATE_EMAIL = "updateEmail";

    /**constant  ALERT_TYPE **/
    public static final String ALERT_TYPE = "alertType";

    /**constant DEVICE_MODEL **/
    public static final String DEVICE_MODEL = "deviceModel";

    /**constant DEVICE_TYPE **/
    public static final String DEVICE_TYPE = "deviceType";

    /**constant DEVICE_NAME **/
    public static final String DEVICE_NAME = "deviceName";

    /**constant HAS_PERMISSIONS **/
    public static final String HAS_PERMISSIONS = "hasPermissions";

    /**constant IS_ACTIVE **/
    public static final String IS_ACTIVE = "isActive";

    /**constant PUSH_TOKEN **/
    public static final String PUSH_TOKEN = "pushToken";

    /**constant INVALID_PUSH_TOKEN **/
    public static final String INVALID_PUSH_TOKEN = "invalidPushToken";

    /**constant INDEX **/
    public static final String INDEX = "index";

    private static final String UTILITY_ERROR = "Utility class";

    /** Default private Constructor. Throws exception when initialized*/
    private RequestParams() {
        throw new IllegalStateException(UTILITY_ERROR);
    }
}