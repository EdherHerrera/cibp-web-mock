package evertec.cibp.web.constants;

/**
 * Interface with name attributes database for Easy Deposit
 * @author Evertec CIBP
 *
 */
public interface RemoteDepositConstants {
	
	public static final String COMMERCIAL_DAILY_AMOUNT = "COMMERCIAL_DAILY_LIMIT_AMOUNT"; // Commercial daily Amount
	public static final String COMMERCIAL_DAILY_ITEM_COUNT = "COMMERCIAL_DAILY_LIMIT_ITEM_COUNT"; // Commercial daily item count
	public static final String COMMERCIAL_MULTIDAY_AMOUNT = "COMMERCIAL_MULTIDAY_LIMIT_AMOUNT"; // Commercial multiday amount
	public static final String COMMERCIAL_MULTIDAY_ITEM_COUNT = "COMMERCIAL_MULTIDAY_LIMIT_ITEM_COUNT"; // Commercial multiday item count
	
	public static final String REGULAR_DAILY_AMOUNT = "REGULAR_DAILY_LIMIT_AMOUNT"; // Retail daily Amount 
	public static final String REGULAR_DAILY_ITEM_COUNT = "REGULAR_DAILY_LIMIT_ITEM_COUNT"; // Retail daily item count
	public static final String REGULAR_MULTIDAY_AMOUNT = "REGULAR_MULTIDAY_LIMIT_AMOUNT"; // Retail multiday amount
	public static final String REGULAR_MULTIDAY_ITEM_COUNT = "REGULAR_MULTIDAY_LIMIT_ITEM_COUNT"; // Retail multiday item count
	
	public static final String WEALTH_DAILY_AMOUNT = "WEALTH_DAILY_LIMIT_AMOUNT"; // Wealth daily Amount
	public static final String WEALTH_DAILY_ITEM_COUNT = "WEALTH_DAILY_LIMIT_ITEM_COUNT"; // Wealth daily item count
	public static final String WEALTH_MULTIDAY_AMOUNT = "WEALTH_MULTIDAY_LIMIT_AMOUNT"; // Wealth multiday amount
	public static final String WEALTH_MULTIDAY_ITEM_COUNT = "WEALTH_MULTIDAY_LIMIT_ITEM_COUNT"; // Wealth multiday item count
	
	public static final String PREMIUM_BANKING_DAILY_AMOUNT = "PREMIUM_DAILY_LIMIT_AMOUNT"; // Premium Banking daily Amount
	public static final String PREMIUM_BANKING_DAILY_ITEM_COUNT = "PREMIUM_DAILY_LIMIT_ITEM_COUNT"; // Premium Banking daily item count
	public static final String PREMIUM_BANKING_MULTIDAY_AMOUNT = "PREMIUM_MULTIDAY_LIMIT_AMOUNT"; // Premium Banking multiday amount
	public static final String PREMIUM_BANKING_MULTIDAY_ITEM_COUNT = "PREMIUM_MULTIDAY_LIMIT_ITEM_COUNT"; // Premium Banking multiday item count

}
