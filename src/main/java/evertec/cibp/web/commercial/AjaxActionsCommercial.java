/**
 * Author: Evertec
 * Company: Evertec
 */
package evertec.cibp.web.commercial;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.google.gson.Gson;

import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * 
 * @author Evertec
 * @see 1.0
 * @version 1.0.0
 * @since ajax action for commercial
 */
public class AjaxActionsCommercial {
	
	/**
	 * singleton instance
	 */
	static AjaxActionsCommercial instance; // singleton instance
	
	/**
	 * getInstance
	 * @return AjacActionsCommercial
	 */
	public static AjaxActionsCommercial getInstance () {
		if (instance == null) {
			instance = new AjaxActionsCommercial();
		}
		return instance;
	}
	
	/**
	 * constructor
	 */
	private AjaxActionsCommercial() {
		instance = null;
	}

	/**
	 * verifyRTPaymentAndRTNotification validate real time of Payee
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws Exception
	 */
	public static ModelAndView verifyRTPaymentAndRTNotification (HttpServletRequest request) {
		
		// Call from portal ebppinbox, with database information
		Map<String, String> model = new HashMap<String, String>(0); // model map to view
		Map<String,String> data = new HashMap<>(0); // data to model view
		final UserSession userSession = KiuwanUtils.cast(UserSession.class, WebUtils.getSessionAttribute(request, "userSession")); // user session
		final char notRealTimePayee = 'N'; // value for not real time payee
		final char realTimePayee = 'Y';// value for real time payee
		char isRTPayment = notRealTimePayee; // not real time payee for default
		char isRTNotification = notRealTimePayee; // not real time payee for default
		char containsPayments = notRealTimePayee; // not history payments for default

		final TVFrontEndPayee payee = Utils.getPayeebyId(request.getParameter("id"), userSession.getPayees(false)); // payee for detail

		if (payee != null && payee.getGlobalPayee() != null) {
			final GlobalPayee globalPayee = payee.getGlobalPayee(); // globar payee information
			if (globalPayee.getRtIndicator() != null 
					&& KiuwanUtils.compareStrChr(globalPayee.getRtIndicator(), realTimePayee)) {
				isRTPayment = realTimePayee;
			}
			if (globalPayee.getRtNotification() != null 
					&& KiuwanUtils.compareStrChr(payee.getGlobalPayee().getRtNotification(), realTimePayee)) {
				isRTNotification = realTimePayee;
				containsPayments = Utils.containsPayments(payee.getFrontEndId(), userSession)? realTimePayee : notRealTimePayee;
			}
		}
		
		final String isRTPaymentKey = "isRTPayment"; // model view key
		final String isRTNotificationKey = "isRTNotification"; // model view key
		final String containsPaymentsKey = "containsPayments"; // model view key
		final String textKey = "text"; // model view key return
		
		data.put(isRTPaymentKey, String.valueOf(isRTPayment));
		data.put(isRTNotificationKey, String.valueOf(isRTNotification));
		data.put(containsPaymentsKey, String.valueOf(containsPayments));
		
		final Gson gson = new Gson(); // gson converter lib
		final String json = gson.toJson(data); // gson dictionary
		
		model.put(textKey,json); 

		return new ModelAndView(textKey, model);
	}
	
}
