package evertec.cibp.web.mapper;

import org.modelmapper.ModelMapper;

public class AlertsCcaModelMapperFactory {
	
	private AlertsCcaModelMapperFactory() {
	}

	
	/**
	 * Gets the mapper.
	 *
	 * @return the mapper
	 */
	public static ModelMapper getMapper() {

		ModelMapper modelMapper = new ModelMapper();
		return modelMapper;

	}

}
