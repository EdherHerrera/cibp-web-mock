package evertec.cibp.web.mapper;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;

import evertec.cibp.core.models.documents.CustomerDocuments;
import evertec.cibp.core.models.documents.CustomerDocumentsId;
import evertec.cibp.core.models.documents.CustomerDocumentsPackage;
import evertec.cibp.web.form.dto.CustomerDocPackageDto;
import evertec.cibp.web.form.dto.CustomerDocsDto;


/**
 * A factory for creating DocumentsModelMapper object configured
 * with document objects mappings detail.
 *
 * @author Erick Vargas
 * @version 1.0
 * @since 1.0
 */
public class DocumentsModelMapperFactory {
	
	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger( DocumentsModelMapperFactory.class );
	
	/** The model mapper. */
	private static ModelMapper modelMapper;
	
	static {
       
		modelMapper = new ModelMapper();
		modelMapper.addConverter( new CustomerDocumentsDtoSetConverter() );
		modelMapper.createTypeMap(String.class, Date.class);
		modelMapper.addConverter( new DateConverter() );
		modelMapper.addConverter(new CustomerDisbursementToDtoConverter());
	}
	
	/**
	 * Instantiates a new documents model mapper factory.
	 */
	private DocumentsModelMapperFactory() {
	}
	
	/**
	 * Gets the mapper.
	 *
	 * @return the mapper
	 */
	public static ModelMapper getMapper() {
		
		return modelMapper;
		
	}
	
	
	/**
	 * The Class DateConverter.
	 * Configures mapping to ISO8601 date format.
	 */
	private static class DateConverter implements Converter<String, Date> {

		/* (non-Javadoc)
		 * @see org.modelmapper.Converter#convert(org.modelmapper.spi.MappingContext)
		 */
		@Override
		public Date convert(MappingContext<String, Date> context) {
			if(context==null || context.getSource()==null)
				return null;

			Date date = null;
			try {
				LocalDateTime localDateTime = LocalDateTime.parse(context.getSource());
				ZonedDateTime zdt = localDateTime.atZone(ZoneId.systemDefault());
				date = Date.from(zdt.toInstant());
			}catch(DateTimeParseException dtpe) {
				logger.error( "Couldn't format String to java.util.Date: " + context.getSource(), dtpe);
			}

			return date;
			
		}
		
	}
	
	/**
	 * The Class CustomerDocumentsDtoSetConverter.
	 * A converter for Set objects of CustomerDocsDto to Set objects of CustomerDocuments 
	 */
	private static class CustomerDocumentsDtoSetConverter implements Converter<Set<CustomerDocsDto>, Set<CustomerDocuments>> {
		
		/* (non-Javadoc)
		 * @see org.modelmapper.Converter#convert(org.modelmapper.spi.MappingContext)
		 */
		public Set<CustomerDocuments> convert(MappingContext<Set<CustomerDocsDto>, Set<CustomerDocuments>> context) {
			Set<CustomerDocsDto> setSourceDTO = context.getSource();
			Set<CustomerDocuments> setDestinationEntity = new HashSet<>();
			
			Iterator<CustomerDocsDto> iter = setSourceDTO.iterator();
			
			while ( iter.hasNext() ) {
				
				CustomerDocsDto dto = iter.next();
				
				CustomerDocumentsPackage customerDP = new CustomerDocumentsPackage();
				customerDP.setDocPackId(dto.getCustomerDocumentsPackage().getDocPackId());
				
				CustomerDocumentsId id = new CustomerDocumentsId();
				id.setDocInfoId(dto.getDocInfoId());
				id.setUploadDate(dto.getUploadDate());
				
				CustomerDocuments entity = new CustomerDocuments();
				entity.setCustomerDocumentsPackage(customerDP);
				entity.setDisplayAfter(dto.getDisplayAfter());
				entity.setDocSearchId(dto.getDocSearchId());
				entity.setDocumentName(dto.getDocumentName());
				entity.setFrontEndText(dto.getFrontEndText());
				entity.setHideDoc(dto.getHideDoc());
				entity.setId(id);
				entity.setLastHiddenBy(dto.getLastHiddenBy());
				entity.setLastHiddenDate(dto.getLastHiddenDate());
				entity.setLastUnhideBy(dto.getLastUnhideBy());
				entity.setStagingUploadDate( dto.getStagingUploadDate() );
				
				setDestinationEntity.add(entity);
			}
			
			return setDestinationEntity;
		}
	}
	
	/**
	 * CustomerDisbursementToDtoConverter
	 * @author et55498
	 * Converts from a CustomerDocumentsPackage to a CustomerDocPackageDto object
	 */
	private static class CustomerDisbursementToDtoConverter implements Converter<CustomerDocumentsPackage, CustomerDocPackageDto>{
		public CustomerDocPackageDto convert(MappingContext<CustomerDocumentsPackage, CustomerDocPackageDto> context){
			
			CustomerDocumentsPackage entityDisbursement = context.getSource();
			CustomerDocPackageDto dtoDisbursement = new CustomerDocPackageDto();
			dtoDisbursement.setDocPackId(entityDisbursement.getDocPackId());
			dtoDisbursement.setHide(entityDisbursement.getHide());
			dtoDisbursement.setStatus(entityDisbursement.getStatus());
			dtoDisbursement.setUploadBy(entityDisbursement.getUploadBy());
			dtoDisbursement.setProfileId(entityDisbursement.getProfileId());
			dtoDisbursement.setDocTypeId(entityDisbursement.getDocTypeId());
			dtoDisbursement.setBankerEmail(entityDisbursement.getBankerEmail());
			dtoDisbursement.setPackSearchId(entityDisbursement.getPackSearchId());
			dtoDisbursement.setLastHiddenBy(entityDisbursement.getLastHiddenBy());
			dtoDisbursement.setLastUnhideBy(entityDisbursement.getLastUnhideBy());
			dtoDisbursement.setDocDate(entityDisbursement.getDocDate());
			dtoDisbursement.setUploadDate(entityDisbursement.getUploadDate());
			dtoDisbursement.setAcceptedDate(entityDisbursement.getAcceptedDate());
			dtoDisbursement.setLastHiddenDate(entityDisbursement.getLastHiddenDate());
			dtoDisbursement.setSignatureDeadlineDate(entityDisbursement.getSignatureDeadlineDate());
			dtoDisbursement.setDocuments(null);
			
			Set<CustomerDocsDto> setDestinationDto = new HashSet<>();
			Set<CustomerDocuments> setSourceDoc = entityDisbursement.getDocuments();
			
			Iterator<CustomerDocuments> iter = setSourceDoc.iterator();
			
			while ( iter.hasNext() ) {
				
				CustomerDocuments document = iter.next();
				CustomerDocsDto docsDto = new CustomerDocsDto();
				
				docsDto.setUploadDate(document.getId().getUploadDate());
				docsDto.setDocInfoId(document.getId().getDocInfoId());
				docsDto.setHideDoc(document.getHideDoc());
				docsDto.setDocSearchId(document.getDocSearchId());
				docsDto.setLastHiddenBy(document.getLastHiddenBy());
				docsDto.setLastUnhideBy(document.getLastUnhideBy());
				docsDto.setDisplayAfter(document.getDisplayAfter());
				docsDto.setFrontEndText(document.getFrontEndText());
				docsDto.setLastHiddenDate(document.getLastHiddenDate());
				docsDto.setStagingUploadDate(document.getStagingUploadDate());
				
				setDestinationDto.add(docsDto);
			}
			
			dtoDisbursement.setDocuments(setDestinationDto);
			
			return dtoDisbursement;
		}
	}
	
}
