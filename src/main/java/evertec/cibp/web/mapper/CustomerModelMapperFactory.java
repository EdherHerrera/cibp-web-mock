package evertec.cibp.web.mapper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerSignonStats;
import evertec.cibp.core.models.mbop.MbopCustomerInfo;
import evertec.cibp.web.form.dto.CustomerProfileDto;
import evertec.cibp.web.form.dto.MbopCustomerInfoDto;


/**
 * A factory for creating CustomerModelMapper object configured
 * with customer objects mappings detail.
 *
 * @author Joel Contreras
 * @version 1.0
 * @since 1.0
 */
public class CustomerModelMapperFactory {


	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger( CustomerModelMapperFactory.class );
	
	/** The model mapper. */
	private static ModelMapper modelMapper;
	
	
	static {
       
		modelMapper = new ModelMapper();
		modelMapper.addConverter( new CustomerProfileDtoSetConverter() );
		modelMapper.addConverter( new MbopCustomerInfoSetConverter() );
		modelMapper.createTypeMap(String.class, Date.class);
		modelMapper.addConverter( new DateConverter() );
	}
	
	/**
	 * Instantiates a new customer model mapper factory.
	 */
	private CustomerModelMapperFactory() {
	}
	
	/**
	 * Gets the mapper.
	 *
	 * @return the mapper
	 */
	public static ModelMapper getMapper() {
		
		return modelMapper;
		
	}
	
	/**
	 * The Class DateConverter.
	 * Configures mapping to ISO8601 date format.
	 */
	private static class DateConverter implements Converter<String, Date> {

		/* (non-Javadoc)
		 * @see org.modelmapper.Converter#convert(org.modelmapper.spi.MappingContext)
		 */
		@Override
		public Date convert(MappingContext<String, Date> context) {
			
			DateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US );
			Date date = null;
						
			try {
				date = df.parse( context.getSource() );
			} catch (ParseException e) {
				logger.error( "Couldn't format String to java.util.Date: " + context.getSource(),
						e);
			}
			
			
			return date;
			
		}
		
	}
	
	
	
	private static class CustomerProfileDtoSetConverter implements Converter<List<CustomerProfileDto>,List<CustomerProfile>>{

		@Override
		public List<CustomerProfile> convert(MappingContext<List<CustomerProfileDto>, List<CustomerProfile>> context) {
			// TODO Auto-generated method stub			
			List<CustomerProfileDto> customerProfileDto = context.getSource();
			List<CustomerProfile> customerProfiles = new ArrayList<>();
			
			Iterator<CustomerProfileDto> iter = customerProfileDto.iterator();

			while ( iter.hasNext() ) {
				CustomerProfileDto dto = iter.next();

			CustomerProfile customerProfile = new CustomerProfile();
			customerProfile.setProfileId(dto.getProfileId());
			customerProfile.setUsername(dto.getUserName());
			customerProfile.setStatus(dto.getStatus());
			customerProfile.setEnrollmentSource(dto.getEnrollmentSource());
			customerProfile.setAuthenticationMethod(dto.getAuthenticationMethod());
			customerProfile.setCustomerFlags(dto.getCustomerFlags());
			customerProfile.setCreationDate(dto.getCreationDate());
			customerProfile.setActivationDate(dto.getActivationDate());
			customerProfile.setBirthDate(dto.getBirthDate());
			customerProfile.setTaxId(dto.getTaxId());
			customerProfile.setEmail(dto.getEmail());
			customerProfile.setPasswordHash(dto.getPasswordHash());
			customerProfile.setPasswordSalt(dto.getPasswordSalt());
			customerProfile.setEnrollmentAccountNumber(dto.getEnrollmentAccountNumber());
			customerProfile.setEnrollmentAccountSubtype(dto.getEnrollmentAccountSubtype());
			customerProfile.setEnrollmentAccountProductId(dto.getEnrollmentAccountProductId());
			customerProfile.setFirstName(dto.getFirstName());
			customerProfile.setLastName(dto.getLastName());
			customerProfile.setMaidenName(dto.getMaidenName());
			customerProfile.setEnrollmentApplicationId(dto.getEnrollmentApplicationId());
			customerProfile.setGender(dto.getGender());
			customerProfile.setIsTransactional(dto.getIsTransactional());
			customerProfile.setUserInterface(dto.getUserInterface());
			customerProfile.setSignonStats(dto.getSignonStats());
			customerProfile.setCustEntitlements(dto.getCustEntitlements());
			
			customerProfiles.add(customerProfile);

			}
			
			return customerProfiles;
		}
		
	}

	private static class MbopCustomerInfoSetConverter implements Converter<MbopCustomerInfoDto,MbopCustomerInfo>{

		@Override
		public MbopCustomerInfo convert(MappingContext<MbopCustomerInfoDto, MbopCustomerInfo> context) {
			// TODO Auto-generated method stub
			MbopCustomerInfoDto mbopCustomerInfoDto = context.getSource();
			MbopCustomerInfo customerInfo = new MbopCustomerInfo();
			
			customerInfo.setMbopId(mbopCustomerInfoDto.getMbopId());
			customerInfo.setProfileId(mbopCustomerInfoDto.getProfileId());
			customerInfo.setEnrollmentSource(mbopCustomerInfoDto.getEnrollmentSource());
			customerInfo.setFirstSignonFail(mbopCustomerInfoDto.getFirstSignonFail());
			customerInfo.setFirstSignonOk(mbopCustomerInfoDto.getFirstSignonOk());
			customerInfo.setLastSignonOk(mbopCustomerInfoDto.getLastSignonOk());
			customerInfo.setLastSignonFail(mbopCustomerInfoDto.getLastSignonFail());
			customerInfo.setMbopPremiaInfo(mbopCustomerInfoDto.getMbopPremiaInfo());
			return customerInfo;
		}
		
		
	}
	


		

}
