package evertec.cibp.web.mapper;


import org.apache.commons.lang.StringEscapeUtils;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A factory for creating DCIModelMapperFactory object configured with alerts
 * objects mappings detail.
 *
 * @author Henry Terrero
 * @since 1.0
 * @version 1.0
 */
public class DCIModelMapperFactory {

    /**
     * The model mapper.
     */
    private static ModelMapper modelMapper;

    static {
        modelMapper = new ModelMapper();
    }

    private DCIModelMapperFactory() {
    }

   /**
     * Gets the mapper.
     *
     * @return the mapper
     */
    public static ModelMapper getMapper() {
        modelMapper.addConverter(new ConvertStringToDate());
        return modelMapper;
    }

    /**
     * ConvertStringToDate
     * @author NA
     * @version 1.0
     * @since 1.0
     */
    private static class ConvertStringToDate implements Converter<String, Date> {
        @Override
        public Date convert(MappingContext<String, Date> context) {

            String dateString = context.getSource();
            Date date = new Date();

            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                date = format.parse(dateString);
            } catch (ParseException e) {
            	System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
            }
            return date;
        }
    }
}

