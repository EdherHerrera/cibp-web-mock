package evertec.cibp.web.mapper;

import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse.AccountValidationStatus;
import evertec.cibp.web.form.dto.TravelNotificationDto;
import evertec.cibp.web.response.AccountIdentifier;
import evertec.cibp.web.response.AccountTv;
import evertec.cibp.web.response.RewardsCurrencyAmount;
import evertec.cibp.web.response.TLRRSCreditResponse;
import evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse;
import evertec.cibp.web.response.TsysLoyaltyRewardsRedemptionResponse;
import evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse.RewardsEarned;
import evertec.cibp.web.response.TsysLoyaltyRewardsRedemptionResponse.CurrencyAmount;
import evertec.cibp.web.response.TsysLoyaltyRewardsRedemptionResponse.CurrencyAmount.CurrencyEnum;
import evertec.creditcards.request.TsysLoyaltyRewardsRecurringStatementCreditRequest;
import evertec.creditcards.request.TsysLoyaltyRewardsRecurringStatementCreditRequest.RecurringCashBackType;
import evertec.creditcards.request.TsysLoyaltyRewardsRedemptionRequest;
import evertec.creditcards.request.TsysLoyaltyRewardsRedemptionRequest.DirectDepositAccountInfo;
import evertec.creditcards.request.TsysLoyaltyRewardsRedemptionRequest.RedemptionType;
import evertec.prophit.service.bean.TravelNotificationResponse;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;

import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * A factory for creating CreditCardModelMapperFactory object configured with
 * vault objects mappings detail.
 *
 * @author Henry Terrero
 * @version 1.0
 * @since 1.0
 */
public class CreditCardModelMapperFactory {

    private static ModelMapper modelMapper; // The model mapper.
    
    /** The Constant microservice response ok status. */
	private static final int MICROSERVICE_RESPONSE_OK_STATUS = 200;

    /** Instantiates a new credit card model mapper factory. */
    private CreditCardModelMapperFactory() {
    }

    static {
        modelMapper = new ModelMapper();
        modelMapper.addConverter(new TravelNotificationDtoSetConverter());
    }

    /**
     * Gets the mapper.
     *
     * @return the mapper
     */
    public static ModelMapper getMapper() {
        return modelMapper;
    }

    /**
     * Converts the Travel Notification Dto
     *
     * @return the travelNotificationResponse
     */
    private static class TravelNotificationDtoSetConverter
    implements Converter<TravelNotificationDto, TravelNotificationResponse> {

        @Override
        public TravelNotificationResponse convert(
                MappingContext<TravelNotificationDto, TravelNotificationResponse> context) {

            TravelNotificationDto dto = context.getSource();
            TravelNotificationResponse travelNotificationResponse = new TravelNotificationResponse();

            travelNotificationResponse.setCaseNumber(dto.getCaseNumber());
            travelNotificationResponse.setMessage(dto.getMessage());
            travelNotificationResponse.setNextMethod(dto.getNextMethod());
            travelNotificationResponse.setPlasticNumber(dto.getPlasticNumber());
            travelNotificationResponse.setStatus(dto.getStatus());
            travelNotificationResponse.setSuccess(dto.isSuccess());

            return travelNotificationResponse;

        }

    }

    /**
     * convertAccountInfoToAccountInfoResponse
     *      Convert object TsysLoyaltyRewardsAccountInfoResponse JAR to TsysLoyaltyRewardsAccountInfoResponse
     * @param accountInfoResponse
     *      CreditCards'TsysLoyaltyRewardsAccountInfoResponse
     *      
     * @return List TsysLoyaltyRewardsAccountInfoResponse
     **/
    public static List<TsysLoyaltyRewardsAccountInfoResponse> convertAccountInfoToAccountInfoResponse(
            evertec.creditcards.response.TsysLoyaltyRewardsAccountInfoResponse accountInfoResponse) {

        List<TsysLoyaltyRewardsAccountInfoResponse> arrayListAccountInfoResponse = new ArrayList<>(0); // list response
        TsysLoyaltyRewardsAccountInfoResponse response = new TsysLoyaltyRewardsAccountInfoResponse(); // unique response

        if (accountInfoResponse != null) {
            RewardsCurrencyAmount available = new RewardsCurrencyAmount(); // Object with amounts data
            if (accountInfoResponse.getRewardsBalance() != null && 
            		accountInfoResponse.getRewardsBalance().getAvailable() != null) {
            	
                available.setValue(accountInfoResponse.getRewardsBalance().getAvailable().getValue());
                available.setRewardscurrency(accountInfoResponse.getRewardsBalance().getAvailable().getRewardscurrency());
                
            }

            RewardsEarned rewardsEarned = new RewardsEarned(); // object that contains rewards amounts info

            rewardsEarned.setAvailable(available);
            response.setRewardsBalance(rewardsEarned);
            
            RewardsCurrencyAmount pooledAvailable = new RewardsCurrencyAmount(); // Object with amounts data (pooled).
            if (accountInfoResponse.getPooledRewardsBalance() != null && 
            		accountInfoResponse.getPooledRewardsBalance().getAvailable() != null) {
            	
                pooledAvailable.setValue(accountInfoResponse.getPooledRewardsBalance().getAvailable().getValue());
                pooledAvailable.setRewardscurrency(accountInfoResponse.getPooledRewardsBalance().getAvailable()
                		.getRewardscurrency());
                
            }

            RewardsEarned pooledRewardsEarned = new RewardsEarned(); // object that contains rewards amounts info

            pooledRewardsEarned.setAvailable(pooledAvailable);
            response.setPooledRewardsBalance(pooledRewardsEarned);
            
            response.setCanRedeemRewards(accountInfoResponse.isCanEarnRewards());
            response.setRewardsAccountStatus(accountInfoResponse.getRewardsAccountStatus());
        }

        arrayListAccountInfoResponse.add(response);
        return arrayListAccountInfoResponse;
    }

    /**
     * Converts the Travel Notification Dto
     *
     * @return the travelNotificationResponse
     */
    private static class ValidateAccountStatusResponseSetConverter implements Converter<LinkedHashMap<String, ?>, ValidateAccountStatusResponse>{

        @Override
        public ValidateAccountStatusResponse convert(MappingContext<LinkedHashMap<String, ?>, ValidateAccountStatusResponse> context) {
            ValidateAccountStatusResponse validateAccountStatusResponse = new ValidateAccountStatusResponse();
            final com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper(); //
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
            LinkedHashMap<String, ?> map = context.getSource();
            
            AccountTv account = new AccountTv(); //mapper.convertValue(map.get("account"), AccountTv.class);
           
            //account = mapper.convertValue(map.get("account"), AccountTv.class);
            if (map.get("account") != null) {
                account.setAccountID(((LinkedHashMap<?, ?>)map.get("account")).get("accountID").toString());
            }
            if (map.get("accountProductId") != null) {
                validateAccountStatusResponse.setAccountProductId(map.get("accountProductId").toString());
            }
            if (map.get("errorString") != null) {
                validateAccountStatusResponse.setErrorString(map.get("errorString").toString());
            }
            if (map.get("auditString") != null) {
                validateAccountStatusResponse.setAuditString(map.get("auditString").toString());
            }
            validateAccountStatusResponse.setStatus(AccountValidationStatus.valueOf(map.get("status").toString()));
            validateAccountStatusResponse.setAccount(account);
            return validateAccountStatusResponse;
        }
    }

    /**
     * Converts the TsysLoyaltyRewardsRedemptionResponse jar response
     *
     * @param redemptionResponse TsysLoyaltyRewardsRedemptionResponse JAR
     * @return the ArrayList<TsysLoyaltyRewardsRedemptionResponse>
     */
    public static MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> convertToRedemptionResponse(
            evertec.creditcards.response.TsysLoyaltyRewardsRedemptionResponse redemptionResponse) {

        List<TsysLoyaltyRewardsRedemptionResponse> arrayListAccountInfoResponse = new ArrayList<>(0); // List responses
        TsysLoyaltyRewardsRedemptionResponse response = new TsysLoyaltyRewardsRedemptionResponse(); // unique response

        CurrencyAmount cashbackAmount = new CurrencyAmount(); // object with redemption amount values

        if (redemptionResponse.getCashbackAmount().getCurrency().name().equals(CurrencyEnum.USD.name())) {
            cashbackAmount.setCurrency(CurrencyEnum.USD);
        }
        cashbackAmount.setValue(redemptionResponse.getCashbackAmount().getValue());

        final Map<String, Object> identifiers = new HashMap<>(0); // object wirh identifiers accounts

        final Set<Map.Entry<String,Object>> entrySet = redemptionResponse.getIdentifiers().entrySet(); // entry set loop
        for (Entry<String, Object> i : entrySet) {
            identifiers.containsKey(i.getKey());
            identifiers.containsValue(i.getValue());
        }
        RewardsCurrencyAmount rewardsBalance = new RewardsCurrencyAmount(); // object with rewards amount values
        rewardsBalance.setRewardscurrency(redemptionResponse.getRewardsBalance().getRewardscurrency());
        rewardsBalance.setValue(redemptionResponse.getRewardsBalance().getValue());

        response.setCashbackAmount(cashbackAmount);
        response.setConfirmationNumber(redemptionResponse.getConfirmationNumber());
        response.setIdentifiers(identifiers);
        response.setRedemptionDate(redemptionResponse.getRedemptionDate());
        response.setRewardsBalance(rewardsBalance);

        MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> microServiceResponse = new MicroServiceResponse<>(); // base return

        arrayListAccountInfoResponse.add(response);
        microServiceResponse.setData(arrayListAccountInfoResponse);        
        microServiceResponse.setStatus(MICROSERVICE_RESPONSE_OK_STATUS);
        return microServiceResponse;
    }

    /**
     * Converts the convertToRedemptionRequest jar response
     *
     * @param request TsysLoyaltyRewardsRedemptionRequest CIBP to TsysLoyaltyRewardsRedemptionRequest JAR
     * @return TsysLoyaltyRewardsRedemptionRequest
     */
    public static TsysLoyaltyRewardsRedemptionRequest convertToRedemptionRequest(
            evertec.cibp.web.request.TsysLoyaltyRewardsRedemptionRequest request) {
        TsysLoyaltyRewardsRedemptionRequest redemptionRequest = new TsysLoyaltyRewardsRedemptionRequest(); // Base tsys request

        if (request.getRedemptionType().name().equals(
                evertec.cibp.web.request.TsysLoyaltyRewardsRedemptionRequest.RedemptionType.STATEMENT_CREDIT.name())) {
            redemptionRequest.setRedemptionType(RedemptionType.STATEMENT_CREDIT);
        } else {
            DirectDepositAccountInfo transferAccountInfo = new DirectDepositAccountInfo(); // object with redemption data

            transferAccountInfo.setAccountNumber(request.getTransferAccountInfo().getAccountNumber());
            transferAccountInfo.setRoutingNumber(request.getTransferAccountInfo().getRoutingNumber());
            transferAccountInfo.setAccountType(request.getTransferAccountInfo().getAccountType());
            redemptionRequest.setRedemptionType(RedemptionType.DIRECT_DEPOSIT);
            redemptionRequest.setTransferAccountInfo(transferAccountInfo);
        }

        redemptionRequest.setRewardsValue(request.getRewardsValue());
        return redemptionRequest;
    }
    
    /**
     * 
     * @param creditCardResponse
     * @return
     */
    private static RewardsCurrencyAmount fillRedemptionValue(
    		evertec.creditcards.response.TsysLoyaltyRewardsRecurringStatementCreditResponse creditCardResponse) {
    	
    	RewardsCurrencyAmount redemptionValue = new RewardsCurrencyAmount(); //
    	
    	redemptionValue.setRewardscurrency(creditCardResponse.getRedemptionValue().getRewardscurrency());
    	redemptionValue.setValue(creditCardResponse.getRedemptionValue().getValue());
    	
    	return redemptionValue;
    	
    }
    
    /**
     * 
     * @param creditCardResponse
     * @return
     */
    private static AccountIdentifier fillAccountIdentifier(
    		evertec.creditcards.response.TsysLoyaltyRewardsRecurringStatementCreditResponse creditCardResponse) {
    	
    	AccountIdentifier accountIdentifier = new AccountIdentifier(); //
    	
    	accountIdentifier.setCardLast4(creditCardResponse.getIdentifiers().getCardLast4());	
		accountIdentifier.setAccountId(creditCardResponse.getIdentifiers().getAccountId());	
		
		return accountIdentifier;
    }
    
    /**
     * 
     * @param creditCardResponse
     * @return
     */
    private static RewardsCurrencyAmount fillRewardsBalance(
    		evertec.creditcards.response.TsysLoyaltyRewardsRecurringStatementCreditResponse creditCardResponse) {
    	
    	RewardsCurrencyAmount redemptionValue = new RewardsCurrencyAmount(); //
    	
    	redemptionValue.setRewardscurrency(creditCardResponse.getRewardsBalance().getValue());
    	
    	return redemptionValue;
    	
    }
    
    /**
     * 
     * @param creditCardResponse
     * @return
     */
    private static TLRRSCreditResponse fillCreditResponse(
    		evertec.creditcards.response.TsysLoyaltyRewardsRecurringStatementCreditResponse creditCardResponse) {
    	
    	TLRRSCreditResponse response = new TLRRSCreditResponse(); //
    	
    	if (creditCardResponse.getRedemptionValue() != null ) {
        	response.setRedemptionValue(fillRedemptionValue(creditCardResponse));
        }
        
        if (creditCardResponse.getIdentifiers() != null) {
        	response.setIdentifiers(fillAccountIdentifier(creditCardResponse));
        } 
        
        if (creditCardResponse.getRewardsBalance() != null) {
        	response.setRewardsBalance(fillRewardsBalance(creditCardResponse));
        }
        
        if (creditCardResponse.getRecurringCashBackType() != null) {
        	final evertec.cibp.web.enums.RecurringCashBackType type = evertec.cibp.web.enums.RecurringCashBackType.valueOf(creditCardResponse.getRecurringCashBackType()); //
        	
        	  response.setRecurringCashBackType(type);
        }
        
        if (creditCardResponse.getItemCode() != null) {
        	response.setItemCode(creditCardResponse.getItemCode()); 
        }
        
		return response;
    }
    
    
    /**
     * Converts the convertRecurringStatementCreditResponse to values 
     *
     * @param creditCardResponse TsysLoyaltyRewardsRecurringStatementCreditResponse JAR to 
     * TsysLoyaltyRewardsRecurringStatementCreditResponse CIBP
     * @return TsysLoyaltyRewardsRecurringStatementCreditResponse
     */
    public static MicroServiceResponse<TLRRSCreditResponse> convertRecurringStatementCreditResponse(
            evertec.creditcards.response.TsysLoyaltyRewardsRecurringStatementCreditResponse creditCardResponse) {
        List<TLRRSCreditResponse> listResponse = new ArrayList<>(0); //
        TLRRSCreditResponse response = new TLRRSCreditResponse(); //
        
        if (creditCardResponse != null) { 
    	   
        	response = fillCreditResponse(creditCardResponse);
       }
       
        listResponse.add(response);
        MicroServiceResponse<TLRRSCreditResponse> microServiceResponse = new MicroServiceResponse<>(); //
        
        microServiceResponse.setData(listResponse);
        microServiceResponse.setStatus(MICROSERVICE_RESPONSE_OK_STATUS);
        
        return microServiceResponse;
    }

    /**
     * Converts the convertRecurringStatementCreditRequest CIBP to  
     *
     * @param cibpWebRequest TsysLoyaltyRewardsRecurringStatementCreditRequest CIBP to 
     * TsysLoyaltyRewardsRecurringStatementCreditRequest JAR
     * @return TsysLoyaltyRewardsRecurringStatementCreditRequest
     */
    public static TsysLoyaltyRewardsRecurringStatementCreditRequest convertRecurringStatementCreditRequest(
            evertec.cibp.web.request.TLRRSCreditRequest cibpWebRequest) {
        TsysLoyaltyRewardsRecurringStatementCreditRequest request = new TsysLoyaltyRewardsRecurringStatementCreditRequest(); //
       
        	if (cibpWebRequest.getItemCode() != null) {
        		request.setItemCode(cibpWebRequest.getItemCode());
        	}
        	
        	if (cibpWebRequest.getRecurringCashBackType() != null) {
		       
        		switch (cibpWebRequest.getRecurringCashBackType()) {
				case MONTHLY:
					request.setRecurringCashBackType(RecurringCashBackType.MONTHLY);
					break;
				case THRESHOLD:
					request.setRecurringCashBackType(RecurringCashBackType.THRESHOLD);
					break;
				default: request.setRecurringCashBackType(RecurringCashBackType.MONTHLY);
					break;
				}
        	}
        return request;
    }
    
}
