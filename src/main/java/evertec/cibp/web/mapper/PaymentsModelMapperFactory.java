package evertec.cibp.web.mapper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.services.responses.PaymentHistoryResponse;
import evertec.cibp.vs.beans.accounts.AccountExt;
import evertec.cibp.vs.beans.payments.RecurringPayment;
import evertec.cibp.web.utils.AccountExtMixin;

/**
 * A factory for creating PaymentsModelMapper object configured with
 * payment objects mappings details.
 *
 * @author Jorge De Los Santos
 * @version 1.0
 * @since 1.0
 */
public class PaymentsModelMapperFactory {
	
	/**
	 * Instantiates a new Payments model mapper factory.
	 */
	private PaymentsModelMapperFactory() {

	}
	
	/**
	 * Gets the mapper.
	 *
	 * @return the mapper
	 */
	public static ModelMapper getMapper() {

		ModelMapper modelMapper = new ModelMapper();
		modelMapper.addConverter(new PaymentHistoryResponseConverter());
		return modelMapper;

	}
	
	private static class PaymentHistoryResponseConverter implements Converter<LinkedHashMap<String, ?>, PaymentHistoryResponse> {

	/**
	 * Convert a LinkedHashMap object to a PaymentHistoryResponse object.
	 * 
	 * @param context the MappingContext object
	 * @return a PaymentHistoryResponse object
	 */
	@Override
	public PaymentHistoryResponse convert(MappingContext<LinkedHashMap<String, ?>, PaymentHistoryResponse> context) {
	
		LinkedHashMap<String, ?> map = context.getSource();
		ObjectMapper objectMapper = createCustomObjectMapper();
		
		PaymentHistoryResponse paymentHistoryResponse = objectMapper.convertValue(map, PaymentHistoryResponse.class);
		PaymentHistoryResponse response = new PaymentHistoryResponse();
		
		response.setInprocessPayments(getPaymentList(paymentHistoryResponse.getInprocessPayments()));
		response.setPayments(getPaymentList(paymentHistoryResponse.getPayments()));
	
		return response;
	
	}
	
	/**
	 * Create a custom ObjectMapper to help mapping complex objects.
	 *
	 * @return the customized ObjectMapper
	 */
	private ObjectMapper createCustomObjectMapper() {
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.setVisibility(mapper.getSerializationConfig().getDefaultVisibilityChecker()
				.withFieldVisibility(JsonAutoDetect.Visibility.ANY)
				.withGetterVisibility(JsonAutoDetect.Visibility.ANY)
				.withSetterVisibility(JsonAutoDetect.Visibility.ANY)
				.withCreatorVisibility(JsonAutoDetect.Visibility.ANY));
		mapper.enable(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES);
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		return mapper;
	}
	
	
	/**
	 * This method use a RecurringPayment object to create TVPayment objects and then return a TVPayment list.
	 * 
	 * @param payments The TVPayment list
	 * @return A List<TVPayment> object
	 */
	private List<TVPayment> getPaymentList(List<TVPayment> payments){
		
		List<TVPayment> paymentList = new ArrayList<>();
		
		if(payments != null && !payments.isEmpty()) {
			
			ObjectMapper objectMapper = createCustomObjectMapper();
			objectMapper.addMixIn(AccountExt.class, AccountExtMixin.class);
			
			payments.forEach(payment ->{
				RecurringPayment backendObject = objectMapper.convertValue(payment.getBackendObject(), RecurringPayment.class);
				TVPayment tvp = new TVPayment(backendObject);
				paymentList.add(tvp);
				
			});
		}
		
		return paymentList;
	}

}


}
