package evertec.cibp.web.mapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringEscapeUtils;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.spi.MappingContext;

import evertec.cibp.core.models.customer.AlertsHistory;
import evertec.cibp.core.models.customer.CustomerBalanceAlert;
import evertec.cibp.web.form.dto.AlertsHistoryDto;
import evertec.cibp.web.form.dto.CustomerBalanceAlertDto;
import evertec.cibp.web.form.dto.EmailConfirmationDto;
import evertec.cibp.web.models.alerts.EmailConfirmation;


/**
 * A factory for creating AlertsModelMapper object configured
 * with alerts objects mappings detail.
 * 
 * @author Luis Alicea
 * @since 1.0
 * @version 1.0
 */
public class AlertsModelMapperFactory {
	
	private AlertsModelMapperFactory() {
	}
	
	/**
	 * Gets the mapper.
	 *
	 * @return the mapper
	 */
	public static ModelMapper getMapper() {
		
        ModelMapper modelMapper = new ModelMapper();
		
		modelMapper.addMappings( new EmailConfirmationMap() );
		modelMapper.addConverter( new ConvertStringToDate() );
		modelMapper.addConverter( new AlertsHistoryDtoSetConverter() );
		modelMapper.addConverter(new BalanceAlertsDtoSetConverter());
		
		return modelMapper;
		
	}
	

	
	/**
	 * The Class EmailConfirmationMap to manage mapping properties.
	 */
	private static class EmailConfirmationMap extends PropertyMap<EmailConfirmationDto, EmailConfirmation> {
		
		/* (non-Javadoc)
		 * @see org.modelmapper.PropertyMap#configure()
		 */
		@Override
		protected void configure() {
		
		}
	}
	
	private static class ConvertStringToDate implements Converter<String,Date>{
		
		@Override
		public Date convert(MappingContext<String,Date> context) {
			
			String dateString = context.getSource();
			Date date = null;

			try {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
				date = format.parse(dateString);
			} catch (ParseException e) {
				System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
			}
			return date;
		}
		
	}
	
	private static class AlertsHistoryDtoSetConverter implements Converter<AlertsHistoryDto,AlertsHistory>{

		@Override
		public AlertsHistory convert(MappingContext<AlertsHistoryDto, AlertsHistory> context) {
			
			AlertsHistoryDto dto = context.getSource();
			AlertsHistory alertsHistory = new AlertsHistory();
			
			alertsHistory.setId(dto.getId());
			alertsHistory.setProfileId(dto.getProfileId());
			alertsHistory.setAccountNumber(dto.getAccountNumber());
			alertsHistory.setCardNumber(dto.getCardNumber());
			alertsHistory.setTaxId(dto.getTaxId());
			alertsHistory.setEventType(dto.getEventType());
			alertsHistory.setMessage(dto.getMessage());
			alertsHistory.setEmail(dto.getEmail());
			alertsHistory.setPhone(dto.getPhone());
			alertsHistory.setDate(dto.getDate());
			alertsHistory.setPaxusTransactionType(dto.getPaxusTransactionType());
			alertsHistory.setSource(dto.getSource());
			alertsHistory.setChannel(dto.getChannel());
	       
			
			return alertsHistory;
		}
		
	}
	
	private static class BalanceAlertsDtoSetConverter implements Converter<CustomerBalanceAlertDto,CustomerBalanceAlert>{

		@Override
		public CustomerBalanceAlert convert(MappingContext<CustomerBalanceAlertDto, CustomerBalanceAlert> context) {
			
			CustomerBalanceAlertDto dto = context.getSource();
			CustomerBalanceAlert balanceAlert = new CustomerBalanceAlert();
			
			balanceAlert.setId(dto.getId());
			balanceAlert.setCustomerProfile(dto.getCustomerProfile());
			balanceAlert.setPermId(dto.getPermId());
			balanceAlert.setNickname(dto.getNickname());
			balanceAlert.setAccountNumber(dto.getAccountNumber());
			balanceAlert.setAccountSubtype(dto.getAccountSubtype());
			balanceAlert.setAccountSection(dto.getAccountSection());
			balanceAlert.setAccountProductId(dto.getAccountProductId());
			balanceAlert.setAlertCondition(dto.getAlertCondition());
			balanceAlert.setChannelPush(dto.getChannelPush());
			balanceAlert.setChannelSMS(dto.getChannelSMS());
			balanceAlert.setChannelEmail(dto.getChannelEmail());
			balanceAlert.setAlertlanguage(dto.getAlertlanguage());
			balanceAlert.setAlertposicion(dto.getAlertposicion());
			
			return balanceAlert;
		}
		
	}

}
