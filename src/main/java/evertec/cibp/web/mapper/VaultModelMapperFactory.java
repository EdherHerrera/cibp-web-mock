/**
 * 
 */
package evertec.cibp.web.mapper;

import org.apache.log4j.Logger;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;

import evertec.cibp.web.form.dto.VaultSignInDto;
import evertec.cibp.web.models.vault.VaultSignIn;

/**
 * A factory for creating VaultModelMapperFactory object configured
 * with vault objects mappings detail.
 *
 * @author Eduardo Agosto
 * @version 1.0
 * @since 1.0
 */
public class VaultModelMapperFactory {
	
	/** The model mapper. */
	private static ModelMapper modelMapper;
	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger( VaultModelMapperFactory.class );

	private VaultModelMapperFactory() { }
	
	static {
        modelMapper = new ModelMapper();
        modelMapper.addConverter( new VaultSignInDtoSetConverter() );
	}
	
	/**
	 * Gets the mapper.
	 *
	 * @return the mapper
	 */
	public static ModelMapper getMapper() {
		return modelMapper;
	}
	
	/**
	 * Converts the Vault SignIn Dto
	 *
	 * @return the VaultSignIn
	 */
	private static class VaultSignInDtoSetConverter implements Converter<VaultSignInDto, VaultSignIn>{
		@Override
		public VaultSignIn convert(MappingContext<VaultSignInDto, VaultSignIn> context) {
			
			VaultSignInDto dto = context.getSource();
			VaultSignIn vSignIn = new VaultSignIn();
			
			vSignIn.setPermId(dto.getPermId());
			vSignIn.setSessionKey(dto.getSessionKey());
			
			return vSignIn;
		}	
	}
}
