package evertec.cibp.web.mapper;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;

import evertec.cibp.core.models.accountopening.AccountApplication;
import evertec.cibp.web.form.dto.AccountApplicationDto;

/**
 * A factory for creating AccountModelMapperFactory object configured
 * with account objects mappings detail.
 *
 * @author Henry Terrero
 * @version 1.0
 * @since 1.0
 */
public class AccountModelMapperFactory {
	
	private AccountModelMapperFactory() { }
	
	/**
	 * Gets the mapper.
	 *
	 * @return the mapper
	 */
	public static ModelMapper getMapper() {
		
        ModelMapper modelMapper = new ModelMapper();
		modelMapper.addConverter( new AccountAccountApplicationDtoSetConverter() );

		return modelMapper;
	}
	
	private static class AccountAccountApplicationDtoSetConverter implements Converter<AccountApplicationDto,AccountApplication>{

		@Override
		public AccountApplication convert(MappingContext<AccountApplicationDto, AccountApplication> context) {
			
			AccountApplicationDto dto = context.getSource();
			AccountApplication accountApplication = new AccountApplication();
			
			accountApplication.setUsername(dto.getUsername());
	        accountApplication.setApplicationId(dto.getApplicationId());
	        accountApplication.setCreationDate(dto.getCreationDate());
	        accountApplication.setValidationType(dto.getValidationType());
	        accountApplication.setSignatureCardComplete(dto.isSignatureCardComplete());
	        accountApplication.setStatus(dto.getStatus());
	        accountApplication.setAccountAvailableInBackend(dto.isAccountAvailableInBackend());
	        accountApplication.setAccountSubtype(dto.getAccountSubtype());
	        accountApplication.setAccountProductId(dto.getAccountProductId());
	        accountApplication.setAccountProductDescription(dto.getAccountProductDescription());
	        accountApplication.setAccountName(dto.getAccountName());
	        accountApplication.setWaiveOverdrafts(dto.isWaiveOverdrafts());
	        accountApplication.setIncomplete(dto.isIncomplete());
	        accountApplication.setAccountHasBalanceInBackend(dto.isAccountHasBalanceInBackend());
	        accountApplication.setAccountNumber(dto.getAccountNumber());
			
			return accountApplication;
		}
		
	}

}
