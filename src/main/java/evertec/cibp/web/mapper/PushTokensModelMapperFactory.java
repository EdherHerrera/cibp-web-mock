package evertec.cibp.web.mapper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import org.apache.log4j.Logger;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;

import evertec.cibp.core.dto.PushTokensDto;
import evertec.cibp.core.models.customer.CibpPushTokens;

/**
 * A factory for creating DocumentsModelMapper object configured
 * with document objects mappings detail.
 *
 * @author Diego Bocca
 * @version 1.0
 * @since 1.0
 */

public class PushTokensModelMapperFactory {
	
	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger( PushTokensModelMapperFactory.class );
	
	/** The model mapper. */
	private static ModelMapper modelMapper;
	
	static {
       
		modelMapper = new ModelMapper();
		modelMapper.addConverter( new PushTokensDtoSetConverter() );
		modelMapper.createTypeMap(String.class, Date.class);
		modelMapper.addConverter( new DateConverter() );
	}
	
	/**
	 * Instantiates a new documents model mapper factory.
	 */
	private PushTokensModelMapperFactory() {
	}
	
	/**
	 * Gets the mapper.
	 *
	 * @return the mapper
	 */
	public static ModelMapper getMapper() {
		
		return modelMapper;
		
	}
	
	
	/**
	 * The Class DateConverter.
	 * Configures mapping to ISO8601 date format.
	 */
	private static class DateConverter implements Converter<String, Date> {

		/* (non-Javadoc)
		 * @see org.modelmapper.Converter#convert(org.modelmapper.spi.MappingContext)
		 */
		@Override
		public Date convert(MappingContext<String, Date> context) {
			
			DateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US );
			Date date = null;
						
			try {
				date = df.parse( context.getSource() );
			} catch (ParseException e) {
				logger.error( "Couldn't format String to java.util.Date: " + context.getSource(),
						e);
			}
			
			
			return date;
			
		}
		
	}
	
	/**
	 * The Class CustomerDocumentsDtoSetConverter.
	 * A converter for Set objects of PushTokensDto to Set objects of CustomerDocuments 
	 */
	private static class PushTokensDtoSetConverter implements Converter<Set<PushTokensDto>, Set<CibpPushTokens>> {
		
		/* (non-Javadoc)
		 * @see org.modelmapper.Converter#convert(org.modelmapper.spi.MappingContext)
		 */
		public Set<CibpPushTokens> convert(MappingContext<Set<PushTokensDto>, Set<CibpPushTokens>> context) {
			Set<PushTokensDto> setSourceDTO = context.getSource();
			Set<CibpPushTokens> setDestinationEntity = new HashSet<>();
			
			Iterator<PushTokensDto> iter = setSourceDTO.iterator();
			
			while ( iter.hasNext() ) {
				
				PushTokensDto dto = iter.next();
				
				
				CibpPushTokens entity = new CibpPushTokens();
				
				entity.setProfileId(dto.getProfileId());
				entity.setPushToken(dto.getToken());
				
//				entity.setDisplayAfter(dto.getDisplayAfter());
//				entity.setDocSearchId(dto.getDocSearchId());
//				entity.setDocumentName(dto.getDocumentName());
//				entity.setFrontEndText(dto.getFrontEndText());
//				entity.setHideDoc(dto.getHideDoc());
//				entity.setLastHiddenBy(dto.getLastHiddenBy());
//				entity.setLastHiddenDate(dto.getLastHiddenDate());
//				entity.setLastUnhideBy(dto.getLastUnhideBy());
//				entity.setStagingUploadDate( dto.getStagingUploadDate() );
				
				setDestinationEntity.add(entity);
			}
			
			return setDestinationEntity;
		}
	}	
}
