package evertec.cibp.web.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.documents.CustomerDocumentsPackage;
import evertec.cibp.web.form.dto.EmailConfirmationDto;
import evertec.cibp.web.mapper.AlertsModelMapperFactory;
import evertec.cibp.web.models.alerts.EmailConfirmation;
import evertec.cibp.web.models.alerts.MessageTemplateType;
import evertec.cibp.web.request.AddNotificationRequest;
import evertec.cibp.web.session.UserSession;

/**
 * The Class EmailNotificationFacade provides a facade to refactoring existing code and
 * implementing new code for the email management operations.
 * Microservices Facade abstract the switch logic from monolithic code and microservice code
 * 
 */
public class EmailNotificationFacade {


	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger( EmailNotificationFacade.class );

	/** The instance. */
	private static EmailNotificationFacade instance = null;

	/** From Email Name for BPPR. */
	private static final String FROM_NAME_BPPR = "Mi Banco Popular";

	/** From Email for BPPR. */
	private static final String FROM_EMAIL_BPPR = "info@bppr.com";

	private static final String POST_EMAIL_NOTIFICATION = "alerts.post.email.notification";


	/** Instantiates a new alerts facade. */
	protected EmailNotificationFacade() {}

	/**
	 * Gets the single instance of AlertsFacade.
	 *
	 * @return single instance of AlertsFacade
	 */
	public static EmailNotificationFacade getInstance() {
		if ( instance == null ) {
			instance = new EmailNotificationFacade();
		}
		return instance;
	}

	/**
	 *Send Notification that only need the name in the context
	 * @param MessageTemplateType template Name
	 * @param userSession the user session
	 * @return the EmailConfirmation
	 */
	public EmailConfirmation sendNotification ( MessageTemplateType messageTemplateType, UserSession userSession ) {

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(POST_EMAIL_NOTIFICATION);
		Map<String,Object> context = new HashMap<>();
		context.put("name", userSession.getCustomerProfile().getFirstName());

		return consumeQueueNotification(messageTemplateType, context, userSession, msProperties.getEndpoint());

	}


	/**
	 *Send Notification
	 * @param MessageTemplateType template Name.
	 * @param Map<String,Object> A map with the variables required for the template.
	 * @param userSession the user session
	 * @return EmailConfirmation
	 */
	public EmailConfirmation sendNotification ( MessageTemplateType messageTemplateType, Map<String,Object> context, UserSession userSession ) {
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(POST_EMAIL_NOTIFICATION);
		return consumeQueueNotification(messageTemplateType, context, userSession, msProperties.getEndpoint());

	}

	/**
	 * Send document acceptance notifications.
	 *
	 * @param userSession the user session
	 * @param CustomerDocumentsPackage the disbursement
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public List<EmailConfirmation> sendDocumentAcceptanceNotifications( UserSession userSession, CustomerDocumentsPackage disbursement ) {

		List<EmailConfirmation> emailNotifications = new ArrayList<>();
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(POST_EMAIL_NOTIFICATION);
		if (msProperties.isEnabled()) {
			Map<String,Object> context = new HashMap<>();
			context.put("name", userSession.getCustomerProfile().getFirstName());
			context.put("loanNumber", disbursement.getPackSearchId());

			emailNotifications.add(consumeQueueNotification(MessageTemplateType.DOCUMENTS_ACCEPTANCE_EMAIL, context, userSession, msProperties.getEndpoint()));

			//CSR Notification
			//CIS customer name MBSE-1801
			context.put("name", disbursement.getCustomerName() != null ? disbursement.getCustomerName() : userSession.getCustomerProfile().getFirstName());
			AddNotificationRequest request = new AddNotificationRequest(FROM_NAME_BPPR, FROM_EMAIL_BPPR, disbursement.getUploadBy(),
					disbursement.getBankerEmail(), null, MessageTemplateType.DOCUMENTS_ACCEPTANCE_CSR_EMAIL.toString(), PreferredLanguage.ENGLISH, context, 0);

			emailNotifications.add(consumeQueueNotification(request, userSession, msProperties.getEndpoint()));
		} else {

			userSession.getCustomerService().sendDocumentAcceptanceEmail(userSession.getCustomerProfile(), userSession.getPreferredLanguage());
			userSession.getCustomerService().sendCSRDocumentAcceptanceEmail(userSession.getCustomerProfile(), disbursement);
		}

		return emailNotifications;
	}

	/**
	 * Consume queueNotification
	 *
	 * @param MessageTemplate the template name
	 * @param context map that contains the template variables
	 * @param userSession the user session
	 * @return EmailConfirmation
	 */
	private EmailConfirmation consumeQueueNotification( MessageTemplateType messageTemplateType, Map<String,Object> context, UserSession userSession, String endpoint ) {

		EmailConfirmation emailConfirmation = new EmailConfirmation();

		CustomerProfile customerProfile = userSession.getCustomerProfile();

		AddNotificationRequest request = new AddNotificationRequest(FROM_NAME_BPPR, FROM_EMAIL_BPPR, customerProfile.getFullName(),
				customerProfile.getEmail(), customerProfile.getProfileId(), messageTemplateType.toString(), userSession.getPreferredLanguage(), context, 0); 


		MicroServiceResponse<EmailConfirmationDto> response 
		= userSession.getRestClientResourceOwner()
		.postForMicroServiceResponse(endpoint, request, null);

		if ( 201 == response.getStatus() ) {

			AlertsModelMapperFactory.getMapper().map( response.getData().get(0), emailConfirmation );

		} else {
			logger.error("Problems consuming queueNotification operation with " + 
					request + ". Status: " + 
					response.getStatus() +
					" " + 
					response.getErrors());
			emailConfirmation = null;
		}

		return emailConfirmation;
	}

	/**
	 * Consume queueNotification
	 *
	 * @param AddNotificationRequest the service request object
	 * @param userSession the user session
	 * @return EmailConfirmation
	 */
	private EmailConfirmation consumeQueueNotification( AddNotificationRequest request, UserSession userSession, String endpoint ) {

		EmailConfirmation emailConfirmation = new EmailConfirmation();

		MicroServiceResponse<EmailConfirmationDto> response 
		= userSession.getRestClientResourceOwner().postForMicroServiceResponse(endpoint, request, null);

		if ( 201 == response.getStatus() ) {

			AlertsModelMapperFactory.getMapper().map( response.getData(), emailConfirmation );

		} else {
			logger.error("Problems consuming queueNotification operation with " + 
					request + ". Status: " + 
					response.getStatus() +
					" " + 
					response.getErrors());
			emailConfirmation = null;
		}

		return emailConfirmation;
	}

}