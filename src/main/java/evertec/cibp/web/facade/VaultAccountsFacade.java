package evertec.cibp.web.facade;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.modelmapper.TypeToken;

import com.evertec.cibp.api.client.RestClient;
import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.accountopening.AccountApplications;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.vault.VaultAccount;
import evertec.cibp.core.services.requests.BankingSessionRequest;
import evertec.cibp.core.services.responses.BankingSessionResponse;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.form.dto.VaultAccountDto;
import evertec.cibp.web.form.dto.VaultSignInDto;
import evertec.cibp.web.mapper.VaultModelMapperFactory;
import evertec.cibp.web.models.vault.VaultSignIn;
import evertec.cibp.web.request.VaultAccountsRequest;
import evertec.cibp.web.services.modules.PortalServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * The Class VaultAccountsFacade provides a facade to refactoring existing code and
 * implementing new code for the vault accounts operations.
 * Microservices Facade abstract the switch logic from monolithic code and microservice code
 * 
 */
public class VaultAccountsFacade {

	/**
	 * The constant logger
	 */
	private static final Logger logger = Logger.getLogger(VaultAccountsFacade.class);

	/**
	 * The instance
	 */
	private static VaultAccountsFacade instance = null;

	/**
	 * Endpoints for vault accounts service
	 */
	private static final String GET_ACCOUNTS = "vaultaccounts.get.accounts";
	private static final String GET_BANKING_SESSION = "vaultaccounts.banking.session";
	private static final int MICROSERVICE_RESPONSE_SUCCESS_STATUS = 202;

	/**
	 * Instantiates a new vault accounts facade.
	 */
	protected VaultAccountsFacade() {
	}

	/**
	 * Gets the single instance of VaultAccountsFacade.
	 *
	 * @return single instance of VaultAccountsFacade
	 */
	public static VaultAccountsFacade getInstance() {
		if (instance == null)
			instance = new VaultAccountsFacade();
		return instance;
	}
	
	/**
	 * Method to get users accounts from the vault.
	 *
	 * @param vaultAccountsRequest
	 *            the request with the necessary params for vault accounts
	 */
	public List<TVFrontendAccount> getAccounts(VaultAccountsRequest vaultAccountsRequest) {
		UserSession userSession = vaultAccountsRequest.getUserSession();
		logger.info("Getting user accounts from the vault");
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_ACCOUNTS);
		if (msProperties.isEnabled()) {
			List<TVFrontendAccount> accounts = consumeVaultAccountsMicroservice(vaultAccountsRequest, msProperties.getEndpoint());
			userSession.clasifyAccounts(accounts, true);
			return accounts;
		}
		
		return userSession.updateGetAccounts();
	}
	
	/**
	 * Method to get users accounts from the vault.
	 *
	 * @param vaultAccountsRequest
	 *            the request with the necessary params for vault accounts
	 */
	public List<TVFrontendAccount> getCcaCardPlastics(VaultAccountsRequest vaultAccountsRequest) {
		final Logger logger = Logger.getLogger(VaultAccountsFacade.class);
		UserSession userSession = vaultAccountsRequest.getUserSession();
		logger.info("Getting user accounts from the vault");
		
		return userSession.updateCcaCardPlastics();
	}
	
	/**
	 * Method to get session key from the vault.
	 *
	 * @param vaultAccountsRequest
	 *            the request with the necessary params for vault accounts
	 */
	public BankingSessionResponse vaultSignIn(VaultAccountsRequest vaultAccountsRequest) {
		UserSession userSession = vaultAccountsRequest.getUserSession();
		logger.info("Sign in to the vault");
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_BANKING_SESSION);
		if (msProperties.isEnabled())
			return consumeVaultSignInMicroservice(vaultAccountsRequest, msProperties.getEndpoint());
		
		CustomerEntitlement ent = userSession.getCustomerProfile().getEntitlementByType(EntitlementType.BANKING);
		evertec.cibp.core.models.entitlement.BankingEntitlement entt = new BankingEntitlement(ent);
		
		HttpServletRequest httpRequest = vaultAccountsRequest.getRequest();
		BankingSessionRequest sessionRequest = new BankingSessionRequest(Utils.generateCustomerInteractionEvent(httpRequest), 
				userSession.getCustomerProfile(), null, entt, false);
		
		return userSession.getCustomerService().getBankingSession(sessionRequest);
	}
	
	/**
	 * Consumes the vault sign on method
	 *
	 * @param vaultAccountsRequest
	 *            the request with the necessary params for vault accounts
	 */
	public BankingSessionResponse consumeVaultSignInMicroservice(VaultAccountsRequest vaultAccountsRequest, String endpoint) {
		UserSession userSession = vaultAccountsRequest.getUserSession();
		BankingEntitlement bankingEntitlement = vaultAccountsRequest.getBankingEntitlement();
		RestClient restClient = userSession.getRestClientResourceOwner();
		
		if (vaultAccountsRequest.getBankingEntitlement() == null) {
			logger.error("No authentication data for customer received. (BankingSession not exist in Customer Profile)");
			return null;
		}
		
		String enrollmentAccountSubtype = bankingEntitlement.getAccountType();
		String enrollmentAccount = bankingEntitlement.getAccountNumber();
		String ssn = bankingEntitlement.getSsn();
		
		if (GenericValidator.isBlankOrNull(enrollmentAccountSubtype))
			enrollmentAccountSubtype = "IDA";
		else if (enrollmentAccountSubtype.equals("AMEX"))
			enrollmentAccountSubtype = "CCA";
		
		// Setting URI
		String requestURI = endpoint +"/"+ssn+"/"+enrollmentAccountSubtype+"/"+enrollmentAccount;
		
		// Microservice call
		MicroServiceResponse<VaultSignInDto> response = restClient.getForMicroServiceResponse(requestURI, null);
		return validateSignInResponse(response);
	}
	
	/**
	 * Consumes the vault account microservice
	 *
	 * @param vaultAccountsRequest
	 *            the request with the necessary params for vault accounts
	 */
	public List<TVFrontendAccount> consumeVaultAccountsMicroservice(VaultAccountsRequest vaultAccountsRequest, String endpoint) {
		UserSession userSession = vaultAccountsRequest.getUserSession();
		
		BankingSession bankingSession = userSession.getBankingSession();
		
		if (bankingSession == null) {
			logger.error("Banking Session is null");
			return new LinkedList<>();
		}
		
		String permId = bankingSession.getPermId().toString();
		String sessionKey = bankingSession.getSessionKey();
		
		// Setting URI
		String requestURI = endpoint +"/"+permId+"/"+sessionKey;
		
		// Microservice call
		MicroServiceResponse<VaultAccountDto> response = userSession.getRestClientResourceOwner()
				.getForMicroServiceResponse(requestURI, null);
		
		return validateAccountsResponse(userSession, response);
	}
	
	
	/**
	 * Validates the response from the sign in service
	 *
	 * @param response
	 *            the dto response
	 */
	public BankingSessionResponse validateSignInResponse(MicroServiceResponse<VaultSignInDto> response) {
		boolean vaultSignInSuccess = (MICROSERVICE_RESPONSE_SUCCESS_STATUS == response.getStatus());
		
		if (!vaultSignInSuccess) {
			logger.error(response.getErrors().get(0));
			return null;
		}
		
		logger.info(response.getData().toString() + ": Vault Sign In success");
		VaultSignIn vaultSignIn = VaultModelMapperFactory.getMapper().map(response.getData().get(0), new TypeToken<VaultSignIn>() {}.getType());
		
		if (vaultSignIn == null || GenericValidator.isBlankOrNull(vaultSignIn.getPermId()) || GenericValidator.isBlankOrNull(vaultSignIn.getSessionKey())) {
			logger.error("OD VaultSignIn is null or contain null values");
			return null;
		}
		
		String sessionKey = vaultSignIn.getSessionKey();
		String permId = vaultSignIn.getPermId();
		
		BankingSessionResponse bankingResp = new BankingSessionResponse();
		BankingSession bankSession = new BankingSession(BigInteger.valueOf(Long.parseLong(permId)), sessionKey);
		bankingResp.setBankingSession(bankSession);
		return bankingResp;
		
//		if (sessionResult.getErrorCode() == 1740) {
//			// SSN and ACCOUNT not match
//			bankingResponse.setErrorCode(sessionResult.getErrorCode());
//			bankingResponse.setErrorString(sessionResult.getErrorDescription());
//			return bankingResponse;
//		} else if (sessionResult.getErrorCode() == 1880) {//Account blocked
//			// SSN and ACCOUNT not match
//			bankingResponse.setErrorCode(sessionResult.getErrorCode());
//			return bankingResponse;
//		}
//		if (sessionResult.getErrorCode() != 0)
//			throw new RuntimeException(sessionResult.getErrorDescription() + " SSN / ACCT SignOn Failed in Vault: "
//					+ bankingEntitlement.getSsn() + " / " + bankingEntitlement.getAccountNumber() + " / " + type);
	}
	
	/**
	 * Validates the Vault Account Service Response
	 * @param response the request with the necessary params for validating the response
	 */
	public List<TVFrontendAccount> validateAccountsResponse(UserSession userSession, MicroServiceResponse<VaultAccountDto> response) {
		boolean vaultAccountSuccess = (MICROSERVICE_RESPONSE_SUCCESS_STATUS == response.getStatus());
		
		if (!vaultAccountSuccess) {
			logger.error(response.getErrors().get(0));
			return new LinkedList<>();
		}
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		logger.info(response.getData().toString() + ": Vault Accounts Request success");
		
		List<VaultAccountDto> dtoList = response.getData();
		List<VaultAccount> accounts = mapper.convertValue( dtoList, new TypeReference<List<VaultAccount>>() { } );
		
		if (accounts == null || accounts.isEmpty()) {
			logger.error("OD VaultAccounts is null or empty");
			return new LinkedList<>();
		}

		return convertVaultAccountToTVFrontend(userSession, accounts);
	}
	
	/**
	 * Converts the Vault Account object to TVFrontendAccount
	 * @param accounts the request with the necessary params for converting the object
	 */
	private List<TVFrontendAccount> convertVaultAccountToTVFrontend(UserSession userSession, List<VaultAccount> accounts) {
		CustomerProfile customerProfile = userSession.getCustomerProfile();
		
		boolean validateEloanApplications = customerProfile.isPendingAccountApplication();
		AccountApplications accountApplications = userSession.getAccountApplications(validateEloanApplications);
		if (accountApplications == null)
			accountApplications = new AccountApplications();
		
		PortalServiceModule portalService = new PortalServiceModule();
		return portalService.createTVFrontendAccountOD(accounts, accountApplications);		
	}
	
	/**
	 * Method to get users accounts from the vault filtered by ProductId.
	 *
	 * @param vaultAccountsRequest
	 *            the request with the necessary params for vault accounts
	 */
	public List<TVFrontendAccount> getAccountsFilteredByProductsId(VaultAccountsRequest vaultAccountsRequest, List<String> lstProducsId) {
		final Logger logger = Logger.getLogger(VaultAccountsFacade.class);
		UserSession userSession = vaultAccountsRequest.getUserSession();
		logger.info("Getting user accounts from the vault");
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_ACCOUNTS);
		if (msProperties.isEnabled()) {
			List<TVFrontendAccount> accounts = consumeVaultAccountsMicroservice(vaultAccountsRequest, msProperties.getEndpoint());
			accounts = getAccountsByProductId(accounts, lstProducsId);
			return accounts;
		}
		
		return userSession.getFilteredAccounts(lstProducsId);
	}
	

	/**
	 * Filter List Account Response Vault by Product Id
	 * @param accounts
	 * @param type
	 * @return List<TVFrontendAccount>
	 */
	private List<TVFrontendAccount> getAccountsByProductId (List<TVFrontendAccount> accounts, List<String> productsId){
		List<TVFrontendAccount> filteredAccountList = new ArrayList<>(1); // filteredAccountList
		
		Set<String> products = new HashSet<>(productsId);

		filteredAccountList = accounts
				  .stream()
				  .filter(account -> products.contains(account.getAccountProductId()))
				  .collect(Collectors.toList());
		
		return filteredAccountList;
	}
}