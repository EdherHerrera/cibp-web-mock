package evertec.cibp.web.facade;

import org.apache.log4j.Logger;

import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;

import evertec.cibp.web.session.UserSession;

/**
 * The Class EnrollmentFacade provides a facade to refactoring existing code and
 * implementing new code for the enrollment operations.
 * Microservices Facade abstract the switch logic from monolithic code and microservice code
 * 
 */
public class EnrollmentFacade {
	
	/**
	 * The constant logger
	 */
	private static final Logger LOGGER = Logger.getLogger(EnrollmentFacade.class);

	/**
	 * The instance
	 */
	private static EnrollmentFacade instance = null;

	/**
	 * Endpoints for account service
	 */
	private static final String DELETE_USER_RESERVATION = "enrollment.delete.user.reservaton";
	
	/**
	 * HTTP status
	 */
	private static final int MICROSERVICE_RESPONSE_SUCCESS_STATUS = 200;
	private static final int MICROSERVICE_RESPONSE_CREATED_STATUS = 201;
	private static final int MICROSERVICE_RESPONSE_NO_CONTENT_STATUS = 204;

	/**
	 * Instantiates a new accounts facade.
	 */
	public EnrollmentFacade() {
	}

	/**
	 * Gets the single instance of AccountsFacade.
	 *
	 * @return single instance of AccountsFacade
	 */
	public static EnrollmentFacade getInstance() {
		if (instance == null) {
			instance = new EnrollmentFacade();
		}
		return instance;
	}
	
	/**
	 * Deletes UsernameReservation from Enrollment Service
	 *
	 * @param userSession, the user session
	 *           
	 */
	@SuppressWarnings("deprecation")
	public boolean deleteUserReservationFromDatabase(UserSession userSession) {
				
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(DELETE_USER_RESERVATION);
		
		if (msProperties.isEnabled()) {
			
			LOGGER.info("MICROSERVICE {microservice} IS ENABLED".replace("{microservice}", "Enrollment"));
			return deleteUserReservationMs(userSession, msProperties.getEndpoint());
			
		} else {
			
			LOGGER.info("MICROSERVICE {microservice} IS DISABLED".replace("{microservice}", "Enrollment"));
			return userSession.getCustomerService().getEnrollService().cancelUsernameReservation(userSession.getUsernameReservation().getUsername());

		}
		
	}
	
	/**
	 * Consumes the deleteUserReservation microservice
	 *
	 * @param userSession, the user session
	 * @param endpoint, microservice endpoint
	 *           
	 */
	private boolean deleteUserReservationMs(UserSession session, String endpoint) {

		LOGGER.info("Consuming microservice: ".concat(endpoint));
		
		MicroServiceResponse response = session.getRestClientResourceOwner().deleteForMicroServiceResponse(endpoint.replace("{username}", session.getUsernameReservation().getUsername().toString()), null);
							
		if (response.getStatus() == MICROSERVICE_RESPONSE_NO_CONTENT_STATUS) {
			
			LOGGER.info("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
			return true;
			
		} else {
			
			LOGGER.error("MICROSERVICE RESPONSE STATUS: ".concat(response.getStatus().toString()).concat(response.getErrors().toString()));
			return false;
			
		}
			
	}

}
