package evertec.cibp.web.facade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.evertec.cibp.api.client.RestClient;
import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;

import evertec.cibp.core.dto.PushTokensDto;
import evertec.cibp.core.models.messaging.push.PushToken;
import evertec.cibp.core.services.requests.PushTokensRequest;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.mapper.PushTokensModelMapperFactory;
import evertec.cibp.web.response.PushTokensResponse;
import evertec.cibp.web.session.UserSession;

/**
 * The Class PushTokensFacade provides a facade to refactoring existing code and
 * implementing new code about push tokens operations.
 * 
 * @author Diego Bocca
 * @since 1.0
 * @version 1.0
 */
public class PushTokensFacade {

	/**
	 * The constant logger
	 */
	private static final Logger logger = Logger.getLogger(PushTokensFacade.class);

	/**
	 * The instance
	 */
	private static PushTokensFacade instance = null;

	/**
	 * Endpoints for push tokens service
	 */
    private static final String PUSH_TOKENS = "pushtokens.tokens";
	
	private static final int MICROSERVICE_RESPONSE_SUCCESS_STATUS = 200;
	private static final int MICROSERVICE_RESPONSE_FAILURE_STATUS = 400;


	/**
	 * Instantiates a new push tokens facade.
	 */
	protected PushTokensFacade() {
	}

	/**
	 * Gets the single instance of PushTokensFacade.
	 *
	 * @return single instance of PushTokensFacade
	 */
	public static PushTokensFacade getInstance() {
		if (instance == null)
			instance = new PushTokensFacade();
		return instance;
	}
	
	/**
	 * Method to get user tokens
	 *
	 * @param vaultAccountsRequest
	 *            the request with the necessary params for vault accounts
	 */
	public PushTokensResponse getTokens(PushTokensRequest pushTokensRequest, UserSession userSession) {
		logger.info("Getting tokens");

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.PUSH_NOTIFICATIONS)) {
			
			MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(PUSH_TOKENS);
			
			if(msProperties.isEnabled()) {
				logger.info("Getting push token(s) through microservice");
				return consumeGetPushTokensMicroservice(pushTokensRequest, userSession, msProperties.getEndpoint());
			}else {
				logger.info("Geting push token(s) through monolithic implementation");
				return consumeGetPushTokensMonolithic(pushTokensRequest, userSession);
			}
			
		} 
		logger.info("Push token functionality disabled");
		return new PushTokensResponse();
		
	}
	
	/**
	 * Method to update user tokens
	 * 
	 * @param pushTokensRequest The request
	 * @param userSession The user Session
	 */
	public PushTokensResponse putTokens(List<PushTokensRequest> pushTokensRequest, UserSession userSession) {
		
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.PUSH_NOTIFICATIONS)) {

			MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(PUSH_TOKENS);
			if (msProperties.isEnabled()) {
				logger.info("Updating push token(s) through microservice");
				return consumePutPushTokensMicroservice(pushTokensRequest, userSession, msProperties.getEndpoint());
				
			} else {
				logger.info("Updating push token(s) through monolithic implementation");
				return consumeUpdatePushTokensMonolithic(pushTokensRequest, userSession);
			}
		}
		logger.info("Push token functionality disabled");
		return new PushTokensResponse();
	}
		
	/**
	 * Consumes the push token microservice
	 *
	 * @param pushTokensRequest
	 *            the request with the necessary params for push tokens
	 */
	private PushTokensResponse consumeGetPushTokensMicroservice(PushTokensRequest pushTokensRequest, UserSession userSession, String endpoint) {

		RestClient restClientAuthentication;
		if (userSession != null && userSession.getRestClientResourceOwner() != null) {
			restClientAuthentication = userSession.getRestClientResourceOwner();
		} else {
			restClientAuthentication = new RestClient();
		}
		//Parameters for the vault microservice
		Map<String,Object> params = new HashMap<>();
		params.put("profileId", pushTokensRequest.getProfileId());
		params.put("token", pushTokensRequest.getToken());
		params.put("fromLastSignon", pushTokensRequest.getFromLastSignon());
		params.put("toLastSignon", pushTokensRequest.getLastSignonDate());
		if (pushTokensRequest.getStatus() != null) {
			params.put("status", pushTokensRequest.getStatus().toString());			
		}
		
		// Microservice call
		MicroServiceResponse<PushTokensDto> response = restClientAuthentication.getForMicroServiceResponse(endpoint, params);
		logger.info(response.getStatus() + ": response status");
		
		return new PushTokensResponse(response, MICROSERVICE_RESPONSE_SUCCESS_STATUS);
		
	}
	
	private PushTokensResponse consumeGetPushTokensMonolithic(PushTokensRequest pushTokensRequest, UserSession userSession) {
		
		List<PushToken> pushTokens =  userSession.getCustomerService().getPushTokens(pushTokensRequest);
		List<PushTokensDto> data = new ArrayList<>();
		pushTokens.stream().forEach(t -> data.addAll(Arrays.asList(PushTokensModelMapperFactory.getMapper().map(t, PushTokensDto.class))));
		
		MicroServiceResponse<PushTokensDto> response = new MicroServiceResponse<>();
		response.setData(data);
		response.setStatus(MICROSERVICE_RESPONSE_SUCCESS_STATUS);

		return new PushTokensResponse(response, MICROSERVICE_RESPONSE_SUCCESS_STATUS);
	}
	
	private PushTokensResponse consumeUpdatePushTokensMonolithic(List<PushTokensRequest> pushTokensRequest, UserSession userSession) {
		
		boolean isSuccess =  userSession.getCustomerService().updatePushToken(pushTokensRequest);

		MicroServiceResponse<PushTokensDto> response = new MicroServiceResponse<>();
		response.setData(new ArrayList<>());
		response.setErrors(new ArrayList<>());
		
		if(isSuccess) {
			response.setStatus(MICROSERVICE_RESPONSE_SUCCESS_STATUS);
		}else {
			response.setStatus(MICROSERVICE_RESPONSE_FAILURE_STATUS);
			// The code validates that the error list is not empty
			response.getErrors().add(new com.evertec.cibp.api.client.model.Error());
		}

		return new PushTokensResponse(response, response.getStatus());
	}
	

	/**
	 * Consumes the push token microservice
	 *
	 * @param pushTokensRequest
	 *            the request with the necessary params for push tokens
	 */
	private PushTokensResponse consumePutPushTokensMicroservice(List<PushTokensRequest> pushTokensRequest, UserSession userSession, String endpoint) {
		
		RestClient restClientAuthentication;
		if (userSession != null && userSession.getRestClientResourceOwner() != null) {
			restClientAuthentication = userSession.getRestClientResourceOwner();
		}else {
			restClientAuthentication = new RestClient();
		}
		
		// Microservice call
		MicroServiceResponse<PushTokensDto> response = restClientAuthentication.putForMicroServiceResponse(endpoint, pushTokensRequest, null);
		logger.info(response.getStatus() + ": response status");
		
		return new PushTokensResponse(response, MICROSERVICE_RESPONSE_SUCCESS_STATUS);
		
	}

}
