package evertec.cibp.web.facade;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.modelmapper.TypeToken;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.WebUtils;

import com.eloan.www.core.deposit.nerds.services.types.ApplicationStatusType;
import com.evertec.cibp.api.client.RestClient;
import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;

import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.models.account.ATHCardTripNote;
import evertec.cibp.core.models.account.DepositProduct;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.accountopening.AccountApplication;
import evertec.cibp.core.models.accountopening.AccountApplications;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.DisposableIdentifier;
import evertec.cibp.web.form.dto.AccountApplicationDto;
import evertec.cibp.web.form.dto.AccountPreferencesDto;
import evertec.cibp.web.form.dto.StatementCopyDto;
import evertec.cibp.web.form.dto.CheckCopyDto;
import evertec.cibp.web.form.dto.DepositProductDto;
import evertec.cibp.web.mapper.AccountModelMapperFactory;
import evertec.cibp.web.request.AccountApplicationRequest;
import evertec.cibp.web.request.CheckCopyRequest;
import evertec.cibp.web.request.ModifyAccountPreferenceRequest;
import evertec.cibp.web.request.PendingApplicationRequest;
import evertec.cibp.web.request.UpdateAccountPreferencesRequest;
import evertec.cibp.web.session.UserSession;
import evertec.dp.service.affiliateAccountInquiry.AffiliatedAcctInqResponse_Type;
import evertec.dp.models.xsd.photocopy.PhotocopyResponse_Type;

/**
 * The Class AccountsFacade provides a facade to refactoring existing code and
 * implementing new code for the account operations.
 * Microservices Facade abstract the switch logic from monolithic code and microservice code
 * 
 */
public class AccountsFacade {

	/**
	 * The constant logger
	 */
	private static final Logger logger = Logger.getLogger(AccountsFacade.class);

	/**
	 * The instance
	 */
	private static AccountsFacade instance = null;

	/**
	 * Endpoints for account service
	 */
	private static final String PUT_ACCT_PREFERENCE_PROP_ID = "accounts.put.preferences";
	private static final String GET_ACCT_APPS_PROP_ID = "accounts.get.applications";
	private static final String GET_ACCT_STATEMENT_PROP_ID = "accounts.get.statement";
	private static final String GET_ACCT_CHECK_COPY_PROP_ID = "accounts.get.check.copy";
	private static final String GET_ACCT_USAVE_INFO_PROP_ID = "accounts.get.usave";
	private static final String POST_ACCT_CARD_TRIP_NOTE_PROP_ID = "accounts.post.card.trip.notes";
	private static final String GET_HARLAND_URL_BY_ACCOUNT = "accounts.harland.url.account";
	private static final String DELETE_ACCOUNT_PENDING_APPLICATION = "accounts.pending.application";
	private static final String GET_DEPOSIT_PRODUCT_TYPE = "accounts.get.deposit.product.type";
	
	/**
	 * HTTP status
	 */
	private static final int MICROSERVICE_RESPONSE_SUCCESS_STATUS = 200;
	private static final int MICROSERVICE_RESPONSE_CREATED_STATUS = 201;
	private static final int MICROSERVICE_RESPONSE_NO_CONTENT_STATUS = 204;

	/** Error message for null customer profile*/
	private static final String CUSTOMER_PROFILE_IS_NULL = "Customer Profile is null, this should not happen.";

	/**
	 * Instantiates a new accounts facade.
	 */
	public AccountsFacade() {
	}

	/**
	 * Gets the single instance of AccountsFacade.
	 *
	 * @return single instance of AccountsFacade
	 */
	public static AccountsFacade getInstance() {
		if (instance == null) {
			instance = new AccountsFacade();
		}
		return instance;
	}

	/**
	 * Updates the account preferences (CUSTOMER_ACCOUNT_PREFERENCE)
	 *
	 * @param modifyAccountPreferenceRequest
	 *            the request with the information to be updated
	 */
	@SuppressWarnings("deprecation")
	public void modifyAccountPreference(ModifyAccountPreferenceRequest modifyAccountPreferenceRequest) {
		parameterValidation(modifyAccountPreferenceRequest);

		CustomerServices customerServices = modifyAccountPreferenceRequest.getUserSession().getCustomerService();
		CustomerProfile customerProfile = modifyAccountPreferenceRequest.getCustomerProfile();
		TVFrontendAccount account = modifyAccountPreferenceRequest.getAccount();
		String nickname = modifyAccountPreferenceRequest.getNickname();
		boolean hide = modifyAccountPreferenceRequest.isHide();

		logger.info("Updating account preferences for customer profile: " + customerProfile.getProfileId()
				+ " and account id: " + account.getAccountKey());
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(PUT_ACCT_PREFERENCE_PROP_ID);

		if (msProperties.isEnabled()) {
			consumeCustomerAccountPreferencesMicroservice(modifyAccountPreferenceRequest, msProperties.getEndpoint());
			
		} else {
			customerServices.modifyAccountPreference(customerProfile, account, nickname, hide);
		}
	}

	/**
	 * Updates the account visibility (CUSTOMER_ACCOUNT_PREFERENCE)
	 *
	 * @param modifyAccountPreferenceRequest
	 *            the request with the information to be updated
	 */
	@SuppressWarnings("deprecation")
	public void modifyAccountVisibility(ModifyAccountPreferenceRequest modifyAccountPreferenceRequest) {
		parameterValidation(modifyAccountPreferenceRequest);

		CustomerServices customerServices = modifyAccountPreferenceRequest.getUserSession().getCustomerService();
		CustomerProfile customerProfile = modifyAccountPreferenceRequest.getCustomerProfile();
		TVFrontendAccount account = modifyAccountPreferenceRequest.getAccount();
		boolean hide = modifyAccountPreferenceRequest.isHide();

		logger.info("Updating account visibility for customer profile: " + customerProfile.getProfileId()
				+ " and account id: " + account.getAccountKey());

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(PUT_ACCT_PREFERENCE_PROP_ID);
		if (msProperties.isEnabled()) {
			consumeCustomerAccountPreferencesMicroservice(modifyAccountPreferenceRequest, msProperties.getEndpoint());
		} else {
			customerServices.modifyAccountVisibility(customerProfile, account, hide);
		}
	}

	/**
	 * Updates the account nickname (CUSTOMER_ACCOUNT_PREFERENCE)
	 *
	 * @param modifyAccountPreferenceRequest
	 *            the request with the information to be updated
	 */
	@SuppressWarnings("deprecation")
	public void modifyAccountNickname(ModifyAccountPreferenceRequest modifyAccountPreferenceRequest) {
		parameterValidation(modifyAccountPreferenceRequest);

		CustomerServices customerServices = modifyAccountPreferenceRequest.getUserSession().getCustomerService();
		CustomerProfile customerProfile = modifyAccountPreferenceRequest.getCustomerProfile();
		TVFrontendAccount account = modifyAccountPreferenceRequest.getAccount();
		String nickname = modifyAccountPreferenceRequest.getNickname();

		logger.info("Updating account nickname for customer profile: " + customerProfile.getProfileId()
				+ " and account id: " + account.getAccountKey());

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(PUT_ACCT_PREFERENCE_PROP_ID);
		if (msProperties.isEnabled()) {
			consumeCustomerAccountPreferencesMicroservice(modifyAccountPreferenceRequest, msProperties.getEndpoint());
		} else {
			customerServices.modifyAccountNickname(customerProfile, account, nickname);
		}
	}
	
	/**
	 * Find the account applications (PENDING_ACCOUNT_APP)
	 *
	 * @param pendingApplicationRequest
	 *            the request with the information that will be used.
	 */
	@SuppressWarnings("deprecation")
	public AccountApplications getPendingApplications(PendingApplicationRequest pendingApplicationRequest) {

		boolean validateLevel1Applications = pendingApplicationRequest.isValidateEloanApplications();
		UserSession userSession = pendingApplicationRequest.getUserSession();

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_ACCT_APPS_PROP_ID);
		if (msProperties.isEnabled()) {
			
			//Validating parameters
			validationOfParameters(pendingApplicationRequest);
			
			if (pendingApplicationRequest.getCustomerProfile() != null) {
				return consumeGetPendingApplicationsMicroservice(
						pendingApplicationRequest.getCustomerProfile().getUsername(),
						userSession, msProperties.getEndpoint());
			} else {
				return consumeGetPendingApplicationsMicroservice(pendingApplicationRequest.getUserName(),
						 userSession, msProperties.getEndpoint());
			}

		} else if (pendingApplicationRequest.getCustomerProfile() != null) {
			return userSession.getCustomerService().getPendingApplications(userSession.getCustomerProfile(),
					validateLevel1Applications);
		} else {
			return userSession.getCustomerService().getPendingApplications(
					userSession.getUsernameReservation().getUsername(), validateLevel1Applications);
		}

	}
	
	
	/**
	 * Gets a Harland url by account from the CustomerService or microservice 
	 * 
	 * @param customerProfile
	 * @param account
	 * @param returnURL
	 * @param bankId
	 * @param userSession
	 * 
	 * */
	@SuppressWarnings("deprecation")
	public String getHarlandUrlByAccount(CustomerProfile customerProfile, AbstractAccount account, String returnURL, UserSession userSession) {

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_HARLAND_URL_BY_ACCOUNT);
		String url = null;
		
		if (msProperties.isEnabled()) {
			
			url = this.consumeGetHarlandUrlByAccount(customerProfile,account,returnURL,userSession.getRestClientResourceOwner(),msProperties.getEndpoint());

		} else {
			url = userSession.getCustomerService().getHarlandUrlByAccount(customerProfile,account,returnURL);
		}
		
		return url;

	}
	
	/**
	 * Gets a Harland url by account from the microservice
	 * 
	 * @param customerProfile
	 * @param account
	 * @param returnURL
	 * @param client The rest client to make the call
	 * @param endpoint The microservice endpoint
	 * */
	private String consumeGetHarlandUrlByAccount(CustomerProfile customerProfile, AbstractAccount account, String returnURL, RestClient client, String endpoint) {
	
		
		logger.info("Consuming microservice");
		
		TVFrontendAccount checkAccount = null;
		String harlandURL = null;
		
		if (account instanceof TVFrontendAccount) {
			checkAccount = (TVFrontendAccount) account;
		} else  {
			throw new ProgrammingError("Account Type is not supported to process this request");
		}
			
		//Adding the parameter.
				Map<String,Object> param = new HashMap<>();
				param.put("productId", checkAccount.getProductDefinition().getProductId().trim());
				param.put("firstName", customerProfile.getFirstName());
				param.put("lastName", customerProfile.getLastName());
				param.put("returnUrl", returnURL);
		
		MicroServiceResponse<String> response = client.getForMicroServiceResponse(endpoint.replace("{accountNumber}", checkAccount.getAccountNumber()), param);
		
		if(response.getStatus() == MICROSERVICE_RESPONSE_SUCCESS_STATUS) {
			logger.info("get harland url by account success response " + response);
			harlandURL=  response.getData().get(0);
		}else {
			
			harlandURL= "?CLIENT_ID" + "=" + "&" + "REQUEST_TYPE=encrypted&DATA=";
		}
		
	
		
		
		return harlandURL;
	}
	
	/**
	 * Gets a Statement Copy from the CustomerService or microservice 
	 * 
	 * @param accountNumber
	 * @param startDate
	 * @param endDate
	 * @param bankId
	 * @param userSession
	 * 
	 * */
	@SuppressWarnings("deprecation")
	public PhotocopyResponse_Type getStatementCopy(String accountNumber, Calendar startDate, Calendar endDate, String bankId, UserSession userSession) {
		logger.info("Get statement copy with params accountNumber="+accountNumber+" bankId"+bankId+" startDate="+startDate+" endDate="+endDate);
		CustomerServices customerService = userSession.getCustomerService();
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_ACCT_STATEMENT_PROP_ID);
		if(msProperties.isEnabled()) {
			return consumeGetStatementCopy(accountNumber, startDate, endDate, bankId, userSession.getRestClientResourceOwner(), msProperties.getEndpoint());
		}

		return customerService.getStatementCopy(accountNumber, startDate, endDate, bankId);
	}
	
	/**
	 * Gets a check copy
	 *
	 * @param request, the HTTP request
	 * @param accountNumber, the Account Number
	 * @param amt, the check amount
	 * @param checkNumber, the check number
	 * @param checkDate, the check date
	 * @param bankId, the bank ID
	 * 
	 */
	@SuppressWarnings("deprecation")
	public PhotocopyResponse_Type getCheckCopy(HttpServletRequest request, 
											   String accountNumber, 
											   BigDecimal amt, 
											   String checkNumber,
											   Calendar checkDate, 
											   String bankId) {
		
		PhotocopyResponse_Type response;
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
	
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_ACCT_CHECK_COPY_PROP_ID);
		if (msProperties.isEnabled()) {
			response = consumeGetCheckCopyMs(new CheckCopyRequest(accountNumber, amt, checkNumber, checkDate, bankId), userSession, msProperties.getEndpoint());
			
		} else {
			try {
				response = userSession.getCustomerService().getCheckCopy(accountNumber, amt, checkNumber, checkDate, bankId);
			
			} catch(Exception e) {
				logger.error("Error while calling getCheckCopy from the monolithic implementation. ",e);
				response = new PhotocopyResponse_Type("-1", "Datapower service may not be working properly", "");
			
			}
		}
		
		return response;
	}
	
	/**
	 * get usave information microservice
	 *
	 * @param accountNumber
	 * @param userSession
	 */
	@SuppressWarnings("deprecation")
	public AffiliatedAcctInqResponse_Type getUSaveInformation(String accountNumber, UserSession userSession) {
		AffiliatedAcctInqResponse_Type acctInqResponse_Type = null;
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_ACCT_USAVE_INFO_PROP_ID);
		
		if (msProperties.isEnabled()) {
			acctInqResponse_Type = consumeGetUSaveInformation(accountNumber, userSession.getRestClientResourceOwner(), msProperties.getEndpoint());
		}else {
			acctInqResponse_Type = userSession.getCustomerService().getUSaveInformation(accountNumber);
		}
		return acctInqResponse_Type;
	}
	
	/**
	 * Adds the ATHCardTripNotes list
	 *
	 * @param tripNotes, the list of notes
	 * @param request, the HTTP Request
	 *           
	 */
	@SuppressWarnings("deprecation")
	public void addATHCardTripNotes(List<ATHCardTripNote> tripNotes, HttpServletRequest request) {
		
		//Building user session object
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(POST_ACCT_CARD_TRIP_NOTE_PROP_ID);
		if (msProperties.isEnabled()) {
			consumeAddATHCardTripNotesMs(tripNotes, userSession, msProperties.getEndpoint());
			
		} else {
			userSession.getCustomerService().addATHCardTripNotes(tripNotes);
			
		}
	}

	/**
	 * Validates the input request for account preferences modification
	 * 
	 * @param modifyAccountPreferenceRequest
	 *            the request to be verified
	 */
	private void parameterValidation(ModifyAccountPreferenceRequest modifyAccountPreferenceRequest) {
		if (modifyAccountPreferenceRequest == null
				|| modifyAccountPreferenceRequest.getUserSession().getCustomerService() == null
				|| modifyAccountPreferenceRequest.getCustomerProfile() == null
				|| modifyAccountPreferenceRequest.getAccount() == null
				|| !(modifyAccountPreferenceRequest.getAccount() instanceof TVFrontendAccount)) {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Sets the account preferences to the selected account
	 * 
	 * @param account
	 *            the account selected by user
	 * @param nickname
	 *            the nickname selected by user
	 * @param hide
	 *            the value selected by user
	 */
	private void setAccountPreferences(TVFrontendAccount account, String nickname, boolean hide) {
		CustomerAccountPreferences customerAccountPreferences = new CustomerAccountPreferences();
		customerAccountPreferences.setHide(hide);

		if (nickname != null) {
			customerAccountPreferences.setNickname(nickname);
			if (account.getCustomerAccountPreferences() == null) {
				customerAccountPreferences.setDepositLimit("");
				customerAccountPreferences.setRdcStatus("");
			}
		} else {
			if (account.getCustomerAccountPreferences() != null) {
				customerAccountPreferences.setNickname(account.getNickname());
			}
		}

		account.setCustomerAccountPreferences(customerAccountPreferences);
	}
	
	

	/**
	 * Get the Account Applications by an Account Application List
	 * 
	 * @param acList
	 *            the account application list
	 */
	private AccountApplications getAccountApplicationsByAccountApplicationList(List<AccountApplication> acList) {

		List<AccountApplication> pendingApps = new LinkedList<>();
		List<AccountApplication> completeApps = new LinkedList<>();

		DisposableIdentifier id = new DisposableIdentifier();

		for (AccountApplication applicationType : acList) {

			applicationType.setFrontEndId(id.getNextDisposableId());
			switch (applicationType.getStatus()) {
			case PENDING:
				applicationType.setIncomplete(!applicationType.getStatus().equals(ApplicationStatusType._processing));
				pendingApps.add(applicationType);
				break;
			case IN_PROCESS:
				applicationType.setIncomplete(false);
				pendingApps.add(applicationType);
				break;
			case COMPLETE:
				applicationType.setIncomplete(false);
				completeApps.add(applicationType);
				break;
			default:
				break;
			}
		}

		return new AccountApplications(pendingApps, completeApps);
	}
	
	/**
	 * Validates the input request for pending applications
	 * 
	 * @param pendingApplicationRequest
	 *            the request to be verified
	 */
	private void validationOfParameters(PendingApplicationRequest pendingApplicationRequest) {
		if (pendingApplicationRequest == null ||
				( pendingApplicationRequest.getCustomerProfile() == null
						&& pendingApplicationRequest.getUserName().isEmpty())) {
			throw new ProgrammingError(CUSTOMER_PROFILE_IS_NULL);
		}
	}
	
	/**
	 * Consumes the update account preferences microservice
	 *
	 * @param modifyAccountPreferenceRequest
	 *            the request with the information to be updated
	 */
	private void consumeCustomerAccountPreferencesMicroservice(ModifyAccountPreferenceRequest modifyAccountPreferenceRequest, String endpoint) {
		UserSession userSession = modifyAccountPreferenceRequest.getUserSession();
		CustomerProfile customerProfile = modifyAccountPreferenceRequest.getCustomerProfile();
		TVFrontendAccount account = modifyAccountPreferenceRequest.getAccount();
		String nickname = modifyAccountPreferenceRequest.getNickname();
		boolean hide = modifyAccountPreferenceRequest.isHide();

		// Setting account preferences
		setAccountPreferences(account, nickname, hide);

		// Setting microservice request
		UpdateAccountPreferencesRequest request = new UpdateAccountPreferencesRequest();
		request.setCustomerProfileId(customerProfile.getProfileId());
		request.setCustomerAccountKey(account.getAccountKey());
		request.setAccountData(account.getCustomerAccountPreferences().getAttributeData());

		// Microservice consuming
		MicroServiceResponse<AccountPreferencesDto> response = userSession.getRestClientResourceOwner()
				.putForMicroServiceResponse(endpoint, request, null);
		
		boolean accountPreferencesUpdated = (MICROSERVICE_RESPONSE_SUCCESS_STATUS == response.getStatus());

		if (accountPreferencesUpdated) {
			logger.info(response.getData().toString() + ": Account preferences updated succesfully.");
		} else {
			logger.info(response.getData().toString() + ": Account preferences were not updated.");
		}

	}
	
	/**
	 * Gets a Statement Copy from the microservice
	 * 
	 * @param accountNumber The account number to get the statement
	 * @param startDate The start date
	 * @param endDate The end date
	 * @param bankId The bank ID
	 * @param client The rest client to make the call
	 * @param endpoint The microservice endpoint
	 * */
	private PhotocopyResponse_Type consumeGetStatementCopy(String accountNumber, Calendar startDate, Calendar endDate, String bankId, RestClient client, String endpoint) {
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		
		StatementCopyDto dto = new StatementCopyDto(accountNumber, bankId, date.format(startDate.getTime()), date.format(endDate.getTime()));
		
		logger.info("Statement copy request "+ dto);
		
		MicroServiceResponse<PhotocopyResponse_Type> response = client.postForMicroServiceResponse(endpoint, dto, null);
		
		if(response.getStatus() == MICROSERVICE_RESPONSE_SUCCESS_STATUS) {
			logger.info("statement copy success response " + response);
			return response.getData().get(0);
		}
		
		logger.info("statement copy error response " + response);
		
		PhotocopyResponse_Type fail = new PhotocopyResponse_Type();
		fail.setStatusCode("-1");
		
		return fail;
	}

	/**
	 * Consumes the getPendingApplications microservice
	 *
	 * @param userName
	 * @param validateLevel1Applications
	 * @param userSession
	 *           
	 */
	private AccountApplications consumeGetPendingApplicationsMicroservice(String userName, UserSession userSession, String endpoint) {

		// Setting microservice
		AccountApplicationRequest applicationRequest = new AccountApplicationRequest(userName);
		AccountApplications accountApplications = null;

		// Microservice consuming
		MicroServiceResponse<AccountApplicationDto> response = userSession.getRestClientResourceOwner()
				.postForMicroServiceResponse(endpoint, applicationRequest, null);


		if (MICROSERVICE_RESPONSE_SUCCESS_STATUS == response.getStatus()) {

			List<AccountApplication> acList = AccountModelMapperFactory.getMapper().map(response.getData(),
					new TypeToken<ArrayList<AccountApplication>>() {
					}.getType());

			accountApplications = this.getAccountApplicationsByAccountApplicationList(acList);

		}

		return accountApplications;

	}

	/**
	 * Consumes the get usave information microservice
	 *
	 * @param accountNumber
	 * @param restClient
	 */
	private AffiliatedAcctInqResponse_Type consumeGetUSaveInformation(String accountNumber, RestClient restClient, String endpoint) {
		
		AffiliatedAcctInqResponse_Type affiliatedAcctInqResponse_Type = null;	
		if (accountNumber == null)
			throw new IllegalArgumentException("AccountNumber cannot be null");
		
		// Setting parameter in URI
	    String requestURI = endpoint.replace("{accountNumber}", accountNumber );
		
		// Microservice consuming
		MicroServiceResponse<AffiliatedAcctInqResponse_Type> response = restClient.getForMicroServiceResponse(requestURI, null);
		
		if (response.getStatus() == MICROSERVICE_RESPONSE_SUCCESS_STATUS) {
	    	logger.info("getUsaveInformation was successfully");
	    	affiliatedAcctInqResponse_Type = AccountModelMapperFactory.getMapper().map(response.getData().get(0),
					new TypeToken<AffiliatedAcctInqResponse_Type>() {
					}.getType());

		}else {
			logger.error("The microservice for "+requestURI + " http status was a "+response.getStatus());
		}
		return affiliatedAcctInqResponse_Type;
	}

	/**
	 * Consumes the addATHCardTripNotes microservice
	 *
	 * @param tripNotes, the list of notes
	 * @param session, the user session
	 *           
	 */
	private boolean consumeAddATHCardTripNotesMs(List<ATHCardTripNote> tripNotes, UserSession session, String endpoint) {
			
		logger.info("Consuming microservice");
		boolean isTripNotePosted = false;
		MicroServiceResponse<?> response = session.getRestClientResourceOwner().postForMicroServiceResponse(endpoint, tripNotes, null);
		if(response == null) {
			logger.error("After calling microservice, response is null.");
		}else {
			logger.info("MICROSERVICE RESPONSE STATUS: " + response.getStatus());		
			if (response.getStatus() == MICROSERVICE_RESPONSE_CREATED_STATUS) {
				logger.info("ATH Card Trip Notes has been saved");
				isTripNotePosted = true;
			} else {
				logger.info(response.getErrors().toString());
			}
		}
		return isTripNotePosted;
	}
	
	/**
	 * Consumes the getCheckCopy microservice
	 *
	 * @param request, the Check copy request
	 * @param session, the User session
	 *  
	 */
	private PhotocopyResponse_Type consumeGetCheckCopyMs(CheckCopyRequest request, UserSession session, String endpoint) {
					
		// Microservice call
		MicroServiceResponse<CheckCopyDto> response = session.getRestClientResourceOwner().postForMicroServiceResponse(endpoint, request, null);
						
		if (response.getStatus() == MICROSERVICE_RESPONSE_SUCCESS_STATUS) {
			
			CheckCopyDto checkCopyDto = AccountModelMapperFactory.getMapper().map(
					response.getData().get(0), new TypeToken<CheckCopyDto>() {}.getType());
			
			return checkCopyDto.getPhotocopyResponse_type();
			
		} else {
			return new PhotocopyResponse_Type("-1",response.getErrors().toString(),"");
			
		}
	}
	
	/**
	 * Cancels Pending Applications
	 * @param session, The UserSession that contains the cancelPendingApplication method
	 * @param applicationId, The application Id
	 * @return boolean, indicates whether the record was removed or not
	 * */
	@SuppressWarnings("deprecation")
	public boolean cancelPendingApplication(UserSession session, String applicationId) {
		
		final String username = session.getUsername();
		
		boolean removed = false;
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(DELETE_ACCOUNT_PENDING_APPLICATION);
		if(!msProperties.isEnabled()) {
			logger.info("enpoint {endpoint} is disabled using legacy service".replace("{endpoint}", msProperties.getEndpoint()));
			//removed = session.getCustomerService().cancelPendingApplication(username, applicationId);
		} else { 
			logger.info("enpoint {endpoint} is enabled using microservice".replace("{endpoint}", msProperties.getEndpoint()));
			removed = consumeCancelPendingApplication(session.getRestClientResourceOwner(), username,
					applicationId, msProperties.getEndpoint());
		}
		return removed;
	}
	
	/**
	 * Consume cancel pending application from microservice
	 * @param client, The RestClient to request the endpoint
	 * @param username, The current username
	 * @param applicationId, The application Id
	 * @param endpoint, The URL to perform the request
	 * */
	private boolean consumeCancelPendingApplication(RestClient client, String username, String applicationId, String endpoint) {
		MicroServiceResponse<?> response = client.deleteForMicroServiceResponse(endpoint.replace("{applicationId}", applicationId), null);
		logger.info("executing "+endpoint+" endpoint");
		return response.getStatus() == MICROSERVICE_RESPONSE_NO_CONTENT_STATUS;
	}
	
	/**
	 * Get DepositProduct Type
	 *
	 * @param bankId, the bank id
	 * @param productId, the product id
	 * @param suffix, the search suffix
	 * 
	 * @return DepositProduct object
	 *           
	 */
	@SuppressWarnings("deprecation")
	public DepositProduct getDepositProductType(String bankId, String productId, String suffix) {
		
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest(), "userSession");
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_DEPOSIT_PRODUCT_TYPE);
		
		if (msProperties.isEnabled()) {
			
			logger.info("MICROSERVICE {microservice} IS ENABLED".replace("{microservice}", "Accounts"));
			return consumeGetDepositProductTypeMs(bankId, productId, suffix, userSession, msProperties.getEndpoint());
			
		} else {
			
			logger.info("MICROSERVICE {microservice} IS DISABLED".replace("{microservice}", "Accounts"));
			return userSession.getCustomerService().getDepositProductType(bankId, productId, suffix);

		}
		
	}
	
	/**
	 * Consumes the getDepositProductType microservice
	 *
	 * @param bankId, the bank id
	 * @param productId, the product id
	 * @param suffix, the search suffix
	 * @param session, the user session
	 * @param endpoint, microservice endpoint
	 * 
	 * @return DepositProduct object
	 *           
	 */
	private DepositProduct consumeGetDepositProductTypeMs(String bankId, String productId, String suffix, UserSession session, String endpoint) {
							
		//Adding parameters.
		Map<String,Object> params = new HashMap<>();
		params.put("bankId", bankId);
		params.put("productId", productId);
		params.put("suffix", suffix);
		
		logger.info("Consuming microservice: ".concat(endpoint));
		MicroServiceResponse<DepositProductDto> response = session.getRestClientResourceOwner().getForMicroServiceResponse(endpoint, params);
							
		if (response.getStatus() == MICROSERVICE_RESPONSE_SUCCESS_STATUS) {
			
			logger.info("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
			
			DepositProductDto depositProductDto = AccountModelMapperFactory.getMapper().map(
					response.getData().get(0), new TypeToken<DepositProductDto>() {}.getType());
			return depositProductDto.getDepositProduct();
			
		} else {
			
			logger.error("MICROSERVICE RESPONSE STATUS: ".concat(response.getStatus().toString()).concat(response.getErrors().toString()));
			return null;
			
		}
			
	}
	
}
