package evertec.cibp.web.facade;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.modelmapper.TypeToken;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.WebUtils;

import com.evertec.cibp.api.client.RestClient;
import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;

import edu.emory.mathcs.backport.java.util.Collections;
import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.models.customer.CustomerPayeePreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.GlobalPayeePreferences;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.PaymentHistoryResponse;

import evertec.cibp.vs.models.requests.CustomerPayeeListActionRequest;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.beans.accounts.AccountExt;
import evertec.cibp.vs.beans.payments.RecurringPayment;
import evertec.cibp.vs.models.requests.ModifyPaymentActionRequest;
import evertec.cibp.vs.models.requests.PaymentActionRequest;
import evertec.cibp.vs.models.results.CustomerPayeeListActionResult;
import evertec.cibp.vs.models.results.ModifyPaymentActionResult;
import evertec.cibp.vs.models.results.PaymentActionResult;
import evertec.cibp.web.form.dto.GlobalPayeePreferencesDto;
import evertec.cibp.web.form.dto.CommonPayeeTreeDto;
import evertec.cibp.web.form.dto.CustomerPayeeListActionResultDto;
import evertec.cibp.web.form.dto.ModifyPaymentActionResultDto;
import evertec.cibp.web.mapper.PaymentsModelMapperFactory;
import evertec.cibp.web.request.CustomerPayeePreferencesRequest;
import evertec.cibp.web.request.PaymentHistoryRequest;
import evertec.cibp.web.response.RecurringPaymentResponse;
import evertec.cibp.web.session.UserSession;
import evertec.dp.models.xsd.ifx.BankInfo_Type;
import evertec.dp.models.xsd.ifx.PostAddrType;
import evertec.dp.models.xsd.ifx.RefInfo_Type;
import evertec.dp.models.xsd.popular.DepAcctId_Type;

/**
 * Copyright 2018 Evertec, Inc. All Rights Reserved.
 * <p>
 * PaymentsFacade class.
 * <p>
 * The class PaymentsFacade provides a facade to refactoring existing code and
 * implementing new code for payments operations.
 * 
 * Microservices Facade abstract the switch logic from monolithic code and microservice code
 * 
 * @author Jorge De Los Santos
 * @version 1.0
 * @since 1.0
 */
public class PaymentsFacade {
	
	/** The Constant logger. */
	private static final Logger LOGGER = Logger.getLogger( PaymentsFacade.class );
	
	/** The instance. */
	private static PaymentsFacade instance = null;
	
	/**
	 * Microservices endpoints
	 */
	private static final String UPDATE_PAYEE_PREFERENCES = "payments.payee.preferences";
	private static final String GET_PEYMENTS_HISTORY = "payments.get.history";
	private static final String MODIFY_REGULAR_PAYMENTS = "payments.modify.regular";
	private static final String SEND_REGULAR_PAYMENTS = "payments.send.regular";
	private static final String GET_CUSTOMER_PAYEES = "payments.get.payees";
	private static final String REVERSE_PAYMENTS = "payments.reverse";
	private static final String GLOBAL_PAYEE = "payments.global.payees";
	private static final String GET_GLOBAL_PAYEE_PREFERENCES = "payments.get.global.payees.preferences";
	private static final String GET_COMMON_PAYEE_TREE = "payments.get.common.payees.tree";

	
	/**
	 * Successful HTTP Codes
	 */
	private static final int MICROSERVICE_RESPONSE_SUCCESS_CREATE = 201;
	private static final int MICROSERVICE_RESPONSE_SUCCESS_STATUS = 200;
	private static final int MICROSERVICE_RESPONSE_ACEPTED_STATUS = 201;
	private static final int MICROSERVICE_RESPONSE_OK_STATUS = 200;


	
	/** Instantiates a new payment facade. */
	protected PaymentsFacade() {}

	/**
	 * Gets the single instance of PaymentsFacade.
	 * 
	 * @return single instance of PaymentsFacade
	 */
	public static PaymentsFacade getInstance() {
		if ( instance == null ) {
			instance = new PaymentsFacade();
		}
		return instance;
	}
	
	/**
	 * Modify Payee NickName
	 * 
	 * @param customerProfile, The customer profile.
	 * @param bankingSession, The banking session.
	 * @param payee, The TVFrontEndPayee
	 * @param nickName, The nick name
	 * @param userSession, the user session
	 */
	public void modifyPayeeNickName(CustomerProfile customerProfile, BankingSession bankingSession,
			TVFrontEndPayee payee, String nickName, UserSession userSession) {
			
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(UPDATE_PAYEE_PREFERENCES);

		
		if (msProperties.isEnabled()) {
			
			this.validateInputParameters(customerProfile, bankingSession, payee, nickName);
			
			if (payee.getCustomerPayeePreferences() == null) {
				CustomerPayeePreferences pref = new CustomerPayeePreferences();
				pref.setNickname(nickName);
				payee.setCustomerPayeePreferences(pref);
			} else {
				payee.getCustomerPayeePreferences().setNickname(nickName);
			}
			
		
			CustomerPayeePreferencesRequest request = new CustomerPayeePreferencesRequest(payee.getCustomerPayeePreferences().getId(),
					customerProfile.getProfileId(),payee.getPayeeKey(), payee.getCustomerPayeePreferences().getAttributeData());
			
			this.consumeUpdatePayeePreferences(request, msProperties.getEndpoint(),userSession);

		} else {
			
			userSession.getCustomerService().modifyPayeeNickname(customerProfile, bankingSession, payee, nickName);
			
		}
	}
	
	/**
	 * Validate Input Parameters
	 * 
	 * @param customerProfile, The customer profile.
	 * @param bankingSession, The banking session.
	 * @param payee, The TVFrontEndPayee
	 * @param nickName, The nick name
	 */
	private void validateInputParameters(CustomerProfile customerProfile, BankingSession bankingSession,
			TVFrontEndPayee payee, String nickName) {
		
		if (customerProfile == null) {
			throw new ProgrammingError("Customer Profile must be defined.");
		}
			

		if (bankingSession == null || bankingSession.getSessionKey() == null || bankingSession.getPermId() == null) {
			throw new ProgrammingError(
					"No authentication data for customer received. (Entitlement and BankingSession not exist)");
		}

		if (!(payee instanceof TVFrontEndPayee)) {
			throw new ProgrammingError("Account must be defined");
		}
		
		if (nickName == null) {
			throw new ProgrammingError("No nickname received to edit");
		}		
		
	}
	
	/**
	 * Consumes the UpdatePayeePreferences microservice
	 *
	 * @param request, the Customer Payee Preferences Request
	 * @param endpoint, microservice endpoint
	 * @param userSession, the user session 
	 *           
	 */
	private void consumeUpdatePayeePreferences(CustomerPayeePreferencesRequest request, String endpoint, UserSession userSession) {
		
		LOGGER.info("Consuming microservice");
		String requestURI = endpoint.replace("{profileId}", request.getProfileId());
		
		MicroServiceResponse<?> response = userSession.getRestClientResourceOwner().putForMicroServiceResponse(requestURI, request, null);
		
		if (response != null) {
			
			if (response.getStatus() == MICROSERVICE_RESPONSE_SUCCESS_CREATE) {
				
				LOGGER.info("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
			} else {
				LOGGER.error("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
			}
		}else {
			LOGGER.error("Error consuming the service.");
		}
		
		
		
	}
	
	
	
	/**
	 * Find the payments history
	 * 
	 * @param request The request with the information that will be used.
	 * @return A PaymentHistoryResponse object than contains a list of payments History
	 */
	@SuppressWarnings("deprecation")
	public PaymentHistoryResponse getPaymentHistory(PaymentHistoryRequest request) {

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_PEYMENTS_HISTORY);
		if (msProperties.isEnabled()) {
			return consumeGetPaymentHistory(request, msProperties.getEndpoint());

		}else {
			PaymentHistoryResponse response = request.getUserSession().getCustomerService().getPaymentHistory(request.getCustomerProfile(), request.getUserSession().getBankingSession(), 
					request.getPaymentFilter(), request.getPaymentSize(), request.getPayeeId(), request.getStartDate());
			return response;
		}
	}
	

	/**
	 * 
	 * Modify regular payments
	 * @param request The ModifyPaymentActionRequest
	 * @param customerService The monolithic services
	 * @return a ModifyPaymentActionResult
	 */
	public ModifyPaymentActionResult modifyRegularPayment(ModifyPaymentActionRequest request, CustomerServices customerService) {

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(MODIFY_REGULAR_PAYMENTS);
		if (msProperties.isEnabled()) {
			
			//TODO: Implementation by Joel Contreras
			return consumeModifyRegularPayment(request,customerService,msProperties.getEndpoint());

		} else {

			if (GenericValidator.isBlankOrNull(request.getPaymentMod().getNewFrequency())) {
				return customerService.getPaymentService().getVaultService().modifyRegularPayment(request);
			} else {
				return customerService.getPaymentService().getVaultService().modifyRecurringPayment(request);
			}
		}
	}

	/**
	 * 
	 * Modify regular payments
	 * @param request The ModifyPaymentActionRequest
	 * @param customerService The monolithic services
	 * @return a ModifyPaymentActionResult
	 */
	private ModifyPaymentActionResult consumeModifyRegularPayment (ModifyPaymentActionRequest request, CustomerServices customerService,String endPoing) {
		
		ModifyPaymentActionResult result = new ModifyPaymentActionResult();
		
        UserSession userSession = (UserSession)
        WebUtils.getSessionAttribute(((ServletRequestAttributes)RequestContextHolder.
        getRequestAttributes()).getRequest(), "userSession");
        
		MicroServiceResponse<ModifyPaymentActionResultDto>  response = userSession.getRestClientResourceOwner().putForMicroServiceResponse(endPoing, request, null) ;
	    
		if (response.getStatus() == MICROSERVICE_RESPONSE_OK_STATUS) {
			LOGGER.info("test good");
			ModifyPaymentActionResultDto dto = PaymentsModelMapperFactory.
					getMapper().
					map( response.getData().get(0), new TypeToken<ModifyPaymentActionResultDto>() {}.getType());
			
			result = PaymentModifyReponseDtoToActionResult(dto);
			
		}else {
			LOGGER.error("test bad");
			result.setErrorCode(1);
		}
		
		return result;
	}
	
	/**
	 * 
	 * PaymentModifyReponseDtoToActionResult
	 * @param dto the type ModifyPaymentActionResultDto
	 * @return a ModifyPaymentActionResult
	 */
	private ModifyPaymentActionResult PaymentModifyReponseDtoToActionResult(ModifyPaymentActionResultDto dto) {
		
		ModifyPaymentActionResult result = new ModifyPaymentActionResult();

		result.setDataInRequest(dto.getDataInRequest());
		result.setEndTime(dto.getEndTime());
		result.setErrorCode(dto.getErrorCode());
		result.setErrorCodeStr(dto.getErrorCodeStr());
		result.setErrorDescription(dto.getErrorDescription());
		result.setIfxRequest(dto.getIfxRequest());
		result.setIfxResponse(dto.getIfxResponse());
		result.setStartTime(dto.getStartTime());
		result.setStatus(dto.getStatus());
		result.setTotalFailed(dto.getTotalFailed());
		result.setTotalSent(dto.getTotalSent());
		result.setTotalSucessfull(dto.getTotalSucessfull());
		result.setRecurrPmt(getReRecurringPaymentFromDto(dto.getRecurrPmt()));
        return result;
	}
	
	/**
	 * 
	 * getReRecurringPaymentFromDto
	 * @param dto the type RecurringPaymentResponse
	 * @return RecurringPayment
	 */
	private RecurringPayment getReRecurringPaymentFromDto(RecurringPaymentResponse dto) {
		RecurringPayment recurring = new RecurringPayment();
		
		recurring.setRemainingInstances(dto.getRemainingInstances());
		recurring.setFinalAmount(dto.getFinalAmount());
		recurring.setFinalProcessingDate(dto.getFinalProcessingDate());
		recurring.setFirstPmDt(dto.getFirstPmDt());
		recurring.setPmtDay1(dto.getPmtDay1());
		recurring.setPmtDay2(dto.getPmtDay2());
		recurring.setAuthPayFlag(dto.isAuthPayFlag());
		recurring.setFixedPmtCurAmt(dto.getFixedPmtCurAmt());


		recurring.setReverseable(dto.isReverseable());
		recurring.setBillerNumber(dto.getBillerNumber());
		recurring.setFrontEndId(dto.getFrontEndId());
		recurring.setVruIdCode(dto.getVruIdCode());
		recurring.setEmployeeName(dto.getEmployeeName());
		recurring.setEmployeeId(dto.getEmployeeId());
		recurring.setRetries(dto.getRetries());
		recurring.setRejectCode(dto.getRejectCode());
		recurring.setStatusCode(dto.getStatusCode());
		recurring.setEffectiveDate(dto.getEffectiveDate());
		recurring.setProcessingDate(dto.getProcessingDate());
		recurring.setAmount(dto.getAmount());
		recurring.setReferenceNumber(dto.getReferenceNumber());
		recurring.setPayeeNickname(dto.getPayeeNickname());
		recurring.setPayeeName(dto.getPayeeName());
		recurring.setPayeeId(dto.getPayeeId());
		recurring.setPaymentId(dto.getPaymentId());
		recurring.setFrequency(dto.getFrequency());

		recurring.setAccountTo(getAccountExtDataPower(dto.getAccountTo()));
		recurring.setAccountFrom(getAccountExtDataPower(dto.getAccountFrom()));

		return recurring;
	}
	
	/**
	 * 
	 * getAccountExtDataPower
	 * @param accountExt
	 * @return AccountExt
	 */
	private static AccountExt getAccountExtDataPower(evertec.cibp.web.response.AccountExt accountExt){


		BankInfo_Type bankInfo_type = new BankInfo_Type();
		PostAddrType postAddrInfo = new PostAddrType();
		RefInfo_Type refInfo_type;


		if (accountExt.getBankInfo() != null) {

			if (accountExt.getBankInfo().getPostAddr() != null){
				postAddrInfo.setStateProv(accountExt.getBankInfo().getPostAddr().getStateProv());
				postAddrInfo.setStartDt(accountExt.getBankInfo().getPostAddr().getStartDt());
				postAddrInfo.setPostalCode(accountExt.getBankInfo().getPostAddr().getPostalCode());
				postAddrInfo.setEndDt(accountExt.getBankInfo().getPostAddr().getEndDt());
				postAddrInfo.setCountry(accountExt.getBankInfo().getPostAddr().getCountry());
				postAddrInfo.setCity(accountExt.getBankInfo().getPostAddr().getAddr1());
				postAddrInfo.setAddrType(accountExt.getBankInfo().getPostAddr().getAddrType());
				postAddrInfo.setAddr4(accountExt.getBankInfo().getPostAddr().getAddr4());
				postAddrInfo.setAddr3(accountExt.getBankInfo().getPostAddr().getAddr3());
				postAddrInfo.setAddr2(accountExt.getBankInfo().getPostAddr().getAddr2());
				postAddrInfo.setAddr1(accountExt.getBankInfo().getPostAddr().getAddr1());

			}



			bankInfo_type.setBankId(accountExt.getBankInfo().getBankId());
			bankInfo_type.setBankIdType(accountExt.getBankInfo().getBankIdType());
			bankInfo_type.setBranchId(accountExt.getBankInfo().getBranchId());
			bankInfo_type.setBranchName(accountExt.getBankInfo().getBranchName());
			bankInfo_type.setCity(accountExt.getBankInfo().getCity());
			bankInfo_type.setCountry(accountExt.getBankInfo().getCountry());
			bankInfo_type.setName(accountExt.getBankInfo().getName());
			bankInfo_type.setPostAddr(null);
			bankInfo_type.setPostalCode(accountExt.getBankInfo().getPostalCode());
			bankInfo_type.setStateProv(accountExt.getBankInfo().getStateProv());


		}

		DepAcctId_Type dpAcct = new DepAcctId_Type();
		dpAcct.setAcctId(accountExt.getAccountNumber());
		dpAcct.setAcctCur(accountExt.getAcctCur());
		dpAcct.setAcctKey(accountExt.getAcctKey());
		dpAcct.setAcctType(accountExt.getAccountType());
		dpAcct.setBankInfo(bankInfo_type);

		AccountExt accountExt1 = new AccountExt(dpAcct);


		return accountExt1;

	}

	
	
	/**
	 * Finds a single globalPayee by its id
	 * 
	 * @param id, globalPayee id
	 * @param userSession, the user Session
	 * 
	 * @return GlobalPayee
	 * */
	@SuppressWarnings({"unchecked", "unused"})
	public GlobalPayee getGlobalPayeeById(Integer id, UserSession userSession) {
		Map<Integer, GlobalPayee> data = getGlobalPayeeById
				(Collections.singletonList(id), userSession);
		return data != null? (GlobalPayee) data.get(id) : null;
	}
	
	
	/**
	 * Finds multiple globalPayees by its id
	 * 
	 * @param ids, List of id's to seek
	 * @param userSession, the user session
	 * @return Map<Integer, GlobalPayee> map of global payees
	 * */
	public Map<Integer, GlobalPayee> getGlobalPayeeById(List<Integer> ids, UserSession userSession) {
		LOGGER.debug("invoking getGlobal Payee By id");
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GLOBAL_PAYEE);
		
		if(msProperties.isEnabled()) {
			return consumeGetGlobalPayeeById(ids, userSession.getRestClientResourceOwner(), msProperties.getEndpoint());
		}
		return userSession.getCustomerService().getGlobalPayeeById(ids);
	}
	
	
	/**
	 * Consume getGlobalPayeeById from MicroService
	 * @param ids, List of id's
	 * @param client, RestClient to perform the call 
	 * */
	private Map<Integer, GlobalPayee> consumeGetGlobalPayeeById(List<Integer> ids, RestClient client, String endpoint) {
		LOGGER.info("Consuming from microservice endpoint {0}".replace("{0}", endpoint));
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", mapToString(ids, ','));
		
		MicroServiceResponse<HashMap<Integer, GlobalPayee>> response = client.getForMicroServiceResponse(endpoint, params);
		
		if(response.getStatus() == MICROSERVICE_RESPONSE_SUCCESS_STATUS)
			return response.getData().get(0);
		
		return null;
	}
	
	/**
	 * maps list of elements to string divided by a given separator
	 * @param list List of elements 
	 * @param separator
	 */
	private String mapToString(@SuppressWarnings("rawtypes") List list, char separator) {
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < list.size(); i++) {
			builder.append(list.get(i));
			if(i < (list.size()-1))
				builder.append(separator);
		}
		return builder.toString();
	}
	
	/**
	 * Send Regular Payments
	 * @param request
	 * @param customerService
	 * @param session
	 * */
	public PaymentActionResult sendRegularPayments(PaymentActionRequest request, CustomerServices customerService, UserSession session) {
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(SEND_REGULAR_PAYMENTS);
		PaymentActionResult regularPaymentResponse = null;
		if (msProperties.isEnabled()) {
			regularPaymentResponse = consumeSendRegularPayment(request, session, msProperties.getEndpoint());
		}else {
			regularPaymentResponse = customerService.getPaymentService().getVaultService().addRegularPayment(request);
		}
		
		return regularPaymentResponse;
	}
	/**
	 * Consumes Send Regular Payments from microservice
	 * @param request
	 * @param client
	 * @param endpoint
	 * */
	private PaymentActionResult consumeSendRegularPayment(PaymentActionRequest request , UserSession session, String endpoint) {
		LOGGER.info("Consuming from microservice endpoint {0}".replace("{0}", endpoint));
		
		MicroServiceResponse<PaymentActionResult>  response = session.getRestClientResourceOwner().postForMicroServiceResponse(endpoint, request, null);
		
		if(response.getStatus() == MICROSERVICE_RESPONSE_SUCCESS_STATUS) {
			LOGGER.info("Everything goes ok, returning response");
			return !response.getData().isEmpty() ? response.getData().get(0) : new PaymentActionResult();
		}
	
		LOGGER.error(StringEscapeUtils.escapeJava("Microservice fails, status :" +response.getStatus()));
		LOGGER.error("Reason : {0}".replace("{0}", response.getErrors().toString()));
		//Just in case of bad gateway
		throw new RuntimeException("Microservice is unable to response");

	}

	/**
	 * Get payees from Payments Service
	 *
	 * @param payeeListRequest, wraps banking session and ssn 
	 * @param customerService, the customer service
	 * 
	 * @return CustomerPayeeListActionResult object
	 *           
	 */
	public CustomerPayeeListActionResult getCustomerPayees(CustomerPayeeListActionRequest payeeListRequest, CustomerServices customerService) {
				
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest(), "userSession");
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_CUSTOMER_PAYEES);
		
		if (msProperties.isEnabled()) {
			
			LOGGER.info("MICROSERVICE {microservice} IS ENABLED".replace("{microservice}", "Payments"));
			return consumeGetCustomerPayeesMs(payeeListRequest.getBankSession(), payeeListRequest.getTaxId(), userSession, msProperties.getEndpoint());
			
		} else {
			
			LOGGER.info("MICROSERVICE {microservice} IS DISABLED".replace("{microservice}", "Payments"));
			return customerService.getPaymentService().getVaultService().getCustomerPayees(payeeListRequest);

		}
		
	}
	
	/**
	 * Consumes the getCustomerPayees microservice
	 *
	 * @param bankingSession, the banking session
	 * @param ssn, SSN for vault service
	 * @param userSession, the user session
	 * @param endpoint, microservice endpoint
	 * 
	 * @return CustomerPayeeListActionResult object
	 *           
	 */
	private CustomerPayeeListActionResult consumeGetCustomerPayeesMs(BankingSession bankingSession, String ssn, UserSession session, String endpoint) {
							
		//Adding parameters.
		Map<String,Object> params = new HashMap<>();
		params.put("permId", bankingSession.getPermId().toString());
		params.put("sessionKey", bankingSession.getSessionKey().toString());
		params.put("ssn", ssn);
		
		LOGGER.info("Consuming microservice: ".concat(endpoint));
		
		MicroServiceResponse<CustomerPayeeListActionResultDto> response = session.getRestClientResourceOwner().getForMicroServiceResponse(endpoint, params);
							
		if (response.getStatus() == MICROSERVICE_RESPONSE_SUCCESS_STATUS) {
			
			LOGGER.info("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
			
			CustomerPayeeListActionResultDto customerPayeeListActionResultDto = PaymentsModelMapperFactory.getMapper().map(
					response.getData().get(0), new TypeToken<CustomerPayeeListActionResultDto>() {}.getType());
			return customerPayeeListActionResultDto.getCustomerPayeeListActionResult();
			
		} else {
			
			LOGGER.error("MICROSERVICE RESPONSE STATUS: ".concat(response.getStatus().toString()).concat(response.getErrors().toString()));
			return null;
			
		}
			
	}
	
	public PaymentActionResult reversePayment(PaymentActionRequest request, CustomerServices customerService) {
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(REVERSE_PAYMENTS);
		PaymentActionResult response = null;
		if (msProperties.isEnabled()) {
			//TODO: Implementation by Henry Terrero
			
		}else {
			response = customerService.getPaymentService().getVaultService().reversePayment(request);
		}
		return response;
		
	}
	
	/**
	 * Consume the getPaymentHistory microservice
	 * 
	 * @param request The request with the information that will be used to consume the microservice.
	 * @param endpoint The URL to consume the microservice
	 * 
	 * @return A PaymentHistoryResponse object than contains a list of payments History
	 */
	private PaymentHistoryResponse consumeGetPaymentHistory(PaymentHistoryRequest request, String endpoint) {

	    String taxId = request.getCustomerProfile().getTaxId();
		String searchType = request.getPaymentFilter().name();
		int paymentSize = request.getPaymentSize();
		String[] payeeId = request.getPayeeId();
		Calendar startDate = request.getStartDate();
		String formattedDate = "";
		if(startDate != null) {
			startDate.add(Calendar.DATE, 0);
			SimpleDateFormat formater = new SimpleDateFormat("MM/dd/yyyy");
			formattedDate = formater.format(startDate.getTime());
			
		}
		
		Map<String,Object> params = new HashMap<>();
		
		params.put("taxId", taxId);
		params.put("searchType", searchType);
		params.put("paymentSize", paymentSize);
		params.put("payeeId",  payeeId != null ? payeeId: "");
		params.put("startDate", formattedDate);
		
		LOGGER.info("Consuming Microservice: ".concat(endpoint));
		
		MicroServiceResponse<PaymentHistoryResponse>  response = request.getUserSession().getRestClientResourceOwner().getForMicroServiceResponse(endpoint, params);
		PaymentHistoryResponse paymentHistoryResponse = null;
		String statusMsg = "Microservice response = null for ".concat(endpoint).concat(" microservice call");
		if(response != null && response.getStatus() != null) {
			statusMsg = "Microservice: ".concat(endpoint).concat(" was consumed and returned a Response with Status: ".concat(response.getStatus().toString()));
		}
		
		if(response != null && response.hasData() && response.getStatus() == MICROSERVICE_RESPONSE_SUCCESS_STATUS) {
			 paymentHistoryResponse = PaymentsModelMapperFactory.getMapper().map( response.getData().get(0), new TypeToken<PaymentHistoryResponse>() {}.getType());
			 
			 LOGGER.info(statusMsg);
		}else {
			 paymentHistoryResponse = new PaymentHistoryResponse();
			 LOGGER.error(statusMsg);
		}
		
        return paymentHistoryResponse;
	}
	
	/**
	 * Consume the getGlobalPayeePreferenceConsume microservice
	 * 
	 * @param payee The Object that have all information that will be used to consume the microservice.
	 * @param userSession all in the session 
	 * 
	 * @return A GlobalPayeePreferences 
	 */
	@SuppressWarnings("deprecation")
	public void getGlobalPayeePreference(GlobalPayee payee,UserSession userSession ) {

		if (payee == null)
			throw new ProgrammingError("A payee is expected");
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_GLOBAL_PAYEE_PREFERENCES);

		String tpid = String.valueOf(payee.getId());

		if (msProperties.isEnabled()) {
		
			GlobalPayeePreferences globals = getGlobalPayeePreferenceConsume(tpid, msProperties.getEndpoint(), userSession);
						
			payee.setPreference(globals);
		
		}else {
			 userSession.getCustomerService().getGlobalPayeePreference(payee);
		}

	}
	
	
	
	/**
	 * Consume the getGlobalPayeePreferenceConsume microservice
	 * 
	 * @param id The information that will be used to consume the microservice.
	 * @param endpoint The URL to consume the microservice
	 * @param userSession all in the session 
	 * 
	 * @return A GlobalPayeePreferences 
	 */
	private GlobalPayeePreferences getGlobalPayeePreferenceConsume(String id,String endpoint,UserSession userSession ) {
		
		GlobalPayeePreferences globalPayyeePreferencesResponse = null;
		
		if (id == null)
			throw new IllegalArgumentException("Id cannot be null");
		
		if (id.equals(""))
			throw new IllegalArgumentException("Id cannot be empty");
		
	    Map<String,Object> params = new HashMap<>();
		params.put("id", id);
		
		MicroServiceResponse<GlobalPayeePreferencesDto>  response = userSession.getRestClientResourceOwner().getForMicroServiceResponse(endpoint, params);

		if (response.getStatus() == MICROSERVICE_RESPONSE_OK_STATUS) {
			globalPayyeePreferencesResponse = PaymentsModelMapperFactory.getMapper().map( response.getData().get(0), new TypeToken<GlobalPayeePreferences>() {}.getType());
			LOGGER.info("Everything goes ok, returning response in endpoint " + endpoint );
		}else {
			LOGGER.error("Microservice fails, status :" + response.getStatus() + " in endpoint " + endpoint);
			LOGGER.error("Reason : {0}".replace("{0}", response.getErrors().toString()));
		}
			
		return globalPayyeePreferencesResponse;
		
	}
	
	
	/**
	 * Consume the getCommonPayeeTree
	 * 
	 * @param userSession all in the session 
	 * 
	 * @return String it is a JSON
	 */
	@SuppressWarnings("deprecation")
	public String getCommonPayeeTree(UserSession userSession) {
		
		String result = "";
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_COMMON_PAYEE_TREE);
		
		if (msProperties.isEnabled()) {
			LOGGER.info("MICROSERVICE {microservice} IS ENABLED".replace("{microservice}", "Payments"));
			result = consumeGetCommonPayeeTree(msProperties.getEndpoint(),userSession);
		}else {
			LOGGER.info("MICROSERVICE {microservice} IS DISABLED".replace("{microservice}", "Payments"));
			result = userSession.getCustomerService().getCommonPayeeTree();
		}

		return result;
	}
	
	
	/**
	 * Consume the getCommonPayeeTree
	 * 
	 * @param endpoint String
	 * @param userSession all in the session 
	 * 
	 * @return String it is a JSON 
	 */
	private String consumeGetCommonPayeeTree(String endpoint,UserSession userSession) {
		
		String result = "";
		
	    Map<String,Object> params = new HashMap<>();
		params.put("id", 1);
		
		MicroServiceResponse<CommonPayeeTreeDto>  response = userSession.getRestClientResourceOwner().getForMicroServiceResponse(endpoint, params);

		if (response.getStatus() == MICROSERVICE_RESPONSE_OK_STATUS) {
			CommonPayeeTreeDto commonPayeeTreeDto = PaymentsModelMapperFactory.getMapper().map( response.getData().get(0), new TypeToken<CommonPayeeTreeDto>() {}.getType());
            
			if (commonPayeeTreeDto != null && !commonPayeeTreeDto.getPayees().equals("")){
				
				LOGGER.info("Everything goes ok, returning response in endpoint " + endpoint );
				result = commonPayeeTreeDto.getPayees();
			}
			
		}else {
			LOGGER.error("Microservice fails, status :" + response.getStatus() + " in endpoint " + endpoint);
			LOGGER.error("Reason : {0}".replace("{0}", response.getErrors().toString()));
		}
		return result;
	}

}
