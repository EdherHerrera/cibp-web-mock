package evertec.cibp.web.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.modelmapper.TypeToken;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.WebUtils;

import com.evertec.cibp.api.client.RestClient;
import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.AlertsHistory;
import evertec.cibp.core.models.customer.CustomerEmailBalanceAlert;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.GetSMSProfileRequest;
import evertec.cibp.web.form.dto.ATHEveryTransAlertDto;
import evertec.cibp.web.form.dto.AlertsHistoryDto;
import evertec.cibp.web.form.dto.CustomerEmailBalanceAlertAccountsReqDto;
import evertec.cibp.web.form.dto.CustomerEmailBalanceAlertDto;
import evertec.cibp.web.form.dto.SmsProfileDto;
import evertec.cibp.web.mapper.AlertsModelMapperFactory;
import evertec.cibp.web.request.AlertsHistoryRequest;
import evertec.cibp.web.session.UserSession;

/**
 * The Class AlertsFacade provides a facade to refactoring existing code and
 * implementing new code for the alerts operations.
 * Microservices Facade abstract the switch logic from monolithic code and microservice code
 * 
 */
public class AlertsFacade {


	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger( AlertsFacade.class );

	/** The instance. */
	private static AlertsFacade instance = null;

	/**
	 * Endpoints for alerts service
	 */
	private static final String GET_ALERTS_HISTORY = "alerts.get.history";
	private static final String GET_SMS_PROFILE = "alerts.get.sms.profile";
	private static final String EMAIL_BALANCE_ALERT = "alerts.email.balance";
	private static final String DELETE_EMAIL_BALANCE_ALERT = "alerts.delete.email.balance";
	private static final String ATH_EVERY_TRANS_ALERT = "alerts.ath.transactions";
	private static final String EMAIL_BALANCE_ALERT_ACCOUNTS = "alerts.email.balances.accounts";
	private static final String ATH_SMS_ALERT_PROFILE = "alerts.ath.sms.profile";

	/**
	 * Successful HTTP Codes
	 */
	private static final int MICROSERVICE_RESPONSE_SUCCESS_STATUS = 200;
	private static final int MICROSERVICE_RESPONSE_CREATED_STATUS = 201;
	private static final int MICROSERVICE_RESPONSE_NOT_CONTENT_STATUS = 204;
	
	/**
	 * Query parameters
	 */
	private static final String SEARCH_PATH_PROFILE_ID = "?search=profileId:";
	private static final String SEARCH_PATH_TAX_ID = "?search=taxId:";
	
	private static final String ALERTS_HISTORY_REQUEST_IS_NULL = "AlertsHistoryRequest is null, this should not happen.";
	private static final String CUSTOMER_PROFILE_LOG = "Customer Profile must be defined.";
	private static final String SMS_ENTITLEMENT_LOG = "SMSEntitlement must be defined.";
	
	public static final String SMS_ALERTS_HISTORY_DAYS="smsAlerts.history.days";
	private static final byte ZERO = 0;
	public static final String ACCOUNT_TYPE_DEPOSIT = "IDA";

	private String alertsHistoryDays;
	
	/** Response status */
	private final static int OK_STATUS_CODE = 200;

	/** Instantiates a new alerts facade. */
	protected AlertsFacade() {}

	/**
	 * Gets the single instance of AlertsFacade.
	 * 
	 * @return single instance of AlertsFacade
	 */
	public static AlertsFacade getInstance() {
		if ( instance == null ) {
			instance = new AlertsFacade();
		}
		return instance;
	}

	/**
	 * Find the alerts history (ALERTS_HISTORY)
	 * 
	 * @param alertsHistoryRequest The request with the information that will be used.
	 * @return A list of AlertsHistory
	 */
	public List<AlertsHistory> getAlertsHistory(AlertsHistoryRequest alertsHistoryRequest,String alertsHistoryDays) {
		this.alertsHistoryDays = alertsHistoryDays;

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_ALERTS_HISTORY);
		if (msProperties.isEnabled()) {
			//Validating parameters
			validationOfParameters(alertsHistoryRequest);
			return consumeGetAlertsHistoryMicroservice(alertsHistoryRequest, msProperties.getEndpoint());

		}else {

			return customerServiceGetAlertsHistory(alertsHistoryRequest);
		}
	}

	/**
	 * Find SmsProfile by SmsProfileId
	 * 
	 * @param userSession the user session
	 * @param GetSMSProfileRequest The request
	 * @return the SMSProfile retrieved
	 */
	@SuppressWarnings("deprecation")
	public SMSProfile getSmsProfile(UserSession userSession, GetSMSProfileRequest getreq) {

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_SMS_PROFILE);
		if (msProperties.isEnabled()) {
			this.validations(getreq);
			return consumeGetSmsProfile(getreq.getSmsEntitlement().getProfileid(), userSession, msProperties.getEndpoint());
		}else {
			return userSession.getCustomerService().getSMSProfile(getreq);
		}	
	}
	
	/**
	 * Updates the CustomerEmailBalanceAlert object in database
	 *
	 * @param alert the object with the information to update.
	 * @param userSession, the user session.
	 * @return The CustomerEmailBalanceAlert updated
	 */
	@SuppressWarnings("deprecation")
	public CustomerEmailBalanceAlert updateCustomerEmailBalanceAlert(CustomerEmailBalanceAlert alert, UserSession userSession) {

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(EMAIL_BALANCE_ALERT);
		if (msProperties.isEnabled()) {
			return consumeUpdateCustEmailBalanceAlertMs(alert, userSession, msProperties.getEndpoint());

		} else {
			return userSession.getCustomerService().UpdateCustomerEmailBalanceAlert(alert);

		}
	}
	
	/**
	 * Saves the CustomerEmailBalanceAlert object
	 *
	 * @param alert the object with the information to insert.
	 * @return The CustomerEmailBalanceAlert inserted
	 */
	@SuppressWarnings("deprecation")
	public CustomerEmailBalanceAlert insertCustomerEmailBalanceAlert(CustomerEmailBalanceAlert alert, UserSession userSession) {

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(EMAIL_BALANCE_ALERT);
		if (msProperties.isEnabled()) {

			return consumeInsertCustEmailBalanceAlertMs(alert, userSession, msProperties.getEndpoint());

		} else {
			return userSession.getCustomerService().InsertCustomerEmailBalanceAlert(alert);

		}
	}
	
	/**
	 * Delete A customerEmailBalance Alert Object
	 * 
	 * @param alert CustomerEmailBalanceAlert
	 * @return True if deleted, false otherwise
	 * */
	@SuppressWarnings("deprecation")
	public boolean deleteCustomerEmailBalanceAlert(CustomerEmailBalanceAlert alert) {		
		UserSession session = (UserSession) WebUtils.getSessionAttribute(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest(), "userSession");

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(DELETE_EMAIL_BALANCE_ALERT);
		if (msProperties.isEnabled()) {
			return consumeDeleteCustomerEmailBalanceAlert(alert.getCustomerProfile().getProfileId(), alert.getId(), session.getRestClientResourceOwner(), msProperties.getEndpoint());

		}

		return session.getCustomerService().DeleteCustomerEmailBalanceAlert(alert);
	}
	
	/**
	 * Delete all customerEmailBalance Alert Objects by profile
	 * 
	 * @param profile CustomerProfile
	 * @return True if deleted, false otherwise 
	 * */
	@SuppressWarnings("deprecation")
	public boolean deleteAllCustomerEmailBalanceAlerts(CustomerProfile profile) {

		UserSession session = (UserSession) WebUtils.getSessionAttribute(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest(), "userSession");

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(DELETE_EMAIL_BALANCE_ALERT);
		if (msProperties.isEnabled()) {
			return consumeDeleteAllCustomerEmailBalanceAlerts(profile.getProfileId(), session.getRestClientResourceOwner(), msProperties.getEndpoint());

		}
		return session.getCustomerService().DeleteCustomerEmailBalanceAlert(profile);
	}
	
	
	/**
	 * Adds the addATHAlertEveryTransactionProfile object
	 *
	 * @param profileId, the profile id
	 * @param accountNumber, the account number
	 * @param cardNumber, the card number
	 * @return True if added, false otherwise
	 */
	@SuppressWarnings("deprecation")
	public boolean addATHAlertEveryTransactionProfile(String profileId, String accountNumber, String cardNumber) {
		UserSession session = (UserSession) WebUtils.getSessionAttribute(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest(), "userSession");
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(ATH_EVERY_TRANS_ALERT);
		if (msProperties.isEnabled()) {
			return consumeAddATHAlertEveryTransProfileMs(new ATHEveryTransAlertDto(profileId, accountNumber, cardNumber), session, msProperties.getEndpoint());

		} else {
			return session.getCustomerService().addATHAlertEveryTransactionProfile(profileId, accountNumber, cardNumber);

		}
	}
	
	/**
	 * Remove the ATHEveryTransAlert object
	 *
	 * @param id, the ath every trans alert id
	 * @param customerService, the customer service
	 * @return True if removed, false otherwise
	 */
	@SuppressWarnings("deprecation")
	public boolean removeAlertEveryTransactionProfile(String id, CustomerServices customerService) {

		if (GenericValidator.isBlankOrNull(id))
			throw new IllegalArgumentException("id=" + id);

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(ATH_EVERY_TRANS_ALERT);
		if (msProperties.isEnabled()) {
			return consumeRemoveAlertEveryTransactionProfile(id, msProperties.getEndpoint());

		} else {
			return customerService.removeAlertEveryTransactionProfileById(id);

		}
	}

	/**
	 * Consumes the getAlertsHistory from the current monolithic implementation
	 *
	 * @param alertsHistoryRequest The Alerts history request object
	 * @return A list of alert history          
	 */
	@SuppressWarnings("deprecation")
	private List<AlertsHistory> customerServiceGetAlertsHistory(AlertsHistoryRequest alertsHistoryRequest ){

		List<AlertsHistory> alertsHistories = null;
		UserSession userSession = alertsHistoryRequest.getUserSession();

		if(!GenericValidator.isBlankOrNull(alertsHistoryRequest.getProfileId())){
			alertsHistories = userSession.getCustomerService().getAlertsHistoryByProfileId(alertsHistoryRequest.getProfileId());
		}else {
			alertsHistories = userSession.getCustomerService().getAlertsHistoryBySSN(alertsHistoryRequest.getTaxId());
		}
		return alertsHistories;
	}

	/**
	 * Consumes the getAlertsHistory microservice
	 *
	 * @param alertsHistoryRequest 
	 * @param userSession
	 *           
	 */
	private List<AlertsHistory> consumeGetAlertsHistoryMicroservice(AlertsHistoryRequest alertsHistoryRequest, String endpoint) {

		// Setting microservice
		List<AlertsHistory> alertsHistories = null;
		UserSession userSession = alertsHistoryRequest.getUserSession();
		Map<String,Object> param = new HashMap<>();
		param.put("days", alertsHistoryDays);

		// Setting URI
		String requestURI = endpoint;

		if(!GenericValidator.isBlankOrNull(alertsHistoryRequest.getProfileId())){
			requestURI = requestURI.concat(SEARCH_PATH_PROFILE_ID).concat(alertsHistoryRequest.getProfileId());
		}else {
			requestURI = requestURI.concat(SEARCH_PATH_TAX_ID).concat(alertsHistoryRequest.getTaxId());
		}

		// Microservice call
		MicroServiceResponse<AlertsHistoryDto> response = userSession.getRestClientResourceOwner()
				.getForMicroServiceResponse(requestURI, param);


		if (MICROSERVICE_RESPONSE_SUCCESS_STATUS == response.getStatus()) {

			List<AlertsHistory> aHistories = AlertsModelMapperFactory.getMapper().map(response.getData(),
					new TypeToken<ArrayList<AlertsHistory>>() {
			}.getType());

			alertsHistories = this.convertListAlertsHistoryToLinkedListAlertsHistory(aHistories);

		}
		return alertsHistories;
	}
	
	/**
	 * Consume microservice to get the SMS profile
	 *
	 * @param ProfileId
	 * @param userSession the user session
	 * @return SMSProfile
	 */
	private SMSProfile consumeGetSmsProfile(String profileId, UserSession userSession, String endpoint) {

		SMSProfile smsProfile = null;

		MicroServiceResponse<SmsProfileDto> response 
		= userSession.getRestClientResourceOwner().getForMicroServiceResponse(endpoint.replace("{profileId}", profileId), null);

		if ( MICROSERVICE_RESPONSE_SUCCESS_STATUS == response.getStatus() ) {

			smsProfile = AlertsModelMapperFactory.getMapper().map(response.getData().get(ZERO), SMSProfile.class);
			Set<SMSCTN> smsCtnSet = AlertsModelMapperFactory.getMapper().map(smsProfile.getCtns(), new TypeToken<HashSet<SMSCTN>>() {}.getType());
			smsProfile.setCtns(smsCtnSet);
		}

		return smsProfile;
	}
	
	/**
	 * Consumes the updateCustomerEmailBalanceAlert microservice
	 *
	 * @param alert, the object with the information to update.
	 * @param userSession, the user session.
	 *           
	 */
	private CustomerEmailBalanceAlert consumeUpdateCustEmailBalanceAlertMs(CustomerEmailBalanceAlert alert, UserSession userSession, String endpoint) {

		// Microservice call
		MicroServiceResponse<CustomerEmailBalanceAlertDto> response = userSession.getRestClientResourceOwner().putForMicroServiceResponse(endpoint, new CustomerEmailBalanceAlertDto(alert), null);

		if (response.getStatus() == MICROSERVICE_RESPONSE_NOT_CONTENT_STATUS) {

			logger.info(endpoint + " MICROSERVICE RESPONSE STATUS: " + response.getStatus());
			return alert;

		} else {

			logger.info(endpoint + " MICROSERVICE RESPONSE STATUS: " + response.getStatus());
			throw new RuntimeException(response.getErrors().toString());

		}
	}
	
	/**
	 * Consumes the insertCustomerEmailBalanceAlert microservice
	 *
	 * @param alert The customer email balance alert
	 * @param userSession The user session
	 *           
	 */
	private CustomerEmailBalanceAlert consumeInsertCustEmailBalanceAlertMs(CustomerEmailBalanceAlert alert, UserSession userSession, String endpoint) {

		// Microservice call
		MicroServiceResponse<CustomerEmailBalanceAlertDto> response = userSession.getRestClientResourceOwner().postForMicroServiceResponse(endpoint, new CustomerEmailBalanceAlertDto(alert), null);

		if (response.getStatus() == MICROSERVICE_RESPONSE_CREATED_STATUS) {
			return alert;
		} else {
			throw new RuntimeException(response.getErrors().toString());
		}
	}
	
	/**
	 * Consumes the deleteCustomerEmailBalanceAlert microservice
	 *
	 * @param profileId The customer profile ID
	 * @param accountId The account ID
	 * @param client The REST client to make the call
	 * @return True if the customer account email balance alert was deleted, false otherwise.          
	 */
	private boolean consumeDeleteCustomerEmailBalanceAlert(String profileId, String accountId, RestClient client, String endpoint) {

		String requestUrl = endpoint.concat("/").concat(accountId)
				.replace("{profileId}", profileId);

		MicroServiceResponse<CustomerEmailBalanceAlertDto> response = client.deleteForMicroServiceResponse(requestUrl, null);

		return response.getStatus() == MICROSERVICE_RESPONSE_NOT_CONTENT_STATUS;
	}
	
	/**
	 * Consumes the delete all CustomerEmailBalanceAlert microservice
	 *
	 * @param profileId The customer profile ID
	 * @param client The REST client
	 * @return True if the customer email balance alert was deleted, false otherwise.             
	 */
	private boolean consumeDeleteAllCustomerEmailBalanceAlerts(String profileId, RestClient client, String endpoint) {

		String requestUrl = endpoint.replace("{profileId}", profileId);

		MicroServiceResponse<CustomerEmailBalanceAlertDto> response = client.deleteForMicroServiceResponse(requestUrl, null);
		return response.getStatus() == MICROSERVICE_RESPONSE_NOT_CONTENT_STATUS;
	}
	
	/**
	 * Consumes the addATHAlertEveryTransactionProfile microservice
	 *
	 * @param request, the microservice request
	 *           
	 */
	private boolean consumeAddATHAlertEveryTransProfileMs(ATHEveryTransAlertDto request, UserSession session, String endpoint) {
		logger.info("Consuming microservice");
		// Microservice call
		MicroServiceResponse<ATHEveryTransAlertDto> response = session.getRestClientResourceOwner().postForMicroServiceResponse(endpoint, request, null);

		if (response.getStatus() == MICROSERVICE_RESPONSE_CREATED_STATUS) {
			logger.info("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
			return true;
		} else {
			logger.info("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
			logger.info(response.getErrors().toString());
			return false;
		}
	}
	
	/**
	 * Consumes the removeAlertEveryTransactionProfile microservice
	 *
	 * @param id, the ath every trans alert id
	 *           
	 */
	private boolean consumeRemoveAlertEveryTransactionProfile(String id, String endpoint) {

		//User session building
		UserSession session = (UserSession) WebUtils.getSessionAttribute(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest(), "userSession");

		//Adding the parameter.
		Map<String,Object> param = new HashMap<>();
		param.put("id", id);

		// Microservice call
		MicroServiceResponse<ATHEveryTransAlertDto> response = session.getRestClientResourceOwner().deleteForMicroServiceResponse(endpoint, param);

		return (response.getStatus() == MICROSERVICE_RESPONSE_NOT_CONTENT_STATUS);
	}
	
	private void validations(GetSMSProfileRequest getreq) {
		if (getreq.getCustomerProfile() == null)
			throw new ProgrammingError(CUSTOMER_PROFILE_LOG);
		if (getreq.getSmsEntitlement() == null)
			throw new ProgrammingError(SMS_ENTITLEMENT_LOG);
	}
	
	/**
	 * Convert List Alerts History to LinkedList Alerts History
	 *
	 * @param alertsHistories
	 *           
	 */
	private List<AlertsHistory> convertListAlertsHistoryToLinkedListAlertsHistory(List<AlertsHistory> alertsHistories) {

		List<AlertsHistory> decoupled = new LinkedList<>();
		decoupled.addAll(alertsHistories);

		return decoupled;
	}


	/**
	 * Validates the input request for alerts history
	 * 
	 * @param alertsHistoryRequest
	 *            the request to be verified
	 */
	private void validationOfParameters(AlertsHistoryRequest alertsHistoryRequest) {
		if (alertsHistoryRequest == null) {
			throw new ProgrammingError(ALERTS_HISTORY_REQUEST_IS_NULL);
		}else if(GenericValidator.isBlankOrNull(alertsHistoryRequest.getProfileId()) && GenericValidator.isBlankOrNull(alertsHistoryRequest.getTaxId())){
			throw new IllegalArgumentException("profileId or taxId=" + alertsHistoryRequest.getProfileId());
		}
	}

	
	/**
	 * get the Customer email balance alert accounts
	 * 
	 * @param request This is a CustomerEmailBalanceAlertAccountsReqDto object.
	 * @return List<CustomerEmailBalanceAlert> alerts
	 */
	@SuppressWarnings("deprecation")
	public List<CustomerEmailBalanceAlert> getCustomerEmailBalanceAlertAccounts(CustomerEmailBalanceAlertAccountsReqDto request) {
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(EMAIL_BALANCE_ALERT_ACCOUNTS);
		List<CustomerEmailBalanceAlert> alerts = new LinkedList<>();
		if(msProperties.isEnabled()) {
			alerts = consumeGetCustomerEmailBalanceAlertAccounts(request.getUserSession().getRestClientResourceOwner(), request.getProfileId(), request.getAccounts(), msProperties.getEndpoint());
		}else {
			
			request.getCustomerService().getCustomerEmailBalanceAlertAccounts(request.getEmailReq());
			for (Iterator<TVFrontendAccount> iterator = request.getAccounts().iterator(); iterator.hasNext();) {
				TVFrontendAccount act = (TVFrontendAccount) iterator.next();
				if (act.getEmailAccountBalenceAlert() != null && act.getEmailAccountBalenceAlert().size() > 0) {
					for (Iterator<CustomerEmailBalanceAlert> iterator1 = act.getEmailAccountBalenceAlert()
							.iterator(); iterator1.hasNext();)
						alerts.add((CustomerEmailBalanceAlert) iterator1.next());
				}
			}
			
		}
		return alerts;
		
	}
	
	/**
	 * Consumes the getCustomerEmailBalanceAlertAccounts micro service
	 *
	 * @param client RestClient object
	 * @param profileId This is the Customer profile ID
	 * @param accounts List of Accounts (List<TVFrontendAccount> ) of Customer
	 * @param endpoint This is the url to consume the micro service
	 * 
	 * @return List<CustomerEmailBalanceAlert> filteredAlerts
	 * 
	 *           
	 */
	private List<CustomerEmailBalanceAlert> consumeGetCustomerEmailBalanceAlertAccounts(RestClient client, String profileId, List<TVFrontendAccount> accounts, String endpoint) {

		List<CustomerEmailBalanceAlert> filteredAlerts = new ArrayList<>();
		endpoint = endpoint.replace("{profileId}", profileId);
		MicroServiceResponse<CustomerEmailBalanceAlertDto> response = client.getForMicroServiceResponse(endpoint, null);
		
		
		if(response != null && response.hasData() && response.getStatus() == OK_STATUS_CODE) {
			
			ArrayList<CustomerEmailBalanceAlert> alerts = AlertsModelMapperFactory.getMapper().map( response.getData(), new TypeToken<ArrayList<CustomerEmailBalanceAlert>>() {}.getType());
			
			accounts.forEach(account -> {
				alerts.forEach(alert -> {
					
					if (account.getAccountKey().equals(
							generateAccountKey(alert.getAccountNumber(), alert.getAccountSubtype(),
									alert.getAccountSection()))) {
						filteredAlerts.add(alert);
					}
				});
				
				
			});
		}else if(response != null && response.hasErrors()) {
			
			logger.error("The microservice call to "+endpoint+" returned an error response: "+response.toString());
		}
		
		return filteredAlerts;
	}
	
	
	/**
	 * Generate account key
	 * 
	 * @param accountNumber This is the account number
	 * @param subType this is the account sub type
	 * @param section This is the account section
	 * 
	 * @return generatedAccountKey The generated account key
	 */
	private static String generateAccountKey(String accountNumber, String subType, String section) {
		StringBuilder generatedAccountKey = new StringBuilder(100);
		generatedAccountKey.append(accountNumber);
		generatedAccountKey.append("|");
		generatedAccountKey.append(subType);
		if (subType != null && subType.equalsIgnoreCase(ACCOUNT_TYPE_DEPOSIT) && section != null) {
			generatedAccountKey.append("|");
			generatedAccountKey.append(section);
		}
		return generatedAccountKey.toString();
	}

}