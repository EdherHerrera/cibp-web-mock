package evertec.cibp.web.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.modelmapper.TypeToken;

import com.evertec.cibp.api.client.RestClient;
import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;
import com.lowagie.text.pdf.codec.Base64;

import evertec.cibp.core.models.documents.CustomerDocuments;
import evertec.cibp.core.models.documents.CustomerDocumentsPackage;
import evertec.cibp.web.form.dto.CustomerDocPackageDto;
import evertec.cibp.web.form.dto.GetDocumentRespDto;
import evertec.cibp.web.mapper.DocumentsModelMapperFactory;
import evertec.cibp.web.request.SignDocumentsRequest;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.DocumentsUtils;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.GetDocumentRequest;
import evertec.otherservices.responses.GetDocumentResp;

/**
 * The Class DocumentsFacade provides a facade to refactoring existing code and
 * implementing new code for the documents operations.
 * Microservices Facade abstract the switch logic from monolithic code and microservice code
 * 
 */
public class DocumentsFacade {


	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger( DocumentsFacade.class );

	/** The instance. */
	private static DocumentsFacade instance = null;

	/**
	 * Endpoints for documents service
	 */
	private static final String GET_DISBURSEMENT_PROP_ID = "documents.get.disbursements";
	private static final String UPDATE_DISBURSEMENT_PROP_ID = "documents.put.disbursements";
	private static final String PUT_DOC_SIGNATURE_PROP_ID = "documents.put.signatures";
	private static final String GET_DOCUMENT_PROP_ID = "documents.get.document";
	
	/** Successful HTTP Status when signing a disbursement*/
	private static final int SIGNDOC_SUCCESS_STATUS = 200;

	/**
	 * Instantiates a new documents facade.
	 */
	protected DocumentsFacade() {
	}

	/**
	 * Gets the single instance of DocumentsFacade.
	 *
	 * @return single instance of DocumentsFacade
	 */
	public static DocumentsFacade getInstance() {
		if ( instance == null ) {
			instance = new DocumentsFacade();
		}

		return instance;
	}

	/**
	 * Gets the disbursements list.
	 *
	 * @param refresh the refresh
	 * @param userSession the user session
	 * @return the disbursements list
	 */
	@SuppressWarnings("deprecation")
	public List<CustomerDocumentsPackage> getDisbursementsList( boolean refresh, UserSession userSession ) {

		List<CustomerDocumentsPackage> disbursementsList = null;

		if ( userSession.getDisbursementsList() == null || refresh ) {

			MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_DISBURSEMENT_PROP_ID);
			if (msProperties.isEnabled()) {
				disbursementsList = consumeFindDisbursementByProfileId(userSession.getCustomerProfile().getProfileId(), userSession,msProperties.getEndpoint(), 1 );

			} else {
				disbursementsList = userSession.getCustomerService().getPendingDisbursements( userSession.getCustomerProfile().getProfileId());
			}

			List<CustomerDocumentsPackage> filteredList = new ArrayList<>();
			if ( disbursementsList != null && !disbursementsList.isEmpty() ) {

				for (CustomerDocumentsPackage pack : disbursementsList) {

					processDocuments( pack, userSession );
					if(pack.getDocuments() != null && !pack.getDocuments().isEmpty()){
						filteredList.add(pack);
					}

				}
				disbursementsList = filteredList;
			}
			userSession.setDisbursementsList( disbursementsList );

		} else if ( userSession.getDisbursementsList() != null ) {
			disbursementsList = userSession.getDisbursementsList();
		}
		return disbursementsList;
	}

	/**
	 * Gets customer signed disbursements list.
	 *
	 * @param userSession the user session
	 * @return the disbursements list
	 */
	@SuppressWarnings("deprecation")
	public List<CustomerDocumentsPackage> getAcceptedDisbursements(UserSession userSession ) {

		List<CustomerDocumentsPackage> disbursementsList = null;
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_DISBURSEMENT_PROP_ID);
		if (msProperties.isEnabled()) {
			disbursementsList = consumeFindDisbursementByProfileId( userSession.getCustomerProfile().getProfileId(), userSession, msProperties.getEndpoint(),2 );
			
		} else {
			disbursementsList = userSession.getCustomerService().getAcceptedDisbursements(userSession.getCustomerProfile().getProfileId() );

		}

		List<CustomerDocumentsPackage> filteredList = new ArrayList<>();
		if ( disbursementsList != null && !disbursementsList.isEmpty() ) {

			for (CustomerDocumentsPackage pack : disbursementsList) {

				processDocuments( pack, userSession );
				if(pack.getDocuments() != null && !pack.getDocuments().isEmpty()){
					filteredList.add(pack);
				}
			}
			disbursementsList = filteredList;
		}

		return disbursementsList;
	}


	/**
	 * Signs the disbursement documents
	 * 
	 * @param request The SignDocumentsRequest
	 * @param customerService The customerService in case monolithic switch implementation.
	 * @return True if signed, false otherwise.
	 */
	@SuppressWarnings("deprecation")
	public boolean signDocuments(String costumerName, CustomerDocumentsPackage disbursement,UserSession userSession){
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(PUT_DOC_SIGNATURE_PROP_ID);
		if (msProperties.isEnabled()) {
			return this.consumeSignDocuments(costumerName, disbursement, userSession.getRestClientResourceOwner(), msProperties.getEndpoint());

		}else{
			logger.info("The cibp-documents-service is off. Delegating the operation signDocuments to customerService");
			return DocumentsUtils.signDocuments(costumerName, disbursement, userSession.getCustomerService());
		}
	}
	
	/**
	 * It dispatch the Document file request to cibp-documents-service or to customer service.
	 * It depends if the microservice flag is active or not
	 * @param request
	 * @param userSession
	 * @return Document response with the file
	 * */
	@SuppressWarnings("deprecation")
	public GetDocumentResp getDocumentFile(GetDocumentRequest request, UserSession userSession) {
		logger.info("Looking for the document file of :"+request.getDocumentId());

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_DOCUMENT_PROP_ID);
		//Verify if documents microservice is working
		if (msProperties.isEnabled()) {
			return consumeGetDocumentFile(request, userSession, msProperties.getEndpoint());
		}else {
			logger.info("The cibp-documents-service is off. Delegating the operation GetDocumentFile to customerService");
			return userSession.getCustomerService().getDocumentFile(request);
		}
	}
	
	/**
	 * Update document package method to switch from monolithic code to
	 * microservice and viceversa.
	 * 
	 * @param disbursement
	 *            the customer documents package to be updated
	 * @param userSession
	 *            the current user session
	 */
	@SuppressWarnings("deprecation")
	public void updateDocumentPackage(CustomerDocumentsPackage disbursement, UserSession userSession) {
		logger.info("Update document package method calling from Documentsfacade.");

		if (disbursement == null || userSession == null) {
			throw new IllegalArgumentException();
		}
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(UPDATE_DISBURSEMENT_PROP_ID);
		
		if (msProperties.isEnabled()) {
			consumeUpdateDocumentPackage(disbursement, userSession.getRestClientResourceOwner(), msProperties.getEndpoint());
		} else {
			logger.info(
					"CIBP Documents Microservices is disabled. Delegating the operation UpdateDocumentPackage to customerService.");
			userSession.getCustomerService().updateDocumentPackage(disbursement);
		}
	}

	/**
	 * Process documents.
	 *
	 * @param pack the pack
	 * @param userSession the user session
	 */
	private void processDocuments( CustomerDocumentsPackage pack, UserSession userSession ) {

		List<CustomerDocuments> docsList = new LinkedList<>();

		if (pack.getDocuments() != null && !pack.getDocuments().isEmpty()) {

			LinkedHashMap<String, CustomerDocuments> docsMap = new LinkedHashMap<>();

			for (CustomerDocuments docs : pack.getDocuments()) {
				if (docs.getHideDoc() == null || "N".equals(docs.getHideDoc()))
					docsMap.put(docs.getId().getDocInfoId(), docs);
				docsList.add(docs);
			}

			pack.setCustomerDocuments( docsList );
			pack.setCustomerDocumentsMap( docsMap );
			pack.setCustomerDocuments( userSession.getOrderedDocsList(pack) );

		}

	}

	/**
	 * Consume find pending by profile id.
	 *
	 * @param profileId the profile id
	 * @param userSession the user session
	 * @param endpoint The endpoint to be called
	 * @param searchType [1: Get pending disbursements, 2: Get signed disbursements]
	 * @return the list of disbursements
	 */
	private List<CustomerDocumentsPackage> consumeFindDisbursementByProfileId( String profileId, UserSession userSession, String endpoint, int searchType) {

		List<CustomerDocumentsPackage> disbursementsList = null;

		Map<String,Object> params = new HashMap<>();
		params.put("profileId", profileId);
		params.put("searchType", searchType);


		MicroServiceResponse<CustomerDocPackageDto> response 
		= userSession.getRestClientResourceOwner().getForMicroServiceResponse(endpoint, params);

		if ( 200 == response.getStatus() ) {

			disbursementsList = DocumentsModelMapperFactory.getMapper().map( response.getData(), 
					new TypeToken<List<CustomerDocumentsPackage>>() {}.getType() );

		} else if ( 404 == response.getStatus() ){
			disbursementsList = new ArrayList<>(); // No more disbursements

		}else {

			logger.error("Problems consuming findDisbursementsByProfileId operation with profileId: " + 
					profileId + ". Status: " + 
					response.getStatus() +
					" " + 
					response.getErrors());
		}

		return disbursementsList;
	}

	/**
	 * Consume sign documents
	 * 
	 * @param request The SignDocumentsRequest object
	 * @param restClient The rest client to make the call
	 * @return True if signature in documents was successful, false otherwise.
	 */
	private boolean consumeSignDocuments(String customerName, CustomerDocumentsPackage disbursement, RestClient restClient, String endpoint) {

		String disbursementId = "Disbursement ID: "+disbursement.getDocPackId();
		logger.info(disbursementId + "Documents microservice will be used to sign documents");

		CustomerDocPackageDto disbursementPack = DocumentsModelMapperFactory.getMapper().map(disbursement,CustomerDocPackageDto.class);
		SignDocumentsRequest request = new SignDocumentsRequest(customerName, disbursementPack);
		MicroServiceResponse<CustomerDocPackageDto> response 
		= restClient.putForMicroServiceResponse(endpoint, request, null);

		boolean isSignatureSuccess = (SIGNDOC_SUCCESS_STATUS == response.getStatus());

		if(isSignatureSuccess){
			logger.info(disbursementId + "Documents signed successfully.");
		}else{
			logger.error(disbursementId + "Problems signing disbursement. Status: " + 
					response.getStatus() + " " + response.getErrors());
		}

		return isSignatureSuccess;
	}


	/**
	 * Process the request to get the file of the document.
	 * This method invokes the cibp-documents-microservice to get the document File
	 * 
	 * @param request
	 * @param userSession
	 * @return Document response with the file
	 * */
	private GetDocumentResp consumeGetDocumentFile(GetDocumentRequest request, UserSession userSession, String endpoint) {

		logger.info("cibp-documents-microservices is enabled");

		GetDocumentResp docResp= new GetDocumentResp();
		RestClient restClient = userSession.getRestClientResourceOwner();
		String requestURI = endpoint.replace("{documentId}", request.getDocumentId());

		logger.debug("Calling the end point: "+requestURI);
		MicroServiceResponse<GetDocumentRespDto> response = restClient.getForMicroServiceResponse(requestURI, null);

		//200 = OK
		if(response.getStatus() == 200) {
			logger.info("Document File recovered successfully ");
			List<GetDocumentRespDto> documentRespDtos =  DocumentsModelMapperFactory.getMapper().map( response.getData(), 
					new TypeToken<List<GetDocumentRespDto>>() {}.getType() );

			if( documentRespDtos != null && !documentRespDtos.isEmpty()) {
				GetDocumentRespDto documentRespDto = documentRespDtos.get(0);

				byte[] documentBytes = Base64.decode(documentRespDto.getDocumentBase64());

				docResp.setResponseStatus(documentRespDto.getResponseStatus());
				docResp.setDocumentByteArray(documentBytes);

				logger.info("document decoded from base64 successfully");
			}
		}else if(response.getStatus() == 404) {
			logger.info("There is not a document file for the request document");
			docResp.setResponseStatus(ResponseStatus.NO_DATA_FOUND);
		}else {
			logger.warn("The document file could not be recovered");
			docResp.setResponseStatus(ResponseStatus.ERROR);
		}

		//Verify errors
		if(response.hasErrors()) {
			for (com.evertec.cibp.api.client.model.Error error : response.getErrors()) {
				logger.error(error);
			}
		}

		return docResp;

	}

	/**
	 * Consumes the update customer package microservice.
	 * 
	 * @param customerDocumentsPackage
	 *            the customer documents package to be updated
	 */
	private boolean consumeUpdateDocumentPackage(CustomerDocumentsPackage disbursement, RestClient restClient, String endpoint) {
		logger.info("CIBP Documents Microservices is enabled: consuming updateDocumentPackage service.");

		if (disbursement == null || restClient == null) {
			throw new IllegalArgumentException();
		}

		String url = endpoint.replace("{disbursementId}", disbursement.getDocPackId());
		MicroServiceResponse<CustomerDocPackageDto> response = restClient.putForMicroServiceResponse(url,disbursement, null);

		// 200 = OK
		boolean documentPackageIsUpToDate = (200 == response.getStatus());

		if (documentPackageIsUpToDate) {
			logger.info(disbursement.getDocPackId() + " disbursementPack successfully updated.");
		} else {
			logger.info(disbursement.getDocPackId() + " response disbursementPack was not updated.");
		}

		return documentPackageIsUpToDate;
	}
}
