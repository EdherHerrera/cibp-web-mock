package evertec.cibp.web.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.modelmapper.TypeToken;
import org.springframework.web.util.WebUtils;

import com.evertec.cibp.api.client.RestClient;
import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;

import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerPhone;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerSignonStats;
import evertec.cibp.core.models.customer.applications.AddressApplication;
import evertec.cibp.core.models.customer.applications.CustomerApplication;
import evertec.cibp.core.models.customer.applications.EpayrollApplication;
import evertec.cibp.core.models.customer.applications.EstatementApplication;
import evertec.cibp.core.models.customer.applications.PhoneApplication;
import evertec.cibp.core.models.customer.devices.CustomerDevice;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.mbop.MbopCustomerInfo;
import evertec.cibp.core.models.mbop.MbopStatus;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.ModifyCustomerRequest;
import evertec.cibp.core.services.responses.CustomerDeviceResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.web.form.dto.CustomerApplicationDto;
import evertec.cibp.web.form.dto.CustomerDeviceDto;
import evertec.cibp.web.form.dto.CustomerEntitlementDto;
import evertec.cibp.web.form.dto.CustomerFlagDto;
import evertec.cibp.web.form.dto.CustomerProfileDto;
import evertec.cibp.web.form.dto.MbopCustomerInfoDto;
import evertec.cibp.web.mapper.CustomerModelMapperFactory;
import evertec.cibp.web.request.CustomerApplicationRequest;
import evertec.cibp.web.request.SaveMbopCustomerInfoRequest;
import evertec.cibp.web.response.GlobalEntitlementResponse;
import evertec.cibp.web.session.UserSession;

/**
 * The Class CustomerFacade provides a facade to refactoring existing code and
 * implementing new code for the customer operations.
 * Microservices Facade abstract the switch logic from monolithic code and microservice code
 * 
 */
public class CustomerFacade {

	/** The Constant logger. */
	private static final Logger LOGGER = Logger.getLogger(CustomerFacade.class);

	/** The instance. */
	private static CustomerFacade instance = null;

	/**
	 * Endpoints for customer service
	 */
	private static final String CUSTOMER_PROFILE = "customers.profile";
	private static final String CUSTOMER_PHONE = "customers.phone";
	private static final String CUSTOMER_ENTITLEMENTS = "customers.entitlements";
	private static final String CUSTOMER_MBOP_INFO = "customers.mbopinformation";
	private static final String CUSTOMER_SIGNON_STATS = "customers.signonstats";
	private static final String CUSTOMER_FLAGS = "customers.flags";
	private static final String CUSTOMER_APPLICATIONS = "customers.application";
	private static final String CUSTOMER_DEVICES = "customers.devices";
	private static final String PUT_CUSTOMER_PHONE = "customers.put.customer.phone";
	private static final String POST_CUSTOMER_DEVICES = "customers.post.customer.devices";
	
	/** Reponse status */
	private final static int NO_CONTENT_STATUS = 204;
	private final static int OK_STATUS = 200;
	private final static int ACCEPTED_STATUS_CODE = 202;
	private final static int MICROSERVICE_RESPONSE_CREATED_STATUS = 201;
	
	//4XX HTTP Errors
	private final static int NOT_FOUND = 404;
	private final static int BAD_REQUEST = 400;
	//5XX HTTP Errors
	private final static int BAD_GATEWAY = 502;
	
	/**
	 * Deprecation constant.
	 */
	private static final String DEPRECATION = "deprecation";
	private static final String GLOBAL_ENTITLEMENT = "customers.global.entitlement";

	/** Instantiates a new customer facade. */
	protected CustomerFacade() {}

	/**
	 * Gets the single instance of AccountsFacade.
	 *
	 * @return single instance of AccountsFacade
	 */
	public static CustomerFacade getInstance() {
		if (instance == null) {
			instance = new CustomerFacade();
		}
		return instance;
	}

	/**
	 * Get Customer Profile By Id
	 * 
	 * @param key The key from which a search will be made. Example: username
	 * @param value The expected value expected of the previous key. Example: theUser123
	 * @param userSession The user session
	 * @return A list of customer profile
	 */
	@SuppressWarnings("deprecation")
	public List<CustomerProfile> getCustomerProfileById(String key, String value, UserSession userSession) {
		if (value == null)
			throw new IllegalArgumentException(value);
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CUSTOMER_PROFILE);
		List<CustomerProfile> profiles  = new ArrayList<CustomerProfile>();
		
		if (msProperties.isEnabled()) {
			String search = key + ":" + value;
		    profiles = consumeGetCustomerProfileById(search, userSession.getRestClientResourceOwner(), msProperties.getEndpoint());
		    if (!profiles.isEmpty()) {
		    	for (CustomerProfile customerProfile : profiles) {
					customerProfile.setSignupCampaign(userSession.getCustomerSignupCampaigns());
				}
		    }
		}else {
	          profiles = userSession.getCustomerService().getMultipleProfilesByTaxId(value);

		}
		return profiles;
	}
	
	/**
	 * Get Customer Phone
	 * 
	 * @param CustomerPhone searchCriteria
	 * @param userSession The user session
	 * @return A customer phone object
	 */
	@SuppressWarnings("deprecation")
	public CustomerPhone getCustomerPhone(final CustomerPhone searchCriteria, UserSession userSession) {
		if (searchCriteria == null)
			throw new IllegalArgumentException("Null search criteria");
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CUSTOMER_PHONE);
		CustomerPhone customerPhone;
		
		if (msProperties.isEnabled()) {
			
			customerPhone = consumeGetCustomerPhone(convertCustomerPhoneToSearchFormat(searchCriteria), userSession.getRestClientResourceOwner(), msProperties.getEndpoint());
		  
		}else {
			customerPhone = userSession.getCustomerService().getCustomerPhone(searchCriteria);

		}
		return customerPhone;
	}
	
	/**
	 * Convert Customer Phone to search format
	 * 
	 * @param CustomerPhone searchCriteria
	 * @return A String
	 */
	private String convertCustomerPhoneToSearchFormat(CustomerPhone searchCriteria) {
		String search="";
		StringBuilder searchBuilder = new StringBuilder(65);
		
		if (searchCriteria.getProfileId() != null) {
			search = searchBuilder.append("profileId").append(":").append(searchCriteria.getProfileId()).append(",").toString();
		}
		
		if (searchCriteria.getPhoneNumber() != null) {
			search = searchBuilder.append("phoneNumber").append(":").append(searchCriteria.getPhoneNumber()).toString();
		}
		
		
		return search;
	}

	/**
	 * Update Customer Entitlements
	 * 
	 * @param userSession the user session
	 * @param request the customer entitlements to update
	 */
	@SuppressWarnings("deprecation")
	public void updateEntitlement(UserSession userSession, List<CustomerEntitlement> request) {
		CustomerProfile customerProfile = userSession.getCustomerProfile();

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CUSTOMER_ENTITLEMENTS);
		if (msProperties.isEnabled()) {
			
			List<CustomerEntitlementDto> entitlements = CustomerModelMapperFactory.getMapper().map(request,
					new TypeToken<List<CustomerEntitlementDto>>() {
					}.getType());

			consumeUpdateEntitlement(customerProfile.getProfileId(), entitlements,
					userSession.getRestClientResourceOwner(), msProperties.getEndpoint());
		} else {
			
			CustomerProfileService customerProfileService = userSession.getCustomerService()
					.getCustomerProfileService();

			for (CustomerEntitlement entitlement : request) {
				customerProfileService.updateEntitlement(customerProfile, entitlement);
			}
		}

	}
	
	/**
	 * Save mbop customer information
	 * 
	 * @param MbopCustomerInfo mbopCustomerInfo
	 * @param UserSession userSession
	 */
	@SuppressWarnings("deprecation")
	public MbopCustomerInfo saveMbopCustomerInfo(MbopCustomerInfo mbopCustomerInfo, UserSession userSession) {

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CUSTOMER_MBOP_INFO);
		MbopCustomerInfo mbopCustomerIinfo = null;
		
		if (msProperties.isEnabled()) {
		   LOGGER.info("MbopCustomerInfo was consume by micro-service");
			SaveMbopCustomerInfoRequest customerInfoRequest = new SaveMbopCustomerInfoRequest();
			customerInfoRequest.setEnrollmentSource(CustomerEnrollmentSource.WEB);
			customerInfoRequest.setProfileId(userSession.getCustomerProfile().getProfileId());
			customerInfoRequest.setStatus(MbopStatus.ACTIVE);
		    mbopCustomerIinfo = consumeSaveMbopCustomerInfo(customerInfoRequest, userSession.getRestClientResourceOwner(), msProperties.getEndpoint());
		}else {
		  LOGGER.info("MbopCustomerInfo was consume by core");
		  mbopCustomerIinfo = userSession.getCustomerService().saveMbopCustomerInfo(mbopCustomerInfo);
		}
        return mbopCustomerIinfo;
	}
	
	/**
	 * Update Customer Signon Stats 
	 * 
	 * @param CustomerSignonStats customerSignonStats
	 * @param UserSession userSession
	 */
	@SuppressWarnings("deprecation")
	public void updateCustomerSignonStats(CustomerSignonStats customerSignonStats,UserSession userSession) {
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CUSTOMER_SIGNON_STATS);
		
		if (msProperties.isEnabled()) {
			 consumeUpdateCustomerSignonStats(customerSignonStats,userSession.getRestClientResourceOwner(), msProperties.getEndpoint());
		     LOGGER.info("UpdateCustomerSignonStats was consume by micro-service");
		}else {
		     LOGGER.info("UpdateCustomerSignonStats was consume by core");
		     userSession.getCustomerService().updateCustomerSigonStats(customerSignonStats);
		}
          		
	}
	
	/**
	 * Add customer flag
	 * 
	 * @param userSession This is the user session that contains the RestClient and the customer profileId.
	 * @param modifyRequest This is the request that contains the flag information.
	 */
	@SuppressWarnings("deprecation")
	public void addCustomerFlag(UserSession userSession, ModifyCustomerRequest modifyRequest) {
		if (userSession == null)
			throw new IllegalArgumentException("UserSession must not be null");
		
		if (modifyRequest == null)
			throw new IllegalArgumentException("ModifyCustomerRequest must not be null");
				
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CUSTOMER_FLAGS);
		if (msProperties.isEnabled()) {
			consumeMicroserviceAddCustomerFlag(userSession, modifyRequest.getProfileId(), modifyRequest.getCustomerFlagType().toString(), msProperties.getEndpoint());
		} else {
			userSession.getCustomerService().addCustomerFlag(modifyRequest);
		}
	}
	
	/** 
	 * Remove Customer Flag
	 * 
	 * @param CustomerProfile customerProfile
	 * @param CustomerFlagType customerFlagType
	 * @param UserSession userSession
	 */
	@SuppressWarnings("deprecation")
	public void removeCustomerFlag(CustomerProfile customerProfile, CustomerFlagType customerFlagType,UserSession userSession) {
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CUSTOMER_FLAGS);
		
		if (msProperties.isEnabled()) {
			consumeRemoveCustomerFlag(customerProfile.getProfileId(),customerFlagType.toString(),userSession.getRestClientResourceOwner(), msProperties.getEndpoint());

		}else {
			userSession.getCustomerService().removeCustomerFlag(customerProfile, customerFlagType);
		}
	}
	
	/**
	 * Save an e-statement application
	 *
	 * @param EstatementApplication estatementApplication
	 * @param application, the Customer application
	 * @param session, the User session
	 *           
	 */
	@SuppressWarnings("deprecation")
	public void saveEstatementApplication(EstatementApplication estatementApplication,CustomerProfile customerProfile, HttpServletRequest request) {
		
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CUSTOMER_APPLICATIONS);
		
		if (msProperties.isEnabled()) {
			consumeSaveCustomerApplicationMs(estatementApplication.getApplication(), userSession, msProperties.getEndpoint());
			
		} else {
			userSession.getCustomerService().saveEstatementApplication(customerProfile, estatementApplication);
		}
		
	}

	/**
	 * Saves Phone Application
	 *
	 * @param phoneApplication, the Phone application
	 * @param request, the HTTP request
	 *           
	 */
	@SuppressWarnings("deprecation")
	public void savePhoneApplication(PhoneApplication phoneApplication, HttpServletRequest request) {
		
		if (phoneApplication != null) {
			
			UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CUSTOMER_APPLICATIONS);
			
			if (msProperties.isEnabled()) {
				consumeSaveCustomerApplicationMs(phoneApplication.getApplication(), userSession, msProperties.getEndpoint());
				
			} else {
				userSession.getCustomerService().savePhoneApplication(userSession.getCustomerProfile(), phoneApplication);
				
			}
		} else {
			LOGGER.error("Phone Application is null, can't be processed");
			
		}
	}
	
		/**
	 * Saves Address Application
	 *
	 * @param addressApplication
	 * @param request, the HTTP request
	 *           
	 */
	@SuppressWarnings("deprecation")
	public void saveAddressApplication(AddressApplication addressApplication,CustomerProfile customerProfile, HttpServletRequest request) {
		
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CUSTOMER_APPLICATIONS);
		
		if (msProperties.isEnabled()) {
			consumeSaveCustomerApplicationMs(addressApplication.getApplication(), userSession, msProperties.getEndpoint());
			
		} else {
			userSession.getCustomerService().saveAddressApplication(customerProfile, addressApplication);
			
		}
		
	}
	
	/**
	 * Saves E-payroll Application
	 *
	 * @param epayrollApplication, the E-payroll application
	 * @param request, the HTTP request
	 *           
	 */
	@SuppressWarnings("deprecation")
	public void saveEpayrollApplication(EpayrollApplication epayrollApplication, HttpServletRequest request) {
		
		if (epayrollApplication == null)
			throw new IllegalArgumentException("epayrollApplication=" + epayrollApplication);
		
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CUSTOMER_APPLICATIONS);
		
		if (msProperties.isEnabled()) {
			consumeSaveCustomerApplicationMs(epayrollApplication.getApplication(), userSession, msProperties.getEndpoint());
			
		} else {
			userSession.getCustomerService().saveEpayrollApplication(userSession.getCustomerProfile(), epayrollApplication);
			
		}
	}
	
	
	/**
	 * Get Customer Device By Profile Id
	 * 
	 * @param profileId, The profile id
	 * @param request, The HTTP request used to get the session object
	 * */
	public CustomerDeviceResponse getCustomerDeviceByProfileId(String profileId, UserSession userSession) {
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CUSTOMER_DEVICES);
		
		if(msProperties.isEnabled()) {
			LOGGER.info("Microservice is enabled: "+msProperties.getEndpoint());
			return consumeGetCustomerDeviceByProfileId(profileId, userSession.getRestClientResourceOwner(), msProperties.getEndpoint());
		}
		
		LOGGER.info("Microservice is disabled, using legacy invokation");
		return userSession.getCustomerService().getCustomerDevicesByProfileId(profileId);
		
	}
	
	/**
	 * Get Consume get customer device by profileId
	 * 
	 * @param profileId, The profileId
	 * @param client, The rest client to perform request client
	 * */
	public CustomerDeviceResponse consumeGetCustomerDeviceByProfileId(String profileId, RestClient client, String endpoint) {
		CustomerDeviceResponse deviceResponse = new CustomerDeviceResponse();
		MicroServiceResponse<CustomerDevice> response = client.getForMicroServiceResponse(endpoint.replace("{profileId}",
				profileId), null);
		
		if(response.getStatus() == OK_STATUS) {
			deviceResponse.setAllDevices(response.getData());
			deviceResponse.setResponseStatus(ResultStatus.SUCCESS);
		}
		else if(response.getStatus() == BAD_REQUEST){
			deviceResponse.setResponseStatus(ResultStatus.ERROR);
			deviceResponse.setResponseDescription("Invalid profileId");
		}
		else if(response.getStatus() == NOT_FOUND) {
			deviceResponse.setResponseStatus(ResultStatus.ERROR);
			deviceResponse.setResponseDescription("No Results");
	
		}else if(response.getStatus() == BAD_GATEWAY){
			deviceResponse.setResponseDescription(response.getErrors().get(0).getTitle());
		}
		
		return deviceResponse;
	}
	
	/**
	 * consume Get Customer Profile By Id
	 * 
	 * @param String value
	 * @param RestClient restClient
	 */
	private List<CustomerProfile> consumeGetCustomerProfileById(String value, RestClient restClient, String endpoint) {
		
		if (value == null || value.isEmpty())
			throw new IllegalArgumentException("value=" + value);

		List<CustomerProfile> customerProfiles = new ArrayList<>();

		Map<String, Object> requestMap = new HashMap<>();
		requestMap.put("search", value);

		MicroServiceResponse<CustomerProfileDto> response = restClient.getForMicroServiceResponse(endpoint, requestMap);

		if (response.getStatus() == OK_STATUS) {

			customerProfiles = CustomerModelMapperFactory.getMapper().map(response.getData(),
					new TypeToken<List<CustomerProfile>>() {
					}.getType());
			LOGGER.info("consumeGetCustomerProfileById was successful");
		} else {
			LOGGER.error("consumeGetCustomerProfileById cannot get the object. Query parameter: "+value);
		}

		return customerProfiles;
	}
	
	/**
	 * consume Get Customer Phone
	 * 
	 * @param String search
	 * @param RestClient restClient
	 * @param String  endpoint
	 */
	private CustomerPhone consumeGetCustomerPhone(String search, RestClient restClient, String endpoint) {
		
		if (search == null || search.isEmpty())
			throw new IllegalArgumentException("search=" + search);

		CustomerPhone customerPhone = null;

		Map<String, Object> requestMap = new HashMap<>();
		requestMap.put("search", search);

		MicroServiceResponse<CustomerProfileDto> response = restClient.getForMicroServiceResponse(endpoint, requestMap);

		if (response.getStatus() == OK_STATUS) {

			customerPhone = CustomerModelMapperFactory.getMapper().map(response.getData().get(0),CustomerPhone.class);
			LOGGER.info("consumeGetCustomerPhone was successful");
		} else {
			LOGGER.error("consumeGetCustomerPhone cannot get the object. Query parameter: "+search);
		}

		return customerPhone;
	}
	
	/**
	 * Update Customer Entitlements This method invokes the
	 * cibp-customer-microservice to update entitlements
	 * 
	 * @param profileId User profile
	 * @param request Entitlements to update
	 * @param restClient rest client
	 * return True if updated, False otherwise
	 */
	private boolean consumeUpdateEntitlement(String profileId, List<CustomerEntitlementDto> request,
			RestClient restClient, String endpoint) {
		boolean updated = false;

		if (profileId == null || profileId.trim().isEmpty())
			throw new IllegalArgumentException("profileId=" + profileId);

		if (request.isEmpty())
			throw new IllegalArgumentException("entitlements=" + request);

		Map<String, Object> urlVariables = new HashMap<>();
		urlVariables.put("profileId", profileId);

		MicroServiceResponse<CustomerEntitlementDto> response = restClient.putForMicroServiceResponse(
				endpoint.replace("{profileId}", profileId), request, null);

		if (response.getStatus() == ACCEPTED_STATUS_CODE) {
			LOGGER.info("update entitlement operation was succesful");
			updated = true;
		} else {
			LOGGER.error("An Exception occurred when consuming update entitlement operation with" + " Request: "
					+ ((!request.isEmpty()) ? request.get(0).getProfileId() : "") + ", Status: " + response.getStatus()
					+ " and Errors:  " + response.getErrors());
		}

		return updated;
	}
	
	/**
	 * consume Save Mbop Customer Info
	 * 
	 * @param SaveMbopCustomerInfoRequest customerInfoRequest
	 * @param RestClient restClient
	 */
	private MbopCustomerInfo consumeSaveMbopCustomerInfo(SaveMbopCustomerInfoRequest customerInfoRequest,RestClient restClient, String endpoint) {
		
		MbopCustomerInfo mbopCustomerInfo = null;
		
		if (customerInfoRequest == null)
			throw new IllegalArgumentException("MbopCustomerInfo=" + customerInfoRequest);

		// Microservice consuming
		MicroServiceResponse<MbopCustomerInfoDto> response = restClient.postForMicroServiceResponse(endpoint, customerInfoRequest, null);

		if (response.getStatus() == OK_STATUS) {
			LOGGER.info("Save MbopCustomerInfo operation was succesful");
			mbopCustomerInfo = CustomerModelMapperFactory.getMapper().map(response.getData(),
					new TypeToken<List<MbopCustomerInfo>>() {
					}.getType());
		}else {
			LOGGER.error("An Exception occurred when consuming save MbopCustomerInfo operation");

		}
	    return mbopCustomerInfo;	
	}
	
	/** 
	 * consume update Customer Signon Stats 
	 * 
	 * @param CustomerSignonStats customerSignonStats
	 * @param RestClient restClient
	 * @return boolean 
	 */
	private boolean consumeUpdateCustomerSignonStats(CustomerSignonStats customerSignonStats,RestClient restClient, String endpoint) {
		
		boolean updated = false;
		
		if (customerSignonStats == null)
			throw new IllegalArgumentException("customerSignonStatsDto cannot be null");
		
		if (customerSignonStats.getProfileId() == null || customerSignonStats.getProfileId().trim().isEmpty())
			throw new IllegalArgumentException("ProfileId cannot be null or empty");

		
		// Microservice consuming
		MicroServiceResponse<MbopCustomerInfoDto> response = restClient.putForMicroServiceResponse(endpoint, customerSignonStats, null);
		if (response.getStatus() == NO_CONTENT_STATUS) {
			LOGGER.info("Update SignonStats was succesful");
		    updated = true;
		}else {
			LOGGER.info("Update SignonStats cannot be complete");
		    updated = false;
		}
        return updated;
	
	}
	
	/**
	 * This method is used to consume the addCustomerFlag action from the customer microservice.
	 * 
	 * @param userSession This is the user session that contains the RestClient and the customer profileId.
	 * @param customerFlagType This is the customer flag type to be added to the customer.
	 * @return This returns true if the flag was saved using the microservice approach. False otherwise.
	 */
	private boolean consumeMicroserviceAddCustomerFlag(UserSession userSession, String profileId, String customerFlagType, String endpoint) {
		boolean added = false;
		
		String requestURI = endpoint.replace("{profileId}", profileId);
		
		try {
			MicroServiceResponse<CustomerFlagDto> response = userSession.getRestClientResourceOwner()
					.postForMicroServiceResponse(requestURI, customerFlagType, null);
			
			if (response.getStatus() == ACCEPTED_STATUS_CODE) {
				LOGGER.info("Customer flag added successfully.");
				added = true;
			} else {
				LOGGER.error(response.getStatus().toString() + " " + response.getErrors());
			}
		} catch (Exception exception) {
			LOGGER.error(exception.getLocalizedMessage());
		}
		
		return added;
	}
	
	/** 
	 * consume remove Customer Flag
	 * 
	 * @param String profileId
	 * @param String customerFlagType
	 * @param RestClient restClient
	 * @return boolean 
	 */
	private boolean consumeRemoveCustomerFlag(String profileId, String customerFlagType,RestClient restClient, String endpoint) {
		
		boolean removed = false;

		if (profileId == null || profileId.equals(""))
			throw new IllegalArgumentException("ProfileId cannot be Null or Empty");
		
		if (customerFlagType == null || customerFlagType.equals(""))
			throw new IllegalArgumentException("CustomerFlagType cannot be Null or Empty");
		
		// Setting URI
		String requestURI = endpoint.concat("/").concat(customerFlagType).replace("{profileId}", profileId) ;
		
		// Microservice consuming
		MicroServiceResponse<?> response = restClient.deleteForMicroServiceResponse(requestURI,null);
		
		if (response.getStatus() == NO_CONTENT_STATUS) {
			LOGGER.info("Update SignonStats was succesful");
			removed = true;
		}else {
			LOGGER.error("Microservice returned the following response for profile id: "+profileId+". "+response.toString());
			removed = false;
		}
        return removed;
	}
	
	/**
	 * Consumes the saveCustomerApplication microservice
	 *
	 * @param application, the Customer application
	 * @param session, the User session
	 *           
	 */
	private void consumeSaveCustomerApplicationMs(CustomerApplication application, UserSession session, String endpoint) {
		
		if (application != null) {
			
			// Setting URI
			String requestURI = endpoint.replace("{profileId}", session.getCustomerProfile().getProfileId());
			
			//Application set up
			application.setCustomerProfile(session.getCustomerProfile());
					
			LOGGER.info("Consuming microservice");
			// Microservice call
			MicroServiceResponse<CustomerApplicationDto> response = session.getRestClientResourceOwner().postForMicroServiceResponse(requestURI, new CustomerApplicationRequest(application), null);
					
			if (response.getStatus() == MICROSERVICE_RESPONSE_CREATED_STATUS) {
				
				LOGGER.info("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
				
			} else {
				
				LOGGER.error("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
				throw new RuntimeException(response.getErrors().toString());
			}
			
		} else {
			LOGGER.error("Customer Application is null, can't be processed");
			
		}
	}
	
	/**
	 * Method addCustomerPhone
	 *
	 * @param userSession, the UserSession
	 * @return completed, boolean value that point if the customerPhone was saved/updated or not
	 *           
	 */
	@SuppressWarnings("deprecation")
	public boolean addCustomerPhone(UserSession userSession) {
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(PUT_CUSTOMER_PHONE);
		String endpoint = msProperties.getEndpoint();
		boolean completed = false;
		
		if (msProperties.isEnabled()) {
			LOGGER.info("MICROSERVICE: ".concat(endpoint).concat(" STATUS: ENABLED"));
			completed = consumeAddCustomerPhone(userSession, endpoint);
		} else {
			LOGGER.warn("MICROSERVICE: ".concat(endpoint).concat(" STATUS: DISABLED"));
			completed = userSession.getCustomerService().addCustomerPhone(userSession.getCustomerProfile().getCustomerPhone());
		}
		return completed;
	}
	
	/**
	 * Method consumeAddCustomerPhone, This method consumes the addCustomerPhone microservice
	 *
	 * @param userSession, the UserSession
	 * @param endpoint, the microservice endpoint
	 * @return completed, boolean value that point if the customerPhone was saved/updated or not
	 *           
	 */
	private boolean consumeAddCustomerPhone(UserSession userSession, String endpoint) {
		LOGGER.info("Consuming microservice ".concat(endpoint));
		CustomerPhone customerPhone = userSession.getCustomerProfile().getCustomerPhone();
		MicroServiceResponse<CustomerApplicationDto> response = userSession.getRestClientResourceOwner().putForMicroServiceResponse(endpoint, customerPhone, null);
		boolean completed = false;
		if(response.getStatus() == NO_CONTENT_STATUS) {
			completed = true;
		}
		return completed;
	}
	
	/**
	 * Method addCustomerDevice
	 *
	 * @param userSession, the UserSession
	 * @return response, CustomerDeviceResponse object
	 *           
	 */
	@SuppressWarnings("deprecation")
	public CustomerDeviceResponse addCustomerDevice(UserSession userSession, CustomerDevice customerDevice) {
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(POST_CUSTOMER_DEVICES);
		String endpoint = msProperties.getEndpoint();
		CustomerDeviceResponse response;
		
		if (msProperties.isEnabled()) {
			response = consumeAddCustomerDevice(userSession, customerDevice, endpoint);
			
		} else {
			response = userSession.getCustomerService().addCustomerDevice(customerDevice);
		}
		return response;
	}
	
	/**
	 * Method consumeAddCustomerDevice, This method consumes the addCustomerDevice microservice
	 *
	 * @param userSession, the UserSession
	 * @param endpoint, the microservice endpoint
	 * @return response, CustomerDeviceResponse object
	 *           
	 */
	private CustomerDeviceResponse consumeAddCustomerDevice(UserSession userSession, CustomerDevice customerDevice, String endpoint) {
		LOGGER.info("Consuming microservice: ".concat(endpoint));
		CustomerDeviceResponse deviceResponse = new CustomerDeviceResponse();
		
		MicroServiceResponse<CustomerDeviceDto> response = userSession.getRestClientResourceOwner().postForMicroServiceResponse(endpoint, customerDevice, null);
		
		if(response.getStatus() == MICROSERVICE_RESPONSE_CREATED_STATUS) {
			LOGGER.info("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
			deviceResponse.setResponseStatus(ResultStatus.SUCCESS);
			
		}else {
			LOGGER.error("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
			deviceResponse.setResponseStatus(ResultStatus.ERROR);
			deviceResponse.setResponseDescription(response.getErrors().get(0).toString());
		}
		
		return deviceResponse;
	}
	
	/**
	 * Get Global Entitlement
	 * @param userSession UserSession
	 * @param entitlementType EntitlementType
	 * @return A global entitlement object
	 */
	@SuppressWarnings(DEPRECATION)
	public GlobalEntitlement getGlobalEntitlement(UserSession userSession, EntitlementType entitlementType) {

		if (entitlementType == null)
			throw new IllegalArgumentException("Null entitlement type");

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GLOBAL_ENTITLEMENT);
		GlobalEntitlement globalEntitlement;

		if (msProperties.isEnabled()) {

			globalEntitlement = consumeGetGlobalEntitlement(entitlementType, userSession.getRestClientResourceOwner(),
					msProperties.getEndpoint());

		} else {
			globalEntitlement = userSession.getCustomerService().getGlobalEntitlement(entitlementType);
		}
		return globalEntitlement;

	}
	
	/**
	 * consume Get Global Entitlement
	 *
	 * @param entitlementType
	 * @param restClient
	 * @param endpoint
	 */
	private GlobalEntitlement consumeGetGlobalEntitlement(EntitlementType entitlementType, RestClient restClient,
			String endpoint) {
		final Logger LOGGER = Logger.getLogger(CustomerFacade.class);
		GlobalEntitlement globalEntitlement = null;

		String requestURI = endpoint.replace("{entitlementType}", entitlementType.toString());

		MicroServiceResponse<GlobalEntitlementResponse> response = restClient.getForMicroServiceResponse(requestURI,
				null);

		if (response.getStatus() == OK_STATUS) {

			globalEntitlement = CustomerModelMapperFactory.getMapper().map(response.getData().get(0),
					GlobalEntitlement.class);
			LOGGER.info(StringEscapeUtils.escapeJava("consumeGetGlobalEntitlement was successful"));
		} else {
			LOGGER.error(StringEscapeUtils.escapeJava("consumeGetGlobalEntitlement cannot get the object. Query parameter: "
					+ entitlementType.toString()));
		}

		return globalEntitlement;
	}

	public Boolean saveEpayrollApplicationNew(EpayrollApplication epayrollApplication, HttpServletRequest request) {

		Boolean saved = false;

		if (epayrollApplication == null)
			throw new IllegalArgumentException("epayrollApplication=" + epayrollApplication);

		UserSession userSession = null;

		if (WebUtils.getSessionAttribute(request, "userSession") instanceof UserSession) {
			userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		}

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CUSTOMER_APPLICATIONS);

		if (msProperties.isEnabled()) {
			consumeSaveCustomerApplicationMs(epayrollApplication.getApplication(), userSession,
					msProperties.getEndpoint());

		} else {
			saved = userSession.getCustomerService().saveEpayrollApplicationNew(userSession.getCustomerProfile(),
					epayrollApplication);

		}
		return saved;
	}
}
