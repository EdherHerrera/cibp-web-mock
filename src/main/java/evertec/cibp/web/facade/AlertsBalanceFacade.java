package evertec.cibp.web.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.modelmapper.TypeToken;

import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;

import evertec.cibp.core.models.customer.CustomerBalanceAlert;
import evertec.cibp.web.form.dto.CustomerBalanceAlertDto;
import evertec.cibp.web.mapper.AlertsModelMapperFactory;
import evertec.cibp.web.session.UserSession;

/**
 * The Class AlertsBalanceFacade provides a facade to refactor existing code and
 * implementing new code for the balance alerts operations.
 * Microservices Facade abstract the switch logic from monolithic code and microservice code
 * 
 */
public class AlertsBalanceFacade {


	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger( AlertsBalanceFacade.class );

	/** The instance. */
	private static AlertsBalanceFacade instance = null;

	/**
	 * Endpoints for alerts service
	 */
	private static final String BALANCE_ALERT = "alerts.customer.balance";

	/**
	 * Successful HTTP Codes
	 */
	private static final int MICROSERVICE_RESPONSE_SUCCESS_STATUS = 200;
	private static final int MICROSERVICE_RESPONSE_NOT_CONTENT_STATUS = 204;

	/** Instantiates a new alerts facade. */
	protected AlertsBalanceFacade() {}

	/**
	 * Gets the single instance of AlertsFacade.
	 * 
	 * @return single instance of AlertsFacade
	 */
	public static AlertsBalanceFacade getInstance() {
		if ( instance == null ) {
			instance = new AlertsBalanceFacade();
		}
		return instance;
	}

	
	/**
	 * Inserts or updates a Customer Balance Alert.
	 *
	 * @param userSession, the user session.
	 * @param customerBalanceAlert, the customer balance alert to insert/update
	 * @return The inserted or updated CustomerBalanceAlert       
	 */
	public CustomerBalanceAlert insertCustomerBalanceAlert(UserSession userSession, CustomerBalanceAlert customerBalanceAlert) {
		
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(BALANCE_ALERT);

		if(msProperties.isEnabled()) {
			List<CustomerBalanceAlertDto> balanceAlerts = new ArrayList<>();
			balanceAlerts.add(new CustomerBalanceAlertDto(customerBalanceAlert));
			// Microservice call
			MicroServiceResponse<CustomerBalanceAlertDto> response = userSession.getRestClientResourceOwner().putForMicroServiceResponse(msProperties.getEndpoint(), balanceAlerts, null);
	
			if (response.getStatus() == MICROSERVICE_RESPONSE_SUCCESS_STATUS) {
				return customerBalanceAlert;
			} else {
				logger.error("The microservice call to "+msProperties.getEndpoint()+" returned an error response: "+response.toString());
				throw new RuntimeException(response.getErrors().toString());
			}
		}else {
			return userSession.getCustomerService().insertCustomerBalanceAlert(customerBalanceAlert);
		}
	}

	/**
	 * Consumes microservice to get the Customer Balance Alert that match the request.
	 *
	 * @param userSession, the user session.
	 * @param profileId, the user profile ID
	 * @return The list of customer balance alerts
	 */
	public List<CustomerBalanceAlert> getCustomerBalanceAlert(UserSession userSession, String profileId) {

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(BALANCE_ALERT);
		List<CustomerBalanceAlert> alerts = new ArrayList<>();
		if(msProperties.isEnabled()) {

			Map<String,Object> param = new HashMap<>();
			param.put("profileId", profileId);
	
			MicroServiceResponse<CustomerBalanceAlertDto> response = userSession.getRestClientResourceOwner().getForMicroServiceResponse(msProperties.getEndpoint(), param);
	
			if ( MICROSERVICE_RESPONSE_SUCCESS_STATUS == response.getStatus() ) {
				alerts = AlertsModelMapperFactory.getMapper().map( response.getData(), new TypeToken<List<CustomerBalanceAlert>>() {}.getType());
			}else {
				logger.error("The microservice call to "+msProperties.getEndpoint()+" returned an error response: "+response.toString());
			}
		}else {
			return userSession.getCustomerService().getCustomerBalanceAlert(profileId);
		}
		return alerts;
	}

	/**
	 * Consumes microservice to delete a Customer Balance Alert.
	 *
	 * @param userSession, the user session.
	 * @param customerBalanceAlert, the customer balance alert to delet
	 * @return True if deleted, false otherwise          
	 */
	public boolean deleteCustomerBalanceAlert(UserSession userSession, CustomerBalanceAlert customerBalanceAlert) {

		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(BALANCE_ALERT);
		
		if(msProperties.isEnabled()) {
			//Adding the parameter.
			Map<String,Object> param = new HashMap<>();
			param.put("alertId", customerBalanceAlert.getId());

			// Microservice call
			MicroServiceResponse<CustomerBalanceAlertDto> response = userSession.getRestClientResourceOwner().deleteForMicroServiceResponse(msProperties.getEndpoint(), param);
	
			return (response.getStatus() == MICROSERVICE_RESPONSE_NOT_CONTENT_STATUS);
		}else {
			return userSession.getCustomerService().deleteCustomerBalanceAlert(customerBalanceAlert);
		}
	}
}