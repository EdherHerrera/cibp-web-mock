package evertec.cibp.web.facade;

import bppr.tv.beans.Sort;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.factory.TVAccountFactory;
import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import bppr.tv.beans.accounts.records.CreditCardAccountRecord;
import bppr.tv.beans.accounts.types.CardAccount;
import com.evertec.cibp.api.client.model.Error;
import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.customer.CustomerSignonSource;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.AccountExpDateValidationRequest;
import evertec.cibp.core.services.requests.AccountZipCodeValidationRequest;
import evertec.cibp.core.services.requests.CardPINAccountInformationValidationRequest;
import evertec.cibp.core.services.requests.TransactionRequest;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse.AccountValidationStatus;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.models.requests.AccountDetailActionRequest;
import evertec.cibp.vs.models.requests.AccountListActionRequest;
import evertec.cibp.vs.models.requests.TransactionActionRequest;
import evertec.cibp.vs.models.requests.VaultSessionActionRequest;
import evertec.cibp.vs.models.results.AccountDetailActionResult;
import evertec.cibp.vs.models.results.AccountListActionResult;
import evertec.cibp.vs.models.results.VaultSessionActionResult;
import evertec.cibp.web.form.dto.TravelNotificationDto;
import evertec.cibp.web.mapper.CreditCardModelMapperFactory;
import evertec.cibp.web.request.TLRRSCreditRequest;
import evertec.cibp.web.request.TravelNotificationRequest;
import evertec.cibp.web.request.TsysLoyaltyRewardsRedemptionRequest;
import evertec.cibp.web.response.TLRRSCreditResponse;
import evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse;
import evertec.cibp.web.response.TsysLoyaltyRewardsRedemptionResponse;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.MicroServiceUtils;
import evertec.cibp.web.utils.Utils;
import evertec.creditcards.exception.TsysLoyaltyRewardsException;
import evertec.prophit.service.bean.TravelNotificationResponse;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.modelmapper.TypeToken;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.WebUtils;

import java.util.ArrayList;
import java.util.List;
/**
 * The Class CreditCardFacade provides a facade to refactoring existing code and
 * implementing new code for the credit card operations.
 * Microservices Facade abstract the switch logic from monolithic code and microservice code
 */
public class CreditCardFacade {
	
	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger( CreditCardFacade.class );
	/** The Constant logger. */
	private static final Logger LOGGER = Logger.getLogger(CreditCardFacade.class);
	
	/**
	 * Endpoints for credit cards service
	 */
	private static final String TRAVEL_NOTIFICATION = "creditcards.travel.notify";
	
	/**
	 * Endpoints for credit cards service current transactions
	 */
	private static final String CURRENT_TRANSACTIONS = "creditcards.transactions.current";

	/** The Constant microservice response ok status. */
	private static final int MICROSERVICE_RESPONSE_OK_STATUS = 200;

	private static final int MICROSERVICE_RESPONSE_ERROR_STATUS = 400;

	/** The instance. */
	private static CreditCardFacade instance = null;

	/** The base URL for creditcards Micro Service */
	private static final String CREDIT_CARDS_URL = ClientUtils.getMicroServiceUrl("creditcards.service.url");

	/**
	 * Endpoints for creditcards service
	 */
	private static final String GET_TSYS_LOYALTY_REWARDS_CARD_NUM_PROP_ID = "creditcards.get.tsys.loyalty.rewards.information.cards";
	private static final String GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID = "creditcards.get.tsys.loyalty.rewards.information.account.ids";
	private static final String POST_TSYS_LOYALTY_REWARDS_CARD_NUM_REDEMPTION_PROP_ID = "creditcards.post.tsys.loyalty.rewards.redemption.cards";
	private static final String POST_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_REDEMPTION_PROP_ID = "creditcards.post.tsys.loyalty.rewards.redemption.account.ids";
	private static final String GET_TLR_RECUR_STAT_CARD_NUM_PROP_ID = "creditcards.get.tsys.loyalty.rewards.automatic.redemption.cards"; //
	private static final String GET_TLR_RECUR_STAT_ACCOUNT_ID_PROP_ID = "creditcards.get.tsys.loyalty.rewards.automatic.redemption.account.ids"; //

    /**
     * Messages Values
     */
    private static final String PARAM_REQUEST_ACCOUNT_ID = "{accountId}";
    private static final String PARAM_REQUEST_CARD_NUMBER = "{cardNumber}";
    private static final String BAD_REQUEST = "Bad Request";
    private static final String ACCOUNT_CARD_NUMBER_MUST_BE_PROVIDED = "An account id or card number must be provided";
    private static final String TSYS_LOYALTY_REWARDS_MS = "consumeTsysLoyaltyRewardsAccountInfoMicroService";
    private static final String REQUEST_BODY_INVALID = "Request body is invalid";
    private static final String TSYS_LOYALTY_REDEMTION_MS = "consumeTsysLoyaltyRewardsRedemptionMicroService";
    private static final String LOG_DISABLED_MS = "This enpoint is disabled in the properties for microservices username: ";
    private static final String LOG_ACCOUNT_CARD_NUMBER_NULL= "Account id and card number are null, username = ";
    private static final String LOG_REWADS_BALANCE_METHOD = "method: redeemTsysLoyaltyRewardsBalance";
    private static final String LOG_ENTERED_TSYS_LOYALTY_ACCOUNT_MS = "Entered consumeTsysLoyaltyProgramAccountInfoMicroService with account id = %s and card number = %s";
    private static final String LOG_USER_SESSION_NULL = "UserSession is null!";
    private static final String LOG_CARD_NUMBER_NULL = "Card Number is null!";
    private static final String LOG_CONSUMING_CREDITCARD_SERVICE = "Consuming creditcards-service endpoint: ";
    private static final String LOG_ENDPOINT_DISABLED = "This enpoint is disabled in the properties";
    private static final String LOG_ENTERED_TSYS_LOYALTY_ACCOUNT_REQUEST_DTO_MS = "Entered consumeTsysLoyaltyProgramAccountInfoMicroService with accpint id = %s and card number = %s and request body = %s";
    private static final String LOG_LOYALTY_REWARDS_NULL_INVALID = "TsysLoyaltyRewardsRedemptionRequest is null or invalid!";
    private static final String CONSUME_TSYS_LOYALTY = "consumeTsysLoyaltyRewardsAccount"; //
    
	/** Instantiates a new credit cards facade. */
	protected CreditCardFacade() {}

	/**
	 * Gets the single instance of CreditCardFacade.
	 *
	 * @return single instance of CreditCardFacade
	 */
	public static CreditCardFacade getInstance() {
		if ( instance == null ) {
			instance = new CreditCardFacade();
		}
		return instance;
	}
	
	
	/**
	 * Create travel notification
	 *
	 * @param travelNotificationRequest
	 * 				the request with the information that will be used.
	 */
	@SuppressWarnings("deprecation")
	public List<TravelNotificationResponse> prophItTravelNotification(
			TravelNotificationRequest travelNotificationRequest) {
		final MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(TRAVEL_NOTIFICATION);

		if (msProperties.isEnabled()) {
			return consumeProphItTravelNotificationMicroservice(travelNotificationRequest, msProperties.getEndpoint());
		}else {
			return travelNotificationRequest.getCustomerService().prophItTravelNotification(travelNotificationRequest.getTravelInfo());
		}
				
	}
	
	/**
	 * Consumes the prophItTravelNotification microservice
	 *
	 * @param travelNotificationRequest
	 *           
	 */
	private List<TravelNotificationResponse> consumeProphItTravelNotificationMicroservice(TravelNotificationRequest travelNotificationRequest, String endpoint) {
		logger.info("Consuming microservice");
				
		List<TravelNotificationResponse> travelNotificationResponses = new ArrayList<>(0);

		// User session building
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(
				((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest(), "userSession");

		// Microservice consuming
		final MicroServiceResponse<TravelNotificationDto> response = userSession.getRestClientResourceOwner()
				.postForMicroServiceResponse(endpoint, travelNotificationRequest.getTravelInfo(), null);

		if (MICROSERVICE_RESPONSE_OK_STATUS == response.getStatus()) {
			
			travelNotificationResponses= CreditCardModelMapperFactory.getMapper().map(response.getData(),
					new TypeToken<ArrayList<TravelNotificationResponse>>() {
					}.getType());
					
			}else {
				logger.error("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
				logger.error(response.getErrors().toString());
			}
		
		return travelNotificationResponses;
	}
	
	/**
	 * Consume Tsys Loyalty Rewards Account Information from
	 * creditcards-service. User Session and card number or account id must be
	 * specified, if both parameters are specified account id will be given
	 * priority.
	 * Method to consult CASH REWARDS
	 * 
	 * @param userSession
	 *            The logged in user's session object
	 * @param accountId
	 *            A unique 11 digit number
	 * @param cardNumber
	 *            A unique 16 digit number
	 * @return MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse>
	 */
	public MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> consumeTsysLoyaltyRewardsAccountInfo(
			UserSession userSession, String accountId, String cardNumber) {
		
		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceResponse = null;
		final MicroserviceStatusResponse msRewardsAccountProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);
		final MicroserviceStatusResponse msRewardsCardNumProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_TSYS_LOYALTY_REWARDS_CARD_NUM_PROP_ID);
		
		final boolean isValid = (!GenericValidator.isBlankOrNull(accountId) && msRewardsAccountProperties.isEnabled()) 
				|| (!GenericValidator.isBlankOrNull(cardNumber) && msRewardsCardNumProperties.isEnabled());
		if (isValid) {
			microServiceResponse = consumeTsysLoyaltyRewardsAccountInfoMicroService(userSession, accountId, cardNumber);
		} else {
			microServiceResponse = new MicroServiceResponse<>();
			if (userSession != null) {
				try {
					final evertec.creditcards.response.TsysLoyaltyRewardsAccountInfoResponse response = userSession.getCustomerService().getTsysLoyaltyRewardsInfo(accountId, cardNumber);
					final List<TsysLoyaltyRewardsAccountInfoResponse> data = CreditCardModelMapperFactory
							.convertAccountInfoToAccountInfoResponse(response);
					microServiceResponse.setData(data);
					microServiceResponse.setStatus(MICROSERVICE_RESPONSE_OK_STATUS);
				} catch (TsysLoyaltyRewardsException e) {
					logger.error(e.getDetail() + userSession.getCustomerProfile().getUsername());

					final Error error = MicroServiceUtils.generateMicroServiceError(e.getStatus(), e.getTitle(),
							e.getDetail(), "consumeTsysLoyaltyRewardsAccount");

					microServiceResponse.getErrors().add(error);
					microServiceResponse.setStatus(e.getStatus());
				}
			} else {
				logger.error(LOG_USER_SESSION_NULL);

				final Error error = MicroServiceUtils.generateMicroServiceError(MICROSERVICE_RESPONSE_ERROR_STATUS, LOG_USER_SESSION_NULL,
						LOG_USER_SESSION_NULL, "consumeTsysLoyaltyRewardsAccount");

				microServiceResponse.getErrors().add(error);
				microServiceResponse.setStatus(MICROSERVICE_RESPONSE_ERROR_STATUS);
			}
		}
		
		return microServiceResponse;
	}
	
	/**
	 * Consume Tsys Loyalty Rewards Account Information from
	 * creditcards-service. User Session and card number or account id must be
	 * specified, if both parameters are specified account id will be given
	 * priority.
	 * Method to consult rewards points (PREMIA RETAIL AND COMMERCIAL).
	 * 
	 * @param userSession
	 *            The logged in user's session object
	 * @param accountId
	 *            A unique 11 digit number
	 * @param cardNumber
	 *            A unique 16 digit number
	 * @param isCommercialProfile
	 * 			  if is profile commercial or consume/retail
	 * @return MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse>
	 */
	public MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> consumeTsysLoyaltyRewardsAccountInfo(
			UserSession userSession, String accountId, String cardNumber, boolean isCommercialProfile) {
		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceResponse = null;
		final MicroserviceStatusResponse msRewardsAccountProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);
		final MicroserviceStatusResponse msRewardsCardNumProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_TSYS_LOYALTY_REWARDS_CARD_NUM_PROP_ID);
		
		final boolean isValid = (!GenericValidator.isBlankOrNull(accountId) && msRewardsAccountProperties.isEnabled()) 
				|| (!GenericValidator.isBlankOrNull(cardNumber) && msRewardsCardNumProperties.isEnabled());
		if (isValid) {
			microServiceResponse = consumeTsysLoyaltyRewardsAccountInfoMicroService(userSession, accountId, cardNumber);
		} else {
			microServiceResponse = new MicroServiceResponse<>();
			if (userSession != null) {
				try {
					final evertec.creditcards.response.TsysLoyaltyRewardsAccountInfoResponse response = userSession.getCustomerService().getTsysLoyaltyRewardsInfo(accountId, cardNumber, isCommercialProfile);
					final List<TsysLoyaltyRewardsAccountInfoResponse> data = CreditCardModelMapperFactory
							.convertAccountInfoToAccountInfoResponse(response);
					microServiceResponse.setData(data);
					microServiceResponse.setStatus(MICROSERVICE_RESPONSE_OK_STATUS);
				} catch (TsysLoyaltyRewardsException e) {
					LOGGER.error(StringEscapeUtils.escapeJava(e.getDetail() + userSession.getCustomerProfile().getUsername()));

					final Error error = MicroServiceUtils.generateMicroServiceError(e.getStatus(), e.getTitle(),
							e.getDetail(), CONSUME_TSYS_LOYALTY); //

					microServiceResponse.getErrors().add(error);
					microServiceResponse.setStatus(e.getStatus());
				}
			} else {
				LOGGER.error(StringEscapeUtils.escapeJava(LOG_USER_SESSION_NULL));
				LOGGER.error(StringEscapeUtils.escapeJava(LOG_CARD_NUMBER_NULL));

				final Error error = MicroServiceUtils.generateMicroServiceError(MICROSERVICE_RESPONSE_ERROR_STATUS, LOG_USER_SESSION_NULL,
						LOG_USER_SESSION_NULL, CONSUME_TSYS_LOYALTY); //

				microServiceResponse.getErrors().add(error);
				microServiceResponse.setStatus(MICROSERVICE_RESPONSE_ERROR_STATUS);
			}
		}
		
		return microServiceResponse;
	}	
	
	/**
	 * Consume Tsys Loyalty Rewards Account Information from
	 * cibp-creditcards-service. User Session and card number or account id must be
	 * specified, if both parameters are specified account id will be given
	 * priority.
	 * 
	 * @param userSession
	 *            The logged in user's session object
	 * @param accountId
	 *            A unique 11 digit number
	 * @param cardNumber
	 *            A unique 16 digit number
	 * @return MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse>
	 */
	private MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> consumeTsysLoyaltyRewardsAccountInfoMicroService(
			UserSession userSession, String accountId, String cardNumber) {

		// PCI compliance requires full card numbers not be logged,
		// only first 6, last 4 or a combination of both may be done.
		final String logDataInfo = String.format(LOG_ENTERED_TSYS_LOYALTY_ACCOUNT_MS, (accountId  != null ? accountId : ""),
				(cardNumber != null && cardNumber.length() > 11 ? cardNumber.substring(0, 6) + "..." + cardNumber.substring(12) : ""));
		logger.info(logDataInfo);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceResponse = new MicroServiceResponse<>();

		// Verify user session
		if (userSession == null) {

			logger.error(LOG_USER_SESSION_NULL);

			final Error error = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_UNPROCESSABLE_ENTITY,
					"Unprocessable Entity", "User session is required but non was found",
					TSYS_LOYALTY_REWARDS_MS);

			microServiceResponse.getErrors().add(error);
			microServiceResponse.setStatus(HttpStatus.SC_UNPROCESSABLE_ENTITY);

			return microServiceResponse;
		}

		// Contains flag (i.e. enabled, disabled) and an end point for the specified
		// micro service.
		MicroserviceStatusResponse msProperties = null;
		// The uri after the placeholder (i.e. /{cardNumber}/) has been substituted.
		String requestUri = "";

		if (accountId != null && !accountId.isEmpty()) {
			// Get the properties for the account id end point.
			msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);

			requestUri = msProperties.getEndpoint().replace(PARAM_REQUEST_ACCOUNT_ID, accountId);

			logger.info(LOG_CONSUMING_CREDITCARD_SERVICE + requestUri);
		} else if (cardNumber != null && !cardNumber.isEmpty()) {
			// Get the properties for the account id end point.
			msProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_TSYS_LOYALTY_REWARDS_CARD_NUM_PROP_ID);

			requestUri = msProperties.getEndpoint().replace(PARAM_REQUEST_CARD_NUMBER, cardNumber);

			// PCI compliance requires full card numbers not be logged,
			// only first 6, last 4 or a combination of both may be done.
			logger.info(LOG_CONSUMING_CREDITCARD_SERVICE + requestUri.substring(0, requestUri.length() - 11)
					+ "..." + requestUri.substring(requestUri.length() - 5));
		} else {

			logger.error(LOG_ACCOUNT_CARD_NUMBER_NULL + userSession.getCustomerProfile().getUsername());

			final Error error = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_BAD_REQUEST, BAD_REQUEST,
					ACCOUNT_CARD_NUMBER_MUST_BE_PROVIDED,
					TSYS_LOYALTY_REWARDS_MS);

			microServiceResponse.getErrors().add(error);
			microServiceResponse.setStatus(HttpStatus.SC_BAD_REQUEST);

			return microServiceResponse;
		}

		if (msProperties.isEnabled()) {
			microServiceResponse = userSession.getRestClientResourceOwner().getForMicroServiceResponse(requestUri,
					null);
		} else {
			logger.error(LOG_ENDPOINT_DISABLED + userSession.getCustomerProfile().getUsername());

			final Error error = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_NOT_IMPLEMENTED, "Not Implemented",
					"The endpoint is disabled", TSYS_LOYALTY_REWARDS_MS);

			microServiceResponse.getErrors().add(error);
			microServiceResponse.setStatus(HttpStatus.SC_NOT_IMPLEMENTED);

			return microServiceResponse;
		}

		// Could never get CreditcardsModelMapper to work
		final ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		if (microServiceResponse.getErrors().isEmpty()) {

			// Map microServiceRawResponse.getData() contains a linkedHashMap of the data it
			// needs to be converted to TsysLoyaltyRewardsAccountInfoResponse with the
			// mapper
			final TsysLoyaltyRewardsAccountInfoResponse tsysLoyaltyResponse = mapper
					.convertValue(microServiceResponse.getData().get(0), TsysLoyaltyRewardsAccountInfoResponse.class);

			// Remove the LinkedHashMap of the Data.
			microServiceResponse.getData().clear();

			// Insert the mapped TsysLoyaltyRewardsAccountInfoResponse in the response.
			microServiceResponse.getData().add(tsysLoyaltyResponse);

			return microServiceResponse;
		} else {

			// Map microServiceRawResponse.getErrors() contains a linkedHashMap of the data
			// it needs to be converted to Error with the mapper
			final Error errorResponse = mapper.convertValue(microServiceResponse.getErrors().get(0), Error.class);

			// Remove the LinkedHashMap of the Data.
			microServiceResponse.getErrors().clear();

			// Insert the mapped TsysLoyaltyRewardsAccountInfoResponse in the response.
			microServiceResponse.getErrors().add(errorResponse);

			return microServiceResponse;
		}
	}
	
	/**
	 * Consume Tsys Loyalty Rewards Account Redemption from
	 * creditcards-service (monolithic) The request body and at least one of cardNumber or
	 * accountId must be specified, if both are specified account id will be given
	 * priority.
	 * 
	 * @param userSession
	 *            The loged in user's session object
	 * @param accountId
	 *            A unique 11 digit number
	 * @param cardNumber
	 *            A unique 16 digit number
	 * @param tsysRedemptionRequest
	 * 				Redemption info for Request
	 * @return MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse>
	 */
	public MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> tsysLoyaltyRewardsRedemption(
			UserSession userSession, String accountId, String cardNumber,
			TsysLoyaltyRewardsRedemptionRequest tsysRedemptionRequest) {
		final Logger LOGGER = Logger.getLogger(CreditCardFacade.class);
		MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> microServiceResponse = new MicroServiceResponse<>();

		if (ClientUtils.getMicroserviceEndpointAndStatus(POST_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_REDEMPTION_PROP_ID)
				.isEnabled()
				|| ClientUtils.getMicroserviceEndpointAndStatus(POST_TSYS_LOYALTY_REWARDS_CARD_NUM_REDEMPTION_PROP_ID)
						.isEnabled()) {
			return consumeTsysLoyaltyRewardsRedemptionMicroService(userSession, accountId, cardNumber,
					tsysRedemptionRequest);
		}

		LOGGER.info(StringEscapeUtils.escapeJava(LOG_DISABLED_MS + userSession.getCustomerProfile().getUsername()));

		try {
			if (GenericValidator.isBlankOrNull(accountId) && GenericValidator.isBlankOrNull(cardNumber)) {
				LOGGER.error(StringEscapeUtils.escapeJava(LOG_ACCOUNT_CARD_NUMBER_NULL + userSession.getCustomerProfile().getUsername()));
				microServiceResponse.getErrors()
						.add(MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_BAD_REQUEST, BAD_REQUEST,
								ACCOUNT_CARD_NUMBER_MUST_BE_PROVIDED,
								TSYS_LOYALTY_REWARDS_MS));
				microServiceResponse.setStatus(HttpStatus.SC_BAD_REQUEST);
				return microServiceResponse;
			}

			if (tsysRedemptionRequest == null || !tsysRedemptionRequest.isValid()) {
				LOGGER.error(StringEscapeUtils.escapeJava(LOG_LOYALTY_REWARDS_NULL_INVALID));
				microServiceResponse.getErrors()
						.add(MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_BAD_REQUEST, BAD_REQUEST,
								REQUEST_BODY_INVALID, TSYS_LOYALTY_REDEMTION_MS));
				microServiceResponse.setStatus(HttpStatus.SC_UNPROCESSABLE_ENTITY);
				return microServiceResponse;
			}
			final evertec.creditcards.request.TsysLoyaltyRewardsRedemptionRequest request = CreditCardModelMapperFactory.convertToRedemptionRequest(tsysRedemptionRequest); //

			return CreditCardModelMapperFactory.convertToRedemptionResponse(
					userSession.getCustomerService().redeemTsysLoyaltyRewardsBalance(accountId, cardNumber,
							request));
		} catch (TsysLoyaltyRewardsException e) {
			microServiceResponse.getErrors().add(MicroServiceUtils.generateMicroServiceError(e.getStatus(),
					e.getTitle(), e.getDetail(), LOG_REWADS_BALANCE_METHOD));
			microServiceResponse.setStatus(e.getStatus());
			return microServiceResponse;
		}
	}

	/**
	 * Consume Tsys Loyalty Rewards Account Redemption from
	 * cibp-creditcards-service. The request body and at least one of cardNumber or
	 * accountId must be specified, if both are specified account id will be given
	 * priority.
	 * 
	 * @param userSession
	 *            The loged in user's session object
	 * @param accountId
	 *            A unique 11 digit number
	 * @param cardNumber
	 *            A unique 16 digit number
	 * @param tsysRedemptionRequestDto
	 * 			  Info redemption request
	 * @return MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse>
	 */
	private MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> consumeTsysLoyaltyRewardsRedemptionMicroService(
			UserSession userSession, String accountId, String cardNumber,
			TsysLoyaltyRewardsRedemptionRequest tsysRedemptionRequestDto) {
		final Logger LOGGER = Logger.getLogger(CreditCardFacade.class);
		// PCI compliance requires full card numbers not be logged,
		// only first 6, last 4 or a combination of both may be done.
		LOGGER.info(StringEscapeUtils.escapeJava(String.format(LOG_ENTERED_TSYS_LOYALTY_ACCOUNT_REQUEST_DTO_MS, accountId, cardNumber.substring(0, 6) + "..." + cardNumber.substring(12),
				tsysRedemptionRequestDto.toString())));

		MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> microServiceResponse = new MicroServiceResponse<>();

		// Verify user session
		if (userSession == null) {
			
			// Log error
			LOGGER.error(StringEscapeUtils.escapeJava(LOG_USER_SESSION_NULL));
			// return error
			final Error error = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_UNPROCESSABLE_ENTITY,
					"Unprocessable Entity", "User session is required but non was found",
					TSYS_LOYALTY_REDEMTION_MS);

			microServiceResponse.getErrors().add(error);
			microServiceResponse.setStatus(HttpStatus.SC_UNPROCESSABLE_ENTITY);

			return microServiceResponse;
		}

		if (tsysRedemptionRequestDto == null || !tsysRedemptionRequestDto.isValid()) {
			
			// Log error
			LOGGER.error(StringEscapeUtils.escapeJava(LOG_LOYALTY_REWARDS_NULL_INVALID));

			// return error
			final Error error = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_BAD_REQUEST, BAD_REQUEST,
					REQUEST_BODY_INVALID, TSYS_LOYALTY_REDEMTION_MS);

			microServiceResponse.getErrors().add(error);
			microServiceResponse.setStatus(HttpStatus.SC_UNPROCESSABLE_ENTITY);

			return microServiceResponse;
		}

		// Contains flag (i.e. enabled, disabled) and an end point for the specified
		// micro service.
		MicroserviceStatusResponse msProperties = null;
		
		// The uri after the placeholder (i.e. /{cardNumber}/) has been substituted.
		String requestUri = CREDIT_CARDS_URL;

		if (accountId != null && !accountId.isEmpty()) {

			msProperties = ClientUtils.getMicroserviceEndpointAndStatus(POST_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_REDEMPTION_PROP_ID);

			requestUri = msProperties.getEndpoint().replace(PARAM_REQUEST_ACCOUNT_ID, accountId);

			LOGGER.info(StringEscapeUtils.escapeJava(LOG_CONSUMING_CREDITCARD_SERVICE + requestUri));
		} else if (cardNumber != null && !cardNumber.isEmpty()) {
			
			msProperties = ClientUtils.getMicroserviceEndpointAndStatus(POST_TSYS_LOYALTY_REWARDS_CARD_NUM_REDEMPTION_PROP_ID);

			requestUri = msProperties.getEndpoint().replace(PARAM_REQUEST_CARD_NUMBER, cardNumber);

			// PCI compliance requires full card numbers not be logged,
			// only first 6, last 4 or a combination of both may be done.
			LOGGER.info(StringEscapeUtils.escapeJava(LOG_CONSUMING_CREDITCARD_SERVICE + requestUri.substring(0, requestUri.length() - 11)
					+ "..." + requestUri.substring(requestUri.length() - 5)));
		} else {
			
			// Log error
			LOGGER.error(StringEscapeUtils.escapeJava(LOG_ACCOUNT_CARD_NUMBER_NULL + userSession.getCustomerProfile().getUsername()));
			
			// return error
			final Error error = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_BAD_REQUEST, BAD_REQUEST,
					ACCOUNT_CARD_NUMBER_MUST_BE_PROVIDED,
					TSYS_LOYALTY_REWARDS_MS);

			microServiceResponse.getErrors().add(error);
			microServiceResponse.setStatus(HttpStatus.SC_BAD_REQUEST);

			return microServiceResponse;
		}

		if (msProperties.isEnabled()) {
			
			microServiceResponse = userSession.getRestClientResourceOwner().postForMicroServiceResponse(requestUri,
					tsysRedemptionRequestDto, null);
		} else {
			
			LOGGER.error(StringEscapeUtils.escapeJava(LOG_ENDPOINT_DISABLED + userSession.getCustomerProfile().getUsername()));
			
			final Error error = MicroServiceUtils.generateMicroServiceError(HttpStatus.SC_NOT_IMPLEMENTED, "Not Implemented",
					"The endpoint is disabled", TSYS_LOYALTY_REWARDS_MS);

			microServiceResponse.getErrors().add(error);
			microServiceResponse.setStatus(HttpStatus.SC_NOT_IMPLEMENTED);

			return microServiceResponse;
		}

		// Could never get CreditcardsModelMapper to work
		final ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		if (microServiceResponse.getErrors().isEmpty()) {

			// Map microServiceRawResponse.getData() contains a linkedHashMap of the data it
			// needs to be converted to TsysLoyaltyRewardsAccountInfoResponse with the
			// mapper
			final TsysLoyaltyRewardsRedemptionResponse tsysLoyaltyResponse = mapper
					.convertValue(microServiceResponse.getData().get(0), TsysLoyaltyRewardsRedemptionResponse.class);

			// Remove the LinkedHashMap of the Data.
			microServiceResponse.getData().clear();

			// Insert the mapped TsysLoyaltyRewardsAccountInfoResponse in the response.
			microServiceResponse.getData().add(tsysLoyaltyResponse);

			return microServiceResponse;
		} else {

			// Map microServiceRawResponse.getErrors() contains a linkedHashMap of the data
			// it needs to be converted to Error with the mapper
			final Error errorResponse = mapper.convertValue(microServiceResponse.getErrors().get(0), Error.class);

			// Remove the LinkedHashMap of the Errors.
			microServiceResponse.getErrors().clear();

			// Insert the mapped TsysLoyaltyRewardsAccountInfoResponse in the response.
			microServiceResponse.getErrors().add(errorResponse);

			return microServiceResponse;
		}
	}
	
	/**
	 * get the credit card current transactions.
	 * 
	 * @param request, the request with the information that will be used.
	 */
	@SuppressWarnings("deprecation")
	public TransactionsResponse getCreditCardCurrentTransactionsMicroservice(
			TransactionRequest request) {
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CURRENT_TRANSACTIONS);
		// User session building
		final UserSession userSession = (UserSession) WebUtils.getSessionAttribute(
						((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest(), "userSession");
		if (msProperties.isEnabled()) {
			return consumeCreditCardCurrentTransactionsMicroservice(request, msProperties, userSession);
		} else {
			return userSession.getCustomerService()
					.getCreditCardCurrentTransactions(request);
		}

	}
	
	/**
	 * get the credit card current transactions.
	 * 
	 * @param request, the request with the information that will be used.
	 */
	@SuppressWarnings("deprecation")
	public ValidateAccountStatusResponse validateCreditCardPinAccountInfoMicroservice(
			CardPINAccountInformationValidationRequest request) {
		MicroserviceStatusResponse msProperties = ClientUtils.getMicroserviceEndpointAndStatus(CURRENT_TRANSACTIONS);
		// User session building
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(
						((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest(), "userSession");
		if (msProperties.isEnabled()) {
			return consumeValidateCreditCardPinAccountInfoMicroservice(request, msProperties, userSession);
		} else {
			return userSession.getCustomerService()
					.validateCrediCardPINAccountInfo(request);
		}

	}
	
	/**
	 * Consumes the credit card current transactions microservice
	 */
	private TransactionsResponse consumeCreditCardCurrentTransactionsMicroservice(
			TransactionRequest request, MicroserviceStatusResponse msProperties, UserSession userSession) {
		logger.info("Consuming microservice");
		TransactionsResponse currentTransactionResponse = new TransactionsResponse();
		AbstractAccount account = request.getAccount();
		TVFrontendAccount genAct = (TVFrontendAccount) account;

		if (!genAct.getProductDefinition().getProductType().equals("CCA")) {
			currentTransactionResponse.addErrorString("Account type expected is invalid");
			return currentTransactionResponse;
		}
		genAct.setCardPlasticAccountRecords(null);
		CardAccount ccaAccount = (CardAccount) genAct.getBackendObject();
		ccaAccount.setCardPlasticAccountRecords(null);
		TransactionActionRequest requestMs = new TransactionActionRequest(false, request.getPageSize(), 
				request.getPageNumber(), Sort.ASCENDING, null, 
				ccaAccount, request.getBankingSession(), request.getStartDate());
		requestMs.getAccountRequested().setRecords(null);
		requestMs.getAccountRequested().setAccountListBalance(null);
		try {
			String requestJson = new ObjectMapper().writeValueAsString(requestMs);
		// Microservice consuming
		MicroServiceResponse<TransactionsResponse> response = userSession.getRestClientResourceOwner()
				.postForMicroServiceResponse(msProperties.getEndpoint(), requestMs, null);

		if (MICROSERVICE_RESPONSE_OK_STATUS == response.getStatus()) {
			currentTransactionResponse = CreditCardModelMapperFactory.getMapper().map(response.getData(),
					new TypeToken<TransactionsResponse>() {
					}.getType());
		} else {
			logger.error("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
			logger.error(response.getErrors().toString());
		}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return currentTransactionResponse;
	}
	
	/**
	 * Consumes the credit card current transactions microservice
	 */
	private ValidateAccountStatusResponse consumeValidateCreditCardPinAccountInfoMicroservice(
			CardPINAccountInformationValidationRequest carPinValidRequest, MicroserviceStatusResponse msProperties, UserSession userSession) {
		logger.info("Consuming microservice");
		ValidateAccountStatusResponse validateAccountStatusResponse = new ValidateAccountStatusResponse();		
		// Microservice consuming
		MicroServiceResponse<ValidateAccountStatusResponse> response = userSession.getRestClientResourceOwner()
				.postForMicroServiceResponse(msProperties.getEndpoint(), carPinValidRequest, null);
		if (MICROSERVICE_RESPONSE_OK_STATUS == response.getStatus()) {
			VaultSessionActionResult result = CreditCardModelMapperFactory.getMapper().map(response.getData(),
					new TypeToken<ArrayList<VaultSessionActionResult>>() {
					}.getType());
			boolean isAudit = false;
			ValidateAccountStatusResponse validStatus = new ValidateAccountStatusResponse();
			// Validating accountNumber with taxId and pin
			VaultSessionActionRequest request = new VaultSessionActionRequest(isAudit, carPinValidRequest.getTaxId(), carPinValidRequest.getAccountNumber(), "CARD",
					carPinValidRequest.getPin(), carPinValidRequest.isValidPin());
			validStatus.addAuditString(result.generateAuditReport());
			validStatus.addErrorString(result.generateErrorReport());
			// Validating ATH number belongs to client
			BankingSession bankingSession = result.getVaultSession();
			validStatus.setPermId(bankingSession.getPermId());
			AccountListActionRequest actListRequest = new AccountListActionRequest(isAudit, false, false);
			AccountListActionResult actListResult = userSession.getCustomerService().getPaymentService().getVaultService().
					getCustomerRelationshipAccountList(bankingSession,
					actListRequest);
			validStatus.addAuditString(actListResult.generateAuditReport());
			validStatus.addErrorString(actListResult.generateErrorReport());
			if (actListResult.getErrorCode() != 0 || actListResult.getAccountsReceived() <= 0) {
				if (carPinValidRequest.getInteractionEvent() != null
						&& carPinValidRequest.getInteractionEvent().getUsername() != null) {
					if (carPinValidRequest.getSignonSource() == null || carPinValidRequest.getSignonSource().equals(CustomerSignonSource.MI_BANCO)) {
						userSession.getCustomerService().getPaymentService().getInteractionService().logAccountValidationFailInteraction(carPinValidRequest.getInteractionEvent(),
								InteractionSeverity.WARNING);
					} else if (carPinValidRequest.getSignonSource().equals(CustomerSignonSource.MBOP)) {
						userSession.getCustomerService().getPaymentService().getInteractionService().mbopLogAccountValidationFailInteraction(
								carPinValidRequest.getInteractionEvent(), InteractionSeverity.WARNING);
					} else
						logger.error("Login Source " + carPinValidRequest.getSignonSource() + " Unknown.");
				}
				validStatus.setStatus(AccountValidationStatus.ACCOUNT_INVALID);
				return validStatus;
			}
			boolean accountBelongs = false;
			String accountType = "CCA";
			List<Account> accounts = actListResult.getAccounts();
			String productId = "";
			for (Account act : accounts) {
				if (carPinValidRequest.getAccountNumber().equals(act.getAccountID())) {
					accountBelongs = true;
					validStatus.setAccount(act);
					productId = act.getRecords().getProductId();
					accountType = act.getSubType();
					break;
				}
			}
			validStatus.setAccountProductId(productId);
			if (!accountBelongs) {
				if (carPinValidRequest.getInteractionEvent() != null
						&& carPinValidRequest.getInteractionEvent().getUsername() != null) {
					if (carPinValidRequest.getSignonSource() == null
							|| carPinValidRequest.getSignonSource() == CustomerSignonSource.MI_BANCO) {
						userSession.getCustomerService().getPaymentService().getInteractionService().logAccountValidationFailInteraction(carPinValidRequest.getInteractionEvent(),
								InteractionSeverity.WARNING);
					}  else if (carPinValidRequest.getSignonSource() == CustomerSignonSource.MBOP) {
						userSession.getCustomerService().getPaymentService().getInteractionService().mbopLogAccountValidationFailInteraction(
								carPinValidRequest.getInteractionEvent(), InteractionSeverity.WARNING);
					} else
						logger.error("Login Source " + carPinValidRequest.getSignonSource() + " Unknown.");
				}
				validStatus.setStatus(AccountValidationStatus.ACCOUNT_INVALID);
				return validStatus;
			}
			if (carPinValidRequest.isValidZipCode()) {
				AccountZipCodeValidationRequest zipReq = new AccountZipCodeValidationRequest(
						carPinValidRequest.getInteractionEvent(), carPinValidRequest.getZipCode(), carPinValidRequest.getAccountNumber(),
						carPinValidRequest.getTaxId(), accountType);
				ValidateAccountStatusResponse zipResponse = validateZipCode(zipReq, userSession);
				validStatus.addAuditString(zipResponse.getAuditString());
				validStatus.addErrorString(zipResponse.getErrorString());
				if (!zipResponse.getStatus().equals(AccountValidationStatus.VALID)) {
					validStatus.setStatus(zipResponse.getStatus());
					return validStatus;
				}
			}
			if (carPinValidRequest.isValidExpDate()) {
				AccountExpDateValidationRequest zipReq = new AccountExpDateValidationRequest(
						carPinValidRequest.getInteractionEvent(), carPinValidRequest.getAccountNumber(), carPinValidRequest.getTaxId(),
						accountType, carPinValidRequest.getExpMonth(), carPinValidRequest.getExpyear());
				ValidateAccountStatusResponse zipResponse = validateExpDate(zipReq, userSession);
				validStatus.addAuditString(zipResponse.getAuditString());
				validStatus.addErrorString(zipResponse.getErrorString());
				if (!zipResponse.getStatus().equals(AccountValidationStatus.VALID)) {
					validStatus.setStatus(zipResponse.getStatus());
					return validStatus;
				}
			}
			if (carPinValidRequest.getInteractionEvent() != null
					&& carPinValidRequest.getInteractionEvent().getUsername() != null) {
				if (carPinValidRequest.getSignonSource() == null
						|| carPinValidRequest.getSignonSource() == CustomerSignonSource.MI_BANCO)
					userSession.getCustomerService().getPaymentService().getInteractionService().logAccountValidationSuccessInteraction(carPinValidRequest.getInteractionEvent(),
							InteractionSeverity.INFO);
				else if (carPinValidRequest.getSignonSource() == CustomerSignonSource.MBOP)
					userSession.getCustomerService().getPaymentService().getInteractionService().mbopLogAccountValidationSuccessInteraction(
							carPinValidRequest.getInteractionEvent(), InteractionSeverity.INFO);
				else
					logger.error("Login Source " + carPinValidRequest.getSignonSource() + " Unknown.");

			}
			
			
			
			
			
		} else {
			logger.error("MICROSERVICE RESPONSE STATUS: " + response.getStatus());
			logger.error(response.getErrors().toString());
		}
		return validateAccountStatusResponse;
	}
	/**
	 * 
	 * @param zipCodevalRequest
	 *            - AccountZipCodeValidationRequest
	 * @return ValidateAccountStatusResponse with the Status of the validation of
	 *         zipCode
	 * 
	 *         This method search for customer information to obtain the mail
	 *         address and compare the zipCode provided and return the status of the
	 *         comparison
	 */
	public ValidateAccountStatusResponse validateZipCode(AccountZipCodeValidationRequest zipCodevalRequest, UserSession userSession) {
		ValidateAccountStatusResponse validStatus = new ValidateAccountStatusResponse();
		String accountNumber = zipCodevalRequest.getAccountNumber();
		String accountType = zipCodevalRequest.getAccountType();
		boolean isAudit = false;
		String zipCode = zipCodevalRequest.getZipCode();
		String taxId = zipCodevalRequest.getTaxId();

		try {

			VaultSessionActionRequest request = new VaultSessionActionRequest(isAudit, taxId, accountNumber,
					accountType, null, false);
			VaultSessionActionResult result = userSession.getCustomerService().getPaymentService().
					getVaultService().validateAccountInformationwithPIN(request);
			validStatus.addAuditString(result.generateAuditReport());
			validStatus.addErrorString(result.generateErrorReport());
			if (result.getErrorCode() == 1880) {
				if (zipCodevalRequest.getInteractionEvent() != null
						&& zipCodevalRequest.getInteractionEvent().getUsername() != null)
					userSession.getCustomerService().getPaymentService().getInteractionService().
					logAccountValidationBlockInteraction(zipCodevalRequest.getInteractionEvent(),
							InteractionSeverity.WARNING);

				validStatus.setStatus(AccountValidationStatus.ACCOUNT_BLOCKED);
				return validStatus;
			} else if (result.getErrorCode() != 0) {
				if (zipCodevalRequest.getInteractionEvent() != null
						&& zipCodevalRequest.getInteractionEvent().getUsername() != null)
					userSession.getCustomerService().getPaymentService().getInteractionService().
					logAccountValidationFailInteraction(zipCodevalRequest.getInteractionEvent(),
							InteractionSeverity.WARNING);
				validStatus.setStatus(AccountValidationStatus.INVALID);
				return validStatus;
			}

			// Validating ATH number belongs to client
			BankingSession bankingSession = result.getVaultSession();

			TVAccountFactory fact = new TVAccountFactory();
			Account account = fact.createAccountFromAccountCode(accountType, accountNumber);

			if (account.getSubType().equals("CCA")) {
				AccountDetailActionRequest detailRQ = new AccountDetailActionRequest(isAudit, account, false, false,
						true, true, false, true);
				AccountDetailActionResult detailRS = userSession.getCustomerService().getPaymentService().
						getVaultService().getBackEndAccountDetail(bankingSession, detailRQ);
				validStatus.addAuditString(detailRS.generateAuditReport());
				validStatus.addErrorString(detailRS.generateErrorReport());
				if (detailRS.getErrorCode() != 0) {
					validStatus.setStatus(AccountValidationStatus.ACCOUNT_INVALID);
					return validStatus;
				}
			} else if (account.getSubType().equals("CARD")) {

				CardPlasticAccountRecord rec = (CardPlasticAccountRecord) account.getRecords();
				account = rec.getAccount();
				if (account == null) {
					validStatus.setStatus(AccountValidationStatus.ACCOUNT_INVALID);
					return validStatus;
				}
				AccountDetailActionRequest detailRQ1 = new AccountDetailActionRequest(isAudit, account, false, false,
						true, true, false, false);
				AccountDetailActionResult detailRS1 = userSession.getCustomerService().getPaymentService().
						getVaultService().getBackEndAccountDetail(bankingSession, detailRQ1);
				validStatus.addAuditString(detailRS1.generateAuditReport());
				validStatus.addErrorString(detailRS1.generateErrorReport());
				if (detailRS1.getErrorCode() != 0) {
					validStatus.setStatus(AccountValidationStatus.ACCOUNT_INVALID);
					return validStatus;
				}
			}

			CreditCardAccountRecord rec = (CreditCardAccountRecord) account.getRecords();

			String zip = rec.getMailingAddress().getPostalCode();
			if (zip.length() == 5 || zip.length() > 5)
				zip = zip.substring(0, 5);

			if (!zip.equalsIgnoreCase(zipCode)) {
				validStatus.setStatus(AccountValidationStatus.INVALID_ZIPCODE);
				return validStatus;
			}

			validStatus.setStatus(AccountValidationStatus.VALID);
			return validStatus;

		} catch (Exception e) {
			validStatus.setException(e);
			throw new RuntimeException(e);
		}

	}

	/**
	 * 
	 * @param expdateValRequest
	 *            - AccountExpDateValidationRequest
	 * @return ValidateAccountStatusResponse
	 * 
	 *         This method validate the Credit Card or Plastic Account Expiration
	 *         date with the exp date provided and return the status of the
	 *         comparison.
	 */
	public ValidateAccountStatusResponse validateExpDate(AccountExpDateValidationRequest expdateValRequest, UserSession userSession) {
		ValidateAccountStatusResponse validStatus = new ValidateAccountStatusResponse();
		String accountNumber = expdateValRequest.getAccountNumber();
		String accountType = expdateValRequest.getAccountType();
		boolean isAudit = false;
		String taxId = expdateValRequest.getTaxId();
		String expMonth = expdateValRequest.getExpMonth();
		String expYear = expdateValRequest.getExpYear();

		try {

			VaultSessionActionRequest request = new VaultSessionActionRequest(isAudit, taxId, accountNumber,
					accountType, null, false);
			VaultSessionActionResult result = userSession.getCustomerService().getPaymentService().
					getVaultService().validateAccountInformationwithPIN(request);
			validStatus.addAuditString(result.generateAuditReport());
			validStatus.addErrorString(result.generateErrorReport());
			if (result.getErrorCode() == 1880) {
				validStatus.setStatus(AccountValidationStatus.ACCOUNT_BLOCKED);
				return validStatus;
			} else if (result.getErrorCode() != 0) {
				validStatus.setStatus(AccountValidationStatus.INVALID);
				return validStatus;
			}

			// Validating ATH number belongs to client
			BankingSession bankingSession = result.getVaultSession();

			TVAccountFactory fact = new TVAccountFactory();
			Account account = fact.createAccountFromAccountCode(accountType, accountNumber);

			if (account.getSubType().equals("CCA")) {
				AccountDetailActionRequest detailRQ = new AccountDetailActionRequest(isAudit, account, false, false,
						true, true, false, true);
				AccountDetailActionResult detailRS = userSession.getCustomerService().getPaymentService().
						getVaultService().getBackEndAccountDetail(bankingSession, detailRQ);
				validStatus.addAuditString(detailRS.generateAuditReport());
				validStatus.addErrorString(detailRS.generateErrorReport());
				if (detailRS.getErrorCode() != 0) {
					validStatus.setStatus(AccountValidationStatus.ACCOUNT_INVALID);
					return validStatus;
				}
			} else if (account.getSubType().equals("CARD")) {

				CardPlasticAccountRecord rec = (CardPlasticAccountRecord) account.getRecords();
				account = rec.getAccount();
				AccountDetailActionRequest detailRQ1 = new AccountDetailActionRequest(isAudit, account, false, false,
						true, true, false, false);
				AccountDetailActionResult detailRS1 = userSession.getCustomerService().getPaymentService().
						getVaultService().getBackEndAccountDetail(bankingSession, detailRQ1);
				validStatus.addAuditString(detailRS1.generateAuditReport());
				validStatus.addErrorString(detailRS1.generateErrorReport());
				if (detailRS1.getErrorCode() != 0) {
					validStatus.setStatus(AccountValidationStatus.ACCOUNT_INVALID);
					return validStatus;
				}
			}

			CreditCardAccountRecord rec = (CreditCardAccountRecord) account.getRecords();

			String zip = rec.getMailingAddress().getPostalCode();
			if (zip.length() == 5 || zip.length() > 5)
				zip = zip.substring(0, 5);

			if (!rec.getExpirationDate().equalsIgnoreCase(expYear + "-" + StringUtils.leftPad(expMonth, 2, "0"))) {
				validStatus.setStatus(AccountValidationStatus.INVALID_EXPDATE);
				return validStatus;
			}

			validStatus.setStatus(AccountValidationStatus.VALID);
			return validStatus;

		} catch (Exception e) {
			validStatus.setException(e);
			throw new RuntimeException(e);
		}

	}
	
	/**
	 *
	 * @param userSession
	 * 	      The logged in user's session object
	 * @param action
	 * 		  A method action that is needed (GET,POST,PUT or DELETE)
	 * @param accountId
	 * 		  A unique 11 digit number
	 * @param cardNumber
	 * 	      A unique 16 digit number
	 * @param request
	 * 		  A request needed for TSYS
	 * @return
	 * 		  MicroServiceResponse<TsysLoyaltyRewardsRecurringStatementCreditResponse>
	 */
	public static MicroServiceResponse<TLRRSCreditResponse> tLRRecurringStatementCredit(
			UserSession userSession, CustomerServices.TsysApiAction action, String accountId, String cardNumber,
			TLRRSCreditRequest request) {
		MicroServiceResponse<TLRRSCreditResponse> microServiceResponse = new MicroServiceResponse<>(); //

		try {
			if (userSession == null) {
				microServiceResponse = userSessionErrorResponse();
			} else if (isMicroserviceActive(accountId, cardNumber)) {
				throw new TsysLoyaltyRewardsException(); 
			} else {
				
				final  evertec.creditcards.response.TsysLoyaltyRewardsRecurringStatementCreditResponse response =
						userSession.getCustomerService().callTsysLoyaltyRewardsRecurringStatementCredit(
								action, accountId, cardNumber, CreditCardModelMapperFactory.convertRecurringStatementCreditRequest(request)); //
				
				microServiceResponse = CreditCardModelMapperFactory.convertRecurringStatementCreditResponse(response);
			}
		} catch (TsysLoyaltyRewardsException e) {
			microServiceResponse = tsysLoyaltyRewardsExceptionResponse(userSession, e);
		}
		return microServiceResponse;
	}
	
	/**
	 * @param <T>
	 * @return
	 */
	private static <T> MicroServiceResponse<T> userSessionErrorResponse() {
		MicroServiceResponse<T> microServiceResponse = new MicroServiceResponse<>(); //
		LOGGER.error(StringEscapeUtils.escapeJava("UserSession is null!"));

		final Error error = MicroServiceUtils.generateMicroServiceError(MICROSERVICE_RESPONSE_ERROR_STATUS, LOG_USER_SESSION_NULL,
				"UserSession is null!", CONSUME_TSYS_LOYALTY); //
		
		microServiceResponse.getErrors().add(error);
		microServiceResponse.setStatus(MICROSERVICE_RESPONSE_ERROR_STATUS);

		return  microServiceResponse;
	}

	/**
	 * @param accountId
	 * @param cardNumber
	 * @return isValid
	 */
	private static boolean isMicroserviceActive(String accountId, String cardNumber) {
		final MicroserviceStatusResponse msRewardsAccountProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID); //
		final MicroserviceStatusResponse msRewardsCardNumProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_TSYS_LOYALTY_REWARDS_CARD_NUM_PROP_ID); //
		final MicroserviceStatusResponse msAutomaticAccountProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_TLR_RECUR_STAT_ACCOUNT_ID_PROP_ID); //
		final MicroserviceStatusResponse msAutomaticCardNumProperties = ClientUtils.getMicroserviceEndpointAndStatus(GET_TLR_RECUR_STAT_CARD_NUM_PROP_ID); //

		final boolean accountIdCondition = (!GenericValidator.isBlankOrNull(accountId)); //
		final boolean cardNumberCondition = (!GenericValidator.isBlankOrNull(cardNumber)); //
		
		final boolean isValid = (accountIdCondition && msRewardsAccountProperties.isEnabled())
				|| (cardNumberCondition && msRewardsCardNumProperties.isEnabled()) 
				|| (accountIdCondition && msAutomaticAccountProperties.isEnabled()) 
				|| (cardNumberCondition && msAutomaticCardNumProperties.isEnabled()); //
		
		return isValid;
	}

	/**
	 * @param userSession
	 * @param e
	 * @param <T>
	 * @return
	 */
	private static <T> MicroServiceResponse<T> tsysLoyaltyRewardsExceptionResponse(UserSession userSession, TsysLoyaltyRewardsException e) {
		MicroServiceResponse<T> microServiceResponse = new MicroServiceResponse<>(); //
		LOGGER.error(Utils.neutralizeMessage(e.getDetail() + userSession.getCustomerProfile().getUsername()));
		
		final Error error = MicroServiceUtils.generateMicroServiceError(e.getStatus(), e.getTitle(),
				e.getDetail(), CONSUME_TSYS_LOYALTY); //
		
		microServiceResponse.getErrors().add(error);
		microServiceResponse.setStatus(e.getStatus());

		return microServiceResponse;
	}
	
}
