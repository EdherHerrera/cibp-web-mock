package evertec.cibp.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.web.view.SignaturecardView;

public class NoCacheMultiactionController extends MultiActionController {
	/** Logger */
	protected final Log logger = LogFactory.getLog(getClass());

	public ModelAndView eaccountsignaturecard(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String pendingAccountId = ServletRequestUtils.getRequiredStringParameter(request, "id");
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		List<TVFrontendAccount> accounts = userSession.getAccounts();
		TVFrontendAccount account = Utils.getAccountById(pendingAccountId, accounts);
		Map<String, Object> model = new HashMap<>();

		TVFrontendAccount accountDetails = (TVFrontendAccount) customerService.getAccountDetails(customer,
				userSession.getBankingSession(), account, true);
		if (accountDetails.getTVAccount().getRecords().getOwners() != null) {
			model.put("account", account);
			model.put("accountdetails", accountDetails);
			return new ModelAndView(new SignaturecardView(), model);
		}
		return new ModelAndView("error");
	}

}
