package evertec.cibp.web.propertyeditors;

import java.beans.PropertyEditorSupport;

import evertec.cibp.vs.beans.Frequency;

public class DefaultFrequencyEditor extends PropertyEditorSupport {

	public void setAsText(String s) throws IllegalArgumentException {
		setValue(Frequency.valueOf(s.toUpperCase()));
	}

	public String getAsText() {
		Frequency frequency = (Frequency) getValue();
		if (frequency != null) {
			return frequency.name();
		}
		return Frequency.MANUALLY.name();

	}

}
