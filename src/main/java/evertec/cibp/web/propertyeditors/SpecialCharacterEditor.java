package evertec.cibp.web.propertyeditors;

import java.beans.PropertyEditorSupport;

import org.apache.commons.lang.StringEscapeUtils;

import evertec.cibp.core.utils.InternationalCharacterUtils;

/**
 * Property editor that eliminates special characters and extra spaces from
 * Strings. Needs to be explictly registered, e.g. for command binding.
 */
public class SpecialCharacterEditor extends PropertyEditorSupport {

	private boolean allowAccents = false;
	private boolean allowSpecialCharacters = false;
	private boolean escapeSpecialCharacters = false;

	public void setAsText(String text) {
		if (text == null) {
			setValue(null);
		} else {
			String value = text.trim();

			value = InternationalCharacterUtils.sanatizeInputs(value, allowAccents, allowSpecialCharacters);
			setValue(value);
		}
	}

	public String getAsText() {
		Object value = getValue();
		if (value != null) {
			String escapeHtml = escapeSpecialCharacters ? StringEscapeUtils.escapeHtml(value.toString()) : value
					.toString();
			return escapeHtml;
		} else
			return "";

	}

	public SpecialCharacterEditor(boolean allowAccents, boolean allowSpecialCharacters, boolean escapeCharacters) {
		super();
		this.allowAccents = allowAccents;
		this.escapeSpecialCharacters = escapeCharacters;
		this.allowSpecialCharacters = allowSpecialCharacters;
	}

}
