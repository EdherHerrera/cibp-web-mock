package evertec.cibp.web.filters;

import evertec.cibp.web.filters.writers.ContentSecurityPolicyWriter;
import evertec.cibp.web.filters.writers.ReferrerPolicyWriter;
import org.apache.log4j.Logger;
import org.springframework.security.web.header.HeaderWriter;
import org.springframework.security.web.header.writers.HstsHeaderWriter;
import org.springframework.security.web.header.writers.XContentTypeOptionsHeaderWriter;
import org.springframework.security.web.header.writers.XXssProtectionHeaderWriter;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;
import org.springframework.util.Assert;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * The WebConfig annotation class on spring framework
 *
 * @author Leonardo Flores (leonardo.flores@evertec.com)
 * @version 1.8
 * @created 2020-08-14 17:35
 * @since 2.0
 */
public class SecurityFiltersChain implements Filter {
    public static final String CONTENT_SECURITY_POLICY_PARAM = "contentSecurityPolicy";
    /**
     * Strict Transport Security Max Age parameter
     */
    public static final String STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM = "strictTransportSecurityMaxAge";
    /**
     * Strict Transport Security Include Subdomains
     */
    public static final String STRICT_TRANSPORT_SECURITY_SUBDOMAINS_PARAM = "strictTransportSecuritySubdomains";

    public static final String XFRAME_OPTIONS_MODE_PARAM = "xFrameOptionsMode";

    public static final String REFERRER_POLICY_PARAM = "referrerPolicy";

    /**
     * constant logger
     **/
    public static final Logger logger = Logger.getLogger(SecurityFiltersChain.class);

    private List<HeaderWriter> writers;
    private String contentSecurityPolicyDirective;
    private Long strictTransportSecurityMaxAge;
    private Boolean strictTransportSecuritySubdomains;
    private String xFrameOptionsMode;
    private String referrerPolicy;

    /**
     * doFilter
     *
     * @param request  ServletRequest
     * @param response ServletResponse
     * @param chain    FilterChain
     * @throws IOException      throws
     * @throws javax.servlet.ServletException throws
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        Assert.notNull(writers);
        Assert.isInstanceOf(HttpServletRequest.class, request);
        Assert.isInstanceOf(HttpServletResponse.class, response);
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        writers.forEach(writer -> writer.writeHeaders(req, res));
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Check required parameters
        Assert.notNull(filterConfig.getInitParameter(CONTENT_SECURITY_POLICY_PARAM)
                ,String.format("The parameter %s is required",CONTENT_SECURITY_POLICY_PARAM));
        Assert.notNull(filterConfig.getInitParameter(STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM)
                ,String.format("The parameter %s is required",STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM));

        this.contentSecurityPolicyDirective = filterConfig.getInitParameter(CONTENT_SECURITY_POLICY_PARAM);
        this.strictTransportSecurityMaxAge = Long.parseLong(filterConfig.getInitParameter(STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM));
        this.strictTransportSecuritySubdomains =
                Objects.isNull(filterConfig.getInitParameter(STRICT_TRANSPORT_SECURITY_SUBDOMAINS_PARAM)) || Boolean.parseBoolean(filterConfig.getInitParameter(STRICT_TRANSPORT_SECURITY_SUBDOMAINS_PARAM));
        this.xFrameOptionsMode = Objects.isNull(filterConfig.getInitParameter(XFRAME_OPTIONS_MODE_PARAM)) ?
                "DENY" : filterConfig.getInitParameter(XFRAME_OPTIONS_MODE_PARAM);
        this.referrerPolicy = Objects.isNull(filterConfig.getInitParameter(REFERRER_POLICY_PARAM)) ? null : filterConfig.getInitParameter(REFERRER_POLICY_PARAM);
        writers = this.createHeaderWriters();
    }

    protected List<HeaderWriter> createHeaderWriters() {
        List<HeaderWriter> writers = new LinkedList<>();
        writers.add(contentSecurityPolicyWriter());
        writers.add(frameOptionsHeaderWriter());
        writers.add(xContentTypeOptionsHeaderWriter());
        writers.add(xXssProtectionHeaderWriter());
        writers.add(hstsHeaderWriter());
        writers.add(referrerPolicyWriter());
        return writers;
    }

    private HeaderWriter contentSecurityPolicyWriter() {
        return new ContentSecurityPolicyWriter(this.contentSecurityPolicyDirective);
    }

    private HeaderWriter frameOptionsHeaderWriter() {
        XFrameOptionsHeaderWriter frameOptionsHeaderWriter;
        switch (this.xFrameOptionsMode) {
            case "ALLOW_FROM":
                frameOptionsHeaderWriter = new XFrameOptionsHeaderWriter(XFrameOptionsHeaderWriter.XFrameOptionsMode.ALLOW_FROM);
                break;
            case "SAMEORIGIN":
                frameOptionsHeaderWriter = new XFrameOptionsHeaderWriter(XFrameOptionsHeaderWriter.XFrameOptionsMode.SAMEORIGIN);
                break;
            default:
                frameOptionsHeaderWriter = new XFrameOptionsHeaderWriter(XFrameOptionsHeaderWriter.XFrameOptionsMode.DENY);
                break;
        }
        return frameOptionsHeaderWriter;
    }

    private HeaderWriter xContentTypeOptionsHeaderWriter() {
        return new XContentTypeOptionsHeaderWriter();
    }

    private HeaderWriter xXssProtectionHeaderWriter() {
        XXssProtectionHeaderWriter xXssProtectionHeaderWriter = new XXssProtectionHeaderWriter();
        xXssProtectionHeaderWriter.setEnabled(true);
        return xXssProtectionHeaderWriter;
    }

    private HeaderWriter hstsHeaderWriter() {
        return new HstsHeaderWriter(httpServletRequest ->
                true, this.strictTransportSecurityMaxAge, this.strictTransportSecuritySubdomains);
    }

    private HeaderWriter referrerPolicyWriter(){
        ReferrerPolicyWriter writer = new ReferrerPolicyWriter();
        if(!Objects.isNull(this.referrerPolicy)){
            writer = new ReferrerPolicyWriter(this.referrerPolicy);
        }
        return writer;
    }

    @Override
    public void destroy() {

    }
}
