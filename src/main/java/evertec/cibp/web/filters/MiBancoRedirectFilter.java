package evertec.cibp.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.owasp.esapi.errors.AccessControlException;
import org.owasp.esapi.reference.DefaultHTTPUtilities;

/**
 * MiBancoRedirectFilter
 * 
 * @author et55596
 * @since 1.2
 * @version 1.0
 */
public class MiBancoRedirectFilter implements Filter {

	protected final Log logger = LogFactory.getLog(getClass());

	public void destroy() {

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		DefaultHTTPUtilities utilities = new DefaultHTTPUtilities();

		String uri = req.getRequestURI();
		String p = req.getQueryString();

		boolean englishSsds = uri.startsWith("/servlet/bppr/online/2000/eng/ssds-enroll.html");
		boolean spanishSsds = uri.startsWith("/servlet/bppr/online/2000/esp/ssds-enroll.html");
		boolean oldcibp = uri.startsWith("/cibp-web");

		HttpServletResponse res = (HttpServletResponse) response;
		try {
			if (englishSsds) {
				utilities.sendRedirect("/actions/ssdsenrollment?" + p);
				return;
			} else if (spanishSsds) {
				utilities.sendRedirect("/actions/ssdsenrollment?" + p + "&lang=es");
				return;
			} else if (oldcibp) {
				utilities.sendRedirect("/actions/login" + p);
				return;
			}
		} catch (AccessControlException | IOException e) {
			logger.error("Error", e);
		}
		
		
		boolean englishmb2 = uri.startsWith("/servlet/bppr/online/2000/eng");
		boolean spanishmb2 = uri.startsWith("/servlet/bppr/online/2000/esp");
		res = (HttpServletResponse) response;
		try {
			if (englishmb2) {
				utilities.sendRedirect(res, "/actions/login");
			} else if (spanishmb2) {
				utilities.sendRedirect(res,"/actions/login?lang=es");
			} else {
				chain.doFilter(req, res);
			}
		} catch (AccessControlException | IOException e) {
			logger.error("Error", e);
		}
	}

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
