package evertec.cibp.web.filters;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.owasp.esapi.ESAPI;
/**
 * CacheableContentFilter
 * @author et55596
 * @since 1.2
 * @version 1.0
 */
public class CacheableContentFilter implements Filter {	
	
    /**constant logger **/
	public static final Logger logger = Logger.getLogger(CacheableContentFilter.class);

	/**
	 * destroy
	 */
	public void destroy() {
		// TODO Auto-generated method stub

	}

	/**
	 * doFilter
	 * @param request ServletRequest
	 * @param response ServletResponse
	 * @param chain FilterChain
	 * @throws IOException throws
	 * @throws ServletException throws
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 6);

		long timeInMillis = cal.getTimeInMillis();
		setContentType(res, "Expires", Long.toString(timeInMillis));
		setContentType(res, "Cache-Control", "max-age=360000, must-revalidate, public");

		if (logger.isDebugEnabled()) {
			logger.debug(StringEscapeUtils.escapeJava("Sending Cache to " + request.getRemoteAddr() + "  for:" + req.getRequestURI()));
		}
		
		chain.doFilter(request, response);

	}

	/**
	 * init
	 * @param arg0 FilterConfig
	 * @throws ServletException throws
	 */
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

	public void setContentType(HttpServletResponse response, String contentType, String contentValue) {
		ESAPI.httpUtilities().setHeader(response, contentType, contentValue);
	}

}
