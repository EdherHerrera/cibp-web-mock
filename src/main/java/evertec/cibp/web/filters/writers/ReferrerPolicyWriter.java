package evertec.cibp.web.filters.writers;

import org.springframework.security.web.header.HeaderWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * This class implements the writer for Referrer-Policy header
 * This writer has been added because is not supported in current spring-security 4.0.4.RELEASE version
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy
 *
 * @author Leonardo Flores (leonardo.flores@evertec.com)
 * @version 1.8
 * @see org.springframework.security.web.header.HeaderWriter
 * @since 2.0
 */
public class ReferrerPolicyWriter implements HeaderWriter {
    private static final String HEADER_NAME = "Referrer-Policy";
    private ReferrerPolicyType policyType;

    public ReferrerPolicyWriter(){
        this.policyType = ReferrerPolicyType.NO_REFERRER_WHEN_DOWNGRADE;
    }

    public ReferrerPolicyWriter(String policyType){
        ReferrerPolicyType.fromString(policyType);
    }

    @Override
    public void writeHeaders(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        httpServletResponse.addHeader(HEADER_NAME, this.policyType.name);
    }

    public enum ReferrerPolicyType {
        NO_REFERRER("no-referrer"),
        NO_REFERRER_WHEN_DOWNGRADE("no-referrer-when-downgrade"),
        ORIGIN("origin"),
        ORIGIN_WHEN_CROSS_ORIGIN("origin-when-cross-origin"),
        SAME_ORIGIN("same-origin"),
        STRICT_ORIGIN("strict-origin"),
        STRICT_ORIGIN_WHEN_CROSS_ORIGIN("strict-origin-when-cross-origin"),
        UNSAFE_URL("unsafe-url");

        protected String name;

        ReferrerPolicyType(String parameterName) {
            this.name = parameterName;
        }

        public String getParameterName() {
            return this.name;
        }

        public static ReferrerPolicyType fromString(String s) throws IllegalArgumentException {
            return Arrays.stream(ReferrerPolicyType.values())
                    .filter(v -> v.name.equals(s))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("unknown value: " + s));
        }
    }
}
