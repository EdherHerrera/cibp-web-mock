package evertec.cibp.web.filters.writers;

import org.springframework.security.web.header.HeaderWriter;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * This class implements the writer for Content-Security-Policy header
 * This writer has been added because is not supported in current spring-security 4.0.4.RELEASE version
 *
 * @author Leonardo Flores (leonardo.flores@evertec.com)
 * @version 1.8
 * @see org.springframework.security.web.header.HeaderWriter
 * @since 2.0
 */
public class ContentSecurityPolicyWriter implements HeaderWriter {
    private static final String CONTENT_SECURITY_POLICY = "Content-Security-Policy";
    private static final String VALID_DIRECTIVES = "[\\w-]+[^/]\\s[\\w-'\\s:/*.;]+";
    private static final String[] DIRECTIVES_KEYS = new String[]{"base-uri", "script-src", "frame-ancestors"};

    private final String directives;

    /**
     * The implementation has been synchronized with next versions of spring-security and this has been used as sample
     * https://docs.spring.io/spring-security/site/docs/4.2.x/reference/html/headers.html#headers-csp-configure
     *
     * @param directives List of directives to apply
     * @implSpec https://docs.spring.io/spring-security/site/docs/4.2.x/reference/html/headers.html#headers-csp-configure
     */
    public ContentSecurityPolicyWriter(String directives) {
        //script-src https://trustedscripts.example.com; report-uri /csp-report-endpoint/
        this.directives = directives;
        this.validateDirectives();
    }
    @Override
    public void writeHeaders(HttpServletRequest request, HttpServletResponse response) {
        response.addHeader(CONTENT_SECURITY_POLICY, this.directives);
    }

    /**
     * Validate all directives format and keys
     */
    private void validateDirectives() {
        Assert.isTrue(this.directives.matches(VALID_DIRECTIVES));
        String[] directives = this.directives.split(";");
        Arrays.stream(directives).forEach(this::validateDirective);
    }

    /**
     * Validate one directive
     *
     * @param directive Full directive to check
     */
    private void validateDirective(String directive) {
        this.checkStartDirective(directive);
        this.checkDirectivesWithoutSemicolon(directive);
        this.checkKnowDirective(directive);
        this.checkValidKeywords(directive);
    }

    private void checkKnowDirective(String directive) {
        String[] keyName = directive.split("\\s");
        String[] notFound = Arrays.stream(DIRECTIVES_KEYS)
                .filter(name -> keyName[0].equals(name))
                .toArray(String[]::new);
        Assert.isTrue(notFound.length == 1, String.format("The directive %s is unknown", keyName[0]));
    }

    /**
     * Check if the directive is found at start and if it's a known directive
     *
     * @param directive
     */
    private void checkStartDirective(String directive) {
        Assert.isTrue(directive.matches("^[\\w-]+[^;].*"));
    }

    private void checkDirectivesWithoutSemicolon(String directive){
        String[] keyName = directive.split("\\s");
        String[] notFound = Arrays.stream(DIRECTIVES_KEYS)
                .filter(name -> directive.contains(name))
                .toArray(String[]::new);
        Assert.isTrue(notFound.length == 1, String.format("I found %s directives [%s] in same directive, maybe you forgot de semicolon ';'. Please check web.xml config",notFound.length,
                Arrays.stream(notFound)
                .map(Object::toString)
                .collect(Collectors.joining("|"))));
    }

    private void checkValidKeywords(String directive) {
        Pattern p = Pattern.compile("'([\\w-]+)'");
        Matcher m = p.matcher(directive);
        List<String> errors = new LinkedList<String>();
        while (m.find()) {
            for (int i = 0; i < m.groupCount(); i++) {
                if (!m.group(i).matches("'(self|unsafe-eval|unsafe-inline|unsafe-hashes)'")) {
                    errors.add(m.group(i));
                }
            }
        }
        Assert.isTrue(errors.size() == 0, String.format("The directive '%s' contains invalid keywords [%s]"
                , directive, errors.stream()
                        .map(Object::toString)
                        .collect(Collectors.joining("|"))));
    }
}
