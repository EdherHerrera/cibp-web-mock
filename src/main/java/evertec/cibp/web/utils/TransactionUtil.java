package evertec.cibp.web.utils;

import evertec.cibp.core.models.base.AbstractTransaction;
import joptsimple.internal.Strings;
import org.apache.commons.collections.MapIterator;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections.map.MultiKeyMap;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class TransactionUtil {

	public String cleanDescriptionForIphone(String in) {

		return in.replaceAll("PURCHASE", "");
	}

	/**
	 * This method cast abstract transaction object in transaction detail to add new attributes.
	 *
	 * @author ET58344, Evertec Inc.
	 * @since 08-27-2021
	 * @param transactions abstract transactions list
	 * @param transactionCodes codes
	 * @param transactionsTypeText
	 */
	public static void getTransactions(List<AbstractTransaction> transactions,
									   String transactionCodes, String transactionsTypeText){
		MultiKeyMap transactionCodesMap = initializeTransactionCodeReasonMap(transactionCodes); //transaction codes map

		if (transactions != null){

			if(!transactions.isEmpty()){

				for(AbstractTransaction eachAbstractTran : transactions){
					
					if (isDescriptionInTransactionsTypeText(eachAbstractTran.getDescription(), transactionsTypeText)) {
						eachAbstractTran.setShowDetailEnabled(
								isShowDetailEnabled(eachAbstractTran.getTransactionCode() == null ? Strings.EMPTY :
										eachAbstractTran.getTransactionCode(), transactionCodesMap));
					}else
						eachAbstractTran.setShowDetailEnabled(Boolean.FALSE);
					

					if(eachAbstractTran.getTransactionCode() == null){
						eachAbstractTran.setTransactionCode(Strings.EMPTY);
					}
				}
			}
		}
	}

	/**
	 * Transaction code-reason map initialization.
	 *
	 * @author ET58344, Evertec Inc.
	 * @since 08-30-2021
	 * @param transactionCodes codes
	 * @return Map<String, String> result map
	 */
	private static MultiKeyMap initializeTransactionCodeReasonMap(String transactionCodes){
		final String comaStr = ","; //coma string
		final String line = "-"; //guion string
		final int one = 1; //one int
		final String[] codes = transactionCodes.split(comaStr); //codes
		String[] codeReason; //code reason
		final MultiKeyMap transactionCodeReasonMap = new MultiKeyMap(); //code reason map

		for(String eachCodeReason : codes) {
			if (!Strings.EMPTY.equals(eachCodeReason)) {
				codeReason = eachCodeReason.split(line);

				if (codeReason.length > one) {
					transactionCodeReasonMap.put(codeReason[0], codeReason[1], codeReason[1]);
				} else {
					transactionCodeReasonMap.put(codeReason[0], Strings.EMPTY, Strings.EMPTY);
				}
			}
		}
		return transactionCodeReasonMap;
	}

	/**
	 * Set the status of activate-deactivate chevron for apps transaction detail in each row.
	 *
	 * @author ET58344, Evertec Inc.
	 * @since 08-30-2021
	 * @param transactionCode code
	 * @param transactionCodesMap transaction codes map
	 * @return boolean result
	 */
	private static boolean isShowDetailEnabled(String transactionCode, MultiKeyMap transactionCodesMap){
		final Pattern patternNumeric = Pattern.compile("-?\\d+(\\.\\d+)?"); //numeric patter
		final MapIterator transCodesIterator = transactionCodesMap.mapIterator(); //iterator
		MultiKey eachCodeReason = new MultiKey(Strings.EMPTY, Strings.EMPTY); //multi key
		boolean result; //final result

		while(transCodesIterator.hasNext()){
			transCodesIterator.next();

			if(transCodesIterator.getKey() instanceof MultiKey){
				eachCodeReason = (MultiKey) transCodesIterator.getKey();
			}
			result = isShowDetailEnabledValidation(patternNumeric, transactionCode,
					eachCodeReason);

			if(result){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Set the status of activate-deactivate chevron for apps transaction detail in each row, method separated
	 * for kiuwan cyclomatic complexity.
	 *
	 * @author ET58344, Evertec Inc.
	 * @since 09-17-2021
	 * @param patternNumeric pattern
	 * @param transactionCode code
	 * @param eachCodeReason code reason
	 * @return boolean result
	 */
	private static boolean isShowDetailEnabledValidation(Pattern patternNumeric, String transactionCode,
														 MultiKey eachCodeReason){

		if(patternNumeric.matcher(transactionCode).matches() ){

			if(Integer.parseInt(eachCodeReason.getKey(0).toString()) == Integer.parseInt(transactionCode) ){
				return Boolean.TRUE;
			}
		} else {

			if(eachCodeReason.getKey(0).equals(transactionCode)){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	
	private static boolean isDescriptionInTransactionsTypeText (String transDescription, String transactionDetailType) {
		
		final String desc = transDescription; // description
		
		String[] transactionsType = transactionDetailType.split(","); // Transaction Type
				
		return Arrays.stream(transactionsType).anyMatch(desc::contains);
	}
}