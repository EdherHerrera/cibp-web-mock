package evertec.cibp.web.utils;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.validator.GenericValidator;
/**
 * AccountMaskUtil
 * @author et55596
 * @since 1.2
 * @version 1.0 
 */
public class AccountMaskUtil {
	
	public static final String ATH_REGULAR = "REG";
	public static final String ATH_INTERNATIONAL = "INT";
	public static final String ATH_UNDEFINED = "UND";
	public static final String DISCOVER = "DIS";

	public String mask(String account) {
		if (account != null && account.length() > 4) {
			String accountmask = "x";
			return accountmask + account.substring(account.length() - 4, account.length());
		} else
			return account;
	}

	public String apiAccountKey(String account) {
		if (account == null)
			return null;
		if (account.equals(""))
			return "";
		return DigestUtils.sha512Hex(account + "F:Gs`(=3");
	}

	/*
	 * ORIGINAL METHOD
	 * 
	 * public String mask(String account){ if (account !=null &&
	 * account.length()>4) { String accountmask = ""; for (int i = 0; i <
	 * account.length()-4; i++) { accountmask += "x"; } return accountmask +
	 * account.substring(account.length()-4, account.length()); }else return
	 * account; }
	 */
	
	public String mask7(String account) {
		if (account != null && account.length() > 7) {
			String accountmask = "x";
			return accountmask + account.substring(account.length() - 7, account.length());
		} else
			return account;
	}
	
	public String ATHType(String plastic) {
		if (plastic.startsWith("0215")) {
			return AccountMaskUtil.ATH_REGULAR;
		} 
		else if (plastic.startsWith("4549")) {
			return AccountMaskUtil.ATH_INTERNATIONAL;
		} 
		else if (plastic.startsWith("60119052")) {
			return AccountMaskUtil.DISCOVER;
		}
		else {
			return AccountMaskUtil.ATH_UNDEFINED;
		}
	}
	
	//MBSFE 297
	public boolean ccaPCB(String account, String pcbAthNotEbillAllowed) {
		boolean showSection = true;
		String currentAccount = account.substring(0, 6);
		String[] accountsNotAllowed = pcbAthNotEbillAllowed.split(",");

		if(account.startsWith("0000")) {
			currentAccount = account.substring(4, 10);
		}
		
		try{
			for (int j = 0; j < accountsNotAllowed.length; ++j) {
				if(currentAccount.equals(accountsNotAllowed[j])){
					showSection=false;
				}
			}
		} catch (Exception e) {
			System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
		}
		return showSection;
	}
	//END MBSFE 297

	/**
	 * This method return card last 4 digits.
	 * @param account account number
	 * @return String result
	 * @since 08-16-2022
	 * @author Popular Inc.
	 */
	public String mask4(String account) {
		if (account != null && account.length() > 4) {
			return account.substring(account.length() - 4);
		} else
			return account;
	}
}
