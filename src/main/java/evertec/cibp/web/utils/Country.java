/**
 * author jocampo
 */
package evertec.cibp.web.utils;

public class Country implements Comparable<Country>{
	
	private String code;
	private String label;
	
	public Country(){}
	
	public Country(String code, String label){
		this.code = code;
		this.label = label;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	public int compareTo(Country countryC) {
		return this.getLabel().compareTo(countryC.getLabel());

	}

}
