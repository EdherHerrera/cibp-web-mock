package evertec.cibp.web.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import evertec.cibp.core.models.payment.TVPayment;

public class PaymentSortbyAccount {

	private LinkedHashMap<String, List<TVPayment>> elementMap = new LinkedHashMap<String, List<TVPayment>>();

	public Integer addPayment(TVPayment payment) {

		String key = payment.getSource().getAccount().getAccountKey();
		List<TVPayment> list = elementMap.get(key);
		if (list == null) {
			list = new ArrayList<TVPayment>();
			list.add(payment);
			elementMap.put(key, list);
			return 1;
		} else {

			list = elementMap.get(key);
			list.add(payment);
			return list.size();
		}

	}

	public List<List<TVPayment>> getElementMap() {

		List<List<TVPayment>> l = new LinkedList<List<TVPayment>>();
		l.addAll(elementMap.values());
		return l;

	}

}