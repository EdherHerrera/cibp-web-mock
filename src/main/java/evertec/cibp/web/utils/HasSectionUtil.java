package evertec.cibp.web.utils;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import edu.emory.mathcs.backport.java.util.Arrays;
import evertec.cibp.core.models.account.AccountsSectionOverwrite;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerSectionOverwrite;
import evertec.cibp.core.models.profile.ProfileTypeEntitlement;
import evertec.cibp.core.models.profile.ProfileTypeSection;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.UserSession;

public class HasSectionUtil {
	
	private static final Logger logger = Logger.getLogger(HasSectionUtil.class);
	public static final int FRAUD_PREVENTION_SERVICE_SECTION= 1;
	
	public static boolean hasSection(UserSession session, Integer sectionId) {		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.SECTION_MANAGEMENT)){
			if (session == null || session.getCustomerProfile() == null || session.getCustomerProfile().getProfileType() == null || 
					session.getCustomerProfile().getProfileType().getProfileTypeEntitlements() == null) {
				logger.error("ERROR: HasSectionUtil hasSection null value");
				return false;
			}
			if(session.getProfileTypeSections().isEmpty())
			{
				iteraSections(session);
			}
		}		
		return session.getProfileTypeSections().containsKey(sectionId);
	}
	
	public static boolean hasSectionPath(UserSession session, String pathSection) {
		if (session == null || session.getCustomerProfile() == null || session.getCustomerProfile().getProfileType() == null || 
				session.getCustomerProfile().getProfileType().getProfileTypeEntitlements() == null) {
			logger.error("ERROR: HasSectionUtil hasSection null value");
			return false;
		}
		else
			return session.getSectionPath().containsValue(pathSection);		
	}
	
	@SuppressWarnings("unchecked")
	public static void iteraSections(UserSession session){
		CustomerProfile profile = session.getCustomerProfile();
		List<String> notAllowedSectionsbyProfile = new LinkedList<>();
		List<String> notAllowedSectionsbyCustomer = new LinkedList<>();
		//List<String> allowedSectionsbyCustomer = new LinkedList<>();
		
		//For NotAllow Sections by Profile Type
		for (ProfileTypeEntitlement ent : profile.getProfileType().getProfileTypeEntitlements()) {
			if (ent == null || ent.getGlobalEntitlementType() == null || ent.getGlobalEntitlementType().getProfileTypeSection() == null ||
					ent.getGlobalEntitlementType().getProfileTypeSection().isEmpty()) {
				logger.error("ERROR: HasSectionUtil hasSection null value in ProfileTypeEntitlement Loop");
				continue;
			}

			Set<ProfileTypeSection> sections = ent.getGlobalEntitlementType().getProfileTypeSection();
			if (sections == null || sections.isEmpty()) {
				logger.error("ERROR: HasSectionUtil sections null value ");
			}
			notAllowedSectionsbyProfile.addAll(Arrays.asList(ent.getProfileTypeNotAllowed()));
			createSessionSectionsMap(sections, session);
			
		}
		
		for (String notAllowedList : notAllowedSectionsbyProfile) {
			session.getProfileTypeSections().remove(Integer.parseInt(notAllowedList));
			session.getSectionPath().remove(Integer.parseInt(notAllowedList));
		}
		
		
		//For NotAllow Sections by Profile
		for(CustomerSectionOverwrite CustSectOverNotAllow :session.getCustomerProfile().getSectionOverwrite()) {
			if(CustSectOverNotAllow == null || CustSectOverNotAllow.getArraySectionsNotAllowed() == null) {
				logger.error("ERROR: HasSectionUtil hasSection Not Allow null value in Customer Section Overwrite Loop");
				continue;
			}
			
			notAllowedSectionsbyCustomer.addAll(Arrays.asList(CustSectOverNotAllow.getArraySectionsNotAllowed()));		
		}
		
		for(String notAllowedListByProfile : notAllowedSectionsbyCustomer) {
			session.getProfileTypeSections().remove(Integer.parseInt(notAllowedListByProfile));
			session.getSectionPath().remove(Integer.parseInt(notAllowedListByProfile));
		}
		
	}
	
	public static void createSessionSectionsMap(Set<ProfileTypeSection> sections, UserSession session) {
		Map<Integer, ProfileTypeSection> sessionSections = session.getProfileTypeSections();
		Map<Integer, String> sectionsPath = session.getSectionPath();
		for (ProfileTypeSection section: sections) {
			sessionSections.put(section.getSectionId(), section);
			sectionsPath.put(section.getSectionId(), section.getPath());
		}
	}
	
	@SuppressWarnings("unchecked")
	public static List<String> hasAccountsSectionNotAllowed(Integer sectionId, UserSession session) {
		Set<AccountsSectionOverwrite> accountsList = new HashSet<>();
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.SECTION_MANAGEMENT))
			accountsList = session.getCustomerProfile().getAccountsSectionOver();
		List<String> notAllowedAccounts = new LinkedList<>();
		for (AccountsSectionOverwrite accountOver: accountsList) {
			if(accountOver.getSectionId().equals(sectionId))
				notAllowedAccounts.addAll(Arrays.asList(accountOver.getAccountsNotAllowed()));
		}
		return notAllowedAccounts;
	}

}
