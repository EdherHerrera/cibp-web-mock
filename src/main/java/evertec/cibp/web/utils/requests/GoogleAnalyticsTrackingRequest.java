/**
 * author jocampo
 */
package evertec.cibp.web.utils.requests;

public class GoogleAnalyticsTrackingRequest {
	
	private String v;
	private String cid;
	private String tid;
	private String t;
	private String ec;
	private String ea;
	private String el;
	private String ev;
	private String cd2;
	private String cd3;
	private String cd4;
	private String cd5;
	private String cd6;
	private String cd7;
	private String cd8;
	private String cd9;
	
	public String getV() {
		return v;
	}
	public void setV(String v) {
		this.v = v;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getT() {
		return t;
	}
	public void setT(String t) {
		this.t = t;
	}
	public String getEc() {
		return ec;
	}
	public void setEc(String ec) {
		this.ec = ec;
	}
	public String getEa() {
		return ea;
	}
	public void setEa(String ea) {
		this.ea = ea;
	}
	public String getEl() {
		return el;
	}
	public void setEl(String el) {
		this.el = el;
	}
	public String getEv() {
		return ev;
	}
	public void setEv(String ev) {
		this.ev = ev;
	}
	public String getCd2() {
		return cd2;
	}
	public void setCd2(String cd2) {
		this.cd2 = cd2;
	}
	public String getCd3() {
		return cd3;
	}
	public void setCd3(String cd3) {
		this.cd3 = cd3;
	}
	public String getCd4() {
		return cd4;
	}
	public void setCd4(String cd4) {
		this.cd4 = cd4;
	}
	public String getCd5() {
		return cd5;
	}
	public void setCd5(String cd5) {
		this.cd5 = cd5;
	}
	public String getCd6() {
		return cd6;
	}
	public void setCd6(String cd6) {
		this.cd6 = cd6;
	}
	public String getCd7() {
		return cd7;
	}
	public void setCd7(String cd7) {
		this.cd7 = cd7;
	}
	public String getCd8() {
		return cd8;
	}
	public void setCd8(String cd8) {
		this.cd8 = cd8;
	}
	public String getCd9() {
		return cd9;
	}
	public void setCd9(String cd9) {
		this.cd9 = cd9;
	}
	
	

}
