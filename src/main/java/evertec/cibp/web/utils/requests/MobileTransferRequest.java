package evertec.cibp.web.utils.requests;

import java.math.BigDecimal;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.models.transfer.TVAccountTransferTarget;

public class MobileTransferRequest {
	private TVAccountTransferSource sou = null;
	private TVAccountTransferTarget tar = null;
	private BigDecimal amount;
	private CustomerProfile profile;
	
	public TVAccountTransferSource getSou() {
		return sou;
	}
	public void setSou(TVAccountTransferSource sou) {
		this.sou = sou;
	}
	public TVAccountTransferTarget getTar() {
		return tar;
	}
	public void setTar(TVAccountTransferTarget tar) {
		this.tar = tar;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public CustomerProfile getProfile() {
		return profile;
	}
	public void setProfile(CustomerProfile profile) {
		this.profile = profile;
	}
	
	
}
