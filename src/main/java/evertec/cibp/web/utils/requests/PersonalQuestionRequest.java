package evertec.cibp.web.utils.requests;

import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;

/**
 * Personal Question Request
 * 
 * @author Jorge L. Morla
 * @version 1.0
 * @since 1.0
 *
 * */
public class PersonalQuestionRequest {

	
	/**
	 * The RSA Action Request
	 * */
	private RSAActionRequest actionRequest;
	
	/**
	 * 
	 * The customerExist
	 * */
	private boolean customerExist;

	
	public RSAActionRequest getActionRequest() {
		return actionRequest;
	}

	public void setActionRequest(RSAActionRequest actionRequest) {
		this.actionRequest = actionRequest;
	}

	public boolean isCustomerExist() {
		return customerExist;
	}

	public void setCustomerExist(boolean customerExist) {
		this.customerExist = customerExist;
	}

	@Override
	public String toString() {
		return "PersonalQuestionRequest [actionRequest=" + actionRequest + ", customerExist=" + customerExist + "]";
	}
}
