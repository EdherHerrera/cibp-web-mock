package evertec.cibp.web.utils.requests;

import javax.servlet.http.HttpServletRequest;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.vs.beans.BankingSession;

public class MobileFilterTransactionRequest {
	
	private HttpServletRequest request;
	private CustomerProfile profile;
	private BankingSession bankingSession; 
	private TVFrontendAccount account;
	private CustomerServices customerService; 
	private TransactionsResponse transactionResponse;
	
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	public CustomerProfile getProfile() {
		return profile;
	}
	public void setProfile(CustomerProfile profile) {
		this.profile = profile;
	}
	public BankingSession getBankingSession() {
		return bankingSession;
	}
	public void setBankingSession(BankingSession bankingSession) {
		this.bankingSession = bankingSession;
	}
	public TVFrontendAccount getAccount() {
		return account;
	}
	public void setAccount(TVFrontendAccount account) {
		this.account = account;
	}
	public CustomerServices getCustomerService() {
		return customerService;
	}
	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}
	public TransactionsResponse getTransactionResponse() {
		return transactionResponse;
	}
	public void setTransactionResponse(TransactionsResponse transactionResponse) {
		this.transactionResponse = transactionResponse;
	}
	
}
