package evertec.cibp.web.utils.requests;

import org.apache.commons.validator.GenericValidator;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @created 11/23/20 6:22 PM
 */
public class GenericRequestUtil {


    public static void putAsStringBooleanParameter(HttpServletRequest request, Map<String, Object> model,
                                                   String parameterName) throws ServletRequestBindingException {
        final boolean retrievedParameter = !GenericValidator
                .isBlankOrNull(ServletRequestUtils.getStringParameter(request, parameterName))
                && ServletRequestUtils.getStringParameter(request, parameterName).equals("true");

        model.put(parameterName, retrievedParameter ? "true" : "false");
    }
}
