package evertec.cibp.web.utils.requests;

import java.util.List;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.services.CustomerServices;

public class GetAccountsRequest {
	private String accountNumber;
	private String type;
	private String productId;
	private String section;
	private List<TVFrontendAccount> accounts;
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public List<TVFrontendAccount> getAccounts() {
		return accounts;
	}
	public void setAccounts(List<TVFrontendAccount> accounts) {
		this.accounts = accounts;
	}
	
}
