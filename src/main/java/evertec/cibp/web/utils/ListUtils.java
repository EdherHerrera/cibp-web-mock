/**
 * 
 */
package evertec.cibp.web.utils;

import java.util.List;

import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.web.enums.CookieCategory;

/**
 * Class to include methods that modify or loop through lists.
 * 
 * @author ET58344, Evertec Inc.
 * @since 02-01-2021
 * @version 1.0
 */
public class ListUtils {
	
	/**
	 * Method that loop list parameter and return the ocurrence of category searched.
	 * 
	 * @author ET58344, Evertec Inc.
	 * @since 02-01-2021
	 * @param cookiePreferenceStatusList list
	 * @param category category
	 * @return CookiePreferenceStatus ocurrence
	 */
	public static CookiePreferenceStatus getCookiePreferenceFromList (List<CookiePreferenceStatus> cookiePreferenceStatusList, 
			CookieCategory category) {
		
		for (CookiePreferenceStatus eachCookiePreferenceStatus : cookiePreferenceStatusList) {
			
			if (eachCookiePreferenceStatus.getCategory().contentEquals(category.toString())) {
				return eachCookiePreferenceStatus;
			}
		}
		
		return null;
	}
}
