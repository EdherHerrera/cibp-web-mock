/**
 * 
 */
package evertec.cibp.web.utils;

import java.io.Serializable;
import java.util.Comparator;

import evertec.cibp.core.models.payment.TVPayment;

public class PaymentSort implements Comparator<TVPayment>, Serializable {

	private static final long serialVersionUID = 1L;

	public int compare(TVPayment a, TVPayment b) {

		return (-1) * a.getEffectiveDate().compareTo(b.getEffectiveDate());
	}

}