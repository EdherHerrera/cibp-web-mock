package evertec.cibp.web.utils;

import org.apache.commons.lang.StringEscapeUtils;

public class ToolTipUtil {

	public String generateTooltip(String message, String tooltipImagePath) {

		StringBuffer sb = new StringBuffer();
		sb.append("  onmouseover=\"Tip('");
		sb.append(StringEscapeUtils.escapeJavaScript(message));
		sb.append(".', BALLOON, true, BALLOONIMGPATH, '");
		sb.append(StringEscapeUtils.escapeJavaScript(tooltipImagePath));
		sb.append(")', FOLLOWMOUSE,false, ABOVE, true, OFFSETX, -17, FADEIN, 150, FADEOUT, 150, PADDING, 8)\" onmouseout=\"UnTip()\" ");

		return sb.toString();

	}

}
