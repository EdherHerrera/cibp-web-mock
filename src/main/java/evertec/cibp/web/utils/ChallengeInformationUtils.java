/**
 * 
 */
package evertec.cibp.web.utils;

/**
 * @author jocampo
 */
public class ChallengeInformationUtils {

	public static final String PASSWORD_ACTION = "PASSWORD";
	public static final String QUESTIONS_ACTION = "QUESTIONS";
	public static final String EMAIL_ACTION = "EMAIL";
	public static final String PHONES_ACTION = "PHONES";
	public static final String ADDRESSES_ACTION = "ADDRESSES";
	public static final String INFORMATION_CHALLENGE = "INFORMATIONCHALLENGE";
	public static final String INFORMATION_ACTION = "ACTION";
	public static final String BALANCE_ACTION = "BALANCE";
	public static final String PERSONAL_INFORMATION_CHANGES_ACTION = "PERSONAL";
	public static final String PAYMENTS_TRANSFERS_ACTION = "PAYMENTSTRANFERS";
	public static final String DEVICE_MANAGEMENT_ACTION = "DEVICEMANAGEMENT";
	public static final String ATH_ACTION = "ATH";
	public static final String CCA_ACTION = "CCA";
	public static final String INFORMATION_PARAMETERS = "PARAMETERS";
	public static final String CHALLENGE_USER = "CHALLENGEUSER";

	public static final String QUESTION_1 = "QUESTION1";
	public static final String QUESTION_2 = "QUESTION2";
	public static final String QUESTION_3 = "QUESTION3";
	public static final String ANSWER_1 = "ANSWER1";
	public static final String ANSWER_2 = "ANSWER2";
	public static final String ANSWER_3 = "ANSWER3";

	public static final String PROVIDER = "PROVIDER";
	public static final String PHONE = "PHONE";
	public static final String FAX = "FAX";

	public static final String RESIDENTIAL_PHONE = "RESPHONE";
	public static final String WORK_PHONE = "WORKPHONE";
	public static final String MOBILE_PHONE = "MOBILEPHONE";
	
//	This code will be eliminated when outreach pass MOD.20170111.MBSD.1172.dci.outreach flag
//	{
	public static final String ADDRESS_CHANGE_TYPE = "ADDRESSCHANGETYPE";
	public static final String APPLIES_TO_BOTH = "APPLIESTOBOTH";
//	}
	
	public static final String ALERTS_FRAME = "ALERTSFRAME";
	public static final String SHOW_UPDATE_ALERT = "SHOWUPDATEALERT";

	public static final String MOBILE_FRAME = "MOBILEFRAME";
	public static final String MOBILE_DELETE = "MOBILEDELETE";
	public static final String MOBILE_DELETE_OOB = "MOBILEDELETEOOB";

	public static final String BALANCE_FRAME = "BALANCEFRAME";
	public static final String BALANCE_MODEL = "BALANCEMODEL";

	public static final String PERSONAL_INFORMATION_CHANGES_FRAME = "PERSONALINFORMATIONCHANGESFRAME";

	public static final String PAYMENTS_TRANSFERS_FRAME = "PAYMENTSTRANSFERSFRAME";

	public static final String ATH_FRAME = "ATHFRAME";
	public static final String ATH_MODEL = "ATHMODEL";
	
	public static final String CCA_FRAME = "CCAFRAME";
	public static final String CCA_MODEL = "CCAMODEL";
	
	public static final String SERVICES_FRAME = "SERVICESFRAME";
	public static final String SERVICES_MODEL = "SERVICESMODEL";

	public static final String ATHM_ACCT_MOD = "ATHMACCTMOD";
	public static final String ATHM_ACCT = "ATHMACCT";
	public static final String ATHM_PLASTIC = "ATHMPLASTIC";
	public static final String ATHM_EXP_DATE = "ATHMEXPDATE";
	public static final String ATHM_ACCT_TYPE = "ATHMACCTTYPE";
	
	
	public static final String NAME = "NAME";
	//public static final String PHONE = "PHONE";  Se utiliza una línea arriba por lo cual se comenta, 
	//											   para una futura iteraciómn o cambio se debe de valorar mantenerlo igual
	public static final String BUSINESS_NAME = "BUSINESS_NAME";
	public static final String BUSINESS_PHONE = "BUSINESS_PHONE";
	public static final String POSTAL_ADDRESS = "POSTAL_ADDRESS";
	public static final String SSNE = "SSNE";
	public static final String ACCOUNT = "ACCOUNT";
	public static final String ESTIMATED_AMOUNT = "ESTIMATED_AMOUNT";
	
	public static final String RESIDENTIAL_ADDRESS_1 = "RESIDENTIAL_ADDRESS_1";
	public static final String RESIDENTIAL_ADDRESS_2 = "RESIDENTIAL_ADDRESS_2";
	public static final String CITY = "CITY";
	public static final String STATE = "STATE";
	public static final String ZIP_CODE = "ZIP_CODE";
	
	public static final String NEW_PASS = "new_pass";
	public static final String OLD_PASS = "old_pass";
	public static final String WORD_LIST = "word_list";
	public static final String LANGUAGE = "language";
	public static final String PROFILE = "profile";

}
