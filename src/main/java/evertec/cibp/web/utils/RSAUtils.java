package evertec.cibp.web.utils;

import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.MobileDevice;

import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.RSAEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.customer.modules.CommonServicesModule;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.UserSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

public class RSAUtils {

	private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RSAUtils.class);

	/**
	 * Method to return a DeviceRequest created with the passed parameters
	 * @param devicePrint The device-print
	 * @param deviceTokenCookie The device token cookie
	 * @param request The HttpServletRequest
	 * @return A DeviceRequest object
	 */
	public static DeviceRequest getDeviceRequest(String devicePrint, String deviceTokenCookie, HttpServletRequest request) {
		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		if(!GenericValidator.isBlankOrNull(deviceTokenCookie)){
			cookie.setValue(deviceTokenCookie);
		}

		DeviceRequest deviceRequest = new DeviceRequest();
		deviceRequest.setDevicePrint(devicePrint);
		deviceRequest.setIpAddress(request.getRemoteAddr());
		deviceRequest.setDeviceTokenCookie(cookie.getValue());
		//deviceRequest.setDeviceTokenFSO(cookie.getValue());
		deviceRequest.setHttpAccept(request.getHeader("Accept"));
		deviceRequest.setHttpAcceptChars(request.getHeader("Accept-Charset"));
		deviceRequest.setHttpAcceptEncoding(request.getHeader("Accept-Encoding"));
		deviceRequest.setHttpAcceptLanguage(request.getHeader("Accept-Language"));
		deviceRequest.setHttpReferrer(request.getHeader("Referer"));
		deviceRequest.setUserAgent(request.getHeader("User-Agent"));
		return deviceRequest;
	}
	
	/**
	 * Method to return a DeviceRequest created with the passed parameters
	 * @param devicePrint The device-print
	 * @param deviceTokenCookie The device token cookie
	 * @param request The HttpServletRequest
	 * @return A DeviceRequest object
	 */
	public static DeviceRequest getDeviceRequest(String devicePrint, String deviceTokenCookie, String sdkJson,
			HttpServletRequest request, boolean isSDKMobileRequest) {
		
		Cookie cookie = getCookie(deviceTokenCookie, request);
		
		DeviceRequest deviceRequest = new DeviceRequest();
		deviceRequest.setIpAddress(request.getRemoteAddr());
		deviceRequest.setDeviceTokenCookie(cookie.getValue());
		deviceRequest.setUserAgent(request.getHeader("User-Agent"));
		
		Boolean isEnabledSDK = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC615_MOBILE_SDK_DATA);
		
		if (isEnabledSDK && isSDKMobileRequest) {
			final MobileDevice  md = new MobileDevice();
			
	        md.setMobileSdkData(sdkJson);
	        deviceRequest.setDeviceIdentifier(new com.rsa.csd.ws.DeviceIdentifier[]{md}); 
		} else {
			deviceRequest.setHttpAcceptLanguage(request.getHeader("Accept-Language"));
			deviceRequest.setDevicePrint(devicePrint);
			//deviceRequest.setDeviceTokenFSO(cookie.getValue());
			deviceRequest.setHttpAcceptChars(request.getHeader("Accept-Charset"));
			deviceRequest.setHttpAcceptEncoding(request.getHeader("Accept-Encoding"));
			deviceRequest.setHttpReferrer(request.getHeader("Referer"));
			deviceRequest.setHttpAccept(request.getHeader("Accept"));
		}
		
		return deviceRequest;
	}


	public static Cookie getCookie(String value, HttpServletRequest request) {
		Cookie cookie = WebUtils.getCookie(request, "PMData");
		if (cookie == null)
			cookie = new Cookie("PMData", "");
		cookie.setPath("/");
		cookie.setMaxAge(31536000);
		cookie.setValue(value);
		return cookie;
	}

	// Create the RSA entitlement for this profile
	public static boolean createRSAEntitlement(CustomerServices customerService, CustomerProfile profile) {
		try {
			CustomerEntitlement entitlement = new CustomerEntitlement(EntitlementType.RSA, EntitlementStatus.ENABLED);
			RSAEntitlement rsaEntitlement = new RSAEntitlement(entitlement);
			rsaEntitlement.setEmptyEntitlement(profile.getProfileId());
			customerService.getCustomerProfileService().addEntitlement(profile,
					rsaEntitlement.getCustomerEntitlement());

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static String setPhoneMask(String phone) {
		StringBuilder sb = new StringBuilder(15);
		StringBuilder temp = new StringBuilder(phone);
		while (temp.length() < 10)
			temp.insert(0, "0");
		char[] chars = temp.toString().toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (i == 3 || i == 6)
				sb.append("-");
			if (i == 3 || i == 4 || i == 5 || i == 6 || i == 7) {
				sb.append("*");
			} else {
				sb.append(chars[i]);
			}
		}
		return sb.toString();
	}

	public static String generateOOBCode(){
		return UUID.randomUUID().toString().replaceAll("-","").substring(0, 16).toUpperCase();
	}
	
	public static void  incrementUserSkips(CustomerServices customerServices, CustomerProfile customerProfile) {
		RSAEntitlement rsaEntitlement =
				new RSAEntitlement(customerProfile.getEntitlementByType(EntitlementType.RSA));
		Integer userSkipsNumber = Integer.parseInt(rsaEntitlement.getOobInterruptionPageSkips());
		userSkipsNumber++;
		rsaEntitlement.setOobInterruptionPageSkips((userSkipsNumber).toString());
		customerServices.updateEntitlement(customerProfile, rsaEntitlement.getCustomerEntitlement());
	}

	public static Integer getUserSkips(CustomerProfile customerProfile) {
		RSAEntitlement rsaEntitlement =
				new RSAEntitlement(customerProfile.getEntitlementByType(EntitlementType.RSA));
		return Integer.parseInt(rsaEntitlement.getOobInterruptionPageSkips());
	}

	public static void validateUserContainSkips(CustomerServices customerServices, CustomerProfile customerProfile){
		RSAEntitlement rsaEntitlement =
				new RSAEntitlement(customerProfile.getEntitlementByType(EntitlementType.RSA));
		if(rsaEntitlement.getOobInterruptionPageSkips() == null){
			rsaEntitlement.setOobInterruptionPageSkips(RSAEntitlement.INITIAL_SKIP);
			customerServices.updateEntitlement(customerProfile, rsaEntitlement.getCustomerEntitlement());
		}
	}
	
	public static boolean isOOBInterruption(UserSession userSession) {
		RSAEntitlement rsaEntitlement =
				new RSAEntitlement(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.RSA));

		if (!isOOBEnrolled(userSession) && rsaEntitlement.getCustomerEntitlement() != null) {
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC1156_OOB_ACTIVATE_INTERRUPTION_PAGE_FOR_ALL_CUSTOMERS) || rsaEntitlement.getOobInterruptionPageEnabled() != null) {
				validateUserContainSkips(userSession.getCustomerService(), userSession.getCustomerProfile());
				return true;
			}
		}
		return false;
	}

	public static boolean isOOBEnrolled(UserSession userSession) {
		CommonServicesModule commonServicesModule = userSession.getCustomerService().getCommonServices();
		CredentialType credentialType = commonServicesModule.getRsaChallengeMethod(userSession.getUsername(), userSession.getDeviceRequest());

		return (credentialType.equals(CredentialType.OOBPHONE) || credentialType.equals(CredentialType.USER_DEFINED));
	}
}
