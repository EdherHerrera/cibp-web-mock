package evertec.cibp.web.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.springframework.context.MessageSource;

import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.models.base.RunningBalanceTransaction;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.SingleCustomerProfileResponse;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.SMSEntitlement;
import evertec.cibp.core.models.postinitialfunding.PIFAccountTypes;
import evertec.cibp.core.models.transfer.TVAccountTransfer;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.AccountsClasificationRequest;
import evertec.cibp.core.services.requests.BankingSessionRequest;
import evertec.cibp.core.services.requests.GetSMSProfileRequest;
import evertec.cibp.core.services.requests.TransactionRequest;
import evertec.cibp.core.services.responses.AccountsClasificationResponse;
import evertec.cibp.core.services.responses.BankingSessionResponse;
import evertec.cibp.core.services.responses.CustomerDeviceResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.core.services.responses.TransferResponse;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.web.enums.MobileResponseStatus;
import evertec.cibp.web.response.MobileDeviceProfileResponse;
import evertec.cibp.web.response.MobileTransactionResponse;
import evertec.cibp.web.response.MobileTransferResponse;
import evertec.cibp.web.utils.requests.GetAccountsRequest;
import evertec.cibp.web.utils.requests.MobileFilterTransactionRequest;
import evertec.cibp.web.utils.requests.MobileTransferRequest;

public class MobileUtil {
	
	protected static final Log logger = LogFactory.getLog(MobileUtil.class);
	private static final int MAX_TRANSACTIONS = 500;
	private static final String TRANSACTIONS_KEY = "transactions";
	private static final String INPROCESS_TRANSACTIONS_KEY = "inprocess";
	
	public static BankingSession getBankingSession(HttpServletRequest request, CustomerProfile profile, CustomerServices customerService) {
		CustomerEntitlement ent = profile.getEntitlementByType(EntitlementType.BANKING);

		if(ent == null || GenericValidator.isBlankOrNull(profile.getUsername())) {
			logger.error("BANKING Entitlement not found");
		} else {
			evertec.cibp.core.models.entitlement.BankingEntitlement entt = new BankingEntitlement(ent);

			BankingSessionRequest sessionRequest = new BankingSessionRequest(
					Utils.generateCustomerInteractionEvent(request), profile, null, entt, false);
			BankingSessionResponse bankingresponse = customerService.getBankingSession(sessionRequest);

			if (bankingresponse != null && bankingresponse.getErrorCode() == 0) {
				BankingSession bankingSession = bankingresponse.getBankingSession();

				if (bankingSession != null) {
					return bankingSession;
				} else {
					logger.error("BankingSession was null or has error for user: "+profile.getUsername());
				}
			} else {
				logger.error("BankingSession Response was null or has error for user: "+profile.getUsername());
			}
		}
		return null;
	}
	public static MobileDeviceProfileResponse getProfileByDeviceToken(HttpServletRequest request, String deviceToken, CustomerServices customerService) {
			
		MobileDeviceProfileResponse response = new MobileDeviceProfileResponse();

		CustomerDeviceResponse deviceResponse = customerService.getCustomerDeviceByDeviceMbopId(deviceToken);

		if(deviceResponse == null || deviceResponse.getDevice() == null || deviceResponse.getResponseStatus() != ResultStatus.SUCCESS) {
			logger.error("DeviceResponse is null.  Verify this device token '"+deviceToken+"' in Customer_Devices Table  ");
			response.setStatus(MobileResponseStatus.ERROR);
		} else {
			if (!GenericValidator.isBlankOrNull(deviceResponse.getDevice().getProfileId())) {

				response.setDevice(deviceResponse.getDevice());

				SingleCustomerProfileResponse profileResponse = customerService.getCustomerProfileService()
						.getCustomerProfileById(deviceResponse.getDevice().getProfileId());

				if (profileResponse != null && profileResponse.getCustomerProfile() != null) {
					response.setProfile(profileResponse.getCustomerProfile());
				}
				response.setStatus(MobileResponseStatus.SUCCESS);
			} else {
				logger.error("Profile Id was null");
				response.setStatus(MobileResponseStatus.ERROR);
			}
		}
		return response;
	}
	
	public static List<TVFrontendAccount> getAccounts(HttpServletRequest request, BankingSession bankingSession, CustomerProfile profile, CustomerServices customerService) {
		List<TVFrontendAccount> accounts = new LinkedList<TVFrontendAccount>();

		if(profile == null || GenericValidator.isBlankOrNull(profile.getUsername())) {
			logger.error("CustomerProfile is null or username is null");
		} else {
			CustomerEntitlement ent = profile.getEntitlementByType(EntitlementType.BANKING);

			if(ent == null) {
				logger.error("BANKING Entitlement not found for user:"+profile.getUsername());	
			} else {

				if (bankingSession != null) {

					List<TVFrontendAccount> userAccounts = null;

					userAccounts = customerService.getCustomerAccounts(profile, bankingSession, true);

					if(userAccounts != null && !userAccounts.isEmpty()) {
						AccountsClasificationRequest accountsRequest = new AccountsClasificationRequest();
						accountsRequest.setUserAccounts(userAccounts);
						accountsRequest.setServiceAcctInq(false);
						accountsRequest.setCustomerProfile(profile);
						accountsRequest.setCustomerService(customerService);

						List<PIFAccountTypes> allowed = customerService.getCreditableAccountTypes();
						
						if (allowed != null) {
							accountsRequest.setAllowed(allowed);
						}
						
						AccountsClasificationResponse accountsResponse = AccountUtils.clasifyAccounts(accountsRequest);
						if(accountsResponse != null && accountsResponse.getDepositsSavingsAccounts() != null) {
							accounts = accountsResponse.getDepositsSavingsAccounts();

							List<TVFrontendAccount> accountsToRemove = new LinkedList<TVFrontendAccount>();
							for (TVFrontendAccount account: accounts)
							{
								if((account.getProductDefinition().getProductStatus().equalsIgnoreCase("CLOSE")) || (account.getProductDefinition().getProductStatusCode().equalsIgnoreCase("C")))
								{
									accountsToRemove.add(account);
								}
							}
							if(!accountsToRemove.isEmpty())
							{
								accounts.removeAll(accountsToRemove);
							}

							if(accountsResponse.getHiddenAccounts() != null && !accountsResponse.getHiddenAccounts().isEmpty()) {
								accounts.removeAll(accountsResponse.getHiddenAccounts());
							}
						} else {
							logger.error("Account Clasification Response was null for user: "+profile.getUsername());
						}

					} else {
						logger.error("No Accounts found for user: "+profile.getUsername()+". The object is null or empty.");
					}
				} else {
					logger.error("BankingSession was null or has error for user: "+profile.getUsername());
				}
			}
		}
		return accounts;
	}

	public static MobileTransactionResponse getAccountTransactions(HttpServletRequest request, BankingSession bankingSession, CustomerProfile profile, TVFrontendAccount account, CustomerServices customerService) {
		MobileTransactionResponse mobileTransactionResponse = new MobileTransactionResponse();

		if(profile == null) {
			logger.error("CustomerProfile is null");
			mobileTransactionResponse.setStatus(MobileResponseStatus.ERROR);
		} else if (account == null || GenericValidator.isBlankOrNull(account.getSubtype())){
			logger.error("Account or Account Subtype is null");
			mobileTransactionResponse.setStatus(MobileResponseStatus.ERROR);
		} else {

			if (bankingSession != null) {
				
				TransactionsResponse transactionResponse = null;
				
				if ("IDA".equalsIgnoreCase(account.getSubtype()) || "PFC".equalsIgnoreCase(account.getSubtype())) {
					// get current transactions

					TransactionRequest depreq2 = new TransactionRequest(
							Utils.generateCustomerInteractionEvent(request), null, profile, bankingSession,
							account, MAX_TRANSACTIONS, 1, true, null, null, null);
					
					transactionResponse = customerService.getDepositCurrentTransactions(depreq2);
					
				} else if (account.getSubtype().equals("CCA")) {

					TransactionRequest ccareq2 = new TransactionRequest(
							Utils.generateCustomerInteractionEvent(request), null, profile, bankingSession,
							account, MAX_TRANSACTIONS, 1, false, null, null, null);
					
					transactionResponse = customerService.getCreditCardCurrentTransactions(ccareq2);
																			
				} else {
					logger.error("Account Subtype is not IDA, PFC or CCA");
					mobileTransactionResponse.setStatus(MobileResponseStatus.ERROR);
					return mobileTransactionResponse;
				}
				
				if (transactionResponse == null || 
						transactionResponse.getStatusResult() != ResultStatus.SUCCESS || 
						transactionResponse.getTransactions() == null || 
						transactionResponse.getTransactions().size()<=0 ) {
					mobileTransactionResponse.setStatus(MobileResponseStatus.ERROR);
					return mobileTransactionResponse;
				}
				
				MobileFilterTransactionRequest transactionRequest = new MobileFilterTransactionRequest();
				transactionRequest.setRequest(request);
				transactionRequest.setProfile(profile);
				transactionRequest.setBankingSession(bankingSession);
				transactionRequest.setAccount(account);
				transactionRequest.setCustomerService(customerService);
				transactionRequest.setTransactionResponse(transactionResponse);
				
				HashMap<String, List<AbstractTransaction>> filteredTransactions = filterAndOrganizeTransactions(transactionRequest);
				
				if(filteredTransactions.get(TRANSACTIONS_KEY) != null) {
					mobileTransactionResponse.setTransactions(filteredTransactions.get(TRANSACTIONS_KEY));	
				}					
				if(filteredTransactions.get(INPROCESS_TRANSACTIONS_KEY) != null) {
					mobileTransactionResponse.setInprocess(filteredTransactions.get(INPROCESS_TRANSACTIONS_KEY));	
				}		
				mobileTransactionResponse.setStatus(MobileResponseStatus.SUCCESS);

			} else {
				logger.error("BankingSession was null or has error");
			}
		
		}
		return mobileTransactionResponse;
	}
	
	public static TVFrontendAccount getAccountByAccountNumberAndTypeAndProductIdAndSection(GetAccountsRequest request) {
		
		if(request == null) {
			return null;
		}
		
		List<TVFrontendAccount> accounts = request.getAccounts();
		String type = request.getType();
		String productId = request.getProductId();
		String accountNumber = request.getAccountNumber();
		String section = request.getSection();
		
		if (accounts != null) {
			for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount act = (TVFrontendAccount) iterator.next();
				if ((act.getSubtype().equals(type) && act.getAccountNumber().substring(act.getAccountNumber().length() - 4).equals(accountNumber))
						&& act.getProductDefinition().getProductId().equals(productId) && act.getAccountSection().equals(section)) {
					return act;
				}
			}
		}
		return null;
	}	
	
	public static MobileTransferResponse makeMobileTransfer(HttpServletRequest request, MobileTransferRequest trfRequest, BankingSession bankSession, CustomerServices customerService,
			CustomerProfile profile, MessageSource messageSource) {

		MobileTransferResponse response = new MobileTransferResponse();
		
		TVAccountTransfer transfer = null;
		
		if(bankSession != null) {
			List<TVAccountTransfer> confTransfers = new LinkedList<TVAccountTransfer>();
			List<TVAccountTransfer> failTransfers = new LinkedList<TVAccountTransfer>();

			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			
			transfer = new TVAccountTransfer(trfRequest.getAmount(), Frequency.MANUALLY, 1, cal, trfRequest.getSou(), trfRequest.getTar());
			
			TransferResponse res = customerService.sendTransfer(trfRequest.getProfile(), bankSession, transfer,
					Utils.generateCustomerInteractionEvent(request));
			
			if(res != null && res.getTransfer() != null && res.getErrorCode() == 0) {
				
				transfer = res.getTransfer();
								
				if(res.getStatus() == ResultStatus.SUCCESS && transfer != null && !GenericValidator.isBlankOrNull(transfer.getReferenceNumber())) {
					response.setTrfRequest(trfRequest);
					response.setDescription(transfer.getReferenceNumber());
					response.setStatus(MobileResponseStatus.SUCCESS);
					confTransfers.add(transfer);
					
				} else {
					response.setTrfRequest(trfRequest);
					response.setDescription(res.getErrorString());
					response.setStatus(MobileResponseStatus.ERROR);	
					String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request).toLowerCase(), messageSource);
					if(GenericValidator.isBlankOrNull(er)) {
						transfer.setTransferMessage(res.getErrorEBA());
					} else {
						transfer.setTransferMessage(er);
					}
					failTransfers.add(transfer);					
				}
				
			} else {
				response.setTrfRequest(trfRequest);
				response.setDescription(res.getErrorString());
				response.setStatus(MobileResponseStatus.ERROR);							
				String er = getTelepagoErrorDescriptor(res.getErrorEBA(), Utils.getRSALanguage(request).toLowerCase(), messageSource);
				if(GenericValidator.isBlankOrNull(er)) {
					transfer.setTransferMessage(res.getErrorEBA());
				} else {
					transfer.setTransferMessage(er);
				}
				failTransfers.add(transfer);					
			}

			boolean hasalert = false;
			CustomerEntitlement e = profile.getEntitlementByType(EntitlementType.ALERTS);
			if (e != null) {
				AlertsEntitlement ent = new AlertsEntitlement(e);
				if (ent != null && ent.getCustomerEntitlement() != null && ent.getAlerts() != null) {
					Set<AlertType> alerts = ent.getAlerts();
					if (alerts.contains(AlertType.TRANSFERS_EMAIL) || alerts.contains(AlertType.TRANSFERS_SMS) || alerts.contains(AlertType.TRANSFERS_PUSH)) {
						hasalert = true;
					}
				}
			}

			if (hasalert) {
				SMSProfile smsProfile = null;
				if (profile.getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
					SMSEntitlement ent = new SMSEntitlement(
							profile.getEntitlementByType(EntitlementType.MOBILE_BANKING));
					GetSMSProfileRequest getreq = new GetSMSProfileRequest(Utils.generateCustomerInteractionEvent(request), profile, ent);
					smsProfile = customerService.getSMSProfile(getreq);
				}
				customerService.sendTransferConfirmation(profile, confTransfers, failTransfers,
							smsProfile, PreferredLanguage.ENGLISH);						
			}
			
		} else {
			logger.error("BankingSession is null");
		}
		
		return response;

	}
	
	private static HashMap<String, List<AbstractTransaction>> filterAndOrganizeTransactions(MobileFilterTransactionRequest request) {
		
		HashMap<String, List<AbstractTransaction>> returnTransactions = new HashMap<String, List<AbstractTransaction>>();
		
		if (request == null || request.getAccount() == null || request.getBankingSession() == null ||
				request.getCustomerService() == null || request.getProfile() == null || 
				request.getRequest() == null) {
			logger.error("All MobileFilterTransactionRequest are required");
			return returnTransactions;
		}
		
		List<AbstractTransaction> transactions = new LinkedList<AbstractTransaction>();
		TransactionsResponse transactionResponse = request.getTransactionResponse();
		int totalpages = request.getTransactionResponse().getPagesAvailable();
		for (int z = 1; z <= totalpages; z++) {
			TransactionRequest depreq = new TransactionRequest(
					Utils.generateCustomerInteractionEvent(request.getRequest()), null, request.getProfile(),
					request.getBankingSession(), request.getAccount(), MAX_TRANSACTIONS, z, true, null, null, null);
			transactionResponse = request.getCustomerService().getDepositCurrentTransactions(depreq);
			if (transactionResponse == null || transactionResponse.getStatusResult() != ResultStatus.SUCCESS || 
					transactionResponse.getTransactions() == null || 
					transactionResponse.getTransactions().size() <= 0)
				continue;

			transactions.addAll(transactionResponse.getTransactions());
		}
		
		List<AbstractTransaction> inprocess = new LinkedList<AbstractTransaction>();
		for (Iterator<AbstractTransaction> iterator = transactions.iterator(); iterator.hasNext();) {
			RunningBalanceTransaction name = (RunningBalanceTransaction) iterator.next();
			if (name.getDate() == null) {
				if(name.getDescription().toUpperCase().indexOf("IN PROCESS") != -1) {
					name.setDescription(name.getDescription().substring("IN PROCESS ".length()));
				}
				inprocess.add(name);
			}
		}
		transactions.removeAll(inprocess);
		
		//Sort and Get Last 10 transactions
		List<AbstractTransaction> last10Transactions = new LinkedList<AbstractTransaction>();
		int indexLimit = 0;
		if(transactions.size()>10) {
			indexLimit = transactions.size()-10;
		}
		
		for(int i=transactions.size()-1; i>=indexLimit; i--) {
			last10Transactions.add(transactions.get(i));
		}
		
		List<AbstractTransaction> last10InProcess = new LinkedList<AbstractTransaction>();
		indexLimit = 0;
		if(inprocess.size()>10) {
			indexLimit = inprocess.size()-10;
		}
		
		for(int i=inprocess.size()-1; i>=indexLimit; i--){
			last10InProcess.add(inprocess.get(i));
		}
		
		returnTransactions.put(INPROCESS_TRANSACTIONS_KEY, last10InProcess);
		returnTransactions.put(TRANSACTIONS_KEY, last10Transactions);
		
		return returnTransactions;
	}
	
	/*
	 * CIBP-1680 bgarcia Obtiene el texto de error actualizado
	 */
	private static String getTelepagoErrorDescriptor(String codigo, String len, MessageSource messageSource) {
		String status = "";
		try {
			if (!"en".equals(len))
				len = "es";
			status = messageSource.getMessage(codigo.trim(), null, new Locale(len)).toUpperCase();
		} catch (Exception e) {
			status = "";
		}

		return status;
	}

	public String getTelepagoErrorDescriptorTest(String codigo, String len, MessageSource messageSource) {
		return getTelepagoErrorDescriptor(codigo, len, messageSource);
	}

}
