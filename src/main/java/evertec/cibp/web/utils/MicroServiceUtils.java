package evertec.cibp.web.utils;

import com.evertec.cibp.api.client.model.Error;
import org.apache.http.HttpStatus;

public class MicroServiceUtils {

	/**
	 * This method creates an Error object, populates it with the provided parameters
	 * and then returns it.
	 *
	 * @param status The HTTP status (200 OK, 404 NOT FOUND, etc.)
	 * @param title The errors title (e.g. Not Found)
	 * @param detail The errors detail (e.g. The requested entity does not exist)
	 * @param source The place where the error was thrown (i.e. the method or end-point)
	 *
	 * @return An Error object with initialized with the parameters values.
	 */
	public static Error generateMicroServiceError(int status, String title, String detail, String source){

		Error error = new Error();

		error.setStatus(String.valueOf(status));
		error.setTitle(title);
		error.setDetail(detail);
		error.setCode(String.valueOf(HttpStatus.SC_BAD_REQUEST));
		error.setResource(source);
		return error;
	}
}
