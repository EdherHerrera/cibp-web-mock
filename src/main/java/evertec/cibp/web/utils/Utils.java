package evertec.cibp.web.utils;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import bppr.tv.beans.transactions.Transaction;
import evertec.cibp.core.enums.TsysAlertStatus;
import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerBalanceAlert;
import evertec.cibp.core.models.customer.CustomerDevices;
import evertec.cibp.core.models.customer.CustomerEmailBalanceAlert;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerPhone;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerProfileLite;
import evertec.cibp.core.models.customer.CustomerProfileType;
import evertec.cibp.core.models.customer.EasyOfacCustomer;
import evertec.cibp.core.models.customer.EasyOfacCustomerStatus;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.SingleCustomerProfileLiteResponse;
import evertec.cibp.core.models.customer.SingleCustomerProfileResponse;
import evertec.cibp.core.models.customer.interaction.EStatementInteraction;
import evertec.cibp.core.services.requests.TransactionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.ChallengeOobRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RsaChallengeRequest;
import evertec.cibp.core.services.responses.CustomerDeviceResponse;
import evertec.cibp.core.services.responses.FingerprintRegistrationResponse;
import evertec.cibp.core.services.responses.PaymentHistoryResponse;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.core.services.responses.adaptiveauth.ChallengeOobResponse;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.models.devices.DeviceInfo;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.velocity.tools.generic.NumberTool;
import org.owasp.csrfguard.CsrfGuard;
import org.owasp.csrfguard.CsrfGuardException;
import org.owasp.csrfguard.util.RandomGenerator;
import org.owasp.esapi.ESAPI;
import org.springframework.context.MessageSource;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import com.google.gson.Gson;
import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceRequest;

import bppr.sms.beans.SMSAccount;
import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import bppr.tv.beans.BankInformation;
import bppr.tv.beans.Owner;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.AccountRecord;
import bppr.tv.beans.accounts.AccountSubtypes;
import bppr.tv.beans.accounts.factory.TVAccountFactory;
import bppr.tv.beans.accounts.records.CreditCardAccountRecord;
import bppr.tv.beans.accounts.types.CardAccount;
import bppr.tv.beans.accounts.types.DepositAccount;
import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.facades.TsysAlertFacade;
import evertec.creditcards.exception.TsysException;
import evertec.creditcards.model.CustomerIdentifiers;
import evertec.creditcards.model.CustomerList;
import evertec.creditcards.response.TsysGeneralInformationResponse;
import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.DepositProduct;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TVFrontendCreditAccount;
import evertec.cibp.core.models.account.TVFrontendDepositAccount;
import evertec.cibp.core.models.aggregation.AggregatedAccount;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.base.AbstractStatement;
import evertec.cibp.core.models.campaign.ActiveCampaignCustomerView;
import evertec.cibp.core.models.campaign.ActiveCampaignView;
import evertec.cibp.core.models.campaign.CampaignSegmentType;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.campaign.MyMessagesCampaign;
import evertec.cibp.core.models.campaign.PortalCampaign;
import evertec.cibp.core.models.customer.applications.ApplicationStatus;
import evertec.cibp.core.models.customer.applications.CustomerApplication;
import evertec.cibp.core.models.customer.applications.CustomerApplicationType;
import evertec.cibp.core.models.customer.devices.CustomerDevice;
import evertec.cibp.core.models.customer.devices.DeviceStatus;
import evertec.cibp.core.models.customer.devices.ProductType;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.customer.interaction.PushNotificationInteraction;
import evertec.cibp.core.models.customer.oob.CustomerOOBChallengeType;
import evertec.cibp.core.models.ebill.EbppPreviousHistoryInfo;
import evertec.cibp.core.models.enrollment.EnrollableAccount;
import evertec.cibp.core.models.enrollment.EnrollmentLiteStatus;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.RDCEntitlement;
import evertec.cibp.core.models.entitlement.RSAEntitlement;
import evertec.cibp.core.models.holiday.Holiday;
import evertec.cibp.core.models.payee.GlobalPayeePaymentType;
import evertec.cibp.core.models.payee.PayeeObject;
import evertec.cibp.core.models.payee.PayeeType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.statements.AccountPDFStmtInfo;
import evertec.cibp.core.models.statements.AccountStmtInfo;
import evertec.cibp.core.models.transfer.TVAccountTransfer;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.models.transfer.TVAccountTransferTarget;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.SendSmsGeneratedCodeRequest;
import evertec.cibp.core.services.requests.StatementRequest;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.BankingDaysUtils;
import evertec.cibp.core.utils.DisposableIdentifier;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.mobilecash.core.models.MobileCashTransaction;
import evertec.cibp.mobilecash.core.models.MobileCashTransactionStatus;
import evertec.cibp.template.utils.PaymentSort;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.models.PaymentFilterType;

import evertec.cibp.web.enums.WebResponseStatus;
import evertec.cibp.web.facade.AccountsFacade;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.EpayrollSearchBean;
import evertec.cibp.web.form.bean.EvendorBean;
import evertec.cibp.web.request.PaymentHistoryRequest;
import evertec.cibp.web.request.RDCEnrollmentInfoRequest;
import evertec.cibp.web.response.RDCEnrollmentInfoResponse;
import evertec.cibp.web.session.EnrollmentSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.session.UserSessionType;
import evertec.dp.service.tvstatement.CreditCardStatement_Type;
import evertec.dp.service.tvstatement.DepositStatement_Type;
import evertec.easyofac.models.CustomerSearch;
import evertec.easyofac.models.ResponseOfac;
import evertec.otherservices.beans.UserAccount;
import evertec.otherservices.beans.RDC.DepositorUser;
import evertec.otherservices.beans.RDC.EnrollAccount;
import evertec.otherservices.beans.RDC.Limits;
import evertec.otherservices.beans.RDC.RDCAccount;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.EnrollUserReq;
import evertec.otherservices.requests.GetAccountListReq;
import evertec.otherservices.responses.EnrollUserResp;
import evertec.otherservices.responses.GetAccountListResp;
import evertec.rsa.RSAConnector.Lang;
import evertec.statementview.StatementViewService;
import evertec.statementview.beans.StatementViewRequest;
import evertec.statementview.beans.StatementViewResponse;

/**
 * Utils
 * @author et55596
 * @since 1.2
 * @version 1.0
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class Utils {

	/**constants PATTERN */
	public static final String PATTERN = getPattern();

	/**constants logger */
	protected static final Log logger = LogFactory.getLog(Utils.class);

	/**constants AMEX */
	public static final String AMEX = "AMEX";

	/**constants CCA */
	public static final String CCA = "CCA";

	/**constants MLA */
	public static final String MLA = "MLA";

	/**constants RVA */
	public static final String RVA = "RVA";

	/**constants LEA */
	public static final String LEA = "LEA";

	/**constants IDA */
	public static final String IDA = "IDA";

	/**constants PFC */
	public static final String PFC = "PFC";

	/**constants ACCT_SECTION_SAV */
	public static final String ACCT_SECTION_SAV = "S";

	/**constants ACCT_SECTION_RES */
	public static final String ACCT_SECTION_RES = "R";

	/**constants DEEP_LINK */
	public static final String DEEP_LINK = "DEEPLINK";

	/**constants BALANCE_PUSH_ */
	public static final String BALANCE_PUSH_ = "BALANCE_PUSH_";

	/**constants ALERT */
	public static final String ALERT = "ALERT";

	/**constants INFO */
	public static final String INFO = "INFO";

	/**constants UPDATE */
	public static final String UPDATE = "UPDATE";

	/**constants EVENTS */
	public static final String EVENTS = "EVENTS";

	/**constants PIF_PREFIX */
	private static final String PIF_PREFIX = "PIF";

	/**Notification Center IDS
	 * constants CUSTOMER_MESSAGE_DELICUENCY_SKIP */
	public static final String CUSTOMER_MESSAGE_DELICUENCY_SKIP = "SKIP-";

	/**constants CUSTOMER_MESSAGE_UNICA_APPLICATION */
	public static final String CUSTOMER_MESSAGE_UNICA_APPLICATION = "UNICA-APPLICATION";

	/**constants CUSTOMER_MESSAGE_DELICUENCY_PASTDUE3 */
	public static final String CUSTOMER_MESSAGE_DELICUENCY_PASTDUE3 = "PASTDUE3-";

	/**constants CUSTOMER_MESSAGE_DELICUENCY_CHARGEOFF */
	public static final String CUSTOMER_MESSAGE_DELICUENCY_CHARGEOFF = "CHARGEOFF-";

	/**constants CUSTOMER_MESSAGE_BIRTHDAY */
	public static final String CUSTOMER_MESSAGE_BIRTHDAY = "BIRTHDAY";

	/**constants CUSTOMER_MESSAGE_PENDING_ACCOUNTS */
	public static final String CUSTOMER_MESSAGE_PENDING_ACCOUNTS = "PENDING";

	/**constants CUSTOMER_MESSAGE_COMPLETED_ACCOUNTS */
	public static final String CUSTOMER_MESSAGE_COMPLETED_ACCOUNTS = "COMPLETED";

	/**constants CUSTOMER_MESSAGE_SHOW_ALERT */
	public static final String CUSTOMER_MESSAGE_SHOW_ALERT = "SHOW_ALERT";

	/**Notification Center icons constants CUSTOMER_MESSAGE_EVENT_ICON */
	public static final String CUSTOMER_MESSAGE_EVENT_ICON = "/img/notificationCenter/event/birthday-icon.svg";

	/**constants CUSTOMER_MESSAGE_INFORMATION_ICON */
	public static final String CUSTOMER_MESSAGE_INFORMATION_ICON = "/img/notificationCenter/information/info-icon@1x.svg";

	/**constants CUSTOMER_MESSAGE_WARNING_ICON */
	public static final String CUSTOMER_MESSAGE_WARNING_ICON = "/img/notificationCenter/warning/alert-icon@1x.svg";

	/**constants CUSTOMER_MESSAGE_CAMPAIGN_ICON */
	public static final String CUSTOMER_MESSAGE_CAMPAIGN_ICON = "/img/notificationCenter/campaign/campaign-icon@1x.svg";

	/**constants CUSTOMER_MESSAGE_PUSH_ICON */
	public static final String CUSTOMER_MESSAGE_PUSH_ICON = "/img/notificationCenter/push/pushNotification-con@1x.svg";

	/**constants PUSH_TOKENS_OP_ENDPOINT */
	public static final String PUSH_TOKENS_OP_ENDPOINT = "pushtokens.tokens";

	/**constants SHOW_OOB */
	public static final String SHOW_OOB = "showOOB";

	/**constants NC_CSS_NEW_MESSAGE */
	public static final String NC_CSS_NEW_MESSAGE = "NC_CSS_NEW_MESSAGE";

	/**constants NC_CSS_VIEWED_MESSAGE */
	public static final String NC_CSS_VIEWED_MESSAGE = "NC_CSS_VIEWED_MESSAGE";

	/**
	 * Matches     132,123,123.23 | 123456.23 | 0.23
	 * Non-Matches 123,12         | 123.123   | 1322,132.23
	 */
	public static final String MONETARY_VALUE = "^([1-9]{1}[\\d]{0,2}(\\,[\\d]{3})*(\\.[\\d]{0,2})?"
			+ "|[1-9]{1}[\\d]{0,}(\\.[\\d]{0,2})?|0(\\.[\\d]{0,2})?|(\\.[\\d]{1,2})?)$";

	/**
	 * strNothing
	 */
	public static final String NOTHING = "";//

	/**Constant SHOW_GDPR*/
	public static final String SHOW_GDPR = "showGDPR"; //show gdpr

	public static final String CARD_NUMBER_LAST_FOUR = "LAST_CARD_NUMBER"; //card number last 4
	public static final String CREDIT_CARD_STR = "Credit Card: "; //credit card string
	public static final String PREVIOUS_AMOUNT = "PREVIOUS_AMOUNT"; //previous amount title
	public static final String PREVIOUS_AMOUNT_STR = "Previous Amount: "; //previous amount string
	public static final String NEW_AMOUNT = "NEW_AMOUNT"; //new amount title
	public static final String NEW_AMOUNT_STR = "New Amount: "; //new amount string

	/**
	 * getEstimateDateAccordingToPaymentType
	 * @return String The estimate date according to the payee's date rules.
	 */
	public static String getEstimateDateAccordingToPaymentType(TVFrontEndPayee tvFrontEndPayee,CustomerServices customerService,Calendar cal,String recurrent,String language){
		final String CHECK_ES="Cheque enviado por correo";
		final String CHECK_EN="Check mailed to payee";
		final String EN_LANGUAGE="EN";
		if (tvFrontEndPayee.getGlobalPayee() != null && tvFrontEndPayee.getGlobalPayee().getPaymentType() != null) {
			if (tvFrontEndPayee.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO
					|| tvFrontEndPayee.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PUBLICO)
				return language.equalsIgnoreCase(EN_LANGUAGE)?CHECK_EN:CHECK_ES;
			else
				return Utils.getEstimateDate(customerService.getHolidays(), cal,
						tvFrontEndPayee.getGlobalPayee().getPaymentType(), recurrent);
		} else
			return Utils.getEstimateDate(customerService.getHolidays(), cal, GlobalPayeePaymentType.INTERNO);
	}
	/**
	 * getPattern
	 * @return String
	 */
	private static String getPattern() {
		try {
			byte[] patternBytes = new byte[] { 40, 40, 63, 61, 46, 42, 92, 100, 41, 40, 63, 61, 46, 42, 91, 65, 45, 90,
					93, 41, 40, 94, 91, 97, 45, 122, 65, 45, 90, 48, 45, 57, 92, 46, 126, 96, -62, -95, 33, 64, 35, 92,
					63, 92, 36, 37, 92, 94, 38, 92, 42, 92, 40, 92, 41, 95, 92, 43, 45, 61, 44, 47, -62, -65, 58, 59,
					39, 34, 92, 91, 92, 93, 92, 123, 92, 125, 92, 92, 124, 60, 62, 93, 42, 36, 41, 41 };
			return new String(patternBytes, "UTF-8");
		} catch (Exception e) {
			return "((?=.*\\d)(?=.*[A-Z])(^[a-zA-Z0-9\\.~`Â¡!@#\\?\\$%\\^&\\*\\(\\)_\\+-=,/Â¿:;'\"\\[\\]\\{\\}\\\\|<>]*$))";
		}
	}

	/**
	 * getEstimateDate
	 * @param holidays List
	 * @param effectiveDate Calendar
	 * @param type GlobalPayeePaymentType
	 * @return String EstimateDate
	 */
	public static String getEstimateDate(List<Holiday> holidays, Calendar effectiveDate, GlobalPayeePaymentType type) {
		BankingDaysUtils bankingDaysUtils = new BankingDaysUtils(holidays);

		int days = 0;
		switch (type) {
			case DIRECT_DEBIT:
				days = 0;
				break;
			case TRANSMISION_ADELANTADA:
				days = 0;
				break;
			case INTERNO:
				days = 0;
				break;
			case NO_GARANTIZADO:
				days = 1;
				break;
			case MULTIPAGO_MENSUALIDAD:
				days = 2;
				break;
			case MULTIPAGO_CUOTA:
				days = 2;
				break;
			case MULTIPAGO_MATRICULA:
				days = 2;
				break;
			case MASTER_CARD_RPPS:
				days = 3;
				break;
			case GARANTIZADO:
				days = 2;
				break;
			case CHEQUE_Y_LISTA_PRIVADO:
				days = 1;
				break;
			case CHEQUE_Y_LISTA_PUBLICO:
				days = 1;
				break;
			// mpicado, cibp-1453, se agrega la condicion para el nuevo tipo, fee
			// category 21
			case COM_EXTERNO_POP:
				days = 0;
				break;
			default:
				days = 1;
				break;
		}

		SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar received = (Calendar) effectiveDate.clone();
		if (days != 0) {
			received = bankingDaysUtils.addBankingDays(received, days);
		}

		return dateformat.format(received.getTime());
	}

	/**
	 * RealTimePayments
	 * @param holidays List
	 * @param effectiveDate Calendar
	 * @param type GlobalPayeePaymentType
	 * @param frequency String
	 * @return EstimateDate
	 */
	public static String getEstimateDate(List<Holiday> holidays, Calendar effectiveDate, GlobalPayeePaymentType type,
										 String frequency) {
		BankingDaysUtils bankingDaysUtils = new BankingDaysUtils(holidays);

		int days = 0;
		switch (type) {
			case DIRECT_DEBIT:
				days = 0;
				break;
			case TRANSMISION_ADELANTADA:
				days = 0;
				break;
			case INTERNO:
				days = 0;
				break;
			case NO_GARANTIZADO:
				days = 1;
				break;
			case MULTIPAGO_MENSUALIDAD:
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138)
						&& isRealTime(effectiveDate.getTime(), frequency)) {
					days = 1;
				} else {
					days = 2;
				}
				break;
			case MULTIPAGO_CUOTA:
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138)
						&& isRealTime(effectiveDate.getTime(), frequency)) {
					days = 1;
				} else {
					days = 2;
				}
				break;
			case MULTIPAGO_MATRICULA:
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138)
						&& isRealTime(effectiveDate.getTime(), frequency)) {
					days = 1;
				} else {
					days = 2;
				}
				break;
			case MASTER_CARD_RPPS:
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138)
						&& isRealTime(effectiveDate.getTime(), frequency)) {
					days = 2;
				} else {
					days = 3;
				}
				break;
			case GARANTIZADO:
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE138)
						&& isRealTime(effectiveDate.getTime(), frequency)) {
					days = 1;
				} else {
					days = 2;
				}
				break;
			case CHEQUE_Y_LISTA_PRIVADO:
				days = 1;
				break;
			case CHEQUE_Y_LISTA_PUBLICO:
				days = 1;
				break;
			case COM_EXTERNO_POP:
				days = 0;
				break;
			default:
				days = 1;
				break;
		}

		SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar received = (Calendar) effectiveDate.clone();
		if (days != 0) {
			received = bankingDaysUtils.addBankingDays(received, days);
		}

		return dateformat.format(received.getTime());
	}

	/**
	 * isRealTime
	 * @param date Date
	 * @param frequency String
	 * @return true o false
	 */
	public static boolean isRealTime(Date date, String frequency) {
		boolean is = false;
		if (isToday(date) && "variable".equals(frequency)) {
			is = true;
		}
		return is;
	}

	/**
	 * isToday
	 * @param date Date
	 * @return true o false
	 */
	private static boolean isToday(Date date) {
		boolean is = false;
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date today = calendar.getTime();

		if (!date.after(today)) {
			is = true;
		}

		return is;
	}
	/**
	 * getRSALanguage
	 * @param request HttpServletRequest
	 * @return RSALanguage
	 */
	public static String getRSALanguage(HttpServletRequest request) {
		/** Set language to RSA **/
		Locale lang = null;
		lang = RequestContextUtils.getLocale(request);
		if (lang == null) {
			return "sp";
		}

		if ("es".equalsIgnoreCase(lang.getLanguage())) {
			return "sp";
		} else if ("en".equalsIgnoreCase(lang.getLanguage())) {
			return "en";
		}

		return "sp";
	}

	/**
	 * getAlertLanguage
	 * @param request HttpServletRequest
	 * @return Alert Language
	 */
	public static String getAlertLanguage(HttpServletRequest request) {
		if (getRSALanguage(request).equalsIgnoreCase("en")) {
			return "I";
		} else {
			return "E";
		}
	}

	/**
	 * getAccountById
	 * @param id String
	 * @param accounts List
	 * @return TVFrontendAccount
	 */
	public static TVFrontendAccount getAccountById(String id, List<TVFrontendAccount> accounts) {
		if (accounts != null) {
			for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount act = iterator.next();
				if (String.valueOf(act.getFrontEndId()).equals(id)) {
					return act;
				}
			}
		}
		return null;
	}

	/**
	 * getAccountTransferSourceById
	 * @param id String
	 * @param accounts List
	 * @return TVFrontendAccount
	 */
	public static TVFrontendAccount getAccountTransferSourceById(String id, List<TVAccountTransferSource> accounts) {
		if (accounts != null) {
			for (Iterator<TVAccountTransferSource> iterator = accounts.iterator(); iterator.hasNext();) {
				TVAccountTransferSource act = iterator.next();
				if (String.valueOf(act.getFrontEndId()).equals(id)) {
					return act.getAccount();
				}
			}
		}
		return null;
	}

	/**
	 * getAggregatedAccountById
	 * @param id int
	 * @param accounts List
	 * @return AggregatedAccount
	 */
	public static AggregatedAccount getAggregatedAccountById(int id, List<AggregatedAccount> accounts) {
		if (accounts != null) {
			for (Iterator<AggregatedAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				AggregatedAccount act = iterator.next();
				if (act.getId() == id) {
					return act;
				}
			}
		}
		return null;
	}

	/***
	 * getSMSAccountById
	 * @param id String
	 * @param accounts List
	 * @return SMSAccount
	 */
	public static SMSAccount getSMSAccountById(String id, List<SMSAccount> accounts) {
		if (accounts != null) {
			for (Iterator<SMSAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				SMSAccount act = iterator.next();
				if (act.getId().equalsIgnoreCase(id)) {
					return act;
				}
			}
		}
		return null;
	}

	/**
	 * sendSms
	 * @param phoneInfo CustomerPhone
	 * @param userSession UserSession
	 * @param request HttpServletRequest
	 * @param message String
	 * @return true o false
	 */
	public static boolean sendSms(CustomerPhone phoneInfo, UserSession userSession, HttpServletRequest request, String message) {
		try {
			CustomerServices customerService = userSession.getCustomerService();
			SendSmsGeneratedCodeRequest codereq = new SendSmsGeneratedCodeRequest(
					Utils.generateCustomerInteractionEvent(request), userSession.getCustomerProfile(), message,
					userSession.getCustomerProfile().getCustomerPhone().getPhoneNumber(),
					userSession.getCustomerProfile().getCustomerPhone().getPhoneProvider()); // TODO:
			// Change
			// provider!!!
			// LALR

			if (getRSALanguage(request).equalsIgnoreCase("en")) {
				customerService.sendGeneratedCode(codereq, PreferredLanguage.ENGLISH);
			} else {
				customerService.sendGeneratedCode(codereq, PreferredLanguage.SPANISH);
			}

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * getLiteFromCustomerProfile
	 * @param customerProfile CustomerProfile
	 * @return customerProfileLite
	 */
	public static CustomerProfileLite getLiteFromCustomerProfile(CustomerProfile customerProfile) {

		final String defaultOfacId = "0"; //default Ofac id for database
		CustomerProfileLite customerProfileLite = new CustomerProfileLite();
		customerProfileLite.setProfileId(customerProfile.getProfileId());
		customerProfileLite.setFirstName(customerProfile.getFirstName());
		customerProfileLite.setLastName(customerProfile.getLastName());
		customerProfileLite.setEmail(customerProfile.getEmail());
		customerProfileLite.setCreationDate(customerProfile.getActivationDate());
		customerProfileLite.setCustomerPhone(customerProfile.getCustomerPhone());
		customerProfileLite.setCountry(customerProfile.getCountry());
		customerProfileLite.setCustomerDevice(customerProfile.getCustomerDevice());
		customerProfileLite.setOfacId(defaultOfacId);

		return customerProfileLite;
	}

	/**
	 * getCustomerProfileFromLite
	 * @param customerProfileLite CustomerProfileLite
	 * @return CustomerProfile
	 */
	public static CustomerProfile getCustomerProfileFromLite(CustomerProfileLite customerProfileLite) {
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setProfileId(customerProfileLite.getProfileId());
		customerProfile.setFirstName(customerProfileLite.getFirstName());
		customerProfile.setLastName(customerProfileLite.getLastName());
		customerProfile.setEmail(customerProfileLite.getEmail());
		customerProfile.setOfacId(customerProfileLite.getOfacId());
		customerProfile.setCreationDate(customerProfileLite.getCreationDate());
		customerProfile.setCustomerPhone(customerProfileLite.getCustomerPhone());
		customerProfile.setCountry(customerProfileLite.getCountry());
		customerProfile.setCustomerPhone(customerProfileLite.getCustomerPhone());
		customerProfile.setCustomerDevice(customerProfileLite.getCustomerDevice());

		return customerProfile;
	}

	/**
	 *  CIBP-1104 getCustomerEmailBalanceAlertById
	 * @param id String
	 * @param accounts List
	 * @return CustomerEmailBalanceAlert
	 */
	public static CustomerEmailBalanceAlert getCustomerEmailBalanceAlertById(String id, List<CustomerEmailBalanceAlert> accounts) {
		if (accounts != null) {
			for (Iterator<CustomerEmailBalanceAlert> iterator = accounts.iterator(); iterator.hasNext();) {
				CustomerEmailBalanceAlert act = iterator.next();
				if (act.getId().equalsIgnoreCase(id)) {
					return act;
				}
			}
		}
		return null;
	}

	/**
	 * getAccountByKey
	 * @param id String
	 * @param accounts List
	 * @return TVFrontendAccount
	 */
	public static TVFrontendAccount getAccountByKey(String id, List<TVFrontendAccount> accounts) {
		if (accounts != null) {
			for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount act = iterator.next();
				if (act.getAccountKey().equals(StringUtils.trimToEmpty(id))) {
					return act;
				}
			}
		}
		return null;
	}

	/**
	 * getAccountByAccountNumberAndType
	 * @param type String
	 * @param accountNumber String
	 * @param accounts List
	 * @param customerService CustomerServices
	 * @return TVFrontendAccount
	 */
	public static TVFrontendAccount getAccountByAccountNumberAndType(String type, String accountNumber,
																	 List<TVFrontendAccount> accounts, CustomerServices customerService) {

		if (accounts != null) {
			for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount act = iterator.next();
				AccountUtils.getSuffix(act);
				if (GenericValidator.isBlankOrNull(act.getAccountNumberSuffix())) {

					/**
					 * MBNA-1177
					 * Method getDepositProductType : CIBP-WEB microservice integration
					 */
					DepositProduct depositProduct = AccountsFacade.getInstance().getDepositProductType(act.getBankId(),
							act.getProductDefinition().getProductId(), act.getAccountNumberSuffix());

					if (depositProduct != null && act.getAccountNumber().equals(accountNumber)) {
						return act;
					}
				} else {
					if ((act.getAccountNumberSuffix().equals(type) && act.getAccountNumber().equals(accountNumber))) {
						return act;
					}
				}
			}
		}
		return null;
	}

	/**
	 * getAccountByAccountNumber
	 * @param id String
	 * @param accounts List
	 * @return TVFrontendAccount
	 */
	public static TVFrontendAccount getAccountByAccountNumber(String id, List<TVFrontendAccount> accounts) {
		if (accounts != null)
			for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount act = iterator.next();
				if (act.getAccountNumber().matches("^\\d+$") && id.matches("^\\d+$")
						&& act.getAccountNumber().trim().length() < 20 && id.trim().length() < 20) {
					if (Long.parseLong(act.getAccountNumber().trim()) == Long.parseLong(id.trim()))
						return act;
				} else {
					if (act.getAccountNumber().trim().equals(id.trim()))
						return act;
				}
			}
		return null;
	}

	/**
	 * getAccountByAccountNumberForIRACAE
	 * @param id String
	 * @param accounts List
	 * @return TVFrontendAccount
	 */
	public static TVFrontendAccount getAccountByAccountNumberForIRACAE(String id, List<TVFrontendAccount> accounts) {
		if (accounts != null)
			for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount act = iterator.next();
				if (act.getAccountNumber().matches("^\\d+$") && id.matches("^\\d+$")
						&& act.getAccountNumber().trim().length() < 20 && id.trim().length() < 20) {
					if (Long.parseLong(act.getAccountNumber().trim()) == Long.parseLong(id.trim()))
						return act;
				} else {
					if (act.getAccountNumber().trim().equals(id.trim())
							|| act.getAccountNumber().replaceFirst("^0+(?!$)", "").startsWith(id))
						return act;
				}
			}
		return null;
	}

	/**
	 * getPlasticRecordByFrontEndId
	 * @param frontEndId String
	 * @param plastics CardPlasticFrontend
	 * @return CardPlasticFrontend
	 */
	public static CardPlasticFrontend getPlasticRecordByFrontEndId(String frontEndId, CardPlasticFrontend[] plastics) {
		if (plastics != null)
			for (CardPlasticFrontend plastic : plastics) {
				if (plastic == null || GenericValidator.isBlankOrNull(plastic.getFrontEndId()))
					return null;

				if (frontEndId.equals(plastic.getFrontEndId())) {
					return plastic;
				}
			}
		return null;
	}

	/**
	 * getTransferById
	 * @param id String
	 * @param transfer List
	 * @return TVAccountTransfer
	 */
	public static TVAccountTransfer getTransferById(String id, List<TVAccountTransfer> transfer) {
		if (transfer != null)
			for (Iterator<TVAccountTransfer> iterator = transfer.iterator(); iterator.hasNext();) {
				TVAccountTransfer act = iterator.next();
				if (String.valueOf(act.getFrontEndId()).equals(id))
					return act;
			}
		return null;
	}

	/**
	 * getPaymentById
	 * @param id String
	 * @param payments List
	 * @return TVPayment
	 */
	public static TVPayment getPaymentById(String id, List<TVPayment> payments) {
		if (payments != null)
			for (Iterator<TVPayment> iterator = payments.iterator(); iterator.hasNext();) {
				TVPayment act = iterator.next();
				if (String.valueOf(act.getFrontEndId()).equals(id))
					return act;
			}
		return null;
	}

	/**
	 * getProcessedPaymentById
	 * @param id String
	 * @param payeeId String
	 * @param referenceNum String
	 * @param payments List
	 * @return TVPayment
	 */
	public static TVPayment getProcessedPaymentById(String id, String payeeId, String referenceNum,
													List<TVPayment> payments) {
		if (payments != null)
			for (Iterator<TVPayment> iterator = payments.iterator(); iterator.hasNext();) {
				TVPayment payment = iterator.next();

				if (payment != null && payment.getPayee() != null
						&& String.valueOf(payment.getPayee().getFrontEndId()).equals(id)
						&& String.valueOf(payment.getTVPayment().getPayeeId()).equals(payeeId)
						&& String.valueOf(payment.getTVPayment().getReferenceNumber()).equals(referenceNum))
					return payment;
			}
		return null;
	}

	/**
	 * getPayeebyPayeeId
	 * @param id String
	 * @param payees List
	 * @return TVFrontEndPayee
	 */
	public static TVFrontEndPayee getPayeebyPayeeId(String id, List<TVFrontEndPayee> payees) {
		if (payees != null)
			for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
				TVFrontEndPayee name = iterator.next();
				if (name.getCustomerPayeeCodes() != null && name.getCustomerPayeeCodes().contains(Integer.parseInt(id)))
					return name;
			}

		return null;

	}

	/**
	 * getPayeebyId
	 * @param id String
	 * @param payees List
	 * @return TVFrontEndPayee
	 */
	public static TVFrontEndPayee getPayeebyId(String id, List<TVFrontEndPayee> payees) {

		if (payees != null && id != null)
			for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
				TVFrontEndPayee name = iterator.next();
				if (name.getFrontEndId().equalsIgnoreCase(id))
					return name;
			}

		return null;

	}

	/**
	 * getPayeebyAccountNumber
	 * @param id String
	 * @param payees List
	 * @return TVFrontEndPayee
	 */
	public static TVFrontEndPayee getPayeebyAccountNumber(String id, List<TVFrontEndPayee> payees) {
		if (payees != null && id != null)
			for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
				TVFrontEndPayee name = iterator.next();
				if (name.getBillingAccount().trim().equalsIgnoreCase(id))
					return name;
			}

		return null;

	}

	/**
	 * getPayeebyAccountNumberType
	 * @param id String
	 * @param payees List
	 * @param payeeId String
	 * @return TVFrontEndPayee
	 */
	public static TVFrontEndPayee getPayeebyAccountNumberType(String id, List<TVFrontEndPayee> payees, String payeeId) {
		if (payees != null && id != null)
			for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext();) {
				TVFrontEndPayee name = iterator.next();
				if (name.getBillingAccount().trim().equalsIgnoreCase(id))
					return name;
			}

		return null;

	}

	/**
	 * getAccountSourceByActKey
	 * @param id String
	 * @param accounts Set
	 * @return TVAccountTransferSource
	 */
	public static TVAccountTransferSource getAccountSourceByActKey(String id, Set<TVAccountTransferSource> accounts) {

		if (accounts != null)
			for (Iterator<TVAccountTransferSource> iterator = accounts.iterator(); iterator.hasNext();) {
				TVAccountTransferSource source = iterator.next();
				TVFrontendAccount act = source.getAccount();
				if (String.valueOf(act.getAccountKey()).equals(id))
					return source;
			}
		return null;
	}

	/**
	 * getAccountSourceByKey
	 * @param id String
	 * @param accounts Set
	 * @return TVFrontendAccount
	 */
	public static TVFrontendAccount getAccountSourceByKey(String id, Set<TVAccountTransferSource> accounts) {

		if (accounts != null)
			for (Iterator<TVAccountTransferSource> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount act = iterator.next().getAccount();
				if (String.valueOf(act.getAccountKey()).equals(id))
					return act;
			}
		return null;
	}

	/**
	 * getAccountSourceByNumber
	 * @param id String
	 * @param accounts Set
	 * @return TVFrontendAccount
	 */
	public static TVFrontendAccount getAccountSourceByNumber(String id, Set<TVAccountTransferSource> accounts) {

		if (accounts != null)
			for (Iterator<TVAccountTransferSource> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount act = iterator.next().getAccount();
				if (act.getAccountNumber().equals(id))
					return act;
			}
		return null;
	}

	/**
	 * getAccountTargetByKey
	 * @param id String
	 * @param accounts Set
	 * @return TVFrontendAccount
	 */
	public static TVFrontendAccount getAccountTargetByKey(String id, Set<TVAccountTransferTarget> accounts) {

		if (accounts != null)
			for (Iterator<TVAccountTransferTarget> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount act = iterator.next().getAccount();
				if (String.valueOf(act.getAccountKey()).equals(id))
					return act;
			}
		return null;
	}

	/**
	 * getUserSession
	 * @param request HttpServletRequest
	 * @return UserSession
	 */
	public static UserSession getUserSession(HttpServletRequest request) {
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

		if (session == null)
			throw new ProgrammingError("UserSession is not available in Session.");
		return session;
	}

	/**
	 * getAccountSource
	 * @param id String
	 * @param sources Set
	 * @return TVAccountTransferSource
	 */
	public static TVAccountTransferSource getAccountSource(String id, Set<TVAccountTransferSource> sources) {
		if (sources != null)
			for (Iterator<TVAccountTransferSource> iterator = sources.iterator(); iterator.hasNext();) {
				TVAccountTransferSource act = iterator.next();
				if (String.valueOf(act.getFrontEndId()).equals(id))
					return act;
			}
		return null;
	}

	/**
	 * getSourceByAccountKey
	 * @param id String
	 * @param sources Set
	 * @return TVAccountTransferSource
	 */
	public static TVAccountTransferSource getSourceByAccountKey(String id, Set<TVAccountTransferSource> sources) {
		if (sources != null)
			for (Iterator<TVAccountTransferSource> iterator = sources.iterator(); iterator.hasNext();) {
				TVAccountTransferSource act = iterator.next();
				if (String.valueOf(act.getAccount().getAccountKey()).equals(id))
					return act;
			}
		return null;
	}

	/**
	 * getAccountTarget
	 * @param id String
	 * @param targets Set
	 * @return TVAccountTransferTarget
	 */
	public static TVAccountTransferTarget getAccountTarget(String id, Set<TVAccountTransferTarget> targets) {
		if (targets != null)
			for (Iterator<TVAccountTransferTarget> iterator = targets.iterator(); iterator.hasNext();) {
				TVAccountTransferTarget act = iterator.next();
				if (String.valueOf(act.getFrontEndId()).equals(id))
					return act;
			}
		return null;
	}

	/**
	 * getTargetByAccountKey
	 * @param id String
	 * @param targets Set
	 * @return TVAccountTransferTarget
	 */
	public static TVAccountTransferTarget getTargetByAccountKey(String id, Set<TVAccountTransferTarget> targets) {
		if (targets != null)
			for (Iterator<TVAccountTransferTarget> iterator = targets.iterator(); iterator.hasNext();) {
				TVAccountTransferTarget act = iterator.next();
				if (String.valueOf(act.getAccount().getAccountKey()).equals(id))
					return act;
			}
		return null;
	}

	/**
	 * getStatementById
	 * @param id String
	 * @param statements List
	 * @return AbstractStatement
	 */
	public static AbstractStatement getStatementById(String id, List<AbstractStatement> statements) {
		if (statements != null)
			for (Iterator<AbstractStatement> iterator = statements.iterator(); iterator.hasNext();) {
				AbstractStatement act = iterator.next();
				if (act.getFrontEndId().equals(id))
					return act;
			}
		return null;
	}

	/**
	 * getStatements
	 * @param statements List
	 * @param accountStatements LinkedHashMap
	 * @param lang String
	 * @param ispremia true o falso
	 * @return LinkedHashMap
	 */
	public static LinkedHashMap<String, String> getStatements(List<AbstractStatement> statements,
															  LinkedHashMap<String, AbstractStatement> accountStatements, String lang, boolean ispremia) {
		LinkedHashMap<String, String> statementMap = new LinkedHashMap<>();
		if (accountStatements == null)
			accountStatements = new LinkedHashMap<>();

		SimpleDateFormat dateF = new SimpleDateFormat("MM/dd/yyyy");
		Locale locale = "en".equalsIgnoreCase(lang) ? Locale.ENGLISH : new Locale("es");

		SimpleDateFormat prmFormat = new SimpleDateFormat("MMMMM yyyy", locale);
		NumberTool numbertool = new NumberTool();
		if (statements != null && !statements.isEmpty()) {
			for (Iterator<AbstractStatement> iterator = statements.iterator(); iterator.hasNext();) {
				AbstractStatement element = iterator.next();
				accountStatements.put(element.getUniqueId(), element);
				if (lang.equals("en") && element.getFrontEndId().equals("1")){
					statementMap.put(element.getFrontEndId(), "Current");
				}
				else if (element.getFrontEndId().equals("1")){
					statementMap.put(element.getFrontEndId(), "Recientes");
				}
				else {
					if (ispremia)
						statementMap.put(element.getFrontEndId(), prmFormat.format(element.getStartDate()));
					else {
						if (dateF.format(element.getStartDate().getTime()).equals("01/02/0001") && lang.equals("en"))
							statementMap.put(element.getFrontEndId(),
									"Open Date - " + dateF.format(element.getEndDate().getTime()));
						else if (dateF.format(element.getStartDate().getTime()).equals("01/02/0001"))
							statementMap.put(element.getFrontEndId(),
									"Apertura - " + dateF.format(element.getEndDate().getTime()));
						else {

							if (element.getEndBalance() != null)
								statementMap.put(element.getFrontEndId(),
										dateF.format(element.getStartDate().getTime()) + " - "
												+ dateF.format(element.getEndDate().getTime()) + " - "
												+ numbertool.format("currency", element.getEndBalance()));
							else
								statementMap.put(element.getFrontEndId(), dateF.format(element.getStartDate().getTime())
										+ " - " + dateF.format(element.getEndDate().getTime()));
						}
					}
				}
			}
		}

		return statementMap;

	}

	/**
	 * verbose version of above for use by json api - Hector
	 * @param statements List
	 * @param lang String
	 * @param ispremia true o false
	 * @return getDetailedStatements
	 */
	public static LinkedHashMap<String, Map> getDetailedStatements(List<AbstractStatement> statements, String lang,
																   boolean ispremia) {
		LinkedHashMap<String, Map> statementMap = new LinkedHashMap<String, Map>();

		SimpleDateFormat dateF = new SimpleDateFormat("MM/dd/yyyy");
		Locale locale = "en".equalsIgnoreCase(lang) ? Locale.ENGLISH : new Locale("es");

		SimpleDateFormat prmFormat = new SimpleDateFormat("MMMMM yyyy", locale);
		NumberTool numbertool = new NumberTool();
		if (statements != null && !statements.isEmpty())
			for (Iterator<AbstractStatement> iterator = statements.iterator(); iterator.hasNext();) {
				AbstractStatement element = iterator.next();

				LinkedHashMap<String, Object> detailedStatement = new LinkedHashMap<String, Object>();
				detailedStatement.put("premia", ispremia);
				detailedStatement.put("frontEndId", element.getFrontEndId());
				if (lang.equals("en") && element.getFrontEndId().equals("1"))
					detailedStatement.put("description", "Current");
				else if (element.getFrontEndId().equals("1"))
					detailedStatement.put("description", "Recientes");
				else {
					if (ispremia) {
						detailedStatement.put("startDate", dateF.format(element.getStartDate().getTime()));
						detailedStatement.put("description", prmFormat.format(element.getStartDate()));
					} else {
						if (dateF.format(element.getStartDate().getTime()).equals("01/02/0001") && lang.equals("en")) {
							detailedStatement.put("description",
									"Open Date - " + dateF.format(element.getEndDate().getTime()));
							detailedStatement.put("endDate", dateF.format(element.getEndDate().getTime()));
						} else if (dateF.format(element.getStartDate().getTime()).equals("01/02/0001")) {
							detailedStatement.put("description",
									"Apertura - " + dateF.format(element.getEndDate().getTime()));
							detailedStatement.put("endDate", dateF.format(element.getEndDate().getTime()));
						} else {
							detailedStatement.put("startDate", dateF.format(element.getStartDate().getTime()));
							detailedStatement.put("endDate", dateF.format(element.getEndDate().getTime()));
							if (element.getEndBalance() != null) {
								detailedStatement.put("description",
										dateF.format(element.getStartDate().getTime()) + " - "
												+ dateF.format(element.getEndDate().getTime()) + " - "
												+ numbertool.format("currency", element.getEndBalance()));
								detailedStatement.put("endBalance",
										numbertool.format("currency", element.getEndBalance()));
							} else {
								detailedStatement.put("description", dateF.format(element.getStartDate().getTime())
										+ " - " + dateF.format(element.getEndDate().getTime()));
							}
						}
					}
				}
				statementMap.put(element.getFrontEndId(), detailedStatement);
			}

		return statementMap;

	}

	/**
	 * getServerHostname
	 * @return String
	 */
	public static String getServerHostname() {

		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "unknown";
		}

	}

	/**
	 * getHiddenToken
	 * @param ip String
	 * @return String
	 */
	public static String getHiddenToken(String ip) {

		String[] ipparts = ip.split("\\.");

		StringBuilder encodedtoken = new StringBuilder();
		encodedtoken.append("z").append(ipparts[3]).append("-").append(ipparts[2]).append("-").append(ipparts[1])
				.append("-").append(ipparts[0]);

		String reversed = encodedtoken.toString();

		reversed = String.valueOf(System.currentTimeMillis()) + reversed;

		reversed = reversed.replaceAll("1", "a");
		reversed = reversed.replaceAll("2", "b");
		reversed = reversed.replaceAll("6", "s");
		reversed = reversed.replaceAll("9", "y");

		return reversed;

	}

	/**
	 * generateCustomerInteractionEvent
	 * @param request HttpServletRequest
	 * @return CustomerInteractionEvent
	 */
	public static CustomerInteractionEvent generateCustomerInteractionEvent(HttpServletRequest request) {

		return generateCustomerInteractionEvent(request, "");

	}

	/**
	 * generateCustomerInteractionEvent
	 * @param request HttpServletRequest
	 * @param username String
	 * @return CustomerInteractionEvent
	 */
	public static CustomerInteractionEvent generateCustomerInteractionEvent(HttpServletRequest request,
																			String username) {

		String sessionId = "";
		String requestUsername = username;
		String profileId = "";

		HttpSession session = request.getSession(false);
		if (session != null) {

			sessionId = session.getId();

			UserSession sessionAttribute = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			if (sessionAttribute != null) {

				CustomerProfile customerProfile = sessionAttribute.getCustomerProfile();
				if (customerProfile != null) {
					profileId = customerProfile.getProfileId();
					requestUsername = customerProfile.getUsername();

				}
			}
		}

		return new CustomerInteractionEvent(request.getRemoteAddr(), sessionId, requestUsername, profileId,
				Utils.getServerHostname());

	}

	/**
	 * getAllDepositAccounts
	 * @param allAccounts List
	 * @return Account Product Id
	 */
	public static List<TVFrontendAccount> getAllDepositAccounts(List<TVFrontendAccount> allAccounts) {

		List<TVFrontendAccount> accounts = new LinkedList<>();

		for (Iterator<TVFrontendAccount> iterator = allAccounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount thisAccount = iterator.next();
			if (thisAccount.getAccountProductId().equals("009") || thisAccount.getAccountProductId().equals("015")
					|| thisAccount.getAccountProductId().equals("017")
					|| thisAccount.getAccountProductId().equals("020")
					|| thisAccount.getAccountProductId().equals("023")
					|| thisAccount.getAccountProductId().equals("024")
					|| thisAccount.getAccountProductId().equals("025")
					|| thisAccount.getAccountProductId().equals("026")
					|| thisAccount.getAccountProductId().equals("028")
					|| thisAccount.getAccountProductId().equals("029")
					|| thisAccount.getAccountProductId().equals("049")
					|| thisAccount.getAccountProductId().equals("051")
					|| thisAccount.getAccountProductId().equals("052")
					|| thisAccount.getAccountProductId().equals("060")
					|| thisAccount.getAccountProductId().equals("069")
					|| thisAccount.getAccountProductId().equals("072")
					|| thisAccount.getAccountProductId().equals("073")
					|| thisAccount.getAccountProductId().equals("080")
					|| thisAccount.getAccountProductId().equals("081")
					|| thisAccount.getAccountProductId().equals("087")) {

				if (!thisAccount.getAccountSection().equals(Utils.ACCT_SECTION_SAV) || thisAccount.getAccountProductId().equals("049")
						|| thisAccount.getAccountProductId().equals("051")
						|| thisAccount.getAccountProductId().equals("052"))
					accounts.add(thisAccount);
			}
		}

		return accounts;
	}

	/**
	 * getDFMDepositAccounts
	 * @param allAccounts List
	 * @param enrollAccounts String
	 * @return accounts
	 */
	public static List<TVFrontendAccount> getDFMDepositAccounts(List<TVFrontendAccount> allAccounts, String enrollAccounts) {
		List<TVFrontendAccount> accounts = new LinkedList<>();
		for (Iterator<TVFrontendAccount> iterator = allAccounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount thisAccount = iterator.next();
			String value = thisAccount.getBankId() + "-" + thisAccount.getAccountProductId();
			if(thisAccount.getSubtype().equals(Utils.IDA)
					&& !thisAccount.getAccountSection().equals(Utils.ACCT_SECTION_RES)
					&& enrollAccounts.contains(value)) {
				accounts.add(thisAccount);
			}
		}

		return accounts;
	}

	/**
	 * hasAtLeastOneDepositAccount
	 * @param allAccounts List
	 * @return true o false
	 */
	public static boolean hasAtLeastOneDepositAccount(List<TVFrontendAccount> allAccounts) {

		boolean hasDepAccount = false;

		for (Iterator<TVFrontendAccount> iterator = allAccounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount thisAccount = iterator.next();
			if (thisAccount.getAccountProductId().equals("009") || thisAccount.getAccountProductId().equals("015")
					|| thisAccount.getAccountProductId().equals("017")
					|| thisAccount.getAccountProductId().equals("020")
					|| thisAccount.getAccountProductId().equals("023")
					|| thisAccount.getAccountProductId().equals("024")
					|| thisAccount.getAccountProductId().equals("025")
					|| thisAccount.getAccountProductId().equals("026")
					|| thisAccount.getAccountProductId().equals("028")
					|| thisAccount.getAccountProductId().equals("029")
					|| thisAccount.getAccountProductId().equals("049")
					|| thisAccount.getAccountProductId().equals("051")
					|| thisAccount.getAccountProductId().equals("052")
					|| thisAccount.getAccountProductId().equals("060")
					|| thisAccount.getAccountProductId().equals("069")
					|| thisAccount.getAccountProductId().equals("072")
					|| thisAccount.getAccountProductId().equals("073")
					|| thisAccount.getAccountProductId().equals("080")
					|| thisAccount.getAccountProductId().equals("081")
					|| thisAccount.getAccountProductId().equals("087")) {

				hasDepAccount = true;
				break;
			}
		}

		return hasDepAccount;
	}

	/**
	 * hasUSaveAccount
	 * @param allAccounts List
	 * @return true o false
	 */
	public static boolean hasUSaveAccount(List<TVFrontendAccount> allAccounts) {

		boolean hasUSaveAccount = false;

		for (Iterator<TVFrontendAccount> iterator = allAccounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount thisAccount = iterator.next();
			if (thisAccount.getAccountProductId().equals("090")) {

				hasUSaveAccount = true;
				break;
			}
		}

		return hasUSaveAccount;
	}

	/**
	 * isDocIdFoundInEbppPrevHistoryInfo
	 * @param docId String
	 * @param listEbppHistory List
	 * @return true o false
	 */
	public static boolean isDocIdFoundInEbppPrevHistoryInfo(String docId,
															List<EbppPreviousHistoryInfo> listEbppHistory) {
		boolean found = false;
		for (Iterator<EbppPreviousHistoryInfo> iterator = listEbppHistory.iterator(); iterator.hasNext();) {
			EbppPreviousHistoryInfo name = iterator.next();
			if (name.getDocId().equals(docId)) {
				found = true;
				break;
			}
		}
		return found;
	}

	/**
	 * isValidPassword
	 * @param password String
	 * @return true o false
	 */
	public static Boolean isValidPassword(String password) {
		return GenericValidator.matchRegexp(password, Utils.PATTERN);
	}

	/**
	 * getAge
	 * @param birthday Date
	 * @return age
	 */
	public static int getAge(Date birthday) {
		GregorianCalendar today = new GregorianCalendar();
		GregorianCalendar bday = new GregorianCalendar();
		GregorianCalendar bdayThisYear = new GregorianCalendar();

		bday.setTime(birthday);
		bdayThisYear.setTime(birthday);
		bdayThisYear.set(Calendar.YEAR, today.get(Calendar.YEAR));

		int age = today.get(Calendar.YEAR) - bday.get(Calendar.YEAR);

		if (today.getTimeInMillis() < bdayThisYear.getTimeInMillis())
			age--;

		return age;
	}

	/**
	 * Get the list of user accounts
	 * @param accInfo String
	 * @param userSession UserSession
	 * @return UserAccount
	 */
	public static UserAccount getEbpp2UserAccountsByAccount(String accInfo, UserSession userSession) {
		// Get the list of user accounts
		List<UserAccount> ebpp2UserAccounts = userSession.getEbpp2UserAccountsAndSummaries();

		if (ebpp2UserAccounts != null) {
			for (Iterator<UserAccount> iterator2 = ebpp2UserAccounts.iterator(); iterator2.hasNext();) {
				UserAccount userAccount = iterator2.next();
				if (userAccount.getAccountNumber().replaceFirst("^0+(?!$)", "")
						.equals(accInfo.replaceFirst("^0+(?!$)", ""))) {
					return userAccount;
				}
			}
		}
		return null;
	}

	/**
	 * Get the list of user accounts
	 * @param accInfo String
	 * @param userSession UserSession
	 * @return true o false
	 */
	public static Boolean getPayeeisCCA(String accInfo, UserSession userSession) {
		// Get the list of user accounts
		List<TVFrontendAccount> acc = userSession.getCcaAvailbleToEbill();

		if (acc != null) {
			for (Iterator<TVFrontendAccount> iterator2 = acc.iterator(); iterator2.hasNext();) {
				TVFrontendAccount acc2 = iterator2.next();
				if (acc2.getAccountNumber().replaceFirst("^0+(?!$)", "").equals(accInfo.replaceFirst("^0+(?!$)", ""))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * enrollUserInRDC
	 * @param enrollmentInfo RDCEnrollmentInfoRequest
	 * @param userSession UserSession
	 * @param request HttpServletRequest
	 * @return RDCEnrollmentInfoResponse
	 */
	public static RDCEnrollmentInfoResponse enrollUserInRDC(RDCEnrollmentInfoRequest enrollmentInfo,
															UserSession userSession, HttpServletRequest request) {
		CustomerProfile profile = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		CustomerEntitlement rdcProfileEntitlement = profile.getEntitlementByType(EntitlementType.RDC);
		RDCEnrollmentInfoResponse response = new RDCEnrollmentInfoResponse();

		response.setRdcEnrollmentRequest(enrollmentInfo);

		if (rdcProfileEntitlement != null && rdcProfileEntitlement.getCustomerEntitlementStatus() != null
				&& (rdcProfileEntitlement.getCustomerEntitlementStatus() == EntitlementStatus.ENABLED
				|| rdcProfileEntitlement.getCustomerEntitlementStatus() == EntitlementStatus.OVERRIDE)
				&& customerService.getGlobalEntitlement(EntitlementType.RDC).getStatus() == EntitlementStatus.ENABLED) {

			List<TVFrontendAccount> accounts = userSession.getFisFilterAccounts();

			// This must be false in order to enroll the user in fis
			if (!userSession.isFisEnrolled()) {
				logger.info("Init FIS enrollment to user: " + userSession.getUsername());
				RDCEntitlement ent = userSession.getFisInfo();

				logger.debug("FIS USER ID AFTER UPDATE IS " + ent.getRdcCustomerId());
				logger.debug("FIS ACCT CHK GRP ID AFTER UPDATE IS " + ent.getRdcAccountGroupChk());
				logger.debug("FIS SSO ID AFTER UPDATE IS " + ent.getRdcSsoUserId());
				EnrollUserReq req = new EnrollUserReq();
				DepositorUser depositor = new DepositorUser();
				depositor.setSsoUserId(ent.getRdcSsoUserId());
				depositor.setUserName(profile.getUsername());
				depositor.setUserEmail(profile.getEmail());
				depositor.setDepositorName(
						replaceAccents(profile.getFirstName()) + " " + replaceAccents(profile.getLastName()));
				depositor.setCustomerId(ent.getRdcCustomerId());
				depositor.setAccountGroupId(ent.getRdcAccountGroupChk());

				Limits limits = new Limits();

				limits.setDailyLimit(enrollmentInfo.getUserDailyAmount());
				limits.setDailyLimitCount(enrollmentInfo.getUserDailyItem());
				limits.setMultiDayLimit(enrollmentInfo.getUserMultidayAmount());
				limits.setMultiDayLimitCount(enrollmentInfo.getUserMultidayItem());

				depositor.setLimits(limits);

				TVFrontendAccount sessionAccs[] = accounts.toArray(new TVFrontendAccount[accounts.size()]);

				if (sessionAccs != null && sessionAccs.length > 0) {
					EnrollAccount[] enrollAccs = new EnrollAccount[sessionAccs.length];

					for (int i = 0; i < enrollAccs.length; i++) {
						enrollAccs[i] = new EnrollAccount();
						enrollAccs[i].setAccountNumber(sessionAccs[i].getAccountNumber());

						AccountUtils.getSuffix(sessionAccs[i]);

						/**
						 * MBNA-1177
						 * Method getDepositProductType : CIBP-WEB microservice integration
						 */
						DepositProduct depositProduct = AccountsFacade.getInstance().getDepositProductType(sessionAccs[i].getBankId(), sessionAccs[i].getProductDefinition().getProductId(),
								sessionAccs[i].getAccountNumberSuffix());

						if (depositProduct != null && "Y".equalsIgnoreCase(depositProduct.getSavIndicator())) {
							enrollAccs[i].setAccountType("SAV");
							enrollAccs[i].setSettlementGroupId(enrollmentInfo.getSettlementGroupIdSav());
						} else if (depositProduct != null) {
							enrollAccs[i].setAccountType("CHK");
							enrollAccs[i].setSettlementGroupId(enrollmentInfo.getSettlementGroupIdChk());
						} else {
							logger.error("Account Product Id = " + sessionAccs[i].getAccountProductId() + " Bank Id ="
									+ sessionAccs[i].getBankId()
									+ " combination not found in the DEPOSIT_PRODUCTS TABLE.  The account will be sent as CHK.");
							enrollAccs[i].setAccountType("CHK");
							enrollAccs[i].setSettlementGroupId(enrollmentInfo.getSettlementGroupIdChk());
						}

						enrollAccs[i].setPayLimit(limits.getDailyLimit());
						enrollAccs[i].setAccountName(sessionAccs[i].getNickname());
						Limits accountLimits = new Limits();
						accountLimits.setDailyLimit(limits.getDailyLimit());
						accountLimits.setDailyLimitCount(limits.getDailyLimitCount());
						accountLimits.setMultiDayLimit(limits.getMultiDayLimit());
						accountLimits.setMultiDayLimitCount(limits.getMultiDayLimitCount());
						if (sessionAccs[i].getCustomerAccountPreferences() != null
								&& sessionAccs[i].getCustomerAccountPreferences().getDepositLimit() != null
								&& !sessionAccs[i].getCustomerAccountPreferences().getDepositLimit().equals("")) {
							accountLimits.setDailyLimit(
									new BigDecimal(sessionAccs[i].getCustomerAccountPreferences().getDepositLimit()));
							accountLimits.setMultiDayLimit(
									new BigDecimal(sessionAccs[i].getCustomerAccountPreferences().getDepositLimit()));
						}
						enrollAccs[i].setLimits(accountLimits);

					}
					req.setDepositorUser(depositor);
					req.setAccount(enrollAccs);
					req.setBid(enrollmentInfo.getRdcBid());
					// Enroll the user in FIS

					CustomerInteractionEvent interaction = Utils.generateCustomerInteractionEvent(request);
					interaction.setCustomStringData("Bid", req.getBid());
					interaction.setCustomStringData("Depositor Name", req.getDepositorUser().getDepositorName());
					interaction.setCustomStringData("Depositor Email", req.getDepositorUser().getUserEmail());
					interaction.setCustomStringData("Depositor UserName", req.getDepositorUser().getUserName());
					interaction.setCustomStringData("Depositor Customer Id", req.getDepositorUser().getCustomerId());
					interaction.setCustomStringData("Depositor Acct Group Id",
							req.getDepositorUser().getAccountGroupId());

					logger.info("FIS USER INFO SENT TO ENROLLMENT: \nCustomerId: " + ent.getRdcCustomerId()
							+ "\nSSO User Id: " + ent.getRdcSsoUserId() + "\nCHK Group Id: "
							+ ent.getRdcAccountGroupChk() + "\nSAV Group Id: " + ent.getRdcAccountGroupSav());
					EnrollUserResp enrollResp = customerService.enrollUserInFIS(req, interaction);
					if (enrollResp != null && enrollResp.getEnrollStatus() == 0
							&& enrollResp.getResponseStatus().equals(ResponseStatus.SUCCESS)) {
						logger.info("Init FIS Accounts Enrollment to user: " + userSession.getUsername());
						ent.setRdcFisEnrolled(true);
						ent.setRdcAcceptedTerms(true);
						List<RDCAccount> rdcAccs = null;

						GetAccountListReq listReq = new GetAccountListReq();
						listReq.setBid(enrollmentInfo.getRdcBid());
						listReq.setUserID(ent.getRdcCustomerId());

						// Get the user accounts in FIS
						GetAccountListResp accListResp = customerService.getAccountList(listReq);
						rdcAccs = CollectionUtils.arrayToList(accListResp.getAccounts());
						if (rdcAccs != null && accListResp.getResponseStatus() != ResponseStatus.ERROR) {
							Hashtable<String, Long> fisAccounts = new Hashtable<>();
							TVFrontendAccount account = null;

							for (int h = 0; h < rdcAccs.size(); h++) {
								account = Utils.getAccountByAccountNumberAndType(rdcAccs.get(h).getAccountType(),
										rdcAccs.get(h).getAccountNumber(), accounts, userSession.getCustomerService());
								if (account != null && account.getFrontEndId() != null
										&& rdcAccs.get(h).getAccountId() != 0)
									fisAccounts.put(account.getFrontEndId(), rdcAccs.get(h).getAccountId());
							}
							userSession.setFisAccounts(fisAccounts);
							userSession.setFisEnrolled(true);
							logger.info("Success - End FIS enrollment for user: " + userSession.getUsername());
							response.setResponseStatus(WebResponseStatus.SUCCESS);
							response.setServiceResponseStatus(enrollResp.getResponseStatus());
						} else {
							userSession.setFisEnrolled(false);
							userSession.setFisAccounts(null);
							logger.info("Failed - End FIS enrollment for user: " + userSession.getUsername());
							response.setResponseStatus(WebResponseStatus.FAILED);
							response.setServiceResponseStatus(enrollResp.getResponseStatus());
						}
					} else {
						userSession.setFisEnrolled(false);
						userSession.setFisAccounts(null);
						logger.info("Failed - End FIS enrollment for user: " + userSession.getUsername());
						response.setResponseStatus(WebResponseStatus.FAILED);
						if (enrollResp != null)
							response.setServiceResponseStatus(enrollResp.getResponseStatus());
						else
							response.setServiceResponseStatus(ResponseStatus.ERROR);
					}
				}
			} else {
				logger.info("User: " + userSession.getUsername() + " already enrolled in FIS in this session!!!!");
				response.setResponseStatus(WebResponseStatus.SUCCESS);
			}
		}
		if (customerService.getGlobalEntitlement(EntitlementType.RDC).getStatus() != EntitlementStatus.ENABLED) {
			logger.info("User: " + userSession.getUsername() + " enrollment failed because RDC is in maintenance!!!!");
			response.setResponseStatus(WebResponseStatus.MAINTENANCE);
		}

		return response;
	}

	/**
	 * replaceAccents
	 * @param string String
	 * @return String
	 */
	public static String replaceAccents(String string) {
		String notAccented = "aeiounuAEIOUNU";
		String accented = "áéíóúñüÁÉÍÓÚÑÜ";

		if (string != null && !"".equals(string))
			return StringUtils.replaceChars(string, accented, notAccented);
		else
			return "";
	}

	/**
	 * CIBP-2353 replaceCampaignVariables
	 * @param p MyMessagesCampaign
	 * @param campcust ActiveCampaignCustomerView
	 * @param logger Log
	 * @return MyMessagesCampaign
	 */
	public static MyMessagesCampaign replaceCampaignVariables(MyMessagesCampaign p, ActiveCampaignCustomerView campcust,
															  Log logger) {
		try {
			String temp;
			String replacement;
			// English Body
			temp = p.getEnglishBody();
			replacement = campcust.getString("messages_body_x1");
			if (replacement != null)
				temp = temp.replaceFirst("X1", replacement);
			replacement = campcust.getString("messages_body_x2");
			if (replacement != null)
				temp = temp.replaceFirst("X2", replacement);
			replacement = campcust.getString("messages_body_x3");
			if (replacement != null)
				temp = temp.replaceFirst("X3", replacement);
			replacement = campcust.getString("messages_body_x4");
			if (replacement != null)
				temp = temp.replaceFirst("X4", replacement);
			p.setEnglishBody(temp);
			// Spanish Body
			temp = p.getSpanishBody();
			replacement = campcust.getString("messages_body_x1_es");
			if (replacement != null)
				temp = temp.replaceFirst("X1", replacement);
			replacement = campcust.getString("messages_body_x2_es");
			if (replacement != null)
				temp = temp.replaceFirst("X2", replacement);
			replacement = campcust.getString("messages_body_x3_es");
			if (replacement != null)
				temp = temp.replaceFirst("X3", replacement);
			replacement = campcust.getString("messages_body_x4_es");
			if (replacement != null)
				temp = temp.replaceFirst("X4", replacement);
			p.setSpanishBody(temp);
		} catch (Exception e) {
			logger.error("Error: Campaign Variable / PortalCampaign ", e);
		}
		return p;
	}

	/**
	 * replaceCampaignVariables
	 * @param p PortalCampaign
	 * @param campcust ActiveCampaignCustomerView
	 * @param logger Log
	 * @return PortalCampaign
	 */
	public static PortalCampaign replaceCampaignVariables(PortalCampaign p, ActiveCampaignCustomerView campcust,
														  Log logger) {

		try {
			String temp;
			String replacement;
			// English Title
			temp = p.getEnglishTitle();
			replacement = campcust.getString("portal_title_x1");
			if (replacement != null)
				temp = temp.replaceFirst("X1", replacement);
			replacement = campcust.getString("portal_title_x2");
			if (replacement != null)
				temp = temp.replaceFirst("X2", replacement);
			replacement = campcust.getString("portal_title_x3");
			if (replacement != null)
				temp = temp.replaceFirst("X3", replacement);
			replacement = campcust.getString("portal_title_x4");
			if (replacement != null)
				temp = temp.replaceFirst("X4", replacement);
			p.setEnglishTitle(temp);
			// Spanish title
			if (replacement != null)
				temp = p.getSpanishTitle();
			replacement = campcust.getString("portal_title_x1_es");
			if (replacement != null)
				temp = temp.replaceFirst("X1", replacement);
			replacement = campcust.getString("portal_title_x2_es");
			if (replacement != null)
				temp = temp.replaceFirst("X2", replacement);
			replacement = campcust.getString("portal_title_x3_es");
			if (replacement != null)
				temp = temp.replaceFirst("X3", replacement);
			replacement = campcust.getString("portal_title_x4_es");
			if (replacement != null)
				temp = temp.replaceFirst("X4", replacement);
			p.setSpanishTitle(temp);
			// English body
			if (replacement != null)
				temp = p.getEnglishBody();
			replacement = campcust.getString("portal_body_x1");
			if (replacement != null)
				temp = temp.replaceFirst("X1", replacement);
			replacement = campcust.getString("portal_body_x2");
			if (replacement != null)
				temp = temp.replaceFirst("X2", replacement);
			replacement = campcust.getString("portal_body_x3");
			if (replacement != null)
				temp = temp.replaceFirst("X3", replacement);
			replacement = campcust.getString("portal_body_x4");
			if (replacement != null)
				temp = temp.replaceFirst("X4", replacement);
			p.setEnglishBody(temp);
			// Spanish body
			temp = p.getSpanishBody();
			replacement = campcust.getString("portal_body_x1_es");
			if (replacement != null)
				temp = temp.replaceFirst("X1", replacement);
			replacement = campcust.getString("portal_body_x2_es");
			if (replacement != null)
				temp = temp.replaceFirst("X2", replacement);
			replacement = campcust.getString("portal_body_x3_es");
			if (replacement != null)
				temp = temp.replaceFirst("X3", replacement);
			replacement = campcust.getString("portal_body_x4_es");
			if (replacement != null)
				temp = temp.replaceFirst("X4", replacement);
			p.setSpanishBody(temp);
			// English teaser title
			temp = p.getTeaserEnglishTitle();
			replacement = campcust.getString("portal_teaser_title_x1");
			if (replacement != null)
				temp = temp.replaceFirst("X1", replacement);
			replacement = campcust.getString("portal_teaser_title_x2");
			if (replacement != null)
				temp = temp.replaceFirst("X2", replacement);
			replacement = campcust.getString("portal_teaser_title_x3");
			if (replacement != null)
				temp = temp.replaceFirst("X3", replacement);
			replacement = campcust.getString("portal_teaser_title_x4");
			if (replacement != null)
				temp = temp.replaceFirst("X4", replacement);
			p.setTeaserEnglishTitle(temp);
			// Spanish teaser title
			temp = p.getTeaserSpanishTitle();
			replacement = campcust.getString("portal_teaser_title_x1_es");
			if (replacement != null)
				temp = temp.replaceFirst("X1", replacement);
			replacement = campcust.getString("portal_teaser_title_x2_es");
			if (replacement != null)
				temp = temp.replaceFirst("X2", replacement);
			replacement = campcust.getString("portal_teaser_title_x3_es");
			if (replacement != null)
				temp = temp.replaceFirst("X3", replacement);
			replacement = campcust.getString("portal_teaser_title_x4_es");
			if (replacement != null)
				temp = temp.replaceFirst("X4", replacement);
			p.setTeaserSpanishTitle(temp);
			// English teaser body
			temp = p.getTeaserEnglishBody();
			replacement = campcust.getString("portal_teaser_body_x1");
			if (replacement != null)
				temp = temp.replaceFirst("X1", replacement);
			replacement = campcust.getString("portal_teaser_body_x2");
			if (replacement != null)
				temp = temp.replaceFirst("X2", replacement);
			replacement = campcust.getString("portal_teaser_body_x3");
			if (replacement != null)
				temp = temp.replaceFirst("X3", replacement);
			replacement = campcust.getString("portal_teaser_body_x4");
			if (replacement != null)
				temp = temp.replaceFirst("X4", replacement);
			p.setTeaserEnglishBody(temp);
			// Spanish teaser body
			temp = p.getTeaserSpanishBody();
			replacement = campcust.getString("portal_teaser_body_x1_es");
			if (replacement != null)
				temp = temp.replaceFirst("X1", replacement);
			replacement = campcust.getString("portal_teaser_body_x2_es");
			if (replacement != null)
				temp = temp.replaceFirst("X2", replacement);
			replacement = campcust.getString("portal_teaser_body_x3_es");
			if (replacement != null)
				temp = temp.replaceFirst("X3", replacement);
			replacement = campcust.getString("portal_teaser_body_x4_es");
			if (replacement != null)
				temp = temp.replaceFirst("X4", replacement);
			p.setTeaserSpanishBody(temp);
		} catch (Exception e) {
			logger.error("Error: Campaign Variable / PortalCampaign ", e);
		}
		return p;
	}

	/**
	 * hasExcludeSegments
	 * @param p PortalCampaign
	 * @param s UserSession
	 * @param wealthCodes String
	 * @param preferredCodes String
	 * @return true o false
	 */
	public static Boolean hasExcludeSegments(PortalCampaign p, UserSession s, String wealthCodes,
											 String preferredCodes) {
		String exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.AUTO.toString());
		if (exclude != null && !exclude.equals("") && s.isHasAuto()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.CCA.toString());
		if (exclude != null && !exclude.equals("") && s.hasCCA()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.CD.toString());
		if (exclude != null && !exclude.equals("") && s.isHasCD()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.COMERCIAL.toString());
		if (exclude != null && !exclude.equals("") && s.isComercialCustomer()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.DDA.toString());
		if (exclude != null && !exclude.equals("") && s.isHasDDA()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.EACCOUNT.toString());
		if (exclude != null && !exclude.equals("") && s.isHasEaccount()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.EBILL.toString());
		if (exclude != null && !exclude.equals("") && s.isHasEbill()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.ESTATMENT.toString());
		if (exclude != null && !exclude.equals("") && s.isHasEstatment()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.INSURANCE.toString());
		if (exclude != null && !exclude.equals("") && s.isHasInsurance()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.IRA.toString());
		if (exclude != null && !exclude.equals("") && s.isHasIRA()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.LEASING.toString());
		if (exclude != null && !exclude.equals("") && s.isHasLeasing()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.MOBILE.toString());
		if (exclude != null && !exclude.equals("")
				&& s.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.MORTGAGE.toString());
		if (exclude != null && !exclude.equals("") && s.isHasMortgage()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.LOAN.toString());
		if (exclude != null && !exclude.equals("") && s.isHasPersonalLoans()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.PREFERRED.toString());
		if (exclude != null && !exclude.equals("") && s.isPreferred(wealthCodes, preferredCodes)) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.PREMIA.toString());
		if (exclude != null && !exclude.equals("") && s.isHasPremia()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.SECURITIES.toString());
		if (exclude != null && !exclude.equals("") && s.isHasSecurities()) {
			return true;
		}
		exclude = p.getCampaign().getExcludeSegment(CampaignSegmentType.WEALTH.toString());
		if (exclude != null && !exclude.equals("") && s.isWealth(wealthCodes, preferredCodes)) {
			return true;
		}
		return false;
	}

	/**
	 * hasIncludeSegments
	 * @param p PortalCampaign
	 * @param s UserSession
	 * @param wealthCodes String
	 * @param preferredCodes String
	 * @return true o false
	 */
	public static Boolean hasIncludeSegments(PortalCampaign p, UserSession s, String wealthCodes,
											 String preferredCodes) {
		String include = p.getCampaign().getIncludeSegment(CampaignSegmentType.AUTO.toString());
		if (include != null && !include.equals("") && s.isHasAuto()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.CCA.toString());
		if (include != null && !include.equals("") && s.hasCCA()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.CD.toString());
		if (include != null && !include.equals("") && s.isHasCD()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.COMERCIAL.toString());
		if (include != null && !include.equals("") && s.isComercialCustomer()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.DDA.toString());
		if (include != null && !include.equals("") && s.isHasDDA()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.EACCOUNT.toString());
		if (include != null && !include.equals("") && s.isHasEaccount()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.EBILL.toString());
		if (include != null && !include.equals("") && s.isHasEbill()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.ESTATMENT.toString());
		if (include != null && !include.equals("") && s.isHasEstatment()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.INSURANCE.toString());
		if (include != null && !include.equals("") && s.isHasInsurance()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.IRA.toString());
		if (include != null && !include.equals("") && s.isHasIRA()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.LEASING.toString());
		if (include != null && !include.equals("") && s.isHasLeasing()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.MOBILE.toString());
		if (include != null && !include.equals("")
				&& s.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.MORTGAGE.toString());
		if (include != null && !include.equals("") && s.isHasMortgage()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.LOAN.toString());
		if (include != null && !include.equals("") && s.isHasPersonalLoans()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.PREFERRED.toString());
		if (include != null && !include.equals("") && s.isPreferred(wealthCodes, preferredCodes)) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.PREMIA.toString());
		if (include != null && !include.equals("") && s.isHasPremia()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.SECURITIES.toString());
		if (include != null && !include.equals("") && s.isHasSecurities()) {
			return true;
		}
		include = p.getCampaign().getIncludeSegment(CampaignSegmentType.WEALTH.toString());
		if (include != null && !include.equals("") && s.isWealth(wealthCodes, preferredCodes)) {
			return true;
		}
		return false;
	}

	/**
	 * setHasStatementOrEbill
	 * @param s UserSession
	 */
	public static void setHasStatementOrEbill(UserSession s) {
		List<UserAccount> ebpp2Accounts = s.getEbpp2UserAccounts();
		/* Check if has a Ebill or a EStatement */
		if (ebpp2Accounts != null) {
			for (Iterator<UserAccount> iterator = ebpp2Accounts.iterator(); iterator.hasNext();) {
				UserAccount userAccount = iterator.next();
				// For this to be an e-Bill, the biller ID must be greater
				// than 1 OR
				// the product ID must be 55, 59 or 60 (credit card)
				if (userAccount.getBillerId() > 1 || (userAccount.getBillerId() == 1
						&& (userAccount.getBillerProductId() == 55 || userAccount.getBillerProductId() == 59
						|| userAccount.getBillerProductId() == 60 || userAccount.getBillerProductId() == 61))) {
					s.setHasEbill(true);
				}
				// For this to be an e-Statement, the biller ID must be 1
				// and
				// the product ID CANNOT be 55, 59, 60 or 61 (credit card)
				if (userAccount.getBillerId() == 1 && !(userAccount.getBillerProductId() == 55
						|| userAccount.getBillerProductId() == 59 || userAccount.getBillerProductId() == 60)
						|| userAccount.getBillerProductId() == 61) {
					s.setHasEstatment(true);
				}
			}
		}
	}

	/**
	 * Cibp-1886 enrollmentAccountExists
	 * @param accounts List
	 * @param profile CustomerProfile
	 * @return true o false
	 */
	public static boolean enrollmentAccountExists(List<TVFrontendAccount> accounts, CustomerProfile profile) {

		String enrollmentAccountNumber = profile.getEnrollmentAccountNumber();
		String subtype = profile.getEnrollmentAccountSubtype();

		// WHEN RETRIEVING ENROLLABLE ACCOUNTS CCA STARTING WITH 0377 TAKES AMEX
		// SUBTYPE VALUE
		// FOR THE CUSTOMER PROFILE
		if (subtype.equals(AMEX)) {
			subtype = CCA;
		}

		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount account = iterator.next();
			if (account.getAccountNumber().equals(enrollmentAccountNumber)
					&& account.getSubtype().equalsIgnoreCase(subtype)) {
				// 1886
				if (account.getTVAccount().getRecords().getStatusCode().equals("C")
						|| account.getTVAccount().getRecords().getStatusCode().equals("I")) {
					return false;
				}
				return true;
			}
		}

		return false;
	}

	/**
	 * This method verifies if user has an ACTIVE enrollable transactional account
	 * that isn't already enrolled.
	 *
	 * @param personalInformationValidationResponse
	 * @return true if customer has ACTIVE enrollable transactional account that
	 *         isn't already enrolled.
	 * @author Hector Ortiz ET56802
	 */
	public static boolean hasActiveEnrollableTransAcc(
			PersonalInformationValidationResponse personalInformationValidationResponse) {
		return hasActiveEnrollableAccountType(personalInformationValidationResponse, IDA)
				|| hasActiveEnrollableAccountType(personalInformationValidationResponse, PFC)
				|| hasActiveEnrollableAccountType(personalInformationValidationResponse, CCA)
				|| hasActiveEnrollableAccountType(personalInformationValidationResponse, AMEX);
	}

	/**
	 * This method validates if a customer has an ACTIVE enrollable account of the
	 * specified type and that account isn't already enrolled.
	 *
	 * @param personalInformationValidationResponse
	 * @param accType
	 *                                                  String specifing the account
	 *                                                  type e.g. CCA, AMEX, etc.
	 * @return true if customer has ACTIVE enrollable IDA that isn't already
	 *         enrolled.
	 * @author Hector Ortiz ET56802
	 */
	public static boolean hasActiveEnrollableAccountType(
			PersonalInformationValidationResponse personalInformationValidationResponse, String accType) {

		if (personalInformationValidationResponse == null
				|| personalInformationValidationResponse.getEnrollableAccounts() == null
				|| personalInformationValidationResponse.getExistingAccounts() == null || accType == null) {
			return false;
		}

		for (EnrollableAccount acc : personalInformationValidationResponse.getEnrollableAccounts()) {
			boolean enrolled = false;
			for (EnrollableAccount extAct : personalInformationValidationResponse.getExistingAccounts()) {
				if (extAct.getAccountId().equalsIgnoreCase(acc.getAccountId())
						&& extAct.getAccountType().equalsIgnoreCase(acc.getAccountType())
						&& extAct.getProductId().equalsIgnoreCase(acc.getProductId())) {
					enrolled = true;
					break;
				}
			}

			if (!enrolled && acc.getAccountType().equalsIgnoreCase(accType) && !acc.getStatus().equalsIgnoreCase("C")
					&& !acc.getStatus().equalsIgnoreCase("I")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * MBSFE-213 enrollmentAccountExists
	 * @param accounts List
	 * @param profile CustomerProfile
	 * @param userSession UserSession
	 * @return true o false
	 */
	public static boolean enrollmentAccountExists(List<TVFrontendAccount> accounts, CustomerProfile profile,
												  UserSession userSession) {

		String enrollmentAccountNumber = profile.getEnrollmentAccountNumber();
		String subtype = profile.getEnrollmentAccountSubtype();

		// WHEN RETRIEVING ENROLLABLE ACCOUNTS CCA STARTING WITH 0377 TAKES AMEX
		// SUBTYPE VALUE
		// FOR THE CUSTOMER PROFILE
		if (subtype.equals(AMEX)) {
			subtype = CCA;
		}

		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount account = iterator.next();
			if (account.getAccountNumber().equals(enrollmentAccountNumber)
					&& account.getSubtype().equalsIgnoreCase(subtype)) {
				// 1886
				String statusCode = account.getStatusCode();
				if ("C".equalsIgnoreCase(statusCode) || "I".equalsIgnoreCase(statusCode)) {
					return false;
				} else if ("N".equalsIgnoreCase(statusCode)) {
					userSession.setEnrollAcctPendingClose(true);
				}
				return true;
			}
		}

		return false;
	}

	/**
	 * mlaAccountStatus
	 * @param account String
	 * @param mlaAccounts String
	 * @return mla Account Status
	 */
	public static String mlaAccountStatus(String account, String mlaAccounts) {
		if (mlaAccounts.length() > 0 && mlaAccounts.contains(account)) {
			String[] accountResource = mlaAccounts.split(",");
			for (int i = 0; i < accountResource.length; i++) {
				String[] values = accountResource[i].split("-");
				if (values.length == 3) {
					if (values[2].equalsIgnoreCase("Y") && account.equalsIgnoreCase(values[0])) {
						return "Active";
					} else if (values[2].equalsIgnoreCase("N") && account.equalsIgnoreCase(values[0])) {
						return "Inactive";
					}
				} else {
					return "Not registered";
				}
			}
		}
		return "Not registered";
	}

	/**
	 * New Ebills implement
	 * @param account TVFrontendAccount
	 * @param userSession UserSession
	 * @param request HttpServletRequest
	 * @return statementsByAccount
	 */
	public static List<AbstractStatement> statementsByAccount(TVFrontendAccount account, UserSession userSession,
															  HttpServletRequest request) {
		List<AbstractStatement> availableStatements = null;
		if (account != null && account.getStatements() == null) {
			try {
				StatementRequest stmtrq = new StatementRequest(Utils.generateCustomerInteractionEvent(request),
						userSession.getCustomerProfile(), userSession.getBankingSession(), account,
						userSession.getMaxStatements());
				availableStatements = userSession.getCustomerService().getAvailableStatements(stmtrq)
						.getStatementList();
				account.setStatements(availableStatements);
			} catch (Exception e) {
				availableStatements = new LinkedList<>();
			}
		}
		if (account != null && account.getStatements() != null) {
			availableStatements = account.getStatements();
		}
		return availableStatements;
	}

	/**
	 * abstractToPdfStmt
	 * @param absStmt AbstractStatement
	 * @param acc TVFrontendAccount
	 * @return AccountPDFStmtInfo
	 */
	public static AccountPDFStmtInfo abstractToPdfStmt(AbstractStatement absStmt, TVFrontendAccount acc) {
		AccountPDFStmtInfo info = new AccountPDFStmtInfo();

		info.setAccount_type("PDFSTMT");
		info.setAccount_name(acc.getNickname());
		info.setAccount_nickname(acc.getNickname());
		info.setAccountNumber(acc.getAccountNumber());
		info.setDocId(absStmt.getPdfID());
		info.setId(absStmt.getPdfID());
		if (absStmt.getEndDate() != null) {
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			info.setInvoice_date(format.format(absStmt.getEndDate()));
		} else {
			info.setInvoice_date("");
		}
		info.setDueDate(absStmt.getEndDate());
		info.setEzStmtProductId(acc.getPayeeFrontEndId());
		info.setViewed(absStmt.isPdfView());

		return info;
	}

	/**
	 * accountByPDFID
	 * @param accounts List
	 * @param pdfId String
	 * @return TVFrontendAccount
	 */
	public static TVFrontendAccount accountByPDFID(List<TVFrontendAccount> accounts, String pdfId) {
		List<AbstractStatement> availableStatements = null;
		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount acc = iterator.next();
			if (acc.getStatements() != null) {
				availableStatements = acc.getStatements();
				for (Iterator<AbstractStatement> iterator2 = availableStatements.iterator(); iterator2.hasNext();) {
					AbstractStatement abstractStatement = iterator2.next();
					if (abstractStatement.getPdfID() != null && abstractStatement.getPdfID().equals(pdfId))
						return acc;
				}
			}
		}
		return null;
	}

	/**
	 * getIdaAccountById
	 * @param id String
	 * @param accounts List
	 * @return TVFrontendDepositAccount
	 */
	public static TVFrontendDepositAccount getIdaAccountById(String id, List<TVFrontendDepositAccount> accounts) {
		if (accounts != null)
			for (Iterator<TVFrontendDepositAccount> iterator = accounts.iterator(); iterator.hasNext();) {
				TVFrontendDepositAccount act = iterator.next();
				if (String.valueOf(act.getFrontEndId()).equals(id))
					return act;
			}
		return null;
	}

	/**
	 * getIdaAccountByAccountNumber
	 * @param accountNumber String
	 * @param idaAccounts List
	 * @return TVFrontendDepositAccount
	 */
	public static TVFrontendDepositAccount getIdaAccountByAccountNumber(String accountNumber,
																		List<TVFrontendDepositAccount> idaAccounts) {
		if (accountNumber != null && idaAccounts != null) {
			for (Iterator<TVFrontendDepositAccount> iterator = idaAccounts.iterator(); iterator.hasNext();) {
				TVFrontendDepositAccount act = iterator.next();
				if (act.getAccountNumber().equals(accountNumber))
					return act;
			}
		}
		return null;
	}

	/**
	 * getStatementByPdfId
	 * @param pdfID String
	 * @param accountsPdfs HashMap
	 * @return AccountPDFStmtInfo
	 */
	public static AccountPDFStmtInfo getStatementByPdfId(String pdfID,
														 HashMap<String, AccountPDFStmtInfo> accountsPdfs) {
		if (accountsPdfs != null) {
			return accountsPdfs.get(pdfID);
		}

		return null;

	}

	/**
	 * accountByHash
	 * @param accounts List
	 * @param hash String
	 * @return TVFrontendAccount
	 */
	public static TVFrontendAccount accountByHash(List<TVFrontendAccount> accounts, String hash) {
		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount acc = iterator.next();
			if (String.valueOf(acc.hashCode()).equals(hash))
				return acc;
		}
		return null;
	}

	/**
	 * filteredEbillsAccounts
	 * @param accounts List
	 * @return  only unique accounts numbers
	 */
	public static List<TVFrontendAccount> filteredEbillsAccounts(List<TVFrontendAccount> accounts) {
		List<TVFrontendAccount> filteredAccounts = new LinkedList<TVFrontendAccount>();
		for (Iterator<TVFrontendAccount> iterator = accounts.iterator(); iterator.hasNext();) {
			TVFrontendAccount acc = iterator.next();
			if (!acc.getTVAccount().getSubType().equals("PRM") && acc.getIsAvailableElecStmt()) {
				filteredAccounts.add(acc);
			}
		}
		return filteredAccounts;
	}

	/**
	 * This method is created just for help a logging
	 * @param request HttpServletRequest
	 * @param userSession UserSession
	 * @param message String
	 */
	public static void logEnrollmentAccount(HttpServletRequest request, UserSession userSession, String message) {
		if (userSession.getCustomerProfile().useCommercialInterface()) {
			CustomerInteractionEvent interactionEvent = Utils.generateCustomerInteractionEvent(request, null);
			interactionEvent.setCustomStringData("message", message);
			userSession.getCustomerService().logEnrollmentNewAccounts(interactionEvent, InteractionSeverity.ERROR);
		}
	}

	/**
	 * haveType
	 * @param types String
	 * @param type String
	 * @return true o false
	 */
	public static boolean haveType(String types, String type) {
		String[] types2 = types.split(",");
		for (int i = 0; i < types2.length; i++) {
			if (types2[i].equals(type)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * markPdfViewed
	 * @param ebill AbstractStatement
	 * @param cca TVFrontendAccount
	 * @param userSession UserSession
	 */
	public static void markPdfViewed(AbstractStatement ebill, TVFrontendAccount cca, UserSession userSession) {
		StatementViewRequest statementViewedRequest = new StatementViewRequest();

		if (ebill.getPdfID() != null && ebill.getEndDate() != null && !ebill.isPdfView()) {

			if (cca != null) {
				try {
					statementViewedRequest.setAccountClassCd("CARD");
					statementViewedRequest.setAccountTypeCd(cca.getSubtype());
					statementViewedRequest.setAccountId(cca.getAccountNumber());
					statementViewedRequest.setBankId(cca.getBankId());
					statementViewedRequest.setSessKey(userSession.getBankingSession().getSessionKey());
					statementViewedRequest.setPDFViewPartyId(userSession.getBankingSession().getPermId().longValue());
					statementViewedRequest.setRqUID(UUID.randomUUID().toString());
					Date stringDate = ebill.getEndDate();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String endDate = sdf.format(stringDate);
					statementViewedRequest.setStatementDt(endDate);
					String permId = String.valueOf(userSession.getCustomerProfile().getPermId());
					statementViewedRequest.setCustPermId(permId);
					StatementViewService stamentService = userSession.getCustomerService().StatementViewedService();
					StatementViewResponse statementViewedResponse = stamentService
							.StatementViewed(statementViewedRequest);
					if (statementViewedResponse != null
							&& statementViewedResponse.getSeverity().indexOf("SUCCESS") < 0) {
						logger.error(statementViewedResponse);
					} else {
						ebill.setPdfView(true);
						userSession.setCcaEbills(null);
						userSession.setCurrentVaultStatements(null);// Reset GreenBox
						userSession.setCcaCurrentStatements(null);
						userSession.setDdaCurrentStatements(null);
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
			}
		}
	}

	/**
	 * markPDFsViewed
	 * @param ebills List
	 * @param cca TVFrontendAccount
	 * @param userSession UserSession
	 */
	public static void markPDFsViewed(List<AbstractStatement> ebills, TVFrontendAccount cca, UserSession userSession) {

		for (Iterator<AbstractStatement> iterator2 = ebills.iterator(); iterator2.hasNext();) {
			AbstractStatement ebill = iterator2.next();
			markPdfViewed(ebill, cca, userSession);

		}
	}

	/**
	 * getCCAAccountsMap
	 * @param accounts List
	 * @param accountsMap Map
	 * @return accountsMap
	 */
	public static Map<Object, Object> getCCAAccountsMap(List<TVFrontendAccount> accounts,
														Map<Object, Object> accountsMap) {
		AccountMaskUtil maskutil = new AccountMaskUtil();
		for (TVFrontendAccount account : accounts) {
			if (account != null && account.getAccountNumber() != null) {
				accountsMap.put(account.getAccountNumber(),
						account.getNickname() + " " + maskutil.mask(account.getAccountNumber()));
			}
		}
		return accountsMap;
	}

	/**
	 * getCCACardPlastics
	 * @param customerService CustomerServices
	 * @param customerProfile CustomerProfile
	 * @param bankingSession BankingSession
	 * @param account TVFrontendAccount
	 * @return cardPlasticsArrayResponse
	 */
	public static CardPlasticFrontend[] getCCACardPlastics(CustomerServices customerService,
														   CustomerProfile customerProfile, BankingSession bankingSession, TVFrontendAccount account) {
		CardPlasticFrontend[] cardPlasticsArrayResponse = null;

		cardPlasticsArrayResponse = customerService.getPlasticAccountRecords(customerProfile, bankingSession, account);
		if (cardPlasticsArrayResponse != null && cardPlasticsArrayResponse.length > 0) {
			return cardPlasticsArrayResponse;
		}
		return cardPlasticsArrayResponse;
	}

	/**
	 * getCCAPlasticsInMap
	 * @param ccaPlastics CardPlasticFrontend
	 * @return plastics
	 */
	public static Map<Object, Object> getCCAPlasticsInMap(CardPlasticFrontend[] ccaPlastics) {
		Map<Object, Object> plastics = new LinkedHashMap<>();
		AccountMaskUtil maskutil = new AccountMaskUtil();

		for (CardPlasticFrontend plastic : ccaPlastics) {
			if (plastic != null && plastic.getCardPlasticAccountRecord() != null
					&& plastic.getCardPlasticAccountRecord().getCardPANId() != null) {
				String id = plastic.getCardPlasticAccountRecord().getCardPANId();
				plastics.put(id, maskutil.mask(id));
			}
		}
		return plastics;
	}

	/**
	 * getCCACardPlastic
	 * @param plastic String
	 * @param plasticList CardPlasticFrontend
	 * @return cardPlastic
	 */
	public static CardPlasticFrontend getCCACardPlastic(String plastic, CardPlasticFrontend[] plasticList) {
		if (!GenericValidator.isBlankOrNull(plastic)) {
			for (CardPlasticFrontend cardPlastic : plasticList) {
				if (cardPlastic.getCardPlasticAccountRecord().getCardPANId().equalsIgnoreCase(plastic)) {
					return cardPlastic;
				}
			}
		}
		return null;
	}

	/**
	 * getCCAEbillDetails
	 * @param request HttpServletRequest
	 * @param ccaAccount TVFrontendAccount
	 * @param ccaEbills List
	 * @param userSession UserSession
	 * @return accStmtInfo
	 */
	public static AccountStmtInfo getCCAEbillDetails(HttpServletRequest request, TVFrontendAccount ccaAccount,
													 List<AccountStmtInfo> ccaEbills, UserSession userSession) {
		if (ccaAccount != null) {
			if (ccaEbills.isEmpty())
				ccaEbills = userSession.getCcaEbills(request);
			for (Iterator<AccountStmtInfo> iterator = ccaEbills.iterator(); iterator.hasNext();) {
				AccountStmtInfo accStmtInfo = iterator.next();
				if (ccaAccount.getAccountNumber().equals(accStmtInfo.getAccountNumber())) {
					return accStmtInfo;
				}
			}

		}

		return null;
	}

	/**
	 * getPayeeJsonValue
	 * @param bankId String
	 * @param productType String
	 * @param json String
	 * @return payeeObjects
	 */
	public static String getPayeeJsonValue(String bankId, String productType, String json) {
		Gson gson = new Gson();
		PayeeObject[] payeeObjects = gson.fromJson(json, PayeeObject[].class);

		for (PayeeObject payeeObject : payeeObjects) {
			if (!GenericValidator.isBlankOrNull(payeeObject.getBankId()) && payeeObject.getBankId().equals(bankId)) {
				PayeeType[] payeeTypes = payeeObject.getValues();
				for (PayeeType payeeType : payeeTypes) {
					if (!GenericValidator.isBlankOrNull(payeeType.getPayeeType())
							&& !GenericValidator.isBlankOrNull(payeeType.getValue())
							&& payeeType.getPayeeType().equalsIgnoreCase(productType)) {
						return payeeType.getValue();
					}
				}
			}
		}
		return "";
	}

	/**
	 * getUserSessionLite
	 * @param customerProfileService CustomerProfileService
	 * @param profileId String
	 * @return UserSession
	 */
	public static UserSession getUserSessionLite(CustomerProfileService customerProfileService, String profileId) {
		UserSession userSession = new UserSession();
		userSession.setUserSessionType(UserSessionType.CUSTOMER_PROFILE_LITE);
		userSession.setLoginCompleteSuccess(true);
		SingleCustomerProfileLiteResponse customerProfileLite = customerProfileService
				.getCustomerProfileLiteById(profileId);
		CustomerProfile profile = Utils.getCustomerProfileFromLite(customerProfileLite.getCustomerProfileLite());
		userSession.setCustomerProfile(profile);
		return userSession;
	}

	/**
	 * doesUserHasFingerprintRegistered
	 * @param session UserSession
	 * @param delayParameters String
	 * @return FingerprintRegistrationResponse
	 */
	public static FingerprintRegistrationResponse doesUserHasFingerprintRegistered(UserSession session,
																				   String delayParameters) {
		FingerprintRegistrationResponse response = new FingerprintRegistrationResponse();
		List<CustomerDevice> customerDevices = session.getCustomerDevices();

		for (CustomerDevice device : customerDevices) {
			if (device != null && !GenericValidator.isBlankOrNull(device.getDeviceMbopId())
					&& ProductType.FINGERPRINT.equals(device.getProductType())
					&& device.getDeviceMbopId().equalsIgnoreCase(session.getFingerprintId())) {

				if (DeviceStatus.DISABLED.equals(device.getDeviceStatus())) {
					response.setStatus(EnrollmentLiteStatus.FINGERPRINT_DISABLED);
					return response;
				}

				Date registeredDate = device.getDeviceRegDate();
				Date endDate = new Date();

				long duration = endDate.getTime() - registeredDate.getTime();
				long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);
				long delay = Long.parseLong(delayParameters);

				if (diffInHours >= delay) {
					response.setStatus(EnrollmentLiteStatus.FINGERPRINT_VALIDATION_SUCCESS);
					return response;
				} else {
					response.setStatus(EnrollmentLiteStatus.FINGERPRINT_DELAY_FAILED);
					long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
					long delayInMinutes = TimeUnit.HOURS.toMinutes(delay);
					response.setMinutesLeft(delayInMinutes - diffInMinutes);
					return response;
				}
			}
		}
		response.setStatus(EnrollmentLiteStatus.FINGERPRINT_NOT_REGISTERED);
		return response;
	}

	/**
	 * addCustomerDevice
	 * @param request HttpServletRequest
	 * @param device CustomerDevice
	 * @param delayParameter String
	 * @return EnrollmentLiteStatus
	 */
	public static EnrollmentLiteStatus addCustomerDevice(HttpServletRequest request, CustomerDevice device,
														 String delayParameter) {
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		session.setCustomerDevices(device);
		CustomerDeviceResponse response = CustomerFacade.getInstance().addCustomerDevice(session, device);

		if (response != null && ResultStatus.SUCCESS.equals(response.getResponseStatus())) {
			session.setCustomerDevices(device);

			if (ProductType.FINGERPRINT.equals(device.getProductType())) {
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT315)) {
					session.getCustomerService().sendNewFingerprintConfirmation(session.getCustomerProfile(),
							session.getSmsProfile(generateCustomerInteractionEvent(request)), device,
							session.getPreferredLanguage());
				} else {
					session.getCustomerService().sendMobileCashNewFingerprintEmail(session.getCustomerProfile(),
							session.getPreferredLanguage());
				}
				return Utils.doesUserHasFingerprintRegistered(session, delayParameter).getStatus();
			}
		}

		return EnrollmentLiteStatus.DEVICE_REGISTRATION_FAILED;
	}

	/**
	 * updateCustomerDevice
	 * @param request HttpServletRequest
	 * @param device CustomerDevice
	 * @return EnrollmentLiteStatus
	 */
	public static EnrollmentLiteStatus updateCustomerDevice(HttpServletRequest request, CustomerDevice device) {
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerDeviceResponse response = session.getCustomerService().updateCustomerDevice(device);

		if (response != null && ResultStatus.SUCCESS.equals(response.getResponseStatus())) {
			session.updateCustomerDevices(response.getDevice());
			session.setCustomerDevices(response.getDevice());

			if (ProductType.FINGERPRINT.equals(device.getProductType())
					&& device.getDeviceStatus().equals(DeviceStatus.ACTIVE)) {
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBMT315)) {
					session.getCustomerService().sendNewFingerprintConfirmation(session.getCustomerProfile(),
							session.getSmsProfile(generateCustomerInteractionEvent(request)), device,
							session.getPreferredLanguage());
				} else {
					session.getCustomerService().sendMobileCashNewFingerprintEmail(session.getCustomerProfile(),
							session.getPreferredLanguage());
				}
			}
			return EnrollmentLiteStatus.DEVICE_UPDATE_SUCCESS;
		}

		return EnrollmentLiteStatus.DEVICE_UPDATE_FAILED;
	}

	/**
	 * getSessionCustomerDeviceByProductType
	 * @param session UserSession
	 * @param type ProductTypeProductType
	 * @return CustomerDevice
	 */
	public static CustomerDevice getSessionCustomerDeviceByProductType(UserSession session, ProductType type) {
		List<CustomerDevice> customerDevices = session.getCustomerDevices();

		if (customerDevices != null && !customerDevices.isEmpty()) {
			for (CustomerDevice device : customerDevices) {
				if (type.equals(device.getProductType()))
					return device;
			}
		}

		return null;
	}

	/**
	 * getSessionCustomerDeviceByProductTypeAndToken
	 * @param session UserSession
	 * @param type ProductType
	 * @param token String
	 * @return CustomerDevice
	 */
	public static CustomerDevice getSessionCustomerDeviceByProductTypeAndToken(UserSession session, ProductType type,
																			   String token) {
		List<CustomerDevice> customerDevices = session.getCustomerDevices();

		if (GenericValidator.isBlankOrNull(token))
			return null;

		if (customerDevices != null && !customerDevices.isEmpty()) {
			for (CustomerDevice device : customerDevices) {
				if (type.equals(device.getProductType()) && device.getDeviceMbopId().equals(token)) {
					return device;
				}
			}
		}

		return null;
	}

	/**
	 * getRelationshipTypeByAcct
	 * @param userSession UserSession
	 * @param account TVFrontendAccount
	 * @param relationshipType String
	 * @return String
	 */
	public static String getRelationshipTypeByAcct(UserSession userSession, TVFrontendAccount account,
												   String[] relationshipType) {
		StringBuilder relationship = new StringBuilder();
		if (account != null) {
			AbstractAccount accountDetails = userSession.getCustomerService().getAccountDetails(
					userSession.getCustomerProfile(), userSession.getBankingSession(), account, true);

			bppr.tv.beans.accounts.Account accountAux = (bppr.tv.beans.accounts.Account) accountDetails
					.getBackendObject();
			AccountRecord record = accountAux.getRecords();

			if (record != null) {
				Owner[] owners = record.getOwners();
				if (owners != null && owners.length > 0) {
					for (Owner ow : owners) {
						if (ow.getTinInfo().getTaxID().equalsIgnoreCase(userSession.getCustomerProfile().getTaxId())) {
							for (String relationType : relationshipType) {
								String[] relationSplit = relationType.split("-");
								String relation = relationSplit[0];
								String relationName = relationSplit[1];
								if (ow.getRelationshipType().equalsIgnoreCase(relation))
									relationship.append(relationName);
							}
						}
					}
				}
			}
		}
		return relationship.toString();
	}

	/**
	 * isCommercialAccount
	 * @param account TVFrontendAccount
	 * @param productId String
	 * @return true o false
	 */
	public static boolean isCommercialAccount(TVFrontendAccount account, String[] productId) {
		for (int i = 0; i < productId.length; i++) {
			if (account.getProductDefinition().getProductId().equals(productId[i])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * deletePreviousEnrollmentData
	 * @param userSession UserSession
	 * @param phoneNumber String
	 */
	public static void deletePreviousEnrollmentData(UserSession userSession, String phoneNumber) {
		CustomerPhone customerPhoneSearchCriteria = new CustomerPhone();
		customerPhoneSearchCriteria.setPhoneNumber(phoneNumber);

		/**
		 * MBNA-2986
		 * Method getCustomerPhone : CIBP-WEB microservice integration
		 *
		 */
		CustomerPhone customerPhoneResult = CustomerFacade.getInstance().getCustomerPhone(customerPhoneSearchCriteria, userSession);

		if (customerPhoneResult != null) {

			List<MobileCashTransaction> transactions = userSession.getMobileCashService()
					.getAllCustomerTrx(phoneNumber);
			if (transactions != null && !transactions.isEmpty()) {
				for (MobileCashTransaction transaction : transactions) {
					transaction.setStatus(MobileCashTransactionStatus.INACTIVE.toString());
					transaction.setTrxExpDate(new Date());
					userSession.getMobileCashService().saveMobileCashTrx(transaction);
				}
			}

			CustomerDeviceResponse customerDeviceResponse = CustomerFacade.getInstance().getCustomerDeviceByProfileId(customerPhoneResult.getProfileId(), userSession);

			if (customerDeviceResponse != null) {
				List<CustomerDevice> customerDevices = customerDeviceResponse.getAllDevices();
				if (customerDevices != null) {
					for (CustomerDevice custDev : customerDevices) {
						if (custDev.getProductType() == ProductType.CASHDROP) {
							userSession.getCustomerService().deleteCustomerDevice(custDev);
						}
					}
				}
			}

			if (customerPhoneResult.getCustomerProfileType() == CustomerProfileType.LITE
					&& userSession.getUserSessionType() == UserSessionType.CUSTOMER_PROFILE_LITE) {
				Utils.removeCustomerLite(userSession, customerPhoneResult);
			} else if (customerPhoneResult.getCustomerProfileType() == CustomerProfileType.LITE
					&& userSession.getUserSessionType() == UserSessionType.CUSTOMER_PROFILE) {
				Utils.removeCustomerLite(userSession, customerPhoneResult);
			} else if (customerPhoneResult.getCustomerProfileType() == CustomerProfileType.REGULAR
					&& userSession.getUserSessionType() == UserSessionType.CUSTOMER_PROFILE_LITE) {
				userSession.getCustomerService().deleteCustomerPhone(customerPhoneResult);

				SingleCustomerProfileResponse customerProfileResponse = userSession.getCustomerService()
						.getCustomerProfileService().getCustomerProfileById(customerPhoneResult.getProfileId());
				CustomerProfile otherCustomerProfile = customerProfileResponse.getCustomerProfile();

				if (otherCustomerProfile != null) {
					CustomerEntitlement entitlement = otherCustomerProfile
							.getEntitlementByType(EntitlementType.CASH_DROP);
					if (entitlement != null) {
						userSession.getCustomerService().getCustomerProfileService()
								.removeEntitlement(otherCustomerProfile, entitlement);
					}
				}
			} else { // REGULAR & REGULAR
				userSession.getCustomerService().deleteCustomerPhone(customerPhoneResult);

				SingleCustomerProfileResponse customerProfileResponse = userSession.getCustomerService()
						.getCustomerProfileService().getCustomerProfileById(customerPhoneResult.getProfileId());
				CustomerProfile otherCustomerProfile = customerProfileResponse.getCustomerProfile();

				if (otherCustomerProfile != null) {
					CustomerEntitlement entitlement = otherCustomerProfile
							.getEntitlementByType(EntitlementType.CASH_DROP);
					if (entitlement != null) {
						if (userSession.getCustomerProfile().getProfileId().equals(entitlement.getProfileId())) {
							userSession.getCustomerService().getCustomerProfileService()
									.removeEntitlement(userSession.getCustomerProfile(), entitlement);
						} else {
							userSession.getCustomerService().getCustomerProfileService()
									.removeEntitlement(otherCustomerProfile, entitlement);
						}
					}
				}
			}
		}
	}

	/**
	 * removeCustomerLite
	 * @param userSession UserSession
	 * @param customerPhoneResult CustomerPhone
	 */
	private static void removeCustomerLite(UserSession userSession, CustomerPhone customerPhoneResult) {

		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EASY_OFAC)) {
			if (!userSession.isNewOfacCustomer()) {
				CustomerSearch search = new CustomerSearch();
				search.setId(userSession.getCustomerProfile().getOfacId());
				ResponseOfac ofacResp = userSession.getCustomerService().removeCustomerEasyOfac(search);

				if (ofacResp != null && ofacResp.getStatus() != null
						&& ofacResp.getStatus().equals(evertec.easyofac.models.ResponseStatus.SUCCESS)) {
					EasyOfacCustomer ofacCust = new EasyOfacCustomer();
					ofacCust.setEasyOfacId(userSession.getCustomerProfile().getOfacId());
					userSession.getCustomerService().removeEasyOfacCustomerInDB(ofacCust);
				}
			}
		}

		CustomerProfileLite prof = new CustomerProfileLite();
		prof.setProfileId(customerPhoneResult.getProfileId());
		prof.setCustomerPhone(customerPhoneResult);
		userSession.getCustomerService().deleteCustomerLite(prof); // deletes
		// profile
		// and
		// customer
		// phone
	}

	/**
	 * convertEasyOfacStatus
	 * @param status ResponseStatus
	 * @return EasyOfacCustomerStatus
	 */
	public static EasyOfacCustomerStatus convertEasyOfacStatus(evertec.easyofac.models.ResponseStatus status) {
		EasyOfacCustomerStatus ofacStatus = null;
		switch (status) {
			case EXCEPTION:
				ofacStatus = EasyOfacCustomerStatus.EXCEPTION;
				break;
			case SAFE:
				ofacStatus = EasyOfacCustomerStatus.SAFE;
				break;
			case UNSAFE:
				ofacStatus = EasyOfacCustomerStatus.UNSAFE;
				break;
			case COUNTRY_MATCH:
				ofacStatus = EasyOfacCustomerStatus.COUNTRY_MATCH;
				break;
			default:
				ofacStatus = EasyOfacCustomerStatus.ERROR;
				break;
		}

		return ofacStatus;
	}

	/**
	 * getExpirationMinutes
	 * @param expirationMinutes String
	 * @param calendar Calendar
	 * @return Date
	 */
	public static Date getExpirationMinutes(String expirationMinutes, Calendar calendar) {
		try {
			calendar.add(Calendar.MINUTE, Integer.valueOf(expirationMinutes));
			return calendar.getTime();
		} catch (Exception e) {
			logger.error("Cannot add expiration minutes to date. " + e.getMessage());
			return calendar.getTime();
		}
	}

	/**
	 * generateUUID
	 * @return String
	 */
	public static String generateUUID() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	/**
	 * getBusinessPaymentList
	 * @param request HttpServletRequest
	 * @param searchBean EpayrollSearchBean
	 * @param objParam Map
	 * @return Map
	 */
	public static Map<String, Object> getBusinessPaymentList(HttpServletRequest request, EpayrollSearchBean searchBean,
															 Map<String, Object> objParam) {

		String[] businessPayee = (String[]) objParam.get("businessPayee");
		int maxPayments = (int) objParam.get("maxPayments");
		String paymentTypeS = (String) objParam.get("paymentTypeS");
		String maxReverseableDays = (String) objParam.get("maxReverseableDays");

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		Map<String, Object> model = new HashMap<>();
		PaymentHistoryResponse transferResponse = null;
		List<TVPayment> payments = new LinkedList<>();
		List<TVPayment> pending = new LinkedList<>();
		List<TVFrontEndPayee> payees = userSession.getPayees(true);

		List<TVPayment> customerPayments = new LinkedList<>();
		List<TVPayment> customerPendingPayments = new LinkedList<>();

		// Set up today's date
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Calendar n = Calendar.getInstance();
		if (Calendar.SATURDAY == n.get(Calendar.DAY_OF_WEEK))
			n.add(Calendar.DAY_OF_WEEK, 2);
		else if (Calendar.SUNDAY == n.get(Calendar.DAY_OF_WEEK))
			n.add(Calendar.DAY_OF_WEEK, 1);

		model.put("todayDate", df.format(n.getTime()));
		df = new SimpleDateFormat("MM");
		model.put("month", df.format(n.getTime()));
		df = new SimpleDateFormat("dd");
		model.put("day", df.format(n.getTime()));

		int action = searchBean.getAction() == 0 ? 1 : searchBean.getAction();

		int paymentSize = maxPayments;
		int month2 = 0;
		Calendar startDate = Calendar.getInstance();
		String payeeId = "";
		List<Integer> payeeIds = new LinkedList<>();

		int year = Calendar.getInstance().get(Calendar.YEAR);

		if (action == EpayrollSearchBean.SEARCH_BY_DATE && searchBean.getDate().equalsIgnoreCase("LAST_15"))
			action = EpayrollSearchBean.SEARCH_LAST15;
		if (action == EpayrollSearchBean.SEARCH_BY_PAYEE && searchBean.getPayeeId().equalsIgnoreCase("LAST_15"))
			action = EpayrollSearchBean.SEARCH_LAST15;

		PaymentFilterType searchType = PaymentFilterType.HISTORY;
		if (action == EpayrollSearchBean.SEARCH_LAST15) {
			searchType = PaymentFilterType.HISTORY;
			paymentSize = 15;
		} else if (action == EpayrollSearchBean.SEARCH_BY_DATE) {
			searchType = PaymentFilterType.HISTORY;
			String date = searchBean.getDate();
			if (date != null && !"none".equals(date) && !date.equalsIgnoreCase("LAST_15")) {
				year = Integer.parseInt(date.substring(0, 4));
				month2 = Integer.parseInt(date.substring(4, date.length()));
				startDate = new GregorianCalendar(year, month2 - 1, 1);
			} else {
				searchType = PaymentFilterType.HISTORY;
				paymentSize = 15;
				date = null;
			}
		} else if (action == EpayrollSearchBean.SEARCH_BY_PAYEE) {
			searchType = PaymentFilterType.HISTORY;
			payeeId = searchBean.getPayeeId();
			if (payeeId != null && !"none".equals(payeeId)) {
				TVFrontEndPayee p = Utils.getPayeebyId(payeeId, payees);
				if (p != null && p.getCustomerPayeeCodes() != null)
					payeeIds = p.getCustomerPayeeCodes();
				if (payeeIds == null) {
					searchType = PaymentFilterType.HISTORY;
					paymentSize = 15;
				}
			} else {
				searchType = PaymentFilterType.HISTORY;
				paymentSize = 15;
			}
		}

		if (action == EpayrollSearchBean.SEARCH_BY_PAYEE) {
			String[] ids = new String[payeeIds.size()];
			for (int l = 0; l < payeeIds.size(); l++) {
				ids[l] = payeeIds.get(l).toString();
			}
			PaymentHistoryRequest paymentHistoryRequest = new PaymentHistoryRequest(customer, userSession, searchType, paymentSize, ids, null);
			transferResponse = PaymentsFacade.getInstance().getPaymentHistory(paymentHistoryRequest);
			if (transferResponse.getPayments() != null)
				payments.addAll(transferResponse.getPayments());
			if (transferResponse.getInprocessPayments() != null)
				pending.addAll(transferResponse.getInprocessPayments());
		} else if (action == EpayrollSearchBean.SEARCH_BY_DATE) {
			PaymentHistoryRequest paymentHistoryRequest = new PaymentHistoryRequest(customer, userSession, searchType, paymentSize, null, startDate);
			transferResponse = PaymentsFacade.getInstance().getPaymentHistory(paymentHistoryRequest);
			if (transferResponse.getPayments() != null)
				payments.addAll(transferResponse.getPayments());
			if (transferResponse.getInprocessPayments() != null)
				pending.addAll(transferResponse.getInprocessPayments());
		} else {
			PaymentHistoryRequest paymentHistoryRequest = new PaymentHistoryRequest(customer, userSession, searchType, paymentSize, null, null);
			transferResponse = PaymentsFacade.getInstance().getPaymentHistory(paymentHistoryRequest);
			if (transferResponse.getPayments() != null)
				payments.addAll(transferResponse.getPayments());
			if (transferResponse.getInprocessPayments() != null)
				pending.addAll(transferResponse.getInprocessPayments());
		}

		if (payments != null)
			for (Iterator<TVPayment> iterator = payments.iterator(); iterator.hasNext();) {
				TVPayment name = iterator.next();

				// Si tiene un recibo asociado al Reference Number activa el
				// link
				name.setHasVendorReceipt(customerService.hasVendorReceipt(name.getReferenceNumber()));

				TVFrontendAccount s = null;
				if (name.getSource() != null) {
					s = Utils.getAccountSourceByKey(name.getSource().getAccount().getAccountKey(),
							userSession.getAccountsPaymentFrom());
					if (s == null)
						s = Utils.getAccountSourceByNumber(name.getSource().getAccount().getAccountNumber(),
								userSession.getAccountsPaymentFrom());
				} else {
					s = Utils.getAccountByAccountNumber(name.getSourceAccountNumber(), userSession.getAccounts());
				}

				if (s != null)
					name.setSource(new TVAccountTransferSource(s));

				TVFrontEndPayee p = Utils.getPayeebyPayeeId(name.getPayeeId(), payees);
				if (p == null)
					p = Utils.getPayeebyAccountNumberType(name.getPayeeBillingAccount(), payees, name.getPayeeId());

				name.setPayee(p);

				boolean isBusinessPayment = false;
				if (p != null && p.getGlobalPayeeIdString() != null)
					isBusinessPayment = ArrayUtils.contains(businessPayee, p.getGlobalPayeeIdString());

				if (p != null && p.getGlobalPayeeIdString() != null
						&& p.getGlobalPayeeIdString().equalsIgnoreCase(paymentTypeS)
						&& name.getTVPayment().getStatusCode().equalsIgnoreCase("OK")) {
					Calendar effectiveDate = name.getTVPayment().getEffectiveDate();
					Calendar temp = Calendar.getInstance();
					temp.set(Calendar.HOUR, 0);
					temp.set(Calendar.MINUTE, 0);
					temp.set(Calendar.SECOND, 0);
					temp.set(Calendar.MILLISECOND, 0);
					temp.set(Calendar.YEAR, effectiveDate.get(Calendar.YEAR));
					temp.set(Calendar.MONTH, effectiveDate.get(Calendar.MONTH));
					temp.set(Calendar.DAY_OF_MONTH, effectiveDate.get(Calendar.DAY_OF_MONTH));

					Calendar today = Calendar.getInstance();
					today.set(Calendar.HOUR, 0);
					today.set(Calendar.MINUTE, 0);
					today.set(Calendar.SECOND, 0);
					today.set(Calendar.MILLISECOND, 0);

					temp.add(Calendar.DAY_OF_MONTH, Integer.parseInt(maxReverseableDays));

					if (temp.before(today))
						name.setReverseable(false);
					else
						name.setReverseable(true);

				} else if (p != null && p.getGlobalPayeeIdString() != null && paymentTypeS.equalsIgnoreCase("All")
						&& (isBusinessPayment)) {

					Calendar effectiveDate = name.getTVPayment().getEffectiveDate();
					Calendar temp = Calendar.getInstance();
					temp.set(Calendar.HOUR, 0);
					temp.set(Calendar.MINUTE, 0);
					temp.set(Calendar.SECOND, 0);
					temp.set(Calendar.MILLISECOND, 0);
					temp.set(Calendar.YEAR, effectiveDate.get(Calendar.YEAR));
					temp.set(Calendar.MONTH, effectiveDate.get(Calendar.MONTH));
					temp.set(Calendar.DAY_OF_MONTH, effectiveDate.get(Calendar.DAY_OF_MONTH));

					Calendar today = Calendar.getInstance();
					today.set(Calendar.HOUR, 0);
					today.set(Calendar.MINUTE, 0);
					today.set(Calendar.SECOND, 0);
					today.set(Calendar.MILLISECOND, 0);

					temp.add(Calendar.DAY_OF_MONTH, Integer.parseInt(maxReverseableDays));

					if (temp.before(today))
						name.setReverseable(false);
					else
						name.setReverseable(true);

				}

				if (p != null && p.getGlobalPayeeIdString() != null) {
					if (p.getGlobalPayeeIdString().equalsIgnoreCase(paymentTypeS)) {
						customerPayments.add(name);
					} else if (paymentTypeS.equalsIgnoreCase("All") && (isBusinessPayment)) {
						customerPayments.add(name);
					}
				}
			}

		BigDecimal totalInProcess = BigDecimal.valueOf(0.0d);
		if (pending != null) {
			List<PIFExternalAccount> pifExternalAccounts = null;
			for (Iterator<TVPayment> iterator = pending.iterator(); iterator.hasNext();) {
				TVPayment name = iterator.next();

				TVFrontendAccount s = null;
				TVFrontEndPayee p = Utils.getPayeebyPayeeId(name.getPayeeId(), payees);
				// Si tiene un recibo asociado al Reference Number activa el
				// link
				name.setHasVendorReceipt(customerService.hasVendorReceipt(name.getReferenceNumber()));

				if (name.getSource() != null) {
					s = Utils.getAccountSourceByKey(name.getSource().getAccount().getAccountKey(),
							userSession.getAccountsPaymentFrom());
					if (s == null)
						s = Utils.getAccountSourceByNumber(name.getSource().getAccount().getAccountNumber(),
								userSession.getAccountsPaymentFrom());
				} else {
					s = Utils.getAccountByAccountNumber(name.getSourceAccountNumber(), userSession.getAccounts());
					if (s == null) { // It's a PIF Account
						if (pifExternalAccounts == null) {
							pifExternalAccounts = customerService
									.getConfirmedPIFExternalAccounts(userSession.getCustomerProfile().getTaxId());
						}
						if (pifExternalAccounts != null) {
							for (Iterator<PIFExternalAccount> pifIterator = pifExternalAccounts.iterator(); pifIterator
									.hasNext();) {
								PIFExternalAccount pifSource = (PIFExternalAccount) pifIterator.next();
								if (pifSource.getAccountNumber().equals(name.getSourceAccountNumber())) {
									BankInformation bankInformation = new BankInformation();
									bankInformation.setBankId(pifSource.getRoutingNumber());
									bankInformation.setBankType("ABA");
									bankInformation.setBankName(pifSource.getBankName());
									CustomerAccountPreferences pref = new CustomerAccountPreferences();
									TVAccountFactory accountFactory = new TVAccountFactory();
									Account sourceAccount = accountFactory.createAccountFromAccountCode("IDA",
											pifSource.getAccountNumber());
									sourceAccount.setAccountID(pifSource.getAccountNumber());
									sourceAccount.setSubType("IDA");
									((DepositAccount) sourceAccount)
											.setSectionIndicator(Character.toString(pifSource.getAccountType()));
									TVFrontendAccount fromAccount = new TVFrontendAccount(sourceAccount);
									fromAccount.setCustomerAccountPreferences(pref);
									fromAccount.setNickname(pifSource.getNickname());
									fromAccount.setHide(pifSource.getShow() == '0');
									fromAccount.getTVAccount().setBankInformation(bankInformation);
									s = fromAccount;
									break;
								}
							}
						}
					}
				}

				if (s != null)
					name.setSource(new TVAccountTransferSource(s));

				p = Utils.getPayeebyAccountNumberType(name.getPayeeBillingAccount(), payees, name.getPayeeId());
				name.setPayee(p);

				if (name.getTVPayment().getRemainingInstances() <= 0) {
					name.getTVPayment().setRemainingInstances(999);
				}
				Calendar cal = Calendar.getInstance();
				cal.setTime(name.getEffectiveDate());

				if (p != null && p.getGlobalPayee() != null && p.getGlobalPayee().getPaymentType() != null)
					if (p.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO
							|| p.getGlobalPayee().getPaymentType() == GlobalPayeePaymentType.CHEQUE_Y_LISTA_PUBLICO)
						name.setEstimateDate("CHECK");
					else
						name.setEstimateDate(Utils.getEstimateDate(customerService.getHolidays(), cal,
								p.getGlobalPayee().getPaymentType()));
				else
					name.setEstimateDate(
							Utils.getEstimateDate(customerService.getHolidays(), cal, GlobalPayeePaymentType.INTERNO));

				if (p != null) {
					if (p.getTelepagoShortName().equals("DIRPM/BPPR") || p.getTelepagoShortName().equals("DIRPM/LEASE")
							|| p.getTelepagoShortName().equals("DIRPM/AUTO"))
						name.setIsdirpm(true);
					else
						name.setIsdirpm(false);
				}
				boolean isBusinessPayment = false;

				if (p != null && p.getGlobalPayeeIdString() != null)
					isBusinessPayment = ArrayUtils.contains(businessPayee, p.getGlobalPayeeIdString());

				if (p != null && p.getGlobalPayeeIdString() != null
						&& p.getGlobalPayeeIdString().equalsIgnoreCase(paymentTypeS)) {
					customerPendingPayments.add(name);

					if (!name.getFrequency().equalsIgnoreCase("mb") && !name.getFrequency().equalsIgnoreCase("tb")) {
						totalInProcess = totalInProcess.add(name.getAmount());
					}
				} else if (paymentTypeS.equalsIgnoreCase("All") && (isBusinessPayment)) {
					customerPendingPayments.add(name);

					if (!name.getFrequency().equalsIgnoreCase("mb") && !name.getFrequency().equalsIgnoreCase("tb")) {
						totalInProcess = totalInProcess.add(name.getAmount());
					}
				}

			}
		}

		model.put("totalInProcess", totalInProcess);
		Collections.sort(customerPayments, new PaymentSort());
		model.put("processedPayments", customerPayments);
		Collections.sort(customerPendingPayments, new PaymentSort());
		model.put("pendingPayments", customerPendingPayments);
		userSession.setPendingPayments(customerPendingPayments);
		userSession.setCustomerPayments(customerPayments);

		return model;
	}

	/**
	 * oobCommercialChallenge
	 * @param request HttpServletRequest
	 * @param command Object
	 * @param errors Errors
	 * @param page int
	 * @return Map
	 */
	public static Map<String, Object> oobCommercialChallenge(HttpServletRequest request, Object command, Errors errors,
															 int page) {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		EvendorBean bean = (EvendorBean) command;
		EvendorBean.ACTION action = bean.getAction();
		Map<String, Object> model = new HashMap<>();

		CustomerProfile profile = userSession.getCustomerProfile();
		CustomerEntitlement rsaEnt = profile.getEntitlementByType(EntitlementType.RSA);

		String phone = null;
		String provider = null;
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);

		/**
		 * MBNA-1985
		 * Method getRsaChallengeMethod : CIBP-WEB microservice integration
		 *
		 */
		//Microservice implementation
		/**
		 * MBNA-1985
		 * Method getRsaChallengeMethod : CIBP-WEB microservice integration
		 *
		 */
		//Microservice implementation
		RsaChallengeRequest rsaChallengeRequest = new RsaChallengeRequest(userSession.getUsername(),deviceRequest);

		CredentialType credentialType = userSession.getCustomerService().rsaGetChallengeMethod(rsaChallengeRequest);
		userSession.setDeviceRequest(deviceRequest);

		if (credentialType == null) {
			return model;
		}

		if (credentialType.equals(CredentialType.OOBPHONE))
			model.put("challengeType", CustomerOOBChallengeType.OOBPHONE.toString());
		else
			model.put("challengeType", CustomerOOBChallengeType.OOBSMS.toString());

		if (action == EvendorBean.ACTION.SEND_SMSCODE || action == EvendorBean.ACTION.VALIDATE_SMSCODE
				|| action == EvendorBean.ACTION.VALIDATE_CALLCODE) {
			// Retrieve primary phone number (MB Text)
			model.put("from", "phone");
			if (profile.getEntitlementByType(EntitlementType.MOBILE_BANKING) != null) {
				SMSProfile smsprofile = userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request));
				if (smsprofile != null && smsprofile.getSmsProfileId() != null && smsprofile.getCtns() != null
						&& !smsprofile.getCtns().isEmpty()) {
					phone = ((SMSCTN) smsprofile.getCtns().iterator().next()).getCtn();
					provider = ((SMSCTN) smsprofile.getCtns().iterator().next()).getProvider();
				}
			}
			String altPhone = rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE);
			if (altPhone != null && !altPhone.equalsIgnoreCase("N")) {
				model.put("hasAltPhone", true);
				model.put("altphone", RSAUtils.setPhoneMask(altPhone));
			} else
				model.put("hasAltPhone", false);
		} else if (action == EvendorBean.ACTION.ALT_PHONE || action == EvendorBean.ACTION.VALIDATE_ALTSMSCODE
				|| action == EvendorBean.ACTION.VALIDATE_ALTCALLCODE) {
			// Retrieve alternate phone number
			if (rsaEnt != null) {
				phone = rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE);
				provider = rsaEnt.getString(RSAEntitlement.ALTERNATEPROVIDER);
				model.put("from", "altphone");
			}
		}

		if (phone == null) {

			if (action == EvendorBean.ACTION.VALIDATE_RECCODE) {
				model.put("from", "reccode");
			} else {
				if (credentialType.equals(CredentialType.OOBPHONE))
					errors.rejectValue("code", "login.oob.call.unable");
				else
					errors.rejectValue("code", "login.oob.codeError.message");

				return model;
			}

		} else {
			model.put("phone", RSAUtils.setPhoneMask(phone));
		}

		// SMS challenge
		if (credentialType.equals(CredentialType.USER_DEFINED) && action != EvendorBean.ACTION.VALIDATE_SMSCODE
				&& action != EvendorBean.ACTION.VALIDATE_ALTSMSCODE) {

			if (action == EvendorBean.ACTION.SEND_SMSCODE || action == EvendorBean.ACTION.ALT_PHONE) {
				if (generateCommercialCode(request, phone, provider, errors))
					model.put("codeSent", true);
				else
					model.put("codeSent", false);

			} else {
				String code = "";
				if (page == 0)
					code = (String) WebUtils.getSessionAttribute(request, "oobCode");
				if (GenericValidator.isBlankOrNull(code)) {
					generateCommercialCode(request, phone, provider, errors);
				}
			}

			// Voice call challenge
		} else {
			if (action != EvendorBean.ACTION.VALIDATE_SMSCODE && action != EvendorBean.ACTION.VALIDATE_ALTSMSCODE
					&& action != EvendorBean.ACTION.VALIDATE_CALLCODE
					&& action != EvendorBean.ACTION.VALIDATE_ALTCALLCODE) {
				if (action == EvendorBean.ACTION.CALL_PHONE) {
					if (makeCommercialCall(request, phone, errors))
						model.put("callMade", true);
					else
						model.put("callMade", false);
				} else {
					makeCommercialCall(request, phone, errors);
				}
				bean.setCode((String) WebUtils.getSessionAttribute(request, "oobCode"));
			}
		}

		model.put("redirectToBlock", userSession.isOobRedirectToBlock());
		model.put("redirectToLogin", userSession.isOobRedirectToLogin());
		return model;
	}

	/**
	 * generateCommercialCode
	 * @param request HttpServletRequest
	 * @param phone String
	 * @param provider String
	 * @param errors Errors
	 * @return true o false
	 */
	public static boolean generateCommercialCode(HttpServletRequest request, String phone, String provider,
												 Errors errors) {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerProfile customer = userSession.getCustomerProfile();
		CustomerServices customerService = userSession.getCustomerService();
		String user = userSession.getUsername();

		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);

		ChallengeOobRequest challengeOobRequest = new ChallengeOobRequest();
		challengeOobRequest.setCredentialType(CredentialType.USER_DEFINED);
		challengeOobRequest.setChallengephone(phone);
		challengeOobRequest.setDeviceRequest(deviceRequest);
		challengeOobRequest.setUsername(user);
		challengeOobRequest.setNewChallenge(true);

		try {

			/**
			 * MBNA-1973
			 * Method challengeOob : CIBP-WEB microservice integration
			 */
			ChallengeOobResponse challengeOobResponse = customerService.getSignOnCustomerServiceModule().challengeOob(challengeOobRequest);

			deviceRequest.setDeviceTokenCookie(challengeOobResponse.getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(challengeOobResponse.getDeviceTokenCookie());
			userSession.setDeviceRequest(deviceRequest);
			userSession.setRsaSessionId(challengeOobResponse.getRsaSessionId());
			userSession.setRsaTransactionId(challengeOobResponse.getRsaTransactionId());

			if (ResultStatus.RSA_UNABLE.equals(challengeOobResponse.getResultStatus())) {
				errors.rejectValue("code", "sms.code.error", "Unable to send code at this time.");
				WebUtils.setSessionAttribute(request, "oobCode", null);
				return false;

			} else if (ResultStatus.RSA_BLOCKED.equals(challengeOobResponse.getResultStatus())) {
				userSession.setOobRedirectToBlock(true);
				WebUtils.setSessionAttribute(request, "oobCode", null);
				logger.error("RSA User " + userSession.getUsername() + " blocked by OOB SMS challenge");
				return false;
			}

			String code = challengeOobResponse.getOtp();
			SendSmsGeneratedCodeRequest codereq = new SendSmsGeneratedCodeRequest(
					Utils.generateCustomerInteractionEvent(request), customer, code, phone, provider);

			if (Utils.getRSALanguage(request).equalsIgnoreCase("en"))
				customerService.sendGeneratedCode(codereq, PreferredLanguage.ENGLISH);
			else
				customerService.sendGeneratedCode(codereq, PreferredLanguage.SPANISH);

			WebUtils.setSessionAttribute(request, "oobCode", code);

			return true;

		} catch (Exception e) {
			errors.rejectValue("code", "sms.code.error", "Unable to send code at this time.");
			WebUtils.setSessionAttribute(request, "oobCode", null);
			return false;
		}
	}

	/**
	 * makeCommercialCall
	 * @param request HttpServletRequest
	 * @param phone String
	 * @param errors Errors
	 * @return true o false
	 */
	public static boolean makeCommercialCall(HttpServletRequest request, String phone, Errors errors) {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String user = userSession.getUsername();
		DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(userSession.getPm_fp(),
				userSession.getDeviceRequest().getDeviceTokenCookie(), request);

		ChallengeOobRequest challengeOobRequest = new ChallengeOobRequest();
		challengeOobRequest.setCredentialType(CredentialType.OOBPHONE);
		challengeOobRequest.setChallengephone(phone);
		challengeOobRequest.setDeviceRequest(deviceRequest);
		challengeOobRequest.setUsername(user);
		challengeOobRequest.setLanguage("EN".equalsIgnoreCase(Utils.getRSALanguage(request)) ? Lang.en : Lang.es);
		challengeOobRequest.setNewChallenge(true);

		try {

			/**
			 * MBNA-1973
			 * Method challengeOob : CIBP-WEB microservice integration
			 */
			ChallengeOobResponse challengeOobResponse = userSession.getCustomerService().getSignOnCustomerServiceModule().challengeOob(challengeOobRequest);

			deviceRequest.setDeviceTokenCookie(challengeOobResponse.getDeviceTokenCookie());
			deviceRequest.setDeviceTokenFSO(challengeOobResponse.getDeviceTokenCookie());
			userSession.setDeviceRequest(deviceRequest);
			userSession.setRsaSessionId(challengeOobResponse.getRsaSessionId());
			userSession.setRsaTransactionId(challengeOobResponse.getRsaTransactionId());

			if (ResultStatus.RSA_UNABLE.equals(challengeOobResponse.getResultStatus())) {
				userSession.setOobRedirectToLogin(true);
				errors.rejectValue("code", "login.oob.call.unable", "Unable to generate call at this time.");
				WebUtils.setSessionAttribute(request, "oobCode", null);
				return false;
			} else if (ResultStatus.RSA_BLOCKED.equals(challengeOobResponse.getResultStatus())) {
				userSession.setOobRedirectToBlock(true);
				WebUtils.setSessionAttribute(request, "oobCode", null);
				logger.error("RSA User " + userSession.getUsername() + " blocked by OOB call challenge");
				return false;
			}

			WebUtils.setSessionAttribute(request, "oobCode", challengeOobResponse.getToken());
			return true;

		} catch (Exception e) {
			errors.rejectValue("code", "login.oob.call.unable", "Unable to generate call at this time.");
			WebUtils.setSessionAttribute(request, "oobCode", null);
			return false;
		}
	}

	/**
	 * getEmailFrequency
	 * @param remainingInstances int
	 * @param vFrequency String
	 * @param messageSource MessageSource
	 * @param request HttpServletRequest
	 * @return String
	 */
	public static String getEmailFrequency(int remainingInstances, String vFrequency, MessageSource messageSource,
										   HttpServletRequest request) {
		String frequency = "";
		if (remainingInstances != 999) {

			frequency = messageSource.getMessage("instances.for.label", null, new Locale(Utils.getRSALanguage(request)))
					+ " " + remainingInstances;

			if ("WW".equals(vFrequency) || "Weekly".equals(vFrequency)) {
				frequency = frequency + " " + messageSource.getMessage("freq.instances.WW", null,
						new Locale(Utils.getRSALanguage(request)));
			} else if ("BW".equals(vFrequency) || "Biweekly".equals(vFrequency)) {
				frequency = frequency + " " + messageSource.getMessage("freq.instances.BW", null,
						new Locale(Utils.getRSALanguage(request)));
			} else if ("MM".equals(vFrequency) || "Monthly".equals(vFrequency)) {
				frequency = frequency + " " + messageSource.getMessage("freq.instances.MM", null,
						new Locale(Utils.getRSALanguage(request)));
			} else if ("QQ".equals(vFrequency) || "Quaterly".equals(vFrequency)) {
				frequency = frequency + " "
						+ messageSource.getMessage("freq.instances.T", null, new Locale(Utils.getRSALanguage(request)));
			} else if ("SA".equals(vFrequency) || "Semiannually".equals(vFrequency)) {
				frequency = frequency + " "
						+ messageSource.getMessage("freq.instances.S", null, new Locale(Utils.getRSALanguage(request)));
			} else if ("AA".equals(vFrequency) || "Annually".equals(vFrequency)) {
				frequency = frequency + " "
						+ messageSource.getMessage("freq.instances.A", null, new Locale(Utils.getRSALanguage(request)));
			}
		} else {

			if ("WW".equals(vFrequency) || "Quaterly".equals(vFrequency)) {
				frequency = frequency + " "
						+ messageSource.getMessage("freq.WW", null, new Locale(Utils.getRSALanguage(request)));
			} else if ("BW".equals(vFrequency) || "Biweekly".equals(vFrequency)) {
				frequency = frequency + " "
						+ messageSource.getMessage("freq.BW", null, new Locale(Utils.getRSALanguage(request)));
			} else if ("MM".equals(vFrequency) || "Monthly".equals(vFrequency)) {
				frequency = frequency + " "
						+ messageSource.getMessage("freq.MM", null, new Locale(Utils.getRSALanguage(request)));
			} else if ("QQ".equals(vFrequency) || "Quaterly".equals(vFrequency)) {
				frequency = frequency + " "
						+ messageSource.getMessage("freq.QQ", null, new Locale(Utils.getRSALanguage(request)));
			} else if ("SA".equals(vFrequency) || "Semiannually".equals(vFrequency)) {
				frequency = frequency + " "
						+ messageSource.getMessage("freq.SA", null, new Locale(Utils.getRSALanguage(request)));
			} else if ("AA".equals(vFrequency) || "Annually".equals(vFrequency)) {
				frequency = frequency + " "
						+ messageSource.getMessage("freq.AA", null, new Locale(Utils.getRSALanguage(request)));
			}
		}

		return frequency;
	}

	/**
	 * getPlasticType
	 * @param panId String
	 * @param productId String
	 * @return String
	 */
	public static String getPlasticType(String panId, String productId) {
		if (panId.startsWith("454951")) {
			return "CLASSIC";
		} else if (panId.startsWith("454952")) {
			return "GOLD";
		} else if (panId.startsWith("454953")) {
			if ("029".equals(productId))
				return "ONE";
			return "SECURITIES";
		} else if (panId.startsWith("021502")) {
			if ("060".equals(productId))
				return "POP";
			return "REGULAR";
		} else {
			return "DEFAULT";
		}
	}

	/**
	 * toAccountStmtInfo
	 * @param ccaStatements List
	 * @param userSession UserSession
	 * @return List AccountStmtInfo
	 */
	public static List<AccountStmtInfo> toAccountStmtInfo(List<CreditCardStatement_Type> ccaStatements,
														  UserSession userSession) {
		List<AccountStmtInfo> vaultEbillsHistory = new ArrayList<>();
		if (ccaStatements != null && !ccaStatements.isEmpty()) {
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			TVAccountFactory tvfactory = new TVAccountFactory();
			DisposableIdentifier id = new DisposableIdentifier();
			for (Iterator<CreditCardStatement_Type> iterator = ccaStatements.iterator(); iterator.hasNext();) {
				CreditCardStatement_Type ccs = iterator.next();
				AccountPDFStmtInfo pdfstm = new AccountPDFStmtInfo();
				List<TVFrontendAccount> accounts = new ArrayList<>();
				accounts.addAll(userSession.getAccounts());
				accounts.addAll(userSession.getClosedAccounts());
				TVFrontendAccount acc = Utils.getAccountByAccountNumber(ccs.getAccountId(), accounts);
				if (ccs.getProductDesc() == null) {
					ccs.setProductDesc("- - -");
				}
				if (acc == null) {
					try {
						Account ddaBack = tvfactory.createCardAccount(ccs.getAccountId());
						acc = new TVFrontendAccount(ddaBack);
						acc.setFrontEndId(id.getNextDisposableId());
						acc.setCustomerAccountPreferences(new CustomerAccountPreferences());
						acc.setNickname(ccs.getProductDesc());
						acc.setAvailableElecStmt(true);
						userSession.getClosedAccounts().add(acc);
					} catch (Exception e) {
						logger.error(e.getMessage());
						continue;
					}
				}
				pdfstm.setAccount_type("PDFSTMT");
				pdfstm.setAccountNumber(ccs.getAccountId());
				pdfstm.setAccount_name(acc.getNickname());
				pdfstm.setAccount_nickname(acc.getNickname());
				pdfstm.setInvoice_date(format.format(ccs.getStatementStarDt().getTime()));
				pdfstm.setAccount_type(CCA);
				pdfstm.setBankId(ccs.getBankId());
				pdfstm.setDisplay(true);
				pdfstm.setDueDate(ccs.getStmtDueDt());
				pdfstm.setProductDescription(acc.getDescription());
				pdfstm.setDocId(ccs.getPdfId());
				pdfstm.setId(ccs.getPdfId());
				pdfstm.setAccountFrontendID(acc.getFrontEndId());

				AccountStmtInfo accStmtInfo = new AccountStmtInfo();
				List<AccountPDFStmtInfo> accountStatements = new ArrayList<>();
				accStmtInfo.setAccountLabel(acc.getNickname());
				accStmtInfo.setAccountNumber(ccs.getAccountId());
				accStmtInfo.setActive(true);
				accStmtInfo.setLast4Digits(new AccountMaskUtil().mask(ccs.getAccountId()));
				accStmtInfo.setLastStatementDate("");
				if (ccs.getStatementCycleDt() != null) {
					pdfstm.setInvoice_date(format.format(ccs.getStatementCycleDt().getTime()));
					accStmtInfo.setLastPaymentDate(format.format(ccs.getStatementCycleDt().getTime()));
					accStmtInfo.setLastStatementDate(format.format(ccs.getStatementCycleDt().getTime()));
				}
				if (ccs.getStmtMinPmtDueAmt() == null)
					ccs.setStmtMinPmtDueAmt(new BigDecimal(0));
				accStmtInfo.setMinimumAmountDue(ccs.getStmtMinPmtDueAmt());

				CardAccount cuenta = (CardAccount) acc.getBackendObject();
				if (cuenta == null) {
					try {
						AbstractAccount response = userSession.getCustomerService().getAccountDetails(
								userSession.getCustomerProfile(), userSession.getBankingSession(), acc, true);
						cuenta = (CardAccount) response.getBackendObject();
						TVFrontendCreditAccount cca = (TVFrontendCreditAccount) response;
						acc.setNickname(cca.getDescription());
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
				}
				if (cuenta != null) {
					CreditCardAccountRecord ccaRecord = (CreditCardAccountRecord) cuenta.getRecords();
					if (ccaRecord.getLastPaymentDate() == null || ccaRecord.getLastPaymentAmount() == null){
						try {
							AbstractAccount response = userSession.getCustomerService().getAccountDetails(
									userSession.getCustomerProfile(), userSession.getBankingSession(), acc, true);
							cuenta = (CardAccount) response.getBackendObject();
							ccaRecord = (CreditCardAccountRecord) cuenta.getRecords();
						} catch (Exception e) {
							logger.error(e.getMessage());
						}
					}
					if (ccaRecord.getLastPaymentDate() != null && ccaRecord.getLastPaymentAmount() != null) {
						accStmtInfo.setLastPaymentDate(format.format(ccaRecord.getLastPaymentDate().getTime()));
						accStmtInfo.setLastPaymentAmount(ccaRecord.getLastPaymentAmount());
					}
				}
				accStmtInfo.setAmountDue(ccs.getStmtEndingBal());
				if (ccs.getStmtDueDt() != null) {
					accStmtInfo.setDueDate(format.format(ccs.getStmtDueDt().getTime()));
				}
				accStmtInfo.setAccountStatements(accountStatements);
				accStmtInfo.setBillerAccountID(acc.hashCode());
				accStmtInfo.setAccountLabel(acc.getNickname());
				accStmtInfo.setAccountNumber(acc.getAccountNumber());
				accountStatements.add(pdfstm);
				vaultEbillsHistory.add(accStmtInfo);
			}
		}
		return vaultEbillsHistory;
	}

	/**
	 * toAccountPDFStmtInfo
	 * @param ddahistory List
	 * @param userSession UserSession
	 * @return List
	 */
	public static List<AccountPDFStmtInfo> toAccountPDFStmtInfo(List<DepositStatement_Type> ddahistory,
																UserSession userSession) {
		List<AccountPDFStmtInfo>  vaultStatement = new ArrayList<>();
		if (ddahistory != null && !ddahistory.isEmpty() ) {
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			TVAccountFactory tvfactory = new TVAccountFactory();
			DisposableIdentifier id = new DisposableIdentifier();
			for (Iterator<DepositStatement_Type> iterator = ddahistory.iterator(); iterator.hasNext();) {
				DepositStatement_Type ddaStmt = iterator.next();
				AccountPDFStmtInfo pdfstm = new AccountPDFStmtInfo();
				List<TVFrontendAccount> accounts = new ArrayList<>();
				accounts.addAll(userSession.getAccounts());
				accounts.addAll(userSession.getClosedAccounts());
				TVFrontendAccount acc = Utils.getAccountByAccountNumber(ddaStmt.getAccountId(),
						accounts);
				if(ddaStmt.getProductDesc() == null) {
					ddaStmt.setProductDesc("- - -");
				}
				if(acc == null) {
					try {
						DepositAccount ddaBack = tvfactory.createDepositAccount(ddaStmt.getAccountId());
						ddaBack.setSubType(AccountSubtypes.DEPOSIT);
						ddaBack.setSectionIndicator(ddaStmt.getSectionInd());
						acc = new TVFrontendAccount(ddaBack);
						acc.setFrontEndId(id.getNextDisposableId());
						acc.setCustomerAccountPreferences(new CustomerAccountPreferences());
						acc.setNickname(ddaStmt.getProductDesc());
						acc.setAvailableElecStmt(true);

						AbstractAccount response = userSession.getCustomerService().getAccountDetails(userSession.getCustomerProfile(), userSession.getBankingSession(), acc, true);
						DepositAccount cuenta = (DepositAccount) response.getBackendObject();
						Owner[] owners = cuenta.getOwners();
						Owner ow = null;//3895801
						for (int i = 0; i < owners.length; i++) {
							if(owners[i].getPermId() != null
									&& owners[i].getPermId().equals(userSession.getBankingSession().getPermId().toString()) ) {
								ow = owners[i];
								break;
							}
						}
						String owner = "";
						if(ow != null) {
							owner = ow.getRelationshipType();
							if((owner != null && owner.equalsIgnoreCase("067") ) ||
									(!(ow.getOwnershipType().equals("Primary") &&
											(ddaStmt.getProductDesc().toUpperCase().equals("MUNICIPALITIES") ||
													ddaStmt.getProductDesc().toUpperCase().equals("AUTHORITIES"))))){
								continue;
							}
						}else {
							continue;
						}

						userSession.getClosedAccounts().add(acc);
					} catch (Exception e) {
						logger.error(e);
						continue;
					}
				}
				pdfstm.setAccount_type("PDFSTMT");
				pdfstm.setAccountNumber(ddaStmt.getAccountId());
				pdfstm.setAccount_name(acc.getNickname());
				pdfstm.setAccount_nickname(acc.getNickname());
				pdfstm.setInvoice_date(format.format(ddaStmt.getStatementStarDt().getTime()));
				pdfstm.setAccount_type(IDA);
				pdfstm.setBankId(ddaStmt.getBankId());
				pdfstm.setDisplay(true);
				pdfstm.setDueDate(ddaStmt.getStmtDueDt());
				pdfstm.setDocId(ddaStmt.getPdfId());
				pdfstm.setId(String.valueOf(acc.hashCode()));
				pdfstm.setAccountFrontendID(acc.getFrontEndId());

				if (ddaStmt.getStatementCycleDt() != null) {
					pdfstm.setInvoice_date(format.format(ddaStmt.getStatementCycleDt().getTime()));
				}
				vaultStatement.add(pdfstm);// Add Statements
			}
		}
		return vaultStatement;
	}

	/**
	 * getCustomerAppbyType
	 * @param customerApplication List
	 * @param appType CustomerApplicationType
	 * @param appStatus ApplicationStatus
	 * @return listResult
	 */
	public static List<CustomerApplication> getCustomerAppbyType(List<CustomerApplication> customerApplication,
																 CustomerApplicationType appType, ApplicationStatus appStatus) {
		List<CustomerApplication> listResult = new ArrayList<>();
		if (customerApplication != null) {

			for (Iterator<CustomerApplication> iteraApps = customerApplication.iterator(); iteraApps.hasNext();) {
				CustomerApplication listCustApp = iteraApps.next();

				if (appStatus.equals(ApplicationStatus.All)) {
					if (appType.equals(listCustApp.getApplicationType()) && listCustApp != null)
						listResult.add(listCustApp);
				} else {
					if (appType.equals(listCustApp.getApplicationType()) && listCustApp != null
							&& appStatus.equals(listCustApp.getApplicationStatus()))
						listResult.add(listCustApp);
				}

			}
		}
		return listResult;
	}

	/**
	 * getCustomerAppbyType
	 * @param customerApplication List
	 * @param appType CustomerApplicationType
	 * @param appStatus ApplicationStatus
	 * @return List CustomerApplication
	 */
	public static List<CustomerApplication> getCustomerAppbyType(List<CustomerApplication> customerApplication,
																 CustomerApplicationType appType, ApplicationStatus... appStatus) {

		List<ApplicationStatus> applicationStatuses = Arrays.asList(appStatus);

		return customerApplication.stream()
				.filter(customerApplication1 -> customerApplication1.getApplicationType().equals(appType))
				.filter(customerApplication1 ->applicationStatuses.stream()
						.anyMatch(applicationStatus -> customerApplication1.getApplicationStatus()
								.equals(applicationStatus))).collect(Collectors.toList());



	}

	/**
	 * getCustomerAppStatus
	 * @param customerApplication List
	 * @return status
	 */
	public static Map<String, ApplicationStatus> getCustomerAppStatus(List<CustomerApplication> customerApplication) {
		Map<String, ApplicationStatus> status = new HashMap<>();
		List<CustomerApplication> listEPayroll = new ArrayList<>();
		List<CustomerApplication> listFraud = new ArrayList<>();
		boolean hasFraudAppActive = false;
		Collections.sort(customerApplication);
		if (customerApplication != null) {
			for (Iterator<CustomerApplication> iteraApps = customerApplication.iterator(); iteraApps.hasNext();) {
				CustomerApplication listCustApp = iteraApps.next();
				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR)) {
					if (listCustApp.getApplicationType().equals(CustomerApplicationType.BUSINESSPAYMENTS_MODIFY))
						listEPayroll.add(listCustApp);
				} else {
					if (listCustApp.getApplicationType().equals(CustomerApplicationType.EPAYROLL_MODIFY))
						listEPayroll.add(listCustApp);
				}
				if (listCustApp.getApplicationType().equals(CustomerApplicationType.VIEW_HISTORY_CHECKS_MODIFY)) {
					listFraud.add(listCustApp);
				}
			}

			if (!listEPayroll.isEmpty()) {
				CustomerApplication lastEpayApp = listEPayroll.get(listEPayroll.size() - 1);
				status.put("epayrollAppStatus", lastEpayApp.getApplicationStatus());
			} else {
				status.put("epayrollAppStatus", ApplicationStatus.EMPTY);
			}

			if (!listFraud.isEmpty()) {
				for (Iterator<CustomerApplication> iteraFraudApps = listFraud.iterator(); iteraFraudApps.hasNext();) {
					CustomerApplication listFraudApp = iteraFraudApps.next();
					if (listFraudApp.getApplicationStatus().equals(ApplicationStatus.APPROVED) || listFraudApp.getApplicationStatus().equals(ApplicationStatus.NEW))
						hasFraudAppActive = true;
				}

				if (hasFraudAppActive)
					status.put("fraudStatus", ApplicationStatus.APPROVED);
				else {
					CustomerApplication lastFraudApp = listFraud.get(listFraud.size() - 1);
					status.put("fraudStatus", lastFraudApp.getApplicationStatus());
				}
			} else {
				status.put("fraudStatus", ApplicationStatus.EMPTY);
			}

		}
		return status;
	}

	/**
	 * isValidDateFormat
	 * @param format String
	 * @param value String
	 * @return true o false
	 */
	public static boolean isValidDateFormat(String format, String value) {
		Date date = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			date = sdf.parse(value);
			if (!value.equals(sdf.format(date))) {
				date = null;
			}
		} catch (ParseException ex) {
			return false;
		}
		return date != null;
	}

	/**
	 * findEmailAlert
	 * @param emailsAlertsAccount List
	 * @param account String
	 * @param accountSection String
	 * @param productType String
	 * @param productId String
	 * @param pos String
	 * @return String
	 */
	public static String findEmailAlert(List<CustomerEmailBalanceAlert> emailsAlertsAccount, String account,
										String accountSection, String productType, String productId, String pos) {
		if (emailsAlertsAccount != null && !emailsAlertsAccount.isEmpty()) {
			for (Iterator<CustomerEmailBalanceAlert> iterator = emailsAlertsAccount.iterator(); iterator.hasNext();) {
				CustomerEmailBalanceAlert account2 =  iterator.next();

				if (account2.getAlertCondition().getOperator().equalsIgnoreCase(pos)
						&& account2.getAccountNumber().equals(account)
						&& account2.getAccountSection().equalsIgnoreCase(accountSection)
						&& account2.getAccountSubtype().equalsIgnoreCase(productType)
						&& account2.getAccountProductId().equalsIgnoreCase(productId))
					return account2.getId();
			}
		}
		return "";
	}

	/**
	 * findBalanceAlert
	 * @param balanceAlertsAccount List
	 * @param account String
	 * @param accountSection String
	 * @param pos String
	 * @return CustomerBalanceAlert
	 */
	public static CustomerBalanceAlert findBalanceAlert(List<CustomerBalanceAlert> balanceAlertsAccount, String account,
														String accountSection, String pos) {
		if (balanceAlertsAccount != null && !balanceAlertsAccount.isEmpty()) {
			for (Iterator<CustomerBalanceAlert> iterator = balanceAlertsAccount.iterator(); iterator.hasNext();) {
				CustomerBalanceAlert account2 = (CustomerBalanceAlert) iterator.next();

				if (account2.getAlertCondition().getOperator().equalsIgnoreCase(pos)
						&& account2.getAccountNumber().equals(account)
						&& account2.getAccountSection().equalsIgnoreCase(accountSection))
					return account2;
			}
		}
		return null;
	}

	/**
	 * findSmsAlert
	 * @param smsAlertsAccount smsAlertsAccount
	 * @param account String
	 * @param accountSection String
	 * @param productType String
	 * @param productId String
	 * @param pos String
	 * @return String
	 */
	public static String findSmsAlert(List<SMSAccount> smsAlertsAccount, String account, String accountSection,
									  String productType, String productId, String pos) {
		if (smsAlertsAccount != null && !smsAlertsAccount.isEmpty()) {
			for (Iterator<SMSAccount> iterator = smsAlertsAccount.iterator(); iterator.hasNext();) {
				SMSAccount account2 = (SMSAccount) iterator.next();
				if (account2.getAlertCondition()!= null &&  //IM 2080104
						account2.getAlertCondition().getOperator() != null &&
						account2.getAlertCondition().getOperator().equalsIgnoreCase(pos)
						&& account2.getAccountNumber().equals(account)
						&& account2.getAccountSection().equalsIgnoreCase(accountSection)
						&& account2.getAccountSubtype().equalsIgnoreCase(productType)
						&& account2.getAccountProductId().equalsIgnoreCase(productId))
					return account2.getId();
			}
		}
		return "";
	}

	/**
	 * matchAccountWithBalanceAlert
	 * @param account TVFrontendAccount
	 * @param alert CustomerEmailBalanceAlert
	 * @return true o false
	 */
	public static boolean matchAccountWithBalanceAlert (TVFrontendAccount account, CustomerEmailBalanceAlert alert) {
		return alert.getAccountNumber().equals(account.getAccountNumber())
				&& alert.getAccountSection().equalsIgnoreCase(account.getAccountSection())
				&& alert.getAccountSubtype().equalsIgnoreCase(account.getProductDefinition().getProductType())
				&& alert.getAccountProductId().equalsIgnoreCase(account.getProductDefinition().getProductId());
	}

	/**
	 * matchAccountWithBalanceAlert
	 * @param account TVFrontendAccount
	 * @param alert SMSAccount
	 * @return true o false
	 */
	public static boolean matchAccountWithBalanceAlert (TVFrontendAccount account, SMSAccount alert) {
		return alert.getAccountNumber().equals(account.getAccountNumber())
				&& alert.getAccountSection().equalsIgnoreCase(account.getAccountSection())
				&& alert.getAccountSubtype().equalsIgnoreCase(account.getProductDefinition().getProductType())
				&& alert.getAccountProductId().equalsIgnoreCase(account.getProductDefinition().getProductId());
	}

	/**
	 * matchAccountWithBalanceAlert
	 * @param account TVFrontendAccount
	 * @param alert CustomerBalanceAlert
	 * @return true o false
	 */
	public static boolean matchAccountWithBalanceAlert (TVFrontendAccount account, CustomerBalanceAlert alert) {
		return alert.getAccountNumber().equals(account.getAccountNumber())
				&& alert.getAccountSection().equalsIgnoreCase(account.getAccountSection())
				&& alert.getAccountSubtype().equalsIgnoreCase(account.getProductDefinition().getProductType())
				&& alert.getAccountProductId().equalsIgnoreCase(account.getProductDefinition().getProductId())
				&& alert.isChannelPushActive();
	}

	/**
	 * updateCustomerBalanceAlert
	 * @param customerBalanceAlert CustomerBalanceAlert
	 * @param profile CustomerProfile
	 * @param language String
	 * @param frontact TVFrontendAccount
	 * @return CustomerBalanceAlert
	 */
	public static CustomerBalanceAlert updateCustomerBalanceAlert (CustomerBalanceAlert customerBalanceAlert,
																   CustomerProfile profile, String language, TVFrontendAccount frontact) {
		customerBalanceAlert.setCustomerProfile(profile);
		customerBalanceAlert.setPermId(profile.getPermId());
		customerBalanceAlert.setNickname(frontact.getNickname());
		customerBalanceAlert.setAccountNumber(frontact.getAccountNumber());
		customerBalanceAlert.setAccountProductId(frontact.getAccountProductId());
		customerBalanceAlert.setAccountSection(frontact.getAccountSection());
		customerBalanceAlert.setAccountSubtype(frontact.getSubtype());
		customerBalanceAlert.setAlertlanguage(language);
		customerBalanceAlert.setAlertposicion("X");

		return customerBalanceAlert;
	}

	/**
	 * createCustomerBalanceAlertInteractionEvent
	 * @param request HttpServletRequest
	 * @param customerBalanceAlert CustomerBalanceAlert
	 * @return CustomerInteractionEvent
	 */
	public static CustomerInteractionEvent createCustomerBalanceAlertInteractionEvent(HttpServletRequest request, CustomerBalanceAlert customerBalanceAlert) {
		CustomerInteractionEvent customerInteractionEvent = generateCustomerInteractionEvent(request);


		return updateCustomerBalanceAlertInteractionEvent(customerInteractionEvent, customerBalanceAlert);
	}

	/**
	 * updateCustomerBalanceAlertInteractionEvent
	 * @param customerInteractionEvent CustomerInteractionEvent
	 * @param customerBalanceAlert CustomerBalanceAlert
	 * @return CustomerInteractionEvent
	 */
	public static CustomerInteractionEvent updateCustomerBalanceAlertInteractionEvent(CustomerInteractionEvent customerInteractionEvent, CustomerBalanceAlert customerBalanceAlert) {
		customerInteractionEvent.setCustomStringData(PushNotificationInteraction.CATEGORY, BALANCE_PUSH_ + customerBalanceAlert.getAlertCondition().getOperator());

		customerInteractionEvent.setCustomStringData(PushNotificationInteraction.ACCOUNT_NUMBER, customerBalanceAlert.getAccountNumber());

		customerInteractionEvent.setCustomStringData(PushNotificationInteraction.AMOUNT, customerBalanceAlert.getAlertCondition().getAmount().toString());

		return customerInteractionEvent;
	}

	/**
	 * Push Notifications Flag, verify if Push flag is on, and MS operation endpoint is Enabled
	 * @return true o false
	 */
	public static boolean isPushEnabled() {
		return StaticMessageSource.isFlagEnabled(StaticMessageSource.PUSH_NOTIFICATIONS);
	}

	/**
	 * isPushPhase2Enabled
	 * @return true o false
	 */
	public static boolean isPushPhase2Enabled() {
		return isPushEnabled() && StaticMessageSource.isFlagEnabled(StaticMessageSource.PUSH_NOTIFICATIONS_PHASE_2);
	}

	/**
	 * messageCampaign
	 * @param userSession UserSession
	 * @param wealthCodes String
	 * @param preferredCodes String
	 * @return CustomerProfile
	 */
	public static Map<String, List<MyMessagesCampaign>> messageCampaign(UserSession userSession, String wealthCodes, String preferredCodes) {
		CustomerProfile customer = null;
		List<ActiveCampaignCustomerView> cust = null;
		CustomerServices customerService = userSession.getCustomerService();
		List<MyMessagesCampaign> alert = new java.util.LinkedList<MyMessagesCampaign>();
		List<MyMessagesCampaign> update = new java.util.LinkedList<MyMessagesCampaign>();
		List<MyMessagesCampaign> info = new java.util.LinkedList<MyMessagesCampaign>();
		List<MyMessagesCampaign> events = new java.util.LinkedList<MyMessagesCampaign>();
		Map<String, List<MyMessagesCampaign>> campaignsList = new HashMap<String, List<MyMessagesCampaign>>();
		if (userSession != null) {
			customer = userSession.getCustomerProfile();
			if (customer != null && customer.getTaxId() != null && customer.getEnrollmentAccountNumber() != null)
				cust = userSession.getCustomerService().getActiveCustCampaign(customer.getTaxId().trim());
		}

		List<ActiveCampaignView> list = userSession.getCustomerService().getActiveCampaignByType(CampaignType.MESSAGES);

		/*Llamado al metodo que actualiza los mensajes segun el segmento de perfiles definido*/
		Utils.updateMessagesList(list, userSession, wealthCodes, preferredCodes);

		if (userSession.isWealth(wealthCodes, preferredCodes)) {
			List<ActiveCampaignView> listWealth = userSession.getCustomerService().getActiveCampaignByType(
					CampaignType.MESSAGES_WEALTH);
			if (listWealth != null && listWealth.size() > 0)
				list.addAll(listWealth);
		} else if (userSession.isPreferred(wealthCodes, preferredCodes)) {
			List<ActiveCampaignView> listPreferred = userSession.getCustomerService().getActiveCampaignByType(
					CampaignType.MESSAGES_PREFERRED);
			if (listPreferred != null && listPreferred.size() > 0)
				list.addAll(listPreferred);
		}

		List<MyMessagesCampaign> lc = new java.util.LinkedList<MyMessagesCampaign>();
		for (Iterator<ActiveCampaignView> iterator = list.iterator(); iterator.hasNext();) {
			MyMessagesCampaign p = new MyMessagesCampaign(iterator.next());

			if (cust != null && cust.size() > 0 && p.getCampaign().getFileCampaign() != null) {
				for (int i = 0; i < cust.size(); i++) {
					if (cust.get(i).getId().getCampaignId().equalsIgnoreCase(p.getCampaign().getId())) {
						List<ActiveCampaignCustomerView> cust2 = customerService.getActiveCustCampaign(customer
								.getTaxId().trim());
						for (Iterator<ActiveCampaignCustomerView> iterator2 = cust2.iterator(); iterator2.hasNext();) {
							ActiveCampaignCustomerView cc = (ActiveCampaignCustomerView) iterator2.next();
							if (cc.getId().getCampaignId().equals(cust.get(i).getId().getCampaignId())) {
								p = Utils.replaceCampaignVariables(p, cc, logger);
								break;
							}
						}
						lc.add(p);
						if (p.getCampaign().getCampaignName().toUpperCase().contains("INFO"))
							info.add(p);
						else if (p.getCampaign().getCampaignName().toUpperCase().contains("WARNING"))
							alert.add(p);
						else if (p.getCampaign().getCampaignName().toUpperCase().contains("UPDATE"))
							update.add(p);
						else if (p.getCampaign().getCampaignName().toUpperCase().contains("EVENT"))
							events.add(p);

						break;
					}
				}
			} else if (p.getCampaign().getFileCampaign() == null) {
				lc.add(p);
				if (p.getCampaign().getCampaignName().toUpperCase().contains("INFO"))
					info.add(p);
				else if (p.getCampaign().getCampaignName().toUpperCase().contains("WARNING"))
					alert.add(p);
				else if (p.getCampaign().getCampaignName().toUpperCase().contains("UPDATE"))
					update.add(p);
				else if (p.getCampaign().getCampaignName().toUpperCase().contains("EVENT"))
					events.add(p);
			}

		}
		campaignsList.put(ALERT, alert);
		campaignsList.put(INFO, info);
		campaignsList.put(UPDATE, update);
		campaignsList.put(EVENTS, events);

		return campaignsList;
	}


	/**
	 * Method to convert object to a string with currency format ("$999.999.99")
	 *
	 * @param obj Object
	 * @return a
	 */
	public static String formatCurrency(Object obj) {
		org.apache.velocity.tools.generic.NumberTool tool = new org.apache.velocity.tools.generic.NumberTool();
		String a = "";
		if (obj != null)
			a = tool.currency(obj);
		if (a.indexOf("-") != -1) {
			a = a.replaceAll("-", "");
			a = "(" + a + ")";
		}
		return a;
	}

	/**
	 * MBC RDC Flag, verify if limits per segment flag is on
	 * @return
	 */
	public static boolean isLimitsPerSegmentEnabled() {
		return StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT);
	}
	/**
	 * Verifies if the user is Eligible to redeem.
	 * @return boolean
	 */
	public static boolean cashRewardsEligible(UserSession userSession, TVFrontendAccount ccaAccountByFrontEndId) {

		if(ccaAccountByFrontEndId != null
				&& ccaAccountByFrontEndId.getTsysLoyaltyRewardsInfo() != null
				&& ccaAccountByFrontEndId.isPrimaryAccount()
				&& ccaAccountByFrontEndId.getAccountProductId().equals("VPCBK")
				&& ccaAccountByFrontEndId.getTsysLoyaltyRewardsInfo().verifyCashRewardsAvailableBalance()
				&& ccaAccountByFrontEndId.getTsysLoyaltyRewardsInfo().getCanRedeemRewards()) {
			return true;
		}
		return false;
	}

	/**
	 * Only active debit accounts will be eligible for account deposit redemption.
	 *
	 * @param acct account detail object
	 * @param userSession session management
	 * @return boolean
	 */
	public static boolean excludeAccounts(TVFrontendAccount acct, UserSession userSession) {
		Account backendObject = (Account) acct.getBackendObject();
		if (backendObject.getRecords() == null)
			return false;
		AccountRecord record = backendObject.getRecords();
		if (backendObject.getRecords().getOwners() != null) {
			for (int i = 0; i < record.getOwners().length; ++i) {
				if (!GenericValidator.isBlankOrNull(record.getOwners()[i].getRelationshipType())
						&& (record.getOwners()[i].getRelationshipType().equals("005")
						|| record.getOwners()[i].getRelationshipType().equals("003"))) {
					if (!acct.getAccountProductId().equalsIgnoreCase("050")
							&& GenericValidator.isBlankOrNull(acct.getAccountNumberSuffix())
							&& acct.getProductDefinition().getProductStatusCode().equals("A")
							&& Utils.isAccountNoPost(userSession.getIdaAccounts(), acct)) {
						return true;
					}
				}
			}
		}
		return false;

	}

	/**
	 * exclude accounts Control Code NoPost for account deposit redemption.
	 *
	 * @param idaAccounts account list IDA
	 * @param acct account detail object
	 * @return boolean
	 */
	public static boolean isAccountNoPost(List<TVFrontendDepositAccount> idaAccounts, TVFrontendAccount acct) {
		boolean postTranCode = false;
		TVFrontendDepositAccount idaAccount = Utils.getIdaAccountByAccountNumber(acct.getAccountNumber(), idaAccounts);
		if (idaAccount != null && (idaAccount.getAccountKey().equals(acct.getAccountKey()) && idaAccount.getIdaAcctInq()
				.getAccountInformation().getGeneralSection().getPostInstruction().equals("0"))) {
			postTranCode = true;
		}
		return postTranCode;
	}

	/**
	 * @param months the least quantity of months that the account age must have
	 * @param accounts the accounts
	 * @param userSession the user session
	 * @return true if the account age is equal or older than the specified months, false otherwise
	 */
	public static boolean isAccountOldEnough(Integer months, List<TVFrontendAccount> accounts,
											 UserSession userSession) {

		if (userSession.getCustomerProfile().useCommercialInterface()) {
			return isEnrollmentAccountOlderThan(months, userSession);

		} else {
			return isOldestAccountOlderThan(months, userSession, accounts);
		}
	}

	/**
	 * @param months the quantity of months
	 * @param userSession the user session
	 */
	private static boolean isEnrollmentAccountOlderThan(Integer months, UserSession userSession) {

		TVFrontendAccount account = Utils.getAccountByAccountNumber(
				userSession.getCustomerProfile().getEnrollmentAccountNumber(),
				userSession.getAccounts());
		AbstractAccount accountDetails = userSession.getCustomerService().getAccountDetails(
				userSession.getCustomerProfile(), userSession.getBankingSession(), account, true);

		if (accountDetails.getBackendObject() instanceof DepositAccount) {
			DepositAccount accountAux = (DepositAccount) accountDetails.getBackendObject();
			Calendar calendar = Calendar.getInstance();

			calendar.add(Calendar.MONTH, -months);

			if (!accountAux.getRecords().getOpenDate().getTime().after(calendar.getTime())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @param months the quantity of months
	 * @param userSession the user session
	 * @param accounts the accounts
	 */
	private static boolean isOldestAccountOlderThan(Integer months, UserSession userSession,
													List<TVFrontendAccount> accounts) {

		Calendar olderThan = Calendar.getInstance();
		Calendar oldest = Calendar.getInstance();
		olderThan.add(Calendar.MONTH, -months);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		CustomerEntitlement ent = userSession.getCustomerProfile().getEntitlementByType(EntitlementType.BANKING);
		BankingEntitlement banking = new BankingEntitlement(ent);
		if (banking.getOldestAccountDate() != null) {
			try {
				Date oldestAccountDate = format.parse(banking.getOldestAccountDate());
				oldest.setTime(oldestAccountDate);
			} catch (ParseException e) {
				logger.error(StringEscapeUtils.escapeJava("UNABLED TO PARSE OLD ACCOUNT DATE"));
			}
			if (!oldest.getTime().after(olderThan.getTime())) {
				return true;
			}
			return false;
		}
		for (int i = 0; i < accounts.size(); i++) {
			TVFrontendAccount account = accounts.get(i);
			AbstractAccount accountDetails = userSession.getCustomerService().getAccountDetails(
					userSession.getCustomerProfile(), userSession.getBankingSession(), account, true);

			if (accountDetails.getBackendObject() instanceof DepositAccount) {
				DepositAccount accountAux = (DepositAccount) accountDetails.getBackendObject();
				if (!accountAux.getRecords().getOpenDate().getTime().after(oldest.getTime())) {
					oldest.setTime(accountAux.getRecords().getOpenDate().getTime());
				}
				if (!accountAux.getRecords().getOpenDate().getTime().after(olderThan.getTime())) {
					return true;
				}
			}
		}
		banking.setOldestAccountDate(format.format(oldest.getTime()));

		CustomerFacade.getInstance().updateEntitlement(userSession,
				Collections.singletonList(banking.getCustomerEntitlement()));

		return false;
	}

	/**
	 * containsPayments
	 * @param frontEndId
	 * @param userSession
	 * @return boolean
	 */
	public static boolean containsPayments(String frontEndId, UserSession userSession) {
		final List<TVFrontEndPayee> payees = userSession.getPayees(true);

		for (Iterator<TVFrontEndPayee> iterator = payees.iterator(); iterator.hasNext(); ) {
			TVFrontEndPayee payee = iterator.next(); // payee object

			if (payee.getFrontEndId().equals(frontEndId)) {
				return payee.hasPreviousRealTimePayment();
			}
		}
		return false;
	}

	public static Map<String, String> detailedAccountFromPIF(PIFExternalAccount account) {

		Map<String, String> accountDetail = new LinkedHashMap<String, String>();
		if (account == null) {
			accountDetail.put("accountName", "");
			accountDetail.put("acctLast4Num", "");
			accountDetail.put("accountBalance", "");
			accountDetail.put("accountNumber", "");
			accountDetail.put("accountSubtype", "");
			accountDetail.put("accountSection", "");
			accountDetail.put("text", "");

		} else {
			AccountMaskUtil mask = new AccountMaskUtil();

			StringBuilder sb = new StringBuilder();

			accountDetail.put("accountName", account.getNickname());
			accountDetail.put("accountLast4Num", mask.mask(account.getAccountNumber()));
			accountDetail.put("accountNumber", account.getAccountNumber());
			accountDetail.put("accountSubtype", PIF_PREFIX);
			accountDetail.put("accountSection", "");

			sb.append(StringUtils.abbreviate(accountDetail.get("accountName"), 15));
			sb.append(" ");
			sb.append(accountDetail.get("accountLast4Num"));
			accountDetail.put("text", sb.toString());
		}
		return accountDetail;


	}

	/**
	 * containsPayments
	 * @return boolean
	 */
	public static boolean onlyHasAccountType (Set<String> accountTypes, List<TVFrontendAccount> lstAccounts) {

		boolean  hasAccountType = true; // hasAccountType

		if (accountTypes == null || accountTypes.isEmpty() || lstAccounts == null
				|| lstAccounts.isEmpty() || accountTypes.contains(null)) {

			hasAccountType = false;

		} else {

			for (TVFrontendAccount tvFrontendAccount : lstAccounts) {

				if (!hasAccountType(accountTypes, tvFrontendAccount.getSubtype())) {

					hasAccountType = false;
					break;

				}
			}
		}


		return hasAccountType;
	}

	/**
	 * @param strUserAgent
	 * @param strRegainTokenTrace
	 * @param uSession
	 * @return String
	 */
	public static String generateRaEventTraceInformation (String strUserAgent, String strRegainTokenTrace, UserSession uSession ) {
		String strTraceInformation = "";//
		final String strTokenInfo = DeviceUtils.LINE_BREAK + "Token: ";//
		final int size = 16;//
		if(strUserAgent != null) {
			if (DeviceUtils.getCustomerDevice(strUserAgent).equals(CustomerDevices.ANDROID) || DeviceUtils.getCustomerDevice(strUserAgent).equals(CustomerDevices.IPHONE)) {
				final String[] userAgentInfo = strUserAgent.split(";"); //Separate User-agent info
				final DeviceInfo deviceInfoTrace = new DeviceInfo(); //DeviceInfo Instance
				StringBuilder builder = new StringBuilder(size);  //StrinBuilder to create Trace Info

				if(DeviceUtils.getCustomerDevice(strUserAgent).equals(CustomerDevices.ANDROID)) {
					builder = getUserAgentAndroidInfo(userAgentInfo, deviceInfoTrace, builder);
					uSession.setUserAgentType(UserAgentType.ANDROID);
				}else if(DeviceUtils.getCustomerDevice(strUserAgent).equals(CustomerDevices.IPHONE)) {
					builder = getUserAgentIphoneInfo(userAgentInfo, deviceInfoTrace, builder);
					uSession.setUserAgentType(UserAgentType.IPHONE);
				}
				builder.append(strTokenInfo + strRegainTokenTrace);
				strTraceInformation = DeviceUtils.getBrowserInfo(strUserAgent, uSession) + builder.toString();
			}else {
				uSession.setUserAgentType(UserAgentType.WEB);
				strTraceInformation = DeviceUtils.getBrowserInfo(strUserAgent, uSession) + (strTokenInfo + strRegainTokenTrace);
			}

		}

		return strTraceInformation;
	}

	/**
	 * @param userAgentInfo
	 * @param deviceInfoTrace
	 * @param builder
	 * @return StringBuilder
	 */
	private static StringBuilder getUserAgentAndroidInfo(String[] userAgentInfo, DeviceInfo deviceInfoTrace, StringBuilder builder ) {
		final char[] androidDeviceModel = userAgentInfo[2].toCharArray();//DeviceModel

		deviceInfoTrace.setOsVersion(userAgentInfo[1]);
		deviceInfoTrace.setDeviceModel(NOTHING);

		for(char c: androidDeviceModel) {
			if(c == ')') {
				break;
			}
			deviceInfoTrace.setDeviceModel(deviceInfoTrace.getDeviceModel() + c);
		}
		builder.append(DeviceUtils.LINE_BREAK)
				.append(DeviceUtils.MODEL).append(deviceInfoTrace.getOsVersion() + deviceInfoTrace.getDeviceModel());

		return builder;
	}

	/**
	 * @param userAgentInfo
	 * @param deviceInfoTrace
	 * @param builder
	 * @return StringBuilder
	 */
	private static StringBuilder getUserAgentIphoneInfo(String[] userAgentInfo, DeviceInfo deviceInfoTrace, StringBuilder builder ) {
		final char[] iphoneOsVersion = userAgentInfo[1].toCharArray();// OsVersion

		deviceInfoTrace.setOsVersion(NOTHING);
		for(char c: iphoneOsVersion) {
			if(c == ')') {
				break;
			}
			deviceInfoTrace.setOsVersion(deviceInfoTrace.getOsVersion() + c);
		}
		builder.append(DeviceUtils.LINE_BREAK)
				.append(DeviceUtils.MODEL).append(deviceInfoTrace.getOsVersion());

		return builder;
	}

	/**
	 * MBSE-2216: filtro de mensajes por segmentos incluidos en caso de que sea diferente de nulo.
	 * @author ET58344
	 * @since 02-04-2020 Modified 11-19-2020 Release Remanentes
	 * @param activeCampaignView listado de mensajes.
	 * @param userSession sesion de usuario.
	 * @param wealthCodes string.
	 * @param preferredCodes string.
	 */
	public static void updateMessagesList(List<ActiveCampaignView> activeCampaignView, UserSession userSession,
										  String wealthCodes, String preferredCodes) {
		final int deleteListSizeMax = 20; //max size list
		List<ActiveCampaignView> deleteList = new ArrayList<>(deleteListSizeMax); //list for delete elements

		if(activeCampaignView != null) {
			activeCampaignView.removeAll(validateCampaignsIncludeExclude(activeCampaignView, userSession, wealthCodes,
					preferredCodes, deleteList));
		}
	}

	/**
	 * MBSE-2216: filtro de mensajes por segmentos incluidos en caso de que sea diferente de nulo, separated for kiwan
	 * cyclomatic complexity defect.
	 * @author ET58344
	 *
	 * @since 11-23-2020
	 * @param activeCampaignView messages list
	 * @param userSession user session
	 * @param wealthCodes codes
	 * @param preferredCodes codes
	 * @param deleteList result list
	 * @return List<ActiveCampaignView> value
	 */
	private static List<ActiveCampaignView> validateCampaignsIncludeExclude (List<ActiveCampaignView> activeCampaignView,
																			 UserSession userSession, String wealthCodes,
																			 String preferredCodes,
																			 List<ActiveCampaignView> deleteList) {
		final String propertyStr = "<properties/>"; //string property
		boolean isInclude; //boleano para validar segmentos

		for (ActiveCampaignView eachCampaign : activeCampaignView) {
			isInclude = false;

			if ((eachCampaign.getIncludeSegments() != null && !eachCampaign.getIncludeSegments().equals(propertyStr))
					|| eachCampaign.getFileCampaign() != null) {
				isInclude = validateCampaignsIncludeExcludes(eachCampaign, userSession, wealthCodes, preferredCodes);

			} else if(eachCampaign.getIncludeSegments() != null && eachCampaign.getIncludeSegments().equals(propertyStr)){
				isInclude = Boolean.TRUE;
			}

			if (!isInclude) {
				deleteList.add(eachCampaign);
			}
		}
		return deleteList;
	}

	/**
	 * MBSE-2216: filtro de mensajes por segmentos incluidos en caso de que sea diferente de nulo, separated for kiwan
	 * cyclomatic complexity defect.
	 * @author ET58344
	 *
	 * @since 11-23-2020
	 * @param userSession user session
	 * @param wealthCodes codes
	 * @param preferredCodes codes
	 * @return List<ActiveCampaignView> value
	 */
	private static boolean validateCampaignsIncludeExcludes(ActiveCampaignView eachCampaign,
															UserSession userSession,
															String wealthCodes, String preferredCodes){
		final String strAllSegments = "ALL"; //segment all
		final boolean isInclude; //boolean

		if (eachCampaign.getIncludeSegment(strAllSegments) != null || eachCampaign.getFileCampaign() != null) {
			isInclude = Boolean.TRUE;

		} else {
			isInclude = Utils.hasMessageIncludeSegments(eachCampaign, userSession, wealthCodes, preferredCodes);
		}
		return isInclude;
	}

	/**
	 *
	 * @param accountTypes
	 * @param strSubType
	 * @return
	 */
	private static boolean hasAccountType (Set<String> accountTypes, String strSubType) {

		return accountTypes.contains(strSubType);
	}

	public static String neutralizeMessage(String message) {
		// ensure no CRLF injection into logs for forging records
		String clean = message.replace( '\n', '_' ).replace( '\r', '_' );
		if ( ESAPI.securityConfiguration().getLogEncodingRequired()) {
			clean = ESAPI.encoder().encodeForHTML(clean);
			if (!message.equals(clean)) {
				clean += " (Encoded)";
			}
		}
		return clean;
	}

	/**
	 * removeInactiveCardPlastics Return Array With Only Valid Card Plastics
	 * @param cardPlastics
	 * @return CardPlasticFrontend[]
	 * ACTIVE is Active CARD
	 * INACTIVE not return in this Array
	 */
	public static CardPlasticFrontend[] removeInactiveCardPlastics (UserSession userSession,
																	CardPlasticFrontend[] cardPlastics) {
		final String inactiveStatusCard = "INACTIVE"; // Active card Status Value

		List<CardPlasticFrontend> activePlastics = new ArrayList(Arrays.asList(cardPlastics.clone())); // active plastic list
		String status = ""; // status card

		if (cardPlastics != null) {
			List<TVFrontendAccount> statusPlastics = userSession.getCcaCardPlastics();
			for (CardPlasticFrontend cardPlastic : cardPlastics) {
				setCCAPlasticCardStatus(statusPlastics, cardPlastic);
				if (!GenericValidator.isBlankOrNull(cardPlastic.getCardPlasticAccountRecord().getStatus())) {
					status = cardPlastic.getCardPlasticAccountRecord().getStatus().trim();

					if (status.contains(inactiveStatusCard)) {
						activePlastics.remove(cardPlastic);
					}
				}
			}
		}


		return activePlastics.toArray(new CardPlasticFrontend[activePlastics.size()]);
	}

	/**
	 * setCCAPlasticCardStatus
	 * @param plastic
	 */
	public static void setCCAPlasticCardStatus (List<TVFrontendAccount> statusPlastics, CardPlasticFrontend plastic) {
		if (statusPlastics != null) {
			for (TVFrontendAccount account: statusPlastics
			) {
				if (account.getAccountKey().startsWith(plastic.getCardPlasticAccountRecord().getCardPANId())) {
					plastic.getCardPlasticAccountRecord().setStatus(account.getStatus());
				}
			}
		}
	}


	/**
	 * @param request
	 * @param eSession
	 * @return String
	 */
	public static String generateDblPrEmailTraceInfo (HttpServletRequest request, EnrollmentSession eSession) {
		String strTraceInformation = "";//
		final String strUserAgent = request.getHeader("user-agent");//
		final UserSession uSession = new UserSession();//

		uSession.setUserAgentType(eSession.getUserAgentType());

		if(strUserAgent != null) {
			if (uSession.getUserAgentType().equals(UserAgentType.WEB)) {
				strTraceInformation = DeviceUtils.getBrowserInfo(strUserAgent, uSession);
			}else {
				generateMobileTrace(uSession, DeviceUtils.getCustomerDevice(strUserAgent), strUserAgent);
				strTraceInformation = DeviceUtils.getMobileInfo(strUserAgent, uSession.getDeviceInfo());
			}

		}

		return strTraceInformation;
	}

	/**
	 * @param uSession
	 * @param customerDevice
	 * @param strUserAgent
	 */
	private static void generateMobileTrace(UserSession uSession, CustomerDevices customerDevice
			, String strUserAgent) {
		final DeviceInfo deviceInfo = new DeviceInfo(true);//

		deviceInfo.setAppVersion(strUserAgent);
		deviceInfo.setDeviceModel(customerDevice.toString());
		uSession.setDeviceInfo(deviceInfo);
	}

	/**
	 * Remove Premia options for dropdown list.
	 * @param modelAndView
	 */
	public static void removePremiaOptions (ModelAndView modelAndView) {

		final ModelMap modelMap = modelAndView.getModelMap(); // modelMap

		removeOptions(modelMap, modelAndView.getViewName());

	}

	/**
	 * Remove options.
	 * @param modelMap
	 * @param strModelViewName
	 */
	private static void removeOptions (ModelMap modelMap, String strModelViewName) {

		final String modelViewTransaction = "transaction"; // modelview name
		final String modelViewDetail = "detail"; // modelview name

		String accountsVar = ""; // STMT_ACCOUNTS
		String accountsMapVar = ""; // STMT_ACCOUNTS_MAP

		if (modelViewTransaction.equalsIgnoreCase(strModelViewName)) {
			accountsVar = "stmtAccounts";
			accountsMapVar = "stmtAccountsMap";
		} else {
			if (modelViewDetail.equalsIgnoreCase(strModelViewName)) {
				accountsVar = "detailAccountsMap";
				accountsMapVar = "detailAccounts";
			}
		}

		remove(modelMap, accountsVar, accountsMapVar);

	}

	/**
	 * Remove the accounts from model.
	 * @param modelMap
	 * @param strAccountsVar
	 * @param strAccountsMapVar
	 */
	private static void remove (ModelMap modelMap, String strAccountsVar, String strAccountsMapVar) {

		Map<String, TVFrontendAccount> tvFrontendAccounts = null; // stmtAccounts
		Map<String, String> tvFrontendAccountsMap = null; // stmtAccountsMap

		if (modelMap.get(strAccountsVar) instanceof Map && modelMap.get(strAccountsMapVar) instanceof Map) {
			tvFrontendAccounts = (Map<String, TVFrontendAccount>) modelMap.get(strAccountsVar);
			tvFrontendAccountsMap = (Map<String, String>) modelMap.get(strAccountsMapVar);

			final String prmKeyName = getPRMKey(tvFrontendAccounts); // key of account

			if (!StringUtils.isEmpty(prmKeyName)) {
				tvFrontendAccountsMap.remove(prmKeyName);
			}

			final String ghostAccountKeyName = getGhostAccountKey(tvFrontendAccounts); // key of account

			if (!StringUtils.isEmpty(ghostAccountKeyName)) {
				tvFrontendAccountsMap.remove(ghostAccountKeyName);
			}

			modelMap.put(strAccountsMapVar, tvFrontendAccountsMap);
		}

	}

	/**
	 * Returns Premia account key.
	 * @param accountMap
	 * @return
	 */
	private static String getPRMKey (Map<String, TVFrontendAccount> accountMap) {

		final String premiaProductType = "PRM"; // prm

		String keyName = ""; // name of account key
		final Set<Entry<String, TVFrontendAccount>> accountEntrySet = accountMap.entrySet(); // account entry set

		for (Map.Entry<String, TVFrontendAccount> account : accountEntrySet) {
			if (premiaProductType.equalsIgnoreCase(account.getValue().getProductDefinition().getProductType())) {
				keyName = account.getKey();
				break;
			}
		}
		return keyName;
	}

	/**
	 * Returns ghost account key.
	 * @param accountMap
	 * @return
	 */
	private static String getGhostAccountKey (Map<String, TVFrontendAccount> accountMap) {

		final String ghostAccountProductId = "PLNOC"; // Ghost account product ID

		String keyName = ""; // name of account key
		final Set<Entry<String, TVFrontendAccount>> accountEntrySet = accountMap.entrySet(); // account entry set

		for (Map.Entry<String, TVFrontendAccount> account : accountEntrySet) {
			if (ghostAccountProductId.equalsIgnoreCase(account.getValue().getProductDefinition().getProductId())) {
				keyName = account.getKey();
				break;
			}
		}

		return keyName;
	}

	/**
	 * Validate redirect to PREMIA.
	 * @param modelAndView
	 * @return
	 */
	public static boolean urlAccessCheck (ModelAndView modelAndView) {

		final String premiaProduct = "PRM"; // prm
		final String modelViewTransaction = "transaction"; // modelview name
		final String modelViewDetail = "detail"; // modelview name

		final ModelMap modelMap = modelAndView.getModelMap(); // modelMap

		boolean hasRedirect = false; // redirect flag

		if (modelViewTransaction.equalsIgnoreCase(modelAndView.getViewName())) {

			final String selectedAccount = "selectedAccount"; // selectedAccount

			if (Optional.ofNullable(modelMap.get(selectedAccount)).map(Object::toString).filter(Objects::nonNull)
					.filter(value -> premiaProduct.equalsIgnoreCase(value)).isPresent()) {

				hasRedirect = true;
			}

		} else {

			if (modelViewDetail.equalsIgnoreCase(modelAndView.getViewName())) {

				final String premiaCycle = "premiacycle"; // premiacycle

				if (Optional.ofNullable(modelMap.get(premiaCycle)).isPresent()) {

					hasRedirect = true;
				}
			}
		}

		return hasRedirect;
	}

	public static boolean hasCustomerCode(List<String> customerCodes, String[] searchCodes) {
		boolean exist = false;

		final int length = searchCodes.length;
		String code = "";
		try {
			for (Iterator<String> iterator = customerCodes.iterator(); iterator.hasNext();) {
				code = KiuwanUtils.cast(String.class, iterator.next());
				if (!GenericValidator.isBlankOrNull(code)) {
					for (int i = 0; i < length; i++) {
						if (code.equals(searchCodes[i])
								|| (Integer.parseInt(code) == Integer.parseInt(searchCodes[i]))) {
							exist = true;
							break;
						}
					}
				}
			}
		} catch (NumberFormatException e) {
			logger.error("Error hasCustomerCode premiumBankingCodes");
		}

		return exist;
	}

	/**
	 * Metodo que verifica los segmentos que posee el mensaje.
	 * @author Included by ET58344
	 * @since 06-04-2020
	 * @param p ActiveCampaignView
	 * @param s UserSession
	 * @param wealthCodes String
	 * @param preferredCodes String
	 * @return Boolean true o false
	 */
	public static Boolean hasMessageIncludeSegments(ActiveCampaignView p, UserSession s, String wealthCodes,
													String preferredCodes) {
		final int strZeroLength = 0; //string zero
		final int listSizeMax = 20; //list size
		boolean result = false; //boolean result
		String includeSegment = StringUtils.EMPTY; //boolean include segment
		String include = null; //string include

		List<String> segmentTypeList = new ArrayList<>(listSizeMax); //list of segments

		segmentTypeList = insertValuesList();

		for(String eachSegment : segmentTypeList) {
			include = p.getIncludeSegment(eachSegment);

			if(include != null && include.length() != strZeroLength){
				includeSegment = eachSegment;
				result = validateSegmentInclude(includeSegment, s);
				result = validateCustomSegmentInclude(result, includeSegment, s, wealthCodes, preferredCodes);

				if(result) {
					break;
				}
			}
		}
		return result;
	}

	/**
	 * Metodo que valida el segmente encontrado y si el user session lo posee.
	 * @author Created by ET58344
	 * @since 14-04-2020
	 * @param includeSegment segmento incluido
	 * @param s user session
	 * @return boolean result
	 */
	static boolean validateSegmentInclude(String includeSegment, UserSession s) {
		boolean result = false; //boolean result

		if(!includeSegment.equals(StringUtils.EMPTY)) {

			switch(includeSegment) {
				case "AUTO":
					result = s.isHasAuto();
					break;
				case "CCA":
					result = s.hasCCA();
					break;
				case "CD":
					result = s.isHasCD();
					break;
				case "COMERCIAL":
					result = s.isComercialCustomer();
					break;
				case "DDA":
					result = s.isHasDDA();
					break;
				default:
					break;
			}
		}
		result = validateSegmentInclude2(result, includeSegment, s);

		return result;
	}

	/**
	 * Metodo que valida el segmente encontrado y si el user session lo posee.
	 * @author Created by ET58344
	 * @since 14-04-2020
	 * @param result boolean result
	 * @param includeSegment segmento incluido
	 * @param s user session
	 * @return boolean result
	 */
	private static boolean validateSegmentInclude2(boolean result, String includeSegment, UserSession s) {
		boolean nResult = result; //new result

		if(!nResult){
			if (!includeSegment.equals(StringUtils.EMPTY)) {

				switch (includeSegment) {
					case "EACCOUNT":
						nResult = s.isHasEaccount();
						break;
					case "EBILL":
						nResult = s.isHasEbill();
						break;
					case "ESTATMENT":
						nResult = s.isHasEstatment();
						break;
					case "INSURANCE":
						nResult = s.isHasInsurance();
						break;
					case "IRA":
						nResult = s.isHasIRA();
						break;
					default:
						break;
				}
			}
		}
		nResult = validateSegmentInclude3(nResult, includeSegment, s);

		return nResult;
	}

	/**
	 * Metodo que valida el segmente encontrado y si el user session lo posee.
	 * @author Created by ET58344
	 * @since 14-04-2020
	 * @param result boolean result
	 * @param includeSegment segmento incluido
	 * @param s user session
	 * @return boolean result
	 */
	private static boolean validateSegmentInclude3(boolean result, String includeSegment, UserSession s) {
		boolean nResult = result; //new result

		if(!nResult){
			if (!includeSegment.equals(StringUtils.EMPTY)) {

				switch (includeSegment) {
					case "LEASING":
						nResult = s.isHasLeasing();
						break;
					case "MORTGAGE":
						nResult = s.isHasMortgage();
						break;
					case "LOAN":
						nResult = s.isHasPersonalLoans();
						break;
					case "PREMIA":
						nResult = s.isHasPremia();
						break;
					case "SECURITIES":
						nResult = s.isHasSecurities();
						break;
					default:
						break;
				}
			}
		}

		return nResult;
	}

	/**
	 * Metodo que valida el segmente encontrado y si el user session lo posee.
	 * @author Created by ET58344
	 * @since 14-04-2020
	 * @param result resultado boleano
	 * @param includeSegment segmento string
	 * @param s variable de session
	 * @param wealthCodes codigos wealth
	 * @param preferredCodes codigos preferred
	 * @return boolean result
	 */
	static boolean validateCustomSegmentInclude(boolean result, String includeSegment, UserSession s,
												String wealthCodes, String preferredCodes) {
		boolean nResult = result; //new result

		if(!nResult){
			if(!includeSegment.equals(StringUtils.EMPTY)) {

				switch (includeSegment) {
					case "MOBILE":
						nResult = s.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING);
						break;
					case "PREFERRED":
						nResult = s.isPreferred(wealthCodes, preferredCodes);
						break;
					case "WEALTH":
						nResult = s.isWealth(wealthCodes, preferredCodes);
						break;
					default:
						break;
				}
			}
		}
		return nResult;
	}

	/**
	 * Metodo que inserta los valores de segmentos en la lista.
	 * @author Created by ET5344
	 * @since 15-04-2020
	 * @return List<String> lista llena
	 */
	private static List<String> insertValuesList() {
		final int listSizeMax = 20; //list size
		List<String> segmentTypeList = new ArrayList<>(listSizeMax); //result list

		segmentTypeList.add(CampaignSegmentType.AUTO.toString());
		segmentTypeList.add(CampaignSegmentType.CCA.toString());
		segmentTypeList.add(CampaignSegmentType.CD.toString());
		segmentTypeList.add(CampaignSegmentType.COMERCIAL.toString());
		segmentTypeList.add(CampaignSegmentType.DDA.toString());
		segmentTypeList.add(CampaignSegmentType.EACCOUNT.toString());
		segmentTypeList.add(CampaignSegmentType.EBILL.toString());
		segmentTypeList.add(CampaignSegmentType.ESTATMENT.toString());
		segmentTypeList.add(CampaignSegmentType.INSURANCE.toString());
		segmentTypeList.add(CampaignSegmentType.IRA.toString());
		segmentTypeList.add(CampaignSegmentType.LEASING.toString());
		segmentTypeList.add(CampaignSegmentType.MORTGAGE.toString());
		segmentTypeList.add(CampaignSegmentType.LOAN.toString());
		segmentTypeList.add(CampaignSegmentType.PREMIA.toString());
		segmentTypeList.add(CampaignSegmentType.SECURITIES.toString());
		segmentTypeList.add(CampaignSegmentType.MOBILE.toString());
		segmentTypeList.add(CampaignSegmentType.PREFERRED.toString());
		segmentTypeList.add(CampaignSegmentType.WEALTH.toString());

		return segmentTypeList;
	}

	/**
	 *
	 * @param request
	 * @throws CsrfGuardException
	 */
	public static boolean validateTokenCsrfGuard(HttpServletRequest request) {

		CsrfGuard csrfGuard = CsrfGuard.getInstance();
		String tokenFromSession = WebUtils.getSessionAttribute(request, csrfGuard.getTokenName()) instanceof String
				? (String) WebUtils.getSessionAttribute(request, csrfGuard.getTokenName())
				: null;
		String tokenFromRequest = request.getParameter(csrfGuard.getTokenName()) instanceof String
				? (String) request.getParameter(csrfGuard.getTokenName())
				: null;

		if (tokenFromRequest == null) {
			/** FAIL: token is missing from the request **/
			return false;
		} else if (!tokenFromSession.equals(tokenFromRequest)) {
			/** FAIL: the request token does not match the session token **/
			return false;
		}

		return true;
	}

	/**
	 *
	 * @param request
	 * @throws CsrfGuardException
	 */
	public static void generateTokenCsrfGuard(HttpServletRequest request)
			throws CsrfGuardException {

		CsrfGuard csrfGuard = CsrfGuard.getInstance();
		String token = RandomGenerator.generateRandomId(csrfGuard.getPrng(), csrfGuard.getTokenLength());
		WebUtils.setSessionAttribute(request, csrfGuard.getTokenName(), token);
		request.setAttribute(csrfGuard.getTokenName(), token);
	}

	/**
	 * getTsysPrimaryCustomerIdentifiers
	 * 
	 * @param cardNumber card number
	 * @return CustomerIdentifiers
	 */
	public static CustomerIdentifiers getTsysPrimaryCustomerIdentifiers(String cardNumber) throws TsysException {

		CustomerIdentifiers custIdentifiers = null;

		ArrayList<CustomerList> customerArrayList = getArrListCustomerList(cardNumber, false);

		if (customerArrayList != null) {
			custIdentifiers = new CustomerIdentifiers();
			for (CustomerList innerInnerList : customerArrayList) {
				custIdentifiers.setCustomerId(innerInnerList.getCustomerId());
				custIdentifiers.setAccountId(innerInnerList.getAccount());
			}
		}

		return custIdentifiers;
	}
	
	/**
	 * getCustomerIdentifiersByNameOnCard
	 * 
	 * @param cardNumber card number
	 * @return CustomerIdentifiers
	 */
	public static CustomerIdentifiers getCustomerIdentifiersByCardLast4(String cardNumber) throws TsysException {
		CustomerIdentifiers custIdentifiers = null;
		final AccountMaskUtil maskUtil = new AccountMaskUtil(); //card mask util

		ArrayList<CustomerList> customerArrayList = getArrListCustomerList(cardNumber, true);

		if (customerArrayList != null) {
			custIdentifiers = new CustomerIdentifiers();
			for (CustomerList innerInnerList : customerArrayList) {
				if (innerInnerList.getName() != null && innerInnerList.getCardLast4().equals(maskUtil.mask4(cardNumber))) {
					custIdentifiers.setCustomerId(innerInnerList.getCustomerId());
					custIdentifiers.setAccountId(innerInnerList.getAccount());
					break;
				}
			}
		}
		return custIdentifiers;
	}
	
	/**
	 * getArrListCistomerList
	 * @param cardNumber card number
	 * @param completeList complete list
	 * @return ArrayList<CustomerList> 
	 */
	private static ArrayList<CustomerList> getArrListCustomerList(String cardNumber, boolean completeList) throws TsysException {
		ArrayList<CustomerList> customerArrayList = null;
		TsysGeneralInformationResponse generalInformationResponse = TsysAlertFacade.getInstance()
				.tsysGeneralInformationByCardNumber(cardNumber, completeList);
		if (generalInformationResponse != null) {
			customerArrayList = generalInformationResponse.getCustomerList();
		}
		return customerArrayList;
	}

	/**
	 * This method call to register event to history log in cibp core.
	 *
	 * @param request request
	 * @param acctNum account number
	 * @param userSession user session
	 * @param customerService customer services
	 * @param status activate or deactivate
	 * @author Evertec Inc.
	 * @since 05-13-2022
	 */
	public static void registerStatusEbillHistoryLog(HttpServletRequest request, String acctNum,
													 UserSession userSession, CustomerServices customerService,
													 String status){

		final AccountMaskUtil maskUtil = new AccountMaskUtil(); //mask util
		final CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request); //customer event
		final String header = request.getHeader("user-agent"); //user agent

		event.setCustomStringData(EStatementInteraction.LAST_CARD_NUMBER, EStatementInteraction.CREDIT_CARD_STR
				.concat(maskUtil.mask(acctNum)));
		event.setCustomStringData(EStatementInteraction.DEVICE, DeviceUtils.getBrowserInfo(header, userSession));

		if (EStatementInteraction.ACTIVATE_STATUS.equals(status)) {
			customerService.logUpdateActivatedEbills(event, InteractionSeverity.INFO);
		} else {
			customerService.logUpdateDesactivatedEbills(event, InteractionSeverity.INFO);
		}
	}

	/**
	 * This method call to register event to history log with register or update alert request information.
	 *
	 * @param request request
	 * @param acctNum account number
	 * @param userSession user session
	 * @param status activate or deactivate
	 * @author Popular Inc.
	 * @since 09-26-2022
	 */
	public static void registerTsysAlertHistoryLog(HttpServletRequest request, String acctNum,
													 UserSession userSession, TsysAlertStatus status){

		final CustomerServices customerService = userSession.getCustomerService(); //customer services
		final AccountMaskUtil maskUtil = new AccountMaskUtil(); //mask util
		final CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request); //customer event
		final String header = request.getHeader("user-agent"); //user agent

		event.setCustomStringData(Utils.CARD_NUMBER_LAST_FOUR, Utils.CREDIT_CARD_STR.concat(maskUtil.mask(acctNum)));
		event.setCustomStringData(EStatementInteraction.DEVICE, DeviceUtils.getBrowserInfo(header, userSession));

		if (TsysAlertStatus.ACTIVATE.equals(status)) {
			customerService.logUpdateActivatedTsysAlerts(event, InteractionSeverity.WARNING);
		} else {
			customerService.logUpdateDeactivatedTsysAlerts(event, InteractionSeverity.WARNING);
		}
	}

	/**
	 * This method call to register event to history log with the alert amount changes.
	 *
	 * @param request request
	 * @param acctNum account number
	 * @param userSession user session
	 * @param previousAmount previous amount
	 * @param newAmount new amount
	 * @author Popular Inc.
	 * @since 09-26-2022
	 */
	public static void registerTsysChangedAlertAmountHistoryLog(HttpServletRequest request, String acctNum,
												   UserSession userSession, String previousAmount, String newAmount){

		final CustomerServices customerService = userSession.getCustomerService(); //customer services
		final AccountMaskUtil maskUtil = new AccountMaskUtil(); //mask util
		final CustomerInteractionEvent event = Utils.generateCustomerInteractionEvent(request); //customer event
		final String header = request.getHeader("user-agent"); //user agent

		event.setCustomStringData(Utils.CARD_NUMBER_LAST_FOUR, Utils.CREDIT_CARD_STR + (maskUtil.mask(acctNum)));
		event.setCustomStringData(EStatementInteraction.DEVICE, DeviceUtils.getBrowserInfo(header, userSession));
		event.setCustomStringData(Utils.PREVIOUS_AMOUNT, Utils.PREVIOUS_AMOUNT_STR + previousAmount.replaceAll(",", ""));
		event.setCustomStringData(Utils.NEW_AMOUNT, Utils.NEW_AMOUNT_STR + newAmount.replaceAll(",", ""));
		customerService.logChangedAlertAmountTsys(event, InteractionSeverity.WARNING);
	}
	
	/**
	 * get transaction by  trace id
	 * @param transactions
	 * @param traceId
	 * @return AbstractTransaction
	 */
	public static Optional<AbstractTransaction> getTransactionByTraceId(List<AbstractTransaction> transactions, String traceId) {
		if (StringUtils.isNotBlank(traceId) && !Objects.isNull(transactions)) {
			return transactions.stream()
					.filter(transaction -> {
						if (transaction.getBackendObject() instanceof Transaction) {
							return ((Transaction) transaction.getBackendObject()).getTraceId().equals(traceId);
						}
						return false;
					})
					.findAny();
		}

		return Optional.empty();
	}

	/**
	 * get transactions CCA OR IDA
	 * @param customerService
	 * @param req
	 * @param account
	 * @return AbstractTransaction
	 */
	public static TransactionsResponse getTransactionsCCAOrIDA(CustomerServices customerService, TransactionRequest req, TVFrontendAccount account) {
		List<AbstractTransaction> transactions = new ArrayList<>();
		TransactionsResponse transactionResponse = new TransactionsResponse();
		if (!Objects.isNull(account)) {
			String productType = account.getProductDefinition().getProductType();
			if (productType.equals("CCA")) {
				transactionResponse = customerService.getCreditCardCurrentTransactions(req);
			} else if (productType.equals("IDA")) {
				transactionResponse = customerService.getDepositCurrentTransactions(req);
			}
		}
		return transactionResponse;
	}
	
	/**
	 * Verify if comes from mobile native app petition
	 * @param userSession
	 * @param header
	 * @return true if is a webview
	 */
	public static boolean isMobile(UserSession userSession) {
		return (userSession.getUserAgentType() == UserAgentType.IPHONE ||
		        userSession.getUserAgentType() == UserAgentType.IPAD ||
		        userSession.getUserAgentType() == UserAgentType.ANDROID);
	}
	
	/**
	 * @param strUserAgent
	 * @param userSession
	 * @return String
	 */
	public static String generateOOBInterruptionEventTraceInformation(String strUserAgent, UserSession userSession) {
		String strTraceInformation = StringUtils.EMPTY;//
		final int size = 16;//
		
		if (Optional.ofNullable(strUserAgent).filter(s->!s.isEmpty()).isPresent()) {
			if (DeviceUtils.getCustomerDevice(strUserAgent).equals(CustomerDevices.ANDROID)
					|| DeviceUtils.getCustomerDevice(strUserAgent).equals(CustomerDevices.IPHONE)) {
				final String[] userAgentInfo = strUserAgent.split(";"); // Separate User-agent info
				final DeviceInfo deviceInfoTrace = new DeviceInfo(); // DeviceInfo Instance
				StringBuilder builder = new StringBuilder(size); // StrinBuilder to create Trace Info

				if (DeviceUtils.getCustomerDevice(strUserAgent).equals(CustomerDevices.ANDROID)) {
					builder = getUserAgentAndroidInfo(userAgentInfo, deviceInfoTrace, builder);
					userSession.setUserAgentType(UserAgentType.ANDROID);
				} else if (DeviceUtils.getCustomerDevice(strUserAgent).equals(CustomerDevices.IPHONE)) {
					builder = getUserAgentIphoneInfo(userAgentInfo, deviceInfoTrace, builder);
					userSession.setUserAgentType(UserAgentType.IPHONE);
				}
				strTraceInformation = DeviceUtils.getBrowserInfo(strUserAgent, userSession) + builder.toString();
			} else {
				userSession.setUserAgentType(UserAgentType.WEB);
				strTraceInformation = DeviceUtils.getBrowserInfo(strUserAgent, userSession);
			}
		}

		return strTraceInformation;
	}	


	/**
	 * This method calls tsys getTsysPrimaryCustomerIdentifiers, returns customer identifier info. verify first in session if exist an customers idenfiers.
	 * @param cardNumber Card Number
	 * @param userSession user session
	 * @return CustomerIdentifiers - list of identifiers for cards
	 */
	public static CustomerIdentifiers getGeneralInformationCustomerIdentifiers(String cardNumber, UserSession userSession) {

		if (userSession.getCcaCustomerIdentifierMap() != null && userSession.getCcaCustomerIdentifierMap().containsKey(cardNumber)) {
			// in session
			return userSession.getCcaCustomerIdentifierMap().get(cardNumber);
		} else {
			AccountMaskUtil maskUtil = new AccountMaskUtil(); //mask util
			// call tsys
			try {
				CustomerIdentifiers customerIdentifiers = Utils.getCustomerIdentifiersByCardLast4(cardNumber); //cust identifiers
				if (customerIdentifiers != null) {
					userSession.setCcaCustomerIdentifierInfo(cardNumber, customerIdentifiers);
					return customerIdentifiers;
				} else {
					return null;
				}
			} catch(TsysException ex){
				logger.error("Tsys error in method getCustomerIdentifiersByCardLast4, with user: " + userSession.getUsername() +
						", account: " + maskUtil.mask(cardNumber) + ", error detail: " + ex.getDetail());
				return null;
			}
		}
	}
}
