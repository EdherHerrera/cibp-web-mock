package evertec.cibp.web.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

public class ImageUtils {
	private static final Logger logger = Logger.getLogger(ImageUtils.class);

	public static boolean checkIfImageExists(String filePath) {
		File f = new File(filePath);
		return f.isFile();
	}

	public static byte[] getImageFromURL(String strUrl) {
		byte[] img = null;
		try {
			URL url = new URL(strUrl);
			URLConnection urlConn = url.openConnection();
			InputStream inStream = urlConn.getInputStream();
			img = IOUtils.toByteArray(inStream);
		} catch (Exception e) {
			logger.debug("Error downloading the image from: " + strUrl);
		}
		return img;
	}

	public static void writeImage(String fileName, byte[] data) {
		
		OutputStream outp = null;
		try {
			// System.out.println(logData);
			File log = new File(fileName);
			if (log.exists()) {
				outp = new FileOutputStream(fileName, false);
				outp.write(data);
				outp.flush();
				outp.close();
			} else {

				outp = new FileOutputStream(fileName);
				outp.write(data);
				outp.flush();
				outp.close();
			}
		} catch (Exception e) {
			System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
		}finally{
			if(outp != null) {
				try {
					outp.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
				}
			}
		}
	}

}
