package evertec.cibp.web.utils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.PFMEntitlement;
import evertec.cibp.core.services.CustomerServices;

public class PFMUtil {

	// Create the PFM entitlement for this profile
	public boolean createPfmEntitlement(CustomerServices customerService, CustomerProfile profile) {
		try {
			PFMEntitlement pfmEntitlement = new PFMEntitlement();
			pfmEntitlement.setEmptyEntitlement();
			customerService.getCustomerProfileService()
					.addEntitlement(profile, pfmEntitlement.getCustomerEntitlement());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// Create the PFM user
	public String createPfmUser(CustomerServices customerService, CustomerProfile profile) {
		return customerService.addPfmUser(String.valueOf(Integer.parseInt(profile.getTaxId())
				+ profile.getEnrollmentAccountNumber()));
	}
}
