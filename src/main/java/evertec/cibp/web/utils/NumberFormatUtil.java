package evertec.cibp.web.utils;

import org.apache.velocity.tools.generic.NumberTool;

public class NumberFormatUtil extends NumberTool {

	@Override
	public String format(String format, Object obj) {
		// TODO Auto-generated method stub
		String a = "";
		if (obj != null)
			a = super.format(format, obj);
		if (a.indexOf("-") != -1) {
			a = a.replaceAll("-", "");
			a = "(" + a + ")";
		}
		return a;

	}

	public String formatAmtNoCur(String format, Object obj) {

		String a = format(format, obj);
		if (a.contains("$"))
			a = a.replaceAll("\\$", "");

		return a;

	}

	public String formatAbsolute(String format, Object obj) {
		// TODO Auto-generated method stub
		String a = "";
		if (obj != null)
			a = super.format(format, obj);
		if (a.indexOf("-") != -1) {
			a = a.replaceAll("-", "");
		}
		return a;

	}
}
