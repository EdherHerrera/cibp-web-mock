/**
 * 
 */
package evertec.cibp.web.utils;

import java.io.Serializable;
import java.util.Comparator;

import evertec.cibp.core.models.transfer.TVAccountTransfer;

public class TransferSort implements Comparator<TVAccountTransfer>, Serializable {

	private static final long serialVersionUID = 1L;

	public int compare(TVAccountTransfer a, TVAccountTransfer b) {

		// String namea = getName(a);;
		// String nameb = getName(b);

		return (-1) * a.getEffectiveDate().compareTo(b.getEffectiveDate());

		// return namea.compareTo(nameb);
	}

	/*
	 * private String getName(TVAccountTransfer a) {
	 * 
	 * 
	 * 
	 * String name = a.getPayeeName(); if(StringUtils.isEmpty(name)) return
	 * "ZZZZZZZZZZZZZZZZZ";
	 * 
	 * return name.toUpperCase();
	 * 
	 * }
	 */

}