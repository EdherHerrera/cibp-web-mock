package evertec.cibp.web.utils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import evertec.cibp.vs.beans.accounts.Account;
import evertec.cibp.vs.enums.account.DPAcctType;

/**
 * An abstract class than help while converting LinkedHashMap<String, ?> object to PaymentHistoryResponse object 
 * 
 * @author Jorge De Los Santos
 * @since 1.0
 * @version 1.0
 * 
 * @see <a href="https://github.com/FasterXML/jackson-docs/wiki/JacksonMixInAnnotations">Jackson MixIn Annotations</a>
 */
public abstract class AccountExtMixin extends Account{
	
	
	@JsonCreator
	public AccountExtMixin(@JsonProperty("dpType") DPAcctType acctType) {
		super(acctType);
	}

}
