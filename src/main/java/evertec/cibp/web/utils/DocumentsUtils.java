package evertec.cibp.web.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import com.itextpdf.text.pdf.PdfReader;

import evertec.cibp.core.models.documents.CustomerDocuments;
import evertec.cibp.core.models.documents.CustomerDocumentsPackage;
import evertec.cibp.core.services.CustomerServices;
import evertec.otherservices.requests.GetDocumentRequest;
import evertec.otherservices.requests.SignDocumentRequest;
import evertec.otherservices.responses.GetDocumentResp;

public class DocumentsUtils {

	/**
	 * @deprecated This method is implemented as a microservice.
	 * Please contact mbna@evertecgroup.onmicrosoft.com if a change will be added
	 * to the implementation. It should be in both projects until a complete
	 * microservices transition is completed.
	 */
	public static boolean signDocuments(String fullCustomerName, CustomerDocumentsPackage disbursement, CustomerServices customerService)
	{
		try{
			Date acceptanceDate = new Date();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy h:mm a");
			String acceptanceMessage = "Documento aceptado de forma electrónica por %s a través de Mi Banco en %s";
			SignDocumentRequest req = new SignDocumentRequest();

			req.setFontSize(8);
			req.setRotation(0);
			req.setAllPagesSigned(true);
			req.setMessage(String.format(acceptanceMessage, fullCustomerName, df.format(acceptanceDate)));

			// Calculation made for font size: 8 (For other sizes, change the
			// charactersInPage variable)
			int charactersInPage = 170;
			int xPosition = Math
					.round((((float) (charactersInPage - req.getMessage().length()) / 2) / charactersInPage) * 610);
			req.setxPosition(xPosition);
			if (disbursement != null) {
				for (Iterator<CustomerDocuments> iterator = disbursement.getCustomerDocuments().iterator(); iterator
						.hasNext();) {
					CustomerDocuments document = iterator.next();
					req.setDocumentumId(document.getDocSearchId());
					// Calculates the Y position to put the signature in the correct
					// place
					int yPosition = 785;
					GetDocumentRequest documentumRequest = new GetDocumentRequest();
					documentumRequest.setDocumentId(document.getDocSearchId());
					GetDocumentResp documentumResponse = customerService.getDocumentFile(documentumRequest);
					if (documentumResponse != null && documentumResponse.getDocumentByteArray() != null
							&& documentumResponse.getDocumentByteArray().length > 0) {
						PdfReader reader = new PdfReader(documentumResponse.getDocumentByteArray());
						if (reader.getNumberOfPages() > 0)
							yPosition = Math.round(reader.getPageSize(1).getHeight()) - 10;
					}
					req.setyPosition(yPosition);
					customerService.signDocument(req);
				}

				disbursement.setAcceptedDate(acceptanceDate);

				return customerService.updateDocumentPackage(disbursement);
			}
			return false;
		}catch(Exception e){
			return false;
		}
	}
}
