package evertec.cibp.web.utils;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;

import com.google.common.base.Optional;

import evertec.cibp.core.models.customer.CustomerDevices;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.models.devices.DeviceInfo;
import evertec.cibp.web.session.UserSession;

import javax.servlet.http.HttpServletRequest;

/**
 * @author jocampo FFEIC
 */
public class DeviceUtils {

    /**constants LOGGER */
    public static final Logger LOGGER = Logger.getLogger(DeviceUtils.class);

    /**constants DEVICE_TOKEN_COOKIE */
    public static final String DEVICE_TOKEN_COOKIE = "MB_DEV";

    /**constants DEVICE */
    public static final String DEVICE = "Device: ";
    
    /**constants MODEL  */
    public static final String MODEL = "Model: ";
    
    /**constants OS_VERSION */
    public static final String OS_VERSION = "OS Version: ";
    
    /**constants APP_VERSION */
    public static final String APP_VERSION = "App Version: ";
    
    /**constants SERVICE_PROVIDER */
    public static final String SERVICE_PROVIDER = "Service Provider: ";
    
    /**constants LINE_BREAK */
    public static final String LINE_BREAK = "<br/>";

    /**constants BROWSER_CHROME */
    public static final String BROWSER_CHROME = "Chrome";
    
    /**constants BROWSER_FIREFOX */
    public static final String BROWSER_FIREFOX = "Firefox";
    
    /**constants BROWSER_MS_IE_OLD */
    public static final String BROWSER_MS_IE_OLD = "MSIE";
    
    /**constants BROWSER_MS_IE_NEW */
    public static final String BROWSER_MS_IE_NEW = "rv:";
    
    /**constants BROWSER_MS_EDGE */
    public static final String BROWSER_MS_EDGE = "Edg";
    
    /**constants BROWSER_SAFARI */
    public static final String BROWSER_SAFARI = "Safari";
    
    /**constants BROWSER_UNKNOWN */
    public static final String BROWSER_UNKNOWN = "unknown";
    
    /**constants BROWSER_VERSION */
    public static final String BROWSER_VERSION = "Version/";
    
    /**constants RESULT_DEVICE_WEB */
    public static final String RESULT_DEVICE_WEB = "Device: WEB";
    
    /**constants RESULT_ANDROID_DEVICE_WEB */
    public static final String RESULT_ANDROID_DEVICE_WEB = "Device: Android WEB";
    
    /**constants RESULT_IPHONE_DEVICE_WEB */
    public static final String RESULT_IPHONE_DEVICE_WEB = "Device: iPhone WEB";
    
    /**constants RESULT_BROWSER */
    public static final String RESULT_BROWSER = "Browser: ";
    
    /**constants RESULT_BROWSER_VERSION */
    public static final String RESULT_BROWSER_VERSION = "Browser Version: ";
    
    /**constants RESULT_BROWSER_IE */
    public static final String RESULT_BROWSER_IE = "Internet Explorer";
    
    /**constants CONNECTION_INFO */
    public static final String CONNECTION_INFO = "CONNECTION_INFO";

    /**
     * getCustomerDevice
     * @param userAgent String
     * @return CustomerDevices
     */
    public static CustomerDevices getCustomerDevice(String userAgent) {

        if (userAgent.indexOf("iPod") != -1) {
            return CustomerDevices.IPOD;
        } else if (userAgent.indexOf("iPhone") != -1) {
            return CustomerDevices.IPHONE;
        } else if (userAgent.indexOf("iPad") != -1) {
            return CustomerDevices.IPAD;
        } else if (userAgent.indexOf("Android") != -1) {
            return CustomerDevices.ANDROID;
        } else if (userAgent.indexOf("BlackBerry") != -1) {
            return CustomerDevices.BLACKBERRY;
        } else if (userAgent.indexOf("Motorola") != -1) {
            return CustomerDevices.MOTOROLA;
        } else if (userAgent.indexOf("IEMobile") != -1) {
            return CustomerDevices.IEMOBILE;
        } else if (userAgent.indexOf("Windows CE") != -1) {
            return CustomerDevices.WINDOWSCE;
        } else {
            return CustomerDevices.PERSONAL;
        }
    }

    /**
     * getBrowserInfo
     * @param userAgent String
     * @param userSession UserSession
     * @return browserName
     */
    public static String getBrowserInfo(String userAgent, UserSession userSession) {
        String browserName = BROWSER_UNKNOWN;
        String browserVersion = BROWSER_UNKNOWN;
        try {
            if (userAgent.contains(BROWSER_CHROME)) { // checking if Chrome or MS Egde
                if (userAgent.contains(BROWSER_MS_EDGE)) {
                    String substring = userAgent.substring(userAgent.indexOf(BROWSER_MS_EDGE)).split("\\)")[0];
                    browserName = substring.split("/")[0];
                    browserVersion = substring.split("/")[1];
                } else {
                    String substring = userAgent.substring(userAgent.indexOf(BROWSER_CHROME)).split(" ")[0];
                    browserName = substring.split("/")[0];
                    browserVersion = substring.split("/")[1];
                }
            } else if (userAgent.contains(BROWSER_FIREFOX)) { // Checking if Firefox
                String substring = userAgent.substring(userAgent.indexOf(BROWSER_FIREFOX)).split(" ")[0];
                browserName = substring.split("/")[0];
                browserVersion = substring.split("/")[1];
            } else if (userAgent.contains(BROWSER_MS_IE_OLD)) { // Checking if Internet Explorer
                String substring = userAgent.substring(userAgent.indexOf(BROWSER_MS_IE_OLD)).split(";")[0];
                browserName = RESULT_BROWSER_IE;
                browserVersion = substring.split(" ")[1];
            } else if (userAgent.contains(BROWSER_MS_IE_NEW)) { // checking if Internet Explorer 11
                String substring = userAgent.substring(userAgent.indexOf(BROWSER_MS_IE_NEW)).split("\\)")[0];
                browserName = RESULT_BROWSER_IE;
                browserVersion = substring.split(BROWSER_MS_IE_NEW)[1];
            } else if (userAgent.contains(BROWSER_SAFARI)) { // checking if Internet Explorer 11
                String substring = userAgent.substring(userAgent.indexOf(BROWSER_VERSION)).split(BROWSER_VERSION)[1];
                browserName = BROWSER_SAFARI;
                browserVersion = substring.split(" ")[0];
            }
        } catch (Exception e) {
            LOGGER.error("Error parsing user agent browser: " + userAgent);
        }
        return (userSession.getUserAgentType().equals(UserAgentType.WEB) ? RESULT_DEVICE_WEB
                : (userSession.getUserAgentType().equals(UserAgentType.ANDROID) ? RESULT_ANDROID_DEVICE_WEB
                        : RESULT_IPHONE_DEVICE_WEB))
                + "<br/>" + RESULT_BROWSER + browserName + "<br/>" + RESULT_BROWSER_VERSION + browserVersion;
    }

    /**
     * getMobileInfo
     * @param userAgent String
     * @param device DeviceInfo
     * @return String
     */
    public static String getMobileInfo(String userAgent, DeviceInfo device) {

        StringBuilder builder = new StringBuilder();
        CustomerDevices customerDivice = Optional.fromNullable(getCustomerDevice(userAgent)).or(CustomerDevices.PERSONAL); 
        
        if(device ==  null) {
            device = new DeviceInfo(true);
        }
        
        builder.append(LINE_BREAK).append(DEVICE).append(customerDivice).append(LINE_BREAK)
        .append(MODEL).append(device.getDeviceModel()).append(LINE_BREAK)
        .append(OS_VERSION).append(device.getOsVersion()).append(LINE_BREAK)
        .append(APP_VERSION).append(device.getAppVersion()).append(LINE_BREAK)
        .append(SERVICE_PROVIDER).append(device.getNetworkProvider()).append(LINE_BREAK)
        .append(LINE_BREAK);
        
        return builder.toString();
    }

    /** Validate user-agent
     * to verify if the request is been made from MiBanco.app.
     *
     * @param request HttpServletRequest
     * @return boolean
     */
    public static boolean isUserAgentMobileApp(HttpServletRequest request) {
        final String userAgentStr = "user-agent";//
        final String miBancoMatchStr = ".*MiBanco\\.app.*";//
        final String userAgent = request.getHeader(userAgentStr);//

        return !GenericValidator.isBlankOrNull(userAgent) && (
                    userAgent.matches(miBancoMatchStr) ||
                    userAgent.matches(".*WebView*"));
    }

}
