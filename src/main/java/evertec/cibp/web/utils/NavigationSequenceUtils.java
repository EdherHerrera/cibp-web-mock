package evertec.cibp.web.utils;

import java.util.List;

import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.web.form.ATHMovilForm;
import evertec.cibp.web.form.AlertTypesForm;
import evertec.cibp.web.form.BalancesForm;
import evertec.cibp.web.form.CcaPurchasesAndWithdrawalsForm;
import evertec.cibp.web.form.ChallengeTypeOOBForm;
import evertec.cibp.web.form.ChangeOOBInfoForm;
import evertec.cibp.web.form.DeactivateOOBForm;
import evertec.cibp.web.form.EditEmailForm;
import evertec.cibp.web.form.EditFrecuencyOOBController;
import evertec.cibp.web.form.OOBChallengeForm;
import evertec.cibp.web.form.PaymentsAndTransfersForm;
import evertec.cibp.web.form.PersonalInformationChangesForm;
import evertec.cibp.web.form.PersonalInformationForm;
import evertec.cibp.web.form.PurchasesAndWithdrawalsForm;
import evertec.cibp.web.form.RecoveryCodeOOBForm;
import evertec.cibp.web.form.SecondaryPhoneOOBForm;
import evertec.cibp.web.form.SmsForm;
import evertec.cibp.web.form.SmsOOBForm;
import evertec.cibp.web.form.mobile.bean.MobileAlert;

public class NavigationSequenceUtils {
	public static String getNextView(List<ASK_FOR_OOB> tasksList) {

		if (tasksList.size() == 0) {
			return "login";
		}

		ASK_FOR_OOB ask = tasksList.get(0);

		return getAskView(ask);
	}

	public static String getAskView(ASK_FOR_OOB ask) {
		switch (ask) {
		
		case CHANGE_OOB:
			return "redirect:changeOOB";
		case EDIT_PRIMARY_PHONE:
			return "redirect:smsOOB";
		case OOB_CHALLENGE:
			return "redirect:loginoob";
		case EDIT_BACKUP_PHONE:
			return "redirect:secondaryPhoneOOB";
		case EDIT_CHALLENGE_TYPE:
			return "redirect:challengeTypeOOB";
		case EDIT_CHALLENGE_FREQUENCY:
			return "redirect:editFrecuencyOOB";
		case GENERATE_RECOVERY_CODE:
			return "redirect:recoveryCodeOOB";
		case DEACTIVATE_2_STEP:
			return "redirect:deactivateOOB";
		case EDIT_SMS:
			return "redirect:sms";
		case ALERT_TYPES:
			return "redirect:alerttypes";
		case PERSONAL_INFORMATION:
			return "redirect:personalinformation";
		case ATH_MOVIL:
			return "window.top.location='athmovil?tab=2';";
		case MY_DOCUMENTS:
			return "if((window.top.location.pathname=='/cibp-web/actions/mydocuments') || (window.top.location.pathname=='/cibp-web/actions/welcome')){window.top.location.href='portal';}else{window.top.location.reload();}"; 
		case BALANCE :
			return "balances";
		case ATH_ALERT_TYPES :
			return "purchasesandwithdrawals";
		case CCA_ALERT_TYPES :
			return "purchasesandwithdrawalscc";
		case PERSONAL_INFORMATION_CHANGES :
			return "personalinformationchanges";
		case PAYMENTS_TRANSFERS :
			return "paymentsandtransfers";
		case EDIT_EMAIL :
			return "editemail";
		case PUSH_DEVICE_REMOVAL:
			return MobileAlert.DEVICE_MANAGEMENT_VIEW;
		default:
			return "error";
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static Class getAskClass(ASK_FOR_OOB ask) {

		switch (ask) {
		case CHANGE_OOB:
			return ChangeOOBInfoForm.class;
		case EDIT_PRIMARY_PHONE:
			return SmsOOBForm.class;
		case OOB_CHALLENGE:
			return OOBChallengeForm.class;
		case EDIT_BACKUP_PHONE:
			return SecondaryPhoneOOBForm.class;
		case EDIT_CHALLENGE_TYPE:
			return ChallengeTypeOOBForm.class;
		case EDIT_CHALLENGE_FREQUENCY:
			return EditFrecuencyOOBController.class;
		case GENERATE_RECOVERY_CODE:
			return RecoveryCodeOOBForm.class;
		case DEACTIVATE_2_STEP:
			return DeactivateOOBForm.class;
		case EDIT_SMS:
			return SmsForm.class;
		case ALERT_TYPES:
			return AlertTypesForm.class;
		case PERSONAL_INFORMATION:
			return PersonalInformationForm.class;
		case MY_DOCUMENTS:
			return OOBChallengeForm.class;
		case ATH_MOVIL:
			return ATHMovilForm.class;
		case BALANCE :
			return BalancesForm.class;
		case ATH_ALERT_TYPES :
			return PurchasesAndWithdrawalsForm.class;
		case CCA_ALERT_TYPES :
			return CcaPurchasesAndWithdrawalsForm.class;
		case PERSONAL_INFORMATION_CHANGES :
			return PersonalInformationChangesForm.class;
		case PAYMENTS_TRANSFERS :
			return PaymentsAndTransfersForm.class;
		case EDIT_EMAIL :
			return EditEmailForm.class;
		default:
			return null;

		}

	}
}
