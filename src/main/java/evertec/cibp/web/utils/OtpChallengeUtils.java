/**
 * OTP Challenge Utility class
 */
package evertec.cibp.web.utils;

import java.util.Date;
import org.apache.commons.lang3.RandomStringUtils;
import evertec.cibp.web.session.UserSession;

/**
 * OTP Challenge Utils
 * 
 * @author Raul Mora
 * @version 1.0
 * @since 1.0
 * @see 1.0
 *
 */
public class OtpChallengeUtils {
	
	/**
	 * The OTP code length
	 */
	private static final int OTP_CODE_LENGTH = 6; // the OTP code length
	
	/**
	 * Default constructor
	 */
	private OtpChallengeUtils() {}
	
	/**
	 * @param session the user session
	 * @return true if challenge was sent, otherwise false
	 */
	public static boolean sendChallenge (UserSession session) {
		
		final String otpCode = RandomStringUtils.randomNumeric(OTP_CODE_LENGTH); // the OTP code
		System.out.println("OTP CODE: " + otpCode);
		Date now = new Date(); // Current Date
		
		session.setOtpChallengeCode(otpCode);
		session.setOtpLastChallengeDate(now);
		
		session.setOtpSendRetries(session.getOtpSendRetries() + 1);
		
		return session.getCustomerService().sendOtpChallengeEmail(session.getCustomerProfile(), 
				otpCode, session.getPreferredLanguage());
	}
	
	/**
	 * @param  userSession the user session
	 * @param  expirationPeriod Expiration Time
	 * @return true if code has Expired, otherwise false
	 */
	public static boolean hasCodeExpired (UserSession userSession, int expirationPeriod) {
		boolean hasExpired = false;//Expired bool
		Date now = new Date(); // Current Date
        Date previous = userSession.getOtpLastChallengeDate(); // User OtpChallengeDate
        final int millis = 1000;
        final int expiredTime = expirationPeriod * millis;
        
        hasExpired = now.getTime() - previous.getTime() >= expiredTime;
        
        return hasExpired;
	}
	
	/**
	 * @param userSession the user session
	 * @param resendMaxLimit Max Resend Retries
	 * @return true if code has Expired, otherwise false
	 */
	public static boolean hasReachedResendLimit (UserSession userSession, int resendMaxLimit) {
		boolean hasReachedLimit = false;//Expired bool
        final int resentCount = userSession.getOtpSendRetries();
        
        hasReachedLimit = resentCount == resendMaxLimit + 1;// (resendMaxLimit + 1) -> Compare including the Fist Attempt of the sendEmail Max limit
        
        return hasReachedLimit;
	}
	
	/**
	 * @param userSession the user session
	 * @param validateMaxLimit Max Validate Retries
	 * @return true if code has Expired, otherwise false
	 */
	public static boolean hasReachedValidateLimit (UserSession userSession, int validateMaxLimit) {
		boolean hasReachedLimit = false;//Expired bool
		final int resentCount = userSession.getOtpValidateRetries();
		
        hasReachedLimit = resentCount >= validateMaxLimit;
        
        return hasReachedLimit;
	}
	
}
