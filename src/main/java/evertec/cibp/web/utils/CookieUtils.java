/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evertec.cibp.web.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.util.CookieGenerator;
import org.springframework.web.util.WebUtils;

/**
 * 
 * @author et23558
 */
public class CookieUtils {

	public static void addCookie(HttpServletResponse response, int age, String name, String value) {
		CookieGenerator cookie = new CookieGenerator();
		cookie.setCookieName(name);
		cookie.setCookieMaxAge(age);
		cookie.addCookie(response, value);
	}

	public static Cookie getCookie(HttpServletRequest request, String name) {
		return WebUtils.getCookie(request, name);
	}

	public static boolean isValidCookie(HttpServletRequest request, String name) {
		Cookie cookie = getCookie(request, name);
		if (cookie == null || cookie.getValue() == null || cookie.getValue().length() == 0) {
			return false;
		} else {
			return true;
		}
	}

	public static void removeCookie(HttpServletResponse response, String name) {
		CookieGenerator cookieGenerator = new CookieGenerator();
		cookieGenerator.setCookieName(name);
		cookieGenerator.removeCookie(response);
	}
}
