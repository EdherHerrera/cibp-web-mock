package evertec.cibp.web.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;

public class JSONUtil {

	public static String escapeString(String originalString) {
		Gson gson = new Gson();
		return gson.toJson(originalString);
	}

	public static String escapeAndRemoveLinks(String originalString) {
		if (originalString == null)
			return escapeString("");
		String l = originalString.replaceAll("<a .*<\\/a>", "");
		return escapeString(l);

	}
	
	public static String dateFormatInbox (String originalString) throws ParseException{
		if (originalString == null)
				return escapeString("");
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date dateInvoice = formatter.parse(originalString);
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMdd");
		String formattedDateString = formatter2.format(dateInvoice);
		
		return escapeString(formattedDateString);
	}

}
