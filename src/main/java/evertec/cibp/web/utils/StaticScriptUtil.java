package evertec.cibp.web.utils;

/**
 * AccountMaskUtil
 * @author et56169
 * @since 1.2
 * @version 1.0 
 */
public class StaticScriptUtil {

	public static final String MINOR = "<";
	public static final String MAJOR = ">";
	public static final String SCRIPT = "SCRIPT";
	public static final String URL_REDIRECT = "/actions/portal";
			
}
