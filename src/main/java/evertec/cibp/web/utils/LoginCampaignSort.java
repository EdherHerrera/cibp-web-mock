/**
 * 
 */
package evertec.cibp.web.utils;

import java.io.Serializable;
import java.util.Comparator;

import evertec.cibp.core.models.campaign.LoginCampaign;

public class LoginCampaignSort implements Comparator<LoginCampaign>, Serializable {

	private static final long serialVersionUID = 1L;
	private int sortby;

	public int compare(LoginCampaign a, LoginCampaign b) {

		if (sortby == 0)
			return a.getCampaign().getUploadedByUser().compareTo(b.getCampaign().getUploadedByUser());
		else if (sortby == 1)
			return a.getCampaign().getCampaignName().compareTo(b.getCampaign().getCampaignName());
		else if (sortby == 2)
			return a.getCampaign().getStartDate().compareTo(b.getCampaign().getStartDate());
		else if (sortby == 3)
			return a.getCampaign().getEndDate().compareTo(b.getCampaign().getEndDate());
		else if (sortby == 4)
			return a.getCampaign().getImageId().compareTo(b.getCampaign().getImageId());
		else
			return a.getCampaign().getId().compareTo(b.getCampaign().getId());
	}

	public int getSortby() {
		return sortby;
	}

	public void setSortby(int sortby) {
		this.sortby = sortby;
	}

}