/**
 * author jocampo
 */
package evertec.cibp.web.utils;

import evertec.cibp.web.utils.requests.GoogleAnalyticsTrackingRequest;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GoogleAnalyticsTracking implements Runnable {
	
	private String url;
	
	protected final Log logger = LogFactory.getLog(getClass());

	public GoogleAnalyticsTracking(GoogleAnalyticsTrackingRequest gaRequest){
		
		this.url = "http://www.google-analytics.com/collect?";
		//this.url = "https://ssl.google-analytics.com/collect?";
		
		this.url += "v=" + gaRequest.getV();             // Version.
		this.url += "&tid=" + gaRequest.getTid();
		this.url += "&cid=" + gaRequest.getCid();
		this.url += "&t=" + gaRequest.getT();         // Event hit type.
		this.url += "&ec=" + encode(gaRequest.getEc());
		this.url += "&ea=" + encode(gaRequest.getEa());
		this.url += "&el=" + encode(gaRequest.getEl());
		this.url += "&ev=" + encode(gaRequest.getEv());
		
		this.url += "&cd2=" + encode(gaRequest.getCd2());
		this.url += "&cd3=" + encode(gaRequest.getCd3());
		this.url += "&cd4=" + encode(gaRequest.getCd4());
		this.url += "&cd5=" + encode(gaRequest.getCd5());
		this.url += "&cd6=" + encode(gaRequest.getCd6());
		this.url += "&cd7=" + encode(gaRequest.getCd7());
		this.url += "&cd8=" + encode(gaRequest.getCd8());
		this.url += "&cd9=" + encode(gaRequest.getCd9());
		this.url += "&cd10=" + encode(gaRequest.getEv());
	}

	/**
	* Posts an Event Tracking message to Google Analytics.
	*/
	public void run(){
		try{
			URL obj = new URL(this.url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			int responseCode = con.getResponseCode();
			if(responseCode != 200){
				throw new Exception("response code " + responseCode);
			}
			/*System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	 
			//print result
			System.out.println(response.toString());*/
		}
		catch (Exception e){
			logger.error("Error while sending analytics data: " + e.getMessage());
		}
	}

	private static String encode(String value){
		try{
			return URLEncoder.encode(value, StandardCharsets.UTF_8.name());
		}
		catch (Exception e){
			return "";
		}
	}

}
