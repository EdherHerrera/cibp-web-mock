package evertec.cibp.web.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.tools.generic.NumberTool;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.web.session.UserSession;

public class AthmUtils {
	protected static final Log logger = LogFactory.getLog(AthmUtils.class.getClass());
	
	public final static String REPORT_REASON_LOST = "LOST";
	public final static String REPORT_REASON_STOLEN = "STOLEN";

	public static TVFrontendAccount getAccountByAccountNumber(String id, UserSession userSession) {
		TVFrontendAccount frontendAccount = null;

		if (id != null && id.trim().length() > 0 && userSession != null && userSession.getAccounts() != null
				&& userSession.getAccounts().size() > 0) {
			frontendAccount = Utils.getAccountByAccountNumber(id, userSession.getAccounts());
		}

		return frontendAccount;
	}

	public static String divideLongMessageWord(String message) {
		String dividedMessage = message;
		if (message != null && !message.contains(" ") && message.length() > 24) {
			dividedMessage = message.substring(0, 24) + " " + message.substring(24, message.length());
		}
		return dividedMessage;
	}

	public static Map<String, String> getAccountDetails(TVFrontendAccount account) {
		Map<String, String> accountDetail = new LinkedHashMap<String, String>();

		if (account == null) {
			accountDetail.put("accountName", "");
			accountDetail.put("acctLast4Num", "");
			accountDetail.put("accountBalance", "");
			accountDetail.put("text", "");
		} else {
			AccountMaskUtil mask = new AccountMaskUtil();
			NumberTool numberTool = new NumberTool();

			StringBuilder sb = new StringBuilder();

			accountDetail.put("accountName", account.getNickname());
			accountDetail.put("accountLast4Num", mask.mask(account.getAccountNumber()));
			accountDetail.put("accountSubtype", account.getSubtype());
			accountDetail.put("accountNumber", account.getTVAccount().getAccountID());

			if (account != null && account.getSubtype().equals("CCA") && account.getAvailableBalance() != null) {
				accountDetail.put("accountBalance", numberTool.format("currency", account.getAvailableBalance()));
				accountDetail.put("accountNumberSuffix", ((account.getAccountNumberSuffix() != null && !account
						.getAccountNumberSuffix().equals("")) ? " " + account.getAccountNumberSuffix() : ""));
			} else if (account != null && account.getPortalBalance() != null) {
				accountDetail.put("accountBalance", numberTool.format("currency", account.getPortalBalance()));
				accountDetail.put("accountNumberSuffix", ((account.getAccountNumberSuffix() != null && !account
						.getAccountNumberSuffix().equals("")) ? " " + account.getAccountNumberSuffix() : ""));
			} else {
				accountDetail.put("accountBalance", "");
				accountDetail.put("accountNumberSuffix", ((account.getAccountNumberSuffix() != null && !account
						.getAccountNumberSuffix().equals("")) ? " " + account.getAccountNumberSuffix() : ""));
			}

			sb.append(StringUtils.left(accountDetail.get("accountName"), 15));
			sb.append(" ");
			sb.append(accountDetail.get("accountLast4Num"));

			if (!accountDetail.get("accountBalance").equals("")) {
				sb.append(" ");
				sb.append(accountDetail.get("accountBalance"));
			}

			if (!accountDetail.get("accountNumberSuffix").equals("")) {
				sb.append(" ");
				sb.append(accountDetail.get("accountNumberSuffix"));
			}

			accountDetail.put("text", sb.toString());
		}

		return accountDetail;
	}

	public static TVFrontendAccount getCardPlasticAccountByPANId(String id, List<TVFrontendAccount> cardPlasticAccounts) {
		if (cardPlasticAccounts != null) {
			for (Iterator<TVFrontendAccount> iterator = cardPlasticAccounts.iterator(); iterator.hasNext();) {
				TVFrontendAccount act = (TVFrontendAccount) iterator.next();

				if (act.getAccountNumber().matches("^\\d+$") && id.matches("^\\d+$")
						&& act.getAccountNumber().trim().length() < 20 && id.trim().length() < 20) {
					if (Long.parseLong(act.getAccountNumber().trim()) == Long.parseLong(id.trim())) {
						return act;
					}
				} else {
					if (act.getAccountNumber().trim().equals(id.trim())) {
						return act;
					}
				}
			}
		}

		return null;
	}

	public static String formatDate(String date) {
		String formattedDate = "";

		try {
			if (date != null && date.trim().length() > 0) {
				SimpleDateFormat sourceSdf = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat targetSdf = new SimpleDateFormat("MM/dd/yyyy");

				formattedDate = targetSdf.format(sourceSdf.parse(date));
			}
		} catch (Exception e) {
			logger.error("Invalid date [" + date + "]: ", e);
		}

		return formattedDate;
	}

	public static Calendar convertStringToCal(String date) {
		Calendar cal = new GregorianCalendar();

		try {
			if (date != null && date.trim().length() > 0) {

				SimpleDateFormat sourceSdf = new SimpleDateFormat("MM/dd/yyyy");
				SimpleDateFormat targetSdfYear = new SimpleDateFormat("yyyy");
				SimpleDateFormat targetSdfMonth = new SimpleDateFormat("MM");
				SimpleDateFormat targetSdfDay = new SimpleDateFormat("dd");

				cal.set(Integer.parseInt(targetSdfYear.format(sourceSdf.parse(date))),
						Integer.parseInt(targetSdfMonth.format(sourceSdf.parse(date))),
						Integer.parseInt(targetSdfDay.format(sourceSdf.parse(date))));
			}
		} catch (Exception e) {
			logger.error("Invalid date [" + date + "]: ", e);
			return null;
		}

		return cal;
	}

	public static Long getNumericPhoneNumber(String phoneNumber) {
		Long numericPhoneNumber = null;

		if (phoneNumber != null && phoneNumber.trim().length() > 0) {
			phoneNumber = phoneNumber.trim();

			phoneNumber = phoneNumber.replace(" ", "");
			phoneNumber = phoneNumber.replace("(", "");
			phoneNumber = phoneNumber.replace(")", "");
			phoneNumber = phoneNumber.replace("-", "");

			if (phoneNumber.length() == 10) {
				try {
					numericPhoneNumber = Long.parseLong(phoneNumber);
				} catch (Exception e) {
					logger.error("Unable to parse phone: [" + phoneNumber + "]", e);
				}
			}
		}

		return numericPhoneNumber;
	}

	public static String getStringNumericPhoneNumber(String phoneNumber) {
		String stringNumericPhoneNumber = null;

		Long numericPhoneNumber = getNumericPhoneNumber(phoneNumber);

		stringNumericPhoneNumber = getStringNumericPhoneNumber(numericPhoneNumber);

		return stringNumericPhoneNumber;
	}

	public static String getStringNumericPhoneNumber(Long phoneNumber) {
		String stringNumericPhoneNumber = null;

		if (phoneNumber != null) {
			try {
				stringNumericPhoneNumber = String.valueOf(phoneNumber);
			} catch (Exception e) {
				logger.error("Unable to parse phone: [" + phoneNumber + "]", e);
			}
		}

		return stringNumericPhoneNumber;
	}

	public static String formatPhoneNumber(String phoneNumber) {
		return formatPhoneNumber(getNumericPhoneNumber(phoneNumber));
	}

	public static String formatPhoneNumber(Long phoneNumber) {
		String formattedPhoneNumber = "";

		try {
			if (phoneNumber != null) {
				String stringPhoneNumber = phoneNumber.toString().trim();

				if (stringPhoneNumber != null && stringPhoneNumber.length() > 0) {
					if (stringPhoneNumber.length() == 10) {
						formattedPhoneNumber = "(" + stringPhoneNumber.substring(0, 3) + ") "
								+ stringPhoneNumber.substring(3, 6) + "-" + stringPhoneNumber.substring(6);
					} else {
						formattedPhoneNumber = stringPhoneNumber;
					}
				}
			}
		} catch (Exception e) {
			logger.error("Invalid phone number [" + phoneNumber + "]: ", e);
		}

		return formattedPhoneNumber;
	}

	public static String formatPhoneNumberDashes(Long phoneNumber) {
		String formattedPhoneNumber = "";

		try {
			if (phoneNumber != null) {
				String stringPhoneNumber = phoneNumber.toString().trim();
				stringPhoneNumber = stringPhoneNumber.replaceAll("-", "");
				if (stringPhoneNumber != null && stringPhoneNumber.length() > 0) {
					if (stringPhoneNumber.length() == 10) {
						formattedPhoneNumber = "(" + stringPhoneNumber.substring(0, 3) + ")" + "-"
								+ stringPhoneNumber.substring(3, 6) + "-" + stringPhoneNumber.substring(6);
					} else {
						formattedPhoneNumber = stringPhoneNumber;
					}
				}
			}
		} catch (Exception e) {
			logger.error("Invalid phone number [" + phoneNumber + "]: ", e);
		}

		return formattedPhoneNumber;
	}

	public static String formatPhoneNumberDashes(String phoneNumber) {
		String formattedPhoneNumber = "";

		try {
			if (phoneNumber != null) {
				String stringPhoneNumber = phoneNumber;
				stringPhoneNumber = stringPhoneNumber.replaceAll("-", "");
				if (stringPhoneNumber != null && stringPhoneNumber.length() > 0) {
					if (stringPhoneNumber.length() == 10) {
						formattedPhoneNumber = "(" + stringPhoneNumber.substring(0, 3) + ")" + "-"
								+ stringPhoneNumber.substring(3, 6) + "-" + stringPhoneNumber.substring(6);
					} else {
						formattedPhoneNumber = stringPhoneNumber;
					}
				}
			}
		} catch (Exception e) {
			logger.error("Invalid phone number [" + phoneNumber + "]: ", e);
		}

		return formattedPhoneNumber;
	}

	public static String formatPhoneNumberDashesOnly(String phoneNumber) {
		String formattedPhoneNumber = "";

		try {
			if (phoneNumber != null) {
				String stringPhoneNumber = phoneNumber;
				if (stringPhoneNumber.contains(" "))
					stringPhoneNumber = stringPhoneNumber.replace(" ", "");
				stringPhoneNumber = stringPhoneNumber.replace("-", "");
				if (stringPhoneNumber.contains("("))
					stringPhoneNumber = stringPhoneNumber.replace("(", "");
				if (stringPhoneNumber.contains(")"))
					stringPhoneNumber = stringPhoneNumber.replace(")", "");

				if (stringPhoneNumber != null && stringPhoneNumber.length() > 0) {
					if (stringPhoneNumber.length() == 10) {
						formattedPhoneNumber = stringPhoneNumber.substring(0, 3) + "-"
								+ stringPhoneNumber.substring(3, 6) + "-" + stringPhoneNumber.substring(6);
					} else {
						formattedPhoneNumber = stringPhoneNumber;
					}
				}
			}
		} catch (Exception e) {
			logger.error("Invalid phone number [" + phoneNumber + "]: ", e);
		}

		return formattedPhoneNumber;
	}

	public static String formatAmount(BigDecimal amount) {
		String formattedAmount = "$0.00";

		try {
			if (amount != null) {
				formattedAmount = NumberFormat.getCurrencyInstance().format(amount);
			}
		} catch (Exception e) {
			logger.error("Invalid amonut [" + (amount == null ? amount : amount.toString()) + "]: ", e);
		}

		return formattedAmount;
	}

	public static String formatPlasticNumber(String plasticNumber) {
		String formattedPlasticNumber = "";

		try {
			if (plasticNumber != null && plasticNumber.trim().length() > 0) {
				formattedPlasticNumber = "x"
						+ (plasticNumber.trim().length() > 4 ? plasticNumber.trim().substring(
								plasticNumber.trim().length() - 4) : plasticNumber.trim());
			}
		} catch (Exception e) {
			logger.error("Invalid plastic number [" + plasticNumber + "]: ", e);
		}

		return formattedPlasticNumber;
	}

	public static String getNumericPin(String pin) {
		String numericPin = null;

		String _pin = pin;

		if (_pin != null && _pin.trim().length() > 0) {
			_pin = _pin.trim();

			_pin = _pin.replace(" ", "");

			if (_pin.length() == 6) {
				try {
					Integer.parseInt(_pin);

					numericPin = _pin;
				} catch (Exception e) {
					logger.error("Unable to parse pin: [" + pin + "]", e);
				}
			}
		}

		return numericPin;
	}

	public static Map<String, String> getAccountText(TVFrontendAccount account) {
		Map<String, String> accountDetail = new LinkedHashMap<String, String>();

		if (account == null) {
			return null;
		} else {
			AccountMaskUtil mask = new AccountMaskUtil();
			NumberTool numberTool = new NumberTool();

			StringBuilder sb = new StringBuilder();

			accountDetail.put("accountName", account.getNickname());
			accountDetail.put("accountLast4Num", mask.mask(account.getAccountNumber()));
			accountDetail.put("accountSubtype", account.getSubtype());

			if (account != null && account.getSubtype().equals("CCA") && account.getAvailableBalance() != null) {
				accountDetail.put("accountBalance", numberTool.format("currency", account.getAvailableBalance()));
				accountDetail.put("accountNumberSuffix", ((account.getAccountNumberSuffix() != null && !account
						.getAccountNumberSuffix().equals("")) ? " " + account.getAccountNumberSuffix() : ""));
			} else if (account != null && account.getPortalBalance() != null) {
				accountDetail.put("accountBalance", numberTool.format("currency", account.getPortalBalance()));
				accountDetail.put("accountNumberSuffix", ((account.getAccountNumberSuffix() != null && !account
						.getAccountNumberSuffix().equals("")) ? " " + account.getAccountNumberSuffix() : ""));
			} else {
				accountDetail.put("accountBalance", "");
				accountDetail.put("accountNumberSuffix", ((account.getAccountNumberSuffix() != null && !account
						.getAccountNumberSuffix().equals("")) ? " " + account.getAccountNumberSuffix() : ""));
			}

			sb.append(StringUtils.left(accountDetail.get("accountName"), 15));
			sb.append(" ");
			sb.append(accountDetail.get("accountLast4Num"));

			if (!accountDetail.get("accountBalance").equals("")) {
				sb.append(" ");
				sb.append(accountDetail.get("accountBalance"));
			}

			if (!accountDetail.get("accountNumberSuffix").equals("")) {
				sb.append(" ");
				sb.append(accountDetail.get("accountNumberSuffix"));
			}

			accountDetail.put("text", sb.toString());
		}

		return accountDetail;
	}
	
	public static boolean isAthmCardAllowed(String cardNumber, String[] allowedCardsPrefixes)
	{
		if(cardNumber!= null && !cardNumber.equals("")
				&& allowedCardsPrefixes != null
				&& allowedCardsPrefixes.length >0){
			
			for(String allowedPrefix: allowedCardsPrefixes){
				if(cardNumber.length() >= allowedPrefix.length()){
					String cardPrefix = cardNumber.substring(0,allowedPrefix.length());
					if(cardPrefix.equals(allowedPrefix)){
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	public static boolean isAthmCardStatusAllowed(String cardStatus, String allowedAthmCardStatus)
	{
		if(cardStatus != null && !cardStatus.equals("")
				&& allowedAthmCardStatus != null && !allowedAthmCardStatus.equals("")){
			
			cardStatus = cardStatus.trim().toLowerCase();
			allowedAthmCardStatus = allowedAthmCardStatus.trim().toLowerCase();
			String[] conditions = allowedAthmCardStatus.split(":");
			if(conditions != null && conditions.length > 0){
				for(String condition:conditions){
					
					String[] valuesWithCondition = condition.split("-");
					if(valuesWithCondition != null && valuesWithCondition.length >1){
						String[] values = valuesWithCondition[1].split(",");
						
						if(condition.contains("startswith")){
							for(String status: values){
								if(cardStatus.startsWith(status)){
									return true;
								}
							}
						}else if(condition.contains("notallowed")){
							for(String status: values){
								if(cardStatus.contains(status)){
									return false;
								}
							}
						}else if(condition.contains("contains")){
							for(String status: values){
								if(cardStatus.contains(status)){
									return true;
								}
							}
						}else if(condition.contains("equals")){
							for(String status: values){
								if(cardStatus.equals(status)){
									return true;
								}
							}
						}
					}
				}
			}
			return false;
		}
		return false;
	}
}