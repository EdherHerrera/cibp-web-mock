package evertec.cibp.web.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

public class DateUtil {

	public String today() {
		return new Date().toString();
	}

	public String todayDay() {
		return new SimpleDateFormat("dd").format(new Date());
	}

	public String todayMonth() {
		return new SimpleDateFormat("MM").format(new Date());

	}

	public String todayYear() {
		return new SimpleDateFormat("yyyy").format(new Date());

	}

	public int year() {
		return Calendar.getInstance().get(Calendar.YEAR);
	}

	public int nextyear() {
		return Calendar.getInstance().get(Calendar.YEAR) + 1;
	}

	public String pad(String input) {

		return StringUtils.leftPad(input, 2, "0");

	}

	public String pad(int input) {

		return StringUtils.leftPad(String.valueOf(input), 2, "0");

	}

	public int nextTwoyears() {
		return Calendar.getInstance().get(Calendar.YEAR) + 2;
	}

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @param date
	 * @return
	 */
	public static boolean isBetweenDate(Date startDate, Date endDate, Date date) {
		Calendar fromDt = Calendar.getInstance();
		fromDt.setTime(startDate);
		Calendar toDt = Calendar.getInstance();
		toDt.setTime(endDate);
		Calendar dt = Calendar.getInstance();
		dt.setTime(date);

		boolean isBefore = dt.before(fromDt);
		boolean isAfter = dt.after(toDt);

		if (!isBefore && !isAfter)
			return true;
		else
			return false;
	}
	
	public static int compareOnlyDate(Date date1, Date date2){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Integer iDate1 = Integer.valueOf(sdf.format(date1));
		Integer iDate2 = Integer.valueOf(sdf.format(date2.getTime()));
		return iDate1.compareTo(iDate2);	
	}
}
