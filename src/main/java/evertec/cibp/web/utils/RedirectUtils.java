/**
 * 
 */
package evertec.cibp.web.utils;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;

/**
 * 
 * @author leandro.baleriani
 * @see OtherClass
 * @version 1.0
 * @since 1.0
 */
public class RedirectUtils {

	/**
	 *  Default constructor
	 */
	private RedirectUtils() {
		
	}
	
	/**
	 * 
	 * @param response
	 * @param strPath
	 * @throws AccessControlException
	 * @throws IOException
	 */
	public static void redirectToPath (HttpServletResponse response, String strPath) 
			throws AccessControlException, IOException {
		
		ESAPI.httpUtilities().sendRedirect(response, strPath);
	}

}
