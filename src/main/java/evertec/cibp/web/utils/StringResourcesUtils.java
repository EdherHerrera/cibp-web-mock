package evertec.cibp.web.utils;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.MessageSource;

/**
 * 
 * @author Evertec CIBP
 *
 */
public class StringResourcesUtils {
	
	public static String getStringEnglishResource(MessageSource messageSource, String resource) {
		return messageSource.getMessage(resource, null, new Locale("en"));
	}
	
	public static String getStringResource(MessageSource messageSource, HttpServletRequest request, String resource) {
		return messageSource.getMessage(resource, null, (Utils
				.getRSALanguage(request).equalsIgnoreCase("en") ? new Locale("en") : new Locale("es")));
	}

}
