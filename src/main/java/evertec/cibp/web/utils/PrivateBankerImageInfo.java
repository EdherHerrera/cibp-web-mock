package evertec.cibp.web.utils;

import javax.servlet.ServletContext;

import org.springframework.web.context.ServletContextAware;

public class PrivateBankerImageInfo implements ServletContextAware {
	private ServletContext servletCtx = null;
	private String imagePath = null;

	public void setServletContext(ServletContext servletContext) {
		// TODO Auto-generated method stub
		servletCtx = servletContext;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getImagePath() {
		return this.servletCtx.getRealPath("") + "/" + imagePath;
	}
}
