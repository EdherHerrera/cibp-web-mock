package evertec.cibp.web.utils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * An abstract class than help while converting LinkedHashMap<String, ?> object to AuthenticateResponse object 
 * 
 * @author Jorge De Los Santos
 * @since 1.0
 * @version 1.0
 * 
 * @see <a href="https://github.com/FasterXML/jackson-docs/wiki/JacksonMixInAnnotations">Jackson MixIn Annotations</a>
 */
public abstract class MessageVersionMixin {
	
	private String value;
	
	@JsonCreator
	public MessageVersionMixin(@JsonProperty("value") String value) {
		
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

}
