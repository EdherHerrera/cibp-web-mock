package evertec.cibp.web.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.rsa.csd.ws.EventType;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.ActionCode;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.DeviceRequest;

import edu.emory.mathcs.backport.java.util.Arrays;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.ATHEveryTransAlert;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.smsAlerts.SmsBean;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.AlertsFacade;
import evertec.cibp.web.form.bean.PurchasesAndWithdrawalsBean;
import evertec.cibp.web.session.UserSession;
import evertec.otherservices.alertsDataTypes.CCAAlertsContactsContactDataType;
import evertec.otherservices.alertsDataTypes.CCAAlertsContactsDataType;
import evertec.otherservices.alertsDataTypes.IACalertsInfoResDataType;
import evertec.otherservices.alertsDataTypes.IACalertsInfoResDataTypeCriteria;
import evertec.otherservices.alertsRequests.AddAlertsCustCriteriaReqType;

public class AlertTypesUtils {

	protected static final Log logger = LogFactory.getLog(AlertTypesUtils.class.getClass());

	static CustomerServices customerService;
	static TVFrontendAccount account;
	static CustomerProfile profile;
	static AccountMaskUtil maskutil;

	// TODO: When all alerts for CCA are implemented, re use the same code from
	// updateATHPurchasesAndWithdrawals. Develop a code that can be used for
	// both
	// type of alerts since they are the same.
	public static boolean purchasesAndWithdrawalsErrors(HttpServletRequest request, Map<String, Object> model,
			Object command) throws Exception {
		PurchasesAndWithdrawalsBean a = (PurchasesAndWithdrawalsBean) command;
		model.put(ChallengeInformationUtils.CCA_ACTION, a);
		boolean errors = false;
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		boolean hasMobileEntitlement = session.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING);
		String myRegExp = "(?:\\d*\\.)?\\d+";
		String alertsPurchaseError = "";

		String alertsEveryPurchaseError = "";
		String onlyDigits;
		String leaveBlank;
		String leaveBlankOrRegister;
		String amountRequired;
		String register;
		String minAmount;
		boolean MBCA627 = StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA627);

		if (GenericValidator.isBlankOrNull(a.getPurchase_thr_chksms()))
			a.setPurchase_thr_chksms("0");
		
		if (GenericValidator.isBlankOrNull(a.getPurchase_thr_chkemail()))
			a.setPurchase_thr_chkemail("0");

		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			onlyDigits = "Only digits greater than zero allowed.";
			leaveBlank = "Leave amount blank or select sms option.";
			amountRequired = "Amount required if selecting sms option.";
			leaveBlankOrRegister = "Leave amount blank or Enroll in SMS alerts.";
			register = "Must register cell phone number to receive sms alerts.";
			minAmount = "Minimum amount allowed is $" + a.getMinAmountForAlert();
		} else {
			onlyDigits = "Solo d\u00EDgitos mayor que cero son v\u00E1lidos.";
			leaveBlank = "Dejar cantidad en blanco o marcar opci\u00F3n sms.";
			amountRequired = "Cantidad requerida si marcas opci\u00F3n sms.";
			leaveBlankOrRegister = "Dejar cantidad en blanco o Inscr\u00EDbete en alertas SMS.";
			register = "Requerido registrar # celular para recibir alertas sms.";
			minAmount = "Cantidad minima permitida es $" + a.getMinAmountForAlert();
		}
		// "^[1-9][0-9]*$"
		// Feature Flag for History MBCA77 change in the regular Expresion to
		// allow decimal quantity
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA77)) {
			if (!GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
					&& !GenericValidator.matchRegexp(a.getAlertsPurchaseminamt(), myRegExp)) {

				alertsPurchaseError = onlyDigits;
				errors = true;
			}
			
			if(MBCA627) {				
				if (!GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
						&& (!a.getPurchase_thr_chksms().equalsIgnoreCase("0") || !a.getPurchase_thr_chkemail().equalsIgnoreCase("0"))
						&& !GenericValidator.isBlankOrNull(a.getMinAmountForAlert()) && Double
								.parseDouble(a.getAlertsPurchaseminamt()) < Double.parseDouble(a.getMinAmountForAlert())) {
					alertsPurchaseError = minAmount;
					errors = true;

				}
			}else {
				// Validate min Amount from CIBP properties MBCA-77
				if (!GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
						&& !a.getPurchase_thr_chksms().equalsIgnoreCase("0")
						&& !GenericValidator.isBlankOrNull(a.getMinAmountForAlert()) && Double
								.parseDouble(a.getAlertsPurchaseminamt()) < Double.parseDouble(a.getMinAmountForAlert())) {
					alertsPurchaseError = minAmount;
					errors = true;

				}
			}

		

		} else {
			if (!GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
					&& !GenericValidator.matchRegexp(a.getAlertsPurchaseminamt(), "^[1-9][0-9]*$")) {

				alertsPurchaseError = onlyDigits;
				errors = true;
			}
		}

		if (GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt()) && !hasMobileEntitlement) {
			alertsPurchaseError = register;
			errors = true;
		}
		if(MBCA627) {
			// "^[1-9][0-9]*$"
			// Feature Flag for History MBCA77 change in the regular Expresion to
			// allow decimal quantity
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA77)) {
				if (!GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
						&& GenericValidator.matchRegexp(a.getAlertsPurchaseminamt(), myRegExp)
						&& (a.getPurchase_thr_chksms().equalsIgnoreCase("0") && a.getPurchase_thr_chkemail().equalsIgnoreCase("0"))) {
					
					if (!alertsPurchaseError.equals(""))
						alertsPurchaseError += "<br>";

					if (hasMobileEntitlement)
						alertsPurchaseError += leaveBlank;
					else
						alertsPurchaseError += leaveBlankOrRegister;

					errors = true;
				}
			} else {
				if (!GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
						&& GenericValidator.matchRegexp(a.getAlertsPurchaseminamt(), "^[1-9][0-9]*$")
						&& (a.getPurchase_thr_chksms().equalsIgnoreCase("0") || a.getPurchase_thr_chkemail().equalsIgnoreCase("0"))) {
					if (!alertsPurchaseError.equals(""))
						alertsPurchaseError += "<br>";

					if (hasMobileEntitlement)
						alertsPurchaseError += leaveBlank;
					else
						alertsPurchaseError += leaveBlankOrRegister;

					errors = true;
				}
			}
		}else {
			// "^[1-9][0-9]*$"
			// Feature Flag for History MBCA77 change in the regular Expresion to
			// allow decimal quantity
			if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA77)) {
				if (!GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
						&& GenericValidator.matchRegexp(a.getAlertsPurchaseminamt(), myRegExp)
						&& a.getPurchase_thr_chksms().equalsIgnoreCase("0")) {
					if (!alertsPurchaseError.equals(""))
						alertsPurchaseError += "<br>";

					if (hasMobileEntitlement)
						alertsPurchaseError += leaveBlank;
					else
						alertsPurchaseError += leaveBlankOrRegister;

					errors = true;
				}
			} else {
				if (!GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
						&& GenericValidator.matchRegexp(a.getAlertsPurchaseminamt(), "^[1-9][0-9]*$")
						&& a.getPurchase_thr_chksms().equalsIgnoreCase("0")) {
					if (!alertsPurchaseError.equals(""))
						alertsPurchaseError += "<br>";

					if (hasMobileEntitlement)
						alertsPurchaseError += leaveBlank;
					else
						alertsPurchaseError += leaveBlankOrRegister;

					errors = true;
				}
			}
		}

		if(MBCA627) {
			if (GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
					&& (a.getPurchase_thr_chksms().equalsIgnoreCase("1") || a.getPurchase_thr_chkemail().equalsIgnoreCase("1"))) {
				if (!alertsPurchaseError.equals(""))
					alertsPurchaseError += "<br>";

				alertsPurchaseError += amountRequired;
				errors = true;
			}
		}else {
			if (GenericValidator.isBlankOrNull(a.getAlertsPurchaseminamt())
					&& (a.getPurchase_thr_chksms().equalsIgnoreCase("1"))) {
				if (!alertsPurchaseError.equals(""))
					alertsPurchaseError += "<br>";

				alertsPurchaseError += amountRequired;
				errors = true;
			}
		}

		if (!alertsPurchaseError.equals(""))
			model.put("alertsPurchaseError", alertsPurchaseError);

		if (!alertsEveryPurchaseError.equals(""))
			model.put("alertsEveryPurchaseError", alertsEveryPurchaseError);

		return errors;

	}

	public static Map<String, ATHEveryTransAlert> getAthEveryTransAlertMap(CustomerServices customerService, String profileId){

		Map<String, ATHEveryTransAlert> map = new HashMap<>();
		List<ATHEveryTransAlert> athEveryTransAlertList = customerService.getATHEveryTransAlertsProfilesByProfileId(profileId);
		if(athEveryTransAlertList != null && !athEveryTransAlertList.isEmpty()) {
			for(ATHEveryTransAlert alert: athEveryTransAlertList) {
				map.put(alert.getAccountNumber().concat(alert.getCardNumber()), alert);
			}
		}
		return map;
	}

	/**
	 * Method to remove record from the ATH_EVERY_TRANS_ALERTS table
	 * @param customerService The customer service interface
	 * @param theProfile The customer profile
	 * @param smsBean The SMS bean
	 * @param logger Log4J object
	 * @return True if record deleted. False otherwise.
	 */
	public static boolean removeAlertEveryTransactionProfileById(CustomerServices customerService, CustomerProfile theProfile, SmsBean smsBean, Logger logger){
		
		boolean removed = false;
		String errorMessage = "";
		try {
		
			Map<String, ATHEveryTransAlert> athEveryTransAlertMap = getAthEveryTransAlertMap(customerService, theProfile.getProfileId());
			String key = (smsBean.getAcctNum()+smsBean.getCardNum());
			if(athEveryTransAlertMap.containsKey(key)) {
				removed = AlertsFacade.getInstance().removeAlertEveryTransactionProfile(athEveryTransAlertMap.get(key).getId(), customerService);
			}
			
		}catch(Exception e) {
			removed = false;
			errorMessage = "\n error : "+ ExceptionUtils.getFullStackTrace(e);
		}
		
		if(!removed) {
			logger.error("Could not remove ATHEveryTransaction Record. ProfileId: "
					+ theProfile.getProfileId()
					+ "\nacct: "
					+ maskutil.mask(smsBean.getAcctNum())
					+ "\ncard: " + maskutil.mask(smsBean.getCardNum()) + "\n" + theProfile.getFullName() + errorMessage);
		}
		
		return removed;
	}

	public static void rsaChallengeForAlertController(HttpServletRequest request, Object command, BindException errors,
			String alertController) throws Exception {
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		PurchasesAndWithdrawalsBean a = (PurchasesAndWithdrawalsBean) command;
		if (a.getAction() != null && a.getAction().equals("2")) {
			Boolean informationChallenge = session.isInformationChallenge();
			if (informationChallenge != null && !informationChallenge && (session.getGuidedSetupSession() == null
					|| !session.getGuidedSetupSession().isGuidedSetupMode())) {

				errors.addError(null);
				String language = Utils.getRSALanguage(request);
				String username = session.getUsername();

				if (StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB) && session.getOobEnroll()) {
					session.setInformationChallenge(session.isOobAuthenticated());

				} else if (session.getInformationQuestion() == null) {

					DeviceRequest deviceRequest = RSAUtils.getDeviceRequest(session.getPm_fp(),
							session.getDeviceRequest().getDeviceTokenCookie(), request);
					/**
					 * MBNA-1955
					 * Method rsaAnalyze : CIBP-WEB microservice integration
					 *
					 */
					//Microservice implementation
					RSAAnalyzeRequest rsaAnalyzeRequest = new RSAAnalyzeRequest(deviceRequest, language, username, EventType.SESSION_SIGNIN.getValue());
					AnalyzeResponse anResp = session.getCustomerService().rsaAnalyze(rsaAnalyzeRequest);

					deviceRequest.setDeviceTokenCookie(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());
					deviceRequest.setDeviceTokenFSO(anResp.getDeviceResult().getDeviceData().getDeviceTokenCookie());

					session.setRsaSessionId(anResp.getIdentificationData().getSessionId());
					session.setRsaTransactionId(anResp.getIdentificationData().getTransactionId());

					session.setDeviceRequest(deviceRequest);

					if (ActionCode.CHALLENGE != anResp.getRiskResult().getTriggeredRule().getActionCode()) {
						session.setInformationChallenge(true);
					} else {
						String email = null;
						if (session.getCustomerProfile() != null)
							email = session.getCustomerProfile().getEmail();

						QuestionResponse personalQuestion;

						if (session.getInformationQuestion() == null) {
							RSAActionRequest actionRequest = new RSAActionRequest(
									Utils.generateCustomerInteractionEvent(request, session.getUsername()),
									session.getUsername(), "", email, 0, session.isRemember(),
									session.getDeviceRequest());

							if (session.getCustomerProfile() != null)
								actionRequest.setUsername(session.getCustomerProfile().getUsername());
							else if (session.getUsernameReservation() != null)
								actionRequest.setUsername(session.getUsernameReservation().getUsername());
							else
								actionRequest.setUsername(username);

							actionRequest.setDeviceRequest(session.getDeviceRequest());
							actionRequest.setRsaSessionId(session.getRsaSessionId());
							actionRequest.setRsaTransactionId(session.getRsaTransactionId());

							actionRequest.setRsaLang(Utils.getRSALanguage(request));

							personalQuestion = session.getCustomerService().getSignOnCustomerServiceModule().getRSAPersonalQuestion(actionRequest);

						} else {
							personalQuestion = new QuestionResponse();
							personalQuestion.setStatusResult(ResultStatus.SUCCESS);
							personalQuestion.setQuestion(session.getInformationQuestion());
						}
						if (ResultStatus.SUCCESS.equals(personalQuestion.getStatusResult())) {
							session.setInformationQuestion(personalQuestion.getQuestion());

						} else if (ResultStatus.ERROR.equals(personalQuestion.getStatusResult())) {
							throw new ModelAndViewDefiningException(new ModelAndView("error"));
						} else if (ResultStatus.BLOCKED.equals(personalQuestion.getStatusResult())) {
							throw new ModelAndViewDefiningException(new ModelAndView("forward:login"));
						}
					}

				}

				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.CHALLENGE_USER, true);
				Map<String, Object> parameters = new HashMap<>();

				if ("CCA".equalsIgnoreCase((alertController))) {
					parameters.put(ChallengeInformationUtils.CCA_ACTION, command);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
							ChallengeInformationUtils.CCA_ACTION);
				} else {
					parameters.put(ChallengeInformationUtils.ATH_ACTION, command);
					WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION,
							ChallengeInformationUtils.ATH_ACTION);
				}

				WebUtils.setSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS, parameters);
			}
		}
	}

	public static String getSuccessStatusMessage(HttpServletRequest request) {
		String statusMessage;
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			statusMessage = "Profile Updated";
		} else {
			statusMessage = "Perfil actualizado";
		}
		return statusMessage;
	}

	public static String getErrorStatusMessage(HttpServletRequest request) {
		String statusMessage;
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			statusMessage = "Unable to Update Profile.";
		} else {
			statusMessage = "No se pudo actualizar.";
		}
		return statusMessage;
	}

	public static String getDeletingErrorProfileNotFoundStatusMessage(HttpServletRequest request) {
		String statusMessage;
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			statusMessage = "Unable to delete. Profile not found.";
		} else {
			statusMessage = "No se pudo Borrar. Perfil no encontrado.";
		}
		return statusMessage;
	}

	public static String getDeletingErrorStatusMessage(HttpServletRequest request) {
		String statusMessage;
		if (Utils.getRSALanguage(request).equalsIgnoreCase("en")) {
			statusMessage = "Error deleting.";
		} else {
			statusMessage = "Error borrando.";
		}
		return statusMessage;
	}

	public static boolean isMobileBanking(UserSession session) {
		boolean mobileBanking;
		if (session.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)) {
			mobileBanking = true;
		} else
			mobileBanking = false;

		return mobileBanking;
	}

	public static IACalertsInfoResDataType[] convertToIACalertsInfo(AddAlertsCustCriteriaReqType custRequest) {
		IACalertsInfoResDataType[] iaCalertsInfoResDataType = new IACalertsInfoResDataType[1];
		IACalertsInfoResDataType alertsInfo = new IACalertsInfoResDataType();

		alertsInfo.setAlertActive(true);

		if (custRequest.getEnrolledNbr() != null) {
			alertsInfo.setEnrolledNbr(custRequest.getEnrolledNbr());
		}
		if (custRequest.getAlertID() != null) {
			alertsInfo.setAlertID(custRequest.getAlertID());
		}
		if (custRequest.getDesc() != null) {
			alertsInfo.setAlertDesc(custRequest.getDesc());
		}
		if (custRequest.getStatus() != null) {
			alertsInfo.setStatus(custRequest.getStatus());
		}
		if (custRequest.getContacts() != null) {
			CCAAlertsContactsDataType contacts = new CCAAlertsContactsDataType();

			if (custRequest.getContacts().getContact() != null) {
				contacts.setContact(
						new CCAAlertsContactsContactDataType[custRequest.getContacts().getContact().length]);

				for (int j = 0; j < custRequest.getContacts().getContact().length; j++) {
					CCAAlertsContactsContactDataType contact = new CCAAlertsContactsContactDataType();

					if (custRequest.getContacts().getContact(j).getID() != null) {
						contact.setID(custRequest.getContacts().getContact(j).getID());
					}
					if (custRequest.getContacts().getContact(j).getX_200() != null) {
						contact.setX_200(custRequest.getContacts().getContact(j).getX_200());
					}
					if (custRequest.getContacts().getContact(j).get_any() != null) {
						contact.set_any(custRequest.getContacts().getContact(j).get_any());
					}

					contacts.setContact(j, contact);
				}
			}
			if (custRequest.getContacts().getContactIDs() != null) {
				contacts.setContactIDs(
						new CCAAlertsContactsContactDataType[custRequest.getContacts().getContactIDs().length]);

				for (int j = 0; j < custRequest.getContacts().getContactIDs().length; j++) {
					CCAAlertsContactsContactDataType contactIDs = new CCAAlertsContactsContactDataType();

					if (custRequest.getContacts().getContactIDs(j).getID() != null) {
						contactIDs.setID(custRequest.getContacts().getContactIDs(j).getID());
					}
					if (custRequest.getContacts().getContactIDs(j).getX_220() != null) {
						contactIDs.setX_220(custRequest.getContacts().getContactIDs(j).getX_220());
					}
					if (custRequest.getContacts().getContactIDs(j).get_any() != null) {
						contactIDs.set_any(custRequest.getContacts().getContactIDs(j).get_any());
					}

					contacts.setContactIDs(j, contactIDs);
				}
			}

			if (custRequest.getContacts().getX_200() != null) {
				contacts.setX_200(custRequest.getContacts().getX_200());
			}
			if (custRequest.getContacts().getX_220() != null) {
				contacts.setX_220(custRequest.getContacts().getX_220());
			}
			if (custRequest.getContacts().get_any() != null) {
				contacts.set_any(custRequest.getContacts().get_any());
			}

			alertsInfo.setContacts(contacts);
		}
		if (custRequest.getCriteria() != null) {
			IACalertsInfoResDataTypeCriteria criteria = new IACalertsInfoResDataTypeCriteria();

			if (custRequest.getCriteria().getCompareValues() != null) {
				criteria.setCompareValues(custRequest.getCriteria().getCompareValues());
			}

			alertsInfo.setCriteria(criteria);
		}

		iaCalertsInfoResDataType[0] = alertsInfo;
		return iaCalertsInfoResDataType;
	}

	public static List<TVFrontendAccount> getCCAAccountsListForAlertsFiltered(UserSession userSession, String filterComercial, String filterRetail, String filterCode, String filterNTIPCode) {
		List<TVFrontendAccount> ccaAccs = userSession.getCcaAccounts();

		// Feature Flag for filters
		if (StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA78)) {
			// Check if the user is comercial.
			if (userSession.isComercialCustomer()) {
				if (filterComercial.equalsIgnoreCase("ENABLED")) {
					// filtrar para comercial
					ccaAccs.removeAll(filterCreditCardByProperty(ccaAccs, filterCode));

				}
			} else if (!userSession.isComercialCustomer()
					&& filterRetail.equalsIgnoreCase("ENABLED")) {
				// filtrar para retail
				ccaAccs.removeAll(filterCreditCardByProperty(ccaAccs, filterCode));
			}
		}
		
		if(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA183)){
			ccaAccs.removeAll(filterCreditCardNTIPCode(ccaAccs, filterNTIPCode));
		}
		
		return ccaAccs;
	}
	
	private static List<TVFrontendAccount> filterCreditCardByProperty(List<TVFrontendAccount> ccaAccs, String filterCode) {
		List<TVFrontendAccount> ccaAccsFiltered = new ArrayList<>();
		
		// AGL: MBCA-78 filter Credit Card
		String[] removedCC = filterCode.split(",");

		for (String cc : removedCC) {
			for (TVFrontendAccount item : ccaAccs) {
				if (item.getAccountKey().substring(0, 6).equalsIgnoreCase(cc))

				{
					// Crear Arreglo Nuevo
					ccaAccsFiltered.add(item);

				}
			}
		}

		return ccaAccsFiltered;
	}
	
	/**
	 * @author et56802 Hector C. Ortiz Colon
	 * 
	 * This method returns a list of credit cards with status code in tsysAlertsCreditCardFilterNTIPCode  
	 * 
	 * @param ccaAccs List<TVFrontEndAccount> of credit cards.
	 * @return List<TVFrontEndAccount> of credit cards with status code in tsysAlertsCreditCardFilterNTIPCode.
	 */
	private static List<TVFrontendAccount> filterCreditCardNTIPCode(List<TVFrontendAccount> ccaAccs, String filterNTIPCode) {
		
		List<TVFrontendAccount> ccaAccsFiltered = new ArrayList<>();
		
		@SuppressWarnings("unchecked")
		List<String> statusCodes = Arrays.asList(filterNTIPCode.split(","));
		
		//Hector C. Ortiz MBCA-183 filter CCA with status codes in List<String> statusCodes.
		for (TVFrontendAccount acc : ccaAccs) {
			if (statusCodes.contains(acc.getStatusCode())){
				ccaAccsFiltered.add(acc);
			}
		}
		return ccaAccsFiltered;
	}


}
