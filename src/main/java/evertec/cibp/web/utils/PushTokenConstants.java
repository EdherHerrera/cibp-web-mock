package evertec.cibp.web.utils;

public class PushTokenConstants {

	public static final String PUSH_TOGGLE_DISABLED = "Push Notifications toggle was disabled by user";
	public static final String DEVICE_REMOVAL = "Push device was removed via Device Management";
	
	/** DEVICE_MODEL_LENGTH **/
	public static final int DEVICE_MODEL_LENGTH = 40;
	
	/** DEVICE_NAME_LENGTH **/
	public static final int DEVICE_NAME_LENGTH = 60;

	/**
	 * PushTokenConstants
	 */
	 private PushTokenConstants() {
		// No-op; won't be called
	 }
}
