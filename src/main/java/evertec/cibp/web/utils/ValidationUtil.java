package evertec.cibp.web.utils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.GenericValidator;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.enrollment.EnrollableAccount;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.PersonalInformationValidationRequest;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse;
import evertec.cibp.core.services.responses.PersonalInformationValidationResponse.EnrollmentValidationStatus;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.UserSession;

public class ValidationUtil
{
	/**
	 * regular expresion for numbers with or without commas ex. 1,324,234 or 3245323
	 */
	public static final String NUM_W_OR_WO_COMMA = "(\\$)?([1-9]{1}[0-9]{0,2})(\\,\\d{3})*$|^(\\$)?([1-9]{1}[0-9]{0,2})(\\d{3})*$";

	/**
	 * Constant keys for the map return by validMLAFields
	 */
	public static final String ORIG_YEAR = "originationYear";
	public static final String PAY_AMNT = "paymentAmount";

	/**
	 * This class is not meant to be instantiated, the 
	 * private constructor is to hide the the implicit 
	 * public one.
	 */
	private ValidationUtil(){}

	/**
	 * This method validates if the account number entered is valid or null
	 * depending on the account type that is sent.
	 * 
	 * Feel free to add different account type validations.
	 * 
	 * @param accountType Ex. CCA, IDA, MLA
	 * @param accountNum Account Number
	 * @return true if the account number and account type combination are valid.
	 * @author Hector Ortiz ET56802
	 */
	public static boolean validAccountNumber(String accountType, String accountNum) {
		if(accountType == null)
			return false;

		if(Utils.MLA.equalsIgnoreCase(accountType)) {
			return !GenericValidator.isBlankOrNull(accountNum) && GenericValidator.matchRegexp(accountNum, "\\d{10}");
		}
		else if (Utils.RVA.equalsIgnoreCase(accountType) || Utils.LEA.equalsIgnoreCase(accountType)) {
			return !GenericValidator.isBlankOrNull(accountNum) && GenericValidator.matchRegexp(accountNum, "\\d{17}");
		}
		else {
			return !GenericValidator.isBlankOrNull(accountNum) && GenericValidator.matchRegexp(accountNum, "\\d+");
		}
	}

	/**
	 * Verifies if user input for MLA fields origination year 
	 * and payment amount are null or invalid.
	 * 
	 * @param originationYear Year that the loan was made
	 * @param paymentAmount Monthly, biweekly, etc... payment
	 * @return returns a map with keys being the name of the field and value is true if it's valid
	 * 
	 * @author Hector Ortiz ET56802
	 */
	public static Map<String, Boolean> validMLAFieldsMap(Integer originationYear, BigDecimal paymentAmount)
	{
		Map <String, Boolean> errorsMap = new HashMap<>();
		errorsMap.put(ORIG_YEAR, validOriginationYear(originationYear));
		errorsMap.put(PAY_AMNT, validPaymentAmount(paymentAmount));
		return errorsMap;
	}

	/**
	 * Verifies if user input for MLA fields origination year 
	 * and payment amount are null or invalid.
	 * 
	 * @param originationYear Year that the loan was made
	 * @param paymentAmount Monthly, biweekly, etc... payment
	 * @return Returns true if all fields are valid.
	 * 
	 * @author Hector Ortiz ET56802
	 */
	public static boolean validLoanFields(Integer originationYear, BigDecimal paymentAmount)
	{
		return validOriginationYear(originationYear) && validPaymentAmount(paymentAmount);
	}

	/**
	 * Verifies if origination year is null or doesn't have 4 numbers.
	 * 
	 * This method is meant for cases where only origination year is
	 * being validated, if origination year is being validated 
	 * with payment amount look at validMLAFields.
	 * 
	 * @param originationYear Year that the loan was made
	 * @return true if originationYear is valid
	 * @author Hector Ortiz ET56802
	 */
	public static boolean validOriginationYear(Integer originationYear)
	{
		return originationYear != null && originationYear.toString().matches("\\d{4}");
	}

	/**
	 * Verifies if payment amount is null or doesn't match regular expression.
	 * 
	 * This method is meant for cases where only payment amount is
	 * being validated, if origination year is being validated 
	 * with payment amount look at validMLAFields.
	 * 
	 * @param paymentAmount Monthly, biweekly, etc... payment
	 * @return true if paymentAmount is valid
	 * @author Hector Ortiz ET56802
	 */
	public static boolean validPaymentAmount(BigDecimal paymentAmount)
	{
		return paymentAmount != null && paymentAmount.toPlainString().matches(NUM_W_OR_WO_COMMA);
	}

	/**
	 * Verifies if the CVV is null or doesn't match regular expression.
	 * 
	 * @param cvv CCA or AMEX cvv
	 * @return true if cvv is valid
	 * @author Hector Ortiz ET56802
	 */
	public static boolean validCVV(String cvv)
	{
		return !GenericValidator.isBlankOrNull(cvv) && GenericValidator.matchRegexp(cvv, "\\d{3}");
	}

	/**
	 * Verifies if the zip code is null or doesn't match regular expression.
	 * 
	 * @param zipCode CCA or AMEX zip code
	 * @return true if zip code is valid
	 * @author Hector Ortiz ET56802
	 */
	public static boolean validZipCode(String zipCode)
	{
		return !GenericValidator.isBlankOrNull(zipCode) && GenericValidator.matchRegexp(zipCode, "\\d{5}");
	}

	/**
	 * Verifies if the expiration year is null or doesn't match regular expression.
	 * 
	 * @param expYear CCA or AMEX expiration year
	 * @return true if expiration year is valid
	 * @author Hector Ortiz ET56802
	 */
	public static boolean validExpirationYear(String expYear)
	{
		return !GenericValidator.isBlankOrNull(expYear) && GenericValidator.matchRegexp(expYear, "\\d{4}");
	}

	/**
	 * Verifies if the expiration month is null or between 1 and 12 (inclusive)
	 * 
	 * @param expMonth CCA or AMEX expiration year
	 * @return true if expiration month is valid
	 * @author Hector Ortiz ET56802
	 */
	public static boolean validExpirationMonth(String expMonth)
	{
		int month = -1;
		try
		{
			month = Integer.parseInt(expMonth);
		}
		catch(Exception e)
		{
			return false;
		}
		return !GenericValidator.isBlankOrNull(expMonth) && month > 0 && month < 13;
	}

	/**
	 * Verifies if the PIN is null or doesn't match regular expression.
	 * 
	 * @param pin AMEX, IDA and PFC PIN
	 * @return true if pin is valid
	 * @author Hector Ortiz ET56802
	 * @author Stephanie Diaz ET56509
	 */
	public static boolean validPin(String pin)
	{
		return !GenericValidator.isBlankOrNull(pin) && GenericValidator.matchRegexp(pin, "\\d{4}");
	}


	/**
	 * Verifies if the ATH number is null or doesn't match regular expression.
	 * 
	 * @param ATH number
	 * @return true if ATHNumber is valid
	 * @author Stephanie Diaz ET56509
	 */
	public static boolean validATHNumber(String athNumber)
	{
		return !GenericValidator.isBlankOrNull(athNumber) && GenericValidator.matchRegexp(athNumber, "\\d{16}");
	}

	/**
	 * Verifies if the Loan Number is null or doesn't match regular expression.
	 * 
	 * @param accountNumber
	 * @return true if account number is valid
	 * @author Stephanie Diaz ET56509
	 */
	public static boolean validAccountNumber(String accountNumber)
	{
		return !GenericValidator.isBlankOrNull(accountNumber) && GenericValidator.matchRegexp(accountNumber, "\\d{9}");
	}

	/**
	 * Verifies if the customer has multiple profiles
	 *  where the user has a profile enrolled with the same account number
	 * 
	 * @param  profile accountNumber
	 * @return true if has multiple profiles
	 * @author Stephanie Diaz ET56509
	 */
	public static boolean hasAlreadyEnrolledProfile(HttpServletRequest request,CustomerProfile profile, String accountNumber) {
		UserSession session = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		CustomerServices customerService = session.getCustomerService();
		Calendar birthDate = Calendar.getInstance();
		birthDate.setTime(profile.getBirthDate());

		PersonalInformationValidationRequest personalRequest = new PersonalInformationValidationRequest(
				null, profile.getTaxId(), birthDate, false, false);
		personalRequest.setValidateMultipleProfiles(true);
		PersonalInformationValidationResponse customerEnrollment = customerService
				.isEnrollableCustomer(personalRequest);
		EnrollmentValidationStatus status =customerEnrollment.getStatus();
		List<EnrollableAccount> existingAccounts = customerEnrollment.getExistingAccounts();

		if ((existingAccounts != null && !existingAccounts.isEmpty()) || status == EnrollmentValidationStatus.ALREADY_ENROLL) {
			for (Iterator<EnrollableAccount> iterator = existingAccounts.iterator(); iterator.hasNext();) {
				EnrollableAccount name = iterator.next();	

				//!name.getProductId().equals("")
				if(!GenericValidator.isBlankOrNull(name.getAccountId())){
					if (name.getAccountId().trim().equalsIgnoreCase(accountNumber) && !GenericValidator.isBlankOrNull(name.getProductId())) {
						return true;	
					} 
				}
			}
		}
		return false;
	}
	
	/**
	 * Verifies if the agreement was checked
	 * 
	 * @param agreement
	 * @return true if agreement is valid
	 * @author Stephanie Diaz ET56509
	 */
	public static boolean validAgreement (String agreement){
		return !GenericValidator.isBlankOrNull(agreement) && agreement.equalsIgnoreCase("agree");
	}
	
	
	
}


