package evertec.cibp.web.utils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.validator.GenericValidator;

import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.UnicaApplication;
import evertec.cibp.web.session.UserSession;
import evertec.otherservices.beans.unica.UnicaApplicationInfo;
import evertec.otherservices.responses.UnicaInqResponse;

/**
 * UnicaUtils
 * @author et55596
 * @since 1.2
 * @version 1.0 
 */
public class UnicaUtils {
	
	private static final String PROPERTY_DELIMITER = ",";
	private static final String PROPERTY_KEY_VALUE_SEPARATOR = "-";
	
	/**
	 * @param userSession
	 * @param profile
	 * @return
	 */
	public static List<UnicaApplication> getApplications(UserSession userSession, String unicaRefresh, String unicaAllowedStatus, 
			String unicaLoanProducts, String unicaCreditProducts, String marketplaceAllowedStatus, String marketplaceProducts) {;
		List<UnicaApplication> unicaApplicationsList = userSession.getUnicaApplications();
		List<UnicaApplicationInfo> creditRequestList = null;
		boolean refresh = "true".equalsIgnoreCase(unicaRefresh);
		
		// Gets customer Unica applications
		if (shouldRetrieveApplications(userSession, refresh)) {
			UnicaInqResponse unicaResponse = userSession.getCustomerService().getUnicaBySSN(userSession.getCustomerProfile().getTaxId());
			
			if (isUnicaResponseValid(unicaResponse)) {
				
				if (unicaResponse.getUnicaInq().getLookupResponse() != null && 
						unicaResponse.getUnicaInq().getLookupResponse().length > 0 ) {
					creditRequestList = Arrays.asList(unicaResponse.getUnicaInq().getLookupResponse());
				} else if (NumberUtils.isParsable(unicaResponse.getUnicaInq().getResponseCode()) 
						&& Integer.parseInt(unicaResponse.getUnicaInq().getResponseCode()) == 0) {
					CustomerFacade.getInstance().removeCustomerFlag(userSession.getCustomerProfile(), CustomerFlagType.PENDING_UNICA_APPLICATION, userSession);
				}
				
			}

			if (creditRequestList != null) {
				unicaApplicationsList = new LinkedList<UnicaApplication>();
				
				getApplicationsWithAllowedStatus(unicaApplicationsList, creditRequestList,
						Arrays.stream(unicaAllowedStatus.split(PROPERTY_DELIMITER)).map(s -> s.split(PROPERTY_KEY_VALUE_SEPARATOR)).collect(Collectors.toMap(s -> s[0], s -> s[1])),
						Arrays.asList(unicaLoanProducts.split(PROPERTY_DELIMITER)), 
						Arrays.asList(unicaCreditProducts.split(PROPERTY_DELIMITER)),
						Arrays.stream(marketplaceAllowedStatus.split(PROPERTY_DELIMITER)).map(s -> s.split(PROPERTY_KEY_VALUE_SEPARATOR)).collect(Collectors.toMap(s -> s[0], s -> s[1])),
						Arrays.asList(marketplaceProducts.split(PROPERTY_DELIMITER)));
			}
		}
		
		userSession.setUnicaApplications(Optional.ofNullable(unicaApplicationsList).orElse(new LinkedList<UnicaApplication>()));
		
		return userSession.getUnicaApplications();
	}

	/**
	 * @param userSession
	 * @param refresh
	 * @return
	 */
	private static boolean shouldRetrieveApplications(UserSession userSession, boolean refresh) {
		return (userSession.getUnicaApplications() == null || refresh) 
				&& !GenericValidator.isBlankOrNull(userSession.getCustomerProfile().getTaxId()) 
				&& userSession.getCustomerProfile().hasFlag(CustomerFlagType.PENDING_UNICA_APPLICATION);
	}

	/**
	 * @param unicaResponse
	 * @return
	 */
	private static boolean isUnicaResponseValid(UnicaInqResponse unicaResponse) {
		return unicaResponse != null && unicaResponse.getResponseStatus() != null
				&& unicaResponse.getResponseStatus().equals(evertec.otherservices.enums.ResponseStatus.SUCCESS)
				&& unicaResponse.getDatapowerStatus() != null
				&& "00".equals(unicaResponse.getDatapowerStatus().getStatusCode())
				&& unicaResponse.getUnicaInq() != null;
	}

	/**
	 * @param unicaApplicationsList
	 * @param creditRequestList
	 * @param unicaAllowedStatus
	 * @param unicaLoanProducts
	 * @param unicaCreditProducts
	 */
	private static void getApplicationsWithAllowedStatus(List<UnicaApplication> unicaApplicationsList,
			List<UnicaApplicationInfo> creditRequestList, Map<String, String> unicaAllowedStatus, List<String> unicaLoanProducts,
			List<String> unicaCreditProducts, Map<String, String> marketplaceAllowedStatus, List<String> marketplaceProducts) {
		
		for (UnicaApplicationInfo appInfo : creditRequestList) {
			
			UnicaApplication application = null;
			String appStatus = appInfo.getApplStatusDesc().toLowerCase();
			Boolean isMarketplace = marketplaceProducts.contains(appInfo.getProductCode());
			
			if (isMarketplace) {
				if (marketplaceAllowedStatus.containsKey(appStatus)) {
					appInfo.setApplStatusDesc(marketplaceAllowedStatus.get(appStatus));
					application = new UnicaApplication();
					application.setUnicaApplicationInfo(appInfo);
					application.setFromMarketplace(true);
					application.setCredit(true);
				}
				
			} else {
				Boolean isUnicaCredit = unicaCreditProducts.contains(appInfo.getProductCode());
				Boolean isUnicaLoan = unicaLoanProducts.contains(appInfo.getProductCode());
				
				if (unicaAllowedStatus.containsKey(appStatus)) {
					appInfo.setApplStatusDesc(unicaAllowedStatus.get(appStatus));
					application = new UnicaApplication();
					application.setUnicaApplicationInfo(appInfo);
					application.setCredit(isUnicaCredit);
					
					if (isUnicaCredit) {
						application.setProductNameVariable("unica.product.creditcard");
					} else if (isUnicaLoan) {
						application.setProductNameVariable("unica.product.personalloan");
					} else {
						application.setProductNameVariable("unica.product.undefined");
					}
				}
			}
			
			if (application != null) {
				unicaApplicationsList.add(application);
			}
		}
	}

}
