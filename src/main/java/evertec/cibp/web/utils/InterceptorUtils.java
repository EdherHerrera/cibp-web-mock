/**
 * 
 */
package evertec.cibp.web.utils;

import java.util.HashSet;
import java.util.Set;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.StaticMessageSource;

/**
 * 
 * @author leandro.baleriani
 * @see OtherClass
 * @since 1.0
 * @version 1.0
 */
public class InterceptorUtils {
    
    /**
     * Default constructor
     */
    private InterceptorUtils () {
    	
    }
	
	/**
	 * check Allowed Views
	 * @param strViewName
	 * @param profile
	 * @return
	 */
	public static boolean checkAllowedViews (String strViewName, CustomerProfile profile) {
		
		final String statusCodeN = "N"; // status code N
		final String transactionView = "transaction"; // Transaction view in MiBanco
		final int allowedViewsCapacity = 1; // allowedViews capacity
		final Set<String> allowedViews = new HashSet<String> (allowedViewsCapacity); // allowedViews for Nontransactionals
		
		allowedViews.add(transactionView);
		
		return StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA965) 
				&& AccountUtils.getIsTransactional(profile).equalsIgnoreCase(statusCodeN)
				&& !allowedViews.contains(strViewName);
	}
}
