package evertec.cibp.web.retirementPlan.control;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;

import java.util.ArrayList;
import java.util.List;

import evertec.cibp.web.form.bean.RetirementPlanBean;
import evertec.cibp.web.response.RetirementPlanInfoResponse;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.relius.retirementplan.ws.wsdl.RetirementPlanBalanceResponse;
import evertec.relius.retirementplan.ws.wsdl.RetirementPlanInfo;


/**
*
* @author S681718
* @version 1.0
* @since 1.0
* @see RetirementPlanUtil
*/
public final class RetirementPlanUtil {

	/**constants logger */
	private static final Log LOGGER = LogFactory.getLog(RetirementPlanUtil.class);//
	
	/**
	 * Default Constructor
	 */
	private RetirementPlanUtil () {
		throw new IllegalStateException("Retirement Plan Utility class");
	}
	
	private final static AccountMaskUtil maskUtil = new AccountMaskUtil();

	/**
	 * Method for filter the response from retirement plan service
	 * @param response
	 * @return list filtered
	 */
	public static RetirementPlanInfoResponse generateFilteredResponse (RetirementPlanBalanceResponse response) {
		RetirementPlanInfoResponse filtered = new RetirementPlanInfoResponse();
		final List<RetirementPlanInfo> plans = response.getPlans();
		List<RetirementPlanBean> plansFilter = new ArrayList<RetirementPlanBean>();
		for (RetirementPlanInfo plan : plans) {
			RetirementPlanBean planClone = new RetirementPlanBean();
			try {
				BeanUtils.copyProperties(plan, planClone);
				planClone.setPlanid(maskUtil.mask(plan.getPlanid()));
				planClone.setAccountLast4Num(maskUtil.mask(plan.getPlanid()));
				plansFilter.add(planClone);
			} catch (BeansException e) {
				LOGGER.info("Retirement Plan Object cant be cloned Fail!");
			}
		}
		filtered.setPlans(plansFilter);
		LOGGER.info("RetirementPlan response filtered");
		
		return filtered;
	}
}
