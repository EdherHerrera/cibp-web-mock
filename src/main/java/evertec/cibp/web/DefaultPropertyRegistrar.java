package evertec.cibp.web;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;

import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.web.propertyeditors.DefaultFrequencyEditor;

public class DefaultPropertyRegistrar implements PropertyEditorRegistrar {

	private boolean allowAccentedCharacters;
	private boolean allowSpecialCharacters;
	private boolean excapeSpecialCharacters;

	public boolean isAllowAccentedCharacters() {
		return allowAccentedCharacters;
	}

	public void setAllowAccentedCharacters(boolean allowAccentedCharacters) {
		this.allowAccentedCharacters = allowAccentedCharacters;
	}

	public void setAllowSpecialCharacters(boolean allowSpecialCharacters) {
		this.allowSpecialCharacters = allowSpecialCharacters;
	}

	public void setExcapeSpecialCharacters(boolean excapeSpecialCharacters) {
		this.excapeSpecialCharacters = excapeSpecialCharacters;
	}

	public void registerCustomEditors(PropertyEditorRegistry registry) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		dateFormat.setLenient(false);
		CustomDateEditor customDateEditor = new CustomDateEditor(dateFormat, true);

		DecimalFormat df = new DecimalFormat("#,##0.##");

		CustomNumberEditor customNumberEditor = new CustomNumberEditor(BigDecimal.class, df, true);
		CustomNumberEditor customIntegerEditor = new CustomNumberEditor(Integer.class, true);
		DefaultFrequencyEditor frequencyPropertyEditor = new DefaultFrequencyEditor();
		evertec.cibp.web.propertyeditors.SpecialCharacterEditor specialCharacterEditor = new evertec.cibp.web.propertyeditors.SpecialCharacterEditor(
				allowAccentedCharacters, allowSpecialCharacters, excapeSpecialCharacters);

		registry.registerCustomEditor(Date.class, null, customDateEditor);
		registry.registerCustomEditor(BigDecimal.class, null, customNumberEditor);
		registry.registerCustomEditor(Integer.class, null, customIntegerEditor);
		registry.registerCustomEditor(Frequency.class, frequencyPropertyEditor);
		registry.registerCustomEditor(String.class, specialCharacterEditor);

	}

}
