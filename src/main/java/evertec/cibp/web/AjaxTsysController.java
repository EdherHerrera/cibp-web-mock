/**
 * @author franb
 * @created 2020-08-12 17:28
 */
package evertec.cibp.web;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.owasp.esapi.errors.AccessControlException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.premia.control.PremiaUtil;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.Utils;

/**
 * Is an action controller, listening for .vm views related to TysisLoyalty API
 *
 * @author franb
 * @version 1.0
 * @see MultiActionController
 * @since 1.2
 */
public class AjaxTsysController extends MultiActionController {
	
	/**
	 * Characters
	 */
	private static final int CHARACTERS = 900;//
	
	/**
	 * Function to build the customer interaction event object.
	 */
	private static Function<HttpServletRequest, CustomerInteractionEvent>customerInteractionEventBuilder = // 
			req -> Utils.generateCustomerInteractionEvent(req); //

	/**
	 * Premia catalog URL (injected by spring property)
	 */
	private String premiaCatalogURL; //
	
	/**
	 * sourceUrl
	 */
	private String sourceUrl; // ;
	
	/**
	 * Product ID starts With zero
	 */
	private String[] premiaAmexZero;//
	
	/**
	 * Number Premia Products
	 */
	private static final int NUM_PREMIA_PRODUCTS = 10; //
	
	/**
	 * Customer service, used to log in the audit log.
	 */
	protected CustomerServices customerService; // 

	/**
	 * 
	 */
	public AjaxTsysController() {
		this.premiaCatalogURL = StringUtils.EMPTY;
		this.sourceUrl = StringUtils.EMPTY;
	}
	
	/**
	 * 
	 * @param premiaCatalogURL
	 * @param sourceUrl
	 */
	public AjaxTsysController(String premiaCatalogURL, String sourceUrl, String[] premiaAmexZero, CustomerServices customerService) {
		this();
		this.premiaCatalogURL = premiaCatalogURL;
		this.sourceUrl = sourceUrl;
		this.premiaAmexZero = premiaAmexZero;
		this.customerService = customerService;
	}

	/**
	 * This method redirect via Tysis, to "Premia" catalog. Expect a
	 * {@link HttpServletRequest} with a {@link }card number, and user email in
	 * {@link UserSession}. Then build an encrypted data token via
	 * {@link evertec.cibp.core.services.CustomerServices#generatePremiaToken} which
	 * satisfy Tysis SSO authentication and authorization protocol, as a query param
	 * of {@link HttpServletResponse} to finally do the redirection.
	 *
	 * @param request  HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws IOException when failed to build redirection
	 * @throws AccessControlException
	 * @return ModelAndView
	 */
	public ModelAndView tsysLoyaltyRewardsRedirection (HttpServletRequest request, HttpServletResponse response)
			throws IOException, AccessControlException {
		
		// audit log
		CustomerInteractionEvent eventLog = customerInteractionEventBuilder.apply(request);
		
		try {

			final String redirectPattern = "%s?%s=%s"; //

			final String strCardNumber = "cardNumber"; //

			final String strToken = "token"; //

			final String frontEndId = request.getParameter(strCardNumber); //

			Map<String, Object> model = new HashMap<>(CHARACTERS);//

			if (WebUtils.getSessionAttribute(request, "userSession") instanceof UserSession) {

				final UserSession session = ((UserSession) WebUtils.getSessionAttribute(request, "userSession")); //
 
				final TVFrontendAccount ccaAccountByFrontEndId = Utils.getAccountById(frontEndId,
						session.getPremiaAccountList());//

				final String email = session.getCustomerProfile().getEmail(); //

				final UserInterface typeUser = session.getCustomerProfile().getUserInterface();//

				final String token = session.getCustomerService().generatePremiaToken(
						PremiaUtil.removeZeroToAccountNumber(ccaAccountByFrontEndId.getAccountNumber(),
								ccaAccountByFrontEndId.getAccountProductId(), getPremiaProducts()),
						sourceUrl, email, typeUser.equals(UserInterface.COMMERCIAL)); //
				
				

				final String redirectUrl = String.format(redirectPattern, premiaCatalogURL, strToken, token); //
				model.put("premiaToken", redirectUrl);
				
				// log trace information
				eventLog.setCustomStringData("TOKEN", token);
				eventLog.setCustomStringData("PREMIA_URL", redirectUrl);
				
				String userAgent = request.getHeader("User-Agent");
				eventLog.setCustomStringData("DEVICE", DeviceUtils.getCustomerDevice(userAgent).toString());
				eventLog.setCustomStringData("CONECTION_INFO", DeviceUtils.getMobileInfo(userAgent, session.getDeviceInfo()));
				
			}
			
			// success, set interaction
			eventLog.setInteraction(CustomerInteractionEventType.PREMIATOKEN_SSO_SUCCESS);

			return new ModelAndView("tsysLoyaltyRewardsRedirection", model);

		} catch (Exception ex) {
			// failed
			eventLog.setInteraction(CustomerInteractionEventType.PREMIATOKEN_SSO_FAIL);
			throw ex;
		} finally {
			if (CustomerInteractionEventType.PREMIATOKEN_SSO_SUCCESS == eventLog.getInteraction()) {
				customerService.logTsysLoyaltyRewardsRedirection(eventLog, InteractionSeverity.INFO);
			} else {
				customerService.logTsysLoyaltyRewardsRedirection(eventLog, InteractionSeverity.WARNING);
			}
		}
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView tsysLoyaltyRewardsAccountInfo(HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> model = new HashMap<>(CHARACTERS);//
		if (WebUtils.getSessionAttribute(request, "userSession") instanceof UserSession) {
			UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
			List<TVFrontendAccount> frontendAccounts = userSession.getPremiaAccountList().stream()
					.filter(tvFrontendAccount -> tvFrontendAccount.isPrimaryAccount())
					.collect(Collectors.toList());

			if (!frontendAccounts.isEmpty() && userSession.isHasPremia()) {
				try {
					final TVFrontendAccount premiaPriority = userSession.getCustomerService()
							.getPriorityPremiaAccount(frontendAccounts);//

					List<TVFrontendAccount> programstmp = userSession.getListRewardPoints();
					if (userSession.isConsultRewardsPoints()) {
						programstmp = PremiaUtil.premiaSelectFrontEndAccount(userSession, premiaPriority, getPremiaProducts());//
						userSession.setListRewardPoints(programstmp);
						userSession.setConsultRewardsPoints(false);
					}

					if (programstmp != null && !programstmp.isEmpty()) {
						double balance = programstmp.get(0).getTsysLoyaltyRewardsInfo().getAvailableBalanceDouble();
						DecimalFormat decimalFormat = new DecimalFormat("#");
						String balanceValue = decimalFormat.format(balance);
						model.put("tsysBalanceRewards", balanceValue);
					}

				} catch (Exception e) {
					model.put("error", "N/A");
				}
			}
		}
		return new ModelAndView("tsysLoyaltyRewardsAccountInfo", model);
	}
	
	/**
	 * 
	 * @return premiaCCAProducts
	 */
	private List<String> getPremiaProducts () {
		final List<String> premiaProducts = new ArrayList<String>(NUM_PREMIA_PRODUCTS); //
		if(premiaAmexZero != null) {
			Collections.addAll(premiaProducts, premiaAmexZero);
		}
		return Collections.unmodifiableList(premiaProducts);
	}
	
	/**
	 * @param premiaCatalogURL the premiaCatalogURL to set
	 */
	public void setPremiaCatalogURL (String premiaCatalogURL) {
		this.premiaCatalogURL = premiaCatalogURL;
	}

	/**
	 * @param sourceUrl the sourceUrl to set
	 */
	public void setSourceUrl (String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}
	
	/**
	 * @param premiaAmexZero the premiaAmexZero to set
	 */
	public void setPremiaAmexZero(String[] premiaAmexZero) {
		this.premiaAmexZero = premiaAmexZero;
	}

	/**
	 * @return The Customer Service
	 */
	public CustomerServices getCustomerService() {
		return customerService;
	}
	
	/**
	 * Set Customer Service
	 * @param customerService
	 */
	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	/**
	 * Set customerInteractionEventBuilder
	 * @param customerInteractionEventBuilder
	 */
	public static void setCustomerInteractionEventBuilder(Function<HttpServletRequest,
														CustomerInteractionEvent> customerInteractionEventBuilder) {
		AjaxTsysController.customerInteractionEventBuilder = customerInteractionEventBuilder;
	}

}