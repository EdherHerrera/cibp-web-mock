package evertec.cibp.web.resolver;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.exceptions.CustomerAssignableException;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.resolver.types.AjaxError;
import evertec.cibp.web.resolver.types.IframeError;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.view.AjaxErrorView;

public class ExceptionResolver implements HandlerExceptionResolver {

	private CustomerServices customerServices;
	private static final Logger logger = Logger.getLogger(ExceptionResolver.class);
	private String[] emails = { "ysoler@evertecinc.com", "rebmaldonado@evertecinc.com", "angtorres@evertecinc.com",
			"eurivera@evertecinc.com" };

	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {

		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");
		String hostname = "unknown";

		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
		}

		if (userSession != null && userSession.getCustomerProfile() != null) {

			CustomerAssignableException customerAssignableException = new CustomerAssignableException(
					userSession.getCustomerProfile(), ex);
			logger.error("Unexpected Condition", customerAssignableException);

			if (Level.DEBUG.equals(logger.getLevel())) {
				customerServices.logException(customerAssignableException, hostname + " " + request.getRequestURI(),
						userSession.getCustomerProfile().getUsername());

				customerServices.sendErrorAlert(userSession.getCustomerProfile(), request.getRemoteAddr(), hostname,
						request.getRequestURI(), ex, emails);
			}

		} else {
			logger.error("Unexpected Condition", ex);

			if (Level.DEBUG.equals(logger.getLevel())) {
				customerServices.logException(ex, hostname + " " + request.getRequestURI(), "-");

				customerServices.sendErrorAlert(null, request.getRemoteAddr(), hostname, request.getRequestURI(), ex,
						emails);
			}

		}

		List allInterfaces = ClassUtils.getAllInterfaces(handler.getClass());

		if (allInterfaces.contains(AjaxError.class)) {

			return new ModelAndView(new AjaxErrorView());

		} else if (allInterfaces.contains(IframeError.class)) {

			return new ModelAndView("error", "exception", ex);
		} else

			return new ModelAndView("error", "exception", ex);

	}

	public CustomerServices getCustomerServices() {
		return customerServices;
	}

	public void setCustomerServices(CustomerServices customerServices) {
		this.customerServices = customerServices;
	}

}
