package evertec.cibp.ebpp2.loader;

import java.rmi.RemoteException;

import evertec.cibp.core.services.CustomerServices;

public class ebpp2Login {

	protected CustomerServices customerService;

	public void executeEbpp2Login() throws RemoteException {
		customerService.ebpp2Login();
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}
}
