package evertec.cibp.errormessagetelepago.loader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.web.context.ServletContextAware;

import evertec.cibp.core.models.csr.ErrorMessageTelepago;
import evertec.cibp.core.services.CustomerServices;

/*
 *  	 CIBP-1680
 *       bgarcia
 *       Bean para implementar la carga de los mensajes de errror 
 * */

public class ErrorMessageTelepagoServiceLoader implements ErrorMessageTelepagoService, ServletContextAware {

	protected CustomerServices customerService;// customer profile
	private ServletContext servletContext;
	String filepath;// path del file

	public String getPath() {
		return this.servletContext.getRealPath("") + "/WEB-INF/" + filepath;
	}

	/*
	 * CIBP-1680 bgarcia Se crean lso archivos
	 */
	public void updateMessageTelepago() {
		List<ErrorMessageTelepago> l = customerService.getErrorTelepagoActualizados();
		StringBuffer englishCamps = new StringBuffer("");
		StringBuffer spanishCamps = new StringBuffer("");
		for (ErrorMessageTelepago e : l) {
			englishCamps.append(e.getCod_error() + "=" + e.getDescripcion_mibanco_ing() + "\n");
			spanishCamps.append(e.getCod_error() + "=" + e.getDescripcion_mibanco_esp() + "\n");
		}
		writeFile(getPath() + "ErroresTelepago.properties", englishCamps.toString());
		writeFile(getPath() + "ErroresTelepago_es.properties", spanishCamps.toString());
	}

	/*
	 * CIBP-1680 bgarcia Agrega un cambio de linea cada 60 caracteres
	 */
	/*
	 * private String FormatoTexto(String h){ StringBuffer str_texto = new
	 * StringBuffer("");
	 * 
	 * if (h.length() > 60){ String[] aux = h.split(" "); int j = 0; int i = 0;
	 * while (i < aux.length){ if (aux[i].trim().length() > 0){ j = j +
	 * aux[i].length(); str_texto.append(" " + aux[i].trim()); if (j >=60){
	 * str_texto.append("<br/>"); j= 0; } } i++; } }else str_texto.append(h);
	 * return str_texto.toString(); }
	 */

	/*
	 * CIBP-1680 bgarcia get para customer profile
	 */
	public CustomerServices getCustomerService() {
		return customerService;
	}

	/*
	 * CIBP-1680 bgarcia set para customer profile
	 */
	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}

	/*
	 * CIBP-1680 bgarcia escribir un archivo
	 */
	private void writeFile(String fileName, String data) {

		PrintWriter out = null;
		
		try {
			// System.out.println(logData);
			File log = new File(fileName);
			if (log.exists()) {
				out = new PrintWriter(new BufferedWriter(new FileWriter(log, false)));
				out.write(data);
				out.flush();
				out.close();
			} else {
				out = new PrintWriter(new BufferedWriter(new FileWriter(log)));
				out.write(data);
				out.flush();
				out.close();
			}
		} catch (Exception e) {
			System.out.println(StringEscapeUtils.escapeJava(e.getMessage()));
		}finally {
			if(out != null) {
				out.close();
			}
		}
	}

	/*
	 * CIBP-1680 bgarcia get file path
	 */
	public String getFilepath() {
		if (filepath != null)
			return filepath.trim();

		return filepath;
	}

	/*
	 * CIBP-1680 bgarcia set file path
	 */
	public void setFilepath(String filepath) {
		if (filepath != null)
			filepath = filepath.trim();
		this.filepath = filepath;
	}

	public void setServletContext(ServletContext servletcontext) {
		this.servletContext = servletcontext;
	}

}
