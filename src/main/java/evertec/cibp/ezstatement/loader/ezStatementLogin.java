package evertec.cibp.ezstatement.loader;

import evertec.cibp.core.services.CustomerServices;

public class ezStatementLogin {

	protected CustomerServices customerService;

	public void executeEzStatementLogin() {
		customerService.ezStatementLogin();
	}

	public CustomerServices getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServices customerService) {
		this.customerService = customerService;
	}
}
