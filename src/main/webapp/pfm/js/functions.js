var transactionId=null;
var mouse_is_inside=false;
var objShown=null;
var comment = "";
var trxDesc = "";
var tags=null;

function setComment(cmt) {
	comment = cmt;
}

function setTrxDesc(des) {
	trxDesc = des;
}

function goTo(url){
	document.location.href = url;
}
function showPreloader(id){
	jQuery('#'+id).addClass("preloader");	
}
function removePreloader(id){
	jQuery('#'+id).removeClass("preloader");	
}
function loadExternalPage(url, id){
	//Clear all data in div
	jQuery('#'+id).html("");
	showPreloader(id);
	jQuery.ajax({
		  url: url,
		  cache: false,
		  success: function(resp){
				removePreloader(id);
				jQuery("#"+id).html(resp);
			  	jQuery("#"+id).dialog('open');
		  }
	});
	
}
function hideRowEditorTrx(save, trxId){
	// Hide row editors
	if(jQuery('.row-editor').is(':visible')){
		// Remove all highlights
		jQuery('.tbl-list tr').removeClass("highlight");
		// Hide editor
		jQuery('.row-editor').hide('fade', {}, 200);
		jQuery('span', $('.row-editor-container')).show();
		jQuery('input:text', $('.row-editor-container')).hide();
		// Show edit button
		jQuery('div.btn-edit', $('.row-editor-container')).show();
	}
	if(save) {
		var tagString = "";
		for(var i=0; i<tags.length; i++)
			tagString += tags[i] + "|";
			
		var pars = {
			transactionId: trxId,
			desc: trxDesc,
			tags: tagString,
			comment: comment
		};

		new Ajax.Request('pfmUpdateTransaction', 
			{ method:'post',
			  parameters:pars,
			  onComplete: function (ret) {
			  	// Reload the entire page of transactions
				reloadTransactionsAfterEditOrPaging(0, 0, trxId);
			  }
		});
	}
	else {
		resetTransaction(trxId);
	}
}
function hideRowEditor(){
	// Hide row editors
	if (jQuery('.row-editor').is(':visible')) {
		// Remove all highlights
		jQuery('.tbl-list tr').removeClass("highlight");
		// Hide editor
		jQuery('.row-editor').hide('fade', {}, 200);
		jQuery('span', $('.row-editor-container')).show();
		jQuery('input:text', $('.row-editor-container')).hide();
		// Show edit button
		jQuery('div.btn-edit', $('.row-editor-container')).show();
	}
}
function hideOtherShownElements(id){
	if(objShown!=null){
		if(jQuery('dd.content', jQuery('#'+objShown)).is(':visible')){
			jQuery('dd.content', jQuery('#'+objShown)).toggle('fast');
		}
		hideRowEditor();
	}
	objShown = id;	

}
function S4() {
    return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
}
function guid() {
    return (S4()+S4());
}
function saveTransactionDetail(){
	// Change all descriptions to the new value
	jQuery('#'+transactionId+' .desc').html(jQuery('#'+transactionId+' input[name="desc"]').val());
	hideTransactionDetail();
}
function selectMonths(year){
	monthsSelected_arr=[];
	// Remove all highlighte cells
	jQuery('.months-timespan td').removeClass('active');
	//
	if(year){
		jQuery('.months-timespan td').each(function(){
			if(jQuery('span.year', jQuery(this)).html()==year){
				monthsSelected_arr.push(jQuery(this).attr('abbr'));
				jQuery(this).addClass('active');
			}
			
		});
	}else{
		jQuery('.months-timespan td').each(function(){
			
			if(!jQuery(this).hasClass("inactive")){
				monthsSelected_arr.push(jQuery(this).attr('abbr'));
				jQuery(this).addClass('active');
			}
			
		});
	}
	buildSelectedMonthArray();
}
function selectOneMonth(thismonth) {
	monthsSelected_arr=[];
	// Remove all highlighte cells
	jQuery('.months-timespan td').removeClass('active');
	//
	jQuery('.months-timespan td').each(function(){
			
		if(!jQuery(this).hasClass("inactive")){
			if(jQuery(this).attr('abbr') == thismonth) {
				monthsSelected_arr.push(jQuery(this).attr('abbr'));
				jQuery(this).addClass('active');
			}
		}
			
	});
	buildSelectedMonthArray();
}
function deselectMonths(){
	monthsSelected_arr=[];
	jQuery('.months-timespan td').removeClass('active');
}
function returnMonth(n, lang){
	if(lang=='es-pr'){
		switch(Number(n)){
			case 1:
				return 'ENE';
				break;	
			case 2:
				return 'FEB';
				break;
			case 3:
				return 'MAR';
				break;
			case 4:
				return 'ABR';
				break;
			case 5:
				return 'MAY';
				break;
			case 6:
				return 'JUN';
				break;
			case 7:
				return 'JUL';
				break;
			case 8:
				return 'AGO';
				break;
			case 9:
				return 'SEP';
				break;
			case 10:
				return 'OCT';
				break;
			case 11:
				return 'NOV';
				break;
			case 12:
				return 'DIC';
				break;
		}
	}else{
	
		switch(Number(n)){
			case 1:
				return 'JAN';
				break;	
			case 2:
				return 'FEB';
				break;
			case 3:
				return 'MAR';
				break;
			case 4:
				return 'APR';
				break;
			case 5:
				return 'MAY';
				break;
			case 6:
				return 'JUN';
				break;
			case 7:
				return 'JUL';
				break;
			case 8:
				return 'AUG';
				break;
			case 9:
				return 'SEP';
				break;
			case 10:
				return 'OCT';
				break;
			case 11:
				return 'NOV';
				break;
			case 12:
				return 'DEC';
				break;
		}
		
	}
}
function setMonthRange(lang){
	var actual_txt;
	var msg;
	if(lang=='es-pr'){
		actual_txt = 'Mes actual';
		msg = 'Por favor seleccione por lo menos un mes';
	}else{
		actual_txt = 'Current month';	
		msg = 'Please select at least one month';
	}
	if(monthsSelected_arr.length>0){
		if(monthsSelected_arr.length==1){
			var arr = monthsSelected_arr[0].split('/');
			if(curMonth+'/'+curYear==monthsSelected_arr[0]){
				jQuery('#detail-selected-timespan').html(actual_txt+' ('+returnMonth(curMonth)+' '+curYear+')');
			}else{
				jQuery('#detail-selected-timespan').html(returnMonth(arr[0])+' '+arr[1]);
			}
			return;
		}else{
			var arr = monthsSelected_arr[0].split('/');
			var initialMonth = arr[0];
			var initialYear = arr[1];
			var arr = monthsSelected_arr[monthsSelected_arr.length-1].split('/');
			var lastMonth = arr[0];
			var lastYear = arr[1];
			
			jQuery('#detail-selected-timespan').html(returnMonth(initialMonth, lang)+' '+initialYear+' - '+returnMonth(lastMonth, lang)+' '+lastYear);
			
		}
		
	}else{
		alert(msg);
	}
}
Number.prototype.formatMoney = function(c, d, t){
var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

jQuery(function(){
		 jQuery("body").mouseup(function(){
		 	// Keep the session from expiring...
			resetSessionExpiration();
			if (!mouse_is_inside) {
				if (objShown != null) {
					var hideme = false;
					var idparent = jQuery('#' + objShown).parent().attr('id');
					if (idparent != null || idparent != undefined) {
						if (idparent == 'list-accounts') {
							resetAccounts();
							hideme = true;
						}
						else 
							if (idparent == 'list-categories') {
								resetCategories();
								hideme = true;
							}
							else 
								if (idparent == 'list-tags') {
									resetTags();
									hideme = true;
								}
								else 
									if (idparent == 'list-cred-subcategories') {
										resetCreditSubcategories();
										hideme = true;
									}
									else 
										if (idparent.indexOf("trxdd-") == 0) {
											hideme = true;
										}
						//else if(jQuery('#'+objShown).attr('id').indexOf("editor-") == 0) {
						//	hideme = true;
						//	jQuery('#trx-cancel-btn').click();
						//}
						if (hideme) {
							hideOtherShownElements(objShown);
							objShown = null;
						}
					}
				}
			}
		});			
});

// Help note functions
var clearBackgroundColor;
var boolShowHelpTips = true;
function resetNoteBack(){
	jQuery(".help-note").css('backgroundColor', 'transparent');
}
function initHelpBubbles(showTips){
	    boolShowHelpTips = showTips;	
		// Show help tips if variable == true
		if(boolShowHelpTips==false){
			jQuery(".helptip").addClass('hide');
		}

		// initialize tooltip
		jQuery(".helptip[title]").tooltip({
		 
		   // tweak the position
		   offset: [20, -7],
		 
		   // use the "slide" effect
		   effect: 'slide'/*,
		   events: {
			  def: 'mouseover click,mouseleave click'
			  }*/
		});	
		jQuery(".help-note").mouseover(function(){
			jQuery(this).css('backgroundColor', 'none');	
			
		});
		jQuery(".helptip").mouseover(function(){
			
			// Check to see if parent has help note class
			if(jQuery(this).parent().hasClass('help-note') && !jQuery(this).parent().hasClass('no-effect')){
				
				jQuery(this).parent().css('backgroundColor', '#f8f2cf');
				jQuery(this).parent().stop().animate({ backgroundColor: "#ffffff" }, 3000);
				clearTimeout(clearBackgroundColor);
				clearBackgroundColor = setTimeout("resetNoteBack()", 3010);
			}
		});
}
function initToggleHelp(lang){
		// Set info
		if(boolShowHelpTips==false){
			if(lang=="en") jQuery('#toggle-help span').html('Help tips on');
			else jQuery('#toggle-help span').html('Encender ayuda');
		}else{
			if(lang=="en") jQuery('#toggle-help span').html('Help tips off');
			else jQuery('#toggle-help span').html('Apagar ayuda');
		}
		
		// BTN toggle help tips
		jQuery('#toggle-help').click(function(){
			
			jQuery('.helptip').toggleClass("hide");
			if(jQuery('.helptip').hasClass('hide')){
				boolShowHelpTips = false;				
				if(lang=="en") jQuery('#toggle-help span').html('Help tips on');
				else jQuery('#toggle-help span').html('Encender ayuda');
			}else{
				boolShowHelpTips = true;	
				if(lang=="en") jQuery('#toggle-help span').html('Help tips off');
				else jQuery('#toggle-help span').html('Apagar ayuda');
			}
		});		
}

jQuery.fn.size = function() {
		return this.length;
	};



