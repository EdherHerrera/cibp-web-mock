(function($){
	$.fn.roweditor = function(options) {
		var
		  defaults = {useExternalPage:false, pageUrl:'', pageContainer:''},
		  settings = $.extend({}, defaults, options);
		  var $this = $(this);
		  var uid = guid();
		  // Set unique id for row
		  $this.parent().attr('id', 'editor-'+uid);
		  // Hide all row editor containers
		  $('.row-editor').hide();
	      // Hide all inputs on table
		  $('input:text', $this).hide();
		  
		  // Check to see mouse inside custom dropdowns
	      $this.hover(function(){ 
				mouse_is_inside=true; 
			}, function(){ 
				mouse_is_inside=false; 
		  });
		  // this.each(function() {
		  // Set actions for edit buttons
		  $('.btn-edit a', $this).click(function(){
				
				// Get <td> id to manipulate elements inside
				var $this = $(this).parent().parent().parent();
				hideOtherShownElements($this.attr('id'));

				// Remove all highlights
				$('.tbl-list tr').removeClass("highlight");
				// Highlight <tr>
				//alert($('#'+id).parent().attr('id'));
				$this.parent().addClass("highlight");
				
				// Hide <span>, show the <input> and hide edit button
				$('span',$this).hide();
				$('input:text', $this).slideDown('fast');
				
				$('input:text', $this).slideDown(function(){
					this.style.display = 'block';
				});
				
				$('div.btn-edit', $this).hide();
				
				// Set focus on input
				$('input:text', $this).focus();
				
				$('input:text', $this).keyup(function(){
					$('.desc', $this).html($('input[name="desc"]', $this).val());
				});
				
				$('.row-editor', $this).show('fade', {}, 200);
				
				if(settings.useExternalPage==true){
					
					// Get the parent (transaction id) and set it in session
					var transactionId = $this.parent().attr('id');
					var pars = "transactionId=" + transactionId;

					new Ajax.Request('pfmSetupTransactionIdToEdit', 
		            { method:'post',
		              parameters:pars,
		              onComplete: function (ret) {
						loadExternalPage(settings.pageUrl, 'editor-'+transactionId+' div');
					  }	
		            });
					
				}
		   });
		//});
		  // returns the jQuery object to allow for chainability.
		 
		  return this;
	}
})(jQuery);
