(function($){
	$.fn.customdropdown = function(options) {
		var
		  defaults = {lang:'es-pr', useExternalPage:false, pageUrl:'', pageContainer:''},
		  settings = $.extend({}, defaults, options),
		  shown = false;
		  this.each(function() {
		  	var $this = $(this);
			var uid = guid();
			$this.attr('id', 'drop-'+uid);
	
			// Hide dropdown content
			$('dd.content', $this).hide();
			// Set custom dropdown open action
			$('dt', $this).click(function(){
				shown = ($('dd.content', $this).is(':visible'));
				hideOtherShownElements($this.attr('id'));
				if (!shown) {
					shown = true;
					$('dd.content', $this).toggle('fast');
					// Load external page on build
					if (settings.useExternalPage == true) {
						// Load external page
						if($this.parent().parent().attr('id')!="undefined") {
							var trxId = $this.parent().parent().attr('id')
							// Reset this transaction
							resetTransaction(trxId);
							var pars = "trxId=" + trxId;
							new Ajax.Request('pfmSetupCategoryIdToHighlight', 
		                    { method:'post',
		                      parameters:pars,
		                      onComplete: function (ret) {
							  	loadExternalPage(settings.pageUrl, $this.attr('id') + ' dd.content');
							  }
		                    });
						}
						else
						    loadExternalPage(settings.pageUrl, $this.attr('id') + ' dd.content');
					}
				}
				else {
					// Reset everything
					if($this.parent().attr('id')=='list-accounts')
						resetAccounts();
					else if($this.parent().attr('id')=='list-categories')
						resetCategories();
					else if($this.parent().attr('id')=='list-cred-subcategories')
						resetCreditSubcategories();
					else if($this.parent().attr('id')=='list-tags')
						resetTags();
					shown = false;
				}
			});
			
			
			
			// Set the apply function if the apply button exists
			if($('dd.content .btn-apply', $this)){
				$('.btn-apply', $this).click(function(){
					
					$('dd.content', $this).toggle('fast');
					 var allVals = "";
					 var totalCheckboxes = $('input:checkbox',$this).length;
					 // Clear div content
					 //$('.label', $this).html('');
					 
					 $('input:checkbox', $this).each(function() {
					   allVals += (this.checked) + "|";
					 });
					 shown = false;
					 // Set Todas/All in label
					 //if(allVals.length==totalCheckboxes){
						 //$('.label', $this).append(txtAll);
					//	 return;
					//}
					 
					//$('.label',$this).append(String(allVals.join(', ')));
					
					// Save the correct configurations
					if($this.parent().attr('id')=='list-accounts')
						saveAccountConfiguration(allVals);
					else if($this.parent().attr('id')=='list-categories')
						saveCategoriesConfiguration(allVals);
					else if($this.parent().attr('id')=='list-cred-subcategories')
						saveCreditSubcategoriesConfiguration(allVals);
					else if($this.parent().attr('id')=='list-tags')
						saveTagsConfiguration(allVals);
				
				});
			}
			
			// Set the apply all function if the apply all button exists
			if($('dd.content .btn-apply', $this)){
				var txtSelectAll;
				var txtDeselectAll;
				if(settings.lang=='es-pr'){
					txtSelectAll='Seleccionar Todos'; 
					txtDeselectAll = 'Deseleccionar Todos';
				}else{
					txtSelectAll='Select All'; 
					txtDeselectAll = 'Deselect All';
				}
				$('.btn-select-all', $this).click(function() {
				
					if($('.btn-select-all', $this).html() == txtSelectAll) {
						$('.btn-select-all', $this).html(txtDeselectAll);
						$('.tbl-list :checkbox', $this).prop('checked', true);
					}
					else {
						$('.btn-select-all', $this).html(txtSelectAll);
						$('.tbl-list :checkbox', $this).prop('checked', false);
					}
				});	
			}

			// Select/Deselect all checkboxes underneath
			$(".tbl-list input:checkbox", $this).click(function(){
			
				if($(this).is(':checked')){
					$(' :checkbox', $(this).parent()).prop('checked', true);
				}else{
					$(' :checkbox', $(this).parent()).prop('checked', false);
				}
			
			});
			
			// Check to see mouse inside custom dropdowns
			$this.hover(function(){ 
					mouse_is_inside=true; 
				}, function(){ 
					mouse_is_inside=false;
			});
			
		  });
		  // returns the jQuery object to allow for chainability.
		 
		  return this;
	}
})(jQuery);
