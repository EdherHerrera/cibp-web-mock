// Womancare form script (LC)

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=>document; if((p=>n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=>d[n])&&d.all) x=>d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=>d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function changeDisplay(o, p) {
	theElement = MM_findObj(o);
	theElement.style.display=p;
}

function changeText(e, t) {
	theElement = MM_findObj(e);
	theElement.innerHTML=t;
}

function showForm() {
	changeDisplay('insurance-select-date-form', 'block');
	changeDisplay('insurance-select-date-info', 'none');
}

function hideForm(theLang) {
	var f = document.forms['wcForm'];
	var theAge = calculateAge(f.birthday_day.value, f.birthday_month.value, f.birthday_year.value);
	f.accept_age[0].checked=false;
	f.accept_age[1].checked=false;
	f.selected_plan[0].checked=false;
	f.selected_plan[1].checked=false;
	f.selected_plan[2].checked=false;
	f.monthly_premium.value='';
	changeText('monthly-debit', '$');
	if (isNaN(theAge)) {
		displayBdateError();
		return false;
	} else { // Update all texts 
		changeText('t-age-1', theAge);
		changeText('t-age-2', theAge);
		changeText('t-age-3', theAge);
		displayBdate(theLang);
		displayPremiumBasedOnAge(theAge);
		changeDisplay('insurance-select-date-form', 'none');
		changeDisplay('insurance-select-date-info', 'block');
	}
}

function checkAge(theLang) {
	var f = document.forms['wcForm'];
	var theAge = calculateAge(f.birthday_day.value, f.birthday_month.value, f.birthday_year.value);
	//mpicado
	f.customer_age.value = theAge;
	if($('t-age-1') != null)
	   $('t-age-1').innerHTML = theAge;
	if($('t-age-2') != null)
	   $('t-age-2').innerHTML = theAge;
	if($('t-age-3') != null)
	   $('t-age-3').innerHTML = theAge;
	if($('t-bday') != null ){
		if (theLang == 'es'){
			$('t-bday').innerHTML =  $('birthday_day').value + " de " + getMonthName($('birthday_month').value, 'es') + " de " + $('birthday_year').value;
		}
		else{
			$('t-bday').innerHTML =  getMonthName($('birthday_month').value, 'en') + " " + $('birthday_day').value + ", " + $('birthday_year').value;
		}
	}
	//end mpicado
	
	/*if (!isNaN(theAge))
		if (theAge > 64)
			if (theLang == 'es') document.location.href='losentimos.html';
			else document.location.href='losentimos-en.html';*/
}

function validateForm(theForm) {
	//mpicado, se cambiaron los ids de los campos para que concordaran con los que pone velocity que son iguales a los nombres del commandBean
	var f=theForm;
	var isValid = true;
	
	//mpicado, se agrega validacion para que no se de submit al formulario si no ha presionado OK al momento de cambiar la edad
	changeAge = MM_findObj('insurance-select-date-form');
	if (changeAge.style.display=='block'){
		isValid = false;
		displayBdateError();
	}
		
	if (!f.accept_age[0].checked && !f.accept_age[1].checked) { changeDisplay('r_accept_age', 'inline'); isValid = false; }
	/*mpicado, se comento este else debido al cibp-2261
	else{
		//mpicado, se hace validacion de la edad maxima ya que no se hacia una vez cambiada desde el formulario
		if(f.customer_age.value > f.max_age.value){
			//changeDisplay('r_accept_age', 'inline');
			//isValid = false;
		}
		else
			changeDisplay('r_accept_age', 'none');
	}*/
	if (!f.selected_plan[0].checked && !f.selected_plan[1].checked && !f.selected_plan[2].checked) { changeDisplay('r_selected_plan', 'inline'); isValid = false; }
	else changeDisplay('r_selected_plan', 'none');
	if (f.customerName.value.length < 3) { changeDisplay('r_fname', 'inline'); isValid = false; }
	else changeDisplay('r_fname', 'none');
	if (f.customerAddress.value.length < 3) { changeDisplay('r_address', 'inline'); isValid = false; }
	else changeDisplay('r_address', 'none');
	if (f.customerLastName.value.length < 3) { changeDisplay('r_lname', 'inline'); isValid = false; }
	else changeDisplay('r_lname', 'none');
	if (f.customerCity.value.length < 3) { changeDisplay('r_city', 'inline'); isValid = false; }
	else changeDisplay('r_city', 'none');
	if (f.customerMaidenName.value.length < 3) { changeDisplay('r_mlname', 'inline'); isValid = false; }
	else changeDisplay('r_mlname', 'none');
	if (f.customerState.value.length < 2) { changeDisplay('r_state', 'inline'); isValid = false; }
	else changeDisplay('r_state', 'none');
	if (f.customerZipCode.value.length < 3) { changeDisplay('r_zipcode', 'inline'); isValid = false; }
	else changeDisplay('r_zipcode', 'none');
	//mpicado
	if (f.customerAccount.value == "none") { changeDisplay('r_account', 'inline'); isValid = false; }
	else changeDisplay('r_account', 'none');
	if (!f.accept_terms.checked) { changeDisplay('r_accept_terms', 'inline'); isValid = false; }
	else changeDisplay('r_accept_terms', 'none');
	if (!isValid) document.location.href='#formstart';
	return isValid;
}

function selectedPlanChanged(theValue) {
	var f = document.forms['wcForm'];
	var mp = MM_findObj('premium-'+theValue).innerHTML;
	f.monthly_premium.value = mp;
	changeText('monthly-debit', mp);
}

function displayBdateError() {
	changeDisplay('bdate-error', 'inline');
	setTimeout("changeDisplay('bdate-error', 'none')", 3000);
}

function displayBdate(theLang) {
	var f = document.forms['wcForm'];
	var bday = f.birthday_day.value;
	var bmo = f.birthday_month.value;
	var byr = f.birthday_year.value;
	var longBDate = "";
	if (theLang == 'es') longBDate = bday+' de '+getMonthName(bmo, theLang)+' de '+byr;
	else longBDate = getMonthName(bmo, theLang)+' '+bday+', '+byr;
	changeText('t-bday', longBDate);
}

function displayPremiumBasedOnAge(theAge) {
	if (theAge < 35) {
		changeText('premium-1', '$7.95');
		changeText('premium-2', '$16.00');
		changeText('premium-3', '$23.75');
	} else if (theAge >= 35 && theAge <= 44) {
		changeText('premium-1', '$9.75');
		changeText('premium-2', '$19.25');
		changeText('premium-3', '$28.75');
	} else if (theAge >= 45 && theAge <= 54) {
		changeText('premium-1', '$12.00');
		changeText('premium-2', '$23.75');
		changeText('premium-3', '$35.75');
	} else if (theAge >= 55 && theAge <= 64) {
		changeText('premium-1', '$13.25');
		changeText('premium-2', '$26.25');
		changeText('premium-3', '$39.50');
	} else {
		changeText('premium-1', '$');
		changeText('premium-2', '$');
		changeText('premium-3', '$');
	}
}

function getMonthName(theMonth, theLang) {
	if (theLang == 'es') {
		switch(theMonth) {
			case '1': return 'enero'; break;
			case '2': return 'febrero'; break;
			case '3': return 'marzo'; break;
			case '4': return 'abril'; break;
			case '5': return 'mayo'; break;
			case '6': return 'junio'; break;
			case '7': return 'julio'; break;
			case '8': return 'agosto'; break;
			case '9': return 'septiembre'; break;
			case '10': return 'octubre'; break;
			case '11': return 'noviembre'; break;
			case '12': return 'diciembre'; break;
			default: return ''; break;
		}
	} else {
		switch(theMonth) {
			case '1': return 'January'; break;
			case '2': return 'February'; break;
			case '3': return 'March'; break;
			case '4': return 'April'; break;
			case '5': return 'May'; break;
			case '6': return 'June'; break;
			case '7': return 'July'; break;
			case '8': return 'August'; break;
			case '9': return 'September'; break;
			case '10': return 'October'; break;
			case '11': return 'November'; break;
			case '12': return 'December'; break;
			default: return ''; break;
		}
	}
}

function calculateAge(theDay, theMonth, theYear) {
	var bday = parseInt(theDay);
	var bmo = (parseInt(theMonth)-1);
	var byr = parseInt(theYear);
	if (isNaN(bday)||isNaN(bmo)||isNaN(byr)) return 'x';
	var age;
	var now = new Date();
	tday = now.getDate();
	tmo = now.getMonth();
	tyr = now.getFullYear();
	if((tmo > bmo)||(tmo==bmo & tday>=bday)) age=byr;
	else age=byr+1;
	return tyr-age;
}
