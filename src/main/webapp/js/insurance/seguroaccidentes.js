// SeguroAccidentes form script

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=>document; if((p=>n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=>d[n])&&d.all) x=>d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=>d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function changeDisplay(o, p) {
	theElement = MM_findObj(o);
	theElement.style.display=p;
}


var no_digito = /\D/g;

function changeText(e, t) {
	theElement = MM_findObj(e);
	theElement.innerHTML = t;
}

function showForm() {
	changeDisplay('insurance-select-date-form', 'block');
	changeDisplay('insurance-select-date-info', 'none');
}

function hideForm(theLang) {
	var f = document.getElementById('wcForm');
	var theAge = calculateAge(f.birthday_day.value, f.birthday_month.value, f.birthday_year.value);
	f.accept_age[0].checked=false;
	f.accept_age[1].checked=false;
	//f.monthly_premium.value='';
	if (isNaN(theAge)) {
		displayBdateError();
		return false;
	} else { // Update all texts 
		changeText('t-age-1', theAge);
		changeText('t-age-2', theAge);
		displayBdate(theLang);
		changeDisplay('insurance-select-date-form', 'none');
		changeDisplay('insurance-select-date-info', 'block');
	}
}

function checkAge(theLang) {
	var f = document.getElementById('wcForm');
	var theAge = calculateAge(f.birthday_day.value, f.birthday_month.value, f.birthday_year.value);
	f.customer_age.value = theAge;
}

function validateForm(theForm) {
	var theAge = 0;
	var f=theForm;
	var isValid = true;
	if (!f.accept_age[0].checked && !f.accept_age[1].checked) { changeDisplay('r_accept_age', 'inline'); isValid = false; }
	else changeDisplay('r_accept_age', 'none');
	if (!f.selected_plan[0].checked && !f.selected_plan[1].checked && !f.selected_plan[2].checked && !f.selected_plan[3].checked && !f.selected_plan[4].checked && !f.selected_plan[5].checked && !f.selected_plan[6].checked && !f.selected_plan[7].checked && !f.selected_plan[8].checked && !f.selected_plan[9].checked && !f.selected_plan[10].checked) { changeDisplay('r_selected_plan', 'inline'); isValid = false; }
	else changeDisplay('r_selected_plan', 'none');
	if (f.fname.value.length < 3) { changeDisplay('r_fname', 'inline'); isValid = false; }
	else changeDisplay('r_fname', 'none');
	if (f.lname.value.length < 3) { changeDisplay('r_lname', 'inline'); isValid = false; }
	else changeDisplay('r_lname', 'none');
	if (f.direccion.value.length < 3) { changeDisplay('r_address', 'inline'); isValid = false; }
	else changeDisplay('r_address', 'none');
	if (f.city.value.length < 3) { changeDisplay('r_city', 'inline'); isValid = false; }
	else changeDisplay('r_city', 'none');
	if (f.mlname.value.length < 3) { changeDisplay('r_mlname', 'inline'); isValid = false; }
	else changeDisplay('r_mlname', 'none');
	if (f.state.value.length < 2) { changeDisplay('r_state', 'inline'); isValid = false; }
	else changeDisplay('r_state', 'none');
	if (f.zipcode.value.length < 5) { changeDisplay('r_zipcode', 'inline'); isValid = false; }
	else changeDisplay('r_zipcode', 'none');
	if (f.tele_residencial.value.length < 10) { changeDisplay('r_tele_residencial', 'inline'); isValid = false; }
	else changeDisplay('r_tele_residencial', 'none');
	if (f.customerAccount.value == 'none') { changeDisplay('r_account', 'inline'); isValid = false; }
	else changeDisplay('r_account', 'none');
	if (f.beneficiario_1.value.length < 3) { changeDisplay('r_beneficiario_1', 'inline'); isValid = false; }
	else changeDisplay('r_beneficiario_1', 'none');
	if (f.beneficiario_1_1.value.length < 3) { changeDisplay('r_beneficiario_1_1', 'inline'); isValid = false; }
	else changeDisplay('r_beneficiario_1_1', 'none');
	if (!f.accept_terms.checked) { changeDisplay('r_accept_terms', 'inline'); isValid = false; }
	else changeDisplay('r_accept_terms', 'none');
	
	///Sumamos el porcentaje del beneficiario.
	var porcentaje = 0;
    for (var i=1;i<=4;i++){
    	var campo = "porcentaje_" + i;
		if (document.getElementById(campo).value != ''){
       		if(!isNaN(parseInt(document.getElementById(campo).value))){
       			porc = parseInt(document.getElementById(campo).value, 10);
				porcentaje += porc;
				document.getElementById(campo).value = porc;
        	}	
      	}
  }	
	if (porcentaje != 100){
		changeDisplay('msg_percent_error', 'block');
		isValid = false;
	}else{
		changeDisplay('msg_percent_error', 'none');	
	}

	///Fin porcentaje del beneficiario.
	
    for (var i=1;i<=4;i++){
		var beneficiario_n = "beneficiario_" + i;
		var beneficiario_a = "beneficiario_" + i + "_" + i;
		if (document.getElementById(beneficiario_n).value != ""){
			if(document.getElementById(beneficiario_a).value != ""){
			
			changeDisplay('r_beneficiario_1_1', 'none');
				
			}else{ changeDisplay('r_beneficiario_1_1', 'inline'); isValid = false; }
			
		}//end if eneficiario_n
		
  	}//End for
	
	if(document.getElementById("beneficiario_1").value != "" && document.getElementById("beneficiario_1_1").value != "" && (document.getElementById("porcentaje_1").value == "" || document.getElementById("porcentaje_1").value == "0" || document.getElementById("porcentaje_1").value == "00" || document.getElementById("porcentaje_1").value == "000")) {
		changeDisplay('msg_percent_error', 'block');
		isValid = false;
	}
	
	if(document.getElementById("beneficiario_2").value != "" && document.getElementById("beneficiario_2_2").value != "" && (document.getElementById("porcentaje_2").value == "" || document.getElementById("porcentaje_2").value == "0" || document.getElementById("porcentaje_2").value == "00" || document.getElementById("porcentaje_2").value == "000")) {
		changeDisplay('msg_percent_error', 'block');
		isValid = false;
	}
	
	if(document.getElementById("beneficiario_3").value != "" && document.getElementById("beneficiario_3_3").value != "" && (document.getElementById("porcentaje_3").value == "" || document.getElementById("porcentaje_3").value == "0" || document.getElementById("porcentaje_3").value == "00" || document.getElementById("porcentaje_3").value == "000")) {
		changeDisplay('msg_percent_error', 'block');
		isValid = false;
	}
	
	if(document.getElementById("beneficiario_4").value != "" && document.getElementById("beneficiario_4_4").value != "" && (document.getElementById("porcentaje_4").value == "" || document.getElementById("porcentaje_4").value == "0" || document.getElementById("porcentaje_4").value == "00" || document.getElementById("porcentaje_4").value == "000")) {
		changeDisplay('msg_percent_error', 'block');
		isValid = false;
	}
	
	if(!(document.getElementById("porcentaje_1").value == "" || document.getElementById("porcentaje_1").value == "0" || document.getElementById("porcentaje_1").value == "00" || document.getElementById("porcentaje_1").value == "000")  && (document.getElementById("beneficiario_1").value == "")) {
		changeDisplay('r_beneficiario_1', 'inline');
		isValid = false;
	}
	
	if(!(document.getElementById("porcentaje_1").value == "" || document.getElementById("porcentaje_1").value == "0" || document.getElementById("porcentaje_1").value == "00" || document.getElementById("porcentaje_1").value == "000")  && (document.getElementById("beneficiario_1_1").value == "")) {
		changeDisplay('r_beneficiario_1_1', 'inline');
		isValid = false;
	}
	
	if(!(document.getElementById("porcentaje_2").value == "" || document.getElementById("porcentaje_2").value == "0" || document.getElementById("porcentaje_2").value == "00" || document.getElementById("porcentaje_2").value == "000") && (document.getElementById("beneficiario_2").value == "")) {
		changeDisplay('r_beneficiario_1', 'inline');
		isValid = false;
	}
	
	if(!(document.getElementById("porcentaje_2").value == "" || document.getElementById("porcentaje_2").value == "0" || document.getElementById("porcentaje_2").value == "00" || document.getElementById("porcentaje_2").value == "000") && (document.getElementById("beneficiario_2_2").value == "")) {
		changeDisplay('r_beneficiario_1_1', 'inline');
		isValid = false;
	}
	
	if(!(document.getElementById("porcentaje_3").value == "" || document.getElementById("porcentaje_3").value == "0" || document.getElementById("porcentaje_3").value == "00" || document.getElementById("porcentaje_3").value == "000") && (document.getElementById("beneficiario_3").value == "")) {
		changeDisplay('r_beneficiario_1', 'inline');
		isValid = false;
	}
	
	if(!(document.getElementById("porcentaje_3").value == "" || document.getElementById("porcentaje_3").value == "0" || document.getElementById("porcentaje_3").value == "00" || document.getElementById("porcentaje_3").value == "000") && (document.getElementById("beneficiario_3_3").value == "")) {
		changeDisplay('r_beneficiario_1_1', 'inline');
		isValid = false;
	}
	
	if(!(document.getElementById("porcentaje_4").value == "" || document.getElementById("porcentaje_4").value == "0" || document.getElementById("porcentaje_4").value == "00" || document.getElementById("porcentaje_4").value == "000") && (document.getElementById("beneficiario_4").value == "")) {
		changeDisplay('r_beneficiario_1', 'inline');
		isValid = false;
	}
	
	if(!(document.getElementById("porcentaje_4").value == "" || document.getElementById("porcentaje_4").value == "0" || document.getElementById("porcentaje_4").value == "00" || document.getElementById("porcentaje_4").value == "000") && (document.getElementById("beneficiario_4_4").value == "")) {
		changeDisplay('r_beneficiario_1_1', 'inline');
		isValid = false;
	}
	
	if(document.getElementById('insurance-select-date-form').style.display == 'block') {
		changeDisplay('bdate-error2', 'inline');
		isValid = false;
	}
	else
		changeDisplay('bdate-error2', 'none');
	
	changeDisplay('r_dependiente_1', 'none');
	changeDisplay('r_dependiente_1_1', 'none');
	changeDisplay('r_relacion_1', 'none');
	changeDisplay('r_relacion_fecha1', 'none');
	
	changeDisplay('r_dependiente_2', 'none');
	changeDisplay('r_dependiente_2_2', 'none');
	changeDisplay('r_relacion_2', 'none');
	changeDisplay('r_relacion_fecha2', 'none');
	
	changeDisplay('r_dependiente_3', 'none');
	changeDisplay('r_dependiente_3_3', 'none');
	changeDisplay('r_relacion_3', 'none');
	changeDisplay('r_relacion_fecha3', 'none');
	
	changeDisplay('r_dependiente_4', 'none');
	changeDisplay('r_dependiente_4_4', 'none');
	changeDisplay('r_relacion_4', 'none');
	changeDisplay('r_relacion_fecha4', 'none');
	
	changeDisplay('r_dependiente_5', 'none');
	changeDisplay('r_dependiente_5_5', 'none');
	changeDisplay('r_relacion_5', 'none');
	changeDisplay('r_relacion_fecha5', 'none');
	
	changeDisplay('r_dependiente_6', 'none');
	changeDisplay('r_dependiente_6_6', 'none');
	changeDisplay('r_relacion_6', 'none');
	changeDisplay('r_relacion_fecha6', 'none');
	
	changeDisplay('r_dependiente_7', 'none');
	changeDisplay('r_dependiente_7_7', 'none');
	changeDisplay('r_relacion_7', 'none');
	changeDisplay('r_relacion_fecha7', 'none');
	
	changeDisplay('r_relacion_edad1_D', 'none');
	changeDisplay('r_relacion_edad1_T', 'none');
	changeDisplay('r_relacion_edad2_D', 'none');
	changeDisplay('r_relacion_edad2_T', 'none');
	changeDisplay('r_relacion_edad3_D', 'none');
	changeDisplay('r_relacion_edad3_T', 'none');
	changeDisplay('r_relacion_edad4_D', 'none');
	changeDisplay('r_relacion_edad4_T', 'none');
	changeDisplay('r_relacion_edad5_D', 'none');
	changeDisplay('r_relacion_edad5_T', 'none');
	changeDisplay('r_relacion_edad6_D', 'none');
	changeDisplay('r_relacion_edad6_T', 'none');
	changeDisplay('r_relacion_edad7_D', 'none');
	changeDisplay('r_relacion_edad7_T', 'none');
	
	
	if (document.getElementById('plan').value == 1) {
		if(document.getElementById('relacion_1').value != "" && document.getElementById('dependiente_1').value == "") {
			changeDisplay('r_dependiente_1', 'inline');
			isValid = false;
		}
		
		if(document.getElementById('relacion_1').value != "" && document.getElementById('dependiente_1_1').value == "") {
			changeDisplay('r_dependiente_1_1', 'inline');
			isValid = false;
		}

		if (document.getElementById('dependiente_1_1').value != "" && (document.getElementById('dependiente_1').value == "")) {
			changeDisplay('r_dependiente_1', 'inline');
			isValid = false;
		}

		if (document.getElementById('dependiente_1').value != "" && (document.getElementById('dependiente_1_1').value == "")) {
			changeDisplay('r_dependiente_1_1', 'inline');
			isValid = false;
		}

		if (document.getElementById('relacion_1').value == "" && (document.getElementById('dependiente_1_1').value != "" || document.getElementById('dependiente_1').value != "")) {
			changeDisplay('r_relacion_1', 'inline');
			isValid = false;
		}

		if ((document.getElementById('relacion_1_dia').value == 0 || document.getElementById('relacion_1_mes').value == 0 || document.getElementById('relacion_1_anio').value == 0) && (document.getElementById('dependiente_1_1').value != "" || document.getElementById('dependiente_1').value != "" || document.getElementById('relacion_1').value != "")) {
			changeDisplay('r_relacion_fecha1', 'inline');
			isValid = false;
		}
		else{
			theAge = calculateAge(document.getElementById('relacion_1_dia').value, document.getElementById('relacion_1_mes').value, document.getElementById('relacion_1_anio').value)
			if(document.getElementById('relacion_1').value == 'D'){
				if(theAge > 18){
					isValid = false;
					changeDisplay('r_relacion_edad1_D', 'inline');
				}
			}
			else if(document.getElementById('relacion_1').value == 'T'){
				if(theAge > 23){
					isValid = false;
					changeDisplay('r_relacion_edad1_T', 'inline');	
				}
			}
		}
		
		
		if(document.getElementById('relacion_2').value != "" && document.getElementById('dependiente_2').value == "") {
			changeDisplay('r_dependiente_2', 'inline');
			isValid = false;
		}
		
		if(document.getElementById('relacion_2').value != "" && document.getElementById('dependiente_2_2').value == "") {
			changeDisplay('r_dependiente_2_2', 'inline');
			isValid = false;
		}

		if (document.getElementById('dependiente_2_2').value != "" && (document.getElementById('dependiente_2').value == "")) {
			changeDisplay('r_dependiente_2', 'inline');
			isValid = false;
		}

		if (document.getElementById('dependiente_2').value != "" && (document.getElementById('dependiente_2_2').value == "")) {
			changeDisplay('r_dependiente_2_2', 'inline');
			isValid = false;
		}

		if (document.getElementById('relacion_2').value == "" && (document.getElementById('dependiente_2_2').value != "" || document.getElementById('dependiente_2').value != "")) {
			changeDisplay('r_relacion_2', 'inline');
			isValid = false;
		}

		if ((document.getElementById('relacion_2_dia').value == 0 || document.getElementById('relacion_2_mes').value == 0 || document.getElementById('relacion_2_anio').value == 0) && (document.getElementById('dependiente_2_2').value != "" || document.getElementById('dependiente_2').value != "" || document.getElementById('relacion_2').value != "")) {
			changeDisplay('r_relacion_fecha2', 'inline');
			isValid = false;
		}
		else{
			theAge = calculateAge(document.getElementById('relacion_2_dia').value, document.getElementById('relacion_2_mes').value, document.getElementById('relacion_2_anio').value)
			if(document.getElementById('relacion_2').value == 'D'){
				if(theAge > 18){
					isValid = false;
					changeDisplay('r_relacion_edad2_D', 'inline');
				}
			}
			else if(document.getElementById('relacion_2').value == 'T'){
				if(theAge > 23){
					isValid = false;
					changeDisplay('r_relacion_edad2_T', 'inline');
				}
			}
		}
		
		
		
        if(document.getElementById('relacion_3').value != "" && document.getElementById('dependiente_3').value == "") {
			changeDisplay('r_dependiente_3', 'inline');
			isValid = false;
		}
		
		if(document.getElementById('relacion_3').value != "" && document.getElementById('dependiente_3_3').value == "") {
			changeDisplay('r_dependiente_3_3', 'inline');
			isValid = false;
		}
		
		if (document.getElementById('dependiente_3_3').value != "" && (document.getElementById('dependiente_3').value == "")) {
			changeDisplay('r_dependiente_3', 'inline');
			isValid = false;
		}

		if (document.getElementById('dependiente_3').value != "" && (document.getElementById('dependiente_3_3').value == "")) {
			changeDisplay('r_dependiente_3_3', 'inline');
			isValid = false;
		}

		if (document.getElementById('relacion_3').value == "" && (document.getElementById('dependiente_3_3').value != "" || document.getElementById('dependiente_3').value != "")) {
			changeDisplay('r_relacion_3', 'inline');
			isValid = false;
		}

		if ((document.getElementById('relacion_3_dia').value == 0 || document.getElementById('relacion_3_mes').value == 0 || document.getElementById('relacion_3_anio').value == 0) && (document.getElementById('dependiente_3_3').value != "" || document.getElementById('dependiente_3').value != "" || document.getElementById('relacion_3').value != "")) {
			changeDisplay('r_relacion_fecha3', 'inline');
			isValid = false;
		}
		else{
			theAge = calculateAge(document.getElementById('relacion_3_dia').value, document.getElementById('relacion_3_mes').value, document.getElementById('relacion_3_anio').value)
			if(document.getElementById('relacion_3').value == 'D'){
				if(theAge > 18){
					isValid = false;
					changeDisplay('r_relacion_edad3_D', 'inline');
				}
			}
			else if(document.getElementById('relacion_3').value == 'T'){
				if(theAge > 23){
					isValid = false;
					changeDisplay('r_relacion_edad3_T', 'inline');
				}
			}
		}
		
		if(document.getElementById('relacion_4').value != "" && document.getElementById('dependiente_4').value == "") {
			changeDisplay('r_dependiente_4', 'inline');
			isValid = false;
		}
		
		if(document.getElementById('relacion_4').value != "" && document.getElementById('dependiente_4_4').value == "") {
			changeDisplay('r_dependiente_4_4', 'inline');
			isValid = false;
		}

		if (document.getElementById('dependiente_4_4').value != "" && (document.getElementById('dependiente_4').value == "")) {
			changeDisplay('r_dependiente_4', 'inline');
			isValid = false;
		}

		if (document.getElementById('dependiente_4').value != "" && (document.getElementById('dependiente_4_4').value == "")) {
			changeDisplay('r_dependiente_4_4', 'inline');
			isValid = false;
		}

		if (document.getElementById('relacion_4').value == "" && (document.getElementById('dependiente_4_4').value != "" || document.getElementById('dependiente_4').value != "")) {
			changeDisplay('r_relacion_4', 'inline');
			isValid = false;
		}

		if ((document.getElementById('relacion_4_dia').value == 0 || document.getElementById('relacion_4_mes').value == 0 || document.getElementById('relacion_4_anio').value == 0) && (document.getElementById('dependiente_4_4').value != "" || document.getElementById('dependiente_4').value != "" || document.getElementById('relacion_4').value != "")) {
			changeDisplay('r_relacion_fecha4', 'inline');
			isValid = false;
		}
		else{
			theAge = calculateAge(document.getElementById('relacion_4_dia').value, document.getElementById('relacion_4_mes').value, document.getElementById('relacion_4_anio').value)
			if(document.getElementById('relacion_4').value == 'D'){
				if(theAge > 18){
					isValid = false;
					changeDisplay('r_relacion_edad4_D', 'inline');
				}
			}
			else if(document.getElementById('relacion_4').value == 'T'){
				if(theAge > 23){
					isValid = false;
					changeDisplay('r_relacion_edad4_T', 'inline');
				}
			}
		}
		
		if(document.getElementById('relacion_5').value != "" && document.getElementById('dependiente_5').value == "") {
			changeDisplay('r_dependiente_5', 'inline');
			isValid = false;
		}
		
		if(document.getElementById('relacion_5').value != "" && document.getElementById('dependiente_5_5').value == "") {
			changeDisplay('r_dependiente_5_5', 'inline');
			isValid = false;
		}

		if (document.getElementById('dependiente_5_5').value != "" && (document.getElementById('dependiente_5').value == "")) {
			changeDisplay('r_dependiente_5', 'inline');
			isValid = false;
		}

		if (document.getElementById('dependiente_5').value != "" && (document.getElementById('dependiente_5_5').value == "")) {
			changeDisplay('r_dependiente_5_5', 'inline');
			isValid = false;
		}

		if (document.getElementById('relacion_5').value == "" && (document.getElementById('dependiente_5_5').value != "" || document.getElementById('dependiente_5').value != "")) {
			changeDisplay('r_relacion_5', 'inline');
			isValid = false;
		}

		if ((document.getElementById('relacion_5_dia').value == 0 || document.getElementById('relacion_5_mes').value == 0 || document.getElementById('relacion_5_anio').value == 0) && (document.getElementById('dependiente_5_5').value != "" || document.getElementById('dependiente_5').value != "" || document.getElementById('relacion_5').value != "")) {
			changeDisplay('r_relacion_fecha5', 'inline');
			isValid = false;
		}		else{
			theAge = calculateAge(document.getElementById('relacion_5_dia').value, document.getElementById('relacion_5_mes').value, document.getElementById('relacion_5_anio').value)
			if(document.getElementById('relacion_5').value == 'D'){
				if(theAge > 18){
					isValid = false;
					changeDisplay('r_relacion_edad5_D', 'inline');
				}
			}
			else if(document.getElementById('relacion_5').value == 'T'){
				if(theAge > 23){
					isValid = false;
					changeDisplay('r_relacion_edad5_T', 'inline');
				}
			}
		}
		
		
		if(document.getElementById('relacion_6').value != "" && document.getElementById('dependiente_6').value == "") {
			changeDisplay('r_dependiente_6', 'inline');
			isValid = false;
		}
		
		if(document.getElementById('relacion_6').value != "" && document.getElementById('dependiente_6_6').value == "") {
			changeDisplay('r_dependiente_6_6', 'inline');
			isValid = false;
		}

		if (document.getElementById('dependiente_6_6').value != "" && (document.getElementById('dependiente_6').value == "")) {
			changeDisplay('r_dependiente_6', 'inline');
			isValid = false;
		}

		if (document.getElementById('dependiente_6').value != "" && (document.getElementById('dependiente_6_6').value == "")) {
			changeDisplay('r_dependiente_6_6', 'inline');
			isValid = false;
		}

		if (document.getElementById('relacion_6').value == "" && (document.getElementById('dependiente_6_6').value != "" || document.getElementById('dependiente_6').value != "")) {
			changeDisplay('r_relacion_6', 'inline');
			isValid = false;
		}

		if ((document.getElementById('relacion_6_dia').value == 0 || document.getElementById('relacion_6_mes').value == 0 || document.getElementById('relacion_6_anio').value == 0) && (document.getElementById('dependiente_6_6').value != "" || document.getElementById('dependiente_6').value != "" || document.getElementById('relacion_6').value != "")) {
			changeDisplay('r_relacion_fecha6', 'inline');
			isValid = false;
		}
		else{
			theAge = calculateAge(document.getElementById('relacion_6_dia').value, document.getElementById('relacion_6_mes').value, document.getElementById('relacion_6_anio').value)
			if(document.getElementById('relacion_6').value == 'D'){
				if(theAge > 18){
					isValid = false;
					changeDisplay('r_relacion_edad6_D', 'inline');
					
				}
			}
			else if(document.getElementById('relacion_6').value == 'T'){
				if(theAge > 23){
					isValid = false;
					changeDisplay('r_relacion_edad6_T', 'inline');
				}
			}
		}
		
		if(document.getElementById('relacion_7').value != "" && document.getElementById('dependiente_7').value == "") {
			changeDisplay('r_dependiente_7', 'inline');
			isValid = false;
		}
		
		if(document.getElementById('relacion_7').value != "" && document.getElementById('dependiente_7_7').value == "") {
			changeDisplay('r_dependiente_7_7', 'inline');
			isValid = false;
		}

		if (document.getElementById('dependiente_7_7').value != "" && (document.getElementById('dependiente_7').value == "")) {
			changeDisplay('r_dependiente_7', 'inline');
			isValid = false;
		}

		if (document.getElementById('dependiente_7').value != "" && (document.getElementById('dependiente_7_7').value == "")) {
			changeDisplay('r_dependiente_7_7', 'inline');
			isValid = false;
		}

		if (document.getElementById('relacion_7').value == "" && (document.getElementById('dependiente_7_7').value != "" || document.getElementById('dependiente_7').value != "")) {
			changeDisplay('r_relacion_7', 'inline');
			isValid = false;
		}

		if ((document.getElementById('relacion_7_dia').value == 0 || document.getElementById('relacion_7_mes').value == 0 || document.getElementById('relacion_7_anio').value == 0) && (document.getElementById('dependiente_7_7').value != "" || document.getElementById('dependiente_7').value != "" || document.getElementById('relacion_7').value != "")) {
			changeDisplay('r_relacion_fecha7', 'inline');
			isValid = false;
		}
		else{
			theAge = calculateAge(document.getElementById('relacion_7_dia').value, document.getElementById('relacion_7_mes').value, document.getElementById('relacion_7_anio').value)
			if(document.getElementById('relacion_7').value == 'D'){
				if(theAge > 18){
					isValid = false;
					changeDisplay('r_relacion_edad7_D', 'inline');
				}
			}
			else if(document.getElementById('relacion_7').value == 'T'){
				if(theAge > 23){
					isValid = false;
					changeDisplay('r_relacion_edad7_T', 'inline');
				}
			}
		}

		changeDisplay('onespouse', 'none');
		changeDisplay('spousenodep', 'none');
		changeDisplay('depnospouse', 'none');
		changeDisplay('spouseanddep', 'none');
		
		var f = document.getElementById('wcForm').monthly_premium.value;
		var r1 = document.getElementById('relacion_1').value;
		var r2 = document.getElementById('relacion_2').value;
		var r3 = document.getElementById('relacion_3').value;
		var r4 = document.getElementById('relacion_4').value;
		var r5 = document.getElementById('relacion_5').value;
		var r6 = document.getElementById('relacion_6').value;
		var r7 = document.getElementById('relacion_7').value;
		
		var fn1 = document.getElementById('dependiente_1').value;
		var ln1 = document.getElementById('dependiente_1_1').value;
		var fn2 = document.getElementById('dependiente_2').value;
		var ln2 = document.getElementById('dependiente_2_2').value;
		var fn3 = document.getElementById('dependiente_3').value;
		var ln3 = document.getElementById('dependiente_3_3').value;
		var fn4 = document.getElementById('dependiente_4').value;
		var ln4 = document.getElementById('dependiente_4_4').value;
		var fn5 = document.getElementById('dependiente_5').value;
		var ln5 = document.getElementById('dependiente_5_5').value;
		var fn6 = document.getElementById('dependiente_6').value;
		var ln6 = document.getElementById('dependiente_6_6').value;
		var fn7 = document.getElementById('dependiente_7').value;
		var ln7 = document.getElementById('dependiente_7_7').value;
			
		var spouseCount = 0;
		if(r1=="S" && fn1!="" && ln1!="") spouseCount++;
		if(r2=="S" && fn2!="" && ln2!="") spouseCount++;
		if(r3=="S" && fn3!="" && ln3!="") spouseCount++;
		if(r4=="S" && fn4!="" && ln4!="") spouseCount++;
		if(r5=="S" && fn5!="" && ln5!="") spouseCount++;
		if(r6=="S" && fn6!="" && ln6!="") spouseCount++;
		if(r7=="S" && fn7!="" && ln7!="") spouseCount++;
			
		var depCount = 0;
		if((r1=="D" || r1=="T") && fn1!="" && ln1!="") depCount++;
		if((r2=="D" || r2=="T") && fn2!="" && ln2!="") depCount++;
		if((r3=="D" || r3=="T") && fn3!="" && ln3!="") depCount++;
		if((r4=="D" || r4=="T") && fn4!="" && ln4!="") depCount++;
		if((r5=="D" || r5=="T") && fn5!="" && ln5!="") depCount++;
		if((r6=="D" || r6=="T") && fn6!="" && ln6!="") depCount++;
		if((r7=="D" || r7=="T") && fn7!="" && ln7!="") depCount++;

		if(spouseCount > 1) {
			changeDisplay('onespouse', 'inline');
			isValid = false;
		}
		else {
			if(spouseCount == 0 || depCount == 0) {
				changeDisplay('spouseanddep', 'inline');
				isValid = false;
			}
		}
	}


	if (!isValid) document.location.href='#formstart';
	return isValid;
}

function selectedPlanChanged(theValue, isFamily) {
	var f = document.getElementById('wcForm');
	mp = theValue;
	f.monthly_premium.value = mp;
	changeText('monthly-debit', mp);
	document.getElementById('plan').value = isFamily;
	if(isFamily)
		document.getElementById('dependent-table').style.display = '';
	else
		document.getElementById('dependent-table').style.display = 'none';
}

function displayBdateError() {
	changeDisplay('bdate-error', 'inline');
	setTimeout("changeDisplay('bdate-error', 'none')", 3000);
}

function displayBdate(theLang) {
	var f = document.getElementById('wcForm');
	var bday = f.birthday_day.value;
	var bmo = f.birthday_month.value;
	var byr = f.birthday_year.value;
	var longBDate = "";
	if (theLang == 'es') longBDate = bday+' de '+getMonthName(bmo, theLang)+' de '+byr;
	else longBDate = getMonthName(bmo, theLang)+' '+bday+', '+byr;
}

function displayPremiumBasedOnAge(theAge) {
	if (theAge < 35) {
		changeText('premium-1', '$7.95');
		changeText('premium-2', '$16.00');
		changeText('premium-3', '$23.75');
	} else if (theAge >= 35 && theAge <= 44) {
		changeText('premium-1', '$9.75');
		changeText('premium-2', '$19.25');
		changeText('premium-3', '$28.75');
	} else if (theAge >= 45 && theAge <= 54) {
		changeText('premium-1', '$12.00');
		changeText('premium-2', '$23.75');
		changeText('premium-3', '$35.75');
	} else if (theAge >= 55 && theAge <= 64) {
		changeText('premium-1', '$13.25');
		changeText('premium-2', '$26.25');
		changeText('premium-3', '$39.50');
	} else {
		changeText('premium-1', '$');
		changeText('premium-2', '$');
		changeText('premium-3', '$');
	}
}

function getMonthName(theMonth, theLang) {
	if (theLang == 'es') {
		switch(theMonth) {
			case '1': return 'enero'; break;
			case '2': return 'febrero'; break;
			case '3': return 'marzo'; break;
			case '4': return 'abril'; break;
			case '5': return 'mayo'; break;
			case '6': return 'junio'; break;
			case '7': return 'julio'; break;
			case '8': return 'agosto'; break;
			case '9': return 'septiembre'; break;
			case '10': return 'octubre'; break;
			case '11': return 'noviembre'; break;
			case '12': return 'diciembre'; break;
			default: return ''; break;
		}
	} else {
		switch(theMonth) {
			case '1': return 'January'; break;
			case '2': return 'February'; break;
			case '3': return 'March'; break;
			case '4': return 'April'; break;
			case '5': return 'May'; break;
			case '6': return 'June'; break;
			case '7': return 'July'; break;
			case '8': return 'August'; break;
			case '9': return 'September'; break;
			case '10': return 'October'; break;
			case '11': return 'November'; break;
			case '12': return 'December'; break;
			default: return ''; break;
		}
	}
}

function calculateAge(theDay, theMonth, theYear) {
	var bday = parseInt(theDay);
	var bmo = (parseInt(theMonth)-1);
	var byr = parseInt(theYear);
	if (isNaN(bday)||isNaN(bmo)||isNaN(byr)) return 'x';
	var age;
	var now = new Date();
	tday = now.getDate();
	tmo = now.getMonth();
	tyr = now.getFullYear();
	if((tmo > bmo)||(tmo==bmo & tday>=bday)) age=byr;
	else age=byr+1;
	return tyr-age;
}

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if(charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}
