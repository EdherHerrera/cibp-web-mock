function changeAccountType(acct) {
	var value = acct.options[acct.selectedIndex].value;
	if (value == 'acc') {
		showIDA();
	} else if (value == 'vm') {
		showCCA();
	} else if (value == 'amex') {
		showAMEX();
	} else if (value == 'redi') {
		showREDI();
	} else if (value == 'mla') {
		showMLA();
	} else if (value == 'auto') {
		showAuto();
	}
}

function showIDA() {
	$('acc-opt').style.display = 'block';
	if ($('vm-opt')) {
		$('vm-opt').style.display = 'none';
	}
	if ($('amex-opt')) {
		$('amex-opt').style.display = 'none';
	}
	if ($('redi-opt')) {
		$('redi-opt').style.display = 'none';
	}
	if ($('mla-opt')) {
		$('mla-opt').style.display = 'none';
	}
	if ($('auto-opt')) {
		$('auto-opt').style.display = 'none';
	}
}

function showCCA() {
	$('vm-opt').style.display = 'block';
	if ($('acc-opt')) {
		$('acc-opt').style.display = 'none';
	}
	if ($('amex-opt')) {
		$('amex-opt').style.display = 'none';
	}
	if ($('redi-opt')) {
		$('redi-opt').style.display = 'none';
	}
	if ($('mla-opt')) {
		$('mla-opt').style.display = 'none';
	}
	if ($('auto-opt')) {
		$('auto-opt').style.display = 'none';
	}
}

function showAMEX() {
	$('amex-opt').style.display = 'block';
	if ($('acc-opt')) {
		$('acc-opt').style.display = 'none';
	}
	if ($('vm-opt')) {
		$('vm-opt').style.display = 'none';
	}
	if ($('redi-opt')) {
		$('redi-opt').style.display = 'none';
	}
	if ($('mla-opt')) {
		$('mla-opt').style.display = 'none';
	}
	if ($('auto-opt')) {
		$('auto-opt').style.display = 'none';
	}
}

function showREDI() {
	$('redi-opt').style.display = 'block';
	if ($('acc-opt')) {
		$('acc-opt').style.display = 'none';
	}
	if ($('vm-opt')) {
		$('vm-opt').style.display = 'none';
	}
	if ($('amex-opt')) {
		$('amex-opt').style.display = 'none';
	}
	if ($('mla-opt')) {
		$('mla-opt').style.display = 'none';
	}
	if ($('auto-opt')) {
		$('auto-opt').style.display = 'none';
	}
}

function showMLA() {

	$('mla-opt').style.display = 'block';
	if ($('acc-opt')) {
		$('acc-opt').style.display = 'none';
	}
	if ($('vm-opt')) {
		$('vm-opt').style.display = 'none';
	}
	if ($('redi-opt')) {
		$('redi-opt').style.display = 'none';
	}
	if ($('amex-opt')) {
		$('amex-opt').style.display = 'none';
	}
	if ($('auto-opt')) {
		$('auto-opt').style.display = 'none';
	}
}

function showAuto() {
	
	$('auto-opt').style.display = 'block';
	if ($('acc-opt')) {
		$('acc-opt').style.display = 'none';
	}
	if ($('vm-opt')) {
		$('vm-opt').style.display = 'none';
	}
	if ($('redi-opt')) {
		$('redi-opt').style.display = 'none';
	}
	if ($('amex-opt')) {
		$('amex-opt').style.display = 'none';
	}
	if ($('mla-opt')) {
		$('mla-opt').style.display = 'none';
	}
}
	