/*GDPR Ajax Call*/
function switchChange(id) {
	var idJQ = '#' + id; 

	if ($(idJQ).prop("checked")) {
		updateCookiePrivacySettingsAjax(id, 'ENABLED');
	} else {
		updateCookiePrivacySettingsAjax(id, 'DISABLED');
	}
}

function updateCookiePrivacySettingsAjax(category, status) {

	jQuery.ajax({
		type : 'POST',
		url : 'updateCookiePrivacySettings',
		data : {
			cookieCategory : category,
			status : status
		}
	});
}

$(document).ready(function() {
	
	const checkedAnalytic = $('#swAnalytic');
	const checkedFunctional = $('#swFunctional');
	const checkedAdvertising = $('#swAdvertising');
	
	if(checkedAnalytic.val() === 'true') {
		$("#swAnalytic").attr("checked", "checked");
	}
	
	if(checkedFunctional.val() === 'true') {
		$("#swFunctional").attr("checked", "checked");
	}
	
	if(checkedAdvertising.val() === 'true') {
		$("#swAdvertising").attr("checked", "checked");
	}
});