"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var popCK =
/*#__PURE__*/
function () {
  function popCK() {
    _classCallCheck(this, popCK);

    var instance = this.constructor.instance;

    if (instance) {
      return instance;
    }

    this.constructor.instance = this;
    this._data = [];
  }

  _createClass(popCK, [{
    key: "acceptCookie",
    value: function acceptCookie() {
      document.getElementById("alert-dmp").classList.remove("slideInUp");
      document.getElementById("alert-dmp").classList.add("slideOutDown");
      document.getElementById("overlay-alert").classList.remove("show");
      popCK.instance.setCookie('bppr_dmp', '1', 365);
      var destroyAlert = setTimeout(function () {
        popCK.instance._data["active"].focus();

        document.getElementById("alert-dmp").remove();
        document.body.classList.remove("modal-open");
        document.removeEventListener('keydown', popCK.instance.handleKeyDown);
      }, 1000);
      location.reload();
    }
  }, {
    key: "handleKeyDown",
    value: function handleKeyDown(e) {
      var acceptCookieBtn = document.getElementById("btn-dmp-continue");
      var linkPDFDocument = document.querySelector("popup-cookieinfo a[class='dmp_lnk']");
      var bannerPopCK = document.getElementById("popup-cookieinfo");
      var bannerBack = document.getElementById("overlay-alert");

      if (e.target === acceptCookieBtn) {
        if (e.shiftKey && e.keyCode === 9 || e.keyCode === 9) {
          e.preventDefault();
          linkPDFDocument.focus();
        } else if (e.keyCode === 13) {
          e.preventDefault();
          popCK.instance.acceptCookie();
        }
      } else if (e.target === linkPDFDocument) {
        if (e.shiftKey && e.keyCode === 9) {
          e.preventDefault();
          acceptCookieBtn.focus();
        } else if (e.keyCode === 13) {
          e.preventDefault();
          linkPDFDocument.click();
        } else if (e.keyCode === 9) {
          e.preventDefault();
          acceptCookieBtn.focus();
        }
      } else if (e.target === bannerPopCK || e.target === bannerBack || e.target === bannerPopCK.parentElement) {
        if (e.shiftKey && e.keyCode === 9) {
          e.preventDefault();
          acceptCookieBtn.focus();
        } else if (e.keyCode === 9) {
          e.preventDefault();
          linkPDFDocument.focus();
        }
      }
    }
  }, {
    key: "showDmpAlert",
    value: function showDmpAlert() {
      this._data['active'] = document.activeElement;
      document.body.classList.add("modal-open");
      var bannerPopCK = document.getElementById("popup-cookieinfo");
      bannerPopCK.classList.remove("hide");
      var bannerAlert = document.getElementById("alert-dmp");
      bannerAlert.classList.remove("hide");
      bannerAlert.classList.add("slideInUp");
      bannerAlert.setAttribute("aria-hidden", "false");
      document.getElementById("overlay-alert").classList.add("show");
      document.addEventListener('keydown', popCK.instance.handleKeyDown);
    }
  }, {
    key: "getCookie",
    value: function getCookie(cname) {
        var v = document.cookie.match('(^|;) ?' + cname + '=([^;]*)(;|$)');
        return v ? v[2] : null;
    }
  }, {
    key: "setCookie",
    value: function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
      var expires = "expires=" + d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
  }, {
    key: "watchToChange",
    value: function watchToChange(target) {
      var observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
          if (mutation.target === document.body && mutation.removedNodes.length > 0 && mutation.removedNodes[0].id === "popup-cookieinfo") {
            popCK.instance.setCookie('bppr_dmp', '1', 365);
            document.removeEventListener('keydown', popCK.instance.handleKeyDown);
          } else if (mutation.target.id === "popup-cookieinfo") {
            popCK.instance.setCookie('bppr_dmp', '1', 365);
            document.removeEventListener('keydown', popCK.instance.handleKeyDown);
          }
        });
      }); // configuration of the observer:

      var config = {
        attributes: false,
        childList: true,
        characterData: false
      }; // pass in the target node, as well as the observer options

      observer.observe(target, config);
    }
  }]);

  return popCK;
}();

document.addEventListener('DOMContentLoaded', function (event) {
  var popCKInstance = new popCK();
  Object.freeze(popCKInstance); //export default popCKInstance;

  document.getElementById("btn-dmp-continue").addEventListener("click", popCKInstance.acceptCookie);

  if (popCKInstance.getCookie('bppr_dmp') === '') {
    var popupck = document.getElementsByTagName('popup-cookieinfo');
    var bodyEl = document.getElementsByTagName('body');
    if (popupck.length > 0) {
      bodyEl[0].appendChild(popupck[0]);
    }
    popCKInstance.showDmpAlert();
    popCKInstance.watchToChange(popupck[0]);
    popCKInstance.watchToChange(document.body);
  } else if (popCKInstance.getCookie('bppr_dmp') === '' && popCKInstance.getCookie('PMData') !== '' && popCKInstance.getCookie('MB_DEV') !== '') {
    popCKInstance.setCookie('bppr_dmp', '1', 365);
  }
});

function customizeCookies() {
  popCK.instance.setCookie('bppr_dmp', '1', 365);
  var form = document.createElement("form");
  form.method = "POST";
  form.action = "gdprPrivacySettings";
  document.body.appendChild(form);
  form.submit();
}