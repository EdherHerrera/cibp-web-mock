class popCK {
    constructor() {
        const instance = this.constructor.instance;
        if(instance) {
            return instance;
        }
        this.constructor.instance = this;
        this._data = [];
    }
    acceptCookie() {
        document.getElementById("alert-dmp").classList.remove("slideInUp");
        document.getElementById("alert-dmp").classList.add("slideOutDown");
        document.getElementById("overlay-alert").classList.remove("show");
        popCK.instance.setCookie('bppr_dmp', '1', 365);
        var destroyAlert = setTimeout(function() {
        	popCK.instance._data["active"].focus();
            document.getElementById("alert-dmp").remove();
            document.body.classList.remove("modal-open");
            document.removeEventListener('keydown', popCK.instance.handleKeyDown);
        }, 1000);
        location.reload();
    }
    
    handleKeyDown(e) {
    	const acceptCookieBtn = document.getElementById("btn-dmp-continue");
    	const linkPDFDocument = document.querySelector("popup-cookieinfo a[class='dmp_lnk']");
    	const bannerPopCK = document.getElementById("popup-cookieinfo");
    	const bannerBack = document.getElementById("overlay-alert");
    	 if (e.target === acceptCookieBtn) {
        	 if ( (e.shiftKey && e.keyCode === 9) || e.keyCode === 9) {
                 e.preventDefault();
                 linkPDFDocument.focus();
             } else if (e.keyCode === 13) {                 
                 e.preventDefault();              
                 popCK.instance.acceptCookie();
             }
        } else if (e.target === linkPDFDocument) {
        	 if (e.shiftKey && e.keyCode === 9) {
                 e.preventDefault()
                 acceptCookieBtn.focus();
             } else if (e.keyCode === 13) {                 
                 e.preventDefault();
                 linkPDFDocument.click();
             } else if (e.keyCode === 9) {
                 e.preventDefault();
                 acceptCookieBtn.focus();
             }
        } else if (e.target === bannerPopCK 
        		|| e.target === bannerBack
        		|| e.target === bannerPopCK.parentElement) {
       	 if (e.shiftKey && e.keyCode === 9) {
             e.preventDefault()
             acceptCookieBtn.focus();
         }  else if (e.keyCode === 9) {
             e.preventDefault();
             linkPDFDocument.focus();
         } 
    }
    	 
    }
    showDmpAlert() {
    	this._data['active'] = document.activeElement;
	    document.body.classList.add("modal-open");
	    const bannerPopCK = document.getElementById("popup-cookieinfo");
	    bannerPopCK.classList.remove("hide");
	    const bannerAlert = document.getElementById("alert-dmp");
	    bannerAlert.classList.remove("hide");
	    bannerAlert.classList.add("slideInUp");
	    bannerAlert.setAttribute("aria-hidden", "false");
	    document.getElementById("overlay-alert").classList.add("show");
	    document.addEventListener('keydown', popCK.instance.handleKeyDown);
    }
    
    getCookie(cname) {
	    var name = cname + "=";
	    var ca = document.cookie.split(';');
	    for (var i = 0; i < ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0) == ' ') {
	            c = c.substring(1);
	        }
	        if (c.indexOf(name) == 0) {
	            return c.substring(name.length, c.length);
	        }
	    }
	    return "";
    }
    
    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    
    watchToChange (target) {
    	var observer = new MutationObserver(function (mutations) {
		    mutations.forEach(function (mutation) {
		    	if(mutation.target === document.body 
		    			&& mutation.removedNodes.length > 0 && mutation.removedNodes[0].id === "popup-cookieinfo") {
		    		popCK.instance.setCookie('bppr_dmp', '1', 365);
		    		document.removeEventListener('keydown', popCK.instance.handleKeyDown);
		    	} else if(mutation.target.id === "popup-cookieinfo") {
		    		popCK.instance.setCookie('bppr_dmp', '1', 365);
		    		document.removeEventListener('keydown', popCK.instance.handleKeyDown);
		    	}
		    	
		    });
		});

		// configuration of the observer:
		var config = {
		    attributes: false,
		    childList: true,
		    characterData: false
		};

		// pass in the target node, as well as the observer options
		observer.observe(target, config);
    }

}


document.addEventListener('DOMContentLoaded', function(event) {
 
    const popCKInstance = new popCK();
    Object.freeze(popCKInstance);
    //export default popCKInstance;
   
    document.getElementById("btn-dmp-continue").addEventListener("click", popCKInstance.acceptCookie);

    if (popCKInstance.getCookie('bppr_dmp') === '') {
        var popupck = document.getElementsByTagName('popup-cookieinfo');
    	var bodyEl = document.getElementsByTagName('body');
    	if (popupck.length > 0) {
    	   bodyEl[0].appendChild(popupck[0]);
    	}
        popCKInstance.showDmpAlert();
        popCKInstance.watchToChange(popupck[0]);
        popCKInstance.watchToChange(document.body);
    } else if (popCKInstance.getCookie('bppr_dmp') === '' && popCKInstance.getCookie('PMData') !== '' && popCKInstance.getCookie('MB_DEV') !== '') {
        popCKInstance.setCookie('bppr_dmp', '1', 365);
    }
});

function customizeCookies() {
    popCK.instance.setCookie('bppr_dmp', '1', 365);
    var form = document.createElement("form");
    form.method = "POST";
    form.action = "gdprPrivacySettings";
    document.body.appendChild(form);
    form.submit();
}
