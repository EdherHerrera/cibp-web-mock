var divs = new Array();
divs[0] = 'util';
divs[1] = 'cable';
divs[2] = 'cells';
divs[3] = 'credits';
divs[4] = 'loan';
divs[5] = 'gov';

var cabledivs = new Array();
cabledivs[0] = 'liberty';

var celldivs = new Array();
celldivs[0] = 'sprint';

var creditdivs = new Array();
creditdivs[0] = 'reserv';
creditdivs[1] = 'city';
creditdivs[2] = 'gap';
creditdivs[3] = 'mbna';
creditdivs[4] = 'sams';
creditdivs[5] = 'penny';
creditdivs[6] = 'sears';


var loandivs = new Array();
loandivs[0] = 'mla';
loandivs[1] = 'personal';
loandivs[2] = 'lease';
loandivs[3] = 'finance';
loandivs[4] = 'ford';

function hideDivs(idx) {
	// Hide all the divs
	if ($(divs[idx]).style.display != 'none') {
		var i = 0;
		for (i = 0; i < divs.length; i++) {
			if (i != idx) {
				
				if($(divs[i]).style.display != "none") {
					
					$(divs[i]).style.display = 'none';
					
					switch(i) {
						
						case 0:
							$('plus').toggle();
							$('minus').toggle();
							break;
							
						case 1:
							$('plus2').toggle();
							$('minus2').toggle();
							break;
							
						case 2:
							$('plus5').toggle();
							$('minus5').toggle();
							break;
							
						case 3:
							$('plus7').toggle();
							$('minus7').toggle();
							break;
							
						case 4:
							$('plus16').toggle();
							$('minus16').toggle();
							break;
							
						case 5:
							$('plus22').toggle();
							$('minus22').toggle();
							break;
					}
					
					hideChildDivs(i);
				}
			}
			else
				hideChildDivs(i);
		}
	}
}

function hideChildDivs(idx) {
	// Based on the index, hide the appropriate children
	switch (idx) {
	
		case 1:
			hideCableDivs(-1);
			break;
			
		case 2:
			hideCellsDivs(-1);
			break;
			
		case 3:
			hideCreditDivs(-1);
			break;
			
		case 4:
			hideLoanDivs(-1);
			break;
			
	}
}

function hideCableDivs(idx) {
	var i = 0;
	for (i = 0; i < cabledivs.length; i++) {
		if (i != idx) {
			
			if ($(cabledivs[i]).style.display != "none") {
				$(cabledivs[i]).style.display = 'none';
				
				switch (i) {
				
					case 0:
						$('plus3').toggle();
						$('minus3').toggle();
						break;
				
				}
			}
		}
	}
}

function hideCellsDivs(idx){
	var i = 0;
	for (i = 0; i < celldivs.length; i++) {
		if (i != idx) {
			if ($(celldivs[i]).style.display != "none") {
				$(celldivs[i]).style.display = 'none';
				
				switch (i) {
				
					case 0:
						$('plus6').toggle();
						$('minus6').toggle();
						break;
				}
			}
		}
	}
}

function hideCreditDivs(idx){
	var i = 0;
	for (i = 0; i < creditdivs.length; i++) {

		if (i != idx) {
			if ($(creditdivs[i]).style.display != "none") {
				$(creditdivs[i]).style.display = 'none';
				
				switch (i) {
				
					case 0:
						$('plus9').toggle();
						$('minus9').toggle();
						break;
						
					case 1:
						$('plus10').toggle();
						$('minus10').toggle();
						break;
					case 2:
						$('plus23').toggle();
						$('minus23').toggle();
						break;						
					case 3:
						$('plus11').toggle();
						$('minus11').toggle();
						break;
						
					case 4:
						$('plus13').toggle();
						$('minus13').toggle();
						break;
						
					case 5:
						$('plus14').toggle();
						$('minus14').toggle();
						break;
						
					case 6:
						$('plus15').toggle();
						$('minus15').toggle();
						break;

				}
			}
		}
	}
}

function hideLoanDivs(idx){
	var i = 0;
	for (i = 0; i < loandivs.length; i++) {
		if (i != idx) {
			if ($(loandivs[i]).style.display != "none") {
				$(loandivs[i]).style.display = 'none';
				
				switch (i) {
				
					case 0:
						$('plus17').toggle();
						$('minus17').toggle();
						break;
						
					case 1:
						$('plus18').toggle();
						$('minus18').toggle();
						break;
						
					case 2:
						$('plus19').toggle();
						$('minus19').toggle();
						break;
						
					case 3:
						$('plus20').toggle();
						$('minus20').toggle();
						break;
						
					case 4:
						$('plus21').toggle();
						$('minus21').toggle();
						break;
				}
			}
		}
	}
}

