function searchTransactions()
{
	$('action').value = 'SEARCH';
	
	document.athmovil.submit();
}

function addFriend()
{
	$('action').value = 'ADDFRIEND';
	
	document.athmovil.submit();
}

function toggleAddFriend()
{
	if(!$('addFriend').visible())
	{
		$('friendName').value = "";	
		$('friendNameError').update();

		$('friendPhone').value = "";
		$('friendPhoneError').update();
	}
	
	$('addFriend').toggle();
	$('plus').toggle();
	$('minus').toggle();
}

function showAddFriend()
{
	$('addFriend').show();
	$('plus').hide();
	$('minus').show();
}

function deleteFriend(friendId, friendName, friendPhone)
{
	if(!friendName.blank())
	{
		$('action').value = 'DELETEFRIEND';
		$('friendId').value = friendId;
		$('friendName').value = friendName;
		$('friendPhone').value = friendPhone;
		
		document.athmovil.submit();
	}
}

function toggleEditProfilePhoneForm(phone)
{
	$('editProfilePhone').toggle();
	
	destroyotherHtml('editProfilePhone');
	
	$('amupdatephoneHolder').update();
	
	if($('editProfilePhone').visible())
	{
		createIframe('athmupdatephone?phone=' + phone,'athmupdatephoneDiv', 'amupdatephoneHolder', '99%', '225px');
	}
}

function toggleEditProfileAccount()
{
	if(!$('editProfileAccount').visible())
	{
		$('accountError').update();
	}
	
	$('editProfileAccount').toggle();
	
	destroyotherHtml('editProfileAccount');
}

function editAccount()
{
	$('action').value = 'EDITACCOUNT';
	
	document.athmovil.submit();
}

function toggleEditDailyAmount()
{
	if(!$('editDailyAmount').visible())
	{
		$('dailyTransferLimit').value = "";
		$('dailyAmountError').update();
	}
	
	$('editDailyAmount').toggle();
	
	destroyotherHtml('editDailyAmount');
}

function editDailyAmount()
{
	$('action').value = 'EDITDAILYAMOUNT';
	
	document.athmovil.submit();
}

function toggleEditTransferAmount()
{
	if(!$('editTransferAmount').visible())
	{
		$('transferLimit').value = "";
		$('transferAmountError').update();
	}
	
	$('editTransferAmount').toggle();
	
	destroyotherHtml('editTransferAmount');
}

function editTransferAmount()
{
	$('action').value = 'EDITTRANSFERAMOUNT';
	
	document.athmovil.submit();
}

function toggleReceiveSmsAlerts()
{
	$('editReceiveSmsAlerts').toggle();
	
	destroyotherHtml('editReceiveSmsAlerts');
}

function editReceiveSmsAlerts()
{
	$('action').value = 'EDITRECEIVESMS';
	
	document.athmovil.submit();
}

function toggleEditEmail()
{
	if(!$('editEmail').visible())
	{
		$('email').value = "";
		$('emailError').update();
		
		$('confirmEmail').value = "";
		$('confirmEmailError').update();
	}
	
	$('editEmail').toggle();
	
	destroyotherHtml('editEmail');
}

function editEmail()
{
	$('action').value = 'EDITEMAIL';
	
	document.athmovil.submit();
}
