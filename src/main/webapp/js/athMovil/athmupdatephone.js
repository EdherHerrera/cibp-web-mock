Event.observe(window, "load", initPage);

function initPage() 
{
	// Page 0 - Phone Types
	resetRadioInputs("phoneType", "CEL");
	refreshPhoneTypeFields();
	
	// Page 1 - Validation Types
	resetRadioInputs("validationType", "SMS");
	refreshValidationTypeFields();
}

function resetRadioInputs(name, def) 
{
	var allUnchecked = true;
	
	$$('[name=' + name + ']').each(
		function(item){				
			if(item.checked)
			{
				allUnchecked = false;
			}
		}, this
	);
	
	if(allUnchecked)
	{
		$$('[name=' + name + ']').each(
			function(item){				
				item.checked = item.value == def;
			}, this
		);
	}
}

function refreshPhoneTypeFields() {
	$$('[name="phoneType"]').each(
		function(item){				
			if(item.checked)
			{
				if(item.value == "CEL")
				{
					$('providerTypeSelect').show();
					
					$('trValidationType').show();
				}
				else
				{
					$('providerTypeSelect').hide();
					
					$('trValidationType').hide();
				}
				
				refreshValidationTypeFields();
			}
		}, this
	);
}

function refreshValidationTypeFields() {
	if($('trValidationType').visible())
	{
		$$('[name="validationType"]').each(
			function(item){				
				if(item.checked)
				{
					if(item.value == "SMS")
					{
						$('trSmsInstructions').show();
						$('trPinInstructions').hide();
					}
					else if(item.value == "IVR")
					{
						$('trSmsInstructions').hide();
						$('trPinInstructions').show();
					}
				}
			}, this
		);
	}
	else
	{
		$('trSmsInstructions').hide();
		$('trPinInstructions').show();
	}
}
