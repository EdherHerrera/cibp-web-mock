Event.observe(window, "load", initPage);

function initPage() 
{
	refreshFriendsFields();
}

function refreshFriendsFields() {
	if($('friend').value == "--")
	{
		$('friend').selectedIndex = 0;
	}
	
	if($('friend').value == "other")
	{
		if($('trPhoneNumber'))
		{
			$('trPhoneNumber').show();
		}
		else
		{
			$('phoneNumber').show();
		}
	}
	else
	{
		if($('trPhoneNumber'))
		{
			$('trPhoneNumber').hide();
		}
		else
		{
			$('phoneNumber').hide();
		}
	}
}

function resetFriendsFields() {
	if($('friend').value.blank())
	{
		$('phoneNumber').value = "";
	}
}
