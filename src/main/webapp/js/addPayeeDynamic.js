var _urlImagePlus = '/cibp-web/img/plus.gif';
var _urlImageMinus = '/cibp-web/img/minus.gif';
var _urlImageRealTimePaymentsEn = '/cibp-web/img/EnvioRapido_en.gif';
var _urlImageRealTimePaymentsSp = '/cibp-web/img/EnvioRapido_es.gif';
var _realTimePaymentLinkEn = 'http://www.popular.com/pr/help/eng/guarantee.html';
var _realTimePaymentLinkSp = 'http://www.popular.com/pr/help/esp/guarantee.html';
var _realTimePaymentInfoEn = 'Payments to this payee are debited from your deposit account immediately so that we can send them faster.  Click to learn more.';
var _realTimePaymentInfoSp = 'Pagos a este comercio se debitaran de tu cuenta de dep&oacute;sito al momento para poder enviarse m&aacute;s r&aacute;pido.  Haz clic para m&aacute;s informaci&oacute;n.';
var _realTimePayments;
var _mapPaymentTypes;

function setLanguage (language) {
	var success = false;
	
	if (language) {
		_language = language;
		success = true;
	}
	
	return success;
}

function setPaymentTypes (map) {
	var success = false;
	
	if (map) {
		_mapPaymentTypes = map.substring(1, map.length-1);
		success = true;
	}
	
	return success;
}

function getPaymentType (paymentId) {
	var paymentTypes = _mapPaymentTypes.split(",");
	
	for (var i = 0; i < paymentTypes.length; i++) {
		var tmpPayment = paymentTypes[i].trim();
		var tmpId = tmpPayment.split("=");
		if (tmpId[0] == paymentId) {
			return tmpId[1];
		}
	}
	
	return '';
}

function setRealTimePayments (map) {
	_realTimePayments = [];
	
	var realTimePaymentsResult = map.split(",");
	
	for (var i = 0; i < realTimePaymentsResult.length; i++) {
		if (realTimePaymentsResult[i].indexOf('Y') > -1) {
			_realTimePayments.push(realTimePaymentsResult[i].split("=")[0].trim());
		}
	}
}

function getUID () {
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function s4 () {
	return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}

function toggleSection (uid) {
	$(uid).toggle();
	$(uid + 'imgPlus').toggle();
	$(uid + 'imgMinus').toggle();
}

function createCategoryHeader (node, uid) {
	// Add image plus
	var imgPlus = document.createElement("img");
	imgPlus.setAttribute('id', uid + 'imgPlus');
	imgPlus.setAttribute('src', _urlImagePlus);
	
	// Add image minus
	var imgMinus = document.createElement("img");
	imgMinus.setAttribute('id', uid + 'imgMinus');
	imgMinus.setAttribute('src', _urlImageMinus);
	imgMinus.setAttribute('style', 'display:none;');

	// Span with both images
	var span = document.createElement('span');
	span.appendChild(imgPlus);
	span.appendChild(imgMinus);

	// Create wrapper with localized text
	var a = document.createElement('a');
	a.appendChild(span);
	
	if (_language == "en") {
		a.appendChild(document.createTextNode(node.textEN));
	} else {
		a.appendChild(document.createTextNode(node.textSP));
	}
	
	// Add toggle event
	a.setAttribute('onClick', 'toggleSection(\'' + uid + '\');');
	
	return a;
}

function createPayeeLink (node) {
	var a = document.createElement('a');
	a.setAttribute('href', 'addexistingpayee?payee=' + node.id);
	a.setAttribute('class', 'payee-link');
	
	if (_language == "en") {
		a.appendChild(document.createTextNode(node.textEN));
	} else {
		a.appendChild(document.createTextNode(node.textSP));
	}
	
	return a;
}

function createRealTimeLink (realTimeNotificationsClaro) {
	var aRT = document.createElement('a');
	var imgRTP = document.createElement("img");
	var tipStyle = ", BGCOLOR,'#E8F4F8', BORDERCOLOR, '#9BB9CF', WIDTH, 350, ABOVE,true, OFFSETX,-17, FADEIN,150, FADEOUT,150, PADDING,8";
	
	if (_language == "en") {
		aRT.setAttribute('href', _realTimePaymentLinkEn);
		imgRTP.setAttribute('src', _urlImageRealTimePaymentsEn);
		aRT.setAttribute('onmouseover', 'Tip(\'' + _realTimePaymentInfoEn + '\'' + tipStyle + ')');
	} else {
		aRT.setAttribute('href', _realTimePaymentLinkSp);
		imgRTP.setAttribute('src', _urlImageRealTimePaymentsSp);
		aRT.setAttribute('onmouseover', 'Tip(\'' + _realTimePaymentInfoSp + '\'' + tipStyle + ')');
	}
	
	aRT.setAttribute('target', '_BLANK');
	aRT.setAttribute('onmouseout', 'UnTip()');
	if(realTimeNotificationsClaro==false){
		aRT.appendChild(imgRTP);
	}
	return aRT;
}

function createReceivedPaymentParagraph (node) {
	var paymentType = getPaymentType(node.id);
	var p = document.createElement('p');
	var textNode;
	
	switch(paymentType) {
	    case 'TRANSMISION_ADELANTADA':
	    	if (_language == "en") {
	    		textNode = document.createTextNode("same day");
	    	} else {
	    		textNode = document.createTextNode("mismo día");
	    	}
	        break;
	    case 'NO_GARANTIZADO':
	    	if (_language == "en") {
	    		textNode = document.createTextNode("1 day");
	    	} else {
	    		textNode = document.createTextNode("1 día");
	    	}
	        break;
	    case 'MULTIPAGO_MENSUALIDAD':
	    	if (_language == "en") {
	    		textNode = document.createTextNode("1 day");
	    	} else {
	    		textNode = document.createTextNode("1 día");
	    	}
	        break;
	    case 'MULTIPAGO_MATRICULA':
	    	if (_language == "en") {
	    		textNode = document.createTextNode("1 day");
	    	} else {
	    		textNode = document.createTextNode("1 día");
	    	}
	        break;
	    case 'MULTIPAGO_CUOTA':
	    	if (_language == "en") {
	    		textNode = document.createTextNode("1 day");
	    	} else {
	    		textNode = document.createTextNode("1 día");
	    	}
	        break;
	    case 'MASTER_CARD_RPPS':
	    	if (_language == "en") {
	    		textNode = document.createTextNode("2 days");
	    	} else {
	    		textNode = document.createTextNode("2 días");
	    	}
	        break;
	    case 'INTERNO':
	    	if (_language == "en") {
	    		textNode = document.createTextNode("same day");
	    	} else {
	    		textNode = document.createTextNode("mismo día");
	    	}
	        break;
	    case 'GARANTIZADO':
	    	if (_language == "en") {
	    		textNode = document.createTextNode("1 day");
	    	} else {
	    		textNode = document.createTextNode("1 día");
	    	}
	        break;
	    case 'CHEQUE_Y_LISTA_PRIVADO':
	    	textNode = document.createTextNode("CHK IMG");
	        break;
	    case 'CHEQUE_Y_LISTA_PUBLICO':
	    	textNode = document.createTextNode("CHK IMG");
	        break;
	    case 'DIRECT_DEBIT':
	    	if (_language == "en") {
	    		textNode = document.createTextNode("same day");
	    	} else {
	    		textNode = document.createTextNode("mismo día");
	    	}
	        break;
	    case 'COM_EXTERNO_POP':
	    	if (_language == "en") {
	    		textNode = document.createTextNode("same day");
	    	} else {
	    		textNode = document.createTextNode("mismo día");
	    	}
	        break;
	    case 'COM_EXTERNO_RT':
	    	if (_language == "en") {
	    		textNode = document.createTextNode("next day");
	    	} else {
	    		textNode = document.createTextNode("próximo día");
	    	}
	        break;
	    default:
	    	textNode = document.createTextNode("");
	    	break;
	}
	
	p.appendChild(textNode);
	
	return p;
}

function getHTMLTree (data, tag, childWrapper, hidden, idUl, level, realTimeNotificationsClaro) {
	var htmlTree = document.createElement('ul');
	childWrapper = childWrapper != false ? childWrapper : "li";
	
	// Set id for ul section (if needed)
	if (idUl) {
		htmlTree.setAttribute('id', idUl);
	}
	
	// Hide ul section for children nodes
	if (hidden) {
		htmlTree.setAttribute('style', 'display:none;');
	}
	
	// Nodes and subnodes
	for (var i = 0; i < data.length; i++) {
		var node = data[i];
		var htmlElement = document.createElement(tag);

		if (node.hasOwnProperty("children")) {
			//add class subcategory if level is greater than 0 
			if (level > 0) {
				htmlElement.setAttribute('class', 'subcategory');
			}
			// Get id for subsection (used for toggle event)
			var uid = getUID();
			var a = createCategoryHeader(node, uid);
			
			// Add category to tree
			htmlElement.appendChild(a);
			htmlElement.appendChild(getHTMLTree(node.children, tag, childWrapper, true, uid, ++level,realTimeNotificationsClaro));
			level--;
		} else {
			// Payee		
			var a = createPayeeLink(node);
			htmlElement.appendChild(a);
			htmlElement.setAttribute('class', 'payee');
			
			// RealTime payments section
			if (_realTimePayments.indexOf(node.id.toString()) > -1) {
				var aRT = createRealTimeLink(realTimeNotificationsClaro);
				htmlElement.appendChild(aRT);
			} else {
				htmlElement.appendChild(document.createElement('p'));
			}
			
			// Received payments
			var p = createReceivedPaymentParagraph(node);
			htmlElement.appendChild(p);
		}

		htmlTree.appendChild(htmlElement);
	}

	return htmlTree;
}












function getHTMLTree2 (data, tag, childWrapper, hidden, idUl, level) {
	var htmlTree = document.createElement('ons-list');
	
	
	// Set id for ul section (if needed)
	if (idUl) {
		htmlTree.setAttribute('id', idUl);
	}
	
	// Hide ul section for children nodes
	if (hidden) {
		htmlTree.setAttribute('style', 'display:none;');
	}
	
	// Nodes and subnodes
	for (var i = 0; i < data.length; i++) {
		var node = data[i];
		var htmlElement = document.createElement('ons-list-item');
		htmlElement.setAttribute('modifier', 'chevron');
		htmlElement.setAttribute('tappable', '');
		htmlElement.setAttribute('id', 'push-button_'  + i);
		var a = document.createElement('span');
		var label = '';
		if (_language == "en") {
			a = document.createTextNode(node.textEN);
			label = node.textEN;
		} else {
			a = document.createTextNode(node.textSP);
			label = node.textSP;
		}
		if(childWrapper){
			htmlElement.setAttribute('onClick','document.querySelector(\'#myNavigator\').pushPage(\'page2.html\', {data: {title: \''+label+'\', index: '+ i +', id : false }});');
		}else{
			if(node.hasOwnProperty("children")){
				var dataJson = JSON.stringify(data[level].children);				
				htmlElement.setAttribute('onClick','document.querySelector(\'#myNavigator\').pushPage(\'page2.html\', {data: {title: \''+label+'\', index: '+ i +', id : false, data : '+dataJson+' }});');
			}else{
				htmlElement.setAttribute('onClick','document.querySelector(\'#myNavigator\').pushPage(\'page3.html\', {data: {title: \''+label+'\', index: '+ i +', id: '+ node.id +' }});');
			}
			
		}
		// Add category to tree
		htmlElement.appendChild(a);
		htmlTree.appendChild(htmlElement);
	}
	return htmlTree;
}

function createPayeeChildList(data, id, index) {
	var htmlTree = document.createElement('ons-list');
	// Set id for ul section (if needed)
	if (id) {
		htmlTree.setAttribute('id', id);
	}
	
	// Nodes and subnodes
	for (var i = 0; i < data.length; i++) {
		var node = data[i];
		if(i == index){
			if (node && node.hasOwnProperty("children")) {
				return getHTMLTree2(node.children,'div',false,false, id,i);
			}else{
				return getHTMLTree2(data,'div',false,false, id,i);
			}
			
		}		
	}
	return htmlTree;
}






function setLocalizedReceivedPayments() {
	if (_language == "en") {
		document.styleSheets[0].insertRule('#commonPayeesJSON ul li.payee:first-child:before, #commonPayeesJSON ul li.subcategory:first-child:before { content: \'Received payments*\'; }', 0);
	} else {
		document.styleSheets[0].insertRule('#commonPayeesJSON ul li.payee:first-child:before, #commonPayeesJSON ul li.subcategory:first-child:before { content: \'Pagos recibidos*\'; }', 0);
	}
}





