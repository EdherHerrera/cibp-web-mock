var Popup = {
  open: function(options)
  {
    this.options = {
      url: '#',
      width: 600,
      height: 500,
      name:"_blank",
      location:"no",
      menubar:"no",
      toolbar:"no",
      status:"yes",
      scrollbars:"yes",
      resizable:"yes",
      left:"",
      top:"",
      normal:false
    }
    Object.extend(this.options, options || {});

    if (this.options.normal){
        this.options.menubar = "yes";
        this.options.status = "yes";
        this.options.toolbar = "yes";
        this.options.location = "yes";
    }

    this.options.width = this.options.width < screen.availWidth?this.options.width:screen.availWidth;
    this.options.height=this.options.height < screen.availHeight?this.options.height:screen.availHeight;
    var openoptions = 'width='+this.options.width+',height='+this.options.height+',location='+this.options.location+',menubar='+this.options.menubar+',toolbar='+this.options.toolbar+',scrollbars='+this.options.scrollbars+',resizable='+this.options.resizable+',status='+this.options.status
    if (this.options.top!="")openoptions+=",top="+this.options.top;
    if (this.options.left!="")openoptions+=",left="+this.options.left;
    window.open(this.options.url, this.options.name,openoptions );
    return false;
  }
}

function gotolocation(ref)
{	
	window.location.replace(ref);
}

function setDelay(ref)
{
	window.setTimeout(function(){ location.replace(ref); }, 5000);
}

function getPayments(page, url) {
	
	var test = $F('paymentSearch');
	if(test.length==0){
		test = '!!';
	}
	var pars = 's='+test+'&page='+page;
	var myAjax = new Ajax.Updater( 'resultPane', url, { method: 'get', parameters: pars });
	
}

function getStmt(placeholder, url, account) {
	var myAjax = new Ajax.Updater(placeholder, url, {method: 'get', parameters: 'account='+account});
}


function getloginCampaign(placeholder, url) {
	var myAjax = new Ajax.Updater(placeholder, url, {method: 'get', parameters: ''});
}

function reportError(){$('resultdiv').innerHTML = 'Error:  System Not Available';}

function showcalexport(i,b,x,y,m,d) {
	Calendar.setup({
	inputField: i,
	button: b,
	ifFormat:"%m/%d/%Y",
	step:1, //show all years
	weekNumbers:false,
	range:[x-2, x], //we substract years lo allow wider range of dates in the past
	electric:false,
	date: new Date(x,m-1,d),
	dateStatusFunc: function (date) {
		var today = new Date(x,m-1,d+1);
		var past = new Date(x,m-1,d);
		past.setMonth(past.getMonth()-18);
		return (date > today || date < past)? true : false;
		}
	});
}

function showcalcheckcopy(i,b,x,y,m,d) {
	Calendar.setup({
	inputField: i,
	button: b,
	ifFormat:"%m/%d/%Y",
	step:1, //show all years
	weekNumbers:false,
	range:[x-7, x], //we substract years lo allow wider range of dates in the past
	electric:false,
	date: new Date(x,m-1,d),
	dateStatusFunc: function (date) {
		var today = new Date(x,m-1,d+1);
		var past = new Date(x,m-1,d);
		past.setMonth(past.getMonth()-84);
		return (date > today || date < past)? true : false;
		}
	});
}

function showcalendar(i,b,x,y,m,d) {
	Calendar.setup({
	inputField: i,
	button: b,
	ifFormat:"%m/%d/%Y",
	step:1, //show all years
	weekNumbers:false, 
	range:[x, y],
	electric:false,
	date: new Date(x,m-1,d),
	dateStatusFunc: function (date) {
		var today = new Date(x,m-1,d);
		var future = new Date(x,m-1,d);
		future.setMonth(future.getMonth()+12);
		return (date < today || date > future || date.getDay()==6 || date.getDay()==0 || isHoliday(date))?true:false;
		}
	});
}

function showcalendarAccount(i,b,x,y,m,d) {
	Calendar.setup({
	inputField: i,
	button: b,
	ifFormat:"%m/%d/%Y",
	step:1, //show all years
	weekNumbers:false, 
	range:[x, x+2],
	electric:false,
	date: new Date(x,m-1,d),
	dateStatusFunc: function (date) {
		var today = new Date(x,m-1,d);
		var future = new Date(x+2,m-1,d);
		future.setMonth(future.getMonth());
		return (date < today || date > future || date.getDay()==6 || date.getDay()==0 || isHoliday(date))?true:false;
		}
	});
}

function isHoliday(date) {
  var hs = Try.these(function(){return getHolidays()});
  if(hs!=null)
  for(var i=0; i<hs.length; i++){
    var h = hs[i];
    var dude = new Date(h.substring(6),h.substring(0,2)-1,h.substring(3,5));
    if(date.getDate()==dude.getDate() && date.getMonth()==dude.getMonth() && date.getYear()==dude.getYear())
      return true;
  }
  return false;
}

function createIframe(url, iframeName, placeholder, width, height) {
	if($(iframeName)==null) {
		iframe =  Builder.node('iframe',{src:url,name:iframeName,id:iframeName,frameborder:'0'});
		iframe.style.border="0px";
		iframe.style.width = (width==null||width=="") ?"450px":width;
		iframe.style.height = height==null ?"200px":height;
		$(placeholder).appendChild(iframe);
		
		Element.addClassName(iframe, 'loading'); //funciona en ie6
		
	} else {
		$(placeholder).update('');
	}
}

function createIframe2(url, iframeName, placeholder, width, height, scrollbar) {
	if($(iframeName)==null) {
		iframe =  Builder.node('iframe',{src:url,name:iframeName,id:iframeName,frameborder:'0',scolling:scrollbar});
		iframe.style.border="0px";
		iframe.style.width = (width==null||width=="") ?"450px":width;
		iframe.style.height = height==null ?"200px":height;
		$(placeholder).appendChild(iframe);

		Element.addClassName(iframe, 'loading'); //funciona en ie6

	} else {
		$(placeholder).update('');
	}
}

function destroyIframes(dude) {
	var f = $$('.iframes');
	for (var i=0; i<f.length; i++) {
		var id = f[i].id;
		if (id!=dude) {
			$(id).update('');
			($('minus'+id)!=null)?$('minus'+id).hide():'';
			($('plus'+id)!=null)?$('plus'+id).show():'';
		}
	}
}

function createHtml(url, placeholder, tempholder) {
 	
 	var complete = function(t){removeLoading(t,placeholder);}
 	if($(tempholder)==null) {
		span = Builder.node('span',{id:tempholder});
		$(placeholder).appendChild(span);
		Element.addClassName(placeholder, 'loading');
		var myAjax = new Ajax.Updater({success:tempholder}, url, {onFailure:showError,onComplete:complete});
	} else {
		$(placeholder).update('');
	}
}

function removeLoading(t, placeholder)
{
	Element.removeClassName(placeholder, 'loading');

	Element.addClassName(placeholder, 'loadingafter');
	
}

function destroyotherHtml(dude) {
	var f = $$('.destroyable');
	for (var i=0; i<f.length; i++) {
		var id = f[i].id;
		if (id!=dude) {
			$(id).hide();
		}
	}
}

function showError(request) {
	alert('Sorry. There was an error.');
}

function goToWizardPage(formId, page){
 var param = document.createElement("input");
 param.name = '_target'+page;
 param.type = 'hidden';
 param.value="";
 
 $(formId).appendChild(param);
 $(formId).submit();
}

function tabNext(obj,len,next_field) {
	if (obj.value.length == len) {
			next_field.focus();
	}
}

function changeLang(lang, uri, query) {
	var complete = function(t){pagelang(t, uri);}
	/*mpicado, agrego el replace debido a que no se estaba contemplando mas de una variable por parametro en el URL,
	* desde aca cambio el '&' por la ',' para que no se envien por parametros separados y en el webAppController lo vuelvo
	* a cambiar para que se puedan enviar los parametros separados al recargar la pagina*/
	var pars = 'newlang='+lang+'&query='+query.replace("&",",");
	var myAjax = new Ajax.Request('changelanguage', {method:'post',parameters:pars,onSuccess:complete});
}

function changeSize(size, uri, query) {
	var complete = function(t){pagelang(t, uri);}
	var pars = 'newsize='+size+'&query='+query;
	var myAjax = new Ajax.Request('changesize', {method:'post',parameters:pars,onSuccess:complete, onError:showError});
}

function pagelang(t, uri) {
   window.parent.location.replace(uri + t.responseText);
}

function hideIframes() {
	var iframe = document.getElementsByTagName('iframe');
	for(var i=0;i<iframe.length;i++)
	{
		iframe[i].style.display="none"; 
	}
}

function showIframes() {
	var iframe = document.getElementsByTagName('iframe');
	for(var i=0;i<iframe.length;i++)
	{
		iframe[i].style.display="block";
	}
}

function showSessionExpiredAlert() {
	hideIframes();
	Modalbox.show($('sessionExpirationAlert'), {title: this.title, width: 530, overlayClose: false});
}

function expireSession() {
	window.parent.location.replace("logout?expired");
}


function setStat(cmid, area, clickview) {
  var pars = 'cmid='+cmid+'&cov='+clickview+'&cma='+ area;
  var myAjax = new Ajax.Request('campaignStatistic', {method:'post',parameters:pars});
}
