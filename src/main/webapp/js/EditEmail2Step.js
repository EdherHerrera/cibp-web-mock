/* MBIM-650 - J.JIMENEZ
 * SE CREA JS PARA EL CAMBIO DE EMAIL CUANDO EL CLIENTE ACTIVA VERIFICACION DE 2 PASOS
 * AQUI MUESTRA EL POPUP PARA LA INSERCION DEL CODIGO ENVIADO POR MEDIO SMS
 */

/* APERTURA DE POPUP */
function challengeOob() {
	var oobIframe = 
		"<div class='iframes' id='oobIframePlaceholder'> \
			<iframe name='oobFrame' class='loading' id='oobFrame' src='loginoob' frameborder='0' style='border: 0px currentColor; border-image: none; width: 480px; height: 275px;'></iframe> \
		</div>";
	Modalbox.show(oobIframe, {title: this.title, width: 500, height:295, overlayClose:false});
	Modalbox.MBwindow.setStyle({top: 150 + "px"});
	Event.stopObserving(document, "keypress", Modalbox.kbdHandler );
    document.onkeydown=function(e){
		!e?e=window.event:null;
		if (e.keyCode == 13 ){
			return false;
		} 
	} 

}

/* CIERRA EL POPUP Y RETORNA A LA PAGINA DONDE ESTE LOCALIZADO - ACTUALIZA EL EMAIL */
function oobValidation() {
	var url = 'personalinformation?oob=true';
	var complete = function(t){showResponseEmail(t);}
	var myAjax = new Ajax.Request(url, {method:'post',onComplete:complete});
}

function rsaValidation() {
	setTimeout(hideEvents,100);
	var answer = $("answer").value;
	var url = 'personalinformation?answer=' + answer;
	var complete = function(t){showResponseEmail(t);}
	var myAjax = new Ajax.Request(url, {method:'post',onComplete:complete});
}
/* MBIM-650 Se renombra Funcion */
function showResponseEmail(t) 
{
		var resp = t.responseText.split('-');
		var status = resp[0];
	var nq = resp[1];
		if(status=='success')
		{
			window.parent.location.reload();
	}
}
