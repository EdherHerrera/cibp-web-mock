var _language;
var _pageSize = 250;

function setLanguage(language) {
	var success = false;

	if (language) {
		_language = language;
		success = true;
	}

	return success;
}

function toggleSection(uid) {
	$(uid).toggle();
	$(uid + 'imgPlus').toggle();
	$(uid + 'imgMinus').toggle();
}

function getHTMLTree2(data, tag, childWrapper, hidden, idUl, level) {
	var htmlTree = document.createElement('ons-list');

	// Set id for ul section (if needed)
	if (idUl) {
		htmlTree.setAttribute('id', idUl);
	}

	// Hide ul section for children nodes
	if (hidden) {
		htmlTree.setAttribute('style', 'display:none;');
	}

	// Nodes and subnodes
	for (var i = 0; i < data.length; i++) {
		var node = data[i];
		var htmlElement = document.createElement('ons-list-item');
		htmlElement.setAttribute('modifier', 'chevron');
		htmlElement.setAttribute('tappable', '');
		htmlElement.setAttribute('id', 'push-button_' + i);
		var label = '';
		if (_language == "en") {
			label = node.textEN;
		} else {
			label = node.textSP;
		}
		var a = document.createTextNode(label);
		label = label.replace(/\'/g,'&apos;');
		if (childWrapper) {
			htmlElement
					.setAttribute(
							'onClick',
							'document.querySelector(\'#myNavigator\').pushPage(\'page2.html\', {data: {title: \''
									+ label
									+ '\', index: '
									+ i
									+ ', id : false }});');
		} else {
			if (node.hasOwnProperty("children")) {
				var dataJson = JSON.stringify(node.children);
				htmlElement
						.setAttribute(
								'onClick',
								'document.querySelector(\'#myNavigator\').pushPage(\'page2.html\', {data: {title: \''
										+ label
										+ '\', index: '
										+ i
										+ ', id : false, data : '
										+ dataJson
										+ ' }});');
			} else {
				htmlElement
						.setAttribute(
								'onClick',
								'document.querySelector(\'#myNavigator\').pushPage(\'page3.html\', {data: {title: \''
										+ label
										+ '\', index: '
										+ i
										+ ', id: '
										+ node.id + ' }});');
			}

		}
		// Add category to tree
		htmlElement.appendChild(a);
		htmlTree.appendChild(htmlElement);
	}
	return htmlTree;
}

function createPayeeChildList(data, id, index) {
	var htmlTree = document.createElement('ons-list');
	htmlTree.setAttribute('index', index);
	// Set id for ul section (if needed)
	if (id) {
		htmlTree.setAttribute('id', id);
	}
	// Nodes and subnodes
	
	for (var i = 0; i < data.length; i++) {
		var node = data[i];
		if(data.length <=  index){
			if (node && node.hasOwnProperty("children")) {
				return getHTMLTree2(node.children, 'div', false, false, id, i);
			} else {
				return getHTMLTree2(data, 'div', false, false, id, i);
			}
		}
		if (i == index) {
			if (node && node.hasOwnProperty("children")) {
				return getHTMLTree2(node.children, 'div', false, false, id, i);
			} else {
				return getHTMLTree2(data, 'div', false, false, id, i);
			}
		}
	}
	return htmlTree;
}

function getPayments(page, urlPar) {

	var test = document.getElementById("searchPayee").value;
	if (test.length == 0) {
		test = '!!';
	}

	j.ajax({
		url : urlPar,
		cache : false,
		data : {
			's' : test,
			'page' : page,
			'maxResults' : _pageSize
		}
	}).done(function(html) {
		document.getElementById("searchPayess").innerHTML = html;
	})
}

function searchPayee(keyword, hideDiv, showDiv, page, urlPar,inputId) {
	// Change for hidediv
	var x = document.getElementsByName(hideDiv);
	//document.getElementById( 'searchPayee').disabled='disabled';
	if (keyword.length > 0) {
		document.getElementById(showDiv).innerHTML = '';
		for (i = 0; i < x.length; i++) {
			x[i].style.display = "none";
		}
		 var modal = document.querySelector('#mSearch');
		
		j.ajax({
			url : urlPar,
			cache : false,
			beforeSend: function () {
				if(ons.platform.isIOS()) {
					document.getElementById('addPayees-search-ios').style.display = 'block';
					
				} else if(ons.platform.isAndroid()) {
					document.getElementById('addPayees-search-android').style.display = 'flex';
						
				} else {
					document.getElementById('addPayees-search-ios').style.display = 'block';
					
				}
				modal.show();	
			},
			data : {
				's' : keyword,
				'page' : page,
				'maxResults' : _pageSize
			}
		}).done(function(html) {
			modal.hide();
			var check = document.getElementById( inputId).value.length;			
			if(check > 0){
				document.getElementById(showDiv).innerHTML = html;
			}
		})
	} else {
		document.getElementById(showDiv).innerHTML = '';
		for (i = 0; i < x.length; i++) {
			x[i].style.display = "block";
		}
	}

}

function getPayeeImgSrc(urlPar, payeeId) {
	j.ajax({
		url : urlPar,
		cache : false,
		data : {
			'payeeId' : payeeId
		}
	}).done(function(response) {
		if (response) {
			var res = JSON.parse(response);
			if(res.isCCA != "true"){				
				if(res.askforebill){				
					document.getElementById("listEbill").style.display = 'block';
					document.getElementById("listEbill").checked = false;
				} else {
					document.getElementById("listEbill").style.display = 'none';
				}
			} else {
				if(res.eBillsFlag && res.askforebill) {
					ebillNumber = document.getElementById("billing");
					if(ebillNumber) { 
						ebillNumber.addEventListener('input', function (evt) {
							evaluateCCAOwner(this.value);
						});
					}
				} else 
					document.getElementById("listEbill").style.display = 'none';
				
			}

			var zipCode = document.getElementById("msgEmail");
			if(res.askforzipcode == "true"){
				zipCode.value = true;
			}else{
				zipCode.value = false;
			}
						
			var img = document.getElementById("img_help");
			var imgfs = document.getElementById("img_help_fs");
			img.src = res.imgSrc;
			imgfs.src = res.imgSrc;
			if (res.imgSrc) {
				img.style.display = 'block';
			} else {
				img.style.display = 'none';
			}
			document.getElementById("helpText").innerHTML = res.textHelp;
			document.getElementById("customerEmail").innerHTML = res.customerEmail;
			if(res.payeeName)
				document.getElementById("namePayee").innerHTML = res.payeeName;
			
		}
	})
}


function imgFull() {
	var modal = document.querySelector('#mImg');
	var imgfs = document.getElementById("img_help_fs");
	modal.toggle();
}

function isLandscape() {
	if(window.orientation) {
	  return (window.orientation === 90 || window.orientation === -90); 
	} else return false;
	}

function setPayee(idPayee, namePayee){
	document.getElementById("selectedPayee").value=idPayee;
	document.getElementById("selectedPayee").name=namePayee;
}

function createAlertDialog(div) {
	document.getElementById('my-alert-dialog').show();
	document.getElementById(div).style.display = "block";
}

var hideAlertDialog = function() {
  document.getElementById('my-alert-dialog').hide();
}

function hidenMsjError(){
	document.getElementById('msjErrorBillernumber').style.display = "none";
	document.getElementById('msjErrorBillerConfirmation').style.display = "none";
	document.getElementById('msjErrorNickname').style.display = "none";
	document.getElementById('msjErrorInvalidEmail').style.display = "none";
	document.getElementById('msjErrorRequired').style.display = "none";
	document.getElementById('msjErrorMatchEmail').style.display = "none";
	document.getElementById('msjErrorZipCode').style.display = "none";
	
} 
function savePayee(showDiv, urlPar){
	hidenMsjError();
	
	if(document.querySelector('#billing').value == "" || isInValidPayeeAccountNumber(document.querySelector('#billing').value))
	{
		createAlertDialog('msjErrorBillernumber');
	}	
	else if(document.querySelector('#billing').value != document.querySelector('#billingConfirm').value)
	{
		createAlertDialog('msjErrorBillerConfirmation');
	}	
	else if(document.querySelector('#nickname').value == "" || isInValidPayeeNickname(document.querySelector('#nickname').value))
	{
		createAlertDialog('msjErrorNickname');
	}
	else if(document.getElementById('ebill').checked && document.getElementById("msgEmail").value && (document.getElementById('zipCode').value == "" || isInvalidZipCode()))
	{
		createAlertDialog('msjErrorZipCode');
	}
	else if((document.getElementById('ebill').checked && document.getElementById('listEmail').style.display === "block") && document.getElementById('email').value == "" )
	{
		createAlertDialog('msjErrorRequired');
	}
	else if((document.getElementById('ebill').checked && document.getElementById('listEmail').style.display === "block") && isInvalidEmail())
	{
		createAlertDialog('msjErrorInvalidEmail');
	}
	else if((document.getElementById('ebill').checked && document.getElementById('listEmail').style.display === "block") && (document.getElementById('email').value != document.getElementById('emailConfirm').value))
	{
		createAlertDialog('msjErrorMatchEmail');
	}			
	else
	{
		 var modal = document.querySelector('#mSave');
			j.ajax({
				url : urlPar,
				cache : false,
				beforeSend: function () {
					if(ons.platform.isIOS()) {
						document.getElementById('addPayees-loading-ios').style.display = 'block';
						
					} else if(ons.platform.isAndroid()) {
						document.getElementById('addPayees-loading-android').style.display = 'flex';
						
					} else {
						document.getElementById('addPayees-loading-ios').style.display = 'block';
						
					}
					modal.show();	
				},
				data : {
					'payeeId' : document.querySelector('#selectedPayee').value,
					'billingNumber' : document.querySelector('#billing').value,
					'confirmBillingNumber' : document.querySelector('#billingConfirm').value,
					'nickname' : document.querySelector('#nickname').value,
					'ebill' : document.querySelector('#ebill').checked,
					'zipCode' : document.querySelector('#zipCode').value,
					'editEmail' : document.getElementById('listEmail').style.display === "block",
					'email' : document.querySelector('#email').value,
					'emailConfirm' : document.querySelector('#emailConfirm').value
				}
			}).done(function(response) {
				
				var res = JSON.parse(response);
				modal.hide();
				if(res.errorCode != ""){
					ga('send','event','MiBanco','Mobile Add Payee', 'kAddPayeeFail');
					document.querySelector('#myNavigator').pushPage('error-page.html', {
						data: {title: '',
								id: '',
								namePayee: ''+ document.getElementById("selectedPayee").name+'',
								payeeNumber : ''+document.querySelector('#billing').value+'',
								errorMessage : ''+res.errorMessage+'',
								statusMessage : ''+res.statusMessage+'',
								errorCode : ''+res.errorCode+''}});
					//Enviar mensaje a la pantalla de error			
				}else{
					//Enviar evento sucessful
					ga('send','event','MiBanco','Mobile Add Payee', 'kAddPayeeSuccessful');
					document.querySelector('#myNavigator').pushPage('success-page.html', {
						data: {title: '',
								id: '',
								namePayee: ''+ document.getElementById("selectedPayee").name+'',
								payeeNumber : ''+document.querySelector('#billing').value+''}});
				}
			})
	 }	
}

function isInValidPayeeNickname(input) {
	var answer=false;
	var pattern = new RegExp("^[A-Za-z0-9]+((\\s|-)?[A-Za-z0-9]+)*$","g");
    answer = pattern.test(input);
    return !answer;
}

function isInValidPayeeAccountNumber(input) {
	var answer=false;
	var pattern = new RegExp("^[A-Za-z0-9-]+$","g");
    answer = pattern.test(input);
    return !answer;
}

function showChangeEmail(){
	if(document.getElementById('listEmail').style.display === "none"){
		showRsaDialogAddPayee('challenge-dialog', 'rsaChallenge.html','', 'changeEmail');
	}else{
		document.getElementById('listEmail').style.display = "none";
	}
}

function changeEmail(){
	document.getElementById('listEmail').style.display = "block";
}

var showRsaDialogAddPayee = function(id, html, nextScreen, fn) {
	  var dialog = document.getElementById(id);
	  if (dialog) {
	    dialog.parentNode.removeChild(dialog);
	  }
	    ons.createElement(html, { append: true })
	      .then(function(dialog) {
	        jQuery.ajax({
	    		url : 'rsaChallengeMethod',
	    		data: {}
	    	}).done(function(response) {
	    		var res = JSON.parse(response);
	    		if(res.isOobAuthenticated == "true"){
	    			changeEmail();
	    		}else{
	    			if(res.MBSD3755 == "true"){	    				
	    				dialog.show();
	    				jQuery("#oobChallenge").load(
	    						"../actions/initRsa?nextScreen=" + nextScreen + "&function="+fn);
	    			}else{
	    				if (dialog) {
	    				    dialog.parentNode.removeChild(dialog);
	    				}
	    				changeEmail();
	    			}
	    		}
	    	})
	      });
	};

var hideDialog = function(id) {
	if (document.getElementById(id)) {
		document.getElementById(id).hide();
	}
};

function showEmail(){
	var ebill = document.getElementById('ebill');
	if(ebill.checked){
		if(document.getElementById("msgEmail").value){
			document.getElementById('msgEmail').style.display = 'block';
			document.getElementById('labelZipCode').style.display = 'block';
			document.getElementById('labelEmail').style.display = 'block';
			document.getElementById('labelEmail').style.display = 'block';
			document.getElementById('linkEmail').style.display = 'block';
		}else{
			document.getElementById('labelZipCode').style.display = 'none';
			document.getElementById('msgEmail').style.display = 'block';
			document.getElementById('labelEmail').style.display = 'block';
			document.getElementById('linkEmail').style.display = 'block';
		}
		
	}else{
		document.getElementById("labelZipCode").style.display = 'none';
		document.getElementById('msgEmail').style.display = 'none';
		document.getElementById('labelEmail').style.display = 'none';
		document.getElementById('linkEmail').style.display = 'none';
		document.getElementById('listEmail').style.display = "none";
		
	}	
}

function isInvalidEmail() {
	var valor = document.getElementById("email").value;
	var patron = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
	if (patron.test(valor)){ 
        return false;
    }else{
        return true;
    }
}

function isInvalidZipCode() {
	    var valor = document.getElementById("zipCode").value;
	    var patron = /^\d{5}$/;
	    if (patron.test(valor) && (!isNaN(valor))){ 
	        return false;
	    }else{
	        return true;
	    }
}
function chgBack() {
	var backButton = document.getElementById("bkSp");
	var backButton_ = document.getElementById("bkSp_");
	var inputSearch = document.getElementById("searchPayee");
	if (inputSearch && inputSearch.value.length > 0) {
		backButton.onclick = function () { window.location.href='mobileAddPayee?refresh'; };
		backButton_.onclick = function () { window.location.href='mobileAddPayee?refresh'; };
	} else {
		backButton.onclick = function () { window.location.href='mobileAddPayee?back'; };
		backButton_.onclick = function () { window.location.href='mobileAddPayee?back'; };
	}
}

function validForm(elements) {
	var data = elements.data[0];
	var disabled = false;
	if(data.length > 0) {
		for(i=0; i < data.length; i++) {
			if(data[i].value==null || data[i].value=="") {disabled = true; break;} 
		}
	} else {
		disabled = true
	}
	document.getElementById('addPayee').disabled = disabled;	
}

function deleteDialog(idDialog,payeeId) {
    var elem = document.getElementById(idDialog);
    var payeeIdHidden = document.getElementById('payeeToDelete');
    if(payeeIdHidden)
    	payeeIdHidden .value = payeeId;
    if (elem) {
    	elem.show();
     }
  };
function hideDialog(id) {
	var elem = document.getElementById(id);
    if (elem) {
    	elem.hide();
    }
};

function deletePayee(urlPar){
	 var modal = document.querySelector('#mDelete');
	 var payeeId = document.getElementById('payeeToDelete').value;
	j.ajax({
		url : urlPar,
		cache : false,
		beforeSend: function () {
			if(ons.platform.isIOS()) {
				document.getElementById('addPayees-loading-ios').style.display = 'block';
				
			} else if(ons.platform.isAndroid()) {
				document.getElementById('addPayees-loading-android').style.display = 'flex';
				
			} else {
				document.getElementById('addPayees-loading-ios').style.display = 'block';
				
			}
			modal.show();	
		},
		data : {
			'payeeId' : payeeId,
			'action' : "DELETE"
		}
	}).done(function(response) {
		// preserve newlines, etc - use valid JSON
		/*response = response.replace(/\\n/g, "\\n")  
               .replace(/\\'/g, "\\'")
               .replace(/\\"/g, '\\"')
               .replace(/\\&/g, "\\&")
               .replace(/\\r/g, "\\r")
               .replace(/\\t/g, "\\t")
               .replace(/\\b/g, "\\b")
               .replace(/\\f/g, "\\f");*/
		// remove non-printable and other non-valid JSON chars
		//response = response.replace(/[\u0000-\u0019]+/g,"");
		//var res = JSON.stringify(response);
		var resp = JSON.parse(response);
		modal.hide();
		hideDialog('deleteConfirmation');
		if(resp.errorCode && resp.errorCode != ""){
			document.getElementById('errorContent').innerHTML = resp.errorMessage;
			editPayeeList(resp.payees);
			deleteDialog('errorAlert');
			
		}else{	
			//var parent = document.getElementById("editPayeeList");
			//var child = document.getElementById(payeeId);
			//parent.removeChild(child);
			editPayeeList(resp.payees);
			ga('send', 'pageview',tag);
			ga('legacyProperty.send', 'pageview',tag);
			document.getElementById('successContent').innerHTML = resp.statusMessage;
			deleteDialog('successAlert');
		}
	})
	
};

function deletePayeeDo(){
	 var modal = document.querySelector('#mDelete');
	 var form = document.getElementById('deleteForm');
	 if(ons.platform.isIOS()) {
			document.getElementById('addPayees-loading-ios').style.display = 'block';
			
		} else if(ons.platform.isAndroid()) {
			document.getElementById('addPayees-loading-android').style.display = 'flex';
			
		} else {
			document.getElementById('addPayees-loading-ios').style.display = 'block';
			
		}
		modal.show();
		form.submit();
};

function editPayeeList(accounts) {
	var htmlTree = document.getElementById("editPayeeList");
	while (htmlTree.firstChild) {
		htmlTree.removeChild(htmlTree.firstChild);
	}
	if (accounts == null || accounts.length < 1) {
		var htmlElement = document.createElement('ons-list-item');
		var divLeft = document.createElement('div');
		divLeft.setAttribute('class', "left");
		var a = document.createTextNode(notFoundPayeeText);
		divLeft.appendChild(a);
		htmlElement.appendChild(divLeft);
		htmlTree.appendChild(htmlElement);
		return;
	}
	
	
	for (var i = 0; i < accounts.length; i++) {
		var node = accounts[i];
		var htmlElement = document.createElement('ons-list-item');
		htmlElement.setAttribute('id', accounts[i].frontEndId);
		var label = '';
		var a = document.createTextNode(label);
		var divLeft = document.createElement('div');
		divLeft.setAttribute('class', "left");
		var divCenter = document.createElement('div');
		divCenter.setAttribute('class', "center delButtonText");
		var fab = document.createElement('ons-fab');
		fab.setAttribute('modifier', 'mini');
		fab.setAttribute('onClick',	'deleteDialog(\'deleteConfirmation\',\''
				+ accounts[i].frontEndId
				+ '\');');
		var imgMini = document.createElement('img');
		imgMini.setAttribute('onerror', 'this.src="'+imagePath+'/img/newPayee/boxTest.png"');
		imgMini.setAttribute('src', imagePath + '/img/newPayee/Payees/payeeNumber-' 
					+ accounts[i].globalPayeeId
					+ '.png');
		imgMini.setAttribute('style', "width : 70px;");
		var iconDelete = document.createElement('ons-icon');
		if(ons.platform.isAndroid()) 
			iconDelete.setAttribute('icon', "md-delete");
		else 
			iconDelete.setAttribute('icon', "md-minus");
		var nickName = document.createElement('span');
		nickName.setAttribute('class', "list-item__title");
		nickName.innerHTML = accounts[i].nickname;
		var billAccount = document.createElement('span');
		billAccount.setAttribute('class', "list-item__subtitle");
		billAccount.innerHTML = accounts[i].billingAccount;
		
		fab.appendChild(iconDelete);
		if(ons.platform.isIOS()) {
			divLeft.appendChild(fab);
			
		}
		divLeft.appendChild(imgMini);
		
		divCenter.appendChild(nickName);
		divCenter.appendChild(billAccount);
		if(ons.platform.isAndroid()) {
			divCenter.appendChild(fab);
			
		}
		htmlElement.appendChild(divLeft);
		htmlElement.appendChild(divCenter);

		htmlTree.appendChild(htmlElement);
	} 
	
}

function evaluateCCAOwner(billerNumber) {
	var noWhitespaceValue = billerNumber.replace(/\s+/g, '');
	var noWhitespaceCount = noWhitespaceValue.length;
	if (noWhitespaceCount % 16 == 0) {
		var pars = 'billerNumber=' + billerNumber;
		j.ajax({
			url : "evaluateCCAOwner",
			cache : false,
			type : 'post',
			data : {
				'billerNumber' : billerNumber
			}
		}).done(completeCCAValidation);

	} else {
		document.getElementById("listEbill").style.display = 'none';
		document.getElementById("listEbill").checked = false;
		
	}
}

function completeCCAValidation(ccaNotOfUser) {
	if (ccaNotOfUser === "T") {
		document.getElementById("listEbill").style.display = 'block';
		document.getElementById("listEbill").checked = false;
		

	} else {
		document.getElementById("listEbill").style.display = 'none';
		document.getElementById("listEbill").checked = false;
		var ebill = document.getElementById('ebill').checked = false;
		showEmail();
		
	}

}
