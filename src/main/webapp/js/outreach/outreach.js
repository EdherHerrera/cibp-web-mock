    
function returnPlaceholder(type) {
	var item = [];
	jQuery.grep(addressPlaceholders, function (item) {
	if (item.type === type) {
			return item;
	}});
	return item;
}

function showHideState() {
	var elem = $('#_address_country');
	if (elem.val() == 'US') {
		$('#state-container').show();
	} else if (elem.val() == 'PR') {
		$('#state-container').hide();
	}
}
	
function setPRFields() {
	$('#_address_town_container').show();
	$('#address_country_PR-container').show();
	$('#_address_country_PR').val('PR');
	$('#_address_country_PR').attr('disabled', true);
	$('#address_country-container').hide();
	$('#_address_country').val('');
	$('#state-container').hide();
	if(!MBSD1994) {
		$('#_address_field_2').removeClass('optional');
		if(!$('#_address_field_2').hasClass('required'))
		$('#_address_field_2').addClass('required');
	}
	changeTypeAddressType($('#_address_field_2'));
}



function skipProcess(element) {
	$(element).toggle();
	document.getElementById('procCont').style.display="block";
    document.getElementById('action').value = 'SKIP';
    document.getElementById('outreach').submit();   
}

function dataEdit(id) {
	newElement(id,"true");
}

function confirm(){
	if(document.getElementById('new_address').value == 'true'){	
		newElement("addressCountry", document.getElementById('_address_country_new').value);
		sortAddress();		
		if(document.getElementById('_address_country_new').value == 'PR'){
			newElement("addressTown", document.getElementById('_address_state_pr').value);
			newElement("addressPostalCode", document.getElementById('_address_zip_new').value);
			newElement("addressPostalCode2", document.getElementById('address_zip_new').value);
		}else if(document.getElementById('_address_country_new').value == 'US' || document.getElementById('_address_country_new').value == 'VI'){
			newElement("addressTown", document.getElementById('_address_town_new').value);
			newElement("addressState", document.getElementById('_address_state_new').value);
			newElement("addressPostalCode", document.getElementById('_address_zip_new').value);
			newElement("addressPostalCode2", document.getElementById('address_zip_new').value);
		}else{
			//International Address
			newElement("addressTown", document.getElementById('_address_city_new').value);
			newElement("addressPostalCode", document.getElementById('_address_intl_zip').value);
		}		
	}else{
	
		if(document.getElementById('_address_type').value == 4 && document.getElementById('_address_field_2').value == ""){
			newElement("newAddress1", document.getElementById('_address_field_1').value);
		}else{
			newElement("newAddress1", document.getElementById('_address_field_1').value);
			newElement("newAddress2", document.getElementById('_address_field_2').value);
		}	
		if(document.getElementById('_address_type').value == 1 
				|| document.getElementById('_address_type').value == 2 
				|| document.getElementById('_address_type').value == 3){
			newElement("addressCountryPR", document.getElementById('_address_country_PR').value);
			newElement("addressType", document.getElementById('_address_type').value);		
		}else if(document.getElementById('_address_type').value == 4 ){
			newElement("addressCountry", document.getElementById('_address_country').value);
			newElement("addressState", document.getElementById('_address_state').value);
			newElement("addressType", document.getElementById('_address_type').value);
		}else if(document.getElementById('_address_type').value == 5){
			newElement("addressCountry", document.getElementById('_address_country').value);
			newElement("addressType", document.getElementById('_address_type').value);
		}
		
		if(document.getElementById('_address_type').value != 5){
			newElement("addressTown", document.getElementById('_address_town').value);
		}
		
		newElement("addressPostalCode", document.getElementById('_address_zip').value);
		newElement("addressPostalCode2", document.getElementById('address_zip').value);
		
	}

	document.getElementById('action').value = 'CONFIRM';
	$('#btn-continue').addClass('disabled');
	document.getElementById('outreach').submit();

}

	function sortAddress(){
		if(document.getElementById('_address_field_1_new').value != "" && document.getElementById('_address_field_2_new').value == ""){
			newElement("newAddress1", document.getElementById('_address_field_1_new').value);
		}else if(document.getElementById('_address_field_1_new').value == "" && document.getElementById('_address_field_2_new').value != ""){
			newElement("newAddress1", document.getElementById('_address_field_2_new').value);
		}else if(document.getElementById('_address_field_1_new').value != "" && document.getElementById('_address_field_2_new').value != ""){
			newElement("newAddress1", document.getElementById('_address_field_1_new').value);
			newElement("newAddress2", document.getElementById('_address_field_2_new').value);
		}
	}

function newElement(id,value){
	
	document = document.getElementById('outreach');
	var x = document.createElement("input");
	x.setAttribute("type", "hidden");
    x.setAttribute("name", id);
    x.setAttribute("id", id);
    x.setAttribute("value", value);  
    document.outreach.appendChild(x);

}

function confirmProcess(element) {
	$("input:focus").blur();
	$(element).focus();
	if(editInfo) {
		if(validateFormFields()) 
			confirm();
		 else {
			$('#btn-continue').addClass('disabled');
			$('#_confirm').prop('checked', false);	
		}
		
	} else  
		confirm();
	
}

/**
 * Validate required fields that have been prefilled.
 * @method prefilledRequiredFields
 */
function validateFormFields(){
	var valid = true;
	var validOp = true;
	$('input:visible:not(.optional):not(:checkbox), select.required:visible').each(function() {
        valid = validateFieldForm($(this), false);
		if(!valid) {
			$(this).focus();
			return valid;
		}
    });
	if(!valid) return valid;
	$('input.optional:visible:not(:checkbox):not(.novalidate)').each(function() {
		//msg = obj.attr('data-msg-invalid');
		validOp = validateFieldForm($(this), true);
		if(validOp) {
			isValid($(this),true,'');
		} else {
		if(!MBSD1994) {isValid($(this),false,$(this).attr('data-msg-required'));}
			$(this).focus();
			return validOp;
		}
	});
	return valid && validOp;
}


/**
 * Validates different input types.
 * @method validateField
 * @param {object} obj - Input element to be processed.
 */
function validateFieldForm(obj, optional) {
	$('span.required', obj.parent()).remove();
    var val = obj.val();
    var msg = obj.attr('data-msg-required');
    
    var msgValidate = obj.attr('data-msg');
    var validationType = $.trim(obj.attr('data-validate'));
    
    switch (validationType) {
    case 'dropdown':
        // Validate user has selected an option
    	 
        return val != '-';
        break;
    case 'email':
        // Validate if has email pattern
    	if(val.length === 0) { obj.attr('data-msg-required',obj.attr('data-msg-requiredM')); isValid(obj, false, obj.attr('data-msg-required')); return false; }
    	else { obj.attr('data-msg-required',obj.attr('data-msg_invalid')); var tmp = isValidEmailAddress(val); if(tmp) isValid(obj, true, ''); else isValid(obj, false, obj.attr('data-msg-required')); return tmp;}
        break;
    case 'email_confirm':
        // Validate if has email pattern
        if ($('#_email').val() != $('#_email_confirm').val()) {
            isValid(obj, false, msg);
        }
        break;
    case 'ss':
        // Validate length of characters
        return val.length === 11;
        break;
    case 'phone':
        // Code here
    	if(optional && val.length > 0 && val.length === 12) return true;
    	else if(optional && val.length === 0) return true; 
    	else if(val.length === 12) return true;
    	else {obj.attr('data-msg-required',obj.attr('data-msg_invalid')); return false;}
        break;
    case 'text':
    	return val.length > 1;
        break;
    case 'date':
        // Code here
        return val.length === 10 ;
        break;
    case 'city':
        return val.length > 2;
        break;
    case 'state':
        // Validate user has selected an option
        return val != '-';
        break;
    case 'zip':
        // Code here
    		if(val.length > 0 && !optional) {
    			var pattern = /(^\d{5}$)/;
    			if(isValidZipCode(obj,pattern)) {
    				isValid(obj, true, '');
    				return true;
    			} else {
    				isValid(obj, false, msgValidate);
					if(MBSD1994) {visualValid($('#address_zip_new'));}
    				return false;
    			}
    		} else if (optional && val.length > 0) {
    			var pattern = /(^\d{4}$)/;
    			if(isValidZipCode(obj,pattern)) {
    				isValid(obj, true, '');
    				return true;
    			} else {
    				isValid(obj, false, msgValidate);
				if(MBSD1994) {visualValid($('#address_zip_new'));}
    				return false;
    			}
    		} else if (optional && val.length === 0) return true;
    		else {
    		if(MBSD1994) {
				isValid(obj, false, msg);
				visualValid($('#address_zip_new')); }
    			return false;
    		}
	        
	        break;
    case 'zipInt':
        // Code here
    		if (optional && val.length > 0) {
    			if(isValidZipCode(obj,/^[a-zA-Z0-9]{1,10}$/)) {
    				isValid(obj, true, '');
    				return true;
    			} else {
    				isValid(obj, false, msgValidate);
    				return false;
    			}
    		} else if (optional && val.length === 0) return true;
    		else {
    			return false;
    		}
	        
	        break;
    case 'addressNoPostal':
    	  $('span.required', obj.parent()).remove();
    	  if(MBSD1994) {
	  		var noEmpty = noEmptyAddressFields();
			var mainfield = findMainField('addressNoPostal','field');
			$('span.required', mainfield.parent()).remove();
			if(noEmpty == 0) {
				isValid(mainfield, false, mainfield.attr('data-msg-required')); 
				return false;
		  }
		}
    	 if(optional && val.length > 1) {
    		 if (isValidAddressNoPostal(obj, val)) {
  	 			isValid(obj, true, ''); 
  	 			return true;
  	 		 } else {
  	 		 if(MBSD1994){isValid(mainfield, false, msgValidate);}
  	 		 else {isValid(obj, false, msgValidate); }
 	 			return false;
  	 		 }
    	 } else if(optional && val.length === 0) return true; 
    	 else {
     	 	if(val.length > 1) {
     	 		if (isValidAddressNoPostal(obj, val)) {
     	 			isValid(obj, true, ''); 
     	 			return true;
     	 		} else {
				if(MBSD1994 && obj.prop('id').search('field')>0)
					isValid(mainfield, false, msgValidate); 
     		 		else
					isValid(obj, false, msgValidate); 
     	 			return false;
     	 		}
     	 		
     	 	} else {
     	 		if (!obj.hasClass('optional')) {
     	 			isValid(obj, false, msg);
     	 			return false;
     	 		}
     	 	}
    		 
    		}
        break;
	
	default :
		if(optional) return true;
		break;
	    }
 

}

function validatePrefilledRequiredFieldsCustom(){
    $('input.required, select.required').each(function(){
        validateFieldCustom($(this));
    });
}

function validateFieldCustom(obj) {
    var val = obj.val();
    var msg = obj.attr('data-msg-required');
    var msgValidate = obj.attr('data-msg');
    
    var validationType = obj.attr('data-validate');
    switch (validationType) {
    case 'addressNoPostal':

    	$('span.required', obj.parent()).remove();
    	var id = obj.prop('id');
		if(MBSD1994) {
			var mainfield = findMainField('addressNoPostal','field');
			$('span.required', mainfield.parent()).remove();
		}
    	var findNext = "input[id^='_address_field_'][id*='_new'][id!='" + id + "']";
		var addressNextTo = jQuery(findNext);
		if(val.length > 1){
    		if (isValidAddressNoPostal(obj, val)) {
    			isValid(obj, true, '');
    			if(id.match('new$')) {
        			if (addressNextTo.val().length == 0) { 
        				addressNextTo.focus();
        				addressNextTo.focusout();
        			}
        		}
    		} else {
				if(MBSD1994 && id.search('field')>0)
					isValid(mainfield, false, msgValidate); 
				else
					isValid(obj, false, msgValidate);
    		}     		
    	}else{
    		if(id.match('new$')) {
    		
    			if (addressNextTo.val().length > 1 && id.search('field')>0) { 
    				isValid(obj, true, '');
    				obj.removeClass('required');
    				if(!obj.hasClass('optional'))
    					obj.addClass('optional');
    			} else { 
				if(MBSD1994 && id.search('field')>0)
					isValid(mainfield, false, mainfield.attr('data-msg-required')); 
				else    	
					isValid(obj, false, msg); 
    				obj.removeClass('optional');
    				if(!obj.hasClass('required')) 
    					obj.addClass('required');
    				
    			}
    		} else {
    			if(!obj.hasClass('optional'))
				if(MBSD1994 && id.search('field')>0)
					isValid(mainfield, false, mainfield.attr('data-msg-required')); 
				else
					isValid(obj, false, msg);
    		}
    	}
    	
        break;
    }

}

function isValidAddressNoPostal(obj, addressNoPostal) {
    var answer=false;
    var answer1=false;
    
	var pattern = /(G*[(\.|\s|\-)]*P[(\.|\s|\-)]*O[(\.|\s|\-)]*Box)|(P[(\.|\s|\-)]*M[(\.|\s|\-)]*B[(\.)]*)|(R[(\.|\s)]*R[(\.|\s|\-)]*Box)|(H[(\.|\s)]*C[(\.|\s|\-)]*Box)|(Call[(\.|\s|\-)]*Box)/i;
	var pattern1 = /^[a-zA-Z0-9\s]+$/;
    if((addressNoPostal != "") || $(obj).hasClass('optional')){
		answer = pattern.test(addressNoPostal);
		answer1= pattern1.test(addressNoPostal);
	}
	return !(answer || !answer1);
};

function isValidZipCode(obj,pattern) {
	if(pattern)
	return pattern.test(obj.val());
	else return false;
}

function changeTypeAddressType(obj){
	if($(obj).hasClass('required')){
		
		$(obj).focusout(function () {
		  
			// Validate field
		    validateField($(this));
		    validateFieldCustom($(this));
		});
		$(obj).change(function () {
	        // Validate field
	        validateField($(this));
	        validateFieldCustom($(this));
	    });
		validateField($(obj));
        
	}else if($(obj).hasClass('optional')){
		$(obj).focusout(function () {
			validateFieldCustom($(this));
			
		});
			 
		$(obj).change(function () {
			validateFieldCustom($(this));
			
		});
		isValid(obj, true, '');
		
	}
	validateFieldCustom($(obj));
	
}

function initAddress(obj) {
	//document.getElementById('_address_country_new').selectedIndex = 0;
	jQuery('#_address_country_new').val('PR').change();
}

function noEmptyAddressFields() {
	var findFields = "input[data-validate='addressNoPostal'][id*=field][id*='_new']";
	var addressFields = jQuery(findFields);
	var noEmpty = 0;
	if (addressFields.length > 0) {
		addressFields.each(function() {noEmpty += $(this).val().length;})
	}
	return noEmpty;

}

function findMainField(dataValidate,idCustom) {
	var findFields = "input[data-validate='"+dataValidate+"'][id*="+idCustom+"][id*='_new'][class*='mainfield']";
	var mainField= jQuery(findFields);
	return mainField;
}

function visualValid(obj) {
	$('span.required', obj.parent()).remove();
        // Get the parent div that holds the input obj
    obj.parent().removeClass('has-error error-field').addClass('has-success success-field');
	
}

$(function () {
    // Set fields that are only numeric
    $('.alphanumeric').keydown(function (e) {
    	
    	var regex = new RegExp("^[a-zA-Z0-9]+$");
    	
    	// Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [ 8, 9, 27, 13, 110, 32]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a alphanumeric and stop the keypress
        if (!regex.test(e.key)) {
            e.preventDefault();
        }
        
    });
});