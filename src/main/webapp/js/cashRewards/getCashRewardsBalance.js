
function getCashBackInfo(frontEndId, onCompleteFunction) {
	var pars = 'frontEndId=' + frontEndId;

	new Ajax.Request('tsysLoyaltyRewardsBalanceInformation', 
	{
		method:'get',
		contentType : 'application/json; charset=utf-8',
		dataType:'json',
		parameters:pars,
		onComplete: onCompleteFunction
	});
}

function setUpDisabledButton(cashBackEnabledButton, cashBackDisabledButton, errorMessage) {
	cashBackEnabledButton.parentElement.removeChild(cashBackEnabledButton);
	cashBackDisabledButton.setAttribute("onmouseover", "Tip('" + errorMessage + "', BGCOLOR,'#E8F4F8', BORDERCOLOR,'#9BB9CF', WIDTH,140, ABOVE,true, OFFSETX,-17, FADEIN,150, FADEOUT,150, PADDING,8)");
	cashBackDisabledButton.style.display = 'block';
}

function cashBackWidgetLoader(frontEndId, custemerServiceErrorMessage, minimumBalanceErrorMessage, noBalancePlaceholder) {
	var ajaxCallback = function(response) {

		var cashBackWidgetAmountDiv = document.getElementById("cashBackAmount");
		var cashBackDisabledButton = document.getElementById('cashBackDisabledButton');
		var cashBackEnabledButton = document.getElementById('cashBackButtonEnabled');
		
			cashBackWidgetAmountDiv.classList.remove("loading"); 
			cashBackWidgetAmountDiv.classList.add("cashBackWidgetAmount");

		var responseJson = "";
		
		try {
			responseJson = JSON.parse(response.responseText);
		}
		catch(err) {
			cashBackWidgetAmountDiv.innerText = noBalancePlaceholder;
			setUpDisabledButton(cashBackEnabledButton, cashBackDisabledButton, custemerServiceErrorMessage);
			return;
		}
		
		if(responseJson.status != '200' || !responseJson.data[0].availableRewardsBalance) {
			cashBackWidgetAmountDiv.innerText = noBalancePlaceholder;
			setUpDisabledButton(cashBackEnabledButton, cashBackDisabledButton, custemerServiceErrorMessage);
			return;
		}
		
		cashBackWidgetAmountDiv.innerText = responseJson.data[0].availableRewardsBalance;
		
		if(!responseJson.data[0].canRedeemRewards) {
			setUpDisabledButton(cashBackEnabledButton, cashBackDisabledButton, custemerServiceErrorMessage);
			return;
		}
	
		if(responseJson.data[0].availableBalanceDouble < responseJson.data[0].minimumRewardsBalance) {
			setUpDisabledButton(cashBackEnabledButton, cashBackDisabledButton, minimumBalanceErrorMessage);
		} else {
			cashBackDisabledButton.parentElement.removeChild(cashBackDisabledButton);
			cashBackEnabledButton.style.display = 'block';
		}
	}
	getCashBackInfo(frontEndId, ajaxCallback);
}


function getCashBackBalance(frontEndId, noBalancePlaceholder) {

	var ajaxCallBack = function(response) {	
		var loyaltyRewardsDiv = document.getElementById("vpcbk-"+frontEndId);
		
		var json = '';
		
		try {
			json = JSON.parse(response.responseText);
		}
		catch(err) {
			loyaltyRewardsDiv.innerText = noBalancePlaceholder;
		}
		
		var result = json.data[0];
		var errors = json.errors[0];

		if(errors){
			loyaltyRewardsDiv.innerText = noBalancePlaceholder;
		} else if (result.availableRewardsBalance == null){
			loyaltyRewardsDiv.innerText = noBalancePlaceholder;
		} else{
			loyaltyRewardsDiv.innerText = result.availableRewardsBalance;
		}	 
	}
	getCashBackInfo(frontEndId, ajaxCallBack);	
}
