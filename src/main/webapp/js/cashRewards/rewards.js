function submitForm(action) {
  $(":disabled").each(function(e) {
    $(this).removeAttr("disabled");
  });
  document.getElementById("rewardsRedemption").submit();
}

function goToWizardPage(formId, page) {
  var param = document.createElement("input");
  param.name = "_target" + page;
  param.type = "hidden";
  param.value = "";
  document.getElementById(formId).appendChild(param);
  document.getElementById(formId).submit();
}

function goToFinishPage(formId) {
  var param = document.createElement("input");
  param.name = "_finish";
  param.type = "hidden";
  param.value = "";
  document.getElementById(formId).appendChild(param);
  document.getElementById("rewardsRedemption").submit();
}

function canEnableButton() {
    return $("#termsAndCond").is(":checked") && 
            (($("#directDeposit").is(":checked") && $.trim($("#acctNumber").val()) !== "none") ||
            $("#statementCredit").is(":checked")) &&
            isEnteredAmountValid();
}

function isEnteredAmountValid() {
	
	var amountEntered = parseFloat($("#redeemAmount").val());
	if(isNaN(amountEntered)) return false;
	
	var minimumAmount = parseFloat($("#minimumAmount").val());
	var maximumAmount = parseFloat($("#maximumAmount").val());
	
	return amountEntered >= minimumAmount && amountEntered <= maximumAmount;
}

function uncheckTermsAndConditions() {
	$("#termsAndCond").prop("checked", false);
}

function toggleRedemptionOptions(redemptionOption) {
	uncheckTermsAndConditions();
	toggleContinueButton();
	if(redemptionOption == "DIRECT_DEPOSIT") {
		$("#directDeposit").prop("checked", true);
	    $("#statementCredit").prop("checked", false);
	    
	    $("#directDepositOption")
	    		.removeClass("directDepositOption")
	    		.addClass("selectedOption");
	      
	    $("#statementCreditOption")
	    		.removeClass("selectedOption")
	    		.addClass("statementCreditOption");
	} 
	else {
		$("#statementCredit").prop("checked", true);
	    $("#directDeposit").prop("checked", false);
	    
	    $("#statementCreditOption")
	    		.removeClass("statementCreditOption")
	    		.addClass("selectedOption");
	      
	    $("#directDepositOption")
	    		.removeClass("selectedOption")
	    		.addClass("directDepositOption");
	}
}

function toggleContinueButton() {
	if(canEnableButton()) {
		$("#continue")
	    .removeClass("btn-disabled")
	    .addClass("btn-enabled");
	}
	else {
		$("#continue")
	    .removeClass("btn-enabled")
	    .addClass("btn-disabled");
	}
}

function amountEnteredOnKeyUp() {
	uncheckTermsAndConditions();
	toggleContinueButton();
	
	if(!isEnteredAmountValid()) {
	    var minimumAmount = parseFloat($("#minimumAmount").val());
	    var maximumAmount = parseFloat($("#maximumAmount").val());
	    var amountEntered = parseFloat($("#redeemAmount").val());

	    if (amountEntered > maximumAmount) {
	    	$("#redeemAmount").css("borderWidth", "1px");
	    	$("#redeemAmount").css("borderColor", "red");

	    	$("#highAmountError").css("display", "block");
	    	$("#lowAmountError").css("display", "none");
	    }
	}
	else {
		$("#redeemAmount").css("borderColor", "#dce0e5");
		$("#highAmountError").css("display", "none");
		$("#lowAmountError").css("display", "none");
	}
}

function ammountEnteredOnblur() {
	toggleContinueButton();

	if(!isEnteredAmountValid()) {
		$("#redeemAmount").css("borderWidth", "1px");
	    $("#redeemAmount").css("borderColor", "red");
	    
	    var minimumAmount = parseFloat($("#minimumAmount").val());
	    var maximumAmount = parseFloat($("#maximumAmount").val());
	    var amountEntered = parseFloat($("#redeemAmount").val());

	    if(amountEntered < minimumAmount || isNaN(amountEntered)) {
	    	$("#lowAmountError").css("display", "block");
	    	$("#highAmountError").css("display", "none");
	    }
	}
	else {
		$("#redeemAmount").css("borderColor", "#dce0e5");
		$("#highAmountError").css("display", "none");
		$("#lowAmountError").css("display", "none");
	}
}

$(document).ready(function() {
	//When going back to the cash rewards configuration page terms and conditions
	//must be unchecked.
	uncheckTermsAndConditions();
	
	// Prevent safari from using cache when back is pressed
	window.onpageshow = function (event) {
        if (event.persisted) {
        window.location.reload();
        }
	}

	$("#redeemAmount").inputmask({
	    alias: "numeric",
	    allowMinus: false,
	    digits: 2,
	    max: 9999999999.99,
	    rightAlign: false
	});
});
