function getRetirementPlanInfo(onCompleteFunction) {
	var pars = "fisRetirementPlanInformation";
	new Ajax.Request('fisRetirementPlanInformation', {
		method : 'get',
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		parameters : pars,
		onComplete : onCompleteFunction
	});
}



function getRetirementPlan() {

	var retplantitle = document.getElementById("retplantitle");
	retplantitle.style.display = "none";

	var plan = document.getElementById("plan");
	plan.style.display = "none";

	var retirementPlanAccountNewBadge = document.getElementById("retplanbad");

	var planDisclaimer = document.getElementById("planDisclaimer");
	planDisclaimer.style.display = "none";
	
	var retPlanDowntime = document.getElementById("retPlanDowntime");
	retPlanDowntime.style.display = "none";
	
		var ajaxCallBack = function(response) {

		var json = '';
		json = JSON.parse(response.responseText);

		if (json.hasRetPlan && json.retplanDowntimeMessage == false) {

			retplantitle.style.display = retplantitle.style.display === 'none' ? ''
					: 'none';

			if (!json.retirementPlanAccountNewBadge) {
				retirementPlanAccountNewBadge.style.display = "none";
			}
			
			var formatter = new Intl.NumberFormat('en-US', {
				  style: 'currency',
				  currency: 'USD'
				});


			for (var i = 0; i < json.retplan.plans.length; i += 1) {

				var txt = "";
				var currTR = plan.parentNode;

				var newTR = document.createElement("tr");

				txt += "<td><a href='https://www.yourplanaccess.com/banco/' target='_blank'>"
						+ json.retplan.plans[i].plannam + "</a></td>";
				txt += "<td>" + json.retplan.plans[i].accountLast4Num + "</td>";
				txt += "<td class='balance'>"
						+ formatter.format(json.retplan.plans[i].planTotalBalance) + "</td>";

				newTR.innerHTML = txt;

				currTR.insertBefore(newTR, plan);
			}

			planDisclaimer.style.display = planDisclaimer.style.display === 'none' ? ''
					: 'none';

			document.getElementById("loading").style.display = "none";

		} else if (json.hasRetPlan && json.retplanDowntimeMessage) {

			retplantitle.style.display = retplantitle.style.display === 'none' ? ''
					: 'none';

			retirementPlanAccountNewBadge.style.display = "none";

			retPlanDowntime.style.display = retPlanDowntime.style.display === 'none' ? ''
					: 'none';

			document.getElementById("loading").style.display = "none";
		}

		document.getElementById("loading").style.display = "none";

	}
	getRetirementPlanInfo(ajaxCallBack);
}
