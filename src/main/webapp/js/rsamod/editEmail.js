var required = function(fields) {
    var valid = true;
    fields.each(function() { // iterate all
        var $this = jQuery(this);
        if ((($this.is(':text') || $this.is('textarea')) && !$this.val()) || // text and textarea
            ($this.is(':radio') && !jQuery('input[name=' + $this.attr("name") + ']:checked').length)) { // radio
            valid = false;
        }
    });
    return valid;
};

var validateRealTime = function() {
    var fields = jQuery(":input");
    fields.on('keyup change keypress blur', function() {
        if (required(fields)) {
            jQuery("#btnValidateEmail").prop('disabled', false);
        } else {
            jQuery("#btnValidateEmail").prop('disabled', true);
            cleanErrorMessages();
        }
    });
};

function validateEmailData() {
    var newEmail = $('#emailInput').val();
    var confirmEmail = $('#confirmEmailInput').val();

    cleanErrorMessages();
    editEmailToogleForm();
    $.ajax({
        type: "POST",
        url: "/cibp-web/actions/mobileChangeEmail/validate",
        data: "newEmail=" + newEmail + "&confirmEmail=" + confirmEmail
    }).done(function(response) {
        showRsaDialogForEmailChange('challenge-dialog', 'rsaChallenge.html');
    }).fail(function(e) {
        editEmailToogleForm();
        if (e.status === 422) {
            response = JSON.parse(e.responseText);
            for (var i = 0; i < response.length; i++) {
                var error = response[i];
                var errorToDisplay = jQuery('#' + error.fieldName + 'Error');
                errorToDisplay.html(errorToDisplay.html() + error.message);
            }
        }
    });
}

function cleanErrorMessages() {
    $('*[id*=Error]').each(function () {
        $(this).html('');
    });
}

function showRsaDialogForEmailChange(id, html) {
    var dialog = document.getElementById(id);

    if (dialog) {
        dialog.parentNode.removeChild(dialog);
    }

    ons.createElement(html, { append: true })
        .then(function(dialog) {
            jQuery.ajax({
                url : 'rsaChallengeMethod',
                data: {}
            }).done(function(response) {
                if(isOobAuthenticated(response)) {
                    editEmail();
                } else {
                    var promRsaDialog = dialog.show({'callback' : cancelSetAction()});
                    jQuery("#oobChallenge").load("../actions/initRsa?function=editEmail");
                    promRsaDialog.then(function(result) {
                    var myVar = setTimeout(cancelSetAction, 2000);
                    }
                    , function(err) {
                    	console.log(err);
                    });
                }
            })
        });
}

function isOobAuthenticated(plainResponse) {
    return JSON.parse(plainResponse).isOobAuthenticated === "true";
}

function editEmail() {
    var email = document.getElementById('emailInput').value;
    var emailConfirm = document.getElementById('confirmEmailInput').value;
    var owaspCsrfToken = document.getElementById('owaspCsrfToken').value;
    
    jQuery.ajax({
        type: 'post',
        url: 'editemail',
        data: {
            email: email,
            emailconfirm: emailConfirm,
            owaspCsrfToken:owaspCsrfToken
        }
    }).done(function (response) {
        var parsedResponse = jQuery.parseHTML(response);
        var errors = jQuery('#errors', parsedResponse).text();

        if (errors === '') {
            dialogWithIntervalNewLocation('dialogEditEmailSuccess', '?back', timeInterval);
            return;
        }

        dialogWithIntervalNewLocation('dialogEditEmailFail', '?back', timeInterval);
    }).fail(function (response) {
        dialogWithIntervalNewLocation('dialogEditEmailSystemFail', '?back', timeInterval);
    });
}

function editEmailToogleForm() {
	var btnValidateEmail = $("#btnValidateEmail");
    var inEmail = $("#emailInput");
    var inConfirmEmail = $("#confirmEmailInput");
    var btnValidateEmailDisabled = btnValidateEmail.prop('disabled');
    var inEmailDisabled = inEmail.prop('disabled');
    var inConfirmEmailDisabled = inConfirmEmail.prop('disabled');
    inEmail.attr("disabled", !inEmailDisabled);
    inConfirmEmail.attr("disabled", !inConfirmEmailDisabled);
    btnValidateEmail.attr("disabled", !btnValidateEmailDisabled);
}

function cancelSetAction () {
    var alink = $('#btnRsaCancel');
    if(alink[0]) {
	    alink[0].onclick = function () {
       	hideDialog('challenge-dialog');
      	editEmailToogleForm();
    	};
	}
}