/**
 * RSA manage tool
 * Const
 * Version 1.0.0.2
 */
 
const RSANAVIGATORELEMENT = '#rsaEnrollNavigator';
var services = "Service: ";
var fnNew = '';
var btnBack = '';
const ATTRIBUTE_CLICK = "onclick";
const CONTEXT = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
const REDIRECT_LOGIN =window.location.protocol+"//"+ window.location.host +CONTEXT+"/actions/login";
const NAME_RSMOD_TAG = 'rsaMod';

var delgaelement = document.getElementById("delga");

var valAg = false;
/**End**/

if(delgaelement != null){
	valAg = delgaelement.getAttribute(value);
}
function validatePrimaryNumber(){
	var num = document.getElementById("mainNumber").value;
	var carrier = document.getElementById("carrierLabel").innerText;
	if(num != undefined  && num != ""){
		num = num.replace(/[^0-9.]/g, "");
	}
	if(num != undefined  && num != "" && num.length == 10 && carrier != undefined && carrier != "" && carrier != "Select" && carrier != "Seleccionar"){
		var btnSave = document.getElementById("btnSavePrimaryNumber");
		btnSave.style.backgroundColor='#2278C9';
		showDialog('terms-dialog', 'terms.html');
	}else{
		showDialog('dialogSMSProviderFail', 'dialogSMSProviderFail.html');
		if(!num && num.length < 10 && document.getElementById("rsaSMSProfileError")) {
			document.getElementById("rsaSMSProfileError").innerHTML = _msgSMSPhoneInvalid;
		} else if(!carrier && carrier === "Select" && carrier === "Seleccionar") {
			document.getElementById("rsaSMSProfileError").innerHTML = _msgSMSProviderInvalid;
		}

	}
}

function selectPreference(){
	var sms = document.getElementById("divSms");
	var call = document.getElementById("divCall");
	if(document.getElementById("radioSms").checked == true){
		document.getElementById("imgSms").src = "../img/rsa/SMS_Active.svg";
		document.getElementById("imgCall").src = "../img/rsa/Phone_Inactive.svg";
		document.getElementById("challengeType").value = 'OOBSMS';

		sms.style.border ="1px solid #3D81CE";
		sms.style.borderradius ="5px";
		sms.style.backgroundColor ="#EFF3F7";
		sms.style.boxshadow = "0 1px 2px 0 rgba(0,0,0,0.15)";

		call.style.border ="1px solid #CCD1D6";
		call.style.borderradius ="5px";
		call.style.backgroundColor ="#FFFFFF";
	}

	if(document.getElementById("radioCall").checked == true){
		document.getElementById("imgSms").src = "../img/rsa/SMS_Inactive.svg";
		document.getElementById("imgCall").src = "../img/rsa/Phone_Active.svg";
		document.getElementById("challengeType").value = 'OOBPHONE';

		sms.style.border ="1px solid #CCD1D6";
		sms.style.borderradius ="5px";
		sms.style.backgroundColor ="#FFFFFF";

		call.style.border ="1px solid #3D81CE";
		call.style.borderradius ="5px";
		call.style.backgroundColor ="#EFF3F7";
		call.style.boxshadow = "0 1px 2px 0 rgba(0,0,0,0.15)";
	}

}

function getRsaQuestion(urlPar) {
	j.ajax({
		url : urlPar,
		cache : false
	}).done(function(response) {
		if (response) {
			var res = JSON.parse(response);
			document.getElementById("securityQuestion").innerHTML = res.securityQuestion;
			document.getElementById("btnValidateQuestion").disabled=false;
			jQuery('#question-loading').fadeOut('fast',
				function() {
					document.getElementById("question").style.display = 'block';
					jQuery('#question').fadeIn('slow');
				}
			);

			if(res.devicetoken != ""){
				setRSAToken(res.devicetoken);
			}
		}
	})
}

var check = "please check later";
function getRsaQuestions(urlPar) {
	j.ajax({
		beforeSend: function(){
			document.getElementById("btnEditQuestions").disabled=true;
		},
		url : urlPar,
		cache : false
	}).done(function(response) {
		if (response) {
			var res = JSON.parse(response);
			if(res.result == 'success'){
				extractQuestions(res.qsedit1, "sq1");
				extractQuestions(res.qsedit2, "sq2");
				extractQuestions(res.qsedit3, "sq3");
				if(res.devicetoken != ""){
					setRSAToken(res.devicetoken);
				}
			} else {
				alert(check);
			}
			document.getElementById("btnEditQuestions").disabled=false;

		}
	})
}


function extractQuestions(questionsObject, selectComp) {
	var getKeys = Object.keys(questionsObject);
	if(getKeys.length > 0) {
		for(var key in getKeys) {
			addOptionQuestions(selectComp,questionsObject[getKeys[key]],getKeys[key]);
		}
	}
}

function addOptionQuestions(selectComp,label,value) {
	var option = document.createElement("option");
	option.text = label;
	option.value = value;
	document.getElementById(selectComp).add(option);
}


/** Add cookie  */
function setRSAToken(deviceToken){
	var date = new Date();
	date.setTime(date.getTime()+(365*24*60*60*1000));
	var expires = "; expires="+date.toGMTString();
	document.cookie='PMData='+ deviceToken +';expires='+expires+';path=/';
}

function validateQuestion(element, nextPage) {
	var answer = jQuery('#answer')[0].value;
	jQuery.ajax({
		beforeSend: function(){
			element.disabled = true;
		},
		url : 'validateQuestion',
		data: {'answer': answer}
	}).done(function(response) {
		sendAction(response, nextPage);
		element.disabled = false;
	})
}

function sendAction(response, nextPage) {
	if (response) {
		var res = JSON.parse(response);
		if(res.messageError != ""){
			jQuery('#errorAnswer')[0].innerHTML = res.messageError;
			document.getElementById("onslistQuestion").style.paddingTop= 'initial';
			document.getElementById("border").style.border= '1px solid rgba(255,0,0,1)';
		}else if(res.result == "SUCCESS"){
			document.getElementById("onslistQuestion").style.paddingTop= '20px';
			document.getElementById("border").style.border= '1px solid rgba(255,255,255)';
			jQuery('#errorAnswer')[0].innerHTML = '';
			if (nextPage) {
				document.querySelector(RSANAVIGATORELEMENT).pushPage(nextPage, {data: {title: ''}});
			} else if(res.addPhoneNumber == "true"){
				document.querySelector(RSANAVIGATORELEMENT).pushPage('rsaPhone.html', {data: {title: ''}});
			} else {
				document.querySelector(RSANAVIGATORELEMENT).pushPage('rsaSecondaryPhone.html',
					{data: {title: '',primaryPhone : res.primaryPhone}}
				);
			}
		} else if(res.result == "BLOCKED"){
			location.replace('rsaEnroll?block');
		}
	}
}

function validateQuestionRsa(element, nextPage, fn) {
	var answer = jQuery('#answer')[0].value;
	jQuery.ajax({
		beforeSend: function(){
			element.disabled = true;
			jQuery('#msgResponse')[0].innerHTML = '';
		},
		url : 'validateQuestion',
		data: {'answer': answer}
	}).done(function(response) {
		if(fn){
			getFunction(response, fn);
		}else{
			nextToAction(response, nextPage);
		}
		element.disabled = false;
	}).fail(function(jqXHR, textStatus) {
		var status = services + textStatus + ' ' + jqXHR.status;
		alert(status);
		element.disabled = false;
	})
}

function nextToAction(response, args) {
	if(response) {
		var res = JSON.parse(response);
		if(res.messageError != ""){
			jQuery('#msgResponse')[0].innerHTML = res.messageError;
		} else if(res.result == "BLOCKED") {
			if(res.onsenUI) {
				location.replace('rsaEnroll?block');
			} else {
				redirectUser();
			}
		} else if(res.result == "SUCCESS") {
			if(res.onsenUI) {
				hideDialog('challenge-dialog');
			} else { toggleRsaModal(document.getElementsByTagName(NAME_RSMOD_TAG)[0]); }
			if(isFunction(args)) {
				args();
				btnBack.onclick = fnNew;
			} else if (Array.isArray(args)) {
				//Array check
			} else if (isObject(args)) {
				//Object check
			}
			//args will be check to verify type of action
		}
	}
}

function getFunction(response, fn){
	var _fn = fn.replace(/['"()]+/g, '');
	nextToAction(response, window[_fn]);
}

function addAlternateNumber() {
	document.getElementById("divAddAlternateNumber").style.display = 'block';
	document.getElementById("divMask").style.display = 'block';
}


var hideDialog = function(id) {
	if(document.getElementById(id)){
		document.getElementById(id).hide();
	}
};

var hideDialogAndDisabledButton = function(nextScreen,id) {
	document.getElementById(id).hide();
	if(nextScreen == 'terms.html'){
		disabledScreenRecoveryCode(false);
	}
};

var hideDialogRSABlock = function(id, user, isMobile) {
	document.getElementById(id).hide();
	if(isMobile){
		//This line send information to native device for close session.
		location.replace('rsaEnroll?block');
	}else{
		redirectUser();
	}
};

var showRsaDialog = function(id, html, nextScreen) {
	var dialog = document.getElementById(id);
	if (dialog) {
		dialog.parentNode.removeChild(dialog);
	}
	ons.createElement(html, { append: true })
		.then(function(dialog) {
			jQuery.ajax({
				url : 'rsaChallengeMethod',
				data: {}
			}).done(function(response) {
				var res = JSON.parse(response);

				if(res.isOobAuthenticated == "true"){
					if(nextScreen == "terms.html"){
						newRecoveryCode('newRecoveryCodeOOB', this);
					}else{
						document.querySelector(RSANAVIGATORELEMENT).pushPage(nextScreen, {data: {title: ''}});
					}
				}else{
					dialog.show();
					if(res.challengeMethod == "OOBSMS"){
						jQuery("#oobChallenge").load("../actions/initRsa?nextScreen=" + nextScreen);
					}else{
						jQuery("#oobChallenge").load("../actions/initRsa?call=true&nextScreen=" + nextScreen);
					}
				}
			})
		});
};


var showDialog = function(id, html,event,method) {
	var dialog = document.getElementById(id);
	if (dialog) {

			let d = dialog.show();
			d.then('','');

		dialogSecondaryPhone(dialog);

	} else {
		ons.createElement(html, { append: true })
			.then(function(dialog) {

				if(event && method) {dialog.addEventListener(event, method);}
				dialog.show();
				dialogSecondaryPhone(dialog);
			});
	}
};

function dialogSecondaryPhone(dialog){
	if(dialog.id == 'dialogSecondaryPhone'){
		if(jQuery("#secondaryPhoneP")[0].childNodes[0].textContent != '(XXX) XXX-XXXX'
			&& jQuery("#secondaryPhoneP")[0].childNodes[0].textContent != ''){
			changeDialogSecondaryPhoneStyle("display: inline-block", "margin-top: 10px;", "padding: 0 5px 70px 10px;");
		}else {
			changeDialogSecondaryPhoneStyle("display:none", "", "padding: 0 5px 0 10px;");
		}
	}
}

function changeDialogSecondaryPhoneStyle(display, margin ,padding){
	var styleCss = display + ";" + margin;
	jQuery("#idDeleteSecondaryPhone").attr("style", styleCss);
	jQuery("#idlistItemSecondaryPhone").attr("style", padding);
}

function showActionSheet (id, html) {
	var sheet = document.getElementById(id);

	if (sheet) {

		let s = sheet.show();
		 s.then('','');

	} else {
		ons.createElement(html + '.html', { append: true })
			.then(function(sheet) {
				sheet.show();
			});
	}
}



function hideSheet (id) {
	document.getElementById(id).hide();
}

function selectCarrier(inputId, labelId, value,carrierId){
	var input = document.getElementById(inputId);
	var label = document.getElementById(labelId);
	input.value = carrierId;
	/**input.onchange();**/
	label.innerHTML = value;
}
function selectSecundaryCarrier(inputId, labelId, value, id){
	var input = document.getElementById(inputId);
	var label = document.getElementById(labelId);
	input.value = value;
	/**input.onchange();**/
	label.innerHTML = value;
	document.getElementById('alternateProvider').value=value;
	document.getElementById('secondaryCarrierHide').value=id;
}

function saveSecondaryPhone(){
	var secondaryNumberInp = jQuery("#secondaryNumberInp")[0];
	var carrierSecundary = document.getElementById("carrierSecundary");
	var secPhone = secondaryNumberInp.value;
	var secPhoneLast = document.getElementById("secondaryNumberInp");
	var lastSecPhone = secPhoneLast.value;
	var carrSec = carrierSecundary.value;

	if(secPhone != null){
		secPhone = secPhone.replace(/[^0-9.]/g, "");
	}
	if(carrSec != null && carrSec != '' && secPhone != null && secPhone.length == 10) {
		document.getElementById("secondaryPhoneSelected").style.display = 'block';
		document.getElementById("buttomAddSecondaryPhone").style.display = 'none';
		document.getElementById("dialogSecondaryPhone").hide();
		document.getElementById("secondaryPhoneP").textContent = lastSecPhone;
		document.getElementById("secondaryPhoneHide").value = secPhone;
		//document.getElementById("secondaryCarrierHide").value = carrSec;
	} else {
		showDialog('dialogSMSProviderAltFail', 'dialogSMSProviderAltFail.html');
		if(!secPhone && secPhone.length < 10 && document.getElementById("rsaSMSProfileError")) {
			document.getElementById("rsaSMSProfileError").innerHTML = _msgSMSPhoneInvalid;
		} else if (!carrSec && carrSec === "Select" && carrSec === "Seleccionar") {
			document.getElementById("rsaSMSProfileError").innerHTML = _msgSMSProviderInvalid;
		}


	}
}

function clearSecondaryPhone(){
	jQuery("#secondaryNumberInp")[0].value = '';
	jQuery("#secondaryPhoneP")[0].value = '';
	jQuery("#secondaryPhoneP")[0].childNodes[0].textContent='';
	jQuery("#secondaryPhoneHide")[0].value = '';
	jQuery("#secondaryPhoneSelected")[0].style.display = 'none';
	jQuery("#buttomAddSecondaryPhone")[0].style.display = 'block';
	jQuery("#dialogSecondaryPhone")[0].hide();
}



function clearSecondary(mbsd3686) {
	document.getElementById("secondaryNumberInp").value = '';
	if(!mbsd3686) {document.getElementById("carrierSecundary").value = '';}
	var secondaryPhoneN = document.getElementById("secondaryPhoneHide");
	var secondatyCarrierN = document.getElementById("secondaryCarrierHide");
	if(secondaryPhoneN && secondaryPhoneN.value && secondaryPhoneN.value === 'N'
		&& secondatyCarrierN && secondatyCarrierN.value && secondatyCarrierN.value === 'N') {
		document.getElementById("secondaryPhoneHide").innerHTML = 'N';
		document.getElementById("secondaryCarrierHide").innerHTML = 'N';
		document.getElementById("secondaryPhoneSelected").style.display = 'none';
		document.getElementById("buttomAddSecondaryPhone").style.display = 'block';
	}
}

function notReceiveCode() {
	document.getElementById("notCode").style.display = 'block';
}

function generateNewCode(isAltPhone) {
	msgContainer = jQuery("#msgResponse");
	msgContainer.html("");
	jQuery.ajax({
		url : 'sendNewCode',
		cache : false,
		data: {'isAltPhone': isAltPhone}

	}).done(function(response) {
		if (response) {
			var res = JSON.parse(response);
			if(res.result == "BLOCKED"){
				if(res.onsenUI && res.onsenUI == "true") {
					location.replace('rsaEnroll?block');
				} else {
					redirectUser();
				}
			} else if(res.result){
				msgContainer.html(res.message);
				msgContainer.removeClass("messageFail");
				msgContainer.addClass("messageSuccess");
			} else {
				msgContainer.html("Service not available, try later");
				msgContainer.removeClass("messageSuccess");
				msgContainer.addClass("messageFail");
			}

		} else {
			msgContainer.html("Service not available, try later");
			msgContainer.removeClass("messageSuccess");
			msgContainer.addClass("messageFail");

		}
	});
}

function newCall(isAltPhone) {
	msgContainer = jQuery("#msgResponse");
	msgContainer.html("");
	jQuery.ajax({
		url : 'requestNewCall',
		cache : false,
		data: {'isAltPhone': isAltPhone}

	}).done(function(response) {
		if (response) {
			var res = JSON.parse(response);
			codeDisplay = jQuery("#codeUsr");
			if(res.result == "BLOCKED"){
				if(res.onsenUI && res.onsenUI == "true") {
					location.replace('rsaEnroll?block');
				} else {
					redirectUser();
				}
			} else if(res.result){
				msgContainer.html(res.message);
				msgContainer.removeClass("messageFail");
				msgContainer.addClass("messageSuccess");
				codeDisplay.html(res.callCode);
			}else{
				msgContainer.html("Service not available, try later");
				msgContainer.removeClass("messageSuccess");
				msgContainer.addClass("messageFail");
			}

		} else {
			msgContainer.html("Service not available, try later");
			msgContainer.removeClass("messageSuccess");
			msgContainer.addClass("messageFail");

		}
	});
}

function stringToFunction(fn){
	var fcn ="";
	if(fn){
		var _fn = fn.replace(/['"()]+/g, '');
		fcn = window[_fn];
	}
	return fcn;
}

function nextAction(args){
	if(isFunction(args)) {
		args();
	} else if (Array.isArray(args)) {
		//Array check
	} else if (isObject(args)) {
		//Object check
	}
}

function validateCodePopUpResultTrue(res, fn, nextScreen) {
	if (res.onsenUI) {
		hideDialog('challenge-dialog');
	} else {
		toggleRsaModal(document.getElementsByTagName(NAME_RSMOD_TAG)[0]);
	}
	if (fn) {
		if (isString(fn)) {
			var arg = stringToFunction(fn);
			nextAction(arg);
		} else {
			nextAction(fn);
			btnBack.onclick = fnNew;

		}
	} else {
		if (nextScreen == "terms.html") {
			newRecoveryCode('newRecoveryCodeOOB', this);
		} else {
			document.querySelector(RSANAVIGATORELEMENT).pushPage(nextScreen, {data: {title: ''}});
		}
	}
}

function verifyCodePopUp(nextScreen, fn, element) {
	msgContainer = jQuery("#msgResponse");
	msgContainer.html("");
	jQuery.ajax({
		beforeSend: function(){
			if(element) {
				element.disabled = true;
			}
		},
		url : 'verifyRsaCode',
		cache : false,
		data: {'codeUsr': jQuery('#codeUsr')[0].value}
	}).done(function(response) {
		if (response) {
			console.log(response);
			var res = JSON.parse(response);
			if (res.result && res.result == "BLOCKED") {
				if (res.onsenUI) {
					location.replace('rsaEnroll?block');
				} else {
					redirectUser();
				}
			} else if(res.result && res.result == "true"){
				validateCodePopUpResultTrue.call(this, res, fn, nextScreen);
			} else {
				msgContainer.html(res.message);
				msgContainer.addClass("messageFail");
			}
		} else {
			msgContainer.html("Service not available, try later");
			msgContainer.addClass("messageFail");
		}

		if (element) {
			element.disabled = false;
		}
	});
}

function validateCallResultTrue(fn, nextScreen) {
	if (fn) {
		if (isString(fn)) {
			var arg = stringToFunction(fn);
			nextAction(arg);
		} else {
			nextAction(fn);
			btnBack.onclick = fnNew;
		}
	} else {
		if (nextScreen == "terms.html") {
			newRecoveryCode('newRecoveryCodeOOB', this);
		} else {
			document.querySelector(RSANAVIGATORELEMENT).pushPage(nextScreen, {data: {title: ''}});
		}
	}
}

function verifyCallPopUp(nextScreen, fn, element) {
	msgContainer = jQuery("#msgResponse");
	msgContainer.html("");
	jQuery.ajax({
		beforeSend: function(){
			if(element) {
				element.disabled = true;
			}
		},
		url : 'verifyRsaCode',
		cache : false,
		data: {'call': 'call',
			'codeUsr': jQuery('#codeUsr')[0].textContent}
	}).done(function(response) {
		if (response) {
			var res = JSON.parse(response);
			if(res.result && res.result == "BLOCKED"){
				if(res.onsenUI) {
					location.replace('rsaEnroll?block');
				} else {
					redirectUser();
				}
			} else if (res.result && res.result == "true") {
				if (res.onsenUI) {
					hideDialog('challenge-dialog');
				} else { toggleRsaModal(document.getElementsByTagName(NAME_RSMOD_TAG)[0]); }
				validateCallResultTrue.call(this, fn, nextScreen);
			} else {
				msgContainer.html(res.message);
				msgContainer.addClass("messageFail");
			}
		} else {
			msgContainer.html("Service not available, try later");
			msgContainer.addClass("messageFail");
		}
		if (element) {
			element.disabled = false;
		}
	});
}



function sendToQuestion(){
	document.querySelector(RSANAVIGATORELEMENT).pushPage('rsaEditQuestions.html', {data: {title: ''}});
}

function verifyCodeMod() {
	msgContainer = jQuery("#msgResponse");
	msgContainer.html("");
	if(jQuery('#codeUsr')[0].value) {
		jQuery.ajax({
			url : 'verifyRsaCode',
			cache : false

		}).done(function(response) {
			if (response) {
				var divRsa = document.createElement("rsaMod") ;
				if(document.getElementsByTagName("rsaMod")[0]) {divRsa = document.getElementById("rsaMod");}
				else {divRsa.setAttribute("id","rsaMod");}
				var res = jQuery.parseHTML(response);
				divRsa.innerHTML = res;
				//var res = JSON.parse(response);
			} else {
				msgContainer.html("Service not available, try later");
				msgContainer.addClass("messageFail");
			}
		});
	} else {
		msgContainer.html("Its required");
	}
}


function verifyRecoveryCode(msg) {
	msgContainer = jQuery("#msgResponse");
	msgContainer.html("");
	jQuery.ajax({
		url : 'verifyRecoveryCode',
		cache : false,
		data: {'recoveryCodeUsr': jQuery('#recoveryCodeUsr')[0].value}
	}).done(function(response) {
		if (response) {
			var res = JSON.parse(response);
			if(res.message != ""){
				msgContainer.html(res.message);
				msgContainer.addClass("messageFail");
			}else{
				//This line send information to native device for close session.
				location.replace('rsaEnroll?endSession');
			}
		} else {
			msgContainer.html("Service not available, try later");
			msgContainer.addClass("messageFail");
		}
	});
}

function verifySelectedQuestion (comp) {
	var selectQ = document.getElementById(comp);
	var errQ = document.getElementById('err'+comp);
	errQ.innerText='';
	selectQ.classList.remove("inQsError");
	if(selectQ.value) {
		return true;
	} else {
		errQ.innerText=_msgSelectQuestion;
		selectQ.classList.add("inQsError");
		return false;
	}
}

function verifyAnswerQuestion (ans,cans) {
	var errQ = document.getElementById('err'+ans);
	errQ.innerText='';
	var answer = document.getElementById(ans);
	answer.parentElement.classList.remove("inQsError");
	var confirmation = document.getElementById(cans);
	confirmation.parentElement.classList.remove("inQsError");
	if(answer.value && confirmation.value && answer.value === confirmation.value) {
		return true;
	} else {

		answer.parentElement.classList.add("inQsError");
		confirmation.parentElement.classList.add("inQsError");
		if(answer.value && confirmation.value) {
			errQ.innerText=_msgInputNotMatch;
		} else {
			errQ.innerText=_msgRequired;
		}
		return false;
	}
}

function validateEditQuestionResponse(url, mobile) {
	if (url == 'rsaOOBDeactivate') {
		showDialog('dialogOOBDeactivateFail', 'dialogOOBDeactivateFail.html');
		document.getElementById("btnEditQuestions").disabled = false;
	} else {
		if (mobile) {
			dialogWithIntervalNewLocation('dialogEditQuestionsFail', 'rsaEditQuestions?back', res.timeInterval);

		} else {
			alert(res.messageError);
		}

	}
}

function validateEditQuestions(url, mobile) {
	if(verifySelectedQuestion('sq1') & verifyAnswerQuestion('ans1','cans1') &
		verifySelectedQuestion('sq2') & verifyAnswerQuestion('ans2','cans2') &
		verifySelectedQuestion('sq3') & verifyAnswerQuestion('ans3','cans3')) {

		jQuery.ajax({
			beforeSend: function(){
				document.getElementById("btnEditQuestions").disabled=true;
			},
			url : url,
			cache : false,
			method: 'POST',
			contentType: 'application/json',
			dataType: "text",
			processData: false,
			data : JSON.stringify({
				'sq1' : document.getElementById('sq1').value,
				'sq2' : document.getElementById('sq2').value,
				'sq3' : document.getElementById('sq3').value,
				'ans1' : document.getElementById('ans1').value,
				'cans1' : document.getElementById('cans1').value,
				'ans2' : document.getElementById('ans2').value,
				'cans2' : document.getElementById('cans2').value,
				'ans3' : document.getElementById('ans3').value,
				'cans3' : document.getElementById('cans3').value,
				'typeDevice' : document.querySelector('input[name="typeDevice"]:checked').value
			})
		}).done(function(response) {
			if (response) {
				callBackValidateEditQuestions(response, url, mobile);
			} else{
				validateEditQuestionResponse(url, mobile);
			}
		})
	}
}

function callBackValidateEditQuestions (response, url, mobile) {
	var res = JSON.parse(response);
	if(res.result == 'success'){
		if(valAg) {
			ga('send','event','MiBanco','Mobile OOB', 'kEditRsaQuestionsSuccess');
		}
		if(url == 'rsaOOBDeactivate') {
			dialogWithIntervalNewLocation('dialogOOBDeactivateSuccess','rsaEnroll?questionrsa', res.timeInterval);
		} else {
			if(mobile) {
				dialogWithIntervalNewLocation('dialogEditQuestionsSuccess','rsaEditQuestions?back', res.timeInterval);

			} else {
				alert(res.message);
			}

		}
	}else{

		if(url == 'rsaOOBDeactivate') {
			showDialog('dialogOOBDeactivateFail', 'dialogOOBDeactivateFail.html');
			document.getElementById("btnEditQuestions").disabled=false;
		} else {
			if(mobile) {
				dialogWithIntervalNewLocation('dialogEditQuestionsFail','rsaEditQuestions?back', res.timeInterval);

			} else {
				alert(res.messageError);
			}


		}
	}
}

var rsaModule = (function () {

	// options: an object containing configuration options for the singleton
	// e.g var options = { name: "test", pointX: 5};
	function rsaMod( options ) {

		// set options to the options supplied
		// or an empty object if none are provided
		options = options || {};

		// set some properties for our singleton
		this.title = options.title || "2 Step Verification";

		this.text = options.text || "Type here the verification code we have just sent to your registered number:";

		var  initRsa = function(customText){

			jQuery.ajax({
				url : 'initRsa',
				cache : false,
				data : {
					'customText' : customText
				}
			}).done(function(response) {
				if (response) {
					var divRsa = document.createElement("div") ;
					if(document.body.contains("rsaMod")) {divRsa = document.getElementById("rsaMod");}
					else {divRsa.setAttribute("id","rsaMod");}
					var res = jQuery.parseHTML(response);
					divRsa.innerHTML = res;

				} else {
					document.getElementById("msgResponse");
				}
			});

		}

		var generateNewCode = function() {
			jQuery.ajax({
				url : 'sendNewCode',
				cache : false,
				data : {
					'customText' : customText
				}
			}).done(function(response) {
				if (response) {
					var res = jQuery.parseHTML(response);
					document.getElementById("msgResponse").innerHTML.res;
				} else {
					document.getElementById("msgResponse");
				}
			});
		}

		return {
			initRsa : initRsa,
			generateNewCode : generateNewCode
		};
	}

	// our instance holder
	var instance;

	// an emulation of static variables and methods
	var _static = {

		name: "rsaModule",

		// Method for getting an instance. It returns
		// a singleton instance of a singleton object
		getInstance: function( options ) {
			if( instance === undefined ) {
				instance = new rsaMod( options );
			}

			return instance;

		}
	};

	return _static;

})();


var Platform = function () {
	var platformConst = {};
	Object.defineProperty(platformConst, 'IOS', {
		value: 'ios',
		writable: false
	});
	Object.defineProperty(platformConst, 'ANDROID', {
		value: 'android',
		writable: false
	});
	Object.defineProperty(platformConst, 'OTHER', {
		value: 'other',
		writable: false
	});
	//----------------
	// iOS?
	//----------------
	/**
	 * @method isIOS
	 * @signature isIOS()
	 * @return {Boolean}
	 */
	function isIOS() {
		return (/iPhone|iPad|iPod/i.test(navigator.userAgent));
	}

	//----------------
	// Android versions
	//----------------
	/**
	 * @method isAndroid
	 * @signature isAndroid()
	 * @return {Boolean}
	 */
	function isAndroid() {
		return (/Android/i.test(navigator.userAgent));
	}
	function getMobileOS() {
		if (this.isAndroid()) {
			return platformConst.ANDROID;
		} else if (this.isIOS()) {
			return platformConst.IOS;
		} else {
			return platformConst.OTHER;
		}
	}

	function loadCss(contextPath) {


		var prefix = contextPath+'/css/rsa/';
		var cssId = 'rsaMobileCSS';  // you could encode the css path itself to generate id..
		var platform = this.getMobileOS();
		var cssPlatform = ''
		switch(platform) {
			case platformConst.ANDROID:
				cssPlatform = 'rsaAndroid.css';
				break;
			case platformConst.IOS:
				cssPlatform = 'rsaIOS.css';
				break;
			default:
				cssPlatform = '';
		}
		if (!document.getElementById(cssId) && cssPlatform)
		{
			var head  = document.getElementsByTagName('head')[0];
			var link  = document.createElement('link');
			link.id   = cssId;
			link.rel  = 'stylesheet';
			link.type = 'text/css';
			link.href = prefix + cssPlatform;
			link.media = 'all';
			head.appendChild(link);

		}
	}

	function loadScript(directory, files){
		var head = document.getElementsByTagName("head")[0]
		var done = false
		var extension = '.js'
		//for (var file of files){
		for (var i = 0; i < files.length ; i++) {
			var path = directory + files[i] + extension
			var script = document.createElement("script")
			script.src = path
			script.type = "text/javascript"
			script.onload = script.onreadystatechange = function() {
				if ( !done && (!this.readyState ||
					this.readyState == "loaded" || this.readyState == "complete") ) {
					done = true
					script.onload = script.onreadystatechange = null   // cleans up a little memory:
					head.removeChild(script)  // to avoid douple loading
				}
			};
			head.appendChild(script)
			done = false
		}
	}

	function loadStyle(directory, files){
		var head = document.getElementsByTagName("head")[0]
		var extension = '.css'
		//for (var file of files){
		for (var i = 0; i < files.length ; i++) {
			var path = directory + files[i] + extension
			var link = document.createElement("link")
			link.href = path
			link.type = "text/css"
			link.rel = "stylesheet"
			head.appendChild(link)
		}
	}




	/**
	 * @return functions
	 */
	return {
		isAndroid : isAndroid,
		isIOS : isIOS,
		getMobileOS : getMobileOS,
		loadCss : loadCss
	};
};


function pushPageInterval(id, html, hide, dialog, dataArray) {
	setTimeout(function(){
		document.querySelector(id).pushPage(html, { animation: 'none' , data: dataArray });
		if(hide)
			hideDialog(dialog);
	}, 3000);
}

function replacePageInterval(id, html, hide, dialog, dataArray) {
	setTimeout(function(){
		document.querySelector(id).replacePage(html, { animation: 'none' , data: dataArray });
		if(hide)
			hideDialog(dialog);
	}, 3000);
}

//RSA Enroll
function rsaOOBEnroll(urlPar){
	var challengeType = document.getElementById("challengeType").value;
	var radios = document.getElementsByName('frequency');
	var alternatePhone = document.getElementById("secondaryPhoneHide").value;
	var alternateProvider = document.getElementById("secondaryCarrierHide").value;
	document.getElementById("btnContinueDiv").style.visibility = "hidden";
	var frequency = 'false';
	for (var i = 0, length = radios.length; i < length; i++) {
		if (radios[i].checked) {
			frequency = radios[i].value;
			break;
		}
	}
	jQuery.ajax({
		beforeSend: function(){
			document.getElementById("btnContinueDiv").disabled = true;
			document.getElementById("btnContinueDiv").style.visibility = "hidden";
		},
		url : urlPar,
		cache : false,
		data : {
			'challengeType' : challengeType,
			'frequency' : frequency,
			'alternatePhone' : alternatePhone,
			'alternateProvider' : alternateProvider
		}
	}).done(function(response) {
		if (response) {
			var res = JSON.parse(response);

			if(res.result == 'success'){
				data = { 'oneTimeCode' : res.oneTimeCode};
				pushPageInterval(RSANAVIGATORELEMENT, 'recoveryCode.html',true,'dialogCodeOOB',data);
			}else{
				showDialog('dialogCodeFail', 'dialogCodeFail.html');
				document.getElementById("btnContinueDiv").style.visibility = "visible";
				document.getElementById("btnContinueDiv").disabled = true;

			}
		}else{
			showDialog('dialogCodeFail', 'dialogCodeFail.html');
			document.getElementById("btnContinueDiv").style.visibility = "visible";
			document.getElementById("btnContinueDiv").disabled = true;

		}
	})
}


//RSA Enroll
function sendSmsCode(urlPar, resend){
	var mainNumberInp = document.getElementById("mainNumberInp").value;
	if(mainNumberInp != undefined  && mainNumberInp != "") {
		mainNumberInp = mainNumberInp.replace(/[^0-9.]/g, "");
	}
	var carrier = document.getElementById("carrier").value;
	jQuery.ajax({
		beforeSend: function(){
			if(resend == 'true'){
				document.getElementById("btnValidateCodeDiv").style.visibility = "hidden";
			}else{
				document.getElementById("btnTermsSMS").style.visibility = "hidden";
			}
		},
		url : urlPar,
		cache : false,
		data : {
			'mainNumberInp' : mainNumberInp,
			'carrier' : carrier,
			'resend' : resend
		}
	}).done(function(response) {
		if (response) {
			var res = JSON.parse(response);
			if(resend == 'true'){
				document.getElementById("btnValidateCodeDiv").style.visibility = "visible";
			}else{
				document.getElementById("btnTermsSMS").style.visibility = "visible";
				if(res.result == 'success'){
					document.querySelector(RSANAVIGATORELEMENT).pushPage('rsaPhoneCode.html', { animation: 'none' });
					hideDialog('terms-dialog');
				}else{
					showDialog('dialogCodeFail', 'dialogCodeFail.html');
				}
			}
		}else{
			if(resend == 'true'){

			}else{
				showDialog('dialogCodeFail', 'dialogCodeFail.html');
			}
		}
	})
}

function rsaSMSEnroll(urlPar){
	var codeNumberInp = document.getElementById("codeNumberInp").value;
	jQuery.ajax({
		beforeSend: function(){
			//showDialog('dialogLoadingOOB', 'dialogLoadingOOB.html');
			//document.getElementById("rsaPhoneCode-personalLoader").style.display = 'block';
			//document.getElementById("rsaPhoneCode-content").style.display = 'none';
			document.getElementById("btnValidateCodeDiv").style.visibility = "hidden";
			document.getElementById("errorCodeNumber").style.display = "none";
			document.getElementById("boxCodeNumber").className = "list-item";
		},
		url : urlPar,
		cache : false,
		data : {
			'codeNumberInp' : codeNumberInp
		}
	}).done(function(response) {
		if (response) {
			var res = JSON.parse(response);
			if(res.result == 'success'){
				data = {primaryPhone : res.primaryPhone};
				pushPageInterval(RSANAVIGATORELEMENT, 'rsaSecondaryPhone.html',true,'dialogCode',data);
				showDialog('dialogCode', 'dialogCode.html');
			}else{
				//Show error
				hideDialog('dialogCode');
				jQuery('#rsaPhoneCode-personal-loading').fadeOut('fast',
					function() {
						document.getElementById("rsaPhoneCode-content").style.display = 'block';
						jQuery('#rsaPhoneCode-content').fadeIn('slow');
					}
				);
				document.getElementById("btnValidateCodeDiv").style.visibility = "visible";
				document.getElementById("errorCodeNumber").style.display = "block";
				document.getElementById("boxCodeNumber").className = "list-item errorBorder";
			}
		}else{
			hideDialog('dialogCode');
			jQuery('#rsaPhoneCode-personal-loading').fadeOut('fast',
				function() {
					document.getElementById("rsaPhoneCode-content").style.display = 'block';
					jQuery('#rsaPhoneCode-content').fadeIn('slow');
				}
			);
			document.getElementById("btnValidateCodeDiv").style.visibility = "visible";
			showDialog('dialogCodeFail', 'dialogCodeFail.html');
		}
	})

}

function rsaChallenge(){
	jQuery("#challenge-dialog").load("/actions/initRsa" );
}

function loadEditData(page){

	var data = loadRsaInfo();
	if(data) {
		updatePage(data, page);
	} else {
		rsaGetInfo(page).catch(alert);
	}

}

/*async function rsaGetInfo2(page) {

    const url = 'rsaGetInfo';
      var respuesta = await fetch(url);
      if(respuesta.status == 200) {
        var json = await respuesta.json();
        updatePage(json,page);
      saveRsaInfo(json);
      } else {
          throw new Error(respuesta.status);
      }
}*/

function rsaGetInfo(page){
	jQuery.ajax({
		url : 'rsaGetInfo',
		data: {}
	}).done(function(response) {
		var res = JSON.parse(response);
		if(res.result == 'true'){
			updatePage(res,page);
			saveRsaInfo(res);
		}
	})
}

function updatePage(response,page){
	if(page.id == 'editRsa') {
		document.getElementById("pcPrimaryPhone").textContent = response.primaryPhone;
		if(response.altPhone != "")
			document.getElementById("pcAlternatyPhone").textContent = response.altPhone;
		if(response.challengeMethod === "OOBSMS"){
			document.getElementById("pcSendTypeSMS").style.display = "block";
			document.getElementById("pcSendTypeCall").style.display = "none";
		}else{
			document.getElementById("pcSendTypeSMS").style.display = "none";
			document.getElementById("pcSendTypeCall").style.display = "block";
		}

		if(response.frecuencyOOB === "true"){
			document.getElementById("pcFrecuencyAlways").style.display = "block";
			document.getElementById("pcFrecuencyRequired").style.display = "none";
		}else{
			document.getElementById("pcFrecuencyAlways").style.display = "none";
			document.getElementById("pcFrecuencyRequired").style.display = "block";
		}

		jQuery('#question-loading').fadeOut('fast',
			function() {
				document.getElementById("personal-content").style.display = 'block';
				jQuery('#personal-content').fadeIn('slow');
			}
		);
	} else if (page.id == 'rsaEditOOB') {
		document.getElementById("primaryPhoneLbl2").textContent = response.primaryPhone;
		if(response.altPhone && response.altPhone != "") {
			document.getElementById("secondaryPhoneP").textContent = response.altPhone;
			document.getElementById("secondaryPhoneSelected").style.display = 'block';
			document.getElementById("buttomAddSecondaryPhone").style.display = 'none';
			document.getElementById("secondaryPhoneHide").value = response.altPhoneHide;
		}

		if(response.challengeMethod === "OOBSMS"){
			document.getElementById("radioSms").checked = true;
		} else {
			document.getElementById("radioCall").checked = true;
		}
		selectPreference();
		if(response.frecuencyOOB === "true"){
			document.getElementById("frqAlways").checked = true;
		} else {
			document.getElementById("frqAllow").checked = true;
		}
	}
}

function saveRsaInfo (data) {
	if(data) {
		if (typeof(Storage) !== "undefined") {
			if (sessionStorage.rsaInfo) {
				sessionStorage.rsaInfo = JSON.stringify(data);
			} else {
				sessionStorage.setItem("rsaInfo", JSON.stringify(data));
			}
		}
	}
}

function loadRsaInfo () {
	if (typeof(Storage) !== "undefined") {
		if (sessionStorage.rsaInfo) {
			return JSON.parse(sessionStorage.rsaInfo);
		}
	}
}

function newRecoveryCode(urlPar, element) {
	jQuery.ajax({
		beforeSend: function(){
			element.disabled = true;
		},
		url : urlPar,
		cache : false
	}).done(function(response) {
		if (response) {
			var res = JSON.parse(response);
			element.disabled = false;
			if(res.result == 'success'){
				data = { 'oneTimeCode' : res.recoveryCode, 'action' : 'edit'};
				pushPageInterval(RSANAVIGATORELEMENT, 'recoveryCode.html',true,'dialogRecoveryCode',data);
			}else{
				showDialog('dialognewRecoveryCodeFail', 'dialognewRecoveryCodeFail.html');
				hideDialog('dialogRecoveryCode');
			}
		}else{
			showDialog('dialognewRecoveryCodeFail', 'dialognewRecoveryCodeFail.html');
			hideDialog('dialogRecoveryCode');
		}
	}).fail(function(jqXHR, textStatus) {
		var status = services + textStatus + ' ' + jqXHR.status;
		alert(status);
		element.disabled = false;
		hideDialog('dialogRecoveryCode');
	})
}

function updateRsaProfile(urlPar, element) {
	var challengeType = document.getElementById("challengeType").value;
	var radios = document.getElementsByName('frequency');
	var alternatePhone = document.getElementById("secondaryPhoneHide").value;
	var alternateProvider = document.getElementById("secondaryCarrierHide").value;
	var primaryPhoneUpd = document.getElementById("primaryPhoneUpdHide").value;
	var frequency = 'false';
	for (var i = 0, length = radios.length; i < length; i++) {
		if (radios[i].checked) {
			frequency = radios[i].value;
			break;
		}
	}
	jQuery.ajax({
		beforeSend: function(){
			element.disabled = true;
		},
		data : {
			'challengeType' : challengeType,
			'frequency' : frequency,
			'alternatePhone' : alternatePhone,
			'alternateProvider' : alternateProvider,
			'primaryPhoneUpd' : primaryPhoneUpd
		},
		url : urlPar,
		cache : false
	}).done(function(response) {
		if (response) {
			var res = JSON.parse(response);
			if(res.result == 'success'){
				showDialog('dialogUpdateOOB', 'dialogUpdateOOB.html');
				var timeInterval = res.timeInterval * 1000;
				setTimeout(function(){
					location.replace('rsaEnroll?back');
				}, timeInterval);
			} else if (res.result == 'noChanges') {
				location.replace('rsaEnroll');
			}else{
				showDialog('dialognewRecoveryCodeFail', 'dialognewRecoveryCodeFail.html');
				document.getElementById("rsaProfileMessageErr").innerHTML = res.messageError;
			}
		}else{
			showDialog('dialognewRecoveryCodeFail', 'dialognewRecoveryCodeFail.html');
			document.getElementById("rsaProfileMessageErr").innerHTML = res.messageError;
		}
	}).fail(function(jqXHR, textStatus) {
		var status = services + textStatus + ' ' + jqXHR.status;
		alert(status);
		element.disabled = false;
		hideDialog('dialogRecoveryCode');
	})
}

function updateRsaMobileProfile(urlPar, element) {
	var codeNumberInp = document.getElementById("codeNumberInp").value;
	jQuery.ajax({
		beforeSend: function(){
			//showDialog('dialogLoadingOOB', 'dialogLoadingOOB.html');
			//document.getElementById("rsaPhoneCode-personalLoader").style.display = 'block';
			//document.getElementById("rsaPhoneCode-content").style.display = 'none';
			document.getElementById("btnValidateCodeDiv").style.visibility = "hidden";
			document.getElementById("errorCodeNumber").style.display = "none";
			document.getElementById("boxCodeNumber").className = "list-item";
		},
		url : urlPar,
		cache : false,
		data : {
			'codeNumberInp' : codeNumberInp
		}
	}).done(function(response) {
		if (response) {
			var res = JSON.parse(response);
			document.getElementById("btnValidateCodeDiv").style.visibility = "visible";
			if(res.result == 'success'){
				hideDialog('dialogLoadingOOB');
				if(valAg) {
					ga('send', 'event', 'MiBanco', 'Mobile OOB', 'kAddOobPrimaryPhoneSuccess');
				}
				data = {primaryPhone : res.primaryPhone};
				var x = res.primaryPhone.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
				var elem = document.getElementById('primaryPhoneLbl');
				if(elem) {
					document.querySelector(RSANAVIGATORELEMENT).resetToPage('rsaSecondaryPhone.html',{data : data });
					document.getElementById("primaryPhoneLbl").textContent = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
				}else{
					document.querySelector(RSANAVIGATORELEMENT).popPage({ animation: 'slide', data : data });	  					
	  				dialogWithIntervalPopPreviousPage('dialogMobileUpdateOOB', res.timeInterval);
					document.getElementById("primaryPhoneLbl2").textContent = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
					document.getElementById("primaryPhoneUpdHide").value = 'true';
				}
			}else{
				//Show error
				hideDialog('dialogLoadingOOB');
				jQuery('#rsaPhoneCode-personal-loading').fadeOut('fast',
					function() {
						document.getElementById("rsaPhoneCode-content").style.display = 'block';
						jQuery('#rsaPhoneCode-content').fadeIn('slow');
					}
				);

				document.getElementById("errorCodeNumber").style.display = "block";
				document.getElementById("boxCodeNumber").className = "list-item errorBorder";
			}
		}else{
			hideDialog('dialogLoadingOOB');
			jQuery('#rsaPhoneCode-personal-loading').fadeOut('fast',
				function() {
					document.getElementById("rsaPhoneCode-content").style.display = 'block';
					jQuery('#rsaPhoneCode-content').fadeIn('slow');
				}
			);
			showDialog('dialogCodeFail', 'dialogCodeFail.html');
		}
	})
}

function disabledScreenRecoveryCode(isDisabled){
	document.getElementById("btnGRCode").disabled = isDisabled;
	document.getElementById("btnCancelRC").disabled = isDisabled;
}
function dialogWithIntervalNewLocation(dialogId, locationTo, timeInterval) {

	if(locationTo=='rsaEnroll?oobEnrolled'){
		if(valAg) {
			ga('send', 'event', 'MiBanco', 'Mobile OOB', 'kEnrollOobSuccess');
		}
	}

	timeInterval = timeInterval * 1000;

	disableSelectTextStyle();

	showDialog(dialogId, dialogId + '.html');
	setTimeout(function(){
		hideDialog(dialogId);
		location.replace(locationTo);
	}, timeInterval);
}

function dialogWithIntervalPopPreviousPage(dialogId, timeInterval) {
	if(timeInterval != undefined  && timeInterval != "") {
		timeInterval = timeInterval * 1000;	
	} else {
		timeInterval = 4 * 1000;
		
	}
	showDialog(dialogId, dialogId + '.html');
	setTimeout(function(){ 
		 hideDialog(dialogId);
		 document.querySelector(RSANAVIGATORELEMENT).popPage({ animation: 'slide', data : data });
	  }, timeInterval);
}

function disableSelectTextStyle() {
	var disableTextClassStyle = jQuery('.disable_text_highlighting')[0];
	if (disableTextClassStyle) {
		disableTextClassStyle.style.userSelect = "none";
		disableTextClassStyle.style.webkitUserSelect = "none";
		disableTextClassStyle.style.msUserSelect = "none";
		disableTextClassStyle.style.mozUserSelect = "none";
		disableTextClassStyle.style.oUserSelect = "none";
	}
}

function updateDisplay(newPath, nextScreen, phone, nameFunction) {
	jQuery.ajax({
		beforeSend: function(){
			//logic before send
		},
		data : {
			// data to ajax
			'nextScreen' : nextScreen,
			'phone': phone,
			'function' : nameFunction
		},
		url : newPath,
		cache : false
	}).done(function(response) {
		if (response) {
			var el = document.createElement( 'html' );
			el.innerHTML = response;
			var resHTML = el.getElementsByTagName(NAME_RSMOD_TAG);
			var displayTo = document.getElementsByTagName(NAME_RSMOD_TAG);
			if(resHTML.length > 0 && displayTo.length > 0) {
				if(displayTo.length > 1) {
					displayTo[1].innerHTML = resHTML[0].innerHTML;
				} else {
					displayTo[0].innerHTML = resHTML[0].innerHTML;
				}
			}

		}else{

		}
	}).fail(function(jqXHR, textStatus) {
		var status = services + textStatus + ' ' + jqXHR.status;
		alert(status);
	})
}

var disabledButton = function(evnt){
	$(evnt.this).attr('disabled','disabled');
}

function isFunction (value) {
	return typeof value === 'function';
}

function isObject (value) {
	return value && typeof value === 'object' && value.constructor === Object;
}


function rebuildAlternatePhoneInformation() {
	var alternateNumber = jQuery('#secondaryNumberInp')[0];

	if (alternateNumber && alternateNumber.value !== undefined) {
		var secPhone = alternateNumber.value;
		var secPhoneLast = jQuery('#secondaryNumberInp')[0];
		var lastSecPhone = secPhoneLast.value;

		if (secPhone != null) {
			secPhone = secPhone.replace(/[^0-9.]/g, "");
		}

		if (secPhone != null && secPhone.length == 10) {
			jQuery('#secondaryPhoneSelected')[0].style.display = 'block';
			jQuery('#buttomAddSecondaryPhone')[0].style.display = 'none';
			jQuery('#dialogSecondaryPhone')[0].hide();
			jQuery('#secondaryPhoneP')[0].innerHTML = lastSecPhone;
			jQuery('#secondaryPhoneHide')[0].value = secPhone;
		}
	}
}

function isString (value) {
	return typeof value === 'string' || value instanceof String;
}

function initRsa(element) {
	var displayTo = document.getElementsByTagName(NAME_RSMOD_TAG);
	var loadingRsa = document.getElementsByTagName('rsaMod-load');
	jQuery.ajax({
		url : 'rsaCheckStatus',
		data: {}
	}).done(function(response) {
		var res = JSON.parse(response);
		if( (res.oobEnrolled == "true" && res.isOobAuthenticated == "true")
			|| (res.oobEnrolled == "false" && res.isRsaAuthenticated == "true")){
			btnBack.onclick = fnNew;
			btnBack.onclick();
		}else{
			if(displayTo.length > 0) {
				toggleRsaModal(displayTo[0]);
			} else {
				jQuery.ajax({
					beforeSend: function(){
						//logic before send
						toggleRsaModal(loadingRsa[0]);
					},
					data : {
						// data to ajax
						'nextScreen' : '',
						'phone' : ''
					},
					url : "initRsa",
					cache : false
				}).done(function(response) {
					toggleRsaModal(loadingRsa[0]);
					if (response) {
						callBackInitRsa(response);
					} else{
						//Error message
					}

				}).fail(function(jqXHR, textStatus) {
					var status = services + textStatus + ' ' + jqXHR.status;
					alert(status);
				})
			}
		}
	})

}


function callBackInitRsa(response) {

	var el = document.createElement( 'html' );
	el.innerHTML = response;
	var resHTML = el.getElementsByTagName(NAME_RSMOD_TAG);
	var displayTo = document.getElementsByTagName(NAME_RSMOD_TAG);
	var typeMod = resHTML[0].getAttribute('type');
	var target;
	if(displayTo.length == 0) {
		displayTo = document.createElement("rsaMod");
		displayTo.classList.add("modal-web");
		displayTo.innerHTML = resHTML[0].innerHTML;
		displayTo.setAttribute('type', typeMod);
		document.body.appendChild(displayTo);
		toggleRsaModal(displayTo);
		target = displayTo;
	} else {
		if(resHTML.length > 0 && displayTo.length > 0) {
			displayTo[0].innerHTML = resHTML[0].innerHTML;
			displayTo[0].setAttribute('type', typeMod);
			toggleRsaModal(displayTo[0]);
			target = displayTo[0];
		}
	}
	var btnCancel = document.getElementById("btnRsaCancel");
	btnCancel.onclick = function () {toggleRsaModal(document.getElementsByTagName(NAME_RSMOD_TAG)[0]);}
	var btnRsaValidation = document.getElementById("btnVRsa");
	var btnVName = setValidationBtn(typeMod);
	btnRsaValidation.onclick = Function(btnVName);

	var observer = new MutationObserver(function (mutations) {
		mutations.forEach(function (mutation) {
			var btnCancel = document.getElementById("btnRsaCancel");
			if(btnCancel) btnCancel.onclick = function () {toggleRsaModal(document.getElementsByTagName(NAME_RSMOD_TAG)[0]);}
			var btnRsaValidation = document.getElementById("btnVRsa");
			var btnVName = setValidationBtn(typeMod);
			if (btnRsaValidation) btnRsaValidation.onclick = Function(btnVName);
		});
	});

	// configuration of the observer:
	var config = {
		attributes: true,
		childList: true,
		characterData: true
	};

	// pass in the target node, as well as the observer options
	observer.observe(target, config);
}

function redirectUser() {
	window.location=REDIRECT_LOGIN;
}

function toggleRsaModal(modal) {
	modal.classList.toggle("modal-show");
}

function evalBtnAttribute(btnFnBack) {
	if (btnFnBack) {
		var btnFnName = btnFnBack.getAttribute(ATTRIBUTE_CLICK);
		if (btnFnName) {
			fnNew = new Function(btnFnName);
		}
	}
}

//Function firts try to Rsa Module
function setupPayeeRsa() {
	var btnFnBack = document.getElementById('editEmailAction');
	btnBack = btnFnBack;
	evalBtnAttribute(btnFnBack);
	btnFnBack.onclick = function() { initRsa(this)};
	initLoading();
}

//Function to handled Rsa interceptor and data from server
function setupRsa () {
	if (typeof(Storage) !== "undefined") {
		if (sessionStorage.rsaSt) {
			//function to setup element with challenge
		} else {
			//
			sessionStorage.setItem("rsaSt", {fn : "fnNew", name : "else"});
		}
	}
}

function setValidationBtn(type) {
	if(type === 'question')
		return "validateQuestionRsa(this, fnNew, '')";
	else if (type === 'sms')
		return "verifyCodePopUp('', fnNew, this)";
	else (type === 'call')
	return "verifyCallPopUp('', fnNew, this)";
}
function initLoading() {
	var displayTo = document.createElement("rsaMod-load");
	var loading = document.createElement("div");
	var mP = document.createElement("div");
	mP.classList.add("modal");
	var mPB = document.createElement("div");
	mPB.classList.add("modal-body");
	mP.appendChild(mPB);
	mPB.appendChild(loading);
	var iconLoading = document.createElement("img");
	iconLoading.src = "/cibp-web/img/rsa/load-c.svg";
	loading.appendChild(iconLoading);
	loading.classList.add("initLoadingIcon");
	displayTo.classList.add("modal-web");
	displayTo.appendChild(mP);
	document.body.appendChild(displayTo);
}
