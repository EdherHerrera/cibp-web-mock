/**
 * Util to check platform Android/IOS/Web when is used onsenui and need a custom css profile
 * 
 */

var PlatformDevice = function (nameContext) { 
	var platformConst = {};
	
	Object.defineProperty(platformConst, 'IOS', {
		  value: 'ios',
		  writable: false
		});
	Object.defineProperty(platformConst, 'ANDROID', {
		  value: 'android',
		  writable: false
		});
	Object.defineProperty(platformConst, 'OTHER', {
		  value: 'other',
		  writable: false
		});
	Object.defineProperty(platformConst, 'NAMECONTEXT', {
		  value: nameContext,
		  writable: false
		});
	Object.defineProperty(platformConst, 'CCSIOS', {
		  value: platformConst.NAMECONTEXT+'IOS',
		  writable: false
		});
	Object.defineProperty(platformConst, 'CSSANDROID', {
		  value: platformConst.NAMECONTEXT+'Android',
		  writable: false
		});
    //----------------
    // iOS?
    //----------------
    /**
     * @method isIOS
     * @signature isIOS()
     * @return {Boolean}
     */
    function isIOS() {
        return (/iPhone|iPad|iPod/i.test(navigator.userAgent));
    }

    //----------------
    // Android versions
    //----------------
    /**
     * @method isAndroid
     * @signature isAndroid()
     * @return {Boolean}
     */
    function isAndroid() {
        return (/Android/i.test(navigator.userAgent));
    }
    function getMobileOS() {
      if (this.isAndroid()) {
        return platformConst.ANDROID;
      } else if (this.isIOS()) {
        return platformConst.IOS;
      } else {
        return platformConst.OTHER;
      }
    }
    
    function loadCss(contextPath) {
    	
    	var cssPath = '/css/';
    	var cssSufix = '.css';
    	var prefix = contextPath+cssPath+platformConst.NAMECONTEXT+'/';
    	var cssId = platformConst.NAMECONTEXT + 'CSS';  // you could encode the css path itself to generate id..
    	var platform = this.getMobileOS();
    	var cssPlatform = ''
    	switch(platform) {
        case platformConst.ANDROID:
            cssPlatform = platformConst.CSSANDROID + cssSufix;
            break;
        case platformConst.IOS:
        	cssPlatform = platformConst.CCSIOS + cssSufix;
            break;
        default:
        	cssPlatform = '';
        	break;
    }
    	if (!document.getElementById(cssId) && cssPlatform)
    	{
    	    var head  = document.getElementsByTagName('head')[0];
    	    var link  = document.createElement('link');
    	    link.id   = cssId;
    	    link.rel  = 'stylesheet';
    	    link.type = 'text/css';
    	    link.href = prefix + cssPlatform;
    	    link.media = 'all';
    	    head.appendChild(link);
    	    
    	}
    }
    
    /**
     * @return functions
     */
    return {
    	isAndroid : isAndroid,
    	isIOS : isIOS,
    	getMobileOS : getMobileOS,
    	loadCss : loadCss
    };
  };