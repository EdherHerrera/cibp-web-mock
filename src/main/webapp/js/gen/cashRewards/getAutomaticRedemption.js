
function getAutomaticRedemptionInfo(frontEndId, methodAction, onCompleteFunction) {
	var pars = {'frontEndId': frontEndId, 'methodAction': methodAction};

	new Ajax.Request('tsysLoyaltyRewardsAutomaticRedemption', 
	{
		method: 'get',
		contentType : 'application/json; charset=utf-8',
		dataType:'json',
		parameters:pars,
		onComplete: onCompleteFunction
	});
}

function getCashBackBalance(frontEndId, methodAction) {

	var ajaxCallBack = function(response) {	
		var loyaltyRewardsDiv = $("vpcbk-"+frontEndId);
		
		var json = '';
		
		try {
			json = JSON.parse(response.responseText);
		}
		catch(err) {
			loyaltyRewardsDiv.innerText = noBalancePlaceholder;
		}
		
		var result = json.data[0];
		var errors = json.errors[0];

		if(errors){
			loyaltyRewardsDiv.innerText = noBalancePlaceholder;
		} else if (result.availableRewardsBalance == null){
			loyaltyRewardsDiv.innerText = noBalancePlaceholder;
		} else{
			loyaltyRewardsDiv.innerText = result.availableRewardsBalance;
		}	 
	}
	getAutomaticRedemptionInfo(frontEndId, methodAction, ajaxCallBack);	
}