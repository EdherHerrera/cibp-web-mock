function evaluateCCAOwner(billerNumber) {
	var noWhitespaceValue = billerNumber.replace(/\s+/g, '');
	var noWhitespaceCount = noWhitespaceValue.length;
	if (noWhitespaceCount % 16 == 0) {
		var pars = 'billerNumber=' + billerNumber;
		var myAjax = new Ajax.Request('evaluateCCAOwner', {
			method : 'post',
			parameters : pars,
			onComplete : completeCCAValidation
		});
	} else {
		$("eBillq").hide();
		$("ebill").hide();
		$("activateEbillFalse").checked = true;
	}
}

function completeCCAValidation(ccaNotOfUser) {
	if (ccaNotOfUser.responseText == "T") {
		$("eBillq").show();

	} else {
		$("eBillq").hide();
		$("ebill").hide();
	}

}

function getBankName(routingNumber) {
	var complete = function(t) {
		showResponseBankName(t);
	}
	if (routingNumber.length == 9) {
		var pars = 'routingNumber=' + routingNumber;
		var myAjax = new Ajax.Request('getBankNameFromRoutingNumber', {
			method : 'post',
			parameters : pars,
			onComplete : complete
		});
	} else
		document.getElementById('bankname').innerHTML = '';
}
function showResponseBankName(t) {
	document.getElementById('bankname').innerHTML = t.responseText;
	document.getElementById("bank_Name").value = t.responseText;
}

function createJSON(str) {
	var jsonBody = {};
	var StrArray = str.split('|');
	for (var i = 0; i < StrArray.length; i++) {
		var subItem = StrArray[i].split('~');
		jsonBody[subItem[0]] = subItem[1];
	}
	return jsonBody;
}

function showAlertMessagePayroll(response) {
	var messageBox = document.getElementById('success_msg');
	messageBox.innerHTML = "";
	var row = messageBox.insertRow(0);
	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var message = "";
	cell1.innerHTML = "";
	if (response.status == "failed") {
		message = response.errorMessage;
		var message = "<tr><td><span style='padding-right:50px;padding-left:50px;background-color:#FFFF99;font-size:11px;'>"
				+ message
				+ "<//span><script type='text/javascript'>removeMsg();<//script><//td><//tr>";
		cell2.innerHTML = message;
		if (response.errorCode != "") {
			var row2 = messageBox.insertRow(1);
			var cell1_1 = row2.insertCell(0);
			var cell2_1 = row2.insertCell(1);
			cell1_1.innerHTML = "";
			var message2 = "<tr><td> <div id='Popup1' class='transparent'><p style='text-align:right;' width='200px'><a href='javascript:Hide()'><img src='../img/bntCloseErrTelepago.jpg' width='20' height='20' border='0'/></a>&nbsp;&nbsp;&nbsp;&nbsp;</p><div style='text-align:left;'>"
					+ response.errorString
					+ "<br/><br/>"
					+ response.errorCode
					+ "<br/><br/></div></div></td></tr>"
			cell2_1.innerHTML = message2;
		}
	} else if (response.status == "successful") {
		message = response.successMessage;
		var message = "<tr><td><span style='padding-right:50px;padding-left:50px;background-color:#FFFF99;font-size:11px;'>"
				+ message
				+ "<//span><script type='text/javascript'>removeMsg();<//script><//td><//tr>";
		cell2.innerHTML = message;
		cleanPayrollField();
	}
	window.scrollTo(0, 0);
}

function showAlertMessageVendor(response) {
	var messageBox = document.getElementById('success_msg');
	messageBox.innerHTML = "";
	var row = messageBox.insertRow(0);
	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var message = "";
	cell1.innerHTML = "";
	if (response.status == "failed") {
		message = response.errorMessage;
		var message = "<tr><td><span style='padding-right:50px;padding-left:50px;background-color:#FFFF99;font-size:11px;'>"
				+ message
				+ "<//span><script type='text/javascript'>removeMsg();<//script><//td><//tr>";
		cell2.innerHTML = message;
		if (response.errorCode != "") {
			var row2 = messageBox.insertRow(1);
			var cell1_1 = row2.insertCell(0);
			var cell2_1 = row2.insertCell(1);
			cell1_1.innerHTML = "";
			var message2 = "<tr><td> <div id='Popup1' class='transparent'><p style='text-align:right;' width='200px'><a href='javascript:Hide()'><img src='../img/bntCloseErrTelepago.jpg' width='20' height='20' border='0'/></a>&nbsp;&nbsp;&nbsp;&nbsp;</p><div style='text-align:left;'>"
					+ response.errorString
					+ "<br/><br/>"
					+ response.errorCode
					+ "<br/><br/></div></div></td></tr>"
			cell2_1.innerHTML = message2;
		}
	} else if (response.status == "successful") {
		message = response.successMessage;
		var message = "<tr><td><span style='padding-right:50px;padding-left:50px;background-color:#FFFF99;font-size:11px;'>"
				+ message
				+ "<//span><script type='text/javascript'>removeMsg();<//script><//td><//tr>";
		cell2.innerHTML = message;
		cleanVendorField();
	}
	window.scrollTo(0, 0);
}

function createParams() {
	var inputs = document.getElementsByTagName("input");
	var selects = document.getElementsByTagName("select");
	var textareas = document.getElementsByTagName("textarea");
	var params = "";
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].type == "text" || inputs[i].type == "hidden") {
			params += inputs[i].name + "=" + inputs[i].value + "&";
		} else if (inputs[i].type == "radio") {
			if (inputs[i].checked) {
				params += inputs[i].name + "=" + inputs[i].value + "&";
			}
		}
	}
	for(var i = 0; i < selects.length; i++){
		params += selects[i].name +  "=" + selects[i].value + "&";
	}
	for(var i = 0; i < textareas.length; i++){
		params += textareas[i].name +  "=" + textareas[i].value + "&";
	}
	return (params.substring(0, params.length - 1));
}

function cleanPayrollField() {
	document.getElementById("employeeName").value = "";
	document.getElementById("employeeId").value = "";
	document.getElementById("employeeIdConf").value = "";
	document.getElementById("billingNumber").value = "";
	document.getElementById("billingNumberConf").value = "";
	document.getElementById("nickname").value = "";
	document.getElementById("check").checked = false;
	document.getElementById("saving").checked = false;
	var errors = document.getElementsByClassName("error");
	for (i = 0; i < errors.length; i++) {
		errors[i].style.display = "none";
	}
}

function cleanVendorField() {
	
	document.getElementById('vendorName').value = "";
	document.getElementById('vendorId').value = "";
	document.getElementById('vendorIdConf').value = "";
	document.getElementById('email').value = "";
	document.getElementById('emailConf').value = "";
	document.getElementById("address").value = "";
	document.getElementById("city").value = "";
	document.getElementById("zipCode").value = "";
	document.getElementById("state").selectedIndex = "0";
	document.getElementById("comments").value = "";	
	document.getElementById("billingNumber").value;
	document.getElementById("billingNumberConf").value;;
	document.getElementById("billingNumber").value = "";
	document.getElementById("billingNumberConf").value = "";
	document.getElementById("nickname").value = "";
	document.getElementById("check").checked = false;
	document.getElementById("saving").checked = false;
	var errors = document.getElementsByClassName("error");
	for (i = 0; i < errors.length; i++) {
		errors[i].style.display = "none";
	}
}

function validatePayrollForm() {
	var errors = document.getElementsByClassName("error");
	for (i = 0; i < errors.length; i++) {
		errors[i].style.display = "none";
	}

	var isValid = true;
	var isRequiredCompare = true;
	var employeeName = document.getElementById("employeeName").value;
	var employeeId = document.getElementById("employeeId").value;
	var employeeIdConf = document.getElementById("employeeIdConf").value;
	var employeeRoutingNumber = document
			.getElementById("employeeRoutingNumber").value;
	var billingNumber = document.getElementById("billingNumber").value;
	var billingNumberConf = document.getElementById("billingNumberConf").value;
	var check = document.getElementById("check").value;
	var saving = document.getElementById("saving").value;
	var nickname = document.getElementById("nickname").value;
	
	var accountType;
	if (!check && !saving) {
	} else {
		var elements = document.getElementsByName("accounttype");
		for (var i = 0, l = elements.length; i < l; i++) {
			if (elements[i].checked) {
				accountType = elements[i].value;
			}
		}
	}
	
	if (employeeName == "") {
		isValid = false;
		document.getElementById("errorName").style.display = "block";
	} else if (employeeName != "") {
		var regex = new RegExp("^[A-Za-z0-9]+((\\s|-)?[A-Za-z0-9]+)*$");
		if (!regex.test(employeeName)) {
			document.getElementById("errorName").style.display = "block";
		}
	}

	if (employeeId == "") {
		isValid = false;
		isRequiredCompare = false;
		document.getElementById("errorEmployeeId").style.display = "block";
	} else if (employeeId != "") {
		var regex = new RegExp("^[A-Za-z0-9]+((\\s|-)?[A-Za-z0-9]+)*$");
		if (!regex.test(employeeId)) {
			isValid = false;
			isRequiredCompare = false;
			document.getElementById("errorEmployeeId").style.display = "block";
		}
	}

	if (employeeIdConf == "") {
		isValid = false;
		isRequiredCompare = false;
		document.getElementById("errorEmployeeIdConf").style.display = "block";
	} else if (employeeIdConf != "") {
		var regex = new RegExp("^[A-Za-z0-9]+((\\s|-)?[A-Za-z0-9]+)*$");
		if (!regex.test(employeeIdConf)) {
			isValid = false;
			isRequiredCompare = false;
			document.getElementById("errorEmployeeIdConf").style.display = "block";
		}
	}

	if (isRequiredCompare && employeeId != employeeIdConf) {
		isValid = false;
		document.getElementById("errorEmployeeIdConfNoMatch").style.display = "block";
	}

	isRequiredCompare = true;
	if (employeeRoutingNumber == "") {
		isValid = false;
		document.getElementById("errorEmployeeRoutingNumber").style.display = "block";
	} else if (employeeRoutingNumber != "") {
		var regex = new RegExp("^[A-Za-z0-9-]+$");
		if (!regex.test(employeeRoutingNumber)) {
			isValid = false;
			document.getElementById("errorEmployeeRoutingNumber").style.display = "block";
		}
	}

	if (billingNumber == "") {
		isValid = false;
		isRequiredCompare = false;
		document.getElementById("errorBillingNumber").style.display = "block";
	} else if (billingNumber != "") {
		var regex = new RegExp("^[A-Za-z0-9-]+$");
		if (!regex.test(billingNumber)) {
			isValid = false;
			document.getElementById("errorBillingNumber").style.display = "block";
		}
	}

	if (billingNumberConf == "") {
		isValid = false;
		isRequiredCompare = false;
		document.getElementById("errorBillingNumberConf").style.display = "block";
	}

	if (isRequiredCompare && billingNumber != billingNumberConf) {
		isValid = false;
		document.getElementById("errorBillingNumberConfNoMatch").style.display = "block";
	}

	if (!accountType && (accountType == null || accountType == undefined)) {
		isValid = false;
		document.getElementById("errorAccounttype").style.display = "block";
	}

	if (nickname == "") {
		isValid = false;
		document.getElementById("errorNickname").style.display = "block";
	} else if (nickname != "") {
		var regex = new RegExp("^[A-Za-z0-9]+((\\s|-)?[A-Za-z0-9]+)*$");
		if (!regex.test(nickname)) {
			isValid = false;
			document.getElementById("errorNickname").style.display = "block";
		}
	}
	return isValid;
}

function addEpayrollPayee(btnAddTextProcessing, btnAddTextNormal) {
	var btnAddPayroll = document.getElementById('btnAddPayrollPayee');
	if (validatePayrollForm()) {
		btnAddPayroll.value = btnAddTextProcessing;
		btnAddPayroll.disabled = true;
		var params = createParams();
		var ajaxAux = new Ajax.Request('addEpayrollPayee', {
			method : 'post',
			asynchronous : true,
			postBody : params,
			onComplete : function(a_oRequest) {
				var jsonResponse = createJSON(a_oRequest.responseText);
				showAlertMessagePayroll(jsonResponse);
				btnAddPayroll.disabled = false;
				btnAddPayroll.value = btnAddTextNormal;
			}
		});
	}
}

function validateVendorForm(){
	
	var emailRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	
	var errors = document.getElementsByClassName("error");
	for (i = 0; i < errors.length; i++) {
		errors[i].style.display = "none";
	}
	var isValid = true;
	var isRequiredCompare = true;
	
	var vendorName = document.getElementById('vendorName').value;
	var vendorId = document.getElementById('vendorId').value;
	var vendorIdConf = document.getElementById('vendorIdConf').value;
	var emailVendor = document.getElementById('email').value;
	var emailVendorConf = document.getElementById('emailConf').value;
	var routingNumber = document.getElementById('routingNumber').value
	
	var billingNumber = document.getElementById("billingNumber").value;
	var billingNumberConf = document.getElementById("billingNumberConf").value;
	var check = document.getElementById("check").value;
	var saving = document.getElementById("saving").value;
	var nickname = document.getElementById("nickname").value;
	
	var zipcode = document.getElementById("zipCode").value;
	var accountType;
	
	if (!check && !saving) {
	} else {
		var elements = document.getElementsByName("accounttype");
		for (var i = 0, l = elements.length; i < l; i++) {
			if (elements[i].checked) {
				accountType = elements[i].value;
			}
		}
	}

	if (vendorName == "") {
		isValid = false;
		document.getElementById("errorName").style.display = "block";
	} else if (vendorName != "") {
		var regex = new RegExp("^[A-Za-z0-9]+((\\s|-)?[A-Za-z0-9]+)*$");
		if (!regex.test(vendorName)) {
			document.getElementById("errorName").style.display = "block";
		}
	}
	
	if (vendorId == "") {
		isValid = false;
		isRequiredCompare = false;
		document.getElementById("errorVendorId").style.display = "block";
	} else if (vendorId != "") {
		var regex = new RegExp("^[A-Za-z0-9]+((\\s|-)?[A-Za-z0-9]+)*$");
		if (!regex.test(vendorId)) {
			isValid = false;
			isRequiredCompare = false;
			document.getElementById("errorVendorId").style.display = "block";
		}
	}
	
	if (vendorIdConf == "") {
		isValid = false;
		isRequiredCompare = false;
		document.getElementById("errorVendorIdConf").style.display = "block";
	} else if (vendorIdConf != "") {
		var regex = new RegExp("^[A-Za-z0-9]+((\\s|-)?[A-Za-z0-9]+)*$");
		if (!regex.test(vendorIdConf)) {
			isValid = false;
			isRequiredCompare = false;
			document.getElementById("errorVendorIdConf").style.display = "block";
		}
	}
	
	if (isRequiredCompare && vendorId != vendorIdConf) {
		isValid = false;
		document.getElementById("errorVendorIdConfNoMatch").style.display = "block";
	}

	isRequiredCompare = true;
	
	if (emailVendor == "") {
		isValid = false;
		isRequiredCompare = false;
		document.getElementById("errorEmailVendorId").style.display = "block";
	} else if (emailVendor != "") {
		if (!emailRegex.test(emailVendor)) {
			isValid = false;
			isRequiredCompare = false;
			document.getElementById("errorValidEmailVendorId").style.display = "block";
		}
	}
	
	if (emailVendorConf == "") {
		isValid = false;
		isRequiredCompare = false;
		document.getElementById("errorEmailVendorConf").style.display = "block";
	} else if (emailVendorConf != "") {
		if (!emailRegex.test(emailVendorConf)) {
			isValid = false;
			isRequiredCompare = false;
			document.getElementById("errorValidEmailVendorConfId").style.display = "block";
		}
	}
	
	if (isRequiredCompare && emailVendor != emailVendorConf) {
		isValid = false;
		document.getElementById("errorEmailVendorConfNoMatch").style.display = "block";
	}
	
	if(zipcode != ""){
		var regex = new RegExp("(?!([0123456789])\\1{4})\\d{5}");
		if(!regex.test(zipcode)){
			isValid = false;
			document.getElementById("errorZipId").style.display = "block";
		}
	}
	
	isRequiredCompare = true;
	
	if (routingNumber == "") {
		isValid = false;
		document.getElementById("errorRoutingNumber").style.display = "block";
	} else if (routingNumber != "") {
		var regex = new RegExp("^[A-Za-z0-9-]+$");
		if (!regex.test(routingNumber)) {
			isValid = false;
			document.getElementById("errorRoutingNumber").style.display = "block";
		}
	}
	if (billingNumber == "") {
		isValid = false;
		isRequiredCompare = false;
		document.getElementById("errorBillingNumber").style.display = "block";
	} else if (billingNumber != "") {
		var regex = new RegExp("^[A-Za-z0-9-]+$");
		if (!regex.test(billingNumber)) {
			isValid = false;
			document.getElementById("errorBillingNumber").style.display = "block";
		}
	}

	if (billingNumberConf == "") {
		isValid = false;
		isRequiredCompare = false;
		document.getElementById("errorBillingNumberConf").style.display = "block";
	}

	if (isRequiredCompare && billingNumber != billingNumberConf) {
		isValid = false;
		document.getElementById("errorBillingNumberConfNoMatch").style.display = "block";
	}

	if (!accountType && (accountType == null || accountType == undefined)) {
		isValid = false;
		document.getElementById("errorAccounttype").style.display = "block";
	}

	if (nickname == "") {
		isValid = false;
		document.getElementById("errorNickname").style.display = "block";
	} else if (nickname != "") {
		var regex = new RegExp("^[A-Za-z0-9]+((\\s|-)?[A-Za-z0-9]+)*$");
		if (!regex.test(nickname)) {
			isValid = false;
			document.getElementById("errorNickname").style.display = "block";
		}
	}
	return isValid;
}

function addVendorPayee(btnAddTextProcessing, btnAddTextNormal) {
	var btnAddPayroll = document.getElementById('btnAddPayrollPayee');
	if (validateVendorForm()) {
		btnAddPayroll.value = btnAddTextProcessing;
		btnAddPayroll.disabled = true;
		var params = createParams();
		console.log(params);
		var ajaxAux = new Ajax.Request('addVendorPayee', {
			method : 'post',
			asynchronous : true,
			postBody : params,
			onComplete : function(a_oRequest) {
				var jsonResponse = createJSON(a_oRequest.responseText);
				showAlertMessageVendor(jsonResponse);
				btnAddPayroll.disabled = false;
				btnAddPayroll.value = btnAddTextNormal;
			}
		});
	}
}
