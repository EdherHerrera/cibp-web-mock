#set($layout="layout/json/json-layout.vm")
{ 
  "responder_name": "portal",
  "rsa_cookie": "$!rsa_cookie",
  "responder_message": "account_information",
	"page_title": "Portal",
	"error": "",
	"goback": "",
	"flags": {
			"deletedUser": #if($deletedUser) true  #else false #end,
			"fullyEnrolledCustomer": #if($fullyEnrolledCustomer) true  #else false #end,
			"accounts": #if($accounts) true  #else false #end,
			"guardianRepPayeeAccts": #if($guardianRepPayeeAccts) true  #else false #end,
			"secins": #if($secins) true  #else false #end,
			"programs": #if($programs) true  #else false #end
	},
	"content": {
		"athmSso": "$!athmSso",
		"pushNotifications": "$!pushNotifications",
		"limitsPerSegment": "$!limitsPerSegment",
		"ebills" : "$!ebills",
		"outreach" : "$!outreach",
		"interruptionPage" : "$!interruptionPage",
		"showAlertsSplash" : #if($outreach) false #else "$!showAlertsSplash" #end,
		"isComercialCustomer" : "$!isComercialCustomer",
		"MBCA1559":"$!premiaFlag",
		"showMarketplace":"$!showMarketplace",
		"customerEmail":"$!customerEmail",
		"MBSD3806":"$!MBSD3806",
		#if($hasVIAccount)
			"hasVIAccount" : "$!hasVIAccount",
		#end
		#if($flagMBCPremiumBanking)
			"isPremiumBanking" : "$!isPremiumBanking",
		#end
		"isWealth" : "$!isWealth",
		#if($retplanDowntimeMessage)
			"rpDowntimeMessage" : "$!retplanDowntimeMessage",
		#end
		#if($isWealth)
        "showPrivateBankerButtonApp" : "$!showPrivateBankerButtonApp",
		"flagMBFM169" : "$!flagMBFM169",
		"flagMBSE2513" : "$!flagMBSE2513",
        #end
		#if($flagMBCA201)
		"isTransactional" : $!isTransactional,
		#end
		#if($showMenuRSAQuestions)
			"isOOBEnrolled" : $isOOBEnrolled,
		#end
	    #if($flagMBFIS581)
		"flagMBFIS581" : $!flagMBFIS581,
		#end
		"ownership" : "$!ownershipType",
		"customerInfo": {
			"customerName": "$!customername",
			"is_today_birthday":	#if($birthday) true #else false #end
		},
		"labels": {
			"deletedUserString": "#springMessageText("" "We're sad to see you go! It was great having you as a customer. Your online username and password has been deleted.")",	
			"portal.welcome": "#springMessageText("portal.welcome" "Hi")",
			"portal.label.birthday": "#springMessageText("portal.label.birthday" "Happy Birthday!")",
			"portal.popularaccount.label": "#springMessageText("portal.popularaccount.label" "Popular Accounts")",
			"portal.account.name.label": "#springMessageText("portal.account.name.label" "Account Name")",
			"portal.account.number.label": "#springMessageText("portal.account.number.label" "Account Number")",
			"portal.account.balance.label": "#springMessageText("portal.account.balance.label" "Balance")",
			"portal.account.secutity.inversion.label": "#springMessageText("portal.account.secutity.inversion.label" "Securities and Investments")",
			"detail.sec.ins.information": "#springMessageText("detail.sec.ins.information" "Insurance and Investment Products are: not deposits - not FDIC Insured. Not bank guaranteed - may lose value. Insurance products also not insured by other government agencies.")",
			"delinquency.promise.text": "#springMessageText("delinquency.promise.text" "This account has the following payment promise")",
			"delinquency.promise.amount.text": "#springMessageText("delinquency.promise.amount.text" "")",
			"delinquency.date.text": "#springMessageText("delinquency.date.text" "Date:")",
			"delinquency.pastdue.text": "#springMessageText("delinquency.pastdue.text" "Amount and days past due:")",
			"delinquency.pastdue1.text": "#springMessageText("delinquency.pastdue1.text" "This account is past due. Please make your payment.")",
			"delinquency.pastdue2.text": "#springMessageText("delinquency.pastdue2.text" "This account is past due. Please make your payment.")",
			"delinquency.pastdue.days.text": "#springMessageText("delinquency.pastdue.days.text" "days")",
			"delinquency.pastduecall.text": "#springMessageText("delinquency.pastduecall.text" "Please call 787-522-1512 for assistance with your payment:")",
			"delinquency.paynow.link": "#springMessageText("delinquency.paynow.link" "PAY NOW")",
			"delinquency.skip.text": "#springMessageText("delinquency.skip.text" "We need to validate important information about your account.<br/>Please call 787-721-2777.")",
			"delinquency.overlimit.text": "#springMessageText("delinquency.overlimit.text" "Your credit card is overlimit:")",
			"delinquency.overlimitcall.text": "#springMessageText("delinquency.overlimitcall.text" "Please call us at.:")",
			"delinquency.overlimit.amount": "#springMessageText("delinquency.overlimit.amount" "Overlimit amount:")",
			"delinquency.chargeoff.text": "#springMessageText("delinquency.chargeoff.text" "")"
		},
#if($accounts)
		"accounts": [
	#set($firstElement="true")
	#foreach($act in $accounts)
		#if($firstElement == "false") , #end
			{
				"frontEndId": "$!act.frontEndId",
				"nickname": $jsonTool.escapeString($!act.nickname),
				"apiAccountKey" : "$!accountTool.apiAccountKey($!act.accountNumber)",
				"accountLast4Num": "$!accountTool.mask($!act.accountNumber)",
				"accountNumberSuffix": "$!act.accountNumberSuffix",
				"accountSection": "$!act.accountSection",
				"subtype": "$!act.subtype",
				"productId": "$!act.accountProductId",
				"portalBalance": "#if($act.subtype!="PRM")$!numberTool.format('currency', $!act.portalBalance)#else $!numberTool.format('integer', $!act.portalBalance)#end",
				"balanceColorRed": #if($act.subtype=="CCA" || $act.subtype=="MLA" || $act.subtype=="LEA" || $act.subtype=="RVA" || $act.subtype=="ILA" || ($act.subtype=="IDA" && $act.accountSection && $act.accountSection == "R"))true #else false #end,
				"features": {
					"showStatement": #if($act.features.showStatement) true #else false #end
				},
				"newAccount": #if($newAccount) true #else false #end,
				"showFullAccount": #if($showFullAccount) true #else false #end,
				
				#if($onOff) 
					"onOffCount": "$!act.athOnOffCount",
					"onOffElegible": #if($act.elegibleForOnOff) true #else false #end,
					"onOffFetched": #if($act.onOffFetched) true #else false #end,
				#end
				
				#if($act.showFullAccount) 
					"accountNumber": "$!act.accountNumber",
				#end
				
				#if($cashRewardsFlag)
					##Only primary cash rewards customers are allowed to see the accumulated balance and redeem.
					##isPrimaryMap is a hashMap that maps the frontEndId to a boolean indicating if they are the primary owner 
					"isCashRewardsEligible" : 
					#if($!act.accountProductId == 'VPCBK' && $!isPrimaryMap.get($act.frontEndId)) 
						true 
					#else 
						false 
					#end,
					
					"tsysLoyaltyRewardsInfo": 
					#if($!act.tsysLoyaltyRewardsInfo) 
						{
							"canRedeemRewards" : $!act.tsysLoyaltyRewardsInfo.canRedeemRewards,
							"rewardsAccountStatus" : "$!act.tsysLoyaltyRewardsInfo.rewardsAccountStatus",
							"availableRewardsBalance" : 
								#if($!act.tsysLoyaltyRewardsInfo.availableRewardsBalance) 
									"$!act.tsysLoyaltyRewardsInfo.availableRewardsBalance", 
								#else 
									null, 
								#end
							"minimumRewardsBalance" : $!act.tsysLoyaltyRewardsInfo.minimumRewardsBalance,
							"availableBalanceDouble" : $!act.tsysLoyaltyRewardsInfo.availableBalanceDouble
						},
					#else 
						null,
					#end 
				#end
				
				##Delinquency area
				#if($act.delinquency &&  ($act.accountNumberSuffix && $act.accountNumberSuffix == "RES" || $act.subtype =="LEA" || $act.subtype =="ILA" || $act.subtype =="RVA" || $act.subtype =="CCA" || ($act.features.sectionsCount == "1" && $act.subtype == "IDA")))
					"delinquency": {
						"delinquencyType": "$!act.delinquencyType",
						
						#if($act.delinquencyType == "PROMISE")
							"label": "delinquency.promise.text",
							"promiseAmountRaw": "$!act.delinquencyInfo.promiseAmount",
							"promiseAmount": "$!numberTool.format('currency', $!act.delinquencyInfo.promiseAmount)",
							"promiseDateRaw": "$!act.delinquencyInfo.promiseDate",
							"promiseDate": "$!dateTool.format('MM-dd-yyyy', $!act.delinquencyInfo.promiseDate)",
							"dateFormat": "MM-dd-yyyy",							
						#elseif($act.delinquencyType == "DPD_60_PLUS" || $act.delinquencyType == "DPD_30_59" || $act.delinquencyType == "DPD_1_29")
							#if($act.delinquencyType == "DPD_30_59")
								"label": "delinquency.pastdue2.text",
							#elseif($act.delinquencyType == "DPD_1_29")
								"label": "delinquency.pastdue1.text",
							#end
							"amountRaw": "$!act.delinquencyInfo.amount",
							"amount": "$!numberTool.format('currency', $!act.delinquencyInfo.amount)",
							"daysPastDue": "$!act.delinquencyInfo.daysPastDue",
							#if($billpayEnable)
								"quickpayment": {
									"href": "quickpaymentportal?referenceAccountId=$act.frontEndId",
									"label": "delinquency.paynow.link"
								},
							#end
							"phoneNumber": "787-522-1512",
						#elseif($act.delinquencyType == "SKIP")
							"label": "delinquency.skip.text",
							"phoneNumber": "787-721-2777",
						#elseif($act.delinquencyType == "OVERLIMIT")
							"label": "delinquency.overlimit.text",
							"amountRaw": "$!act.delinquencyInfo.amount",
							"amount": "$!numberTool.format('currency', $!act.delinquencyInfo.amount)",
						#elseif($act.delinquencyType == "CHARGE_OFF")
							#springMessageText("delinquency.chargeoff.text" ""),
						#end
						"billpayEnable": #if($billpayEnable) true #else false #end
					},
				#end 
				
				#if($act.features.showStatement && !($act.subtype=="PRM"))
					"href": "transaction?account=$act.frontEndId"
				#else
					"href": "detail?account=$act.frontEndId"
				#end
			}
		#set($firstElement="false")
	#end## end foreach accounts

	#if($guardianRepPayeeAccts)
	#set($firstElement="false")
	#foreach($act in $guardianRepPayeeAccts)
		#if($firstElement == "false") , #end
			{ 
				"frontEndId": "$!act.frontEndId",
				"nickname": $jsonTool.escapeString($!act.nickname),
				"apiAccountKey" : "$!accountTool.apiAccountKey($!act.accountNumber)",
				"accountLast4Num": "$!accountTool.mask($!act.accountNumber)",
				"accountNumberSuffix": "$!act.accountNumberSuffix",
				"accountSection": "$!act.accountSection",
				"subtype": "$!act.subtype",
				"productId": "$!act.accountProductId",
				"portalBalance": "#if($act.subtype!="PRM")$!numberTool.format('currency', $!act.portalBalance)#else $!numberTool.format('integer', $!act.portalBalance)#end",
				"balanceColorRed": #if($act.subtype=="CCA" || $act.subtype=="MLA" || $act.subtype=="LEA" || $act.subtype=="RVA" || $act.subtype=="ILA" || ($act.subtype=="IDA" && $act.accountSection && $act.accountSection == "R"))true #else false #end,
				"features": {
					"showStatement": #if($act.features.showStatement) true #else false #end
				},
				"newAccount": #if($newAccount) true #else false #end,
				"showFullAccount": #if($showFullAccount) true #else false #end,
				
				#if($act.showFullAccount) 
					"accountNumber": "$!act.accountNumber",
				#end
				
				##Delinquency area
				#if($act.delinquency &&  ($act.accountNumberSuffix == "RES" || $act.subtype =="LEA" || $act.subtype =="ILA" || $act.subtype =="RVA" || $act.subtype =="CCA" || ($act.features.sectionsCount == "1" && $act.subtype == "IDA")))
					"delinquency": {
						"delinquencyType": "$!act.delinquencyType",
						
						#if($act.delinquencyType == "PROMISE")
							"label": "delinquency.promise.text",
							"promiseAmountRaw": "$!act.delinquencyInfo.promiseAmount",
							"promiseAmount": "$!numberTool.format('currency', $!act.delinquencyInfo.promiseAmount)",
							"promiseDateRaw": "$!act.delinquencyInfo.promiseDate",
							"promiseDate": "$!dateTool.format('MM-dd-yyyy', $!act.delinquencyInfo.promiseDate)",
							"dateFormat": "MM-dd-yyyy",							
						#elseif($act.delinquencyType == "DPD_60_PLUS" || $act.delinquencyType == "DPD_30_59" || $act.delinquencyType == "DPD_1_29")
							#if($act.delinquencyType == "DPD_30_59")
								"label": "delinquency.pastdue2.text",
							#elseif($act.delinquencyType == "DPD_1_29")
								"label": "delinquency.pastdue1.text",
							#end
							"amountRaw": "$!act.delinquencyInfo.amount",
							"amount": "$!numberTool.format('currency', $!act.delinquencyInfo.amount)",
							"daysPastDue": "$!act.delinquencyInfo.daysPastDue",
							#if($billpayEnable)
								"quickpayment": {
									"href": "quickpaymentportal?referenceAccountId=$act.frontEndId",
									"label": "delinquency.paynow.link"
								},
							#end
							"phoneNumber": "787-522-1512",
						#elseif($act.delinquencyType == "SKIP")
							"label": "delinquency.skip.text",
							"phoneNumber": "787-721-2777",
						#elseif($act.delinquencyType == "OVERLIMIT")
							"label": "delinquency.overlimit.text",
							"amountRaw": "$!act.delinquencyInfo.amount",
							"amount": "$!numberTool.format('currency', $!act.delinquencyInfo.amount)",
						#elseif($act.delinquencyType == "CHARGE_OFF")
							#springMessageText("delinquency.chargeoff.text" ""),
						#end
						"billpayEnable": #if($billpayEnable) true #else false #end
					},
				#end

				#if($act.features.showStatement && !($act.subtype =="MLA" || $act.subtype=="PRM"))
					"href": "transaction?account=$act.frontEndId"
				#else
					"href": "detail?account=$act.frontEndId"
				#end
			}
		#set($firstElement="false")
	#end## end foreach guardianRepPayeeAccts
	#end## end guardianRepPayeeAccts



		],
#end## end accounts

#if($rdcAccounts)
		"rdcAccounts": [
	#set($firstElement="true")
	#foreach($act in $rdcAccounts)
		#if($firstElement == "false") , #end
		{
				"frontEndId": "$!act.frontEndId",
				"nickname": $jsonTool.escapeString($!act.nickname),
				"apiAccountKey" : "$!accountTool.apiAccountKey($!act.accountNumber)",
				"accountLast4Num": "$!accountTool.mask($!act.accountNumber)",
				"accountNumberSuffix": "$!act.accountNumberSuffix",
				"accountSection": "$!act.accountSection",
				"subtype": "$!act.subtype",
				"depositLimit": "$!act.customerAccountPreferences.depositLimit"
		}
		#set($firstElement="false")
	#end
	],
#end

#**
 MBCA - 1586 and 1585
 For Premia Sections always sent one account for display on devices
 but for bussines logic is neccesary display the logo premia so
 it will forced to sent PRM and productid 1 to no change logic
 for accounts on mobiles it will be force to use PRM and productid 1
 *#
#if($programs)
"programs": [
	#set($firstElement="true")
	#foreach($act in $programs)
		#if($firstElement == "false") , #end
		#if($premiaFlag == true)
		{
		"href": "tsysRoyaltRedirection?cardNumber=$act.frontEndId",
		"nickname": $jsonTool.escapeString($!premiaProgramText),
		"accountLast4Num": "",
		"subtype": "$!premiaMobileSubtype",
		"productId": "$!premiaMobileProdId",
		"frontEndId": "$!act.frontEndId"
		}
		#else
		{
		"href": "detail?account=$act.frontEndId",
		"nickname": $jsonTool.escapeString($!act.nickname),
		"accountLast4Num": "$!accountTool.mask($!act.accountNumber)",
		"subtype": "$!act.subtype",
		"productId": "$!act.accountProductId",
		"portalBalance": "$!numberTool.format('integer', $!act.portalBalance)",
		"frontEndId": "$!act.frontEndId"
		}
		#end

		#set($firstElement="false")
	#end ##end foreach programs
],
#end ##end programs

#if($secins)
		"secins": [
	#set($firstElement="true")
	#foreach($act in $secins)
		#if($firstElement == "false") , #end
		{
			"nickname": $jsonTool.escapeString($!act.nickname),
			"accountLast4Num": "$!accountTool.mask($!act.accountNumber)",
			"subtype": "$!act.subtype",
			"productId": "$!act.accountProductId",
			"portalBalance": "$!numberTool.format('currency', $!act.NetworthBalance)",
			"frontEndId": "$!act.frontEndId"
		}
		#set($firstElement="false")
	#end ##end foreach secins		
		],
#end## end secins
		"secinsLabel": "#springMessageText("portal.account.secutity.inversion.label" "Securities and Investments")",
		"secinsInformation": "#springMessageText("detail.sec.ins.information" "Insurance and Investment Products are: not deposits - not FDIC Insured. Not bank guaranteed - may lose value. Insurance products also not insured by other government agencies.")"
	}
}
