package evertec.cibp.web.signon;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.web.form.interruptions.InterruptionForm;

@RunWith(PowerMockRunner.class)
public class SignonSequenceUtilsUT {
	
	@Test
	public void whengetAskView_givenOobInterruptionPageValue_thenReturnOOBInterruptionForm () {
		Class classResponse = SignonSequenceUtils.getAskClass(ASK_FOR.LOGIN_INTERRUPTION_PAGE);
		
		assertEquals(InterruptionForm.class, classResponse);
	}

}
