package evertec.cibp.web.services.modules;

import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CookiePreferenceStatusService;
import evertec.cibp.web.utils.CookieUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CookieUtils.class})
public class CookiePreferenceServiceModuleUT extends AbstractModelAndViewTests {

    @Mock
    private CustomerServices customerServices;

    @Mock
    private CookiePreferenceStatusService cookiePreferenceStatusService;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    private final List<CookiePreferenceStatus> cookiePreferenceStatusList = new ArrayList<>();

    @InjectMocks
    @Spy
    private final CookiePreferenceServiceModule controller = new CookiePreferenceServiceModule();

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(CookieUtils.class);

        request = PowerMockito.mock(HttpServletRequest.class);
        response = PowerMockito.mock(HttpServletResponse.class);
        controller.setCustomerService(customerServices);

        CookiePreferenceStatus cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("ENABLED");
        cookiePreferenceStatus.setCategory("ANALYTICS");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);

        cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("ENABLED");
        cookiePreferenceStatus.setCategory("FUNCTIONAL");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);

        cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("ENABLED");
        cookiePreferenceStatus.setCategory("ADVERTISING");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);
    }

    /**
     * when CreateProfileCookiePreference
     * Then ReturnListPreferences
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     */
    @Test
    public void whenCreateProfileCookiePreference_ThenReturnListPreferences() {

        when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
        when(cookiePreferenceStatusService.saveCookiePreferenceStatus(any())).thenReturn(Boolean.TRUE);
        when(cookiePreferenceStatusService.getCookiePreferenceStatusByProfileId(any())).thenReturn(cookiePreferenceStatusList);

        List<CookiePreferenceStatus> result = controller.createProfileCookiePreference("sdf564asd6f54as6df54asd6f");
        assertNotNull(result);
    }

    /**
     * when DeleteSelectedCookies
     * Given AnalyticsCategory
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     */
    @Test
    public void whenDeleteSelectedCookies_GivenAnalyticsCategory() throws Exception {

        PowerMockito.doNothing().when(CookieUtils.class, "removeCookie", any(), any());
        boolean result = controller.deleteSelectedCookies("ANALYTICS", response);

        assertTrue(result);
    }

    /**
     * when DeleteSelectedCookies
     * Given FunctionalCategory
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     */
    @Test
    public void whenDeleteSelectedCookies_GivenFunctionalCategory() throws Exception {

        PowerMockito.doNothing().when(CookieUtils.class, "removeCookie", any(), any());
        boolean result = controller.deleteSelectedCookies("FUNCTIONAL", response);

        assertTrue(result);
    }

    /**
     * when DeleteSelectedCookies
     * Given AdvertisingCategory
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     */
    @Test
    public void whenDeleteSelectedCookies_GivenAdvertisingCategory() throws Exception {

        cookiePreferenceStatusList.get(0).setStatus("ENABLED");
        PowerMockito.doNothing().when(CookieUtils.class, "removeCookie", any(), any());
        boolean result = controller.deleteSelectedCookies("ADVERTISING", response);

        assertTrue(result);
    }

    /**
     * when DeleteAllCookiePreference
     * Given CookieStatusDisabled
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     */
    @Test
    public void whenDeleteAllCookiePreference_GivenCookieStatusDisabled() throws Exception {

        cookiePreferenceStatusList.get(0).setStatus("DISABLED");
        PowerMockito.doNothing().when(CookieUtils.class, "removeCookie", any(), any());
        when(controller.deleteSelectedCookies("ADVERTISING", response)).thenReturn(Boolean.TRUE);

        controller.deleteAllCookiePreference(cookiePreferenceStatusList, response);
    }
}