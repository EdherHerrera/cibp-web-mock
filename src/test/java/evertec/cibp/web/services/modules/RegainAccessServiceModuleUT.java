package evertec.cibp.web.services.modules;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.enums.RegainEmailStatus;
import evertec.cibp.core.models.RegainAccessEmail;
import evertec.cibp.core.models.RegainAccessToken;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;

import static evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType.*;
import static evertec.cibp.core.utils.StaticMessageSource.MBFC224;
import static evertec.cibp.core.utils.StaticMessageSource.isFlagEnabled;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, WebUtils.class, CustomerFacade.class, RSAUtils.class, Utils.class, WebUtils.class, PasswordHashUtil.class, ApiCallerFactory.class})
public class RegainAccessServiceModuleUT {

    @InjectMocks
    private RegainAccessServiceModule regainAccessServiceModule;

    @Mock
    private CustomerServices customerServices;

    private MockHttpServletRequest request;

    @Mock
    private ApiCaller apiCaller;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ApiCallerFactory.class);
        when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(Utils.class);
        request = new MockHttpServletRequest();
        request.addHeader("user-agent", "user-agent value");
    }

    @Test
    public void whenIsEmailBlocked_GivenUnblockedEmail_ThenFalse() {
        String someEmail = "someEmail@hostname.com";

        RegainAccessEmail accessEmail = new RegainAccessEmail();
        accessEmail.setEmail(someEmail);
        accessEmail.setStatus(RegainEmailStatus.UNBLOCKED);

        when(customerServices.getRegainAccessEmail(someEmail)).thenReturn(accessEmail);
        regainAccessServiceModule.isEmailBlocked(someEmail);

        assertFalse(regainAccessServiceModule.isEmailBlocked(someEmail));
    }

    @Test
    public void whenLogRegainAccessEvent_GivenCustomerInteractionEventType_ThenLogCustomerInteractionEventType() {
        CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
        RegainAccessToken regainAccessToken = new RegainAccessToken("someEmail@hostname.com", "token", null, null);

        when(isFlagEnabled(MBFC224)).thenReturn(true);
        when(Utils.generateRaEventTraceInformation(anyString(), anyString(), any(UserSession.class)))
                .thenReturn("Trace Info");
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class)))
                .thenReturn(customerInteractionEvent);

        regainAccessServiceModule
                .logRegainAccessEvent(request, regainAccessToken, null, RA_EMAIL_TOKEN_REQUESTED);
        verify(customerServices)
                .customerEmailTokenSentSuccess(customerInteractionEvent, regainAccessToken.getEmail());

        regainAccessServiceModule
                .logRegainAccessEvent(request, regainAccessToken, null, RA_EMAIL_TOKEN_EXPIRED);
        verify(customerServices)
                .customerTokenExpiredInteraction(customerInteractionEvent, regainAccessToken.getEmail());

        regainAccessServiceModule
                .logRegainAccessEvent(request, regainAccessToken, null, REGAIN_ACCESS_EMAIL_BLOCK);
        verify(customerServices)
                .customersRegainAccessEmailBlockedInteraction(customerInteractionEvent, regainAccessToken.getEmail());

        regainAccessServiceModule
                .logRegainAccessEvent(request, regainAccessToken, null, REGAIN_ACCESS_PERSONAL_SUCCESS);
        verify(customerServices)
                .customerMatchInformationEvent(customerInteractionEvent, true, null);

        regainAccessServiceModule
                .logRegainAccessEvent(request, regainAccessToken, null, REGAIN_ACCESS_PERSONAL_FAIL);
        verify(customerServices)
                .customerMatchInformationEvent(customerInteractionEvent, false, regainAccessToken.getEmail());

        regainAccessServiceModule
                .logRegainAccessEvent(request, regainAccessToken, null, REGAIN_ACCESS_ACCT_SUCCESS);
        verify(customerServices)
                .customerMatchAccountEvent(customerInteractionEvent, true);

        regainAccessServiceModule
                .logRegainAccessEvent(request, regainAccessToken, null, REGAIN_ACCESS_ACCT_FAIL);
        verify(customerServices)
                .customerMatchAccountEvent(customerInteractionEvent, false);
    }

}
