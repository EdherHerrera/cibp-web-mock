package evertec.cibp.web.services.modules;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Utils.class })
public class OOBInterruptionModuleUT {

	@Mock
	private CustomerServices customerServices;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private MockHttpServletRequest request;

	@Mock
	private UserSession userSession;

	@InjectMocks
	OOBInterruptionModule oobInterruptionModule;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(Utils.class);
		request = new MockHttpServletRequest();
		request.addHeader("user-agent", "user-agent value");
		
	}

	@Test
	public void whenlogOOBInterruptionEvents_GivenSuccess_ThenVerifyLogOOBInterruptionPage() throws Exception {

		String eventType = "oob_intpg_skip";
		CustomerInteractionEvent interactionEvent = new CustomerInteractionEvent();

		when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(interactionEvent);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileId()).thenReturn("12345678");
		when(customerProfile.getUsername()).thenReturn("prueba1234");
		when(Utils.generateOOBInterruptionEventTraceInformation(any(), any())).thenReturn("test1234");

		Whitebox.invokeMethod(oobInterruptionModule, "logOOBInterruptionEvents", request, userSession, eventType);
		verify(customerServices).logOOBInterruptionPage(interactionEvent,
				CustomerInteractionEventType.OOB_INTERRUPTION_PAGE_SKIP);
	}
	
	@Test
	public void whenlogOOBInterruptionEvents_GivenTraceNull_ThenVerifyLogOOBInterruptionPage() throws Exception {

		String eventType = "oob_intpg_skip";
		CustomerInteractionEvent interactionEvent = new CustomerInteractionEvent();
		
		when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(interactionEvent);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileId()).thenReturn("12345678");
		when(customerProfile.getUsername()).thenReturn("prueba1234");
		when(Utils.generateOOBInterruptionEventTraceInformation(any(), any())).thenReturn(null);

		Whitebox.invokeMethod(oobInterruptionModule, "logOOBInterruptionEvents", request, userSession, eventType);
		verify(customerServices).logOOBInterruptionPage(interactionEvent,
				CustomerInteractionEventType.OOB_INTERRUPTION_PAGE_SKIP);
	}	

}
