package evertec.cibp.web.services.modules;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.vs.beans.payments.RecurringPayment;
import evertec.cibp.vs.models.results.PaymentListActionResult;
import evertec.cibp.vs.services.TransactionVaultServices;
import evertec.cibp.web.session.UserSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * PaymentsServiceModule
 * @author s679338
 */
@RunWith(PowerMockRunner.class)
@SuppressWarnings("unchecked")
@PrepareForTest({ApiCallerFactory.class})
public class PaymentsServiceModuleUT {
	
	@Mock
	private CustomerProfile mCustomerProfile;
	@Mock
	private CustomerServices mCustomerService;
	@Mock 
	private TransactionVaultServices mTransactionVaultServices;
	@Mock
	private CustomerEntitlement mCustomerEntitlement;
	@Mock
	private PaymentListActionResult mPaymentListActionResult;
	@Mock
	private RecurringPayment mRecurringPayment;

	@Mock
	private ApiCaller apiCaller;
	
	@InjectMocks
	private PaymentsServiceModule controller;

	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

		when(mCustomerService.getVaultService()).thenReturn(mTransactionVaultServices);
		when(mCustomerProfile.getEntitlementByType(any())).thenReturn(mCustomerEntitlement);
	}

	/**
	 * when Validate IfHas Processed Payment By Payees
	 * give ListPaymentsEmpty
	 * then All Payees Return False In hasPreviousRealTimePayment Flag
	 * @throws Exception
	 */
	@Test
	public void whenValidateIfHasProcessedPaymentByPayees_giveListPaymentsEmpty_thenAllPayeesReturnFalseInFlag() throws Exception {
		TVFrontEndPayee tvFrontEndPayee = new TVFrontEndPayee();
		UserSession userSession = new UserSession();
		userSession.setCustomerProfile(mCustomerProfile);
		userSession.setCustomerService(mCustomerService);
		List<TVFrontEndPayee> payees = new ArrayList() {{
			add(tvFrontEndPayee);
		}};
		when(userSession.getCustomerProfile().getEntitlementByType(any())).thenReturn(mCustomerEntitlement);
		when(mTransactionVaultServices.getPaymentHistory(any())).thenReturn(mPaymentListActionResult);
		when(mPaymentListActionResult.getPayments()).thenReturn(new ArrayList());
		when(userSession.getCustomerService().getVaultService().getPaymentHistory(any())).thenReturn(mPaymentListActionResult);
		Whitebox.invokeMethod(controller, "validateIfHasProcessedPaymentByPayees", userSession, payees);
		
		assertTrue(!payees.get(0).hasPreviousRealTimePayment());
	}
	
	/**
	 * when Validate IfHas Processed Payment By Payees
	 * give List Status Ok Payments
	 * then All Payees Return True In hasPreviousRealTimePayment Flag
	 * @throws Exception
	 */
	@Test
	public void whenValidateIfHasProcessedPaymentByPayees_giveListStatusOkPayments_thenAllPayeesReturnTrueInFlag() throws Exception {
		UserSession userSession = new UserSession();
		userSession.setCustomerProfile(mCustomerProfile);
		userSession.setCustomerService(mCustomerService);
		TVFrontEndPayee tvFrontEndPayee = new TVFrontEndPayee();
		List<TVFrontEndPayee> payees = new ArrayList() {{
			add(tvFrontEndPayee);
		}};
		tvFrontEndPayee.setCustomerPayeeCodes(new ArrayList() {{
			add(1);
		}});
		tvFrontEndPayee.setFrontEndId("001");
		when(mTransactionVaultServices.getPaymentHistory(any())).thenReturn(mPaymentListActionResult);
		when(mPaymentListActionResult.getPayments()).thenReturn(new ArrayList() {{
			add(mRecurringPayment);
		}});
		when(mRecurringPayment.getStatusCode()).thenReturn("OK");
		when(mRecurringPayment.getPayeeId()).thenReturn("001");
		
		Whitebox.invokeMethod(controller, "validateIfHasProcessedPaymentByPayees", userSession, payees);
		
		assertTrue(payees.get(0).hasPreviousRealTimePayment());
	}
	
	/**
	 * when Validate IfHas Processed Payment By Payees
	 * give List Status IA Payments
	 * then All Payees Return True In hasPreviousRealTimePayment Flag
	 * @throws Exception
	 */
	@Test
	public void whenValidateIfHasProcessedPaymentByPayees_giveListStatusIAPayments_thenAllPayeesReturnTrueInFlag() throws Exception {
		UserSession userSession = new UserSession();
		userSession.setCustomerProfile(mCustomerProfile);
		userSession.setCustomerService(mCustomerService);
		TVFrontEndPayee tvFrontEndPayee = new TVFrontEndPayee();
		List<TVFrontEndPayee> payees = new ArrayList() {{
			add(tvFrontEndPayee);
		}};
		tvFrontEndPayee.setCustomerPayeeCodes(new ArrayList() {{
			add(1);
		}});
		tvFrontEndPayee.setFrontEndId("001");
		when(mTransactionVaultServices.getPaymentHistory(any())).thenReturn(mPaymentListActionResult);
		when(mPaymentListActionResult.getPayments()).thenReturn(new ArrayList() {{
			add(mRecurringPayment);
		}});
		when(mRecurringPayment.getStatusCode()).thenReturn("IA");
		when(mRecurringPayment.getPayeeId()).thenReturn("001");
		
		Whitebox.invokeMethod(controller, "validateIfHasProcessedPaymentByPayees", userSession, payees);
		
		assertTrue(payees.get(0).hasPreviousRealTimePayment());
	}
	
	/**
	 * when Validate IfHas Processed Payment By Payees
	 * give List Other Status Payments
	 * then All Payees Return True In hasPreviousRealTimePayment Flag
	 * @throws Exception
	 */
	@Test
	public void whenValidateIfHasProcessedPaymentByPayees_giveListOtherStatusPayments_thenAllPayeesReturnFalseInFlag() throws Exception {
		UserSession userSession = new UserSession();
		userSession.setCustomerProfile(mCustomerProfile);
		userSession.setCustomerService(mCustomerService);
		TVFrontEndPayee tvFrontEndPayee = new TVFrontEndPayee();
		List<TVFrontEndPayee> payees = new ArrayList() {{
			add(tvFrontEndPayee);
		}};
		tvFrontEndPayee.setCustomerPayeeCodes(new ArrayList() {{
			add(1);
		}});
		tvFrontEndPayee.setFrontEndId("001");
		when(mTransactionVaultServices.getPaymentHistory(any())).thenReturn(mPaymentListActionResult);
		when(mPaymentListActionResult.getPayments()).thenReturn(new ArrayList() {{
			add(mRecurringPayment);
		}});
		when(mRecurringPayment.getStatusCode()).thenReturn("");
		when(mRecurringPayment.getPayeeId()).thenReturn("001");
		
		Whitebox.invokeMethod(controller, "validateIfHasProcessedPaymentByPayees", userSession, payees);
		
		assertTrue(!payees.get(0).hasPreviousRealTimePayment());
	}

}
