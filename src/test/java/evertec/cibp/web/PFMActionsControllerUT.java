/**
 * 
 */
package evertec.cibp.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.pfm.PFMAccountWrapper;
import evertec.cibp.core.models.pfm.PFMCategoryWrapper;
import evertec.cibp.core.models.pfm.PFMSortOrderWrapper;
import evertec.cibp.core.models.pfm.PFMTransactionRecordWrapper;
import evertec.cibp.core.models.pfm.PFMTransactionWrapper;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * @author ET56883 - Michael Ortiz
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({PFMActionsController.class, CustomerServices.class, WebUtils.class, Utils.class, ServletRequestUtils.class})
public class PFMActionsControllerUT extends AbstractModelAndViewTests  {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@InjectMocks
	private PFMActionsController controller;
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private TVFrontendAccount tvFrontendAccount;
	
	@Mock
	private PFMTransactionRecordWrapper pfmTransactionRecordWrapper;
	
	@Mock
	private HashMap<String, String> checkImageMap;
	
	@Mock
	private PFMAccountWrapper pfmAccountWrapper;



	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(ServletRequestUtils.class);

		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
		when(tvFrontendAccount.getAccountNumber()).thenReturn("000000000");

		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCheckImageMap()).thenReturn(checkImageMap);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void whenPfmTransactions_GivenCheckImageTransaction_ThenAddCheckImageMapToSession() {
		
		try {
			
			when(ServletRequestUtils.getStringParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn("true", "03-2020", "03-2020");
			when(ServletRequestUtils.getIntParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn(1);

			
			when(customerService.getPfmTransactions(ArgumentMatchers.any(), 
					ArgumentMatchers.any(List.class), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.anyInt(), 
					any(PFMSortOrderWrapper.class))).
			thenReturn(pfmTransactionRecordWrapper);
			
			PFMTransactionWrapper pfmTransactionWrapper = mock(PFMTransactionWrapper.class);
			
			List<PFMTransactionWrapper> pfmTransactionWrapperList = new ArrayList<>();
			
			pfmTransactionWrapperList.add(pfmTransactionWrapper);
	
			when(pfmTransactionRecordWrapper.getPfmTransactions()).thenReturn(pfmTransactionWrapperList);
			
			controller.pfmTransactions(request, response);
			
			// Validate that the map gets inserted into session.

			verify(userSession, times(1)).getCheckImageMap();
			
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void whenPfmIncomeTransactions_GivenCheckImageTransaction_ThenAddCheckImageMapToSession() {
		
		try {
			
			when(ServletRequestUtils.getStringParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn("true", "03-2020", "03-2020");
			when(ServletRequestUtils.getIntParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn(1);

			
			when(customerService.getPfmTransactions(ArgumentMatchers.any(), 
					ArgumentMatchers.any(List.class), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.any(PFMSortOrderWrapper.class),
					ArgumentMatchers.any(PFMCategoryWrapper.class)))
			.thenReturn(pfmTransactionRecordWrapper);
			
			PFMTransactionWrapper pfmTransactionWrapper = mock(PFMTransactionWrapper.class);
			
			List<PFMTransactionWrapper> pfmTransactionWrapperList = new ArrayList<>();
			
			pfmTransactionWrapperList.add(pfmTransactionWrapper);
	
			when(pfmTransactionRecordWrapper.getPfmTransactions()).thenReturn(pfmTransactionWrapperList);
			
			controller.pfmIncomeTransactions(request, response);
			
			// Validate that the map gets inserted into session.
			verify(userSession, times(1)).getCheckImageMap();
			
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void whenPfmSubcategoryTransactions_GivenCheckImageTransaction_ThenAddCheckImageMapToSession() {
		
		try {
			
			when(ServletRequestUtils.getStringParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn("true", "03-2020", "03-2020");
			when(ServletRequestUtils.getIntParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn(1);

			
			when(customerService.getPfmTransactionsSubcategory(ArgumentMatchers.any(), 
					ArgumentMatchers.any(List.class), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.any(PFMSortOrderWrapper.class))).
			thenReturn(pfmTransactionRecordWrapper);
			
			PFMTransactionWrapper pfmTransactionWrapper = mock(PFMTransactionWrapper.class);
			
			List<PFMTransactionWrapper> pfmTransactionWrapperList = new ArrayList<>();
			
			pfmTransactionWrapperList.add(pfmTransactionWrapper);
	
			when(pfmTransactionRecordWrapper.getPfmTransactions()).thenReturn(pfmTransactionWrapperList);
			
			controller.pfmSubcategoryTransactions(request, response);
			
			// Validate that the map gets inserted into session.
			verify(userSession, times(1)).getCheckImageMap();
			
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void whenPfmResetCategoryTransactions_GivenCheckImageTransaction_ThenAddCheckImageMapToSession() {
		
		try {
			
			when(userSession.getPfmTransactionRecord()).thenReturn(pfmTransactionRecordWrapper);
			
			controller.pfmResetCategoryTransactions(request, response);
			
			// Validate that the map gets inserted into session.
			verify(userSession, times(1)).getCheckImageMap();
			
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void whenPfmReloadCategoryTransactions_GivenCheckImageTransaction_ThenAddCheckImageMapToSession() {
		
		try {
			
			when(userSession.getPfmTransactionRecord()).thenReturn(pfmTransactionRecordWrapper);
			
			when(userSession.getPfmTransactionRecord().getCurrentPage()).thenReturn(0);
			when(userSession.getPfmTransactionRecord().getPagingRecords()).thenReturn(0);

			
			when(ServletRequestUtils.getStringParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn("true", "03-2020", "03-2020");
			when(ServletRequestUtils.getIntParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn(1);

			
			when(customerService.getPfmTransactions(ArgumentMatchers.any(), 
					ArgumentMatchers.any(List.class), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.any(PFMSortOrderWrapper.class))).
			thenReturn(pfmTransactionRecordWrapper);
			
			PFMTransactionWrapper pfmTransactionWrapper = mock(PFMTransactionWrapper.class);
			
			List<PFMTransactionWrapper> pfmTransactionWrapperList = new ArrayList<>();
			
			pfmTransactionWrapperList.add(pfmTransactionWrapper);
	
			when(pfmTransactionRecordWrapper.getPfmTransactions()).thenReturn(pfmTransactionWrapperList);
			
			controller.pfmReloadCategoryTransactions(request, response);
			
			// Validate that the map gets inserted into session.
			verify(userSession, times(1)).getCheckImageMap();
			
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void whenPfmReloadSubcategoryTransactions_GivenCheckImageTransaction_ThenAddCheckImageMapToSession() {
		
		try {
			
			
			when(userSession.getPfmTransactionRecord()).thenReturn(pfmTransactionRecordWrapper);
			when(userSession.getPfmTransactionSubcategoryRecord()).thenReturn(pfmTransactionRecordWrapper);
			when(userSession.getPfmTransactionSubcategoryRecord().getCurrentPage()).thenReturn(0);
			when(userSession.getPfmTransactionSubcategoryRecord().getPagingRecords()).thenReturn(0);
		
			when(ServletRequestUtils.getStringParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn("true", "03-2020", "03-2020");
			when(ServletRequestUtils.getIntParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn(1);

			
			when(customerService.getPfmTransactionsSubcategory(ArgumentMatchers.any(), 
					ArgumentMatchers.any(List.class), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.any(PFMSortOrderWrapper.class))).
			thenReturn(pfmTransactionRecordWrapper);
			
			PFMTransactionWrapper pfmTransactionWrapper = mock(PFMTransactionWrapper.class);
			
			when(pfmTransactionWrapper.getTransactionId()).thenReturn("123456789");
			
			List<PFMTransactionWrapper> pfmTransactionWrapperList = new ArrayList<>();
			
			pfmTransactionWrapperList.add(pfmTransactionWrapper);
	
			when(pfmTransactionRecordWrapper.getPfmTransactions()).thenReturn(pfmTransactionWrapperList);
			
			controller.pfmReloadSubcategoryTransactions(request, response);
			
			// Validate that the map gets inserted into session.
			verify(userSession, times(1)).getCheckImageMap();
			
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void whenPfmTagTransactions_GivenCheckImageTransaction_ThenAddCheckImageMapToSession() {
		
		try {
			
			when(userSession.getPfmTransactionRecord()).thenReturn(pfmTransactionRecordWrapper);
			when(userSession.getPfmTransactionRecord().getCurrentPage()).thenReturn(0);
			when(userSession.getPfmTransactionRecord().getPagingRecords()).thenReturn(0);
		
			when(ServletRequestUtils.getStringParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn("true", "03-2020", "03-2020");
			when(ServletRequestUtils.getIntParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn(1);

			when(customerService.getPfmTransactionsTag(ArgumentMatchers.any(), 
					ArgumentMatchers.any(List.class), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.any(PFMSortOrderWrapper.class),
					ArgumentMatchers.any(),
					ArgumentMatchers.any(List.class))).
			thenReturn(pfmTransactionRecordWrapper);
			
			PFMTransactionWrapper pfmTransactionWrapper = mock(PFMTransactionWrapper.class);
			
			when(pfmTransactionWrapper.getTransactionId()).thenReturn("123456789");
			
			List<PFMTransactionWrapper> pfmTransactionWrapperList = new ArrayList<>();
			
			pfmTransactionWrapperList.add(pfmTransactionWrapper);
	
			when(pfmTransactionRecordWrapper.getPfmTransactions()).thenReturn(pfmTransactionWrapperList);
			
			controller.pfmTagTransactions(request, response);
			
			// Validate that the map gets inserted into session.
			verify(userSession, times(1)).getCheckImageMap();
			
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
	}
	
	@Test
	public void whenPfmAccountDetailsTransactions_GivenCheckImageTransaction_ThenAddCheckImageMapToSession() {
		
		try {
			
			CustomerEntitlement entitlement = mock(CustomerEntitlement.class);
			when(entitlement.getString(any())).thenReturn("12312312323123");
			
			when(userSession.getCustomerProfile().getEntitlementByType(any(EntitlementType.class)))
			.thenReturn(entitlement);
			when(userSession.getCustomerProfile().getTaxId()).thenReturn("598000000");
			when(userSession.getCustomerProfile().getEnrollmentAccountNumber()).thenReturn("000000000");

			when(userSession.getPfmTransactionRecord()).thenReturn(pfmTransactionRecordWrapper);
			when(userSession.getPfmTransactionRecord().getCurrentPage()).thenReturn(0);
			when(userSession.getPfmTransactionRecord().getPagingRecords()).thenReturn(0);
		
			when(ServletRequestUtils.getStringParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn("true", "03-2020", "03-2020");
			when(ServletRequestUtils.getIntParameter(ArgumentMatchers.any(HttpServletRequest.class), ArgumentMatchers.any()))
			.thenReturn(1);

			when(customerService.getPfmTransactionsByAccount(
					ArgumentMatchers.any(), 		
					ArgumentMatchers.any(PFMAccountWrapper.class), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.any(), 
					ArgumentMatchers.anyInt(), 
					ArgumentMatchers.anyInt(),  
					ArgumentMatchers.any(PFMSortOrderWrapper.class)))
			.thenReturn(pfmTransactionRecordWrapper);
			
			PFMTransactionWrapper pfmTransactionWrapper = mock(PFMTransactionWrapper.class);
			
			when(pfmTransactionWrapper.getTransactionId()).thenReturn("123456789");
			
			List<PFMTransactionWrapper> pfmTransactionWrapperList = new ArrayList<>();
			
			pfmTransactionWrapperList.add(pfmTransactionWrapper);
	
			when(pfmTransactionRecordWrapper.getPfmTransactions()).thenReturn(pfmTransactionWrapperList);
			
			// Had to make this in order to test final line.
			PFMAccountWrapper accountWrapper = mock(PFMAccountWrapper.class);
			when(accountWrapper.getAccountId()).thenReturn("11111");
			
			PowerMockito.whenNew(PFMAccountWrapper.class).withAnyArguments().thenReturn(accountWrapper);
			
			controller.pfmAccountDetailsTransactions(request, response);
			
			// Validate that the map gets inserted into session.
			verify(userSession, times(1)).getCheckImageMap();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
	}

}
