/**
 * package that contains the mappers
 * <p>
 * Project: CIBP Web
 * Company: Evertec
 */
package evertec.cibp.web.controller.dto.mapper;

import edu.emory.mathcs.backport.java.util.Arrays;

import evertec.cibp.web.controller.dto.response.ErrorMessage;

import org.junit.Before;
import org.junit.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.validation.FieldError;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * The ErrorsMapperUT
 *
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @version 1.8
 * @created 2020-02-12 17:28
 * @see ErrorsMapper
 * @since 1.0
 */
public class ErrorsMapperUT {

    /**
     * First object name
     */
    private static final String TEST_1_OBJECT_NAME = "test1"; // First object name
    /**
     * First field name
     */
    private static final String FIELD_1_NAME = "field1"; // First field name
    /**
     * First default message value
     */
    private static final String DEFAULT_MESSAGE_1_VALUE = "defaultMessage1"; // First default message value
    /**
     * Second object name
     */
    private static final String TEST_2_OBJECT_NAME = "test2"; // Second object name
    /**
     * Second field name
     */
    private static final String FIELD_2_NAME = "field2"; //Second field name
    /**
     * Second Default message value
     */
    private static final String DEFAULT_MESSAGE_2_VALUE = "defaultMessage2"; //Second Default message value
    /**
     * Number two use in general terms.
     */
    private static final int NUMBER_TWO = 2; //Number two use in general terms.
    /**
     * Instance to test
     */
    private ErrorsMapper mapper; //Instance to test

    /**
     * Set up for all methods
     */
    @Before
    public void setUp() {
        mapper =  new ErrorsMapperImpl();
    }

    /**
     * Validate the single mapping
     */
    @Test
    public void toErrorResponseTest() {

        //Given
        final FieldError firstError =
                new FieldError(TEST_1_OBJECT_NAME, FIELD_1_NAME, DEFAULT_MESSAGE_1_VALUE); // Value to use in test
        //When
        ErrorMessage result = null;// result to evaluate

        result = mapper.toErrorResponse(firstError);

        //Then
        validateSingleMapping(firstError, result);
    }

    /**
     * Validate the list mapping
     */
    @Test
    public void toErrorListResponseTest() {

        //Given
        final FieldError firstError =
                new FieldError(TEST_1_OBJECT_NAME, FIELD_1_NAME, DEFAULT_MESSAGE_1_VALUE); // First field error test value
        final FieldError secondError =
                new FieldError(TEST_2_OBJECT_NAME, FIELD_2_NAME, DEFAULT_MESSAGE_2_VALUE); // Second field error test value
        final List<FieldError> errorList = Arrays.asList(new FieldError[]{firstError, secondError}); // Variable to mapp

        //When
        List<ErrorMessage> responseList = null; // Method result to evaluate

        responseList = mapper.toErrorResponseList(errorList);

        //Then
        assertEquals(NUMBER_TWO, errorList.size());
        validateSingleMapping(firstError, responseList.get(0));
        validateSingleMapping(secondError, responseList.get(1));
    }

    /**
     * @param error  to assert
     * @param result of the test
     */
    private static void validateSingleMapping(FieldError error, ErrorMessage result) {
        assertEquals(error.getField(), result.getFieldName());
        assertEquals(error.getDefaultMessage(), result.getMessage());
    }
}
