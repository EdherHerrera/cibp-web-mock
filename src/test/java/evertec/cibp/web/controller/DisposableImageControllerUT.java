package evertec.cibp.web.controller;

import static org.mockito.Mockito.when;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.mobiusview.client.navigation.client.MobiusViewClient;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * Unit testing WebAppController java class.
 * @author jquesadag
 * @since 14-02-2020
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({DisposableImageController.class,WebUtils.class, Utils.class, CustomerServices.class, StaticMessageSource.class, CustomerFacade.class})
public class DisposableImageControllerUT extends AbstractModelAndViewTests  {

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @Mock
    private UserSession userSession;

    @InjectMocks
    private DisposableImageController controller;

    @Before
    public void setUp() throws Exception {

        controller = PowerMockito.spy(new DisposableImageController());
        MockitoAnnotations.initMocks(this);
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(CustomerFacade.class);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
    }

    @Test
    public void whenDownloadImage_GivenValidImageKey_DownloadAndDeleteFile() throws Exception {
       
        
        String filename = "";
        
        File tempFile = File.createTempFile("MBImage", ".tmp");
	    filename = tempFile.getName();
	    tempFile.deleteOnExit();
	     
        String key = MobiusViewClient.encrypt(filename);
        request.setParameter("key", key);
        assertTrue(tempFile.exists());
        controller.downloadImage(request, response,key);
        assertFalse(tempFile.exists());
    }
    
    @Test
    public void whenDownloadImage_GivenInvalidImageKey_Fail() throws Exception {
       
        
        String filename = "";
        
        File tempFile = File.createTempFile("MBImage", ".tmp");
	    filename = tempFile.getName();
	    tempFile.deleteOnExit();
	     
        String key = MobiusViewClient.encrypt(filename);
        request.setParameter("key", filename);
        assertTrue(tempFile.exists());
        controller.downloadImage(request, response,filename);
        assertTrue(tempFile.exists());
        tempFile.delete();
    }
    
    @Test
    public void whenDownloadImage_GivenWrongImageKey_Fail() throws Exception {
       
        
        String filename = "";
        
        File tempFile = File.createTempFile("MBImage", ".tmp");
	    filename = tempFile.getName();
	    tempFile.deleteOnExit();	     
        String key = MobiusViewClient.encrypt(filename+"1");
        request.setParameter("key", key);
        assertTrue(tempFile.exists());
        controller.downloadImage(request, response,key);
        assertTrue(tempFile.exists());
        tempFile.delete();
    }

}
