/**
 * 
 */
package evertec.cibp.web.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.web.controller.bean.EmailData;
import evertec.cibp.web.controller.dto.mapper.ErrorsMapper;
import evertec.cibp.web.controller.dto.mapper.ErrorsMapperImpl;
import evertec.cibp.web.session.UserSession;

/**
 * @author mdias
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class})
public class MobileChangeEmailControllerIT {
	
	private MockHttpServletRequest request;
	
	private static final String FIRST_EMAIL_TEST = "test@test.com"; 
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private BindingResult errors;
	
	@InjectMocks
	private MobileChangeEmailController controller; 
	
	private static Validator validator; 

    @BeforeClass
    public static void generalSetUp() {
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory(); // Factory to get the validator

        validator = factory.getValidator();
    }
	
	@Before
	public void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(WebUtils.class);
		
		request = new MockHttpServletRequest();

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);

        final ErrorsMapper errorsMapper = new ErrorsMapperImpl(); // Error mapper to use in class

        controller = new MobileChangeEmailController(errorsMapper);
	}
	
	
	 @Test
	 public void whenITDownstreamValidateFieldsNoErrors_executedFromMobileChangeEmailController() {

	        //Given
	        when(errors.hasErrors()).thenReturn(false);
	        final EmailData emailData = new EmailData(FIRST_EMAIL_TEST, FIRST_EMAIL_TEST);

	        //When
	        final ResponseEntity<?> result = controller.validateFields(emailData, errors); 

	        Set<ConstraintViolation<EmailData>> constraintViolations = null; 

	        constraintViolations = validator.validate(emailData);

	        //Then
	        assertEquals(HttpStatus.OK, result.getStatusCode());
	        assertEquals(constraintViolations.size(), 0);
	    }

}
