/**
 * 
 */
package evertec.cibp.web.controller.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder.NodeBuilderDefinedContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.util.WebUtils;

import org.apache.commons.beanutils.BeanUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.web.controller.bean.EmailData;
import evertec.cibp.web.session.UserSession;

/**
 * @author mdias
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, BeanUtils.class})
public class FieldMatchValidatorUT {
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private BindingResult errors;
	
	@Mock
	private ConstraintValidatorContext context;
	
	@Mock
	private ConstraintViolationBuilder constraintBuilder;
	
	@Mock
	private NodeBuilderDefinedContext node;
	
	@Mock
	private ConstraintValidatorContext constraintValidatorContext;
	
	@InjectMocks
	private FieldMatchValidator fieldValidator;
	
    private static final String FIRST_EMAIL_TEST = "test@test.com"; //Email correct email

    private static final String SECOND_CORRECT_EMAIL = "Test@test.com"; //Second correct email with capital letter

    private MockHttpServletRequest request;
  
	@Before
	public void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(BeanUtils.class);
		
		 request = new MockHttpServletRequest();
		 
		 fieldValidator = PowerMockito.spy(new FieldMatchValidator());
		 context = mock(ConstraintValidatorContext.class, RETURNS_DEEP_STUBS);
		 constraintBuilder = mock(ConstraintViolationBuilder.class, RETURNS_DEEP_STUBS);
		 node = mock(NodeBuilderDefinedContext.class, RETURNS_DEEP_STUBS);
		 constraintValidatorContext = mock(ConstraintValidatorContext.class, RETURNS_DEEP_STUBS);
		 
		 when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
	     when(userSession.getCustomerProfile()).thenReturn(customerProfile);
	
	}

	@SuppressWarnings("deprecation")
	@Test
    public void whenIsValid_givenEmailAndConfirmationNotMatchEmail_thenReturnFalse() throws Exception{
		
		when(errors.hasErrors()).thenReturn(true);
        EmailData emailData = new EmailData(FIRST_EMAIL_TEST, SECOND_CORRECT_EMAIL);// Email data for the test
        when(BeanUtils.getProperty((Object)emailData,"newEmail")).thenReturn(FIRST_EMAIL_TEST);
        when(BeanUtils.getProperty((Object)emailData,"confirmEmail")).thenReturn(SECOND_CORRECT_EMAIL);
        
        when(context.buildConstraintViolationWithTemplate(any())).thenReturn(constraintBuilder);
        when(constraintBuilder.addNode(any())).thenReturn(node);
        when(node.addConstraintViolation()).thenReturn(constraintValidatorContext);
        
        
        Boolean result = fieldValidator.isValid(emailData, context);
        assertFalse(result);
	}
	
	@SuppressWarnings("deprecation")
	@Test
    public void whenIsValid_givenEmailAndConfirmationMatch_thenReturnTrue() throws Exception{
		
		when(errors.hasErrors()).thenReturn(true);
        final EmailData emailData = new EmailData(FIRST_EMAIL_TEST, FIRST_EMAIL_TEST);// Email data for the test
        
        when(BeanUtils.getProperty(any(),any())).thenReturn(FIRST_EMAIL_TEST);
        
        
        when(context.buildConstraintViolationWithTemplate(any())).thenReturn(constraintBuilder);
        when(constraintBuilder.addNode(any())).thenReturn(node);
        when(node.addConstraintViolation()).thenReturn(constraintValidatorContext);
       
        
        Boolean result = fieldValidator.isValid(emailData, context);
        assertTrue(result);
	}

}
