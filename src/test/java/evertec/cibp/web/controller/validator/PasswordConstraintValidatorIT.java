package evertec.cibp.web.controller.validator;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.AssertTrue;
import javax.servlet.http.Cookie;

import org.apache.commons.beanutils.BeanUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.web.form.ChangePasswordForm;
import evertec.cibp.web.form.bean.ChangePasswordBean;
import evertec.cibp.web.session.UserSession;
import com.rsa.csd.ws.DeviceRequest;

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, BeanUtils.class})
public class PasswordConstraintValidatorIT {

    @InjectMocks
    private ChangePasswordForm changePasswordForm;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private ChangePasswordBean command;

    private BindException error;

    @Mock
    evertec.cibp.web.controller.validator.PasswordConstraintValidator passwordConstraintValidator;

    @Mock
    private UserSession userSession;

    @Mock
    private ChangePasswordBean dude;

    @Mock
    CustomerProfile profile;


    @Mock
    DeviceRequest deviceRequest;

    @Mock
    Map<String, String> paramsResult = new HashMap<String, String>();

    @Mock
    Map<String, Object> paramsResult1 = new HashMap<String, Object>();

    @Before
    public void setUp() throws Exception {


        PowerMockito.mockStatic(WebUtils.class);
        command.setPassword("Test15368");
        Map<Object, Object> paramsResult1 = new HashMap<Object, Object>();
        paramsResult1.put("prueba", "prueba");


        when(command.getOrigpassword()).thenReturn("Test1234");
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(WebUtils.getCookie(request, "PMData")).thenReturn(new Cookie("PMData", ""));
        when(userSession.getCustomerProfile()).thenReturn(profile);
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(deviceRequest.getDeviceTokenCookie()).thenReturn(null);
        when(userSession.getInformationQuestion()).thenReturn(paramsResult1);
        when(passwordConstraintValidator.validateRules(any(), any(), any())).thenReturn(paramsResult);
    }

    @Test
    public void whenUpstreamGeIsValid_ExecutedFromonBindAndValidateChangePasswordForm_GivenInValidPassword_ThenTrue() {

        try {
            when(command.getPassword()).thenReturn("Test123456");
            when(command.getPasswordconfirm()).thenReturn("Test123456");
            ServletRequestDataBinder binder = new ServletRequestDataBinder(command, "command");
            error = new BindException(binder.getBindingResult());
            Whitebox.invokeMethod(changePasswordForm, "onBindAndValidate", request, command, error);
            //verify(passwordConstraintValidator, times(1)).isValid(any());
            //error.get
            assertTrue(error.getBindingResult().getFieldErrors().size() > 0);

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

}