package evertec.cibp.web.controller.validator;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.Utils;


@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, BeanUtils.class})
public class PasswordConstraintValidatorUT {
	
	@InjectMocks
	private PasswordConstraintValidator passwordConstraintValidator;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void whenIsValid_GivenPasswordwithoutwordinthelistLanguageEn_ThenReturnFalse() {
		Map<String, String> paramsPass = new HashMap<String, String>();
		CustomerProfile profile = new CustomerProfile();
		profile.setLastName("Jorge");
		profile.setFirstName("Hunquera");
		profile.setUsername("cubano");	
		paramsPass.put(ChallengeInformationUtils.NEW_PASS,"Password5895*");
		paramsPass.put(ChallengeInformationUtils.OLD_PASS,"Test1234");
		paramsPass.put(ChallengeInformationUtils.WORD_LIST,"password;username;clave");
		paramsPass.put(ChallengeInformationUtils.LANGUAGE,"en");

		Map<String, String>  paramsResult = passwordConstraintValidator.validateRules(paramsPass,profile,Boolean.FALSE);
		
		assertEquals(paramsResult.get("validate"), "false");
		assertTrue(paramsResult.get("message").contains("Password does not allow words related to the authentication process. (E.g. password,username, user)"));
	}
	
	
	@Test
	public void whenIsValid_GivenPasswordwithoutUpperCaseLanguageEn_ThenReturnFalse() {
		Map<String, String> paramsPass = new HashMap<String, String>();
		CustomerProfile profile = new CustomerProfile();
		profile.setLastName("Jorge");
		profile.setFirstName("Hunquera");
		profile.setUsername("cubano");	
	
		paramsPass.put(ChallengeInformationUtils.NEW_PASS,"casita5895");
		paramsPass.put(ChallengeInformationUtils.OLD_PASS,"Test1234");
		paramsPass.put(ChallengeInformationUtils.WORD_LIST,"password;username;clave");
		paramsPass.put(ChallengeInformationUtils.LANGUAGE,"en");

		Map<String, String>  paramsResult = passwordConstraintValidator.validateRules(paramsPass,profile,Boolean.FALSE);
		
		assertEquals(paramsResult.get("validate"), "false");
		assertTrue(paramsResult.get("message").contains("Password must contain 1 or more uppercase characters"));
	}
	
	@Test
	public void whenIsValid_GivenPasswordwithoutLowerCaseLanguageEn_ThenReturnFalse() {
		CustomerProfile profile = new CustomerProfile();
		profile.setLastName("Jorge");
		profile.setFirstName("Hunquera");
		profile.setUsername("cubano");	
		Map<String, String> paramsPass = new HashMap<String, String>();
		paramsPass.put(ChallengeInformationUtils.NEW_PASS,"CASITA5895*");
		paramsPass.put(ChallengeInformationUtils.OLD_PASS,"Test1234");
		paramsPass.put(ChallengeInformationUtils.WORD_LIST,"password;username;clave");
		paramsPass.put(ChallengeInformationUtils.LANGUAGE,"en");
		
		Map<String, String>  paramsResult = passwordConstraintValidator.validateRules(paramsPass,profile,Boolean.FALSE);
		
		assertEquals(paramsResult.get("validate"), "false");
		assertTrue(paramsResult.get("message").contains("Password must contain 1 or more lowercase characters"));
	}
	
	@Test
	public void whenIsValid_GivenPasswordwithoutDigitLanguageEn_ThenReturnFalse() {
		CustomerProfile profile = new CustomerProfile();
		profile.setLastName("Jorge");
		profile.setFirstName("Hunquera");
		profile.setUsername("cubano");	
		String password = "CASITAcasita*";
		String wordList = "password;username;clave";
		Map<String, String> paramsPass = new HashMap<String, String>();
		paramsPass.put(ChallengeInformationUtils.NEW_PASS,"CASITAcasita*");
		paramsPass.put(ChallengeInformationUtils.OLD_PASS,"Test1234");
		paramsPass.put(ChallengeInformationUtils.WORD_LIST,"password;username;clave");
		paramsPass.put(ChallengeInformationUtils.LANGUAGE,"en");
	
		Map<String, String>  paramsResult = passwordConstraintValidator.validateRules(paramsPass,profile,Boolean.FALSE);
		
		assertEquals(paramsResult.get("validate"), "false");
		assertTrue(paramsResult.get("message").contains("Password must contain 1 or more digit characters"));
	}
	
	@Test
	public void whenIsValid_GivenPasswordwithAllPolicy_ThenReturnTrue() {
		CustomerProfile profile = new CustomerProfile();
		profile.setLastName("Jorge");
		profile.setFirstName("Hunquera");
		profile.setUsername("cubano");	
		Map<String, String> paramsPass = new HashMap<String, String>();
		paramsPass.put(ChallengeInformationUtils.NEW_PASS,"Ccasita15689*");
		paramsPass.put(ChallengeInformationUtils.OLD_PASS,"Test1234");
		paramsPass.put(ChallengeInformationUtils.WORD_LIST,"password;username;clave");
		paramsPass.put(ChallengeInformationUtils.LANGUAGE,"en");
		Map<String, String>  paramsResult = passwordConstraintValidator.validateRules(paramsPass,profile,Boolean.FALSE);
		assertEquals(paramsResult.get("validate"), "true");
	}
	
	@Test
	public void whenIsValid_GivenPasswordwithoutAlphabeticalLanguageEn_ThenReturnFalse() {
		CustomerProfile profile = new CustomerProfile();
		profile.setLastName("Jorge");
		profile.setFirstName("Hunquera");
		profile.setUsername("cubano");	

		Map<String, String> paramsPass = new HashMap<String, String>();
		paramsPass.put(ChallengeInformationUtils.NEW_PASS,"Aabcde18567*");
		paramsPass.put(ChallengeInformationUtils.OLD_PASS,"Test1234");
		paramsPass.put(ChallengeInformationUtils.WORD_LIST,"password;username;clave");
		paramsPass.put(ChallengeInformationUtils.LANGUAGE,"en");
		Map<String, String>  paramsResult = passwordConstraintValidator.validateRules(paramsPass,profile,Boolean.FALSE);
		
		assertEquals(paramsResult.get("validate"), "false");
		assertTrue(paramsResult.get("message").contains("Password does not allow sequences of 4 consecutive characters. (E.g. ABCD)."));
	}
	
	@Test
	public void whenIsValid_GivenPasswordwithoutNumericalSequenceLanguageEn_ThenReturnFalse() {
		CustomerProfile profile = new CustomerProfile();
		profile.setLastName("Jorge");
		profile.setFirstName("Hunquera");
		profile.setUsername("cubano");	
		
		Map<String, String> paramsPass = new HashMap<String, String>();
		paramsPass.put(ChallengeInformationUtils.NEW_PASS,"Casita123456*");
		paramsPass.put(ChallengeInformationUtils.OLD_PASS,"Test1234");
		paramsPass.put(ChallengeInformationUtils.WORD_LIST,"password;username;clave");
		paramsPass.put(ChallengeInformationUtils.LANGUAGE,"en");
		Map<String, String>  paramsResult = passwordConstraintValidator.validateRules(paramsPass,profile,Boolean.FALSE);
		
		assertEquals(paramsResult.get("validate"), "false");
		assertTrue(paramsResult.get("message").contains("Password does not allow sequences of 4 consecutive numbers."));
	}
	
	@Test
	public void whenIsValid_GivenPasswordwithoutSpecialCharacterLanguageEn_ThenReturnFalse() {
		CustomerProfile profile = new CustomerProfile();
		profile.setLastName("Jorge");
		profile.setFirstName("Hunquera");
		profile.setUsername("cubano");	
		String password = "Casita15397";
		String wordList = "password;username;clave";
		Map<String, String> paramsPass = new HashMap<String, String>();
		paramsPass.put(ChallengeInformationUtils.NEW_PASS,"Casita15397");
		paramsPass.put(ChallengeInformationUtils.OLD_PASS,"Test1234");
		paramsPass.put(ChallengeInformationUtils.WORD_LIST,"password;username;clave");
		paramsPass.put(ChallengeInformationUtils.LANGUAGE,"en");
		Map<String, String>  paramsResult = passwordConstraintValidator.validateRules(paramsPass,profile,Boolean.FALSE);
		
		assertEquals(paramsResult.get("validate"), "false");
		assertTrue(paramsResult.get("message").contains("Password must have at least one special character or symbol."));
	}
	
	@Test
	public void whenIsValid_GivenPasswordwithoutSpecialCharacterLanguageEN_ThenReturnFalse() {
		CustomerProfile profile = new CustomerProfile();
		profile.setLastName("Jorge");
		profile.setFirstName("Hunquera");
		profile.setUsername("cubano");	

		Map<String, String> paramsPass = new HashMap<String, String>();
		paramsPass.put(ChallengeInformationUtils.NEW_PASS,"Casita15397");
		paramsPass.put(ChallengeInformationUtils.OLD_PASS,"Test1234");
		paramsPass.put(ChallengeInformationUtils.WORD_LIST,"password;username;clave");
		paramsPass.put(ChallengeInformationUtils.LANGUAGE,"en");

		Map<String, String>  paramsResult = passwordConstraintValidator.validateRules(paramsPass,profile,Boolean.FALSE);
		
		assertEquals(paramsResult.get("validate"), "false");
		assertTrue(paramsResult.get("message").contains("Password must have at least one special character or symbol. (E.g. *^$!&~#) ."));
	}
	
	@Test
	public void whenIsValid_GivenPasswordwithouSamePasswordOriginCharacterLanguageEN_ThenReturnFalse() {
		CustomerProfile profile = new CustomerProfile();
		profile.setLastName("Jorge");
		profile.setFirstName("Hunquera");
		profile.setUsername("cubano");	

		Map<String, String> paramsPass = new HashMap<String, String>();
		paramsPass.put(ChallengeInformationUtils.NEW_PASS,"Test1234");
		paramsPass.put(ChallengeInformationUtils.OLD_PASS,"Test1234");
		paramsPass.put(ChallengeInformationUtils.WORD_LIST,"password;username;clave");
		paramsPass.put(ChallengeInformationUtils.LANGUAGE,"en");
		
		Map<String, String>  paramsResult = passwordConstraintValidator.validateRules(paramsPass,profile,Boolean.FALSE);
		
		assertEquals(paramsResult.get("validate"), "false");
		assertTrue(paramsResult.get("message").contains("Password matches one of 1 previous passwords"));
	}	

}
