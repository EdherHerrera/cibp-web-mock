/**
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @created 2020-02-12 17:28
 */

package evertec.cibp.web.controller;

import edu.emory.mathcs.backport.java.util.Arrays;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.web.controller.bean.EmailData;
import evertec.cibp.web.controller.dto.mapper.ErrorsMapper;
import evertec.cibp.web.controller.dto.mapper.ErrorsMapperImpl;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.owasp.csrfguard.CsrfGuardException;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.util.WebUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static evertec.cibp.web.controller.MobileChangeEmailController.CURRENT_EMAIL_FIELD_NAME;
import static evertec.cibp.web.controller.MobileChangeEmailController.TIME_INTERVAL_FIELD_NAME;
import static evertec.cibp.web.controller.MobileChangeEmailController.VIEW_RELATIVE_PATH;
import static evertec.cibp.web.controller.bean.EmailData.EMAIL_TEXT_VALIDATION_INVALID_KEY;
import static evertec.cibp.web.controller.bean.EmailData.EMAIL_TEXT_VALIDATION_NOT_MATCH_KEY;
import static evertec.cibp.web.controller.bean.EmailData.EMAIL_TEXT_VALIDATION_REQ_CONF_KEY;
import static evertec.cibp.web.controller.bean.EmailData.EMAIL_TEXT_VALIDATION_REQUIRED_KEY;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * The MobileChangeEmailControllerUT
 *
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @version 1.8
 * @created 2020-02-12 17:28
 * @see MobileChangeEmailController
 * @since 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, Utils.class})
public class MobileChangeEmailControllerUT {

    /**
     * New email name
     */
    private static final String NEW_EMAIL_FIELD_NAME = "newEmail"; //New email name

    /**
     * Confirm email name
     */
    private static final String CONFIRM_EMAIL_FIELD_NAME = "confirmEmail"; //Confirm email name

    /**
     * Email correct email
     */
    private static final String FIRST_EMAIL_TEST = "test@test.com"; //Email correct email

    /**
     * Second correct email
     */
    private static final String SECOND_CORRECT_EMAIL = "test2@test.com"; //Second correct email

    /**
     * Email with only name evaluate
     */
    private static final String ONLY_NAME_EMAIL = "test2"; //Email with only name evaluate

    /**
     * Email with arrow evaluate
     */
    private static final String NAME_AND_ARROW_EMAIL = "test2@"; //Email with arrow evaluate

    /**
     * Email with arrow and domain to evaluate
     */
    private static final String NAME_ARROW_DOMAIN_EMAIL = "test2@test"; //Email with arrow and domain to evaluate

    /**
     * Email with arrow and type to evaluate
     */
    private static final String NAME_ARROW_TYPE_EMAIL = "test2@.com"; //Email with arrow and type to evaluate

    /**
     * First incorrect email to evaluate
     */
    private static final String FIRST_INCORRECT_EMAIL = "123";//First incorrect email to evaluate

    /**
     * Second incorrect email to evaluate
     */

    private static final String SECOND_INCORRECT_EMAIL = "1234";//Second incorrect email to evaluate


    /**
     * List of VALID emails formats to check
     */
    private static final List<String> VALID_EMAILS = Arrays.asList(new String[]{"email@yahoo.com", "email-100@yahoo.com",
            "Email.100@yahoo.com", "email111@email.com", "email-100@email.net",
            "email.100@email.com.au", "emAil@1.com", "email@gmail.com.com",
            "email+100@gmail.com", "emAil-100@yahoo-test.com",
            "email_100@yahoo-test.ABC.CoM"}); // List of VALID emails formats to check

    /**
     * List of INVALID emails formats to check
     * On change from type of valid email from apache commons validator was removed two cases on UT
     * because apache validator take it like valid
     *  --"test@test.com "
     *  --" test@test.com"
     */
    private static final List<String> INVALID_EMAILS = Arrays.asList(new String[]{"example.com",
            "A@b@c@domain.com", "a”b(c)d,e:f;gi[j\\k]l@domain.com", "abc”test”email@domain.com",
            "abc is”not\\valid@domain.com", "abc\\ is\\”not\\valid@domain.com",
            ".test@domain.com", "test@domain..com",
            "test@test"}); // List of INVALID emails formats to check

    /**
     * Spring mock request
     */
    private MockHttpServletRequest request; //Spring mock request

    /**
     * User session to emulate
     */
    @Mock
    private UserSession userSession; //User session to emulate

    /**
     * Profile that contains email
     */
    @Mock
    private CustomerProfile customerProfile; //Profile that contains email

    /**
     * The errors on the validate method
     */
    @Mock
    private BindingResult errors; //The errors on the validate method

    /**
     * The class to evaluate
     */
    private MobileChangeEmailController controller; // The class to evaluate

    /**
     * The validator to use in the class
     * changed the validator from spring validator to apache Validator
     */
    private static Validator validator; //The validator to use in the class

    @BeforeClass
    public static void generalSetUp() {
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory(); // Factory to get the validator

        validator = factory.getValidator();
    }

    @Before
    public void setUp() throws Exception {

        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Utils.class);
        request = new MockHttpServletRequest();

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);

        final ErrorsMapper errorsMapper = new ErrorsMapperImpl(); // Error mapper to use in class

        controller = new MobileChangeEmailController(errorsMapper);
    }

    @Test
    public void whenMobileChangeEmailSuccessFull() {

        //Given
        final ModelMap modelMap = new ModelMap(); // The modelMap used in tests for the view
        final String email = FIRST_EMAIL_TEST; // The email to test
        final int intervalSeconds = 5; // Interval seconds to use in test
        when(userSession.getCustomerProfile().getEmail()).thenReturn(email);

        //When
        controller.setTimeInterval(intervalSeconds);

        String pageName = null; //The test response to evaluate

        try {
			pageName = controller.mobileChangeEmail(request, modelMap);
		} catch (CsrfGuardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        //Then
        assertEquals(VIEW_RELATIVE_PATH, pageName);
        assertEquals(email, modelMap.get(CURRENT_EMAIL_FIELD_NAME));
        assertEquals(intervalSeconds, modelMap.get(TIME_INTERVAL_FIELD_NAME));
    }

    @Test
    public void whenMobileChangeEmailNoValEmail() {

        //Given
        final ModelMap modelMap = new ModelMap(); // The modelMap used in tests for the view
        final int intervalSeconds = 5; // Interval seconds to use in test
        when(userSession.getCustomerProfile().getEmail()).thenReturn(null);

        //When
        controller.setTimeInterval(intervalSeconds);

        String pageName = null; //The test response to evaluate

        try {
			pageName = controller.mobileChangeEmail(request, modelMap);
		} catch (CsrfGuardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        //Then
        assertEquals(VIEW_RELATIVE_PATH, pageName);
        assertEquals(null, modelMap.get(CURRENT_EMAIL_FIELD_NAME));
        assertEquals(intervalSeconds, modelMap.get(TIME_INTERVAL_FIELD_NAME));
    }

    @Test
    public void whenMobileChangeEmailNoValProfile() {

        //Given
        final ModelMap modelMap = new ModelMap(); // The modelMap used in tests for the view
        final int intervalSeconds = 5; // Interval seconds to use in test

        when(userSession.getCustomerProfile()).thenReturn(null);

        //When
        controller.setTimeInterval(intervalSeconds);

        String pageName = null; //The test response to evaluate

        try {
			pageName = controller.mobileChangeEmail(request, modelMap);
		} catch (CsrfGuardException e) {
			e.printStackTrace();
		}

        //Then
        assertEquals(VIEW_RELATIVE_PATH, pageName);
        assertEquals(null, modelMap.get(CURRENT_EMAIL_FIELD_NAME));
        assertEquals(intervalSeconds, modelMap.get(TIME_INTERVAL_FIELD_NAME));
    }

    @Test
    public void validateFieldsNoErrors() {

        //Given
        when(errors.hasErrors()).thenReturn(false);
        final EmailData emailData = new EmailData(FIRST_EMAIL_TEST, FIRST_EMAIL_TEST);// Email data for the test

        //When
        final ResponseEntity<?> result = controller.validateFields(emailData, errors); // Response from the tested method

        Set<ConstraintViolation<EmailData>> constraintViolations = null; // variable to evaluate the ConstraintViolations

        constraintViolations = validator.validate(emailData);

        //Then
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(constraintViolations.size(), 0);
    }

    @Test
    public void validateFieldsEmptyFieldsError() {

        //Given
        when(errors.hasErrors()).thenReturn(true);
        final EmailData emailData = new EmailData();// Email data for the test

        //When
        final ResponseEntity<?> result = controller.validateFields(emailData, errors); // Response from the tested method

        Set<ConstraintViolation<EmailData>> constraintViolations = null; // variable to evaluate the ConstraintViolations

        constraintViolations = validator.validate(emailData);

        //Then
        final int numConstantViolation = 3;//Number of constants

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, result.getStatusCode());
        assertEquals(constraintViolations.size(), numConstantViolation);
        assertEquals(1, countConstraintsWithMessages(constraintViolations, NEW_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_REQUIRED_KEY})));

        assertEquals(2, countConstraintsWithMessages(constraintViolations, CONFIRM_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_REQ_CONF_KEY, EMAIL_TEXT_VALIDATION_NOT_MATCH_KEY})));
    }

    @Test
    public void validateFieldsIncorrectBothFieldsFormat() {

        //Given
        when(errors.hasErrors()).thenReturn(true);
        final EmailData emailData = new EmailData(FIRST_INCORRECT_EMAIL, SECOND_INCORRECT_EMAIL);// Email data for the test

        //When
        final ResponseEntity<?> result = controller.validateFields(emailData, errors); // Response from the tested method

        Set<ConstraintViolation<EmailData>> constraintViolations = null; // variable to evaluate the ConstraintViolations

        constraintViolations = validator.validate(emailData);

        //Then
        final int numConstantViolation = 2;//Number of constants

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, result.getStatusCode());
        assertEquals(constraintViolations.size(), numConstantViolation);

        assertEquals(1, countConstraintsWithMessages(constraintViolations, NEW_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY})));


        assertEquals(1, countConstraintsWithMessages(constraintViolations, CONFIRM_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY, EMAIL_TEXT_VALIDATION_NOT_MATCH_KEY})));
    }

    @Test
    public void validateCorrectAndIncorrectFormat() {

        //Given
        when(errors.hasErrors()).thenReturn(true);
        final EmailData emailData = new EmailData(FIRST_EMAIL_TEST, SECOND_INCORRECT_EMAIL);// Email data for the test

        //When
        final ResponseEntity<?> result = controller.validateFields(emailData, errors); // Response from the tested method

        Set<ConstraintViolation<EmailData>> constraintViolations = null; // variable to evaluate the ConstraintViolations

        constraintViolations = validator.validate(emailData);

        //Then
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, result.getStatusCode());
        assertEquals(constraintViolations.size(), 1);

        assertEquals(0, countConstraintsWithMessages(constraintViolations, NEW_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY})));


        assertEquals(1, countConstraintsWithMessages(constraintViolations, CONFIRM_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY, EMAIL_TEXT_VALIDATION_NOT_MATCH_KEY})));
    }

    @Test
    public void validateCorrectFormatButDifferent() {

        //Given
        when(errors.hasErrors()).thenReturn(true);
        final EmailData emailData = new EmailData(FIRST_EMAIL_TEST, SECOND_CORRECT_EMAIL);// Email data for the test

        //When
        final ResponseEntity<?> result = controller.validateFields(emailData, errors); // Response from the tested method

        Set<ConstraintViolation<EmailData>> constraintViolations = null; // variable to evaluate the ConstraintViolations

        constraintViolations = validator.validate(emailData);

        //Then
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, result.getStatusCode());
        assertEquals(constraintViolations.size(), 1);

        assertEquals(0, countConstraintsWithMessages(constraintViolations, NEW_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY})));


        assertEquals(1, countConstraintsWithMessages(constraintViolations, CONFIRM_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_NOT_MATCH_KEY})));
    }


    @Test
    public void validateOnlyEmailName() {

        //Given
        when(errors.hasErrors()).thenReturn(true);
        final EmailData emailData = new EmailData(ONLY_NAME_EMAIL, ONLY_NAME_EMAIL);// Email data for the test

        //When
        final ResponseEntity<?> result = controller.validateFields(emailData, errors); // Response from the tested method

        Set<ConstraintViolation<EmailData>> constraintViolations = null; // variable to evaluate the ConstraintViolations

        constraintViolations = validator.validate(emailData);

        //Then
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, result.getStatusCode());
        assertEquals(constraintViolations.size(), 1);

        assertEquals(1, countConstraintsWithMessages(constraintViolations, NEW_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY})));


        assertEquals(0, countConstraintsWithMessages(constraintViolations, CONFIRM_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY})));
    }


    @Test
    public void validateEmailNameAndArrow() {

        //Given
        when(errors.hasErrors()).thenReturn(true);
        final EmailData emailData = new EmailData(NAME_AND_ARROW_EMAIL, NAME_AND_ARROW_EMAIL); // Email data for the test

        //When
        final ResponseEntity<?> result = controller.validateFields(emailData, errors); // Response from the tested method

        Set<ConstraintViolation<EmailData>> constraintViolations = null; // variable to evaluate the ConstraintViolations

        constraintViolations = validator.validate(emailData);

        //Then
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, result.getStatusCode());
        assertEquals(constraintViolations.size(), 1);

        assertEquals(1, countConstraintsWithMessages(constraintViolations, NEW_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY})));


        assertEquals(0, countConstraintsWithMessages(constraintViolations, CONFIRM_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY})));
    }


    @Test
    public void validateEmailNameArrowAndDomain() {

        //Given
        when(errors.hasErrors()).thenReturn(true);
        final EmailData emailData = new EmailData(NAME_ARROW_DOMAIN_EMAIL, NAME_ARROW_DOMAIN_EMAIL);// Email data for the test

        //When
        final ResponseEntity<?> result = controller.validateFields(emailData, errors); // Response from the tested method

        Set<ConstraintViolation<EmailData>> constraintViolations = null; // variable to evaluate the ConstraintViolations

        constraintViolations = validator.validate(emailData);

        //Then
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, result.getStatusCode());
        assertEquals(constraintViolations.size(), 1);

        assertEquals(1, countConstraintsWithMessages(constraintViolations, NEW_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY})));


        assertEquals(0, countConstraintsWithMessages(constraintViolations, CONFIRM_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY})));
    }

    @Test
    public void validateEmailNameArrowDomainAndType() {

        //Given
        when(errors.hasErrors()).thenReturn(true);
        final EmailData emailData = new EmailData(NAME_ARROW_TYPE_EMAIL, NAME_ARROW_TYPE_EMAIL);// Email data for the test

        //When
        final ResponseEntity<?> result = controller.validateFields(emailData, errors); // Response from the tested method

        Set<ConstraintViolation<EmailData>> constraintViolations = null; // Constraint validations set

        constraintViolations = validator.validate(emailData);

        //Then
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, result.getStatusCode());
        assertEquals(constraintViolations.size(), 1);

        assertEquals(1, countConstraintsWithMessages(constraintViolations, NEW_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY})));


        assertEquals(0, countConstraintsWithMessages(constraintViolations, CONFIRM_EMAIL_FIELD_NAME,
                Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY})));
    }

    @Test
    public void validateValidEmail() {

        //Given
        when(errors.hasErrors()).thenReturn(false);

        VALID_EMAILS.forEach(email -> {
            final EmailData emailData = new EmailData(email, email);// Email data for the test

            //When
            final ResponseEntity<?> result = controller.validateFields(emailData, errors); // Response from the tested method

            Set<ConstraintViolation<EmailData>> constraintViolations = null; // variable to evaluate the ConstraintViolations

            constraintViolations = validator.validate(emailData);

            //Then
            assertEquals(HttpStatus.OK, result.getStatusCode());
            assertEquals(constraintViolations.size(), 0);
        });

    }


    @Test
    public void validateInValidEmail() {

        //Given
        when(errors.hasErrors()).thenReturn(true);

        INVALID_EMAILS.forEach(email -> {
            final EmailData emailData = new EmailData(email, email);// Email data for the test

            //When
            final ResponseEntity<?> result = controller.validateFields(emailData, errors); // Response from the tested method

            Set<ConstraintViolation<EmailData>> constraintViolations = null; // variable to evaluate the ConstraintViolations

            constraintViolations = validator.validate(emailData);

            //Then
            assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, result.getStatusCode());
            assertEquals(constraintViolations.size(), 1);

            assertEquals(1, countConstraintsWithMessages(constraintViolations, NEW_EMAIL_FIELD_NAME,
                    Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY})));


            assertEquals(0, countConstraintsWithMessages(constraintViolations, CONFIRM_EMAIL_FIELD_NAME,
                    Arrays.asList(new String[]{EMAIL_TEXT_VALIDATION_INVALID_KEY,
                            EMAIL_TEXT_VALIDATION_NOT_MATCH_KEY})));
        });
    }

    /**
     * Count the Constraints With Messages according a list of lstMessages and field to match.
     *
     * @param constraintViolationSet set to iterate
     * @param strFieldName           name to compare
     * @param lstMessages            list of lstMessages to evaluate
     * @return the count of errors
     */
    private static int countConstraintsWithMessages(Set<ConstraintViolation<EmailData>> constraintViolationSet,
                                                    String strFieldName, List<String> lstMessages) {
        int result = 0; // Validation error counter

        final Iterator<ConstraintViolation<EmailData>> iterator = constraintViolationSet.iterator(); // The iterator set
        while (iterator.hasNext()) {

            final ConstraintViolation<EmailData> constraintViolation = iterator.next(); // The variable to control

            if (strFieldName.equals(constraintViolation.getPropertyPath().toString()) &&
                    lstMessages.contains(constraintViolation.getMessage())) {
                result++;
            }
        }
        return result;
    }
}
