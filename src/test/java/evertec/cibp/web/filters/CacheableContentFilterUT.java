package evertec.cibp.web.filters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;

import static org.mockito.Mockito.mock;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CacheableContentFilter.class})
public class CacheableContentFilterUT {
    private String[] directives = new String[]{"script-src 'self' 'unsafe-eval' 'unsafe-inline' " +
            "'unsafe-hashes' https://*.bancopopular.com https://ajax.googleapis.com " +
            "https://*.google-analytics.com " +
            "https://js-agent.newrelic.com https://bam.nr-data.net",
            "frame-ancestors 'self' https://*.bancopopular.com"
    };
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void whenLoggin_GivenCacheableContentFilter_ThenReturnDefaults() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = mock(FilterChain.class);
        CacheableContentFilter cacheableContentFilter = new CacheableContentFilter();
        FilterConfig config = mock(FilterConfig.class);
        cacheableContentFilter.init(config);

        cacheableContentFilter.doFilter(request, response, filterChain);

    }

}
