package evertec.cibp.web.filters;

import evertec.cibp.web.filters.writers.ReferrerPolicyWriter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import static junit.framework.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ReferrerPolicyWriter.class})
public class ReferrerPolicyWriterUT {
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenInvalidString_ParseReferrerPolicyTypeValue_ThenReturnException(){
        ReferrerPolicyWriter.ReferrerPolicyType.fromString("noreferrerwhendowngrade");
    }

    @Test
    public void givenAllValidValues_ParseReferencePolicyTypeValue_ThenReturnOk(){
        ReferrerPolicyWriter.ReferrerPolicyType.fromString("no-referrer");
        ReferrerPolicyWriter.ReferrerPolicyType.fromString("no-referrer-when-downgrade");
        ReferrerPolicyWriter.ReferrerPolicyType.fromString("origin");

        ReferrerPolicyWriter.ReferrerPolicyType.fromString("origin-when-cross-origin");
        ReferrerPolicyWriter.ReferrerPolicyType.fromString("same-origin");
        ReferrerPolicyWriter.ReferrerPolicyType.fromString("strict-origin-when-cross-origin");
        ReferrerPolicyWriter.ReferrerPolicyType.fromString("strict-origin");
        ReferrerPolicyWriter.ReferrerPolicyType.fromString("unsafe-url");
    }

    @Test
    public void givenNullValue_NotRequired_ThenReturnDefaults() throws Exception {
        ReferrerPolicyWriter writer = new ReferrerPolicyWriter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        writer.writeHeaders(request, response);
        assertEquals(response.getHeaderNames().size(), 1);
        assertEquals(response.getHeader("Referrer-Policy"), "no-referrer-when-downgrade");
    }

    @Test
    public void givenNoReferrerWhenDowngradeValue_Parse_ThenReturnDefaults() throws Exception {
        ReferrerPolicyWriter writer = new ReferrerPolicyWriter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        writer.writeHeaders(request, response);
        assertEquals(response.getHeaderNames().size(), 1);
        assertEquals(response.getHeader("Referrer-Policy"), "no-referrer-when-downgrade");
    }

    @Test(expected = IllegalArgumentException.class)
    public void giveInvalidValueWriter_Parse_ThenReturnIllegalArgument() throws Exception {
        ReferrerPolicyWriter writer = new ReferrerPolicyWriter("invalid-value-referrer");
    }

    @Test
    public void giveValidWriterConstructor_Parse_ThenReturnOk(){
        new ReferrerPolicyWriter("no-referrer");
        new ReferrerPolicyWriter("no-referrer-when-downgrade");
        new ReferrerPolicyWriter("origin");

        new ReferrerPolicyWriter("origin-when-cross-origin");
        new ReferrerPolicyWriter("same-origin");
        new ReferrerPolicyWriter("strict-origin-when-cross-origin");
        new ReferrerPolicyWriter("strict-origin");
        new ReferrerPolicyWriter("unsafe-url");
    }


}
