package evertec.cibp.web.filters;

import evertec.cibp.web.filters.writers.ReferrerPolicyWriter;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SecurityFiltersChain.class})
public class SecurityFiltersChainUT {
    private String[] directives = new String[]{"script-src 'self' 'unsafe-eval' 'unsafe-inline' " +
            "'unsafe-hashes' https://*.bancopopular.com https://ajax.googleapis.com " +
            "https://*.google-analytics.com " +
            "https://js-agent.newrelic.com https://bam.nr-data.net",
            "frame-ancestors 'self' https://*.bancopopular.com"
    };

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void givenDefaults_OnlyRequired_ThenReturnDefaults() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = mock(FilterChain.class);
        SecurityFiltersChain securityFiltersChain = new SecurityFiltersChain();
        FilterConfig config = mock(FilterConfig.class);
        when(config.getInitParameter(SecurityFiltersChain.CONTENT_SECURITY_POLICY_PARAM)).thenReturn(String.join(";", directives));
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM)).thenReturn("31536000");
        securityFiltersChain.init(config);

        securityFiltersChain.doFilter(request, response, filterChain);
        assertEquals(response.getHeaderNames().size(), 6);
        assertEquals(response.getHeader(XFrameOptionsHeaderWriter.XFRAME_OPTIONS_HEADER), "DENY");
        assertEquals(response.getHeader("X-Content-Type-Options"), "nosniff");
        assertEquals(response.getHeader("X-XSS-Protection"), "1; mode=block");
        assertEquals(response.getHeader("Strict-Transport-Security"), "max-age=31536000 ; includeSubDomains");
        assertEquals(response.getHeader("Content-Security-Policy"), String.join(";", directives));
        assertEquals(response.getHeader("Referrer-Policy"), "no-referrer-when-downgrade");
    }

    @Test
    public void givenDefaults_HasBeenChanged_ThenReturnModifiedVersion() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = mock(FilterChain.class);
        SecurityFiltersChain securityFiltersChain = new SecurityFiltersChain();
        FilterConfig config = mock(FilterConfig.class);
        when(config.getInitParameter(SecurityFiltersChain.CONTENT_SECURITY_POLICY_PARAM)).thenReturn(String.join(";", directives));
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM)).thenReturn("31536000");
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_SUBDOMAINS_PARAM)).thenReturn("true");
        securityFiltersChain.init(config);

        securityFiltersChain.doFilter(request, response, filterChain);
        assertEquals(response.getHeaderNames().size(), 6);
        assertEquals(response.getHeader(XFrameOptionsHeaderWriter.XFRAME_OPTIONS_HEADER), "DENY");
        assertEquals(response.getHeader("X-Content-Type-Options"), "nosniff");
        assertEquals(response.getHeader("X-XSS-Protection"), "1; mode=block");
        assertEquals(response.getHeader("Strict-Transport-Security"), "max-age=31536000 ; includeSubDomains");
        assertEquals(response.getHeader("Content-Security-Policy"), String.join(";", directives));
    }
    @Test
    public void givenXFrameOptions_SAMEORIGIN_ThenReturnSAMEORIGIN() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = mock(FilterChain.class);
        SecurityFiltersChain securityFiltersChain = new SecurityFiltersChain();
        FilterConfig config = mock(FilterConfig.class);

        when(config.getInitParameter(SecurityFiltersChain.CONTENT_SECURITY_POLICY_PARAM)).thenReturn(String.join(";", directives));
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM)).thenReturn("31536000");
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_SUBDOMAINS_PARAM)).thenReturn("false");
        when(config.getInitParameter(SecurityFiltersChain.XFRAME_OPTIONS_MODE_PARAM)).thenReturn("SAMEORIGIN");
        securityFiltersChain.init(config);

        securityFiltersChain.doFilter(request, response, filterChain);
        assertEquals(response.getHeaderNames().size(), 6);
        assertEquals(response.getHeader(XFrameOptionsHeaderWriter.XFRAME_OPTIONS_HEADER), "SAMEORIGIN");
    }

    @Test
    public void givenXFrameOptions_DENY_ThenReturnDENY() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = mock(FilterChain.class);
        SecurityFiltersChain securityFiltersChain = new SecurityFiltersChain();
        FilterConfig config = mock(FilterConfig.class);

        when(config.getInitParameter(SecurityFiltersChain.CONTENT_SECURITY_POLICY_PARAM)).thenReturn(String.join(";", directives));
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM)).thenReturn("31536000");
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_SUBDOMAINS_PARAM)).thenReturn("false");
        when(config.getInitParameter(SecurityFiltersChain.XFRAME_OPTIONS_MODE_PARAM)).thenReturn("DENY");
        securityFiltersChain.init(config);

        securityFiltersChain.doFilter(request, response, filterChain);
        assertEquals(response.getHeaderNames().size(), 6);
        assertEquals(response.getHeader(XFrameOptionsHeaderWriter.XFRAME_OPTIONS_HEADER), "DENY");
    }

    @Test
    public void givenContentSecurity_Valid_ThenReturnOk() throws ServletException {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = mock(FilterChain.class);
        SecurityFiltersChain securityFiltersChain = new SecurityFiltersChain();
        FilterConfig config = mock(FilterConfig.class);

        when(config.getInitParameter(SecurityFiltersChain.CONTENT_SECURITY_POLICY_PARAM)).thenReturn(String.join(";", directives));
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM)).thenReturn("31536000");
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_SUBDOMAINS_PARAM)).thenReturn("false");
        when(config.getInitParameter(SecurityFiltersChain.XFRAME_OPTIONS_MODE_PARAM)).thenReturn("SAMEORIGIN");
        securityFiltersChain.init(config);
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenContentSecurity_Invalid_ThenReturnError() throws ServletException {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = mock(FilterChain.class);
        SecurityFiltersChain securityFiltersChain = new SecurityFiltersChain();
        FilterConfig config = mock(FilterConfig.class);
        //Test the invalid directive
        String[] invalidDirective = new String[]{"script-src 'self' https://*.bancopopular.com","frame-ancestros 'self' https://*.bancopopular.com"};

        when(config.getInitParameter(SecurityFiltersChain.CONTENT_SECURITY_POLICY_PARAM)).thenReturn(String.join(";", invalidDirective));
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM)).thenReturn("31536000");
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_SUBDOMAINS_PARAM)).thenReturn("false");
        when(config.getInitParameter(SecurityFiltersChain.XFRAME_OPTIONS_MODE_PARAM)).thenReturn("SAMEORIGIN");
        securityFiltersChain.init(config);
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenContentSecurity_InvalidKeywords_ThenReturnError() throws ServletException {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = mock(FilterChain.class);
        SecurityFiltersChain securityFiltersChain = new SecurityFiltersChain();
        FilterConfig config = mock(FilterConfig.class);
        String[] invalidDirective = new String[]{"script-src 'self' 'pepe' https://*.bancopopular.com"};

        when(config.getInitParameter(SecurityFiltersChain.CONTENT_SECURITY_POLICY_PARAM)).thenReturn(String.join(";", invalidDirective));
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM)).thenReturn("31536000");
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_SUBDOMAINS_PARAM)).thenReturn("false");
        when(config.getInitParameter(SecurityFiltersChain.XFRAME_OPTIONS_MODE_PARAM)).thenReturn("SAMEORIGIN");
        securityFiltersChain.init(config);
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenContentSecurity_InvalidWebConfig_ThenReturnError() throws ServletException {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = mock(FilterChain.class);
        SecurityFiltersChain securityFiltersChain = new SecurityFiltersChain();
        FilterConfig config = mock(FilterConfig.class);
        String[] invalidDirective = new String[]{"script-src 'self' 'unsafe-eval' 'unsafe-inline' 'unsafe-hashes' https://*.bancopopular.com https://ajax.googleapis.com https://*.google-analytics.com https://js-agent.newrelic.com https://bam.nr-data.net",
                " frame-ancestors 'self' https://*.bancopopular.com"};

        when(config.getInitParameter(SecurityFiltersChain.CONTENT_SECURITY_POLICY_PARAM)).thenReturn(String.join(";", invalidDirective));
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM)).thenReturn("31536000");
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_SUBDOMAINS_PARAM)).thenReturn("false");
        when(config.getInitParameter(SecurityFiltersChain.XFRAME_OPTIONS_MODE_PARAM)).thenReturn("SAMEORIGIN");
        securityFiltersChain.init(config);
    }
    @Test(expected = IllegalArgumentException.class)
    public void givenContentSecurity_InvalidWebConfigSemicolon_ThenReturnError() throws ServletException {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = mock(FilterChain.class);
        SecurityFiltersChain securityFiltersChain = new SecurityFiltersChain();
        FilterConfig config = mock(FilterConfig.class);
        String invalidDirective = "script-src 'self' 'unsafe-eval' 'unsafe-inline' 'unsafe-hashes' " +
                "https://*.bancopopular.com https://ajax.googleapis.com " +
                "https://*.google-analytics.com https://js-agent.newrelic.com https://bam.nr-data.net " +
                "frame-ancestors 'self' https://*.bancopopular.comm";

        when(config.getInitParameter(SecurityFiltersChain.CONTENT_SECURITY_POLICY_PARAM)).thenReturn(invalidDirective);
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM)).thenReturn("31536000");
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_SUBDOMAINS_PARAM)).thenReturn("false");
        when(config.getInitParameter(SecurityFiltersChain.XFRAME_OPTIONS_MODE_PARAM)).thenReturn("SAMEORIGIN");
        securityFiltersChain.init(config);
    }

    @Test
    public void givenReferrerPolicy_NoWebConfig_ThenReturnDefaults() throws Exception {
        ReferrerPolicyWriter writer = new ReferrerPolicyWriter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        writer.writeHeaders(request, response);
        assertEquals(response.getHeaderNames().size(), 1);
        assertEquals(response.getHeader("Referrer-Policy"), "no-referrer-when-downgrade");
    }

    @Test(expected = IllegalArgumentException.class)
    @Ignore
    public void givenReferrerPolicyInvalidWebConfig_ThenReturnError() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        HttpSecurity httpSecurity = mock(HttpSecurity.class);
        FilterChain filterChain = mock(FilterChain.class);
        SecurityFiltersChain securityFiltersChain = new SecurityFiltersChain();
        FilterConfig config = mock(FilterConfig.class);

        //Required params
        when(config.getInitParameter(SecurityFiltersChain.CONTENT_SECURITY_POLICY_PARAM)).thenReturn(String.join(";",this.directives));
        when(config.getInitParameter(SecurityFiltersChain.STRICT_TRANSPORT_SECURITY_MAX_AGE_PARAM)).thenReturn("31536000");
        //Force error
        when(config.getInitParameter(SecurityFiltersChain.REFERRER_POLICY_PARAM)).thenReturn("noreferrerwhendowngrade");
        securityFiltersChain.init(config);
    }
}
