package evertec.cibp.web;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.*;

import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.models.base.SimpleTransaction;
import evertec.cibp.core.services.responses.TransactionsResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import bppr.tv.beans.transactions.Transaction;

import evertec.cibp.core.services.requests.TransactionRequest;


/**
 * IT WebAppController java class.
 * @author jOrtiz
 * @since 21-07-2021
 * @version 1.0
 */
@SuppressWarnings("deprecation")
@RunWith(PowerMockRunner.class)
@PrepareForTest({TransactionRequest.class,TVAccountProductDefinition.class,
        TransactionsResponse.class,WebUtils.class,
        Utils.class, CustomerServices.class,
        StaticMessageSource.class, CustomerFacade.class})
public class WebAppControllerIT extends AbstractModelAndViewTests  {

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @Mock
    private UserSession userSession;

    @Mock
    private CustomerServices customerService;


    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private CustomerFacade customerFacade;


    @Mock
    private TVFrontendAccount tvFrontendAccount;

    @Mock
    private TransactionsResponse transactionResponse;

    @Mock
    private TransactionRequest transactionRequest;

    @Mock
    private TVAccountProductDefinition tvAccountProductDefinition;

    @InjectMocks
    private WebAppController controller;

    @Before
    public void setUp() throws Exception {

        controller = PowerMockito.spy(new WebAppController());
        MockitoAnnotations.initMocks(this);
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Utils.class,CALLS_REAL_METHODS);
        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(CustomerFacade.class);
        PowerMockito.mockStatic(TVAccountProductDefinition.class);
        PowerMockito.mockStatic(TransactionRequest.class);

        transactionResponse = mock(TransactionsResponse.class);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(CustomerFacade.getInstance()).thenReturn(customerFacade);
    }

    @Test
    public void whenHasTransAddScreen_thenReturnTrue() throws Exception {
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional = PowerMockito.mock(Optional.class);

        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        tran.setMemo("PURCHASE juan goban ON 05/08/21 guaynabo pr");
        abstransac.setDescription("PURCHASE juan goban ON 05/08/21 guaynabo pr");

        abstransac.setBackendObject(tran);

        abstractTransactions.add(abstransac);

        transOptional = Optional.of(abstransac);
        request.setParameter("account", "1232313123");
        request.setParameter("trace", "123231312344545");


        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");

        List<AbstractTransaction> transactions = PowerMockito.mock(List.class);

        tvFrontendAccount.setFrontEndId("1232313123");
        when(tvFrontendAccount.getProductDefinition()).thenReturn(tvAccountProductDefinition);
        when(tvAccountProductDefinition.getProductType()).thenReturn("CCA");

        when(customerService.getDepositCurrentTransactions(any(TransactionRequest.class))).thenReturn(transactionResponse);
        when(customerService.getCreditCardCurrentTransactions(any(TransactionRequest.class))).thenReturn(transactionResponse);

        when(transactionResponse.getTransactions()).thenReturn(abstractTransactions);
        ModelAndView model = controller.transAddScreen(request, response);

        PowerMockito.verifyStatic(times(1));
        Utils.getTransactionsCCAOrIDA(any(), any(), any());

        PowerMockito.verifyStatic(times(1));
        Utils.getTransactionByTraceId(any(), any());
    }

    @Test
    public void whenHasTransAddScreenPaginate_thenReturnTrue() throws Exception {//yerme
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional = PowerMockito.mock(Optional.class);
        SimpleTransaction abstransac = new SimpleTransaction();
        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        tran.setMemo("PURCHASE juan goban ON 05/08/21 guaynabo pr");
        abstransac.setDescription("PURCHASE juan goban ON 05/08/21 guaynabo pr");
        request.setParameter("account", "1232313123");
        request.setParameter("trace", "123231312344545");
        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getProductDefinition()).thenReturn(tvAccountProductDefinition);
        when(tvAccountProductDefinition.getProductType()).thenReturn("CCA");
        abstransac.setBackendObject(tran);
        abstractTransactions.add(abstransac);
        transOptional = Optional.of(abstransac);
        when(transactionResponse.getPagesAvailable()).thenReturn(2);
        when(customerService.getDepositCurrentTransactions(any(TransactionRequest.class))).thenReturn(transactionResponse);
        when(customerService.getCreditCardCurrentTransactions(any(TransactionRequest.class))).thenReturn(transactionResponse);
        ModelAndView model = controller.transAddScreen(request, response);
    }

    @Test
    public void whenHasTransAddScreenPaginateSecondPage_thenReturnTrue() throws Exception {//yerme
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional = PowerMockito.mock(Optional.class);
        SimpleTransaction abstransac = new SimpleTransaction();
        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        tran.setMemo("PURCHASE juan goban ON 05/08/21 guaynabo pr");
        abstransac.setDescription("PURCHASE juan goban ON 05/08/21 guaynabo pr");
        request.setParameter("account", "1232313123");
        request.setParameter("trace", "123231312344545");
        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getProductDefinition()).thenReturn(tvAccountProductDefinition);
        when(tvAccountProductDefinition.getProductType()).thenReturn("CCA");
        abstransac.setBackendObject(tran);
        abstractTransactions.add(abstransac);
        transOptional = Optional.of(abstransac);
        when(transactionResponse.getPagesAvailable()).thenReturn(2);
        when(customerService.getDepositCurrentTransactions(any(TransactionRequest.class))).thenReturn(transactionResponse);
        TransactionsResponse tResp = new TransactionsResponse();
        SimpleTransaction st = new SimpleTransaction();
        Transaction tr = new Transaction();
        tr.setTraceId("123231312344545");
        st.setBackendObject(tr);
        st.setDescription("PURCHASE");
        tResp.setTransactions(Collections.singletonList(st));
        when(customerService.getCreditCardCurrentTransactions(any(TransactionRequest.class))).thenReturn(transactionResponse).thenReturn(tResp);
        ModelAndView model = controller.transAddScreen(request, response);
    }

    @Test
    public void whenHasTransAddScreen_givenPurchaseWithoutLocation_thenReturnTrue() throws Exception {
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional = PowerMockito.mock(Optional.class);

        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        tran.setMemo("PURCHASE juan goban ON 05/08/21");
        abstransac.setDescription("PURCHASE juan goban ON 05/08/21");

        abstransac.setBackendObject(tran);

        abstractTransactions.add(abstransac);

        transOptional = Optional.of(abstransac);
        request.setParameter("account", "1232313123");
        request.setParameter("trace", "123231312344545");


        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");

        tvFrontendAccount.setFrontEndId("1232313123");
        when(tvFrontendAccount.getProductDefinition()).thenReturn(tvAccountProductDefinition);
        when(tvAccountProductDefinition.getProductType()).thenReturn("IDA");

        when(customerService.getDepositCurrentTransactions(any(TransactionRequest.class))).thenReturn(transactionResponse);
        when(customerService.getCreditCardCurrentTransactions(any(TransactionRequest.class))).thenReturn(transactionResponse);

        when(transactionResponse.getTransactions()).thenReturn(abstractTransactions);
        ModelAndView model = controller.transAddScreen(request, response);

        PowerMockito.verifyStatic(times(1));
        Utils.getTransactionsCCAOrIDA(any(), any(), any());

        PowerMockito.verifyStatic(times(1));
        Utils.getTransactionByTraceId(any(), any());
        
        assertTrue(model.getModel().get("location").equals(""));
    }
    
    @Test
    public void whenHasTransAddScreen_givenATHWithdrawalWithoutLocation_thenReturnTrue() throws Exception {
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional = PowerMockito.mock(Optional.class);

        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        tran.setMemo("ATH WITHDRAWAL 3695 BPPR BAYAMON ON 11/30/21");
        abstransac.setDescription("ATH WITHDRAWAL 3695 BPPR BAYAMON ON 11/30/21");

        abstransac.setBackendObject(tran);

        abstractTransactions.add(abstransac);

        transOptional = Optional.of(abstransac);
        request.setParameter("account", "1232313123");
        request.setParameter("trace", "123231312344545");


        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");

        tvFrontendAccount.setFrontEndId("1232313123");
        when(tvFrontendAccount.getProductDefinition()).thenReturn(tvAccountProductDefinition);
        when(tvAccountProductDefinition.getProductType()).thenReturn("IDA");

        when(customerService.getDepositCurrentTransactions(any(TransactionRequest.class))).thenReturn(transactionResponse);
        when(customerService.getCreditCardCurrentTransactions(any(TransactionRequest.class))).thenReturn(transactionResponse);

        when(transactionResponse.getTransactions()).thenReturn(abstractTransactions);
        ModelAndView model = controller.transAddScreen(request, response);

        PowerMockito.verifyStatic(times(1));
        Utils.getTransactionsCCAOrIDA(any(), any(), any());

        PowerMockito.verifyStatic(times(1));
        Utils.getTransactionByTraceId(any(), any());
        
        assertTrue(model.getModel().get("location").equals(""));
    }
    
    @Test
    public void whenHasTransAddScreen_givenATHWithdrawalWithLocation_thenReturnTrue() throws Exception {
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional = PowerMockito.mock(Optional.class);

        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        tran.setMemo("ATH WITHDRAWAL 3695 BPPR BAYAMON ON 11/30/21 BAYAMON PR");
        abstransac.setDescription("ATH WITHDRAWAL 3695 BPPR BAYAMON ON 11/30/21 BAYAMON PR");

        abstransac.setBackendObject(tran);

        abstractTransactions.add(abstransac);

        transOptional = Optional.of(abstransac);
        request.setParameter("account", "1232313123");
        request.setParameter("trace", "123231312344545");


        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");

        tvFrontendAccount.setFrontEndId("1232313123");
        when(tvFrontendAccount.getProductDefinition()).thenReturn(tvAccountProductDefinition);
        when(tvAccountProductDefinition.getProductType()).thenReturn("IDA");

        when(customerService.getDepositCurrentTransactions(any(TransactionRequest.class))).thenReturn(transactionResponse);
        when(customerService.getCreditCardCurrentTransactions(any(TransactionRequest.class))).thenReturn(transactionResponse);

        when(transactionResponse.getTransactions()).thenReturn(abstractTransactions);
        ModelAndView model = controller.transAddScreen(request, response);

        PowerMockito.verifyStatic(times(1));
        Utils.getTransactionsCCAOrIDA(any(), any(), any());

        PowerMockito.verifyStatic(times(1));
        Utils.getTransactionByTraceId(any(), any());
        
        assertTrue(model.getModel().get("location").equals("BAYAMON PR"));
    }
  
}