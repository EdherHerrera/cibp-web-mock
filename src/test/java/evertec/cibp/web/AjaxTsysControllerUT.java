package evertec.cibp.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TsysLoyaltyRewardsInfo;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.index.globalpayee.GlobalPayeeIndexBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ WebUtils.class, Utils.class})
public class AjaxTsysControllerUT extends AbstractModelAndViewTests {

	private HttpServletResponse response;

	@Mock
	private GlobalPayeeIndexBean globalPayeeIndexBean;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private HttpServletRequest mockRequest;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private TVFrontendAccount ccaAccountByFrontEndId;
	
	@Mock
	private List<TVFrontendAccount> listCcaAccountByFrontEndId;
	
	@Mock
	private CustomerInteractionEvent eventLog;
	
	@Mock
	private TsysLoyaltyRewardsInfo tsysLoyaltyRewardsInfo;

	@InjectMocks
	private AjaxTsysController ajaxTsysController;
	
	@Before
	public void setUp() throws Exception {
			
		response = new MockHttpServletResponse();		
		userSession = PowerMockito.mock(UserSession.class);
		customerService = PowerMockito.mock(CustomerServices.class);
		mockRequest = PowerMockito.mock(HttpServletRequest.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Utils.class);
		// setup ajaxTsysController 
		when(eventLog.getInteraction()).thenReturn(CustomerInteractionEventType.PREMIATOKEN_SSO_SUCCESS);
		AjaxTsysController.setCustomerInteractionEventBuilder(req -> eventLog);
		ajaxTsysController.setCustomerService(customerService);
		String[] premiaAmexZero = {"UAREG","UVNOV","APREG","UVVSI","UVREG","UVNOV"};
		ajaxTsysController.setPremiaAmexZero(premiaAmexZero);
		when(mockRequest.getHeader("User-Agent")).thenReturn("");
	}

	/**
     * When tsysLoyaltyRewardsRedirection
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsRedirectionRetail_ThenReturnModelAndView() throws Exception {
		
		when(mockRequest.getParameter("cardNumber")).thenReturn("0123456789");
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getEmail()).thenReturn("email@evertecinc.com");
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(userSession.getCustomerService()).thenReturn(customerService);		
		when(Utils.getAccountById(any(), any())).thenReturn(ccaAccountByFrontEndId);
		when(ccaAccountByFrontEndId.getAccountNumber()).thenReturn("0549542114539357");
		when(ccaAccountByFrontEndId.getAccountProductId()).thenReturn("UAREG");
		ajaxTsysController.setPremiaCatalogURL("http://development.premia.com/ssoClient/login.htm");
		ajaxTsysController.setSourceUrl("www.mibanco.com");
		when(customerService.generatePremiaToken("0123456789", "www.mibanco.com", "email@evertecinc.com", false)).thenReturn("34567854efdbnhmj55rfgy43ref");
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsRedirection(mockRequest, response);

		assertViewName(modelAndView, "tsysLoyaltyRewardsRedirection");
	}
	
	/**
     * When tsysLoyaltyRewardsRedirection
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsRedirectionCommercial_ThenReturnModelAndView() throws Exception {
		
		when(mockRequest.getParameter("cardNumber")).thenReturn("0123456789");
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getEmail()).thenReturn("email@evertecinc.com");
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.COMMERCIAL);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(Utils.getAccountById(any(), any())).thenReturn(ccaAccountByFrontEndId);
		when(ccaAccountByFrontEndId.getAccountNumber()).thenReturn("4549542114539357");
		when(ccaAccountByFrontEndId.getAccountProductId()).thenReturn("UAREG");
		ajaxTsysController.setPremiaCatalogURL("http://development.premia.com/ssoClient/login.htm");
		ajaxTsysController.setSourceUrl("www.mibanco.com");
		when(customerService.generatePremiaToken("0123456789", "www.mibanco.com", "email@evertecinc.com", false)).thenReturn("34567854efdbnhmj55rfgy43ref");
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsRedirection(mockRequest, response);

		assertViewName(modelAndView, "tsysLoyaltyRewardsRedirection");
	}
	
	/**
     * When tsysLoyaltyRewardsRedirection
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsRedirectionRetailSession_ThenReturnModelAndView() throws Exception {
		
		when(mockRequest.getParameter("cardNumber")).thenReturn("0123456789");
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(null);
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsRedirection(mockRequest, response);

		assertViewName(modelAndView, "tsysLoyaltyRewardsRedirection");
	}
	
	/**
	 * When tsysLoyaltyRewardsRedirection failed to get tsys URL
	 * Given valid parameters
	 * Then set PREMIATOKEN_SSO_FAIL
	 * @throws IOException 
	 * @throws Exception 
	 */
	@Test
	public void whenTsysLoyaltyRewardsRedirectionFailed_ThenSetPREMIATOKEN_SSO_FAIL() throws Exception {
		try {
			doNothing().when(eventLog).setInteraction(CustomerInteractionEventType.PREMIATOKEN_SSO_FAIL);
			when(eventLog.getInteraction()).thenReturn(CustomerInteractionEventType.PREMIATOKEN_SSO_FAIL);
			when(mockRequest.getParameter("cardNumber")).thenReturn("0123456789");
			when(mockRequest.getHeader("User-Agent")).thenReturn(null);
			when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
			when(userSession.getCustomerProfile()).thenReturn(customerProfile);
			when(customerProfile.getEmail()).thenReturn("email@evertecinc.com");
			when(customerProfile.getUserInterface()).thenReturn(UserInterface.COMMERCIAL);
			when(userSession.getCustomerService()).thenReturn(customerService);
			when(Utils.getAccountById(any(), any())).thenReturn(ccaAccountByFrontEndId);
			when(ccaAccountByFrontEndId.getAccountNumber()).thenReturn("4549542114539357");
			when(ccaAccountByFrontEndId.getAccountProductId()).thenReturn("UAREG");
			ajaxTsysController.setPremiaCatalogURL("http://development.premia.com/ssoClient/login.htm");
			ajaxTsysController.setSourceUrl("www.mibanco.com");
			when(customerService.generatePremiaToken("0123456789", "www.mibanco.com", "email@evertecinc.com", false)).thenReturn("34567854efdbnhmj55rfgy43ref");
			ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsRedirection(mockRequest, response);

			assertViewName(modelAndView, "tsysLoyaltyRewardsRedirection");
		} catch (Exception ex) {
			verify(eventLog, times(1)).setInteraction(CustomerInteractionEventType.PREMIATOKEN_SSO_FAIL);
		}
	}
	
	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsAccountInfoListAccountNull_ThenReturnModelAndView() throws Exception {
		listCcaAccountByFrontEndId = getDummyPremiaAccountList();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getPremiaAccountList()).thenReturn(listCcaAccountByFrontEndId);
		when(userSession.isHasPremia()).thenReturn(false);
		
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);

		assertNull(modelAndView.getModelMap().get("tsysBalanceRewards"));
		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");
	}
	
	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsAccountInfoHasPremia_ThenReturnModelAndView() throws Exception {
		listCcaAccountByFrontEndId = getDummyPremiaAccountList();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getPremiaAccountList()).thenReturn(listCcaAccountByFrontEndId);
		when(listCcaAccountByFrontEndId.get(0).isPrimaryAccount()).thenReturn(true);
		when(userSession.isHasPremia()).thenReturn(false);
		
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);

		assertNull(modelAndView.getModelMap().get("tsysBalanceRewards"));
		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");
	}
	
	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsAccountInfoListAccountSize1_ThenReturnModelAndView() throws Exception {
		listCcaAccountByFrontEndId = getDummyPremiaAccountList();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getPremiaAccountList()).thenReturn(listCcaAccountByFrontEndId);
		when(listCcaAccountByFrontEndId.get(0).isPrimaryAccount()).thenReturn(true);
		when(userSession.isHasPremia()).thenReturn(true);
		
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);

		assertNull(modelAndView.getModelMap().get("tsysBalanceRewards"));
		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");
	}
	
	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsAccountInfoAccountPrimaryFalse_ThenReturnModelAndView() throws Exception {
		listCcaAccountByFrontEndId = getDummyPremiaAccountList();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getPremiaAccountList()).thenReturn(listCcaAccountByFrontEndId);
		when(userSession.isHasPremia()).thenReturn(true);
		
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);

		assertNull(modelAndView.getModelMap().get("tsysBalanceRewards"));
		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");
	}
	
	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsAccountInfoAccountPrimaryTrue_ThenReturnModelAndView() throws Exception {
		listCcaAccountByFrontEndId = getDummyPremiaAccountList();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getPremiaAccountList()).thenReturn(listCcaAccountByFrontEndId);
		when(listCcaAccountByFrontEndId.get(0).isPrimaryAccount()).thenReturn(true);
		when(userSession.isHasPremia()).thenReturn(true);
		
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);

		assertNull(modelAndView.getModelMap().get("tsysBalanceRewards"));
		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");
	}
	
	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsAccountInfo_ConsultRewardsPointsTrue_ThenReturnModelAndView() throws Exception {
		listCcaAccountByFrontEndId = getDummyPremiaAccountList();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getPremiaAccountList()).thenReturn(listCcaAccountByFrontEndId);
		when(listCcaAccountByFrontEndId.get(0).isPrimaryAccount()).thenReturn(true);
		when(userSession.isHasPremia()).thenReturn(true);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.isConsultRewardsPoints()).thenReturn(true);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(ccaAccountByFrontEndId);
		when(ccaAccountByFrontEndId.getAccountProductId()).thenReturn("UAREG");
		when(ccaAccountByFrontEndId.getAccountNumber()).thenReturn("0345891534161455");
		
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);

		assertNull(modelAndView.getModelMap().get("tsysBalanceRewards"));
		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");
	}
	
	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsAccountInfo_PremiaAmexZero_ThenReturnModelAndView() throws Exception {
		ajaxTsysController.setPremiaAmexZero(null);
		listCcaAccountByFrontEndId = getDummyPremiaAccountList();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getPremiaAccountList()).thenReturn(listCcaAccountByFrontEndId);
		when(listCcaAccountByFrontEndId.get(0).isPrimaryAccount()).thenReturn(true);
		when(userSession.isHasPremia()).thenReturn(true);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.isConsultRewardsPoints()).thenReturn(true);
		when(ccaAccountByFrontEndId.getAccountProductId()).thenReturn("UAREG");
		when(ccaAccountByFrontEndId.getAccountNumber()).thenReturn("0000");
		
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);

		assertNull(modelAndView.getModelMap().get("tsysBalanceRewards"));
		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");
	}

	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsAccountInfo_ConsultRewardsPointsFalse_ThenReturnModelAndView() throws Exception {
		listCcaAccountByFrontEndId = getDummyPremiaAccountList();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getPremiaAccountList()).thenReturn(listCcaAccountByFrontEndId);
		when(listCcaAccountByFrontEndId.get(0).isPrimaryAccount()).thenReturn(true);
		when(userSession.isHasPremia()).thenReturn(true);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.isConsultRewardsPoints()).thenReturn(false);
		
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);

		assertNull(modelAndView.getModelMap().get("tsysBalanceRewards"));
		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");
	}
	
	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsAccountInfoListTVFrontendAccountNull_ThenReturnModelAndView() throws Exception {
		listCcaAccountByFrontEndId = getDummyPremiaAccountList();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getPremiaAccountList()).thenReturn(listCcaAccountByFrontEndId);
		when(listCcaAccountByFrontEndId.get(0).isPrimaryAccount()).thenReturn(true);
		when(userSession.isHasPremia()).thenReturn(true);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.isConsultRewardsPoints()).thenReturn(false);
		when(userSession.getListRewardPoints()).thenReturn(null);
		
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);
		
		assertNull(modelAndView.getModelMap().get("tsysBalanceRewards"));
		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");

	}
	
	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsAccountInfo_ThenReturnModelAndView() throws Exception {
		listCcaAccountByFrontEndId = getDummyPremiaAccountList();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getPremiaAccountList()).thenReturn(listCcaAccountByFrontEndId);
		when(listCcaAccountByFrontEndId.get(0).isPrimaryAccount()).thenReturn(true);
		when(userSession.isHasPremia()).thenReturn(true);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.isConsultRewardsPoints()).thenReturn(false);
		when(userSession.getListRewardPoints()).thenReturn(null);
		
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);

		assertTrue(modelAndView.getModelMap().isEmpty());
		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");
	}
	
	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsAccountInfoUseSessionNull_ThenReturnModelAndView() throws Exception {
		listCcaAccountByFrontEndId = getDummyPremiaAccountList();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(null);
				
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);

		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");
	}
	
	/**
	 * Get a list of dummy ghost accounts.
	 * @return
	 */
	
	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void WhentsysLoyaltyRewardsAccountInfo_GivenPremiaAccountsIsEmpty_ThenReturn() throws Exception {
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.isHasPremia()).thenReturn(true);
		when(userSession.getPremiaAccountList()).thenReturn(new ArrayList<>());
		
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);

		assertTrue(modelAndView.getModelMap().isEmpty());
		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");
	}
	
	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void WhentsysLoyaltyRewardsAccountInfo_GivenPremiaAccountsIsNull_ThenReturn() throws Exception {
		listCcaAccountByFrontEndId = getDummyPremiaAccountList();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getPremiaAccountList()).thenReturn(listCcaAccountByFrontEndId);
		when(listCcaAccountByFrontEndId.get(0).isPrimaryAccount()).thenReturn(true);
		when(userSession.isHasPremia()).thenReturn(true);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.isConsultRewardsPoints()).thenReturn(false);
		when(userSession.getListRewardPoints()).thenReturn(listCcaAccountByFrontEndId);
		when(listCcaAccountByFrontEndId.get(0).getTsysLoyaltyRewardsInfo()).thenReturn(tsysLoyaltyRewardsInfo);
		when(tsysLoyaltyRewardsInfo.getAvailableBalanceDouble()).thenReturn(0D);
		
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);

		//assertTrue(modelAndView.getModelMap().isEmpty()); //tsysBalanceRewards
		assertNotNull(modelAndView.getModelMap().get("tsysBalanceRewards"));
		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");
	}		
	
	/**
	 * Get a list of dummy ghost accounts.
	 * @return
	 */
	private List<TVFrontendAccount> getDummyPremiaAccountList() {

		TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("CCA", "4549548381926524", "R"));
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription",
				"dummyAccountNumberDescription",
				"dummyAccountNumber",
				"VPPRE",
				"CCA",
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		acc.setPrimaryAccount(true);
		when(acc.getAccountProductId()).thenReturn("VPPRE");
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);

		return accList;
	}
}
	

