package evertec.cibp.web.form;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ContactUsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Unit testing ContactUsForm java class.
 * @author Evertec ET58344
 * @since 09-18-2020
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CustomerFacade.class, StaticMessageSource.class, ContactUsForm.class, Utils.class})
public class ContactUsFormUT extends AbstractModelAndViewTests  {

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private UserSession userSession;

	@Mock
	private ContactUsBean bean;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private BindException error;

	@Mock
	private TVFrontendAccount tvFrontendAccount;

	@Mock
	private Map<String, String> departmentList;

	@Mock
	private CustomerServices customerService;

	@InjectMocks
	@Spy
	private final ContactUsForm controller = new ContactUsForm();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);
	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-18-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getAccounts()).thenReturn(new ArrayList<>());
		when(Utils.getRSALanguage(any())).thenReturn("EN");

		Map<String, Object> result = controller.referenceData(request);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when OnSubmit
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-18-2020
	 * @exception Exception error
	 */
	@Test
	public void whenOnSubmit_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("cookieAnalyticEnabled", true);

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(bean.getToinfo()).thenReturn(null);
		when(bean.getSubcategory()).thenReturn(null);
		controller.departmentList = departmentList;
		when(departmentList.get(anyString())).thenReturn(null);
		when(bean.getMessage()).thenReturn("abcde123456");
		when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
		when(bean.getTelephone1()).thenReturn("1");
		when(bean.getTelephone2()).thenReturn("2");
		when(bean.getTelephone3()).thenReturn("3");
		when(tvFrontendAccount.getAccountNumberSuffix()).thenReturn(null);
		when(customerProfile.getPermId()).thenReturn(new BigInteger("123456789"));
		when(customerProfile.getEnrollmentAccountNumber()).thenReturn("123456789");
		when(tvFrontendAccount.getAccountNumber()).thenReturn("987654321");
		when(userSession.getCustomerService()).thenReturn(customerService);
		PowerMockito.doNothing().when(customerService).sendKanaEmail(any(CustomerProfile.class),
				any(), any(), any(), any(PreferredLanguage.class), any());
		PowerMockito.doReturn(mav).when((SimpleFormController)controller, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any());

		ModelAndView mavResult = controller.onSubmit(request, response, bean, error);
		assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}
}
