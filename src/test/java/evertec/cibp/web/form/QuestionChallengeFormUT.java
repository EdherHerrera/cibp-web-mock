package evertec.cibp.web.form;

import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.campaign.loader.CampaignPaths;
import evertec.cibp.core.models.customer.CustomerDevices;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.customer.modules.SignOnCustomerServiceModule;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.ValidationQuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.form.bean.QuestionChallengeBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StaticMessageSource.class, CustomerUtils.class, WebUtils.class, ResultStatus.class, RSAUtils.class,
		Utils.class, DeviceUtils.class, SignonSequenceUtils.class, SignOnCustomerServiceModule.class})
public class QuestionChallengeFormUT extends AbstractModelAndViewTests {

	private static final String IS_SECURITIES_TRUE = "true";
	private static final String IS_SECURITIES_FALSE = "false";
	private static final String USER_SESSION = "userSession";
	private static final String USERNAME = "valdesec";
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private CustomerServices customerServices;

	@Mock
	private QuestionChallengeBean command;
	
	@Mock 
	private ValidationQuestionResponse responseAnswer;
	
	@Mock 
	private DeviceRequest deviceRequest;	

	@Mock
	private UserSession userSession;

	@Mock
	private QuestionResponse questionResponse;

	@Mock
	private CampaignPaths campaignPaths;

	@Mock
	private BindException error;

	@Mock
	private MessageSource messageSource;

	@Mock
	private File file;

	@Mock
	private BindException errors;

	@InjectMocks
	private QuestionChallengeForm controller;

	@Mock
	private SignOnCustomerServiceModule signOnCustomerServiceModule;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		errors = new BindException(command, "QuestionChallengeBean");
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerUtils.class);
		PowerMockito.mockStatic(ResultStatus.class);
		PowerMockito.mockStatic(RSAUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(DeviceUtils.class);
		PowerMockito.mockStatic(SignonSequenceUtils.class);
		responseAnswer = PowerMockito.mock(ValidationQuestionResponse.class, Mockito.RETURNS_DEEP_STUBS);
		when(WebUtils.getCookie(request, "PMData")).thenReturn(new Cookie("PMData", "PMData"));
		when(WebUtils.getSessionAttribute(request, USER_SESSION)).thenReturn(userSession);
		when(command.getAnswer()).thenReturn("2");
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerServices);
		when(customerServices.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);
		when(signOnCustomerServiceModule.validateRSAQuestion(any(RSAActionRequest.class)))
				.thenReturn(responseAnswer);

	}

	@Test
	@Ignore
	public void whenOnSubmit_GivenIsSecuritiesCustomerTrue_ReturnViewForgotPasswordSec() throws Exception {
		customerProfile.setIsSecurities(IS_SECURITIES_TRUE);
		customerProfile.setUsername(USERNAME);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getUserAgent()).thenReturn("");
		when(userSession.getUsername()).thenReturn(USERNAME);
		when(CustomerUtils.isSecurities(customerProfile)).thenReturn(Boolean.TRUE);
		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.BLOCKED);
		ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
		assertViewName(modelAndView, "redirect:forgotpasswordsec");
	}
	
	@Test
	@Ignore
	public void whenOnSubmit_GivenIsSecuritiesCustomerFalse_ReturnViewLogin() throws Exception {
		customerProfile.setIsSecurities(IS_SECURITIES_FALSE);
		customerProfile.setUsername(USERNAME);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getUserAgent()).thenReturn("");
		when(userSession.getUsername()).thenReturn(USERNAME);
		when(CustomerUtils.isSecurities(customerProfile)).thenReturn(Boolean.FALSE);
		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.BLOCKED);
		ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
		assertViewName(modelAndView, "forward:login");
	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-23-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		Map<Object, Object> mapResult = new HashMap<>();
		mapResult.put("1234", "????");

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(Utils.getRSALanguage(any())).thenReturn("es");
		when(userSession.getUsername()).thenReturn("Jonas");
		when(userSession.getCustomerProfile()).thenReturn(null);
		
		Map mapQuestion = new HashMap<>();
		mapQuestion.put("question1", "Mock?");
		when(userSession.getQuestion()).thenReturn(mapQuestion);
		PowerMockito.whenNew(QuestionResponse.class).withNoArguments().thenReturn(questionResponse);
		when(questionResponse.getStatusResult()).thenReturn(ResultStatus.DELETED);
		when(questionResponse.getQuestion()).thenReturn(mapResult);
		controller.setPaths(campaignPaths);
		controller.setMessageSource(messageSource);
		when(messageSource.getMessage(anyString(), any(), anyString(), any(Locale.class))).thenReturn("asdasdfasf");
		when(WebUtils.getCookie(request, "PMData")).thenReturn(null);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn("sdfasdfas1263541351");
		when(campaignPaths.getFilePath()).thenReturn("asdfasfasfasdfasf");
		PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(file);
		when(file.exists()).thenReturn(Boolean.FALSE);

		Map<String, Object> result = controller.referenceData(request, command, error);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when OnSubmit
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-23-2020
	 * @exception Exception error
	 */
	@Test
	public void whenOnSubmit_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(command.getAnswer()).thenReturn("TRUE");
		when(userSession.getUsername()).thenReturn("Jonas");
		when(command.isRemember()).thenReturn(Boolean.FALSE);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getUserAgent()).thenReturn("json");
		when(DeviceUtils.getCustomerDevice(any())).thenReturn(CustomerDevices.ANDROID);
		when(customerProfile.getUsername()).thenReturn("Jonas");
		when(userSession.getRsaSessionId()).thenReturn("4654654564");
		when(userSession.getRsaTransactionId()).thenReturn("8798798789789");
		when(userSession.getQuestion()).thenReturn(new HashMap<>());
		when(Utils.getRSALanguage(any())).thenReturn("en");
		when(customerServices.getSignOnCustomerServiceModule().validateRSAQuestion(any())).thenReturn(responseAnswer);
		when(responseAnswer.getRsaSessionId()).thenReturn("1231321");
		when(responseAnswer.getRsaTransactionId()).thenReturn("asefas54564");
		when(WebUtils.getCookie(request, "PMData")).thenReturn(null);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn("asdfasdf5456465");
		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.DISABLED);
		when(SignonSequenceUtils.getNextView(any())).thenReturn("redirect:question");
		when(userSession.getCustomerService()).thenReturn(customerServices);

		ModelAndView result = controller.onSubmit(request, response, command, error);
		assertViewName(result, "redirect:question");
	}

	@Test
	public void whenOnSubmit_GivenIsMobileSDKRequest_ThenReturnModelValueTrue () throws Exception {
		when(command.getDevice_info_rsa()).thenReturn("Test");
		when(command.getRsa_cookie()).thenReturn("Test");
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(command.getAnswer()).thenReturn("TRUE");
		when(userSession.getUsername()).thenReturn("Jonas");
		when(command.isRemember()).thenReturn(Boolean.FALSE);
		when(deviceRequest.getUserAgent()).thenReturn("json");
		when(DeviceUtils.getCustomerDevice(any())).thenReturn(CustomerDevices.ANDROID);
		when(customerProfile.getUsername()).thenReturn("Jonas");
		when(userSession.getRsaSessionId()).thenReturn("4654654564");
		when(userSession.getRsaTransactionId()).thenReturn("8798798789789");
		when(userSession.getQuestion()).thenReturn(new HashMap<>());
		when(Utils.getRSALanguage(any())).thenReturn("en");
//		when(RSAUtils.validateRSAQuestion  (any()).thenReturn(responseAnswer);
		when(responseAnswer.getRsaSessionId()).thenReturn("1231321");
		when(responseAnswer.getRsaTransactionId()).thenReturn("asefas54564");
		when(WebUtils.getCookie(request, "PMData")).thenReturn(null);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn("asdfasdf5456465");
		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.DISABLED);
		when(SignonSequenceUtils.getNextView(any())).thenReturn("redirect:question");
		when(userSession.getCustomerService()).thenReturn(customerServices);
//		when(customerServices.validateRSAQuestion(any())).thenReturn(responseAnswer);

		ModelAndView result = controller.onSubmit(request, response, command, error);
		assertViewName(result, "redirect:question");
	}
}
