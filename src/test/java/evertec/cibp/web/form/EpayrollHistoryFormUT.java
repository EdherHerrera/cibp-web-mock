package evertec.cibp.web.form;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerWealthPreferred;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ResultStatus.class, CustomerFacade.class,StaticMessageSource.class, WebUtils.class, Utils.class, CustomerWealthPreferred.class, EpayrollHistoryForm.class})
public class EpayrollHistoryFormUT extends AbstractModelAndViewTests{
	
	@Mock
	private MessageSource messageSource;

	@InjectMocks
	private MakePayrollForm controller;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerFacade customerFacade;

	@Spy
	private EpayrollHistoryForm epayrollHistoryForm = new EpayrollHistoryForm(); 

	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);
		
		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
	}
	
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(Utils.getRSALanguage(any(HttpServletRequest.class))).thenReturn("es");
		when(epayrollHistoryForm.getGlobalEntitlement()).thenReturn(null);

	}
	
	@Test
	public void whenGetTelepagoErrorDescriptor_GivenMessageError_ThenReturnMessageErrorSucces() throws Exception {
		controller.setMessageSource(messageSource);
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("OK");

		String valueValidate = Whitebox.invokeMethod(controller, "getTelepagoErrorDescriptor", "ebpp.module", "ens");
		assertEquals(valueValidate, "OK");
	}

}
