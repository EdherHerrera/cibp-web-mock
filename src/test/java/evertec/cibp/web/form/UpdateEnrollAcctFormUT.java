package evertec.cibp.web.form;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.UpdateEnrollAcctBean;
import evertec.cibp.web.session.UserSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

import static org.mockito.Mockito.*;


@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, WebUtils.class})
public class UpdateEnrollAcctFormUT extends AbstractModelAndViewTests {

    private HttpServletRequest request;
    private HttpServletResponse response;

    @Mock
    private UserSession userSession;

    @InjectMocks
    private UpdateEnrollAcctForm controller;

    @Before
    public void setUp() throws Exception {

        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(WebUtils.class);

        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

    }

    @Test
    public void whenReferenceData_GivenCustomerProfileIfxError_IsTrue_ThenReturnModelAndViewWithMessage() throws Exception {
        UpdateEnrollAcctBean bean = new UpdateEnrollAcctBean();
        BindException error = new BindException(bean, "UpdateEnrollAcct");

        CustomerProfile customerProfile = new CustomerProfile();
        customerProfile.setIfxError(true);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(WebUtils.getSessionAttribute(request, "sessionEnrollUpdate")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA577)).thenReturn(true);

        Map<String, Object> map =  this.controller.referenceData(request, bean, error, 1);

        boolean respMap = (boolean) map.get("ifxError");

        assertTrue(respMap);
    }


}