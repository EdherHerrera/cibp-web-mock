package evertec.cibp.web.form.mobilecach;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.google.common.collect.Iterables;

import evertec.cibp.core.dto.PushTokensDto;
import evertec.cibp.core.enums.PushTokenStatus;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.PushTokensRequest;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.MobileResponseStatus;
import evertec.cibp.web.facade.PushTokensFacade;
import evertec.cibp.web.form.mobilecash.MobileCashTrxSubmitForm;
import evertec.cibp.web.request.PushTokensMobileRequest;
import evertec.cibp.web.response.PushTokensResponse;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, WebUtils.class, PushTokensFacade.class, Iterables.class})
public class MobileCashTrxSubmitFormUT extends AbstractModelAndViewTests {

	
	private static final String PROFILE_ID = "ff8081811ac1d21c011ac2a4a5a02f2d";
	private static final String VIEW_NAME = "pushTokenResponse";
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock 
	private CustomerProfileService customerProfileService;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private PushTokensMobileRequest statusRequest;
	
	@Mock 
	private UserSession userSession;
	
	@Mock
	private PushTokensResponse pushTokensResponse;
	
	@InjectMocks
	private MobileCashTrxSubmitForm controller;
	
	@Mock
	private PushTokensFacade facade;
	

	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(PushTokensFacade.class);
		PowerMockito.mockStatic(Iterables.class);
				
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		
		request.addParameter("pushToken", RandomStringUtils.randomAlphabetic(155));
		request.addParameter("deviceType", "Android");
		request.addParameter("deviceModel", "S10+");
		request.addParameter("deviceName", "Armando's s10+");
		
	}

	
	@Test
	public void whenreferenceData_Givenalldata_ThenReturnMap() throws Exception {
			
//TODO: finish UT of MBKW-116 STORY	
		
		//Map<String, Object> modelAndView = controller.referenceData(request, response, null); 
		
	} 
	
}
