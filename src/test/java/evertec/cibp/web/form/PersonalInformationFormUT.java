package evertec.cibp.web.form;

import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerPassword;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.applications.AddressApplication;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.customer.modules.SignOnCustomerServiceModule;
import evertec.cibp.core.services.interfaces.CustomerInteractionService;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.RsaQuestionsUpdateResponse;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR_OOB;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.PersonalInformationBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.DeviceRequest;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Mission to 100% coverage..
 * @author jnieves
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({CustomerProfile.class, UserSession.class,
        CustomerServices.class, CustomerInteractionEvent.class,
        WebUtils.class, Utils.class, StaticMessageSource.class,
        RSAUtils.class, PersonalInformationForm.class})
public class PersonalInformationFormUT extends AbstractModelAndViewTests {

    private static final String TEST = "TEST";
    private static final String ID = "ID";
    private static final String DEFAULT_VIEW = "text";

    private Map<Object,Object> map;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private UserSession userSession;

    @Mock
    private CustomerServices customerServices;

    @Mock
    private CustomerInteractionEvent customerInteractionEvent;
    
    @Mock
    private CustomerEntitlement customerEntitlement;

    @Mock
    AlertsEntitlement alertsEntitlement;
    
    @Mock
    private CustomerFacade customerFacade;
    
    @Mock
    private BindException errors;
    
    @Mock
    private CustomerInteractionService customerInteractionService;
    
    @Mock
    private DeviceRequest deviceRequest;
    
    @Mock
    private SignOnCustomerServiceModule signOnCustomerServiceModule;
    
    @Mock
    ModifyCustomerProfileResponse modifyCustomerProfileResponse;
    
    @Mock
    CustomerProfileService customerProfileService;
    
    @Mock
    RsaQuestionsUpdateResponse rsaQuestionsUpdateResponse;
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    
    @Mock
    PersonalInformationBean personalInformationBean;

    @InjectMocks
    @Spy
    private final PersonalInformationForm controller = new PersonalInformationForm();

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(CustomerProfile.class);
        PowerMockito.mockStatic(UserSession.class);
        PowerMockito.mockStatic(CustomerServices.class);
        PowerMockito.mockStatic(CustomerInteractionEvent.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(RSAUtils.class);
        
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

    }

    public void init(){
        map = new HashMap<Object, Object>();
        map.put(TEST, TEST);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        
        when(Utils.generateCustomerInteractionEvent(any(), any())).thenReturn(customerInteractionEvent);
        when(customerServices.getInteractionService()).thenReturn(customerInteractionService);
    }

    private void referenceDataMocks() {
         when(Utils.getRSALanguage(any())).thenReturn("en");
         when(deviceRequest.getDeviceTokenCookie()).thenReturn("PMData");
         when(userSession.getInformationQuestion()).thenReturn(null);// for when referenceData is called
    }

    @Test
    public void whenOnSubmit_GivenNoInformationChanged_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "empty-");
    }
    
    @Test
    public void whenOnSubmit_GivenInformationChanged_AndOOBEmpty_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(true);
        
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(true);
        
        List<ASK_FOR_OOB> askList =new ArrayList<>();
        askList.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
        
        request.addParameter("oob", StringUtils.EMPTY);
        
        when(userSession.getTasksListOOB()).thenReturn(askList);
        
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "empty-");
    }
    
    @Test
    public void whenOnSubmit_GivenInformationChanged_WithActionPassword_EN_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        
        final String ACTION = ChallengeInformationUtils.PASSWORD_ACTION;
        
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(true);
        
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(true);
        
        List<ASK_FOR_OOB> askList =new ArrayList<>();
        askList.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
        
        request.addParameter("oob", "Not Empty");
        
        when(userSession.getTasksListOOB()).thenReturn(askList);
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION)).thenReturn(ACTION);
        
        Map<String, Object> map = new HashMap<>();
        map.put(ChallengeInformationUtils.PASSWORD_ACTION, RandomStringUtils.random(4));
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS)).thenReturn(map);
        
        Set<AlertType> alertType = new HashSet<AlertType>();
        
        alertType.add(AlertType.CHANGE_PASS_PUSH);
        
        customerEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        
        List<CustomerEntitlement> newEntitlementList = new LinkedList<CustomerEntitlement>();
        
        GlobalEntitlement globalEnt = new GlobalEntitlement();
        globalEnt.setType(EntitlementType.ALERTS);
        
        CustomerEntitlement newEntitlement = new CustomerEntitlement();
		newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\""+AlertType.CHANGE_PASS_PUSH.toString()+"\"/>\n"
						+ "</properties>");
		
		newEntitlement.setCustomerProfile(customerProfile);
		newEntitlementList.add(newEntitlement);
        
        when(customerProfile.getEntitlements()).thenReturn(new HashSet<CustomerEntitlement>(newEntitlementList));
        
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
        
        when(alertsEntitlement.getCustomerEntitlement()).thenReturn(customerEntitlement);
        
        when(alertsEntitlement.getAlerts()).thenReturn(alertType);
        
        when(customerServices.modifyCustomerPassword(any())).thenReturn(modifyCustomerProfileResponse);
        when(modifyCustomerProfileResponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        
        List<CustomerPassword> passwordList = new LinkedList<CustomerPassword>();
        
        CustomerPassword csPass = new CustomerPassword();
        csPass.setPasswordDate(new Date());
        csPass.setProfileId(RandomStringUtils.random(4));
        passwordList.add(csPass);
        
        when(userSession.getPasswords()).thenReturn(passwordList);
        controller.setMaxPasswordRecords(1);
        
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        
        when(modifyCustomerProfileResponse.getCustomerProfile()).thenReturn(customerProfile);
        
        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
        
        when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
        
        when(Utils.getRSALanguage(any())).thenReturn("en");
        
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "success-");
    }
    
    @Test
    public void whenOnSubmit_GivenInformationChanged_WithActionPassword_ES_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        
        final String ACTION = ChallengeInformationUtils.PASSWORD_ACTION;
        
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(true);
        
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(true);
        
        List<ASK_FOR_OOB> askList =new ArrayList<>();
        askList.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
        
        request.addParameter("oob", "Not Empty");
        
        when(userSession.getTasksListOOB()).thenReturn(askList);
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION)).thenReturn(ACTION);
        
        Map<String, Object> map = new HashMap<>();
        map.put(ChallengeInformationUtils.PASSWORD_ACTION, RandomStringUtils.random(4));
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS)).thenReturn(map);
        
        Set<AlertType> alertType = new HashSet<AlertType>();
        
        alertType.add(AlertType.CHANGE_PASS_PUSH);
        
        customerEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        
        List<CustomerEntitlement> newEntitlementList = new LinkedList<CustomerEntitlement>();
        
        GlobalEntitlement globalEnt = new GlobalEntitlement();
        globalEnt.setType(EntitlementType.ALERTS);
        
        CustomerEntitlement newEntitlement = new CustomerEntitlement();
		newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\""+AlertType.CHANGE_PASS_PUSH.toString()+"\"/>\n"
						+ "</properties>");
		
		newEntitlement.setCustomerProfile(customerProfile);
		newEntitlementList.add(newEntitlement);
        
        when(customerProfile.getEntitlements()).thenReturn(new HashSet<CustomerEntitlement>(newEntitlementList));
        
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
        
        when(alertsEntitlement.getCustomerEntitlement()).thenReturn(customerEntitlement);
        
        when(alertsEntitlement.getAlerts()).thenReturn(alertType);
        
        when(customerServices.modifyCustomerPassword(any())).thenReturn(modifyCustomerProfileResponse);
        when(modifyCustomerProfileResponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        
        List<CustomerPassword> passwordList = new LinkedList<CustomerPassword>();
        
        CustomerPassword csPass = new CustomerPassword();
        csPass.setPasswordDate(new Date());
        csPass.setProfileId(RandomStringUtils.random(4));
        passwordList.add(csPass);
        
        when(userSession.getPasswords()).thenReturn(passwordList);
        controller.setMaxPasswordRecords(1);
        
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        
        when(modifyCustomerProfileResponse.getCustomerProfile()).thenReturn(customerProfile);
        
        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
        
        when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.SPANISH);
        
        when(Utils.getRSALanguage(any())).thenReturn("es");
        
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "success-");
    }
    
    @Test
    public void whenOnSubmit_GivenInformationChanged_WithActionPassword_And_ResultStatusErrorEN_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        
        final String ACTION = ChallengeInformationUtils.PASSWORD_ACTION;
        
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(true);
        
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(true);
        
        List<ASK_FOR_OOB> askList =new ArrayList<>();
        askList.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
        
        request.addParameter("oob", "Not Empty");
        
        when(userSession.getTasksListOOB()).thenReturn(askList);
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION)).thenReturn(ACTION);
        
        Map<String, Object> map = new HashMap<>();
        map.put(ChallengeInformationUtils.PASSWORD_ACTION, "testrandomstring");
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS)).thenReturn(map);
        
        Set<AlertType> alertType = new HashSet<AlertType>();
        
        alertType.add(AlertType.CHANGE_PASS_PUSH);
        
        customerEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        
        List<CustomerEntitlement> newEntitlementList = new LinkedList<CustomerEntitlement>();
        
        GlobalEntitlement globalEnt = new GlobalEntitlement();
        globalEnt.setType(EntitlementType.ALERTS);
        
        CustomerEntitlement newEntitlement = new CustomerEntitlement();
        newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\""+AlertType.CHANGE_PASS_PUSH.toString()+"\"/>\n"
						+ "</properties>");
		
		newEntitlement.setCustomerProfile(customerProfile);
		newEntitlementList.add(newEntitlement);
        
        when(customerProfile.getEntitlements()).thenReturn(new HashSet<CustomerEntitlement>(newEntitlementList));
        
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
        
        when(alertsEntitlement.getCustomerEntitlement()).thenReturn(customerEntitlement);
        
        when(alertsEntitlement.getAlerts()).thenReturn(alertType);
        
        when(customerServices.modifyCustomerPassword(any())).thenReturn(modifyCustomerProfileResponse);
        when(modifyCustomerProfileResponse.getStatus()).thenReturn(ResultStatus.ERROR);
        
        List<CustomerPassword> passwordList = new LinkedList<CustomerPassword>();
        
        CustomerPassword csPass = new CustomerPassword();
        csPass.setPasswordDate(new Date());
        csPass.setProfileId(RandomStringUtils.random(4));
        passwordList.add(csPass);
        
        when(userSession.getPasswords()).thenReturn(passwordList);
        controller.setMaxPasswordRecords(1);
        
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        
        when(modifyCustomerProfileResponse.getCustomerProfile()).thenReturn(customerProfile);
        
        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
        
        when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
        
        when(Utils.getRSALanguage(any())).thenReturn("en");
        
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "success-");
    }
    
    @Test
    public void whenOnSubmit_GivenInformationChanged_WithActionPassword_And_ResultStatusErrorES_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        
        final String ACTION = ChallengeInformationUtils.PASSWORD_ACTION;
        
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(true);
        
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(true);
        
        List<ASK_FOR_OOB> askList =new ArrayList<>();
        askList.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
        
        request.addParameter("oob", "Not Empty");
        
        when(userSession.getTasksListOOB()).thenReturn(askList);
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION)).thenReturn(ACTION);
        
        Map<String, Object> map = new HashMap<>();
        map.put(ACTION, "testrandomstring");
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS)).thenReturn(map);
        
        Set<AlertType> alertType = new HashSet<AlertType>();
        
        alertType.add(AlertType.CHANGE_PASS_PUSH);
        
        customerEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        
        List<CustomerEntitlement> newEntitlementList = new LinkedList<CustomerEntitlement>();
        
        GlobalEntitlement globalEnt = new GlobalEntitlement();
        globalEnt.setType(EntitlementType.ALERTS);
        
        CustomerEntitlement newEntitlement = new CustomerEntitlement();
        newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\""+AlertType.CHANGE_PASS_PUSH.toString()+"\"/>\n"
						+ "</properties>");
		
		newEntitlement.setCustomerProfile(customerProfile);
		newEntitlementList.add(newEntitlement);
        
        when(customerProfile.getEntitlements()).thenReturn(new HashSet<CustomerEntitlement>(newEntitlementList));
        
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
        
        when(alertsEntitlement.getCustomerEntitlement()).thenReturn(customerEntitlement);
        
        when(alertsEntitlement.getAlerts()).thenReturn(alertType);
        
        when(customerServices.modifyCustomerPassword(any())).thenReturn(modifyCustomerProfileResponse);
        when(modifyCustomerProfileResponse.getStatus()).thenReturn(ResultStatus.ERROR);
        
        List<CustomerPassword> passwordList = new LinkedList<CustomerPassword>();
        
        CustomerPassword csPass = new CustomerPassword();
        csPass.setPasswordDate(new Date());
        csPass.setProfileId(RandomStringUtils.random(4));
        passwordList.add(csPass);
        
        when(userSession.getPasswords()).thenReturn(passwordList);
        controller.setMaxPasswordRecords(1);
        
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        
        when(modifyCustomerProfileResponse.getCustomerProfile()).thenReturn(customerProfile);
        
        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
        
        when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
        
        when(Utils.getRSALanguage(any())).thenReturn("es");
        
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "success-");
    }
    
    @Test
    public void whenOnSubmit_GivenInformationChanged_WithActionQuestion_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        
        final String ACTION = ChallengeInformationUtils.QUESTIONS_ACTION;
        
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(true);
        
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(true);
        
        List<ASK_FOR_OOB> askList =new ArrayList<>();
        askList.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
        
        request.addParameter("oob", "Not Empty");
        
        when(userSession.getTasksListOOB()).thenReturn(askList);
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION)).thenReturn(ACTION);
        
        Map<String, Object> map = new HashMap<>();
        map.put(ACTION, "testrandomstring");
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS)).thenReturn(map);
        
        Set<AlertType> alertType = new HashSet<AlertType>();
        
        alertType.add(AlertType.CHANGE_PASS_PUSH);
        
        customerEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        
        List<CustomerEntitlement> newEntitlementList = new LinkedList<CustomerEntitlement>();
        
        GlobalEntitlement globalEnt = new GlobalEntitlement();
        globalEnt.setType(EntitlementType.ALERTS);
        
        CustomerEntitlement newEntitlement = new CustomerEntitlement();
        newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\""+AlertType.QUESTIONS_EDIT_PUSH.toString()+"\"/>\n"
						+ "</properties>");
		
		newEntitlement.setCustomerProfile(customerProfile);
		newEntitlementList.add(newEntitlement);
        
        when(customerProfile.getEntitlements()).thenReturn(new HashSet<CustomerEntitlement>(newEntitlementList));
        
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
        
        when(alertsEntitlement.getCustomerEntitlement()).thenReturn(customerEntitlement);
        
        when(alertsEntitlement.getAlerts()).thenReturn(alertType);
        
        when(customerServices.modifyCustomerPassword(any())).thenReturn(modifyCustomerProfileResponse);
        
        List<CustomerPassword> passwordList = new LinkedList<CustomerPassword>();
        
        CustomerPassword csPass = new CustomerPassword();
        csPass.setPasswordDate(new Date());
        csPass.setProfileId(RandomStringUtils.random(4));
        passwordList.add(csPass);
        
        when(userSession.getPasswords()).thenReturn(passwordList);
        controller.setMaxPasswordRecords(1);
        
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        
        when(modifyCustomerProfileResponse.getCustomerProfile()).thenReturn(customerProfile);
        
        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
        
        when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);

        when(customerServices.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);
        
        when(customerServices.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any())).thenReturn(rsaQuestionsUpdateResponse);
        
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        
        when(deviceRequest.getDeviceTokenCookie()).thenReturn("something");
        
        when(rsaQuestionsUpdateResponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        
        when(Utils.getRSALanguage(any())).thenReturn("en");
        
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "success-");
    }
    
    @Test
    public void whenOnSubmit_GivenInformationChanged_WithActionQuestion_ES_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        
        final String ACTION = ChallengeInformationUtils.QUESTIONS_ACTION;
        
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(true);
        
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(true);
        
        List<ASK_FOR_OOB> askList =new ArrayList<>();
        askList.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
        
        request.addParameter("oob", "Not Empty");
        
        when(userSession.getTasksListOOB()).thenReturn(askList);
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION)).thenReturn(ACTION);
        
        Map<String, Object> map = new HashMap<>();
        map.put(ACTION, "testrandomstring");
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS)).thenReturn(map);
        
        Set<AlertType> alertType = new HashSet<AlertType>();
        
        alertType.add(AlertType.CHANGE_PASS_PUSH);
        
        customerEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        
        List<CustomerEntitlement> newEntitlementList = new LinkedList<CustomerEntitlement>();
        
        GlobalEntitlement globalEnt = new GlobalEntitlement();
        globalEnt.setType(EntitlementType.ALERTS);
        
        CustomerEntitlement newEntitlement = new CustomerEntitlement();
        newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\""+AlertType.QUESTIONS_EDIT_PUSH.toString()+"\"/>\n"
						+ "</properties>");
		
		newEntitlement.setCustomerProfile(customerProfile);
		newEntitlementList.add(newEntitlement);
        
        when(customerProfile.getEntitlements()).thenReturn(new HashSet<CustomerEntitlement>(newEntitlementList));
        
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
        
        when(alertsEntitlement.getCustomerEntitlement()).thenReturn(customerEntitlement);
        
        when(alertsEntitlement.getAlerts()).thenReturn(alertType);
        
        when(customerServices.modifyCustomerPassword(any())).thenReturn(modifyCustomerProfileResponse);
        
        List<CustomerPassword> passwordList = new LinkedList<CustomerPassword>();
        
        CustomerPassword csPass = new CustomerPassword();
        csPass.setPasswordDate(new Date());
        csPass.setProfileId(RandomStringUtils.random(4));
        passwordList.add(csPass);
        
        when(userSession.getPasswords()).thenReturn(passwordList);
        controller.setMaxPasswordRecords(1);
        
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        
        when(modifyCustomerProfileResponse.getCustomerProfile()).thenReturn(customerProfile);
        
        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
        
        when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);

        when(customerServices.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);
        
        when(customerServices.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any())).thenReturn(rsaQuestionsUpdateResponse);
        
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        
        when(deviceRequest.getDeviceTokenCookie()).thenReturn("something");
        
        when(rsaQuestionsUpdateResponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        
        when(Utils.getRSALanguage(any())).thenReturn("es");
        
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "success-");
    }
    
    @Test
    public void whenOnSubmit_GivenInformationChanged_WithActionQuestion_And_ResultStatusError_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        
        final String ACTION = ChallengeInformationUtils.QUESTIONS_ACTION;
        
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(true);
        
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(true);
        
        List<ASK_FOR_OOB> askList =new ArrayList<>();
        askList.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
        
        request.addParameter("oob", "Not Empty");
        
        when(userSession.getTasksListOOB()).thenReturn(askList);
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION)).thenReturn(ACTION);
        
        Map<String, Object> map = new HashMap<>();
        map.put(ACTION, "testrandomstring");
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS)).thenReturn(map);
        
        Set<AlertType> alertType = new HashSet<AlertType>();
        
        alertType.add(AlertType.CHANGE_PASS_PUSH);
        
        customerEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        
        List<CustomerEntitlement> newEntitlementList = new LinkedList<CustomerEntitlement>();
        
        GlobalEntitlement globalEnt = new GlobalEntitlement();
        globalEnt.setType(EntitlementType.ALERTS);
        
        CustomerEntitlement newEntitlement = new CustomerEntitlement();
        newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\""+AlertType.QUESTIONS_EDIT_PUSH.toString()+"\"/>\n"
						+ "</properties>");
		
		newEntitlement.setCustomerProfile(customerProfile);
		newEntitlementList.add(newEntitlement);
        
        when(customerProfile.getEntitlements()).thenReturn(new HashSet<CustomerEntitlement>(newEntitlementList));
        
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
        
        when(alertsEntitlement.getCustomerEntitlement()).thenReturn(customerEntitlement);
        
        when(alertsEntitlement.getAlerts()).thenReturn(alertType);
        
        when(customerServices.modifyCustomerPassword(any())).thenReturn(modifyCustomerProfileResponse);
        
        List<CustomerPassword> passwordList = new LinkedList<CustomerPassword>();
        
        CustomerPassword csPass = new CustomerPassword();
        csPass.setPasswordDate(new Date());
        csPass.setProfileId(RandomStringUtils.random(4));
        passwordList.add(csPass);
        
        when(userSession.getPasswords()).thenReturn(passwordList);
        controller.setMaxPasswordRecords(1);
        
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        
        when(modifyCustomerProfileResponse.getCustomerProfile()).thenReturn(customerProfile);
        
        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
        
        when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);

        when(customerServices.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);
        
        when(customerServices.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any())).thenReturn(rsaQuestionsUpdateResponse);
        
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        
        when(deviceRequest.getDeviceTokenCookie()).thenReturn("something");
        
        when(rsaQuestionsUpdateResponse.getStatus()).thenReturn(ResultStatus.ERROR);
        
        when(Utils.getRSALanguage(any())).thenReturn("en");
        
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "success-");
    }
    
    @Test
    public void whenOnSubmit_GivenInformationChanged_WithActionQuestion_And_ResultStatusErrorES_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        
        final String ACTION = ChallengeInformationUtils.QUESTIONS_ACTION;
        
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(true);
        
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(true);
        
        List<ASK_FOR_OOB> askList =new ArrayList<>();
        askList.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
        
        request.addParameter("oob", "Not Empty");
        
        when(userSession.getTasksListOOB()).thenReturn(askList);
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION)).thenReturn(ACTION);
        
        Map<String, Object> map = new HashMap<>();
        map.put(ACTION, "testrandomstring");
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS)).thenReturn(map);
        
        Set<AlertType> alertType = new HashSet<AlertType>();
        
        alertType.add(AlertType.CHANGE_PASS_PUSH);
        
        customerEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        
        List<CustomerEntitlement> newEntitlementList = new LinkedList<CustomerEntitlement>();
        
        GlobalEntitlement globalEnt = new GlobalEntitlement();
        globalEnt.setType(EntitlementType.ALERTS);
        
        CustomerEntitlement newEntitlement = new CustomerEntitlement();
        newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\""+AlertType.QUESTIONS_EDIT_PUSH.toString()+"\"/>\n"
						+ "</properties>");
		
		newEntitlement.setCustomerProfile(customerProfile);
		newEntitlementList.add(newEntitlement);
        
        when(customerProfile.getEntitlements()).thenReturn(new HashSet<CustomerEntitlement>(newEntitlementList));
        
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
        
        when(alertsEntitlement.getCustomerEntitlement()).thenReturn(customerEntitlement);
        
        when(alertsEntitlement.getAlerts()).thenReturn(alertType);
        
        when(customerServices.modifyCustomerPassword(any())).thenReturn(modifyCustomerProfileResponse);
        
        List<CustomerPassword> passwordList = new LinkedList<CustomerPassword>();
        
        CustomerPassword csPass = new CustomerPassword();
        csPass.setPasswordDate(new Date());
        csPass.setProfileId(RandomStringUtils.random(4));
        passwordList.add(csPass);
        
        when(userSession.getPasswords()).thenReturn(passwordList);
        controller.setMaxPasswordRecords(1);
        
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        
        when(modifyCustomerProfileResponse.getCustomerProfile()).thenReturn(customerProfile);
        
        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
        
        when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);

        when(customerServices.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);
        
        when(customerServices.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any())).thenReturn(rsaQuestionsUpdateResponse);
        
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        
        when(deviceRequest.getDeviceTokenCookie()).thenReturn("something");
        
        when(rsaQuestionsUpdateResponse.getStatus()).thenReturn(ResultStatus.ERROR);
        
        when(Utils.getRSALanguage(any())).thenReturn("es");
        
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "success-");
    }
    
    @Test
    public void whenOnSubmit_GivenInformationChanged_WithActionPhone_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        
        final String ACTION = ChallengeInformationUtils.PHONES_ACTION;
        
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(true);
        
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(true);
        
        List<ASK_FOR_OOB> askList =new ArrayList<>();
        askList.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
        
        request.addParameter("oob", "Not Empty");
        
        when(userSession.getTasksListOOB()).thenReturn(askList);
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION)).thenReturn(ACTION);
        
        Map<String, Object> map = new HashMap<>();
        map.put(ACTION, "testrandomstring");
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS)).thenReturn(map);
        
        Set<AlertType> alertType = new HashSet<AlertType>();
        
        alertType.add(AlertType.CHANGE_PASS_PUSH);
        
        customerEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        
        List<CustomerEntitlement> newEntitlementList = new LinkedList<CustomerEntitlement>();
        
        GlobalEntitlement globalEnt = new GlobalEntitlement();
        globalEnt.setType(EntitlementType.ALERTS);
        
        CustomerEntitlement newEntitlement = new CustomerEntitlement();
        newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\""+AlertType.PHONE_EDIT_PUSH.toString()+"\"/>\n"
						+ "</properties>");
		
		newEntitlement.setCustomerProfile(customerProfile);
		newEntitlementList.add(newEntitlement);
        
        when(customerProfile.getEntitlements()).thenReturn(new HashSet<CustomerEntitlement>(newEntitlementList));
        
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
        
        when(alertsEntitlement.getCustomerEntitlement()).thenReturn(customerEntitlement);
        
        when(alertsEntitlement.getAlerts()).thenReturn(alertType);
        
        when(customerServices.modifyCustomerPassword(any())).thenReturn(modifyCustomerProfileResponse);
        
        List<CustomerPassword> passwordList = new LinkedList<CustomerPassword>();
        
        CustomerPassword csPass = new CustomerPassword();
        csPass.setPasswordDate(new Date());
        csPass.setProfileId(RandomStringUtils.random(4));
        passwordList.add(csPass);
        
        when(userSession.getPasswords()).thenReturn(passwordList);
        controller.setMaxPasswordRecords(1);
        
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        
        when(modifyCustomerProfileResponse.getCustomerProfile()).thenReturn(customerProfile);
        
        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
        
        when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);

        when(customerServices.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);
        
        when(customerServices.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any())).thenReturn(rsaQuestionsUpdateResponse);
        
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        
        when(deviceRequest.getDeviceTokenCookie()).thenReturn("something");
        
        when(rsaQuestionsUpdateResponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        
        when(Utils.getRSALanguage(any())).thenReturn("en");
        
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "success-");
    }
    
    @Test
    public void whenOnSubmit_GivenInformationChanged_WithActionAddress_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        
        final String ACTION = ChallengeInformationUtils.ADDRESSES_ACTION;
        
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(true);
        
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(true);
        
        List<ASK_FOR_OOB> askList =new ArrayList<>();
        askList.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
        
        request.addParameter("oob", "Not Empty");
        
        when(userSession.getTasksListOOB()).thenReturn(askList);
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION)).thenReturn(ACTION);
        
        Map<String, Object> map = new HashMap<>();
        map.put(ACTION, "testrandomstring");
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS)).thenReturn(map);
        
        Set<AlertType> alertType = new HashSet<AlertType>();
        
        alertType.add(AlertType.CHANGE_PASS_PUSH);
        
        customerEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        
        List<CustomerEntitlement> newEntitlementList = new LinkedList<CustomerEntitlement>();
        
        GlobalEntitlement globalEnt = new GlobalEntitlement();
        globalEnt.setType(EntitlementType.ALERTS);
        
        CustomerEntitlement newEntitlement = new CustomerEntitlement();
        newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\""+AlertType.PHONE_EDIT_PUSH.toString()+"\"/>\n"
						+ "</properties>");
		
		newEntitlement.setCustomerProfile(customerProfile);
		newEntitlementList.add(newEntitlement);
        
        when(customerProfile.getEntitlements()).thenReturn(new HashSet<CustomerEntitlement>(newEntitlementList));
        
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
        
        when(alertsEntitlement.getCustomerEntitlement()).thenReturn(customerEntitlement);
        
        when(alertsEntitlement.getAlerts()).thenReturn(alertType);
        
        when(customerServices.modifyCustomerPassword(any())).thenReturn(modifyCustomerProfileResponse);
        
        List<CustomerPassword> passwordList = new LinkedList<CustomerPassword>();
        
        CustomerPassword csPass = new CustomerPassword();
        csPass.setPasswordDate(new Date());
        csPass.setProfileId(RandomStringUtils.random(4));
        passwordList.add(csPass);
        
        when(userSession.getPasswords()).thenReturn(passwordList);
        controller.setMaxPasswordRecords(1);
        
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        
        when(modifyCustomerProfileResponse.getCustomerProfile()).thenReturn(customerProfile);
        
        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
        
        when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);

        when(customerServices.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);
        
        when(customerServices.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any())).thenReturn(rsaQuestionsUpdateResponse);
        
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        
        when(deviceRequest.getDeviceTokenCookie()).thenReturn("something");
        
        when(rsaQuestionsUpdateResponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        
        when(Utils.getRSALanguage(any())).thenReturn("en");
        
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "success-");
    }
    
    @Test
    public void whenOnSubmit_GivenInformationChanged_WithActionAddress_OutReachFlag_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        
        final String ACTION = ChallengeInformationUtils.ADDRESSES_ACTION;
        
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(true);
        
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(true);
        
        List<ASK_FOR_OOB> askList =new ArrayList<>();
        askList.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
        
        request.addParameter("oob", "Not Empty");
        
        when(userSession.getTasksListOOB()).thenReturn(askList);
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION)).thenReturn(ACTION);
        
        Map<String, Object> map = new HashMap<>();
        map.put(ACTION, "testrandomstring");
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS)).thenReturn(map);
        
        Set<AlertType> alertType = new HashSet<AlertType>();
        
        alertType.add(AlertType.CHANGE_PASS_PUSH);
        
        customerEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        
        List<CustomerEntitlement> newEntitlementList = new LinkedList<CustomerEntitlement>();
        
        GlobalEntitlement globalEnt = new GlobalEntitlement();
        globalEnt.setType(EntitlementType.ALERTS);
        
        CustomerEntitlement newEntitlement = new CustomerEntitlement();
        newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\""+AlertType.PHONE_EDIT_PUSH.toString()+"\"/>\n"
						+ "</properties>");
		
		newEntitlement.setCustomerProfile(customerProfile);
		newEntitlementList.add(newEntitlement);
        
        when(customerProfile.getEntitlements()).thenReturn(new HashSet<CustomerEntitlement>(newEntitlementList));
        
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
        
        when(alertsEntitlement.getCustomerEntitlement()).thenReturn(customerEntitlement);
        
        when(alertsEntitlement.getAlerts()).thenReturn(alertType);
        
        when(customerServices.modifyCustomerPassword(any())).thenReturn(modifyCustomerProfileResponse);
        
        List<CustomerPassword> passwordList = new LinkedList<CustomerPassword>();
        
        CustomerPassword csPass = new CustomerPassword();
        csPass.setPasswordDate(new Date());
        csPass.setProfileId(RandomStringUtils.random(4));
        passwordList.add(csPass);
        
        when(userSession.getPasswords()).thenReturn(passwordList);
        controller.setMaxPasswordRecords(1);
        
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        
        when(modifyCustomerProfileResponse.getCustomerProfile()).thenReturn(customerProfile);
        
        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
        
        when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);

        when(customerServices.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);
        
        when(customerServices.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any())).thenReturn(rsaQuestionsUpdateResponse);
        
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        
        when(deviceRequest.getDeviceTokenCookie()).thenReturn("something");
        
        when(rsaQuestionsUpdateResponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        
        when(Utils.getRSALanguage(any())).thenReturn("en");
        
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OUTREACH)).thenReturn(true);
        
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "success-");
    }
    
    @Test
    public void whenOnSubmit_GivenInformationChanged_WithActionAddress_OutReachFlagFalse_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        
        final String ACTION = ChallengeInformationUtils.ADDRESSES_ACTION;
        
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(true);
        
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(true);
        
        List<ASK_FOR_OOB> askList =new ArrayList<>();
        askList.add(ASK_FOR_OOB.PERSONAL_INFORMATION);
        
        request.addParameter("oob", "Not Empty");
        
        when(userSession.getTasksListOOB()).thenReturn(askList);
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_ACTION)).thenReturn(ACTION);
        
        Map<String, Object> map = new HashMap<>();
        map.put(ACTION, "testrandomstring");
        map.put(ChallengeInformationUtils.ADDRESS_CHANGE_TYPE, "RESIDENTIAL");
        map.put(AddressApplication.RESIDENTIAL_ADDRESS1, StringUtils.EMPTY);
        map.put(AddressApplication.RESIDENTIAL_ADDRESS2, StringUtils.EMPTY);
        map.put(AddressApplication.RESIDENTIAL_CITY, StringUtils.EMPTY);
        map.put(AddressApplication.RESIDENTIAL_STATE, StringUtils.EMPTY);
        map.put(AddressApplication.RESIDENTIAL_ZIPCODE, StringUtils.EMPTY);
        map.put(AddressApplication.NEW_POSTAL_ADDRESS1, StringUtils.EMPTY);
        map.put(AddressApplication.NEW_POSTAL_ADDRESS2, StringUtils.EMPTY);
        map.put(AddressApplication.NEW_POSTAL_CITY, StringUtils.EMPTY);
        map.put(AddressApplication.NEW_POSTAL_STATE, StringUtils.EMPTY);
        map.put(AddressApplication.NEW_POSTAL_ZIPCODE, StringUtils.EMPTY);
        map.put(AddressApplication.OLD_POSTAL_ADDRESS1, StringUtils.EMPTY);
        map.put(AddressApplication.OLD_POSTAL_ADDRESS2, StringUtils.EMPTY);
        map.put(AddressApplication.OLD_POSTAL_CITY, StringUtils.EMPTY);
        map.put(AddressApplication.OLD_POSTAL_STATE, StringUtils.EMPTY);
        map.put(AddressApplication.OLD_POSTAL_ZIPCODE, StringUtils.EMPTY);
        
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.INFORMATION_PARAMETERS)).thenReturn(map);
        
        Set<AlertType> alertType = new HashSet<AlertType>();
        
        alertType.add(AlertType.CHANGE_PASS_PUSH);
        
        customerEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        
        List<CustomerEntitlement> newEntitlementList = new LinkedList<CustomerEntitlement>();
        
        GlobalEntitlement globalEnt = new GlobalEntitlement();
        globalEnt.setType(EntitlementType.ALERTS);
        
        CustomerEntitlement newEntitlement = new CustomerEntitlement();
        newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\""+AlertType.PHONE_EDIT_PUSH.toString()+"\"/>\n"
						+ "</properties>");
		
		newEntitlement.setCustomerProfile(customerProfile);
		newEntitlementList.add(newEntitlement);
        
        when(customerProfile.getEntitlements()).thenReturn(new HashSet<CustomerEntitlement>(newEntitlementList));
        
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
        
        when(alertsEntitlement.getCustomerEntitlement()).thenReturn(customerEntitlement);
        
        when(alertsEntitlement.getAlerts()).thenReturn(alertType);
        
        when(customerServices.modifyCustomerPassword(any())).thenReturn(modifyCustomerProfileResponse);
        
        List<CustomerPassword> passwordList = new LinkedList<CustomerPassword>();
        
        CustomerPassword csPass = new CustomerPassword();
        csPass.setPasswordDate(new Date());
        csPass.setProfileId(RandomStringUtils.random(4));
        passwordList.add(csPass);
        
        when(userSession.getPasswords()).thenReturn(passwordList);
        controller.setMaxPasswordRecords(1);
        
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        
        when(modifyCustomerProfileResponse.getCustomerProfile()).thenReturn(customerProfile);
        
        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
        
        when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);

        when(customerServices.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);
        
        when(customerServices.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any())).thenReturn(rsaQuestionsUpdateResponse);
        
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        
        when(deviceRequest.getDeviceTokenCookie()).thenReturn("something");
        
        when(rsaQuestionsUpdateResponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        
        when(Utils.getRSALanguage(any())).thenReturn("en");
        
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OUTREACH)).thenReturn(false);
        
        ModelAndView modelAndView = controller.onSubmit(request, response, personalInformationBean, errors);
        assertViewName(modelAndView, DEFAULT_VIEW);
        assertModelAttributeValue(modelAndView, "text", "success-");
    }

    /**
     * when ReferenceData
     * Given ShowGdprTrue
     * Then ReturnFrameGdprFlagTrue
     * @author ET58344, Evertec Inc.
     * @since 03-16-2021
     */
    @Test
    public void whenReferenceData_GivenShowGdprTrue_ThenReturnFrameGdprFlagTrue() throws Exception {

        HashMap<String, Object> model = new HashMap<>();
        PowerMockito.whenNew(HashMap.class).withNoArguments().thenReturn(model);
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(WebUtils.getSessionAttribute(request, ChallengeInformationUtils.MOBILE_FRAME)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)).thenReturn(Boolean.FALSE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OUTREACH)).thenReturn(Boolean.TRUE);
        when(userSession.getOobEnroll()).thenReturn(Boolean.FALSE);
        when(WebUtils.getSessionAttribute(request, "SHOW_OOB")).thenReturn(Boolean.TRUE);
        when(WebUtils.getSessionAttribute(request, Utils.SHOW_GDPR)).thenReturn(Boolean.TRUE);
        when(userSession.isInformationChallenge()).thenReturn(Boolean.FALSE);
        PowerMockito.doReturn(model).when(controller, "loadPersonalInformation", any(), any(), any());
        Map<String, Object> modelResult = controller.referenceData(request, personalInformationBean, errors);
        assertEquals(modelResult.get("gdprFrame"), Boolean.TRUE);
    }
}
