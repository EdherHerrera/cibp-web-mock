package evertec.cibp.web.form;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.services.customer.modules.SignOnCustomerServiceModule;
import evertec.cibp.core.services.requests.ModifyAllCustomerEmailsRequest;
import evertec.cibp.core.services.responses.UsernameValidationResponse;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.owasp.csrfguard.CsrfGuard;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.DeviceRequest;


import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.RsaQuestionsUpdateResponse;
import evertec.cibp.core.utils.StaticMessageSource;

import evertec.cibp.web.form.bean.EditEmailBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import evertec.prophit.service.DefaultProphITService;
import evertec.prophit.service.bean.CustomerInquiryBean;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Utils.class, WebUtils.class, StaticMessageSource.class, CcServiceController.class ,CsrfGuard.class ,
		RSAUtils.class, MessageSource.class, CustomerUtils.class})
public class EditEmailFormUT {


	@InjectMocks
	private EditEmailForm editEmailForm;

	private MockHttpServletRequest request;

	@Mock
	private HttpServletResponse responseMock;

	@Mock
	private Errors errors;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private DefaultProphITService defaultProphITService;

	@Mock
	private TVFrontendAccount tvFrontendAccount;

	@Mock
	private CustomerInquiryBean customerInquiryBean;

	@Mock
	private BindException error;

	@Mock
	private DeviceRequest deviceRequest;

	@Mock
	private EditEmailBean editEmailBean;

	@Mock
	private RsaQuestionsUpdateResponse rsaQuestionsUpdateResponse;

	@Mock
	private CsrfGuard csrfGuard;

	@Mock
	private GlobalEntitlement globalEntitlement;

	@Mock
	MessageSource messageSource;

	@Mock
	private SignOnCustomerServiceModule signOnCustomerServiceModule;



	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		request = new MockHttpServletRequest();
		messageSource = mock(MessageSource.class);

		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(RSAUtils.class);
		PowerMockito.mockStatic(CsrfGuard.class);
		PowerMockito.mockStatic(CustomerUtils.class);
		editEmailBean = mock(EditEmailBean.class);

		when(WebUtils.getSessionAttribute(any(), eq("userSession"))).thenReturn(userSession);



		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);


		when(customerService.getGlobalEntitlement(EntitlementType.EBILLS)).thenReturn(globalEntitlement);
		when(customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus()).thenReturn(EntitlementStatus.ENABLED);
		when(customerService.prophItService()).thenReturn(defaultProphITService);
		when(userSession.getProphItCustomerInfo()).thenReturn(customerInquiryBean);
		when(userSession.getCryptoKey()).thenReturn("test");
		when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
		when(Utils.getRSALanguage(any())).thenReturn("en");
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(CsrfGuard.getInstance()).thenReturn(csrfGuard);
		when(csrfGuard.getTokenName()).thenReturn("OWASP-CSRFTOKEN");
		when(WebUtils.getSessionAttribute(any(), eq("OWASP-CSRFTOKEN"))).thenReturn("SYVA-5QK5-RMQF-8IEV-P9ZC-DJTZ-FOKQ-1H7I");
		when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn("HIJKLM");
		when(customerService.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);
		when(signOnCustomerServiceModule.updateUserRSAQuestionSelection(any())).thenReturn(rsaQuestionsUpdateResponse);
		when(editEmailBean.getSkipRSA()).thenReturn(true);

	}

	@Test
	public final void whenOnSubmi_GiventHttpServletRequest_ThenReturnHttpServletResponseObjectBindException() {

		try {

			String token = "SYVA-5QK5-RMQF-8IEV-P9ZC-DJTZ-FOKQ-1H7I";
			WebUtils.setSessionAttribute(request,"OWASP-CSRFTOKEN", token);
			request.setAttribute("OWASP-CSRFTOKEN", token);
			EditEmailBean editEmailBeanReference = new EditEmailBean();
			editEmailBeanReference.setSkipRSA(true);
			when(error.getTarget()).thenReturn(editEmailBeanReference);
			editEmailForm.onSubmit(request, responseMock, editEmailBean, error);

			assertTrue(errors.getFieldErrors().isEmpty());

		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}

	}

	@Test
	public final void whenOnSubmit_GivenOOBRequest_ThenReturnHttpServletResponseObjectSuccesfull() {

		try {

			EditEmailBean editEmailBeanReference = new EditEmailBean();
			editEmailBeanReference.setSkipRSA(true);

			List<UsernameValidationResponse.ASK_FOR_OOB> askForOobList = new ArrayList<>();
			askForOobList.add(UsernameValidationResponse.ASK_FOR_OOB.OOB_CHALLENGE);
			askForOobList.add(UsernameValidationResponse.ASK_FOR_OOB.ALERT_TYPES);
			askForOobList.add(UsernameValidationResponse.ASK_FOR_OOB.EDIT_EMAIL);

			Map<String, Object> parameters = new HashMap<>();



			parameters.put(ChallengeInformationUtils.EMAIL_ACTION,editEmailBeanReference);


			when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)).thenReturn(true);

			when(WebUtils.getSessionAttribute(any(),eq("PARAMETERS"))).thenReturn(parameters);

			when(messageSource.getMessage(any(), any(), any())).thenReturn("ENABLED");

			when(userSession.getOobEnroll()).thenReturn(true);
			when(userSession.isOobAuthenticated()).thenReturn(true);

			when(editEmailBean.getEmail()).thenReturn("g.d@popular.com");
			when(editEmailBean.getEmailconfirm()).thenReturn("g.d@popular.com");
			when(userSession.getTasksListOOB()).thenReturn(askForOobList);

			when(customerProfile.getEmail()).thenReturn("g.d@popular.com");

			when(CustomerUtils.modifyAllEmails(any(ModifyAllCustomerEmailsRequest.class))).thenReturn(customerProfile);


			request.setAttribute("userSession", userSession);


			when(error.getTarget()).thenReturn(editEmailBeanReference);

			editEmailForm.setMessageSource(messageSource);

			ModelAndView modelAndView = editEmailForm.onSubmit(request, responseMock, editEmailBean, error);

			modelAndView.getModel().get("confirm");
			assertEquals(true,modelAndView.getModel().get("confirm"));


		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}

	}

	@Test
	public final void whenReferenceData_HttpServletRequestObject_ThenReturnErrors() {

		try {
			EditEmailBean editEmailBeanReference = new EditEmailBean();
			editEmailBeanReference.setSkipRSA(true);
			editEmailForm.referenceData(request, editEmailBeanReference, errors);

			assertTrue(errors.getFieldErrors().isEmpty());

		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}

	}

}
