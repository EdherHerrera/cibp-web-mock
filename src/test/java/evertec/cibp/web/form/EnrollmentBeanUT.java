package evertec.cibp.web.form;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.MultipleCustomerProfileResponse;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.EnrollmentBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, WebUtils.class, CustomerFacade.class, RSAUtils.class, Utils.class, WebUtils.class})
public class EnrollmentBeanUT {
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private CustomerProfile profile;
	
	@Mock
	MultipleCustomerProfileResponse customerResponse;
	
	@Mock 
	private CustomerProfileService customerProfileService;
	
	@Mock 
	private UserSession userSession;
	
	@Mock 
	private CustomerFacade customerFacade;
	
	@InjectMocks
	private EnrollmentBean enrollmentBean;
	
	@Before
	public void setUp() throws Exception {
		
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(RSAUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(WebUtils.class);
		enrollmentBean = new EnrollmentBean();
	}

	@Test
	public void gettersAndSettersAccountAthNumber() throws Exception {
		
		enrollmentBean.setClearedAccountNumber(anyString());
		enrollmentBean.setClearedATHNumber(anyString());
		enrollmentBean.setShowAccountATHNumber(anyString());
		assertNotNull(enrollmentBean.getClearedAccountNumber());
		assertNotNull(enrollmentBean.getClearedATHNumber());
		assertNotNull(enrollmentBean.getShowAccountATHNumber());	
	}

}
