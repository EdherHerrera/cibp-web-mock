package evertec.cibp.web.form;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.signup.SignupCampaign;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.CampaignConcurrencyBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Unit testing CampaignsForm java class.
 * @author Evertec ET58344
 * @since 09-17-2020
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CustomerFacade.class, StaticMessageSource.class, CampaignsForm.class, Utils.class,
		ServletRequestUtils.class})
public class CampaignsFormUT extends AbstractModelAndViewTests  {

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private UserSession userSession;

	@Mock
	private CampaignConcurrencyBean campaignConcurrencyBean;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private SignupCampaign sc;

	@Mock
	private BindException error;

	@InjectMocks
	private CampaignsForm controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(ServletRequestUtils.class);
	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-17-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getAccounts()).thenReturn(new ArrayList<>());
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(request.getAttribute(anyString())).thenReturn(new Object());

		Map<String, Object> result = controller.referenceData(request, sc, error);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when OnSubmit
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-17-2020
	 * @exception Exception error
	 */
	@Test
	public void whenOnSubmit_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(ServletRequestUtils.getStringParameter(any(), anyString())).thenReturn("abc");
		when(userSession.getAccounts()).thenReturn(new ArrayList<>());
		PowerMockito.doNothing().when(campaignConcurrencyBean).subConcurrency();
		controller.setConcurrencyBean(campaignConcurrencyBean);

		ModelAndView mavResult = controller.onSubmit(request, response, sc, error);
		assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}
}
