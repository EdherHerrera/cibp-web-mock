package evertec.cibp.web.form;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.PersonalInformationChangesBean;
import evertec.cibp.web.session.UserSession;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, WebUtils.class, CustomerFacade.class})
public class PersonalInformationChangesFormUT extends AbstractModelAndViewTests {
	
	private HttpServletRequest request;
	
	private static final String USERSESSION = "userSession";
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private CustomerProfile profile;
	
	@Mock 
	private CustomerProfileService customerProfileService;
	
	@Mock 
	private UserSession userSession;
	
	@Mock 
	private CustomerFacade customerFacade;	
	
	@InjectMocks
	private PersonalInformationChangesForm controller;

	@Before
	public void setUp() throws Exception {
		
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		
		request = new MockHttpServletRequest();
	}

	@Test
	public void whenProcessFinish_GivenMBSD3336FlagFalse_ThenReturnMapWhenMBSD3363Null() throws Exception {
		PersonalInformationChangesBean bean = new PersonalInformationChangesBean();
		Map<Object, Object> informationQuestion =  new HashMap<>();
		informationQuestion.put("answer", "what");

		BindException error = new BindException(bean, "EnrollmentBean");
		when(WebUtils.getSessionAttribute(request, USERSESSION)).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(profile);
		when(userSession.getInformationQuestion()).thenReturn(informationQuestion);
		
		Map<String, Object> map = controller.referenceData(request, bean, error);

		assertEquals(map.get("MBSD3336"), null);
	}
	
	@Test
	@Ignore
	public void whenProcessFinish_GivenMBSD3336FlagTrue_ThenReturnMapWhenMBSD3363True() throws Exception {
		PersonalInformationChangesBean bean = new PersonalInformationChangesBean();
		Map<Object, Object> informationQuestion =  new HashMap<>();
		informationQuestion.put("answer", "what");

		BindException error = new BindException(bean, "EnrollmentBean");
		when(WebUtils.getSessionAttribute(request, USERSESSION)).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(profile);
		when(userSession.getInformationQuestion()).thenReturn(informationQuestion);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3336)).thenReturn(Boolean.TRUE);	
		Map<String, Object> map = controller.referenceData(request, bean, error);

		assertEquals(map.get("MBSD3336"), true);
	}
	

	
	
}
