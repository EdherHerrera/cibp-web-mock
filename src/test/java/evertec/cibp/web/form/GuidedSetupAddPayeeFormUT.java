package evertec.cibp.web.form;

import static org.mockito.Mockito.when;

import java.util.GregorianCalendar;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.test.web.AbstractModelAndViewTests;

import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Utils.class ,GregorianCalendar.class})
public class GuidedSetupAddPayeeFormUT extends AbstractModelAndViewTests {

	@Mock
	private MessageSource messageSource;

	@InjectMocks
	private GuidedSetupAddPayee controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(GregorianCalendar.class);
	}
	
	@Test
	public void whenGetTelepagoErrorDescriptor_GivenMessageError_ThenReturnMessageErrorSucces() throws Exception {
		controller.setMessageSource(messageSource);
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("OK");

		String valueValidate = Whitebox.invokeMethod(controller, "getTelepagoErrorDescriptor", "ebpp.module", "ens");
		assertEquals(valueValidate, "OK");
	}

}
