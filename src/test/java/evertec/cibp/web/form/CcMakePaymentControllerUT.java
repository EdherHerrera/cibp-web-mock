package evertec.cibp.web.form;

import bppr.tv.beans.accounts.records.CreditCardAccountRecord;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.CCPortalPayments;
import evertec.cibp.web.response.AccountTv;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;


import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.AccountBalances;
import bppr.tv.beans.accounts.types.CardAccount;
import bppr.tv.beans.balances.Balance;


import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.services.customer.modules.PaymentServiceModule;
import evertec.cibp.vs.beans.payments.Payee;
import evertec.cibp.vs.services.TransactionVaultServices;


import evertec.cibp.web.services.modules.PaymentsServiceModule;

import org.mockito.InjectMocks;

import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import java.math.BigDecimal;
import java.util.*;


/**
 * Unit testing CcMakePaymentController java class.
 * @author Evertec ET58344
 * @since 09-17-2020
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CustomerFacade.class, StaticMessageSource.class, CcMakePaymentController.class, Utils.class,
		CustomerServices.class,CustomerProfile.class ,PaymentsServiceModule.class,TransactionVaultServices.class, PaymentServiceModule.class})
public class CcMakePaymentControllerUT extends AbstractModelAndViewTests  {

	@InjectMocks
	private CcMakePaymentController ccMakePaymentController;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private UserSession userSession;

	@Mock
	private BindException error;

	@Mock
	private GlobalEntitlement globalEntitlement;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerFacade customerFacade;

	@Mock
	private CCPortalPayments ccMakePayment;

	@Mock
	private CustomerProfile customerProfile;

	@Spy
	private final CcMakePaymentController controller = new CcMakePaymentController();

	private CCPortalPayments ccPortalPayments;
	private BindException errors;
	private CustomerServices customerServices;


	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(PaymentsServiceModule.class);

		customerServices  = mock(CustomerServices.class);
		response = new MockHttpServletResponse();
		ccMakePaymentController = new CcMakePaymentController();
		ccMakePaymentController.setSessionForm(true);
		ccPortalPayments = mock(CCPortalPayments.class);
		errors = mock(BindException.class);

	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-17-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);
		when(customerFacade.getGlobalEntitlement(any(UserSession.class), any(EntitlementType.class)))
				.thenReturn(globalEntitlement);
		when(customerService.getHolidays()).thenReturn(null);
		when(ccMakePayment.getAccountId()).thenReturn("123456");
		when(ccMakePayment.getAccountInfo()).thenReturn(new TVFrontendAccount(new AccountTv())).thenReturn(null);
		PowerMockito.doReturn(new HashMap<>()).when(controller, "getAccountFromPaymentMap",
				any(HttpServletRequest.class), any(UserSession.class), any(Boolean.class));
		PowerMockito.doReturn(new HashMap<>()).when(controller, "getPaymentProcess",
				any(UserSession.class));


		Map<String, Object> result = controller.referenceData(request, ccMakePayment, error);
		assertNotNull(result);
	}

	/**
	 * when OnSubmit
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-17-2020
	 * @exception Exception error
	 */
	@Test
	public void whenOnSubmit_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("cookieAnalyticEnabled", true);

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(ccMakePayment.getAccountId()).thenReturn("123456");
		when(ccMakePayment.getAccountInfo()).thenReturn(new TVFrontendAccount(new AccountTv())).thenReturn(null);
		PowerMockito.doReturn(new HashMap<>()).when(controller, "getAccountFromPaymentMap",
				any(HttpServletRequest.class), any(UserSession.class), any(Boolean.class));
		PowerMockito.doReturn(new HashMap<>()).when(controller, "getPaymentProcess",
				any(UserSession.class));
		when(request.getParameter("id")).thenReturn("abc");
		when(request.getParameter("formAction")).thenReturn("abc");
		when(customerProfile.hasEntitlement(EntitlementType.ALERTS)).thenReturn(Boolean.FALSE);
		PowerMockito.doReturn(mav).when((SimpleFormController)controller, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any());

		ModelAndView mavResult = controller.onSubmit(request, response, ccMakePayment, error);
		assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}

	@Test
	public void when_GivenParamsSuccesfullOpertationTrue_ThenReturn_RefreshAccounts()throws Exception{

		CreditCardAccountRecord accountRecord = new CreditCardAccountRecord();
		AccountBalances accountBalances = new AccountBalances();
		Balance balance = new Balance("Avail",BigDecimal.ONE);
		accountBalances.add("Avail",balance);
		Calendar calendar = Calendar.getInstance();
		accountRecord.setDueDate( calendar);
		Account account = new CardAccount();
		account.setAccountID("4549540498591697");
		account.setRecords(accountRecord);
		account.setBalances(accountBalances);
		account.setAccountListBalance(balance);


		AccountBalances accountBalancesDist = new AccountBalances();
		Balance balanceDist = new Balance("Avail",BigDecimal.TEN);
		accountBalances.add("Avail",balance);

		accountRecord.setDueDate( calendar);
		Account accountDist = new CardAccount();
		accountDist.setAccountID("4549540498591697");
		accountDist.setRecords(accountRecord);
		accountDist.setBalances(accountBalancesDist);
		accountDist.setAccountListBalance(balanceDist);

		BankInformation bankInformation = new BankInformation();
		bankInformation.setBankId("Mi Banco");
		account.setBankInformation(bankInformation);

		accountDist.setBankInformation(bankInformation);

		GlobalEntitlement globalEntitlementaux = new GlobalEntitlement();
		globalEntitlementaux.setType(EntitlementType.BANKING);

		GlobalEntitlement globalEntitlement = mock(GlobalEntitlement.class);

		CustomerAccountPreferences customerAccountPreferences = new CustomerAccountPreferences();

		TVFrontendAccount tvFrontendAccountDist = new TVFrontendAccount(accountDist);

		TVFrontendAccount tvFrontendAccount = new TVFrontendAccount(account);
		tvFrontendAccount.setCustomerAccountPreferences(customerAccountPreferences);
		tvFrontendAccount.setNickname("Pago Test");
		tvFrontendAccount.setFrontEndId("123");


		List<TVFrontendAccount> tvFrontendAccounts = new ArrayList<>();

		tvFrontendAccounts.add(tvFrontendAccount);
		when((UserSession) WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);

		when(Utils.getRSALanguage(request)).thenReturn("en");




		when(userSession.getCustomerService()).thenReturn(customerServices);

		when(userSession.getAccounts()).thenReturn(tvFrontendAccounts);

		when((UserSession) WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		when(userSession.getCustomerService().getGlobalEntitlement(EntitlementType.EBILLS)).thenReturn(globalEntitlement);

		when(userSession.getLanguage()).thenReturn("en");

		MessageSource messageSource = mock(MessageSource.class);

		when(messageSource.getMessage("credit.card.portal.tab.make.payment.autopay.payamountmin", null, new Locale("en"))).thenReturn(" ");

		when(messageSource.getMessage("credit.card.portal.tab.make.payment.autopay.payamountcomplete", null, new Locale("en"))).thenReturn(" ");

		Payee accountPayee = new Payee();

		accountPayee.setBillingAccount("4549540498591697");

		List<Integer> integerList = new ArrayList<>();
		integerList.add(1);

		TVFrontEndPayee tvFrontEndPayee = new TVFrontEndPayee(accountPayee);
		tvFrontEndPayee.setFrontEndId("1");
		tvFrontEndPayee.setCustomerPayeeCodes(integerList);

		when(ccPortalPayments.getPayeeInfo()).thenReturn(tvFrontEndPayee);
		when(ccPortalPayments.getSinglePayAmount()).thenReturn(BigDecimal.ONE);
		when(ccPortalPayments.getSinglePayDate()).thenReturn(new Date());

		ccMakePaymentController.setMessageSource(messageSource);



		

		ccMakePaymentController.referenceData(request,ccPortalPayments,errors);



	}

	@Test
	public void when_GivenParamsSuccesfullOpertationFalse_ThenReturn_NotRefreshAccounts()throws Exception{

		CreditCardAccountRecord accountRecord = new CreditCardAccountRecord();
		Calendar calendar = Calendar.getInstance();
		accountRecord.setDueDate( calendar);
		Account account = new CardAccount();
		account.setAccountID("4549540498591697");
		account.setRecords(accountRecord);
		BankInformation bankInformation = new BankInformation();
		bankInformation.setBankId("Mi Banco");
		account.setBankInformation(bankInformation);

		GlobalEntitlement globalEntitlementaux = new GlobalEntitlement();
		globalEntitlementaux.setType(EntitlementType.BANKING);

		GlobalEntitlement globalEntitlement = mock(GlobalEntitlement.class);

		CustomerAccountPreferences customerAccountPreferences = new CustomerAccountPreferences();

		TVFrontendAccount tvFrontendAccount = new TVFrontendAccount(account);
		tvFrontendAccount.setCustomerAccountPreferences(customerAccountPreferences);
		tvFrontendAccount.setNickname("Pago Test");
		tvFrontendAccount.setFrontEndId("123");

		List<TVFrontendAccount> tvFrontendAccounts = new ArrayList<>();

		tvFrontendAccounts.add(tvFrontendAccount);
		when((UserSession) WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);

		when(Utils.getRSALanguage(request)).thenReturn("en");
		when(userSession.getCustomerService()).thenReturn(customerServices);

		when(userSession.getAccounts()).thenReturn(tvFrontendAccounts);

		when((UserSession) WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		when(userSession.getCustomerService().getGlobalEntitlement(EntitlementType.EBILLS)).thenReturn(globalEntitlement);

		when(userSession.getLanguage()).thenReturn("en");

		MessageSource messageSource = mock(MessageSource.class);

		when(messageSource.getMessage("credit.card.portal.tab.make.payment.autopay.payamountmin", null, new Locale("en"))).thenReturn(" ");

		when(messageSource.getMessage("credit.card.portal.tab.make.payment.autopay.payamountcomplete", null, new Locale("en"))).thenReturn(" ");

		Payee accountPayee = new Payee();

		accountPayee.setBillingAccount("4549540498591697");

		List<Integer> integerList = new ArrayList<>();
		integerList.add(1);

		TVFrontEndPayee tvFrontEndPayee = new TVFrontEndPayee(accountPayee);
		tvFrontEndPayee.setFrontEndId("1");
		tvFrontEndPayee.setCustomerPayeeCodes(integerList);

		when(ccPortalPayments.getPayeeInfo()).thenReturn(tvFrontEndPayee);
		when(ccPortalPayments.getSinglePayAmount()).thenReturn(BigDecimal.ONE);
		when(ccPortalPayments.getSinglePayDate()).thenReturn(new Date());


		ccMakePaymentController.setMessageSource(messageSource);




		ccMakePaymentController.referenceData(request,ccPortalPayments,errors);



	}

	@Test
	public void when_GivenParamsSuccesfullOpertationFalseAndAccount_ThenReturn_NotRefreshAccounts()throws Exception{

		CreditCardAccountRecord accountRecord = new CreditCardAccountRecord();
		Calendar calendar = Calendar.getInstance();
		accountRecord.setDueDate( calendar);
		Account account = new CardAccount();
		account.setAccountID("4549540498591697");
		account.setRecords(accountRecord);
		BankInformation bankInformation = new BankInformation();
		bankInformation.setBankId("Mi Banco");
		account.setBankInformation(bankInformation);

		GlobalEntitlement globalEntitlementaux = new GlobalEntitlement();
		globalEntitlementaux.setType(EntitlementType.BANKING);

		GlobalEntitlement globalEntitlement = mock(GlobalEntitlement.class);

		CustomerAccountPreferences customerAccountPreferences = new CustomerAccountPreferences();

		TVFrontendAccount tvFrontendAccount = new TVFrontendAccount(account);
		tvFrontendAccount.setCustomerAccountPreferences(customerAccountPreferences);
		tvFrontendAccount.setNickname("Pago Test");
		tvFrontendAccount.setFrontEndId("123");

		List<TVFrontendAccount> tvFrontendAccounts = new ArrayList<>();

		tvFrontendAccounts.add(tvFrontendAccount);
		when((UserSession) WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);

		when(Utils.getRSALanguage(request)).thenReturn("en");
		when(userSession.getCustomerService()).thenReturn(customerServices);

		when(userSession.getAccounts()).thenReturn(tvFrontendAccounts);

		when((UserSession) WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		when(userSession.getCustomerService().getGlobalEntitlement(EntitlementType.EBILLS)).thenReturn(globalEntitlement);

		when(userSession.getLanguage()).thenReturn("en");

		MessageSource messageSource = mock(MessageSource.class);

		when(messageSource.getMessage("credit.card.portal.tab.make.payment.autopay.payamountmin", null, new Locale("en"))).thenReturn(" ");

		when(messageSource.getMessage("credit.card.portal.tab.make.payment.autopay.payamountcomplete", null, new Locale("en"))).thenReturn(" ");

		Payee accountPayee = new Payee();

		accountPayee.setBillingAccount("4549540498591697");

		List<Integer> integerList = new ArrayList<>();
		integerList.add(1);

		TVFrontEndPayee tvFrontEndPayee = new TVFrontEndPayee(accountPayee);
		tvFrontEndPayee.setFrontEndId("1");
		tvFrontEndPayee.setCustomerPayeeCodes(integerList);

		when(ccPortalPayments.getPayeeInfo()).thenReturn(tvFrontEndPayee);
		when(ccPortalPayments.getSinglePayAmount()).thenReturn(BigDecimal.ONE);
		when(ccPortalPayments.getSinglePayDate()).thenReturn(new Date());


		ccMakePaymentController.setMessageSource(messageSource);



		when(ccPortalPayments.getAccountInfo()).thenReturn(tvFrontendAccount);

		ccMakePaymentController.referenceData(request,ccPortalPayments,errors);



	}


	@Test
	public void whenGetPaymentProcess_GivenLanguageES_ThenReturnPaymentProcessTypeIsNotNull () throws Exception {

		MessageSource messageSource = mock(MessageSource.class);
		when(messageSource.getMessage("credit.card.portal.tab.make.payment.autopay.payamountmin", null, new Locale("es"))).thenReturn(" ");
		when(messageSource.getMessage("credit.card.portal.tab.make.payment.autopay.payamountcomplete", null, new Locale("es"))).thenReturn(" ");
		when(userSession.getLanguage()).thenReturn("es");
		controller.setMessageSource(messageSource);
		Map<String, String> result = Whitebox.invokeMethod(controller, "getPaymentProcess", userSession);
		assertNotNull(result);
	}


}
