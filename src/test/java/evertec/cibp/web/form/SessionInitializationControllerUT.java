package evertec.cibp.web.form;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;

import evertec.cibp.core.models.customer.applications.ApplicationStatus;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.entitlement.PayrollEntitlement;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.DeviceRequest;

import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.types.CardAccount;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerWealthPreferred;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.CompleteLoginProcessRequest;
import evertec.cibp.core.services.responses.opac.CISOpacCustomerInfoResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.interruptions.OOBInterruptionForm;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * UT for SessionInitializationController
 * 
 * @author alizano
 * 
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ StaticMessageSource.class, WebUtils.class, Utils.class, OOBInterruptionForm.class})
public class SessionInitializationControllerUT extends AbstractModelAndViewTests {

    public static final String BROWSER_CHROME = "Chrome";
    private MockHttpServletRequest request;
    private HttpServletResponse response;

    public static String UNODOSTRES = "123456789";
    private static String IDA = "IDA";
    public static BigInteger PREMIAPERMID = BigInteger.valueOf(Long.parseLong("549126001001"));


    @Mock
    private DeviceRequest deviceRequest;
    
    @Mock
    private CustomerServices customerService;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private UserSession userSession;

    @Mock
    private BankingSession bankingSession;

    @Mock
    private CustomerEntitlement customerEntitlementAlerts;

    @InjectMocks
    private SessionInitializationController controller;

    @Mock
    protected CustomerFacade customerFacade;

    @Mock
    private AlertsEntitlement entitlement;

    @Mock
    private Set<AlertType> customeralerts;

    @Mock
    private CustomerProfileService customerProfileService;
    
    @Mock
    private CustomerWealthPreferred customerWealthPreferred;

    @Mock
    private PayrollEntitlement payrollEntitlement;

    @Before
    public void setUp() throws Exception {

        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Calendar.class);
        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(OOBInterruptionForm.class);
        

        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        customerEntitlementAlerts = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
        when(customerProfile.getEnrollmentAccountNumber()).thenReturn(UNODOSTRES);
        when(customerProfile.getEnrollmentAccountSubtype()).thenReturn(IDA);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        controller.setMaxStatements(Integer.MAX_VALUE);
        when(customerProfile.getEntitlementByType(EntitlementType.BANKING))
                .thenReturn(new CustomerEntitlement(EntitlementType.BANKING, EntitlementStatus.ENABLED));
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        request.addHeader("user-agent",
                "Mozilla/5.0 (Linux; U; Android 2.3.1; en-us; MID Build/GINGERBREAD) AppleWebKit/533.1 (KHTML, like Gecko) "
                        + "Version/4.0 Mobile Safari/533.1. Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) "
                        + "Chrome/44.0.2403.157 Safari/537.36.");
        when(customerService.completeLoginProcess(any(CompleteLoginProcessRequest.class)))
                .thenReturn(new ModifyCustomerProfileResponse());
        when(customerProfile.getBirthDate()).thenReturn(new Date());
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3336)).thenReturn(Boolean.TRUE);

    }

    @Test
    public void whenHandleRequest_GivenAlertMBSD3336FlagsTrue_ThenReturnModelAndViewForwardPortal() throws Exception {

        when(Utils.enrollmentAccountExists(userSession.getAccounts(), customerProfile)).thenReturn(Boolean.TRUE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class)))
                .thenReturn(new CustomerInteractionEvent());

        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS))
                .thenReturn(customerEntitlementAlerts);

        ModelAndView modelAndView = controller.handleRequestInternal(request, response);

        assertViewName(modelAndView, "forward:portal");
    }

    @Test
    public void whenHandleRequest_GivenAlertMBSD3336FlagsFalse_ThenReturnModelAndViewForwardPortal() throws Exception {

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3336)).thenReturn(Boolean.FALSE);
        when(Utils.enrollmentAccountExists(userSession.getAccounts(), customerProfile)).thenReturn(Boolean.TRUE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class)))
                .thenReturn(new CustomerInteractionEvent());
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:portal");
    }

    @Test
    public void whenHandleRequest_GivenEnrollmentAccountExistsFalse_ThenReturnModelAndViewForwardPortal()
            throws Exception {
        when(Utils.enrollmentAccountExists(userSession.getAccounts(), customerProfile)).thenReturn(Boolean.FALSE);

        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class)))
                .thenReturn(new CustomerInteractionEvent());
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:updEnrollAcct");
    }

    @Test
    public void whenHandleRequest_GivenMBSD3336True_EntitlementgetAlerts_null_ThenReturnModelAndViewForwardPortal()
            throws Exception {
        customerEntitlementAlerts = null;
        entitlement = null;
        when(Utils.enrollmentAccountExists(userSession.getAccounts(), customerProfile)).thenReturn(Boolean.TRUE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class)))
                .thenReturn(new CustomerInteractionEvent());
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS))
                .thenReturn(customerEntitlementAlerts);
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:portal");
    }

    @Test
    public void whenHandleRequest_GivenMBSD3336True_customeralertsEmpty_ThenReturnModelAndViewForwardPortal()
            throws Exception {
        entitlement = new AlertsEntitlement(customerEntitlementAlerts);
        customeralerts = new HashSet<AlertType>();
        entitlement.setAlerts(customeralerts);
        when(Utils.enrollmentAccountExists(userSession.getAccounts(), customerProfile)).thenReturn(Boolean.TRUE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class)))
                .thenReturn(new CustomerInteractionEvent());
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS))
                .thenReturn(customerEntitlementAlerts);
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:portal");
    }

    @Test
    public void whenHandleRequest_GivenMBSD3336True_without_any_AlertType_fail_update_ThenReturnModelAndViewForwardPortal()
            throws Exception {
        entitlement = new AlertsEntitlement(customerEntitlementAlerts);
        customeralerts = new HashSet<AlertType>();
        customeralerts.add(AlertType.PAYMENTS_EMAIL);
        customeralerts.add(AlertType.TRANSFERS_EMAIL);
        entitlement.setAlerts(customeralerts);
        when(Utils.enrollmentAccountExists(userSession.getAccounts(), customerProfile)).thenReturn(Boolean.TRUE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class)))
                .thenReturn(new CustomerInteractionEvent());
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS))
                .thenReturn(customerEntitlementAlerts);
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:portal");
    }

    @Test
    public void whenHandleRequest_GivenMBSD3336isTrue_without_any_AlertType_ThenReturnModelAndViewForwardPortal()
            throws Exception {

        entitlement = new AlertsEntitlement(customerEntitlementAlerts);
        customeralerts = new HashSet<AlertType>();
        customeralerts.add(AlertType.PAYMENTS_EMAIL);
        customeralerts.add(AlertType.TRANSFERS_EMAIL);
        entitlement.setAlerts(customeralerts);
        when(Utils.enrollmentAccountExists(userSession.getAccounts(), customerProfile)).thenReturn(Boolean.TRUE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class)))
                .thenReturn(new CustomerInteractionEvent());
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS))
                .thenReturn(customerEntitlementAlerts);
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:portal");
    }

    @Test
    public void whenHandleRequest_GivenMBSD3336True_and_all_alerts_ThenReturnModelAndViewForwardPortal()
            throws Exception {

        entitlement = new AlertsEntitlement(customerEntitlementAlerts);
        customeralerts = new HashSet<AlertType>();
        customeralerts.add(AlertType.CHANGE_PASS_EMAIL);
        customeralerts.add(AlertType.ACCESS_BLOCK_EMAIL);
        customeralerts.add(AlertType.QUESTIONS_BLOCK_EMAIL);
        customeralerts.add(AlertType.QUESTIONS_EDIT_EMAIL);
        customeralerts.add(AlertType.EMAIL_EDIT_EMAIL);
        customeralerts.add(AlertType.PHONE_EDIT_EMAIL);
        customeralerts.add(AlertType.ADRESS_EDIT_EMAIL);
        customeralerts.add(AlertType.MOBILE_PHONE_EDIT_EMAIL);
        customeralerts.add(AlertType.NEW_FINGERPRINT_EMAIL);
        
        entitlement.setAlerts(customeralerts);
        when(Utils.enrollmentAccountExists(userSession.getAccounts(), customerProfile)).thenReturn(Boolean.TRUE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class)))
                .thenReturn(new CustomerInteractionEvent());
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.ALERTS))
                .thenReturn(customerEntitlementAlerts);
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:portal");
    }

    @Test
    public void whenHandleRequestInternal_GivenMBSE_1980_Flag_True_And_User_AgentIsWeb() throws Exception {

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.TRUE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:updEnrollAcct");
    }
    
    @Test
    public void whenHandleRequestInternal_GivenMBSE_1980_Flag_True_And_User_AgentIsAndroid() throws Exception {

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.TRUE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.ANDROID);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:updEnrollAcct");
    }
    
    @Test
    public void whenHandleRequestInternal_GivenMBSE_1980_Flag_True_And_User_AgentIsIphone() throws Exception {

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.TRUE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:updEnrollAcct");
    }
    
    @Test
    public void whenHandleRequestInternal_GivenMBSE_1980_Flag_True_And_User_AgentIsIpad() throws Exception {

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.TRUE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPAD);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:updEnrollAcct");
    }
    
    @Test
    public void whenHandleRequestInternal_GivenMBSE_1980_Flag_False_And_User_AgentIsWeb() throws Exception {

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.FALSE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:updEnrollAcct");
    }
    
    @Test
    public void whenHandleRequestInternal_GivenMBSE_1980_Flag_False_And_User_AgentIsAndroid() throws Exception {

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.FALSE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.ANDROID);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:updEnrollAcct");
    }
    
    @Test
    public void whenHandleRequestInternal_GivenMBSE_1980_Flag_False_And_User_AgentIsIphone() throws Exception {

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.FALSE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:updEnrollAcct");
    }
    
    @Test
    public void whenHandleRequestInternal_GivenMBSE_1980_Flag_False_And_User_AgentIsIpad() throws Exception {

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.FALSE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPAD);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertViewName(modelAndView, "forward:updEnrollAcct");
    }
    
    @Test
    public void whenHandleRequestInternal_GivenMBCA_1559_Flag_True_And_Get_PremiaAccounts() throws Exception {
    	userSession.setCustomerService(customerService);
    	CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
    	cisResponse.setCustomerCodes(getCustomerCodesList());
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getCISCustomerInfo(any())).thenReturn(cisResponse);
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertNotNull(modelAndView);
    }

    @Test
    public void whenHandleRequestInternal_GivenMBCA_1559_Flag_True_And_Non_PremiaAccounts() throws Exception {
    	userSession.setCustomerService(customerService);
    	CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
    	cisResponse.setCustomerCodes(getCustomerCodesWithoutPremiaCodeList());
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getCISCustomerInfo(any())).thenReturn(cisResponse);
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertNotNull(modelAndView);
    }

    @Test
    public void whenHandleRequestInternal_GivenMBCA_1559_Flag_False_And_Return_Portal() throws Exception {

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.FALSE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertNotNull(modelAndView);
    }



    /**
     * Test the happy path of user that has premia account and has been redirected from TLP
     * Created for MBCA-1926
     * @throws Exception
     */
    @Test
    @Ignore
    public void whenHandleRequest_GivenIsRewardPointsFlagsTrueAndUserPremia_ThenReturnModelAndViewForwardPortalAndPremiaRedirect() throws Exception {
        userSession.setCustomerService(customerService);
        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
        cisResponse.setCustomerCodes(getCustomerCodesList());

        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getCISCustomerInfo(any())).thenReturn(cisResponse);
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());
        when(userSession.getCustomerService()).thenReturn(customerService);

        //Force the needed Feature flags
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.FALSE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);

        //Force redirect reward points. Param is pushed by MBCA-2019
        when(userSession.isRewardPoints()).thenReturn(true);
        when(userSession.getPremiaAccounts()).thenReturn(getPremiaAccounts());
        when(customerService.getPriorityPremiaAccount(any())).thenReturn(getPremiaAccounts().get(1));

        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertNotNull(modelAndView);
        assertModelAttributeValue(modelAndView,"cardNumber",getPremiaAccounts().get(1).getFrontEndId());
        assertModelAttributeValue(modelAndView,"redirectToPremia",true);
        assertViewName(modelAndView, "forward:portal");
    }

    /**
     * Test the happy path of user that has premia account and has not been redirected from TLP
     *  Created for MBCA-1926
     * @throws Exception
     */
    @Test
    public void whenHandleRequest_GivenIsRewardPointsFlagsFalseAndUserPremia_ThenReturnModelAndViewForwardPortalAndWithoutPremiaParam() throws Exception {
        userSession.setCustomerService(customerService);
        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
        cisResponse.setCustomerCodes(getCustomerCodesList());

        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getCISCustomerInfo(any())).thenReturn(cisResponse);
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());
        when(userSession.getCustomerService()).thenReturn(customerService);

        //Force the needed Feature flags
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.FALSE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);

        //Force not redirect reward param. Param is pushed by MBCA-2019
        when(userSession.isRewardPoints()).thenReturn(false);
        when(userSession.getPremiaAccounts()).thenReturn(getPremiaAccounts());
        when(customerService.getPriorityPremiaAccount(any())).thenReturn(getPremiaAccounts().get(1));

        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertNotNull(modelAndView);
        assertNull(modelAndView.getModel().get("cardNumber"));
        assertNull(modelAndView.getModel().get("redirectToPremia"));
        assertViewName(modelAndView, "forward:portal");
    }


    /**
     * Feature: MBCA-1925
     * Description: Mi Banco redirect Mi Banco (redirect reward points param not exist), pop-up hidden
     *
     * @throws Exception
     */
    @Test
    public void whenHandleRequestInternal_GivenIsNotRewardPointsFlag_ThenThereIsNoNonPremiaFlag() throws Exception {
        //Given
        userSession.setCustomerService(customerService);
        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
        cisResponse.setCustomerCodes(getCustomerCodesWithoutPremiaCodeList());

        //When
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.FALSE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getCISCustomerInfo(any())).thenReturn(cisResponse);
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());

        //Then
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertNotNull(modelAndView);
        assertNull(modelAndView.getModel().get(SessionInitializationController.NON_PREMIA_STATE_FLAG));
        assertViewName(modelAndView, "forward:portal");
    }


    /**
     * Feature: MBCA-1925
     * Description: Mi Banco redirect from TLP page (redirect reward points param), pop-up hidden when the NonPremiaFlag is not enabled because
     * the user HAS premia account
     *
     * @throws Exception
     */
    @Test
    @Ignore
    public void whenHandleRequestInternal_GivenThereIsRewardPointsFlagAndHasCustomerCodes_ThenNonPremiaFlagIsFalse() throws Exception {
        //Given
        userSession.setCustomerService(customerService);
        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
        cisResponse.setCustomerCodes(getPremiaCustomerCodesList());

        //When
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.FALSE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getCISCustomerInfo(any())).thenReturn(cisResponse);
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());

        //Force redirect reward points. Param is pushed by MBCA-2019
        when(userSession.isRewardPoints()).thenReturn(true);
        when(userSession.getPremiaAccounts()).thenReturn(getPremiaAccounts());
        when(customerService.getPriorityPremiaAccount(any())).thenReturn(getPremiaAccounts().get(1));


        //Then
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertNotNull(modelAndView);
        assertModelAttributeValue(modelAndView,SessionInitializationController.NON_PREMIA_STATE_FLAG,false);
        assertViewName(modelAndView, "forward:portal");
    }

    /**
     * Feature: MBCA-1925
     * Description: Mi Banco redirect from TLP page (redirect reward points param), pop-up opened when the NonPremiaFlag is enabled because
     * the user has NO premia account
     *
     * @throws Exception
     */
    @Test
    @Ignore
    public void whenHandleRequestInternal_GivenThereIsRewardPointsFlagAndHasNoCustomerCodes_ThenNonPremiaFlagIsTrue() throws Exception {
        //Given
        userSession.setCustomerService(customerService);
        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
        cisResponse.setCustomerCodes(getCustomerCodesWithoutPremiaCodeList());

        //When
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.FALSE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);
        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getCISCustomerInfo(any())).thenReturn(cisResponse);
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());

        //Force redirect reward points. Param is pushed by MBCA-2019
        when(userSession.isRewardPoints()).thenReturn(true);

        //Then
        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertNotNull(modelAndView);
        assertModelAttributeValue(modelAndView,SessionInitializationController.NON_PREMIA_STATE_FLAG,true);
        assertViewName(modelAndView, "forward:portal");
    }
    
    /**
     * Test the happy path of user that has premia account and has been redirected from TLP
     * Created for MBCA-remove call datapower
     * @throws Exception
     */
    @Test
    @Ignore
    public void whenHandleRequestInternal_GivenIsRewardPointsAndIsHasPremia_ThenReturnModelAndViewRedirectToPremiaTrueAndHasPremia() throws Exception {
    	
    	List<String> codes = new ArrayList<>();
    	codes.add("016"); codes.add("516");
    	//when
    	when(userSession.getCustWealthPreferred()).thenReturn(customerWealthPreferred); 
    	when(customerWealthPreferred.getCodes()).thenReturn(codes);
    	when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());

        //Force the needed Feature flags
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.FALSE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);        

        //Force redirect reward points. Param is pushed by MBCA-2019
        when(userSession.isRewardPoints()).thenReturn(true);
        when(userSession.getPremiaAccounts()).thenReturn(getPremiaAccounts());
        when(customerService.getPriorityPremiaAccount(any())).thenReturn(getPremiaAccounts().get(1));

        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertNotNull(modelAndView);
        assertModelAttributeValue(modelAndView,SessionInitializationController.NON_PREMIA_STATE_FLAG,false);
        assertModelAttributeValue(modelAndView,"redirectToPremia",true);
        ArgumentCaptor<Boolean> propertiesCaptor =
                ArgumentCaptor.forClass(Boolean.class);

        verify(userSession, times(1)).setHasPremia(propertiesCaptor.capture());
        Boolean sessionValue = propertiesCaptor.getValue();
        assertTrue(sessionValue);        
    }
    
    /**
     * Test if client doesn't have customer codes premia, no redirected from TLP
     * Created for MBCA-remove call datapower
     * @throws Exception
     */
    @Test
    @Ignore
    public void whenHandleRequestInternal_GivenIsRewardPointsAndHasNoPREMIA_ThenReturnModelAndViewRedirectToPremiaFalseAndHasNoPremia() throws Exception {
    	
    	List<String> codes = new ArrayList<>();
    	codes.add("111"); codes.add("222");
    	//when
    	when(userSession.getCustWealthPreferred()).thenReturn(customerWealthPreferred); 
    	when(customerWealthPreferred.getCodes()).thenReturn(codes);
    	when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());

        //Force the needed Feature flags
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.FALSE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);        

        //Force redirect reward points. Param is pushed by MBCA-2019
        when(userSession.isRewardPoints()).thenReturn(true);
        when(userSession.getPremiaAccounts()).thenReturn(getPremiaAccounts());
        when(customerService.getPriorityPremiaAccount(any())).thenReturn(getPremiaAccounts().get(1));

        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertNotNull(modelAndView);
        assertModelAttributeValue(modelAndView,SessionInitializationController.NON_PREMIA_STATE_FLAG,true);
        ArgumentCaptor<Boolean> propertiesCaptor =
                ArgumentCaptor.forClass(Boolean.class);

        verify(userSession, times(1)).setHasPremia(propertiesCaptor.capture());
        Boolean sessionValue = propertiesCaptor.getValue();
        assertFalse(sessionValue);        
    }
    
    /**
     * Test if client doesn't have customer codes premia, no redirected from TLP
     * Created for MBCA-remove call datapower
     * @throws Exception
     */
    @Test
    public void whenHandleRequestInternal_GivenCustomerWealthPreferredisNull_ThenReturnModelAndViewCustWealthPreferredisNull() throws Exception {
    	
    	//when
    	when(userSession.getCustWealthPreferred()).thenReturn(null); 
    	when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());

        //Force the needed Feature flags
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.FALSE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);        

        controller.handleRequestInternal(request, response);
        ArgumentCaptor<Boolean> propertiesCaptor =
                ArgumentCaptor.forClass(Boolean.class);

        verify(userSession, times(1)).setHasPremia(propertiesCaptor.capture());
        Boolean sessionValue = propertiesCaptor.getValue();
        assertFalse(sessionValue);        
    }
    
    /**
     * Test if client doesn't have customer codes premia, no redirected from TLP, redirected from TLP
     * Created for MBCA-remove call datapower
     * @throws Exception
     */
    @Test
    @Ignore
    public void whenHandleRequestInternal_GivenRewardPointsFalseAndIsHasPremia_ThenReturnModelAndViewNoRewardPoints() throws Exception {
    	
    	List<String> codes = new ArrayList<>();
    	codes.add("016"); codes.add("516");
    	//when
    	when(userSession.getCustWealthPreferred()).thenReturn(customerWealthPreferred); 
    	when(customerWealthPreferred.getCodes()).thenReturn(codes);
    	when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());

        //Force the needed Feature flags
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.FALSE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);        

        //Force redirect reward points. Param is pushed by MBCA-2019
        when(userSession.isRewardPoints()).thenReturn(false);

        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertNotNull(modelAndView);
        ArgumentCaptor<Boolean> propertiesCaptor =
                ArgumentCaptor.forClass(Boolean.class);

        verify(userSession, times(1)).setHasPremia(propertiesCaptor.capture());
        Boolean sessionValue = propertiesCaptor.getValue();
        assertTrue(sessionValue);        
    }
    
    /**
     * Test the happy path of user that has premia account and has been redirected from TLP
     * Created for MBCA-remove call datapower
     * @throws Exception
     */
    @Test
    public void whenHandleRequestInternal_GivenPremiaFlagFalse_ThenReturnModelAndViewPremiaFlagFalse() throws Exception {
    	
    	//when
    	when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());

        //Force the needed Feature flags
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.FALSE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.FALSE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);        

        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        assertNotNull(modelAndView);
        assertViewName(modelAndView, "forward:portal");
        
    }        

    /**
	 * Get a list of dummy Customer codes.
	 * @return
	 */
	public static List<String> getCustomerCodesList() {

		return Stream.of("16", "11", "181", "0",
				"9").collect(Collectors.toList());

	}

	/**
	 * Get a list of dummy Customer codes.
	 * @return
	 */
	public static List<String> getPremiaCustomerCodesList() {

		return Stream.of("016").collect(Collectors.toList());
	}

	/**
	 * Get a empty list of dummy Customer codes.
	 * @return
	 */
	public static List<String> getCustomerCodesWithoutPremiaCodeList() {
		return  Stream.of("11", "181", "0",
				"9").collect(Collectors.toList());
	}
    /**
     * Return Premia accounts
     * Created for MBCA-1926
     * @return List<TVFrontendAccount>
     */
    private List<TVFrontendAccount> getPremiaAccounts() {
        CardAccount cardAccount1 = new CardAccount();
        cardAccount1.setAccountID("4549000973559305");
        cardAccount1.setSubType("CCA");
        cardAccount1.setBankInformation(new BankInformation());
        CardAccount cardAccount2 = new CardAccount();
        cardAccount2.setAccountID("4549542114539357");
        cardAccount2.setSubType("CCA");
        cardAccount2.setBankInformation(new BankInformation());
        return Stream.of(new MockTVFrontendAccount(cardAccount1,"asdadfasdfasdfdsf"),
                new MockTVFrontendAccount(cardAccount2,"werqowierwqerqwer"))
                .collect(Collectors.toList());
    }

    /**
     * Test the happy path of getFPSDateFirstSignon()
     * Created for MBIM-1489
     * @throws Exception
     */
    @Test
    public void whenHandleRequest_getFPSDateFirstSignon_HappyPath() throws Exception {
        userSession.setCustomerService(customerService);
        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
        cisResponse.setCustomerCodes(getCustomerCodesList());


        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getCISCustomerCode(any())).thenReturn(cisResponse).thenThrow(Exception.class );
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerProfile.hasEntitlement(EntitlementType.PAYROLL)).thenReturn(true);

        when(WebUtils.getSessionAttribute(request, "fromEnrollment")).thenReturn(true);

        when(customerProfile.getEntitlementByType(EntitlementType.PAYROLL)).thenReturn(new CustomerEntitlement());


        //Force the needed Feature flags
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE)).thenReturn(Boolean.TRUE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);

        when(customerProfile.getEnrollmentAccountProductId()).thenReturn("088");

        //Force redirect reward points. Param is pushed by MBCA-2019
        when(userSession.isRewardPoints()).thenReturn(true);
        when(userSession.getPremiaAccounts()).thenReturn(getPremiaAccounts());
        when(customerService.getPriorityPremiaAccount(any())).thenReturn(getPremiaAccounts().get(1));

        when(customerProfile.getUserInterface()).thenReturn(UserInterface.COMMERCIAL);

        when(userSession.getFraudPrevAppStatus()).thenReturn(ApplicationStatus.APPROVED);

        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.PAYROLL)).thenReturn(customerEntitlementAlerts);

        when(payrollEntitlement.getFPSDateFirstSignon()).thenReturn("2021-06-01");
        assertEquals("2021-06-01",payrollEntitlement.getFPSDateFirstSignon());
        when(payrollEntitlement.isFPSShowNewBubbleAlert()).thenReturn(true);
        assertTrue(payrollEntitlement.isFPSShowNewBubbleAlert());


        ModelAndView modelAndView = controller.handleRequestInternal(request, response);

    }
    /**
     * Test the happy path of getFPSDateFirstSignon()
     * Created for MBIM-1489
     * @throws Exception
     */
    @Test
    public void whenHandleRequest_getFPSDateFirstSignon_ExepctionPath() throws Exception {
        userSession.setCustomerService(customerService);
        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
        cisResponse.setCustomerCodes(getCustomerCodesList());


        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getCISCustomerCode(any())).thenReturn(cisResponse).thenThrow(Exception.class );
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerProfile.hasEntitlement(EntitlementType.PAYROLL)).thenReturn(true);

        when(WebUtils.getSessionAttribute(request, "fromEnrollment")).thenReturn(true);

        when(customerProfile.getEntitlementByType(EntitlementType.PAYROLL)).thenReturn(new CustomerEntitlement());


        //Force the needed Feature flags
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE)).thenReturn(Boolean.TRUE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);

        when(customerProfile.getEnrollmentAccountProductId()).thenReturn("088");

        //Force redirect reward points. Param is pushed by MBCA-2019
        when(userSession.isRewardPoints()).thenReturn(true);
        when(userSession.getPremiaAccounts()).thenReturn(getPremiaAccounts());
        when(customerService.getPriorityPremiaAccount(any())).thenReturn(getPremiaAccounts().get(1));

        when(customerProfile.getUserInterface()).thenReturn(UserInterface.COMMERCIAL);

        when(userSession.getFraudPrevAppStatus()).thenReturn(ApplicationStatus.APPROVED);

        when(payrollEntitlement.getFPSDateFirstSignon()).thenThrow(Exception.class);
        when(payrollEntitlement.isFPSShowNewBubbleAlert()).thenReturn(true);
        assertTrue(payrollEntitlement.isFPSShowNewBubbleAlert());


        ModelAndView modelAndView = controller.handleRequestInternal(request, response);

    }
    /**
     * Test the happy path of getFPSDateFirstSignon()
     * Created for MBIM-1489
     * @throws Exception
     */
    @Test
    public void whenHandleRequest_getFPSDateFirstSignon_AlternatePath() throws Exception {
        userSession.setCustomerService(customerService);
        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
        cisResponse.setCustomerCodes(getCustomerCodesList());

        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getCISCustomerCode(any())).thenReturn(cisResponse).thenThrow(Exception.class );
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());
        when(userSession.getCustomerService()).thenReturn(customerService);

        when(customerProfile.hasEntitlement(EntitlementType.PAYROLL)).thenReturn(true);

        when(WebUtils.getSessionAttribute(request, "fromEnrollment")).thenReturn(true);

        when(customerProfile.getEntitlementByType(EntitlementType.PAYROLL)).thenReturn(new CustomerEntitlement());

        //Force the needed Feature flags
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE)).thenReturn(Boolean.TRUE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);

        when(customerProfile.getEnrollmentAccountProductId()).thenReturn("088");

        //Force redirect reward points. Param is pushed by MBCA-2019
        when(userSession.isRewardPoints()).thenReturn(true);
        when(userSession.getPremiaAccounts()).thenReturn(getPremiaAccounts());
        when(customerService.getPriorityPremiaAccount(any())).thenReturn(getPremiaAccounts().get(1));

        when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);


        when(userSession.getFraudPrevAppStatus()).thenReturn(ApplicationStatus.APPROVED);

        ModelAndView modelAndView = controller.handleRequestInternal(request, response);

    }
    
    /**
     * Test the happy path of getFPSDateFirstSignon()
     * Created for MBIM-1489
     * @throws Exception
     */
    @Test
    public void whenHandleRequest_givenLogin_thenValidateApplyForInterruptionPageWebVersion() throws Exception {
        userSession.setCustomerService(customerService);
        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
        cisResponse.setCustomerCodes(getCustomerCodesList());

        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getCISCustomerCode(any())).thenReturn(cisResponse).thenThrow(Exception.class );
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());
        when(userSession.getCustomerService()).thenReturn(customerService);

        when(customerProfile.hasEntitlement(EntitlementType.PAYROLL)).thenReturn(true);

        when(WebUtils.getSessionAttribute(request, "fromEnrollment")).thenReturn(true);

        when(customerProfile.getEntitlementByType(EntitlementType.PAYROLL)).thenReturn(new CustomerEntitlement());

        //Force the needed Feature flags
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE)).thenReturn(Boolean.TRUE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);

        when(customerProfile.getEnrollmentAccountProductId()).thenReturn("088");

        //Force redirect reward points. Param is pushed by MBCA-2019
        when(userSession.isRewardPoints()).thenReturn(true);
        when(userSession.getPremiaAccounts()).thenReturn(getPremiaAccounts());
        when(customerService.getPriorityPremiaAccount(any())).thenReturn(getPremiaAccounts().get(1));

        when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);


        when(userSession.getFraudPrevAppStatus()).thenReturn(ApplicationStatus.APPROVED);
        
        ModelAndView oobInterruption = new ModelAndView();
        when(OOBInterruptionForm.applyForInterruptionPageWebVersion(any())).thenReturn(oobInterruption);

        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
        
        assertEquals(oobInterruption, modelAndView);

    }


}

/**
 * This class create a Mock account to override the required frontEndId
 * to generate de redirect login
 * Created for MBCA-1926
 * @see BaseObject
 */
class MockTVFrontendAccount extends TVFrontendAccount {
    private String frontendId;

    public MockTVFrontendAccount(Account tvAccount, String frontendId) {
        super(tvAccount);
        this.frontendId = frontendId;
    }

    public String getFrontEndId(){
        return this.frontendId;
    }
}