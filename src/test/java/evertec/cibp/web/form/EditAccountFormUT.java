package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.Errors;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.UserAccount;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ Utils.class, WebUtils.class, StaticMessageSource.class, EditAccountForm.class, CustomerServices.class })
public class EditAccountFormUT extends AbstractModelAndViewTests {
	
	private HttpServletRequest request;
	private Object command;
	private Errors errors;
	
	
	@Mock
	private StaticMessageSource staticMessageSource;

	@Mock
	private MessageSource messageSource;

	@Mock
	private UserSession userSession;

	@Mock
	private TVAccountProductDefinition productDefinition;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private CustomerEntitlement ezstmtEntitlement;
	
	@Mock
	private CustomerInteractionEvent customerInteractionEvent; 
	
	@Mock
	private CustomerServices customerService;
		
	
	@InjectMocks
	private  EditAccountForm edit;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);		
		//PowerMockito.mockStatic(CustomerServices.class);
		//customerService = PowerMockito.mock(CustomerServices.class);
		
		request = new MockHttpServletRequest();
		command = new Object();
		
		edit = PowerMockito.spy(new EditAccountForm());
	
	}
	
	@Test
	
	public void whenReferenceData_giveebpp2UserAccountsNotNull_thenReturnObjectNotNull()throws Exception {
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getAccounts()).thenReturn(null);
		when(userSession.getTrfAccounts()).thenReturn(null);
		when(customerProfile.getEntitlementByType(EntitlementType.EZSTMT)).thenReturn(ezstmtEntitlement);
		when(userSession.getCustomerProfile().hasEntitlement(EntitlementType.EZSTMT)).thenReturn(false);
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerInteractionEvent);
		when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(null);		
		when(userSession.isEbpp2UserIdLookedUp()).thenReturn(true);
		when(userSession.getEbpp2UserId()).thenReturn("ebpp2");
        List<UserAccount> accounts = new ArrayList<>();
		when(userSession.getEbpp2UserAccounts()).thenReturn(accounts);
		
		
		//prueba submetodo
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerInteractionEvent);
		when(userSession.getLogEbppStatus()).thenReturn(true);
		//		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(false);		
		when(customerProfile.hasEntitlement(EntitlementType.MOBILE_BANKING)).thenReturn(false);
		when(userSession.hasVirginIslandAccount()).thenReturn(false);
		
		List<PIFExternalAccount> pif = new LinkedList<PIFExternalAccount>();
		when((userSession.getCustomerProfile().getTaxId())).thenReturn("");
		//when(customerService.getPIFExternalAccounts(userSession.getCustomerProfile().getTaxId())).thenReturn(pif);//*************************************************//
		when(customerProfile.getTaxId()).thenReturn("123456");
		when(customerService.getPIFExternalAccounts(any(String.class))).thenReturn(pif);
				
				
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD792)).thenReturn(true);
		when(userSession.getInterbankFundingAllowed()).thenReturn(false);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)).thenReturn(false);
		
		
		
		
		
		
		assertNotNull(edit.referenceData(request, command, errors));		
	}
	
	@Test
	public void whenReferenceData_giveebpp2UserAccountsisNull_thenReturnObjectNotNull()throws Exception {
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getAccounts()).thenReturn(null);
		when(userSession.getTrfAccounts()).thenReturn(null);
		when(customerProfile.getEntitlementByType(EntitlementType.EZSTMT)).thenReturn(ezstmtEntitlement);
		when(userSession.getCustomerProfile().hasEntitlement(EntitlementType.EZSTMT)).thenReturn(false);
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerInteractionEvent);
		when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(null);		
		when(userSession.isEbpp2UserIdLookedUp()).thenReturn(true);
		when(userSession.getEbpp2UserId()).thenReturn("ebpp2");
        //List<UserAccount> accounts = new ArrayList<>();
		when(userSession.getEbpp2UserAccounts()).thenReturn(null);
		
		
		//prueba submetodo
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerInteractionEvent);
		when(userSession.getLogEbppStatus()).thenReturn(true);
		//		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(false);		
		when(customerProfile.hasEntitlement(EntitlementType.MOBILE_BANKING)).thenReturn(false);
		when(userSession.hasVirginIslandAccount()).thenReturn(false);
		
		List<PIFExternalAccount> pif = new LinkedList<PIFExternalAccount>();
		when((userSession.getCustomerProfile().getTaxId())).thenReturn("");
		//when(customerService.getPIFExternalAccounts(userSession.getCustomerProfile().getTaxId())).thenReturn(pif);//*************************************************//
		when(customerProfile.getTaxId()).thenReturn("123456");
		when(customerService.getPIFExternalAccounts(any(String.class))).thenReturn(pif);
				
				
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD792)).thenReturn(true);
		when(userSession.getInterbankFundingAllowed()).thenReturn(false);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)).thenReturn(false);
		
		
		
		
		
		
		assertNotNull(edit.referenceData(request, command, errors));		
	}
		
}