package evertec.cibp.web.form.validator;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.validation.BindException;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.EnrollmentBean;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StaticMessageSource.class, WebUtils.class })
public class EnrollmentFormPageValidatorUT {
		
	//private EnrollmentBean enrollmentBean;

	@Before
	public void setUp() throws Exception {
				
		//PowerMockito.mockStatic(CustomerFlagType.class);
		//PowerMockito.spy(controller);
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(WebUtils.class);

		
		
	}
	
	@Test
	public void whenValidateCaseFourtHaveValidAgreementThenReturnTrue() {
		
		EnrollmentFormPageValidator enrollValidator = new EnrollmentFormPageValidator(4, UserAgentType.ANDROID);
		EnrollmentBean enrollmentBean = new EnrollmentBean();
		enrollmentBean.setBsmartAgreement("bsmart Agrement..");
		enrollmentBean.setAgreement("agree");
		BindException error = new BindException(enrollmentBean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		enrollValidator.validate(enrollmentBean, error);
		assertTrue(0 == error.getFieldErrorCount());
	}
	
	@Test
	public void whenValidateCaseFourtHaveValidAgreementWithEpayrollFlagDisabledThenReturnTrue() {
		
		EnrollmentFormPageValidator enrollValidator = new EnrollmentFormPageValidator(4, UserAgentType.IPHONE);
		EnrollmentBean enrollmentBean = new EnrollmentBean();
		enrollmentBean.setBsmartAgreement("bsmart Agrement..");
		enrollmentBean.setAgreement("agree");
		BindException error = new BindException(enrollmentBean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(false);
		enrollValidator.validate(enrollmentBean, error);
		assertTrue(0 == error.getFieldErrorCount());
	}
	
	@Test
	public void whenValidateCaseFourtHaveInValidAgreementThenReturnTrue() {
		
		EnrollmentFormPageValidator enrollValidator = new EnrollmentFormPageValidator(4, UserAgentType.ANDROID);
		EnrollmentBean enrollmentBean = new EnrollmentBean();
		enrollmentBean.setBsmartAgreement("");
		enrollmentBean.setAgreement("");
		BindException error = new BindException(enrollmentBean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		enrollValidator.validate(enrollmentBean, error);
		assertTrue(0 != error.getFieldErrorCount());
	}
	
	@Test
	public void whenValidateCaseFourtHaveInValidBsmartAgreementButUserNoAgreeThenReturnTrue() {
		
		EnrollmentFormPageValidator enrollValidator = new EnrollmentFormPageValidator(4, UserAgentType.ANDROID);
		EnrollmentBean enrollmentBean = new EnrollmentBean();
		enrollmentBean.setBsmartAgreement("bsmart Agrement..");
		enrollmentBean.setAgreement("");
		BindException error = new BindException(enrollmentBean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		enrollValidator.validate(enrollmentBean, error);
		assertTrue(0 != error.getFieldErrorCount());
	}
	
	@Test
	public void whenValidateCaseThreeHaveValidAnswersThenReturnTrue() {
		
		EnrollmentFormPageValidator enrollValidator = new EnrollmentFormPageValidator(3, UserAgentType.ANDROID);
		EnrollmentBean enrollmentBean = new EnrollmentBean();
		enrollmentBean.setQuestion1("question1");
		enrollmentBean.setQuestion2("question2");
		enrollmentBean.setQuestion3("question3");
		enrollmentBean.setAnswer1("casa");
		enrollmentBean.setAnswer1confirm("casa");
		enrollmentBean.setAnswer2("maria");
		enrollmentBean.setAnswer2confirm("maria");
		enrollmentBean.setAnswer3("america");
		enrollmentBean.setAnswer3confirm("america");
		enrollmentBean.setComputerType("public");
		BindException error = new BindException(enrollmentBean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		enrollValidator.validate(enrollmentBean, error);
		assertTrue(0 == error.getFieldErrorCount());
	}
	
	@Test
	public void whenValidateCaseThreeHaveValidAnswersWithNoSelectedComputerTypeThenReturnTrue() {
		
		EnrollmentFormPageValidator enrollValidator = new EnrollmentFormPageValidator(3, UserAgentType.ANDROID);
		EnrollmentBean enrollmentBean = new EnrollmentBean();
		enrollmentBean.setQuestion1("question1");
		enrollmentBean.setQuestion2("question2");
		enrollmentBean.setQuestion3("question3");
		enrollmentBean.setAnswer1("casa");
		enrollmentBean.setAnswer1confirm("casa");
		enrollmentBean.setAnswer2("maria");
		enrollmentBean.setAnswer2confirm("maria");
		enrollmentBean.setAnswer3("america");
		enrollmentBean.setAnswer3confirm("america");
		enrollmentBean.setComputerType("");
		BindException error = new BindException(enrollmentBean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		enrollValidator.validate(enrollmentBean, error);
		ArrayList<String> fieldErrors = new ArrayList<>();
		error.getAllErrors().stream().forEach(e -> fieldErrors.add(e.getDefaultMessage()));
		assertTrue(fieldErrors.contains("Please select a computer type you are using"));
	}
	
	@Test
	public void whenValidateCaseTthreeHaveInValidAnswerOneThenReturnTrue() {
		
		EnrollmentFormPageValidator enrollValidator = new EnrollmentFormPageValidator(3, UserAgentType.IPHONE);
		EnrollmentBean enrollmentBean = new EnrollmentBean();
		enrollmentBean.setAnswer1("%#@");
		enrollmentBean.setAnswer1confirm("%#@");
		enrollmentBean.setAnswer2("maria");
		enrollmentBean.setAnswer2confirm("maria");
		enrollmentBean.setAnswer3("casa");
		enrollmentBean.setAnswer3confirm("casa");
		enrollmentBean.setComputerType("public");
		BindException error = new BindException(enrollmentBean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		enrollValidator.validate(enrollmentBean, error);
		assertTrue(0 != error.getFieldErrorCount());
	}
	
	@Test
	public void whenValidateCaseTthreeAnswerOneNotMatchThenReturnTrue() {
		
		EnrollmentFormPageValidator enrollValidator = new EnrollmentFormPageValidator(3, UserAgentType.IPHONE);
		EnrollmentBean enrollmentBean = new EnrollmentBean();
		enrollmentBean.setQuestion1("question1");
		enrollmentBean.setQuestion2("question2");
		enrollmentBean.setQuestion3("question3");
		enrollmentBean.setAnswer1("casa");
		enrollmentBean.setAnswer1confirm("perro");
		enrollmentBean.setAnswer2("maria");
		enrollmentBean.setAnswer2confirm("maria");
		enrollmentBean.setAnswer3("pablo");
		enrollmentBean.setAnswer3confirm("pablo");
		enrollmentBean.setComputerType("public");
		BindException error = new BindException(enrollmentBean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		enrollValidator.validate(enrollmentBean, error);
		ArrayList<String> fieldErrors = new ArrayList<>();
		error.getAllErrors().stream().forEach(e -> fieldErrors.add(e.getDefaultMessage()));
		assertTrue(fieldErrors.contains("Answers values must match"));
	}
	
	@Test
	public void whenValidateCaseTthreeHaveInValidAnswerTwoThenReturnTrue() {
		
		EnrollmentFormPageValidator enrollValidator = new EnrollmentFormPageValidator(3, UserAgentType.IPHONE);
		EnrollmentBean enrollmentBean = new EnrollmentBean();
		enrollmentBean.setAnswer1("maria");
		enrollmentBean.setAnswer1confirm("maria");
		enrollmentBean.setAnswer2("%#@");
		enrollmentBean.setAnswer2confirm("%#@");
		enrollmentBean.setAnswer3("casa");
		enrollmentBean.setAnswer3confirm("casa");
		enrollmentBean.setComputerType("public");
		BindException error = new BindException(enrollmentBean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		enrollValidator.validate(enrollmentBean, error);
		assertTrue(0 != error.getFieldErrorCount());
	}
	
	@Test
	public void whenValidateCaseTthreeAnswerTwoNotMatchThenReturnTrue() {
		
		EnrollmentFormPageValidator enrollValidator = new EnrollmentFormPageValidator(3, UserAgentType.IPHONE);
		EnrollmentBean enrollmentBean = new EnrollmentBean();
		enrollmentBean.setQuestion1("question1");
		enrollmentBean.setQuestion2("question2");
		enrollmentBean.setQuestion3("question3");
		enrollmentBean.setAnswer1("casa");
		enrollmentBean.setAnswer1confirm("casa");
		enrollmentBean.setAnswer2("maria");
		enrollmentBean.setAnswer2confirm("tornillo");
		enrollmentBean.setAnswer3("pablo");
		enrollmentBean.setAnswer3confirm("pablo");
		enrollmentBean.setComputerType("public");
		BindException error = new BindException(enrollmentBean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		enrollValidator.validate(enrollmentBean, error);
		ArrayList<String> fieldErrors = new ArrayList<>();
		error.getAllErrors().stream().forEach(e -> fieldErrors.add(e.getDefaultMessage()));
		assertTrue(fieldErrors.contains("Answers values must match"));
	}
	
	
	
	@Test
	public void whenValidateCaseTthreeHaveInValidAnswerTthreeThenReturnTrue() {
		
		EnrollmentFormPageValidator enrollValidator = new EnrollmentFormPageValidator(3, UserAgentType.ANDROID);
		EnrollmentBean enrollmentBean = new EnrollmentBean();
		enrollmentBean.setAnswer1("casa");
		enrollmentBean.setAnswer1confirm("casa");
		enrollmentBean.setAnswer2("maria");
		enrollmentBean.setAnswer2confirm("maria");
		enrollmentBean.setAnswer3("%#@");
		enrollmentBean.setAnswer3confirm("%#@");
		enrollmentBean.setComputerType("public");
		BindException error = new BindException(enrollmentBean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		enrollValidator.validate(enrollmentBean, error);
		assertTrue(0 != error.getFieldErrorCount());
	}
	
	@Test
	public void whenValidateCaseTthreeAnswerTthreeNotMatchThenReturnTrue() {
		
		EnrollmentFormPageValidator enrollValidator = new EnrollmentFormPageValidator(3, UserAgentType.ANDROID);
		EnrollmentBean enrollmentBean = new EnrollmentBean();
		enrollmentBean.setQuestion1("question1");
		enrollmentBean.setQuestion2("question2");
		enrollmentBean.setQuestion3("question3");
		enrollmentBean.setAnswer1("casa");
		enrollmentBean.setAnswer1confirm("casa");
		enrollmentBean.setAnswer2("maria");
		enrollmentBean.setAnswer2confirm("maria");
		enrollmentBean.setAnswer3("awd");
		enrollmentBean.setAnswer3confirm("casfreea");
		enrollmentBean.setComputerType("public");
		BindException error = new BindException(enrollmentBean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		enrollValidator.validate(enrollmentBean, error);
		ArrayList<String> fieldErrors = new ArrayList<>();
		error.getAllErrors().stream().forEach(e -> fieldErrors.add(e.getDefaultMessage()));
		assertTrue(fieldErrors.contains("Answers values must match"));
	}

	
}
