package evertec.cibp.web.form;

import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.ValidateInfoBean;
import evertec.cibp.web.session.UserSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, StaticMessageSource.class})
public class LoginSSDSForcedFormUT extends AbstractModelAndViewTests {

    @Mock
    HttpServletRequest request;

    @Mock
    UserSession session;

    @Mock
    CustomerProfile profile;

    @Mock
    CustomerServices customerService;

    @Mock
    ValidateInfoBean command;

    @InjectMocks
    LoginSSDSForcedForm controller;

    private static final String USER_SESSION = "userSession";
    private static final String INACTIVE = "inactive";
    BindException errors;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(StaticMessageSource.class);
        command = PowerMockito.spy(new ValidateInfoBean());
        errors = new BindException(command, "ValidateInfoBean");
    }

    /**
     * when OnBindAndValidate
     * Given PageTwoAndFlagMustChangeUsernameEnabled
     * Then ReturnError
     *
     * @author Evertec by ET58344
     * @since 11-27-2020
     */
    @Test
    public void whenOnBindAndValidate_GivenPageTwoAndFlagMustChangeUsernameEnabled_ThenReturnError() throws Exception {

        when(WebUtils.getSessionAttribute(request, USER_SESSION)).thenReturn(session);
        when(session.getCustomerProfile()).thenReturn(profile);
        when(session.getCustomerService()).thenReturn(customerService);
        when(WebUtils.getSessionAttribute(request, INACTIVE)).thenReturn(null);
        when(command.getAction()).thenReturn(ValidateInfoBean.ACTION.USERNAME);
        when(command.getUsername()).thenReturn("bobesponja");
        when(command.getPassword()).thenReturn("Bobesponja1234");
        when(command.getPasswordconfirm()).thenReturn("Bobesponja1234");
        when(profile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(Boolean.TRUE);
        when(profile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
        when(profile.getUsername()).thenReturn("patricioestrella");
        when(customerService.isUsernameAvailable(anyString())).thenReturn(Boolean.TRUE);
        when(command.isCheckAvaliability()).thenReturn(Boolean.FALSE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989)).thenReturn(Boolean.TRUE);

        controller.onBindAndValidate(request, command, errors, 2);
        assertEquals(errors.getAllErrors().size(), 1);
    }

    /**
     * when OnBindAndValidate
     * Given PageTwoAndFlagMustChangePasswordEnabled
     * Then ReturnError
     *
     * @author Evertec by ET58344
     * @since 11-27-2020
     */
    @Test
    public void whenOnBindAndValidate_GivenPageTwoAndFlagMustChangePasswordEnabled_ThenReturnError() throws Exception {

        when(WebUtils.getSessionAttribute(request, USER_SESSION)).thenReturn(session);
        when(session.getCustomerProfile()).thenReturn(profile);
        when(session.getCustomerService()).thenReturn(customerService);
        when(WebUtils.getSessionAttribute(request, INACTIVE)).thenReturn(Boolean.TRUE);
        when(command.getAction()).thenReturn(ValidateInfoBean.ACTION.USERNAME);
        when(command.getUsername()).thenReturn("bobesponja");
        when(command.getPassword()).thenReturn("Bobesponja1234");
        when(command.getPasswordconfirm()).thenReturn("Bobesponja1234");
        when(profile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(Boolean.FALSE);
        when(profile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
        when(profile.getUsername()).thenReturn("bobesponja");
        when(customerService.isUsernameAvailable(anyString())).thenReturn(Boolean.TRUE);
        when(command.isCheckAvaliability()).thenReturn(Boolean.FALSE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989)).thenReturn(Boolean.TRUE);

        controller.onBindAndValidate(request, command, errors, 2);
        assertEquals(errors.getAllErrors().size(), 1);
    }
}