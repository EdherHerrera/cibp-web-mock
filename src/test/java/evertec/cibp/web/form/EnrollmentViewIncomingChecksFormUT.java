package evertec.cibp.web.form;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.stubbing.OngoingStubbing;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.BaseCommandController;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import com.newrelic.agent.deps.com.google.common.base.Verify;
import com.sun.xml.rpc.processor.modeler.j2ee.xml.iconType;

import evertec.cibp.core.models.customer.AlertsHistory;
//  added necessaries clasess 
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.applications.ViewIncomingChecksApplication;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.form.bean.AddressBean;
import evertec.cibp.web.form.bean.EnrollmentViewIncomingChecksBean;
import evertec.cibp.web.form.bean.EnrollmentViewIncomingCheksAccountListBean;
import evertec.cibp.web.session.UserSession;
import oracle.net.aso.b;


// @RunWith attaches a runner with the test class to initialize the test data
@RunWith(PowerMockRunner.class)
@PrepareForTest({ WebUtils.class })

public class EnrollmentViewIncomingChecksFormUT {
	
	
	
	
	
	//@InjectMocks annotation is used to create objects and inject mocked dependencies - create class instances
	//@Spy
	@InjectMocks
	private EnrollmentViewIncomingChecksForm controller;

	@Captor
 	 private ArgumentCaptor<ViewIncomingChecksApplication> application;
	 
	//@Mock annotation is used to create the mock object to be injected, they're will be necessarie for enter each data
	@Mock
	private HttpServletRequest mHttpServletRequest;
	@Mock
	private HttpServletResponse mHttpServletResponse;
	@Mock
	private BindException mBindException;
	@Mock
	private UserSession mUserSession;
	@Mock 
	private CustomerProfile mCustomerProfile;
	@Mock
	private EnrollmentViewIncomingChecksBean mEnrollmentViewIncomingChecksBean;
	@Mock
	private CustomerServices mCustomerService;
	@Mock
	private AddressBean mAddressBean;
	@Mock
	private MessageSource mMessageSource;
	@Mock
	private WebUtils mWebUtils;
	@Mock
	 private SimpleFormController mshowForm;
	@Mock
	private ViewIncomingChecksApplication mView;
	 
	 
 
 	
	private List<EnrollmentViewIncomingCheksAccountListBean> mListEnrollmentViewIncomingCheksAccountListBean;
	private EnrollmentViewIncomingCheksAccountListBean mEnrollmentViewIncomingCheksAccountListBean;

	
	 
	
	
	
	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(WebUtils.class);
		
		mEnrollmentViewIncomingCheksAccountListBean = new EnrollmentViewIncomingCheksAccountListBean();
		mEnrollmentViewIncomingCheksAccountListBean.setIsAdded("YES");
		
		mListEnrollmentViewIncomingCheksAccountListBean = new ArrayList();
		mListEnrollmentViewIncomingCheksAccountListBean.add(mEnrollmentViewIncomingCheksAccountListBean);
		when(mEnrollmentViewIncomingChecksBean.getListAccounts()).thenReturn(mListEnrollmentViewIncomingCheksAccountListBean);
		when(WebUtils.getSessionAttribute(any(), eq("userSession"))).thenReturn(mUserSession);
		when(mUserSession.getCustomerProfile()).thenReturn(mCustomerProfile);
		when(mEnrollmentViewIncomingChecksBean.getMailing()).thenReturn(mAddressBean);
	    when(mEnrollmentViewIncomingChecksBean.getResidential()).thenReturn(mAddressBean);
	    when(mEnrollmentViewIncomingChecksBean.getListAccounts()).thenReturn(mListEnrollmentViewIncomingCheksAccountListBean);
		 
	    
	    
	  //setup

	     controller = PowerMockito.spy(controller);
 
	      
	     
	}

	/**
	 * when On Submit
	 * Give N/A
	 * Then Return ModelAndView
	 * 
	 * @throws Exception
	 */
 

	 
	@Test
	public void whenOnSubmit_Given_ThenReturnForm() throws Exception {
	
  	
  	//testcase
 
		Mockito.doReturn(null).when(controller).referenceData(any(), any(), any());
		ModelAndView model = controller.onSubmit(mHttpServletRequest, mHttpServletResponse, mEnrollmentViewIncomingChecksBean, mBindException);
		
		Mockito.verify(mCustomerService, Mockito.times(1)).saveViewIncomingChecksApplication(any(), application.capture());
 		 
  		assertEquals("[{~account~:~null~, ~disposition~:~null~, ~smq~:~N~}]", application.getValue().getAccountsList()); // After change
  		
		assertNotNull(model);
       
	}

}

