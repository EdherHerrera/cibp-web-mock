package evertec.cibp.web.form;

import static org.mockito.Mockito.when;

import java.util.Locale;

import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.types.CardAccount;
import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.EStatementInteraction;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.AddPayeeResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.AddExistingPayeeBean;
import evertec.cibp.web.session.GuidedSetupSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.Utils;
import evertec.prophit.ProphITService;
import evertec.prophit.service.bean.CustomerInquiryBean;
import evertec.prophit.service.bean.EbillIndType;
import evertec.prophit.service.bean.MaintenceEbillBean;
import evertec.prophit.service.bean.MaintenceEbillResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, CustomerServices.class, ApiCallerFactory.class, Utils.class,
        DeviceUtils.class, WebUtils.class, PaymentsFacade.class})
public class AddExistingPayeeFormUT extends AbstractModelAndViewTests {

    private final List<TVFrontendAccount> accountList = new ArrayList<>();

    private final Account account = new CardAccount();

    @Mock
    private AddExistingPayeeBean bean;

    @Mock
    private CustomerInquiryBean inquiryBean;

    @Mock
    private MaintenceEbillResponse maintenceEbillResponse;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private BindException errors;

    @Mock
    private BankingSession bankingSession;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private UserSession userSession;

    @Mock
    private CustomerServices customerService;

    @Mock
    private GlobalPayee globalPayee;

    @Mock
    private PaymentsFacade paymentsFacade;

    @Mock
    private GuidedSetupSession guidedSetupSession;

    @Mock
    private CustomerInteractionEvent customerInteractionEvent;

    @Mock
    private ApiCaller apiCaller;

    @Mock
    private AddPayeeResponse addPayeeResponse;

    @Mock
    private ProphITService prophITService;

    @Mock
	private MessageSource messageSource;

    @Spy
    @InjectMocks
    private final AddExistingPayeeForm controller = new AddExistingPayeeForm();

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(ApiCallerFactory.class);
        when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(DeviceUtils.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(PaymentsFacade.class);
    }

    @Test
    public void whenOnSubmit_GivenActivateEbill_ThenRegisterEvent() throws Exception {

        callTestEbill();
        controller.onSubmit(request, response, bean, errors);
        PowerMockito.verifyStatic(Utils.class);
        Utils.registerStatusEbillHistoryLog(any(HttpServletRequest.class), anyString(), any(UserSession.class),
                any(CustomerServices.class), anyString());
    }

    private void callTestEbill() throws Exception {

        account.setAccountID("54554654654");
        account.setSubType("CCA");
        TVFrontendAccount frontendAccount = new TVFrontendAccount(account);
        frontendAccount.setBackendObject(account);
        accountList.add(frontendAccount);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        when(bean.getPayee()).thenReturn("518");
        when(PaymentsFacade.getInstance()).thenReturn(paymentsFacade);
        when(paymentsFacade.getGlobalPayeeById(anyInt(), any(UserSession.class))).thenReturn(globalPayee);
        PowerMockito.doNothing().when(paymentsFacade).getGlobalPayeePreference(any(GlobalPayee.class), any(UserSession.class));
        when(userSession.getGuidedSetupSession()).thenReturn(guidedSetupSession);
        when(guidedSetupSession.isGuidedSetupMode()).thenReturn(false);
        when(userSession.isEbpp2UserIdLookedUp()).thenReturn(true);
        when(bean.getNickname()).thenReturn("VISA BPPR");
        when(bean.getBillingNumber()).thenReturn("546465464654");
        when(bean.getEmail()).thenReturn(null);
        when(bean.getZipCode()).thenReturn("12345");
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1701)).thenReturn(false);
        when(bean.getActivateEbill()).thenReturn(true);
        when(userSession.getAccounts()).thenReturn(accountList);
        when(Utils.getAccountByAccountNumber(anyString(), any(List.class))).thenReturn(frontendAccount);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(true);
        when(userSession.getEbpp2UserId()).thenReturn("123456");
        when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerInteractionEvent);
        when(customerService.addCustomerPayeeWithEbill(any(CustomerProfile.class), any(BankingSession.class),
                any(GlobalPayee.class), anyString(), anyString(), anyBoolean(), anyString(), anyString(),
                any(CustomerInteractionEvent.class), anyString(), anyBoolean())).thenReturn(addPayeeResponse);
        callValidations();
        callValidationsTest();
    }

    private void callValidations() throws Exception {
        when(customerService.prophItService()).thenReturn(prophITService);
        when(prophITService.makeCustomerInquiry(anyString())).thenReturn(inquiryBean);
        when(inquiryBean.getEbillInd()).thenReturn(EbillIndType.ELIGIBLE.toString());
        when(inquiryBean.getCaseNumber()).thenReturn("5466");
        when(inquiryBean.getChannelId()).thenReturn("665");
        when(inquiryBean.getImpersonateId()).thenReturn("1234");
        when(inquiryBean.getKeepCaseOpen()).thenReturn(true);
        when(inquiryBean.getNewContact()).thenReturn(true);
        when(inquiryBean.getOrgId()).thenReturn("47879");
        when(inquiryBean.getOrgLevel()).thenReturn("7");
    }

    private void callValidationsTest() throws Exception {
        PowerMockito.when(prophITService.maintenceEBill(any(MaintenceEbillBean.class))).thenReturn(maintenceEbillResponse);
        when(maintenceEbillResponse.getMessage()).thenReturn("Successfull");
        PowerMockito.doReturn(Boolean.TRUE).when(customerService).sendEbillsCCAEmailActivationSuccess(customerProfile,
                PreferredLanguage.SPANISH, "VISA BPPR");
        customerProfile.setPermId(new BigInteger("4654654132132165"));
        customerProfile.setTaxId("555321456");
        customerProfile.setUsername("gallo");
        when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerInteractionEvent);
        PowerMockito.doNothing().when(customerService).logUpdateStatementStatus(customerInteractionEvent,
                InteractionSeverity.INFO);
        PowerMockito.doNothing().when(Utils.class, "registerStatusEbillHistoryLog", any(HttpServletRequest.class),
                anyString(), any(UserSession.class), any(CustomerServices.class), any(EStatementInteraction.class));
        PowerMockito.when(Utils.getRSALanguage(request)).thenReturn("en");
        when(customerProfile.getFirstName()).thenReturn("Juanico");
        when(customerProfile.getEnrollmentAccountNumber()).thenReturn("4556123456987548");
        when(globalPayee.getName()).thenReturn("VISA");
        PowerMockito.doNothing().when(customerService).sendEmailPayee(any(CustomerProfile.class),
                any(PreferredLanguage.class), any(Map.class));
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
    }
	
    @Test
	public void whenGetTelepagoErrorDescriptor_GivenMessageError_ThenReturnMessageErrorSucces() throws Exception {
		controller.setMessageSource(messageSource);
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("OK");

		String valueValidate = Whitebox.invokeMethod(controller, "getTelepagoErrorDescriptor", "ebpp.module", "ens","errorCode");
		assertEquals(valueValidate, "OK");
	}
}
