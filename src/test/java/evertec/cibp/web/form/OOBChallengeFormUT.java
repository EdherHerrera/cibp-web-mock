package evertec.cibp.web.form;

import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.campaign.loader.CampaignPaths;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.UsernameValidationResponse;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.form.bean.OOBChallengeBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import joptsimple.internal.Strings;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.File;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ WebUtils.class, CookieCategory.class, Utils.class })
public class OOBChallengeFormUT extends AbstractModelAndViewTests {

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private UserSession userSession;

    @Mock
    private CustomerServices customerServices;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private CampaignPaths paths;

    @Mock
    private Errors errors;

    @Mock
    private OOBChallengeBean bean;

    @Mock
    private DeviceRequest deviceRequest;

    @Mock
    private CustomerEntitlement rsaEnt;

    @Mock
    private List<UsernameValidationResponse.ASK_FOR> taskList;

    @InjectMocks
    private OOBChallengeForm form;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Utils.class);
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
    }

    @Test
    public void referenceData() throws Exception {

        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.FALSE);
        when(request.getParameter("source")).thenReturn(Strings.EMPTY);
        when(request.getHeader(anyString())).thenReturn(Strings.EMPTY);
        when(Utils.getServerHostname()).thenReturn("localhost");
        when(errors.hasFieldErrors()).thenReturn(Boolean.FALSE);
        when(paths.getFilePath()).thenReturn("C://");
        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(null);
        form.setPaths(paths);
        when(bean.getAction()).thenReturn(null);
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(rsaEnt);
        when(userSession.getTasksList()).thenReturn(taskList);
        taskList.add(UsernameValidationResponse.ASK_FOR.RECOVERY_CODE);
        when(userSession.getUsername()).thenReturn("galloo");
        when(userSession.isLoginCompleteSuccess()).thenReturn(Boolean.TRUE);
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(deviceRequest.getDeviceTokenCookie()).thenReturn("dasdfasdfasd");

        Map<String, Object> result = form.referenceData(request, bean, errors, 3);
    }

    @Test
    public void whenProcessFinish_GivenRedirectToLogin_ThenReturnModelAndView () throws Exception {
        HttpSession session = new MockHttpSession();

        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn("abcdefg");
        when(userSession.getPm_fp()).thenReturn(Strings.EMPTY);
        when(userSession.getMobileSdkData()).thenReturn(Strings.EMPTY);
        when(request.getHeader("User-Agent")).thenReturn(Strings.EMPTY);
        when(request.getSession()).thenReturn(session);
        request.getSession().setAttribute("oobCode", "123456");

        ReflectionTestUtils.setField(form, "redirectToLogin", true);

        ModelAndView model = form.processFinish(request, response, any(), any());
        assertViewName(model, "redirect:login?rsa_cookie=abcdefg");
    }


    @Test
    public void whenProcessFinish_GivenRedirectToBlock_ThenReturnModelAndView () throws Exception {
        HttpSession session = new MockHttpSession();

        when(userSession.getUsername()).thenReturn("test");
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn("abcdefg");
        when(userSession.getPm_fp()).thenReturn(Strings.EMPTY);
        when(userSession.getMobileSdkData()).thenReturn(Strings.EMPTY);
        when(request.getHeader("User-Agent")).thenReturn(Strings.EMPTY);
        when(request.getSession()).thenReturn(session);
        request.getSession().setAttribute("oobCode", "123456");

        ReflectionTestUtils.setField(form, "redirectToBlock", true);

        ModelAndView model = form.processFinish(request, response, any(), any());
        assertViewName(model, "redirect:login?username=test&rsa_cookie=abcdefg");
    }
}