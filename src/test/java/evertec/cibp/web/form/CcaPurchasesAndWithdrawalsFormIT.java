package evertec.cibp.web.form;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.core.enums.TsysAlertStatus;
import evertec.cibp.core.facades.TsysAlertFacade;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.core.utils.alerts.AlertCCAUtils;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.facade.CreditCardFacade;
import evertec.cibp.web.form.bean.PurchasesAndWithdrawalsBean;
import evertec.cibp.web.form.bean.SmsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AlertTypesUtils;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.Utils;
import evertec.creditcards.model.CustomerIdentifiers;
import evertec.creditcards.model.Phone;
import evertec.creditcards.model.Program;
import evertec.creditcards.model.alerts.AlertCustomerIdentifiers;
import evertec.creditcards.model.alerts.AlertDetails;
import evertec.creditcards.model.alerts.UserNotificationPreference;
import evertec.creditcards.model.alerts.UserNotificationPreferences;
import evertec.creditcards.request.alerts.TsysConsumerEssentialsPhoneNumbersRequest;
import evertec.creditcards.request.alerts.TsysAlertPreferenceCreateUpdateRequest;
import evertec.creditcards.response.TsysConsumerEssentialsPhoneNumbersResponse;
import evertec.creditcards.response.alerts.TsysAlertPreferencesResponse;
import evertec.creditcards.response.alerts.preferences.TsysAlertPreferenceCreateUpdateResponse;
import evertec.creditcards.response.alerts.preferences.contacts.TsysAlertContactInformationResponse;
import evertec.creditcards.response.alerts.preferences.contacts.TsysRetrieveAlertContactInformationResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

/**
 * @author mdias
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, Utils.class, StaticMessageSource.class, CcaPurchasesAndWithdrawalsForm.class,
		CreditCardFacade.class, TsysAlertFacade.class, AlertTypesUtils.class, ESAPI.class, AlertCCAUtils.class,
		ApiCallerFactory.class, DeviceUtils.class})
public class CcaPurchasesAndWithdrawalsFormIT {
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private TsysAlertFacade tsysAlertFacade;

	@Mock
	private BankingSession bankingSession;

	@Mock
	private CustomerServices customerService;

	@Mock
	private TVFrontendAccount tvFrontendAccount;

	@Mock
	private DeviceRequest deviceRequest;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private BindException errors;

	@Mock
	private HTTPUtilities httpUtilities;

	@Mock
	private ApiCaller apiCaller;

	private PurchasesAndWithdrawalsBean bean;
	private CustomerIdentifiers customerIdentifiers;
	private TsysAlertPreferencesResponse alertPreferencesResponse;
	private ArrayList<UserNotificationPreferences> notificationPreferencesArrayList;
	private UserNotificationPreferences notificationPreferences;
	private List<TVFrontendAccount> ccaAccounts;
	private Map<String, Object> model;
	private CardPlasticFrontend[] ccaPlastics;
	private ArrayList<AlertDetails> alertDetailsArrayList;
	private AlertDetails alertDetails;
	private CardPlasticFrontend cardPlasticFrontend;
	private Map<Object, Object> accountsMap;
	private Map<String, CardPlasticFrontend> preferences;
	
	@InjectMocks
	@Spy
	private CcaPurchasesAndWithdrawalsForm ccaPurchasesAndWithdrawalsForm = new CcaPurchasesAndWithdrawalsForm("0.01",
			"454925,454903", "ENABLED", "ENABLED", "6");
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(TsysAlertFacade.class);
		PowerMockito.mockStatic(AlertTypesUtils.class);
		PowerMockito.mockStatic(ESAPI.class);
		PowerMockito.mockStatic(AlertCCAUtils.class);
		PowerMockito.mockStatic(ApiCallerFactory.class);
		PowerMockito.mockStatic(DeviceUtils.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

		customerIdentifiers = new CustomerIdentifiers();
		alertPreferencesResponse = new TsysAlertPreferencesResponse();
		notificationPreferencesArrayList = new ArrayList<>();
		notificationPreferences = new UserNotificationPreferences();
		bean = new PurchasesAndWithdrawalsBean();
		tvFrontendAccount = mock(TVFrontendAccount.class);
		ccaAccounts = new ArrayList<>();
		ccaPlastics = new CardPlasticFrontend[0];
		model = new HashMap<>();
		alertDetailsArrayList = new ArrayList<>();
		alertDetails = new AlertDetails();
		cardPlasticFrontend = new CardPlasticFrontend();
		accountsMap = new LinkedHashMap<>();
		preferences = new LinkedHashMap<>();
	}

	@Test
	public void whenInquiryTsysAlertPreferences_GivenAlertPreferencesResponseOK_ThenReturnResponseNotNull() throws Exception {

		notificationPreferences.setAlert("PURCHASE_AMOUNT");
		notificationPreferences.setActiveFlag("true");
		alertDetails.setName("thresholdAmount");
		alertDetails.setSelectedValue("45.0");
		alertDetailsArrayList.add(alertDetails);
		notificationPreferences.setAlertDetails(alertDetailsArrayList);
		notificationPreferencesArrayList.add(notificationPreferences);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA77)).thenReturn(true);

		bean.setAthplastic1("4549542260934550");
		CardPlasticAccountRecord accountRecord = new CardPlasticAccountRecord();
		accountRecord.setCardPANId("04549542260934550");
		cardPlasticFrontend = new CardPlasticFrontend(accountRecord);
		cardPlasticFrontend.setTsysCcaAlertPreferences(alertPreferencesResponse);
		ccaPlastics = new CardPlasticFrontend[1];
		ccaPlastics[0] = cardPlasticFrontend;
		PowerMockito.doReturn("04549542260934550").when(ccaPurchasesAndWithdrawalsForm,
				"getAthPlastic", anyString());

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		alertPreferencesResponse.setUserNotificationPreferences(notificationPreferencesArrayList);
		bean.setCcaAccount("4549542260934550");
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);

		when(tsysAlertFacade.tsysAlertPreferenceByAccountCustomerId(any(AlertCustomerIdentifiers.class)))
				.thenReturn(alertPreferencesResponse);

		when(userSession.getCcaAccounts()).thenReturn(ccaAccounts);
		when(Utils.getAccountByAccountNumber(anyString(), anyList())).thenReturn(tvFrontendAccount);
		when(Utils.getCCACardPlastics(any(CustomerServices.class), any(CustomerProfile.class),
				any(BankingSession.class), any(TVFrontendAccount.class))).thenReturn(ccaPlastics);
		TsysAlertPreferencesResponse response = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm,
				"inquiryTsysAlertPreferences", userSession, bean, model);
		assertEquals("45.00", model.get("purchase_min_amt"));
	}

	@Test
	public void whenOnSubmit_GivenAction2AndUpdateAlertOption_ThenReturnModel() throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("statusMessage", null);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)).thenReturn(false);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn("Test-Cookie");
		when(userSession.getOobEnroll()).thenReturn(false);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		PowerMockito.doReturn("4549542260934550").when(ccaPurchasesAndWithdrawalsForm,
				"getAthPlastic", anyString());
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsMinAlertQty()).thenReturn("0.01");
		bean.setAthplastic1("4549542260934550");
		bean.setAction("2");
		bean.setPurchase_thr_chksms("1");
		bean.setAlertsPurchaseminamt("");
		accountsMap.put("none", "Select a Credit Card");
		accountsMap.put("VISA BPPR", "x4550");
		accountsMap.put("MASTERCARD JET BLUE", "x5345");
		PowerMockito.doReturn(accountsMap).when(ccaPurchasesAndWithdrawalsForm,
				"getAccountsMap", any(HttpServletRequest.class));
		when(AlertTypesUtils.purchasesAndWithdrawalsErrors(request, model, bean)).thenReturn(false);

		TsysRetrieveAlertContactInformationResponse contactInformationResponse = new TsysRetrieveAlertContactInformationResponse();
		Program program3 = new Program();
		Phone phone = new Phone();
		phone.setType("ALT2");
		ArrayList<Phone> phones = new ArrayList<>();
		phones.add(phone);
		program3.setProgramName("PROGRAM3");
		program3.setPhones(phones);
		ArrayList<Program> programs = new ArrayList<>();
		programs.add(program3);
		contactInformationResponse.setPrograms(programs);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);

		userSessionGetCustomerIdentifiers();
		addPhoneNumberAndAlertPreference();
		saveAlertPhoneContactTSYS();
		saveAlertContactInformationTSYS();

		PowerMockito.doReturn(mav).when((SimpleFormController) ccaPurchasesAndWithdrawalsForm, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any());

		ModelAndView modelAndView = ccaPurchasesAndWithdrawalsForm.onSubmit(request, response, bean, errors);
		assertNull(modelAndView.getModel().get("statusMessage"));
	}

	private void userSessionGetCustomerIdentifiers(){

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		HashMap<String, CustomerIdentifiers> customerIdentifiersMap = new HashMap<>();
		customerIdentifiersMap.put("4549542260934550", customerIdentifiers);


		when(userSession.getCcaCustomerIdentifierMap()).thenReturn(customerIdentifiersMap);
	}

	private void addPhoneNumberAndAlertPreference() {

		when(AlertTypesUtils.getSuccessStatusMessage(any(HttpServletRequest.class))).thenReturn("Profile Updated");
	}

	private void saveAlertPhoneContactTSYS() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		bean.setCcaAccount("4549542260934550");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		SmsBean smsBean = new SmsBean();
		smsBean.setPhone1("7875462514");
		TsysConsumerEssentialsPhoneNumbersResponse phoneNumbersResponse = new TsysConsumerEssentialsPhoneNumbersResponse();
		PowerMockito.doReturn(smsBean).when(ccaPurchasesAndWithdrawalsForm, "getSMSProfile", any(UserSession.class), any(HttpServletRequest.class));
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		when(tsysAlertFacade.tsysUpdateCustomerPhoneNumbersByIdentifiers(anyString(), anyString(), any(TsysConsumerEssentialsPhoneNumbersRequest.class)))
				.thenReturn(phoneNumbersResponse);
	}

	private void saveAlertContactInformationTSYS() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		SmsBean smsBean = new SmsBean();
		smsBean.setPhone1("7875462514");
		PowerMockito.doReturn(smsBean).when(ccaPurchasesAndWithdrawalsForm, "getSMSProfile", any(UserSession.class), any(HttpServletRequest.class));

		TsysAlertContactInformationResponse response = new TsysAlertContactInformationResponse();
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		when(tsysAlertFacade.tsysAlertContactInformationUpdate(any(CustomerIdentifiers.class), anyList()))
				.thenReturn(response);
		bean.setCcaAccount("4549542260934550");
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getUsername()).thenReturn("galloo");
	}

	@Test
	public void whenSaveUpdateAlertPreference_GivenSmsBeanNotNull_ThenReturn() throws Exception {

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		Program program3 = new Program();
		Phone phone = new Phone();
		phone.setType("ALT2");
		phone.setValue("+17879584561");
		ArrayList<Phone> phones = new ArrayList<>();
		phones.add(phone);
		program3.setProgramName("PROGRAM_3");
		program3.setPhones(phones);
		program3.setSelectedPhones(phones);
		SmsBean smsBean = new SmsBean();
		smsBean.setPhone1("7879584561");
		bean.setAthplastic1("4549542260934550");

		PowerMockito.doReturn(smsBean).when(ccaPurchasesAndWithdrawalsForm, "getSMSProfile", any(UserSession.class),
				any(HttpServletRequest.class));
		PowerMockito.doReturn("7879584561").when(AlertCCAUtils.class, "removePRCountryCodeContactNumber", anyString());
		saveUpdatePurchaseAmountAlertPreferencesTSYS();
		when(AlertTypesUtils.getSuccessStatusMessage(any(HttpServletRequest.class))).thenReturn("Profile Updated");
		PowerMockito.doNothing().when(userSession).updateTsysCCACustomerAlerts(anyString());

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "saveUpdateAlertPreference", request, bean, model, program3);
		PowerMockito.verifyPrivate(ccaPurchasesAndWithdrawalsForm, times(1)).invoke("saveUpdateAlertPreference",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), anyMap(), any(Program.class));
	}

	private void saveUpdatePurchaseAmountAlertPreferencesTSYS() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		bean.setAthplastic1("4549542260934550");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		alertDetails.setName("thresholdAmount");
		alertDetails.setSelectedValue("50.0");
		alertDetailsArrayList.add(alertDetails);
		notificationPreferences.setActiveFlag("true");
		notificationPreferences.setAlert("PURCHASE_AMOUNT");
		notificationPreferences.setAlertDetails(alertDetailsArrayList);
		notificationPreferencesArrayList.add(notificationPreferences);
		alertPreferencesResponse.setUserNotificationPreferences(notificationPreferencesArrayList);
		cardPlasticFrontend.setTsysCcaAlertPreferences(alertPreferencesResponse);
		preferences.put("4549542260934550", cardPlasticFrontend);
		when(userSession.getTsysCCACustomerAlerts()).thenReturn(preferences);
		PowerMockito.doReturn(new ArrayList<UserNotificationPreference>()).when(AlertCCAUtils.class, "changeTypeAndUpdateNotificationPreferencesType",
				any(ArrayList.class), anyString(), anyString(), anyString());
		bean.setAlertsPurchaseminamt("50.0");
		ccaPurchasesAndWithdrawalsForm.setOriginalAmount("100.55");
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		TsysAlertPreferenceCreateUpdateResponse response = new TsysAlertPreferenceCreateUpdateResponse();
		response.setSuccess(true);
		when(tsysAlertFacade.tsysUpdateAlertsPreferences(any(TsysAlertPreferenceCreateUpdateRequest.class))).thenReturn(response);
	}

	@Test
	public void whenRemoveAlertPreference_ThenReturnResultModelSuccess() throws Exception {

		Map<String, Object> model = new HashMap<>();
		bean.setAthplastic1("4549542260934550");
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(new CustomerInteractionEvent());
		when(DeviceUtils.getBrowserInfo(anyString(), any(UserSession.class))).thenReturn("Mozilla...");
		PowerMockito.doNothing().when(customerService).logChangedAlertAmountTsys(any(), any(InteractionSeverity.class));
		saveUpdatePurchaseAmountAlertPreferencesTSYS();
		when(AlertTypesUtils.getSuccessStatusMessage(any(HttpServletRequest.class))).thenReturn("Profile Updated");
		PowerMockito.doNothing().when(userSession).updateTsysCCACustomerAlerts(anyString());
		PowerMockito.doNothing().when(Utils.class, "registerTsysAlertHistoryLog", any(HttpServletRequest.class), anyString(),
				any(UserSession.class), any(TsysAlertStatus.class));

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "removeAlertPreference", bean, request, model);
		PowerMockito.verifyPrivate(ccaPurchasesAndWithdrawalsForm, times(1)).invoke("removeAlertPreference",
				any(PurchasesAndWithdrawalsBean.class), any(HttpServletRequest.class), anyMap());
		PowerMockito.verifyPrivate(ccaPurchasesAndWithdrawalsForm, times(1)).invoke("registerAlertAmountChangedHistoryLog",
				any(), any(), any(), any(), any());
	}
}
