package evertec.cibp.web.form;

import evertec.cibp.campaign.loader.CampaignPaths;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerPayeePreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.customer.DefaultCustomerService;
import evertec.cibp.core.services.customer.modules.PaymentServiceModule;
import evertec.cibp.core.services.interfaces.CustomerInteractionService;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.InternationalCharacterUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.EpayrollSearchBean;
import evertec.cibp.web.form.bean.ModPaymentBean;
import evertec.cibp.web.request.ModifyPaymentRequest;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.services.modules.SignOnCustomerServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.Utils;
import evertec.cibp.vs.models.results.ModifyPaymentActionResult;
import evertec.cibp.vs.services.TransactionVaultServices;
import evertec.cibp.vs.beans.payments.RecurringPayment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.owasp.esapi.ESAPI;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import evertec.cibp.vs.beans.payments.PaymentModify;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PaymentsServiceModule.class,CustomerPayeePreferences.class,InternationalCharacterUtils.class,CustomerServices.class,CustomerProfile.class,UserSession.class, WebUtils.class, Utils.class, ResultStatus.class, UserAgentType.class, StaticMessageSource.class, ServletRequestUtils.class, ESAPI.class, SignonSequenceUtils.class, SignOnCustomerServiceModule.class, 
	CustomerUtils.class,CustomerProfile.class})
public class BusinessPaymentHistoryFormUT extends AbstractModelAndViewTests{

    @InjectMocks
    private BusinessPaymentHistoryForm businessPaymentHistoryForm;
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	private static final String IS_SECURITIES_TRUE = "true";

	@Mock
	private MessageSource messageSource;
	@Mock
	private TransactionVaultServices vaultServices;
	@Mock
	private BindException errors;
	@InjectMocks
	private DefaultCustomerService service;
    @Mock
    private CampaignPaths paths;
    @Mock
    private CustomerProfile customerProfile;
	@Mock
	private UserSession userSession;
	@Mock
	private SignOnCustomerServiceModule customerServiceModule;
	@Mock
	private UsernameValidationResponse responseAnswer;
    @Mock
    private CustomerServices customerServices;
    
	@Mock
	private EpayrollSearchBean epayrollSearchBean;

    @Mock
    private CustomerInteractionService customerInteractionService;
    private static final String VIEW = "businesspaymenthistory";

	@Mock
	private PaymentServiceModule paymentsServiceModule;
	@Mock
	private ModifyPaymentActionResult modifyPaymentActionResult;
	@Mock
	private PaymentsFacade paymentsFacade;
	@Mock
	private PaymentResponse res;
	@Mock
	private ModifyPaymentRequest modifyPaymentRequest ;
	
	@Mock
	private TVFrontEndPayee tvFrontEndPayee;
	
	@Mock
	public CustomerPayeePreferences customerPayeePreferences;
	
	@Before
	public void setUp() throws Exception {
        PowerMockito.mockStatic(CustomerProfile.class);
        PowerMockito.mockStatic(UserSession.class);
        PowerMockito.mockStatic(CustomerServices.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(CustomerPayeePreferences.class);
        PowerMockito.mockStatic(PaymentsServiceModule.class);
        
        epayrollSearchBean = new EpayrollSearchBean();
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
	}
	
    public void init(){
   
    	Calendar calModDate = Calendar.getInstance();
    	tvFrontEndPayee = mock(TVFrontEndPayee.class);
    	epayrollSearchBean = mock(EpayrollSearchBean.class);
    	PaymentsServiceModule paymentsServiceModule = mock(PaymentsServiceModule.class);
    	Map<String, Object> objParam = new HashMap<>();
    	RecurringPayment recurringPayment = new RecurringPayment();
    	List<TVPayment> customerPayments = new LinkedList<>();
    	TVPayment tvPayment = new   TVPayment(recurringPayment);
    	TVFrontendAccount account = new TVFrontendAccount("IDA","12232","03");  	
    	TVAccountTransferSource source = new TVAccountTransferSource(account);
    	PaymentModify paymentM = new PaymentModify();
		paymentM.setNewFrequency("WEEKLY");
		paymentM.setNewAmount(BigDecimal.valueOf(2));
		paymentM.setRecurringPmt(recurringPayment);
		paymentM.setNewEffectiveDate(calModDate);
		PaymentResponse res = new PaymentResponse();

    	tvPayment.setPayee(tvFrontEndPayee);
    	tvPayment.getTVPayment().setAmount(new BigDecimal(1250));
    	tvPayment.getTVPayment().setFrequency("regl");
    	tvPayment.getTVPayment().setFrequency("regl");
    	tvPayment.getTVPayment().setRemainingInstances(123);
    	tvPayment.getTVPayment().setReferenceNumber("");
    	tvPayment.setFrontEndId("123");
    	tvPayment.setEstimateDate("sep");
    	tvPayment.setSource(source);
    	customerPayments.add(tvPayment);
    	objParam.put("processedPayments", customerPayments);
    	objParam.put("pendingPayments", customerPayments);
    	
    	List<ModPaymentBean> favorites =   new ArrayList<ModPaymentBean>();
        ModPaymentBean modPaymentBean = new  ModPaymentBean();
        modPaymentBean.setModTransferId("mod");
        modPaymentBean.setModFreq("WEEKLY");
        modPaymentBean.setModInstancesType(IS_SECURITIES_TRUE);
        modPaymentBean.setModInstances(IS_SECURITIES_TRUE);
        modPaymentBean.setModDate(calModDate.getTime());
        favorites.add(modPaymentBean);
        
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);      
        when(Utils.getRSALanguage(request)).thenReturn("sp");
        
        when(epayrollSearchBean.getAction()).thenReturn(4);
        when(epayrollSearchBean.getModPayment()).thenReturn("mod");
        when(epayrollSearchBean.getFavorites()).thenReturn(favorites);
        when(epayrollSearchBean.getPaymentType()).thenReturn("VendorID");
        when(Utils.getPaymentById("mod", userSession.getPendingPayments())).thenReturn(tvPayment);
        when(customerProfile.getEntitlementByType(EntitlementType.BANKING)).thenReturn(new CustomerEntitlement());
        when(PaymentsServiceModule.getInstance()).thenReturn(paymentsServiceModule);
        when(paymentsServiceModule.modifyPayment(any())).thenReturn(res);
        
        //reference data
        when(tvFrontEndPayee.getNickname()).thenReturn("nickname");
        when(Utils.getBusinessPaymentList(any(), any(), any())).thenReturn(objParam);
    }	
    
    @Test
	public  void testOnSubmitHttpServletRequestHttpServletResponseObjectBindException(){
		try {
	    	init();
	    	errors = new BindException(epayrollSearchBean, "epayrollSearchBean");
			ModelAndView modelAndView;
			modelAndView = businessPaymentHistoryForm.onSubmit(request, response,epayrollSearchBean, errors);
		    assertViewName(modelAndView, VIEW);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		}
    }
    
	@Test
	public void whenGetTelepagoErrorDescriptor_GivenMessageError_ThenReturnMessageErrorSucces() throws Exception {
		businessPaymentHistoryForm.setMessageSource(messageSource);
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("OK");

		String valueValidate = Whitebox.invokeMethod(businessPaymentHistoryForm, "getTelepagoErrorDescriptor", "ebpp.module", "ens");
		assertEquals(valueValidate, "OK");
	}
}