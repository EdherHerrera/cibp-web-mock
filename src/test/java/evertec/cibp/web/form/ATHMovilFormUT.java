package evertec.cibp.web.form;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.otherservices.responses.AthmCustomerInfoResp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.BindException;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ATHMovilBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

import java.util.Map;

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CustomerFacade.class, StaticMessageSource.class, Utils.class})
public class ATHMovilFormUT {
	
	@Mock
	private UserSession userSession;

	@Mock
	private CustomerFacade customerFacade;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private ATHMovilBean athMovilFormBean;

	private MockHttpServletRequest request;

	@Mock
	private CustomerEntitlement customerEntitlement;

	@Mock
	private CustomerServices customerService;

	@Mock
	private AthmCustomerInfoResp athmCustomerInfoResp;

	@Mock
	private MessageSource messageSource;

	@Mock
	private BindException error;

	@InjectMocks
	private ATHMovilForm aTHMovilForm;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);

		request = new MockHttpServletRequest();
		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
	}
	
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession, userSession, false);
		when(messageSource.getMessage(anyString(), any(), any())).thenReturn("test");
		when(Utils.getRSALanguage(any(HttpServletRequest.class))).thenReturn("es");
		when(customerService.getCustomerInfo(any())).thenReturn(athmCustomerInfoResp);

		Map<String, Object> result = aTHMovilForm.referenceData(request, athMovilFormBean, error);
		assertNotNull(result);

	}
}
