package evertec.cibp.web.form.payee;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.AddPayeeResponse;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.AddExistingPayeeBean;
import evertec.cibp.web.session.GuidedSetupSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ServletRequestUtils.class, WebUtils.class, CustomerServices.class, PaymentsFacade.class, AddPayeeResponse.class})
public class MobileAddPayeeControllerIT extends AbstractModelAndViewTests {
	private HttpServletRequest request;
	private HttpServletResponse response;
	
	@Mock
	private AddExistingPayeeBean payeeBean;
	
	@Mock
	private ServletRequestUtils servletRequestUtils;
	
	@Mock
	private MobileAddPayeeController mobileAddPayeeController;
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private PaymentsFacade paymentsFacade;
	
	@Mock
	private GuidedSetupSession guidedSetupSession;
	
	@Mock
	private AddPayeeResponse res;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private BankingSession bankingSession;

	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(WebUtils.class);
		request = new MockHttpServletRequest();
		
		mobileAddPayeeController = PowerMockito.spy(new MobileAddPayeeController());
		customerService = PowerMockito.mock(CustomerServices.class);
		mobileAddPayeeController.setCustomerService(customerService);
		PowerMockito.mockStatic(PaymentsFacade.class);		
		PowerMockito.mockStatic(ServletRequestUtils.class);
		
		userSession.setLanguage("en");	
		
		String key = "123456789";
		bankingSession = new BankingSession(new BigInteger("1689012310001001"), key);
		
		customerProfile = new CustomerProfile();
		customerProfile.setUsername("automaremeva");
		customerProfile.setTaxId("472334747");
		customerProfile.getEntitlementByType(EntitlementType.EBILLS);
		customerProfile.setProfileId("8a81a0b7794246ca0179428815e10012");
		customerProfile.setPermId(new BigInteger("1689012310001001"));
	}

	@Test
	public void whenmobileSavePayee_GivenModel_ThenSavePayeeInvoked() throws Exception {
		payeeBean = new AddExistingPayeeBean();
		payeeBean.setBillingNumber("963851753");
		payeeBean.setBillingNumberConf("963851753");
		payeeBean.setNickname("uttestnickname");
		payeeBean.setPayee("331");
		payeeBean.setZipcode(null);
		
		GlobalPayee global = new GlobalPayee();
		global = new GlobalPayee();
		global.setBankId("1000");
		global.setMerchantNumber("099800005900");
		global.setName("PRT/CLARO");
		global.setShortName("PRT/CLARO");
		global.setId(new Integer(payeeBean.getPayee()));
		
		when(ServletRequestUtils.getStringParameter(request, "billingNumber")).thenReturn(payeeBean.getBillingNumber());
		when(ServletRequestUtils.getStringParameter(request, "confirmBillingNumber")).thenReturn(payeeBean.getBillingNumberConf());
		when(ServletRequestUtils.getStringParameter(request, "nickname")).thenReturn(payeeBean.getNickname());
		when(ServletRequestUtils.getStringParameter(request, "payeeId")).thenReturn(payeeBean.getPayee());
		when(ServletRequestUtils.getStringParameter(request, "zipCode")).thenReturn(payeeBean.getZipCode());
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getLanguage()).thenReturn("en");
		when(customerService.getGlobalPayeeById(anyInt())).thenReturn(global);
		when(PaymentsFacade.getInstance()).thenReturn(paymentsFacade);
		
		Map<String, Object> model = Whitebox.invokeMethod(mobileAddPayeeController,"validate",request, payeeBean);
		assertNotNull(model);
		
		when(userSession.getGuidedSetupSession()).thenReturn(guidedSetupSession);		
		when(userSession.isEbpp2UserIdLookedUp()).thenReturn(true);
		when(customerService.addCustomerPayeeWithoutEbillWithErrorDescription(customerProfile, bankingSession, global, 
				"963851753", "uttestnickname", Utils.generateCustomerInteractionEvent(request), null, null, null, 
				null, false, false)).thenReturn(res);
		
		mobileAddPayeeController.mobileSavePayee(request, response);
	}

}
