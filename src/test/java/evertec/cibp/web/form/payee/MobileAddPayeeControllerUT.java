package evertec.cibp.web.form.payee;

import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerEntitlement4Hibernate;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.core.services.responses.AddPayeeResponse;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.facade.PaymentsFacade;
import evertec.cibp.web.form.bean.AddExistingPayeeBean;
import evertec.cibp.web.session.GuidedSetupSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Utils.class, WebUtils.class, CustomerServices.class, PaymentsFacade.class, AddPayeeResponse.class})
public class MobileAddPayeeControllerUT extends AbstractModelAndViewTests{
	
	private HttpServletRequest request;
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private BankingSession bankingSession;
			
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private AddExistingPayeeBean payeeBean;
	
	@Mock
	private GlobalPayee global;
	
	@Mock
	private PaymentsFacade paymentsFacade;
	
	@Mock
	MessageSource messageSource;
	
	@Mock
	private GuidedSetupSession guidedSetupSession;
	
	@Mock
	private AddPayeeResponse res;
	
	@Mock
	private CustomerInteractionEvent customerInteractionEvent;
			
	@InjectMocks
	private MobileAddPayeeController mobileAddPayeeController;
	
	private static final String profileId = "8a81a0b7794246ca0179428815e10012";
	
	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(WebUtils.class);
		mobileAddPayeeController = PowerMockito.spy(new MobileAddPayeeController());
		customerService = PowerMockito.mock(CustomerServices.class);
		PowerMockito.mockStatic(PaymentsFacade.class);
		PowerMockito.mockStatic(AddPayeeResponse.class);
		mobileAddPayeeController.setMessageSource(messageSource);
		mobileAddPayeeController.setCustomerService(customerService);		
		
		request = new MockHttpServletRequest();
						
		String key = "123456789";
		bankingSession = new BankingSession(new BigInteger("1689012310001001"), key);
		
		payeeBean = new AddExistingPayeeBean();
		payeeBean.setBillingNumber("963851753");
		payeeBean.setBillingNumberConf("963851753");
		payeeBean.setNickname("uttestnickname");

		userSession.setLanguage("en");
		userSession.setBankingSession(bankingSession);
		
		customerProfile = new CustomerProfile();
		customerProfile.setUsername("automaremeva");
		customerProfile.setTaxId("472334747");
		customerProfile.getEntitlementByType(EntitlementType.EBILLS);
		customerProfile.setProfileId(profileId);
		customerProfile.setPermId(new BigInteger("1689012310001001"));
		
		customerInteractionEvent = new CustomerInteractionEvent();
		customerInteractionEvent.setUsername("automaremeva");
		customerInteractionEvent.setProfileId(profileId);
		customerInteractionEvent.setIp("10.2.0.4");
		customerInteractionEvent.setSessionId("18y38124913fd");
		customerInteractionEvent.setServer("servertest");
	}
	
	@Test
	public void whenSavePayee_givePayeeWithNotEbill_thenReturnSuccess()throws Exception {
		payeeBean.setPayee("331");
				
		GlobalPayee global = new GlobalPayee();
		global = new GlobalPayee();
		global.setBankId("1000");
		global.setMerchantNumber("099800005900");
		global.setName("PRT/CLARO");
		global.setShortName("PRT/CLARO");
		global.setId(new Integer(payeeBean.getPayee()));
								
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		
		when(customerService.getGlobalPayeeById(anyInt())).thenReturn(global);		
		
		when(PaymentsFacade.getInstance()).thenReturn(paymentsFacade);
		when(userSession.getGuidedSetupSession()).thenReturn(guidedSetupSession);		
		when(userSession.isEbpp2UserIdLookedUp()).thenReturn(true);
		
		when(customerService.addCustomerPayeeWithoutEbillWithErrorDescription(customerProfile, bankingSession, global, 
				"963851753", "uttestnickname", Utils.generateCustomerInteractionEvent(request), null, null, null, 
				null, false, false)).thenReturn(res);
		
		Map<String, Object> result = Whitebox.invokeMethod(mobileAddPayeeController,"savePayee",request, payeeBean);
		assertNotNull(result);
	}
	
	@Test
	public void whenSavePayee_givePayeeWithEbill_thenReturnSuccess()throws Exception {
		payeeBean.setPayee("359");
		payeeBean.setActivateEbill(true);
		payeeBean.setZipcode(null);
				
		GlobalPayee global = new GlobalPayee();
		global = new GlobalPayee();
		global.setBankId("1000");
		global.setMerchantNumber("099800005900");
		global.setName("PRT/CLARO");
		global.setShortName("PRT/CLARO");
		global.setId(new Integer(payeeBean.getPayee()));
						
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		
		when(customerService.getGlobalPayeeById(anyInt())).thenReturn(global);		
		
		when(PaymentsFacade.getInstance()).thenReturn(paymentsFacade);
		when(userSession.getGuidedSetupSession()).thenReturn(guidedSetupSession);		
		when(userSession.isEbpp2UserIdLookedUp()).thenReturn(true);
		
		Map<String, Object> result = Whitebox.invokeMethod(mobileAddPayeeController,"savePayee",request, payeeBean);
		assertNotNull(result);
	}
	
	@Test
	public void whenSavePayee_givePayeeWithEbillAndAccounIsNull_thenReturnSuccess()throws Exception {
		payeeBean.setPayee("359");
		payeeBean.setActivateEbill(true);
		payeeBean.setZipcode("00000");
		payeeBean.setBillingNumber(null);
				
		GlobalPayee global = new GlobalPayee();
		global = new GlobalPayee();
		global.setBankId("1000");
		global.setMerchantNumber("099800005900");
		global.setName("PRT/CLARO");
		global.setShortName("PRT/CLARO");
		global.setId(new Integer(payeeBean.getPayee()));
						
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		
		when(customerService.getGlobalPayeeById(anyInt())).thenReturn(global);		
		
		when(PaymentsFacade.getInstance()).thenReturn(paymentsFacade);
		when(userSession.getGuidedSetupSession()).thenReturn(guidedSetupSession);		
		when(userSession.isEbpp2UserIdLookedUp()).thenReturn(true);
				
		when(customerService.addCustomerPayeeWithEbill(customerProfile, bankingSession, global, 
				payeeBean.getBillingNumber(), payeeBean.getNickname(), true, userSession.getEbpp2UserId(), payeeBean.getZipcode(), 
				Utils.generateCustomerInteractionEvent(request), payeeBean.getPayee(), false)).thenReturn(res);
		
		Map<String, Object> result = Whitebox.invokeMethod(mobileAddPayeeController,"savePayee",request, payeeBean);
		assertNotNull(result);
	}
	
	@Test
	public void whenSavePayee_givePayeeWithEbillAndNotEqualPayee_thenReturnSuccess()throws Exception {
		payeeBean.setPayee("331");
		payeeBean.setActivateEbill(true);
		payeeBean.setZipcode("00000");
		payeeBean.setBillingNumber(null);
				
		GlobalPayee global = new GlobalPayee();
		global = new GlobalPayee();
		global.setBankId("1000");
		global.setMerchantNumber("099800005900");
		global.setName("PRT/CLARO");
		global.setShortName("PRT/CLARO");
		global.setId(new Integer(payeeBean.getPayee()));
						
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		
		when(customerService.getGlobalPayeeById(anyInt())).thenReturn(global);		
		
		when(PaymentsFacade.getInstance()).thenReturn(paymentsFacade);
		when(userSession.getGuidedSetupSession()).thenReturn(guidedSetupSession);		
		when(userSession.isEbpp2UserIdLookedUp()).thenReturn(true);
		
		when(customerService.addCustomerPayeeWithEbill(customerProfile, bankingSession, global, 
				payeeBean.getBillingNumber(), payeeBean.getNickname(), true, userSession.getEbpp2UserId(), payeeBean.getZipcode(), 
				Utils.generateCustomerInteractionEvent(request), payeeBean.getPayee(), false)).thenReturn(res);
		
		Map<String, Object> result = Whitebox.invokeMethod(mobileAddPayeeController,"savePayee",request, payeeBean);
		assertNotNull(result);
	}
	
	@Test
	public void whenSavePayee_givePayeeNull_thenReturnSuccess()throws Exception {
		AddExistingPayeeBean payeeBean = new AddExistingPayeeBean();
		
		payeeBean.setBillingNumber("963851753");
		payeeBean.setBillingNumberConf("963851753");
		payeeBean.setNickname("uttestnickname");
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getLanguage()).thenReturn("en");
		
		String ADDPAYEE_INVALIDPAYEE_ERROR = "addpayee.payee.invalid";

		when(messageSource.getMessage(ADDPAYEE_INVALIDPAYEE_ERROR, null, new Locale("es"))).thenReturn("");
		Map<String, Object> result = Whitebox.invokeMethod(mobileAddPayeeController,"savePayee",request, payeeBean);
		assertNotNull(result);
	}
	
	@Test
	public void whenSavePayee_giveGlobalNull_thenReturnSuccess()throws Exception {
		payeeBean.setPayee("331");
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getLanguage()).thenReturn("en");
		
		String ADDPAYEE_INVALIDPAYEE_ERROR = "addpayee.payee.invalid";

		when(messageSource.getMessage(ADDPAYEE_INVALIDPAYEE_ERROR, null, new Locale("es"))).thenReturn("");
		Map<String, Object> result = Whitebox.invokeMethod(mobileAddPayeeController,"savePayee",request, payeeBean);
		assertNotNull(result);
	}
	
	@Test
	public void whenSavePayee_givePayeeNickNameNull_thenReturnSuccess()throws Exception {
		payeeBean.setPayee("331");
		payeeBean.setNickname(null);
		
		List<CustomerProfile> profiles = new ArrayList<>();
		List<CustomerEntitlement4Hibernate> custEntitlements = new ArrayList<>();
		Set<CustomerEntitlement> entitlements = new HashSet<>();
		CustomerEntitlement custEntitlement = new CustomerEntitlement();
		GlobalEntitlement gloEnt = new GlobalEntitlement();
		gloEnt.setStrType("BANKING");
		
		custEntitlement.setGlobalEntitlement(gloEnt);
		custEntitlement.setProfileId(profileId);
		entitlements.add(custEntitlement);
		customerProfile.setCustEntitlements(custEntitlements);
		profiles.add(customerProfile);
		
		GlobalPayee global = new GlobalPayee();
		global = new GlobalPayee();
		global.setBankId("1000");
		global.setMerchantNumber("099800005900");
		global.setName("PRT/CLARO");
		global.setShortName("PRT/CLARO");
		global.setId(new Integer(payeeBean.getPayee()));
		BankingEntitlement bankingEntitlement = new BankingEntitlement(custEntitlement);
						
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		
		when(customerService.getGlobalPayeeById(anyInt())).thenReturn(global);		
		
		when(PaymentsFacade.getInstance()).thenReturn(paymentsFacade);
		when(userSession.getGuidedSetupSession()).thenReturn(guidedSetupSession);		
		when(userSession.isEbpp2UserIdLookedUp()).thenReturn(false);
		PowerMockito.whenNew(BankingEntitlement.class).withArguments(customerProfile.getEntitlementByType(EntitlementType.EBILLS)).thenReturn(bankingEntitlement);
		
		Map<String, Object> result = Whitebox.invokeMethod(mobileAddPayeeController,"savePayee",request, payeeBean);
		assertNotNull(result);
	}

}
