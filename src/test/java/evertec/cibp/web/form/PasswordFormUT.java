package evertec.cibp.web.form;

import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.campaign.loader.CampaignPaths;
import evertec.cibp.core.models.customer.CustomerDevices;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.profile.ProfileType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.ValidateLoginInfoRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.PasswordBean;
import evertec.cibp.web.form.dto.DeviceInfoDto;
import evertec.cibp.web.models.devices.DeviceInfo;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.DeviceUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CustomerInteractionEvent.class, CustomerServices.class, StaticMessageSource.class, CustomerUtils.class, WebUtils.class, DeviceUtils.class })
public class PasswordFormUT extends AbstractModelAndViewTests {
	
    private static final String IS_SECURITIES_TRUE = "true";
    private static final String IS_SECURITIES_FALSE = "false";
    private static final String SECURITIES_CUSTOMER = "valdesec";
    private static final String NOT_SECURITIES_CUSTOMER = "tuhijo1";
    private static final String KEY_SECURITIES = "isSecuritiesCustomer";
    private static final String KEY_MBFIS_MOBILE = "MBFIS_MOBILE";
    private static final String PATH = "/test/";
    private static final String USER_SESSION = "userSession";
    
    public static final String BROWSER_CHROME = "Chrome";
    public static final String IPHONE_APP_USERAGENT = "iPhone";
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private DeviceInfoDto deviceUtilsDto;

    @Mock
    private CustomerProfile customerProfile;
    
    @Mock
    private CustomerServices customerServices;
    
    @Mock
    private ModifyCustomerProfileResponse modifyCustomerProfileResponse;
    
    @Mock
    private DeviceRequest deviceRequest;

    @Mock
    private Map<String, Object> map;

    @Mock
    private PasswordBean command;

    @Mock
    private UserSession userSession;
    
    @Mock
    private BindException errors;

    @Mock
    private CampaignPaths paths;

    @Mock
    private ProfileType profileType;

    @InjectMocks
    private PasswordForm controller;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        request = new MockHttpServletRequest();
        request.addHeader("user-agent", BROWSER_CHROME);
        response = new MockHttpServletResponse();
        deviceUtilsDto = new DeviceInfoDto();
        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(CustomerUtils.class);
        PowerMockito.mockStatic(DeviceUtils.class);
        when(WebUtils.getSessionAttribute(request, USER_SESSION)).thenReturn(userSession);
        when(command.getAnswer()).thenReturn("answer");
        when(userSession.getUsername()).thenReturn("username");
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(DeviceUtils.getCustomerDevice(any())).thenReturn(CustomerDevices.IPOD);
        when(CustomerUtils.isSecurities(customerProfile)).thenReturn(Boolean.TRUE);
        when(userSession.getCustomerProfile().getStatus()).thenReturn(CustomerStatus.ENROLLED);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(customerServices.validatePasswordSecurities(any(ValidateLoginInfoRequest.class))).thenReturn(modifyCustomerProfileResponse);
        when(modifyCustomerProfileResponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
    }

    @Test
    public void whenReferenceData_GivenIsSecuritiesCustomerFalse_ReturnFalse () {

        try {

            customerProfile.setIsSecurities(IS_SECURITIES_FALSE);
            when(userSession.getCustomerProfile()).thenReturn(customerProfile);
            when(CustomerUtils.isSecurities(customerProfile)).thenReturn(Boolean.FALSE);
            when(customerProfile.getIsSecurities()).thenReturn(IS_SECURITIES_FALSE);
            when(userSession.getUsername()).thenReturn(NOT_SECURITIES_CUSTOMER);
            when(WebUtils.getSessionAttribute(request, USER_SESSION)).thenReturn(userSession);
            when(AccountUtils.getIsTransactional(customerProfile)).thenReturn("N");
        
            paths.setFilePath(PATH);
            when(paths.getFilePath()).thenReturn(PATH);
            when(paths.getImagePath()).thenReturn(PATH);
            controller.setPaths(paths);
        
            map = controller.referenceData(request, command, new BindException("", ""));
            assertTrue(map.containsKey(KEY_SECURITIES) && map.get(KEY_SECURITIES).equals(Boolean.FALSE));
        } catch (Exception e) {
            fail(e.getMessage());
        }

	}

    @Test
    public void whenReferenceData_GivenIsSecuritiesCustomerTrue_ReturnTrue () {

        try {

            customerProfile.setIsSecurities(IS_SECURITIES_TRUE);
            when(userSession.getCustomerProfile()).thenReturn(customerProfile);
            when(CustomerUtils.isSecurities(customerProfile)).thenReturn(Boolean.TRUE);
            when(customerProfile.getIsSecurities()).thenReturn(IS_SECURITIES_TRUE);
            when(userSession.getUsername()).thenReturn(SECURITIES_CUSTOMER);
            when(WebUtils.getSessionAttribute(request, USER_SESSION)).thenReturn(userSession);
            when(AccountUtils.getIsTransactional(customerProfile)).thenReturn("N");
            paths.setFilePath(PATH);
            when(paths.getFilePath()).thenReturn(PATH);
            when(paths.getImagePath()).thenReturn(PATH);

            controller.setPaths(paths);
            map = controller.referenceData(request, command, new BindException("", ""));

            assertTrue(map.containsKey(KEY_SECURITIES) && map.get(KEY_SECURITIES).equals(Boolean.TRUE));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    @Test
    public void whenReferenceData_GivenIsSecuritiesCustomerFalseAndIsTransactional_ReturnFalse () {

        try {

            customerProfile.setIsSecurities(IS_SECURITIES_FALSE);
            when(userSession.getCustomerProfile()).thenReturn(customerProfile);
            when(CustomerUtils.isSecurities(customerProfile)).thenReturn(Boolean.FALSE);
            when(customerProfile.getIsSecurities()).thenReturn(IS_SECURITIES_FALSE);
            when(userSession.getUsername()).thenReturn(NOT_SECURITIES_CUSTOMER);
            when(WebUtils.getSessionAttribute(request, USER_SESSION)).thenReturn(userSession);
            when(AccountUtils.getIsTransactional(customerProfile)).thenReturn("Y");
        
            paths.setFilePath(PATH);
            when(paths.getFilePath()).thenReturn(PATH);
            when(paths.getImagePath()).thenReturn(PATH);
            controller.setPaths(paths);
        
            map = controller.referenceData(request, command, new BindException("", ""));
            assertTrue(map.containsKey(KEY_SECURITIES) && map.get(KEY_SECURITIES).equals(Boolean.FALSE));
        } catch (Exception e) {
            fail(e.getMessage());
        }

	}


    @Test
    public void whenReferenceData_GivenMBFIS_MOBILE_Flag_True_ReturnTrue () {

        try {

            customerProfile.setIsSecurities(IS_SECURITIES_TRUE);
            when(customerProfile.getIsSecurities()).thenReturn(IS_SECURITIES_TRUE);
            when(userSession.getUsername()).thenReturn(SECURITIES_CUSTOMER);
            when(userSession.getCustomerProfile()).thenReturn(customerProfile);
            when(WebUtils.getSessionAttribute(request, USER_SESSION)).thenReturn(userSession);
            when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_MOBILE)).thenReturn(Boolean.TRUE);

            paths.setFilePath(PATH);
            when(paths.getFilePath()).thenReturn(PATH);
            when(paths.getImagePath()).thenReturn(PATH);

            controller.setPaths(paths);
            map = controller.referenceData(request, command, new BindException("", ""));
            assertTrue(map.containsKey(KEY_MBFIS_MOBILE) && map.get(KEY_MBFIS_MOBILE).equals(Boolean.TRUE));

        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void whenReferenceData_GivenMBFIS_MOBILE_Flag_False_ReturnFalse () {

        try {
            customerProfile.setIsSecurities(IS_SECURITIES_TRUE);
            when(customerProfile.getIsSecurities()).thenReturn(IS_SECURITIES_TRUE);
            when(userSession.getUsername()).thenReturn(SECURITIES_CUSTOMER);
            when(userSession.getCustomerProfile()).thenReturn(customerProfile);
            when(WebUtils.getSessionAttribute(request, USER_SESSION)).thenReturn(userSession);
            when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_MOBILE)).thenReturn(Boolean.FALSE);

            paths.setFilePath(PATH);
            when(paths.getFilePath()).thenReturn(PATH);
            when(paths.getImagePath()).thenReturn(PATH);

            controller.setPaths(paths);
            map = controller.referenceData(request, command, new BindException("", ""));
            assertTrue(map.containsKey(KEY_MBFIS_MOBILE) && map.get(KEY_MBFIS_MOBILE).equals(Boolean.FALSE));

        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    @Test
    public void whenOnSubmit_Given_MBSE_1980_Flag_FalseAndUserAgentIsWeb_ReturnView() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.FALSE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
        assertViewName(modelAndView, "login");
    }
    
    @Test
    public void whenOnSubmit_Given_MBSE_1980_Flag_FalseAndUserAgentIsAndroid_ReturnView() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.FALSE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.ANDROID);
        ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
        assertViewName(modelAndView, "login");
    }
    
    @Test
    public void whenOnSubmit_Given_MBSE_1980_Flag_FalseAndUserAgentIsIphone_ReturnView() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.FALSE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
        assertViewName(modelAndView, "login");
    }
    
    @Test
    public void whenOnSubmit_Given_MBSE_1980_Flag_FalseAndUserAgentIsIpad_ReturnView() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.FALSE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPAD);
        ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
        assertViewName(modelAndView, "login");
    }
    
    @Test
    public void whenOnSubmit_Given_MBSE_1980_Flag_TrueAndUserAgentIsWeb_ReturnView() throws Exception {
                
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.TRUE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        when(DeviceUtils.getBrowserInfo(BROWSER_CHROME, userSession)).thenReturn("");
        ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
        assertViewName(modelAndView, "login");
    }
    
    @Test
    public void whenOnSubmit_Given_MBSE_1980_Flag_TrueAndUserAgentIsAndroid_ReturnView() throws Exception {
                
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.TRUE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.ANDROID);
        when(DeviceUtils.getBrowserInfo(BROWSER_CHROME, userSession)).thenReturn("");
        ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
        assertViewName(modelAndView, "login");
    }
    
    @Test
    public void whenOnSubmit_Given_MBSE_1980_Flag_TrueAndUserAgentIsIphone_ReturnView() throws Exception {
                
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.TRUE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(DeviceUtils.getBrowserInfo(BROWSER_CHROME, userSession)).thenReturn("");
        ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
        assertViewName(modelAndView, "login");
    }
    
    @Test
    public void whenOnSubmit_Given_MBSE_1980_Flag_TrueAndUserAgentIsIpad_ReturnView() throws Exception {
                
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.TRUE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPAD);
        when(DeviceUtils.getBrowserInfo(BROWSER_CHROME, userSession)).thenReturn("");
        ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
        assertViewName(modelAndView, "login");
    }
    
    @Test
    public void whenOnSubmit_Given_MBSE_1980_Flag_TrueAndUserAgentJSON_ThenReturnLoginView() throws Exception {
                
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.TRUE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
        when(command.getDeviceModel()).thenReturn("Iphone X");
        when(command.getAppVersion()).thenReturn("3.3.3");
        when(command.getOsVersion()).thenReturn("12.1.2");
        when(command.getNetworkProvider()).thenReturn("Claro Puerto Rico");
        when(DeviceUtils.getMobileInfo(any(String.class), any(DeviceInfo.class))).thenReturn("");
        ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
        assertViewName(modelAndView, "login");
    }

    /**
     * when ReferenceData
     * Given IsCookieCategoryEnabledTrue
     * Then ReturnModelValueTrue
     *
     * @author Evertec ET58344
     * @since 09-22-2020
     */
    @Test
    public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () {

        try {
            when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
            customerProfile.setIsSecurities(IS_SECURITIES_TRUE);
            when(customerProfile.getIsSecurities()).thenReturn(IS_SECURITIES_TRUE);
            when(userSession.getUsername()).thenReturn(SECURITIES_CUSTOMER);
            when(userSession.getCustomerProfile()).thenReturn(customerProfile);
            when(WebUtils.getSessionAttribute(request, USER_SESSION)).thenReturn(userSession);
            when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_MOBILE)).thenReturn(Boolean.FALSE);

            paths.setFilePath(PATH);
            when(paths.getFilePath()).thenReturn(PATH);
            when(paths.getImagePath()).thenReturn(PATH);

            controller.setPaths(paths);
            map = controller.referenceData(request, command, new BindException("", ""));
            assertEquals(Boolean.TRUE, map.get("cookieAnalyticEnabled"));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * when OnSubmit
     * Given IsCookieCategoryEnabledTrue
     * Then ReturnModelValueTrue
     *
     * @author Evertec ET58344
     * @since 09-22-2020
     * @exception Exception error
     */
    @Test
    public void whenOnSubmit_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

        when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1980)).thenReturn(Boolean.TRUE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
        when(command.getDeviceModel()).thenReturn("Iphone X");
        when(command.getAppVersion()).thenReturn("3.3.3");
        when(command.getOsVersion()).thenReturn("12.1.2");
        when(command.getNetworkProvider()).thenReturn("Claro Puerto Rico");
        when(DeviceUtils.getMobileInfo(any(String.class), any(DeviceInfo.class))).thenReturn("");
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getProfileType()).thenReturn(profileType);
        when(profileType.getId()).thenReturn(ProfileType.Type.RETAIL.getId());
        ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
        assertNull(modelAndView.getModel().get("cookieAnalyticEnabled"));
    }
}
