package evertec.cibp.web.form.interruptions;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.eloan.nerds.services.NerdsException;

import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ WebUtils.class, RSAUtils.class})
public class InterruptionFormUT extends AbstractModelAndViewTests {
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	

	@InjectMocks
	private InterruptionForm controller;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		request = new MockHttpServletRequest();

		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(RSAUtils.class);
	}
	
	@Test
	public void whenLoginInterruptionPage_givenUserWithInterruption_thenRedirectToInterruption () throws NerdsException {
		UserSession userSession = Mockito.mock(UserSession.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(RSAUtils.isOOBInterruption(userSession)).thenReturn(true);
		
		ModelAndView modelView = controller.loginInterruptionPage(request, response);
		
		assertEquals("forward:oobInterruptionPage", modelView.getViewName());
	}
	
	@Test
	public void whenLoginInterruptionPage_givenUserWithoutInterruption_thenNotRedirectToInterruption () throws NerdsException {
		UserSession userSession = Mockito.mock(UserSession.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(RSAUtils.isOOBInterruption(userSession)).thenReturn(false);
		
		ModelAndView modelView = controller.loginInterruptionPage(request, response);
		assertNull(modelView);
	}

}
