package evertec.cibp.web.form.interruptions;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.owasp.csrfguard.CsrfGuardException;
import org.owasp.csrfguard.util.RandomGenerator;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.eloan.nerds.services.NerdsException;

import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.web.services.modules.OOBInterruptionModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Utils.class, WebUtils.class, RandomGenerator.class, RSAUtils.class})
public class OOBInterruptionFormIT {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Mock
	UserSession userSession;
	
	@Mock
	CustomerProfile customerProfile;
	
	@Mock
	OOBInterruptionModule oobInterruptionModule;
	
	private static final int SKIPS_ENABLED = Integer.valueOf(0);
	
	private static final int SKIPS_ENABLED_2 = Integer.valueOf(3);
	
	@InjectMocks
	private OOBInterruptionForm oobInterruptionForm;
	
	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(RSAUtils.class);
		PowerMockito.mockStatic(RandomGenerator.class);
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		when(Utils.getUserSession(any())).thenReturn(userSession);
	
	}

	@Test
	public void whenHandleRequest_GivensLoginInterruptionEnableFalse_ThenReturnPortal() throws ServletException, IOException, Exception {
		ModelAndView modelAndView = oobInterruptionForm.handleRequest(request, response);
		assertNotNull(modelAndView);
		verify(userSession).getIsLoginInterruptionEnable();
	}
	
	@Test
	public void whenHandleRequest_GivensLoginInterruptionEnableTrue_ThenReturnPortal() throws ServletException, IOException, Exception {
		when(userSession.getIsLoginInterruptionEnable()).thenReturn(true);
		request.setMethod("GET");
		oobInterruptionForm.handleRequest(request, response);
		verify(userSession).getIsLoginInterruptionEnable();
	}	
	
	@Test
	public void whenOObInterruptionPage_GivenskipsEnabledFalse_Then() throws NerdsException, CsrfGuardException {
	
		when(RSAUtils.getUserSkips(any())).thenReturn(1);
		oobInterruptionForm.setSkipsAvailable(SKIPS_ENABLED);

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		CustomerEntitlement customerEntitlement = new CustomerEntitlement();
		GlobalEntitlement globalEntitlement = new GlobalEntitlement();
		customerEntitlement.setGlobalEntitlement(globalEntitlement);
		globalEntitlement.setType(EntitlementType.MOBILE_BANKING);
		when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);

		ModelAndView modelAndView = oobInterruptionForm.oobInterruptionPage(request, response);
		assertNotNull(modelAndView);
		assertTrue(modelAndView.getViewName().toString().equals("oobInterruptionPage"));
		verify(oobInterruptionModule).logOOBInterruptionEvents(any(), any(), any());
	}

	@Test
	public void whenOObInterruptionPage_GivenskipsEnabledTrue_Then() throws NerdsException, CsrfGuardException {
	
		when(RSAUtils.getUserSkips(any())).thenReturn(1);
		oobInterruptionForm.setSkipsAvailable(SKIPS_ENABLED_2);

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		CustomerEntitlement customerEntitlement = new CustomerEntitlement();
		GlobalEntitlement globalEntitlement = new GlobalEntitlement();
		customerEntitlement.setGlobalEntitlement(globalEntitlement);
		globalEntitlement.setType(EntitlementType.MOBILE_BANKING);
		when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		request.setParameter("skipAction","test");
		request.setParameter("nextAction","test");

		ModelAndView modelAndView = oobInterruptionForm.oobInterruptionPage(request, response);
		assertNotNull(modelAndView);
		verify(oobInterruptionModule).logOOBInterruptionEvents(any(), any(), any());
	}	
}
