package evertec.cibp.web.form.interruptions;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.owasp.csrfguard.CsrfGuardException;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.eloan.nerds.services.NerdsException;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.RSAEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.CustomerProfileVault;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.services.modules.OOBInterruptionModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ Utils.class, SignonSequenceUtils.class, StaticMessageSource.class, RSAUtils.class, WebUtils.class})
public class OOBInterruptionFormUT extends AbstractModelAndViewTests {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerEntitlement customerEntitlement;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private CustomerServices customerService;
	
	@Mock
	private SMSProfile smsProfile;
	
	@Mock
	private CustomerProfileVault customerProfileVault;
	
	@Spy
	private OOBInterruptionModule oobInterruptionModule = new OOBInterruptionModule();

	@InjectMocks
	private OOBInterruptionForm controller;

	@Before
	public void setUp() throws Exception {
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();

		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(SignonSequenceUtils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(RSAUtils.class);
		PowerMockito.mockStatic(WebUtils.class);
	}
	
	@Test
	public void whenHandleRequest_givenDisabledFlagInterruption_thenReturnPortalView () throws ServletException, IOException, Exception {
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getIsLoginInterruptionEnable()).thenReturn(false);
		
		ModelAndView modelView = controller.handleRequest(request, response);
		
		assertEquals("redirect:portal", ((RedirectView)modelView.getView()).getUrl());
	}

	@Test
	public void whenOobInterruptionPage_givenWithoutParams_thenReturnOobInterruptionPageView() throws NerdsException, CsrfGuardException {
		ReflectionTestUtils.setField(controller, "skipsAvailable", 1);
		ReflectionTestUtils.setField(controller, "customerService", customerService);
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerEntitlement.getString(any())).thenReturn(RSAEntitlement.INITIAL_SKIP);
		when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);

		Mockito.doNothing().when(oobInterruptionModule).logOOBInterruptionEvents(any(), any(), any());		
		
		ModelAndView modelView = controller.oobInterruptionPage(request,response);

		assertViewName(modelView, "oobInterruptionPage");
		assertEquals("vm/oobInterruptionPage.vm", modelView.getModel().get("view"));
	}

	@Test
	public void whenOobInterruptionPage_givenWithSkipParams_thenReturnRedirectWelcomeView() throws NerdsException, CsrfGuardException {
		request.addParameter("skipAction", "skipAction");

		ReflectionTestUtils.setField(controller, "skipsAvailable", 1);
		ReflectionTestUtils.setField(controller, "customerService", customerService);
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerEntitlement.getString(any())).thenReturn(RSAEntitlement.INITIAL_SKIP);
		when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);
		when(SignonSequenceUtils.getNextView(any())).thenReturn("redirect:welcome");
		Mockito.doNothing().when(oobInterruptionModule).logOOBInterruptionEvents(any(), any(), any());		

		ModelAndView modelView = controller.oobInterruptionPage(request,response);

		RedirectView redirectView = (RedirectView) modelView.getView();
		assertEquals("welcome", redirectView.getUrl());
	}
	
	@Test
	public void whenoobInterruptionPage_givenSettingsUI_returnAllValuesInModel () throws NerdsException, CsrfGuardException {
		request.addHeader("user-agent", "iPhone App; MiBanco.app; JSON Client; es-mx");
		
		ReflectionTestUtils.setField(controller, "skipsAvailable", 1);
		ReflectionTestUtils.setField(controller, "customerService", customerService);
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerEntitlement.getString(any())).thenReturn(RSAEntitlement.INITIAL_SKIP);
		when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);
		when(customerProfile.getFirstName()).thenReturn("pepe");

		smsProfile = PowerMockito.mock(SMSProfile.class);
		when(userSession.getSmsProfile(any())).thenReturn(smsProfile);
        when(smsProfile.getSmsProfileId()).thenReturn("7879231845");
        
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctns.add(ctn);
        when(smsProfile.getCtns()).thenReturn(ctns);
        
        when(userSession.getCustomerProfile().getEmail()).thenReturn("test@test.com");
        
		Mockito.doNothing().when(oobInterruptionModule).logOOBInterruptionEvents(any(), any(), any());		

		ModelAndView modelView = controller.oobInterruptionPage(request,response);

		String EDIT_RSA_PAGE = "rsaEdit";
		String ONSEN_LIBS = "onsenUI";
		String TIME_INTERVAL = "timeInterval";
		
		assertEquals(false, modelView.getModel().get(EDIT_RSA_PAGE));
		assertEquals(true, modelView.getModel().get(ONSEN_LIBS));
		assertEquals(5, modelView.getModel().get(TIME_INTERVAL));
		assertEquals("mobile", modelView.getModel().get("agentType"));
		assertEquals("pepe", modelView.getModel().get("username"));
	}
	
	@Test
	public void whenApplyForInterruptionPageWebVersion_givenIniciativeFlagDisabled_thenReturnModelViewNull() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC920_OOB_INTERRUPTION_PAGE)).thenReturn(Boolean.FALSE);
		
		ModelAndView modelView = OOBInterruptionForm.applyForInterruptionPageWebVersion(userSession);
		
		assertNull(modelView);
	}
	
	@Test
	public void whenApplyForInterruptionPageWebVersion_givenRsaUtilsDisabled_thenReturnModelViewNull() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC920_OOB_INTERRUPTION_PAGE)).thenReturn(Boolean.TRUE);
		when(RSAUtils.isOOBInterruption(userSession)).thenReturn(false);
		
		ModelAndView modelView = OOBInterruptionForm.applyForInterruptionPageWebVersion(userSession);
		
		assertNull(modelView);
	}
	
	@Test
	public void whenApplyForInterruptionPageWebVersion_givenisElegibleToOutreach_thenReturnModelViewNull() {
		when(userSession.isElegibleToOutreach()).thenReturn(true);
		
		ModelAndView modelView = OOBInterruptionForm.applyForInterruptionPageWebVersion(userSession);
		
		assertNull(modelView);
	}
	
	@Test
	public void whenApplyForInterruptionPageWebVersion_givenFlagsEnabledAndJsonClient_thenReturnModelViewNull() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC920_OOB_INTERRUPTION_PAGE)).thenReturn(Boolean.TRUE);
		when(RSAUtils.isOOBInterruption(userSession)).thenReturn(true);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
		
		ModelAndView modelView = OOBInterruptionForm.applyForInterruptionPageWebVersion(userSession);
		
		assertNull(modelView);
	}
	
	@Test
	public void whenApplyForInterruptionPageWebVersion_givenFlagsEnabledAndWebClient_thenReturnModelViewNoNull() {
		ArrayList<ASK_FOR> taskList = new ArrayList<>();
		taskList.add(ASK_FOR.SESSION_INIT);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC920_OOB_INTERRUPTION_PAGE)).thenReturn(Boolean.TRUE);
		when(RSAUtils.isOOBInterruption(userSession)).thenReturn(true);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
		when(userSession.getTasksList()).thenReturn(taskList);
		when(SignonSequenceUtils.getNextView(any())).thenReturn("oobInterruptionPage");
		
		ModelAndView modelView = OOBInterruptionForm.applyForInterruptionPageWebVersion(userSession);
		
		assertNotNull(modelView);
	}
	
	@Test
	public void whenApplyForInterruptionPageMobileVersion_givenIniciativeFlagDisable_thenModelEmpty () {
		Map<String, Object> model = new HashMap<>();
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC920_OOB_INTERRUPTION_PAGE)).thenReturn(Boolean.FALSE);
		
		OOBInterruptionForm.applyForInterruptionPageMobileVersion(userSession, model);
		
		assertTrue(model.isEmpty());
	}
	
	@Test
	public void whenApplyForInterruptionPageMobileVersion_givenRsaUtilsDisabled_thenModelEmpty () {
		Map<String, Object> model = new HashMap<>();
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC920_OOB_INTERRUPTION_PAGE)).thenReturn(Boolean.TRUE);
		when(RSAUtils.isOOBInterruption(userSession)).thenReturn(false);
		
		OOBInterruptionForm.applyForInterruptionPageMobileVersion(userSession, model);
		
		assertTrue(model.isEmpty());
	}
	
	@Test
	public void whenApplyForInterruptionPageMobileVersion_givenFlagsEnabledAndWebClient_thenModelEmpty () {
		Map<String, Object> model = new HashMap<>();
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC920_OOB_INTERRUPTION_PAGE)).thenReturn(Boolean.TRUE);
		when(RSAUtils.isOOBInterruption(userSession)).thenReturn(true);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
		
		OOBInterruptionForm.applyForInterruptionPageMobileVersion(userSession, model);
		
		assertTrue(model.isEmpty());
	}
	
	@Test
	public void whenApplyForInterruptionPageMobileVersion_givenFlagsEnabledAndJsonClient_thenModelWithData () {
		Map<String, Object> model = new HashMap<>();
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC920_OOB_INTERRUPTION_PAGE)).thenReturn(Boolean.TRUE);
		when(RSAUtils.isOOBInterruption(userSession)).thenReturn(true);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
		
		OOBInterruptionForm.applyForInterruptionPageMobileVersion(userSession, model);
		
		assertFalse(model.isEmpty());
		assertTrue((boolean) model.get("outreach"));
		assertTrue((boolean) model.get("interruptionPage"));
	}
	
	@Test
	public void whenApplyCompatibilityWithOuchReach_givenOOBFlagDisabled_thenReturnOutReachView () {
		ModelAndView modelView = OOBInterruptionForm.applyCompatibilityWithOuchReach(userSession);
		
		assertEquals("redirect:outreach?close", modelView.getViewName());
	}
	
	@Test
	public void whenApplyCompatibilityWithOuchReach_givenOOBFlagEnabled_thenReturOobInterruptionView () {
		when(RSAUtils.isOOBInterruption(any())).thenReturn(true);
		
		ModelAndView modelView = OOBInterruptionForm.applyCompatibilityWithOuchReach(userSession);
		
		assertEquals("forward:loginInterruptionPage", modelView.getViewName());
	}
	
	@Test
	public void whenIsUserAgentMobileApp_whenAndroidUserAgent_thenReturnTrue () {
		request.addHeader("user-agent", "Android");
		String isMobile = controller.getUserAgentMobileApp(request);
		assertNotNull(isMobile);
		assertTrue(isMobile.equals("mobile"));
	}

	@Test
	public void whenIsUserAgentMobileApp_wheniOSUserAgent_thenReturnTrue () {
		request.addHeader("user-agent", "iPhone");
		String isMobile = controller.getUserAgentMobileApp(request);
		assertNotNull(isMobile);
		assertTrue(isMobile.equals("mobile"));
	}
	
	@Test
	public void whenIsUserAgentMobileApp_whenMiBancoAppUserAgent_thenReturnTrue () {
		request.addHeader("user-agent", "iPhone App; MiBanco.app; JSON Client; es-mx");
		String isMobile = controller.getUserAgentMobileApp(request);
		assertNotNull(isMobile);
		assertTrue(isMobile.equals("mobile"));
	}
	
	@Test
	public void whenIsUserAgentMobileApp_whenWebViewUserAgent_thenReturnTrue () {
		request.addHeader("user-agent", "WebView");
		String isMobile = controller.getUserAgentMobileApp(request);
		assertNotNull(isMobile);
		assertTrue(isMobile.equals("mobile"));
	}
	
	@Test
	public void whenIsUserAgentMobileApp_whenDesktopUserAgent_thenReturnFalse () {
		request.addHeader("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36");
		String isMobile = controller.getUserAgentMobileApp(request);
		assertNotNull(isMobile);
		assertTrue(isMobile.equals("web"));
	}
	
	@Test
	public void whenGetPhoneNumber_givenMobilePhoneNumber_thenReturnMobilePhoneNumber() {
		when(userSession.getCustomerProfileVault()).thenReturn(customerProfileVault);
		when(customerProfileVault.getMobilePhoneNumber()).thenReturn("5555555555");
		
		Map.Entry<String, String> phoneDetail = controller.getPhoneNumber(userSession, request);
		assertEquals(phoneDetail.getValue(), "5555555555");
	}
	
	@Test
	public void whenGetPhoneNumber_givenMobileHomeNumber_thenReturnMobileHomeNumber() {
		when(userSession.getCustomerProfileVault()).thenReturn(customerProfileVault);
		when(customerProfileVault.getHomeNumber()).thenReturn("4444444444");
		
		Map.Entry<String, String> phoneDetail = controller.getPhoneNumber(userSession, request);
		assertEquals(phoneDetail.getValue(), "4444444444");
	}
	
	@Test
	public void whenCanContinue_givenWebUserWithInterruptionAndPortalRequest_thenResultFalse () {
		request.setPathInfo("/portal");
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
		when(userSession.getIsLoginInterruptionEnable()).thenReturn(true);
		
		boolean result = OOBInterruptionForm.canContinue(userSession, request);
		
		assertFalse(result);
	}
	
	@Test
	public void whenCanContinue_givenJSONbUserWithInterruptionAndPortalRequest_thenResultTrue () {
		request.setPathInfo("/portal");
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
		when(userSession.getIsLoginInterruptionEnable()).thenReturn(true);
		
		boolean result = OOBInterruptionForm.canContinue(userSession, request);
		
		assertTrue(result);
	}
	
	@Test
	public void whenCanContinue_givenWebUserWithInterruptionAndLoginInterruptionRequest_thenResultTrue () {
		request.setPathInfo("/loginInterruptionPage");
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
		when(userSession.getIsLoginInterruptionEnable()).thenReturn(true);
		
		boolean result = OOBInterruptionForm.canContinue(userSession, request);
		
		assertTrue(result);
	}
	
	@Test
	public void whenCanContinue_givenWebUserWithoutInterruptionAndPortalRequest_thenResultTrue () {
		request.setPathInfo("/portal");
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
		when(userSession.getIsLoginInterruptionEnable()).thenReturn(false);
		
		boolean result = OOBInterruptionForm.canContinue(userSession, request);
		
		assertTrue(result);
	}
	
	@Test
	public void whenrsaSMSConfirmCode_whenGivenCorrectCode_thenResultSucess () throws Exception {
		request.setParameter("codeNumberInp", "1111");
		
		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when( WebUtils.getSessionAttribute(request, "smsGeneratedCode")).thenReturn("1111");
		
		ModelAndView result = controller.rsaSMSConfirmCode(request, response);
		
		assertTrue(result.getModel().get("result").equals("success"));
	}
	
	@Test
	public void whenrsaSMSConfirmCode_whenGivenIncorrectCode_thenResultFail () throws Exception {
		request.setParameter("codeNumberInp", "2222");
		
		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when( WebUtils.getSessionAttribute(request, "smsGeneratedCode")).thenReturn("1111");
		
		ModelAndView result = controller.rsaSMSConfirmCode(request, response);
		
		assertTrue(result.getModel().get("result").equals("fail"));
	}
	
	@Test
	public void whensaveOOBVoicePhoneNumber_givenFalseResendParams_thenResultSuccess () throws Exception {
		when(Utils.getUserSession(any())).thenReturn(userSession);
		request.setParameter("resend", "false");
		request.setParameter("mainNumberInp", "5555555555");
		request.setParameter("carrier", "NOC");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)).thenReturn(Boolean.TRUE);
		
		ModelAndView result = controller.saveOOBVoicePhoneNumber(request, response);
		
		assertTrue(result.getModel().get("result").equals("success"));
	}
	
	@Test
	public void whensaveOOBVoicePhoneNumber_givenTrueResendParams_thenResultSuccess () throws Exception {
		when(Utils.getUserSession(any())).thenReturn(userSession);
		request.setParameter("resend", "true");
		when( WebUtils.getSessionAttribute(request, "smsPhonereceived")).thenReturn("5555555555");
		when( WebUtils.getSessionAttribute(request, "smsProvider")).thenReturn("NOC");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)).thenReturn(Boolean.TRUE);
		
		ModelAndView result = controller.saveOOBVoicePhoneNumber(request, response);
		
		assertTrue(result.getModel().get("result").equals("success"));
	}
	
	@Test
	public void whensaveOOBVoicePhoneNumber_givenEmptyParams_thenResultFail () throws Exception {
		when(Utils.getUserSession(any())).thenReturn(userSession);
		request.setParameter("resend", "false");
		request.setParameter("mainNumberInp", "");
		request.setParameter("carrier", "");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)).thenReturn(Boolean.TRUE);
		
		ModelAndView result = controller.saveOOBVoicePhoneNumber(request, response);
		
		assertTrue(result.getModel().get("result").equals("fail"));
	}
	
	@Test
	public void whensaveOOBVoicePhoneNumber_givenNullParams_thenResultFail () throws Exception {
		when(Utils.getUserSession(any())).thenReturn(userSession);
		request.setParameter("resend", "false");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)).thenReturn(Boolean.TRUE);
		
		ModelAndView result = controller.saveOOBVoicePhoneNumber(request, response);
		
		assertTrue(result.getModel().get("result").equals("fail"));
	}
	
	@Test
	public void whensaveOOBVoicePhoneNumber_givenNullResend_thenResultFail () throws Exception {
		when(Utils.getUserSession(any())).thenReturn(userSession);
		String resend = null;
		request.setParameter("resend", resend);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)).thenReturn(Boolean.TRUE);
		
		ModelAndView result = controller.saveOOBVoicePhoneNumber(request, response);
		
		assertTrue(result.getModel().get("result").equals("fail"));
	}	
	
	@Test
	public void whensaveOOBVoicePhoneNumber_givenNullmainNumberInp_thenResultFail () throws Exception {
		when(Utils.getUserSession(any())).thenReturn(userSession);
		String resend = null;
		request.setParameter("resend", resend);
		String mainNumberInp = null;
		request.setParameter("mainNumberInp", mainNumberInp);
		String carrier = null;
		request.setParameter("carrier", carrier);		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)).thenReturn(Boolean.TRUE);
		
		ModelAndView result = controller.saveOOBVoicePhoneNumber(request, response);
		
		assertTrue(result.getModel().get("result").equals("fail"));
	}	
	
	@Test
	public void whensaveOOBVoicePhoneNumber_givenResend_thenResultFail () throws Exception {
		when(Utils.getUserSession(any())).thenReturn(userSession);
		request.setParameter("resend", "otro");
		request.setParameter("mainNumberInp", "");
		request.setParameter("carrier", "");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)).thenReturn(Boolean.TRUE);
		
		ModelAndView result = controller.saveOOBVoicePhoneNumber(request, response);
		
		assertTrue(result.getModel().get("result").equals("fail"));
	}	
	
	@Test
	public void whenGetOobInterruptionContactInfo_GivenResultFail_ThenRSAForm() throws Exception {
		UserSession userSession = Mockito.mock(UserSession.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(Utils.isMobile(userSession)).thenReturn(false);
		when(userSession.isInformationChallenge()).thenReturn(false);
		
		ModelAndView modelAndView = controller.getOobInterruptionContactInfo(request, response);
		assertNotNull(modelAndView);
		assertTrue(modelAndView.getModel().get("result").equals("fail"));

	}
	
	@Test
	public void whenGetOobInterruptionContactInfo_GivenResultEmailPhoneNull_ThenRSAForm() throws Exception {
		UserSession userSession = Mockito.mock(UserSession.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(Utils.isMobile(userSession)).thenReturn(false);
		when(userSession.isInformationChallenge()).thenReturn(true);
		CustomerProfileVault customerProfileVault = new CustomerProfileVault();
		customerProfileVault.setHomeNumber(null);
		customerProfileVault.setMobilePhoneNumber(null);
		customerProfileVault.setSecondaryPhone(null);
		when(userSession.getCustomerProfileVault()).thenReturn(customerProfileVault);
		
		ModelAndView modelAndView = controller.getOobInterruptionContactInfo(request, response);
		assertNotNull(modelAndView);
		assertTrue(modelAndView.getModel().get("result").equals("success"));

	}
	
	@Test
	public void whenGetOobInterruptionContactInfo_GivenResultPhoneShort_ThenRSAForm() throws Exception {
		UserSession userSession = Mockito.mock(UserSession.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(Utils.isMobile(userSession)).thenReturn(false);
		when(userSession.isInformationChallenge()).thenReturn(true);
		CustomerProfileVault customerProfileVault = new CustomerProfileVault();
		customerProfileVault.setHomeNumber("1");
		customerProfileVault.setMobilePhoneNumber(null);
		customerProfileVault.setSecondaryPhone(null);
		when(userSession.getCustomerProfileVault()).thenReturn(customerProfileVault);
		
		ModelAndView modelAndView = controller.getOobInterruptionContactInfo(request, response);
		assertNotNull(modelAndView);
		assertTrue(modelAndView.getModel().get("result").equals("success"));

	}	
	
	@Test
	public void whenGetOobInterruptionContactInfo_GivenResultSuccess_ThenRSAForm() throws Exception {
		UserSession userSession = Mockito.mock(UserSession.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(Utils.isMobile(userSession)).thenReturn(true);
		when(userSession.isInformationChallenge()).thenReturn(true);
		CustomerEntitlement custEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);

		smsProfile = PowerMockito.mock(SMSProfile.class);
		when(userSession.getSmsProfile(any())).thenReturn(smsProfile);
        when(smsProfile.getSmsProfileId()).thenReturn("7879231845");
        
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctns.add(ctn);
        when(smsProfile.getCtns()).thenReturn(ctns);		
		when(customerProfile.getEmail()).thenReturn("f@gmail.com");
		when(customerProfile.getEntitlementByType(any())).thenReturn(custEntitlement);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		
		
		ModelAndView modelAndView = controller.getOobInterruptionContactInfo(request, response);
		assertNotNull(modelAndView);
		assertTrue(modelAndView.getModel().get("result").equals("success"));

	}	
	
	@Test
	public void whenSaveEventsOOBInterruption_GivenResultSuccess_ThenRSAForm() {
		when(Utils.getUserSession(any())).thenReturn(userSession);
		request.setParameter("eventType", "test");

		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();		
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		
		UserSession userSession = Mockito.mock(UserSession.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		
		Mockito.doNothing().when(oobInterruptionModule).logOOBInterruptionEvents(any(), any(), any());	
		
		ModelAndView modelAndView = controller.saveEventsOOBInterruption(request, response);
		assertNotNull(modelAndView);
		assertTrue(modelAndView.getModel().get("result").equals("success"));
		
	}
	
	@Test
	public void whenSaveEventsOOBInterruption_GivenResultNoSuccess_ThenRSAForm() {
		when(Utils.getUserSession(any())).thenReturn(userSession);
		request.setParameter("eventType", "");

		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();		
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		
		UserSession userSession = Mockito.mock(UserSession.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		
		Mockito.doNothing().when(oobInterruptionModule).logOOBInterruptionEvents(any(), any(), any());	
		
		ModelAndView modelAndView = controller.saveEventsOOBInterruption(request, response);
		assertNotNull(modelAndView);
		assertTrue(modelAndView.getModel().get("result").equals(""));
		
	}	
	
}
