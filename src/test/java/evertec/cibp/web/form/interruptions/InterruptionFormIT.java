package evertec.cibp.web.form.interruptions;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;
import com.eloan.nerds.services.NerdsException;

import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ WebUtils.class, Utils.class, RSAUtils.class })
public class InterruptionFormIT {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Mock
	UserSession userSession;

	@Mock
	HttpSession session;

	@Mock
	RSAUtils rsaUtils;

	@InjectMocks
	private InterruptionForm interruptionForm; // = new InterruptionForm();

	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(RSAUtils.class);
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
	}

	@SuppressWarnings("static-access")
	@Test
	public void whenLoginInterruptionPage_GivenOOBInterruptionTrue_ThenViewNameOOBInterruptionPage()
			throws NerdsException {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(RSAUtils.isOOBInterruption(Mockito.any(UserSession.class))).thenReturn(true);

		ModelAndView modelAndView = interruptionForm.loginInterruptionPage(request, response);
		assertNotNull(modelAndView);
		assertTrue(modelAndView.getViewName().equals("forward:oobInterruptionPage"));
		verify(rsaUtils, times(1)).isOOBInterruption(any());

	}

	@SuppressWarnings("static-access")
	@Test
	public void whenLoginInterruptionPage_GivenUserSessionNull_ThenViewNameNull() throws NerdsException {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(null);

		ModelAndView modelAndView = interruptionForm.loginInterruptionPage(request, response);
		assertNull(modelAndView);
		verify(rsaUtils, times(1)).isOOBInterruption(any());
	}

	@SuppressWarnings("static-access")
	@Test
	public void whenLoginInterruptionPage_GivenOOBInterruptionFalse_ThenViewNameNull() throws NerdsException {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(RSAUtils.isOOBInterruption(Mockito.any(UserSession.class))).thenReturn(false);

		ModelAndView modelAndView = interruptionForm.loginInterruptionPage(request, response);
		assertNull(modelAndView);
		verify(rsaUtils, times(1)).isOOBInterruption(any());
	}

}
