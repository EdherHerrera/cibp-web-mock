package evertec.cibp.web.form;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.Errors;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.MortgageAccountRecord;
import bppr.tv.beans.accounts.types.LoanAccount;
import bppr.tv.beans.payments.Payee;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.PaymentHistoryResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.vs.beans.payments.RecurringPayment;
import evertec.cibp.vs.models.PaymentFilterType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.PaymentBean;
import evertec.cibp.web.form.bean.PaymentSearchBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CustomerFacade.class, StaticMessageSource.class, Utils.class})
public class PaymentHistoryFormUT extends AbstractModelAndViewTests{
	
	@Mock
	private UserSession userSession;

	@Mock
	private CustomerFacade customerFacade;
	
	private MockHttpServletRequest request;

	@Mock
	private Errors errors;
	
	@Mock
	private PaymentSearchBean paymentSearchBean;
	
	@Mock
	private TVFrontEndPayee tvFrontEndPayee;
	
	@InjectMocks
	private PaymentHistoryForm paymentHistoryForm;
	
	@Mock
	private List<TVFrontEndPayee> payees;
	
	@Mock
	private List<TVPayment> listTVPayment;
	
	@Mock
	private TVPayment tvPayment;
	
	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private CustomerServices customerService;
	
	@Mock
	private MessageSource messageSource;
	
    @Mock
    private PaymentHistoryResponse transferResponse;
	
	@Mock
	private BankingSession bankingSession;
	
	@Mock
	private TVFrontendAccount account;
	
	@Mock
	private LoanAccount loanAccount;
	
	@Mock
	private MortgageAccountRecord mlaRecord;

	@InjectMocks
	private PaymentHistoryForm controller;

	
	@Before
	public void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		request = new MockHttpServletRequest();
		customerService = mock(CustomerServices.class) ;
		paymentHistoryForm.setMessageSource(messageSource);
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(customerProfile.getTaxId()).thenReturn("123456789");
	}
	
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(Boolean.TRUE);
		String payeeId ="1232312";
		LocalDate holiday = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY));
		PaymentBean paymentBeanMock = mock(PaymentBean.class);
		TVAccountTransferSource tvAccountTransferSourceMock = mock(TVAccountTransferSource.class);
		TVFrontendAccount tvFrontendAccountMock = mock(TVFrontendAccount.class);
		Payee payeeMock = mock(Payee.class);
		GlobalPayee globalPayeeMock = mock(GlobalPayee.class);
		TVFrontEndPayee payeeEndMock = mock(TVFrontEndPayee.class);
		payeeEndMock.setBackendObject(payeeMock);

		List<TVFrontEndPayee> payees = new ArrayList<TVFrontEndPayee>();
		payees.add(payeeEndMock);
		List<PaymentBean> paymentBeans = new ArrayList<PaymentBean>();
		paymentBeans.add(paymentBeanMock);
		when(paymentBeanMock.getAmount()).thenReturn(BigDecimal.ONE);
		when(paymentBeanMock.getPayeeid()).thenReturn(payeeId);
		when(payeeEndMock.getBillingAccount()).thenReturn("");
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getAccountsPaymentFrom()).thenReturn(new HashSet<>());
		when(userSession.getPayessBasicEbpp(true)).thenReturn(payees);
		
		when(Utils.getPayeebyId(payeeId, payees)).thenReturn(payeeEndMock);

		when(Utils.getAccountByAccountNumber(any(),any())).thenReturn(account);
		BigDecimal amount = new BigDecimal(899);
		when(account.getSubtype()).thenReturn("MLA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(mlaRecord);
		when (mlaRecord.getCurrentAmount()).thenReturn(amount);
		
		when(paymentBeanMock.getEffectiveDate()).thenReturn(Date.from(holiday.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(payeeEndMock.getGlobalPayee()).thenReturn(globalPayeeMock);
		when(payeeEndMock.getGlobalPayee().getRtNotification()).thenReturn("Y");
		when(payeeEndMock.getGlobalPayee().getRtIndicator()).thenReturn("N");
		List<Integer> payeeCodes = new ArrayList<>();
		payeeCodes.add(1234);
		when((payeeEndMock.getCustomerPayeeCodes())).thenReturn(payeeCodes);

		when((paymentBeanMock.getRecurrent())).thenReturn("recurrent");
		when((paymentBeanMock.getFrequency())).thenReturn(Frequency.MANUALLY);
		when(tvAccountTransferSourceMock.getAccount()).thenReturn(tvFrontendAccountMock);

		when(paymentSearchBean.getAction()).thenReturn(1);
		RecurringPayment recurringPayment = new RecurringPayment();
		List<TVPayment> customerPayments = new LinkedList<>();
    	TVPayment tvPayment = new   TVPayment(recurringPayment);
    	TVAccountTransferSource source = new TVAccountTransferSource(account);
    	
    	tvPayment.setPayee(tvFrontEndPayee);
    	tvPayment.getTVPayment().setAmount(new BigDecimal(1250));
    	tvPayment.getTVPayment().setFrequency("regl");
    	tvPayment.getTVPayment().setFrequency("regl");
    	tvPayment.getTVPayment().setRemainingInstances(123);
    	tvPayment.getTVPayment().setReferenceNumber("");
    	tvPayment.setFrontEndId("123");
    	tvPayment.setEstimateDate("sep");
    	tvPayment.setSource(source);
    	customerPayments.add(tvPayment);

    	when(customerService.getPaymentHistory(customerProfile, bankingSession, PaymentFilterType.HISTORY,15, null, null)).thenReturn(transferResponse);
		
		Map<String, Object> result = paymentHistoryForm.referenceData(request, paymentSearchBean, errors);
		assertNotNull(result);
		
	}

	
	@Test
	public void whenGetTelepagoErrorDescriptor_GivenMessageError_ThenReturnMessageErrorSucces() throws Exception {
		controller.setMessageSource(messageSource);
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("OK");

		String valueValidate = Whitebox.invokeMethod(controller, "getTelepagoErrorDescriptor", "ebpp.module", "ens");
		assertEquals(valueValidate, "OK");
	}

}
