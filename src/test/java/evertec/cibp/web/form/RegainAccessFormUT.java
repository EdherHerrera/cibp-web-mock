package evertec.cibp.web.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import evertec.cibp.web.services.modules.RegainAccessServiceModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.enums.RegainTokenResource;
import evertec.cibp.core.models.RegainAccessToken;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerProfileStatus;
import evertec.cibp.core.models.customer.MultipleCustomerProfileResponse;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.ResetPasswordBean;
import evertec.cibp.web.request.TsysLoyaltyRewardsRedemptionRequest;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

/**
 * 
 * @author s679338
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ StaticMessageSource.class, WebUtils.class })
public class RegainAccessFormUT extends AbstractModelAndViewTests {

	private final static String BEAN = "ResetPasswordBean";

	private final static String RESOURCE = "resource";

	private MockHttpServletRequest request;

	private MockHttpServletResponse response;

	@Mock
	private RegainAccessServiceModule regainAccessServiceModule;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfileService customerProfileService;

	@Mock
	private List<CustomerProfile> profiles;

	@InjectMocks
	private RegainAccessForm controller;

	@Mock
	private BindException errors;
	
	@Mock
	private MultipleCustomerProfileResponse mMultipleCustomerProfileResponse;
	
	@Mock
	private CustomerProfile mCustomerProfile;
	
	@Captor
	private ArgumentCaptor<String> urlCapture;

	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(WebUtils.class);

		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		profiles = new LinkedList<>();
		errors = new BindException(new ResetPasswordBean(), "QuestionChallengeBean");
	}

	@Test
	public void testWhenReferenceData_GivenEmptyResource_ThenReturnDefaultValue() throws Exception{
		final ResetPasswordBean bean = new ResetPasswordBean();
		final BindException error = new BindException(bean, BEAN);

		request.setParameter(RESOURCE, "");

		Map<String, Object> model = controller.referenceData(request, bean, error);
		assertEquals(RegainTokenResource.OPTIONS, model.get("resource"));
	}
	
	@Test
	public void testWhenReferenceData_GivenProfileIsSecurities_ThenThrowsModelAndViewDefiningException() {
		final ResetPasswordBean bean = new ResetPasswordBean();
		final BindException error = new BindException(bean, BEAN);
		
		when(WebUtils.getSessionAttribute(request, "regainSecurities")).thenReturn(true);

		request.setParameter(RESOURCE, "USERID");

		try {
		Map<String, Object> model = controller.referenceData(request, bean, error);
		fail("Expected ModelAndViewDefiningException.");
		}catch (ModelAndViewDefiningException e){
			assertTrue(e.getClass() == ModelAndViewDefiningException.class);
		}
		
	}

	@Test
	public void testWhenReferenceData_GivenUserIdResource_ThenReturnCorrectResource() throws Exception {
		final ResetPasswordBean bean = new ResetPasswordBean();
		final BindException error = new BindException(bean, BEAN);
		
		request.setParameter(RESOURCE, "USERID");

		Map<String, Object> model = controller.referenceData(request, bean, error);
		assertEquals(RegainTokenResource.USERID, model.get("resource"));
	}

	@Test
	public void testWhenReferenceData_GivenPswResource_ThenReturnCorrectResource() throws Exception{
		final ResetPasswordBean bean = new ResetPasswordBean();
		final BindException error = new BindException(bean, BEAN);

		request.setParameter(RESOURCE, "PSW");

		Map<String, Object> model = controller.referenceData(request, bean, error);
		assertEquals(RegainTokenResource.PSW, model.get("resource"));
	}

	@Test
	public void testWhenReferenceData_GivenRandomUserIdResource_ThenReturnDefaultResource() throws Exception{
		final ResetPasswordBean bean = new ResetPasswordBean();
		final BindException error = new BindException(bean, BEAN);

		request.setParameter(RESOURCE, "EWRGEWRG");

		Map<String, Object> model = controller.referenceData(request, bean, error);
		assertEquals(RegainTokenResource.OPTIONS, model.get("resource"));
	}

	@Test
	public void testWhenOnSubmit_GivenNullOrEmptyEmail_ThenReturnError() throws Exception {
		final ResetPasswordBean bean = new ResetPasswordBean();
		request.setParameter(RESOURCE, "USERID");

		controller.onSubmit(request, response, bean, errors);

		bean.setEmail(null);
		assertFalse(errors.getFieldErrors().isEmpty());
		assertEquals("Required", errors.getFieldErrors().get(0).getDefaultMessage());

		bean.setEmail("");
		assertFalse(errors.getFieldErrors().isEmpty());
		assertEquals("Required", errors.getFieldErrors().get(0).getDefaultMessage());
	}
	
	@Test
	public void testWhenOnSubmit_NoMatch_EmailConfirm_ThenReturnError() throws Exception {
		final ResetPasswordBean bean = new ResetPasswordBean();
		request.setParameter(RESOURCE, "USERID");
		
		bean.setEmail("jesica@gmail.com");
		bean.setEmailconfirm("juan@gmail.com");
		
		controller.onSubmit(request, response, bean, errors);
        
		assertEquals("Invalid email address", errors.getFieldErrors().get(0).getDefaultMessage());
	}
	
	@Test
	public void testWhenOnSubmit_InvalidEmail_EmailConfirm_ThenReturnError() throws Exception {
		final ResetPasswordBean bean = new ResetPasswordBean();
		bean.setEmail("platanito");
		bean.setEmailconfirm("platanito");
		HashMap<Object, Object> model = new HashMap<>();
		String view = "regainaccessEmailSent";

		request.setParameter(RESOURCE, "USERID");
		request.setParameter("language", "en");

		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(any()))
				.thenReturn(new MultipleCustomerProfileResponse(profiles));

		controller.onSubmit(request, response, bean, errors);

		PowerMockito.verifyPrivate(controller, times(1))
				.invoke("showForm", request, errors, view, model);

		assertNotNull(errors.getFieldError("email"));
	}

	
	@Test
	public void testWhenOnSubmit_Match_EmailConfirm_ThenReturnError() throws Exception {
		final ResetPasswordBean bean = new ResetPasswordBean();
		request.setParameter(RESOURCE, "USERID");
		request.setPathInfo("/regainaccess");
		bean.setEmail("jesica@gmail.com");
		bean.setEmailconfirm("jesica@gmail.com");
		
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(new MultipleCustomerProfileResponse());
		controller.onSubmit(request, response, bean, errors);
        
		assertTrue(errors.getFieldErrors().isEmpty());
	}
	
	@Test
	public void testWhenOnSubmit_GivenValidEmail_ThenReturnSuccess() throws Exception {
		final ResetPasswordBean bean = new ResetPasswordBean();
		bean.setEmail("some@email.com");
		HashMap<Object, Object> model = new HashMap<>();
		String view = "regainaccessEmailSent";

		request.setParameter(RESOURCE, "USERID");
		request.setParameter("language", "en");

		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(any()))
				.thenReturn(new MultipleCustomerProfileResponse(profiles));

		controller.onSubmit(request, response, bean, errors);

		PowerMockito.verifyPrivate(controller, times(1))
				.invoke("showForm", request, errors, view, model);

		assertNull(errors.getFieldError("email"));
	}

	@Test
	public void testWhenOnSubmit_GivenInvalidEmail_ThenReturnFail() throws Exception {
		final ResetPasswordBean bean = new ResetPasswordBean();
		bean.setEmail("");
		HashMap<Object, Object> model = new HashMap<>();
		String view = "regainaccessEmailSent";

		request.setParameter(RESOURCE, "USERID");
		request.setParameter("language", "en");

		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(any()))
				.thenReturn(new MultipleCustomerProfileResponse(profiles));

		controller.onSubmit(request, response, bean, errors);

		PowerMockito.verifyPrivate(controller, times(1))
				.invoke("showForm", request, errors, view, model);

		assertNotNull(errors.getFieldError("email"));
	}
	
	@Test
	public void whenOnSubmit_GiveEmptyResource_thenErrorRadioButton() throws Exception {
		final ResetPasswordBean bean = new ResetPasswordBean();
		bean.setEmail("jorge.hernandez@evertecinc.com");
		bean.setEmailconfirm("jorge.hernandez@evertecinc.com");
		bean.setResourceRegainAccess("");
		
		request.setParameter(RESOURCE, "OPTIONS");
		request.setParameter("language", "en");
		request.setPathInfo("/regainaccess");
		
		controller = Mockito.spy(controller);
		Mockito.doNothing().when(controller).manageProfileToken(any(), any(), any());
		
		ModelAndView modelview = controller.onSubmit(request, response, bean, errors);
		
		assertEquals(RegainTokenResource.OPTIONS, modelview.getModel().get("resource"));
		assertNotNull(errors.getFieldError("resourceRegainAccess"));
	}
	
	@Test
	public void whenOnSubmit_GivePasswordResource_thenAsignPasswordResourceType() throws Exception {
		final ResetPasswordBean bean = new ResetPasswordBean();
		bean.setEmail("jorge.hernandez@evertecinc.com");
		bean.setEmailconfirm("jorge.hernandez@evertecinc.com");
		bean.setResourceRegainAccess("password");
		
		request.setParameter(RESOURCE, "OPTIONS");
		request.setParameter("language", "en");
		request.setPathInfo("/regainaccess");
		
		controller = Mockito.spy(controller);
		Mockito.doNothing().when(controller).manageProfileToken(any(), any(), any());
		
		ModelAndView modelview = controller.onSubmit(request, response, bean, errors);
		
		assertEquals(RegainTokenResource.OPTIONS, modelview.getModel().get("resource"));
		assertEquals("regainaccessEmailSent", modelview.getViewName());
		verify(controller, times(1)).manageProfileToken(any(), any(), any());
		
	}

	public void whenfoundInEmailBlackList_GiveValidEmail_thenReturnFalse() throws Exception {
		controller.setStrEmailBlackList("fail@test.com");
		boolean result = Whitebox.invokeMethod(controller, "foundInEmailBlackList", "test@test.com");
		assertFalse(result);

	}
	
	@Test
	public void whenfoundInEmailBlackList_GiveinValidEmail_thenReturnTrue() throws Exception {
		controller.setStrEmailBlackList("fail@test.com");
		boolean result = Whitebox.invokeMethod(controller, "foundInEmailBlackList", "fail@test.com");
		assertTrue(result);
	}
	
	@Test
	public void whenfoundInEmailBlackList_GiveEmptyBlacklist_thenReturnFalse() throws Exception {
		controller.setStrEmailBlackList("");
		boolean result = Whitebox.invokeMethod(controller, "foundInEmailBlackList", "test@test.com");
		assertFalse(result);
	}
	
	@Test
	public void whenisValidEmailInputs_GiveinValidEmail_thenReturnTrue() throws Exception {
		controller.setStrEmailBlackList("fail@test.com");
		boolean result = Whitebox.invokeMethod(controller, "isValidEmailInputs", "test@test.com", "test@test.com", request, errors);
		assertTrue(result);
	}
	
	@Test
	public void whenisValidEmailInputs_GiveinInvalidEmail_thenReturnFalase() throws Exception {
		controller.setStrEmailBlackList("fail@test.com");
		boolean result = Whitebox.invokeMethod(controller, "isValidEmailInputs", "fail@test.com", "fail@test.com", request, errors);
		assertFalse(result);
	}
	
	@Test
	public void whenmanageProfileToken_givenSchemaHttp_thenReturnUrlWithHttp () {
		List<CustomerProfile> customerProfiles = new ArrayList(){{
			add(mCustomerProfile);
		}};

		controller.setStrSchemaRegainAccessMail("http");
		
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(any())).thenReturn(mMultipleCustomerProfileResponse);
		when(mMultipleCustomerProfileResponse.getStatus()).thenReturn(CustomerProfileStatus.FOUND);
		when(mMultipleCustomerProfileResponse.getCustomerProfiles()).thenReturn(customerProfiles);
		when(customerService.saveRegainAccessTokenAndSendEmail(urlCapture.capture(), any(), any(), any())).thenReturn(true);
		
		
		controller.manageProfileToken(request, "email", RegainTokenResource.USERID);
		
		String url = urlCapture.getValue();
		assertTrue(url.contains("http"));
		assertFalse(url.contains("https"));
	}
	
	@Test
	public void whenmanageProfileToken_givenSchemaHttps_thenReturnUrlWithHttps () {
		List<CustomerProfile> customerProfiles = new ArrayList(){{
			add(mCustomerProfile);
		}};

		controller.setStrSchemaRegainAccessMail("https");
		
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(any())).thenReturn(mMultipleCustomerProfileResponse);
		when(mMultipleCustomerProfileResponse.getStatus()).thenReturn(CustomerProfileStatus.FOUND);
		when(mMultipleCustomerProfileResponse.getCustomerProfiles()).thenReturn(customerProfiles);
		when(customerService.saveRegainAccessTokenAndSendEmail(urlCapture.capture(), any(), any(), any())).thenReturn(true);
		
		
		controller.manageProfileToken(request, "email", RegainTokenResource.USERID);
		
		String url = urlCapture.getValue();
		assertTrue(url.contains("https"));
	}
	
	@Mock
	private RegainAccessToken mRegainAccessToken;
	
	@Test
	public void whenGenerateEndpoint_givenBlockedResourceType_thenResultPasswordBlockedMobileParam () throws Exception {
		String resultUrl = Whitebox.invokeMethod(controller, "generateEndpoint" , "baseUrl", mRegainAccessToken, "strLang", RegainTokenResource.BLOCKED);
		assertTrue(resultUrl.contains("passwordBlockedMobile=true"));
	}
	
	@Test
	public void whenGenerateEndpoint_givenUserResourceType_thenNotResultPasswordBlockedMobileParam () throws Exception {
		String resultUrl = Whitebox.invokeMethod(controller, "generateEndpoint" , "baseUrl", mRegainAccessToken, "strLang", RegainTokenResource.USERID);
		assertFalse(resultUrl.contains("passwordBlockedMobile=true"));
	}
	
	@Test
	public void whenGenerateEndpoint_givenPassResourceType_thenNotResultPasswordBlockedMobileParam () throws Exception {
		String resultUrl = Whitebox.invokeMethod(controller, "generateEndpoint" , "baseUrl", mRegainAccessToken, "strLang", RegainTokenResource.PSW);
		assertFalse(resultUrl.contains("passwordBlockedMobile=true"));
	}
	
	@Test
	public void whenGenerateEndpoint_givenOptionsResourceType_thenNotResultPasswordBlockedMobileParam () throws Exception {
		String resultUrl = Whitebox.invokeMethod(controller, "generateEndpoint" , "baseUrl", mRegainAccessToken, "strLang", RegainTokenResource.OPTIONS);
		assertFalse(resultUrl.contains("passwordBlockedMobile=true"));
	}
	
	@Test
	public void testWhenOnSubmit_Input_Email_with_WhiteSpace_ThenEmailSpaceAreRemoved() throws Exception {
		final ResetPasswordBean bean = new ResetPasswordBean();
		request.setParameter(RESOURCE, "USERID");
		request.setPathInfo("/regainaccess");
		bean.setEmail("bolivar.diaz@everteinc.com    ");
		bean.setEmailconfirm("bolivar.diaz@everteinc.com    ");
		
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(new MultipleCustomerProfileResponse());
		controller.onSubmit(request, response, bean, errors);
        
		String emailTrim = bean.getEmail().trim();
		verify(regainAccessServiceModule, times(1)).isEmailBlocked(emailTrim);
	}
	
}
