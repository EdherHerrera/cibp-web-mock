package evertec.cibp.web.form;

import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.RegainAccessToken;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.MultipleCustomerProfileResponse;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.QuestionChallengeBean;
import evertec.cibp.web.form.bean.ResetPasswordBean;
import evertec.cibp.web.services.modules.RegainAccessServiceModule;
import evertec.cibp.web.session.CaptchaSession;
import evertec.cibp.web.session.PasswordResetSession;
import evertec.cibp.web.session.UserSession;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StaticMessageSource.class, WebUtils.class, CaptchaSession.class })
public class ResetPasswordUsernameUT extends AbstractModelAndViewTests {

	private static final String CAPTCHA_SESSION = "captchaSession";//
	
	private MockHttpServletRequest request;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile profile;

	@Mock
	private List<CustomerProfile> profiles;

	@Mock
	private CustomerProfileService customerProfileService;

	@Mock
	private UserSession userSession;
	
	@Mock
	private PasswordResetSession passwordResetSession;

	@InjectMocks
	private ResetPasswordForm controller;

	@Mock
	private BindException errors;

	@Mock
	private QuestionChallengeBean command;
	
	@Mock 
	private CaptchaSession mCaptchaSession;
	
	@Mock
	private RegainAccessServiceModule mRegainAccessServiceModule;
	
	private static final String PASS_RESET_SESSION = "passwordResetSession";

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		profiles = new LinkedList<CustomerProfile>();
		request = new MockHttpServletRequest();
		errors = new BindException(command, "QuestionChallengeBean");

		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CaptchaSession.class);
		
		profile = new CustomerProfile();
		
		passwordResetSession = new PasswordResetSession();
		controller.setRegainAccessServiceModule(mRegainAccessServiceModule);
		Mockito.doNothing().when(mRegainAccessServiceModule).logRegainAccessEvent(any(), any(), any(), any());
		when(WebUtils.getSessionAttribute(request, PASS_RESET_SESSION)).thenReturn(passwordResetSession);
	}

	@Test
	public void testWhenOnBindAndValidate_GivenEmptyTaxId1_ThenReturnErrorTrue() throws Exception {
		ResetPasswordBean bean = new ResetPasswordBean();
		BindException error = new BindException(bean, "ResetPasswordBean");

		taxIdValidation(bean, error, "", "11", "1234");
	}

	@Test
	public void testWhenOnBindAndValidate_GivenEmptyTaxId2_ThenReturnErrorTrue() throws Exception {
		ResetPasswordBean bean = new ResetPasswordBean();
		BindException error = new BindException(bean, "ResetPasswordBean");

		taxIdValidation(bean, error, "324", "", "1234");
	}

	@Test
	public void testWhenOnBindAndValidate_GivenEmptyTaxId3_ThenReturnErrorTrue() throws Exception {
		ResetPasswordBean bean = new ResetPasswordBean();
		BindException error = new BindException(bean, "ResetPasswordBean");

		taxIdValidation(bean, error, "324", "11", "");
	}

	@Test
	public void testWhenOnBindAndValidate_GivenEmptyDobDay_ThenReturnErrorTrue() throws Exception {
		ResetPasswordBean bean = new ResetPasswordBean();
		BindException error = new BindException(bean, "ResetPasswordBean");

		dobValidation(bean, error, 0, 10, 1990);
	}

	@Test
	public void testWhenOnBindAndValidate_GivenEmptyDobYear_ThenReturnErrorTrue() throws Exception {
		ResetPasswordBean bean = new ResetPasswordBean();
		BindException error = new BindException(bean, "ResetPasswordBean");

		dobValidation(bean, error, 10, 10, 0000);
	}

	/**
	 * @throws Exception
	 */
	@Test
	public void testWhenOnBindAndValidatePage0_GivenValidParameters_ThenReturnTrue() throws Exception {
		ResetPasswordBean bean = createBean("583", "51", "3734", 03, 05, 1967);
		BindException error = new BindException(bean, "ResetPasswordBean");

		String taxId = bean.getTaxId1() + bean.getTaxId2() + bean.getTaxId3();

		profile.setUsername("mayra3");
		profile.setTaxId(taxId);
		profile.setStatus(CustomerStatus.ACTIVE);
		
		addBirthdateToProfile(bean);
		
		passwordResetSession.setInteractionEvent(new CustomerInteractionEvent("", "", "", "", ""));
		when(customerService.getCustomerProfiles(taxId)).thenReturn(new MultipleCustomerProfileResponse(profiles));
		when(WebUtils.getSessionAttribute(request, CAPTCHA_SESSION)).thenReturn(mCaptchaSession);
		when((CaptchaSession.class.cast(any()))).thenReturn(mCaptchaSession);

		controller.onBindAndValidate(request, bean, error, 0);
		assertFalse(error.getFieldErrors().isEmpty());

	}
	
	/**
	 * @throws Exception
	 */
	@Test
	public void testWhenReferenceDataPage0_GivenValidParameters_ThenReturnTrue() throws Exception {
		ResetPasswordBean bean = createBean("583", "51", "3734", 03, 05, 1967);
		BindException error = new BindException(bean, "ResetPasswordBean");

		String taxId = bean.getTaxId1() + bean.getTaxId2() + bean.getTaxId3();

		profile.setUsername("mayra3");
		profile.setTaxId(taxId);
		profile.setStatus(CustomerStatus.ACTIVE);
		
		addBirthdateToProfile(bean);
		
		passwordResetSession.setInteractionEvent(new CustomerInteractionEvent("", "", "", "", ""));
		when(customerService.getCustomerProfiles(taxId)).thenReturn(new MultipleCustomerProfileResponse(profiles));
		when(WebUtils.getSessionAttribute(request, CAPTCHA_SESSION)).thenReturn(mCaptchaSession);
		when((CaptchaSession.class.cast(any()))).thenReturn(mCaptchaSession);
		request.setPathInfo("");

		controller.referenceData(request, bean, error, 0);
		assertTrue(error.getFieldErrors().isEmpty());

	}
	
	@Mock
	private RegainAccessToken mRegainAccessToken;
	@Mock
	private CustomerProfileService mCustomerProfileService;
	@Mock
	private MultipleCustomerProfileResponse mMultipleCustomerProfileResponse;
	
	/**
	 * @throws Exception
	 */
	@Test
	public void testWhenOnBindAndValidatePage1_GivenGoodParameters_ThenReturnTrue() throws Exception {
		ResetPasswordBean bean = createBean("583", "51", "3734", 03, 05, 1967);
		BindException error = new BindException(bean, "ResetPasswordBean");

		addAccountValuesToBean(bean);

		String taxId = bean.getTaxId1() + bean.getTaxId2() + bean.getTaxId3();

		profile.setUsername("mayra3");
		profile.setTaxId(taxId);
		profile.setStatus(CustomerStatus.ACTIVE);
		profile.setEnrollmentAccountNumber("132384485");
		
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy", RequestContextUtils.getLocale(request));
		
		
		Date date = new GregorianCalendar(1967, Calendar.MAY, 3).getTime();
		profile.setBirthDate(date);

		profiles.add(profile);
		
		passwordResetSession.setInteractionEvent(new CustomerInteractionEvent("", "", "", "", ""));
		when(customerService.getCustomerProfiles(taxId)).thenReturn(new MultipleCustomerProfileResponse(profiles));
		when(WebUtils.getSessionAttribute(request, CAPTCHA_SESSION)).thenReturn(mCaptchaSession);
		when((CaptchaSession.class.cast(any()))).thenReturn(mCaptchaSession);
		when(mCaptchaSession.isCaptchaValid(any())).thenReturn(true);
		
		when(customerService.getRegainAccessToken(any())).thenReturn(mRegainAccessToken);
		when(mRegainAccessToken.getEmail()).thenReturn("");
		when(customerService.getCustomerProfileService()).thenReturn(mCustomerProfileService);
		when(mCustomerProfileService.getCustomerProfileByExactFilter(any())).thenReturn(mMultipleCustomerProfileResponse);
		HashMap<Object, CustomerProfile> hashMap = Mockito.mock(HashMap.class);
		PowerMockito.whenNew(HashMap.class).withNoArguments().thenReturn(hashMap);
		when(hashMap.size()).thenReturn(1);

		controller.onBindAndValidate(this.request, bean, error, 0);
		assertFalse(error.getFieldErrors().isEmpty());

	}
	
	/**
	 * @param bean
	 * @throws ParseException
	 */
	private void addBirthdateToProfile(ResetPasswordBean bean) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", RequestContextUtils.getLocale(request));
		String birthdate = String.valueOf(bean.getDobday()) +"-"+ String.valueOf(bean.getDobmonth()) +"-"+
				String.valueOf(bean.getDobyear());
		
		Date date = sdf.parse(birthdate);
		profile.setBirthDate(date);

		profiles.add(profile);
	}

	/**
	 * @param bean
	 * @param error
	 * @param taxId1
	 * @param taxId2
	 * @param taxId3
	 * @throws Exception
	 */
	private void taxIdValidation(ResetPasswordBean bean, BindException error, String taxId1, String taxId2,
			String taxId3) throws Exception {
		bean.setTaxId1(taxId1);
		bean.setTaxId2(taxId2);
		bean.setTaxId3(taxId3);
		bean.setDobday(11);
		bean.setDobmonth(10);
		bean.setDobyear(1990);

		onBindAndValidate(bean, error);
	}

	/**
	 * @param bean
	 * @param error
	 * @param dobDay
	 * @param dobMonth
	 * @param dobYear
	 * @throws Exception
	 */
	private void dobValidation(ResetPasswordBean bean, BindException error, int dobDay, int dobMonth, int dobYear)
			throws Exception {
		bean.setTaxId1("542");
		bean.setTaxId2("22");
		bean.setTaxId3("1990");
		bean.setDobday(dobDay);
		bean.setDobmonth(dobMonth);
		bean.setDobyear(dobYear);

		onBindAndValidate(bean, error);
	}

	/**
	 * @param taxId1
	 * @param taxId2
	 * @param taxId3
	 * @param dobDay
	 * @param dobMonth
	 * @param dobYear
	 */
	private ResetPasswordBean createBean(String taxId1, String taxId2, String taxId3, int dobDay, int dobMonth, int dobYear) {
		ResetPasswordBean bean = new ResetPasswordBean();
		
		bean.setTaxId1(taxId1);
		bean.setTaxId2(taxId2);
		bean.setTaxId3(taxId3);
		bean.setDobday(dobDay);
		bean.setDobmonth(dobMonth);
		bean.setDobyear(dobYear);
		return bean;
	}
	
	/**
	 * @param bean
	 */
	private ResetPasswordBean addAccountValuesToBean(ResetPasswordBean bean) {
		bean.setAccountnumber("132384485");
		bean.setPin("1234");
		bean.setCvv("");
		bean.setAccountInfoType("IDA");
		return bean;
	}

	/**
	 * @param bean
	 * @param error
	 * @throws Exception
	 */
	private void onBindAndValidate(ResetPasswordBean bean, BindException error) throws Exception {
		when(WebUtils.getSessionAttribute(request, CAPTCHA_SESSION)).thenReturn(mCaptchaSession);
		when((CaptchaSession.class.cast(any()))).thenReturn(mCaptchaSession);
		
		controller.onBindAndValidate(this.request, bean, error, 0);
		assertFalse(error.getFieldErrors().isEmpty());
		FieldError field = (FieldError) error.getFieldErrors().get(0);
		assertTrue(field.getDefaultMessage().equals("Required"));
	}

}
