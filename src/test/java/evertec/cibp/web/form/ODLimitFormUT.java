package evertec.cibp.web.form;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.profile.ProfileType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.VaultAccountsFacade;
import evertec.cibp.web.form.bean.ODLimitBean;
import evertec.cibp.web.request.VaultAccountsRequest;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StaticMessageSource.class, VaultAccountsFacade.class, ServletRequestUtils.class, WebUtils.class, Utils.class,
                ODLimitForm.class})
public class ODLimitFormUT extends AbstractModelAndViewTests {

	@Mock
    private HttpServletRequest request;

	@Mock
    private HttpServletResponse response;

    private static final String USER_SESSION = "userSession";

    @Mock
    private CustomerServices customerService;

    @Mock
    private VaultAccountsFacade vaultAccountsFacade;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private ProfileType profileType;

    @Mock
    private UserSession userSession;

    @Mock
    private CustomerProfile profile;

    @Mock
    private Map<String, Object> map;

    @Mock
    private BindException errors;

    @Mock
    private ODLimitBean command;

    @Spy
    @InjectMocks
    private final ODLimitForm controller = new ODLimitForm();

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(ServletRequestUtils.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(VaultAccountsFacade.class);
        when(VaultAccountsFacade.getInstance()).thenReturn(vaultAccountsFacade);

        errors = new BindException(command, "ODLimitBean");
        when(WebUtils.getSessionAttribute(request, USER_SESSION)).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(profile);

        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
    }

    @Test
    public void whenReferenceData_GivenAccountsThenMapContainList () throws Exception {

        List<TVFrontendAccount> tvaclist = new ArrayList<>();
        TVFrontendAccount cuenta = mock(TVFrontendAccount.class);

        VaultAccountsRequest vaultRequest = mock(VaultAccountsRequest.class);
        cuenta.setAccountNumberSuffix("RES");

        tvaclist.add(cuenta);

        when(userSession.getAccounts()).thenReturn(tvaclist);

        when(cuenta.getAccountProductId()).thenReturn("015");

        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getCustomerProfile().getProfileType()).thenReturn(profileType);
        when(userSession.getCustomerProfile().getProfileType().getId()).thenReturn(4);
        when(vaultAccountsFacade.getAccounts(vaultRequest)).thenReturn(tvaclist);

        map = controller.referenceData(request, command, errors);
        assertTrue(map.containsKey("accountList"));
    }

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-22-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(request.getParameter(anyString())).thenReturn(null);
		when(userSession.getAccounts()).thenReturn(new ArrayList<>());
		when(Utils.getRSALanguage(any())).thenReturn("en");
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(ProfileType.Type.RETAIL.getId());

		Map<String, Object> result = controller.referenceData(request, command, errors);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when OnSubmit
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-22-2020
	 * @exception Exception error
	 */
	@Test
	public void whenOnSubmit_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("cookieAnalyticEnabled", true);

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(request.getParameter(anyString())).thenReturn(null);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(Utils.getRSALanguage(any())).thenReturn("en");
		when(command.getAccountList()).thenReturn(new ArrayList<>());
		PowerMockito.doReturn(mav).when((SimpleFormController)controller, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any());

		ModelAndView mavResult = controller.onSubmit(request, response, command, errors);
		assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}
}
