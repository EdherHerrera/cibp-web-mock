package evertec.cibp.web.form;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.validation.Errors;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ATHMovilBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CustomerFacade.class, StaticMessageSource.class, ATHMovilForm.class, Utils.class})
public class ATHMovilFormIT {
	
	@Mock
	private UserSession userSession;

	@Mock
	private CustomerFacade customerFacade;
	
	@Mock
	private CustomerEntitlement customerEntitlement;

	@Mock
	private HttpServletRequest requestValue;

	@Mock
	private ATHMovilBean athMovilFormBean;

	@Mock
	private Errors errors;

	@Spy
	private final ATHMovilForm aTHMovilForm = new ATHMovilForm();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);

		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
	}
	
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(Utils.getRSALanguage(any(HttpServletRequest.class))).thenReturn("es");
		when(athMovilFormBean.getDailyTransferLimit()).thenReturn(null);

	}

}
