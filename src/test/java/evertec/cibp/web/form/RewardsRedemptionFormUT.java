package evertec.cibp.web.form;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.evertec.cibp.api.client.model.Error;
import com.evertec.cibp.api.client.model.MicroServiceResponse;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TsysLoyaltyRewardsInfo;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.web.facade.CreditCardFacade;
import evertec.cibp.web.form.bean.RewardsRedemptionBean;
import evertec.cibp.web.response.RewardsCurrencyAmount;
import evertec.cibp.web.response.TsysLoyaltyRewardsRedemptionResponse;
import evertec.cibp.web.response.TsysLoyaltyRewardsRedemptionResponse.CurrencyAmount;
import evertec.cibp.web.response.TsysLoyaltyRewardsRedemptionResponse.CurrencyAmount.CurrencyEnum;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.creditcards.exception.TsysLoyaltyRewardsException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Utils.class, WebUtils.class, CreditCardFacade.class })
public class RewardsRedemptionFormUT {

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private Map<Object, Object> map;

	@Mock
	private RewardsRedemptionBean rewardsRedemptionBean;

	@Mock
	private UserSession userSession;

	@Mock
	private TVFrontendAccount ccaAccount;

	@Mock
	private TsysLoyaltyRewardsInfo tsysLoyaltyRewardsInfo;

	@Mock
	private Errors errors;

	@Mock
	private MessageSource messageSource;

	@Mock
	private CreditCardFacade creditCardFacade;

	@Mock
	private TsysLoyaltyRewardsRedemptionResponse loyaltyRewardsRedemptionResponse;

	@Mock
	private Error error;

	@Mock
	private MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> tsysLoyaltyRewardsRedemptionResponse;

	@Mock
	private List<com.evertec.cibp.api.client.model.Error> errorList;

	@InjectMocks
	@Spy
	private RewardsRedemptionForm redemptionController = new RewardsRedemptionForm();

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	private BindException bindExceptionError;


	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		request = new MockHttpServletRequest();
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CreditCardFacade.class);
	}

	@Test
	public void whenReferenceData_GivenExcludeAccuntIsTrue_ThenReturnTrue() throws Exception {
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(Utils.getAccountById(any(), any())).thenReturn(ccaAccount);
		when(ccaAccount.getTsysLoyaltyRewardsInfo()).thenReturn(tsysLoyaltyRewardsInfo);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		when(messageSource.getMessage(any(), any(), any(), any())).thenReturn("The amount to be inserted must range between {0} and {1}.");


		TVFrontendAccount depositAccount1 = mock(TVFrontendAccount.class);
		when(depositAccount1.getSubtype()).thenReturn("CCA");

		TVFrontendAccount depositAccount2 = mock(TVFrontendAccount.class);
		when(depositAccount2.getSubtype()).thenReturn("CCA");

		List<TVFrontendAccount> frontendAccounts = new ArrayList<TVFrontendAccount>();
		frontendAccounts.add(depositAccount1);
		frontendAccounts.add(depositAccount2);

		when(Utils.excludeAccounts(depositAccount1, userSession)).thenReturn(true);

		when(userSession.getAccounts()).thenReturn(frontendAccounts);

		Map<String, Object> model = redemptionController.referenceData(request, rewardsRedemptionBean, errors, 0);
		assertTrue(Utils.excludeAccounts(depositAccount1, userSession));
		assertTrue(model.containsKey("redemptionBean"));		
	}

	@Test
	public void whenReferenceData_GivenExcludeAccuntIsFalse_ThenReturnFalse() throws Exception {
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(Utils.getAccountById(any(), any())).thenReturn(ccaAccount);
		when(ccaAccount.getTsysLoyaltyRewardsInfo()).thenReturn(tsysLoyaltyRewardsInfo);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		when(messageSource.getMessage(any(), any(), any(), any())).thenReturn("The amount to be inserted must range between {0} and {1}.");


		TVFrontendAccount depositAccount1 = mock(TVFrontendAccount.class);
		when(depositAccount1.getSubtype()).thenReturn("IDA");

		TVFrontendAccount depositAccount2 = mock(TVFrontendAccount.class);
		when(depositAccount2.getSubtype()).thenReturn("IDA");

		List<TVFrontendAccount> frontendAccounts = new ArrayList<TVFrontendAccount>();
		frontendAccounts.add(depositAccount1);
		frontendAccounts.add(depositAccount2);

		when(Utils.excludeAccounts(depositAccount1, userSession)).thenReturn(false);
		when(userSession.getAccounts()).thenReturn(frontendAccounts);

		Map<String, Object> model = redemptionController.referenceData(request, rewardsRedemptionBean, errors, 0);
		assertFalse(Utils.excludeAccounts(depositAccount1, userSession));
		assertTrue(model.containsKey("redemptionBean"));
	}

	@Test
	public void whenReferenceData_GivenSubTypeInvalid_ThenReturnFalse() throws Exception {

		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(Utils.getAccountById(any(), any())).thenReturn(ccaAccount);
		when(ccaAccount.getTsysLoyaltyRewardsInfo()).thenReturn(tsysLoyaltyRewardsInfo);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		when(messageSource.getMessage(any(), any(), any(), any())).thenReturn("The amount to be inserted must range between {0} and {1}.");

		TVFrontendAccount depositAccount1 = mock(TVFrontendAccount.class);
		when(depositAccount1.getSubtype()).thenReturn("IDA");

		TVFrontendAccount depositAccount2 = mock(TVFrontendAccount.class);
		when(depositAccount2.getSubtype()).thenReturn("IDA");

		List<TVFrontendAccount> frontendAccounts = new ArrayList<TVFrontendAccount>();
		frontendAccounts.add(depositAccount1);
		frontendAccounts.add(depositAccount2);

		when(Utils.excludeAccounts(depositAccount1, userSession)).thenReturn(true);
		when(ccaAccount.getSubtype()).thenReturn("CCA");
		when(userSession.getAccounts()).thenReturn(frontendAccounts);

		Map<String, Object> model = redemptionController.referenceData(request, rewardsRedemptionBean, errors, 0);
		assertTrue(model.containsKey("redemptionBean"));
	}

	@Test
	public void whenReferenceData_GivenAccountListNull_ThenReturnFalse() throws Exception {
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(Utils.getAccountById(any(), any())).thenReturn(ccaAccount);
		when(ccaAccount.getTsysLoyaltyRewardsInfo()).thenReturn(tsysLoyaltyRewardsInfo);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		when(messageSource.getMessage(any(), any(), any(), any())).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(userSession.getAccounts()).thenReturn(null);

		Map<String, Object> model = redemptionController.referenceData(request, rewardsRedemptionBean, errors, 0);
		assertTrue(model.containsKey("redemptionBean"));
	}

	@Test
	public void whenReferenceData_GivenPageInvalid_ThenReturnFalse() throws Exception {
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(Utils.getAccountById(any(), any())).thenReturn(ccaAccount);
		when(ccaAccount.getTsysLoyaltyRewardsInfo()).thenReturn(tsysLoyaltyRewardsInfo);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		when(messageSource.getMessage(any(), any(), any(), any())).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(userSession.getAccounts()).thenReturn(null);

		Map<String, Object> model = redemptionController.referenceData(request, rewardsRedemptionBean, errors, 3);
		assertTrue(model.containsKey("redemptionBean"));
	}

	/**
     * When PerformCashRewardsRedemption
     * Given Successful Redemption for Statement Credit whit ResponseData
     * @throws TsysLoyaltyRewardsException
     */
	@Test
	public void  whenPerformCashRewardsRedemption_GivenSuccessfulRedemptionStatementCredit_ThenReturnResponseData()  throws TsysLoyaltyRewardsException{

		when(ccaAccount.getAccountNumber()).thenReturn("1234567890123456");
		when(ccaAccount.getBankId()).thenReturn("021502011");
		redemptionController.setRoutingNumberJson("{\"routingNumber\":\"021502011\"}");

		when(rewardsRedemptionBean.getRedemptionType()).thenReturn("STATEMENT_CREDIT");
		when(rewardsRedemptionBean.getDirectDepositAccount()).thenReturn(ccaAccount);
		when(rewardsRedemptionBean.getRedeemAmount()).thenReturn(10.00);

		TVFrontendAccount depositAccount1 = mock(TVFrontendAccount.class);
		when(depositAccount1.getAccountNumber()).thenReturn("1234567890123456");
		rewardsRedemptionBean.setCashRewardsAccount(depositAccount1);
		when(rewardsRedemptionBean.getCashRewardsAccount()).thenReturn(depositAccount1);
		when(WebUtils.getSessionAttribute(any(), anyString())).thenReturn(userSession);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(creditCardFacade.tsysLoyaltyRewardsRedemption(any(),any(), any(), any())).thenReturn(tsysLoyaltyRewardsRedemptionResponse);
		when(tsysLoyaltyRewardsRedemptionResponse.getData()).thenReturn(new ArrayList<TsysLoyaltyRewardsRedemptionResponse>() {{
			add(loyaltyRewardsRedemptionResponse);
		}});
		redemptionController.performCashRewardsRedemption(rewardsRedemptionBean, userSession);
		assertNotNull(tsysLoyaltyRewardsRedemptionResponse.getData());
	}

	/**
     * When PerformCashRewardsRedemption
     * Given Redemption for Statement Credit whit ResponseError
     * @throws TsysLoyaltyRewardsException
     */
	@Test
	public void  whenPerformCashRewardsRedemption_GivenRedemptionStatementCredit_ThenReturnResponseError() throws TsysLoyaltyRewardsException{

		when(ccaAccount.getAccountNumber()).thenReturn("1234567890123456");
		when(ccaAccount.getBankId()).thenReturn("021502011");
		redemptionController.setRoutingNumberJson("{\"routingNumber\":\"021502011\"}");

		when(rewardsRedemptionBean.getRedemptionType()).thenReturn("STATEMENT_CREDIT");
		when(rewardsRedemptionBean.getDirectDepositAccount()).thenReturn(ccaAccount);
		when(rewardsRedemptionBean.getRedeemAmount()).thenReturn(10.00);

		TVFrontendAccount depositAccount1 = mock(TVFrontendAccount.class);
		when(depositAccount1.getAccountNumber()).thenReturn("1234567890123456");
		rewardsRedemptionBean.setCashRewardsAccount(depositAccount1);
		when(rewardsRedemptionBean.getCashRewardsAccount()).thenReturn(depositAccount1);
		when(Utils.getUserSession(request)).thenReturn(userSession);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(creditCardFacade.tsysLoyaltyRewardsRedemption(any(),any(), any(), any())).thenReturn(tsysLoyaltyRewardsRedemptionResponse);
		when(tsysLoyaltyRewardsRedemptionResponse.getData()).thenReturn(null);		
		when(tsysLoyaltyRewardsRedemptionResponse.getErrors()).thenReturn(new ArrayList<com.evertec.cibp.api.client.model.Error>() {{
			add(error);
		}});		
		redemptionController.performCashRewardsRedemption(rewardsRedemptionBean, userSession);
		assertNotNull(tsysLoyaltyRewardsRedemptionResponse.getErrors());
	}

	/**
	 * When PerformCashRewardsRedemption
	 * Given Redemption for Direct Deposit whit ResponseError
	 * @throws TsysLoyaltyRewardsException
	 */
	@Test
	@Ignore
	public void  whenPerformCashRewardsRedemption_GivenRedemptionDirectDeposit_ThenReturnResponseError() throws TsysLoyaltyRewardsException{
	
		when(ccaAccount.getAccountNumber()).thenReturn("1234567890123456");
		when(ccaAccount.getBankId()).thenReturn("001");
		redemptionController.setRoutingNumberJson("{\"001\" : \"021502011\", \"002\" : \"021606674\", \"004\" : \"021502011\", \"006\" : \"021502011\"}");
		when(rewardsRedemptionBean.getRedemptionType()).thenReturn("DIRECT_DEPOSIT");
		when(rewardsRedemptionBean.getDirectDepositAccount()).thenReturn(ccaAccount);
		when(rewardsRedemptionBean.getRedeemAmount()).thenReturn(10.00);
	
		TVFrontendAccount depositAccount1 = mock(TVFrontendAccount.class);
		when(depositAccount1.getAccountNumber()).thenReturn("1234567890123456");
		rewardsRedemptionBean.setCashRewardsAccount(depositAccount1);
		when(rewardsRedemptionBean.getCashRewardsAccount()).thenReturn(depositAccount1);
		when(WebUtils.getSessionAttribute(any(), anyString())).thenReturn(userSession);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(creditCardFacade.tsysLoyaltyRewardsRedemption(any(),any(), any(), any())).thenReturn(tsysLoyaltyRewardsRedemptionResponse);
		when(tsysLoyaltyRewardsRedemptionResponse.getData()).thenReturn(null);
		when(tsysLoyaltyRewardsRedemptionResponse.getErrors()).thenReturn(new ArrayList<com.evertec.cibp.api.client.model.Error>() {{
			add(error);
		}});		
		redemptionController.performCashRewardsRedemption(rewardsRedemptionBean, userSession);
		assertNotNull(tsysLoyaltyRewardsRedemptionResponse.getErrors());
	}	

	/**
	 * When PerformCashRewardsRedemption
	 * Given Successful Redemption for Direct Deposit with ResponseData
	 * @throws TsysLoyaltyRewardsException
	 */
	@Test
	@Ignore
	public void  whenPerformCashRewardsRedemption_GivenSuccessfulRedemptionDirectDeposit_ThenReturnResponseData() throws TsysLoyaltyRewardsException{
	
		when(ccaAccount.getAccountNumber()).thenReturn("1234567890123456");
		when(ccaAccount.getBankId()).thenReturn("001");
		redemptionController.setRoutingNumberJson("{\"001\" : \"021502011\", \"002\" : \"021606674\", \"004\" : \"021502011\", \"006\" : \"021502011\"}");
		when(rewardsRedemptionBean.getRedemptionType()).thenReturn("DIRECT_DEPOSIT");
		when(rewardsRedemptionBean.getDirectDepositAccount()).thenReturn(ccaAccount);
		when(rewardsRedemptionBean.getRedeemAmount()).thenReturn(10.00);
	
		TVFrontendAccount depositAccount1 = mock(TVFrontendAccount.class);
		when(depositAccount1.getAccountNumber()).thenReturn("1234567890123456");
		rewardsRedemptionBean.setCashRewardsAccount(depositAccount1);
		when(rewardsRedemptionBean.getCashRewardsAccount()).thenReturn(depositAccount1);
		when(WebUtils.getSessionAttribute(any(), anyString())).thenReturn(userSession);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(creditCardFacade.tsysLoyaltyRewardsRedemption(any(),any(), any(), any())).thenReturn(tsysLoyaltyRewardsRedemptionResponse);
		when(tsysLoyaltyRewardsRedemptionResponse.getErrors()).thenReturn(new ArrayList<com.evertec.cibp.api.client.model.Error>() {{
			add(error);
		}});		
		redemptionController.performCashRewardsRedemption(rewardsRedemptionBean, userSession);
		assertNotNull(tsysLoyaltyRewardsRedemptionResponse.getErrors());
	}
		
	@Test
	public void whenProcessFinish_GivenSuccesfulRedemption_ThenConfirmationNumberIsSentToVm() throws Exception {
		
		TsysLoyaltyRewardsRedemptionResponse redemptionResponse = new TsysLoyaltyRewardsRedemptionResponse();
		
		redemptionResponse.setConfirmationNumber("1234567890123456");
		redemptionResponse.setRedemptionDate(new Date().toString());
		
		RewardsCurrencyAmount rewardsBalance = new RewardsCurrencyAmount();
		rewardsBalance.setRewardscurrency("CASH");
		rewardsBalance.setValue("10.00");
		redemptionResponse.setRewardsBalance(rewardsBalance);
		
		CurrencyAmount cashbackAmount = new CurrencyAmount();
		cashbackAmount.setCurrency(CurrencyEnum.USD);
		cashbackAmount.setValue("10.00");
		redemptionResponse.setCashbackAmount(cashbackAmount);
		
		Map<String, Object> identifiers = new HashMap<>();
		identifiers.put("last4", "1234");
		redemptionResponse.setIdentifiers(identifiers);
		
		doReturn(redemptionResponse).when(redemptionController).performCashRewardsRedemption(rewardsRedemptionBean, userSession);
		when(ccaAccount.getTsysLoyaltyRewardsInfo()).thenReturn(tsysLoyaltyRewardsInfo);
		when(rewardsRedemptionBean.getCashRewardsAccount()).thenReturn(ccaAccount);
		when(Utils.getUserSession(request)).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		
		ModelAndView modelAndView = redemptionController.processFinish(request, response, rewardsRedemptionBean, bindExceptionError);
		
		assertTrue(modelAndView.getModel().containsKey("confirmationNumber"));
	}
	
	@Test
	public void whenProcessFinish_GivenErrorRedemption_ThenConfirmationNumberIsNotPresent() throws Exception {
		
		doReturn(null).when(redemptionController).performCashRewardsRedemption(rewardsRedemptionBean, userSession);
		when(ccaAccount.getTsysLoyaltyRewardsInfo()).thenReturn(tsysLoyaltyRewardsInfo);
		when(rewardsRedemptionBean.getCashRewardsAccount()).thenReturn(ccaAccount);
		when(Utils.getUserSession(request)).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		
		ModelAndView modelAndView = redemptionController.processFinish(request, response, rewardsRedemptionBean, bindExceptionError);
		
		assertFalse(modelAndView.getModel().containsKey("confirmationNumber"));
	}
	
	@Test
	public void whenReferenceData_GivenRedemptionConfigurationStep_ThenTermsAndConditionsIsSetToFalse() throws Exception {
		when(Utils.getUserSession(request)).thenReturn(userSession);
		when(Utils.getAccountById(any(), any())).thenReturn(ccaAccount);
		when(ccaAccount.getTsysLoyaltyRewardsInfo()).thenReturn(tsysLoyaltyRewardsInfo);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		when(messageSource.getMessage(any(), any(), any(), any()))
				.thenReturn("The amount to be inserted must range between {0} and {1}.");

		TVFrontendAccount depositAccount1 = mock(TVFrontendAccount.class);
		when(depositAccount1.getSubtype()).thenReturn("CCA");

		TVFrontendAccount depositAccount2 = mock(TVFrontendAccount.class);
		when(depositAccount2.getSubtype()).thenReturn("CCA");

		List<TVFrontendAccount> frontendAccounts = new ArrayList<TVFrontendAccount>();
		frontendAccounts.add(depositAccount1);
		frontendAccounts.add(depositAccount2);

		when(Utils.excludeAccounts(depositAccount1, userSession)).thenReturn(true);

		when(userSession.getAccounts()).thenReturn(frontendAccounts);

		redemptionController.referenceData(request, rewardsRedemptionBean, errors, 0);
		
		verify(rewardsRedemptionBean).setTermsAndCond(false);
	}

}
