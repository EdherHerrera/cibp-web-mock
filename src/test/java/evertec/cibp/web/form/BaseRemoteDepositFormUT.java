package evertec.cibp.web.form;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import evertec.cibp.core.models.configuration.ConfParameters;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.constants.RemoteDepositConstants;
import evertec.cibp.web.request.RDCEnrollmentInfoRequest;
import evertec.cibp.web.session.UserSession;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class})
public class BaseRemoteDepositFormUT {
	
	
	final String rdcConstant = "RDC"; // Constant property filter of registers BD

	@Mock
	private UserSession userSession;
	
	@InjectMocks
	private BaseRemoteDepositForm baseRemoteDepositForm;
	
	@Mock
	private List<ConfParameters> confParameterLimitsList;
	
	@Mock
	private Map<String, String> confParameterLimits;
	
	@Mock
	private Stream<ConfParameters> streamData;
	
	@Mock
	private CustomerServices customerServices;
	
	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(StaticMessageSource.class);
		MockitoAnnotations.initMocks(this);
	}
	
	/////////////////////////////// setRDCLimitFieldsFromDB
	
	/**
	 * when SetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagDisabled_thenReturnZeroValues
	 * Flag DFM_LIMITS_PER_SEGMENT Disabled
	 * Then return variables to 0 or null
	 */
	@Test
	public void whenSetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagDisabled_thenReturnNullAndZeroValues() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(false);
		
		baseRemoteDepositForm.setRDCLimitFieldsFromDB(userSession);
		
		assertNull(baseRemoteDepositForm.mDailyAmount);
		assertEquals(0, baseRemoteDepositForm.mDailyItem);
		assertNull(baseRemoteDepositForm.mMultidayAmount);
		assertEquals(0, baseRemoteDepositForm.mMultidayItem);
	}
	
	/**
	 * when SetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagDisabled_thenReturnZeroValues
	 * Flag DFM_LIMITS_PER_SEGMENT Enabled
	 * confParameterLimits null
	 * Then return variables to 0 or null
	 */
	@Test
	public void whenSetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagEnabledAndConfParameterLimitsNull_thenReturnNullAndZeroValues() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(true);
		when(customerServices.getConfParametersByApp(any())).thenReturn(confParameterLimitsList);
		when(confParameterLimitsList.stream()).thenReturn(streamData);
		when(streamData.collect(any())).thenReturn(null);
		
		baseRemoteDepositForm.setRDCLimitFieldsFromDB(userSession);
		
		assertNull(baseRemoteDepositForm.mDailyAmount);
		assertEquals(0, baseRemoteDepositForm.mDailyItem);
		assertNull(baseRemoteDepositForm.mMultidayAmount);
		assertEquals(0, baseRemoteDepositForm.mMultidayItem);
	}
	
	/**
	 * when SetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagDisabled_thenReturnZeroValues
	 * Flag DFM_LIMITS_PER_SEGMENT Enabled
	 * confParameterLimits empty
	 * Then return variables to 0 or null
	 */
	@Test
	public void whenSetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagEnabledAndConfParameterLimitsEmpty_thenReturnNullAndZeroValues() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(true);
		when(customerServices.getConfParametersByApp(any())).thenReturn(confParameterLimitsList);
		when(confParameterLimitsList.stream()).thenReturn(streamData);
		when(streamData.collect(any())).thenReturn(confParameterLimits);
		when(confParameterLimits.isEmpty()).thenReturn(true);
		
		baseRemoteDepositForm.setRDCLimitFieldsFromDB(userSession);
		
		assertNull(baseRemoteDepositForm.mDailyAmount);
		assertEquals(0, baseRemoteDepositForm.mDailyItem);
		assertNull(baseRemoteDepositForm.mMultidayAmount);
		assertEquals(0, baseRemoteDepositForm.mMultidayItem);
	}
	
	/**
	 * when SetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagDisabled_thenReturnZeroValues
	 * Flag DFM_LIMITS_PER_SEGMENT Enabled
	 * userSession with Commercial User
	 * Then return variables with commercial database values
	 */
	@Test
	public void whenSetRDCLimitFieldsFromDB_GivenToFlagEnabledAndCommercialUser_thenReturnCommercialValues() {
		confParameterLimits = new HashMap<String, String>();
		confParameterLimits.put(RemoteDepositConstants.COMMERCIAL_DAILY_AMOUNT, "1");
		confParameterLimits.put(RemoteDepositConstants.COMMERCIAL_DAILY_ITEM_COUNT, "1");
		confParameterLimits.put(RemoteDepositConstants.COMMERCIAL_MULTIDAY_AMOUNT, "1");
		confParameterLimits.put(RemoteDepositConstants.COMMERCIAL_MULTIDAY_ITEM_COUNT, "1");
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(true);
		when(customerServices.getConfParametersByApp(any())).thenReturn(confParameterLimitsList);
		when(confParameterLimitsList.stream()).thenReturn(streamData);
		when(streamData.collect(any())).thenReturn(confParameterLimits);
		when(userSession.isComercialCustomer()).thenReturn(true);
		
		baseRemoteDepositForm.setRDCLimitFieldsFromDB(userSession);
		
		assertNotNull(baseRemoteDepositForm.mDailyAmount);
		assertNotEquals(0, baseRemoteDepositForm.mDailyItem);
		assertNotNull(baseRemoteDepositForm.mMultidayAmount);
		assertNotEquals(0, baseRemoteDepositForm.mMultidayItem);
	}
	
	/**
	 * when SetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagDisabled_thenReturnZeroValues
	 * Flag DFM_LIMITS_PER_SEGMENT Enabled
	 * userSession with Commercial User
	 * Then return variables 0 with not database values
	 */
	@Test
	public void whenSetRDCLimitFieldsFromDB_GivenToFlagEnabledAndCommercialUserAndNotDatabaseValues_thenReturnZeroValues() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(true);
		when(customerServices.getConfParametersByApp(any())).thenReturn(confParameterLimitsList);
		when(confParameterLimitsList.stream()).thenReturn(streamData);
		when(streamData.collect(any())).thenReturn(confParameterLimits);
		when(confParameterLimits.isEmpty()).thenReturn(false);
		when(confParameterLimits.get(any())).thenReturn(null);
		when(userSession.isComercialCustomer()).thenReturn(true);
		
		baseRemoteDepositForm.setRDCLimitFieldsFromDB(userSession);

		assertEquals(0, baseRemoteDepositForm.mDailyAmount.intValue());
		assertEquals(0, baseRemoteDepositForm.mDailyItem);
		assertEquals(0, baseRemoteDepositForm.mMultidayAmount.intValue());
		assertEquals(0, baseRemoteDepositForm.mMultidayItem);
	}
	
	/**
	 * when SetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagDisabled_thenReturnZeroValues
	 * Flag DFM_LIMITS_PER_SEGMENT Enabled
	 * userSession with Wealth User
	 * Then return variables with wealth database values
	 */
	@Test
	public void whenSetRDCLimitFieldsFromDB_GivenToFlagEnabledAndWealthUser_thenReturnZeroValues() {
		confParameterLimits = new HashMap<String, String>();
		confParameterLimits.put(RemoteDepositConstants.WEALTH_DAILY_AMOUNT, "1");
		confParameterLimits.put(RemoteDepositConstants.WEALTH_DAILY_ITEM_COUNT, "1");
		confParameterLimits.put(RemoteDepositConstants.WEALTH_MULTIDAY_AMOUNT, "1");
		confParameterLimits.put(RemoteDepositConstants.WEALTH_MULTIDAY_ITEM_COUNT, "1");
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(true);
		when(customerServices.getConfParametersByApp(any())).thenReturn(confParameterLimitsList);
		when(confParameterLimitsList.stream()).thenReturn(streamData);
		when(streamData.collect(any())).thenReturn(confParameterLimits);
		when(userSession.isWealth(any(), any())).thenReturn(true);
		
		baseRemoteDepositForm.setRDCLimitFieldsFromDB(userSession);
		assertNotNull(baseRemoteDepositForm.mDailyAmount);
		assertNotEquals(0, baseRemoteDepositForm.mDailyItem);
		assertNotNull(baseRemoteDepositForm.mMultidayAmount);
		assertNotEquals(0, baseRemoteDepositForm.mMultidayItem);
	}
	
	/**
	 * when SetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagDisabled_thenReturnZeroValues
	 * Flag DFM_LIMITS_PER_SEGMENT Enabled
	 * userSession with wealth User
	 * Then return variables null or 0 with not database values
	 */
	@Test
	public void whenSetRDCLimitFieldsFromDB_GivenToFlagEnabledAndWealthUserAndNotDatabaseValues_thenReturnZeroValues() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(true);
		when(customerServices.getConfParametersByApp(any())).thenReturn(confParameterLimitsList);
		when(confParameterLimitsList.stream()).thenReturn(streamData);
		when(streamData.collect(any())).thenReturn(confParameterLimits);
		when(confParameterLimits.isEmpty()).thenReturn(false);
		when(userSession.isWealth(any(), any())).thenReturn(true);
		when(confParameterLimits.isEmpty()).thenReturn(false);
		when(confParameterLimits.get(any())).thenReturn(null);
		
		baseRemoteDepositForm.setRDCLimitFieldsFromDB(userSession);
		
		assertEquals(0, baseRemoteDepositForm.mDailyAmount.intValue());
		assertEquals(0, baseRemoteDepositForm.mDailyItem, 0);
		assertEquals(0, baseRemoteDepositForm.mMultidayAmount.intValue());
		assertEquals(0, baseRemoteDepositForm.mMultidayItem);
	}
	
	/**
	 * when SetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagDisabled_thenReturnZeroValues
	 * Flag DFM_LIMITS_PER_SEGMENT Enabled
	 * userSession with Premium Banking User
	 * Then return variables with premium banking database values
	 */
	@Test
	@Ignore
	public void whenSetRDCLimitFieldsFromDB_GivenToFlagEnabledAndPremiumBankingUser_thenReturnZeroValues() {
		confParameterLimits = new HashMap<String, String>();
		confParameterLimits.put(RemoteDepositConstants.PREMIUM_BANKING_DAILY_AMOUNT, "1");
		confParameterLimits.put(RemoteDepositConstants.PREMIUM_BANKING_DAILY_ITEM_COUNT, "1");
		confParameterLimits.put(RemoteDepositConstants.PREMIUM_BANKING_MULTIDAY_AMOUNT, "1");
		confParameterLimits.put(RemoteDepositConstants.PREMIUM_BANKING_MULTIDAY_ITEM_COUNT, "1");
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(true);
		when(customerServices.getConfParametersByApp(any())).thenReturn(confParameterLimitsList);
		when(confParameterLimitsList.stream()).thenReturn(streamData);
		when(streamData.collect(any())).thenReturn(confParameterLimits);
		when(userSession.isPremiumBanking(any(), any(), any())).thenReturn(true);
		
		baseRemoteDepositForm.setRDCLimitFieldsFromDB(userSession);
		assertNotNull(baseRemoteDepositForm.mDailyAmount);
		assertNotEquals(0, baseRemoteDepositForm.mDailyItem);
		assertNotNull(baseRemoteDepositForm.mMultidayAmount);
		assertNotEquals(0, baseRemoteDepositForm.mMultidayItem);
	}
	
	/**
	 * when SetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagDisabled_thenReturnZeroValues
	 * Flag DFM_LIMITS_PER_SEGMENT Enabled
	 * userSession with premium banking User
	 * Then return variables null or 0 with not database values
	 */
	@Test
	public void whenSetRDCLimitFieldsFromDB_GivenToFlagEnabledAndPremiumBankingUserAndNotDatabaseValues_thenReturnZeroValues() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(true);
		when(customerServices.getConfParametersByApp(any())).thenReturn(confParameterLimitsList);
		when(confParameterLimitsList.stream()).thenReturn(streamData);
		when(streamData.collect(any())).thenReturn(confParameterLimits);
		when(confParameterLimits.isEmpty()).thenReturn(false);
		when(userSession.isPremiumBanking(any(), any(), any())).thenReturn(true);
		when(confParameterLimits.isEmpty()).thenReturn(false);
		when(confParameterLimits.get(any())).thenReturn(null);
		
		baseRemoteDepositForm.setRDCLimitFieldsFromDB(userSession);
		
		assertEquals(0, baseRemoteDepositForm.mDailyAmount.intValue());
		assertEquals(0, baseRemoteDepositForm.mDailyItem);
		assertEquals(0, baseRemoteDepositForm.mMultidayAmount.intValue());
		assertEquals(0, baseRemoteDepositForm.mMultidayItem);
	}
	
	/**
	 * when SetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagDisabled_thenReturnZeroValues
	 * Flag DFM_LIMITS_PER_SEGMENT Enabled
	 * userSession with Retail User
	 * Then return variables with retail database values
	 */
	@Test
	public void whenSetRDCLimitFieldsFromDB_GivenToFlagEnabledAndRetailUser_thenReturnZeroValues() {
		confParameterLimits = new HashMap<String, String>();
		confParameterLimits.put(RemoteDepositConstants.REGULAR_DAILY_AMOUNT, "1");
		confParameterLimits.put(RemoteDepositConstants.REGULAR_DAILY_ITEM_COUNT, "1");
		confParameterLimits.put(RemoteDepositConstants.REGULAR_MULTIDAY_AMOUNT, "1");
		confParameterLimits.put(RemoteDepositConstants.REGULAR_MULTIDAY_ITEM_COUNT, "1");
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(true);
		when(customerServices.getConfParametersByApp(any())).thenReturn(confParameterLimitsList);
		when(confParameterLimitsList.stream()).thenReturn(streamData);
		when(streamData.collect(any())).thenReturn(confParameterLimits);
		
		baseRemoteDepositForm.setRDCLimitFieldsFromDB(userSession);
		assertNotNull(baseRemoteDepositForm.mDailyAmount);
		assertNotEquals(0, baseRemoteDepositForm.mDailyItem);
		assertNotNull(baseRemoteDepositForm.mMultidayAmount);
		assertNotEquals(0, baseRemoteDepositForm.mMultidayItem);
	}
	
	/**
	 * when SetRDCLimitFieldsFromDB_GivenToLimitsPerSegmentFlagDisabled_thenReturnZeroValues
	 * Flag DFM_LIMITS_PER_SEGMENT Enabled
	 * userSession with retail User
	 * Then return variables null or 0 with not database values
	 */
	@Test
	public void whenSetRDCLimitFieldsFromDB_GivenToFlagEnabledAndRetailUserAndNotDatabaseValues_thenReturnZeroValues() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(true);
		when(customerServices.getConfParametersByApp(any())).thenReturn(confParameterLimitsList);
		when(confParameterLimitsList.stream()).thenReturn(streamData);
		when(streamData.collect(any())).thenReturn(confParameterLimits);
		when(confParameterLimits.isEmpty()).thenReturn(false);
		when(confParameterLimits.get(any())).thenReturn(null);
		
		baseRemoteDepositForm.setRDCLimitFieldsFromDB(userSession);
		
		assertEquals(0, baseRemoteDepositForm.mDailyAmount.intValue());
		assertEquals(0, baseRemoteDepositForm.mDailyItem);
		assertEquals(0, baseRemoteDepositForm.mMultidayAmount.intValue());
		assertEquals(0, baseRemoteDepositForm.mMultidayItem);
	}
	
	/////////////////////////////// generateEnrollmentInfoRequest
	
	/**
	 * when generateEnrollmentInfoRequest
	 * Flag DFM_LIMITS_PER_SEGMENT Disabled
	 * userSession with wealth User
	 */
	@Test
	public void whenGenerateEnrollmentInfoRequest_GivenToFlagDisabledAndWealthUser_thenReturnWealthValuesRequest() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(false);
		when(userSession.isWealth(any(), any())).thenReturn(true);
		
		RDCEnrollmentInfoRequest request = baseRemoteDepositForm.generateEnrollmentInfoRequest(userSession);
		
		assertEquals(baseRemoteDepositForm.wealthDailyAmount, request.getUserDailyAmount());
		assertEquals(baseRemoteDepositForm.wealthDailyItem, request.getUserDailyItem());
		assertEquals(baseRemoteDepositForm.wealthMultidayAmount, request.getUserMultidayAmount());
		assertEquals(baseRemoteDepositForm.wealthMultidayItem, request.getUserMultidayItem());	
	}
	
	/**
	 * when generateEnrollmentInfoRequest
	 * Flag DFM_LIMITS_PER_SEGMENT Disabled
	 * userSession with retail User
	 */
	@Test
	public void whenGenerateEnrollmentInfoRequest_GivenToFlagDisabledAndRetailUser_thenReturnWealthValuesRequest() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(false);
		RDCEnrollmentInfoRequest request = baseRemoteDepositForm.generateEnrollmentInfoRequest(userSession);
		
		assertEquals(baseRemoteDepositForm.regularDailyAmount, request.getUserDailyAmount());
		assertEquals(baseRemoteDepositForm.regularDailyItem, request.getUserDailyItem());
		assertEquals(baseRemoteDepositForm.regularMultidayAmount, request.getUserMultidayAmount());
		assertEquals(baseRemoteDepositForm.regularMultidayItem, request.getUserMultidayItem());	
	}
	
	/**
	 * when generateEnrollmentInfoRequest
	 * Flag DFM_LIMITS_PER_SEGMENT Disabled
	 * userSession with commercial User
	 */
	@Test
	public void whenGenerateEnrollmentInfoRequest_GivenToFlagDisabledAndCommercialUser_thenReturnWealthValuesRequest() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(false);
		when(userSession.isComercialCustomer()).thenReturn(true);
		
		RDCEnrollmentInfoRequest request = baseRemoteDepositForm.generateEnrollmentInfoRequest(userSession);	
		
		assertNull(request.getUserDailyAmount());
		assertEquals(0,request.getUserDailyItem());
		assertNull(request.getUserMultidayAmount());
		assertEquals(0, request.getUserMultidayItem());
	}
	
	/**
	 * when generateEnrollmentInfoRequest
	 * Flag DFM_LIMITS_PER_SEGMENT Enabled
	 * userSession with wealth User
	 */
	@Test
	public void whenGenerateEnrollmentInfoRequest_GivenToFlagEnabledAndWealthUser_thenReturnWealthValuesRequest() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(true);
		when(userSession.isWealth(any(), any())).thenReturn(true);
		
		RDCEnrollmentInfoRequest request = baseRemoteDepositForm.generateEnrollmentInfoRequest(userSession);	
		
		assertNull(request.getUserDailyAmount());
		assertEquals(0, request.getUserDailyItem());
		assertNull(request.getUserMultidayAmount());
		assertEquals(0, request.getUserMultidayItem());
	}
	
	/**
	 * when generateEnrollmentInfoRequest
	 * Flag DFM_LIMITS_PER_SEGMENT Enabled
	 * userSession with wealth User
	 */
	@Test
	public void whenGenerateEnrollmentInfoRequest_GivenToFlagEnabledAndRetailUser_thenReturnWealthValuesRequest() {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_LIMITS_PER_SEGMENT)).thenReturn(true);
		
		RDCEnrollmentInfoRequest request = baseRemoteDepositForm.generateEnrollmentInfoRequest(userSession);	
		
		assertNull(request.getUserDailyAmount());
		assertEquals(0, request.getUserDailyItem());
		assertNull(request.getUserMultidayAmount());
		assertEquals(0, request.getUserMultidayItem());
	}

}
