package evertec.cibp.web.form;

import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.form.bean.CCPortalServices;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.prophit.service.DefaultProphITService;
import evertec.prophit.service.bean.CustomerInquiryBean;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Utils.class, WebUtils.class, StaticMessageSource.class, CcServiceController.class })
public class CcServiceControllerUT {

	private MockHttpServletRequest request;

	@Mock
	private HttpServletRequest requestMock;

	@Mock
	private HttpServletResponse responseMock;

	@Mock
	private Errors errors;

	@Mock
	private CCPortalServices cCPortalServices;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private DefaultProphITService defaultProphITService;

	@Mock
	private TVFrontendAccount tvFrontendAccount;

	@Mock
	private CustomerInquiryBean customerInquiryBean;

	@Mock
	private CCPortalServices ccServices;

	@Mock
	private BindException error;

	@Mock
	private TVAccountProductDefinition tVAccountProductDefinition;

	@InjectMocks
	@Spy
	private final CcServiceController ccServiceController = new CcServiceController();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		request = new MockHttpServletRequest();
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);

		when(WebUtils.getSessionAttribute(any(), eq("userSession"))).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerService.prophItService()).thenReturn(defaultProphITService);
		when(userSession.getProphItCustomerInfo()).thenReturn(customerInquiryBean);
		when(userSession.getCryptoKey()).thenReturn("test");
		when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);

	}

	@Test
	public void whenReferenceData_GiveAdditionalCardCostIsZero_ThenErrorIsNotPresent() throws Exception {

		//Assemble
		request.setParameter("screen", "additional");
		request.setParameter("nickname", "BLACK Dual");
		request.setParameter("id", "9437201364002000");

		when(tvFrontendAccount.getAccountNumber()).thenReturn("123456789");
		when(tvFrontendAccount.getProductDefinition()).thenReturn(tVAccountProductDefinition);
		when(customerInquiryBean.getAccountNumber()).thenReturn("123456789");
		when(defaultProphITService.getAdditionalCardCost(any())).thenReturn("0");
		PowerMockito.doReturn("DISABLED").when(ccServiceController, "getPinRequestStatus");
		
		//Act
		ccServiceController.referenceData(request, cCPortalServices, errors);
		
		//Assert
		verify(errors, never()).rejectValue(eq("error"), eq("credit.card.portal.tab.cc.services.additional.cost"), any());
	}

	@Test
	public void whenReferenceData_GiveAdditionalCardCostIsNotZero_ThenErrorIsPresent() throws Exception {
		
		//Assemble
		request.setParameter("screen", "additional");
		request.setParameter("nickname", "BLACK Dual");
		request.setParameter("id", "9437201364002000");

		when(tvFrontendAccount.getAccountNumber()).thenReturn("123456789");
		when(tvFrontendAccount.getProductDefinition()).thenReturn(tVAccountProductDefinition);
		when(customerInquiryBean.getAccountNumber()).thenReturn("123456789");
		when(defaultProphITService.getAdditionalCardCost(any())).thenReturn("1");
		PowerMockito.doReturn("DISABLED").when(ccServiceController, "getPinRequestStatus");

		//Act
		ccServiceController.referenceData(request, cCPortalServices, errors);
		
		//Assert
		verify(errors).rejectValue(eq("error"), eq("credit.card.portal.tab.cc.services.additional.cost"), any());
	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-18-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		PowerMockito.doReturn("1234").when(ccServiceController, "getPinRequestStatus");
		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(requestMock.getParameter(anyString())).thenReturn(null);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.prophItService()).thenReturn(new DefaultProphITService());
		when(userSession.getCryptoKey()).thenReturn("654321");

		Map<String, Object> result = ccServiceController.referenceData(requestMock, ccServices, error);
		Assert.assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when Onsubmit
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-18-2020
	 * @exception Exception error
	 */
	@Test
	public void whenOnsubmit_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("cookieAnalyticEnabled", true);

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
		when(customerService.prophItService()).thenReturn(new DefaultProphITService());
		when(requestMock.getParameter(anyString())).thenReturn("null");
		when(ccServices.getAccountPlasticNumbers()).thenReturn(null);
		when(tvFrontendAccount.getAccountNumber()).thenReturn(null);
		PowerMockito.doReturn("1234").when(ccServiceController, "getPinRequestStatus");
		PowerMockito.doReturn(mav).when((SimpleFormController)ccServiceController, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any());

		ModelAndView mavResult = ccServiceController.onSubmit(requestMock, responseMock, ccServices, error);
		Assert.assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}
}
