package evertec.cibp.web.form;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.campaign.loader.CampaignPaths;
import evertec.cibp.core.models.BinaryData.BinaryRecord;
import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.core.models.customer.CustomerDevices;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.customer.DefaultCustomerService;
import evertec.cibp.core.services.interfaces.CookiePreferenceStatusService;
import evertec.cibp.core.services.requests.ValidateLoginInfoRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.LoginBean;
import evertec.cibp.web.services.modules.CookiePreferenceServiceModule;
import evertec.cibp.web.services.modules.SignOnCustomerServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.ModelAndViewAssert;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SignOnCustomerServiceModule.class, StaticMessageSource.class,
		CustomerUtils.class, Utils.class, SignonSequenceUtils.class, DeviceUtils.class, WebUtils.class,
		ESAPI.class, ApiCallerFactory.class})
public class LoginFormUT {

	private static final String IS_SECURITIES_TRUE = "true";
	private static final String PATH = "/test/";

	private MockHttpServletResponse response;
	private MockHttpServletRequest request;

	@Mock
	private CampaignPaths paths;

	@Mock
	private HttpServletRequest httpServletRequest;

	@Mock
	private UserSession userSession;

	@Mock
	private LoginForm controller;

	@Mock
	private CookiePreferenceStatusService cookiePreferenceStatusService;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private SignOnCustomerServiceModule customerServiceModule;

	@Mock
	private UsernameValidationResponse responseAnswer;

	@Mock
	private BindException errors;

	@Mock
	private LoginBean command;

	@Mock
	private ValidateLoginInfoRequest validateLoginInfoRequest;

	@Mock
	private HTTPUtilities httpUtils;

	@Mock
	private BinaryRecord binaryRecord;

	@Mock
	private HTTPUtilities httPUtilities;

	@Mock
	private UsernameValidationResponse usernameValidationResponse;

	@Mock
	private CookiePreferenceServiceModule cookiePreferenceServiceModule;

	@Mock
	private ApiCaller apiCaller;

	private final List<CookiePreferenceStatus> cookiePreferenceStatusList = new ArrayList<>();

	@InjectMocks
	private DefaultCustomerService service;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

		PowerMockito.mockStatic(SignOnCustomerServiceModule.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(CustomerUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(SignonSequenceUtils.class);
		PowerMockito.mockStatic(DeviceUtils.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(ESAPI.class);
		httpServletRequest = PowerMockito.mock(HttpServletRequest.class);
		response = new MockHttpServletResponse();
		userSession = PowerMockito.mock(UserSession.class);
		controller = PowerMockito.spy(new LoginForm());
		service = PowerMockito.spy(new DefaultCustomerService());
		command = PowerMockito.spy(new LoginBean());
		request = new MockHttpServletRequest();
		usernameValidationResponse = PowerMockito.mock(UsernameValidationResponse.class);
		
		errors = new BindException(command, "LoginBean");

		request.addHeader("user-agent", UserAgentType.WEB);
		
		paths.setFilePath(PATH);
		controller.setPaths(paths);
		when(paths.getFilePath()).thenReturn(PATH);
		
		when(userSession.getUsername()).thenReturn("username");
		when(customerProfile.getUsername()).thenReturn("username");

		when(WebUtils.getCookie(request, "PMData")).thenReturn(new Cookie("PMData", "PMData"));
		when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
		
		when(responseAnswer.getCustomerProfile()).thenReturn(customerProfile);
		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.SUCCESS);

		CookiePreferenceStatus cookiePreferenceStatus = new CookiePreferenceStatus();
		cookiePreferenceStatus.setStatus("ENABLED");
		cookiePreferenceStatus.setCategory("ANALYTICS");
		cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
		cookiePreferenceStatusList.add(cookiePreferenceStatus);

		cookiePreferenceStatus = new CookiePreferenceStatus();
		cookiePreferenceStatus.setStatus("ENABLED");
		cookiePreferenceStatus.setCategory("FUNCTIONAL");
		cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
		cookiePreferenceStatusList.add(cookiePreferenceStatus);

		cookiePreferenceStatus = new CookiePreferenceStatus();
		cookiePreferenceStatus.setStatus("ENABLED");
		cookiePreferenceStatus.setCategory("ADVERTISING");
		cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
		cookiePreferenceStatusList.add(cookiePreferenceStatus);

		controller.setCookiePreferenceService(cookiePreferenceServiceModule);

		customerProfile = PowerMockito.mock(CustomerProfile.class);
    	customerProfile.setStatus(CustomerStatus.ACTIVE);
    	customerProfile.setIsSecurities(IS_SECURITIES_TRUE);
    	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP)).thenReturn(Boolean.FALSE);
	}

	@Test
	public void testLoginForm() throws Exception {

		/*
		//PowerMockito.doReturn(controller).when(controller, "getSession", request);
		//PowerMockito.doReturn(new CustomerProfile()).when(userSession, "getCustomerProfile");

		try {
			command.setUsername("valdesec");
			request.addHeader("user-agent","Mozilla/5.0 (Linux; U; Android 2.3.1; en-us; MID Build/GINGERBREAD) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1. Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36.");
			controller.setCustomerService(service);
			ModelAndView modelAndView = controller.onSubmit(request, response, command, new BindException("", ""));
			assertEquals("login", modelAndView.getViewName());
		} catch (Exception e) {
			fail();
		}
		*/

    }
	
//	
//     /**
//      * Unit testing de funcionalidad agregada en las líneas 170 a la 177, para el proceso de resetPasswordSec del equipo MBFIS
//      * @throws Exception
//      */
//	@Test
//    public void whenOnSubmit_GivenResultStatusNotSuccess_ReturnModelAndView () {
//        try {
//        	UsernameValidationResponse validateUsernameResponse = new UsernameValidationResponse();
//        	customerProfile = new CustomerProfile();
//        	customerProfile.setStatus(CustomerStatus.ACTIVE);
//            customerProfile.setIsSecurities(IS_SECURITIES_TRUE);
//        	validateUsernameResponse.setCustomerProfile(customerProfile);
//            command.setUsername("valdesec");
//           
//            validateUsernameResponse.setStatusResult(ResultStatus.SUCCESS);
//            when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP)).thenReturn(Boolean.TRUE);
//            when(customerServiceModule.validateUsername(any(CustomerServices.class), any(ValidateLoginInfoRequest.class))).thenReturn(validateUsernameResponse);
//            when(SignonSequenceUtils.getNextView(userSession)).thenReturn("error");
//            
//            ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
//            assertNull(modelAndView.getViewName());
//        } catch (Exception e) {
//            fail(e.getMessage());
//        }
//    }
//	//Test MBFIS-517
//	@Test
//	public void whenOnSubmit_GivenMUST_CHANGE_USERNAME_AndSecurities_ThenReturnViewForgotPasswordSec()
//			throws Exception {
//
//		command.setUsername("valdesec");
//	
//		
//		when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(Boolean.TRUE);
//		
//		when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
//		when(customerServiceModule.validateUsername(any(CustomerServices.class),
//				any(ValidateLoginInfoRequest.class))).thenReturn(responseAnswer);
//		
//		when(responseAnswer.getCustomerProfile()).thenReturn(customerProfile);
//		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
//		when(responseAnswer.getCustomerProfile().getStatus()).thenReturn(CustomerStatus.ACTIVE);
//		
//		when(CustomerUtils.isSecurities(customerProfile)).thenReturn(Boolean.TRUE);		
//		
//		controller.onSubmit(request, response, command, errors);
//		assertTrue(response.getRedirectedUrl().equalsIgnoreCase("forgotpasswordsec"));
//
//	}
//	
//	@Test
//	public void whenOnSubmit_GivenMUST_CHANGE_USERNAME_AndNotSecurities_ThenReturnModelAndView()
//			throws Exception {
//
//		command.setUsername("valdesec");
//	
//		
//		when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(Boolean.TRUE);
//		
//		when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
//		when(customerServiceModule.validateUsername(any(CustomerServices.class),
//				any(ValidateLoginInfoRequest.class))).thenReturn(responseAnswer);
//		
//		when(responseAnswer.getCustomerProfile()).thenReturn(customerProfile);
//		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
//		when(responseAnswer.getCustomerProfile().getStatus()).thenReturn(CustomerStatus.ACTIVE);
//		
//		when(CustomerUtils.isSecurities(customerProfile)).thenReturn(Boolean.FALSE);		
//		
//		ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
//		assertViewName(modelAndView, null);
//
//	}
//	
//	@Test
//	public void whenOnSubmit_GivenMUST_CHANGE_USERNAME_False_AndSecurities_ThenReturnViewModelAndView()
//			throws Exception {
//
//		command.setUsername("valdesec");
//	
//		
//		when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(Boolean.FALSE);
//		
//		when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
//		when(customerServiceModule.validateUsername(any(CustomerServices.class),
//				any(ValidateLoginInfoRequest.class))).thenReturn(responseAnswer);
//		
//		when(responseAnswer.getCustomerProfile()).thenReturn(customerProfile);
//		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
//		when(responseAnswer.getCustomerProfile().getStatus()).thenReturn(CustomerStatus.ACTIVE);
//		
//		when(CustomerUtils.isSecurities(customerProfile)).thenReturn(Boolean.TRUE);		
//		
//		ModelAndView modelAndView = controller.onSubmit(request, response, command, errors);
//		assertViewName(modelAndView, null);
//
//	}
//	
//	@Test(expected = NullPointerException.class)
//	public void whenOnSubmit_GivenMUST_CHANGE_USERNAME_True_AndCustomerProfileNull_ThenNullPointerException()
//			throws Exception {
//
//		command.setUsername("valdesec");
//	
//		
//		when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(Boolean.TRUE);
//		
//		when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
//		when(customerServiceModule.validateUsername(any(CustomerServices.class),
//				any(ValidateLoginInfoRequest.class))).thenReturn(responseAnswer);
//		
//		when(responseAnswer.getCustomerProfile()).thenReturn(null);
//		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
//		when(responseAnswer.getCustomerProfile().getStatus()).thenReturn(CustomerStatus.ACTIVE);
//		
//		when(CustomerUtils.isSecurities(customerProfile)).thenReturn(Boolean.TRUE);		
//		
//		
//		controller.onSubmit(request, response, command, errors);
//
//	}
//	
//	@Test(expected = NullPointerException.class)
//	public void whenOnSubmit_GivenMUST_CHANGE_USERNAME_False_AndCustomerProfileNull_ThenNullPointerException()
//			throws Exception {
//
//		command.setUsername("valdesec");
//	
//		
//		when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(Boolean.FALSE);
//		
//		when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
//		when(customerServiceModule.validateUsername(any(CustomerServices.class),
//				any(ValidateLoginInfoRequest.class))).thenReturn(responseAnswer);
//		
//		when(responseAnswer.getCustomerProfile()).thenReturn(null);
//		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
//		when(responseAnswer.getCustomerProfile().getStatus()).thenReturn(CustomerStatus.ACTIVE);
//		
//		when(CustomerUtils.isSecurities(customerProfile)).thenReturn(Boolean.TRUE);		
//		
//		
//		controller.onSubmit(request, response, command, errors);
//	}	
//	//END Test MBFIS-517
//	
//	@Test
//	public void whenReferenceData_GivenFlag_MBSD3806_True_And_LoadLoginPage_ThenReturnModelAndView()
//			throws Exception {
//
//		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3806)).thenReturn(Boolean.TRUE);
//		
//		when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
//		when(customerServiceModule.validateUsername(any(CustomerServices.class),
//				any(ValidateLoginInfoRequest.class))).thenReturn(responseAnswer);
//		
//		when(responseAnswer.getCustomerProfile()).thenReturn(customerProfile);
//		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
//		when(responseAnswer.getCustomerProfile().getStatus()).thenReturn(CustomerStatus.ACTIVE);
//		
//		
//		Map<String, Object> modelAndView = controller.referenceData(request, command, errors);
//		assertEquals(modelAndView.get("MBSD3806"), "true");
//
//	}
//	
//	@Test
//	public void whenReferenceData_GivenFlag_MBSD3806_False_And_LoadLoginPage_ThenReturnModelAndView()
//			throws Exception {
//
//		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3806)).thenReturn(Boolean.FALSE);
//		
//		when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
//		when(customerServiceModule.validateUsername(any(CustomerServices.class),
//				any(ValidateLoginInfoRequest.class))).thenReturn(responseAnswer);
//		
//		when(responseAnswer.getCustomerProfile()).thenReturn(customerProfile);
//		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
//		when(responseAnswer.getCustomerProfile().getStatus()).thenReturn(CustomerStatus.ACTIVE);
//		
//		
//		Map<String, Object> modelAndView = controller.referenceData(request, command, errors);
//		assertEquals(modelAndView.get("MBSD3806"), "false");
//
//	}
//

	/**
	 * when OnSubmit
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 * @author ET58344, Evertec Inc.
	 * @since 03-05-2021
	 * @throws Exception exception
	 */
	@Test
	public void whenOnSubmit_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue()
			throws Exception {
		controller.setCustomerService(customerService);
		command.setUsername("valdesec");

		UsernameValidationResponse validateUsernameResponse = new UsernameValidationResponse();
    	validateUsernameResponse.setCustomerProfile(customerProfile);
    	validateUsernameResponse.setStatusResult(ResultStatus.SUCCESS);
    	when(customerService.validateUsername(any(ValidateLoginInfoRequest.class))).thenReturn(validateUsernameResponse);
		
		PowerMockito.whenNew(UserSession.class).withArguments(any(), any(), any()).thenReturn(userSession);
		when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(Boolean.TRUE);
		when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE179)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP)).thenReturn(Boolean.TRUE);
		when(CustomerUtils.isSecurities(any())).thenReturn(Boolean.FALSE);
		when(responseAnswer.getCustomerProfile()).thenReturn(null);
		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
		when(responseAnswer.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getStatus()).thenReturn(CustomerStatus.ACTIVE);
		when(responseAnswer.getDeviceRequest()).thenReturn(null);
		when(responseAnswer.getRsaSessionId()).thenReturn("1234");
		when(responseAnswer.getRsaTransactionId()).thenReturn("1234");
		when(responseAnswer.getTasksList()).thenReturn(null);
		when(responseAnswer.getAccountsToValidate()).thenReturn(null);
		when(Utils.getRSALanguage(any())).thenReturn("");
		when(customerService.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
		when(cookiePreferenceStatusService.getCookiePreferenceStatusByProfileId(any())).thenReturn(cookiePreferenceStatusList);
		when(customerProfile.getProfileId()).thenReturn("987654321asflkj");
		PowerMockito.doNothing().when(userSession).setCookiePreferenceStatusList(any());
		when(SignonSequenceUtils.getNextView(any())).thenReturn("login");
		when(command.getPromo()).thenReturn("");
		when(ESAPI.httpUtilities()).thenReturn(httPUtilities);
		PowerMockito.doNothing().when(httPUtilities).addCookie(any(Cookie.class));
		PowerMockito.doNothing().when(cookiePreferenceServiceModule).deleteAllCookiePreference(any(), any());
		when(httpServletRequest.getHeader("User-Agent")).thenReturn("android-test-agent");
		when(DeviceUtils.getCustomerDevice(anyString())).thenReturn(CustomerDevices.IPHONE);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileId()).thenReturn("546sdf4a65sf46a5sdf46as4df");

		ModelAndView mavResult = controller.onSubmit(request, response, command, errors);
		ModelAndViewAssert.assertViewName(mavResult, "login");
	}

	/**
	 * when OnSubmit
	 * Given PreferencesNotNull
	 * Then ReturnModelValueTrue
	 * @author ET58344, Evertec Inc.
	 * @since 03-05-2021
	 * @throws Exception exception
	 */
	@Test
	public void whenOnSubmit_GivenPreferencesNotNull_ThenReturnModelValueTrue()
			throws Exception {
		controller.setCustomerService(customerService);
		command.setUsername("valdesec");
		
		UsernameValidationResponse validateUsernameResponse = new UsernameValidationResponse();
    	validateUsernameResponse.setCustomerProfile(customerProfile);
    	validateUsernameResponse.setStatusResult(ResultStatus.SUCCESS);
    	when(customerService.validateUsername(any(ValidateLoginInfoRequest.class))).thenReturn(validateUsernameResponse);
		
		PowerMockito.whenNew(UserSession.class).withArguments(any(), any(), any()).thenReturn(userSession);
		when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(Boolean.TRUE);
		when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE179)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP)).thenReturn(Boolean.TRUE);
		when(CustomerUtils.isSecurities(any())).thenReturn(Boolean.FALSE);
		when(responseAnswer.getCustomerProfile()).thenReturn(null);
		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
		when(responseAnswer.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getStatus()).thenReturn(CustomerStatus.ACTIVE);
		when(CustomerUtils.isSecurities(customerProfile)).thenReturn(Boolean.TRUE);
		when(httpServletRequest.getHeader("User-Agent")).thenReturn("android-test-agent");
		when(DeviceUtils.getCustomerDevice(anyString())).thenReturn(CustomerDevices.BLACKBERRY);
		controller.onSubmit(httpServletRequest, response, command, errors);
	}	
	//END Test MBFIS-517
	
	@Test
	
	public void whenReferenceData_GivenFlag_MBSD3806_True_And_LoadLoginPage_ThenReturnModelAndView()
			throws Exception {

		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3806)).thenReturn(Boolean.TRUE);
		
		when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
		when(customerService.validateUsername(any(ValidateLoginInfoRequest.class))).thenReturn(responseAnswer);
		
		when(responseAnswer.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getStatus()).thenReturn(CustomerStatus.ACTIVE);
		when(responseAnswer.getDeviceRequest()).thenReturn(null);
		when(responseAnswer.getRsaSessionId()).thenReturn("1234");
		when(responseAnswer.getRsaTransactionId()).thenReturn("1234");
		when(responseAnswer.getTasksList()).thenReturn(null);
		when(responseAnswer.getAccountsToValidate()).thenReturn(null);
		when(Utils.getRSALanguage(any())).thenReturn("");
		when(customerService.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
		when(cookiePreferenceStatusService.getCookiePreferenceStatusByProfileId(any())).thenReturn(new ArrayList<>());
		when(customerProfile.getProfileId()).thenReturn("987654321asflkj");
		PowerMockito.doNothing().when(userSession).setCookiePreferenceStatusList(any());
		when(SignonSequenceUtils.getNextView(any())).thenReturn("login");
		when(command.getPromo()).thenReturn("");
		when(ESAPI.httpUtilities()).thenReturn(httPUtilities);
		PowerMockito.doNothing().when(httPUtilities).addCookie(any(Cookie.class));
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		PowerMockito.doNothing().when(cookiePreferenceServiceModule).deleteAllCookiePreference(any(), any());
		when(cookiePreferenceServiceModule.createProfileCookiePreference(any())).thenReturn(cookiePreferenceStatusList);
		when(httpServletRequest.getHeader("User-Agent")).thenReturn("android-test-agent");
		when(DeviceUtils.getCustomerDevice(anyString())).thenReturn(CustomerDevices.IPHONE);

		ModelAndView mavResult = controller.onSubmit(request, response, command, errors);
		ModelAndViewAssert.assertViewName(mavResult, "login");
	}
	
	/**
	 * when OnSubmit
	 * Given PreferencesNull
	 * Then ReturnModelValueTrue
	 * @author ET58344, Evertec Inc.
	 * @since 03-05-2021
	 * @throws Exception exception
	 */
	@Test
	public void whenReferenceData_GivenFlag_MBSD3806_False_And_LoadLoginPage_ThenReturnModelAndView()
			throws Exception {
		controller.setCustomerService(customerService);
		command.setUsername("valdesec");
		
		UsernameValidationResponse validateUsernameResponse = new UsernameValidationResponse();
    	validateUsernameResponse.setCustomerProfile(customerProfile);
    	validateUsernameResponse.setStatusResult(ResultStatus.SUCCESS);
    	when(customerService.validateUsername(any(ValidateLoginInfoRequest.class))).thenReturn(validateUsernameResponse);
		
		PowerMockito.whenNew(UserSession.class).withArguments(any(), any(), any()).thenReturn(userSession);
		when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(Boolean.TRUE);
		when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE179)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP)).thenReturn(Boolean.TRUE);
		when(CustomerUtils.isSecurities(any())).thenReturn(Boolean.FALSE);
		when(responseAnswer.getCustomerProfile()).thenReturn(null);
		when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
		when(responseAnswer.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getStatus()).thenReturn(CustomerStatus.ACTIVE);
		when(responseAnswer.getDeviceRequest()).thenReturn(null);
		when(responseAnswer.getRsaSessionId()).thenReturn("1234");
		when(responseAnswer.getRsaTransactionId()).thenReturn("1234");
		when(responseAnswer.getTasksList()).thenReturn(null);
		when(responseAnswer.getAccountsToValidate()).thenReturn(null);
		when(Utils.getRSALanguage(any())).thenReturn("");
		when(customerService.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
		when(cookiePreferenceStatusService.getCookiePreferenceStatusByProfileId(any())).thenReturn(null);
		when(customerProfile.getProfileId()).thenReturn("987654321asflkj");
		PowerMockito.doNothing().when(userSession).setCookiePreferenceStatusList(any());
		when(SignonSequenceUtils.getNextView(any())).thenReturn("login");
		when(command.getPromo()).thenReturn("");
		when(ESAPI.httpUtilities()).thenReturn(httPUtilities);
		PowerMockito.doNothing().when(httPUtilities).addCookie(any(Cookie.class));
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		PowerMockito.doNothing().when(cookiePreferenceServiceModule).deleteAllCookiePreference(any(), any());
		when(cookiePreferenceServiceModule.createProfileCookiePreference(any())).thenReturn(cookiePreferenceStatusList);
		when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class), anyString())).thenReturn(new CustomerInteractionEvent());
		when(httpServletRequest.getHeader("User-Agent")).thenReturn("android-test-agent");
		when(DeviceUtils.getCustomerDevice(anyString())).thenReturn(CustomerDevices.IPHONE);

		ModelAndView mavResult = controller.onSubmit(httpServletRequest, response, command, errors);
		ModelAndViewAssert.assertViewName(mavResult, "login");
	}

	/**
	 * when OnSubmit
	 * Given Result Status Fraud blocked
	 * Then ReturnModelAndView
	 * @author Evertec Inc.
	 * @since 12-16-2021
	 * @throws Exception exception
	 */
	@Test
	public void whenOnSubmit_GivenResultStatusFraudBlocked_ThenReturnModelAndView()
			throws Exception {
		when(usernameValidationResponse.getStatusResult()).thenReturn(ResultStatus.FRAUD_BLOCKED);
		when(customerService.validateUsername(any())).thenReturn(usernameValidationResponse);
		when(command.getDevice_info_rsa()).thenReturn("test");
		controller.setCustomerService(customerService);
		command.setUsername("test");
		when(ESAPI.httpUtilities()).thenReturn(httPUtilities);
		when(httpServletRequest.getHeader("User-Agent")).thenReturn("android-test-agent");
		when(DeviceUtils.getCustomerDevice(anyString())).thenReturn(CustomerDevices.IPHONE);

		ModelAndView mavResult = controller.onSubmit(httpServletRequest, response, command, errors);
		ModelAndViewAssert.assertViewName(mavResult, "login");
	}

	/**
	 * when OnSubmit
	 * Given Result Status Pass Reset Blocked
	 * Then ReturnModelAndView
	 * @author Evertec Inc.
	 * @since 12-16-2021
	 * @throws Exception exception
	 */
	@Test
	public void whenOnSubmit_GivenResultStatusPassResetBlocked_ThenReturnModelAndView()
			throws Exception {
		when(usernameValidationResponse.getStatusResult()).thenReturn(ResultStatus.PASS_RESET_BLOCKED);
		when(customerService.validateUsername(any())).thenReturn(usernameValidationResponse);
		controller.setCustomerService(customerService);
		command.setUsername("test");

		when(ESAPI.httpUtilities()).thenReturn(httPUtilities);
		when(httpServletRequest.getHeader("User-Agent")).thenReturn("android-test-agent");
		when(DeviceUtils.getCustomerDevice(anyString())).thenReturn(CustomerDevices.IPHONE);

		ModelAndView mavResult = controller.onSubmit(httpServletRequest, response, command, errors);
		ModelAndViewAssert.assertViewName(mavResult, "login");
	}

	/**
	 * when OnSubmit
	 * Given Result Status Pass Blocked
	 * Then ReturnModelAndView
	 * @author Evertec Inc.
	 * @since 12-16-2021
	 * @throws Exception exception
	 */
	@Test
	public void whenOnSubmit_GivenResultStatusPassBlocked_ThenReturnModelAndView()
			throws Exception {
		when(usernameValidationResponse.getStatusResult()).thenReturn(ResultStatus.PASS_BLOCKED);
		when(customerService.validateUsername(any())).thenReturn(usernameValidationResponse);
		controller.setCustomerService(customerService);
		command.setUsername("test");

		when(ESAPI.httpUtilities()).thenReturn(httPUtilities);
		when(httpServletRequest.getHeader("User-Agent")).thenReturn("android-test-agent");
		when(DeviceUtils.getCustomerDevice(anyString())).thenReturn(CustomerDevices.IPHONE);

		ModelAndView mavResult = controller.onSubmit(httpServletRequest, response, command, errors);
		ModelAndViewAssert.assertViewName(mavResult, "login");
	}

	/**
	 * when OnSubmit
	 * Given Result Status Deny
	 * Then ReturnModelAndView
	 * @author Evertec Inc.
	 * @since 12-16-2021
	 * @throws Exception exception
	 */
	@Test
	public void whenOnSubmit_GivenResultStatusDeny_ThenReturnModelAndView()
			throws Exception {
		when(usernameValidationResponse.getStatusResult()).thenReturn(ResultStatus.DENY);
		when(customerService.validateUsername(any())).thenReturn(usernameValidationResponse);
		controller.setCustomerService(customerService);
		command.setUsername("test");

		when(ESAPI.httpUtilities()).thenReturn(httPUtilities);
		when(httpServletRequest.getHeader("User-Agent")).thenReturn("android-test-agent");
		when(DeviceUtils.getCustomerDevice(anyString())).thenReturn(CustomerDevices.IPHONE);

		ModelAndView mavResult = controller.onSubmit(httpServletRequest, response, command, errors);
		ModelAndViewAssert.assertViewName(mavResult, "login");
	}

	/**
	 * when OnSubmit
	 * Given Result Status Error
	 * Then ReturnModelAndView
	 * @author Evertec Inc.
	 * @since 12-16-2021
	 * @throws Exception exception
	 */
	@Test
	public void whenOnSubmit_GivenResultStatusError_ThenReturnModelAndView()
			throws Exception {
		when(usernameValidationResponse.getStatusResult()).thenReturn(ResultStatus.ERROR);
		when(customerService.validateUsername(any())).thenReturn(usernameValidationResponse);
		controller.setCustomerService(customerService);
		command.setUsername("test");

		when(ESAPI.httpUtilities()).thenReturn(httPUtilities);
		when(httpServletRequest.getHeader("User-Agent")).thenReturn("android-test-agent");
		when(DeviceUtils.getCustomerDevice(anyString())).thenReturn(CustomerDevices.IPHONE);

		ModelAndView mavResult = controller.onSubmit(httpServletRequest, response, command, errors);
		ModelAndViewAssert.assertViewName(mavResult, "login");
	}

	/**
	 * when GenerateDeviceRequest
	 * Given isEnableSDK and isSDKMobileRequest
	 * Then Return DeviceRequest
	 * @author Evertec Inc.
	 * @since 01-03-2022
	 * @throws Exception exception
	 */
	@Test
	public void whenGenerateDeviceRequest_givenIsEnableSDKAndIsSDKMobileRequest_thenReturnDeviceRequest() throws Exception {
		LoginBean loginBean = new LoginBean();
		Cookie cookie = new Cookie("test", "test");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC615_MOBILE_SDK_DATA)).thenReturn(true);
		DeviceRequest request = Whitebox.invokeMethod(controller, "generateDeviceRequest", httpServletRequest, true, loginBean, cookie, "test");
		assertNotNull(request);
	}
}
