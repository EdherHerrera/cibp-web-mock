package evertec.cibp.web.form;

import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;
import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerWealthPreferred;
import evertec.cibp.core.models.customer.applications.EpayrollApplication;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.EpayrollServiceBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.*;


@RunWith(PowerMockRunner.class)
@PrepareForTest({CustomerFacade.class,StaticMessageSource.class, WebUtils.class, Utils.class, CustomerWealthPreferred.class, ClientUtils.class})
public class EpayrollServiceFormIT extends AbstractModelAndViewTests {

    private MockHttpServletRequest request;

    @Mock
    private UserSession userSession;

    @InjectMocks
    private EpayrollServiceForm controller;


    @Before
    public void setUp() throws Exception {

        PowerMockito.mockStatic(Utils.class);

        PowerMockito.mockStatic(WebUtils.class);

        request = new MockHttpServletRequest();


        super.setUp();

    }



    @Test
    public void whenOnBindAndValidate_ValidateTelephoneInput_ShouldBe_Number() throws Exception {
        EpayrollServiceBean bean = new EpayrollServiceBean();
        BindException error = new BindException(bean, "EpayrollServiceBean");
        EpayrollServiceForm mock = spy(controller);
        request = new MockHttpServletRequest();
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(Utils.getRSALanguage(request)).thenReturn("message");
        bean.setPhone1("123");
        bean.setPhone2("123");
        bean.setPhone3("1234");
        bean.setBusinessPhone1("123");
        bean.setBusinessPhone2("123");
        bean.setBusinessPhone3("1234");

        this.controller.onBindAndValidate(request, bean, error);
        assertNull(error.getFieldError("phone3"));
        assertNull(error.getFieldError("businessPhone3"));
    }

    @Test
    public void whenOnBindAndValidate_ValidateTelephoneInput_ShouldBe_Number_ThenReturnError() throws Exception {
        EpayrollServiceBean bean = new EpayrollServiceBean();
        BindException error = new BindException(bean, "EpayrollServiceBean");
        EpayrollServiceForm mock = spy(controller);
        request = new MockHttpServletRequest();
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(Utils.getRSALanguage(request)).thenReturn("message");
        bean.setPhone1("Test");
        bean.setPhone2("Test");
        bean.setPhone3("Test");
        bean.setBusinessPhone1("Test");
        bean.setBusinessPhone2("Test");
        bean.setBusinessPhone3("Test");

        this.controller.onBindAndValidate(request, bean, error);
        assertNotNull(error.getFieldError("phone3"));
        assertNotNull(error.getFieldError("businessPhone3"));
    }



}
