package evertec.cibp.web.form;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.LinkedHashMap;
import java.util.Map;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.Errors;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.profile.ProfileType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.CCPortalVacations;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;



@RunWith(PowerMockRunner.class)
@PrepareForTest({ Utils.class, WebUtils.class, StaticMessageSource.class })
public class CcVacationsControllerUT {

    @Mock
    private UserSession userSession;

    @Mock
    private MessageSource messageSource;

    @Mock
    private CustomerServices customerService;

    @Mock
    private CustomerProfile customerProfile;

    @InjectMocks
    @Spy
    private CcVacationsController controller = new CcVacationsController();

    private MockHttpServletRequest request;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        request = new MockHttpServletRequest();
        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(StaticMessageSource.class);

        when(WebUtils.getSessionAttribute(any(), eq("userSession"))).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
    }

    @Test
    public void WhenGetDomesticMap_GivenLanguajeSP_ThenReturnNotNull() throws Exception
    {
        Map<String, Object> result = new LinkedHashMap<String, Object>();

        when(userSession.getLanguage()).thenReturn("sp");
        when(messageSource.getMessage(eq("credit.card.portal.vacations.domestic.yes"), any(),any())).thenReturn("Si");
        when(messageSource.getMessage(eq("credit.card.portal.vacations.domestic.no"), any(),any())).thenReturn("No");

        result = Whitebox.invokeMethod(controller, "getDomesticMap", userSession);
        assertNotNull(result);
    }

    @Test
    public void WhenGetDomesticMap_GivenLanguajeES_ThenReturnNotNull() throws Exception
    {
        Map<String, Object> result = new LinkedHashMap<String, Object>();

        when(userSession.getLanguage()).thenReturn("es");
        when(messageSource.getMessage(eq("credit.card.portal.vacations.domestic.yes"), any(),any())).thenReturn("Si");
        when(messageSource.getMessage(eq("credit.card.portal.vacations.domestic.no"), any(),any())).thenReturn("No");

        result = Whitebox.invokeMethod(controller, "getDomesticMap", userSession);
        assertNotNull(result);
    }

    @Test
    public void WhenGetDomesticMap_GivenLanguajeEN_ThenReturnNotNull() throws Exception
    {
        Map<String, Object> result = new LinkedHashMap<String, Object>();

        when(userSession.getLanguage()).thenReturn("en");
        when(messageSource.getMessage(eq("credit.card.portal.vacations.domestic.yes"), any(),any())).thenReturn("Yes");
        when(messageSource.getMessage(eq("credit.card.portal.vacations.domestic.no"), any(),any())).thenReturn("No");

        result = Whitebox.invokeMethod(controller, "getDomesticMap", userSession);
        assertNotNull(result);
    }

}

