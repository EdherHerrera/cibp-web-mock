package evertec.cibp.web.form;

import evertec.cibp.core.models.accountopening.dci.DCICustomer;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.DCIQuestionsBean;
import evertec.cibp.web.form.bean.PaymentsListBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import static org.mockito.Mockito.mock;
import java.util.*;
import evertec.cibp.core.models.branch.BranchZipcodeAssignment;
import evertec.cibp.vs.enums.address.AddressType;
import evertec.cibp.vs.models.requests.CustomerAddressesRequest;
import evertec.cibp.vs.models.results.CustomerAddressesResult;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.web.form.bean.PaymentBean;



/**
 * Unit testing DCIQuestionsForm java class.
 * @author Evertec ET58344
 * @since 09-18-2020
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CustomerFacade.class, StaticMessageSource.class, DCIQuestionsForm.class, Utils.class})
public class DCIQuestionsFormUT extends AbstractModelAndViewTests  {

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private UserSession userSession;

	@Mock
	private DCIQuestionsBean bean;

	@Mock
	private BindException error;

	@Mock
	private DCICustomer dciCustomer;

	@Mock
	private BankingSession bankingSession;

	@Mock
	private CustomerInteractionEvent customerInteractionEvent;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private MessageSource messageSource;

	@Mock
	private PaymentsListBean paymentListBean;

	@InjectMocks
	private DCIQuestionsForm dciQuestionForm;

	@InjectMocks
	@Spy
	private final DCIQuestionsForm controller = new DCIQuestionsForm();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();

		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(messageSource.getMessage("ebpp.module",null,new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(paymentListBean.getSingleAccountFrom()).thenReturn("multiple");
	}


	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-18-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession)
				.thenReturn(Boolean.TRUE);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getDciCustomer()).thenReturn(dciCustomer);
		when(dciCustomer.getAccount()).thenReturn("123456789");
		when(userSession.isWealth(anyString(), anyString())).thenReturn(Boolean.TRUE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE165)).thenReturn(Boolean.TRUE);

		Map<String, Object> result = controller.referenceData(request);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when OnSubmit
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-18-2020
	 * @exception Exception error
	 */
	@Test
	public void whenOnSubmit_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getDciQuestions()).thenReturn(new ArrayList<>());
		when(userSession.getDciQuestionAnswers()).thenReturn(new ArrayList<>());
		when(userSession.getDciCustomer()).thenReturn(new DCICustomer());
		when(Utils.getRSALanguage(any())).thenReturn("en");
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(dciCustomer.getAccount()).thenReturn("1234567890");
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(bankingSession.getPermId()).thenReturn(new BigInteger("000111222333"));
		when(customerService.getCustomerAddresses(any())).thenReturn(null);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OPAC)).thenReturn(Boolean.FALSE);
		PowerMockito.doNothing().when(customerService).updateDCICustomer(any());
		PowerMockito.doNothing().when(customerService).updateDCICustomerAnswers(any(), any());
		PowerMockito.doNothing().when(customerInteractionEvent).putString(any(), anyString());

		ModelAndView mavResult = controller.onSubmit(request, response, bean, error);
		assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}

	@Test
	public void whenDciWuestionsForm_GivenGivenValidaPostalCode_ThenReturnSuccess() throws Exception {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);

		List<TVFrontEndPayee> payeesMock = new ArrayList();
		payeesMock.add(mock(TVFrontEndPayee.class));

		when(userSession.getPayessBasicEbpp(true)).thenReturn(payeesMock);

		Set<TVAccountTransferSource> accountsFrom = new HashSet();
		accountsFrom.add(mock(TVAccountTransferSource.class));

		when(userSession.getAccountsPaymentFrom()).thenReturn(accountsFrom);

		DCICustomer dciCustomer = new DCICustomer();
		dciCustomer.setAccount("123456");
		when(userSession.getDciCustomer()).thenReturn(dciCustomer);
		BankingSession bankingSession = new BankingSession(new BigInteger("1234567890"), "345345344234242");
		when(userSession.getBankingSession()).thenReturn(bankingSession);

		when(Utils.getRSALanguage(request)).thenReturn("es");

		CustomerInteractionEvent interactionEvent = new CustomerInteractionEvent();
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(interactionEvent);

		BranchZipcodeAssignment assignment = new BranchZipcodeAssignment();
		assignment.setBankCode("123");
		assignment.setBranchCode("123");
		assignment.setZipcode("00601");
		when(userSession.getCustomerService().getBranchZipcodeAssignment(any())).thenReturn(assignment);

		//when(userSession.getCustomerService().removeDCIControllsToAccount(any(), any(), any(), any())).;

		CustomerAddressesRequest addR = new CustomerAddressesRequest();
		addR.setPermId("1234567890");
		CustomerAddressesResult result = new CustomerAddressesResult();
		Map<AddressType, evertec.cibp.vs.beans.Address> addresses = new HashMap<AddressType, evertec.cibp.vs.beans.Address>();
		evertec.cibp.vs.beans.Address address = new evertec.cibp.vs.beans.Address();
		address.setAddr1("sfdf");
		address.setAddr2("Puerto Rico 00601-0000");
		addresses.put(AddressType.POSTAL, address);
		result.setAddresses(addresses);
		when(userSession.getCustomerService().getCustomerAddresses(any())).thenReturn(result);

		List<PaymentBean> payments = new ArrayList();
		payments.add(mock(PaymentBean.class));
		when(paymentListBean.getPayments()).thenReturn(payments);

		ModelAndView model = controller.onSubmit(request, response, paymentListBean, error);

		//assertViewName(model, "makePayment");
		assertModelAttributeValue(model, "text", "success");

	}

	@Test
	public void whenDciWuestionsForm_GivenGivenValidaPostalCodeWithoutSeparator_ThenReturnSuccess() throws Exception {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);

		List<TVFrontEndPayee> payeesMock = new ArrayList();
		payeesMock.add(mock(TVFrontEndPayee.class));

		when(userSession.getPayessBasicEbpp(true)).thenReturn(payeesMock);

		Set<TVAccountTransferSource> accountsFrom = new HashSet();
		accountsFrom.add(mock(TVAccountTransferSource.class));

		when(userSession.getAccountsPaymentFrom()).thenReturn(accountsFrom);

		DCICustomer dciCustomer = new DCICustomer();
		dciCustomer.setAccount("123456");
		when(userSession.getDciCustomer()).thenReturn(dciCustomer);
		BankingSession bankingSession = new BankingSession(new BigInteger("1234567890"), "345345344234242");
		when(userSession.getBankingSession()).thenReturn(bankingSession);

		when(Utils.getRSALanguage(request)).thenReturn("es");

		CustomerInteractionEvent interactionEvent = new CustomerInteractionEvent();
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(interactionEvent);

		BranchZipcodeAssignment assignment = new BranchZipcodeAssignment();
		assignment.setBankCode("123");
		assignment.setBranchCode("123");
		assignment.setZipcode("00601");
		when(userSession.getCustomerService().getBranchZipcodeAssignment(any())).thenReturn(assignment);

		//when(userSession.getCustomerService().removeDCIControllsToAccount(any(), any(), any(), any())).;

		CustomerAddressesRequest addR = new CustomerAddressesRequest();
		addR.setPermId("1234567890");
		CustomerAddressesResult result = new CustomerAddressesResult();
		Map<AddressType, evertec.cibp.vs.beans.Address> addresses = new HashMap<AddressType, evertec.cibp.vs.beans.Address>();
		evertec.cibp.vs.beans.Address address = new evertec.cibp.vs.beans.Address();
		address.setAddr1("sfdf");
		address.setAddr2("Puerto Rico 00601");
		addresses.put(AddressType.POSTAL, address);
		result.setAddresses(addresses);
		when(userSession.getCustomerService().getCustomerAddresses(any())).thenReturn(result);

		List<PaymentBean> payments = new ArrayList();
		payments.add(mock(PaymentBean.class));
		when(paymentListBean.getPayments()).thenReturn(payments);

		ModelAndView model = controller.onSubmit(request, response, paymentListBean, error);

		//assertViewName(model, "makePayment");
		assertModelAttributeValue(model, "text", "success");

	}

	@Test
	public void whenDciWuestionsForm_GivenGivenValidaPostalCodeOutOfAddress_ThenReturnSuccess() throws Exception {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);

		List<TVFrontEndPayee> payeesMock = new ArrayList();
		payeesMock.add(mock(TVFrontEndPayee.class));

		when(userSession.getPayessBasicEbpp(true)).thenReturn(payeesMock);

		Set<TVAccountTransferSource> accountsFrom = new HashSet();
		accountsFrom.add(mock(TVAccountTransferSource.class));

		when(userSession.getAccountsPaymentFrom()).thenReturn(accountsFrom);

		DCICustomer dciCustomer = new DCICustomer();
		dciCustomer.setAccount("123456");
		when(userSession.getDciCustomer()).thenReturn(dciCustomer);
		BankingSession bankingSession = new BankingSession(new BigInteger("1234567890"), "345345344234242");
		when(userSession.getBankingSession()).thenReturn(bankingSession);

		when(Utils.getRSALanguage(request)).thenReturn("es");

		CustomerInteractionEvent interactionEvent = new CustomerInteractionEvent();
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(interactionEvent);

		BranchZipcodeAssignment assignment = new BranchZipcodeAssignment();
		assignment.setBankCode("123");
		assignment.setBranchCode("123");
		assignment.setZipcode("00601");
		when(userSession.getCustomerService().getBranchZipcodeAssignment(any())).thenReturn(assignment);

		//when(userSession.getCustomerService().removeDCIControllsToAccount(any(), any(), any(), any())).;

		CustomerAddressesRequest addR = new CustomerAddressesRequest();
		addR.setPermId("1234567890");
		CustomerAddressesResult result = new CustomerAddressesResult();
		Map<AddressType, evertec.cibp.vs.beans.Address> addresses = new HashMap<AddressType, evertec.cibp.vs.beans.Address>();
		evertec.cibp.vs.beans.Address address = new evertec.cibp.vs.beans.Address();
		address.setAddr1("sfdf");
		address.setAddr2("Puerto Rico 00601");
		address.setPostalCode("00601");
		addresses.put(AddressType.POSTAL, address);
		result.setAddresses(addresses);
		when(userSession.getCustomerService().getCustomerAddresses(any())).thenReturn(result);

		List<PaymentBean> payments = new ArrayList();
		payments.add(mock(PaymentBean.class));
		when(paymentListBean.getPayments()).thenReturn(payments);

		ModelAndView model = controller.onSubmit(request, response, paymentListBean, error);

		//assertViewName(model, "makePayment");
		assertModelAttributeValue(model, "text", "success");

	}

	@Test
	public void whenDciWuestionsForm_GivenGivenNullPostalCodeButPostalCodeInAddress_ThenReturnSuccess() throws Exception {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);

		List<TVFrontEndPayee> payeesMock = new ArrayList();
		payeesMock.add(mock(TVFrontEndPayee.class));

		when(userSession.getPayessBasicEbpp(true)).thenReturn(payeesMock);

		Set<TVAccountTransferSource> accountsFrom = new HashSet();
		accountsFrom.add(mock(TVAccountTransferSource.class));

		when(userSession.getAccountsPaymentFrom()).thenReturn(accountsFrom);

		DCICustomer dciCustomer = new DCICustomer();
		dciCustomer.setAccount("123456");
		when(userSession.getDciCustomer()).thenReturn(dciCustomer);
		BankingSession bankingSession = new BankingSession(new BigInteger("1234567890"), "345345344234242");
		when(userSession.getBankingSession()).thenReturn(bankingSession);

		when(Utils.getRSALanguage(request)).thenReturn("es");

		CustomerInteractionEvent interactionEvent = new CustomerInteractionEvent();
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(interactionEvent);

		BranchZipcodeAssignment assignment = new BranchZipcodeAssignment();
		assignment.setBankCode("123");
		assignment.setBranchCode("123");
		assignment.setZipcode("00601");
		when(userSession.getCustomerService().getBranchZipcodeAssignment(any())).thenReturn(assignment);

		//when(userSession.getCustomerService().removeDCIControllsToAccount(any(), any(), any(), any())).;

		CustomerAddressesRequest addR = new CustomerAddressesRequest();
		addR.setPermId("1234567890");
		CustomerAddressesResult result = new CustomerAddressesResult();
		Map<AddressType, evertec.cibp.vs.beans.Address> addresses = new HashMap<AddressType, evertec.cibp.vs.beans.Address>();
		evertec.cibp.vs.beans.Address address = new evertec.cibp.vs.beans.Address();
		address.setAddr1("sfdf");
		address.setAddr2("Puerto Rico 00601");
		address.setPostalCode(null);
		addresses.put(AddressType.POSTAL, address);
		result.setAddresses(addresses);
		when(userSession.getCustomerService().getCustomerAddresses(any())).thenReturn(result);

		List<PaymentBean> payments = new ArrayList();
		payments.add(mock(PaymentBean.class));
		when(paymentListBean.getPayments()).thenReturn(payments);

		ModelAndView model = controller.onSubmit(request, response, paymentListBean, error);

		//assertViewName(model, "makePayment");
		assertModelAttributeValue(model, "text", "success");

	}

}
