package evertec.cibp.web.form.rsamod;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceData;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.DeviceResult;
import com.rsa.csd.ws.IdentificationData;
import com.rsa.csd.ws.StatusHeader;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.customer.oob.CustomerOOBChallengeType;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.RSAEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.customer.modules.CommonServicesModule;
import evertec.cibp.core.services.customer.modules.EnrollmentCustomerServiceModule;
import evertec.cibp.core.services.customer.modules.SignOnCustomerServiceModule;
import evertec.cibp.core.services.interfaces.CustomerInteractionService;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.DeleteUserRSARequest;
import evertec.cibp.core.services.requests.adaptiveauth.ChallengeOobRequest;
import evertec.cibp.core.services.requests.adaptiveauth.EnrollmentInformationinRSARequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RsaChallengeRequest;
import evertec.cibp.core.services.requests.adaptiveauth.ValidateOobChallengeRequest;
import evertec.cibp.core.services.responses.OobRecoveryCodeStatusResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.RsaQuestionsUpdateResponse;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.ValidationQuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.ChallengeOobResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionSelectionResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ChangeOOBBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

/**
 * UT for Rsa Challenge Form
 * 
 * @author alizano
 * 
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ServletRequestUtils.class, MessageSource.class, StaticMessageSource.class, WebUtils.class,
		CustomerFacade.class, SignOnCustomerServiceModule.class, EnrollmentCustomerServiceModule.class, 
		CommonServicesModule.class, RSAUtils.class, RSAChallengeForm.class })
public class RSAChallengeFormUT extends AbstractModelAndViewTests {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	/** Recovery Code block 60 minutes*/
	private int recCodeBlockLength;

	/** Wait time in messages */
	private int timeInterval;

	private CustomerEntitlement customerEntitlement;
	private CustomerEntitlement rsaEntitlement;
	private CustomerEntitlement alertsEntitlement;

	private static final String OOB_CODE = "oobCode";
	private static final String CODEUSER = "codeUsr";
	private static final String USERAGENT = "user-agent";
	private static final String ANSWER = "answer";
	private static final String CALL_TYPE = "call";
	private static final String SMS_TYPE = "sms";
	private static final String RECOVERY_CODE_USR = "recoveryCodeUsr";
	private static final String FOR_ALT_PHONE = "isAltPhone";
	private static final String DEFAULT_CARRIER = "NOC";
	private static final String ADD_PHONE_NUMBER = "addPhoneNumber";
	private static final String QUESTION_1 = "sq1";
	private static final String ANSWER_1 = "ans1";
	private static final String C_ANSWER_1 = "cans1";
	private static final String QUESTION_2 = "sq2";
	private static final String ANSWER_2 = "ans2";
	private static final String C_ANSWER_2 = "cans2";
	private static final String QUESTION_3 = "sq3";
	private static final String ANSWER_3 = "ans3";
	private static final String C_ANSWER_3 = "cans3";
	private static final String TYPEDEVICE = "typeDevice";
	private static final String COOKIE_PMDATA = "PMData";
	private static final String QUESTION_TEXT = "Pregunta capsiosa";
	private static final String QUESTION_TEXT_2 = "Pregunta capsiosa 2";
	private static final String QUESTION_TEXT_3 = "Pregunta capsiosa 3";
	private static final String EMAIL_USER = "retailonlinebanking@evertecinc.com";
	private static final int _4_0_9 = 409;
	private static final int _2_0_0 = 200;


	@Mock
	private CustomerInteractionEvent customerInteractionEvent;
	@Mock
	private CustomerInteractionService customerInteractionService;
	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private UserSession userSession;

	@Mock
	private SMSProfile smsprofile;

	@Mock
	private DeviceRequest deviceRequest;

	@Mock
	private
	CustomerFacade customerFacade;

	@Mock
	private ChallengeOobResponse challengeOobResponse;

	@Mock
	private MessageSource messageSource;

	@Mock
	private CustomerProfileService customerProfileService;

	@Mock
	private OobRecoveryCodeStatusResponse oobRecoveryCodeStatusResponse;

	@Mock
	private List<ASK_FOR> tasks;

	@InjectMocks
	@Spy
	private final RSAChallengeForm controller = new RSAChallengeForm();

	@Mock
    QuestionResponse personalQuestion;

	@Mock
	AnalyzeResponse anResp;

	@Mock
	DeviceResult deviceResult;

	@Mock
	DeviceData deviceData;

	@Mock
	IdentificationData idenData;

	@Mock
	ValidationQuestionResponse validateQuestion;

	@Mock
	RsaQuestionsUpdateResponse updateRSAresponse;

	@Mock
    QuestionSelectionResponse questionResponse;

	@Mock
	private CustomerEntitlement rsaEnt;

    @Mock
    Cookie cookie;
    @Mock
    StatusHeader header;

    @Mock
    private CommonServicesModule commonServicesModule;

    @Mock
    private SignOnCustomerServiceModule signOnCustomerServiceModule;

    @Mock
    private EnrollmentCustomerServiceModule enrollmentCustomerServiceModule;

    @Before
    public void setUp() throws Exception {

        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(CustomerFacade.class);
        PowerMockito.mockStatic(RSAUtils.class);
        PowerMockito.mockStatic(MessageSource.class);
        PowerMockito.mockStatic(ServletRequestUtils.class);

        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        customerEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);
        alertsEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        rsaEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);
        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"4554654654\"/>\r\n" + "</properties>");
        alertsEntitlement.setAttributeData("<properties>\r\n"
                + "  <property name=\"ALERT_3\" value=\"QUESTIONS_EDIT_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_2\" value=\"PAYMENTS_CANCEL_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_1\" value=\"TRANSFERS_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_10\" value=\"NEW_FINGERPRINT_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_COUNT\" value=\"18\"/>\r\n"
                + "  <property name=\"ALERT_9\" value=\"PAYMENTS_MODIFY_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_8\" value=\"CHANGE_PASS_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_7\" value=\"EMAIL_EDIT_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_6\" value=\"PAYMENTS_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_5\" value=\"TRANSFERS_MODIFY_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_4\" value=\"TRANSFERS_CANCEL_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_11\" value=\"ACCESS_BLOCK_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_12\" value=\"QUESTIONS_BLOCK_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_13\" value=\"PHONE_EDIT_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_14\" value=\"ADRESS_EDIT_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_15\" value=\"MOBILE_PHONE_EDIT_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_16\" value=\"NEW_DEVICE_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_17\" value=\"QUESTIONS_BLOCK_SMS\"/>\r\n"
                + "  <property name=\"ALERT_18\" value=\"QUESTIONS_BLOCK_PUSH\"/>\r\n"
                + "  <property name=\"ALERT_19\" value=\"QUESTIONS_EDIT_SMS\"/>\r\n"
                + "  <property name=\"ALERT_20\" value=\"QUESTIONS_EDIT_PUSH\"/>\r\n"
                + "  </properties>");
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING))
                .thenReturn(customerEntitlement);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.RSA)).thenReturn(rsaEntitlement);
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        when(userSession.getPm_fp()).thenReturn("ABCDEFG");
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn("HIJKLM");
        when(userSession.getUsername()).thenReturn("milecita29");
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBEMAIL);
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctns.add(ctn);
        when(smsprofile.getCtns()).thenReturn(ctns);

        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(),
                userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);
        when(customerService.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);
        when(customerService.getEnrollmentService()).thenReturn(enrollmentCustomerServiceModule);
        when(customerService.getEnrollmentService().getCommonService()).thenReturn(commonServicesModule);

    }

    @Test
    public void whenRsaGetInfo_GivenCustomerProfileNull_ThenReturnModelAndViewWithResultFalse() throws Exception {

        when(userSession.getCustomerProfile()).thenReturn(null);
        ModelAndView modelAndView = controller.rsaGetInfo(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", false);
    }

    @Test
    public void whenRsaGetInfo_GivenCustomerProfileNotNull_ThenReturnModelAndViewWithResultTrue() throws Exception {

        ModelAndView modelAndView = controller.rsaGetInfo(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRsaGetInfo_GivenAltPhoneNull_ThenReturnenrollSMSTrue() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  </properties>");

        ModelAndView modelAndView = controller.rsaGetInfo(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "EnrollSMS", true);
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRsaGetInfo_GivenAltPhoneBlank_ThenReturnenrollSMSTrue() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);

        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"\"/>\r\n" + "</properties>");

        ModelAndView modelAndView = controller.rsaGetInfo(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "EnrollSMS", true);
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRsaGetInfo_GivenAltPhoneN_ThenReturnenrollSMSTrue() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"N\"/>\r\n" + "</properties>");

        ModelAndView modelAndView = controller.rsaGetInfo(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "EnrollSMS", true);
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRsaGetInfo_GivenSmsProfileNull_ThenReturnenrollSMSFalse() throws Exception {

        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(null);
        ModelAndView modelAndView = controller.rsaGetInfo(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "EnrollSMS", false);
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRsaGetInfo_GivenRSAEntitlementNull_ThenReturnenrollSMSFalse() throws Exception {

        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.RSA)).thenReturn(null);
        ModelAndView modelAndView = controller.rsaGetInfo(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "EnrollSMS", true);
        assertModelAttributeValue(modelAndView, "result", false);
    }

    @Test
    public void whenRsaGetInfo_GivenMobileEntitlementNull_ThenReturnenrollSMSFalse() throws Exception {

        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(null);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.RSA)).thenReturn(rsaEntitlement);
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(null);
        ModelAndView modelAndView = controller.rsaGetInfo(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "EnrollSMS", false);
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRsaGetInfo_GivenAltProviderBlank_ThenReturnenrollSMSTrue() throws Exception {
        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        ModelAndView modelAndView = controller.rsaGetInfo(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "EnrollSMS", true);
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRsaGetInfo_GivenAltProviderNull_ThenReturnenrollSMSTrue() throws Exception {
        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        ModelAndView modelAndView = controller.rsaGetInfo(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "EnrollSMS", true);
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRsaGetInfo_GivenAltProviderN_ThenReturnenrollSMSTrue() throws Exception {
        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"N\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        ModelAndView modelAndView = controller.rsaGetInfo(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "EnrollSMS", true);
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRsaGetInfo_GivenFrecuencyNULL_ThenReturnFrecuencyFalse() throws Exception {
        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        ModelAndView modelAndView = controller.rsaGetInfo(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "frecuencyOOB", "FALSE");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRsaGetInfo_GivenFrecuencyBlank_ThenReturnFrecuencyFalse() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        ModelAndView modelAndView = controller.rsaGetInfo(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "frecuencyOOB", "FALSE");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRsaGetInfo_GivenFrecuency_ThenReturnFrecuencyValue() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"true\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        ModelAndView modelAndView = controller.rsaGetInfo(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "frecuencyOOB", "true");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRsaGetInfo_GivenchallengeTypeNull_ThenReturnChallengeMethod() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class)))
                .thenReturn(null);
        ModelAndView modelAndView = controller.rsaGetInfo(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "challengeMethod", CustomerOOBChallengeType.OOBSMS.toString());
    }

    @Test
    public void whenRsaGetInfo_GivenchallengeTypeOOBPHONE_ThenReturnChallengeMethod() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
        ModelAndView modelAndView = controller.rsaGetInfo(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "challengeMethod", CustomerOOBChallengeType.OOBPHONE.toString());
    }

    @Test
    public void whenRsaGetInfo_GivenDeviceRequestNull_ThenenrollSMSTrue()  throws Exception {

        when(RSAUtils.getDeviceRequest(any(String.class),
                any(String.class), any(MockHttpServletRequest.class))).thenReturn(deviceRequest);
        when(deviceRequest.getDeviceTokenCookie()).thenReturn(null);
        ModelAndView modelAndView = controller.rsaGetInfo(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "EnrollSMS", true);
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenInitRsa_GivenCodeNull_ThenReturnModel() throws Exception {

        Map<Object,Object> informationQuestion = new HashMap <Object,Object>();
        informationQuestion.put("1",QUESTION_TEXT);
        when(userSession.getInformationQuestion()).thenReturn(informationQuestion);
        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.USER_DEFINED);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenInitRsa_GivenCodeBlank_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("");
        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.USER_DEFINED);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenInitRsa_GivenCode_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.USER_DEFINED);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }


    @Test
    public void whenInitRsa_GivenCall_RsaDontResponse_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("993344");
        request.setParameter(CODEUSER, "993344");
        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isLoginCompleteSuccess()).thenReturn(false);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class)))
                .thenReturn(null);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenInitRsa_GivenCall_Rsa_Blocked_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isLoginCompleteSuccess()).thenReturn(true);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class)))
                .thenReturn(challengeOobResponse);

        when(challengeOobResponse.getResultStatus()).thenReturn(ResultStatus.RSA_BLOCKED);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenInitRsa_GivenCall_Rsa_Unabled_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isLoginCompleteSuccess()).thenReturn(true);

        final Locale language = RequestContextUtils.getLocale(request);
        when(messageSource.getMessage("rsa.error.message.call.unable", null, language)).thenReturn("myErrorMessage");
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class)))
                .thenReturn(challengeOobResponse);
        when(challengeOobResponse.getResultStatus()).thenReturn(ResultStatus.RSA_UNABLE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenInitRsa_GivenCall_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
        request.addHeader(USERAGENT, "iPhone");
        userSession.setLoginCompleteSuccess(false);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class)))
                .thenReturn(challengeOobResponse);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenInitRsa_GivenSMS_RsaDontResponse_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("993344");
        request.setParameter(CODEUSER, "993344");
        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.USER_DEFINED);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isLoginCompleteSuccess()).thenReturn(false);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class)))
                .thenReturn(null);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenInitRsa_GivenSMS_Rsa_Blocked_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.USER_DEFINED);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isLoginCompleteSuccess()).thenReturn(true);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class)))
                .thenReturn(challengeOobResponse);

        when(challengeOobResponse.getResultStatus()).thenReturn(ResultStatus.RSA_BLOCKED);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenInitRsa_GivenSMS_Rsa_Unabled_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.USER_DEFINED);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isLoginCompleteSuccess()).thenReturn(true);

        final Locale language = RequestContextUtils.getLocale(request);
        when(messageSource.getMessage("rsa.error.message.call.unable", null, language)).thenReturn("myErrorMessage");
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class)))
                .thenReturn(challengeOobResponse);
        when(challengeOobResponse.getResultStatus()).thenReturn(ResultStatus.RSA_UNABLE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenInitRsa_GivenSMS_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.USER_DEFINED);
        request.addHeader(USERAGENT, "iPhone");
        userSession.setLoginCompleteSuccess(false);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class)))
                .thenReturn(challengeOobResponse);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenInitRsa_GivenCall_Rsa_GivenCall_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.addHeader(USERAGENT, "iPhone");
        request.setParameter(CALL_TYPE,CALL_TYPE);

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
        when(userSession.isLoginCompleteSuccess()).thenReturn(true);
        final Locale language = RequestContextUtils.getLocale(request);
        when(messageSource.getMessage("rsa.error.message.call.unable", null, language)).thenReturn("myErrorMessage");
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class)))
                .thenReturn(challengeOobResponse);
        when(challengeOobResponse.getResultStatus()).thenReturn(ResultStatus.RSA_UNABLE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenInitRsa_RsaGivenQuestion_ThenReturnModel() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.QUESTION);
        request.addHeader(USERAGENT, "iPhone");
        userSession.setLoginCompleteSuccess(true);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        PowerMockito.whenNew(QuestionResponse.class).withAnyArguments().thenReturn(personalQuestion);
        personalQuestion.setStatusResult(ResultStatus.SUCCESS);

        when(customerService.rsaAnalyze(any(RSAAnalyzeRequest.class))).thenReturn(anResp);
        when(anResp.getStatusHeader()).thenReturn(header);
        when(header.getStatusCode()).thenReturn(_2_0_0);
        when(anResp.getDeviceResult()).thenReturn(deviceResult);
        when(anResp.getIdentificationData()).thenReturn(idenData);
        when(idenData.getSessionId()).thenReturn("dummySession");
        when(idenData.getTransactionId()).thenReturn("dummyTrans");
        when(deviceData.getDeviceTokenCookie()).thenReturn("dummy Cookie1");
        when(deviceData.getDeviceTokenFSO()).thenReturn("dummy FSO 1");
        when(deviceResult.getDeviceData()).thenReturn(deviceData);
        when(customerProfile.getEmail()).thenReturn(EMAIL_USER);
        when(customerService.getSignOnCustomerServiceModule().getRSAPersonalQuestion(any(RSAActionRequest.class))).thenReturn(personalQuestion);
        when(personalQuestion.getDeviceRequest()).thenReturn(deviceRequest);
        when(deviceRequest.getDeviceTokenCookie()).thenReturn("dummy Cookie 2");
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        Map<Object,Object> informationQuestion = new HashMap <Object,Object>();
        informationQuestion.put("1",QUESTION_TEXT);
        when(userSession.getInformationQuestion()).thenReturn(informationQuestion);
        when(personalQuestion.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test(expected = ModelAndViewDefiningException.class)
    public void whenInitRsa_GivenQuestion_RsaGivenResultError_ThenReturnModel() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.QUESTION);
        request.addHeader(USERAGENT, "iPhone");
        userSession.setLoginCompleteSuccess(true);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);

        Map<Object,Object> informationQuestion = new HashMap <Object,Object>();
        informationQuestion.put("1",QUESTION_TEXT);
        when(userSession.getInformationQuestion()).thenReturn(informationQuestion);
        personalQuestion.setStatusResult(ResultStatus.ERROR);
        PowerMockito.whenNew(QuestionResponse.class).withAnyArguments().thenReturn(personalQuestion);


        doReturn(ResultStatus.ERROR).when(personalQuestion).getStatusResult();
        when(customerService.rsaAnalyze(any(RSAAnalyzeRequest.class))).thenReturn(anResp);
        when(anResp.getStatusHeader()).thenReturn(header);
        when(header.getStatusCode()).thenReturn(_4_0_9);
        when(personalQuestion.getStatusResult()).thenReturn(ResultStatus.ERROR);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test(expected = ModelAndViewDefiningException.class)
    public void whenInitRsa_GivenQuestion_RsaGivenResultBlocked_ThenReturnModel() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.QUESTION);
        request.addHeader(USERAGENT, "iPhone");
        userSession.setLoginCompleteSuccess(true);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);

        Map<Object,Object> informationQuestion = new HashMap <Object,Object>();
        informationQuestion.put("1",QUESTION_TEXT);
        when(userSession.getInformationQuestion()).thenReturn(informationQuestion);
        PowerMockito.whenNew(QuestionResponse.class).withAnyArguments().thenReturn(personalQuestion);
        personalQuestion.setStatusResult(ResultStatus.BLOCKED);

        when(customerService.rsaAnalyze(any(RSAAnalyzeRequest.class))).thenReturn(anResp);
        when(anResp.getStatusHeader()).thenReturn(header);
        when(header.getStatusCode()).thenReturn(_2_0_0);
        when(anResp.getDeviceResult()).thenReturn(deviceResult);
        when(anResp.getIdentificationData()).thenReturn(idenData);
        when(idenData.getSessionId()).thenReturn("dummySession");
        when(idenData.getTransactionId()).thenReturn("dummyTrans");
        when(deviceData.getDeviceTokenCookie()).thenReturn("dummy Cookie1");
        when(deviceData.getDeviceTokenFSO()).thenReturn("dummy FSO 1");
        when(deviceResult.getDeviceData()).thenReturn(deviceData);
        when(customerProfile.getEmail()).thenReturn(EMAIL_USER);
        when(customerService.getSignOnCustomerServiceModule().getRSAPersonalQuestion(any(RSAActionRequest.class))).thenReturn(personalQuestion);
        when(personalQuestion.getDeviceRequest()).thenReturn(deviceRequest);
        when(deviceRequest.getDeviceTokenCookie()).thenReturn("dummy Cookie 2");
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);

        doReturn(ResultStatus.BLOCKED).when(personalQuestion).getStatusResult();
        //when(personalQuestion.getStatusResult()).thenReturn(ResultStatus.ERROR);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenInitRsa_GivenCodeBlank_RsaGivenQuestion_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("");

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.QUESTION);
        Map<Object,Object> informationQuestion = new HashMap <Object,Object>();
        informationQuestion.put("1",QUESTION_TEXT);
        when(userSession.getInformationQuestion()).thenReturn(informationQuestion);
    }

    public void whenInitRsa_parametereNewCode_isN_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(SMS_TYPE,SMS_TYPE);
        when(ServletRequestUtils.getStringParameter(request, SMS_TYPE)).thenReturn(SMS_TYPE);
        when(ServletRequestUtils.getStringParameter(request, "newCode")).thenReturn("N");
        request.addHeader(USERAGENT, "iPhone");
        RsaChallengeRequest rsaChallengeRequest = new RsaChallengeRequest(userSession.getUsername(),deviceRequest);
        when(userSession.isLoginCompleteSuccess()).thenReturn(true);

        final Locale language = RequestContextUtils.getLocale(request);
        when(messageSource.getMessage("rsa.error.message.call.unable", null, language)).thenReturn("myErrorMessage");

        when(customerService.rsaGetChallengeMethod(rsaChallengeRequest)).thenReturn(CredentialType.OOBPHONE);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class)))
                .thenReturn(challengeOobResponse);
        when(challengeOobResponse.getResultStatus()).thenReturn(ResultStatus.RSA_UNABLE);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenInitRsa_ParametereNewCodeIsEmpty_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(SMS_TYPE,SMS_TYPE);
        when(ServletRequestUtils.getStringParameter(request, SMS_TYPE)).thenReturn(SMS_TYPE);
        when(ServletRequestUtils.getStringParameter(request, "newCode")).thenReturn("");
        request.addHeader(USERAGENT, "iPhone");
//		RsaChallengeRequest rsaChallengeRequest = new RsaChallengeRequest(userSession.getUsername(),deviceRequest);
        when(userSession.isLoginCompleteSuccess()).thenReturn(true);

        final Locale language = RequestContextUtils.getLocale(request);
        when(messageSource.getMessage("rsa.error.message.call.unable", null, language)).thenReturn("myErrorMessage");

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class)))
                .thenReturn(challengeOobResponse);
        when(challengeOobResponse.getResultStatus()).thenReturn(ResultStatus.RSA_UNABLE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenSendNewCode_GivenCode_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        ModelAndView modelAndView = controller.sendNewCode(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenSendNewCode_GivenCodeAltPhone_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");

        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("7873708980");

        ModelAndView modelAndView = controller.sendNewCode(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenSendNewCode_GivenSMSProfileNull_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("");
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(any(CustomerEntitlement.class));
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(null);
        when(smsprofile.getSmsProfileId()).thenReturn(null);
        when(smsprofile.getCtns()).thenReturn(null);
        ModelAndView modelAndView = controller.sendNewCode(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenSendNewCode_GivenCtnsEmpty_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("");
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(any(CustomerEntitlement.class));
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(null);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        when(smsprofile.getCtns()).thenReturn(ctns);
        ModelAndView modelAndView = controller.sendNewCode(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenSendNewCode_GivenCtnsAndProfileIdEmpty_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("");
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(any(CustomerEntitlement.class));
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(null);
        when(smsprofile.getSmsProfileId()).thenReturn("");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        when(smsprofile.getCtns()).thenReturn(ctns);
        ModelAndView modelAndView = controller.sendNewCode(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenSendNewCode_GivenCtnsNull_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("");
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(any(CustomerEntitlement.class));
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(null);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        when(smsprofile.getCtns()).thenReturn(null);
        ModelAndView modelAndView = controller.sendNewCode(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenSendNewCode_GivenSMSProfileEmpty_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("");
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(any(CustomerEntitlement.class));
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctn.setId("12345");
        ctn.setProvider("asdfvb");
        ctns.add(ctn);
        smsprofile.setCtns(ctns);
        when(smsprofile.getCtns()).thenReturn(ctns);
        ModelAndView modelAndView = controller.sendNewCode(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenSendNewCode_GivenSMSProfile_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");

        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("");
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(any(CustomerEntitlement.class));
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctns.add(ctn);
        when(smsprofile.getCtns()).thenReturn(ctns);

        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class))).thenReturn(challengeOobResponse);


        ModelAndView modelAndView = controller.sendNewCode(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    public void whenRequestNewCall_GivenCode_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");

        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("7873708980");

        ModelAndView modelAndView = controller.requestNewCall(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRequestNewCall_GivenSMSProfileNull_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("");
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(any(CustomerEntitlement.class));
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(null);
        when(smsprofile.getSmsProfileId()).thenReturn(null);
        when(smsprofile.getCtns()).thenReturn(null);
        ModelAndView modelAndView = controller.requestNewCall(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRequestNewCall_GivenCtnsEmpty_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("");
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(any(CustomerEntitlement.class));
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(null);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        when(smsprofile.getCtns()).thenReturn(ctns);
        ModelAndView modelAndView = controller.requestNewCall(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRequestNewCall_GivenCtnsAndProfileIdEmpty_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("");
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(any(CustomerEntitlement.class));
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(null);
        when(smsprofile.getSmsProfileId()).thenReturn("");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        when(smsprofile.getCtns()).thenReturn(ctns);
        ModelAndView modelAndView = controller.requestNewCall(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRequestNewCall_GivenCtnsNull_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("");
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(any(CustomerEntitlement.class));
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(null);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        when(smsprofile.getCtns()).thenReturn(null);
        ModelAndView modelAndView = controller.requestNewCall(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRequestNewCall_GivenSMSProfileEmpty_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("");
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(any(CustomerEntitlement.class));
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctn.setId("12345");
        ctn.setProvider("asdfvb");
        ctns.add(ctn);
        smsprofile.setCtns(ctns);
        when(smsprofile.getCtns()).thenReturn(ctns);
        ModelAndView modelAndView = controller.requestNewCall(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenRequestNewCall_GivenSMSProfile_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");

        when(ServletRequestUtils.getStringParameter(request, FOR_ALT_PHONE)).thenReturn("");
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(any(CustomerEntitlement.class));
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctns.add(ctn);
        when(smsprofile.getCtns()).thenReturn(ctns);

        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class))).thenReturn(challengeOobResponse);


        ModelAndView modelAndView = controller.requestNewCall(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenVerifyRsaCode_GivenCallNull_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE, CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isOobLogin()).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.TRUE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(null);
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRsaCode_GivenCodeUser_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE, CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "");
        when(userSession.isOobLogin()).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.TRUE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_ALLOW);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
      //  doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBSuccessInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRsaCode_GivenCodeUserRsaAllow222_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE, CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(userSession.isOobLogin()).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.TRUE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
       // doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBSuccessInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_ALLOW);

        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenVerifyRsaCode_GivenCodeUserRsaAllow_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE, CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_ALLOW);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
       // doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBSuccessInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenVerifyRsaCode_GivenCodeUserRsaUnable_FailSignon_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isOobLogin()).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.TRUE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_UNABLE);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);

        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
      //  doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBSuccessInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRsaCode_GivenCodeUserRsaUnable_NoLogin_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isOobLogin()).thenReturn(Boolean.FALSE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.TRUE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_UNABLE);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
       // doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBSuccessInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;

        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRsaCode_GivenCodeUserRsaUnable_FlagFalse_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isOobLogin()).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.FALSE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_UNABLE);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
       // doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBSuccessInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        assertViewName(modelAndView, "rsaForm");
    }




    @Test
    public void whenVerifyRsaCode_GivenCodeUserRsaExpired_FailSignon_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isOobLogin()).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.TRUE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_EXPIRED);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
       // doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBSuccessInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        assertViewName(modelAndView, "rsaForm");
//		assertModelAttributeValue(modelAndView, "result", true);
    }


    @Test
    public void whenVerifyRsaCode_GivenCodeUserRsaExpired_NoLogin_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isOobLogin()).thenReturn(Boolean.FALSE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.TRUE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_EXPIRED);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
       // doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBSuccessInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRsaCode_GivenCodeUserExpired_FlagFalse_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isOobLogin()).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.FALSE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_EXPIRED);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
      //  doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBSuccessInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        assertViewName(modelAndView, "rsaForm");
    }



    @Test
    public void whenVerifyRsaCode_GivenCodeUserRsaBlocked_FailSignon_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isOobLogin()).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.TRUE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_BLOCKED);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
       // doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBFailInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);
        assertViewName(modelAndView, "rsaForm");
    }


    @Test
    public void whenVerifyRsaCode_GivenCodeUserRsaBlocked_NoLogin_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isOobLogin()).thenReturn(Boolean.FALSE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.TRUE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_BLOCKED);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
       // doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBFailInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRsaCode_GivenCodeUserBlocked_FlagFalse_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isOobLogin()).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.FALSE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_BLOCKED);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
       // doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBFailInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRsaCode_GivenCodeUserRsaFailed_FielSignon_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPad");
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPAD);
        when(userSession.isOobLogin()).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.TRUE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_FAILED);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
      //  doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBFailInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRsaCode_GivenCodeUserRsaFailed_NoLogin_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "Android");
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.ANDROID);
        when(userSession.isOobLogin()).thenReturn(Boolean.FALSE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.TRUE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_FAILED);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
       // doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBFailInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRsaCode_GivenCodeUserFailed_FlagFalse_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isOobLogin()).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.FALSE);
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        when(customerService.rsaValidateOobChallenge(any(ValidateOobChallengeRequest.class))).thenReturn(ResultStatus.RSA_FAILED);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
      //  doThrow(new RuntimeException()).when(customerInteractionService).logChallengeOOBFailInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        assertViewName(modelAndView, "rsaForm");
    }



    @Test
    public void whenverifyRsaCode_GivenCall_ThenReturnModel() throws Exception {
        when(WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        userSession.setLoginCompleteSuccess(false);
        ModelAndView modelAndView = controller.verifyRsaCode(request, response);

        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(),
                userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);

        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenNewRecoveryCodeOOB_GivenNullEntitlement_ThenReturnModel() throws Exception {

        //Assemble
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(null);

        //Act
        ModelAndView modelAndView = controller.newRecoveryCodeOOB(request, response);

        //Assert
        assertThat(modelAndView.getModelMap().get("recoveryCode"), nullValue());
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenNewRecoveryCodeOOB_GivenValidEntitlement_ThenReturnModel() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"true\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        //Assemble
        when(RSAUtils.generateOOBCode()).thenReturn("1234567890123456");
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(rsaEntitlement);
        when(customerService.sendOobUpdateEmail(customerProfile,
                userSession.getPreferredLanguage(), ChangeOOBBean.ACTION.GENERATE_RECOVERY_CODE.name())).thenReturn(false);
        when(customerService.modifyCustomerRSAEntitlement(any())).thenReturn(customerProfile);

        Boolean alwaysChallenge = (rsaEntitlement.getString("ALWAYSCHALLENGE").equalsIgnoreCase("FALSE")) ? false : true;

        //Act
        ModelAndView modelAndView = controller.newRecoveryCodeOOB(request, response);

        //Assert
        verify(customerService).modifyCustomerRSAEntitlement(any());
        assertTrue(alwaysChallenge);
        assertViewName(modelAndView, "rsaForm");
        assertViewName(modelAndView, "rsaForm");
    }


    @Test
    public void whenNewRecoveryCodeOOB_GivenSendEmail_ThenReturnModel() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"true\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        //Assemble
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(rsaEntitlement);
        when(customerService.sendOobUpdateEmail(customerProfile,
                userSession.getPreferredLanguage(), ChangeOOBBean.ACTION.GENERATE_RECOVERY_CODE.name())).thenReturn(true);
        when(customerService.modifyCustomerRSAEntitlement(any())).thenReturn(customerProfile);


        Boolean alwaysChallenge = (rsaEntitlement.getString("ALWAYSCHALLENGE").equalsIgnoreCase("FALSE")) ? false : true;

        //Act
        ModelAndView modelAndView = controller.newRecoveryCodeOOB(request, response);

        //Assert
        verify(customerService).modifyCustomerRSAEntitlement(any());
        assertTrue(alwaysChallenge);
        assertModelAttributeValue(modelAndView, "result", "success");
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenNewRecoveryCodeOOB_GivenNoSendEmail_ThenReturnModel() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        //Assemble
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getUsername()).thenReturn("milecita29");
        when(customerProfile.getEmail()).thenReturn("ut@evertecinc.com");
        when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(rsaEntitlement);
        when(customerService.sendOobUpdateEmail(customerProfile,
                userSession.getPreferredLanguage(), ChangeOOBBean.ACTION.GENERATE_RECOVERY_CODE.name())).thenReturn(false);
        when(customerService.modifyCustomerRSAEntitlement(any())).thenReturn(customerProfile);

        Boolean alwaysChallenge = (rsaEntitlement.getString("ALWAYSCHALLENGE").equalsIgnoreCase("FALSE")) ? false : true;

        //Act
        ModelAndView modelAndView = controller.newRecoveryCodeOOB(request, response);

        //Assert
        verify(customerService).modifyCustomerRSAEntitlement(any());
        assertFalse(alwaysChallenge);
        assertModelAttributeValue(modelAndView, "result", "success");
        assertViewName(modelAndView, "rsaForm");
    }


    @Test
    public void whenUpdateRsaMobileProfile_GivenNullEntitlement_ThenReturnModel() throws Exception {

        //Assemble
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(null);

        //Act
        ModelAndView modelAndView = controller.updateRsaMobileProfile(request, response);

        //Assert
        assertThat(modelAndView.getModelMap().get("primaryPhone"), nullValue());
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenUpdateRsaMobileProfile_GivenValidPhoneCode_ThenReturnModel() throws Exception {
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING))
                .thenReturn(customerEntitlement);

        when( WebUtils.getSessionAttribute(request, "smsGeneratedCode")).thenReturn("6644");
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(customerEntitlement);
        request.setParameter("codeNumberInp", "6644");
        ModelAndView modelAndView = controller.updateRsaMobileProfile(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", "success");
    }

    @Test
    public void whenUpdateRsaMobileProfile_GivenInvalidPhoneCode_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, "smsGeneratedCode")).thenReturn("6644");
        request.setParameter("codeNumberInp", "7777");
        ModelAndView modelAndView = controller.updateRsaMobileProfile(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, "result", "fail");
    }

    @Test
    public void whenUpdateRsaMobileProfile_GivenValidEntitlement_ThenReturnModel() throws Exception {

        //Assemble
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING))
                .thenReturn(customerEntitlement);
        when( WebUtils.getSessionAttribute(request, "smsPhonereceived")).thenReturn("7873708980");
        when( WebUtils.getSessionAttribute(request, "smsProvider")).thenReturn("PR7");
        when( WebUtils.getSessionAttribute(request, "smsGeneratedCode")).thenReturn("6644");
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(customerEntitlement);
        request.setParameter("codeNumberInp", "6644");

        //Act
        ModelAndView modelAndView = controller.updateRsaMobileProfile(request, response);

        //Assert

        assertModelAttributeValue(modelAndView, "result", "success");
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRecovery_LessEntitlementType_ThenReturnModel() throws Exception {
        request.addHeader(USERAGENT, "");
        //Act
        ModelAndView modelAndView = controller.verifyRecoveryCode(request, response);
        assertViewName(modelAndView, "rsaForm");
    }


    @Test
    public void whenVerifyRecoveryCode_FailFlagEnabledInLogin_ThenReturnModel() throws Exception {
        request.addHeader(USERAGENT, "iPhone");
        //Assemble
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getOOBRecoveryCodeStatus(customerProfile, getRecCodeBlockLength())).thenReturn(null);

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.TRUE);
        when(userSession.isOobLogin()).thenReturn(Boolean.TRUE);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);

        //Act
        ModelAndView modelAndView = controller.verifyRecoveryCode(request, response);

        //Assert
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRecoveryCode_FailFlagDisabledInLogin_ThenReturnModel() throws Exception {
        request.addHeader(USERAGENT, "iPhone");
        //Assemble
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getOOBRecoveryCodeStatus(customerProfile, getRecCodeBlockLength())).thenReturn(null);

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.FALSE);
        when(userSession.isOobLogin()).thenReturn(Boolean.TRUE);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);

        //Act
        ModelAndView modelAndView = controller.verifyRecoveryCode(request, response);

        //Assert
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRecoveryCode_FailFlagEnabledNoInLogin_ThenReturnModel() throws Exception {
        request.addHeader(USERAGENT, "iPhone");
        //Assemble
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getOOBRecoveryCodeStatus(customerProfile, getRecCodeBlockLength())).thenReturn(null);

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1754)).thenReturn(Boolean.TRUE);
        when(userSession.isOobLogin()).thenReturn(Boolean.FALSE);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);

        //Act
        ModelAndView modelAndView = controller.verifyRecoveryCode(request, response);

        //Assert
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRecoveryCode_Block_ThenReturnModel() throws Exception {
        request.addHeader(USERAGENT, "iPhone");
        //Assemble
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getOOBRecoveryCodeStatus(customerProfile, getRecCodeBlockLength())).thenReturn(oobRecoveryCodeStatusResponse);
        when(oobRecoveryCodeStatusResponse.isBlocked()).thenReturn(true);

        //Act
        ModelAndView modelAndView = controller.verifyRecoveryCode(request, response);

        //Assert
        assertTrue(oobRecoveryCodeStatusResponse.isBlocked());
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRecoveryCode_NoRSA_ThenReturnModel() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n"
                + "  <property name=\"CODE_SALT\" value=\"N\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"N\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"N\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"N\"/>\r\n" + "</properties>");

        request.addHeader(USERAGENT, "iPhone");
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getOOBRecoveryCodeStatus(customerProfile, getRecCodeBlockLength())).thenReturn(oobRecoveryCodeStatusResponse);
        when(oobRecoveryCodeStatusResponse.isBlocked()).thenReturn(false);

        //Act
        ModelAndView modelAndView = controller.verifyRecoveryCode(request, response);

        //Assert
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRecoveryCode_FailRSA_ThenReturnModel() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n"
                + "  <property name=\"CODE_SALT\" value=\"N\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"N\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"S\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"N\"/>\r\n" + "</properties>");

        request.addHeader(USERAGENT, "iPhone");
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getOOBRecoveryCodeStatus(customerProfile, getRecCodeBlockLength())).thenReturn(oobRecoveryCodeStatusResponse);
        when(oobRecoveryCodeStatusResponse.isBlocked()).thenReturn(false);

        //Act
        ModelAndView modelAndView = controller.verifyRecoveryCode(request, response);

        //Assert
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRecoveryCode_NoDeleteUserRSA_ThenReturnModel() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n"
                + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"9d823f422643d8d96da09b3abe6ac6374a76f34ac0b636c9a50c17f0376bb2c3\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        request.addHeader(USERAGENT, "iPhone");
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getOOBRecoveryCodeStatus(customerProfile, getRecCodeBlockLength())).thenReturn(oobRecoveryCodeStatusResponse);
        when(oobRecoveryCodeStatusResponse.isBlocked()).thenReturn(false);
        when(ServletRequestUtils.getStringParameter(request, RECOVERY_CODE_USR)).thenReturn("1270210404");

        when(customerService.getEnrollmentService().getCommonService().deleteUserRSA(any(DeleteUserRSARequest.class))).thenReturn(false);

        //Act
        ModelAndView modelAndView = controller.verifyRecoveryCode(request, response);

        //Assert
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRecoveryCodeOOB_LoginCompleteNoSuccess_ThenReturnModel() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n"
                + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"9d823f422643d8d96da09b3abe6ac6374a76f34ac0b636c9a50c17f0376bb2c3\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        request.addHeader(USERAGENT, "iPhone");
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getOOBRecoveryCodeStatus(customerProfile, getRecCodeBlockLength())).thenReturn(oobRecoveryCodeStatusResponse);
        when(oobRecoveryCodeStatusResponse.isBlocked()).thenReturn(false);
        when(ServletRequestUtils.getStringParameter(request, RECOVERY_CODE_USR)).thenReturn("1270210404");

        when(customerService.getEnrollmentService().getCommonService().deleteUserRSA(any(DeleteUserRSARequest.class))).thenReturn(true);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
        when(CustomerFacade.getInstance()).thenReturn(customerFacade);
        when(userSession.isLoginCompleteSuccess()).thenReturn(true);

        //Act
        ModelAndView modelAndView = controller.verifyRecoveryCode(request, response);

        //Assert
        assertViewName(modelAndView, "rsaForm");
    }





    @Test
    public void whenVerifyRecoveryCodeOOB_MUST_CHANGE_USERNAME_ThenReturnModel() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n"
                + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"9d823f422643d8d96da09b3abe6ac6374a76f34ac0b636c9a50c17f0376bb2c3\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        request.addHeader(USERAGENT, "iPhone");
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getOOBRecoveryCodeStatus(customerProfile, getRecCodeBlockLength())).thenReturn(oobRecoveryCodeStatusResponse);
        when(oobRecoveryCodeStatusResponse.isBlocked()).thenReturn(false);
        when(ServletRequestUtils.getStringParameter(request, RECOVERY_CODE_USR)).thenReturn("1270210404");

        when(customerService.getEnrollmentService().getCommonService().deleteUserRSA(any(DeleteUserRSARequest.class))).thenReturn(true);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
        when(CustomerFacade.getInstance()).thenReturn(customerFacade);
        when(userSession.isLoginCompleteSuccess()).thenReturn(false);
        when(userSession.getTasksList()).thenReturn(tasks);
        when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(true);

        //Act
        ModelAndView modelAndView = controller.verifyRecoveryCode(request, response);

        //Assert
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRecoveryCodeOOB_MUST_CHANGE_PASSWORD_ThenReturnModel() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n"
                + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"9d823f422643d8d96da09b3abe6ac6374a76f34ac0b636c9a50c17f0376bb2c3\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        request.addHeader(USERAGENT, "iPhone");
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getOOBRecoveryCodeStatus(customerProfile, getRecCodeBlockLength())).thenReturn(oobRecoveryCodeStatusResponse);
        when(oobRecoveryCodeStatusResponse.isBlocked()).thenReturn(false);
        when(ServletRequestUtils.getStringParameter(request, RECOVERY_CODE_USR)).thenReturn("1270210404");

        when(customerService.getEnrollmentService().getCommonService().deleteUserRSA(any(DeleteUserRSARequest.class))).thenReturn(true);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
        when(CustomerFacade.getInstance()).thenReturn(customerFacade);
        when(userSession.isLoginCompleteSuccess()).thenReturn(false);
        when(userSession.getTasksList()).thenReturn(tasks);
        when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_PASSWORD)).thenReturn(true);

        //Act
        ModelAndView modelAndView = controller.verifyRecoveryCode(request, response);

        //Assert
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRecoveryCodeOOB_RsaEnroll_ThenReturnModel() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n"
                + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"9d823f422643d8d96da09b3abe6ac6374a76f34ac0b636c9a50c17f0376bb2c3\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        request.addHeader(USERAGENT, "iPhone");
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getOOBRecoveryCodeStatus(customerProfile, getRecCodeBlockLength())).thenReturn(oobRecoveryCodeStatusResponse);
        when(oobRecoveryCodeStatusResponse.isBlocked()).thenReturn(false);
        when(ServletRequestUtils.getStringParameter(request, RECOVERY_CODE_USR)).thenReturn("1270210404");

        when(customerService.getEnrollmentService().getCommonService().deleteUserRSA(any(DeleteUserRSARequest.class))).thenReturn(true);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
        when(CustomerFacade.getInstance()).thenReturn(customerFacade);
        when(userSession.isLoginCompleteSuccess()).thenReturn(false);
        when(userSession.getTasksList()).thenReturn(tasks);
        when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_PASSWORD)).thenReturn(false);
        when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(false);

        //Act
        ModelAndView modelAndView = controller.verifyRecoveryCode(request, response);

        //Assert
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenVerifyRecoveryCodeOOB_RsaEnrollWeb_ThenReturnModel() throws Exception {

        rsaEntitlement.setAttributeData("<properties>\r\n"
                + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"9d823f422643d8d96da09b3abe6ac6374a76f34ac0b636c9a50c17f0376bb2c3\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"7879999987\"/>\r\n" + "</properties>");

        request.addHeader(USERAGENT, "iPhone");
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getOOBRecoveryCodeStatus(customerProfile, getRecCodeBlockLength())).thenReturn(oobRecoveryCodeStatusResponse);
        when(oobRecoveryCodeStatusResponse.isBlocked()).thenReturn(false);
        when(ServletRequestUtils.getStringParameter(request, RECOVERY_CODE_USR)).thenReturn("1270210404");

        when(customerService.getEnrollmentService().getCommonService().deleteUserRSA(any(DeleteUserRSARequest.class))).thenReturn(true);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
        when(CustomerFacade.getInstance()).thenReturn(customerFacade);
        when(userSession.isLoginCompleteSuccess()).thenReturn(false);
        when(userSession.getTasksList()).thenReturn(tasks);
        when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_PASSWORD)).thenReturn(false);
        when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(false);

        //Act
        ModelAndView modelAndView = controller.verifyRecoveryCode(request, response);

        //Assert
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaAltPhone_GivenCodeNull_ThenReturnModel() throws Exception {
        request.setParameter(FOR_ALT_PHONE, "true");
        ModelAndView modelAndView = controller.rsaAltPhone(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaAltPhone_GivenCodeBlank_ThenReturnModel() throws Exception {
        request.setParameter(FOR_ALT_PHONE, "true");
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("");
        ModelAndView modelAndView = controller.rsaAltPhone(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaAltPhone_GivenCode_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        ModelAndView modelAndView = controller.rsaAltPhone(request, response);
        assertViewName(modelAndView, "rsaForm");
    }


    @Test
    public void whenRsaAltPhone_GivenCall_RsaDontResponse_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("993344");
        request.setParameter(CODEUSER, "993344");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isLoginCompleteSuccess()).thenReturn(false);

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class))).thenReturn(null);
        ModelAndView modelAndView = controller.rsaAltPhone(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaAltPhone_GivenCall_Rsa_Blocked_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");

        when(userSession.isLoginCompleteSuccess()).thenReturn(true);

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class)))
                .thenReturn(challengeOobResponse);

        when(challengeOobResponse.getResultStatus()).thenReturn(ResultStatus.RSA_BLOCKED);

        ModelAndView modelAndView = controller.rsaAltPhone(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaAltPhone_GivenCall_Rsa_Unabled_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE,CALL_TYPE);
        when(ServletRequestUtils.getStringParameter(request, CALL_TYPE)).thenReturn(CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.isLoginCompleteSuccess()).thenReturn(true);

        final Locale language = RequestContextUtils.getLocale(request);
        when(messageSource.getMessage("rsa.error.message.call.unable", null, language)).thenReturn("myErrorMessage");

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class))).thenReturn(challengeOobResponse);
        when(challengeOobResponse.getResultStatus()).thenReturn(ResultStatus.RSA_UNABLE);
        ModelAndView modelAndView = controller.rsaAltPhone(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaAltPhone_GivenCall_ThenReturnModel() throws Exception {
        when( WebUtils.getSessionAttribute(request, OOB_CODE)).thenReturn("996644");
        request.setParameter(CODEUSER, "996644");
        request.setParameter(CALL_TYPE, CALL_TYPE);
        request.addHeader(USERAGENT, "iPhone");
        userSession.setLoginCompleteSuccess(false);

        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
        when(customerService.getSignOnCustomerServiceModule().challengeOob(any(ChallengeOobRequest.class))).thenReturn(challengeOobResponse);
        ModelAndView modelAndView = controller.rsaAltPhone(request, response);

        assertViewName(modelAndView, "rsaForm");
    }



    @Test
    public void whenupdateRsaProfile_setTime_ThenReturnModel() throws Exception {
        //Act
        ModelAndView modelAndView = controller.updateRsaProfile(request, response);

        //Assert
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaEnroll_GivenCustomerNotNull_ThenReturnModel() throws Exception {
        request.addHeader(USERAGENT, "iPhone");
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)).thenReturn(Boolean.TRUE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.rsaEnroll(request, response);

        assertModelAttributeValue(modelAndView, "MBSD3686", true);
        assertModelAttributeValue(modelAndView, "defCarrier", DEFAULT_CARRIER);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaEnroll_GivenCustomerNotNull_FlagFalse_ThenReturnModel() throws Exception {
        request.addHeader(USERAGENT, "iPhone");
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)).thenReturn(Boolean.FALSE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.rsaEnroll(request, response);

        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaEnroll_GivenCodeBlank_ThenReturnModel() throws Exception {
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.rsaEnroll(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaEditOOB_GivenCustomerNotNull_ThenReturnModel() throws Exception {
        request.addHeader(USERAGENT, "iPhone");
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)).thenReturn(Boolean.TRUE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.rsaEditOOB(request, response);

        assertModelAttributeValue(modelAndView, "MBSD3686", true);
        assertModelAttributeValue(modelAndView, "defCarrier", DEFAULT_CARRIER);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaEditOOB_GivenCustomerNotNull_FlagFalse_ThenReturnModel() throws Exception {
        request.addHeader(USERAGENT, "iPhone");
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)).thenReturn(Boolean.FALSE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.rsaEditOOB(request, response);

        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaEditOOB_GivenCodeBlank_ThenReturnModel() throws Exception {
        request.addHeader(USERAGENT, "iPhone");
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.rsaEditOOB(request, response);
        assertViewName(modelAndView, "rsaForm");
    }


    @Test
    public void whenNonCodes_BackButton_ThenReturnModel() throws Exception {
        ModelAndView modelAndView = controller.nonCodes(request, response);
        assertViewName(modelAndView, "rsaForm");
    }


    @Test
    public void whenValidateQuestion_GivenSMSProfile_ThenReturnModel() throws Exception {
        customerEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);
        when(customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        when(userSession.isInformationChallenge()).thenReturn(Boolean.TRUE);
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctn.setId("12345");
        ctn.setProvider("asdfvb");
        ctns.add(ctn);
        smsprofile.setCtns(ctns);
        when(smsprofile.getCtns()).thenReturn(ctns);
        ModelAndView modelAndView = controller.validateQuestion(request, response);
        assertViewName(modelAndView, "rsaForm");
        //assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenValidateQuestion_GivenSMSProfileEmpty_ThenReturnModel() throws Exception {
        customerEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);
        when(customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        when(userSession.isInformationChallenge()).thenReturn(Boolean.TRUE);
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(null);
        ModelAndView modelAndView = controller.validateQuestion(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, ADD_PHONE_NUMBER, true);
    }

    @Test
    public void whenValidateQuestion_GivenMobileEntiltementNull_ThenReturnModel() throws Exception {

        when(customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(null);
        when(userSession.isInformationChallenge()).thenReturn(Boolean.TRUE);
        ModelAndView modelAndView = controller.validateQuestion(request, response);
        assertViewName(modelAndView, "rsaForm");
        assertModelAttributeValue(modelAndView, ADD_PHONE_NUMBER, true);
    }

    @Test
    public void whenValidateQuestion_GivenAnswerNull_ThenReturnModel() throws Exception {
        customerEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);
        when(customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        when(userSession.isInformationChallenge()).thenReturn(Boolean.FALSE);
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctn.setId("12345");
        ctn.setProvider("asdfvb");
        ctns.add(ctn);
        smsprofile.setCtns(ctns);
        when(smsprofile.getCtns()).thenReturn(ctns);
        ModelAndView modelAndView = controller.validateQuestion(request, response);
        assertViewName(modelAndView, "rsaForm");
        //assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenValidateQuestion_GivenAnswer_ThenReturnModel() throws Exception {
        request.setParameter(ANSWER, "1");
        customerEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);
        when(customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        when(userSession.isInformationChallenge()).thenReturn(Boolean.FALSE);
        when(ServletRequestUtils.getStringParameter(request, ANSWER)).thenReturn("1");
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(deviceRequest.getUserAgent()).thenReturn("iPhone");
        when(customerService.getSignOnCustomerServiceModule().validateRSAQuestion(any(RSAActionRequest.class))).thenReturn(validateQuestion);
        when(validateQuestion.getRsaSessionId()).thenReturn("DummySessionRsa");
        when(validateQuestion.getRsaTransactionId()).thenReturn("DummyTransactionRsa");
        when(validateQuestion.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctn.setId("12345");
        ctn.setProvider("asdfvb");
        ctns.add(ctn);
        smsprofile.setCtns(ctns);
        when(smsprofile.getCtns()).thenReturn(ctns);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
        doThrow(new RuntimeException()).when(customerInteractionService).logRsaQuestionValidationSuccessInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;

        ModelAndView modelAndView = controller.validateQuestion(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenValidateQuestion_GivenAnswer_ThenRSAReturnBLOCKED_ThenReturnModel() throws Exception {
        request.setParameter(ANSWER, "1");
        customerEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);
        when(customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(alertsEntitlement);
        when(userSession.isInformationChallenge()).thenReturn(Boolean.FALSE);
        when(ServletRequestUtils.getStringParameter(request, ANSWER)).thenReturn("1");
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(deviceRequest.getUserAgent()).thenReturn("iPhone");
        when(customerService.getSignOnCustomerServiceModule().validateRSAQuestion(any(RSAActionRequest.class))).thenReturn(validateQuestion);
        when(validateQuestion.getRsaSessionId()).thenReturn("DummySessionRsa");
        when(validateQuestion.getRsaTransactionId()).thenReturn("DummyTransactionRsa");
        when(validateQuestion.getStatusResult()).thenReturn(ResultStatus.BLOCKED);
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctn.setId("12345");
        ctn.setProvider("asdfvb");
        ctns.add(ctn);
        smsprofile.setCtns(ctns);
        when(smsprofile.getCtns()).thenReturn(ctns);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
        doThrow(new RuntimeException()).when(customerInteractionService).logRsaQuestionValidationFailInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;

        ModelAndView modelAndView = controller.validateQuestion(request, response);
        assertViewName(modelAndView, "rsaForm");
        //assertModelAttributeValue(modelAndView, "result", true);
    }

    @Test
    public void whenValidateQuestion_GivenAnswer_ThenRSAReturnEXCEPTION_ThenReturnModel() throws Exception {
        request.setParameter(ANSWER, "1");
        customerEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);
        when(customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        when(userSession.isInformationChallenge()).thenReturn(Boolean.FALSE);
        when(ServletRequestUtils.getStringParameter(request, ANSWER)).thenReturn("1");
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(deviceRequest.getUserAgent()).thenReturn("iPhone");
        when(customerService.getSignOnCustomerServiceModule().validateRSAQuestion(any(RSAActionRequest.class))).thenReturn(validateQuestion);
        when(validateQuestion.getRsaSessionId()).thenReturn("DummySessionRsa");
        when(validateQuestion.getRsaTransactionId()).thenReturn("DummyTransactionRsa");
        when(validateQuestion.getStatusResult()).thenReturn(ResultStatus.EXCEPTION);
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctn.setId("12345");
        ctn.setProvider("asdfvb");
        ctns.add(ctn);
        smsprofile.setCtns(ctns);
        when(smsprofile.getCtns()).thenReturn(ctns);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
        doThrow(new RuntimeException()).when(customerInteractionService).logRsaQuestionValidationFailInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.validateQuestion(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenValidateQuestion_GivenAnswer_ThenRSAReturnERROR_ThenRenewQuestion_ThenReturnModel() throws Exception {
        request.setParameter(ANSWER, "1");

        customerEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);
        when(customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        when(userSession.isInformationChallenge()).thenReturn(Boolean.FALSE);
        when(ServletRequestUtils.getStringParameter(request, ANSWER)).thenReturn("1");
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(deviceRequest.getUserAgent()).thenReturn("iPhone");
        when(customerService.getSignOnCustomerServiceModule().validateRSAQuestion(any(RSAActionRequest.class))).thenReturn(validateQuestion);
        when(validateQuestion.getRsaSessionId()).thenReturn("DummySessionRsa");
        when(validateQuestion.getRsaTransactionId()).thenReturn("DummyTransactionRsa");
        when(validateQuestion.getStatusResult()).thenReturn(ResultStatus.ERROR);
        when(customerService.getSignOnCustomerServiceModule().getRSAPersonalQuestion(any(RSAActionRequest.class))).thenReturn(personalQuestion);
        when(personalQuestion.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctn.setId("12345");
        ctn.setProvider("asdfvb");
        ctns.add(ctn);
        smsprofile.setCtns(ctns);
        when(smsprofile.getCtns()).thenReturn(ctns);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
        doThrow(new RuntimeException()).when(customerInteractionService).logRsaQuestionValidationFailInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.validateQuestion(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenValidateQuestion_GivenAnswer_ThenRSAReturnERROR_ThenERRORonGetNewQuestion_ThenReturnModel() throws Exception {
        request.setParameter(ANSWER, "1");

        customerEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);
        when(customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        when(userSession.isInformationChallenge()).thenReturn(Boolean.FALSE);
        when(ServletRequestUtils.getStringParameter(request, ANSWER)).thenReturn("1");
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(deviceRequest.getUserAgent()).thenReturn("iPhone");
        when(customerService.getSignOnCustomerServiceModule().validateRSAQuestion(any(RSAActionRequest.class))).thenReturn(validateQuestion);
        when(validateQuestion.getRsaSessionId()).thenReturn("DummySessionRsa");
        when(validateQuestion.getRsaTransactionId()).thenReturn("DummyTransactionRsa");
        when(validateQuestion.getStatusResult()).thenReturn(ResultStatus.ERROR);
        when(customerService.getSignOnCustomerServiceModule().getRSAPersonalQuestion(any(RSAActionRequest.class))).thenReturn(personalQuestion);
        when(personalQuestion.getStatusResult()).thenReturn(ResultStatus.ERROR);
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctn.setId("12345");
        ctn.setProvider("asdfvb");
        ctns.add(ctn);
        smsprofile.setCtns(ctns);
        when(smsprofile.getCtns()).thenReturn(ctns);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
        doThrow(new RuntimeException()).when(customerInteractionService).logRsaQuestionValidationFailInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.validateQuestion(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenValidateQuestion_GivenAnswer_ThenRSAReturnERROR_ThenBLOCKEDonGetNewQuestion_ThenReturnModel() throws Exception {
        request.setParameter(ANSWER, "1");

        customerEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);
        when(customerProfile.getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(alertsEntitlement);
        when(userSession.isInformationChallenge()).thenReturn(Boolean.FALSE);
        when(ServletRequestUtils.getStringParameter(request, ANSWER)).thenReturn("1");
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(deviceRequest.getUserAgent()).thenReturn("iPhone");
        when(customerService.getSignOnCustomerServiceModule().validateRSAQuestion(any(RSAActionRequest.class))).thenReturn(validateQuestion);
        when(validateQuestion.getRsaSessionId()).thenReturn("DummySessionRsa");
        when(validateQuestion.getRsaTransactionId()).thenReturn("DummyTransactionRsa");
        when(validateQuestion.getStatusResult()).thenReturn(ResultStatus.ERROR);
        when(customerService.getSignOnCustomerServiceModule().getRSAPersonalQuestion(any(RSAActionRequest.class))).thenReturn(personalQuestion);
        when(personalQuestion.getStatusResult()).thenReturn(ResultStatus.BLOCKED);
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctn.setId("12345");
        ctn.setProvider("asdfvb");
        ctns.add(ctn);
        smsprofile.setCtns(ctns);
        when(smsprofile.getCtns()).thenReturn(ctns);
        when(userSession.getCustomerService().getInteractionService()).thenReturn(customerInteractionService);
        doThrow(new RuntimeException()).when(customerInteractionService).logRsaQuestionValidationFailInteraction(customerInteractionEvent, InteractionSeverity.INFO) ;
        ModelAndView modelAndView = controller.validateQuestion(request, response);
        assertViewName(modelAndView, "rsaForm");
    }


    @Test
    public void whenrsaChallengeMethod_GivenAnswer_Question_ThenReturnModel() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.QUESTION);
        when(userSession.isInformationChallenge()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(false);
        when(userSession.isOobAuthenticated()).thenReturn(false);
        ModelAndView modelAndView = controller.rsaChallengeMethod(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenrsaChallengeMethod_GivenAnswer_SMS_ThenReturnModel() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.USER_DEFINED);
        when(userSession.getOobEnroll()).thenReturn(true);
        when(userSession.isOobAuthenticated()).thenReturn(true);
        ModelAndView modelAndView = controller.rsaChallengeMethod(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenrsaChallengeMethod_GivenAnswer_SMS_noEnroll_ThenReturnModel() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.USER_DEFINED);
        when(userSession.getOobEnroll()).thenReturn(false);
        when(userSession.isOobAuthenticated()).thenReturn(true);
        ModelAndView modelAndView = controller.rsaChallengeMethod(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenrsaChallengeMethod_GivenAnswer_OOBPHONE_ThenReturnModel() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
        when(userSession.getOobEnroll()).thenReturn(true);
        when(userSession.isOobAuthenticated()).thenReturn(true);
        ModelAndView modelAndView = controller.rsaChallengeMethod(request, response);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3755)).thenReturn(Boolean.FALSE);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenrsaChallengeMethod_GivenAnswer_OOBPHONE_noAuthenticated_ThenReturnModel() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
        when(userSession.getOobEnroll()).thenReturn(true);
        when(userSession.isOobAuthenticated()).thenReturn(false);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3755)).thenReturn(Boolean.TRUE);
        ModelAndView modelAndView = controller.rsaChallengeMethod(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenrsaChallengeMethod_GivenAnswer_Null_ThenReturnModel() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(null);

        ModelAndView modelAndView = controller.rsaChallengeMethod(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaCheckStatusMethod_GivenAnswer_Question_ThenReturnModel() throws Exception {
        when(userSession.isInformationChallenge()).thenReturn(true);
        when(userSession.getOobEnroll()).thenReturn(false);
        when(userSession.isOobAuthenticated()).thenReturn(false);
        ModelAndView modelAndView = controller.rsaCheckStatus(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaCheckStatusMethod_GivenAnswer_MobileIPHONE_ThenReturnModel() throws Exception {
        when(userSession.getOobEnroll()).thenReturn(true);
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.rsaCheckStatus(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaCheckStatusMethod_GivenAnswer_MobileANDROID_ThenReturnModel() throws Exception {
        when(userSession.getOobEnroll()).thenReturn(true);
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.ANDROID);
        ModelAndView modelAndView = controller.rsaCheckStatus(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaCheckStatusMethod_GivenAnswer_MobileIPAD_ThenReturnModel() throws Exception {
        when(userSession.getOobEnroll()).thenReturn(true);
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPAD);
        ModelAndView modelAndView = controller.rsaCheckStatus(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaCheckStatusMethod_GivenAnswer_Null_ThenReturnModel() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(null);

        ModelAndView modelAndView = controller.rsaCheckStatus(request, response);
        assertViewName(modelAndView, "rsaForm");
    }


    @Test
    public void whenRsaEditQuestions_GivenAnswer_MobileIPAD_ThenReturnModel() throws Exception {
        when(userSession.getOobEnroll()).thenReturn(true);
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPAD);
        ModelAndView modelAndView = controller.rsaEditQuestions(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaEditQuestions_GivenAnswer_Web_ThenReturnModel() throws Exception {
        when(userSession.getOobEnroll()).thenReturn(true);
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        ModelAndView modelAndView = controller.rsaEditQuestions(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaModifyQuestions_GivenAgentTypeIPhone_ThenReturnModel() throws Exception {

        when(ServletRequestUtils.getStringParameter(request, QUESTION_1)).thenReturn("Dummy 1");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_2)).thenReturn("Dummy 2");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_3)).thenReturn("Dummy 3");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, TYPEDEVICE)).thenReturn("private");

        when(customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any(EnrollmentInformationinRSARequest.class))).thenReturn(updateRSAresponse);
        when(userSession.getOobEnroll()).thenReturn(true);
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(updateRSAresponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(alertsEntitlement);
        ModelAndView modelAndView = controller.rsaModifyQuestions(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaModifyQuestions_GivenAgentTypeIPhone_GivenRsaResponseERROR_ThenReturnModel() throws Exception {

        when(ServletRequestUtils.getStringParameter(request, QUESTION_1)).thenReturn("Dummy 1");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_2)).thenReturn("Dummy 2");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_3)).thenReturn("Dummy 3");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, TYPEDEVICE)).thenReturn("private");

        when(customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any(EnrollmentInformationinRSARequest.class))).thenReturn(updateRSAresponse);
        when(userSession.getOobEnroll()).thenReturn(true);
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(updateRSAresponse.getStatus()).thenReturn(ResultStatus.ERROR);

        ModelAndView modelAndView = controller.rsaModifyQuestions(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaModifyQuestions_GivenAgentTypeIPhone_GivenDistincAnswer_ThenReturnModel() throws Exception {

        when(ServletRequestUtils.getStringParameter(request, QUESTION_1)).thenReturn("Dummy 1");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_1)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_2)).thenReturn("Dummy 2");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_3)).thenReturn("Dummy 3");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, TYPEDEVICE)).thenReturn("private");

        when(userSession.getOobEnroll()).thenReturn(true);
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        ModelAndView modelAndView = controller.rsaModifyQuestions(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaModifyQuestions_GivenAgentTypeWeb_ThenReturnModel() throws Exception {

        when(ServletRequestUtils.getStringParameter(request, QUESTION_1)).thenReturn("Dummy 1");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_2)).thenReturn("Dummy 2");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_3)).thenReturn("Dummy 3");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, TYPEDEVICE)).thenReturn("private");

        when(customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any(EnrollmentInformationinRSARequest.class))).thenReturn(updateRSAresponse);
        when(userSession.getOobEnroll()).thenReturn(true);
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        when(updateRSAresponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(alertsEntitlement);
        ModelAndView modelAndView = controller.rsaModifyQuestions(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaOOBDeactivate_GivenAgentTypeIPhone_GivenDistincAnswer_ThenReturnModel() throws Exception {

        when(ServletRequestUtils.getStringParameter(request, QUESTION_1)).thenReturn("Dummy 1");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_1)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_2)).thenReturn("Dummy 2");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_3)).thenReturn("Dummy 3");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, TYPEDEVICE)).thenReturn("private");

        when(customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any(EnrollmentInformationinRSARequest.class))).thenReturn(updateRSAresponse);
        ModelAndView modelAndView = controller.rsaOOBDeactivate(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaOOBDeactivate_GivenAgentTypeIPhone_ThenReturnModel() throws Exception {

        when(ServletRequestUtils.getStringParameter(request, QUESTION_1)).thenReturn("Dummy 1");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_2)).thenReturn("Dummy 2");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_3)).thenReturn("Dummy 3");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, TYPEDEVICE)).thenReturn("private");

        when(customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any(EnrollmentInformationinRSARequest.class))).thenReturn(updateRSAresponse);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(updateRSAresponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
        when(CustomerFacade.getInstance()).thenReturn(customerFacade);
        //when(customerFacade.logCustomerUsageData(any(CustomerProfile.class), any(CustomerActions.class))).thenReturn(false);
        ModelAndView modelAndView = controller.rsaOOBDeactivate(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaOOBDeactivate_GivenAgentTypeIPhone_GivenPublicTypeDevice_ThenReturnModel() throws Exception {

        when(ServletRequestUtils.getStringParameter(request, QUESTION_1)).thenReturn("Dummy 1");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_2)).thenReturn("Dummy 2");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_3)).thenReturn("Dummy 3");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, TYPEDEVICE)).thenReturn("public");

        when(customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any(EnrollmentInformationinRSARequest.class))).thenReturn(updateRSAresponse);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(updateRSAresponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
        when(CustomerFacade.getInstance()).thenReturn(customerFacade);
       // when(customerFacade.logCustomerUsageData(any(CustomerProfile.class), any(CustomerActions.class))).thenReturn(false);
        ModelAndView modelAndView = controller.rsaOOBDeactivate(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaOOBDeactivate_GivenAgentTypeIPhone_CookieNull_ThenReturnModel() throws Exception {

        when(ServletRequestUtils.getStringParameter(request, QUESTION_1)).thenReturn("Dummy 1");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_2)).thenReturn("Dummy 2");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_3)).thenReturn("Dummy 3");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, TYPEDEVICE)).thenReturn("private");

        when(customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any(EnrollmentInformationinRSARequest.class))).thenReturn(updateRSAresponse);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(updateRSAresponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
        when(CustomerFacade.getInstance()).thenReturn(customerFacade);
        when(WebUtils.getCookie(request, COOKIE_PMDATA)).thenReturn(null);
       // when(customerFacade.logCustomerUsageData(any(CustomerProfile.class), any(CustomerActions.class))).thenReturn(false);
        ModelAndView modelAndView = controller.rsaOOBDeactivate(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaOOBDeactivate_GivenAgentTypeIPhone_CookieNotNull_ThenReturnModel() throws Exception {

        when(ServletRequestUtils.getStringParameter(request, QUESTION_1)).thenReturn("Dummy 1");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_2)).thenReturn("Dummy 2");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_3)).thenReturn("Dummy 3");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, TYPEDEVICE)).thenReturn("private");

        when(customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any(EnrollmentInformationinRSARequest.class))).thenReturn(updateRSAresponse);
        when(userSession.getOobEnroll()).thenReturn(true);
        when(userSession.isOobAuthenticated()).thenReturn(true);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(updateRSAresponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
        when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(alertsEntitlement);
        ModelAndView modelAndView = controller.rsaOOBDeactivate(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaOOBDeactivate_GivenAgentTypeIPhone_GivenRsaResponseERROR_ThenReturnModel() throws Exception {

        when(ServletRequestUtils.getStringParameter(request, QUESTION_1)).thenReturn("Dummy 1");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_1)).thenReturn("1");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_2)).thenReturn("Dummy 2");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_2)).thenReturn("2");
        when(ServletRequestUtils.getStringParameter(request, QUESTION_3)).thenReturn("Dummy 3");
        when(ServletRequestUtils.getStringParameter(request, ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, C_ANSWER_3)).thenReturn("3");
        when(ServletRequestUtils.getStringParameter(request, TYPEDEVICE)).thenReturn("private");

        when(customerService.getSignOnCustomerServiceModule().updateUserRSAQuestionSelection(any(EnrollmentInformationinRSARequest.class))).thenReturn(updateRSAresponse);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(updateRSAresponse.getStatus()).thenReturn(ResultStatus.ERROR);

        when(CustomerFacade.getInstance()).thenReturn(customerFacade);
       // when(customerFacade.logCustomerUsageData(any(CustomerProfile.class), any(CustomerActions.class))).thenReturn(false);
        ModelAndView modelAndView = controller.rsaOOBDeactivate(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaGetQuestions_GivenAgentTypeIphone_ThenReturnModel () throws Exception {

        when(customerService.rsaGetQuestionsSelection(any(RSAActionRequest.class))).thenReturn(questionResponse);
        when(customerService.getSignOnCustomerServiceModule().validateRSAQuestion(any(RSAActionRequest.class))).thenReturn(validateQuestion);
        when(questionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getUsername()).thenReturn("milecita29");
        when(customerProfile.getEmail()).thenReturn("ut@evertecinc.com");
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);

        Map<Object,Object> informationQuestion1 = new HashMap <Object,Object>();
        informationQuestion1.put("1",QUESTION_TEXT);
        Map<Object,Object> informationQuestion2 = new HashMap <Object,Object>();
        informationQuestion2.put("2",QUESTION_TEXT_2);
        Map<Object,Object> informationQuestion3 = new HashMap <Object,Object>();
        informationQuestion3.put("3",QUESTION_TEXT_3);

        when(questionResponse.getQuestionMap1()).thenReturn(informationQuestion1);
        when(questionResponse.getQuestionMap2()).thenReturn(informationQuestion2);
        when(questionResponse.getQuestionMap3()).thenReturn(informationQuestion3);
        ModelAndView modelAndView = controller.rsaGetQuestions(request, response);
        assertViewName(modelAndView, "rsaForm");
    }

    @Test
    public void whenRsaGetQuestions_GivenAgentTypeIphone_RSAGivenError_ThenReturnModel () throws Exception {

        when(customerService.rsaGetQuestionsSelection(any(RSAActionRequest.class))).thenReturn(questionResponse);
        when(customerService.getSignOnCustomerServiceModule().validateRSAQuestion(any(RSAActionRequest.class))).thenReturn(validateQuestion);
        when(questionResponse.getStatusResult()).thenReturn(ResultStatus.ERROR);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getUsername()).thenReturn("milecita29");
        when(customerProfile.getEmail()).thenReturn("ut@evertecinc.com");
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);

        ModelAndView modelAndView = controller.rsaGetQuestions(request, response);
        assertViewName(modelAndView, "rsaForm");
    }


    @Test(expected = ModelAndViewDefiningException.class)
    public void whenInitRsaWeb_RsaGivenQuestion_GivenException_ThenReturnErrorModel() throws Exception {

        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.QUESTION);
        request.addHeader(USERAGENT, "iPhone");
        userSession.setLoginCompleteSuccess(true);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        when(userSession.getInformationQuestion()).thenReturn(null);
        StatusHeader header = new StatusHeader();
        header.setStatusCode(200);
        deviceResult.setDeviceData(deviceData);
        when(customerService.rsaAnalyze(any(RSAAnalyzeRequest.class))).thenReturn(anResp);
        when(personalQuestion.getStatusResult()).thenReturn(ResultStatus.EXCEPTION);
        when(anResp.getStatusHeader()).thenReturn(header);
        when(anResp.getDeviceResult()).thenReturn(deviceResult);
        when(anResp.getIdentificationData()).thenReturn(idenData);
        when(idenData.getSessionId()).thenReturn("dummySession");
        when(idenData.getTransactionId()).thenReturn("dummyTrans");
        when(deviceData.getDeviceTokenCookie()).thenReturn("dummy Cookie1");
        when(deviceData.getDeviceTokenFSO()).thenReturn("dummy FSO 1");
        when(deviceResult.getDeviceData()).thenReturn(deviceData);
        when(customerProfile.getEmail()).thenReturn(EMAIL_USER);
        when(customerService.getSignOnCustomerServiceModule().getRSAPersonalQuestion(any(RSAActionRequest.class))).thenReturn(personalQuestion);
        when(personalQuestion.getDeviceRequest()).thenReturn(deviceRequest);
        when(deviceRequest.getDeviceTokenCookie()).thenReturn("dummy Cookie 2");
        when(RSAUtils.getDeviceRequest(userSession.getPm_fp(), userSession.getDeviceRequest().getDeviceTokenCookie(), request)).thenReturn(deviceRequest);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        ModelAndView modelAndView = controller.initRsa(request, response);
        assertViewName(modelAndView, "error");
    }


    /**
     * @return the recCodeBlockLength
     */
    public int getRecCodeBlockLength() {
        return recCodeBlockLength;
    }

    /**
     * @param recCodeBlockLength the recCodeBlockLength to set
     */
    public void setRecCodeBlockLength(int recCodeBlockLength) {
        this.recCodeBlockLength = recCodeBlockLength;
    }

    /**
     * @return the timeInterval
     */
    public int getTimeInterval() {
        return timeInterval;
    }

    /**
     * @param timeInterval the timeInterval to set
     */
    public void setTimeInterval(int timeInterval) {
        this.timeInterval = timeInterval;
    }

	/**
	 * when InitRsa
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenInitRsa_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		PowerMockito.doReturn("54654654").when(controller, "getChallengeMethod", any(), any());
		PowerMockito.doReturn("54654654").when(controller, "getDisplayChallenge", any());
		when(ServletRequestUtils.getStringParameter(request, "nextScreen")).thenReturn("next");
		when(ServletRequestUtils.getStringParameter(request, "cancelButton")).thenReturn("cancel");
		when(ServletRequestUtils.getStringParameter(request, "newCode")).thenReturn("N");
		when(ServletRequestUtils.getStringParameter(request, "function")).thenReturn("function");
		when(ServletRequestUtils.getStringParameter(request, "phone")).thenReturn("____-____");

		ModelAndView result = controller.initRsa(request, response);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when NonMainPhone
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenNonMainPhone_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(rsaEnt);
		when(rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE)).thenReturn("88888888");
		when(ServletRequestUtils.getStringParameter(request, "nextScreen")).thenReturn("next");
		when(ServletRequestUtils.getStringParameter(request, "function")).thenReturn("function");
		when(ServletRequestUtils.getStringParameter(request, "phone")).thenReturn("____-____");

		ModelAndView result = controller.nonMainPhone(request, response);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when RecoveryCode
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenRecoveryCode_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getEntitlementByType(EntitlementType.RSA)).thenReturn(rsaEnt);
		when(rsaEnt.getString(RSAEntitlement.ALTERNATEPHONE)).thenReturn("88888888");

		ModelAndView result = controller.recoveryCode(request, response);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when NonCodes
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void wheNonCodes_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(ServletRequestUtils.getStringParameter(request, "nextScreen")).thenReturn("next");
		when(ServletRequestUtils.getStringParameter(request, "function")).thenReturn("function");
		when(ServletRequestUtils.getStringParameter(request, "phone")).thenReturn("____-____");

		ModelAndView result = controller.nonCodes(request, response);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when RsaAltPhone
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenRsaAltPhone_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		PowerMockito.doReturn("54654654").when(controller, "getChallengeMethod", any(), any());
		PowerMockito.doReturn("54654654").when(controller, "getDisplayChallenge", any());
		when(ServletRequestUtils.getStringParameter(request, "nextScreen")).thenReturn("next");
		when(ServletRequestUtils.getStringParameter(request, "function")).thenReturn("function");
		PowerMockito.doNothing().when(controller, "generateCode", any(HttpServletRequest.class), any(),
				any(), any(), any(Boolean.class));

		ModelAndView result = controller.rsaAltPhone(request, response);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when RsaEnroll
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenRsaEnroll_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)).thenReturn(Boolean.FALSE);
		PowerMockito.doReturn(Boolean.FALSE).when(controller, "isMobile", any(HttpServletRequest.class));

		ModelAndView result = controller.rsaEnroll(request, response);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when RsaAddMobile
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenRsaAddMobile_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		PowerMockito.doReturn(Boolean.FALSE).when(controller, "isMobile", any(HttpServletRequest.class));

		ModelAndView result = controller.rsaAddMobile(request, response);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when RsaEditOOB
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenRsaEditOOB_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3686)).thenReturn(Boolean.FALSE);

		ModelAndView result = controller.rsaEditOOB(request, response);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when RsaEditQuestions
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenRsaEditQuestions_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		PowerMockito.doReturn(Boolean.FALSE).when(controller, "isMobile", any(HttpServletRequest.class));

		ModelAndView result = controller.rsaEditQuestions(request, response);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}

}
