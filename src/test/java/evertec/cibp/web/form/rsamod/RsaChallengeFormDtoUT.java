package evertec.cibp.web.form.rsamod;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import static com.iona.common.misc.Assertor.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Saul Castro
 * @since 6/27/2022
 */
@RunWith(PowerMockRunner.class)
public class RsaChallengeFormDtoUT {
    private RsaChallengeFormDTO dto1;
    private RsaChallengeFormDTO dto2;


    @Before
    public void setUp() {
        dto1 = new RsaChallengeFormDTO().
                setAns1("1")
                .setAns2("2")
                .setAns3("3")
                .setCans1("11")
                .setCans2("22")
                .setCans3("33")
                .setSq1("111")
                .setSq2("222")
                .setSq3("333")
                .setTypeDevice(null);

        dto2 = new RsaChallengeFormDTO()
                .setAns1("1")
                .setAns2("2")
                .setAns3("3")
                .setCans1("11")
                .setCans2("22")
                .setCans3("33")
                .setSq1("111")
                .setSq2("222")
                .setSq3("333")
                .setTypeDevice(null);
    }


    @Test
    public void whenHasCode_GivenParams_ThenAssertIsTrue() {
        assertEquals(dto1, dto2);
        assertTrue(dto1.hashCode() == dto2.hashCode());
    }

    @Test
    public void whenEquals_GivenParams_ThenAssertIsTrue() {
        assertTrue(dto1.equals(dto2) && dto2.equals(dto1));
        assertTrue(dto1.hashCode() == dto2.hashCode());
    }
    
    @Test
    public void whenEquals_GetSetSq1_ThenAssertIsTrue() {
    	dto1.setSq1("sq1");
    	assertNotNull(dto1.getSq1());
    }
    
    @Test
    public void whenEquals_GetSetSq2_ThenAssertIsTrue() {
    	dto1.setSq2("sq1");
    	assertNotNull(dto1.getSq2());
    }
    
    @Test
    public void whenEquals_GetSetSq3_ThenAssertIsTrue() {
    	dto1.setSq2("sq2");
    	assertNotNull(dto1.getSq2());
    }

    @Test
    public void whenEquals_GetSetAns1_ThenAssertIsTrue() {
    	dto1.setAns1("ans1");
    	assertNotNull(dto1.getAns1());
    }
    
    @Test
    public void whenEquals_GetSetAns2_ThenAssertIsTrue() {
    	dto1.setAns2("ans1");
    	assertNotNull(dto1.getAns2());
    }
    
    @Test
    public void whenEquals_GetSetAns3_ThenAssertIsTrue() {
    	dto1.setAns3("ans1");
    	assertNotNull(dto1.getAns3());
    }
    
    @Test
    public void whenEquals_GetSetCans1_ThenAssertIsTrue() {
    	dto1.setCans1("cans1");
    	assertNotNull(dto1.getCans1());
    }
    
    @Test
    public void whenEquals_GetSetCans2_ThenAssertIsTrue() {
    	dto1.setCans1("cans2");
    	assertNotNull(dto1.getCans2());
    }
    
    @Test
    public void whenEquals_GetSetCans3_ThenAssertIsTrue() {
    	dto1.setCans1("cans3");
    	assertNotNull(dto1.getCans3());
    }
}
