package evertec.cibp.web.form.rsamod;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;

import evertec.cibp.core.services.customer.modules.SignOnCustomerServiceModule;
import evertec.cibp.core.services.requests.adaptiveauth.EnrollmentInformationinRSARequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAActionRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RSAAnalyzeRequest;
import evertec.cibp.core.services.requests.adaptiveauth.RsaChallengeRequest;
import evertec.cibp.core.services.responses.adaptiveauth.ChallengeOobResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionSelectionResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceData;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.DeviceResult;
import com.rsa.csd.ws.IdentificationData;
import com.rsa.csd.ws.StatusHeader;

import bppr.sms.beans.SMSCTN;
import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.CustomerFlag;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.responses.OobRecoveryCodeStatusResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.RsaQuestionsUpdateResponse;
import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.core.services.responses.ValidationQuestionResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ServletRequestUtils.class, MessageSource.class, StaticMessageSource.class, WebUtils.class, CustomerFacade.class, SignOnCustomerServiceModule.class, RSAUtils.class})
public class RSAChallengeFormIT extends AbstractModelAndViewTests {

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    /** Recovery Code block 60 minutes*/
    private int recCodeBlockLength;

    /** Wait time in messages */
    private int timeInterval;

    private CustomerEntitlement customerEntitlement;
    private CustomerEntitlement rsaEntitlement;
    private CustomerEntitlement alertsEntitlement;
    private static final String USERAGENT = "user-agent";
    private static final String COOKIE_PMDATA = "PMData";
    private static final String QUESTION_TEXT = "Pregunta capsiosa";
    private static final String EMAIL_USER = "retailonlinebanking@evertecinc.com";
    private static final int _4_0_9 = 409;
    private static final int _2_0_0 = 200;

    @Mock
    private CustomerServices customerService;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private UserSession userSession;

    @Mock
    private SMSProfile smsprofile;

    @Mock
    private DeviceRequest deviceRequest;

    @Mock
    private ChallengeOobResponse challengeOobResponse;

    @Mock
    private RSAUtils rsaUtils;

    @Mock
    private MessageSource messageSource;

	/*@Mock
	private RequestContextUtils requestContextUtils;*/

    @Mock
    private CustomerProfileService customerProfileService;

    @Mock
    private OobRecoveryCodeStatusResponse oobRecoveryCodeStatusResponse;

    @Mock
    private List<ASK_FOR> tasks;

    @Mock
    private Set<CustomerFlag> customerFlags;

    @InjectMocks
    private RSAChallengeForm controller;

    @Mock
    QuestionResponse personalQuestion;

    @Mock
    AnalyzeResponse anResp;

    @Mock
    DeviceResult deviceResult;

    @Mock
    DeviceData deviceData;

    @Mock
    IdentificationData idenData;

    @Mock
    ValidationQuestionResponse validateQuestion;

    @Mock
    EnrollmentInformationinRSARequest eirsa;

    @Mock
    RsaQuestionsUpdateResponse updateRSAresponse;

    @Mock
    QuestionSelectionResponse questionResponse;

    @Mock
    AlertsEntitlement alertsEntitlement1;

    @Mock
    Cookie cookie;
    @Mock
    StatusHeader header;

    @Mock
    private SignOnCustomerServiceModule signOnCustomerServiceModule;

    @Before
    public void setUp() throws Exception {

        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(CustomerFacade.class);
        PowerMockito.mockStatic(MessageSource.class);
        PowerMockito.mockStatic(ServletRequestUtils.class);
        PowerMockito.spy(RSAUtils.class);
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        customerEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);
        alertsEntitlement = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
        rsaEntitlement = new CustomerEntitlement(EntitlementType.MOBILE_BANKING, EntitlementStatus.ENABLED);
        rsaEntitlement.setAttributeData("<properties>\r\n" + "  <property name=\"CODE_SALT\" value=\"1270210404\"/>\r\n"
                + "  <property name=\"ALTERNATE_PROVIDER\" value=\"US4\"/>\r\n"
                + "  <property name=\"PROFILEID\" value=\"8a81a0b7688179d801688180c973000b\"/>\r\n"
                + "  <property name=\"ALWAYSCHALLENGE\" value=\"false\"/>\r\n"
                + "  <property name=\"CODE_HASH\" value=\"e7dadcdb81a56e5294df01d50132b8b0c096f76aa21f7c35ef8f015a64c6aafa\"/>\r\n"
                + "  <property name=\"ALTERNATE_PHONE\" value=\"4554654654\"/>\r\n" + "</properties>");
        alertsEntitlement.setAttributeData("<properties>\r\n"
                + "  <property name=\"ALERT_3\" value=\"QUESTIONS_EDIT_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_2\" value=\"PAYMENTS_CANCEL_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_1\" value=\"TRANSFERS_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_10\" value=\"NEW_FINGERPRINT_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_COUNT\" value=\"18\"/>\r\n"
                + "  <property name=\"ALERT_9\" value=\"PAYMENTS_MODIFY_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_8\" value=\"CHANGE_PASS_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_7\" value=\"EMAIL_EDIT_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_6\" value=\"PAYMENTS_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_5\" value=\"TRANSFERS_MODIFY_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_4\" value=\"TRANSFERS_CANCEL_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_11\" value=\"ACCESS_BLOCK_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_12\" value=\"QUESTIONS_BLOCK_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_13\" value=\"PHONE_EDIT_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_14\" value=\"ADRESS_EDIT_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_15\" value=\"MOBILE_PHONE_EDIT_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_16\" value=\"NEW_DEVICE_EMAIL\"/>\r\n"
                + "  <property name=\"ALERT_17\" value=\"QUESTIONS_BLOCK_SMS\"/>\r\n"
                + "  <property name=\"ALERT_18\" value=\"QUESTIONS_BLOCK_PUSH\"/>\r\n"
                + "  <property name=\"ALERT_19\" value=\"QUESTIONS_EDIT_SMS\"/>\r\n"
                + "  <property name=\"ALERT_20\" value=\"QUESTIONS_EDIT_PUSH\"/>\r\n"
                + "  </properties>");
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING))
                .thenReturn(customerEntitlement);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.RSA)).thenReturn(rsaEntitlement);
        when(userSession.getSmsProfile(Utils.generateCustomerInteractionEvent(request))).thenReturn(smsprofile);
        when(smsprofile.getSmsProfileId()).thenReturn("123456789");
        when(userSession.getPm_fp()).thenReturn("ABCDEFG");
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn("HIJKLM");
        when(userSession.getUsername()).thenReturn("milecita29");
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBEMAIL);
        when(customerService.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);

        Set<bppr.sms.beans.SMSCTN> ctns = new HashSet<bppr.sms.beans.SMSCTN>();
        SMSCTN ctn = new SMSCTN();
        ctn.setCtn("7879231845");
        ctns.add(ctn);
        when(smsprofile.getCtns()).thenReturn(ctns);


    }

    @Test
    public void whenInitRsa_RsaGivenQuestion_ThenReturnRsaModel() throws Exception {
        when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.QUESTION);
        request.addHeader(USERAGENT, "iPhone");
        userSession.setLoginCompleteSuccess(true);
        when(userSession.getCustomerProfile().getEntitlementByType(EntitlementType.MOBILE_BANKING)).thenReturn(customerEntitlement);
        when(userSession.getInformationQuestion()).thenReturn(null);
        StatusHeader header = new StatusHeader();
        header.setStatusCode(200);
        deviceResult.setDeviceData(deviceData);
        when(customerService.rsaAnalyze(any(RSAAnalyzeRequest.class))).thenReturn(anResp);
        when(personalQuestion.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
        when(anResp.getStatusHeader()).thenReturn(header);
        when(anResp.getDeviceResult()).thenReturn(deviceResult);
        when(anResp.getIdentificationData()).thenReturn(idenData);
        when(idenData.getSessionId()).thenReturn("dummySession");
        when(idenData.getTransactionId()).thenReturn("dummyTrans");
        when(deviceData.getDeviceTokenCookie()).thenReturn("dummy Cookie1");
        when(deviceData.getDeviceTokenFSO()).thenReturn("dummy FSO 1");
        when(deviceResult.getDeviceData()).thenReturn(deviceData);
        when(customerProfile.getEmail()).thenReturn(EMAIL_USER);
        when(customerService.getSignOnCustomerServiceModule().getRSAPersonalQuestion(any(RSAActionRequest.class))).thenReturn(personalQuestion);
        when(deviceRequest.getDeviceTokenCookie()).thenReturn("dummy Cookie 2");
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);

        when(WebUtils.getCookie(request, COOKIE_PMDATA)).thenReturn(cookie);

        Map<Object,Object> informationQuestion = new HashMap <Object,Object>();
        informationQuestion.put("1",QUESTION_TEXT);
        when(userSession.getInformationQuestion()).thenReturn(informationQuestion);
        when(personalQuestion.getStatusResult()).thenReturn(ResultStatus.SUCCESS);

        ModelAndView modelAndView = controller.initRsa(request, response);

        verifyStatic(RSAUtils.class, atLeast(1));
        RSAUtils.getDeviceRequest(userSession.getPm_fp(),userSession.getDeviceRequest().getDeviceTokenCookie(),request );

        assertViewName(modelAndView, "rsaForm");
    }


    /**
     * @return the recCodeBlockLength
     */
    public int getRecCodeBlockLength() {
        return recCodeBlockLength;
    }

    /**
     * @param recCodeBlockLength the recCodeBlockLength to set
     */
    public void setRecCodeBlockLength(int recCodeBlockLength) {
        this.recCodeBlockLength = recCodeBlockLength;
    }

    /**
     * @return the timeInterval
     */
    public int getTimeInterval() {
        return timeInterval;
    }

    /**
     * @param timeInterval the timeInterval to set
     */
    public void setTimeInterval(int timeInterval) {
        this.timeInterval = timeInterval;
    }

}
