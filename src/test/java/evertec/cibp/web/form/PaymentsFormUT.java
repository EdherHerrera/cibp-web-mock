package evertec.cibp.web.form;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bppr.tv.beans.accounts.records.MortgageAccountRecord;
import bppr.tv.beans.accounts.types.LoanAccount;
import bppr.tv.beans.payments.Payee;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.beans.Frequency;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.PaymentBean;
import evertec.cibp.web.form.bean.PaymentsListBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, WebUtils.class, Utils.class})
public class PaymentsFormUT extends AbstractModelAndViewTests {

	private HttpServletRequest request;
	private HttpServletResponse response;

	@Mock
	private CustomerServices customerService;

	@Mock
	private BindException error;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private PaymentsListBean paymentListBean;

	@Mock
	private MessageSource messageSource;


	@Mock
	private BankingSession bankingSession;

	@Mock
	private TVFrontendAccount account;

	@Mock
	private TVFrontendAccount accountDetail;

	@Mock
	private LoanAccount loanAccount;

	@Mock
	private MortgageAccountRecord mlaRecord;

	@InjectMocks
	private PaymentsForm paymentForm;


	@Before
	public void setUp() throws Exception {

		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();

		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(messageSource.getMessage("ebpp.module",null,new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(paymentListBean.getSingleAccountFrom()).thenReturn("multiple");

		paymentForm.setMessageSource(messageSource);

		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(customerProfile.getTaxId()).thenReturn("123456789");
	}


	@Test
	public void whenprocessFinish_GivenPaymentsNotNull_ThenReturnModelAndViewPage2() throws Exception {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);

		List<TVFrontEndPayee> payeesMock = new ArrayList();
		payeesMock.add(mock(TVFrontEndPayee.class));

		when(userSession.getPayessBasicEbpp(true)).thenReturn(payeesMock);

		Set<TVAccountTransferSource> accountsFrom = new HashSet();
		accountsFrom.add(mock(TVAccountTransferSource.class));

		when(userSession.getAccountsPaymentFrom()).thenReturn(accountsFrom);

		List<PaymentBean> payments = new ArrayList();
		payments.add(mock(PaymentBean.class));
		when(paymentListBean.getPayments()).thenReturn(payments);

		ModelAndView model = paymentForm.processFinish(request, response, paymentListBean, error);

		assertViewName(model, "makePayment");
		assertModelAttributeValue(model, "page", "2");

	}

	@Test
	public void whenReferenceData_GivenRealTimeNotificationFlagTrue_ThenReturnModelAndViewPage1() throws Exception {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(Utils.getAccountByAccountNumber("1232312",new ArrayList<>())).thenReturn(null);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(Boolean.TRUE);
		when(paymentListBean.getSingleAccountFrom()).thenReturn("multiple");
		when(messageSource.getMessage("ebpp.module",null,new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		List<TVFrontEndPayee> payeesMock = new ArrayList();
		when(userSession.getPayessBasicEbpp(true)).thenReturn(payeesMock);

		Set<TVAccountTransferSource> accountsFrom = new HashSet();
		when(userSession.getAccountsPaymentFrom()).thenReturn(accountsFrom);

		List<PaymentBean> payments = new ArrayList();
		when(paymentListBean.getPayments()).thenReturn(payments);

		Map<String, Object> model = paymentForm.referenceData(request, paymentListBean, error,1);


	}


	@Test
	public void whenReferenceData_GivenRealTimeNotificationFlagFalse_ThenReturnModelAndViewPage1() throws Exception {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(Utils.getAccountByAccountNumber("1232312",new ArrayList<>())).thenReturn(null);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(Boolean.FALSE);
		when(paymentListBean.getSingleAccountFrom()).thenReturn("multiple");
		when(messageSource.getMessage("ebpp.module",null,new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		List<TVFrontEndPayee> payeesMock = new ArrayList();
		when(userSession.getPayessBasicEbpp(true)).thenReturn(payeesMock);

		Set<TVAccountTransferSource> accountsFrom = new HashSet();
		when(userSession.getAccountsPaymentFrom()).thenReturn(accountsFrom);

		List<PaymentBean> payments = new ArrayList();
		when(paymentListBean.getPayments()).thenReturn(payments);

		Map<String, Object> model = paymentForm.referenceData(request, paymentListBean, error,1);


	}

	@Test
	public void whenprocessFinish_GivenPaymentsNotNull_ThenReturnModelAndViewVendorActivatedFalse() throws Exception {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);

		List<TVFrontEndPayee> payeesMock = new ArrayList();
		payeesMock.add(mock(TVFrontEndPayee.class));

		when(userSession.getPayessBasicEbpp(true)).thenReturn(payeesMock);

		Set<TVAccountTransferSource> accountsFrom = new HashSet();
		accountsFrom.add(mock(TVAccountTransferSource.class));

		when(userSession.getAccountsPaymentFrom()).thenReturn(accountsFrom);

		List<PaymentBean> payments = new ArrayList();
		payments.add(mock(PaymentBean.class));
		when(paymentListBean.getPayments()).thenReturn(payments);

		ModelAndView model = paymentForm.processFinish(request, response, paymentListBean, error);

		assertViewName(model, "makePayment");
		assertModelAttributeValue(model, "isEvendorActivated", false);

	}


	@Test
	public void whenprocessFinish_GivenPaymentsNotNull_ThenReturnModelAndViewTelenominaIDNull() throws Exception {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);

		List<TVFrontEndPayee> payeesMock = new ArrayList();
		payeesMock.add(mock(TVFrontEndPayee.class));

		when(userSession.getPayessBasicEbpp(true)).thenReturn(payeesMock);

		Set<TVAccountTransferSource> accountsFrom = new HashSet();
		accountsFrom.add(mock(TVAccountTransferSource.class));

		when(userSession.getAccountsPaymentFrom()).thenReturn(accountsFrom);

		List<PaymentBean> payments = new ArrayList();
		payments.add(mock(PaymentBean.class));
		when(paymentListBean.getPayments()).thenReturn(payments);

		ModelAndView model = paymentForm.processFinish(request, response, paymentListBean, error);

		assertViewName(model, "makePayment");
		assertNull(model.getModel().get("telenominaID"));

	}


	@Test
	public void whenprocessFinish_GivenPaymentsNotNull_ThenReturnModelAndViewPaymentSortEmpty() throws Exception {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);

		List<TVFrontEndPayee> payeesMock = new ArrayList();
		payeesMock.add(mock(TVFrontEndPayee.class));

		when(userSession.getPayessBasicEbpp(true)).thenReturn(payeesMock);

		Set<TVAccountTransferSource> accountsFrom = new HashSet();
		accountsFrom.add(mock(TVAccountTransferSource.class));

		when(userSession.getAccountsPaymentFrom()).thenReturn(accountsFrom);

		List<PaymentBean> payments = new ArrayList();
		payments.add(mock(PaymentBean.class));
		when(paymentListBean.getPayments()).thenReturn(payments);

		ModelAndView model = paymentForm.processFinish(request, response, paymentListBean, error);

		assertViewName(model, "makePayment");
		assertTrue(((List) model.getModel().get("paymentsort")).isEmpty());

	}


	@Test(expected = NullPointerException.class)
	public void whenprocessFinish_GivenPaymentsIsNull_ThenReturnNullPointerWxc() throws Exception {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);

		List<TVFrontEndPayee> payeesMock = new ArrayList();
		payeesMock.add(mock(TVFrontEndPayee.class));

		when(userSession.getPayessBasicEbpp(true)).thenReturn(payeesMock);

		Set<TVAccountTransferSource> accountsFrom = new HashSet();
		accountsFrom.add(mock(TVAccountTransferSource.class));

		when(userSession.getAccountsPaymentFrom()).thenReturn(accountsFrom);

		List<PaymentBean> payments = new ArrayList();
		payments.add(mock(PaymentBean.class));
		when(paymentListBean.getPayments()).thenReturn(null);

		paymentForm.processFinish(request, response, null, error);

	}

	/**
	 * when Reference Data
	 * Given N/A
	 * Then return real time date information
	 * @throws Exception
	 */
	@Test
	public void whenReferenceData_ThenReturnRealTimeDateInfoModelAndViewPage1() throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		final Calendar rtN = Calendar.getInstance(); // effective date for real Time

		Map<String, Object> model = paymentForm.referenceData(request, paymentListBean, error, 0);

		assertNotNull(model);
		assertTrue(model.containsKey("todayDateRealTime"));
		assertTrue(model.get("todayDateRealTime").equals(df.format(rtN.getTime())));
		assertTrue(model.containsKey("monthRealTime"));
		assertTrue(model.containsKey("dayRealTime"));
	}


	@Test
	public void whenReferenceData_GivenRealTimeNotificationFlagTrue_GivenActualDate_ThenReturnModelAndViewPage1() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(Boolean.TRUE);
		String payeeId ="1232312";
		PaymentBean paymentBeanMock = mock(PaymentBean.class);
		TVAccountTransferSource tvAccountTransferSourceMock = mock(TVAccountTransferSource.class);
		TVFrontendAccount tvFrontendAccountMock = mock(TVFrontendAccount.class);
		Payee payeeMock = mock(Payee.class);
		GlobalPayee globalPayeeMock = mock(GlobalPayee.class);
		TVFrontEndPayee payeeEndMock = mock(TVFrontEndPayee.class);
		payeeEndMock.setBackendObject(payeeMock);

		List<PaymentBean> paymentBeans = new ArrayList<>();
		paymentBeans.add(paymentBeanMock);
		when(paymentBeanMock.getAmount()).thenReturn(BigDecimal.ONE);
		when(paymentBeanMock.getPayeeid()).thenReturn(payeeId);
		when(payeeEndMock.getBillingAccount()).thenReturn("");
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getAccountsPaymentFrom()).thenReturn(new HashSet<>());
		when(userSession.getPayessBasicEbpp(true)).thenReturn(new ArrayList());
		when(customerService.telepagoIdValidForPIFTransfer("")).thenReturn(false);
		when(Utils.getPayeebyId(payeeId, new ArrayList())).thenReturn(payeeEndMock);

		when(paymentBeanMock.getEffectiveDate()).thenReturn(new Date());
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(paymentListBean.getSingleAccountFrom()).thenReturn("single");
		when(paymentListBean.getFavorites()).thenReturn(paymentBeans);
		when(Utils.getAccountSource("single", new HashSet())).thenReturn(tvAccountTransferSourceMock);
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(payeeEndMock.getGlobalPayee()).thenReturn(globalPayeeMock);
		when(payeeEndMock.getGlobalPayee().getRtNotification()).thenReturn("Y");
		when(payeeEndMock.getGlobalPayee().getRtIndicator()).thenReturn("Y");
		List<Integer> payeeCodes = new ArrayList<>();
		payeeCodes.add(1234);
		when((payeeEndMock.getCustomerPayeeCodes())).thenReturn(payeeCodes);

		when((paymentBeanMock.getRecurrent())).thenReturn("recurrent");
		when((paymentBeanMock.getFrequency())).thenReturn(Frequency.MANUALLY);
		when(tvAccountTransferSourceMock.getAccount()).thenReturn(tvFrontendAccountMock);
		Map<String, Object> model = paymentForm.referenceData(request, paymentListBean, error, 1);

		LocalDate effectiveDate = paymentBeanMock.getEffectiveDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		assertTrue(effectiveDate.isEqual(LocalDate.now()));

	}


	@Test
	public void whenReferenceData_GivenRealTimeNotificationFlagFalse_MoveHolidayToBusinessDay_ThenReturnModelAndViewPage1() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(Boolean.TRUE);
		String payeeId ="1232312";
		LocalDate holiday = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY));
		PaymentBean paymentBeanMock = mock(PaymentBean.class);
		TVAccountTransferSource tvAccountTransferSourceMock = mock(TVAccountTransferSource.class);
		TVFrontendAccount tvFrontendAccountMock = mock(TVFrontendAccount.class);
		Payee payeeMock = mock(Payee.class);
		GlobalPayee globalPayeeMock = mock(GlobalPayee.class);
		TVFrontEndPayee payeeEndMock = mock(TVFrontEndPayee.class);
		payeeEndMock.setBackendObject(payeeMock);

		List<PaymentBean> paymentBeans = new ArrayList<>();
		paymentBeans.add(paymentBeanMock);
		when(paymentBeanMock.getAmount()).thenReturn(BigDecimal.ONE);
		when(paymentBeanMock.getPayeeid()).thenReturn(payeeId);
		when(payeeEndMock.getBillingAccount()).thenReturn("");
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getAccountsPaymentFrom()).thenReturn(new HashSet<>());
		when(userSession.getPayessBasicEbpp(true)).thenReturn(new ArrayList());
		when(customerService.telepagoIdValidForPIFTransfer("")).thenReturn(false);
		when(Utils.getPayeebyId(payeeId, new ArrayList())).thenReturn(payeeEndMock);

		when(paymentBeanMock.getEffectiveDate()).thenReturn(Date.from(holiday.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(paymentListBean.getSingleAccountFrom()).thenReturn("single");
		when(paymentListBean.getFavorites()).thenReturn(paymentBeans);
		when(Utils.getAccountSource("single", new HashSet())).thenReturn(tvAccountTransferSourceMock);
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(payeeEndMock.getGlobalPayee()).thenReturn(globalPayeeMock);
		when(payeeEndMock.getGlobalPayee().getRtNotification()).thenReturn("Y");
		when(payeeEndMock.getGlobalPayee().getRtIndicator()).thenReturn("N");
		List<Integer> payeeCodes = new ArrayList<>();
		payeeCodes.add(1234);
		when((payeeEndMock.getCustomerPayeeCodes())).thenReturn(payeeCodes);

		when((paymentBeanMock.getRecurrent())).thenReturn("recurrent");
		when((paymentBeanMock.getFrequency())).thenReturn(Frequency.MANUALLY);
		when(tvAccountTransferSourceMock.getAccount()).thenReturn(tvFrontendAccountMock);
		Map<String, Object> model = paymentForm.referenceData(request, paymentListBean, error, 1);

		LocalDate effectiveDate = paymentBeanMock.getEffectiveDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		assertFalse(effectiveDate.isEqual(LocalDate.now()));

	}

	@Test
	public void whenReferenceData_GivenPayeeMortgage_ThenReturnModelAndViewPage() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(Boolean.TRUE);
		String payeeId ="1232312";
		LocalDate holiday = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY));
		PaymentBean paymentBeanMock = mock(PaymentBean.class);
		TVAccountTransferSource tvAccountTransferSourceMock = mock(TVAccountTransferSource.class);
		TVFrontendAccount tvFrontendAccountMock = mock(TVFrontendAccount.class);
		Payee payeeMock = mock(Payee.class);
		GlobalPayee globalPayeeMock = mock(GlobalPayee.class);
		TVFrontEndPayee payeeEndMock = mock(TVFrontEndPayee.class);
		payeeEndMock.setBackendObject(payeeMock);

		List<TVFrontEndPayee> payees = new ArrayList<TVFrontEndPayee>();
		payees.add(payeeEndMock);
		List<PaymentBean> paymentBeans = new ArrayList<PaymentBean>();
		paymentBeans.add(paymentBeanMock);
		when(paymentBeanMock.getAmount()).thenReturn(BigDecimal.ONE);
		when(paymentBeanMock.getPayeeid()).thenReturn(payeeId);
		when(payeeEndMock.getBillingAccount()).thenReturn("");
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getAccountsPaymentFrom()).thenReturn(new HashSet<>());
		when(userSession.getPayessBasicEbpp(true)).thenReturn(payees);
		when(customerService.telepagoIdValidForPIFTransfer("")).thenReturn(false);
		when(Utils.getPayeebyId(payeeId, payees)).thenReturn(payeeEndMock);

		when(Utils.getAccountByAccountNumber(any(),any())).thenReturn(account);
		BigDecimal amount = new BigDecimal(899);
		when(account.getSubtype()).thenReturn("MLA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(mlaRecord);
		when (mlaRecord.getCurrentAmount()).thenReturn(amount);

		when(paymentBeanMock.getEffectiveDate()).thenReturn(Date.from(holiday.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(paymentListBean.getSingleAccountFrom()).thenReturn("single");
		when(paymentListBean.getFavorites()).thenReturn(paymentBeans);

		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(payeeEndMock.getGlobalPayee()).thenReturn(globalPayeeMock);
		when(payeeEndMock.getGlobalPayee().getRtNotification()).thenReturn("Y");
		when(payeeEndMock.getGlobalPayee().getRtIndicator()).thenReturn("N");
		List<Integer> payeeCodes = new ArrayList<>();
		payeeCodes.add(1234);
		when((payeeEndMock.getCustomerPayeeCodes())).thenReturn(payeeCodes);

		when((paymentBeanMock.getRecurrent())).thenReturn("recurrent");
		when((paymentBeanMock.getFrequency())).thenReturn(Frequency.MANUALLY);
		when(tvAccountTransferSourceMock.getAccount()).thenReturn(tvFrontendAccountMock);
		Map<String, Object> model = paymentForm.referenceData(request, paymentListBean, error, 1);
		assertTrue(model.containsKey("payeesMap"));

	}

	@Test
	public void whenGetAmountDueInfo_givenAccountSubtypeMLA_thenGetAmountDueFromAccountDetailsInvoked () throws Exception {

		when(account.getSubtype()).thenReturn("MLA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(mlaRecord);

		BigDecimal amount = new BigDecimal(899);

		MortgageAccountRecord mortRecord = PowerMockito.mock(MortgageAccountRecord.class);
		LoanAccount loan = PowerMockito.mock(LoanAccount.class);

		when (accountDetail.getBackendObject()).thenReturn(loan);
		when (loan.getRecords()).thenReturn(mortRecord);
		when (mortRecord.getCurrentAmount()).thenReturn(amount);

		when(customerService.getAccountDetails(customerProfile, bankingSession, account, true)).thenReturn(accountDetail);

		BigDecimal amountR = Whitebox.invokeMethod(paymentForm, "getAmountDueInfo", account, userSession);

		assertEquals(amount, amountR);
	}

	@Test
	public void whenGetAmountDueFromAccountDetails_givenBackendObjectNull_thenReturnZero () throws Exception {

		BigDecimal zero = new BigDecimal(0);
		when(customerService.getAccountDetails(customerProfile, bankingSession, account, true)).thenReturn(accountDetail);

		BigDecimal amount = Whitebox.invokeMethod(paymentForm, "getAmountDueFromAccountDetails", account, userSession);

		assertTrue(amount.equals(zero));
	}

	@Test
	public void whenGetAmountDueFromAccountDetails_givenMortgageRecordNull_ThenReturnZero () throws Exception {

		BigDecimal zero = new BigDecimal(0);

		LoanAccount loan = PowerMockito.mock(LoanAccount.class);

		when (accountDetail.getBackendObject()).thenReturn(loan);

		when(customerService.getAccountDetails(customerProfile, bankingSession, account, true)).thenReturn(accountDetail);

		BigDecimal amount = Whitebox.invokeMethod(paymentForm, "getAmountDueFromAccountDetails", account, userSession);

		assertTrue(amount.equals(zero));
	}

	@Test
	public void whenGetAmountDueInfo_givenAccountSubtypeMLAAndNotNullAmountDue_thenGetAmountDueFromAccountDetailsNotInvoked () throws Exception {

		when(account.getSubtype()).thenReturn("MLA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(mlaRecord);

		BigDecimal amount = new BigDecimal(899);

		when (mlaRecord.getCurrentAmount()).thenReturn(amount);

		BigDecimal amountR = Whitebox.invokeMethod(paymentForm, "getAmountDueInfo", account, userSession);

		assertEquals(amount, amountR);
	}

	@Test
	public void whenGetAmountDueInfo_givenAccountSubtypeLEA_thenReturnPortalBalance () throws Exception {
		BigDecimal portalBalance = new BigDecimal(1145.54);
		when(account.getSubtype()).thenReturn("LEA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(account.getPortalBalance()).thenReturn(portalBalance);

		BigDecimal amountR = Whitebox.invokeMethod(paymentForm, "getAmountDueInfo", account, userSession);

		assertEquals(amountR, portalBalance);
	}

	@Test
	public void whenGetAmountDueInfo_givenAccountSubtypeLEAAndPortalBalanceNull_thenReturnZero () throws Exception {
		BigDecimal zero = new BigDecimal(0);
		when(account.getSubtype()).thenReturn("LEA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(account.getPortalBalance()).thenReturn(null);

		BigDecimal amountR = Whitebox.invokeMethod(paymentForm, "getAmountDueInfo", account, userSession);

		assertEquals(amountR, zero);
	}

	@Test
	public void whenGetAmountDueInfo_givenBackendObjectNull_thenAmountDueNotSet () throws Exception {
		BigDecimal portalBalance = new BigDecimal(1145.54);
		when(account.getSubtype()).thenReturn("MLA");
		when(account.getBackendObject()).thenReturn(null);
		when(account.getPortalBalance()).thenReturn(portalBalance);
		BigDecimal amountR = Whitebox.invokeMethod(paymentForm, "getAmountDueInfo", account, userSession);
		assertEquals(amountR, portalBalance);
	}

	@Test
	public void whenGetAmountDueInfo_givenMortgageRecordNull_ThenReturnZero () throws Exception {

		BigDecimal zero = new BigDecimal(0);

		when(account.getSubtype()).thenReturn("MLA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(null);

		BigDecimal amount = Whitebox.invokeMethod(paymentForm, "getAmountDueInfo", account, userSession);

		assertTrue(amount.equals(zero));
	}

	@Test
	public void whenReferenceData_GivenPayeeMortgage_ThenReturnShowAmountDueMsgTrue() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(Boolean.TRUE);
		String payeeId ="1232312";
		LocalDate holiday = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY));
		PaymentBean paymentBeanMock = mock(PaymentBean.class);
		TVAccountTransferSource tvAccountTransferSourceMock = mock(TVAccountTransferSource.class);
		TVFrontendAccount tvFrontendAccountMock = mock(TVFrontendAccount.class);
		Payee payeeMock = mock(Payee.class);
		GlobalPayee globalPayeeMock = mock(GlobalPayee.class);
		TVFrontEndPayee payeeEndMock = mock(TVFrontEndPayee.class);
		payeeEndMock.setBackendObject(payeeMock);

		List<TVFrontEndPayee> payees = new ArrayList<TVFrontEndPayee>();
		payees.add(payeeEndMock);
		List<PaymentBean> paymentBeans = new ArrayList<PaymentBean>();
		paymentBeans.add(paymentBeanMock);
		when(paymentBeanMock.getAmount()).thenReturn(BigDecimal.ONE);
		when(paymentBeanMock.getPayeeid()).thenReturn(payeeId);
		when(payeeEndMock.getBillingAccount()).thenReturn("");
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getAccountsPaymentFrom()).thenReturn(new HashSet<>());
		when(userSession.getPayessBasicEbpp(true)).thenReturn(payees);
		when(customerService.telepagoIdValidForPIFTransfer("")).thenReturn(false);
		when(Utils.getPayeebyId(payeeId, payees)).thenReturn(payeeEndMock);

		when(Utils.getAccountByAccountNumber(any(),any())).thenReturn(account);
		BigDecimal amount = new BigDecimal(899);
		when(account.getSubtype()).thenReturn("MLA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(mlaRecord);
		when (mlaRecord.getCurrentAmount()).thenReturn(amount);

		when(paymentBeanMock.getEffectiveDate()).thenReturn(Date.from(holiday.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(paymentListBean.getSingleAccountFrom()).thenReturn("single");
		when(paymentListBean.getFavorites()).thenReturn(paymentBeans);

		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(payeeEndMock.getGlobalPayee()).thenReturn(globalPayeeMock);
		when(payeeEndMock.getGlobalPayee().getRtNotification()).thenReturn("Y");
		when(payeeEndMock.getGlobalPayee().getRtIndicator()).thenReturn("N");
		List<Integer> payeeCodes = new ArrayList<>();
		payeeCodes.add(1234);
		when((payeeEndMock.getCustomerPayeeCodes())).thenReturn(payeeCodes);

		when((paymentBeanMock.getRecurrent())).thenReturn("recurrent");
		when((paymentBeanMock.getFrequency())).thenReturn(Frequency.MANUALLY);
		when(tvAccountTransferSourceMock.getAccount()).thenReturn(tvFrontendAccountMock);
		Map<String, Object> model = paymentForm.referenceData(request, paymentListBean, error, 1);
		verify(payeeEndMock, times(1)).setShowAmountDueMessage(true);

	}

	@Test
	public void whenReferenceData_GivenPayeeVisa_ThenReturnShowAmountDueMsgTrue() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(Boolean.TRUE);
		String payeeId ="1232312";
		LocalDate holiday = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY));
		PaymentBean paymentBeanMock = mock(PaymentBean.class);
		TVAccountTransferSource tvAccountTransferSourceMock = mock(TVAccountTransferSource.class);
		TVFrontendAccount tvFrontendAccountMock = mock(TVFrontendAccount.class);
		Payee payeeMock = mock(Payee.class);
		GlobalPayee globalPayeeMock = mock(GlobalPayee.class);
		TVFrontEndPayee payeeEndMock = mock(TVFrontEndPayee.class);
		payeeEndMock.setBackendObject(payeeMock);

		List<TVFrontEndPayee> payees = new ArrayList<TVFrontEndPayee>();
		payees.add(payeeEndMock);
		List<PaymentBean> paymentBeans = new ArrayList<PaymentBean>();
		paymentBeans.add(paymentBeanMock);
		when(paymentBeanMock.getAmount()).thenReturn(BigDecimal.ONE);
		when(paymentBeanMock.getPayeeid()).thenReturn(payeeId);
		when(payeeEndMock.getBillingAccount()).thenReturn("");
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getAccountsPaymentFrom()).thenReturn(new HashSet<>());
		when(userSession.getPayessBasicEbpp(true)).thenReturn(payees);
		when(customerService.telepagoIdValidForPIFTransfer("")).thenReturn(false);
		when(Utils.getPayeebyId(payeeId, payees)).thenReturn(payeeEndMock);

		when(Utils.getAccountByAccountNumber(any(),any())).thenReturn(account);
		BigDecimal amount = new BigDecimal(899);
		when(account.getSubtype()).thenReturn("CCA");
		//when(account.getBackendObject()).thenReturn(loanAccount);
		//when(loanAccount.getRecords()).thenReturn(mlaRecord);
		//when (mlaRecord.getCurrentAmount()).thenReturn(amount);

		when(paymentBeanMock.getEffectiveDate()).thenReturn(Date.from(holiday.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(paymentListBean.getSingleAccountFrom()).thenReturn("single");
		when(paymentListBean.getFavorites()).thenReturn(paymentBeans);

		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(payeeEndMock.getGlobalPayee()).thenReturn(globalPayeeMock);
		when(payeeEndMock.getGlobalPayee().getRtNotification()).thenReturn("Y");
		when(payeeEndMock.getGlobalPayee().getRtIndicator()).thenReturn("N");
		List<Integer> payeeCodes = new ArrayList<>();
		payeeCodes.add(1234);
		when((payeeEndMock.getCustomerPayeeCodes())).thenReturn(payeeCodes);

		when((paymentBeanMock.getRecurrent())).thenReturn("recurrent");
		when((paymentBeanMock.getFrequency())).thenReturn(Frequency.MANUALLY);
		when(tvAccountTransferSourceMock.getAccount()).thenReturn(tvFrontendAccountMock);
		Map<String, Object> model = paymentForm.referenceData(request, paymentListBean, error, 1);
		verify(payeeEndMock, times(1)).setShowAmountDueMessage(false);

	}

}