package evertec.cibp.web.form;

import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.types.CardAccount;
import bppr.tv.beans.accounts.types.DepositAccount;
import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TVFrontendDepositAccount;
import evertec.cibp.core.models.accountopening.AccountApplication;
import evertec.cibp.core.models.accountopening.AccountApplications;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerPayeePreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.flags.CustomerFlag;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.EStatementInteraction;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.enrollment.UsernameReservation;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.beans.payments.Payee;
import evertec.cibp.web.form.bean.EbppStatusUpdateBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.Utils;
import evertec.prophit.ProphITService;
import evertec.prophit.service.bean.CustomerInquiryBean;
import evertec.prophit.service.bean.EbillIndType;
import evertec.prophit.service.bean.MaintenceEbillBean;
import evertec.prophit.service.bean.MaintenceEbillResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigInteger;
import java.util.*;

import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, CustomerServices.class, ApiCallerFactory.class, Utils.class,
        DeviceUtils.class, WebUtils.class})
public class EbppStatusUpdateUT extends AbstractModelAndViewTests {

    private EbppStatusUpdate form;

    private MockHttpServletRequest request;

    private HttpSession session;

    private UserSession userSession;

    private CustomerServices customerService;

    private CustomerProfile customerProfile;

    @Mock
    private ApiCaller apiCaller;
    
    private TVFrontEndPayee tVFrontEndPayee;

    private TVFrontendAccount tVFrontendAccount;
    
    private CustomerInteractionEvent customerInteractionEvent;
    
    private ProphITService prophITService;
    
    private TVFrontendDepositAccount idaAccount;

    private final List<TVFrontEndPayee> payeeList = new ArrayList<>();
    private final TVFrontEndPayee frontEndPayee = new TVFrontEndPayee();
    private final List<TVFrontendAccount> accountList = new ArrayList<>();
    private final Account account = new CardAccount();
    private TVFrontendAccount frontendAccount = new TVFrontendAccount(account);
    private final ModelAndView mav = new ModelAndView();

    @Mock
    private CustomerPayeePreferences customerPayeePreferences;

    @Mock
    private CustomerAccountPreferences customerAccountPreferences;

    @Mock
    private EbppStatusUpdateBean bean;

    @Mock
    private CustomerInquiryBean inquiryBean;

    @Mock
    private MaintenceEbillResponse maintenceEbillResponse;

    @Mock
    private HttpServletRequest servletRequest;

    @Mock
    private HttpServletResponse servletResponse;

    @Mock
    private BindException errors;

    @Mock
    private UserSession usrSession;

    @Mock
    private BankingSession bankingSession;

    @Mock
    private CustomerServices customerServices;

    @Mock
    private CustomerProfile profile;

    @Mock
    private MessageSource messageSource;

    @Spy
    @InjectMocks
    private final EbppStatusUpdate controller = new EbppStatusUpdate();

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(ApiCallerFactory.class);
        when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

        PowerMockito.mockStatic(StaticMessageSource.class);
        form = new EbppStatusUpdate();
        request = new MockHttpServletRequest();
        session = new MockHttpSession();
        userSession = new UserSession();
        customerService = mock(CustomerServices.class) ;
        customerProfile = new CustomerProfile();
        tVFrontEndPayee = mock(TVFrontEndPayee.class);
        tVFrontendAccount = mock(TVFrontendAccount.class);
        customerInteractionEvent = mock(CustomerInteractionEvent.class);
        prophITService = mock(ProphITService.class);
        idaAccount = mock(TVFrontendDepositAccount.class);
        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(DeviceUtils.class);
        PowerMockito.mockStatic(WebUtils.class);
    }

    @Test
    public void whenActivateEstament_English_ThenReturn_NotNull() throws  Exception{

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        customerProfile.setProfileId("22");
        Set<CustomerFlag> customerFlagsSet = new HashSet<>();
        CustomerFlag customerFlag  =  new CustomerFlag(CustomerFlagType.PENDING_ACCOUNT_APPLICATION);

        MessageSource messageSource = mock(MessageSource.class);

        when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn(EntitlementStatus.ENABLED.toString());

        when(messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.one", null, new Locale("en"))).thenReturn("At this moment we can't activate your e-Statement");

        when(messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.two", null, new Locale("en"))).thenReturn("  , please try again later. Sorry for the inconvenience.");

        customerFlagsSet.add(customerFlag);
        customerProfile.setCustomerFlags(customerFlagsSet);

        when(Utils.generateCustomerInteractionEvent(Mockito.any())).thenReturn(customerInteractionEvent);
        when(Utils.getAccountById(Mockito.any(), Mockito.any())).thenReturn(tVFrontendAccount);
        when(tVFrontendAccount.getNickname()).thenReturn("");
        when(tVFrontendAccount.getSubtype()).thenReturn("IDA");
        when(Utils.getRSALanguage(any())).thenReturn("en");
        when(idaAccount.getStmtDisposition()).thenReturn("M");
        when(idaAccount.getAccountNumber()).thenReturn("xxxx");
        when(Utils.getIdaAccountById(any(), any())).thenReturn(idaAccount);
      
        BankingSession bankingSession = new BankingSession(BigInteger.ONE,"");

        List<TVFrontendAccount> tvFrontendAccountList = new ArrayList<>();

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.TRUE);

        DepositAccount depositAccount = new DepositAccount();

        depositAccount.setSectionIndicator(null);
        Account accountD = (Account) depositAccount;

        accountD.setSubType("IDA");
        accountD.setAccountID("123");

        Account account = new Account() { };

        account.setSubType("IDA");
        account.setAccountID("123");

        TVFrontendAccount tvFrontendAccount = new TVFrontendAccount(accountD);
        tvFrontendAccount.setFrontEndId("123");

        tvFrontendAccountList.add(tvFrontendAccount);

        List<TVFrontendDepositAccount> tvFrontendDepositAccounts = new ArrayList<>();

        TVFrontendDepositAccount tvFrontendDepositAccount = new TVFrontendDepositAccount(accountD);

        tvFrontendDepositAccount.setStmtDisposition("M");

        tvFrontendDepositAccount.setFrontEndId("123");

        tvFrontendDepositAccounts.add(tvFrontendDepositAccount);

        userSession.setIdaAccounts(tvFrontendDepositAccounts);

        userSession.setBankingSession(bankingSession);

        userSession.setAccounts(tvFrontendAccountList);
        userSession.setEbpp2UserIdLookedUp(true);
        userSession.setEbpp2UserId("123");

        UsernameReservation usernameReservation = new UsernameReservation();

        usernameReservation.setUsername("gdiazlander");

        userSession.setUsernameReservation(usernameReservation);

        List<AccountApplication> accountApplicationList = new ArrayList<>();

        accountApplicationList.add(new AccountApplication());

        AccountApplications accountApplications = mock(AccountApplications.class);
        accountApplications.setCompletedApplications(accountApplicationList);


        when(customerService.getPendingApplications(userSession.getCustomerProfile(),true)).thenReturn(accountApplications);

        when(customerService.getCustomerAccounts(customerProfile,accountApplications,bankingSession,true)).thenReturn(tvFrontendAccountList);

        userSession.setCustomerProfile(customerProfile);
        userSession.setCustomerService(customerService);
        session.setAttribute("userSession",userSession);
        request.setSession(session);

        form.setMessageSource(messageSource);

        HttpServletResponse response = mock(HttpServletResponse.class);

        EbppStatusUpdateBean ebppStatusUpdateBean = new EbppStatusUpdateBean();
        ebppStatusUpdateBean.setAction(EbppStatusUpdateBean.ACTION.UPDATE_ESTMT_STATUS);
        ebppStatusUpdateBean.setId("123");

        BindException errors = new BindException(ebppStatusUpdateBean, "EbppStatusUpdateBean");

        ModelAndView model = form.onSubmit(request,response,ebppStatusUpdateBean,errors);

        assertEquals("At this moment we can't activate your e-Statement  , please try again later. Sorry for the inconvenience.",userSession.getStatusmessage());

        assertEquals(model.getModelMap().get("completeprocess"), true);
    }

    @Test
    public void whenActivateEstament_Spanish_ThenReturn_NotNull() throws  Exception{

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        customerProfile.setProfileId("22");
        Set<CustomerFlag> customerFlagsSet = new HashSet<>();
        CustomerFlag customerFlag  =  new CustomerFlag(CustomerFlagType.PENDING_ACCOUNT_APPLICATION);

        customerFlagsSet.add(customerFlag);
        customerProfile.setCustomerFlags(customerFlagsSet);

        BankingSession bankingSession = new BankingSession(BigInteger.ONE,"");

        List<TVFrontendAccount> tvFrontendAccountList = new ArrayList<>();

        MessageSource messageSource = mock(MessageSource.class);


        when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn(" ");

        when(messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.one", null, new Locale("es"))).thenReturn("En estos momentos no podemos activar el Estado de Cuenta electronico de");

        when(messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.two", null, new Locale("es"))).thenReturn("  , por favor intenta mas tarde. Disculpa los inconvenientes. ");

        when(Utils.generateCustomerInteractionEvent(Mockito.any())).thenReturn(customerInteractionEvent);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.TRUE);

        when(Utils.getAccountById(Mockito.any(), Mockito.any())).thenReturn(tVFrontendAccount);
        when(tVFrontendAccount.getNickname()).thenReturn("");
        when(tVFrontendAccount.getSubtype()).thenReturn("IDA");
        when(Utils.getRSALanguage(any())).thenReturn("es");
        when(idaAccount.getStmtDisposition()).thenReturn("M");
        when(idaAccount.getAccountNumber()).thenReturn("xxxx");
        when(Utils.getIdaAccountById(any(), any())).thenReturn(idaAccount);
        DepositAccount depositAccount = new DepositAccount();

        depositAccount.setSectionIndicator(null);
        Account accountD = (Account) depositAccount;

        accountD.setSubType("IDA");
        accountD.setAccountID("123");


        Account account = new Account() { };

        account.setSubType("IDA");
        account.setAccountID("123");

        TVFrontendAccount tvFrontendAccount = new TVFrontendAccount(accountD);
        tvFrontendAccount.setFrontEndId("123");

        tvFrontendAccountList.add(tvFrontendAccount);

        List<TVFrontendDepositAccount> tvFrontendDepositAccounts = new ArrayList<>();

        TVFrontendDepositAccount tvFrontendDepositAccount = new TVFrontendDepositAccount(accountD);

        tvFrontendDepositAccount.setCustomerAccountPreferences(new CustomerAccountPreferences());

        tvFrontendDepositAccount.setStmtDisposition("M");

        tvFrontendDepositAccount.setFrontEndId("123");

        tvFrontendDepositAccount.setNickname("Cuenta Pago AAA");

        tvFrontendDepositAccounts.add(tvFrontendDepositAccount);

        userSession.setIdaAccounts(tvFrontendDepositAccounts);

        userSession.setBankingSession(bankingSession);

        userSession.setAccounts(tvFrontendAccountList);
        userSession.setEbpp2UserIdLookedUp(true);
        userSession.setEbpp2UserId("123");

        UsernameReservation usernameReservation = new UsernameReservation();

        usernameReservation.setUsername("gdiazlander");

        userSession.setUsernameReservation(usernameReservation);

        List<AccountApplication> accountApplicationList = new ArrayList<>();

        accountApplicationList.add(new AccountApplication());

        AccountApplications accountApplications = mock(AccountApplications.class);
        accountApplications.setCompletedApplications(accountApplicationList);


        when(customerService.getPendingApplications(userSession.getCustomerProfile(),true)).thenReturn(accountApplications);

        when(customerService.getCustomerAccounts(customerProfile,accountApplications,bankingSession,true)).thenReturn(tvFrontendAccountList);

        userSession.setCustomerProfile(customerProfile);
        userSession.setCustomerService(customerService);
        session.setAttribute("userSession",userSession);
        request.setSession(session);

        form.setMessageSource(messageSource);

        HttpServletResponse response = mock(HttpServletResponse.class);

        EbppStatusUpdateBean ebppStatusUpdateBean = new EbppStatusUpdateBean();
        ebppStatusUpdateBean.setAction(EbppStatusUpdateBean.ACTION.UPDATE_ESTMT_STATUS);
        ebppStatusUpdateBean.setId("123");

        BindException errors = new BindException(ebppStatusUpdateBean, "EbppStatusUpdateBean");


        request.addPreferredLocale(Locale.ROOT);


        ModelAndView model = form.onSubmit(request,response,ebppStatusUpdateBean,errors);

        assertEquals("En estos momentos no podemos activar el Estado de Cuenta electronico de  , por favor intenta mas tarde. Disculpa los inconvenientes. ",userSession.getStatusmessage());

        assertEquals(model.getModelMap().get("completeprocess"), true);
    }
    
    @Test
    public void whenActivateCCA_ThenReturnCompleteProcessTrue() throws Exception{

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        Set<CustomerFlag> customerFlagsSet = new HashSet<>();
        CustomerFlag customerFlag  =  new CustomerFlag(CustomerFlagType.PENDING_ACCOUNT_APPLICATION);

        MessageSource messageSource = mock(MessageSource.class);
        
        when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn(EntitlementStatus.ENABLED.toString());

        when(messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.one", null, new Locale("en"))).thenReturn("At this moment we can't activate your e-Statement");

        when(messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.two", null, new Locale("en"))).thenReturn("  , please try again later. Sorry for the inconvenience.");
        
        customerFlagsSet.add(customerFlag);
        customerProfile.setCustomerFlags(customerFlagsSet);

        BankingSession bankingSession = new BankingSession(BigInteger.ONE,"");

        List<TVFrontendAccount> tvFrontendAccountList = new ArrayList<>();
        when(Utils.getAccountById(Mockito.any(), Mockito.any())).thenReturn(tVFrontendAccount);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.TRUE);

        DepositAccount depositAccount = new DepositAccount();

        depositAccount.setSectionIndicator(null);
        Account accountD = (Account) depositAccount;

        accountD.setSubType("CCA");
        accountD.setAccountID("123");

        Account account =mock(Account.class);

        account.setSubType("CCA");
        account.setAccountID("123");

        TVFrontendAccount tvFrontendAccount = new TVFrontendAccount(accountD);
        tvFrontendAccount.setFrontEndId("123");

        tvFrontendAccountList.add(tvFrontendAccount);



        userSession.setBankingSession(bankingSession);

        userSession.setAccounts(tvFrontendAccountList);
        userSession.setEbpp2UserIdLookedUp(true);
        userSession.setEbpp2UserId("123");
        userSession.setCcaAvailbleToEbill(tvFrontendAccountList);
        UsernameReservation usernameReservation = new UsernameReservation();

        usernameReservation.setUsername("gdiazlander");

        userSession.setUsernameReservation(usernameReservation);

        List<AccountApplication> accountApplicationList = new ArrayList<>();

        accountApplicationList.add(new AccountApplication());

        AccountApplications accountApplications = mock(AccountApplications.class);
        accountApplications.setCompletedApplications(accountApplicationList);


        when(customerService.getPendingApplications(userSession.getCustomerProfile(),true)).thenReturn(accountApplications);

        when(customerService.getCustomerAccounts(customerProfile,accountApplications,bankingSession,true)).thenReturn(tvFrontendAccountList);

        userSession.setCustomerProfile(customerProfile);
        userSession.setCustomerService(customerService);
        session.setAttribute("userSession",userSession);
        request.setSession(session);

        form.setMessageSource(messageSource);
       
        
        HttpServletResponse response = mock(HttpServletResponse.class);
        when( Utils.getPayeeisCCA(Mockito.any(),Mockito.any())).thenReturn(true);
        when(Utils.getPayeebyId(Mockito.any(),Mockito.any())).thenReturn(tVFrontEndPayee);
        when(tVFrontendAccount.getAccountNumber()).thenReturn("123456789");
        when(Utils.getAccountByAccountNumber(Mockito.any(),Mockito.any())).thenReturn(tVFrontendAccount);
        when(DeviceUtils.getBrowserInfo(Mockito.any(),Mockito.any())).thenReturn(DeviceUtils.BROWSER_CHROME);
        when(Utils.generateCustomerInteractionEvent(Mockito.any())).thenReturn(customerInteractionEvent);
        when(tVFrontendAccount.getSubtype()).thenReturn("CCA");
        when(customerService.prophItService()).thenReturn(prophITService);
        CustomerInquiryBean customerInquiryBean = new CustomerInquiryBean();
        customerInquiryBean.setEbillInd(EbillIndType.ELIGIBLE.toString());
        when(customerService.prophItService().makeCustomerInquiry(Mockito.any())).thenReturn(customerInquiryBean);
        EbppStatusUpdateBean ebppStatusUpdateBean = new EbppStatusUpdateBean();
        ebppStatusUpdateBean.setAction(EbppStatusUpdateBean.ACTION.UPDATE_ESTMT_STATUS);
        ebppStatusUpdateBean.setId("123");

        BindException errors = new BindException(ebppStatusUpdateBean, "EbppStatusUpdateBean");

        ModelAndView model = form.onSubmit(request,response,ebppStatusUpdateBean,errors);

        assertEquals(model.getModelMap().get("completeprocess"), true);
    }
    
    @Test
    public void whenDesActivateCCA_ThenReturnCompleteProcessTrue() throws Exception{

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        Set<CustomerFlag> customerFlagsSet = new HashSet<>();
        CustomerFlag customerFlag  =  new CustomerFlag(CustomerFlagType.PENDING_ACCOUNT_APPLICATION);

        MessageSource messageSource = mock(MessageSource.class);
        
        when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn(EntitlementStatus.ENABLED.toString());
        when(messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.one", null, new Locale("en"))).thenReturn("At this moment we can't activate your e-Statement");

        when(messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.two", null, new Locale("en"))).thenReturn("  , please try again later. Sorry for the inconvenience.");
        
        customerFlagsSet.add(customerFlag);
        customerProfile.setCustomerFlags(customerFlagsSet);

        BankingSession bankingSession = new BankingSession(BigInteger.ONE,"");

        List<TVFrontendAccount> tvFrontendAccountList = new ArrayList<>();
        when(Utils.getAccountById(Mockito.any(), Mockito.any())).thenReturn(tVFrontendAccount);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.TRUE);

        DepositAccount depositAccount = new DepositAccount();

        depositAccount.setSectionIndicator(null);
        Account accountD = (Account) depositAccount;

        accountD.setSubType("CCA");
        accountD.setAccountID("123");

        Account account =mock(Account.class);

        account.setSubType("CCA");
        account.setAccountID("123");

        TVFrontendAccount tvFrontendAccount = new TVFrontendAccount(accountD);
        tvFrontendAccount.setFrontEndId("123");

        tvFrontendAccountList.add(tvFrontendAccount);



        userSession.setBankingSession(bankingSession);

        userSession.setAccounts(tvFrontendAccountList);
        userSession.setEbpp2UserIdLookedUp(true);
        userSession.setEbpp2UserId("123");
        userSession.setCcaAvailbleToEbill(tvFrontendAccountList);
        UsernameReservation usernameReservation = new UsernameReservation();

        usernameReservation.setUsername("gdiazlander");

        userSession.setUsernameReservation(usernameReservation);

        List<AccountApplication> accountApplicationList = new ArrayList<>();

        accountApplicationList.add(new AccountApplication());

        AccountApplications accountApplications = mock(AccountApplications.class);
        accountApplications.setCompletedApplications(accountApplicationList);


        when(customerService.getPendingApplications(userSession.getCustomerProfile(),true)).thenReturn(accountApplications);

        when(customerService.getCustomerAccounts(customerProfile,accountApplications,bankingSession,true)).thenReturn(tvFrontendAccountList);

        userSession.setCustomerProfile(customerProfile);
        userSession.setCustomerService(customerService);
        session.setAttribute("userSession",userSession);
        request.setSession(session);

        form.setMessageSource(messageSource);
       
        
        HttpServletResponse response = mock(HttpServletResponse.class);
        when( Utils.getPayeeisCCA(Mockito.any(),Mockito.any())).thenReturn(true);
        when(Utils.getPayeebyId(Mockito.any(),Mockito.any())).thenReturn(tVFrontEndPayee);
        when(tVFrontendAccount.getAccountNumber()).thenReturn("123456789");
        when(Utils.getAccountByAccountNumber(Mockito.any(),Mockito.any())).thenReturn(tVFrontendAccount);
        when(DeviceUtils.getBrowserInfo(Mockito.any(),Mockito.any())).thenReturn(DeviceUtils.BROWSER_CHROME);
        when(Utils.generateCustomerInteractionEvent(Mockito.any())).thenReturn(customerInteractionEvent);
        when(tVFrontendAccount.getSubtype()).thenReturn("CCA");
        when(customerService.prophItService()).thenReturn(prophITService);
        CustomerInquiryBean customerInquiryBean = new CustomerInquiryBean();
        customerInquiryBean.setEbillInd(EbillIndType.ACTIVE.toString());
        when(customerService.prophItService().makeCustomerInquiry(Mockito.any())).thenReturn(customerInquiryBean);
        EbppStatusUpdateBean ebppStatusUpdateBean = new EbppStatusUpdateBean();
        ebppStatusUpdateBean.setAction(EbppStatusUpdateBean.ACTION.UPDATE_ESTMT_STATUS);
        ebppStatusUpdateBean.setId("123");

        BindException errors = new BindException(ebppStatusUpdateBean, "EbppStatusUpdateBean");

        ModelAndView model = form.onSubmit(request,response,ebppStatusUpdateBean,errors);

        assertEquals(model.getModelMap().get("completeprocess"), true);
    }

    @Test
    public void whenOnSubmit_GivenActivateEbill_ThenRegisterEvent() throws Exception {

        callTestEbill(EbillIndType.ACTIVE);
        PowerMockito.verifyStatic(Utils.class);
        Utils.registerStatusEbillHistoryLog(any(HttpServletRequest.class), anyString(), any(UserSession.class),
                any(CustomerServices.class), anyString());
    }

    @Test
    public void whenOnSubmit_GivenDeactivateEbill_ThenRegisterEvent() throws Exception {

        callTestEbill(EbillIndType.DEACTIVE);
        PowerMockito.verifyStatic(Utils.class);
        Utils.registerStatusEbillHistoryLog(any(HttpServletRequest.class), anyString(), any(UserSession.class),
                any(CustomerServices.class), anyString());
    }

    private void callTestEbill(EbillIndType ebillIndType) throws Exception {
        Payee payee = new Payee();
        payee.setBillingAccount("4654654654");
        frontEndPayee.setCustomerPayeePreferences(customerPayeePreferences);
        frontEndPayee.setBackendObject(payee);
        payeeList.add(frontEndPayee);

        account.setAccountID("54554654654");
        frontendAccount = new TVFrontendAccount(account);
        frontendAccount.setBackendObject(account);
        accountList.add(frontendAccount);

        usrSession = mock(UserSession.class);
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(usrSession);
        when(usrSession.getCustomerProfile()).thenReturn(customerProfile);
        when(usrSession.getCustomerService()).thenReturn(customerService);
        when(usrSession.getBankingSession()).thenReturn(bankingSession);
        request.setParameter("showInsidePortal", "false");
        request.setParameter("origId", "1234");
        when(usrSession.isEbpp2UserIdLookedUp()).thenReturn(true);
        when(usrSession.getEbpp2UserId()).thenReturn("123456");
        when(customerPayeePreferences.getNickname()).thenReturn("VISA PREMIA");
        when(usrSession.getPayees(true)).thenReturn(payeeList);
        when(Utils.getPayeebyId(anyString(), any(List.class))).thenReturn(frontEndPayee);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(true);
        callValidations(ebillIndType);
        callValidationsTest();

        HttpServletResponse response = mock(HttpServletResponse.class);
        EbppStatusUpdateBean ebppStatusUpdateBean = new EbppStatusUpdateBean();
        ebppStatusUpdateBean.setAction(EbppStatusUpdateBean.ACTION.UPDATE_EBILL_STATUS);
        ebppStatusUpdateBean.setId("123");
        ebppStatusUpdateBean.setEmail(null);
        BindException errors = new BindException(ebppStatusUpdateBean, "EbppStatusUpdateBean");
        session.setAttribute("userSession",usrSession);
        request.setSession(session);

        form.onSubmit(request,response,ebppStatusUpdateBean,errors);
    }

    private void callValidations(EbillIndType ebillIndType) throws Exception {
        when(Utils.getPayeeisCCA(anyString(), any(UserSession.class))).thenReturn(true);
        when(usrSession.getAccounts()).thenReturn(accountList);
        when(Utils.getAccountByAccountNumber(anyString(), any(List.class))).thenReturn(frontendAccount);
        when(customerService.prophItService()).thenReturn(prophITService);
        when(prophITService.makeCustomerInquiry(anyString())).thenReturn(inquiryBean);
        when(inquiryBean.getEbillInd()).thenReturn(ebillIndType.toString());
        when(inquiryBean.getCaseNumber()).thenReturn("5466");
        when(inquiryBean.getChannelId()).thenReturn("665");
        when(inquiryBean.getImpersonateId()).thenReturn("1234");
        when(inquiryBean.getKeepCaseOpen()).thenReturn(true);
        when(inquiryBean.getNewContact()).thenReturn(true);
        when(inquiryBean.getOrgId()).thenReturn("47879");
        when(inquiryBean.getOrgLevel()).thenReturn("7");
    }

    private void callValidationsTest() throws Exception {
        PowerMockito.when(prophITService.maintenceEBill(any(MaintenceEbillBean.class))).thenReturn(maintenceEbillResponse);
        when(maintenceEbillResponse.getMessage()).thenReturn("Successfull");
        PowerMockito.when(Utils.getRSALanguage(request)).thenReturn("en");
        PowerMockito.doReturn(Boolean.TRUE).when(customerService).sendEbillsCCAEmailActivationSuccess(customerProfile,
                PreferredLanguage.SPANISH, "VISA PREMIA");
        customerProfile.setPermId(new BigInteger("4654654132132165"));
        customerProfile.setTaxId("555321456");
        customerProfile.setUsername("gallo");
        when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerInteractionEvent);
        PowerMockito.doNothing().when(customerService).logUpdateStatementStatus(customerInteractionEvent,
                InteractionSeverity.INFO);
        PowerMockito.doNothing().when(Utils.class, "registerStatusEbillHistoryLog", any(HttpServletRequest.class),
                anyString(), any(UserSession.class), any(CustomerServices.class), any(EStatementInteraction.class));
    }

    @Test
    public void whenOnSubmit_GivenActivateEstatement_ThenRegisterEvent() throws Exception {

        callTestEstatement(EbillIndType.ACTIVE);
        PowerMockito.verifyStatic(Utils.class);
        Utils.registerStatusEbillHistoryLog(any(HttpServletRequest.class), anyString(), any(UserSession.class),
                any(CustomerServices.class), anyString());
    }

    @Test
    public void whenOnSubmit_GivenDeactivateEstatement_ThenRegisterEvent() throws Exception {

        callTestEstatement(EbillIndType.DEACTIVE);
        PowerMockito.verifyStatic(Utils.class);
        Utils.registerStatusEbillHistoryLog(any(HttpServletRequest.class), anyString(), any(UserSession.class),
                any(CustomerServices.class), anyString());
    }

    private void callTestEstatement(EbillIndType ebillIndType) throws Exception {

        account.setAccountID("54554654654");
        account.setSubType("CCA");
        frontendAccount = new TVFrontendAccount(account);
        frontendAccount.setCustomerAccountPreferences(customerAccountPreferences);
        customerPayeePreferences.setNickname("MASTERCARD BLACK");
        frontendAccount.setBackendObject(account);
        accountList.add(frontendAccount);

        usrSession = mock(UserSession.class);
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(usrSession);
        when(usrSession.getCustomerProfile()).thenReturn(customerProfile);
        when(usrSession.getCustomerService()).thenReturn(customerService);
        when(usrSession.getBankingSession()).thenReturn(bankingSession);
        request.setParameter("showInsidePortal", "false");
        request.setParameter("origId", "1234");
        when(usrSession.isEbpp2UserIdLookedUp()).thenReturn(true);
        when(usrSession.getEbpp2UserId()).thenReturn("123456");
        when(customerPayeePreferences.getNickname()).thenReturn("VISA PREMIA");
        when(usrSession.getPayees(true)).thenReturn(payeeList);
        when(Utils.getAccountById(anyString(), any(List.class))).thenReturn(frontendAccount);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(true);
        callValidations(ebillIndType);
        callValidationsTest();

        HttpServletResponse response = mock(HttpServletResponse.class);
        EbppStatusUpdateBean ebppStatusUpdateBean = new EbppStatusUpdateBean();
        ebppStatusUpdateBean.setAction(EbppStatusUpdateBean.ACTION.UPDATE_ESTMT_STATUS);
        ebppStatusUpdateBean.setId("123");
        ebppStatusUpdateBean.setEmail(null);
        BindException errors = new BindException(ebppStatusUpdateBean, "EbppStatusUpdateBean");
        session.setAttribute("userSession",usrSession);
        request.setSession(session);

        form.onSubmit(request,response,ebppStatusUpdateBean,errors);
    }
}
