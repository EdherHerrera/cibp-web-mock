package evertec.cibp.web.form.insurance;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.form.insurance.bean.FamilyCareBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.Date;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StaticMessageSource.class, FamilyCareForm.class, WebUtils.class, Utils.class })
public class FamilyCareFormUT extends AbstractModelAndViewTests {

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private FamilyCareBean bean;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private UserSession userSession;

	@Mock
	private BankingSession bankingSession;

	@Mock
	private BindException errors;

	@InjectMocks
	private FamilyCareForm controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Utils.class);
	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		Integer maxAge = 2025;
		Integer minAge = 2021;
		controller.setMinAge(minAge);
		controller.setMaxAge(maxAge);

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(request.getParameter("nothanks")).thenReturn(null);
		when(userSession.getBankingSession()).thenReturn(new BankingSession(
				new BigInteger("546544"), "asfasfasdf654654"));
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(Utils.getRSALanguage(request)).thenReturn("en");

		Map<String, Object> result = controller.referenceData(request, bean, errors, 2);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when ProcessFinish
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenProcessFinish_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		Integer maxAge = 2025;
		Integer minAge = 2021;
		controller.setMinAge(minAge);
		controller.setMaxAge(maxAge);

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(request.getParameter("birthday_day")).thenReturn("28");
		when(request.getParameter("birthday_month")).thenReturn("09");
		when(request.getParameter("birthday_year")).thenReturn("2020");
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(bankingSession.getPermId()).thenReturn(new BigInteger("5465465456"));
		when(customerService.getDataCustomerProfile(any())).thenReturn(null);
		when(bean.getCustomerAge()).thenReturn(2020);
		when(Utils.getRSALanguage(request)).thenReturn("en");

		ModelAndView result = controller.processFinish(request, response, bean, errors);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}
}
