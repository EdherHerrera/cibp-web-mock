package evertec.cibp.web.form;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ATHMUpdatePhoneBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Unit testing ATHMUpdatePhoneForm java class.
 * @author Evertec ET58344
 * @since 09-15-2020
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CustomerFacade.class, StaticMessageSource.class, ATHMUpdatePhoneForm.class, Utils.class, ApiCallerFactory.class})
public class ATHMUpdatePhoneFormUT extends AbstractModelAndViewTests  {

	private MockHttpServletRequest request;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerFacade customerFacade;

	@Mock
	private HttpServletRequest requestValue;

	@Mock
	private ATHMUpdatePhoneBean athmUpdatePhoneBean;

	@Mock
	private Errors errors;

	@Mock
	private ApiCaller apiCaller;


	@Spy
	private final ATHMUpdatePhoneForm spyController = new ATHMUpdatePhoneForm();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

		request = new MockHttpServletRequest();
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);

		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-11-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		int page = 3;

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(Utils.getRSALanguage(any(HttpServletRequest.class))).thenReturn("es");
		when(athmUpdatePhoneBean.getCurrentPhone()).thenReturn(null);

		Map<String, Object> result = spyController.referenceData(requestValue, athmUpdatePhoneBean, errors, page);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when ProceessCancelFinishResult
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-11-2020
	 * @exception Exception error
	 */
	@Test
	public void whenProceessCancelFinishResult_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);

		ModelAndView modelAndView = Whitebox.invokeMethod(spyController, "proceessCancelFinishResult", request);
		assertEquals(Boolean.TRUE, modelAndView.getModel().get("cookieAnalyticEnabled"));
	}
}
