package evertec.cibp.web.form.outreach;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.interruptions.OOBInterruptionForm;
import evertec.cibp.web.form.outreach.bean.OutReachBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ WebUtils.class, OOBInterruptionForm.class, Utils.class })
public class OutReachFormUT {
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private OutReachBean command;
	
	@Mock
	private BindException error;
	
	@InjectMocks
	private OutReachForm outReachForm;
	
	@Before
	public void setUp() throws Exception {
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(OOBInterruptionForm.class);
		PowerMockito.mockStatic(Utils.class);
		
		outReachForm = Mockito.spy(outReachForm);
	}

	@Test
	public void whenHandleRequest_givenOOBInterruptionPage_thenDisplayOobInterruption() throws ServletException, IOException, Exception {
		ModelAndView oobInterruption = new ModelAndView();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(OOBInterruptionForm.applyCompatibilityWithOuchReach(any())).thenReturn(oobInterruption);
		
		ModelAndView modelView = outReachForm.handleRequest(request, response);
		assertEquals(oobInterruption, modelView);
	}
	
	@Test
	public void whenIsMobile_givenIphoneUserAgent_thenReturnTrue() throws Exception {
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
		boolean result = Whitebox.invokeMethod(outReachForm, "isMobile", userSession, "WebView");
		assertTrue(result);
	}
	
	@Test
	public void whenIsMobile_givenAndroidUserAgent_thenReturnTrue() throws Exception {
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.ANDROID);
		boolean result = Whitebox.invokeMethod(outReachForm, "isMobile", userSession, "WebView");
		assertTrue(result);
	}
	
	@Test
	public void whenIsMobile_givenJsonUserAgent_thenReturnFalse() throws Exception {
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
		boolean result = Whitebox.invokeMethod(outReachForm, "isMobile", userSession, "WebView");
		assertFalse(result);
	}

}
