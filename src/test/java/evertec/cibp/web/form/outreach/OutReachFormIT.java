package evertec.cibp.web.form.outreach;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import evertec.cibp.web.form.interruptions.OOBInterruptionForm;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Utils.class, WebUtils.class, RSAUtils.class })
public class OutReachFormIT {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Mock
	UserSession userSession;

	@Mock
	OOBInterruptionForm oobInterruptionForm;

	@InjectMocks
	OutReachForm outReachForm;

	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(RSAUtils.class);
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		when(Utils.getUserSession(any())).thenReturn(userSession);

	}

	@SuppressWarnings("static-access")
	@Test
	public void whenHandleRequest_GivenIsNotElegibleOutReach_ThenOutReach()
			throws ServletException, IOException, Exception {
		when(RSAUtils.isOOBInterruption(any())).thenReturn(false);

		ModelAndView modelAndView = outReachForm.handleRequest(request, response);
		assertNotNull(modelAndView);
		assertTrue(modelAndView.getViewName().toString().equals("redirect:outreach?close"));
		verify(oobInterruptionForm, times(1)).applyCompatibilityWithOuchReach(any());
	}

	@SuppressWarnings("static-access")
	@Test
	public void whenHandleRequest_GivenIsElegibleOutReach_Then() throws ServletException, IOException, Exception {
		when(RSAUtils.isOOBInterruption(any())).thenReturn(false);
		when(userSession.isElegibleToOutreach()).thenReturn(true);
		when(userSession.isAlreadyShownOutreach()).thenReturn(false);
		request.setMethod("GET");

		ModelAndView modelAndView = outReachForm.handleRequest(request, response);
		assertNotNull(modelAndView);
		assertTrue(modelAndView.getViewName().toString().equals("outreach"));
		verify(oobInterruptionForm, times(0)).applyCompatibilityWithOuchReach(any());
	}

}
