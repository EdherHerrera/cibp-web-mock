package evertec.cibp.web.form;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import bppr.tv.beans.accounts.types.CardAccount;
import bppr.tv.beans.balances.Balance;
import evertec.cibp.core.models.account.TVFrontendCreditAccount;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.web.response.AccountTv;
import evertec.dp.service.modules.CustomerService;
import jdk.nashorn.internal.objects.Global;
import org.apache.commons.validator.GenericValidator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.AccountRecord;
import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import bppr.tv.beans.accounts.records.DepositAccountRecord;
import bppr.tv.beans.accounts.types.DepositAccount;
import bppr.tv.beans.accounts.types.LoanAccount;
import bppr.tv.beans.balances.Balance;
import evertec.cibp.core.models.account.AccountFeatures;
import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.*;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.vault.VaultAccount;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.TransactionRequest;
import evertec.cibp.core.services.responses.PremiaTransactionResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CreditCardFacade;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.TransactionFormBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.CookieUtils;
import evertec.cibp.web.utils.TransactionUtil;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Utils.class, WebUtils.class, StaticMessageSource.class, CustomerServices.class,
		CustomerProfile.class, CustomerProfileService.class, TVFrontendAccount.class,  BankingSession.class,
		TransactionsResponse.class, CustomerFacade.class, GenericValidator.class, TransactionRequest.class,
		TVAccountProductDefinition.class, TransactionForm.class, CookieUtils.class, Cookie.class, EntitlementStatus.class,
		TransactionUtil.class, AccountUtils.class, CreditCardFacade.class,})
		

public class TransactionFormUT extends AbstractModelAndViewTests{

	private static final String VIEW = "transaction";
	
	private List<AbstractTransaction> abstractTransactionList;
	private List<AbstractTransaction> abstractTransactionListPRM;
	private List<TVFrontendAccount> userAccounts;
    private TVFrontendAccount tvFrontendAccount;

    @Mock
    private HttpServletRequest request;

	private MockHttpServletResponse response;	
	
	@Mock
    private CreditCardFacade creditCardFacade;

    @Mock
    private StaticMessageSource staticMessageSource;

    @Mock
    private MessageSource messageSource;

    @Mock
    private HashSet<String> transCode;

    @Mock
    private UserSession userSession;

    @Mock
    private TransactionFormBean transactionFormBean;

    @Mock
    private TVFrontendAccount account;

    @Mock
    private TVAccountProductDefinition productDefinition;
    //
    @Mock
    private BindException bindException;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private BankingSession bankingSession;

    @Mock
    private CustomerServices customerServices;

    @Mock
    private TransactionFormBean transaction;

    @Mock
    private    javax.servlet.http.Cookie versionCookie;

    @Mock
    private    TransactionsResponse transactionResponse;

    @Mock
    private TVAccountProductDefinition tvAccountProductDefinition;

    @Mock
    private    AbstractStatement availableStatement;

    @Mock
    private CustomerFacade customerFacade;

    @Mock
    TransactionRequest transactionRequest;

    @Mock
    private GlobalEntitlement globalEntitlement;

    @Mock
    private CustomerServices customerService;

    @Mock
    private BindException errors;

    @InjectMocks
    private TransactionForm transactionForm;

	@Mock
    private PremiaTransactionResponse premiaTransactionResponse;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(GenericValidator.class);
        PowerMockito.mockStatic(CookieUtils.class);
        PowerMockito.mockStatic(EntitlementStatus.class);
        PowerMockito.mockStatic(CustomerFacade.class);
        PowerMockito.mockStatic(Cookie.class);
		PowerMockito.mockStatic(TransactionUtil.class);
		PowerMockito.mockStatic(CreditCardFacade.class);
	    PowerMockito.mockStatic(AccountUtils.class);

        HashSet<String> codes = new HashSet<String>() {
            /**
             *
             */
            private static final long serialVersionUID = 6582118491344902064L;

            {
                add("VSELT");
                add("APAXI");
                add("VWREG");
                add("VWVSI");
                add("APREG");
                add("UAREG");
                add("UVREG");
                add("UVVSI");
                add("VSICN");
                add("VSNOV");
                add("VSRTN");
                add("VSSEC");
                add("UVNOV");
                add("AEELT");
                add("AEICN");
                add("AEULT");
                add("UAULT");
                add("MCEDG");
                add("UMEDG");
                add("VWASI");
                add("VWAIN");
                add("MWAWR");
                add("VWVSI");
                add("VPPRE");
                add("VPCBK");
                add("VWAIP");
                add("MWREG");
                add("MGJBC");
                add("MWJBB");
                add("MGJBG");
            }
        };

        transCode = codes;
       String transactionPOSCodes = "622,625,675,679";
		String transactionEFTCodes = "518-98,581-98,618-98,681-96,681-98";
		String transactionATHWithdrawalCodes = "615-0,671-0,676-0,615-145,671-145";
		String transactionATHMovilCodes = "615-214,671-214,676-214";
		String transactionTypes = "PURCHASE,ATH WITHDRAWAL,EFT";
		transactionForm = PowerMockito.spy(new TransactionForm(24, 50,
                "VSELT UAREG UAULT UMEDG UVNOV UVREG UVVSI", false,
                "078,089", transactionPOSCodes,transactionEFTCodes, transactionATHWithdrawalCodes,
                transactionATHMovilCodes, transactionTypes));
        transactionForm.setMessageSource(messageSource);
        transactionForm.setUnversalProducts("VSELT UAREG UAULT UMEDG UVNOV UVREG UVVSI");
        when(AccountUtils.getIsTransactional(any())).thenReturn("true");
        request = PowerMockito.mock(HttpServletRequest.class);
    }

    @Test
    public void whenGetImagePath_GivenProductIdMGJBCAndLanguageEN_ThenReturnImagePath() throws Exception {
        String imgPath = null;
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(true);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("MGJBC");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/jetBlueCard.png");

        imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/jetBlueCard.png");
    }

    @Test
    public void whenGetImagePath_GivenProductIdMWJBBAndLanguageEN_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(true);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("MWJBB");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/jetBlueEleva.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/jetBlueEleva.png");
    }

    @Test
    public void whenGetImagePath_GivenProductIdMGJBGAndLanguageEN_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(true);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("MGJBG");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/jetBlueMastercard.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/jetBlueMastercard.png");
    }

    @Test
    public void whenGetImagePath_GivenProductIdMGJBCAndLanguageES_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(true);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("MGJBC");
        when(userSession.getLanguage()).thenReturn("ES");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/jetBlueCard.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/jetBlueCard.png");
    }

    @Test
    public void whenGetImagePath_GivenProductIdMWJBBAndLanguageES_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(true);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("MWJBB");
        when(userSession.getLanguage()).thenReturn("ES");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/jetBlueEleva.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/jetBlueEleva.png");
    }

    @Test
    public void whenGetImagePath_GivenProductIdMGJBGAndLanguageES_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(true);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("MGJBG");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/jetBlueMastercard.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/jetBlueMastercard.png");
    }

    @Test
    public void whenGetImagePath_GivenProductIdMGJBCAndLanguageENAndFlagFalse_ThenReturnImagePath() throws Exception {
        String imgPath = null;
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(false);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("MGJBC");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/jetBlueCard.png");

        imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/jetBlueCard.png");
    }

    @Test
    public void whenGetImagePath_GivenProductIdMWJBBAndLanguageENAndFlagFalse_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(false);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("MWJBB");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/jetBlueEleva.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/jetBlueEleva.png");
    }

    @Test
    public void whenGetImagePath_GivenProductIdMGJBGAndLanguageENAndFlagFalse_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(false);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("MGJBG");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/jetBlueMastercard.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/jetBlueMastercard.png");
    }

    @Test
    public void whenGetImagePath_GivenProductIdMGJBCAndLanguageESAndFlagFalse_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(false);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("MGJBC");
        when(userSession.getLanguage()).thenReturn("ES");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/jetBlueCard.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/jetBlueCard.png");
    }

    @Test
    public void whenGetImagePath_GivenProductIdMWJBBAndLanguageESAndFlagFalse_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(false);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("MWJBB");
        when(userSession.getLanguage()).thenReturn("ES");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/jetBlueEleva.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/jetBlueEleva.png");
    }

    @Test
    public void whenGetImagePath_GivenProductIdMGJBGAndLanguageESAndFlagFalse_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(false);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("MGJBG");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/jetBlueMastercard.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/jetBlueMastercard.png");
    }


    @Test
    public void whenGetImagePath_GivenProductDescriptionPremiaBusinessRewardsSecuredAndLanguageESAndFlagTrue_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(true);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("VBSEC");
        when(userSession.getLanguage()).thenReturn("ES");

        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/businessRewardSecured.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/businessRewardSecured.png");
    }

    @Test
    @Ignore("WITH ERROR")
    public void whenGetImagePath_GivenProductDescriptionPremiaBusinessRewardsAndLanguageESAndFlagTrue_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(true);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("VBSG1");
        when(userSession.getLanguage()).thenReturn("ES");

        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/businessRewards.png");


        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/businessRewards.png");
    }


    @Test
    @Ignore("WITH ERROR")
    public void whenGetImagePath_GivenProductDescriptionPremiaBusinessRewardsMaxAndLanguageESAndFlagTrue_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(true);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("VBSG2");
        when(userSession.getLanguage()).thenReturn("ES");

        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/businessMaxRewards.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/businessMaxRewards.png");
    }




    @Test
    public void whenGetImagePath_GivenProductDescriptionPremiaBusinessRewardsSecuredAndLanguageESAndFlagFalse_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(false);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("VBSEC");
        when(userSession.getLanguage()).thenReturn("ES");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/businessRewardSecured.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/businessRewardSecured.png");
    }

    @Test
    @Ignore("WITH ERROR")
    public void whenGetImagePath_GivenProductDescriptionPremiaBusinessRewardsAndLanguageESAndFlagFalse_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(false);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("VBSG1");
        when(userSession.getLanguage()).thenReturn("ES");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/businessRewards.png");


        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/businessRewards.png");
    }


    @Test
    @Ignore("WITH ERROR")
    public void whenGetImagePath_GivenProductDescriptionPremiaBusinessRewardsMaxAndLanguageESAndFlagFalse_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(false);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductDescription()).thenReturn("VBSG2");
        when(productDefinition.getProductId()).thenReturn("VBSG2");
        when(userSession.getLanguage()).thenReturn("ES");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/businessMaxRewards.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/businessMaxRewards.png");
    }




    @Test
    public void whenGetImagePath_GivenProductDescriptionPremiaBusinessRewardsSecuredAndLanguageENAndFlagTrue_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(true);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("VBSEC");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/businessRewardSecured.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/businessRewardSecured.png");
    }

    @Test
    @Ignore("WITH ERROR")
    public void whenGetImagePath_GivenProductDescriptionPremiaBusinessRewardsAndLanguageENAndFlagTrue_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(true);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("VBSG1");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/businessRewards.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/businessRewards.png");
    }


    @Test
    @Ignore("WITH ERROR")
    public void whenGetImagePath_GivenProductDescriptionPremiaBusinessRewardsMaxAndLanguageENAndFlagTrue_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(true);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("VBSG2");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/businessMaxRewards.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/businessMaxRewards.png");
    }




    @Test
    public void whenGetImagePath_GivenProductDescriptionPremiaBusinessRewardsSecuredAndLanguageENAndFlagFalse_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(false);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("VBSEC");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/businessRewardSecured.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/businessRewardSecured.png");
    }

    @Test
    @Ignore("WITH ERROR")
    public void whenGetImagePath_GivenProductDescriptionPremiaBusinessRewardsAndLanguageENAndFlagFalse_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(false);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductId()).thenReturn("VBSG1");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/businessRewards.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/businessRewards.png");
    }


    @Test
    @Ignore("WITH ERROR")
    public void whenGetImagePath_GivenProductDescriptionPremiaBusinessRewardsMaxAndLanguageENAndFlagFalse_ThenReturnImagePath() throws Exception {
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA816)).thenReturn(false);
        when(transactionFormBean.getAccountInfo()).thenReturn(account);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(productDefinition.getProductDescription()).thenReturn("VBSG2");
        when(productDefinition.getProductId()).thenReturn("VBSG2");
        when(userSession.getLanguage()).thenReturn("EN");
        when(messageSource.getMessage("credit.card.portal.img." + productDefinition.getProductId(), null,
                new Locale(userSession.getLanguage()))).thenReturn("/img/cc/tarjetas/businessMaxRewards.png");

        String imgPath = Whitebox.invokeMethod(transactionForm, "getImagePath", transactionFormBean, userSession);

        assertEquals(imgPath, "/img/cc/tarjetas/businessMaxRewards.png");
    }


    @Test
    @Ignore("WITH ERROR")
    public void whenReferenceData_GivenFlagMBSE2513_ThenModelConstainFlag() throws Exception
    {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        request.getParameterMap().put("isTab", "1");
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);

        when(messageSource.getMessage(any(String.class), any(), any())).thenReturn("DISABLED");
        when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);

        when(userSession.getPfmFirstTime()).thenReturn("Y");

        //when(CustomerFacade.getInstance()).thenReturn(customerFacade);

        when(customerServices.getGlobalEntitlement(EntitlementType.PFM)).thenReturn(globalEntitlement);

        when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.DISABLED);

        Map<String, Object> model =  transactionForm.referenceData(request, transactionFormBean, bindException);
        assertTrue(model.containsKey("flagMBSE2513"));

    }

    @Test
    @Ignore("WITH ERROR")
    public void whenReferenceData_GivenFlagMBSE2513True_ThenTabNumOne() throws Exception
    {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        request.getParameterMap().put("isTab", "1");
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);

        List<TVFrontendAccount> listAccounts = getDummyAccountList();
        when(userSession.getAccounts()).thenReturn(listAccounts);
        when(transactionFormBean.getAccount()).thenReturn(null);

        when(messageSource.getMessage(any(String.class), any(), any())).thenReturn("DISABLED");
        when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);

        when(userSession.getPfmFirstTime()).thenReturn("Y");

        when(CustomerFacade.getInstance()).thenReturn(customerFacade);

        when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.PFM)).thenReturn(globalEntitlement);

        when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.DISABLED);
        //when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513)).thenReturn(true);

        Map<String, Object> model =  transactionForm.referenceData(request, transactionFormBean, bindException);
        assertTrue(model.containsKey("flagMBSE2513"));

    }

    @Test
    @Ignore("WITH ERROR")
    public void whenOnSubmit_GivenFlagMBSE2513TrueAndAccountMLA_ThenRedirectToAccountSummary() throws Exception
    {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(Utils.getAccountById(any(), any())).thenReturn(account);

        when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);

        when(account.getAccountNumber()).thenReturn("0377810550806217");
        when(transactionFormBean.getCategory()).thenReturn("");
        when(transactionFormBean.getActivity()).thenReturn("ALL");
        when(transactionFormBean.getAccount()).thenReturn("0377810550806217");

        when(messageSource.getMessage("transactions.showAll.hide", null, Locale.ENGLISH)).thenReturn("NOENABLED");

        when(Utils.getRSALanguage(request)).thenReturn("sp");

        when(account.getAccountProductId()).thenReturn("AEULT");

        when(account.getSubtype()).thenReturn("MLA");

        when(account.getProductDefinition()).thenReturn(tvAccountProductDefinition);

        AccountFeatures features = new AccountFeatures();
        features.setShowStatement(true);
        when(account.getFeatures()).thenReturn(features);

        //when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513)).thenReturn(true);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        ModelAndView mav = new ModelAndView();

        PowerMockito.doReturn(mav).when((TransactionForm)transactionForm, "showForm",
                any(HttpServletRequest.class), any(BindException.class), anyString(), any());

        ModelAndView modelAndView =  transactionForm.onSubmit(request, response, transactionFormBean, bindException);

        assertTrue(modelAndView.getViewName().contains("redirect:detail?account"));

    }

    @Test
    public void whenOnSubmit_GivenFlagMBSE2513FalseAndAccountMLA_ThenNotRedirectToAccountSummary() throws Exception
    {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        initializeOnSubmit();
		initializeTransactionList();

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(Utils.getAccountById(any(), any())).thenReturn(account);

        when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);

        when(account.getAccountNumber()).thenReturn("0377810550806217");
        when(transactionFormBean.getCategory()).thenReturn("");
        when(transactionFormBean.getActivity()).thenReturn("ALL");

        when(messageSource.getMessage("transactions.showAll.hide", null, Locale.ENGLISH)).thenReturn("NOENABLED");

        when(Utils.getRSALanguage(request)).thenReturn("sp");

        when(account.getAccountProductId()).thenReturn("AEULT");

        when(account.getSubtype()).thenReturn("MLA");

        when(account.getProductDefinition()).thenReturn(tvAccountProductDefinition);

        AccountFeatures features = new AccountFeatures();
        features.setShowStatement(true);
        when(account.getFeatures()).thenReturn(features);

        ////when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513)).thenReturn(false);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);

        //PowerMockito.doReturn(transactionRequest).when(transactionForm).CreateTransactionRequest(any(HttpServletRequest.class), any(CustomerProfile.class), any(BankingSession.class), any(TVFrontendAccount.class), anyInt());

        transactionResponse.setStatusResult(ResultStatus.SUCCESS);

        when(customerServices.getMortgageTransactions(any())).thenReturn(transactionResponse);

        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
        List<AbstractTransaction> transactions = PowerMockito.mock(List.class);
        when(transactionResponse.getTransactions()).thenReturn(transactions);
        when(transactions.size()).thenReturn(1);
        ModelAndView mav = new ModelAndView();
		mav.getModel().put("transactions", transactions);
        //mav.getModel().put("cookieAnalyticEnabled", true);
        PowerMockito.doReturn(mav).when((TransactionForm)transactionForm, "showForm",
                any(HttpServletRequest.class), any(BindException.class), anyString(), any());

        transactionForm.onSubmit(request, response, transactionFormBean, bindException);

        PowerMockito.verifyPrivate(transactionForm, times(1))
                .invoke("showForm", any(), any(), any(), any());
    }

    @Test
    public void whenOnSubmit_GivenCCATransactions_ThenReturnFlagHideCharegardInModel() throws Exception
    {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        String cycle = "-1";
        String key  = "8594094180910000";

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(Utils.getAccountById(any(), any())).thenReturn(account);
        when(CookieUtils.getCookie(any(), any())).thenReturn(versionCookie);
        List<AbstractStatement> availableStatements = anyList();
        versionCookie.setValue("mobile");
        when(transactionFormBean.getPageReq()).thenReturn(1);
        when(transactionFormBean.getTotalPages()).thenReturn(1);
        when(transactionFormBean.getCycle()).thenReturn(cycle);
        when(account.getAccountNumber()).thenReturn("0377810550806217");
        when(transactionFormBean.getCategory()).thenReturn("");
        when(transactionFormBean.getActivity()).thenReturn("ALL");

        when(messageSource.getMessage("transactions.showAll.hide", null, Locale.ENGLISH)).thenReturn("NOENABLED");

        when(Utils.getRSALanguage(request)).thenReturn("sp");
        when(GenericValidator.isBlankOrNull(key)).thenReturn(false);
        when(account.getAccountProductId()).thenReturn("AEULT");
        when(account.getSubtype()).thenReturn("CCA");
        when(account.getProductDefinition()).thenReturn(tvAccountProductDefinition);
        when(versionCookie.getValue()).thenReturn("mobile");

        AccountFeatures features = new AccountFeatures();
        features.setShowStatement(true);
        when(account.getFeatures()).thenReturn(features);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ACH_PAYMENT_NT)).thenReturn(true);
        when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("Y");
        when(account.getStatements()).thenReturn(availableStatements);
        when(account.getSubtype()).thenReturn("CCA");
        //when(Utils.isAccountPrimaryOwner(userSession, account)).thenReturn(true);
        when(account.getTsysLoyaltyRewardsInfo()).thenReturn(null);
        when(Utils.getStatementById(cycle, availableStatements)).thenReturn(null);
        when(userSession.getDisplayTransactionDowntime()).thenReturn(false);

        //PowerMockito.doReturn(transactionRequest).when(transactionForm).CreateTransactionRequest(any(HttpServletRequest.class), any(CustomerProfile.class), any(BankingSession.class), any(TVFrontendAccount.class), anyInt());

        PowerMockito.whenNew(TransactionRequest.class).withAnyArguments().thenReturn(transactionRequest);
        transactionResponse.setStatusResult(ResultStatus.ERROR);
        when(customerServices.getCreditCardStatementTransactions(any())).thenReturn(transactionResponse);

        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.ERROR);


        when(transactionResponse.getPagesAvailable()).thenReturn(1);
        when(account.getCardPlasticAccountRecords()).thenReturn(null);
        when(transactionResponse.isCardPanId()).thenReturn(true);
        when(transactionForm.getUnversalProducts()).thenReturn("VSELT UAREG UAULT UMEDG UVNOV UVREG UVVSI");
        when(account.getAccountProductId()).thenReturn("AEULT");

        when(transactionResponse.isChargeCard()).thenReturn(false);
        when(transactionResponse.isRevolvingCard()).thenReturn(false);

        ModelAndView mav = new ModelAndView();
        mav.getModel().put("flagHideChargegardLink",StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2535));
        mav.setViewName(VIEW);
        PowerMockito.doReturn(mav).when((TransactionForm)transactionForm, "showForm",
                any(HttpServletRequest.class),any(HttpServletResponse.class), any(BindException.class), any(Map.class));

        when(Utils.getStatementById(any(), any())).thenReturn(availableStatement);
        when(availableStatement.getEndBalance()).thenReturn(new BigDecimal(100));

        ModelAndView modelAndView =  transactionForm.onSubmit(request, response, transactionFormBean, bindException);
        assertTrue(modelAndView.getModel().containsKey("flagHideChargegardLink"));
    }


    /**
     * Get a list of dummy ghost accounts.
     * @return
     */
    private List<TVFrontendAccount> getDummyAccountList() {

        TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("MLA", "0701039224", "A"));
        TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
                "dummyProductDescription",
                "dummyAccountNumberDescription",
                "dummyAccountNumber",
                "VPPRE",
                "MLA",
                "0", // Success
                "dummyId");
        acc.setProductDefinition(productDefinition);

        AccountFeatures features = new AccountFeatures();
        features.setShowStatement(true);

        acc.setFeatures(features);

        when(acc.getHide()).thenReturn(false);
        when(acc.getNickname()).thenReturn("123456789012345");
        List<TVFrontendAccount> accList = new LinkedList<>();
        accList.add(acc);

        return accList;
    }


    /**
     * when ReferenceData
     * Given IsCookieCategoryEnabledTrue
     * Then ReturnModelValueTrue
     *
     * @author Evertec ET58344
     * @since 09-24-2020
     * @exception Exception error
     */
    @Test
    public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(CookieUtils.getCookie(request, "client-version")).thenReturn(null);
        when(userSession.getPortalAccounts()).thenReturn(new ArrayList<>());
        PowerMockito.doReturn("64654654").when(transactionForm, "getPinRequestStatus");
        when(request.getParameter("isTab")).thenReturn("1");
        PowerMockito.doReturn(Boolean.FALSE).when(transactionForm, "getShowAllMessageSourceStatus");
        when(customerProfile.hasEntitlement(EntitlementType.BILLPAY)).thenReturn(Boolean.FALSE);
        when(Utils.getRSALanguage(request)).thenReturn("es");
        when(userSession.getAccounts()).thenReturn(null);
        when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.ATHM);
        when(customerProfile.hasEntitlement(EntitlementType.PFM)).thenReturn(Boolean.TRUE);
        when(userSession.getPfmFirstTime()).thenReturn("Y");
        when(customerProfile.getEntitlementByType(EntitlementType.PFM)).thenReturn(null);
        when(CustomerFacade.getInstance()).thenReturn(customerFacade);
        when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.PFM)).thenReturn(globalEntitlement);
        when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.BLOCKED);

		when(transactionFormBean.getAccountInfo()).thenReturn(null);
        when(userSession.getBankingSession()).thenReturn(null);
        when(transactionFormBean.getAccount()).thenReturn("4564646");
        when(transactionFormBean.getActivity()).thenReturn("asdfasfa");
        when(customerService.getGlobalEntitlement(EntitlementType.PFM)).thenReturn(globalEntitlement);

        Map<String, Object> result = transactionForm.referenceData(request, transactionFormBean, errors);
        assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
    }

    public void whenCreateTransactionRequest_CreateNewTransactionRequest_ThenReturnObjectTransactionRequest() throws Exception
    {

        TransactionRequest result = transactionForm.CreateTransactionRequest(request, customerProfile, bankingSession, account, anyInt());
        assertNotNull(result);
    }


    @Test
    public void whenOnSubmit_GivenNotCCATransactionsInMobileView_ThenReturnModelAndView() throws Exception
    {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        String cycle = "-1";
        String key  = "8594094180910000";




        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(Utils.getAccountById(any(), any())).thenReturn(account);
        when(CookieUtils.getCookie(any(), any())).thenReturn(versionCookie);


        List<AbstractStatement> availableStatements = anyList();
        versionCookie.setValue("mobile");



        when(transactionFormBean.getPageReq()).thenReturn(1);
        when(transactionFormBean.getTotalPages()).thenReturn(1);
        when(transactionFormBean.getCycle()).thenReturn(cycle);

        when(account.getAccountNumber()).thenReturn("0377810550806217");
        when(transactionFormBean.getCategory()).thenReturn("");
        when(transactionFormBean.getActivity()).thenReturn("ALL");

        when(messageSource.getMessage("transactions.showAll.hide", null, Locale.ENGLISH)).thenReturn("NOENABLED");

        when(Utils.getRSALanguage(request)).thenReturn("sp");


        when(GenericValidator.isBlankOrNull(key)).thenReturn(false);


        when(account.getAccountProductId()).thenReturn("AEULT");


        when(account.getSubtype()).thenReturn("CCA");


        when(account.getProductDefinition()).thenReturn(tvAccountProductDefinition);
        when(versionCookie.getValue()).thenReturn("mobile");


        AccountFeatures features = new AccountFeatures();
        features.setShowStatement(true);
        when(account.getFeatures()).thenReturn(features);



        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ACH_PAYMENT_NT)).thenReturn(true);

        when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("Y");

        when(account.getStatements()).thenReturn(availableStatements);




        when(account.getSubtype()).thenReturn("CCA");
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(true);
        //when(Utils.isAccountPrimaryOwner(userSession, account)).thenReturn(true);
        when(account.getTsysLoyaltyRewardsInfo()).thenReturn(null);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA812)).thenReturn(true);



        when(Utils.getStatementById(cycle, availableStatements)).thenReturn(availableStatement);
        when(customerServices.getCreditCardStatementTransactions(any())).thenReturn(transactionResponse);
        when(userSession.getDisplayTransactionDowntime()).thenReturn(false);


        transactionResponse.setStatusResult(ResultStatus.SUCCESS);

        when(customerServices.getCreditCardCurrentTransactions(any())).thenReturn(transactionResponse);


        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);


        when(transactionResponse.getPagesAvailable()).thenReturn(1);
        when(account.getCardPlasticAccountRecords()).thenReturn(null);
        when(transactionResponse.isCardPanId()).thenReturn(true);
        when(transactionForm.getUnversalProducts()).thenReturn("VSELT UAREG UAULT UMEDG UVNOV UVREG UVVSI");
        when(account.getAccountProductId()).thenReturn("AEULT");

        when(transactionResponse.isChargeCard()).thenReturn(false);
        when(transactionResponse.isRevolvingCard()).thenReturn(false);



        ModelAndView mav = new ModelAndView();
        mav.getModel().put("cookieAnalyticEnabled", true);
        mav.setViewName(VIEW);
        PowerMockito.doReturn(mav).when((TransactionForm)transactionForm, "showForm",
                any(HttpServletRequest.class), any(BindException.class), anyString(), any());

        ModelAndView modelAndView =  transactionForm.onSubmit(request, response, transactionFormBean, bindException);
        assertViewName(modelAndView, VIEW);

    }

    @Test
    public void whenOnSubmit_GivenCCATransactionsInMobileView_ThenReturnModelAndView() throws Exception
    {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        String cycle = "-1";
        String key  = "8594094180910000";


        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(Utils.getAccountById(any(), any())).thenReturn(account);
        when(CookieUtils.getCookie(any(), any())).thenReturn(versionCookie);



        List<AbstractStatement> availableStatements = anyList();
        versionCookie.setValue("mobile");



        when(transactionFormBean.getPageReq()).thenReturn(1);
        when(transactionFormBean.getTotalPages()).thenReturn(1);
        when(transactionFormBean.getCycle()).thenReturn(cycle);
        when(account.getAccountNumber()).thenReturn("0377810550806217");
        when(transactionFormBean.getCategory()).thenReturn("");
        when(transactionFormBean.getActivity()).thenReturn("ALL");

        when(messageSource.getMessage("transactions.showAll.hide", null, Locale.ENGLISH)).thenReturn("NOENABLED");

        when(Utils.getRSALanguage(request)).thenReturn("sp");


        when(GenericValidator.isBlankOrNull(key)).thenReturn(false);


        when(account.getAccountProductId()).thenReturn("AEULT");


        when(account.getSubtype()).thenReturn("CCA");


        when(account.getProductDefinition()).thenReturn(tvAccountProductDefinition);
        when(versionCookie.getValue()).thenReturn("mobile");


        AccountFeatures features = new AccountFeatures();
        features.setShowStatement(true);
        when(account.getFeatures()).thenReturn(features);



        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ACH_PAYMENT_NT)).thenReturn(true);

        when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("Y");

        when(account.getStatements()).thenReturn(availableStatements);








        when(account.getSubtype()).thenReturn("CCA");
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(true);
        //when(Utils.isAccountPrimaryOwner(userSession, account)).thenReturn(true);
        when(account.getTsysLoyaltyRewardsInfo()).thenReturn(null);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA812)).thenReturn(true);



        when(Utils.getStatementById(cycle, availableStatements)).thenReturn(null);
        when(userSession.getDisplayTransactionDowntime()).thenReturn(false);


        PowerMockito.doReturn(transactionRequest).when(transactionForm).CreateTransactionRequest(any(HttpServletRequest.class), any(CustomerProfile.class), any(BankingSession.class), any(TVFrontendAccount.class), anyInt());



        transactionResponse.setStatusResult(ResultStatus.SUCCESS);

        when(customerServices.getCreditCardCurrentTransactions(any())).thenReturn(transactionResponse);


        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);


        when(transactionResponse.getPagesAvailable()).thenReturn(1);
        when(account.getCardPlasticAccountRecords()).thenReturn(null);
        when(transactionResponse.isCardPanId()).thenReturn(true);
        when(transactionForm.getUnversalProducts()).thenReturn("VSELT UAREG UAULT UMEDG UVNOV UVREG UVVSI");
        when(account.getAccountProductId()).thenReturn("AEULT");

        when(transactionResponse.isChargeCard()).thenReturn(false);
        when(transactionResponse.isRevolvingCard()).thenReturn(false);



        ModelAndView mav = new ModelAndView();
        mav.getModel().put("cookieAnalyticEnabled", true);
        mav.setViewName(VIEW);
        PowerMockito.doReturn(mav).when((TransactionForm)transactionForm, "showForm",
                any(HttpServletRequest.class), any(BindException.class), anyString(), any());

        ModelAndView modelAndView =  transactionForm.onSubmit(request, response, transactionFormBean, bindException);
        assertViewName(modelAndView, VIEW);

    }



    @Test
    public void whenOnSubmit_GivenCCATransactionsInMobileView_ThenReturnModelAndViewAllPage() throws Exception
    {
        response = new MockHttpServletResponse();

        String cycle = "2";
        String key  = "8594094180910000";
        String strShowAllPages = "yes";
        int maxTransactions = 0;
        ReflectionTestUtils.setField(transactionForm, // inject into this object
                "maxTransactions", // assign to this field
                maxTransactions); // object to be injected

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(Utils.getAccountById(any(), any())).thenReturn(account);
        when(CookieUtils.getCookie(any(), any())).thenReturn(versionCookie);

        when(request.getParameter("allPages")).thenReturn(strShowAllPages);

        List<AbstractStatement> availableStatements = anyList();
        versionCookie.setValue("mobile");

        when(transactionFormBean.getPageReq()).thenReturn(1);
        when(transactionFormBean.getTotalPages()).thenReturn(2);
        when(transactionFormBean.getCycle()).thenReturn(cycle);
        when(account.getAccountNumber()).thenReturn("0377810550806217");
        when(transactionFormBean.getCategory()).thenReturn("");
        when(transactionFormBean.getActivity()).thenReturn("ALL");

        when(messageSource.getMessage("transactions.showAll.hide", null, Locale.ENGLISH)).thenReturn("NOENABLED");

        when(Utils.getRSALanguage(request)).thenReturn("sp");


        when(GenericValidator.isBlankOrNull(key)).thenReturn(false);


        when(account.getAccountProductId()).thenReturn("AEULT");


        when(account.getSubtype()).thenReturn("CCA");


        when(account.getProductDefinition()).thenReturn(tvAccountProductDefinition);
        when(versionCookie.getValue()).thenReturn("mobile");


        AccountFeatures features = new AccountFeatures();
        features.setShowStatement(true);
        when(account.getFeatures()).thenReturn(features);

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ACH_PAYMENT_NT)).thenReturn(true);

        when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("Y");

        when(account.getStatements()).thenReturn(availableStatements);


        when(account.getSubtype()).thenReturn("CCA");
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(true);
        //when(Utils.isAccountPrimaryOwner(userSession, account)).thenReturn(true);
        when(account.getTsysLoyaltyRewardsInfo()).thenReturn(null);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA812)).thenReturn(true);


        when(Utils.getStatementById(cycle, availableStatements)).thenReturn(availableStatement);
        when(userSession.getDisplayTransactionDowntime()).thenReturn(false);

        PowerMockito.whenNew(TransactionRequest.class).withAnyArguments().thenReturn(transactionRequest);

        when(new TransactionRequest(
                Utils.generateCustomerInteractionEvent(request), availableStatement, customerProfile, bankingSession,
                account, maxTransactions, 1, false, null, null, null))
                .thenReturn(transactionRequest);

        transactionResponse.setStatusResult(ResultStatus.SUCCESS);

        when(customerServices.getCreditCardStatementTransactions(any())).thenReturn(transactionResponse);

        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);

        when(transactionResponse.getPagesAvailable()).thenReturn(1);
        when(account.getCardPlasticAccountRecords()).thenReturn(null);
        when(transactionResponse.isCardPanId()).thenReturn(true);
        when(transactionForm.getUnversalProducts()).thenReturn("VSELT UAREG UAULT UMEDG UVNOV UVREG UVVSI");
        when(account.getAccountProductId()).thenReturn("AEULT");

        when(transactionResponse.isChargeCard()).thenReturn(false);
        when(transactionResponse.isRevolvingCard()).thenReturn(false);

        ModelAndView mav = new ModelAndView();
        mav.getModel().put("cookieAnalyticEnabled", true);
        mav.setViewName(VIEW);
        PowerMockito.doReturn(mav).when((TransactionForm)transactionForm, "showForm",
                any(HttpServletRequest.class), any(BindException.class), anyString(), any());

        ModelAndView modelAndView =  transactionForm.onSubmit(request, response, transactionFormBean, bindException);
        assertViewName(modelAndView, VIEW);

    }

    private OngoingStubbing<Object> When(Cookie cookie) {
        // TODO Auto-generated method stub
        return null;
    }

    @Test
    public void whenReferenceData_GivenAccountCCA_ThenReturnFlagHideChargegardLinkEnable() throws Exception {

        List<TVFrontendAccount> frontendAccounts = new ArrayList<>();
        Balance balance = new Balance("balance", new BigDecimal("11500"));
        Account tvAccount = new CardAccount();
        tvAccount.setAccountID("34212132132");
        TVFrontendAccount firstAccount = new TVFrontendCreditAccount(tvAccount);
        AccountFeatures accountFeatures = new AccountFeatures();

        accountFeatures.setShowStatement(true);
        firstAccount.setCustomerAccountPreferences(new CustomerAccountPreferences());
        firstAccount.setFeatures(accountFeatures);
        firstAccount.setHide(false);
        firstAccount.setNickname("CUENTA PRUEBA");
        firstAccount.setAccountNumberSuffix("");
        firstAccount.setFrontEndId("34212132133");
        //frontendAccounts.add(firstAccount);

        TVFrontendAccount frontendAccount = PowerMockito.mock(TVFrontendAccount.class);
        AccountFeatures features = PowerMockito.mock(AccountFeatures.class);
        CardPlasticAccountRecord accountRecord = PowerMockito.mock(CardPlasticAccountRecord.class);
        when(features.isShowStatement()).thenReturn(true);
        when(frontendAccount.getFeatures()).thenReturn(features);
        when(frontendAccount.getHide()).thenReturn(false);
        when(frontendAccount.getNickname()).thenReturn("CUENTA PRUEBA");
        when(frontendAccount.getAccountNumberSuffix()).thenReturn("");
        when(frontendAccount.getFrontEndId()).thenReturn("34212132133");
        when(frontendAccount.getAccountNumber()).thenReturn("34212132132");
        when(frontendAccount.getAccountProductId()).thenReturn("022");
        when(frontendAccount.getPortalBalance()).thenReturn(null);
        when(frontendAccount.getSubtype()).thenReturn("CCA");
        when(accountRecord.getCardPANId()).thenReturn("x2132");
        when(frontendAccount.getCardPlasticAccountRecords()).thenReturn(new CardPlasticAccountRecord[]{accountRecord});
        frontendAccounts.add(frontendAccount);

        response = new MockHttpServletResponse();
        globalEntitlement = PowerMockito.mock(GlobalEntitlement.class);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(Utils.getAccountById(any(), any())).thenReturn(account);
        when(CookieUtils.getCookie(any(), any())).thenReturn(versionCookie);
        when(request.getParameter("isTab")).thenReturn("1");
        when(GenericValidator.isBlankOrNull("isTab")).thenReturn(false);
        when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
        when(userSession.getPfmFirstTime()).thenReturn("Y");
        when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.BLOCKED);
        when(customerServices.getGlobalEntitlement(EntitlementType.PFM)).thenReturn(globalEntitlement);
        when(transactionFormBean.getAccount()).thenReturn("34212132132");
        when(GenericValidator.isBlankOrNull("34212132132")).thenReturn(true);
        when(userSession.getAccounts()).thenReturn(frontendAccounts);
        when(transactionFormBean.getActivity()).thenReturn("x2132");
        PowerMockito.doReturn("local/tomcat/img/").when(transactionForm, "getImagePath",
                any(TransactionFormBean.class), any(UserSession.class));
        when(userSession.hasVirginIslandAccount()).thenReturn(false);
        when(userSession.hasBritishVirginIslandAccount()).thenReturn(false);
        when(userSession.getDisplayTransactionDowntime()).thenReturn(true);
        when(userSession.getDisplayTransactionDowntimeMessages()).thenReturn(new String[]{"Downtime Message"});
        when(Utils.getRSALanguage(request)).thenReturn("es");
        PowerMockito.doReturn(true).when(transactionForm, "getShowAllMessageSourceStatus");

        List<AbstractStatement> availableStatements = anyList();
        versionCookie.setValue("mobile");

        transactionForm.referenceData(request, transactionFormBean, bindException);
    }


	 @Test
	    public void whenOnSubmit_GivenAccountSubtypeMLA_ThenReturnModelTransactionsMLAWithShowDetailEnabled() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();

	        List<AbstractTransaction> abstractTransactionList = new ArrayList<>();
	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("MLA");
	        mav.getModel().put("transactions", abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypeIDA_ThenReturnModelTransactionsIDAWithShowDetailEnabled() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();
	        DepositStatement depositStatement = new DepositStatement();
	        depositStatement.setStartBalance(new BigDecimal("100"));

	        List<AbstractTransaction> abstractTransactionList = new ArrayList<>();
	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("IDA");
	        when(transactionFormBean.getCycle()).thenReturn("2");
	        when(account.getStatements()).thenReturn(new ArrayList<>());
	        when(Utils.getRSALanguage(any())).thenReturn("en");
	        when(Utils.getStatements(any(List.class), any(LinkedHashMap.class), any(String.class), any(Boolean.class)))
	                .thenReturn(new LinkedHashMap<>());
	        when(Utils.getDetailedStatements(anyList(), any(String.class), any(Boolean.class))).thenReturn(new LinkedHashMap<>());
	        when(Utils.getStatementById(anyString(), any())).thenReturn(depositStatement);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
	        when(request.getParameter("allPages")).thenReturn("yes");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getTotalPages()).thenReturn(1);
	        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(new CustomerInteractionEvent());
	        when(customerServices.getDepositStatementTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(transactionResponse.getImageKeyMap()).thenReturn(new HashMap<>());
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(account.getProductDefinition()).thenReturn(productDefinition);
	        when(productDefinition.getId()).thenReturn("SAVING");

	        mav.getModel().put("transactions", abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypePFC_ThenReturnModelTransactionsPFCWithShowDetailEnabled() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();
	        DepositStatement depositStatement = new DepositStatement();
	        depositStatement.setStartBalance(new BigDecimal("100"));

	        List<AbstractTransaction> abstractTransactionList = new ArrayList<>();
	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("PFC");
	        when(transactionFormBean.getCycle()).thenReturn("2");
	        when(account.getStatements()).thenReturn(new ArrayList<>());
	        when(Utils.getRSALanguage(any())).thenReturn("en");
	        when(Utils.getStatements(any(List.class), any(LinkedHashMap.class), any(String.class), any(Boolean.class)))
	                .thenReturn(new LinkedHashMap<>());
	        when(Utils.getDetailedStatements(anyList(), any(String.class), any(Boolean.class))).thenReturn(new LinkedHashMap<>());
	        when(Utils.getStatementById(anyString(), any())).thenReturn(depositStatement);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
	        when(request.getParameter("allPages")).thenReturn("no");

	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getTotalPages()).thenReturn(1);
	        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(new CustomerInteractionEvent());
	        when(customerServices.getDepositStatementTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(transactionResponse.getImageKeyMap()).thenReturn(new HashMap<>());
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(account.getProductDefinition()).thenReturn(productDefinition);
	        when(productDefinition.getId()).thenReturn("SAVING");

	        mav.getModel().put("transactions", abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));

	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypeIDA_ThenReturnModelTransactionsIDAWithShowDetailEnabledCycleOne() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();
	        DepositStatement depositStatement = new DepositStatement();
	        depositStatement.setStartBalance(new BigDecimal("100"));

	        List<AbstractTransaction> abstractTransactionList = new LinkedList<>();
	        RunningBalanceTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransaction.setDescription("IN PROCESS");
	        abstractTransaction.setType("CR");
	        abstractTransaction.setBalance(new BigDecimal("500"));
	        abstractTransactionList.add(abstractTransaction);

	        abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransaction.setDescription("CURRENT");
	        abstractTransaction.setType("CR");
	        abstractTransaction.setDate(Calendar.getInstance());
	        abstractTransaction.setBalance(new BigDecimal("10000"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("PRA").thenReturn("IDA");
	        when(transactionFormBean.getCycle()).thenReturn("1");
	        when(account.getStatements()).thenReturn(new ArrayList<>());
	        when(customerServices.getAccountDetails(any(CustomerProfile.class), any(BankingSession.class),
	                any(TVFrontendAccount.class), any(Boolean.class))).thenReturn(null);
	        when(Utils.getRSALanguage(any())).thenReturn("en");
	        when(Utils.getStatements(any(List.class), any(LinkedHashMap.class), any(String.class), any(Boolean.class)))
	                .thenReturn(new LinkedHashMap<>());
	        when(Utils.getDetailedStatements(anyList(), any(String.class), any(Boolean.class))).thenReturn(new LinkedHashMap<>());
	        when(Utils.getStatementById(anyString(), any())).thenReturn(depositStatement);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
	        when(request.getParameter("allPages")).thenReturn("yes");

	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getTotalPages()).thenReturn(1);
	        when(account.getPortalBalance()).thenReturn(new BigDecimal("5000"));
	        when(customerServices.getDepositCurrentTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(new CustomerInteractionEvent());
	        when(transactionResponse.getImageKeyMap()).thenReturn(new HashMap<>());
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        PowerMockito.doReturn(this.abstractTransactionList).when(transactionForm,
	                "setTransactionsCodeReason", any());
	        when(account.getProductDefinition()).thenReturn(productDefinition);
	        when(productDefinition.getId()).thenReturn("SAVING");

	        mav.getModel().put("inprocess", this.abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("inprocess");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypeIDA_ThenReturnModelTransactionsIDAWithShowDetailEnabledCycleOneAllPagesFalse() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();
	        DepositStatement depositStatement = new DepositStatement();
	        depositStatement.setStartBalance(new BigDecimal("100"));

	        List<AbstractTransaction> abstractTransactionList = new LinkedList<>();
	        RunningBalanceTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransaction.setDescription("IN PROCESS");
	        abstractTransaction.setType("CR");
	        abstractTransaction.setBalance(new BigDecimal("500"));
	        abstractTransactionList.add(abstractTransaction);

	        abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransaction.setDescription("CURRENT");
	        abstractTransaction.setType("CR");
	        abstractTransaction.setDate(Calendar.getInstance());
	        abstractTransaction.setBalance(new BigDecimal("10000"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("PRA").thenReturn("IDA");
	        when(transactionFormBean.getCycle()).thenReturn("1");
	        when(account.getStatements()).thenReturn(new ArrayList<>());
	        when(customerServices.getAccountDetails(any(CustomerProfile.class), any(BankingSession.class),
	                any(TVFrontendAccount.class), any(Boolean.class))).thenReturn(null);
	        when(Utils.getRSALanguage(any())).thenReturn("en");
	        when(Utils.getStatements(any(List.class), any(LinkedHashMap.class), any(String.class), any(Boolean.class)))
	                .thenReturn(new LinkedHashMap<>());
	        when(Utils.getDetailedStatements(anyList(), any(String.class), any(Boolean.class))).thenReturn(new LinkedHashMap<>());
	        when(Utils.getStatementById(anyString(), any())).thenReturn(depositStatement);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
	        when(request.getParameter("allPages")).thenReturn("no");

	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getTotalPages()).thenReturn(1);
	        when(account.getPortalBalance()).thenReturn(new BigDecimal("5000"));
	        when(customerServices.getDepositCurrentTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(Utils.generateCustomerInteractionEvent(any())).thenReturn(new CustomerInteractionEvent());
	        when(transactionResponse.getImageKeyMap()).thenReturn(new HashMap<>());
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        PowerMockito.doReturn(this.abstractTransactionList).when(transactionForm,
	                "setTransactionsCodeReason", any());
	        when(account.getProductDefinition()).thenReturn(productDefinition);
	        when(productDefinition.getId()).thenReturn("SAVING");

	        mav.getModel().put("inprocess", this.abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("inprocess");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypeCCA_ThenReturnModelTransactionsCCAWithShowDetailEnabled() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();
	        Cookie cookie = PowerMockito.mock(Cookie.class);
	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        AbstractStatement statement = transactionRequest.getStatement();
	        TransactionRequest transactionRequest1 = new TransactionRequest(
	                event, statement, customerProfile,
	                bankingSession, account, 9999, 1, false, null, null, null);
	        CreditCardStatement depositStatement = new CreditCardStatement();
	        depositStatement.setStartBalance(new BigDecimal("10000"));

	        List<AbstractTransaction> abstractTransactionList = new ArrayList<>();
	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("CCA");
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getCycle()).thenReturn("2");
	        when(Utils.getStatementById(anyString(), any())).thenReturn(depositStatement);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getTotalPages()).thenReturn(1);
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(customerServices.getCreditCardStatementTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(CookieUtils.getCookie(any(HttpServletRequest.class), anyString())).thenReturn(cookie);
	        when(cookie.getValue()).thenReturn("mobile");
	        when(transactionFormBean.getActivity()).thenReturn("xxx");

	        mav.getModel().put("transactions", abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypeCCA_ThenReturnModelTransactionsCCAWithShowDetailEnabledStmtNull() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();
	        Cookie cookie = PowerMockito.mock(Cookie.class);
	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        AbstractStatement statement = transactionRequest.getStatement();
	        TransactionRequest transactionRequest1 = new TransactionRequest(
	                event, statement, customerProfile,
	                bankingSession, account, 9999, 1, false, null, null, null);
	        CreditCardStatement depositStatement = new CreditCardStatement();
	        depositStatement.setStartBalance(new BigDecimal("10000"));

	        List<AbstractTransaction> abstractTransactionList = new ArrayList<>();
	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("CCA");
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getCycle()).thenReturn("2");
	        when(Utils.getStatementById(anyString(), any())).thenReturn(null);
	        when(request.getParameter("allPages")).thenReturn("yes");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(customerServices.getCreditCardStatementTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getTotalPages()).thenReturn(1);
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(CookieUtils.getCookie(any(HttpServletRequest.class), anyString())).thenReturn(cookie);
	        when(cookie.getValue()).thenReturn("mobile");
	        when(transactionFormBean.getActivity()).thenReturn("xxx");

	        mav.getModel().put("transactions", abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        when(Utils.getStatementById(anyString(), any())).thenReturn(availableStatement);
	        when(availableStatement.getEndBalance()).thenReturn(new BigDecimal("1250"));
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypeCCA_ThenReturnModelTransactionsCCAWithShowDetailEnabledShowAllPages() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();
	        Cookie cookie = PowerMockito.mock(Cookie.class);
	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        AbstractStatement statement = transactionRequest.getStatement();
	        TransactionRequest transactionRequest1 = new TransactionRequest(
	                event, statement, customerProfile,
	                bankingSession, account, 9999, 1, false, null, null, null);
	        CreditCardStatement depositStatement = new CreditCardStatement();
	        depositStatement.setStartBalance(new BigDecimal("10000"));

	        List<AbstractTransaction> abstractTransactionList = new ArrayList<>();
	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("CCA");
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getCycle()).thenReturn("2");
	        when(Utils.getStatementById(anyString(), any())).thenReturn(null);
	        when(request.getParameter("allPages")).thenReturn("no");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(customerServices.getCreditCardStatementTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getTotalPages()).thenReturn(1);
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(CookieUtils.getCookie(any(HttpServletRequest.class), anyString())).thenReturn(cookie);
	        when(cookie.getValue()).thenReturn("mobile");
	        when(transactionFormBean.getActivity()).thenReturn("xxx");

	        mav.getModel().put("transactions", abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        when(Utils.getStatementById(anyString(), any())).thenReturn(availableStatement);
	        when(availableStatement.getEndBalance()).thenReturn(new BigDecimal("1250"));
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypeCCA_ThenReturnModelTransactionsCCAWithShowDetailEnabledCycleOneShowAllPages() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();
	        Cookie cookie = PowerMockito.mock(Cookie.class);
	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        AbstractStatement statement = transactionRequest.getStatement();
	        TransactionRequest transactionRequest1 = new TransactionRequest(
	                event, statement, customerProfile,
	                bankingSession, account, 9999, 1, false, null, null, null);
	        CreditCardStatement depositStatement = new CreditCardStatement();
	        depositStatement.setStartBalance(new BigDecimal("10000"));

	        List<AbstractTransaction> abstractTransactionList = new ArrayList<>();
	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("CCA");
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getCycle()).thenReturn("1");
	        when(Utils.getStatementById(anyString(), any())).thenReturn(null);
	        when(request.getParameter("allPages")).thenReturn("yes");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
	        when(customerServices.getCreditCardCurrentTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getTotalPages()).thenReturn(1);
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(CookieUtils.getCookie(any(HttpServletRequest.class), anyString())).thenReturn(cookie);
	        when(cookie.getValue()).thenReturn("mobile");
	        when(transactionFormBean.getActivity()).thenReturn("xxx");

	        mav.getModel().put("transactions", abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypeCCA_ThenReturnModelTransactionsCCAWithShowDetailEnabledCycleTwoAllPagesTrue() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();
	        Cookie cookie = PowerMockito.mock(Cookie.class);
	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        AbstractStatement statement = transactionRequest.getStatement();
	        TransactionRequest transactionRequest1 = new TransactionRequest(
	                event, statement, customerProfile,
	                bankingSession, account, 9999, 1, false, null, null, null);
	        CreditCardStatement depositStatement = new CreditCardStatement();
	        depositStatement.setStartBalance(new BigDecimal("10000"));

	        List<AbstractTransaction> abstractTransactionList = new ArrayList<>();
	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("CCA");
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getCycle()).thenReturn("2");
	        when(Utils.getStatementById(anyString(), any())).thenReturn(null);
	        when(request.getParameter("allPages")).thenReturn("yes");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(customerServices.getCreditCardStatementTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getTotalPages()).thenReturn(1);
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(CookieUtils.getCookie(any(HttpServletRequest.class), anyString())).thenReturn(cookie);
	        when(cookie.getValue()).thenReturn("mobile");
	        when(transactionFormBean.getActivity()).thenReturn("xxx");

	        mav.getModel().put("transactions", abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        when(Utils.getStatementById(anyString(), any(List.class))).thenReturn(availableStatement);
	        when(availableStatement.getEndBalance()).thenReturn(new BigDecimal("1250"));
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypeCCA_ThenReturnModelTransactionsCCAWithShowDetailEnabledCycleOneAllPagesFalse() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();
	        Cookie cookie = PowerMockito.mock(Cookie.class);
	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        AbstractStatement statement = transactionRequest.getStatement();
	        TransactionRequest transactionRequest1 = new TransactionRequest(
	                event, statement, customerProfile,
	                bankingSession, account, 9999, 1, false, null, null, null);
	        CreditCardStatement depositStatement = new CreditCardStatement();
	        depositStatement.setStartBalance(new BigDecimal("10000"));

	        List<AbstractTransaction> abstractTransactionList = new ArrayList<>();
	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("CCA");
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getCycle()).thenReturn("2");
	        when(Utils.getStatementById(anyString(), any())).thenReturn(null);
	        when(request.getParameter("allPages")).thenReturn("no");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(customerServices.getCreditCardStatementTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getTotalPages()).thenReturn(1);
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(CookieUtils.getCookie(any(HttpServletRequest.class), anyString())).thenReturn(cookie);
	        when(cookie.getValue()).thenReturn("mobile");
	        when(transactionFormBean.getActivity()).thenReturn("xxx");

	        mav.getModel().put("transactions", abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        when(Utils.getStatementById(anyString(), any(List.class))).thenReturn(availableStatement);
	        when(availableStatement.getEndBalance()).thenReturn(new BigDecimal("1250"));
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypeCCA_ThenReturnModelTransactionsCCAWithShowDetailEnabledCycleTwoAllPagesTrueCCA() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();
	        Cookie cookie = PowerMockito.mock(Cookie.class);
	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        AbstractStatement statement = transactionRequest.getStatement();
	        TransactionRequest transactionRequest1 = new TransactionRequest(
	                event, statement, customerProfile,
	                bankingSession, account, 9999, 1, false, null, null, null);
	        CreditCardStatement depositStatement = new CreditCardStatement();
	        depositStatement.setStartBalance(new BigDecimal("10000"));

	        List<AbstractTransaction> abstractTransactionList = new ArrayList<>();
	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("CCA");
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getCycle()).thenReturn("2");
	        when(Utils.getStatementById(anyString(), any())).thenReturn(new CreditCardStatement());
	        when(request.getParameter("allPages")).thenReturn("yes");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(customerServices.getCreditCardStatementTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getTotalPages()).thenReturn(1);
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(CookieUtils.getCookie(any(HttpServletRequest.class), anyString())).thenReturn(cookie);
	        when(cookie.getValue()).thenReturn("mobile");
	        when(transactionFormBean.getActivity()).thenReturn("xxx");

	        mav.getModel().put("transactions", abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypeCCA_ThenReturnModelTransactionsCCAWithShowDetailEnabledCycleOneAllPagesFalseCCA() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();
	        Cookie cookie = PowerMockito.mock(Cookie.class);
	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        AbstractStatement statement = transactionRequest.getStatement();
	        TransactionRequest transactionRequest1 = new TransactionRequest(
	                event, statement, customerProfile,
	                bankingSession, account, 9999, 1, false, null, null, null);
	        CreditCardStatement depositStatement = new CreditCardStatement();
	        depositStatement.setStartBalance(new BigDecimal("10000"));

	        List<AbstractTransaction> abstractTransactionList = new ArrayList<>();
	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("CCA");
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getCycle()).thenReturn("1");
	        when(Utils.getStatementById(anyString(), any())).thenReturn(null);
	        when(request.getParameter("allPages")).thenReturn("yes");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(customerServices.getCreditCardCurrentTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getTotalPages()).thenReturn(1);
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(CookieUtils.getCookie(any(HttpServletRequest.class), anyString())).thenReturn(cookie);
	        when(cookie.getValue()).thenReturn("mobile");
	        when(transactionFormBean.getActivity()).thenReturn("xxx");

	        mav.getModel().put("transactions", abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypeCCA_ThenReturnModelTransactionsCCAWithShowDetailEnabledCycleOneAllPagesF() throws Exception {
	        initializeOnSubmit();
	        ModelAndView mav = new ModelAndView();
	        Cookie cookie = PowerMockito.mock(Cookie.class);
	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        AbstractStatement statement = transactionRequest.getStatement();
	        TransactionRequest transactionRequest1 = new TransactionRequest(
	                event, statement, customerProfile,
	                bankingSession, account, 9999, 1, false, null, null, null);
	        CreditCardStatement depositStatement = new CreditCardStatement();
	        depositStatement.setStartBalance(new BigDecimal("10000"));

	        List<AbstractTransaction> abstractTransactionList = new ArrayList<>();
	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransactionList.add(abstractTransaction);

	        when(account.getSubtype()).thenReturn("CCA");
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getCycle()).thenReturn("1");
	        when(Utils.getStatementById(anyString(), any())).thenReturn(null);
	        when(request.getParameter("allPages")).thenReturn("no");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(customerServices.getCreditCardCurrentTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getTotalPages()).thenReturn(1);
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(CookieUtils.getCookie(any(HttpServletRequest.class), anyString())).thenReturn(cookie);
	        when(cookie.getValue()).thenReturn("mobile");
	        when(transactionFormBean.getActivity()).thenReturn("xxx");

	        mav.getModel().put("transactions", abstractTransactionList);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypePRM_ThenReturnModelTransactionsPRMWithShowDetailEnabledCategoryCARD() throws Exception {
	        initializeOnSubmit();
	        initializeTransactionList();
	        ModelAndView mav = new ModelAndView();
	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        AbstractStatement statement = PowerMockito.mock(DepositStatement.class);
	        CreditCardStatement depositStatement = new CreditCardStatement();
	        depositStatement.setStartBalance(new BigDecimal("10000"));

	        when(account.getSubtype()).thenReturn("PRM");
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getCycle()).thenReturn("1");
	        when(Utils.getStatementById(anyString(), any())).thenReturn(statement);
	        when(statement.getEndBalance()).thenReturn(new BigDecimal("1500"));
	        when(transactionFormBean.getCategory()).thenReturn("CARD");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(request.getParameter("allPages")).thenReturn("no");
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(customerServices.getPremiaTransactionsByCategory(any())).thenReturn(premiaTransactionResponse);
	        when(premiaTransactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(premiaTransactionResponse.getTransactions()).thenReturn(abstractTransactionListPRM);

	        mav.getModel().put("transactions", abstractTransactionListPRM);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionListPRM).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }

	    @Test
	    public void whenOnSubmit_GivenAccountSubtypePRM_ThenReturnModelTransactionsPRMWithShowDetailEnabledCategoryACCOUNT() throws Exception {
	        initializeOnSubmit();
	        initializeTransactionList();
	        ModelAndView mav = new ModelAndView();
	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        AbstractStatement statement = PowerMockito.mock(DepositStatement.class);
	        CreditCardStatement depositStatement = new CreditCardStatement();
	        depositStatement.setStartBalance(new BigDecimal("10000"));

	        when(account.getSubtype()).thenReturn("PRM");
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getCycle()).thenReturn("1");
	        when(Utils.getStatementById(anyString(), any())).thenReturn(statement);
	        when(statement.getEndBalance()).thenReturn(new BigDecimal("1500"));
	        when(transactionFormBean.getCategory()).thenReturn("ACCOUNT");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(request.getParameter("allPages")).thenReturn("no");
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(customerServices.getPremiaTransactionsByCategory(any())).thenReturn(premiaTransactionResponse);
	        when(premiaTransactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(premiaTransactionResponse.getTransactions()).thenReturn(abstractTransactionListPRM);

	        mav.getModel().put("transactions", abstractTransactionListPRM);
	        PowerMockito.doReturn(mav).when(transactionForm, "showForm",
	                any(HttpServletRequest.class), any(BindException.class), anyString(), any());
	        PowerMockito.doReturn(abstractTransactionListPRM).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        ModelAndView modelAndView = transactionForm.onSubmit(request, response, transactionFormBean, bindException);
	        Object resultObj = modelAndView.getModel().get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;

	        assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	    }
	    
	    @Test
	    public void whenReferenceData_GivenAccountSubtypeMLA_ThenReturnModelTransactionsWithShowDetailEnabled() throws Exception {
	        initializeReferenceData();
	        Balance balance = new Balance("Amount", new BigDecimal("10000"));
	        Account accountBackendObj = new LoanAccount();
	        accountBackendObj.setAccountID("564654564654654");
	        accountBackendObj.setAccountListBalance(balance);
	        accountBackendObj.setSubType("MLA");
	        tvFrontendAccount.setBackendObject(accountBackendObj);
	        userAccounts.add(tvFrontendAccount);

	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        when(transactionFormBean.getAccount()).thenReturn(null);
	        when(userSession.getAccounts()).thenReturn(userAccounts);
	        when(AccountUtils.getIsTransactional(any())).thenReturn("true");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(customerServices.getMortgageTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(request.getParameter("isTab")).thenReturn("");

	        Map<String, Object> model = transactionForm.referenceData(request, transactionFormBean, bindException);
	        Object resultObj = model.get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObj;
	        //assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	        assertNull(transactionsModelResult);
	    }

	    @Test
	    public void whenReferenceData_GivenAccountSubtypeIDA_ThenReturnModelTransactionsWithShowDetailEnabled() throws Exception {
	        initializeReferenceData();
	        List<AbstractStatement> abstractStatements = new ArrayList<>();
	        Balance balance = new Balance("Amount", new BigDecimal("10000"));
	        Account accountBackendObj = new DepositAccount();
	        AccountRecord accountRecord = new DepositAccountRecord();
	        accountRecord.setProductId("465as6f4654");
	        accountBackendObj.setAccountID("564654564654654");
	        accountBackendObj.setAccountListBalance(balance);
	        accountBackendObj.setSubType("IDA");
	        accountBackendObj.setRecords(accountRecord);
	        tvFrontendAccount.setBackendObject(accountBackendObj);
	        tvFrontendAccount.setStatements(abstractStatements);
	        userAccounts.add(tvFrontendAccount);

	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransaction.setDescription("CURRENT");
	        abstractTransaction.setType("DB");
	        abstractTransaction.setDate(Calendar.getInstance());
	        abstractTransactionList.add(abstractTransaction);

	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        when(transactionFormBean.getAccount()).thenReturn(null);
	        when(userSession.getAccounts()).thenReturn(userAccounts);
	        when(AccountUtils.getIsTransactional(any())).thenReturn("true");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(customerServices.getDepositCurrentTransactions(any())).thenReturn(transactionResponse);
	        when(customerService.getMortgageTransactions(any())).thenReturn(transactionResponse);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));	        
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(request.getParameter("isTab")).thenReturn("");
	        when(request.getParameter("trxTabNum")).thenReturn("");
	        when(Utils.getRSALanguage(any(HttpServletRequest.class))).thenReturn("en");
	        when(Utils.getStatements(any(ArrayList.class), any(LinkedHashMap.class), anyString(), any(Boolean.class)))
	                .thenReturn(new LinkedHashMap<>());
	        when(Utils.haveType(anyString(), anyString())).thenReturn(Boolean.FALSE);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1187)).thenReturn(Boolean.FALSE);

	        Map<String, Object> model = transactionForm.referenceData(request, transactionFormBean, bindException);
	        Object resultObjTrans = model.get("transactions");
	        Object resultObjInProc = model.get("inprocess");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObjTrans;
	        //assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	        transactionsModelResult = (ArrayList) resultObjInProc;
	        assertNull(transactionsModelResult);
	    }

	    @Test
	    public void whenReferenceData_GivenAccountSubtypeCCA_ThenReturnModelTransactionsWithoutShowDetailEnabled() throws Exception {
	        initializeReferenceData();
	        transactionForm.setUnversalProducts("0000");

	        List<AbstractStatement> abstractStatements = new ArrayList<>();
	        Balance balance = new Balance("Amount", new BigDecimal("10000"));
	        Account accountBackendObj = new DepositAccount();
	        AccountRecord accountRecord = new CardPlasticAccountRecord();
	        accountRecord.setProductId("465as6f4654");
	        accountBackendObj.setAccountID("564654564654654");
	        accountBackendObj.setAccountListBalance(balance);
	        accountBackendObj.setSubType("CCA");
	        accountBackendObj.setRecords(accountRecord);
	        tvFrontendAccount.setBackendObject(accountBackendObj);
	        tvFrontendAccount.setStatements(abstractStatements);
	        tvFrontendAccount.setCardPlasticAccountRecords(null);
	        userAccounts.add(tvFrontendAccount);

	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransaction.setDescription("CURRENT");
	        abstractTransaction.setType("DB");
	        abstractTransaction.setDate(Calendar.getInstance());
	        abstractTransactionList.add(abstractTransaction);

	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        when(transactionFormBean.getAccount()).thenReturn(null);
	        when(userSession.getAccounts()).thenReturn(userAccounts);
	        when(AccountUtils.getIsTransactional(any())).thenReturn("true");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        when(transactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(request.getParameter("isTab")).thenReturn("");
	        when(request.getParameter("trxTabNum")).thenReturn("");
	        when(Utils.getRSALanguage(any(HttpServletRequest.class))).thenReturn("en");
	        when(Utils.getStatements(any(ArrayList.class), any(LinkedHashMap.class), anyString(), any(Boolean.class)))
	                .thenReturn(new LinkedHashMap<>());
	        when(Utils.haveType(anyString(), anyString())).thenReturn(Boolean.FALSE);
	        when(customerServices.getCreditCardCurrentTransactions(any(TransactionRequest.class)))
	                .thenReturn(transactionResponse);
	        when(transactionResponse.getPagesAvailable()).thenReturn(0);
	        when(transactionResponse.isCardPanId()).thenReturn(Boolean.FALSE);
	        when(transactionFormBean.getActivity()).thenReturn("xxx");
	        when(CookieUtils.getCookie(request, "client-version")).thenReturn(versionCookie);
	        when(versionCookie.getValue()).thenReturn("mobile");

	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE1187)).thenReturn(Boolean.FALSE);
	        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CASH_REWARDS)).thenReturn(Boolean.FALSE);
	        when(userSession.hasVirginIslandAccount()).thenReturn(Boolean.TRUE);

	        Map<String, Object> model = transactionForm.referenceData(request, transactionFormBean, bindException);
	        Object resultObjTrans = model.get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObjTrans;
	        assertNull(transactionsModelResult);
	    }

	    @Test
	    public void whenReferenceData_GivenAccountSubtypePRM_ThenReturnModelTransactionsWithShowDetailEnabled() throws Exception {
	        initializeReferenceData();
	        transactionForm.setUnversalProducts("0000");

	        List<AbstractStatement> abstractStatements = new ArrayList<>();
	        Balance balance = new Balance("Amount", new BigDecimal("10000"));
	        Account accountBackendObj = new DepositAccount();
	        AccountRecord accountRecord = new CardPlasticAccountRecord();
	        accountRecord.setProductId("465as6f4654");
	        accountBackendObj.setAccountID("564654564654654");
	        accountBackendObj.setAccountListBalance(balance);
	        accountBackendObj.setSubType("PRM");
	        accountBackendObj.setRecords(accountRecord);
	        tvFrontendAccount.setBackendObject(accountBackendObj);
	        tvFrontendAccount.setStatements(abstractStatements);
	        tvFrontendAccount.setCardPlasticAccountRecords(null);
	        userAccounts.add(tvFrontendAccount);

	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransaction.setDescription("CURRENT");
	        abstractTransaction.setType("DB");
	        abstractTransaction.setDate(Calendar.getInstance());
	        abstractTransactionList.add(abstractTransaction);

	        CustomerInteractionEvent event = new CustomerInteractionEvent("", "", "", "", "");
	        when(transactionFormBean.getAccount()).thenReturn(null);
	        when(userSession.getAccounts()).thenReturn(userAccounts);
	        when(AccountUtils.getIsTransactional(any())).thenReturn("true");
	        when(userSession.getDisplayTransactionDowntime()).thenReturn(Boolean.FALSE);
	        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(event);
	        PowerMockito.doReturn(abstractTransactionList).when(transactionForm, "setTransactionsCodeReason", any(List.class));
	        when(request.getParameter("isTab")).thenReturn("");
	        when(request.getParameter("trxTabNum")).thenReturn("");
	        when(Utils.getRSALanguage(any(HttpServletRequest.class))).thenReturn("en");
	        when(Utils.getStatements(any(ArrayList.class), any(LinkedHashMap.class), anyString(), any(Boolean.class)))
	                .thenReturn(new LinkedHashMap<>());
	        when(Utils.haveType(anyString(), anyString())).thenReturn(Boolean.FALSE);
	        when(Utils.getStatementById(anyString(), any(ArrayList.class))).thenReturn(new CreditCardStatement());
	        when(customerServices.getPremiaTransactionsByCategory(any(TransactionRequest.class)))
	                .thenReturn(premiaTransactionResponse);
	        when(premiaTransactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
	        when(premiaTransactionResponse.getTransactions()).thenReturn(abstractTransactionList);
	        when(premiaTransactionResponse.getCategoryTotal()).thenReturn(new BigDecimal("123456789"));
	        when(transactionFormBean.getActivity()).thenReturn("xxx");
	        when(CookieUtils.getCookie(request, "client-version")).thenReturn(versionCookie);
	        when(versionCookie.getValue()).thenReturn("mobile");

	        Map<String, Object> model = transactionForm.referenceData(request, transactionFormBean, bindException);
	        Object resultObjTrans = model.get("transactions");
	        List<RunningBalanceTransaction> transactionsModelResult = (List) resultObjTrans;
	        //assertTrue(transactionsModelResult.get(0).getShowDetailEnabled());
	        assertNull(transactionsModelResult);
	    }

	    @Test
	    public void whenSetTransactionsCodeReason_GivenTransactionCodes_ThenReturnTransactionListWithCodes() throws Exception {

	        abstractTransactionList = new ArrayList<>();
	        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
	        abstractTransaction.setTransactionCode("671");
	        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
	        abstractTransaction.setAmount(new BigDecimal("100"));
	        abstractTransaction.setDescription("IN PROCESS");
	        abstractTransaction.setType("CR");
	        abstractTransactionList.add(abstractTransaction);

	        PowerMockito.doNothing().when(TransactionUtil.class, "getTransactions", any(ArrayList.class), anyString(), anyString());
	        List<AbstractTransaction> result = Whitebox
	                .invokeMethod(transactionForm, "setTransactionsCodeReason", abstractTransactionList);
	        assertTrue(result.size() > 0);
	    }
	    
	       
		 private void initializeReferenceData() {

		        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		        when(userSession.getCustomerService()).thenReturn(customerServices);
		        when(userSession.getBankingSession()).thenReturn(bankingSession);
		        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		        when(messageSource.getMessage(any(String.class), any(), any())).thenReturn("DISABLED");
		        when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		        when(userSession.getPfmFirstTime()).thenReturn("Y");
		        //when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		        //when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.PFM)).thenReturn(globalEntitlement);
		        //when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.DISABLED);
		        when(customerServices.getGlobalEntitlement(EntitlementType.PFM)).thenReturn(globalEntitlement);
		        when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.DISABLED);

		        CustomerAccountPreferences customerAccountPreferences = new CustomerAccountPreferences();
		        TVAccountProductDefinition tvAccountProductDefinition = new TVAccountProductDefinition();
		        VaultAccount vaultAccount = new VaultAccount();
		        tvFrontendAccount = new TVFrontendAccount(vaultAccount);
		        AccountFeatures accountFeatures = new AccountFeatures();

		        tvAccountProductDefinition.setProductType("MLA");
		        accountFeatures.setShowStatement(Boolean.TRUE);
		        tvFrontendAccount.setCustomerAccountPreferences(customerAccountPreferences);
		        tvFrontendAccount.setFeatures(accountFeatures);
		        tvFrontendAccount.setHide(Boolean.FALSE);
		        tvFrontendAccount.setNickname("Cuentilla");
		        tvFrontendAccount.setAccountNumberSuffix("MLA4654654654");
		        tvFrontendAccount.setProductDefinition(tvAccountProductDefinition);
		        tvFrontendAccount.setFrontEndId("dsf464465sdf465a456");
		        userAccounts = new ArrayList<>();

		        abstractTransactionList = new ArrayList<>();
		        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
		        abstractTransaction.setTransactionCode("671");
		        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
		        abstractTransaction.setAmount(new BigDecimal("100"));
		        abstractTransaction.setDescription("IN PROCESS");
		        abstractTransaction.setType("CR");
		        abstractTransactionList.add(abstractTransaction);
		    }
	
	@SuppressWarnings("unchecked")
    private void initializeOnSubmit() throws Exception {
        //request = new MockHttpServletRequest();
        //response = new MockHttpServletResponse();
        DepositStatement depositStatement = new DepositStatement();
        depositStatement.setStartBalance(new BigDecimal("100"));

        abstractTransactionList = new ArrayList<>();
        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
        abstractTransaction.setTransactionCode("671");
        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
        abstractTransaction.setAmount(new BigDecimal("100"));
        abstractTransactionList.add(abstractTransaction);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(Utils.getAccountById(any(), any())).thenReturn(account);
        when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
        when(account.getAccountNumber()).thenReturn("0377810550806217");
        when(transactionFormBean.getCategory()).thenReturn("");
        when(transactionFormBean.getActivity()).thenReturn("ALL");
        when(messageSource.getMessage("transactions.showAll.hide", null, Locale.ENGLISH)).thenReturn("NOENABLED");
        when(Utils.getRSALanguage(request)).thenReturn("sp");
        when(account.getAccountProductId()).thenReturn("AEULT");
        when(account.getProductDefinition()).thenReturn(tvAccountProductDefinition);
        AccountFeatures features = new AccountFeatures();
        features.setShowStatement(true);
        when(account.getFeatures()).thenReturn(features);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513)).thenReturn(false);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        transactionResponse.setStatusResult(ResultStatus.SUCCESS);
        when(customerServices.getMortgageTransactions(any())).thenReturn(transactionResponse);
        when(transactionResponse.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
        List<AbstractTransaction> transactions = PowerMockito.mock(List.class);
        when(transactionResponse.getTransactions()).thenReturn(transactions);
        when(transactions.size()).thenReturn(1);
    }
	
    private void initializeTransactionList() {
        abstractTransactionListPRM = new ArrayList<>();
        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
        abstractTransaction.setTransactionCode("671");
        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
        abstractTransaction.setAmount(new BigDecimal("100"));
        abstractTransactionListPRM.add(abstractTransaction);
    }
    
    @Test
    public void whenSetTransactionsCodeReason_GivenTransaction_ThenReturnTransactionList () throws Exception {

        abstractTransactionList = new ArrayList<>();
        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
        abstractTransaction.setTransactionCode("671");
        abstractTransaction.setShowDetailEnabled(Boolean.TRUE);
        abstractTransaction.setAmount(new BigDecimal("100"));
        abstractTransaction.setDescription("ATH Movil 23456");
        abstractTransaction.setType("DB");
        abstractTransactionList.add(abstractTransaction);
        
        List<AbstractTransaction> result = Whitebox
                .invokeMethod(transactionForm, "setTransactionsCodeReason", abstractTransactionList);
        assertTrue(result.size() > 0);
    }

}
