package evertec.cibp.web.form;

import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;
import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerWealthPreferred;
import evertec.cibp.core.models.customer.applications.EpayrollApplication;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.EpayrollServiceBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.*;
import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PrepareForTest({CustomerFacade.class,StaticMessageSource.class, WebUtils.class, Utils.class, CustomerWealthPreferred.class, ClientUtils.class})
public class EpayrollServiceFormUT extends AbstractModelAndViewTests {

    private MockHttpServletRequest request;

    private HttpServletResponse response;

    @Mock
    private CustomerFacade customerFacade;

    @Mock
    private UserSession userSession;

    @Mock
    private CustomerServices customerService;

    @InjectMocks
    private EpayrollServiceForm controller;

    @Mock
    private TVFrontendAccount account;

    @Mock
    private MessageSource messageSource;

    @Mock
    private DeviceRequest deviceRequest;

    @Mock
    private CardPlasticAccountRecord cardPlasticAccountRecord;

    @Mock
    private CardPlasticFrontend cardPlasticFrontend;

    @Mock
    private EpayrollApplication epayrollApplication;

    @Mock
    private MicroserviceStatusResponse microserviceStatusResponse;

    @Mock
    private CustomerFacade customerFacadeSingleton;

    @Before
    public void setUp() throws Exception {

        PowerMockito.mockStatic(Utils.class);

        PowerMockito.mockStatic(StaticMessageSource.class);

        PowerMockito.mockStatic(WebUtils.class);

        PowerMockito.mockStatic(ClientUtils.class);

        PowerMockito.mockStatic(CustomerFacade.class);

        request = new MockHttpServletRequest();

        response = new MockHttpServletResponse();

        super.setUp();

    }

    @Test
    @Ignore("TIENE ERRORES")
    public void whenOnBindAndValidate_FormatEstimatedAmountVendorAndPayroll_ThenReturnErrorFalse() throws Exception {
        EpayrollServiceBean bean = new EpayrollServiceBean();
        BindException error = new BindException(bean, "EpayrollServiceBean");
        EpayrollServiceForm mock = spy(controller);
        bean.setPayrollEstimatedAmount("1,000.00");
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR)).thenReturn(true);

        boolean isEvendorActivated = StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR);

        if (isEvendorActivated) {
            bean.setVendorEstimatedAmount("1,000.00");
        }
        verifyPrivate(mock).invoke("format", ArgumentMatchers.anyString());

        this.controller.onBindAndValidate(this.request, bean, error);
        assertNull(error.getFieldError("vendorEstimatedAmount"));
        assertNull(error.getFieldError("payrollEstimatedAmount"));
    }

    @Test
    public void whenOnBindAndValidate_ValidateTelephoneInput_ShouldBe_Number() throws Exception {
        EpayrollServiceBean bean = new EpayrollServiceBean();
        BindException error = new BindException(bean, "EpayrollServiceBean");
        EpayrollServiceForm mock = spy(controller);
        request = new MockHttpServletRequest();
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(Utils.getRSALanguage(request)).thenReturn("message");
        bean.setPhone1("123");
        bean.setPhone2("123");
        bean.setPhone3("1234");
        bean.setBusinessPhone1("123");
        bean.setBusinessPhone2("123");
        bean.setBusinessPhone3("1234");

        this.controller.onBindAndValidate(request, bean, error);
        assertNull(error.getFieldError("phone3"));
        assertNull(error.getFieldError("businessPhone3"));
    }

    @Test
    public void whenOnBindAndValidate_ValidateTelephoneInput_ShouldBe_Number_ThenReturnError() throws Exception {
        EpayrollServiceBean bean = new EpayrollServiceBean();
        BindException error = new BindException(bean, "EpayrollServiceBean");
        EpayrollServiceForm mock = spy(controller);
        request = new MockHttpServletRequest();
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(Utils.getRSALanguage(request)).thenReturn("message");
        bean.setPhone1("Test");
        bean.setPhone2("Test");
        bean.setPhone3("Test");
        bean.setBusinessPhone1("Test");
        bean.setBusinessPhone2("Test");
        bean.setBusinessPhone3("Test");

        this.controller.onBindAndValidate(request, bean, error);
        assertNotNull(error.getFieldError("phone3"));
        assertNotNull(error.getFieldError("businessPhone3"));
    }

    @Test
    public void whenOnSubmit_saveEpayrollApplicationNew_Given_ThenReturnTrue() throws Exception {

        EpayrollServiceBean bean = getBean();

        request = new MockHttpServletRequest();

        response = new MockHttpServletResponse();

        BindException error = new BindException(bean, "EpayrollServiceBean");

        Map<Object, Object> informationQuestion = new HashMap<>();
        informationQuestion.put("01", "value1");
        informationQuestion.put("02", "value2");
        informationQuestion.put("03", "value3");

        List<TVFrontendAccount> listadoTVFrontendAccount = new ArrayList();
        listadoTVFrontendAccount.add(new TVFrontendAccount(new Account() {
        }));
        listadoTVFrontendAccount.add(new TVFrontendAccount(new Account() {
        }));

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);

        when(userSession.getAccounts()).thenReturn(listadoTVFrontendAccount);

        when(Utils.getAccountById(any(), any())).thenReturn(account);

        when(account.getAccountNumber()).thenReturn("1234567");

        CustomerProfile customerProfile = new CustomerProfile();

        when(userSession.getCustomerService()).thenReturn(customerService);

        when(userSession.getCustomerService().saveEpayrollApplicationNew(customerProfile, epayrollApplication)).thenReturn(true);

        when(ClientUtils.getMicroserviceEndpointAndStatus("customers.application")).thenReturn(microserviceStatusResponse);

        when(ClientUtils.getMicroserviceEndpointAndStatus("customers.application").isEnabled()).thenReturn(false);

        when(customerFacade.saveEpayrollApplicationNew(epayrollApplication, request)).thenReturn(true);

        when(messageSource.getMessage(any(), any())).thenReturn("message");

        when(Utils.getRSALanguage(request)).thenReturn("message");

        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);

        when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn("1234567");

        when(userSession.getInformationQuestion()).thenReturn(informationQuestion);

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLLENHA)).thenReturn(false);

        when(userSession.getCustomerProfile()).thenReturn(customerProfile);

        CardPlasticFrontend[] cardPlasticFrontends = new CardPlasticFrontend[2];

        when(userSession.getCardPlastics(account)).thenReturn(cardPlasticFrontends);

        CardPlasticFrontend[] plasticFrontendArray = userSession.getCardPlastics(account);

        plasticFrontendArray[0] = cardPlasticFrontend;

        when(plasticFrontendArray[0].getCardPlasticAccountRecord()).thenReturn(cardPlasticAccountRecord);

        when(userSession.getCustomerService().saveEpayrollApplicationNew(userSession.getCustomerProfile(),
                epayrollApplication)).thenReturn(false);

        when(plasticFrontendArray[0].getCardPlasticAccountRecord().getCardPANId()).thenReturn("1234567");

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR)).thenReturn(false);

        assertNotNull(controller.onSubmit(request, response, bean, error));
    }

    @Test(expected = RuntimeException.class)
    public void whenOnSubmit_saveEpayrollApplicationNew_Given_null_ThenReturnRuntimeException() throws Exception {

        EpayrollServiceBean bean = new EpayrollServiceBean();

        BindException error = new BindException(bean, "EpayrollServiceBean");

        CustomerFacade.getInstance().saveEpayrollApplicationNew(null, null);

        controller.onSubmit(request, response, bean, error);
    }


    @Test
    public void whenOnSubmit_saveEpayrollApplicationNew_Given_ThenReturnFalse() throws Exception {

        EpayrollServiceBean bean = getBean();

        request = new MockHttpServletRequest();

        response = new MockHttpServletResponse();

        BindException error = new BindException(bean, "EpayrollServiceBean");

        Map<Object, Object> informationQuestion = new HashMap<>();
        informationQuestion.put("01", "value1");
        informationQuestion.put("02", "value2");
        informationQuestion.put("03", "value3");

        List<TVFrontendAccount> listadoTVFrontendAccount = new ArrayList();
        listadoTVFrontendAccount.add(new TVFrontendAccount(new Account() {
        }));
        listadoTVFrontendAccount.add(new TVFrontendAccount(new Account() {
        }));

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);

        when(userSession.getAccounts()).thenReturn(listadoTVFrontendAccount);

        when(Utils.getAccountById(any(), any())).thenReturn(account);

        when(account.getAccountNumber()).thenReturn("1234567");

        CustomerProfile customerProfile = new CustomerProfile();

        when(userSession.getCustomerService()).thenReturn(customerService);

        when(ClientUtils.getMicroserviceEndpointAndStatus("customers.application")).thenReturn(microserviceStatusResponse);

        when(ClientUtils.getMicroserviceEndpointAndStatus("customers.application").isEnabled()).thenReturn(false);

        when(messageSource.getMessage(any(), any())).thenReturn("message");

        when(Utils.getRSALanguage(request)).thenReturn("message");

        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);

        when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn("1234567");

        when(userSession.getInformationQuestion()).thenReturn(informationQuestion);

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLLENHA)).thenReturn(true);

        when(userSession.getCustomerProfile()).thenReturn(customerProfile);

        CardPlasticFrontend[] cardPlasticFrontends = new CardPlasticFrontend[0];

        when(userSession.getCardPlastics(account)).thenReturn(cardPlasticFrontends);

        CustomerFacade cf = mock(CustomerFacade.class);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);

        when(CustomerFacade.getInstance()).thenReturn(cf);

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR)).thenReturn(true);

        when(CustomerFacade.getInstance().saveEpayrollApplicationNew(any(EpayrollApplication.class), eq(request))).thenReturn(true);

        assertNotNull(controller.onSubmit(request, response, bean, error));
    }

    @Test
    public void whenOnSubmit_saveEpayrollApplicationNew_Given_ThenReturnFalse_VendorFalse() throws Exception {

        EpayrollServiceBean bean = getBean();

        request = new MockHttpServletRequest();

        response = new MockHttpServletResponse();

        BindException error = new BindException(bean, "EpayrollServiceBean");

        Map<Object, Object> informationQuestion = new HashMap<>();
        informationQuestion.put("01", "value1");
        informationQuestion.put("02", "value2");
        informationQuestion.put("03", "value3");

        List<TVFrontendAccount> listadoTVFrontendAccount = new ArrayList();
        listadoTVFrontendAccount.add(new TVFrontendAccount(new Account() {
        }));
        listadoTVFrontendAccount.add(new TVFrontendAccount(new Account() {
        }));

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);

        when(userSession.getAccounts()).thenReturn(listadoTVFrontendAccount);

        when(Utils.getAccountById(any(), any())).thenReturn(account);

        when(account.getAccountNumber()).thenReturn("1234567");

        CustomerProfile customerProfile = new CustomerProfile();

        when(userSession.getCustomerService()).thenReturn(customerService);

        when(ClientUtils.getMicroserviceEndpointAndStatus("customers.application")).thenReturn(microserviceStatusResponse);

        when(ClientUtils.getMicroserviceEndpointAndStatus("customers.application").isEnabled()).thenReturn(false);

        when(messageSource.getMessage(any(), any())).thenReturn("message");

        when(Utils.getRSALanguage(request)).thenReturn("message");

        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);

        when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn("1234567");

        when(userSession.getInformationQuestion()).thenReturn(informationQuestion);

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLLENHA)).thenReturn(true);

        when(userSession.getCustomerProfile()).thenReturn(customerProfile);

        CardPlasticFrontend[] cardPlasticFrontends = new CardPlasticFrontend[0];

        when(userSession.getCardPlastics(account)).thenReturn(cardPlasticFrontends);

        CustomerFacade cf = mock(CustomerFacade.class);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);

        when(CustomerFacade.getInstance()).thenReturn(cf);

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR)).thenReturn(false);

        when(CustomerFacade.getInstance().saveEpayrollApplicationNew(any(EpayrollApplication.class), eq(request))).thenReturn(true);

        assertNotNull(controller.onSubmit(request, response, bean, error));
    }

    private EpayrollServiceBean getBean(){

        EpayrollServiceBean bean = new EpayrollServiceBean();
        bean.setName("name");
        bean.setPhone1("1234567");
        bean.setPhone2("1234567");
        bean.setPhone3("1234567");
        bean.setBusinessName("business");
        bean.setBusinessPhone1("1234567");
        bean.setBusinessPhone2("1234567");
        bean.setBusinessPhone3("1234567");

        bean.setResidentialAddress1("Address");
        bean.setResidentialAddress2("Address");
        bean.setResidentialCity("city");
        bean.setResidentialState("states");
        bean.setResidentialZipCode1("code1");

        bean.setFaxPhone1("1234567");
        bean.setFaxPhone2("1234567");
        bean.setFaxPhone3("1234567");

        bean.setSsne1("12345");
        bean.setSsne2("12345");
        bean.setSsne3("12345");

        bean.setAccount("12345");

        bean.setPayrollEstimatedAmount("12345");
        bean.setVendorEstimatedAmount("12345");
        return bean;
    }
}
