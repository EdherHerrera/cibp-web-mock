package evertec.cibp.web.form.mobile;

import com.google.gson.Gson;
import evertec.cibp.core.models.customer.CustomerViewedNotification;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.UnicaApplication;
import evertec.cibp.web.models.customer.CustomerMessageContent;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.UnicaUtils;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.unica.UnicaApplicationInfo;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.script.mustache.SearchTemplateRequest;
import org.elasticsearch.search.SearchHit;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;
import org.apache.commons.collections.CollectionUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.mockito.MockitoAnnotations;
import com.thoughtworks.xstream.XStream;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.web.models.customer.*;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest({SearchHit.class, SearchRequest.class, CustomerServices.class, WebUtils.class,
        StaticMessageSource.class, MobileNotificationCenterController.class, Utils.class, UnicaUtils.class,
        WebUtils.class, MobileNotificationCenterController.class, StaticMessageSource.class, CollectionUtils.class})
public class MobileNotificationCenterControllerUT extends AbstractModelAndViewTests {
    
    private static final String PROFILE_ID = "ff8081811ac1d21c011ac2a4a5a02f2d";
    private static final String ELASTICSEARCH_INDEX = "push-notifications";
    private static final String ELASTICSEARCH_ENDPOINT = "http://127.0.0.1:9200";
    private static final String ELASTICSEARCH_SEARCH_TEMPLATE = "push-notifications-detail";
    private static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
	private static final String CREDIT_APP_INPROCESS_MSG = "in process";
	private static final String CREDIT_APP_APPROVED_MSG = "approved";
	private static final String CREDIT_APP_TURNEDDOWN_MSG = "turned down";
	private static final String CREDIT_APP_BOOKED_MSG = "booked";
    private static String claimInProcess;
    final Properties attributes = null;


    private SearchHit searchHit;

    private SearchRequest searchRequest;
    
    @Mock
    private UserSession userSession;

    @Mock
    private MessageSource messageSource;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private CustomerServices customerService;

    @Mock
    private CustomerProfile customerProfile;


    @Mock
    private CustomerViewedNotification viewedNotification;

    @Mock
    private Properties attributesResult;


    @InjectMocks
    @Spy
    private final MobileNotificationCenterController controller =  new MobileNotificationCenterController();
    
    @Before
    public void setUp() throws Exception {
        searchHit = PowerMockito.mock(SearchHit.class);
        searchRequest = PowerMockito.mock(SearchRequest.class);
        request = new MockHttpServletRequest();
        customerService = PowerMockito.mock(CustomerServices.class);
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(CollectionUtils.class);
        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(UnicaUtils.class);
        
        controller.setElasticsearchEndpoint(ELASTICSEARCH_ENDPOINT);
        controller.setElasticsearchTemplatePush(ELASTICSEARCH_SEARCH_TEMPLATE);
        
        when(messageSource.getMessage("notification.center.unica.cc.inprocess", null, new Locale("en"))).thenReturn(CREDIT_APP_INPROCESS_MSG);
        when(messageSource.getMessage("notification.center.unica.cc.approved", null, new Locale("en"))).thenReturn(CREDIT_APP_APPROVED_MSG);
        when(messageSource.getMessage("notification.center.unica.cc.turneddown", null, new Locale("en"))).thenReturn(CREDIT_APP_TURNEDDOWN_MSG);
        when(messageSource.getMessage("notification.center.unica.cc.booked", null, new Locale("en"))).thenReturn(CREDIT_APP_BOOKED_MSG);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void whenGetNotificationCenter() throws Exception {

        when(WebUtils.getSessionAttribute(any(), anyString())).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);

        List<CustomerMessageContent> customerMessageContents = new ArrayList<>();
        CustomerMessageContent customerMC = new CustomerMessageContent("123456", CustomerMessageContentType.INFORMATION, "Test" , "url");
        customerMC.setCampaignDate(new Date());
        customerMC.setType(CustomerMessageContentType.INFORMATION);
        customerMC.setMessage("Test");
        customerMessageContents.add(customerMC);
        customerMC = new CustomerMessageContent("123456", CustomerMessageContentType.INFORMATION, "Test2" , "url");
        customerMC.setCampaignDate(new Date());
        customerMC.setType(CustomerMessageContentType.INFORMATION);
        customerMC.setMessage("Test2");
        customerMessageContents.add(customerMC);
        when(userSession.getMessagesList()).thenReturn(customerMessageContents);


        PowerMockito.doNothing().when(controller, "addCreditApplications", any(HttpServletRequest.class), any(List.class));

        when(userSession.getNewNotifications()).thenReturn(Boolean.FALSE);

        when(CollectionUtils.isEmpty(anyCollection())).thenReturn(Boolean.FALSE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);

        PowerMockito.doReturn(null).when(controller, "getPushNotificationsList", any());
        ModelAndView result = controller.notificationCenter(request, response);
        List<CustomerMessageContent> customerMCResult = (ArrayList<CustomerMessageContent>) result.getModel()
                .get("notificationsList");
        assertEquals(userSession.getMessagesList().get(0).getMessageCSS(), "NC_CSS_NEW_MESSAGE");
        verify(userSession).setNewNotifications(true);
    }

    @Test
    public void whenGetNotificationCenterCollectionUtilsisEmpty() throws Exception {

        when(WebUtils.getSessionAttribute(any(), anyString())).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);

        List<CustomerMessageContent> customerMessageContents = new ArrayList<>();
        CustomerMessageContent customerMC = new CustomerMessageContent("123456", CustomerMessageContentType.INFORMATION, "Test" , "url");
        customerMC.setCampaignDate(new Date());
        customerMC.setType(CustomerMessageContentType.INFORMATION);
        customerMC.setMessage("Test");
        customerMessageContents.add(customerMC);
        customerMC = new CustomerMessageContent("123456", CustomerMessageContentType.INFORMATION, "Test2" , "url");
        customerMC.setType(CustomerMessageContentType.INFORMATION);
        customerMC.setMessage("Test2");
        customerMessageContents.add(customerMC);
        when(userSession.getMessagesList()).thenReturn(customerMessageContents);


        PowerMockito.doNothing().when(controller, "addCreditApplications", any(HttpServletRequest.class), any(List.class));

        when(CollectionUtils.isEmpty(anyCollection())).thenReturn(Boolean.TRUE);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);

        PowerMockito.doReturn(null).when(controller, "getPushNotificationsList", any());
        ModelAndView result = controller.notificationCenter(request, response);
        verify(userSession).setNewNotifications(false);
        verify(userSession).setMessagesList(customerMessageContents);
    }

    /*@Test
	public void whenUserSessionGetClaimMessagesIsNull() throws Exception {
		when(WebUtils.getSessionAttribute(any(), anyString())).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerClaims()).thenReturn(new ArrayList<CustomerClaims>());
		PowerMockito.whenNew(CustomerClaimUtils.class).withAnyArguments().thenReturn(claimsActions);

		when(userSession.getClaimMessages()).thenReturn(null);

		when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);

		List<CustomerClaimMessage> claimServices = new ArrayList<>();
		List<CustomerClaims> customerClaims = new ArrayList<>();
		when(customerService.findAllClaimByCustomerProfile(customerProfile)).thenReturn(customerClaims);
		when(claimsActions.getClaims(userSession, request, claimInProcess)).thenReturn(claimServices);

		customerClaims.add(new CustomerClaims("8a81a0b76e85657016e8583691c0011", "8a81a0b7692ba32a01696e6118e203bd",
				"30004-08MAY2030", "New Status", "Status changed", new Date()));

		PowerMockito.doNothing().when(customerService).removeCustomerClaims(customerClaims);

		ModelAndView result = controller.notificationCenter(request, response);
		assertViewName(result, "notificationCenter");
	}*/

    @Test
    public void whenUserSessionGetViewedNotificationIsNull() throws Exception {
        when(WebUtils.getSessionAttribute(any(), anyString())).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);


        PowerMockito.doNothing().when(controller, "addCreditApplications", any(HttpServletRequest.class), any(List.class));

        when(userSession.getViewedNotification()).thenReturn(null);

        List<CustomerViewedNotification> viewedNotifications = new ArrayList<>();

        when(userSession.getCustomerService().getCustomerViewedNotifications(userSession.getCustomerProfile())).thenReturn(viewedNotifications);

        viewedNotifications.add(new CustomerViewedNotification(PROFILE_ID));

        when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);

        PowerMockito.doReturn(null).when(controller, "getPushNotificationsList", any());
        ModelAndView result = controller.notificationCenter(request, response);

        assertViewName(result, "notificationCenter");
    }

    @Test
    public void whenUserSessionGetViewedNotificationIsNotNull() throws Exception {
        when(WebUtils.getSessionAttribute(any(), anyString())).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);

        when(userSession.getViewedNotification()).thenReturn(viewedNotification);
        when(userSession.getViewedNotification()).thenReturn(new CustomerViewedNotification()).thenReturn(viewedNotification);
        when(viewedNotification.getAttributeData()).thenReturn("");

        List<CustomerViewedNotification> viewedNotification = new ArrayList<>();
        viewedNotification.add(userSession.getViewedNotification());

        List<CustomerMessageContent> customerMessageContents = new ArrayList<>();
        CustomerMessageContent customerMC = new CustomerMessageContent("123456", CustomerMessageContentType.INFORMATION, "mensaje" , "url");
        customerMC.setId("123456");
        customerMC.setCampaignDate(new Date());
        customerMC.setType(CustomerMessageContentType.INFORMATION);
        customerMessageContents.add(customerMC);
        when(userSession.getMessagesList()).thenReturn(customerMessageContents);


        PowerMockito.doNothing().when(controller, "addCreditApplications", any(HttpServletRequest.class), any(List.class));

        when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);

        PowerMockito.doReturn(null).when(controller, "getPushNotificationsList", any());
        ModelAndView result = controller.notificationCenter(request, response);
        List<CustomerMessageContent> customerMCResult = (ArrayList<CustomerMessageContent>) result.getModel().get("notificationsList");
        assertViewName(result, "notificationCenter");
    }

    @Test
    public void whenUserSessionGetViewedNotificationIsNotNullisNoBlankOrNull() throws Exception {
        when(WebUtils.getSessionAttribute(any(), anyString())).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);

        when(userSession.getViewedNotification()).thenReturn(viewedNotification);
        when(userSession.getViewedNotification()).thenReturn(new CustomerViewedNotification()).thenReturn(viewedNotification);
        when(viewedNotification.getAttributeData()).thenReturn("");

        final XStream xstream = new XStream();

        Properties propertiesResult = (Properties) xstream.fromXML("<properties>\r\n"
                + "  <property name=\"402869d67531d9e40175418116050055\" value=\"1603228588745\"/>\r\n"
                + "</properties>");

        PowerMockito.doReturn(attributesResult).when(controller, "getAttributes", anyString());
        when(attributesResult.getProperty(anyString())).thenReturn("465465465456");

        List<CustomerMessageContent> customerMessageContents = new ArrayList<>();
        CustomerMessageContent customerMC = new CustomerMessageContent("123456", CustomerMessageContentType.INFORMATION, "mensaje" , "url");
        customerMC.setId("123456");
        customerMC.setCampaignDate(new Date());
        customerMC.setType(CustomerMessageContentType.INFORMATION);
        customerMessageContents.add(customerMC);
        when(userSession.getMessagesList()).thenReturn(customerMessageContents);


        PowerMockito.doNothing().when(controller, "addCreditApplications", any(HttpServletRequest.class), any(List.class));

        when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);

        PowerMockito.doReturn(null).when(controller, "getPushNotificationsList", any());
        ModelAndView result = controller.notificationCenter(request, response);
        List<CustomerMessageContent> customerMCResult = (ArrayList<CustomerMessageContent>) result.getModel()
                .get("notificationsList");
        assertViewName(result, "notificationCenter");
    }

    @Test
    public void whenUserSessionGetViewedNotificationIsNotNullException() throws Exception {
        when(WebUtils.getSessionAttribute(any(), anyString())).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);

        when(userSession.getViewedNotification()).thenReturn(viewedNotification);
        when(userSession.getViewedNotification()).thenReturn(new CustomerViewedNotification()).thenReturn(viewedNotification);
        when(viewedNotification.getAttributeData()).thenReturn("");

        List<CustomerViewedNotification> viewedNotification = new ArrayList<>();
        viewedNotification.add(userSession.getViewedNotification());

        List<CustomerMessageContent> customerMessageContents = new ArrayList<>();
        CustomerMessageContent customerMC = new CustomerMessageContent("123456", CustomerMessageContentType.INFORMATION, "mensaje" , "url");
        customerMC.setCampaignDate(new Date());
        customerMessageContents.add(customerMC);
        when(userSession.getMessagesList()).thenReturn(customerMessageContents);


        PowerMockito.doNothing().when(controller, "addCreditApplications", any(HttpServletRequest.class), any(List.class));

        when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);

        PowerMockito.doReturn(null).when(controller, "getPushNotificationsList", any());
        ModelAndView result = controller.notificationCenter(request, response);
        List<CustomerMessageContent> customerMCResult = (ArrayList<CustomerMessageContent>) result.getModel().get("notificationsList");
        assertViewName(result, "notificationCenter");
    }

    @Test
    public void whenDifferentUserSessionGetUserAgentTypeThenReturnUserAgentTypeJSONAndCustomerMessageContentsIsEmpty() throws Exception {

        when(WebUtils.getSessionAttribute(any(), anyString())).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);


        List<CustomerMessageContent> customerMessageContents = new ArrayList<>();

        PowerMockito.doNothing().when(controller, "addCreditApplications", any(HttpServletRequest.class), any(List.class));

        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        when(userSession.getMessagesList()).thenReturn(customerMessageContents);

        PowerMockito.whenNew(CustomerViewedNotification.class).withArguments(userSession.getCustomerProfile().getProfileId()).thenReturn(viewedNotification);

        when(viewedNotification.getAttributeData()).thenReturn("TEST");
        PowerMockito.doNothing().when(customerService).updateCustomerViewedNotification(viewedNotification);

        ModelAndView result = controller.notificationCenter(request, response);
        assertViewName(result, "notificationCenter");

    }


    @Test
    public void whenGetPushNotificationsList_GivenNoRestClient_ThenReturnEmptyList() throws Exception {
        
        List<CustomerMessageContent> msgList = Whitebox.invokeMethod(controller, "getPushNotificationsList", userSession);
        
        assertTrue(msgList.isEmpty());
    }
    
    @Test
    public void whenGetPushSearchRequest_GivenProfileId_ThenReturnSearchTemplateRequest() throws Exception {
        
        controller.setElasticsearchIndexPush(ELASTICSEARCH_INDEX);
        
        SearchTemplateRequest request = Whitebox.invokeMethod(controller, "getPushSearchRequest", PROFILE_ID);
        
        assertEquals(PROFILE_ID, request.getScriptParams().get("profileId"));
        assertEquals(ScriptType.STORED, request.getScriptType());
        assertTrue(request.getScriptParams().containsKey("from"));
        assertTrue(request.getScriptParams().containsKey("to"));
        
    }
    
    @Test
    @SuppressWarnings("unchecked")
    public void whenGetNotificationCenterPushItemGivenValidRecordThenReturnValidItem() throws Exception{
        
        final String pushNotification = "{" + 
                "  'transactionId' : '16426d7a5d1cf35b02787e83'," + 
                "  'transactionDate' : '2019-07-03T14:26:36'," + 
                "  'transactionStatus' : 'Success'," + 
                "  'transaction' : {" + 
                "    'request' : {" + 
                "      'send' : {" + 
                "        'customer' : {" + 
                "          'userId' : 'ff80808137a9e0f80137c2c0644c61b3'," + 
                "          'username' : 'deadpool'," + 
                "          'cisNumber' : '3799508070'" + 
                "        }," + 
                "        'source' : {" + 
                "          'owner' : 'CIBP'," + 
                "          'channel' : 'Android'," + 
                "          'alertType' : '5'" + 
                "        }," + 
                "        'messages' : {" + 
                "          'push' : {" + 
                "            'message' : {" + 
                "              'token' : 'f_KLb1LuiTs:APA91bG_gNIrPL6a6LeNDRdYygqhxEcyTGIFipP5aAZWpI4DZHgLMArKijy_7zqPLa2alvxxx_tk7rI3PSuskOwa9mDEnD7b5rK3dO21C9slNzguplkAsY3dZtd3GBWyzydXJ_esNtgy'," + 
                "              'notification' : {" + 
                "                'body' : 'Su dispositivo iPhone X ha sido removido de Mi Banco Alertas.'" + 
                "              }" + 
                "            }" + 
                "          }" + 
                "        }" + 
                "      }" + 
                "    }," + 
                "    'response' : {" + 
                "      'statusCode' : '00'," + 
                "      'statusDesc' : 'Push notification was processed'," + 
                "      'statusDetail' : {" + 
                "        'name' : 'projects/popular-ios-app/messages/0:1562178396071800%63888a0363888a03'" + 
                "      }" + 
                "    }" + 
                "  }" + 
                "}";
        final Gson gson = new Gson();
        Map<String, Object> sourceMap = gson.fromJson(pushNotification, Map.class);

        when(searchHit.getSourceAsMap()).thenReturn(sourceMap);

        CustomerMessageContent pushItem = Whitebox.invokeMethod(controller, 
                "getNotificationCenterPushItem", searchHit, PreferredLanguage.ENGLISH);
        
        assertEquals("Su dispositivo iPhone X ha sido removido de Mi Banco Alertas.", pushItem.getMessage());
        assertEquals("16426d7a5d1cf35b02787e83", pushItem.getId());
        assertEquals("July 3, 2019 2:26 PM", pushItem.getDate());
        assertEquals("/img/notificationCenter/push/pushNotification-con@1x.svg", pushItem.getIconUrl());
    }
    
    @Test
    @Ignore
    @SuppressWarnings("unchecked")
    public void whenGetNotificationCenterPushItemGivenRecordWithNoMessageThenReturnNullItem() throws Exception{
        
        String pushNotificationNoMsg = "{" + 
                "  'transactionId' : '16426d7a5d1cf35b02787e83'," + 
                "  'transactionDate' : '2019-07-03T14:26:36'," + 
                "  'transactionStatus' : 'Success'," + 
                "  'transaction' : {" + 
                "    'request' : {" + 
                "      'send' : {" + 
                "        'customer' : {" + 
                "          'userId' : 'ff80808137a9e0f80137c2c0644c61b3'," + 
                "          'username' : 'deadpool'," + 
                "          'cisNumber' : '3799508070'" + 
                "        }," + 
                "        'source' : {" + 
                "          'owner' : 'CIBP'," + 
                "          'channel' : 'Android'," + 
                "          'alertType' : '5'" + 
                "        }," + 
                "        'messages' : {" + 
                "          'push' : {" + 
                "            'message' : {" + 
                "              'token' : 'f_KLb1LuiTs:APA91bG_gNIrPL6a6LeNDRdYygqhxEcyTGIFipP5aAZWpI4DZHgLMArKijy_7zqPLa2alvxxx_tk7rI3PSuskOwa9mDEnD7b5rK3dO21C9slNzguplkAsY3dZtd3GBWyzydXJ_esNtgy'" + 
                "            }" + 
                "          }" + 
                "        }" + 
                "      }" + 
                "    }," + 
                "    'response' : {" + 
                "      'statusCode' : '00'," + 
                "      'statusDesc' : 'Push notification was processed'," + 
                "      'statusDetail' : {" + 
                "        'name' : 'projects/popular-ios-app/messages/0:1562178396071800%63888a0363888a03'" + 
                "      }" + 
                "    }" + 
                "  }" + 
                "}";
        final Gson gson = new Gson();
        Map<String, Object> sourceMap = gson.fromJson(pushNotificationNoMsg, Map.class);

        when(searchHit.getSourceAsMap()).thenReturn(sourceMap);

        CustomerMessageContent pushItem = Whitebox.invokeMethod(controller, 
                "getNotificationCenterPushItem", searchHit, PreferredLanguage.ENGLISH);
        
        assertNull(pushItem);
    }
    
    @Test
    @SuppressWarnings("unchecked")
    public void whenGetNotificationCenterPushItemGivenRecordWithNoTransactionIdThenReturnItemWithNullId() throws Exception{
        
        final String pushNotificationNoId = "{" + 
                "  'transactionDate' : '2019-07-0314:26:36'," + 
                "  'transactionStatus' : 'Success'," + 
                "  'transaction' : {" + 
                "    'request' : {" + 
                "      'send' : {" + 
                "        'customer' : {" + 
                "          'userId' : 'ff80808137a9e0f80137c2c0644c61b3'," + 
                "          'username' : 'deadpool'," + 
                "          'cisNumber' : '3799508070'" + 
                "        }," + 
                "        'source' : {" + 
                "          'owner' : 'CIBP'," + 
                "          'channel' : 'Android'," + 
                "          'alertType' : '5'" + 
                "        }," + 
                "        'messages' : {" + 
                "          'push' : {" + 
                "            'message' : {" + 
                "              'token' : 'f_KLb1LuiTs:APA91bG_gNIrPL6a6LeNDRdYygqhxEcyTGIFipP5aAZWpI4DZHgLMArKijy_7zqPLa2alvxxx_tk7rI3PSuskOwa9mDEnD7b5rK3dO21C9slNzguplkAsY3dZtd3GBWyzydXJ_esNtgy'," + 
                "              'notification' : {" + 
                "                'body' : 'Su dispositivo iPhone X ha sido removido de Mi Banco Alertas.'" + 
                "              }" + 
                "            }" + 
                "          }" + 
                "        }" + 
                "      }" + 
                "    }," + 
                "    'response' : {" + 
                "      'statusCode' : '00'," + 
                "      'statusDesc' : 'Push notification was processed'," + 
                "      'statusDetail' : {" + 
                "        'name' : 'projects/popular-ios-app/messages/0:1562178396071800%63888a0363888a03'" + 
                "      }" + 
                "    }" + 
                "  }" + 
                "}";
        final Gson gson = new Gson();
        Map<String, Object> sourceMap = gson.fromJson(pushNotificationNoId, Map.class);

        when(searchHit.getSourceAsMap()).thenReturn(sourceMap);

        CustomerMessageContent pushItem = Whitebox.invokeMethod(controller, 
                "getNotificationCenterPushItem", searchHit, PreferredLanguage.ENGLISH);
        
        assertEquals("", pushItem.getId());
    }
    
    @Test
    @SuppressWarnings("unchecked")
    public void whenGetNotificationCenterPushItemGivenRecordWithInvalidDateThenReturnItemWithEmptyDate() throws Exception{
        
        final String pushNotificationInvalidDate = "{" + 
                "  'transactionId' : '16426d7a5d1cf35b02787e83'," + 
                "  'transactionDate' : '2019-07-0314:26:36'," + 
                "  'transactionStatus' : 'Success'," + 
                "  'transaction' : {" + 
                "    'request' : {" + 
                "      'send' : {" + 
                "        'customer' : {" + 
                "          'userId' : 'ff80808137a9e0f80137c2c0644c61b3'," + 
                "          'username' : 'deadpool'," + 
                "          'cisNumber' : '3799508070'" + 
                "        }," + 
                "        'source' : {" + 
                "          'owner' : 'CIBP'," + 
                "          'channel' : 'Android'," + 
                "          'alertType' : '5'" + 
                "        }," + 
                "        'messages' : {" + 
                "          'push' : {" + 
                "            'message' : {" + 
                "              'token' : 'f_KLb1LuiTs:APA91bG_gNIrPL6a6LeNDRdYygqhxEcyTGIFipP5aAZWpI4DZHgLMArKijy_7zqPLa2alvxxx_tk7rI3PSuskOwa9mDEnD7b5rK3dO21C9slNzguplkAsY3dZtd3GBWyzydXJ_esNtgy'," + 
                "              'notification' : {" + 
                "                'body' : 'Su dispositivo iPhone X ha sido removido de Mi Banco Alertas.'" + 
                "              }" + 
                "            }" + 
                "          }" + 
                "        }" + 
                "      }" + 
                "    }," + 
                "    'response' : {" + 
                "      'statusCode' : '00'," + 
                "      'statusDesc' : 'Push notification was processed'," + 
                "      'statusDetail' : {" + 
                "        'name' : 'projects/popular-ios-app/messages/0:1562178396071800%63888a0363888a03'" + 
                "      }" + 
                "    }" + 
                "  }" + 
                "}";
        final Gson gson = new Gson();
        Map<String, Object> sourceMap = gson.fromJson(pushNotificationInvalidDate, Map.class);

        when(searchHit.getSourceAsMap()).thenReturn(sourceMap);

        CustomerMessageContent pushItem = Whitebox.invokeMethod(controller, 
                "getNotificationCenterPushItem", searchHit, PreferredLanguage.ENGLISH);
        
        assertEquals("", pushItem.getDate());
    }
    
    @Test
    @SuppressWarnings("unchecked")
    public void whenGetNotificationCenterPushItemGivenRecordWithNoDateThenReturnItemWithEmptyDate() throws Exception{
        
        final String pushNotificationNoDate = "{" + 
                "  'transactionId' : '16426d7a5d1cf35b02787e83'," + 
                "  'transactionStatus' : 'Success'," + 
                "  'transaction' : {" + 
                "    'request' : {" + 
                "      'send' : {" + 
                "        'customer' : {" + 
                "          'userId' : 'ff80808137a9e0f80137c2c0644c61b3'," + 
                "          'username' : 'deadpool'," + 
                "          'cisNumber' : '3799508070'" + 
                "        }," + 
                "        'source' : {" + 
                "          'owner' : 'CIBP'," + 
                "          'channel' : 'Android'," + 
                "          'alertType' : '5'" + 
                "        }," + 
                "        'messages' : {" + 
                "          'push' : {" + 
                "            'message' : {" + 
                "              'token' : 'f_KLb1LuiTs:APA91bG_gNIrPL6a6LeNDRdYygqhxEcyTGIFipP5aAZWpI4DZHgLMArKijy_7zqPLa2alvxxx_tk7rI3PSuskOwa9mDEnD7b5rK3dO21C9slNzguplkAsY3dZtd3GBWyzydXJ_esNtgy'," + 
                "              'notification' : {" + 
                "                'body' : 'Su dispositivo iPhone X ha sido removido de Mi Banco Alertas.'" + 
                "              }" + 
                "            }" + 
                "          }" + 
                "        }" + 
                "      }" + 
                "    }," + 
                "    'response' : {" + 
                "      'statusCode' : '00'," + 
                "      'statusDesc' : 'Push notification was processed'," + 
                "      'statusDetail' : {" + 
                "        'name' : 'projects/popular-ios-app/messages/0:1562178396071800%63888a0363888a03'" + 
                "      }" + 
                "    }" + 
                "  }" + 
                "}";
        final Gson gson = new Gson();
        Map<String, Object> sourceMap = gson.fromJson(pushNotificationNoDate, Map.class);

        when(searchHit.getSourceAsMap()).thenReturn(sourceMap);

        CustomerMessageContent pushItem = Whitebox.invokeMethod(controller, 
                "getNotificationCenterPushItem", searchHit, PreferredLanguage.ENGLISH);
        
        assertEquals("", pushItem.getDate());
    }
    
    @Test
    @SuppressWarnings("unchecked")
    public void whenGetNotificationCenterPushItemGivenPushSent3HoursAgoThenReturnItemWithDateWithHoursFormat() throws Exception {
        
        final String pushNotificationRecent = "{" + 
                "  'transactionId' : '16426d7a5d1cf35b02787e83'," + 
                "  'transactionDate' : '" + DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN).format(LocalDateTime.now().minusHours(3)) + "'," +
                "  'transactionStatus' : 'Success'," + 
                "  'transaction' : {" + 
                "    'request' : {" + 
                "      'send' : {" + 
                "        'customer' : {" + 
                "          'userId' : 'ff80808137a9e0f80137c2c0644c61b3'," + 
                "          'username' : 'deadpool'," + 
                "          'cisNumber' : '3799508070'" + 
                "        }," + 
                "        'source' : {" + 
                "          'owner' : 'CIBP'," + 
                "          'channel' : 'Android'," + 
                "          'alertType' : '5'" + 
                "        }," + 
                "        'messages' : {" + 
                "          'push' : {" + 
                "            'message' : {" + 
                "              'token' : 'f_KLb1LuiTs:APA91bG_gNIrPL6a6LeNDRdYygqhxEcyTGIFipP5aAZWpI4DZHgLMArKijy_7zqPLa2alvxxx_tk7rI3PSuskOwa9mDEnD7b5rK3dO21C9slNzguplkAsY3dZtd3GBWyzydXJ_esNtgy'," + 
                "              'notification' : {" + 
                "                'body' : 'Su dispositivo iPhone X ha sido removido de Mi Banco Alertas.'" + 
                "              }" + 
                "            }" + 
                "          }" + 
                "        }" + 
                "      }" + 
                "    }," + 
                "    'response' : {" + 
                "      'statusCode' : '00'," + 
                "      'statusDesc' : 'Push notification was processed'," + 
                "      'statusDetail' : {" + 
                "        'name' : 'projects/popular-ios-app/messages/0:1562178396071800%63888a0363888a03'" + 
                "      }" + 
                "    }" + 
                "  }" + 
                "}";
        final String hoursAgo = "3 hours ago";
        final PreferredLanguage prefLanguage = PreferredLanguage.ENGLISH;
        final Gson gson = new Gson();
        Map<String, Object> sourceMap = gson.fromJson(pushNotificationRecent, Map.class);

        when(searchHit.getSourceAsMap()).thenReturn(sourceMap);
        when(messageSource.getMessage(any(String.class), any(Object[].class), any(Locale.class))).thenReturn(hoursAgo);

        CustomerMessageContent pushItem = Whitebox.invokeMethod(controller, 
                "getNotificationCenterPushItem", searchHit, prefLanguage);
        
        assertEquals(hoursAgo, pushItem.getDate());
    }
    
    @Test
    @SuppressWarnings("unchecked")
    @Ignore("WITH ERROR")
    public void whenGetNotificationCenterPushItemGivenRecentPushSent3MinutesAgoThenReturnItemWithDateWithMinutesFormat() throws Exception {
        
        final String pushNotificationRecent = "{" + 
                "  'transactionId' : '16426d7a5d1cf35b02787e83'," + 
                "  'transactionDate' : '" + DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN).format(LocalDateTime.now().minusMinutes(3)) + "'," +
                "  'transactionStatus' : 'Success'," + 
                "  'transaction' : {" + 
                "    'request' : {" + 
                "      'send' : {" + 
                "        'customer' : {" + 
                "          'userId' : 'ff80808137a9e0f80137c2c0644c61b3'," + 
                "          'username' : 'deadpool'," + 
                "          'cisNumber' : '3799508070'" + 
                "        }," + 
                "        'source' : {" + 
                "          'owner' : 'CIBP'," + 
                "          'channel' : 'Android'," + 
                "          'alertType' : '5'" + 
                "        }," + 
                "        'messages' : {" + 
                "          'push' : {" + 
                "            'message' : {" + 
                "              'token' : 'f_KLb1LuiTs:APA91bG_gNIrPL6a6LeNDRdYygqhxEcyTGIFipP5aAZWpI4DZHgLMArKijy_7zqPLa2alvxxx_tk7rI3PSuskOwa9mDEnD7b5rK3dO21C9slNzguplkAsY3dZtd3GBWyzydXJ_esNtgy'," + 
                "              'notification' : {" + 
                "                'body' : 'Su dispositivo iPhone X ha sido removido de Mi Banco Alertas.'" + 
                "              }" + 
                "            }" + 
                "          }" + 
                "        }" + 
                "      }" + 
                "    }," + 
                "    'response' : {" + 
                "      'statusCode' : '00'," + 
                "      'statusDesc' : 'Push notification was processed'," + 
                "      'statusDetail' : {" + 
                "        'name' : 'projects/popular-ios-app/messages/0:1562178396071800%63888a0363888a03'" + 
                "      }" + 
                "    }" + 
                "  }" + 
                "}";
        final String minutesAgo = "3 minutes ago";
        final PreferredLanguage prefLanguage = PreferredLanguage.ENGLISH;
        final Gson gson = new Gson();
        Map<String, Object> sourceMap = gson.fromJson(pushNotificationRecent, Map.class);

        when(searchHit.getSourceAsMap()).thenReturn(sourceMap);
        when(messageSource.getMessage(any(String.class), any(Object[].class), any(Locale.class))).thenReturn(minutesAgo);

        CustomerMessageContent pushItem = Whitebox.invokeMethod(controller, 
                "getNotificationCenterPushItem", searchHit, prefLanguage);
        
        assertEquals(minutesAgo, pushItem.getDate());
    }
    
    @Test
    @SuppressWarnings("unchecked")
    public void whenGetNotificationCenterPushItemGivenPushSent1SecondAgoThenReturnItemWithSecondsFormat() throws Exception {
        
        final String pushNotificationRecent = "{" + 
                "  'transactionId' : '16426d7a5d1cf35b02787e83'," + 
                "  'transactionDate' : '" + DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN).format(LocalDateTime.now().minusSeconds(1)) + "'," +
                "  'transactionStatus' : 'Success'," + 
                "  'transaction' : {" + 
                "    'request' : {" + 
                "      'send' : {" + 
                "        'customer' : {" + 
                "          'userId' : 'ff80808137a9e0f80137c2c0644c61b3'," + 
                "          'username' : 'deadpool'," + 
                "          'cisNumber' : '3799508070'" + 
                "        }," + 
                "        'source' : {" + 
                "          'owner' : 'CIBP'," + 
                "          'channel' : 'Android'," + 
                "          'alertType' : '5'" + 
                "        }," + 
                "        'messages' : {" + 
                "          'push' : {" + 
                "            'message' : {" + 
                "              'token' : 'f_KLb1LuiTs:APA91bG_gNIrPL6a6LeNDRdYygqhxEcyTGIFipP5aAZWpI4DZHgLMArKijy_7zqPLa2alvxxx_tk7rI3PSuskOwa9mDEnD7b5rK3dO21C9slNzguplkAsY3dZtd3GBWyzydXJ_esNtgy'," + 
                "              'notification' : {" + 
                "                'body' : 'Su dispositivo iPhone X ha sido removido de Mi Banco Alertas.'" + 
                "              }" + 
                "            }" + 
                "          }" + 
                "        }" + 
                "      }" + 
                "    }," + 
                "    'response' : {" + 
                "      'statusCode' : '00'," + 
                "      'statusDesc' : 'Push notification was processed'," + 
                "      'statusDetail' : {" + 
                "        'name' : 'projects/popular-ios-app/messages/0:1562178396071800%63888a0363888a03'" + 
                "      }" + 
                "    }" + 
                "  }" + 
                "}";
        final String minutesAgo = "1 second ago";
        final PreferredLanguage prefLanguage = PreferredLanguage.ENGLISH;
        final Gson gson = new Gson();
        Map<String, Object> sourceMap = gson.fromJson(pushNotificationRecent, Map.class);

        when(searchHit.getSourceAsMap()).thenReturn(sourceMap);
        when(messageSource.getMessage(any(String.class), any(Object[].class), any(Locale.class))).thenReturn(minutesAgo);

        CustomerMessageContent pushItem = Whitebox.invokeMethod(controller, 
                "getNotificationCenterPushItem", searchHit, prefLanguage);
        
        assertEquals(minutesAgo, pushItem.getDate());
    }
    
    /**
     * when NotificationCenter
     * Given IsCookieCategoryEnabledTrue
     * Then ReturnModelValueTrue
     *
     * @author Evertec ET58344
     * @since 09-29-2020
     * @exception Exception error
     */
    @Test
    public void whenNotificationCenter_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

        when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
        when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getMessagesList()).thenReturn(new ArrayList<>());
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1925)).thenReturn(Boolean.FALSE);
        when(userSession.getNewNotifications()).thenReturn(Boolean.FALSE);
        when(userSession.getViewedNotification()).thenReturn(new CustomerViewedNotification()).thenReturn(null);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
        PowerMockito.doReturn(new ArrayList<>()).when(controller, "getPushNotificationsList", any());
        PowerMockito.doNothing().when(controller, "addCreditApplications", any(HttpServletRequest.class), any(List.class));

        ModelAndView result = controller.notificationCenter(request, response);
        assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
    }

    @Test
    public void whenAddCreditApplications_GivenUnicaApplicationsNotNull_ThenMessagesHaveNoChanges() throws Exception {
    	List<CustomerMessageContent> messages = null;
    	List<UnicaApplication> applications = new ArrayList<UnicaApplication>();
    	
    	when(Utils.getUserSession(any())).thenReturn(userSession);
    	when(userSession.getUnicaApplications()).thenReturn(applications);
    	
    	Whitebox.invokeMethod(controller, "addCreditApplications", request, messages);
    	
    	assertEquals(null, messages);
    }
    
    @Test
    @Ignore("WITH ERROR")
    public void whenAddCreditApplications_GivenUnicaApplicationsNullAndGetApplicationsNullList_ThenMessagesHaveNoChanges() throws Exception {
    	List<CustomerMessageContent> messages = null;
    	
    	when(Utils.getUserSession(any())).thenReturn(userSession);
    	when(userSession.getUnicaApplications()).thenReturn(null);
    	when(UnicaUtils.getApplications(any(), any(), any(), any(), any(), any(), any())).thenReturn(null);
    	when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
    	
    	Whitebox.invokeMethod(controller, "addCreditApplications", request, messages);
    	
    	assertEquals(null, messages);
    }
    
    @Test
    public void whenAddCreditApplications_GivenUnicaApplicationsNullAndGetApplicationsEmptyList_ThenMessagesHaveNoChanges() throws Exception {
    	List<CustomerMessageContent> messages = null;
    	
    	when(Utils.getUserSession(any())).thenReturn(userSession);
    	when(userSession.getUnicaApplications()).thenReturn(null);
    	when(UnicaUtils.getApplications(any(), any(), any(), any(), any(), any(), any())).thenReturn(new ArrayList<UnicaApplication>());
    	when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
    	
    	Whitebox.invokeMethod(controller, "addCreditApplications", request, messages);
    	
    	assertEquals(null, messages);
    }
    
    @Test
    public void whenAddCreditApplications_GivenApplicationsListWithOnePersonalLoan_ThenMessagesAreEmpty() throws Exception {
    	List<CustomerMessageContent> messages = new ArrayList<CustomerMessageContent>();
    	UnicaApplication application = new UnicaApplication();
    	application.setCredit(false);
    	application.setProductNameVariable("unica.product.personalloan");
    	UnicaApplicationInfo applicationInfo = new UnicaApplicationInfo();
    	applicationInfo.setApplicationId("5345345543543");
    	applicationInfo.setApplStatusDesc("approved");
    	application.setUnicaApplicationInfo(applicationInfo);
    	List<UnicaApplication> applications = new ArrayList<UnicaApplication>();
    	applications.add(application);
    	
    	when(Utils.getUserSession(any())).thenReturn(userSession);
    	when(userSession.getUnicaApplications()).thenReturn(null);
    	when(UnicaUtils.getApplications(any(), any(), any(), any(), any(), any(), any())).thenReturn(applications);
    	when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
    	
    	Whitebox.invokeMethod(controller, "addCreditApplications", request, messages);
    	
    	assertTrue(messages.isEmpty());
    }
    
    @Test
    public void whenAddCreditApplications_GivenApplicationsListWithOneCreditNotFromMarketplace_ThenMessagesAreEmpty() throws Exception {
    	List<CustomerMessageContent> messages = new ArrayList<CustomerMessageContent>();
    	UnicaApplication application = new UnicaApplication();
    	application.setCredit(true);
    	application.setFromMarketplace(false);
    	UnicaApplicationInfo applicationInfo = new UnicaApplicationInfo();
    	applicationInfo.setApplicationId("5345345543543");
    	applicationInfo.setApplStatusDesc("approved");
    	application.setUnicaApplicationInfo(applicationInfo);
    	List<UnicaApplication> applications = new ArrayList<UnicaApplication>();
    	applications.add(application);
    	
    	when(Utils.getUserSession(any())).thenReturn(userSession);
    	when(userSession.getUnicaApplications()).thenReturn(null);
    	when(UnicaUtils.getApplications(any(), any(), any(), any(), any(), any(), any())).thenReturn(applications);
    	when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
    	
    	Whitebox.invokeMethod(controller, "addCreditApplications", request, messages);
    	
    	assertTrue(messages.isEmpty());
    }
    
    @Test
    public void whenAddCreditApplications_GivenApplicationsListWithOneCreditInProcessItem_ThenMessagesHasOneInProcess() throws Exception {
    	List<CustomerMessageContent> messages = new ArrayList<CustomerMessageContent>();
    	UnicaApplication application = new UnicaApplication();
    	application.setCredit(true);
    	application.setFromMarketplace(true);
    	UnicaApplicationInfo applicationInfo = new UnicaApplicationInfo();
    	applicationInfo.setApplicationId("5345345543543");
    	applicationInfo.setApplStatusDesc("inprocess");
    	application.setUnicaApplicationInfo(applicationInfo);
    	List<UnicaApplication> applications = new ArrayList<UnicaApplication>();
    	applications.add(application);
    	
    	when(Utils.getUserSession(any())).thenReturn(userSession);
    	when(userSession.getUnicaApplications()).thenReturn(null);
    	when(UnicaUtils.getApplications(any(), any(), any(), any(), any(), any(), any())).thenReturn(applications);
    	when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
    	
    	Whitebox.invokeMethod(controller, "addCreditApplications", request, messages);
    	
    	assertEquals(CREDIT_APP_INPROCESS_MSG, messages.get(0).getMessage());
    }
    
    @Test
    public void whenAddCreditApplications_GivenApplicationsListWithOneCreditTurnedDownItem_ThenMessagesHasOneTurnedDown() throws Exception {
    	List<CustomerMessageContent> messages = new ArrayList<CustomerMessageContent>();
    	UnicaApplication application = new UnicaApplication();
    	application.setCredit(true);
    	application.setFromMarketplace(true);
    	UnicaApplicationInfo applicationInfo = new UnicaApplicationInfo();
    	applicationInfo.setApplicationId("5345345543543");
    	applicationInfo.setApplStatusDesc("turneddown");
    	application.setUnicaApplicationInfo(applicationInfo);
    	List<UnicaApplication> applications = new ArrayList<UnicaApplication>();
    	applications.add(application);
    	
    	when(Utils.getUserSession(any())).thenReturn(userSession);
    	when(userSession.getUnicaApplications()).thenReturn(null);
    	when(UnicaUtils.getApplications(any(), any(), any(), any(), any(), any(), any())).thenReturn(applications);
    	when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
    	
    	Whitebox.invokeMethod(controller, "addCreditApplications", request, messages);
    	
    	assertEquals(CREDIT_APP_TURNEDDOWN_MSG, messages.get(0).getMessage());
    }
    
    @Test
    public void whenAddCreditApplications_GivenApplicationsListWithOneCreditBookedItem_ThenMessagesHasOneBookedDown() throws Exception {
    	List<CustomerMessageContent> messages = new ArrayList<CustomerMessageContent>();
    	UnicaApplication application = new UnicaApplication();
    	application.setCredit(true);
    	application.setFromMarketplace(true);
    	UnicaApplicationInfo applicationInfo = new UnicaApplicationInfo();
    	applicationInfo.setApplicationId("5345345543543");
    	applicationInfo.setApplStatusDesc("booked");
    	application.setUnicaApplicationInfo(applicationInfo);
    	List<UnicaApplication> applications = new ArrayList<UnicaApplication>();
    	applications.add(application);
    	
    	when(Utils.getUserSession(any())).thenReturn(userSession);
    	when(userSession.getUnicaApplications()).thenReturn(null);
    	when(UnicaUtils.getApplications(any(), any(), any(), any(), any(), any(), any())).thenReturn(applications);
    	when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
    	
    	Whitebox.invokeMethod(controller, "addCreditApplications", request, messages);
    	
    	assertEquals(CREDIT_APP_BOOKED_MSG, messages.get(0).getMessage());
    }
    
    @Test
    public void whenAddCreditApplications_GivenApplicationsListWithOneCreditApprovedItem_ThenMessagesHasOneApproved() throws Exception {
    	List<CustomerMessageContent> messages = new ArrayList<CustomerMessageContent>();
    	UnicaApplication application = new UnicaApplication();
    	application.setCredit(true);
    	application.setFromMarketplace(true);
    	UnicaApplicationInfo applicationInfo = new UnicaApplicationInfo();
    	applicationInfo.setApplicationId("5345345543543");
    	applicationInfo.setApplStatusDesc("approved");
    	application.setUnicaApplicationInfo(applicationInfo);
    	List<UnicaApplication> applications = new ArrayList<UnicaApplication>();
    	applications.add(application);
    	
    	when(Utils.getUserSession(any())).thenReturn(userSession);
    	when(userSession.getUnicaApplications()).thenReturn(null);
    	when(UnicaUtils.getApplications(any(), any(), any(), any(), any(), any(), any())).thenReturn(applications);
    	when(messageSource.getMessage("notification.center.unica.cc.approved", new Object[] { applicationInfo.getApplicationId() }, new Locale("en"))).thenReturn(CREDIT_APP_APPROVED_MSG);
    	when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
    	
    	Whitebox.invokeMethod(controller, "addCreditApplications", request, messages);
    	
    	assertEquals(CREDIT_APP_APPROVED_MSG, messages.get(0).getMessage());
    }
    
    @Test
    public void whenAddCreditApplications_GivenApplicationsListWithOneCreditUndefinedStatus_ThenMessagesAreEmpty() throws Exception {
    	List<CustomerMessageContent> messages = new ArrayList<CustomerMessageContent>();
    	UnicaApplication application = new UnicaApplication();
    	application.setCredit(true);
    	application.setFromMarketplace(true);
    	UnicaApplicationInfo applicationInfo = new UnicaApplicationInfo();
    	applicationInfo.setApplicationId("5345345543543");
    	applicationInfo.setApplStatusDesc("gdfsgdf");
    	application.setUnicaApplicationInfo(applicationInfo);
    	List<UnicaApplication> applications = new ArrayList<UnicaApplication>();
    	applications.add(application);
    	
    	when(Utils.getUserSession(any())).thenReturn(userSession);
    	when(userSession.getUnicaApplications()).thenReturn(null);
    	when(UnicaUtils.getApplications(any(), any(), any(), any(), any(), any(), any())).thenReturn(applications);
    	when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
    	
    	Whitebox.invokeMethod(controller, "addCreditApplications", request, messages);
    	
    	assertTrue(messages.isEmpty());
    }
}
