package evertec.cibp.web.form.mobile;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.devices.CustomerDevice;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.postinitialfunding.PIFAccountTypes;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.AccountsClasificationRequest;
import evertec.cibp.core.services.responses.AccountsClasificationResponse;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.MobileResponseStatus;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.facade.VaultAccountsFacade;
import evertec.cibp.web.form.mobile.bean.MobileGetBalanceBean;
import evertec.cibp.web.response.MobileDeviceProfileResponse;
import evertec.cibp.web.utils.MobileUtil;
import org.apache.commons.validator.GenericValidator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import evertec.cibp.core.models.customer.CustomerDevices;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.PasswordBean;
import evertec.cibp.web.form.dto.DeviceInfoDto;
import evertec.cibp.web.session.UserSession;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import evertec.cibp.core.services.responses.AccountsClasificationResponse;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BindException;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MobileUtil.class, GenericValidator.class, MobileResponseStatus.class})
public class MobileGetBalanceFormIT
{
	@Mock
	private UserSession userSession;

	@Mock
	private BankingSession bankingSession;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private MobileDeviceProfileResponse deviceProfileRespnse;

	@Mock
	private MobileGetBalanceBean mobileBalanceBean;

	@Mock
	private BindException error;

	@Mock
	private MobileGetBalanceBean submit;

	@Mock
	private Date date;

	@Mock
	private CustomerDevice customerDevice;


	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	@InjectMocks
	private MobileGetBalanceForm mobileGetBalanceForm;


	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mobileGetBalanceForm = PowerMockito.spy(new MobileGetBalanceForm());
		request = new MockHttpServletRequest();
		response =  new MockHttpServletResponse();
		PowerMockito.mockStatic(MobileUtil.class);
		PowerMockito.mockStatic(GenericValidator.class);
		PowerMockito.mockStatic(MobileResponseStatus.class);
		mobileGetBalanceForm.setCustomerService(customerService);
	}

	@Test
	public void whenOnSubmit_GivenCustomerServiceAndMobileDeviceProfileResponse_thenReturnShowFormNotNull() throws Exception
	{
		long tempo = 20211208; // cambiar fecha de ser necesaria
		List<TVFrontendAccount> accounts = new LinkedList<TVFrontendAccount>();

		when(GenericValidator.isBlankOrNull(mobileBalanceBean.getDeviceToken())).thenReturn(false);
		when(MobileUtil.getProfileByDeviceToken(request, mobileBalanceBean.getDeviceToken(), customerService)).thenReturn(deviceProfileRespnse);
		when(deviceProfileRespnse.getDevice()).thenReturn(customerDevice);
		when(deviceProfileRespnse.getStatus()).thenReturn(MobileResponseStatus.SUCCESS);
		when(customerDevice.getDeviceLastUsageDate()).thenReturn(date);
		when(date.getTime()).thenReturn(tempo);
		when(date.before(date)).thenReturn(true);
		when(deviceProfileRespnse.getProfile()).thenReturn(customerProfile);
		when(MobileUtil.getBankingSession(request, deviceProfileRespnse.getProfile(), customerService)).thenReturn(bankingSession);
		//
		//
		// when(MobileUtil.getAccounts(request, bankingSession, deviceProfileRespnse.getProfile(), customerService)).thenReturn(accounts);//
		when(deviceProfileRespnse.getProfile().getUsername()).thenReturn("username");
		assertNotNull(mobileGetBalanceForm.onSubmit(request, response, submit, error));
	}

}
