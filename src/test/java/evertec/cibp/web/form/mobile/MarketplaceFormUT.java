package evertec.cibp.web.form.mobile;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.owasp.esapi.Encoder;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.SingleCustomerProfileResponse;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerInteractionService;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.crypto.CryptoUtils;
import evertec.cibp.web.enums.OtpChallengeStatus;
import evertec.cibp.web.enums.UnicaAuthCode;
import evertec.cibp.web.form.mobile.MarketplaceForm.MarketeplaceAction;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.OtpChallengeUtils;
import evertec.cibp.web.utils.Utils;
import evertec.solicitudunica.response.UnicaAccessTokenResp;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Utils.class, Calendar.class, OtpChallengeUtils.class, UriUtils.class, MarketplaceForm.class, Logger.class})
public class MarketplaceFormUT extends AbstractModelAndViewTests {
	
	private static final String FALSE = "false";

	private static final String TRUE = "true";

	private static final String STATUS = "status";

	private static final String ERROR = "error";

	private static final String CODE = "code";

	private static final String VALIDATE_OTP_CODE = "validateOtpCode";

	private static final String RESEND_CODE = "resendCode";

	private static final String GENERATE_OTP_CODE = "generateOtpCode";
	
	private static final String UNICA_AUTH_CODE = "UNICA_AUTHORIZATION_CODE";
	
	private static final String UNICA_REFERRER = "UNICA_REFERRER";
	
	private static final String UNICA_ACCESS_TOKEN = "UNICA_ACCESS_TOKEN";
	
	private static final String UNICA_URL = "http://192.168.176.2:8080/un/appRetrieve.htm";

	private MockHttpServletRequest request;
	
	private MockHttpServletResponse response;
	
	private MockHttpSession httpSession;
	
	private Date todayDate;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock 
	private CustomerInteractionService customerInteractionService;
	
	@Mock
	private UserSession userSession;
	
	@Mock 
	private SingleCustomerProfileResponse singleCustomerProfileResponse;
	
	@Mock 
	private CustomerProfileService customerProfileService;
	
	@Mock 
	private Calendar calendar;
	
	@Mock
	private CryptoUtils cryptoUtils;
	
	@Mock
	private Encoder encoder;
	
	@Mock
	private Logger loggerMock;
	
	@InjectMocks
	private MarketplaceForm controller;

	@Before
	public void setUp() throws Exception {
		
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		httpSession = new MockHttpSession();
		todayDate = new Date();
		
		controller.setLimitProfileCreatedDays(30);
		controller.setLimitInfoProfileEditedDays(30);
		controller.setLimitOobEditedDays(30);
		controller.setAccountOldMonths(90);
		controller.setResendCodeMaxRetries(2);
		controller.setUnicaUrl(UNICA_URL);
		controller.setUnicaEncryptionKey("b3BlbnNzaC1rZXkt");
		controller.setUnicaEncryptionInitVector("BOAFFTi9beIrfQ34");
		
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(Calendar.class);
		PowerMockito.mockStatic(OtpChallengeUtils.class);
		PowerMockito.mockStatic(UriUtils.class);
		PowerMockito.mockStatic(Logger.class);
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getUsername()).thenReturn("");
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByUsername(any())).thenReturn(singleCustomerProfileResponse);
		when(singleCustomerProfileResponse.getCustomerProfile()).thenReturn(customerProfile);
		when(Logger.getLogger(any(Class.class))).thenReturn(loggerMock);
		
		doNothing().when(loggerMock).error(any());
		
	}
	
	@Test
	public void whenAcceptMarketplaceTerms_GivenInvalidProduct_ThenReturnSucessFalse() throws Exception {
		String product = "BLAH";
		
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		
		when(userSession.getCustomerService()).thenReturn(customerService);
		
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		
		request.addParameter("product", product);
		
		ModelAndView modelAndView = controller.acceptMarketplaceTerms(request, response);
		
		assertViewName(modelAndView, "acceptMarketplaceTerms");
		assertModelAttributeValue(modelAndView, "success", false);
		
	}
	
	@Test
	public void whenAcceptMarketplaceTerms_GivenNoProduct_ThenReturnSucessFalse() throws Exception {
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		
		when(userSession.getCustomerService()).thenReturn(customerService);
		
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		
		ModelAndView modelAndView = controller.acceptMarketplaceTerms(request, response);
		
		assertViewName(modelAndView, "acceptMarketplaceTerms");
		assertModelAttributeValue(modelAndView, "success", false);
		
	}
	
	@Test
	public void whenAcceptMarketplaceTerms_GivenValidProduct_ThenReturnSucessTrue() throws Exception {
		String product = "visa_cashreward";
		
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		
		when(userSession.getCustomerService()).thenReturn(customerService);
		
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		
		request.addParameter("product", product);
		
		ModelAndView modelAndView = controller.acceptMarketplaceTerms(request, response);
		
		assertViewName(modelAndView, "acceptMarketplaceTerms");
		assertModelAttributeValue(modelAndView, "success", true);
	}
	
	@Test
	public void whenGenerateOtpCode_GivenValidRequest_And_ResendCodeFalse_ThenReturnErrorFalse() throws Exception {
		String viewName = GENERATE_OTP_CODE;
		request.addParameter(RESEND_CODE, FALSE);
		
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.sendOtpChallengeEmail(any(), any(), any())).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getOtpSendRetries()).thenReturn(0);
		when(OtpChallengeUtils.hasReachedResendLimit(userSession, 2)).thenReturn(false);
		when(OtpChallengeUtils.sendChallenge(userSession)).thenReturn(true);
		
		ModelAndView modelAndView = controller.generateOtpCode(request, response);
		
		assertViewName(modelAndView, viewName);
		assertModelAttributeValue(modelAndView, ERROR, false);
		assertModelAttributeValue(modelAndView, STATUS, OtpChallengeStatus.OTP_SERVICE_SUCCESS.toString());
	}
	
	@Test
	public void whenGenerateOtpCode_GivenValidRequest_And_ResendCodeFalse_ThenReturnErrorTrue() throws Exception {
		String viewName = GENERATE_OTP_CODE;
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		
		when(userSession.getCustomerService()).thenReturn(customerService);
		
		when(customerService.sendOtpChallengeEmail(any(), any(), any())).thenReturn(Boolean.FALSE);
		
		when(userSession.getCustomerService()).thenReturn(customerService);
		
		request.addParameter(RESEND_CODE, FALSE);
		
		ModelAndView modelAndView = controller.generateOtpCode(request, response);
		
		assertViewName(modelAndView, viewName);
		assertModelAttributeValue(modelAndView, ERROR, true);
		assertModelAttributeValue(modelAndView, STATUS, OtpChallengeStatus.OTP_SERVICE_FAILED.toString());
	}
	
	@Test
	public void whenGenerateOtpCode_GivenValidRequest_And_ResendCodeTrue_ThenReturnErrorFalse() throws Exception {
		String viewName = GENERATE_OTP_CODE;
		request.addParameter(RESEND_CODE, TRUE);
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.sendOtpChallengeEmail(any(), any(), any())).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getOtpSendRetries()).thenReturn(0);
		when(OtpChallengeUtils.hasReachedResendLimit(userSession, 2)).thenReturn(false);
		when(OtpChallengeUtils.sendChallenge(userSession)).thenReturn(true);

		ModelAndView modelAndView = controller.generateOtpCode(request, response);
		
		assertViewName(modelAndView, viewName);
		assertModelAttributeValue(modelAndView, ERROR, false);
		assertModelAttributeValue(modelAndView, STATUS, OtpChallengeStatus.OTP_SERVICE_SUCCESS.toString());
	}
	
	@Test
	public void whenGenerateOtpCode_GivenValidRequest_And_ResendCodeFalse_And_ResendCodeMaxRetries_ThenReturnErrorFalse() throws Exception {
		String viewName = GENERATE_OTP_CODE;
		request.addParameter(RESEND_CODE, FALSE);
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.sendOtpChallengeEmail(any(), any(), any())).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getOtpSendRetries()).thenReturn(3);//Has + 1 to identify first try
		when(OtpChallengeUtils.hasReachedResendLimit(userSession, 2)).thenReturn(true);
		when(OtpChallengeUtils.sendChallenge(userSession)).thenReturn(true);
		
		ModelAndView modelAndView = controller.generateOtpCode(request, response);
		
		assertViewName(modelAndView, viewName);
		assertModelAttributeValue(modelAndView, ERROR, false);
		assertModelAttributeValue(modelAndView, STATUS, OtpChallengeStatus.RESEND_LIMIT_REACHED.toString());
	}
	
	@Test
	public void whenGenerateOtpCode_GivenValidRequest_And_ResendCodeTrue_And_ResendCodeMaxRetries_ThenReturnErrorFalse() throws Exception {
		String viewName = GENERATE_OTP_CODE;
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.sendOtpChallengeEmail(any(), any(), any())).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getOtpSendRetries()).thenReturn(3);;//Has + 1 to identify first try
		when(OtpChallengeUtils.hasReachedResendLimit(userSession, 2)).thenReturn(true);
		when(OtpChallengeUtils.sendChallenge(userSession)).thenReturn(true);
		
		request.addParameter(RESEND_CODE, TRUE);
		
		ModelAndView modelAndView = controller.generateOtpCode(request, response);
		
		assertViewName(modelAndView, viewName);
		assertModelAttributeValue(modelAndView, ERROR, false);
		assertModelAttributeValue(modelAndView, STATUS, OtpChallengeStatus.RESEND_LIMIT_EXCEEDED.toString());
	}
	
	@Test
	public void whenValidateOtpCode_GivenValidRequest_And_MissingInfo_ThenReturnErrorTrue() throws Exception {
		String viewName = VALIDATE_OTP_CODE;
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		
		when(userSession.getOtpChallengeCode()).thenReturn("123456");
		
		request.addParameter(CODE, "");
		
		ModelAndView modelAndView = controller.validateOtpCode(request, response);
		
		assertViewName(modelAndView, viewName);
		assertModelAttributeValue(modelAndView, ERROR, true);
		assertModelAttributeValue(modelAndView, STATUS, OtpChallengeStatus.MISSING_INFO.toString());
	}
	
	@Test
	public void whenValidateOtpCode_GivenValidRequest_And_HasCodeExpired_ThenReturnErrorFalse() throws Exception {
		String viewName = VALIDATE_OTP_CODE;
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		Date expiredDate = new Date();
        final int expiredTime = 5 * 1000;
        expiredDate.setTime(expiredDate.getTime() - expiredTime);
        
		controller.setResendCodeExpiredPeriod(5);
		controller.setValidateCodeMaxRetries(3);
		
		request.addParameter(CODE, "123456");
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getOtpChallengeCode()).thenReturn("123456");
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(userSession.getOtpLastChallengeDate()).thenReturn(expiredDate);
		when(userSession.getOtpValidateRetries()).thenReturn(1);
		when(OtpChallengeUtils.hasCodeExpired(userSession, 5)).thenReturn(true);
		
		ModelAndView modelAndView = controller.validateOtpCode(request, response);
		
		assertViewName(modelAndView, viewName);
		assertModelAttributeValue(modelAndView, ERROR, false);
		assertModelAttributeValue(modelAndView, STATUS, OtpChallengeStatus.CODE_EXPIRED.toString());
	}
	
	@Test
	public void whenValidateOtpCode_GivenValidRequest_And_HasCodeExpired_And_ValidateLimitReached_ThenReturnErroFalse() throws Exception {
		String viewName = VALIDATE_OTP_CODE;
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		Date expiredDate = new Date();
        final int expiredTime = 5 * 1000;
        expiredDate.setTime(expiredDate.getTime() - expiredTime);
        controller.setResendCodeExpiredPeriod(5);
        controller.setValidateCodeMaxRetries(3);
        request.addParameter(CODE, "123456");
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getOtpChallengeCode()).thenReturn("123456");
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(userSession.getOtpLastChallengeDate()).thenReturn(expiredDate);
		when(userSession.getOtpValidateRetries()).thenReturn(3);
		when(OtpChallengeUtils.hasCodeExpired(userSession, 5)).thenReturn(true);
		when(OtpChallengeUtils.hasReachedValidateLimit(userSession, 3)).thenReturn(true);
		
		ModelAndView modelAndView = controller.validateOtpCode(request, response);
		
		assertViewName(modelAndView, viewName);
		assertModelAttributeValue(modelAndView, ERROR, false);
		assertModelAttributeValue(modelAndView, STATUS, OtpChallengeStatus.VALIDATION_LIMIT.toString());
	}
	
	@Test
	public void whenValidateOtpCode_GivenValidRequest_CodeEqual_ThenReturnErrorFalse() throws Exception {
		String viewName = VALIDATE_OTP_CODE;
		
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		
		when(userSession.getCustomerService()).thenReturn(customerService);
		
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		
		when(userSession.getOtpChallengeCode()).thenReturn("123456");
		
		when(userSession.getOtpLastChallengeDate()).thenReturn(new Date());
		
		controller.setResendCodeExpiredPeriod(5);
		
		request.addParameter(CODE, "123456");
		
		ModelAndView modelAndView = controller.validateOtpCode(request, response);
		
		assertViewName(modelAndView, viewName);
		assertModelAttributeValue(modelAndView, ERROR, false);
		assertModelAttributeValue(modelAndView, STATUS, OtpChallengeStatus.VALIDATION_SUCCESS.toString());
	}
	
	@Test
	public void whenValidateOtpCode_GivenValidRequest_CodeNotEqual_ValidationLimitReached_ThenReturnErrorFalse() throws Exception {
		String viewName = VALIDATE_OTP_CODE;
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		controller.setValidateCodeMaxRetries(3);
		controller.setResendCodeExpiredPeriod(5);
		request.addParameter(CODE, "123457");
		
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getOtpChallengeCode()).thenReturn("123456");
		when(userSession.getOtpLastChallengeDate()).thenReturn(new Date());
		when(userSession.getOtpValidateRetries()).thenReturn(3);
		when(OtpChallengeUtils.hasCodeExpired(userSession, 5)).thenReturn(false);
		when(OtpChallengeUtils.hasReachedValidateLimit(userSession, 3)).thenReturn(true);
		
		ModelAndView modelAndView = controller.validateOtpCode(request, response);
		
		assertViewName(modelAndView, viewName);
		assertModelAttributeValue(modelAndView, ERROR, false);
		assertModelAttributeValue(modelAndView, STATUS, OtpChallengeStatus.VALIDATION_LIMIT.toString());
	}
	
	@Test
	public void whenValidateOtpCode_GivenValidRequest_CodeNotEqual_ThenReturnErrorFalse() throws Exception {
		String viewName = VALIDATE_OTP_CODE;
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		
		when(userSession.getOtpChallengeCode()).thenReturn("123456");
		
		when(userSession.getOtpLastChallengeDate()).thenReturn(new Date());
		
		controller.setResendCodeExpiredPeriod(5);
		
		when(userSession.getOtpValidateRetries()).thenReturn(0);
		
		controller.setValidateCodeMaxRetries(3);
		
		request.addParameter(CODE, "123457");
		
		ModelAndView modelAndView = controller.validateOtpCode(request, response);
		
		assertViewName(modelAndView, viewName);
		assertModelAttributeValue(modelAndView, ERROR, false);
		assertModelAttributeValue(modelAndView, STATUS, OtpChallengeStatus.VALIDATION_FAILED.toString());
	}
	
	@Test
	public void whenDetermineAuthenticationChallenge_GivenUserProfileNewlyCreated_ThenNoAuthenticationPresented() throws Exception {
	    Date date = new Date();
	    CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
	    controller.setLimitProfileCreatedDays(30);
		controller.setLimitInfoProfileEditedDays(30);
		controller.setLimitOobEditedDays(30);
		
		when(customerProfile.getCreationDate()).thenReturn(mock(Date.class));
		when(Calendar.getInstance()).thenReturn(calendar);
		when(calendar.getTime()).thenReturn(todayDate);
		when(customerProfile.getCreationDate()).thenReturn(date);
		when(userSession.getOobEnroll()).thenReturn(false);
		when(Utils.isAccountOldEnough(any(), any(), any())).thenReturn(true);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
	
		ModelAndView modelAndView = controller.determineAuthenticationChallenge(request, response);
		
		assertViewName(modelAndView, "determineAuthenticationChallenge");;
		assertModelAttributeValue(modelAndView, "action", MarketeplaceAction.NO_CHALLENGE_PRESENTED.toString());
	}
	
	@Test
	public void whenDetermineAuthenticationChallenge_GiveAccountNotOldEnough_ThenNoAuthenticationPresented() throws Exception {
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		Date creationDateProfile = sdf.parse(dateString);
	    CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
	    controller.setLimitProfileCreatedDays(30);
		controller.setLimitInfoProfileEditedDays(30);
		controller.setLimitOobEditedDays(30);
		
		when(customerProfile.getCreationDate()).thenReturn(mock(Date.class));
		when(customerProfile.getCreationDate()).thenReturn(creationDateProfile);
		when(Calendar.getInstance()).thenReturn(calendar);
		when(calendar.getTime()).thenReturn(todayDate);
		when(Utils.isAccountOldEnough(any(), any(), any())).thenReturn(false);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
	
		ModelAndView modelAndView = controller.determineAuthenticationChallenge(request, response);
		
		assertViewName(modelAndView, "determineAuthenticationChallenge");;
		assertModelAttributeValue(modelAndView, "action", MarketeplaceAction.NO_CHALLENGE_PRESENTED.toString());
	}
	
	@Test
	public void whenDetermineAuthenticationChallenge_GivenUserConditionEmailChallenge_ThenActionEmailChallenge() throws Exception {
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		Date creationDateProfile = sdf.parse(dateString);
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		
		when(customerProfile.getCreationDate()).thenReturn(mock(Date.class));
		when(customerProfile.getCreationDate()).thenReturn(creationDateProfile);
		when(Calendar.getInstance()).thenReturn(calendar);
		when(calendar.getTime()).thenReturn(todayDate);
		when(Utils.isAccountOldEnough(any(), any(), any())).thenReturn(true);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		when(userSession.getOobEnroll()).thenReturn(false);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		
		ModelAndView modelAndView = controller.determineAuthenticationChallenge(request, response);
		
		assertViewName(modelAndView, "determineAuthenticationChallenge");;
		assertModelAttributeValue(modelAndView, "action", MarketeplaceAction.EMAIL_CHALLENGE.toString());
	}
	
	@Test
	public void whenDetermineAuthenticationChallenge_GivenUserEditedOob_ThenActionNoChallenge() throws Exception {
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		Date creationDateProfile = sdf.parse(dateString);
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		
		when(customerProfile.getCreationDate()).thenReturn(mock(Date.class));
		when(customerProfile.getCreationDate()).thenReturn(creationDateProfile);
		when(Calendar.getInstance()).thenReturn(calendar);
		when(calendar.getTime()).thenReturn(todayDate);
		when(userSession.isOobAuthenticated()).thenReturn(true);
		when(userSession.getOobEnroll()).thenReturn(true);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(customerInteractionService.eventExists(any(), any(), any())).thenReturn(true);
		when(Utils.isAccountOldEnough(any(), any(), any())).thenReturn(true);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		
		
		ModelAndView modelAndView = controller.determineAuthenticationChallenge(request, response);
		
		assertViewName(modelAndView, "determineAuthenticationChallenge");;
		assertModelAttributeValue(modelAndView, "action", MarketeplaceAction.NO_CHALLENGE_PRESENTED.toString());
	}
	
	@Test
	public void whenDetermineAuthenticationChallenge_GivenOobAndEmailModified_ThenActionNoChallengePresented() throws Exception {
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		Date creationDateProfile = sdf.parse(dateString);
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		httpSession.setAttribute(UNICA_AUTH_CODE, new Object());
		request.setSession(httpSession);
		
		when(customerProfile.getCreationDate()).thenReturn(mock(Date.class));
		when(customerProfile.getCreationDate()).thenReturn(creationDateProfile);
		when(Calendar.getInstance()).thenReturn(calendar);
		when(calendar.getTime()).thenReturn(todayDate);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		when(userSession.getOobEnroll()).thenReturn(false);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(Utils.isAccountOldEnough(any(), any(), any())).thenReturn(true);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(controller.isEmailModified(userSession)).thenReturn(true);
		when(controller.isOobModified(userSession)).thenReturn(true);
		
		ModelAndView modelAndView = controller.determineAuthenticationChallenge(request, response);
		
		assertViewName(modelAndView, "determineAuthenticationChallenge");;
		assertModelAttributeValue(modelAndView, "action", MarketeplaceAction.NO_CHALLENGE_PRESENTED.toString());
	}
	
	@Test
	public void whenDetermineAuthenticationChallenge_GivenUserVerifyOobEdited_ThenActionOobChallengePresented() throws Exception {
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		Date creationDateProfile = sdf.parse(dateString);
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		
		when(customerProfile.getCreationDate()).thenReturn(mock(Date.class));
		when(customerProfile.getCreationDate()).thenReturn(creationDateProfile);
		when(Calendar.getInstance()).thenReturn(calendar);
		when(calendar.getTime()).thenReturn(todayDate);
		when(Utils.isAccountOldEnough(any(), any(), any())).thenReturn(true);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		when(userSession.getOobEnroll()).thenReturn(true);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(controller.isEmailModified(userSession)).thenReturn(false);
		when(controller.isOobModified(userSession)).thenReturn(false);
		
		ModelAndView modelAndView = controller.determineAuthenticationChallenge(request, response);
		
		assertViewName(modelAndView, "determineAuthenticationChallenge");;
		assertModelAttributeValue(modelAndView, "action", MarketeplaceAction.OOB_CHALLENGE.toString());
	}
	
	@Test
	public void whenDetermineAuthenticationChallenge_GivenUserVerifyOobEditedAndOobEdited_ThenActionEmailChallengePresented() throws Exception {
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		Date creationDateProfile = sdf.parse(dateString);
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		
		when(customerProfile.getCreationDate()).thenReturn(mock(Date.class));
		when(customerProfile.getCreationDate()).thenReturn(creationDateProfile);
		when(Calendar.getInstance()).thenReturn(calendar);
		when(calendar.getTime()).thenReturn(todayDate);
		when(Utils.isAccountOldEnough(any(), any(), any())).thenReturn(true);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		when(userSession.getOobEnroll()).thenReturn(true);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(controller.isEmailModified(userSession)).thenReturn(false);
		when(controller.isOobModified(userSession)).thenReturn(true);
		
		ModelAndView modelAndView = controller.determineAuthenticationChallenge(request, response);
		
		assertViewName(modelAndView, "determineAuthenticationChallenge");;
		assertModelAttributeValue(modelAndView, "action", MarketeplaceAction.EMAIL_CHALLENGE.toString());
	}
	
	@Test
	public void whenDetermineAuthenticationChallenge_GivenUnicaAuthCode_ThenNoAuthenticationPresented() throws Exception {
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		Date creationDateProfile = sdf.parse(dateString);
		httpSession.setAttribute(UNICA_AUTH_CODE, new Object());
		request.setSession(httpSession);

		when(customerProfile.getCreationDate()).thenReturn(creationDateProfile);
		when(Calendar.getInstance()).thenReturn(calendar);
		when(calendar.getTime()).thenReturn(todayDate);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		when(userSession.getOobEnroll()).thenReturn(true);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(controller.isEmailModified(userSession)).thenReturn(false);
		when(controller.isOobModified(userSession)).thenReturn(true);
		
		ModelAndView modelAndView = controller.determineAuthenticationChallenge(request, response);
		
		assertViewName(modelAndView, "determineAuthenticationChallenge");;
		assertModelAttributeValue(modelAndView, "action", MarketeplaceAction.NO_CHALLENGE_PRESENTED.toString());
	}
	
	@Test
	public void whenDetermineAuthenticationChallenge_GivenCustomerHasReachedResendLimit_ThenNoAuthenticationPresented() throws Exception {
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		Date creationDateProfile = sdf.parse(dateString);

		when(customerProfile.getCreationDate()).thenReturn(creationDateProfile);
		when(Calendar.getInstance()).thenReturn(calendar);
		when(calendar.getTime()).thenReturn(todayDate);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(controller.isEmailModified(userSession)).thenReturn(false);
		when(OtpChallengeUtils.hasReachedResendLimit(userSession, 2)).thenReturn(true);
		
		ModelAndView modelAndView = controller.determineAuthenticationChallenge(request, response);
		
		assertViewName(modelAndView, "determineAuthenticationChallenge");;
		assertModelAttributeValue(modelAndView, "action", MarketeplaceAction.NO_CHALLENGE_PRESENTED.toString());
	}
	
	@Test
	public void whenDetermineAuthenticationChallenge_GivenIllegalArgumentExceptionThrown_ThenActionErrorPresented() throws Exception {
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		Date creationDateProfile = sdf.parse(dateString);
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		
		when(customerProfile.getCreationDate()).thenReturn(mock(Date.class));
		when(customerProfile.getCreationDate()).thenReturn(creationDateProfile);
		when(Calendar.getInstance()).thenReturn(calendar);
		when(calendar.getTime()).thenReturn(todayDate);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		when(userSession.getOobEnroll()).thenReturn(true);
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(controller.isEmailModified(userSession)).thenReturn(false);
		when(controller.isOobModified(userSession)).thenReturn(true);
		when(Utils.getUserSession(any())).thenThrow(new IllegalArgumentException());
		
		ModelAndView modelAndView = controller.determineAuthenticationChallenge(request, response);
		
		assertViewName(modelAndView, "determineAuthenticationChallenge");
		assertModelAttributeValue(modelAndView, "action", MarketeplaceAction.ERROR.toString());
	}
	
	@Test
	public void whenGetUnicaUrl_GivenNullReferrer_ThenReturnModelWithNoUrl() throws Exception {
		request.setSession(httpSession);
		
		ModelAndView modelAndView = controller.getUnicaUrl(request, response);
		
		assertViewName(modelAndView, "getUnicaUrl");
		assertFalse(modelAndView.getModelMap().containsKey("url"));
	}
	
	@Test
	public void whenGetUnicaUrl_GivenNullToken_ThenReturnModelWithNoUrl() throws Exception {
		httpSession.setAttribute(UNICA_REFERRER, "jb_mastercard_eleva");
		request.setSession(httpSession);
		
		ModelAndView modelAndView = controller.getUnicaUrl(request, response);
		
		assertViewName(modelAndView, "getUnicaUrl");
		assertFalse(modelAndView.getModelMap().containsKey("url"));
	}
	
	@Test
	public void whenGetUnicaUrl_GivenExpiredAccessTokenAndNullNewToken_ThenReturnModelWithNoUrl() throws Exception {
		
		UnicaAccessTokenResp accessToken = new UnicaAccessTokenResp("kfjshf", "fsdfds", "0");
		httpSession.setAttribute(UNICA_ACCESS_TOKEN, accessToken);
		httpSession.setAttribute(UNICA_REFERRER, "jb_mastercard_eleva");
		request.setSession(httpSession);
		
		ModelAndView modelAndView = controller.getUnicaUrl(request, response);
		
		assertViewName(modelAndView, "getUnicaUrl");
		assertFalse(modelAndView.getModelMap().containsKey("url"));
	}
	
	@Test
	public void whenGetUnicaUrl_GivenAuthCodeNull_ThenReturnModelWithNoUrl() throws Exception {
		
		UnicaAccessTokenResp accessToken = new UnicaAccessTokenResp("kfjshf", "fsdfds", "300");
		httpSession.setAttribute(UNICA_ACCESS_TOKEN, accessToken);
		httpSession.setAttribute(UNICA_REFERRER, "jb_mastercard_eleva");
		request.setSession(httpSession);
		
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		
		ModelAndView modelAndView = controller.getUnicaUrl(request, response);
		
		assertViewName(modelAndView, "getUnicaUrl");
		assertFalse(modelAndView.getModelMap().containsKey("url"));
	}
	
	@Test
	public void whenGetUnicaUrl_GivenOobAuthenticatedInParamaterAndSession_ThenAuthCodeEquals103() throws Exception {
		UnicaAccessTokenResp accessToken = new UnicaAccessTokenResp("kfjshf", "fsdfds", "300");
		httpSession.setAttribute(UNICA_ACCESS_TOKEN, accessToken);
		httpSession.setAttribute(UNICA_REFERRER, "jb_mastercard_eleva");
		request.setSession(httpSession);
		request.addParameter("isOobAuthenticated", "true");
		userSession.setCustomerService(customerService);
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		when(userSession.isOobAuthenticated()).thenReturn(true);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getPermId()).thenReturn(BigInteger.valueOf(534534534L));
		when(customerProfile.getUsername()).thenReturn("danerys");
		when(customerService.tokenGenerate(any(), any(), any())).thenReturn("fsdkljgnkfds");
		PowerMockito.whenNew(CryptoUtils.class).withNoArguments().thenReturn(cryptoUtils);
		when(cryptoUtils.encrypt(any(), any(), any())).thenReturn("lfkjadhsfkljadshflkjadhsfs");
		when(UriUtils.encodeQueryParam(any(), any())).thenReturn("lkfjadshflkjsadhfklj");
		
		ModelAndView modelAndView = controller.getUnicaUrl(request, response);
		
		assertViewName(modelAndView, "getUnicaUrl");
		assertModelAttributeAvailable(modelAndView, "url");
		assertTrue(((String) modelAndView.getModelMap().get("url")).contains("code"));
		assertEquals("103", UnicaAuthCode.valueOf(httpSession.getAttribute(UNICA_AUTH_CODE).toString()).getCode().toString());
	}
	
	@Test
	public void whenGetUnicaUrl_GivenOobAuthenticatedInParamaterAndNotInSession_ThenReturnModelWithNoUrl() throws Exception {
		UnicaAccessTokenResp accessToken = new UnicaAccessTokenResp("kfjshf", "fsdfds", "300");
		httpSession.setAttribute(UNICA_ACCESS_TOKEN, accessToken);
		httpSession.setAttribute(UNICA_REFERRER, "jb_mastercard_eleva");
		request.setSession(httpSession);
		request.addParameter("isOobAuthenticated", "true");
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getPermId()).thenReturn(BigInteger.valueOf(534534534L));
		PowerMockito.whenNew(CryptoUtils.class).withNoArguments().thenReturn(cryptoUtils);
		when(cryptoUtils.encrypt(any(), any(), any())).thenReturn("lfkjadhsfkljadshflkjadhsfs");
		
		ModelAndView modelAndView = controller.getUnicaUrl(request, response);
		
		assertViewName(modelAndView, "getUnicaUrl");
		assertFalse(modelAndView.getModelMap().containsKey("url"));
	}

	@Test
	public void whenGetUnicaUrl_GivenAuthCodeAndNoSsoToken_ThenReturnModelWithNoUrl() throws Exception {
		UnicaAccessTokenResp accessToken = new UnicaAccessTokenResp("kfjshf", "fsdfds", "300");
		httpSession.setAttribute(UNICA_ACCESS_TOKEN, accessToken);
		httpSession.setAttribute(UNICA_REFERRER, "jb_mastercard_eleva");
		httpSession.setAttribute(UNICA_AUTH_CODE, UnicaAuthCode.EMAIL_AUTHENTICATION_SUCCESS);
		request.setSession(httpSession);
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		when(userSession.isOobAuthenticated()).thenReturn(true);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getPermId()).thenReturn(BigInteger.valueOf(534534534L));
		PowerMockito.whenNew(CryptoUtils.class).withNoArguments().thenReturn(cryptoUtils);
		when(cryptoUtils.encrypt(any(), any(), any())).thenReturn("lfkjadhsfkljadshflkjadhsfs");
		when(UriUtils.encodeQueryParam(any(), any())).thenReturn("lkfjadshflkjsadhfklj");
		
		ModelAndView modelAndView = controller.getUnicaUrl(request, response);
		
		assertViewName(modelAndView, "getUnicaUrl");
		assertFalse(modelAndView.getModelMap().containsKey("url"));
	}
	
	@Test
	public void whenGetUnicaUrl_GivenAuthCodeAndSsoToken_ThenReturnModelWithUrl() throws Exception {
		UnicaAccessTokenResp accessToken = new UnicaAccessTokenResp("kfjshf", "fsdfds", "300");
		httpSession.setAttribute(UNICA_ACCESS_TOKEN, accessToken);
		httpSession.setAttribute(UNICA_REFERRER, "jb_mastercard_eleva");
		httpSession.setAttribute(UNICA_AUTH_CODE, UnicaAuthCode.EMAIL_AUTHENTICATION_SUCCESS);
		request.setSession(httpSession);
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		when(userSession.isOobAuthenticated()).thenReturn(true);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getPermId()).thenReturn(BigInteger.valueOf(534534534L));
		when(customerService.tokenGenerate(any(), any(), any())).thenReturn("fsdkljgnkfds");
		PowerMockito.whenNew(CryptoUtils.class).withNoArguments().thenReturn(cryptoUtils);
		when(cryptoUtils.encrypt(any(), any(), any())).thenReturn("lfkjadhsfkljadshflkjadhsfs");
		when(UriUtils.encodeQueryParam(any(), any())).thenReturn("lkfjadshflkjsadhfklj");
		
		ModelAndView modelAndView = controller.getUnicaUrl(request, response);
		
		assertViewName(modelAndView, "getUnicaUrl");
		assertModelAttributeAvailable(modelAndView, "url");
		assertTrue(((String) modelAndView.getModelMap().get("url")).contains("code"));
	}
	
	@Test
	public void whenGetUnicaUrl_GivenEncodeForURLThrowsEncodingException_ThenReturnModelWithNoUrl() throws Exception {
		UnicaAccessTokenResp accessToken = new UnicaAccessTokenResp("kfjshf", "fsdfds", "300");
		httpSession.setAttribute(UNICA_ACCESS_TOKEN, accessToken);
		httpSession.setAttribute(UNICA_REFERRER, "jb_mastercard_eleva");
		httpSession.setAttribute(UNICA_AUTH_CODE, UnicaAuthCode.EMAIL_AUTHENTICATION_SUCCESS);
		request.setSession(httpSession);
		
		when(Utils.getUserSession(any())).thenReturn(userSession);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		when(userSession.isOobAuthenticated()).thenReturn(true);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getPermId()).thenReturn(BigInteger.valueOf(534534534L));
		when(customerService.tokenGenerate(any(), any(), any())).thenReturn("fsdkljgnkfds");
		PowerMockito.whenNew(CryptoUtils.class).withNoArguments().thenReturn(cryptoUtils);
		when(cryptoUtils.encrypt(any(), any(), any())).thenReturn("lfkjadhsfkljadshflkjadhsfs");
		when(UriUtils.encodeQueryParam(any(), any())).thenThrow(UnsupportedEncodingException.class);
		
		ModelAndView modelAndView = controller.getUnicaUrl(request, response);
		
		assertViewName(modelAndView, "getUnicaUrl");
		assertFalse(modelAndView.getModelMap().containsKey("url"));
	}
	
	@Test
	public void whenDetermineAuthenticationChallenge_GivenUserIsOobAuthenticatedAndisOobNoModified_ThenActionNoActionChallenge() throws Exception {

		when(customerProfile.getCreationDate()).thenReturn(mock(Date.class));
		
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		
		Date creationDateProfile = sdf.parse(dateString);
		when(customerProfile.getCreationDate()).thenReturn(creationDateProfile);
		
		when(Calendar.getInstance()).thenReturn(calendar);
		when(calendar.getTime()).thenReturn(todayDate);

		when(OtpChallengeUtils.hasReachedResendLimit(userSession, 2)).thenReturn(false);
		when(Utils.isAccountOldEnough(any(), any(), any())).thenReturn(true);
		
		when(userSession.getOobEnroll()).thenReturn(false);
		controller.setLimitProfileCreatedDays(30);
		controller.setLimitInfoProfileEditedDays(30);
		controller.setLimitOobEditedDays(30);
		
		when(userSession.isOobAuthenticated()).thenReturn(true);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(controller.isOobModified(userSession)).thenReturn(false);
		
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		
		ModelAndView modelAndView = controller.determineAuthenticationChallenge(request, response);
		
		assertViewName(modelAndView, "determineAuthenticationChallenge");;
		assertModelAttributeValue(modelAndView, "action", MarketeplaceAction.NO_CHALLENGE_PRESENTED.toString());
	}
	
	@Test
	public void whenDetermineAuthenticationChallenge_GivenUserisEmailModified_ThenNoActionChallenge() throws Exception {

		when(customerProfile.getCreationDate()).thenReturn(mock(Date.class));
		
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "2014-02-11";
		
		Date creationDateProfile = sdf.parse(dateString);
		when(customerProfile.getCreationDate()).thenReturn(creationDateProfile);
		
		when(Calendar.getInstance()).thenReturn(calendar);
		when(calendar.getTime()).thenReturn(todayDate);
		
		when(OtpChallengeUtils.hasReachedResendLimit(userSession, 2)).thenReturn(false);
		when(Utils.isAccountOldEnough(any(), any(), any())).thenReturn(true);
		
		when(userSession.getOobEnroll()).thenReturn(false);
		
		controller.setLimitProfileCreatedDays(30);
		controller.setLimitInfoProfileEditedDays(30);
		controller.setLimitOobEditedDays(30);
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(controller.isEmailModified(userSession)).thenReturn(true);
		when(controller.isOobModified(userSession)).thenReturn(false);
		
		CustomerInteractionEvent customerInteractionEvent = new CustomerInteractionEvent();
		when(Utils.generateCustomerInteractionEvent(any())).thenReturn(customerInteractionEvent);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		
		ModelAndView modelAndView = controller.determineAuthenticationChallenge(request, response);
		
		assertViewName(modelAndView, "determineAuthenticationChallenge");;
		assertModelAttributeValue(modelAndView, "action", MarketeplaceAction.NO_CHALLENGE_PRESENTED.toString());
	}
		
}
