package evertec.cibp.web.form.mobile;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import evertec.cibp.web.constants.RequestParams;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.google.common.collect.Iterables;

import evertec.cibp.core.dto.PushTokensDto;
import evertec.cibp.core.enums.PushTokenStatus;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.PushTokensRequest;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.MobileResponseStatus;
import evertec.cibp.web.facade.PushTokensFacade;
import evertec.cibp.web.request.PushTokensMobileRequest;
import evertec.cibp.web.response.PushTokensResponse;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, WebUtils.class, PushTokensFacade.class, Iterables.class})
public class MobilePushTokenControllerUT extends AbstractModelAndViewTests {

	
	private static final String PROFILE_ID = "ff8081811ac1d21c011ac2a4a5a02f2d";
	private static final String VIEW_NAME = "pushTokenResponse";
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock 
	private CustomerProfileService customerProfileService;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private PushTokensMobileRequest statusRequest;
	
	@Mock 
	private UserSession userSession;
	
	@Mock
	private PushTokensResponse pushTokensResponse;
	
	@InjectMocks
	private MobilePushTokenController controller;
	
	@Mock
	private PushTokensFacade facade;
	

	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(PushTokensFacade.class);
		PowerMockito.mockStatic(Iterables.class);
				
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		
		request.addParameter("pushToken", RandomStringUtils.randomAlphabetic(155));
		request.addParameter("deviceType", "Android");
		request.addParameter("deviceModel", "S10+");
		request.addParameter("deviceName", "Armando's s10+");
		
	}

	
	@Test
	public void whenResponseisSuccess_GivenOKResponse_ThenPopulatePushTokenRequestAndReturnView() throws Exception {
			
		PushTokensDto mockDto = new PushTokensDto();
		mockDto.setStatus(PushTokenStatus.ACTIVE.toString());
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(PushTokensFacade.getInstance()).thenReturn(facade);
		when(facade.getTokens(any(PushTokensRequest.class), any(UserSession.class))).thenReturn(pushTokensResponse);
		when(pushTokensResponse.hasData()).thenReturn(true);
		when(facade.putTokens(any(), any(UserSession.class))).thenReturn(pushTokensResponse);
		when(userSession.getCustomerProfile().getProfileId()).thenReturn(PROFILE_ID);
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(Iterables.getFirst(any(), any())).thenReturn(mockDto);
	

		
		ModelAndView modelAndView = controller.mobileCheckPushTokenStatus(request, response);
		
		assertViewName(modelAndView, VIEW_NAME);
		assertModelAttributeValue(modelAndView, "status", MobileResponseStatus.SUCCESS);
		
	}
	
	@Test
	public void whenResponseisError_GivenErrorResponse_ThenReturnError() throws Exception {
		PushTokensResponse resp = new PushTokensResponse();
		PushTokensDto mockDto = new PushTokensDto();
		
		resp.setError("randomError");
		mockDto.setStatus(PushTokenStatus.ACTIVE.toString());
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(PushTokensFacade.getInstance()).thenReturn(facade);
		when(facade.getTokens(any(PushTokensRequest.class), any(UserSession.class))).thenReturn(pushTokensResponse);
		when(pushTokensResponse.hasData()).thenReturn(false);
		when(facade.putTokens(any(), any(UserSession.class))).thenReturn(resp);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(Iterables.getFirst(any(), any())).thenReturn(mockDto);
		when(userSession.getCustomerProfile().getProfileId()).thenReturn(PROFILE_ID);
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		
		ModelAndView modelAndView = controller.mobileCheckPushTokenStatus(request, response);
		
		assertViewName(modelAndView, VIEW_NAME);
		assertModelAttributeValue(modelAndView, "status", MobileResponseStatus.ERROR);
		
		
	}
	
	
	@Test
	public void whenTokenIsNull_GivenInvalidToken_ThenErrorStatus() throws Exception {
		
		request.setParameter("pushToken", StringUtils.EMPTY);
		PushTokensResponse resp = new PushTokensResponse();
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(PushTokensFacade.getInstance()).thenReturn(facade);
		when(facade.getTokens(any(PushTokensRequest.class), any(UserSession.class))).thenReturn(pushTokensResponse);
		when(pushTokensResponse.hasData()).thenReturn(false);
	    when(facade.putTokens(any(), any(UserSession.class))).thenReturn(resp);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile().getProfileId()).thenReturn(PROFILE_ID);
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		
		ModelAndView modelAndView = controller.mobileCheckPushTokenStatus(request, response);
		
		assertViewName(modelAndView, VIEW_NAME);
		assertModelAttributeValue(modelAndView, "status", MobileResponseStatus.ERROR);
		
	}
	
	@Test
	public void whenTokenIsInvalid_GivenInvalidStatus_ThenReturnDisableStatus() throws Exception {
		
		PushTokensDto mockDto = new PushTokensDto();
		mockDto.setStatus(PushTokenStatus.INVALID.toString());
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(PushTokensFacade.getInstance()).thenReturn(facade);
		when(facade.getTokens(any(PushTokensRequest.class), any(UserSession.class))).thenReturn(pushTokensResponse);
		when(pushTokensResponse.hasData()).thenReturn(true);
		when(facade.putTokens(any(), any(UserSession.class))).thenReturn(pushTokensResponse);
		when(Iterables.getFirst(any(), any())).thenReturn(mockDto);
		when(userSession.getCustomerProfile().getProfileId()).thenReturn(PROFILE_ID);
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		
		ModelAndView modelAndView = controller.mobileCheckPushTokenStatus(request, response);
		
		assertViewName(modelAndView, VIEW_NAME);
		assertModelAttributeValue(modelAndView, "status", MobileResponseStatus.DISABLE);
		
	}
	
	@Test
	public void whenResponsehasNoDatal_GivenObjectWithoutData_ThenReturnFalse() throws Exception {
		Boolean testResult = Whitebox.invokeMethod(controller, "responseHasData", new PushTokensResponse());
		assertFalse(testResult);
	}
	
	@Test
	public void whenResponseisNull_GivenNullObject_ThenReturnFalse() throws Exception {
		PushTokensResponse resp = null;
		Boolean testResult = Whitebox.invokeMethod(controller, "responseHasData", resp);
		assertFalse(testResult);
	}
	
	@Test
	public void whenResponseHasData_GivenObjectWithData_ThenReturnTrue() throws Exception {
		PushTokensResponse resp = new PushTokensResponse();
		List<PushTokensDto> list = new ArrayList<PushTokensDto>();
		list.add(new PushTokensDto());
		resp.setData(list);
		
		Boolean testResult = Whitebox.invokeMethod(controller, "responseHasData", resp);
		assertTrue(testResult);
	}
	
	@Test
	public void whenShouldLogInitialToken_GivenFalseIsInsertAndSuccessStatus_ThenReturnFalse() throws Exception {
		Boolean testResult = Whitebox.invokeMethod(controller, "shouldLogInitialPushEvent", false, MobileResponseStatus.SUCCESS);
		assertFalse(testResult);
	}
	
	@Test
	public void whenShouldLogInitialToken_GivenTrueIsInsertAndSuccessStatus_ThenReturnTrue() throws Exception {
		Boolean testResult = Whitebox.invokeMethod(controller, "shouldLogInitialPushEvent", true, MobileResponseStatus.SUCCESS);
		assertTrue(testResult);
	}
	
	@Test
	public void whenShouldLogInitialToken_GivenErrorStatusResponseAndTrueIsInsert_ThenReturnfalse() throws Exception {
		Boolean testResult = Whitebox.invokeMethod(controller, "shouldLogInitialPushEvent", true, MobileResponseStatus.ERROR);
		assertFalse(testResult);
	}
	
	@Test
	public void whenShouldLogInitialToken_GivenFalseStatusResponsAndFalseIsInsert_ThenReturnTrue() throws Exception {
		Boolean testResult = Whitebox.invokeMethod(controller, "shouldLogInitialPushEvent", false, MobileResponseStatus.ERROR);
		assertFalse(testResult);
	}
	
	@Test
	public void whenIsCreateDeafultToken_GivenIsFalseInsert_ThenReturnFalse() throws Exception {
		PushTokensRequest req = new PushTokensRequest();
		Boolean testResult = Whitebox.invokeMethod(controller, "createDefaultToken", PROFILE_ID, statusRequest, req);
		assertFalse(testResult);
	}
	
	@Test
	public void whenIsCreateDefaultToken_GivenIsTrueInsertAndHasPermissions_ThenReturnTrue() throws Exception {
		
		PushTokensRequest req = new PushTokensRequest();
		
		PushTokensMobileRequest statusReq = new PushTokensMobileRequest();
		statusReq.setHasPermissions(true);
		Boolean testResult = Whitebox.invokeMethod(controller, "createDefaultToken", PROFILE_ID, statusReq, req);
		assertTrue(testResult);
	}
	
	@Test
	public void whenIsCreateDefaultToken_GivenIsTrueInsertAndHasNoPermissions_ThenReturnFalse() throws Exception {
		
		PushTokensRequest req = new PushTokensRequest();
		statusRequest.setHasPermissions(false);
		Boolean testResult = Whitebox.invokeMethod(controller, "createDefaultToken", PROFILE_ID, statusRequest, req);
		assertFalse(testResult);
	}

	@Test
	public void whenMobileCheckPushTokenStatus_GivenInvalidToken_ThenInvalidateOldToken() throws Exception {
		PushTokensDto mockDto = new PushTokensDto();
		mockDto.setStatus(PushTokenStatus.ACTIVE.toString());
		mockDto.setCreationDate(new Date());
		mockDto.setLastSignonDate(new Date());
		request.addParameter(RequestParams.INVALID_PUSH_TOKEN, "lfjahsdflkadshflkjds");

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(PushTokensFacade.getInstance()).thenReturn(facade);
		when(facade.getTokens(any(PushTokensRequest.class), any(UserSession.class))).thenReturn(pushTokensResponse);
		when(pushTokensResponse.hasData()).thenReturn(true);
		when(facade.putTokens(any(), any(UserSession.class))).thenReturn(pushTokensResponse);
		when(userSession.getCustomerProfile().getProfileId()).thenReturn(PROFILE_ID);
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(Iterables.getFirst(any(), any())).thenReturn(mockDto);

		ModelAndView modelAndView = controller.mobileCheckPushTokenStatus(request, response);

		assertViewName(modelAndView, VIEW_NAME);
		assertModelAttributeValue(modelAndView, "status", MobileResponseStatus.SUCCESS);
	}


}
