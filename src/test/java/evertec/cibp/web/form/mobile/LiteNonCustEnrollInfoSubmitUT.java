package evertec.cibp.web.form.mobile;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.customer.MultipleCustomerProfileResponse;
import evertec.cibp.core.models.enrollment.EnrollmentLiteStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.mobilecash.bean.LiteEnrollmentVerificationBean;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, WebUtils.class, ApiCallerFactory.class})
public class LiteNonCustEnrollInfoSubmitUT extends AbstractModelAndViewTests {

    @Mock
    private CustomerServices customerServices;

    @Mock
    private CustomerProfileService customerProfileService;

    @Mock
    private LiteEnrollmentVerificationBean liteEnrollmentVerificationBean;

    @Mock
    private MultipleCustomerProfileResponse customerProfiles;

    @Mock
    private HttpServletRequest request;

    @Mock
    private ApiCaller apiCaller;

    @InjectMocks
    @Spy
    private final LiteNonCustEnrollInfoSubmit service = new LiteNonCustEnrollInfoSubmit();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ApiCallerFactory.class);
        when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

        service.setCustomerServices(customerServices);
        service.setEasyOfacSuccess(true);
        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(WebUtils.class);
        request = PowerMockito.mock(HttpServletRequest.class);
    }

    @Test
    public void whenNonCustomerValidation_GivenFlagEasyOfacFalse_ThenReturnStatusDisplayTerms() throws Exception {
        when(liteEnrollmentVerificationBean.getEmail()).thenReturn("karolg@losintocables.com");
        when(liteEnrollmentVerificationBean.getPhoneNumber()).thenReturn("5872222222");
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        when(customerProfileService.getCustomerProfileByFilter(any())).thenReturn(customerProfiles);
        when(customerServices.isPhoneNumberInBlacklist(any())).thenReturn(null);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EASY_OFAC)).thenReturn(false);
        when(customerProfiles.getCustomerProfiles()).thenReturn(new ArrayList<>());
        when(liteEnrollmentVerificationBean.getFirstName()).thenReturn("Karol");
        when(liteEnrollmentVerificationBean.getLastName()).thenReturn("G");
        when(liteEnrollmentVerificationBean.getCountry()).thenReturn("Colombia");
        when(customerProfileService.getNextEasyOfacId()).thenReturn("1324658");
        when(liteEnrollmentVerificationBean.getPhoneProvider()).thenReturn("AT&T");
        when(liteEnrollmentVerificationBean.getDeviceId()).thenReturn("4as6sd5456adsf");
        EnrollmentLiteStatus result = Whitebox.invokeMethod(service, "nonCustomerValidation",
                request, liteEnrollmentVerificationBean);
        assertEquals(result, EnrollmentLiteStatus.DISPLAY_TERMS_AND_CONDITIONS);
    }
}