package evertec.cibp.web.form.mobile;

import evertec.cibp.core.services.responses.BankingSessionResponse;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.VaultAccountsFacade;
import evertec.cibp.web.utils.Utils;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.util.WebUtils;

/**
 * UT for SessionInitializationController
 * 
 * @author alizano
 * 
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ StaticMessageSource.class, WebUtils.class, Utils.class, BankingSessionResponse.class, VaultAccountsFacade.class, CustomerUtils.class})
public class SessionInitializationControllerFlowUT extends AbstractModelAndViewTests {
//
//    public static final String BROWSER_CHROME = "Chrome";
//    private MockHttpServletRequest request;
//    private HttpServletResponse response;
//
//    public static String UNODOSTRES = "123456789";
//    private static String IDA = "IDA";
//    public static BigInteger PREMIAPERMID = BigInteger.valueOf(Long.parseLong("549126001001"));
//
//    @Mock
//    private DeviceRequest deviceRequest;
//    
//    @Mock
//    private CustomerServices customerService;
//
//    @Mock
//    private CustomerProfile customerProfile;
//
//    @Mock
//    private UserSession userSession;
//
//    @Mock
//    private BankingSession bankingSession;
//
//    @Mock
//    private CustomerEntitlement customerEntitlementAlerts;
//
//    @InjectMocks
//    private SessionInitializationController controller;
//
//    @Mock
//    protected CustomerFacade customerFacade;
//
//    @Mock
//    private AlertsEntitlement entitlement;
//
//    @Mock
//    private Set<AlertType> customeralerts;
//
//    @Mock
//    private CustomerProfileService customerProfileService;
//
//    @Mock
//    private EnrollmentServiceModule enrollmentServiceModule;
//
//    @Mock
//    private VaultAccountsFacade vaultAccountsFacade;
//
//    @Mock
//    private BankingSessionResponse bankingSessionResponse;
//
//    @Before
//    public void setUp() throws Exception {
//
//        PowerMockito.mockStatic(StaticMessageSource.class);
//        PowerMockito.mockStatic(EnrollmentServiceModule.class);
//        PowerMockito.mockStatic(WebUtils.class);
//        PowerMockito.mockStatic(Calendar.class);
//        PowerMockito.mockStatic(Utils.class);
//
//        ProfileType profileType = new ProfileType();
//        profileType.setId(1234);
//
//        request = new MockHttpServletRequest();
//        response = new MockHttpServletResponse();
//        customerEntitlementAlerts = new CustomerEntitlement(EntitlementType.ALERTS, EntitlementStatus.ENABLED);
//
//        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
//
//        when(EnrollmentServiceModule.getInstance()).thenReturn(enrollmentServiceModule);
//        when(userSession.getCustomerService()).thenReturn(customerService);
//        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
//        when(customerProfile.getEnrollmentAccountNumber()).thenReturn(UNODOSTRES);
//        when(customerProfile.getEnrollmentAccountSubtype()).thenReturn(IDA);
//        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
//        when(customerProfile.getProfileType()).thenReturn(profileType);
//        controller.setMaxStatements(Integer.MAX_VALUE);
//
//        when(customerProfile.getEntitlementByType(EntitlementType.BANKING))
//                .thenReturn(new CustomerEntitlement(EntitlementType.BANKING, EntitlementStatus.ENABLED));
//        when(userSession.getBankingSession()).thenReturn(bankingSession);
//        request.addHeader("user-agent",
//                "Mozilla/5.0 (Linux; U; Android 2.3.1; en-us; MID Build/GINGERBREAD) AppleWebKit/533.1 (KHTML, like Gecko) "
//                        + "Version/4.0 Mobile Safari/533.1. Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) "
//                        + "Chrome/44.0.2403.157 Safari/537.36.");
//        when(customerService.completeLoginProcess(any(CompleteLoginProcessRequest.class)))
//                .thenReturn(new ModifyCustomerProfileResponse());
//        when(customerProfile.getBirthDate()).thenReturn(new Date());
//        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3336)).thenReturn(Boolean.TRUE);
//
//    }
//
//    /**
//     * Feature: MBCA-1925
//     * Description: Mi Banco redirect from TLP page (redirect reward points param), pop-up opened when the NonPremiaFlag is enabled because
//     * the user has NO premia account
//     *
//     * @throws Exception
//     */
//    @Test
//    public void whenHandleRequestInternal_Givenlogs1740_ThenprintLogs() throws Exception {
//        //Given
//        userSession.setCustomerService(customerService);
//        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
//        userSession.setBankingSession(null);
//        //When
//        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);
//        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
//        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
//        when(customerService.getCISCustomerInfo(any())).thenReturn(cisResponse);
//
//        //Force redirect reward points. Param is pushed by MBCA-2019
//        when(userSession.isRewardPoints()).thenReturn(true);
//        when(userSession.getBankingSession()).thenReturn(null);
//        when(userSession.getUserSessionType()).thenReturn(UserSessionType.CUSTOMER_PROFILE);
//        when(userSession.getBankingSession()).thenReturn(null);
//        AccountApplications apps = new AccountApplications();
//        apps.setCompletedApplications(null);
//        when(userSession.getAccountApplications(true)).thenReturn(apps);
//        when(userSession.getUsername()).thenReturn("test");
//
//
//        PowerMockito.mockStatic(BankingSessionResponse.class);
//        PowerMockito.mockStatic(VaultAccountsFacade.class);
//        when(VaultAccountsFacade.getInstance()).thenReturn(vaultAccountsFacade);
//        BankingSessionResponse resp = new BankingSessionResponse();
//        resp.setErrorCode(1740);
//        when(vaultAccountsFacade.vaultSignIn(any())).thenReturn(resp);
//
//        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
//        assertNotNull(modelAndView);
//        assertViewName(modelAndView, "forward:updEnrollAcct");
//    }
//
//    /**
//     * Feature: MBCA-1925
//     * Description: Mi Banco redirect from TLP page (redirect reward points param), pop-up opened when the NonPremiaFlag is enabled because
//     * the user has NO premia account
//     *
//     * @throws Exception
//     */
//    @Test
//    public void whenHandleRequestInternal_QualifiesUpgradeProcess_ThenprintLogs() throws Exception {
//        //Given
//        userSession.setCustomerService(customerService);
//        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
//        userSession.setBankingSession(null);
//        //When
//        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);
//        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
//        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
//        when(customerService.getCISCustomerInfo(any())).thenReturn(cisResponse);
//
//        //Force redirect reward points. Param is pushed by MBCA-2019
//        when(userSession.isRewardPoints()).thenReturn(true);
//        when(userSession.getBankingSession()).thenReturn(null);
//        when(userSession.getUserSessionType()).thenReturn(UserSessionType.CUSTOMER_PROFILE_LITE);
//        when(userSession.getBankingSession()).thenReturn(null);
//        AccountApplications apps = new AccountApplications();
//        apps.setCompletedApplications(null);
//        when(userSession.getAccountApplications(true)).thenReturn(apps);
//        when(userSession.getUsername()).thenReturn("test");
//        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP)).thenReturn(Boolean.TRUE);
//        PowerMockito.mockStatic(CustomerUtils.class);
//        when(CustomerUtils.isSecurities(any())).thenReturn(Boolean.TRUE);
//        userSession.getCustomerProfile().setStatus(CustomerStatus.ACTIVE);
//        //CustomerProfile profile = new CustomerProfile();
//        //profile.setStatus(CustomerStatus.ENROLLED);
//        //when(userSession.getCustomerProfile()).thenReturn(profile);
//        when(userSession.getCustomerProfile().getStatus()).thenReturn(CustomerStatus.ACTIVE);
//
//        PowerMockito.mockStatic(BankingSessionResponse.class);
//        PowerMockito.mockStatic(VaultAccountsFacade.class);
//        when(VaultAccountsFacade.getInstance()).thenReturn(vaultAccountsFacade);
//
//        when(EnrollmentServiceModule.getInstance()).thenReturn(enrollmentServiceModule);
//        BankingSessionResponse resp = new BankingSessionResponse();
//        //resp.setErrorCode(1740);
//        when(vaultAccountsFacade.vaultSignIn(any())).thenReturn(resp);
//        PersonalInformationValidationResponse resppersonal = new PersonalInformationValidationResponse();
//        resppersonal.setHasEnrollableAccounts(true);
//        //PersonalInformationValidationResponse.EnrollmentValidationStatus status = new PersonalInformationValidationResponse.EnrollmentValidationStatus();
//        resppersonal.setStatus(PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE);
//        when(enrollmentServiceModule.isEnrollableCustomer(any(), any())).thenReturn(resppersonal);
//
//        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
//        assertNotNull(modelAndView);
//        assertViewName(modelAndView, "forward:updEnrollAcct");
//    }
//
//    /**
//     * Feature: MBCA-1925
//     * Description: Mi Banco redirect from TLP page (redirect reward points param), pop-up opened when the NonPremiaFlag is enabled because
//     * the user has NO premia account
//     *
//     * @throws Exception
//     */
//    @Test
//    public void whenHandleRequestInternal_NontransactionalEnrollable_ThenprintLogs() throws Exception {
//        //Given
//        userSession.setCustomerService(customerService);
//        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
//        userSession.setBankingSession(null);
//        //When
//        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);
//        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
//        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
//        when(customerService.getCISCustomerInfo(any())).thenReturn(cisResponse);
//
//        //Force redirect reward points. Param is pushed by MBCA-2019
//        when(userSession.isRewardPoints()).thenReturn(true);
//        when(userSession.getBankingSession()).thenReturn(null);
//        when(userSession.getUserSessionType()).thenReturn(UserSessionType.CUSTOMER_PROFILE_LITE);
//        when(userSession.getBankingSession()).thenReturn(null);
//        AccountApplications apps = new AccountApplications();
//        apps.setCompletedApplications(null);
//        when(userSession.getAccountApplications(true)).thenReturn(apps);
//        when(userSession.getUsername()).thenReturn("test");
//        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA_UPDT_ACCTS)).thenReturn(Boolean.TRUE);
//        PowerMockito.mockStatic(CustomerUtils.class);
//        when(CustomerUtils.isSecurities(any())).thenReturn(Boolean.TRUE);
//        userSession.getCustomerProfile().setStatus(CustomerStatus.ACTIVE);
//
//        when(userSession.getCustomerProfile().getStatus()).thenReturn(CustomerStatus.ACTIVE);
//        when(userSession.getCustomerProfile().getIsTransactional()).thenReturn("N");
//
//        PowerMockito.mockStatic(BankingSessionResponse.class);
//        PowerMockito.mockStatic(VaultAccountsFacade.class);
//        when(VaultAccountsFacade.getInstance()).thenReturn(vaultAccountsFacade);
//
//        when(EnrollmentServiceModule.getInstance()).thenReturn(enrollmentServiceModule);
//        BankingSessionResponse resp = new BankingSessionResponse();
//        //resp.setErrorCode(1740);
//        when(vaultAccountsFacade.vaultSignIn(any())).thenReturn(resp);
//        PersonalInformationValidationResponse resppersonal = new PersonalInformationValidationResponse();
//        resppersonal.setHasEnrollableAccounts(true);
//
//        when(Utils.hasActiveEnrollableTransAcc(any())).thenReturn(true);
//
//
//        //PersonalInformationValidationResponse.EnrollmentValidationStatus status = new PersonalInformationValidationResponse.EnrollmentValidationStatus();
//        resppersonal.setStatus(PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE);
//        when(enrollmentServiceModule.isEnrollableCustomer(any(), any())).thenReturn(resppersonal);
//
//        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
//        assertNotNull(modelAndView);
//        assertViewName(modelAndView, "forward:updEnrollAcct");
//    }
//
//    /**
//     * Feature: MBCA-1925
//     * Description: Mi Banco redirect from TLP page (redirect reward points param), pop-up opened when the NonPremiaFlag is enabled because
//     * the user has NO premia account
//     *
//     * @throws Exception
//     */
//    @Test
//    public void whenHandleRequestInternal_MBSFE213_ThenprintLogs() throws Exception {
//        //Given
//        userSession.setCustomerService(customerService);
//        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
//        userSession.setBankingSession(null);
//        //When
//        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.FALSE);
//        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
//        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
//        when(customerService.getCISCustomerInfo(any())).thenReturn(cisResponse);
//
//        //Force redirect reward points. Param is pushed by MBCA-2019
//        when(userSession.isRewardPoints()).thenReturn(true);
//        when(userSession.getBankingSession()).thenReturn(null);
//        when(userSession.getUserSessionType()).thenReturn(UserSessionType.CUSTOMER_PROFILE_LITE);
//        when(userSession.getBankingSession()).thenReturn(null);
//        AccountApplications apps = new AccountApplications();
//        apps.setCompletedApplications(null);
//        when(userSession.getAccountApplications(true)).thenReturn(apps);
//        when(userSession.getUsername()).thenReturn("test");
//        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ACH_PAYMENT_NT)).thenReturn(Boolean.TRUE);
//        PowerMockito.mockStatic(CustomerUtils.class);
//        when(CustomerUtils.isSecurities(any())).thenReturn(Boolean.TRUE);
//        userSession.getCustomerProfile().setStatus(CustomerStatus.ACTIVE);
//
//        when(userSession.getCustomerProfile().getStatus()).thenReturn(CustomerStatus.ACTIVE);
//        when(userSession.getCustomerProfile().getIsTransactional()).thenReturn("N");
//
//        PowerMockito.mockStatic(BankingSessionResponse.class);
//        PowerMockito.mockStatic(VaultAccountsFacade.class);
//        when(VaultAccountsFacade.getInstance()).thenReturn(vaultAccountsFacade);
//
//        when(EnrollmentServiceModule.getInstance()).thenReturn(enrollmentServiceModule);
//        BankingSessionResponse resp = new BankingSessionResponse();
//        //resp.setErrorCode(1740);
//        when(vaultAccountsFacade.vaultSignIn(any())).thenReturn(resp);
//        PersonalInformationValidationResponse resppersonal = new PersonalInformationValidationResponse();
//        resppersonal.setHasEnrollableAccounts(true);
//
//        when(Utils.hasActiveEnrollableTransAcc(any())).thenReturn(true);
//
//
//        //PersonalInformationValidationResponse.EnrollmentValidationStatus status = new PersonalInformationValidationResponse.EnrollmentValidationStatus();
//        resppersonal.setStatus(PersonalInformationValidationResponse.EnrollmentValidationStatus.ENROLLABLE);
//        when(enrollmentServiceModule.isEnrollableCustomer(any(), any())).thenReturn(resppersonal);
//
//        ModelAndView modelAndView = controller.handleRequestInternal(request, response);
//        assertNotNull(modelAndView);
//        assertViewName(modelAndView, "forward:updEnrollAcct");
//    }
//
}

