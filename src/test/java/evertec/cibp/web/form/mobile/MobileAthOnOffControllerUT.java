package evertec.cibp.web.form.mobile;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;


import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.UserSession;
import evertec.onoff.models.PlasticOnOffModel;
import evertec.onoff.models.PlasticOnOffResponse;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class})

public class MobileAthOnOffControllerUT extends AbstractModelAndViewTests {
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock 
	private UserSession userSession;
	
	@Mock
	private PlasticOnOffResponse plasticOnOffReponse;
	
	@InjectMocks
	private MobileAthOnOffController mobileAthOnOffController;


	static final String[] athBins = {"454951", "454952", "454953"};
	
	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(StaticMessageSource.class);
		plasticOnOffReponse = new PlasticOnOffResponse();
	}
	
	@Test
	public void whenSendOnOffActionNotifications_GivenValidPlastic_ThenVerifyMethodWasCalledOnce() throws Exception {
	
		PlasticOnOffModel card = new PlasticOnOffModel();
		card.setCardNumber("4549516151232696");
		card.setStatus("ON");
		plasticOnOffReponse.setCard(card);
		
		CustomerServices cServices = spy(CustomerServices.class);
		when(userSession.getCustomerService()).thenReturn(cServices);
		when(userSession.getAllowedOnOffAthBins()).thenReturn(athBins);
		mobileAthOnOffController.sendOnOffActionNotifications(userSession, plasticOnOffReponse);
	
		Mockito.verify(userSession.getCustomerService(), times(1)).sendOnOffActionNotifications(any(), any(), any());
		
	}
	
	@Test
	public void whenSendOnOffActionNotifications_GivenNullPlastic_ThenVerifyMethodNeverGetsCalled() throws Exception {
	
		PlasticOnOffModel card = new PlasticOnOffModel();
		card.setCardNumber(null);
		card.setStatus(null);

		plasticOnOffReponse.setCard(card);
		
		CustomerServices cServices = spy(CustomerServices.class);
		when(userSession.getCustomerService()).thenReturn(cServices);
		when(userSession.getAllowedOnOffAthBins()).thenReturn(athBins);
		mobileAthOnOffController.sendOnOffActionNotifications(userSession, plasticOnOffReponse);
	
		Mockito.verify(userSession.getCustomerService(), times(0)).sendOnOffActionNotifications(any(), any(), any());
	}
}

