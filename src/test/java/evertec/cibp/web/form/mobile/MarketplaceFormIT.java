package evertec.cibp.web.form.mobile;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerInteractionService;
import evertec.cibp.crypto.CryptoUtils;
import evertec.cibp.web.enums.UnicaAuthCode;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.OtpChallengeUtils;
import evertec.solicitudunica.response.UnicaAccessTokenResp;

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, UriUtils.class, MarketplaceForm.class, OtpChallengeUtils.class})
public class MarketplaceFormIT {
	
	private static final String UNICA_AUTH_CODE = "UNICA_AUTHORIZATION_CODE";
	private static final String UNICA_REFERRER = "UNICA_REFERRER";
	private static final String code = RandomStringUtils.randomNumeric(6);
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	private MockHttpSession httpSession;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock 
	private CustomerInteractionService customerInteractionService;
	
	@Mock 
	private UserSession userSession;
	
	@Mock
	private CryptoUtils cryptoUtils;
	
	@Spy
	@InjectMocks
	private MarketplaceForm controller = new MarketplaceForm();

	@Before
	public void setUp() throws Exception {
		
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		httpSession = new MockHttpSession();
		
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(UriUtils.class);
		PowerMockito.spy(OtpChallengeUtils.class);
		
		controller.setResendCodeMaxRetries(2);
		controller.setUnicaUrl("http://192.168.176.2:8092/un/appRetrievePrefill.htm");
		controller.setResendCodeExpiredPeriod(300);
		controller.setValidateCodeMaxRetries(3);
	}

	@Test
	public void whenITDownstreamAcceptMarketplaceTerms_GivenValidProduct_ThenLogEventReached() throws Exception {
		String username = "kira";
		String product = "visa_cashreward";
		
		request.addParameter("product", product);
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getUsername()).thenReturn(username);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		
		controller.acceptMarketplaceTerms(request, response);
		
		verifyPrivate(controller, times(1)).invoke("logEvent", any(), any());
		
	}
	
	@Test
	public void whenITDownstreamAcceptMarketplaceTerms_GivenInvalidProduct_ThenLogCustomerEventNotReached() throws Exception {
		String product = "invalid";
		
		request.addParameter("product", product);
		
		controller.acceptMarketplaceTerms(request, response);
		
		verifyPrivate(controller, never()).invoke("logEvent", any(), any());
	}
	
	@Test
	public void whenITDownstreamAcceptMarketplaceTerms_GivenEmptyProduct_ThenLogCustomerEventNotReached() throws Exception {
		String product = "";
		
		request.addParameter("product", product);
		
		controller.acceptMarketplaceTerms(request, response);
		
		verifyPrivate(controller, never()).invoke("logEvent", any(), any());
	}
	
	@Test
	public void whenITDownstreamGenerateOtpCode_GivenResendCodeIsNull_ThenSendChallengeReached() {
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService); 
		when(customerService.sendOtpChallengeEmail(any(), any(), any())).thenReturn(true);
		
		controller.generateOtpCode(request, response);
		
		verifyStatic(OtpChallengeUtils.class, times(1));
		OtpChallengeUtils.sendChallenge(userSession);
	}
	
	@Test
	public void whenITDownstreamGenerateOtpCode_GivenResendCodeIsTrueAndSendRetriesIs2_ThenSendChallengeReached() {
		
		request.addParameter("resendCode", Boolean.TRUE.toString());
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getOtpSendRetries()).thenReturn(2);
		when(customerService.sendOtpChallengeEmail(any(), any(), any())).thenReturn(true);
		
		controller.generateOtpCode(request, response);
		
		verifyStatic(OtpChallengeUtils.class, times(1));
		OtpChallengeUtils.sendChallenge(userSession);
	}
	
	@Test
	public void whenITDownstreamGenerateOtpCode_GivenResendCodeIsTrueAndSendRetriesIs3_ThenSendChallengeNotReached() {
		
		request.addParameter("resendCode", Boolean.TRUE.toString());
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getOtpSendRetries()).thenReturn(3);
		
		controller.generateOtpCode(request, response);
		
		verifyStatic(OtpChallengeUtils.class, never());
		OtpChallengeUtils.sendChallenge(userSession);
	}
	
	@Test
	public void whenITDownstreamGetUnicaUrl_GivenNullReferrer_ThenGenerateUnicaUrlNotReached() throws Exception {
		request.setSession(httpSession);
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		
		controller.getUnicaUrl(request, response);
		
		verifyPrivate(controller, never()).invoke("generateUnicaUrl", any(), any(), any());
	}
	
	@Test
	public void whenITDownstreamGetUnicaUrl_GivenValidReferrerAndNullUnicaAccessToken_ThenGenerateUnicaUrlReached() throws Exception {
		UnicaAccessTokenResp tokenResp = new UnicaAccessTokenResp(null, "tokenType", "0");
		
		httpSession.setAttribute(UNICA_REFERRER, "visa_cashreward"); 
		request.setSession(httpSession);
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getUnicaAccessToken()).thenReturn(tokenResp);
		
		controller.getUnicaUrl(request, response);
		
		verifyPrivate(controller, never()).invoke("generateUnicaUrl", any(), any(), any());
	}
	
	@Test
	public void whenITDownstreamGetUnicaUrl_GivenValidReferrerAndUnicaAccessToken_ThenGenerateUnicaUrlReached() throws Exception {
		UnicaAccessTokenResp tokenResp = new UnicaAccessTokenResp("gldksjglkdfj", "tokenType", "0");
		
		httpSession.setAttribute(UNICA_REFERRER, "visa_cashreward"); 
		request.setSession(httpSession);
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getUnicaAccessToken()).thenReturn(tokenResp);
		
		controller.getUnicaUrl(request, response);
		
		verifyPrivate(controller, times(1)).invoke("generateUnicaUrl", any(), any(), any());
	}
	
	@Test
	public void whenITDownstreamGenerateUnicaUrl_GivenNoUnicaAuthCode_ThenGetSsoTokenForUnicaNotReached() throws Exception {
		httpSession.setAttribute(UNICA_REFERRER, "visa_cashreward"); 
		request.setSession(httpSession);
		
		Whitebox.invokeMethod(controller, "generateUnicaUrl", request, userSession, Logger.getLogger(MarketplaceForm.class));
		
		verifyPrivate(controller, never()).invoke("getSsoTokenForUnica", any(), any(), any());
	}
	
	@Test
	public void whenITDownstreamGenerateUnicaUrl_GivenValidUnicaAuthCode_ThenGetSsoTokenForUnicaReached() throws Exception {
		String ssoToken = "2c649210-9463-4567-a7b1-3426f8ceb67b";
		
		httpSession.setAttribute(UNICA_REFERRER, "visa_cashreward");
		httpSession.setAttribute(UNICA_AUTH_CODE, UnicaAuthCode.NO_AUTHENTICATION_PRESENTED);
		request.setSession(httpSession);
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.tokenGenerate(any(), any(), any())).thenReturn(ssoToken);
		PowerMockito.whenNew(CryptoUtils.class).withNoArguments().thenReturn(cryptoUtils);
		when(cryptoUtils.encrypt(any(), any(), any())).thenReturn("lfkjadhsfkljadshflkjadhsfs");
		when(UriUtils.encodeQueryParam(any(), any())).thenReturn("lkfjadshflkjsadhfklj");
		
		Whitebox.invokeMethod(controller, "generateUnicaUrl", request, userSession, Logger.getLogger(MarketplaceForm.class));
		
		verifyPrivate(controller, times(1)).invoke("getSsoTokenForUnica", any(), any(), any());
	}
	
	@Test
	public void whenITDownstreamGenerateUnicaUrl_GivenInvalidUnicaAuthCode_ThenGetSsoTokenForUnicaNotReached() throws Exception {
		httpSession.setAttribute(UNICA_REFERRER, "visa_cashreward");
		request.setSession(httpSession);
		
		Whitebox.invokeMethod(controller, "generateUnicaUrl", request, userSession, Logger.getLogger(MarketplaceForm.class));
		
		verifyPrivate(controller, never()).invoke("getSsoTokenForUnica", any(), any(), any());
	}
	
	@Test
	public void whenITDownstreamValidateOtpCode_GivenCodeNotExpired_ThenHasCodeExpiredReached() throws Exception {
		
		int resendCodeExpiredPeriod = 300;
		
		request.addParameter("code", code);
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getOtpChallengeCode()).thenReturn(code);
		when(userSession.getOtpLastChallengeDate()).thenReturn(new Date());
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		
		controller.validateOtpCode(request, response);
		
		verifyStatic(OtpChallengeUtils.class, times(1));
		OtpChallengeUtils.hasCodeExpired(userSession, resendCodeExpiredPeriod);
	}
	
	@Test
	public void whenITDownstreamValidateOtpCode_GivenCodeExpired_ThenValidateLimitReached() throws Exception {

		request.addParameter("code", code);
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getOtpChallengeCode()).thenReturn(code);
		when(userSession.getOtpLastChallengeDate()).thenReturn(new Date());
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		
		controller.setResendCodeExpiredPeriod(0);
		
		controller.validateOtpCode(request, response);
		
		verifyPrivate(controller, times(1)).invoke("validateLimitReached", any());
	}
	
	@Test
	public void whenITDownstreamValidateOtpCode_GivenCodeExpired_AndValidateLimitReachedFalse_ThenHasReachedResendLimitReached() throws Exception {
		
		request.addParameter("code", code);
		userSession.setOtpChallengeCode(code);
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getOtpChallengeCode()).thenReturn(code);
		when(userSession.getOtpLastChallengeDate()).thenReturn(new Date());
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		
		controller.setResendCodeExpiredPeriod(0);
		controller.setResendCodeMaxRetries(2);
		
		controller.validateOtpCode(request, response);
		
		verifyStatic(OtpChallengeUtils.class, times(1));
		OtpChallengeUtils.hasReachedResendLimit(userSession, 2);
	}
	
	@Test
	public void whenITDownstreamValidateOtpCode_GivenCodeNotExpired_AndInvalidCode_AndLimitReached_ThenSetOtpAuthenticationResultReached() throws Exception {
		
		request.addParameter("code", code);
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getOtpChallengeCode()).thenReturn(RandomStringUtils.randomNumeric(6));
		when(userSession.getOtpLastChallengeDate()).thenReturn(new Date());
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(userSession.getOtpValidateRetries()).thenReturn(4);
		
		controller.setResendCodeExpiredPeriod(2);
		controller.setResendCodeMaxRetries(1);
		
		controller.validateOtpCode(request, response);
		
		verifyPrivate(controller, times(1)).invoke("validateLimitReached", any());
		verifyPrivate(controller, times(1)).invoke("setOtpAuthenticationResult", any(), any(), any());
		verifyPrivate(controller, times(1)).invoke("resetOtpChallenge", any());
	}
	
	@Test
	public void whenITDownstreamValidateOtpCode_GivenCodeNotExpired_AndInvalidCode_ThenSetOtpAuthenticationResultNotReached() throws Exception {
		
		request.addParameter("code", code);
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getOtpChallengeCode()).thenReturn(RandomStringUtils.randomNumeric(6));
		when(userSession.getOtpLastChallengeDate()).thenReturn(new Date());
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getInteractionService()).thenReturn(customerInteractionService);
		when(userSession.getOtpValidateRetries()).thenReturn(1);
		
		controller.setResendCodeExpiredPeriod(2);
		controller.setResendCodeMaxRetries(1);
		
		controller.validateOtpCode(request, response);
		
		verifyPrivate(controller, times(0)).invoke("setOtpAuthenticationResult", any(), any(), any());
	}
	
}
