package evertec.cibp.web.form.mobile.bean;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import bppr.sms.beans.SMSAccount;
import bppr.sms.beans.SMSAlertCondition;
import evertec.cibp.core.models.customer.AlertCondition;
import evertec.cibp.core.models.customer.CustomerBalanceAlert;
import evertec.cibp.core.models.customer.CustomerEmailBalanceAlert;
import evertec.cibp.core.models.customer.EmailAlertCondition;

/**
 * 
 * @author s679338
 *
 */
public class MobileAlertUT {
	
	public static final String GREATER_THAN = "GT";
	public static final String LESS_THAN = "LT";
	
	@Mock
	private CustomerEmailBalanceAlert mCustomerEmailBalanceAlert;
	
	@Mock
	private CustomerBalanceAlert mustomerBalanceAlert;
	
	@Mock
	private AlertCondition mAlertCondition;
	
	@Mock
	private SMSAlertCondition mSMSAlertCondition;
	
	@Mock
	private EmailAlertCondition mEmailAlertCondition;
	
	@Mock 
	private SMSAccount mSMSAccount;
	
	@InjectMocks
	private MobileAlert objectTesting;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * when GetMobileAlert
	 * give CustomerEmailBalanceAlert With AlertCondition Null
	 * then ReturnMobileAlert
	 */
	@Test
	public void whenGetMobileAlert_giveEmailAlertWithAlertConditionNull_thenReturnMobileAlert() {
		MobileAlert mobileAlert = objectTesting.getMobileAlert(mCustomerEmailBalanceAlert);
		
		assertNotNull(mobileAlert);
	}
	
	/**
	 * when GetMobileAlert
	 * give CustomerEmailBalanceAlert With AlertCondition Not Null
	 * then ReturnMobileAlert
	 */
	@Test
	public void teswhenGetMobileAlert_giveEmailAlertWithAlertConditionNotNull_thenReturnMobileAlertt() {
		when(mCustomerEmailBalanceAlert.getAlertCondition()).thenReturn(mEmailAlertCondition);
		
		MobileAlert mobileAlert = objectTesting.getMobileAlert(mCustomerEmailBalanceAlert);
		
		assertNotNull(mobileAlert);
	}
	
	/**
	 * when GetMobileAlert
	 * give CustomerEmailBalanceAlert With AlertCondition with GREATER_THAN value
	 * then ReturnMobileAlert
	 */
	@Test
	public void teswhenGetMobileAlert_giveEmailAlertWithAlertConditionWithValue_thenReturnMobileAlertt() {
		when(mCustomerEmailBalanceAlert.getAlertCondition()).thenReturn(mEmailAlertCondition);
		when(mEmailAlertCondition.getOperator()).thenReturn(GREATER_THAN);
		
		MobileAlert mobileAlert = objectTesting.getMobileAlert(mCustomerEmailBalanceAlert);
		
		assertNotNull(mobileAlert);
	}
	
	/**
	 * when GetMobileAlert
	 * give CustomerEmailBalanceAlert With AlertCondition with LESS_THAN value
	 * then ReturnMobileAlert
	 */
	@Test
	public void teswhenGetMobileAlert_giveEmailAlertWithAlertConditionWithLessValue_thenReturnMobileAlertt() {
		when(mCustomerEmailBalanceAlert.getAlertCondition()).thenReturn(mEmailAlertCondition);
		when(mEmailAlertCondition.getOperator()).thenReturn(LESS_THAN);
		
		MobileAlert mobileAlert = objectTesting.getMobileAlert(mCustomerEmailBalanceAlert);
		
		assertNotNull(mobileAlert);
	}
	
	/**
	 * when GetMobileAlert
	 * give SMSAccount With AlertCondition Null
	 * then ReturnMobileAlert
	 */
	@Test
	public void whenGetMobileAlert_giveSMSAlertWithAlertConditionNull_thenReturnMobileAlert() {
		MobileAlert mobileAlert = objectTesting.getMobileAlert(mSMSAccount);
		
		assertNotNull(mobileAlert);
	}
	
	/**
	 * when GetMobileAlert
	 * give CustomerBalanceAlert With AlertCondition Not Null
	 * then ReturnMobileAlert
	 */
	@Test
	public void teswhenGetMobileAlert_giveSMSAlertWithAlertConditionNotNull_thenReturnMobileAlertt() {
		when(mSMSAccount.getAlertCondition()).thenReturn(mSMSAlertCondition);
		
		MobileAlert mobileAlert = objectTesting.getMobileAlert(mSMSAccount);
		
		assertNotNull(mobileAlert);
	}
	
	/**
	 * when GetMobileAlert
	 * give CustomerBalanceAlert With AlertCondition with GREATER_THAN value
	 * then ReturnMobileAlert
	 */
	@Test
	public void teswhenGetMobileAlert_giveSMSAlertWithAlertConditionWithValue_thenReturnMobileAlertt() {
		when(mSMSAccount.getAlertCondition()).thenReturn(mSMSAlertCondition);
		when(mSMSAlertCondition.getOperator()).thenReturn(GREATER_THAN);
		
		MobileAlert mobileAlert = objectTesting.getMobileAlert(mSMSAccount);
		
		assertNotNull(mobileAlert);
	}
	
	/**
	 * when GetMobileAlert
	 * give CustomerBalanceAlert With AlertCondition with LESS_THAN value
	 * then ReturnMobileAlert
	 */
	@Test
	public void teswhenGetMobileAlert_giveSMSAlertWithAlertConditionWithLessValue_thenReturnMobileAlertt() {
		when(mSMSAccount.getAlertCondition()).thenReturn(mSMSAlertCondition);
		when(mSMSAlertCondition.getOperator()).thenReturn(LESS_THAN);
		
		MobileAlert mobileAlert = objectTesting.getMobileAlert(mSMSAccount);
		
		assertNotNull(mobileAlert);
	}

	/**
	 * when GetMobileAlert
	 * give SMSAccount With AlertCondition Null
	 * then ReturnMobileAlert
	 */
	@Test
	public void whenGetMobileAlert_givePushAlertWithAlertConditionNull_thenReturnMobileAlert() {
		MobileAlert mobileAlert = objectTesting.getMobileAlert(mustomerBalanceAlert);
		
		assertNotNull(mobileAlert);
	}
	
	/**
	 * when GetMobileAlert
	 * give CustomerBalanceAlert With AlertCondition Not Null
	 * then ReturnMobileAlert
	 */
	@Test
	public void teswhenGetMobileAlert_givePushAlertWithAlertConditionNotNull_thenReturnMobileAlertt() {
		when(mustomerBalanceAlert.getAlertCondition()).thenReturn(mAlertCondition);
		
		MobileAlert mobileAlert = objectTesting.getMobileAlert(mustomerBalanceAlert);
		
		assertNotNull(mobileAlert);
	}
	
	/**
	 * when GetMobileAlert
	 * give CustomerBalanceAlert With AlertCondition and amount Not Null
	 * then ReturnMobileAlert
	 */
	@Test
	public void teswhenGetMobileAlert_givePushAlertWithAlertConditionAndAmountNotNull_thenReturnMobileAlertt() {
		when(mustomerBalanceAlert.getAlertCondition()).thenReturn(mAlertCondition);
		when(mAlertCondition.getAmount()).thenReturn(new BigDecimal(0.0));
		
		MobileAlert mobileAlert = objectTesting.getMobileAlert(mustomerBalanceAlert);
		
		assertNotNull(mobileAlert);
	}
	
	/**
	 * when GetMobileAlert
	 * give CustomerBalanceAlert With AlertCondition and Operator  GREATER_THAN
	 * then ReturnMobileAlert
	 */
	@Test
	public void teswhenGetMobileAlert_givePushAlertWithAlertConditionAndOperatorGreater_thenReturnMobileAlertt() {
		when(mustomerBalanceAlert.getAlertCondition()).thenReturn(mAlertCondition);
		when(mAlertCondition.getAmount()).thenReturn(new BigDecimal(0.0));
		when(mAlertCondition.getOperator()).thenReturn(GREATER_THAN);
		
		MobileAlert mobileAlert = objectTesting.getMobileAlert(mustomerBalanceAlert);
		
		assertNotNull(mobileAlert);
	}
	
	/**
	 * when GetMobileAlert
	 * give CustomerBalanceAlert With AlertCondition and Operator LESS_THAN
	 * then ReturnMobileAlert
	 */
	@Test
	public void teswhenGetMobileAlert_givePushAlertWithAlertConditionAndOperatorLess_thenReturnMobileAlertt() {
		when(mustomerBalanceAlert.getAlertCondition()).thenReturn(mAlertCondition);
		when(mAlertCondition.getAmount()).thenReturn(new BigDecimal(0.0));
		when(mAlertCondition.getOperator()).thenReturn(LESS_THAN);
		
		MobileAlert mobileAlert = objectTesting.getMobileAlert(mustomerBalanceAlert);
		
		assertNotNull(mobileAlert);
	}

}
