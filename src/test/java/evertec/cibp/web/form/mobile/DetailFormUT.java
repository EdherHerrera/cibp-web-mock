/**
 * 
 */
package evertec.cibp.web.form.mobile;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.DetailForm;
import evertec.cibp.web.form.bean.DetailBean;
import evertec.cibp.web.session.UserSession;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * @author ET57970
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, StaticMessageSource.class, AccountUtils.class})
public class DetailFormUT extends AbstractModelAndViewTests{
		
	private MockHttpServletRequest request;
	
	@Mock
	DetailBean detailBean;
	
	@Mock
	private UserSession userSession;
	
	@Mock
    private BindException bindException;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private TVFrontendAccount accountMock;
	
	@InjectMocks
	DetailForm detailForm;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(AccountUtils.class);
		
		detailForm = PowerMockito.spy(new DetailForm());
		detailBean = PowerMockito.mock(DetailBean.class);
		request = new MockHttpServletRequest();
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
	}
	
	

    
   

}
