package evertec.cibp.web.form.mobile;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import com.evertec.cibp.api.client.util.ClientUtils;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MobileAlertsController.class, UserSession.class, ClientUtils.class, AccountUtils.class,
		StaticMessageSource.class, WebUtils.class, ApiCallerFactory.class})
public class MobileAlertsControllerUT extends AbstractModelAndViewTests {

	private HttpServletRequest request;
	private HttpServletResponse response;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private ApiCaller apiCaller;

	@InjectMocks
	private MobileAlertsController controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

		PowerMockito.mockStatic(AccountUtils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(WebUtils.class);
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();

		userSession = PowerMockito.spy(new UserSession());
		controller = PowerMockito.spy(new MobileAlertsController());
		PowerMockito.mockStatic(ClientUtils.class);
	}

	@Test
	public void whenMobileAlerts_ReturnModelAndView() throws Exception {
		
		/*
		MicroserviceStatusResponse responsee = new MicroserviceStatusResponse();		
		PowerMockito.doReturn(userSession).when(controller, "getSession", request);
		PowerMockito.doReturn(new CustomerProfile()).when(userSession, "getCustomerProfile");
		PowerMockito.doReturn(responsee).when(ClientUtils.getMicroserviceEndpointAndStatus(any(String.class)));
		PowerMockito.doReturn(true).when(responsee.isEnabled());

		ModelAndView modelAndView = controller.mobileAlerts(request, response);
	
		assertViewName(modelAndView, "mobileAlerts");
		assertModelAttributeAvailable(modelAndView, "hasDDA");
		assertModelAttributeAvailable(modelAndView, "hasCCA");
		assertModelAttributeAvailable(modelAndView, "hasPremia");
		assertModelAttributeAvailable(modelAndView, "useCommercialInterface");
		
		assertModelAttributeAvailable(modelAndView, "showMenuPayroll");
		assertModelAttributeAvailable(modelAndView, "isEvendorActive");
		assertModelAttributeAvailable(modelAndView, "isNonTransactional");
		assertModelAttributeAvailable(modelAndView, "showFraudPreventionService");
		assertModelAttributeAvailable(modelAndView, "isAlertsMSEnabled");
		*/
	}

	/**
	 * when MobileAlerts
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenMobileAlerts_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.hasVisibleDDA()).thenReturn(Boolean.TRUE);
		when(Utils.isPushPhase2Enabled()).thenReturn(Boolean.TRUE);
		when(userSession.hasVisibleCCA()).thenReturn(Boolean.TRUE);
		when(userSession.hasVisiblePremia()).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.useCommercialInterface()).thenReturn(Boolean.TRUE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.TRUE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EVENDOR)).thenReturn(Boolean.TRUE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.FRAUD_PREVENTION_SERVICE)).thenReturn(Boolean.TRUE);
		when(userSession.isHasOnOffEligibleAccount()).thenReturn(Boolean.TRUE);

		ModelAndView result = controller.mobileAlerts(request, response);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}
}
