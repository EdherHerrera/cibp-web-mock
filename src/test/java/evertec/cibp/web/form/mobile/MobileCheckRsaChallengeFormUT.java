package evertec.cibp.web.form.mobile;

import com.rsa.csd.ws.*;
import edu.emory.mathcs.backport.java.util.Collections;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.customer.modules.SignOnCustomerServiceModule;
import evertec.cibp.core.services.responses.adaptiveauth.QuestionResponse;
import evertec.cibp.web.session.UserSession;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;

import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class MobileCheckRsaChallengeFormUT {

    private MockHttpServletRequest request;

    @Mock
    private UserSession userSession;

    @Mock
    private DeviceRequest deviceRequest;

    @Mock
    private CustomerServices customerServices;

    @Mock
    private DeviceResult deviceResult;

    @Mock
    private DeviceData deviceData;

    @Mock
    private IdentificationData identificationData;

    @Mock
    private TriggeredRule triggeredRule;

    @Mock
    private RiskResult riskResult;

    @Mock
    private AnalyzeResponse anResp;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private QuestionResponse questionResponse;
    
    @Mock
    private SignOnCustomerServiceModule signOnCustomerServiceModule;

    @InjectMocks
    MobileCheckRsaChallengeForm form;

    @Before
    public void setUp() {
        request = new MockHttpServletRequest();
    }

    @Test
    public void whenQuestionChallenge_givenActionCodeChallenge_thenReturnTrue () throws Exception {
        deviceData.setDeviceTokenCookie(StringUtils.EMPTY);
        deviceResult.setDeviceData(deviceData);
        anResp.setDeviceResult(deviceResult);
        request.setParameter("device_info_rsa", "test");
        request.setParameter("rsa_cookie", "test");

        when(userSession.getMobileSdkData()).thenReturn(StringUtils.EMPTY);
        when(userSession.getLanguage()).thenReturn("en");
        when(userSession.getUsername()).thenReturn("test");
        when(userSession.getPm_fp()).thenReturn(StringUtils.EMPTY);
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(customerServices.rsaAnalyze(Mockito.any())).thenReturn(anResp);
        when(deviceData.getDeviceTokenCookie()).thenReturn(StringUtils.EMPTY);
        when(deviceResult.getDeviceData()).thenReturn(deviceData);
        when(anResp.getDeviceResult()).thenReturn(deviceResult);
        when(identificationData.getSessionId()).thenReturn(StringUtils.EMPTY);
        when(identificationData.getTransactionId()).thenReturn(StringUtils.EMPTY);
        when(triggeredRule.getActionCode()).thenReturn(ActionCode.CHALLENGE);
        when(riskResult.getTriggeredRule()).thenReturn(triggeredRule);
        when(anResp.getIdentificationData()).thenReturn(identificationData);
        when(anResp.getRiskResult()).thenReturn(riskResult);

        boolean result = Whitebox.invokeMethod(form,"questionChallenge", request, userSession);
        assertTrue(result);
    }

    @Test
    public void whenQuestionChallenge_givenActionCodeDeny_thenReturnFalse() throws Exception {
        deviceData.setDeviceTokenCookie(StringUtils.EMPTY);
        deviceResult.setDeviceData(deviceData);
        anResp.setDeviceResult(deviceResult);
        request.setParameter("device_info_rsa", "test");
        request.setParameter("rsa_cookie", "test");

        when(userSession.getMobileSdkData()).thenReturn(StringUtils.EMPTY);
        when(userSession.getLanguage()).thenReturn("en");
        when(userSession.getUsername()).thenReturn("test");
        when(userSession.getPm_fp()).thenReturn(StringUtils.EMPTY);
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(customerServices.rsaAnalyze(Mockito.any())).thenReturn(anResp);
        when(deviceData.getDeviceTokenCookie()).thenReturn(StringUtils.EMPTY);
        when(deviceResult.getDeviceData()).thenReturn(deviceData);
        when(anResp.getDeviceResult()).thenReturn(deviceResult);
        when(identificationData.getSessionId()).thenReturn(StringUtils.EMPTY);
        when(identificationData.getTransactionId()).thenReturn(StringUtils.EMPTY);
        when(triggeredRule.getActionCode()).thenReturn(ActionCode.DENY);
        when(riskResult.getTriggeredRule()).thenReturn(triggeredRule);
        when(anResp.getIdentificationData()).thenReturn(identificationData);
        when(anResp.getRiskResult()).thenReturn(riskResult);

        boolean result = Whitebox.invokeMethod(form,"questionChallenge", request, userSession);
        assertFalse(result);
    }

    @Test
    public void whenGetQuestion_givenInformationQuestion_thenReturnModel() throws Exception {
        Map<Object, Object> question = Collections.singletonMap("test", "test");

        when(customerProfile.getEmail()).thenReturn("test");
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getInformationQuestion()).thenReturn(question);
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);

        Map<String, Object> result = Whitebox.invokeMethod(form,"getQuestion", request, userSession);
        assertNotNull(result);
    }

    @Test
    public void whenGetQuestion_givenInformationQuestionNull_thenReturnModel() throws Exception {
        when(customerProfile.getEmail()).thenReturn("test");
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getInformationQuestion()).thenReturn(null);
        when(customerServices.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);
        when(customerServices.getSignOnCustomerServiceModule().getRSAPersonalQuestion(Mockito.any())).thenReturn(questionResponse);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);


        Map<String, Object> result = Whitebox.invokeMethod(form,"getQuestion", request, userSession);
        assertNotNull(result);
    }

}
