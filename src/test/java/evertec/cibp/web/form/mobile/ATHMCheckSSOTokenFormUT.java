package evertec.cibp.web.form.mobile;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.ATHMovilEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.mobile.bean.ATHMCheckSSOTokenBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.requests.AthmSsoTokenStatusReq;
import evertec.otherservices.responses.AthmSsoTokenStatusResp;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, WebUtils.class, CustomerFacade.class})
public class ATHMCheckSSOTokenFormUT extends AbstractModelAndViewTests {
	
	private HttpServletRequest request;
	private HttpServletResponse response;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock 
	private CustomerProfileService customerProfileService;
	
	@Mock 
	private UserSession userSession;
	
	@Mock 
	private CustomerFacade customerFacade;
	
	@InjectMocks
	private ATHMCheckSSOTokenForm controller;

	@Before
	public void setUp() throws Exception {
		
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
	}

	@Test
	public void whenOnSubmit_GivenDisabledFlag_ThenReturnModelAndViewWithFlagDisabled() throws Exception {
		ATHMCheckSSOTokenBean bean = new ATHMCheckSSOTokenBean();
		BindException error = new BindException(bean, "ATHMCheckSSOTokenBean");
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ATHM_SSO)).thenReturn(false);
		
		ModelAndView modelAndView = controller.onSubmit(request, response, bean, error);
		
		assertViewName(modelAndView, "athmssotoken");
		assertModelAttributeValue(modelAndView, "flag", false);
	}
	
	@Test
	public void whenOnSubmit_GivenGlobalEntitlementDisabled_ThenReturnModelAndViewWithDowntimeEnabled() throws Exception {
		ATHMCheckSSOTokenBean bean = new ATHMCheckSSOTokenBean();
		BindException error = new BindException(bean, "ATHMCheckSSOTokenBean");
		GlobalEntitlement athmGlobalEntitlement = new GlobalEntitlement();
		
		athmGlobalEntitlement.setStatus(EntitlementStatus.DISABLED);
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ATHM_SSO)).thenReturn(true);
		when(customerService.getGlobalEntitlement(EntitlementType.ATHMOVIL)).thenReturn(athmGlobalEntitlement);
		
		ModelAndView modelAndView = controller.onSubmit(request, response, bean, error);
		
		assertViewName(modelAndView, "athmssotoken");
		assertModelAttributeValue(modelAndView, "downtime", true);
	}
	
	@Test
	public void whenOnSubmit_GivenValidationErrorStatus_ThenReturnModelAndViewWithDowntimeEnabled() throws Exception {
		ATHMCheckSSOTokenBean bean = new ATHMCheckSSOTokenBean();
		BindException error = new BindException(bean, "ATHMCheckSSOTokenBean");
		GlobalEntitlement athmGlobalEntitlement = new GlobalEntitlement();
		AthmSsoTokenStatusResp athmSsoTokenStatusResp = new AthmSsoTokenStatusResp();
		
		athmGlobalEntitlement.setStatus(EntitlementStatus.ENABLED);
		
		athmSsoTokenStatusResp.setStatusCode("46");
		athmSsoTokenStatusResp.setStatusDescription("Validation Error");
		athmSsoTokenStatusResp.setResponseStatus(ResponseStatus.ERROR);
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ATHM_SSO)).thenReturn(true);
		when(customerService.getGlobalEntitlement(EntitlementType.ATHMOVIL)).thenReturn(athmGlobalEntitlement);
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerService.getSsoTokenStatus(any(AthmSsoTokenStatusReq.class))).thenReturn(athmSsoTokenStatusResp);
		
		ModelAndView modelAndView = controller.onSubmit(request, response, bean, error);
		
		assertViewName(modelAndView, "athmssotoken");
		assertModelAttributeValue(modelAndView, "downtime", true);
	}
	
	@Test
	public void whenOnSubmit_GivenStatus98AndInactiveToken_ThenReturnModelAndViewWithSsoToken() throws Exception {
		ATHMCheckSSOTokenBean bean = new ATHMCheckSSOTokenBean();
		BindException error = new BindException(bean, "ATHMCheckSSOTokenBean");
		GlobalEntitlement athmGlobalEntitlement = new GlobalEntitlement();
		CustomerEntitlement customerEntitlement = new ATHMovilEntitlement(Utils.generateUUID()).getCustomerEntitlement();
		AthmSsoTokenStatusResp athmSsoTokenStatusResp = new AthmSsoTokenStatusResp();
		
		athmGlobalEntitlement.setStatus(EntitlementStatus.ENABLED);
		
		athmSsoTokenStatusResp.setStatusCode("98");
		athmSsoTokenStatusResp.setResponseStatus(ResponseStatus.SUCCESS);
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ATHM_SSO)).thenReturn(true);
		when(customerService.getGlobalEntitlement(EntitlementType.ATHMOVIL)).thenReturn(athmGlobalEntitlement);
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.hasEntitlement(EntitlementType.ATHMOVIL)).thenReturn(true);
		when(customerProfile.getEntitlementByType(EntitlementType.ATHMOVIL)).thenReturn(customerEntitlement);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getSsoTokenStatus(any(AthmSsoTokenStatusReq.class))).thenReturn(athmSsoTokenStatusResp);
		
		ModelAndView modelAndView = controller.onSubmit(request, response, bean, error);
		
		assertViewName(modelAndView, "athmssotoken");
		assertTrue(modelAndView.getModel().containsKey("token"));
	}
	
	@Test
	public void whenOnSubmit_GivenStatus98AndActiveToken_ThenReturnModelAndViewWithSsoToken() throws Exception {
		ATHMCheckSSOTokenBean bean = new ATHMCheckSSOTokenBean();
		BindException error = new BindException(bean, "ATHMCheckSSOTokenBean");
		GlobalEntitlement athmGlobalEntitlement = new GlobalEntitlement();	
		AthmSsoTokenStatusResp athmSsoTokenStatusResp = new AthmSsoTokenStatusResp();
		
		bean.setTokenGenerationActive(true);
		
		athmGlobalEntitlement.setStatus(EntitlementStatus.ENABLED);
		
		athmSsoTokenStatusResp.setStatusCode("98");
		athmSsoTokenStatusResp.setResponseStatus(ResponseStatus.SUCCESS);
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ATHM_SSO)).thenReturn(true);
		when(customerService.getGlobalEntitlement(EntitlementType.ATHMOVIL)).thenReturn(athmGlobalEntitlement);
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerService.getSsoTokenStatus(any(AthmSsoTokenStatusReq.class))).thenReturn(athmSsoTokenStatusResp);
		
		ModelAndView modelAndView = controller.onSubmit(request, response, bean, error);
		
		assertViewName(modelAndView, "athmssotoken");
		assertTrue(modelAndView.getModel().containsKey("token"));
	}
	
	@Test
	public void whenOnSubmit_GivenStatus98ActiveTokenAndAthmEntitlement_ThenDeleteAthmEntitlementAndReturnModelAndViewWithSsoToken() throws Exception {
		ATHMCheckSSOTokenBean bean = new ATHMCheckSSOTokenBean();
		BindException error = new BindException(bean, "ATHMCheckSSOTokenBean");
		GlobalEntitlement athmGlobalEntitlement = new GlobalEntitlement();
		CustomerEntitlement customerEntitlement = new ATHMovilEntitlement(Utils.generateUUID()).getCustomerEntitlement();
		AthmSsoTokenStatusResp athmSsoTokenStatusResp = new AthmSsoTokenStatusResp();
		
		bean.setTokenGenerationActive(true);
		
		athmGlobalEntitlement.setStatus(EntitlementStatus.ENABLED);
		
		athmSsoTokenStatusResp.setStatusCode("98");
		athmSsoTokenStatusResp.setResponseStatus(ResponseStatus.SUCCESS);
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ATHM_SSO)).thenReturn(true);
		when(customerService.getGlobalEntitlement(EntitlementType.ATHMOVIL)).thenReturn(athmGlobalEntitlement);
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.hasEntitlement(EntitlementType.ATHMOVIL)).thenReturn(true);
		when(customerProfile.getEntitlementByType(EntitlementType.ATHMOVIL)).thenReturn(customerEntitlement);
		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		when(customerService.getSsoTokenStatus(any(AthmSsoTokenStatusReq.class))).thenReturn(athmSsoTokenStatusResp);
		
		ModelAndView modelAndView = controller.onSubmit(request, response, bean, error);
		
		assertViewName(modelAndView, "athmssotoken");
		assertTrue(modelAndView.getModel().containsKey("token"));
	}

}
