package evertec.cibp.web.form;

import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.AccountServiceBean;
import evertec.cibp.web.form.bean.AlertTypesBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Unit testing AlertTypesForm java class.
 * @author Evertec ET58344
 * @since 09-15-2020
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CustomerFacade.class, StaticMessageSource.class, AccountServicesForm.class, Utils.class,
Calendar.class, GregorianCalendar.class, RequestContextUtils.class, AccountServiceBean.class, ResultStatus.class})
public class AlertTypesFormUT extends AbstractModelAndViewTests  {

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private UserSession userSession;

	@Mock
	private AlertTypesBean alertTypesBean;

	@Mock
	private BindException error;

	@InjectMocks
	private AlertTypesForm controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(GregorianCalendar.class);
		PowerMockito.mockStatic(RequestContextUtils.class);
		PowerMockito.mockStatic(AccountServiceBean.class);
		PowerMockito.mockStatic(ResultStatus.class);
	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-15-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession)
				.thenReturn(userSession).thenReturn(Boolean.FALSE).thenReturn(Boolean.FALSE).thenReturn(Boolean.FALSE)
				.thenReturn(Boolean.FALSE).thenReturn(Boolean.FALSE).thenReturn(Boolean.FALSE).thenReturn(Boolean.FALSE)
				.thenReturn(Boolean.FALSE);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CCA_ALERTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ACH_PAYMENT_NT)).thenReturn(Boolean.FALSE);
		when(userSession.getGuidedSetupSession()).thenReturn(null);
		when(userSession.isInformationChallenge()).thenReturn(Boolean.FALSE);
		when(request.getParameter(anyString())).thenReturn("");
		when(userSession.getCustomerProfile()).thenReturn(null);
		when(Utils.isPushEnabled()).thenReturn(Boolean.FALSE);
		when(userSession.getAccounts()).thenReturn(new ArrayList<>());
		when(userSession.getInformationQuestion()).thenReturn(null);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ON_OFF_ALERTS)).thenReturn(Boolean.FALSE);

		Map<String, Object> result = controller.referenceData(request, alertTypesBean, error);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when OnSubmit
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-15-2020
	 * @exception Exception error
	 */
	@Test
	public void whenOnSubmit_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.isInformationChallenge()).thenReturn(Boolean.FALSE);

		ModelAndView mavResult = controller.onSubmit(request, response, alertTypesBean, error);
		assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}
}
