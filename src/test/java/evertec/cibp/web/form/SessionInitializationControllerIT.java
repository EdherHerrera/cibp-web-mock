package evertec.cibp.web.form;

import static evertec.cibp.web.form.SessionInitializationControllerUT.getPremiaCustomerCodesList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigInteger;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.profile.ProfileType;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;


import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.types.CardAccount;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.CompleteLoginProcessRequest;
import evertec.cibp.core.services.responses.opac.CISOpacCustomerInfoResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.session.UserSession;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Utils.class, StaticMessageSource.class, WebUtils.class})
public class SessionInitializationControllerIT {
    public static BigInteger PREMIAPERMID = BigInteger.valueOf(Long.parseLong("549126001001"));

    @Mock
    CustomerServices customerService;

    @Mock
    CustomerProfile customerProfile;

    @InjectMocks
    SessionInitializationController sessionInitializationController;

    @Mock
    private MockHttpServletRequest httpServletRequest;

    @Mock
    private MockHttpServletResponse httpServletResponse;

    @Mock
    private BankingSession bankingSession;

    @Mock
    private CustomerProfileService customerProfileService;

    @Mock
    UserSession userSession;

    private static final String UNODOSTRES = "123456789";
    private static final String IDA = "IDA";


    @Before
    public void setUp() throws Exception {

        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Utils.class);

        ProfileType profileType = new ProfileType();
        profileType.setId(1234);

        httpServletRequest = new MockHttpServletRequest();
        httpServletResponse = new MockHttpServletResponse();
        when(WebUtils.getSessionAttribute(httpServletRequest, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
        when(customerProfile.getEnrollmentAccountNumber()).thenReturn(UNODOSTRES);
        when(customerProfile.getEnrollmentAccountSubtype()).thenReturn(IDA);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);

        when(customerProfile.getProfileType()).thenReturn(profileType);
        sessionInitializationController.setMaxStatements(Integer.MAX_VALUE);
        when(customerProfile.getEntitlementByType(EntitlementType.BANKING))
                .thenReturn(new CustomerEntitlement(EntitlementType.BANKING, EntitlementStatus.ENABLED));
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        httpServletRequest.addHeader("user-agent",
                "Mozilla/5.0 (Linux; U; Android 2.3.1; en-us; MID Build/GINGERBREAD) AppleWebKit/533.1 (KHTML, like Gecko) "
                        + "Version/4.0 Mobile Safari/533.1. Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) "
                        + "Chrome/44.0.2403.157 Safari/537.36.");
        when(customerService.completeLoginProcess(any(CompleteLoginProcessRequest.class)))
                .thenReturn(new ModifyCustomerProfileResponse());

    }

    @Test
    public void whenHandleRequest_GivenCustomerProfile_Then_SetCustomerProfileIsCalled() throws Exception {

        userSession.setCustomerService(customerService);
        CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
        cisResponse.setCustomerCodes(getCustomerCodesList());

        when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
        when(customerProfile.getPermId()).thenReturn(PREMIAPERMID);
        when(customerService.getCISCustomerCode(any())).thenReturn(cisResponse);
        when(customerService.getPremiaCustomerCodes()).thenReturn(getPremiaCustomerCodesList());
        when(userSession.getCustomerService()).thenReturn(customerService);

        //Force the needed Feature flags
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(Boolean.FALSE);
        when(Utils.enrollmentAccountExists(any(), any())).thenReturn(Boolean.TRUE);

        //Force redirect reward points. Param is pushed by MBCA-2019
        when(userSession.isRewardPoints()).thenReturn(true);
        when(userSession.getPremiaAccounts()).thenReturn(getPremiaAccounts());
        when(customerService.getPriorityPremiaAccount(any())).thenReturn(getPremiaAccounts().get(1));

        ModelAndView modelAndView = sessionInitializationController.handleRequestInternal(httpServletRequest, httpServletResponse);

        verify(userSession).setCustomerProfile(any());

    }


    /**
     * Get a list of dummy Customer codes.
     * @return
     */
    public static List<String> getCustomerCodesList() {

        return Stream.of("16", "11", "181", "0",
                "9").collect(Collectors.toList());

    }


    private List<TVFrontendAccount> getPremiaAccounts() {
        CardAccount cardAccount1 = new CardAccount();
        cardAccount1.setAccountID("4549000973559305");
        cardAccount1.setSubType("CCA");
        cardAccount1.setBankInformation(new BankInformation());
        CardAccount cardAccount2 = new CardAccount();
        cardAccount2.setAccountID("4549542114539357");
        cardAccount2.setSubType("CCA");
        cardAccount2.setBankInformation(new BankInformation());
        return Stream.of(new MockTVFrontendAccount(cardAccount1,"asdadfasdfasdfdsf"),
                new MockTVFrontendAccount(cardAccount2,"werqowierwqerqwer"))
                .collect(Collectors.toList());
    }

}
