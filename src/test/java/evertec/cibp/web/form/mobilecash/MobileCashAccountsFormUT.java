package evertec.cibp.web.form.mobilecash;

import com.evertec.cibp.api.client.RestClient;
import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import com.google.common.collect.Iterables;
import evertec.cibp.core.models.configuration.ConfParameters;
import evertec.cibp.core.models.customer.CustomerPhone;
import evertec.cibp.core.models.customer.devices.CustomerDevice;
import evertec.cibp.core.models.enrollment.EnrollmentLiteStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.FingerprintRegistrationResponse;
import evertec.cibp.core.utils.MicroServiceUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.PushTokensFacade;
import evertec.cibp.web.response.MobileCashAccountResponse;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.validator.GenericValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.Errors;
import org.springframework.web.util.WebUtils;
import java.lang.reflect.Method;
import java.util.*;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RestClient.class, MicroServiceUtils.class, StaticMessageSource.class, WebUtils.class,
		PushTokensFacade.class, Iterables.class, Utils.class, UserSession.class, GenericValidator.class,
		FingerprintRegistrationResponse.class, ApiCallerFactory.class})
public class MobileCashAccountsFormUT extends AbstractModelAndViewTests {

	@Mock
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	private UserSession mockuserSession;
	
	@Mock
	private CustomerServices customerService;

	@Mock 
	private UserSession userSession;
	private FingerprintRegistrationResponse fpResponse ;


	private static final String FOR_MMOBILE_CASH_ACTION = "MOBILE_CASH";

	private static final String MAXIMUN_AMOUNT = "MOBILECASH_TRX_MAXIMUM_AMOUNT";

	private static final String CASHDROP_AMOUNT = "MOBILECASH_CASHDROP_TRX_MAXIMUM_AMOUNT";

	/**
	 * The rest client
	 */
	@Mock
	private RestClient restClient;

	@Mock
	private ApiCaller apiCaller;

	@InjectMocks
	private MobileCashAccountsForm controller;

	@Before
	public void setUp() throws Exception {

		PowerMockito.mockStatic(ApiCallerFactory.class);
		Mockito.when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);
		customerService = mock(CustomerServices.class);
		userSession = mock(UserSession.class);
		fpResponse = mock(FingerprintRegistrationResponse.class);
		mockuserSession = new UserSession();
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		PowerMockito.mockStatic(RestClient.class);
		PowerMockito.mockStatic(MicroServiceUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(UserSession.class);
		PowerMockito.mockStatic(GenericValidator.class);
		PowerMockito.mockStatic(FingerprintRegistrationResponse.class);
	}

	@Test
	public void whenReferenceData_GivenConfParameters_forme_forother_thenCheckValues_maxAmount_maxAmountCashDrop_noEmpty() throws Exception {
		//init mock
		List<MobileCashAccountResponse> cashAccountResponse =  new ArrayList<>();
		Object command = mock(Object.class);
		Errors errors = mock(Errors.class);
		String maxAmount = "300";
		String maxAmountCashDrop = "500";
		CustomerPhone phone =  new CustomerPhone();
		EnrollmentLiteStatus status = EnrollmentLiteStatus.FINGERPRINT_NOT_REGISTERED;
		List<CustomerDevice> empList = Collections.emptyList();
		ConfParameters confParameters_forme = mock(ConfParameters.class);
		confParameters_forme.setValue("220");
		ConfParameters confParameters_forother = mock(ConfParameters.class);
		confParameters_forother.setValue("300");

		PowerMockito.when(MicroServiceUtils.getRestClient()).thenReturn(restClient);
		// given userSession
		Mockito.when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerPhone()).thenReturn(phone);
		when(GenericValidator.isBlankOrNull("value")).thenReturn(false);
		when(userSession.getCustomerDevices()).thenReturn(empList);
		// when
		Method  getCustomerPhone = MobileCashAccountsForm.class.getDeclaredMethod("getCustomerPhone", UserSession.class);
		getCustomerPhone.setAccessible(true);

		//given FingerprintRegistrationResponse
		Whitebox.setInternalState(controller, "delayParameter", "0");
		PowerMockito.when(Utils.doesUserHasFingerprintRegistered(userSession, "0")).thenReturn(fpResponse);
		fpResponse.setStatus(status);
		PowerMockito.when(fpResponse.getStatus()).thenReturn(status);

		//when
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getConfParametersByShortNameAndApp(MAXIMUN_AMOUNT, FOR_MMOBILE_CASH_ACTION)).thenReturn(confParameters_forme);
		when(customerService.getConfParametersByShortNameAndApp(CASHDROP_AMOUNT, FOR_MMOBILE_CASH_ACTION)).thenReturn(confParameters_forother);
		when(confParameters_forme.getValue()).thenReturn(maxAmount);
		when(confParameters_forother.getValue()).thenReturn(maxAmountCashDrop);

		//call method
		Whitebox.invokeMethod(controller, "referenceData", request, command, errors);
		//asserts
		assertTrue(!maxAmount.isEmpty());
		assertTrue(!maxAmountCashDrop.isEmpty());
	}

}
