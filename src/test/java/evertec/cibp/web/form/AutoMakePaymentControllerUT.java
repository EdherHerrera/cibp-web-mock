package evertec.cibp.web.form;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.CCPortalPayments;
import evertec.cibp.web.response.AccountTv;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.MortgageAccountRecord;
import bppr.tv.beans.accounts.types.LoanAccount;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit testing AutoMakePaymentController java class.
 * @author Evertec ET58344
 * @since 09-15-2020
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, StaticMessageSource.class, AutoMakePaymentController.class, Utils.class,
CustomerFacade.class})
public class AutoMakePaymentControllerUT extends AbstractModelAndViewTests  {
	
	@Mock
	private MockHttpServletRequest request;
	
	@Mock
	private MockHttpServletResponse response;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private CCPortalPayments autoMakePayment;

	@Mock
	private BindException error;

	@Mock
	private CustomerFacade customerFacade;
	
	@Mock
	private TVFrontendAccount account;
	
	@Mock
	private LoanAccount loanAccount;
	
	@Mock
	private MortgageAccountRecord mlaRecord;
	
	@Mock
	private MessageSource messageSource;

	@Spy
	private AutoMakePaymentController controller = new AutoMakePaymentController();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		
		controller = PowerMockito.spy(new AutoMakePaymentController());
		
		controller.setMessageSource(messageSource);
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getTaxId()).thenReturn("123456789");
	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-15-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		PowerMockito.doReturn(new HashMap<String, String>()).when(controller, "getAccountFromPaymentMap",
				any(HttpServletRequest.class), any(UserSession.class), any(Boolean.class));
		when(autoMakePayment.getAccountId()).thenReturn("");
		when(autoMakePayment.getAccountInfo()).thenReturn(null);
		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		when(customerFacade.getGlobalEntitlement(any(UserSession.class), any())).thenReturn(null);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ACH_PAYMENT_NT)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBIM76)).thenReturn(Boolean.FALSE);

		Map<String, Object> result = controller.referenceData(request, autoMakePayment, error);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when OnSubmit
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-15-2020
	 * @exception Exception error
	 */
	@Test
	public void whenOnSubmit_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("cookieAnalyticEnabled", true);

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(autoMakePayment.getAccountId()).thenReturn("1234");
		when(autoMakePayment.getAccountInfo()).thenReturn(new TVFrontendAccount(new AccountTv())).thenReturn(null);
		PowerMockito.doReturn(new HashMap<String, String>()).when(controller, "getAccountFromPaymentMap",
				any(HttpServletRequest.class), any(UserSession.class), any(Boolean.class));
		when(customerProfile.hasEntitlement(EntitlementType.ALERTS)).thenReturn(Boolean.FALSE);
		when(request.getParameter(anyString())).thenReturn("abcde");
		PowerMockito.doReturn(mav).when((SimpleFormController)controller, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any());

		ModelAndView mavResult = controller.onSubmit(request, response, autoMakePayment, error);
		assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}
	
	@Test
	public void whenGetAmountDueInfo_givenAccountSubtypeMLA_thenAmountDueSet () throws Exception {
		
		when(account.getSubtype()).thenReturn("MLA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(mlaRecord);
		when(mlaRecord.getCurrentAmount()).thenReturn(new BigDecimal(1789.45));
		
		Whitebox.invokeMethod(controller, "getAmountDueInfo", autoMakePayment, account);
		
		verify(autoMakePayment, times(1)).setCurrentAmountDue(any());
	}
	
	@Test
	public void whenGetAmountDueInfo_givenAccountSubtypeLEA_thenAmountDueNotSet () throws Exception {
		
		when(account.getSubtype()).thenReturn("LEA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(mlaRecord);
		when(mlaRecord.getCurrentAmount()).thenReturn(new BigDecimal(1789.45));
		
		Whitebox.invokeMethod(controller, "getAmountDueInfo", autoMakePayment, account);
		
		verify(autoMakePayment, times(0)).setCurrentAmountDue(any());
	}
	
	@Test
	public void whenGetAmountDueInfo_givenBackendObjectNull_thenAmountDueNotSet () throws Exception {
		
		when(account.getSubtype()).thenReturn("MLA");
		when(account.getBackendObject()).thenReturn(null);		
		Whitebox.invokeMethod(controller, "getAmountDueInfo", autoMakePayment, account);
		verify(autoMakePayment, times(0)).setCurrentAmountDue(any());
	}
	
	@Test
	public void whenGetAmountDueInfo_givenMortgageAccountRecordNull_thenAmountDueNotSet () throws Exception {
		
		when(account.getSubtype()).thenReturn("MLA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(null);
		
		Whitebox.invokeMethod(controller, "getAmountDueInfo", autoMakePayment, account);
		
		verify(autoMakePayment, times(0)).setCurrentAmountDue(any());
	}
	
	@Test
	public void whenReferenceData_givenAccountSubtypeMLA_thenReturnMapWithAccountSubtype () throws Exception {
						
		when(customerService.getConfirmedPIFExternalAccounts(any(String.class))).thenReturn(null);
		
		when(Utils.getAccountById(any(), any())).thenReturn(account);
		when(account.getSubtype()).thenReturn("MLA");
		when(account.getNickname()).thenReturn("Prestamo");
		when(account.getAccountNumber()).thenReturn("12345678");	
		
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(mlaRecord);
		when(mlaRecord.getCurrentAmount()).thenReturn(new BigDecimal(1789.45));
		
        when(Utils.getRSALanguage(request)).thenReturn("en");
        when(messageSource.getMessage(any(), any(), any())).thenReturn("Message");
		
		Map<String, Object> model = controller.referenceData(request, autoMakePayment, null);
		
		assertTrue(model.containsKey("accountSubtype"));
	}
	
	@Test
	public void whenReferenceData_givenAccountNull_thenReturnMapWithoutAccountSubtype () throws Exception {
			
		when(customerService.getConfirmedPIFExternalAccounts(any(String.class))).thenReturn(null);
		
		when(Utils.getAccountById(any(), any())).thenReturn(null);
				
        when(Utils.getRSALanguage(request)).thenReturn("en");
        when(messageSource.getMessage(any(), any(), any())).thenReturn("Message");
		
		Map<String, Object> model = controller.referenceData(request, autoMakePayment, null);
		
		assertFalse(model.containsKey("accountSubtype"));
	}
	
	@Test
	public void whenOnSubmit_givenAccountSubtypeMLA_thenReturnMapWithAccountSubtype () throws Exception {
		
		when(customerService.getConfirmedPIFExternalAccounts(any(String.class))).thenReturn(null);
		
		when(Utils.getAccountById(any(), any())).thenReturn(account);
		when(account.getSubtype()).thenReturn("MLA");
		when(account.getNickname()).thenReturn("Prestamo");
		when(account.getAccountNumber()).thenReturn("12345678");	
		
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(mlaRecord);
		when(mlaRecord.getCurrentAmount()).thenReturn(new BigDecimal(1789.45));
		
        when(Utils.getRSALanguage(request)).thenReturn("en");
        when(messageSource.getMessage(any(), any(), any())).thenReturn("Message");
        
        when(request.getParameter("formAction")).thenReturn("pay");
               
        ModelAndView mav = new ModelAndView();
        
        when(error.getTarget()).thenReturn(autoMakePayment);
        
        
        
        PowerMockito.doReturn(mav).when((AutoMakePaymentController)controller, "showForm", 
        		any(), any(), any(), any());
        
       	
        ModelAndView modelAndView = controller.onSubmit(request, response, autoMakePayment, error);
		
		assertTrue(modelAndView.getModel().containsKey("accountSubtype"));
	}
	
	@Test
	public void whenOnSubmit_givenAccountNull_thenReturnMapWithoutAccountSubtype () throws Exception {
		
		when(customerService.getConfirmedPIFExternalAccounts(any(String.class))).thenReturn(null);
		
		when(Utils.getAccountById(any(), any())).thenReturn(null);
				
        when(Utils.getRSALanguage(request)).thenReturn("en");
        when(messageSource.getMessage(any(), any(), any())).thenReturn("Message");
        
        when(request.getParameter("formAction")).thenReturn("pay");
        
        ModelAndView mav = new ModelAndView();
        
        when(error.getTarget()).thenReturn(autoMakePayment);
        
        PowerMockito.doReturn(mav).when((AutoMakePaymentController)controller, "showForm", 
        		any(), any(), any(), any());
        
       	
        ModelAndView modelAndView = controller.onSubmit(request, response, autoMakePayment, error);
		
		assertFalse(modelAndView.getModel().containsKey("accountSubtype"));
	}

	@Test
	public void whenGetTelepagoErrorDescriptor_GivenMessageError_ThenReturnMessageErrorSucces() throws Exception {
		controller.setMessageSource(messageSource);
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("OK");

		String valueValidate = Whitebox.invokeMethod(controller, "getTelepagoErrorDescriptor", "ebpp.module", "ens","errorCode");
		assertEquals(valueValidate, "OK");
	}
}
