package evertec.cibp.web.form;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.session.GuidedSetupSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Unit testing GuidedSetupCampaignForm java class.
 * @author Evertec ET58344
 * @since 09-21-2020
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CustomerFacade.class, StaticMessageSource.class, GuidedSetupCampaignForm.class, Utils.class, ApiCallerFactory.class})
public class GuidedSetupCampaignFormUT extends AbstractModelAndViewTests  {

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private UserSession userSession;

	@Mock
	private GuidedSetupSession guidedSetupSession;

	@Mock
	private CustomerServices customerService;

	@Mock
	private ApiCaller apiCaller;

	@InjectMocks
	@Spy
	private final GuidedSetupCampaignForm controller = new GuidedSetupCampaignForm();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);
	}

	/**
	 * when GsEbills
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-21-2020
	 * @exception Exception error
	 */
	@Test
	public void whenGsEbills_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerSignupCampaigns()).thenReturn(new ArrayList<>());

		ModelAndView mavResult = controller.gsEbills(request, response);
		assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when GsSMS
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-21-2020
	 * @exception Exception error
	 */
	@Test
	public void whenGsSMS_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerSignupCampaigns()).thenReturn(new ArrayList<>());

		ModelAndView mavResult = controller.gsSMS(request, response);
		assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when GsPIF
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-21-2020
	 * @exception Exception error
	 */
	@Test
	public void whenGsPIF_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerSignupCampaigns()).thenReturn(new ArrayList<>());

		ModelAndView mavResult = controller.gsPIF(request, response);
		assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when GsPayments
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-21-2020
	 * @exception Exception error
	 */
	@Test
	public void whenGsPayments_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerSignupCampaigns()).thenReturn(new ArrayList<>());

		ModelAndView mavResult = controller.gsPayments(request, response);
		assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when GuidedSetupCampaign
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-21-2020
	 * @exception Exception error
	 */
	@Test
	public void whenGuidedSetupCampaign_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerSignupCampaigns()).thenReturn(new ArrayList<>());
		when(request.getParameter(anyString())).thenReturn("0");
		PowerMockito.doReturn("1").when(controller, "nextView", anyString(),
				any(Boolean.class), any(Boolean.class), any(Boolean.class));
		when(userSession.getGuidedSetupSession()).thenReturn(guidedSetupSession);
		when(guidedSetupSession.isHasPIFguidedSetup()).thenReturn(Boolean.TRUE);
		when(guidedSetupSession.isHasEBPPGuidedSetup()).thenReturn(Boolean.TRUE);
		when(guidedSetupSession.isHasPaymentsGuidedSetup()).thenReturn(Boolean.TRUE);
		when(guidedSetupSession.getIndividualguidedSetupCampaignId()).thenReturn("1234");
		when(userSession.getCustomerService()).thenReturn(customerService);
		PowerMockito.doNothing().when(customerService).campaignStatistics(anyString(), anyString(), any());

		ModelAndView mavResult = controller.guidedSetupCampaign(request, response);
		assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}
}
