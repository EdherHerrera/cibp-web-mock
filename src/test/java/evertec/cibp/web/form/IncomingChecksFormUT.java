/**
 * 
 */
package evertec.cibp.web.form;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.Errors;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.models.base.RunningBalanceTransaction;
import evertec.cibp.core.models.configuration.ConfParameters;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.fraud.prevention.IncomingChecks;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.FraudPreventionUtils;
import evertec.cibp.web.form.bean.IncomingCheksBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.HasSectionUtil;

/**
 * @author S683990 - Michael Ortiz
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({IncomingChecksForm.class, WebUtils.class, FraudPreventionUtils.class, HasSectionUtil.class, StringUtils.class})
public class IncomingChecksFormUT {
	
	private MockHttpServletRequest request;
	
	@Mock
	private Object command;
	
	@Mock
	private Errors errors;
	
	@Mock
	private UserSession userSession;
	
	@Mock
    private CustomerProfile customerProfile;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private HashMap<String, String> checkImageMap;
	
	@InjectMocks
	private IncomingChecksForm controller;

	static final String DUMMY_ACCOUNT_NUMBER_STRING  = "000000000";
	static final String DUMMY_PRODUCT_ID_STRING  = "081";
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
		controller = PowerMockito.spy(new IncomingChecksForm());

		controller.setCustomerServices(customerService);
		
		request = new MockHttpServletRequest();
		
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(FraudPreventionUtils.class);
        PowerMockito.mockStatic(HasSectionUtil.class);
        PowerMockito.mockStatic(StringUtils.class);

      
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCheckImageMap()).thenReturn(checkImageMap);
		when(controller.getBusinessAccounts()).thenReturn("051");
		
	}

	@Test
	public void whenReferenceData_GivenCheckImageTransactionThenReturnModelWithEncryptedCheckImageKeyAndSetImageKeyInSession() {
		
		Map<String, Object> modelMap = new HashMap<String, Object>();
		
		TVAccountTransferSource tvAccountTransferSource = mock(TVAccountTransferSource.class);
		RunningBalanceTransaction runningBalanceTransaction = mock(RunningBalanceTransaction.class);
		IncomingChecks incomingChecks = mock(IncomingChecks.class);
		ConfParameters confParameters = mock(ConfParameters.class);
		
		List<IncomingChecks> incomingChecksList = new ArrayList<IncomingChecks>();
		when(incomingChecks.getIssueDate()).thenReturn(mock(Date.class));
		incomingChecksList.add(incomingChecks);
		
		List<AbstractTransaction> abstractTransactionsList = new ArrayList<>();
		when(runningBalanceTransaction.getImageKey()).thenReturn("202012345678900000");
		when(runningBalanceTransaction.isHasImage()).thenReturn(true);
		abstractTransactionsList.add(runningBalanceTransaction);			

		Set<TVAccountTransferSource> tvAccountTransferSources = new HashSet<>();
		tvAccountTransferSources.add(tvAccountTransferSource);
		
		when(tvAccountTransferSource.getAccount()).thenReturn(PowerMockito.mock(TVFrontendAccount.class));
		when(tvAccountTransferSource.getAccount().getProductDefinition()).thenReturn(PowerMockito.mock(TVAccountProductDefinition.class));
		when(tvAccountTransferSource.getAccount().getAccountNumber()).thenReturn(DUMMY_ACCOUNT_NUMBER_STRING);

		when(tvAccountTransferSource.getAccount().getProductDefinition().getProductId()).thenReturn("051");
		
		when(userSession.getAccountsPaymentFrom()).thenReturn(tvAccountTransferSources);
		
		when(StringUtils.containsIgnoreCase(any(), any())).thenReturn(true);
		when(FraudPreventionUtils.getAccountsbyAppType(any())).thenReturn(Arrays.asList(DUMMY_ACCOUNT_NUMBER_STRING));

		when(customerService.getIncomingChecksByFilter(anyMap(), any(Integer.class))).thenReturn(incomingChecksList);
		
		when(confParameters.getValue()).thenReturn("00:00");
		when(customerService.getConfParametersByShortNameAndApp(any(), any())).thenReturn(confParameters);
		
		try {
		
			// Mock private method
	        PowerMockito.when(controller, "getTransactionsImages", 
	        		any(TVFrontendAccount.class), 
	        		any(UserSession.class), 
	        		any(HttpServletRequest.class)).thenReturn(abstractTransactionsList);

	        modelMap = controller.referenceData(request, command, errors);
		
		} catch (Exception e) {
			
			e.printStackTrace();
			fail();
		}
		
		@SuppressWarnings("unchecked")
		List<IncomingCheksBean> incomingCheksBeansList = KiuwanUtils.cast(List.class, modelMap.get("acconutList"));

		assertTrue(!GenericValidator.isBlankOrNull(incomingCheksBeansList.get(0).getFrontEndImageKey()));
		assertTrue(!GenericValidator.isBlankOrNull(incomingCheksBeansList.get(0).getImageKey()));
		assertTrue(incomingCheksBeansList.get(0).isHasImage());
		
		assertTrue(modelMap.get("hasChecks").equals(true));
		verify(userSession.getCheckImageMap(), times(1)).put(any(), any());
	}
	
	@Test
	public void whenReferenceData_GivenNullImageTransactionsThenReturnModelWitoutCheckImage() {
		
		Map<String, Object> modelMap = new HashMap<String, Object>();
		
		TVAccountTransferSource tvAccountTransferSource = mock(TVAccountTransferSource.class);
		ConfParameters confParameters = mock(ConfParameters.class);
		
		Set<TVAccountTransferSource> tvAccountTransferSources = new HashSet<>();
		tvAccountTransferSources.add(tvAccountTransferSource);
		
		when(tvAccountTransferSource.getAccount()).thenReturn(PowerMockito.mock(TVFrontendAccount.class));
		when(tvAccountTransferSource.getAccount().getProductDefinition()).thenReturn(PowerMockito.mock(TVAccountProductDefinition.class));
		when(tvAccountTransferSource.getAccount().getAccountNumber()).thenReturn(DUMMY_ACCOUNT_NUMBER_STRING);

		when(tvAccountTransferSource.getAccount().getProductDefinition().getProductId()).thenReturn("051");
		
		when(userSession.getAccountsPaymentFrom()).thenReturn(tvAccountTransferSources);
		
		when(StringUtils.containsIgnoreCase(any(), any())).thenReturn(true);
		when(FraudPreventionUtils.getAccountsbyAppType(any())).thenReturn(Arrays.asList(DUMMY_ACCOUNT_NUMBER_STRING));
		
		when(confParameters.getValue()).thenReturn("00:00");
		when(customerService.getConfParametersByShortNameAndApp(any(), any())).thenReturn(confParameters);
		
		try {

	        modelMap = controller.referenceData(request, command, errors);
		
		} catch (Exception e) {
			
			e.printStackTrace();
			fail();
		}
		
		@SuppressWarnings("unchecked")
		List<IncomingCheksBean> incomingCheksBeansList = KiuwanUtils.cast(List.class, modelMap.get("acconutList"));

		assertTrue(incomingCheksBeansList.isEmpty());
		
		assertTrue(modelMap.get("hasChecks").equals(false));
		verify(userSession.getCheckImageMap(), times(0)).put(any(), any());
	}
}
