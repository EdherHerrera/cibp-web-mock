package evertec.cibp.web.form;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.StopPaymentResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.AccountServiceBean;
import evertec.cibp.web.response.AccountTv;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Unit testing AccountServicesForm java class.
 * @author Evertec ET58344
 * @since 09-15-2020
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CustomerFacade.class, StaticMessageSource.class, AccountServicesForm.class, Utils.class,
Calendar.class, GregorianCalendar.class, RequestContextUtils.class, AccountServiceBean.class, ResultStatus.class})
public class AccountServicesFormUT extends AbstractModelAndViewTests  {

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private AccountServiceBean searchBean;

	@Mock
	private BindException error;

	@Mock
	private StopPaymentResponse chkres;

	@InjectMocks
	private AccountServicesForm controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(GregorianCalendar.class);
		PowerMockito.mockStatic(RequestContextUtils.class);
		PowerMockito.mockStatic(AccountServiceBean.class);
		PowerMockito.mockStatic(ResultStatus.class);
	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-15-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		Calendar calendar = Calendar.getInstance();

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getAccounts()).thenReturn(new ArrayList<>());
		when(request.getParameter(anyString())).thenReturn("");
		when(StaticMessageSource.isFlagEnabled(anyString())).thenReturn(Boolean.FALSE);
		when(Utils.getRSALanguage(any(HttpServletRequest.class))).thenReturn("en");
		when(RequestContextUtils.getLocale(any(HttpServletRequest.class))).thenReturn(new Locale("en"));
		when(Utils.hasUSaveAccount(any())).thenReturn(Boolean.FALSE);
		when(GregorianCalendar.getInstance()).thenReturn(calendar);

		Map<String, Object> result = controller.referenceData(request);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when OnSubmit
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-15-2020
	 * @exception Exception error
	 */
	@Test
	public void whenOnSubmit_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		Calendar calendar = Calendar.getInstance();

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(request.getParameter(anyString())).thenReturn("");
		when(searchBean.getAction()).thenReturn(AccountServiceBean.ACTION.STOPCHECK);
		when(Utils.getAccountById(any(), any())).thenReturn(new TVFrontendAccount(new AccountTv()));
		when(searchBean.getStopCheckNumber()).thenReturn("");
		when(Utils.generateCustomerInteractionEvent(any(HttpServletRequest.class))).thenReturn(new CustomerInteractionEvent());
		when(userSession.getBankingSession()).thenReturn(new BankingSession(new BigInteger("1"), "2"));
		when(customerService.requestStopPayment(any())).thenReturn(chkres);
		when(chkres.getStatus()).thenReturn(ResultStatus.SUCCESS);
		when(RequestContextUtils.getLocale(any(HttpServletRequest.class))).thenReturn(new Locale("en"));
		when(GregorianCalendar.getInstance()).thenReturn(calendar);

		ModelAndView mavResult = controller.onSubmit(request, response, searchBean, error);
		assertEquals(Boolean.TRUE, mavResult.getModel().get("cookieAnalyticEnabled"));
	}
}
