package evertec.cibp.web.form;


import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.transfer.TVAccountTransferTarget;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.PaymentBean;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, Utils.class, CustomerFacade.class , StaticMessageSource.class,
		PaymentsServiceModule.class, QuickPaymentForm.class})
public class QuickPaymentFormUT extends AbstractModelAndViewTests {
	private HttpServletRequest request;
	private HttpServletResponse response;
	
	@Mock
	private TVFrontEndPayee tvFrontEndPayee;
	@Mock
	private GlobalPayee globalPayee;
	
	@InjectMocks
	@Spy
	private final QuickPaymentForm quickPaymentForm = new QuickPaymentForm();
	
	private static final String PAYEE_DETAIL_FOR_PAYEE_METHOD = "payeeDetailForPayee";
	private final String rtNotificationKeyMap = "rtNotification";
	private final String rtHasPaymentHistoryKeyMap = "rtHasPaymentHistory";
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private ModelMap modelMap;

	@Mock
	private ModelAndView modelAndView;
	
	@Mock
	private Errors errors;
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock 
	private TVFrontEndPayee tvFrontPayee;
	
	@Mock
	private TVAccountTransferTarget tvTarget;
	
	@Mock
	private TVFrontendAccount tvFrontEndAccount;
	
	@Mock
	private PaymentBean paymentBean;
	
	@Mock
	private GlobalEntitlement globalEntitlement;

	@Mock
	private BindException error;
	
	@Mock
	private CustomerFacade customerFacade;

	@Mock
	private PaymentResponse paymentResponse;

	@Mock
	private TVFrontendAccount tvFrontendAccount;

	@Mock
	private CustomerInteractionEvent customerInteractionEvent;


	@Before
	public void setUp() throws Exception {
		
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(PaymentsServiceModule.class);
		
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(Utils.getRSALanguage(request)).thenReturn("en");
		List<PIFExternalAccount> pifAcct = new ArrayList<PIFExternalAccount>();
		PIFExternalAccount from = mock(PIFExternalAccount.class);
		pifAcct.add(from);
		when(customerService.getConfirmedPIFExternalAccounts(userSession.getCustomerProfile()
				.getTaxId())).thenReturn(pifAcct);
		
		PowerMockito.mockStatic(StaticMessageSource.class);
		
		when(tvFrontEndPayee.getGlobalPayee()).thenReturn(globalPayee);
	}

	private void setMocks() {
		List<Integer> list = new ArrayList<>();
		list.add(2);
		List<TVFrontEndPayee> tvFrontEndPayeeList = new ArrayList<>();
		tvFrontEndPayeeList.add(tvFrontEndPayee);
		List<TVFrontendAccount> accounts = new ArrayList<>();
		accounts.add(tvFrontendAccount);
		when(Utils.getUserSession(request)).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerInteractionEvent);
		when(PaymentsServiceModule.sendPayments(eq(customerProfile), eq(userSession), any(TVPayment.class),
				eq(customerInteractionEvent), eq(customerService))).thenReturn(paymentResponse);
		when(paymentBean.getPayeeid()).thenReturn("payeeId");
		when(userSession.getPayees(false)).thenReturn(tvFrontEndPayeeList);
		when(Utils.getPayeebyId("payeeId", tvFrontEndPayeeList)).thenReturn(tvFrontEndPayee);
		when(paymentBean.getAccountFrom()).thenReturn("from");
		when(tvFrontEndPayee.getCustomerPayeeCodes()).thenReturn(list);
		when(Utils.getAccountById("from", accounts)).thenReturn(tvFrontendAccount);
		when(Utils.getRSALanguage(request)).thenReturn("en");
		when(tvFrontEndPayee.getGlobalPayee()).thenReturn(globalPayee);
		when(globalPayee.getIdString()).thenReturn("Id");
	}


	@Test
	@Ignore("WITH ERROR")
	public void whenLoadData_GivenFromAccountsIsPIF_ThenCallDetailedAccountFromPIF() throws Exception {

		List<TVFrontEndPayee> payees = mock(List.class);
				
		when(Utils.getRSALanguage(request)).thenReturn("en");
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		userSession.setLoadebpp(false);
		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.BILLPAY)).thenReturn(globalEntitlement);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);

		List<PIFExternalAccount> pifAcct = new ArrayList<PIFExternalAccount>();
		PIFExternalAccount from = mock(PIFExternalAccount.class);
		pifAcct.add(from);
		when(customerService.getConfirmedPIFExternalAccounts(userSession.getCustomerProfile()
				.getTaxId())).thenReturn(pifAcct);
			
		TVAccountProductDefinition productDef = mock(TVAccountProductDefinition.class);
		
		when(tvFrontEndAccount.getProductDefinition()).thenReturn(productDef);
		when(productDef.getProductType()).thenReturn("43443434");
		
		when(tvFrontEndAccount.getSubtype()).thenReturn("IDA");
		when(tvFrontEndAccount.getPortalBalance()).thenReturn(new BigDecimal(384738));
		Map<String, String> map = new HashMap<>();
		map.put("accountName", "The name");
		map.put("accountLast4Num", "x1234");
		map.put("accountNumber", "123456789");
		map.put("accountSubtype", "PIF");
		map.put("accountSection", "");
		map.put("text", "The name x1234");
		
		when(Utils.detailedAccountFromPIF(from)).thenReturn(map);
		
		Map<String, Object> model = new HashMap<String, Object>();
		Whitebox.invokeMethod(quickPaymentForm, "loadData", request, paymentBean, model);
		
		///Do not Seperate these lines, if separated the static method will not run//
		verifyStatic(Utils.class, times(1));
		Utils.detailedAccountFromPIF(from);
		/////
	}
	
	@Test
	@Ignore("WITH ERROR")
	public void whenLoadData_GivenFromAccountsIsNotPIF_ThenDoNotCallDetailedAccountFromPIF() throws Exception {

		List<TVFrontEndPayee> payees = mock(List.class);
				
		when(Utils.getRSALanguage(request)).thenReturn("en");
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		userSession.setLoadebpp(false);
		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.BILLPAY)).thenReturn(globalEntitlement);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);

		List<PIFExternalAccount> pifAcct = new ArrayList<PIFExternalAccount>();
		PIFExternalAccount from = mock(PIFExternalAccount.class);
		pifAcct.add(from);
			
		
		when(tvFrontEndAccount.getSubtype()).thenReturn("IDA");
		when(tvFrontEndAccount.getPortalBalance()).thenReturn(new BigDecimal(384738));
		Map<String, String> map = new HashMap<>();
		map.put("accountName", "The name");
		map.put("accountLast4Num", "x1234");
		map.put("accountNumber", "123456789");
		map.put("accountSubtype", "PIF");
		map.put("accountSection", "");
		map.put("text", "The name x1234");
		
		when(Utils.detailedAccountFromPIF(from)).thenReturn(map);
		
		Map<String, Object> model = new HashMap<String, Object>();
		Whitebox.invokeMethod(quickPaymentForm, "loadData", request, paymentBean, model);
		
		///Do not Seperate these lines, if separated the static method will not run//
		verifyStatic(Utils.class, times(0));
		Utils.detailedAccountFromPIF(from);
		/////
	}
	
	/**
	 * when PayeeDetailForPayee
	 * given FeatureFlag Enabled and TVFrontEndPaye null
	 * then return NotificationValue on ModelAndView
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPayeeDetailForPayee_GivenFeatureFlagEnabled_AndTVFrontEndPayeeNull_ThenReturnEnabledNotificationValueOnModelAndView() 
			throws Exception {
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(true);
		
		Map<String, String> result = Whitebox.invokeMethod(quickPaymentForm, PAYEE_DETAIL_FOR_PAYEE_METHOD, tvFrontEndPayee, userSession);
		
		assertNotNull(result);
		assertTrue(result.containsKey(rtNotificationKeyMap));
	}
	
	/**
	 * when PayeeDetailForPayee
	 * given FeatureFlag Disabled and TVFrontEndPaye null
	 * then not return NotificationValue on ModelAndView
	 */
	@Test
	@Ignore
	public void whenPayeeDetailForPayee_GivenFeatureFlagDisabled_AndTVFrontEndPayeeNull_ThenNotReturnEnabledNotificationValueOnModelAndView() 
			throws Exception {
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(false);
		
		Map<String, String> result = Whitebox.invokeMethod(quickPaymentForm, PAYEE_DETAIL_FOR_PAYEE_METHOD, tvFrontEndPayee, userSession);
		
		assertNotNull(result);
		assertFalse(result.containsKey(rtNotificationKeyMap));
		
	}
	
	/**
	 * when PayeeDetailForPayee
	 * given FeatureFlag Enabled and TVFrontEndPaye not null
	 * then return NotificationValue on ModelAndView
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPayeeDetailForPayee_GivenFeatureFlagEnabled_AndTVFrontEndPayeeNotNull_ThenReturnEnabledNotificationValueOnModelAndView() 
			throws Exception {
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(true);
		
		Map<String, String> result = Whitebox.invokeMethod(quickPaymentForm, PAYEE_DETAIL_FOR_PAYEE_METHOD, tvFrontEndPayee, userSession);
		
		assertNotNull(result);
		assertTrue(result.containsKey(rtNotificationKeyMap));
	}
	
	/**
	 * when PayeeDetailForPayee
	 * given FeatureFlag Disabled and TVFrontEndPaye not null
	 * then not return NotificationValue on ModelAndView
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPayeeDetailForPayee_GivenFeatureFlagDisabled_AndTVFrontEndPayeeNotNull_ThenNotReturnEnabledNotificationValueOnModelAndView() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(false);
		
		Map<String, String> result = Whitebox.invokeMethod(quickPaymentForm, PAYEE_DETAIL_FOR_PAYEE_METHOD, tvFrontEndPayee, userSession);
		
		assertNotNull(result);
		assertFalse(result.containsKey(rtNotificationKeyMap));
		
	}
	
	/**
	 * when LoadData
	 * give Feature Flag Disabled
	 * Then Not Return Today Date Real Time Value On Model
	 * @throws Exception
	 */
	@Test
	@Ignore("WITH ERROR")
	public void whenLoadData_giveFeatureFlagDisabled_ThenNotReturnTodayDateRealTimeValueOnModel() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(false);
		
		Map<String, Object> model = new HashMap<String, Object>();
		Whitebox.invokeMethod(quickPaymentForm, "loadData", request, paymentBean, model);
		
		assertNotNull(model);
		assertFalse(model.containsKey("todayDateRealTime"));
	}
	
	/**
	 * when LoadData
	 * give Feature Flag Enabled
	 * Then Return Today Date Real Time Value On Model With Today Date
	 * @throws Exception
	 */
	@Test
	@Ignore("WITH ERROR")
	public void whenLoadData_giveFeatureFlagEnabled_ThenReturnTodayDateRealTimeValueOnModel() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(true);
		
		Map<String, Object> model = new HashMap<String, Object>();
		Whitebox.invokeMethod(quickPaymentForm, "loadData", request, paymentBean, model);
		
		assertNotNull(model);
		assertTrue(model.containsKey("todayDateRealTime"));
		
		Calendar today=Calendar.getInstance();
		SimpleDateFormat todayformat = new SimpleDateFormat("MM/dd/yyyy");
		assertTrue(model.get("todayDateRealTime").equals(todayformat.format(today.getTime())));

	}
	//// MBC 2427
	
	/**
	 * when PayeeDetailForPayee
	 * given FeatureFlag Disabled 
	 * then return not exist key rtHasPaymentHistory on ModelAndView
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPayeeDetailForPayee_GivenFeatureFlagDisabled_ThenNotReturHasPaymentHistoryKeyOnModelAndView() 
			throws Exception {
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(false);
		
		Map<String, String> result = Whitebox.invokeMethod(quickPaymentForm, PAYEE_DETAIL_FOR_PAYEE_METHOD, tvFrontEndPayee, userSession);
		
		assertNotNull(result);
		assertFalse(result.containsKey(rtHasPaymentHistoryKeyMap));
	}
	
	/**
	 * when PayeeDetailForPayee
	 * given FeatureFlag Enabled and Not Contains Payments
	 * then return False rtHasPaymentHistory on ModelAndView
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPayeeDetailForPayee_GivenFeatureFlagEnabled_AndNotContainsPayments_ThenReturFalseHasPaymentHistoryOnModelAndView() 
			throws Exception {
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(true);
		when(Utils.containsPayments(any(), any())).thenReturn(false);
		
		Map<String, String> result = Whitebox.invokeMethod(quickPaymentForm, PAYEE_DETAIL_FOR_PAYEE_METHOD, tvFrontEndPayee, userSession);
		
		assertNotNull(result);
		assertTrue(result.containsKey(rtHasPaymentHistoryKeyMap));
		assertTrue(result.get(rtHasPaymentHistoryKeyMap).equals("false"));
	}
	
	/**
	 * when PayeeDetailForPayee
	 * given FeatureFlag Enabled and Not Contains Payments
	 * then return False rtHasPaymentHistory on ModelAndView
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPayeeDetailForPayee_GivenFeatureFlagEnabled_AndContainsPayments_ThenReturFalseHasPaymentHistoryOnModelAndView() 
			throws Exception {
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(true);
		when(Utils.containsPayments(any(), any())).thenReturn(true);
		
		Map<String, String> result = Whitebox.invokeMethod(quickPaymentForm, PAYEE_DETAIL_FOR_PAYEE_METHOD, tvFrontEndPayee, userSession);
		
		assertNotNull(result);
		assertTrue(result.containsKey(rtHasPaymentHistoryKeyMap));
		assertTrue(result.get(rtHasPaymentHistoryKeyMap).equals("true"));
	}
	
	/**
	 * when PayeeDetailForPayee
	 * given 'Y' RtIndicator And RtNotification
	 * then return 'Y' rtNotificationKeyMap on ModelAndView
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPayeeDetailForPayee_GivenTrueRtIndicatorAndRtNotification_ThenReturFalseRealTimePayeeOnModelAndView() 
			throws Exception {
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(true);
		when(Utils.containsPayments(any(), any())).thenReturn(true);
		when(tvFrontEndPayee.getGlobalPayee()).thenReturn(globalPayee);
		when(globalPayee.getRtIndicator()).thenReturn("Y");
		when(globalPayee.getRtNotification()).thenReturn("Y");
		
		Map<String, String> result = Whitebox.invokeMethod(quickPaymentForm, PAYEE_DETAIL_FOR_PAYEE_METHOD, tvFrontEndPayee, userSession);
		
		assertNotNull(result);
		assertTrue(result.containsKey(rtNotificationKeyMap));
		assertTrue(result.get(rtNotificationKeyMap).equalsIgnoreCase("Y"));
	}
	
	/**
	 * when PayeeDetailForPayee
	 * given 'Y' RtIndicator And 'N' RtNotification
	 * then return 'N' rtNotificationKeyMap on ModelAndView
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPayeeDetailForPayee_GivenTrueRtIndicatorAndFalseRtNotification_ThenReturFalseRealTimePayeeOnModelAndView() 
			throws Exception {
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(true);
		when(Utils.containsPayments(any(), any())).thenReturn(true);
		when(tvFrontEndPayee.getGlobalPayee()).thenReturn(globalPayee);
		when(globalPayee.getRtIndicator()).thenReturn("Y");
		when(globalPayee.getRtNotification()).thenReturn("N");
		
		Map<String, String> result = Whitebox.invokeMethod(quickPaymentForm, PAYEE_DETAIL_FOR_PAYEE_METHOD, tvFrontEndPayee, userSession);
		
		assertNotNull(result);
		assertTrue(result.containsKey(rtNotificationKeyMap));
		assertTrue(result.get(rtNotificationKeyMap).equalsIgnoreCase("N"));
	}
	
	/**
	 * when PayeeDetailForPayee
	 * given 'N' RtIndicator And 'Y' RtNotification
	 * then return 'N' rtNotificationKeyMap on ModelAndView
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPayeeDetailForPayee_GivenFalseRtIndicatorAndTrueRtNotification_ThenReturFalseRealTimePayeeOnModelAndView() 
			throws Exception {
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(true);
		when(Utils.containsPayments(any(), any())).thenReturn(true);
		when(globalPayee.getRtIndicator()).thenReturn("N");
		when(globalPayee.getRtNotification()).thenReturn("Y");
		
		Map<String, String> result = Whitebox.invokeMethod(quickPaymentForm, PAYEE_DETAIL_FOR_PAYEE_METHOD, tvFrontEndPayee, userSession);
		
		assertNotNull(result);
		assertTrue(result.containsKey(rtNotificationKeyMap));
		assertTrue(result.get(rtNotificationKeyMap).equalsIgnoreCase("N"));
	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-24-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getFavoritePayments()).thenReturn(new ArrayList<>());
		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		when(customerFacade.getGlobalEntitlement(any(UserSession.class), any())).thenReturn(null);

		Map<String, Object> result = quickPaymentForm.referenceData(request, paymentBean, errors, 0);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when ProcessFinish
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-24-2020
	 * @exception Exception error
	 */
	@Test
	public void whenProcessFinish_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(Utils.getUserSession(any(HttpServletRequest.class))).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerService()).thenReturn(customerService);
		paymentBean = null;
		when(customerProfile.hasEntitlement(EntitlementType.ALERTS)).thenReturn(Boolean.FALSE);
		PowerMockito.doNothing().when(quickPaymentForm, "loadData", any(HttpServletRequest.class),
				any(), any());

		ModelAndView result = quickPaymentForm.processFinish(request, response, paymentBean, error);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}

	@Test
	public void when_ProcessFinish_GivenResponseIsSuccess_ThenMobileCashAccountIsnull() throws Exception {

		request = new MockHttpServletRequest();
		when(paymentResponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
		setMocks();

		quickPaymentForm.processFinish(request, response, paymentBean, error);
		verify(userSession).setMobileCashAccounts(null);
	}

	@Test
	public void when_ProcessFinish_GivenResponseIsNotSuccess_ThenMobileCashAccountIsNotSetNull() throws Exception {

		request = new MockHttpServletRequest();
		when(paymentResponse.getStatus()).thenReturn(ResultStatus.ERROR);
		when(paymentResponse.getErrorEBA()).thenReturn("ERROR_EBA");
		when(Utils.getRSALanguage(request)).thenReturn("en");
		setMocks();

		quickPaymentForm.processFinish(request, response, paymentBean, error);
		verify(userSession, never()).setMobileCashAccounts(null);
	}

}
