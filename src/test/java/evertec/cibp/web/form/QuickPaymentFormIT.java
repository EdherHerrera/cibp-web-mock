package evertec.cibp.web.form;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.transfer.TVAccountTransferTarget;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.PaymentBean;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.ModelAndViewAssert.assertModelAttributeValue;
import static org.springframework.test.web.ModelAndViewAssert.assertViewName;

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, Utils.class, CustomerFacade.class , StaticMessageSource.class,
        PaymentsServiceModule.class, QuickPaymentForm.class})
public class QuickPaymentFormIT {

    private HttpServletRequest request;
    private HttpServletResponse response;

    @Mock
    private TVFrontEndPayee tvFrontEndPayeeMock;

    @Mock
    private GlobalPayee globalPayee;

    @InjectMocks
    @Spy
    private final QuickPaymentForm quickPaymentForm = new QuickPaymentForm();

    @Mock
    private CustomerServices customerService;

    @Mock
    private UserSession userSession;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private PaymentBean paymentBean;

    @Mock
    private BindException error;

    @Mock
    private PaymentResponse paymentResponse;

    @Mock
    private TVFrontendAccount tvFrontendAccount;

    @Mock
    private CustomerInteractionEvent customerInteractionEvent;

    @Before
    public void setUp() throws Exception {

        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(CustomerFacade.class);
        PowerMockito.mockStatic(PaymentsServiceModule.class);

        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(Utils.getRSALanguage(request)).thenReturn("en");

        List<PIFExternalAccount> pifAcct = new ArrayList<PIFExternalAccount>();
        PIFExternalAccount from = mock(PIFExternalAccount.class);
        pifAcct.add(from);
        when(customerService.getConfirmedPIFExternalAccounts(userSession.getCustomerProfile()
                .getTaxId())).thenReturn(pifAcct);

        PowerMockito.mockStatic(StaticMessageSource.class);

        when(tvFrontEndPayeeMock.getGlobalPayee()).thenReturn(globalPayee);
    }

    @Test
    public void whenprocessFinish_GivenPaymentsNotNull_ThenReturnPaymentEstimatedDate() throws Exception {
        List<Integer> list = new ArrayList<>();
        list.add(2);
        List<TVFrontEndPayee> tvFrontEndPayeeList = new ArrayList<>();
        tvFrontEndPayeeList.add(tvFrontEndPayeeMock);
        when(Utils.getUserSession(request)).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(paymentBean.getAccountFrom()).thenReturn("from");
        when(paymentBean.getPayeeid()).thenReturn("payeeId");
        when(Utils.getPayeebyId(anyString(),anyList())).thenReturn(tvFrontEndPayeeMock);
        when(Utils.getAccountById(anyString(), anyList())).thenReturn(tvFrontendAccount);
        when(tvFrontEndPayeeMock.getCustomerPayeeCodes()).thenReturn(list);
        when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerInteractionEvent);
        when(PaymentsServiceModule.sendPayments(eq(customerProfile), eq(userSession), any(TVPayment.class),
                eq(customerInteractionEvent), eq(customerService))).thenReturn(paymentResponse);
        when(paymentResponse.getStatus()).thenReturn(ResultStatus.FOUND);
        when(Utils.getRSALanguage(request)).thenReturn("en");

        ModelAndView model = quickPaymentForm.processFinish(request, response, paymentBean, error);

        assertViewName(model, "quickpayment");
        assertModelAttributeValue(model, "page", "2");

}
}
