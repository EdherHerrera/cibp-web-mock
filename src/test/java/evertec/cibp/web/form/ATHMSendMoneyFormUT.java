package evertec.cibp.web.form;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.ATHMovilEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ATHMSendBean;
import evertec.cibp.web.session.UserSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit testing ATHMSendMoneyForm java class.
 * @author Evertec ET58344
 * @since 09-11-2020
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CustomerFacade.class, StaticMessageSource.class, ATHMSendMoneyForm.class, ApiCallerFactory.class})
public class ATHMSendMoneyFormUT extends AbstractModelAndViewTests  {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private CustomerFacade customerFacade;

	@Mock
	private HttpServletRequest requestValue;

	@Mock
	private ATHMSendBean athmSendBean;

	@Mock
	private GlobalEntitlement entitlement;

	@Mock
	private HashMap<String, Object> hashMap;

	@Mock
	private Errors errors;

	@Mock
	private CustomerEntitlement customerEntitlement;

	@Mock
	private ATHMovilEntitlement athmEntitlement;

	@Spy
	ATHMSendMoneyForm spyAthmSendMoneyForm = new ATHMSendMoneyForm();

	@Mock
	private ApiCaller apiCaller;

	@InjectMocks
	private ATHMSendMoneyForm controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		controller = mock(ATHMSendMoneyForm.class);

		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-11-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		int page = 0;
		String valueStr = "";
		spyAthmSendMoneyForm.setCustomerService(customerService);

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(customerService.getGlobalEntitlement(any(EntitlementType.class))).thenReturn(entitlement);
		when(athmSendBean.getPortal()).thenReturn(null);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getEntitlementByType(any(EntitlementType.class))).thenReturn(customerEntitlement);
		PowerMockito.whenNew(ATHMovilEntitlement.class).withArguments(customerEntitlement).
				thenReturn(athmEntitlement);
		when(StaticMessageSource.isFlagEnabled(anyString())).thenReturn(Boolean.FALSE);
		when(entitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);
		when(athmEntitlement.getPlasticAccount()).thenReturn(valueStr);
		PowerMockito.doNothing().when(spyAthmSendMoneyForm, "loadAccountData",
				any(), any(), any());
		when(athmSendBean.getFriend()).thenReturn(null);
		when(athmSendBean.isMobileApp()).thenReturn(Boolean.FALSE);
		PowerMockito.doNothing().when(spyAthmSendMoneyForm, "loadFriendsData",
				any(), any(), any());

		Map<String, Object> result = spyAthmSendMoneyForm.referenceData(requestValue, athmSendBean, errors, page);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when ProceessCancelFinishResult
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-11-2020
	 * @exception Exception error
	 */
	@Test
	public void whenProceessCancelFinishResult_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);

		ModelAndView modelAndView = Whitebox.invokeMethod(controller, "proceessCancelFinishResult", request);
		assertEquals(Boolean.TRUE, modelAndView.getModel().get("cookieAnalyticEnabled"));
	}
}
