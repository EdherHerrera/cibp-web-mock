package evertec.cibp.web.form;

import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.core.enums.TsysAlertStatus;
import evertec.cibp.core.facades.TsysAlertFacade;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.core.utils.alerts.AlertCCAUtils;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.form.bean.PurchasesAndWithdrawalsBean;
import evertec.cibp.web.form.bean.SmsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AlertTypesUtils;
import evertec.cibp.web.utils.ChallengeInformationUtils;
import evertec.cibp.web.utils.Utils;
import evertec.creditcards.exception.TsysException;
import evertec.creditcards.model.CustomerIdentifiers;
import evertec.creditcards.model.Phone;
import evertec.creditcards.model.Program;
import evertec.creditcards.model.alerts.AlertCustomerIdentifiers;
import evertec.creditcards.model.alerts.AlertDetails;
import evertec.creditcards.model.alerts.UserNotificationPreference;
import evertec.creditcards.model.alerts.UserNotificationPreferences;
import evertec.creditcards.request.alerts.TsysConsumerEssentialsPhoneNumbersRequest;
import evertec.creditcards.request.alerts.TsysAlertPreferenceCreateUpdateRequest;
import evertec.creditcards.response.TsysConsumerEssentialsPhoneNumbersResponse;
import evertec.creditcards.response.alerts.TsysAlertPreferencesResponse;
import evertec.creditcards.response.alerts.preferences.TsysAlertPreferenceCreateUpdateResponse;
import evertec.creditcards.response.alerts.preferences.contacts.TsysAlertContactInformationResponse;
import evertec.creditcards.response.alerts.preferences.contacts.TsysRetrieveAlertContactInformationResponse;
import org.apache.commons.validator.GenericValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

/**
 * @author mdias
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ WebUtils.class, Utils.class, StaticMessageSource.class, ESAPI.class, GenericValidator.class,
		CcaPurchasesAndWithdrawalsForm.class, TsysAlertFacade.class, AlertTypesUtils.class, ApiCallerFactory.class, AlertCCAUtils.class })
public class CcaPurchasesAndWithdrawalsFormUT {

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private CustomerServices customerService;

	@Mock
	private DeviceRequest deviceRequest;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private BindException errors;

	@Mock
	private HTTPUtilities httpUtilities;

	@Mock
	private TVFrontendAccount tvFrontendAccount;

	@Mock
	private BankingSession bankingSession;

	private PurchasesAndWithdrawalsBean bean;
	private Map<Object, Object> accountsMap;
	private List<TVFrontendAccount> ccaAccounts;
	private CardPlasticFrontend cardPlasticFrontend;
	private TsysAlertPreferencesResponse alertPreferencesResponse;
	private ArrayList<UserNotificationPreferences> notificationPreferencesArrayList;
	private ArrayList<AlertDetails> alertDetailsArrayList;
	private AlertDetails alertDetails;
	private UserNotificationPreferences notificationPreferences;
	private Map<String, CardPlasticFrontend> preferences;
	private CustomerIdentifiers customerIdentifiers;
	private CardPlasticFrontend[] ccaPlastics;
	private Map<String, Object> model;

	@Mock
	private TsysAlertFacade tsysAlertFacade;

	@Mock
	private ApiCaller apiCaller;

	@InjectMocks
	@Spy
	private CcaPurchasesAndWithdrawalsForm ccaPurchasesAndWithdrawalsForm = new CcaPurchasesAndWithdrawalsForm("0.01",
			"454925,454903", "ENABLED", "ENABLED", "6");

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(ESAPI.class);
		PowerMockito.mockStatic(TsysAlertFacade.class);
		PowerMockito.mockStatic(AlertTypesUtils.class);
		PowerMockito.mockStatic(ApiCallerFactory.class);
		PowerMockito.mockStatic(AlertCCAUtils.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);
		tvFrontendAccount = mock(TVFrontendAccount.class);

		bean = new PurchasesAndWithdrawalsBean();
		accountsMap = new LinkedHashMap<>();
		ccaAccounts = new ArrayList<>();
		cardPlasticFrontend = new CardPlasticFrontend();
		alertPreferencesResponse = new TsysAlertPreferencesResponse();
		notificationPreferencesArrayList = new ArrayList<>();
		notificationPreferences = new UserNotificationPreferences();
		alertDetailsArrayList = new ArrayList<>();
		alertDetails = new AlertDetails();
		preferences = new LinkedHashMap<>();
		customerIdentifiers = new CustomerIdentifiers();
		ccaPlastics = new CardPlasticFrontend[0];
		model = new HashMap<>();

		when(Utils.getRSALanguage(any(HttpServletRequest.class))).thenReturn("en");
	}

	@Test
	public void whenOnSubmit_GivenActionEqualsOne_ThenReturnModelMapAlertAmountAndPreferencesInSession() throws Exception {

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA627)).thenReturn(false);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)).thenReturn(false);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn("Test-Cookie");
		when(userSession.getOobEnroll()).thenReturn(false);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		PowerMockito.doReturn("4549542260934550").when(ccaPurchasesAndWithdrawalsForm,
				"getAthPlastic", anyString());
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsMinAlertQty()).thenReturn("0.01");
		bean.setAthplastic1("4549542260934550");
		bean.setAction("1");
		accountsMap.put("none", "Select a Credit Card");
		accountsMap.put("VISA BPPR", "x4550");
		accountsMap.put("MASTERCARD JET BLUE", "x5345");
		PowerMockito.doReturn(accountsMap).when(ccaPurchasesAndWithdrawalsForm,
				"getAccountsMap", any(HttpServletRequest.class));
		when(userSession.getCcaAccounts()).thenReturn(ccaAccounts);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);

		alertDetails.setName("thresholdAmount");
		alertDetails.setSelectedValue("50.0");
		alertDetailsArrayList.add(alertDetails);
		notificationPreferences.setActiveFlag("true");
		notificationPreferences.setAlert("PURCHASE_AMOUNT");
		notificationPreferences.setAlertDetails(alertDetailsArrayList);
		notificationPreferencesArrayList.add(notificationPreferences);
		alertPreferencesResponse.setUserNotificationPreferences(notificationPreferencesArrayList);
		cardPlasticFrontend.setTsysCcaAlertPreferences(alertPreferencesResponse);
		preferences.put("4549542260934550", cardPlasticFrontend);
		when(userSession.getTsysCCACustomerAlerts()).thenReturn(preferences);
		PowerMockito.doReturn(notificationPreferences).when(ccaPurchasesAndWithdrawalsForm,
				"getPurchaseAmountNotificationPreferences", any(ArrayList.class));
		PowerMockito.doReturn("50.0").when(ccaPurchasesAndWithdrawalsForm,
				"getAlertPurchaseAmount", any(UserNotificationPreferences.class));

		ModelAndView mav = ccaPurchasesAndWithdrawalsForm.onSubmit(request, response, bean, errors);
		assertEquals("50.0", mav.getModel().get("purchase_min_amt"));
	}

	@Test
	public void whenOnSubmit_GivenActionEqualsOne_ThenReturnModelMapAlertAmountAndPreferencesInSessionAlertsNull() throws Exception {

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA627)).thenReturn(false);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)).thenReturn(false);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn("Test-Cookie");
		when(userSession.getOobEnroll()).thenReturn(false);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		PowerMockito.doReturn("4549542260934550").when(ccaPurchasesAndWithdrawalsForm,
				"getAthPlastic", anyString());
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsMinAlertQty()).thenReturn("0.01");
		bean.setAthplastic1("4549542260934550");
		bean.setAction("1");
		accountsMap.put("none", "Select a Credit Card");
		accountsMap.put("VISA BPPR", "x4550");
		accountsMap.put("MASTERCARD JET BLUE", "x5345");
		PowerMockito.doReturn(accountsMap).when(ccaPurchasesAndWithdrawalsForm,
				"getAccountsMap", any(HttpServletRequest.class));
		when(userSession.getCcaAccounts()).thenReturn(ccaAccounts);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		when(Utils.getCustomerIdentifiersByCardLast4(anyString())).thenReturn(customerIdentifiers);
		when(userSession.getTsysCCACustomerAlerts()).thenReturn(null);
		PowerMockito.doNothing().when(ccaPurchasesAndWithdrawalsForm,
				"inquiryTsysAlertPreferences", any(UserSession.class),
				any(PurchasesAndWithdrawalsBean.class), anyMap());

		ModelAndView mav = ccaPurchasesAndWithdrawalsForm.onSubmit(request, response, bean, errors);
		assertNull(mav.getModel().get("purchase_min_amt"));
	}

	@Test
	public void whenInquiryTsysAlertPreferences_GivenAlertPreferencesResponseOK_ThenReturnResponseNotNull() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		notificationPreferences.setActiveFlag("true");
		notificationPreferencesArrayList.add(notificationPreferences);
		alertPreferencesResponse.setUserNotificationPreferences(notificationPreferencesArrayList);
		bean.setAthplastic1("4549542260934550");
		bean.setCcaAccount("4549542260934550");
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);

		when(tsysAlertFacade.tsysAlertPreferenceByAccountCustomerId(any(AlertCustomerIdentifiers.class)))
				.thenReturn(alertPreferencesResponse);

		PowerMockito.doReturn(notificationPreferences).when(ccaPurchasesAndWithdrawalsForm,
				"getPurchaseAmountNotificationPreferences", any(ArrayList.class));
		PowerMockito.doReturn("125.0").when(ccaPurchasesAndWithdrawalsForm,
				"getAlertPurchaseAmount", any(UserNotificationPreferences.class));
		when(userSession.getCcaAccounts()).thenReturn(ccaAccounts);
		when(Utils.getAccountByAccountNumber(anyString(), anyList())).thenReturn(tvFrontendAccount);
		when(Utils.getCCACardPlastics(any(CustomerServices.class), any(CustomerProfile.class),
				any(BankingSession.class), any(TVFrontendAccount.class))).thenReturn(ccaPlastics);
		PowerMockito.doNothing().when(ccaPurchasesAndWithdrawalsForm, "saveCCAPlasticAlertInSession",
				any(UserSession.class), any(Arrays.class), any(PurchasesAndWithdrawalsBean.class), any(TsysAlertPreferencesResponse.class));
		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "inquiryTsysAlertPreferences", userSession, bean, model);
		assertEquals("125.0", model.get("purchase_min_amt"));
	}

	@Test
	public void whenInquiryTsysAlertPreferences_GivenAlertPreferencesResponseOK_ThenReturnResponseNull() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		alertPreferencesResponse.setUserNotificationPreferences(notificationPreferencesArrayList);
		bean.setAthplastic1("4549542260934550");
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		PowerMockito.doReturn("125.0").when(ccaPurchasesAndWithdrawalsForm,
				"getAlertPurchaseAmount", any(UserNotificationPreferences.class));
		PowerMockito.doReturn(notificationPreferences).when(ccaPurchasesAndWithdrawalsForm,
				"getPurchaseAmountNotificationPreferences", any(ArrayList.class));
		PowerMockito.doNothing().when(ccaPurchasesAndWithdrawalsForm, "saveCCAPlasticAlertInSession",
				any(UserSession.class), any(Arrays.class), any(PurchasesAndWithdrawalsBean.class), any(TsysAlertPreferencesResponse.class));
		when(Utils.getAccountByAccountNumber(anyString(), anyList())).thenReturn(tvFrontendAccount);
		when(Utils.getCCACardPlastics(any(CustomerServices.class), any(CustomerProfile.class),
				any(BankingSession.class), any(TVFrontendAccount.class))).thenReturn(ccaPlastics);
		when(tsysAlertFacade.tsysAlertPreferenceByAccountCustomerId(any(AlertCustomerIdentifiers.class)))
				.thenReturn(null);

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "inquiryTsysAlertPreferences", userSession, bean, model);
		PowerMockito.verifyPrivate(ccaPurchasesAndWithdrawalsForm, times(1)).invoke("inquiryTsysAlertPreferences", any(UserSession.class),
				any(PurchasesAndWithdrawalsBean.class), anyMap());
	}

	@Test
	public void whenInquiryTsysAlertPreferences_GivenCustomerIdentifiersNull_ThenReturnModelError() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		bean.setAthplastic1("4549542260934550");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(null);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm,
				"inquiryTsysAlertPreferences", userSession, bean, model);
		PowerMockito.verifyPrivate(ccaPurchasesAndWithdrawalsForm, times(1)).invoke("inquiryTsysAlertPreferences", any(UserSession.class),
				any(PurchasesAndWithdrawalsBean.class), anyMap());
	}

	@Test
	public void whenInquiryTsysAlertPreferences_GivenAlertPreferencesException_ThenReturnErrorTrue() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getUsername()).thenReturn("galloo");
		alertPreferencesResponse.setUserNotificationPreferences(notificationPreferencesArrayList);
		bean.setAthplastic1("4549542260934550");
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		PowerMockito.doReturn("125.0").when(ccaPurchasesAndWithdrawalsForm,
				"getAlertPurchaseAmount", any(UserNotificationPreferences.class));
		PowerMockito.doReturn(notificationPreferences).when(ccaPurchasesAndWithdrawalsForm,
				"getPurchaseAmountNotificationPreferences", any(ArrayList.class));
		PowerMockito.doNothing().when(ccaPurchasesAndWithdrawalsForm, "saveCCAPlasticAlertInSession",
				any(UserSession.class), any(Arrays.class), any(PurchasesAndWithdrawalsBean.class), any(TsysAlertPreferencesResponse.class));
		when(Utils.getAccountByAccountNumber(anyString(), anyList())).thenReturn(tvFrontendAccount);
		when(Utils.getCCACardPlastics(any(CustomerServices.class), any(CustomerProfile.class),
				any(BankingSession.class), any(TVFrontendAccount.class))).thenReturn(ccaPlastics);
		PowerMockito.doThrow(new TsysException("An error occurred")).when(tsysAlertFacade).tsysAlertPreferenceByAccountCustomerId(any(AlertCustomerIdentifiers.class));

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "inquiryTsysAlertPreferences", userSession, bean, model);
		PowerMockito.verifyPrivate(ccaPurchasesAndWithdrawalsForm, times(1)).invoke("inquiryTsysAlertPreferences", any(UserSession.class),
				any(PurchasesAndWithdrawalsBean.class), anyMap());
	}

	@Test
	public void whenGetAlertPurchaseAmount_GivenAlertAmount_ThenReturnAmount() throws Exception {

		notificationPreferences.setAlert("PURCHASE_AMOUNT");
		notificationPreferences.setActiveFlag("true");
		alertDetails.setSelectedValue("55.5");
		alertDetails.setName("thresholdAmount");
		alertDetailsArrayList.add(alertDetails);
		notificationPreferences.setAlertDetails(alertDetailsArrayList);

		String resultAmount = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "getAlertPurchaseAmount",
				notificationPreferences);
		assertTrue("55.5".equalsIgnoreCase(resultAmount));
	}

	@Test
	public void whenGetAlertPurchaseAmount_GivenAlertAmountFlagEnabled_ThenReturnAmount() throws Exception {

		notificationPreferences.setAlert("PURCHASE_AMOUNT");
		notificationPreferences.setActiveFlag("true");
		alertDetails.setSelectedValue("55.5");
		alertDetails.setName("thresholdAmount");
		alertDetailsArrayList.add(alertDetails);
		notificationPreferences.setAlertDetails(alertDetailsArrayList);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA77)).thenReturn(true);

		String resultAmount = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "getAlertPurchaseAmount",
				notificationPreferences);
		assertTrue("55.50".equalsIgnoreCase(resultAmount));
	}

	@Test
	public void whenGetAlertPurchaseAmount_GivenAlertAmountZeroFlagEnabled_ThenReturnAmount() throws Exception {

		notificationPreferences.setAlert("DECLINED_TRANSACTION");
		notificationPreferences.setActiveFlag("true");
		alertDetails.setSelectedValue("55.5");
		alertDetails.setName("selectedValue");
		alertDetailsArrayList.add(alertDetails);
		notificationPreferences.setAlertDetails(alertDetailsArrayList);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA77)).thenReturn(true);

		String resultAmount = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "getAlertPurchaseAmount",
				notificationPreferences);
		assertTrue("0.00".equalsIgnoreCase(resultAmount));
	}

	@Test
	public void whenSaveCCAPlasticAlertInSession_GivenCcaPlastics_ThenReturnAlertInSession() throws Exception {

		bean.setAthplastic1("4549542260934550");
		CardPlasticAccountRecord accountRecord = new CardPlasticAccountRecord();
		accountRecord.setCardPANId("04549542260934550");
		cardPlasticFrontend = new CardPlasticFrontend(accountRecord);
		cardPlasticFrontend.setTsysCcaAlertPreferences(alertPreferencesResponse);
		ccaPlastics = new CardPlasticFrontend[1];
		ccaPlastics[0] = cardPlasticFrontend;
		PowerMockito.doReturn("04549542260934550").when(ccaPurchasesAndWithdrawalsForm,
				"getAthPlastic", anyString());
		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "saveCCAPlasticAlertInSession", userSession,
				ccaPlastics, bean, alertPreferencesResponse);
		assertNotNull(userSession.getTsysCCACustomerAlerts());
	}

	@Test
	public void whenGetPurchaseAmountNotificationPreferences_GivenNotificationListNotEmpty_ThenReturnNotificationPreference() throws Exception {

		notificationPreferences.setAlert("PURCHASE_AMOUNT");
		notificationPreferences.setActiveFlag("true");
		alertDetails.setName("thresholdAmount");
		alertDetails.setSelectedValue("45.0");
		alertDetailsArrayList.add(alertDetails);
		notificationPreferences.setAlertDetails(alertDetailsArrayList);
		notificationPreferencesArrayList.add(notificationPreferences);

		UserNotificationPreferences preferencesResult = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm,
				"getPurchaseAmountNotificationPreferences", notificationPreferencesArrayList);
		assertNotNull(preferencesResult);
	}

	@Test
	public void whenOnSubmit_GivenActionEqualsTwo_ThenReturnModelMapAlertAmountAndPreferencesInSession() throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("statusMessage", null);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)).thenReturn(false);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn("Test-Cookie");
		when(userSession.getOobEnroll()).thenReturn(false);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		PowerMockito.doReturn("4549542260934550").when(ccaPurchasesAndWithdrawalsForm,
				"getAthPlastic", anyString());
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsMinAlertQty()).thenReturn("0.01");
		bean.setAthplastic1("4549542260934550");
		bean.setAction("2");
		bean.setPurchase_thr_chksms("1");
		bean.setAlertsPurchaseminamt("");
		accountsMap.put("none", "Select a Credit Card");
		accountsMap.put("VISA BPPR", "x4550");
		accountsMap.put("MASTERCARD JET BLUE", "x5345");
		PowerMockito.doReturn(accountsMap).when(ccaPurchasesAndWithdrawalsForm,
				"getAccountsMap", any(HttpServletRequest.class));
		when(AlertTypesUtils.purchasesAndWithdrawalsErrors(request, model, bean)).thenReturn(false);

		TsysRetrieveAlertContactInformationResponse contactInformationResponse = new TsysRetrieveAlertContactInformationResponse();
		contactInformationResponse.setPrograms(new ArrayList<>());
		Program program3 = new Program();
		Phone phone = new Phone();
		phone.setType("ALT2");
		ArrayList<Phone> phones = new ArrayList<>();
		phones.add(phone);
		program3.setPhones(phones);
		program3.setSelectedPhones(phones);
		PowerMockito.doReturn(program3).when(ccaPurchasesAndWithdrawalsForm, "getAlertProgram3",
				any(ArrayList.class));
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);

		PowerMockito.doReturn(contactInformationResponse).when(ccaPurchasesAndWithdrawalsForm,
				"getTsysAlertContactInformation", any(UserSession.class),
				any(PurchasesAndWithdrawalsBean.class));
		PowerMockito.doReturn(mav).when((SimpleFormController) ccaPurchasesAndWithdrawalsForm, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any());

		ModelAndView modelAndView = ccaPurchasesAndWithdrawalsForm.onSubmit(request, response, bean, errors);
		assertNull(modelAndView.getModel().get("statusMessage"));
	}

	@Test
	public void whenOnSubmit_GivenActionEqualsTwoAlt2Null_ThenReturnModelMapAlertAmountAndPreferencesInSession() throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("statusMessage", null);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)).thenReturn(false);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn("Test-Cookie");
		when(userSession.getOobEnroll()).thenReturn(false);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		PowerMockito.doReturn("4549542260934550").when(ccaPurchasesAndWithdrawalsForm,
				"getAthPlastic", anyString());
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsMinAlertQty()).thenReturn("0.01");
		bean.setAthplastic1("4549542260934550");
		bean.setAction("2");
		bean.setPurchase_thr_chksms("1");
		bean.setAlertsPurchaseminamt("");
		accountsMap.put("none", "Select a Credit Card");
		accountsMap.put("VISA BPPR", "x4550");
		accountsMap.put("MASTERCARD JET BLUE", "x5345");
		PowerMockito.doReturn(accountsMap).when(ccaPurchasesAndWithdrawalsForm,
				"getAccountsMap", any(HttpServletRequest.class));
		when(AlertTypesUtils.purchasesAndWithdrawalsErrors(request, model, bean)).thenReturn(false);

		TsysRetrieveAlertContactInformationResponse contactInformationResponse = new TsysRetrieveAlertContactInformationResponse();
		contactInformationResponse.setPrograms(new ArrayList<>());
		Program program3 = new Program();
		Phone phone = new Phone();
		phone.setType("HOME");
		ArrayList<Phone> phones = new ArrayList<>();
		phones.add(phone);
		program3.setPhones(phones);
		PowerMockito.doReturn(program3).when(ccaPurchasesAndWithdrawalsForm, "getAlertProgram3",
				any(ArrayList.class));
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);

		PowerMockito.doNothing().when(ccaPurchasesAndWithdrawalsForm, "addPhoneNumberContactAndAlertPreference",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), anyMap());
		PowerMockito.doReturn(contactInformationResponse).when(ccaPurchasesAndWithdrawalsForm,
				"getTsysAlertContactInformation", any(UserSession.class),
				any(PurchasesAndWithdrawalsBean.class));
		PowerMockito.doReturn(mav).when((SimpleFormController)ccaPurchasesAndWithdrawalsForm, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any());

		ModelAndView modelAndView = ccaPurchasesAndWithdrawalsForm.onSubmit(request, response, bean, errors);
		assertNull(modelAndView.getModel().get("statusMessage"));
	}

	@Test
	public void whenOnSubmit_GivenActionEqualsTwoProgram3Null_ThenReturnModelMapAlertAmountAndPreferencesInSession() throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("statusMessage", "Unable to Update Profile.");

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)).thenReturn(false);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn("Test-Cookie");
		when(userSession.getOobEnroll()).thenReturn(false);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		PowerMockito.doReturn("4549542260934550").when(ccaPurchasesAndWithdrawalsForm,
				"getAthPlastic", anyString());
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsMinAlertQty()).thenReturn("0.01");
		bean.setAthplastic1("4549542260934550");
		bean.setAction("2");
		bean.setPurchase_thr_chksms("1");
		bean.setAlertsPurchaseminamt("");
		accountsMap.put("none", "Select a Credit Card");
		accountsMap.put("VISA BPPR", "x4550");
		accountsMap.put("MASTERCARD JET BLUE", "x5345");
		PowerMockito.doReturn(accountsMap).when(ccaPurchasesAndWithdrawalsForm,
				"getAccountsMap", any(HttpServletRequest.class));
		when(AlertTypesUtils.purchasesAndWithdrawalsErrors(request, model, bean)).thenReturn(false);

		TsysRetrieveAlertContactInformationResponse contactInformationResponse = new TsysRetrieveAlertContactInformationResponse();
		contactInformationResponse.setPrograms(new ArrayList<>());
		PowerMockito.doReturn(null).when(ccaPurchasesAndWithdrawalsForm, "getAlertProgram3",
				any(ArrayList.class));
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		when(AlertTypesUtils.getErrorStatusMessage(any(HttpServletRequest.class))).thenReturn("Unable to Update Profile.");

		PowerMockito.doNothing().when(ccaPurchasesAndWithdrawalsForm, "addPhoneNumberContactAndAlertPreference",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), anyMap());
		PowerMockito.doReturn(contactInformationResponse).when(ccaPurchasesAndWithdrawalsForm,
				"getTsysAlertContactInformation", any(UserSession.class),
				any(PurchasesAndWithdrawalsBean.class));
		PowerMockito.doReturn(mav).when((SimpleFormController)ccaPurchasesAndWithdrawalsForm, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any());

		ModelAndView modelAndView = ccaPurchasesAndWithdrawalsForm.onSubmit(request, response, bean, errors);
		assertNotNull(modelAndView.getModel().get("statusMessage"));
	}

	@Test
	public void whenOnSubmit_GivenActionEqualsTwoContactResponseNull_ThenReturnModelMapAlertAmountAndPreferencesInSession() throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("statusMessage", "Unable to Update Profile.");

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)).thenReturn(false);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn("Test-Cookie");
		when(userSession.getOobEnroll()).thenReturn(false);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		PowerMockito.doReturn("4549542260934550").when(ccaPurchasesAndWithdrawalsForm,
				"getAthPlastic", anyString());
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsMinAlertQty()).thenReturn("0.01");
		bean.setAthplastic1("4549542260934550");
		bean.setAction("2");
		bean.setPurchase_thr_chksms("1");
		bean.setAlertsPurchaseminamt("");
		accountsMap.put("none", "Select a Credit Card");
		accountsMap.put("VISA BPPR", "x4550");
		accountsMap.put("MASTERCARD JET BLUE", "x5345");
		PowerMockito.doReturn(accountsMap).when(ccaPurchasesAndWithdrawalsForm,
				"getAccountsMap", any(HttpServletRequest.class));
		when(AlertTypesUtils.purchasesAndWithdrawalsErrors(request, model, bean)).thenReturn(false);

		PowerMockito.doReturn(null).when(ccaPurchasesAndWithdrawalsForm, "getAlertProgram3",
				any(ArrayList.class));
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		when(AlertTypesUtils.getErrorStatusMessage(any(HttpServletRequest.class))).thenReturn("Unable to Update Profile.");

		PowerMockito.doNothing().when(ccaPurchasesAndWithdrawalsForm, "addPhoneNumberContactAndAlertPreference",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), anyMap());
		PowerMockito.doReturn(null).when(ccaPurchasesAndWithdrawalsForm,
				"getTsysAlertContactInformation", any(UserSession.class),
				any(PurchasesAndWithdrawalsBean.class));
		PowerMockito.doReturn(mav).when((SimpleFormController)ccaPurchasesAndWithdrawalsForm, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any());

		ModelAndView modelAndView = ccaPurchasesAndWithdrawalsForm.onSubmit(request, response, bean, errors);
		assertNotNull(modelAndView.getModel().get("statusMessage"));
	}

	@Test
	public void whenGetTsysAlertContactInformation_GivenCustomerIdentifiersNotNull_ThenReturnResponse() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		bean.setAthplastic1("4549542260934550");

		TsysRetrieveAlertContactInformationResponse contactInformationResponse = new TsysRetrieveAlertContactInformationResponse();
		when(tsysAlertFacade.tsysRetrieveAlertContactInformation(any(CustomerIdentifiers.class))).thenReturn(contactInformationResponse);

		TsysRetrieveAlertContactInformationResponse response = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "getTsysAlertContactInformation",
				userSession, bean);
		assertNotNull(response);
	}

	@Test
	public void whenGetTsysAlertContactInformation_GivenCustomerIdentifiersNotNull_ThenReturnTsysException() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		when(userSession.getUsername()).thenReturn("galloo");
		bean.setAthplastic1("4549542260934550");
		TsysException exception = new TsysException("Error occurred.");

		PowerMockito.doThrow(exception).when(tsysAlertFacade).tsysRetrieveAlertContactInformation(any(CustomerIdentifiers.class));

		TsysRetrieveAlertContactInformationResponse response = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "getTsysAlertContactInformation",
				userSession, bean);
		assertNull(response);
	}

	@Test
	public void whenGetAlertProgramByName_GivenProgramList_ThenReturnProgram() throws Exception {

		Program program3 = new Program();
		Phone phone = new Phone();
		phone.setType("ALT2");
		ArrayList<Phone> phones = new ArrayList<>();
		phones.add(phone);
		program3.setProgramName("PROGRAM_3");
		program3.setPhones(phones);
		ArrayList<Program> programs = new ArrayList<>();
		programs.add(program3);

		Program programResult = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "getAlertProgram3",
				programs);
		assertNotNull(programResult);
	}

	@Test
	public void whenSaveAlertPhoneContactTSYS_GivenSMSBeanNotNull_ThenReturnTrue() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		bean.setAthplastic1("4549542260934550");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		SmsBean smsBean = new SmsBean();
		smsBean.setPhone1("7875462514");
		TsysConsumerEssentialsPhoneNumbersResponse phoneNumbersResponse = new TsysConsumerEssentialsPhoneNumbersResponse();
		PowerMockito.doReturn(smsBean).when(ccaPurchasesAndWithdrawalsForm, "getSMSProfile", any(UserSession.class), any(HttpServletRequest.class));
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		when(tsysAlertFacade.tsysUpdateCustomerPhoneNumbersByIdentifiers(anyString(), anyString(), any(TsysConsumerEssentialsPhoneNumbersRequest.class)))
				.thenReturn(phoneNumbersResponse);

		boolean result = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "saveAlertPhoneContactTSYS", bean, userSession, request);
		assertTrue(result);
	}

	@Test
	public void whenSaveAlertPhoneContactTSYS_GivenSMSBeanNotNull_ThenReturnTsysException() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		bean.setAthplastic1("4549542260934550");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		SmsBean smsBean = new SmsBean();
		smsBean.setPhone1("7875462514");
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getUsername()).thenReturn("galloo");
		PowerMockito.doReturn(smsBean).when(ccaPurchasesAndWithdrawalsForm, "getSMSProfile", any(UserSession.class), any(HttpServletRequest.class));
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);

		TsysException exception = new TsysException("Error occurred.");
		PowerMockito.doThrow(exception).when(tsysAlertFacade).tsysUpdateCustomerPhoneNumbersByIdentifiers(anyString(), anyString(),
				any(TsysConsumerEssentialsPhoneNumbersRequest.class));

		boolean result = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "saveAlertPhoneContactTSYS", bean, userSession, request);
		assertFalse(result);
	}

	@Test
	public void whenSaveAlertContactInformationTSYS_GivenCustomerIdentifiersNotNull_ThenReturnTrue() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		SmsBean smsBean = new SmsBean();
		smsBean.setPhone1("7875462514");
		PowerMockito.doReturn(smsBean).when(ccaPurchasesAndWithdrawalsForm, "getSMSProfile", any(UserSession.class), any(HttpServletRequest.class));

		TsysAlertContactInformationResponse response = new TsysAlertContactInformationResponse();
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		when(tsysAlertFacade.tsysAlertContactInformationUpdate(any(CustomerIdentifiers.class), anyList()))
				.thenReturn(response);
		bean.setAthplastic1("4549542260934550");
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getUsername()).thenReturn("galloo");

		boolean result = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "saveAlertContactInformationTSYS",
				bean, userSession, request);
		assertTrue(result);
	}

	@Test
	public void whenSaveAlertContactInformationTSYS_GivenCustomerIdentifiersNotNull_ThenReturnTsysException() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		SmsBean smsBean = new SmsBean();
		smsBean.setPhone1("7875462514");
		PowerMockito.doReturn(smsBean).when(ccaPurchasesAndWithdrawalsForm, "getSMSProfile", any(UserSession.class), any(HttpServletRequest.class));
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getUsername()).thenReturn("galloo");
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		TsysException exception = new TsysException("Error occurred.");
		PowerMockito.doThrow(exception).when(tsysAlertFacade).tsysAlertContactInformationUpdate(any(CustomerIdentifiers.class),
				anyList());

		bean.setAthplastic1("4549542260934550");
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getUsername()).thenReturn("galloo");

		boolean result = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "saveAlertContactInformationTSYS",
				bean, userSession, request);
		assertFalse(result);
	}

	@Test
	public void whenAddPhoneNumberContactAndAlertPreference_GivenModelAndUserSession() throws Exception {

		Map<String, Object> model = new HashMap<>();
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveAlertPhoneContactTSYS", any(PurchasesAndWithdrawalsBean.class),
				any(UserSession.class), any(HttpServletRequest.class));
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveAlertContactInformationTSYS", any(PurchasesAndWithdrawalsBean.class),
				any(UserSession.class), any(HttpServletRequest.class));
		bean.setAlertsPurchaseminamt("125.5");
		when(AlertTypesUtils.getSuccessStatusMessage(any(HttpServletRequest.class))).thenReturn("Profile Updated");
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveUpdatePurchaseAmountAlertPreferencesTSYS",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), any(UserSession.class), anyString());

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "addPhoneNumberContactAndAlertPreference", request, bean, model);
		assertEquals("Profile Updated", model.get("statusMessage"));
	}

	@Test
	public void whenAddPhoneNumberContactAndAlertPreference_GivenModelAndUserSession_ThenModelError() throws Exception {

		Map<String, Object> model = new HashMap<>();
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		PowerMockito.doReturn(false).when(ccaPurchasesAndWithdrawalsForm, "saveAlertPhoneContactTSYS", any(PurchasesAndWithdrawalsBean.class),
				any(UserSession.class), any(HttpServletRequest.class));
		when(AlertTypesUtils.getErrorStatusMessage(any(HttpServletRequest.class))).thenReturn("Unable to Update Profile.");

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "addPhoneNumberContactAndAlertPreference", request, bean, model);
		assertNotNull(model.get("statusMessage"));
	}

	@Test
	public void whenAddPhoneNumberContactAndAlertPreference_GivenModelAndUserSession_ThenReturnErrorModel() throws Exception {

		Map<String, Object> model = new HashMap<>();
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveAlertPhoneContactTSYS", any(PurchasesAndWithdrawalsBean.class),
				any(UserSession.class), any(HttpServletRequest.class));
		PowerMockito.doReturn(false).when(ccaPurchasesAndWithdrawalsForm, "saveAlertContactInformationTSYS", any(PurchasesAndWithdrawalsBean.class),
				any(UserSession.class), any(HttpServletRequest.class));
		when(AlertTypesUtils.getErrorStatusMessage(any(HttpServletRequest.class))).thenReturn("Unable to Update Profile.");

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "addPhoneNumberContactAndAlertPreference", request, bean, model);
		assertNotNull(model.get("statusMessage"));
	}

	@Test
	public void whenOnSubmit_GivenActionEqualsTwoChkSmsZero_ThenReturnModelMapAlertAmountNull() throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("statusMessage", null);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)).thenReturn(false);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn("Test-Cookie");
		when(userSession.getOobEnroll()).thenReturn(false);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		PowerMockito.doReturn("4549542260934550").when(ccaPurchasesAndWithdrawalsForm,
				"getAthPlastic", anyString());
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsMinAlertQty()).thenReturn("0.01");
		bean.setAthplastic1("4549542260934550");
		bean.setAction("2");
		bean.setPurchase_thr_chksms("0");
		bean.setAlertsPurchaseminamt("");
		accountsMap.put("none", "Select a Credit Card");
		accountsMap.put("VISA BPPR", "x4550");
		accountsMap.put("MASTERCARD JET BLUE", "x5345");
		PowerMockito.doReturn(accountsMap).when(ccaPurchasesAndWithdrawalsForm,
				"getAccountsMap", any(HttpServletRequest.class));
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		when(AlertTypesUtils.purchasesAndWithdrawalsErrors(request, model, bean)).thenReturn(false);
		PowerMockito.doNothing().when(ccaPurchasesAndWithdrawalsForm, "removeAlertPreference", any(PurchasesAndWithdrawalsBean.class),
				any(HttpServletRequest.class), anyMap());
		PowerMockito.doReturn(mav).when((SimpleFormController) ccaPurchasesAndWithdrawalsForm, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any());

		ModelAndView modelAndView = ccaPurchasesAndWithdrawalsForm.onSubmit(request, response, bean, errors);
		assertNull(modelAndView.getModel().get("statusMessage"));
	}

	@Test
	public void whenOnSubmit_GivenActionEqualsTwoChkSmsZero_ThenReturnErrorsInModel() throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("statusMessage", null);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)).thenReturn(false);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn("Test-Cookie");
		when(userSession.getOobEnroll()).thenReturn(false);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		PowerMockito.doReturn("4549542260934550").when(ccaPurchasesAndWithdrawalsForm,
				"getAthPlastic", anyString());
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsMinAlertQty()).thenReturn("0.01");
		bean.setAthplastic1("4549542260934550");
		bean.setAction("2");
		bean.setPurchase_thr_chksms("0");
		bean.setAlertsPurchaseminamt("");
		accountsMap.put("none", "Select a Credit Card");
		accountsMap.put("VISA BPPR", "x4550");
		accountsMap.put("MASTERCARD JET BLUE", "x5345");
		PowerMockito.doReturn(accountsMap).when(ccaPurchasesAndWithdrawalsForm,
				"getAccountsMap", any(HttpServletRequest.class));
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		when(AlertTypesUtils.purchasesAndWithdrawalsErrors(any(HttpServletRequest.class), anyMap(), any(Object.class)))
				.thenReturn(true);
		PowerMockito.doReturn(mav).when((SimpleFormController) ccaPurchasesAndWithdrawalsForm, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any());

		ModelAndView modelAndView = ccaPurchasesAndWithdrawalsForm.onSubmit(request, response, bean, errors);
		assertNull(modelAndView.getModel().get("statusMessage"));
	}

	@Test
	public void whenSetSessionAttributes_GivenParametersCcaAction_ThenReturn() throws Exception {

		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ChallengeInformationUtils.CCA_ACTION, bean);

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "setSessionAttributes", request, bean, parameters);
		PowerMockito.verifyPrivate(ccaPurchasesAndWithdrawalsForm, times(1)).invoke(
				"setSessionAttributes", any(HttpServletRequest.class),
				any(Object.class), anyMap());
	}

	@Test
	public void whenGetAccountsMap_GivenEnLanguage_ThenReturnAccountsMap() throws Exception {

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsAddIsCCAFilterCodeComercial()).thenReturn("ENABLED");
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsAddIsCCAFilterCodeRetail()).thenReturn("ENABLED");
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsCreditCardFilterCode()).thenReturn("454925,454903");
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsCreditCardFilterNTIPCode()).thenReturn("6");
		when(AlertTypesUtils.getCCAAccountsListForAlertsFiltered(any(UserSession.class), anyString(), anyString(), anyString(), anyString()))
				.thenReturn(new ArrayList<>());
		when(Utils.getRSALanguage(any())).thenReturn("en");
		Map<Object, Object> accountsMap = new LinkedHashMap<>();
		PowerMockito.doReturn(accountsMap).when(Utils.class, "getCCAAccountsMap", anyList(), anyMap());

		Map<Object, Object> resultMap = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "getAccountsMap",
				any(HttpServletRequest.class));
		assertNotNull(resultMap);
	}

	@Test
	public void whenGetAccountsMap_GivenEsLanguage_ThenReturnAccountsMap() throws Exception {

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsAddIsCCAFilterCodeComercial()).thenReturn("ENABLED");
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsAddIsCCAFilterCodeRetail()).thenReturn("ENABLED");
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsCreditCardFilterCode()).thenReturn("454925,454903");
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsCreditCardFilterNTIPCode()).thenReturn("6");
		when(AlertTypesUtils.getCCAAccountsListForAlertsFiltered(any(UserSession.class), anyString(), anyString(), anyString(), anyString()))
				.thenReturn(new ArrayList<>());
		when(Utils.getRSALanguage(any())).thenReturn("es");
		Map<Object, Object> accountsMap = new LinkedHashMap<>();
		PowerMockito.doReturn(accountsMap).when(Utils.class, "getCCAAccountsMap", anyList(), anyMap());

		Map<Object, Object> resultMap = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "getAccountsMap",
				any(HttpServletRequest.class));
		assertNotNull(resultMap);
	}

	@Test
	public void whenSaveUpdateAlertPreference_GivenSmsBeanNotNull_ThenReturn() throws Exception {

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		Program program3 = new Program();
		Phone phone = new Phone();
		phone.setType("ALT2");
		phone.setValue("+17879584561");
		ArrayList<Phone> phones = new ArrayList<>();
		phones.add(phone);
		program3.setProgramName("PROGRAM_3");
		program3.setPhones(phones);
		program3.setSelectedPhones(phones);
		SmsBean smsBean = new SmsBean();
		smsBean.setPhone1("7879584561");
		bean.setAthplastic1("4549542260934550");

		PowerMockito.doReturn(smsBean).when(ccaPurchasesAndWithdrawalsForm, "getSMSProfile", any(UserSession.class),
				any(HttpServletRequest.class));
		PowerMockito.doNothing().when(ccaPurchasesAndWithdrawalsForm, "validateAlt2SelectedPhone", any(Phone.class),
				any(SmsBean.class), any(PurchasesAndWithdrawalsBean.class), any(UserSession.class), any(HttpServletRequest.class));
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveUpdatePurchaseAmountAlertPreferencesTSYS",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), any(UserSession.class), anyString());
		when(AlertTypesUtils.getSuccessStatusMessage(any(HttpServletRequest.class))).thenReturn("Profile Updated");
		PowerMockito.doReturn("7879584561").when(AlertCCAUtils.class, "removePRCountryCodeContactNumber", anyString());
		PowerMockito.doNothing().when(userSession).updateTsysCCACustomerAlerts(anyString());
		ccaPurchasesAndWithdrawalsForm.setSaveActivateAlertAuditLog(true);
		PowerMockito.doNothing().when(Utils.class, "registerTsysAlertHistoryLog", any(HttpServletRequest.class), anyString(),
				any(UserSession.class), any(TsysAlertStatus.class));

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "saveUpdateAlertPreference", request, bean, model, program3);
		PowerMockito.verifyPrivate(ccaPurchasesAndWithdrawalsForm, times(1)).invoke("saveUpdateAlertPreference",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), anyMap(), any(Program.class));
	}

	@Test
	public void whenSaveUpdateAlertPreference_GivenAlt2PhoneNull_ThenReturn() throws Exception {

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		Program program3 = new Program();
		Phone phone = new Phone();
		phone.setType("HOME");
		phone.setValue("+17879584561");
		ArrayList<Phone> phones = new ArrayList<>();
		phones.add(phone);
		program3.setProgramName("PROGRAM_3");
		program3.setPhones(phones);
		program3.setSelectedPhones(phones);
		SmsBean smsBean = new SmsBean();
		smsBean.setPhone1("7879584561");

		PowerMockito.doReturn(smsBean).when(ccaPurchasesAndWithdrawalsForm, "getSMSProfile", any(UserSession.class),
				any(HttpServletRequest.class));
		PowerMockito.doNothing().when(ccaPurchasesAndWithdrawalsForm, "validateAlt2SelectedPhone", any(Phone.class),
				any(SmsBean.class), any(PurchasesAndWithdrawalsBean.class), any(UserSession.class), any(HttpServletRequest.class));
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveUpdatePurchaseAmountAlertPreferencesTSYS",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), any(UserSession.class), anyString());
		PowerMockito.doNothing().when(ccaPurchasesAndWithdrawalsForm, "addPhoneNumberContactAndAlertPreference",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), anyMap());
		when(AlertTypesUtils.getSuccessStatusMessage(any(HttpServletRequest.class))).thenReturn("Profile Updated");
		PowerMockito.doNothing().when(userSession).updateTsysCCACustomerAlerts(anyString());

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "saveUpdateAlertPreference", request, bean, model, program3);
		PowerMockito.verifyPrivate(ccaPurchasesAndWithdrawalsForm, times(1)).invoke("saveUpdateAlertPreference",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), anyMap(), any(Program.class));
	}

	@Test
	public void whenValidateAlt2SelectedPhone_GivenAlt2SelectedPhoneNotNull_ThenReturnErrorMessage() throws Exception {

		Phone alt2SelectedPhone = new Phone();
		alt2SelectedPhone.setValue("+17879584562");
		SmsBean smsBean = new SmsBean();
		smsBean.setPhone1("7879584561");
		bean.setAthplastic1("4549542260934550");
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		PowerMockito.doReturn("7879584562").when(AlertCCAUtils.class, "removePRCountryCodeContactNumber", anyString());
		PowerMockito.doReturn(false).when(ccaPurchasesAndWithdrawalsForm, "saveAlertContactInformationTSYS", any(),
				any(), any());
		when(AlertTypesUtils.getErrorStatusMessage(any(HttpServletRequest.class))).thenReturn("Unable to Update Profile.");

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "validateAlt2SelectedPhone", alt2SelectedPhone,
				smsBean, bean, request, model);
		assertEquals("Unable to Update Profile.", model.get("statusMessage"));
	}

	@Test
	public void whenValidateAlt2SelectedPhone_GivenAlt2SelectedPhoneNotNull_ThenReturnErrorMessage2() throws Exception {

		Phone alt2SelectedPhone = new Phone();
		alt2SelectedPhone.setValue("+17879584561");
		SmsBean smsBean = new SmsBean();
		smsBean.setPhone1("7879584561");
		bean.setAthplastic1("4549542260934550");
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		PowerMockito.doReturn("7879584561").when(AlertCCAUtils.class, "removePRCountryCodeContactNumber", anyString());
		PowerMockito.doReturn(false).when(ccaPurchasesAndWithdrawalsForm, "saveAlertContactInformationTSYS", any(),
				any(), any());
		when(AlertTypesUtils.getErrorStatusMessage(any(HttpServletRequest.class))).thenReturn("Unable to Update Profile.");
		PowerMockito.doReturn(false).when(ccaPurchasesAndWithdrawalsForm, "saveUpdatePurchaseAmountAlertPreferencesTSYS",
				any(), any(), any(), anyString());

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "validateAlt2SelectedPhone", alt2SelectedPhone,
				smsBean, bean, request, model);
		assertEquals("Unable to Update Profile.", model.get("statusMessage"));
	}

	@Test
	public void whenValidateAlt2SelectedPhone_GivenAlt2SelectedPhoneNotNull_ThenReturnSuccessMessage() throws Exception {

		Phone alt2SelectedPhone = null;
		SmsBean smsBean = new SmsBean();
		smsBean.setPhone1("7875461235");
		bean.setAthplastic1("4549542260934550");
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveAlertContactInformationTSYS", bean,
				userSession, request);
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveUpdatePurchaseAmountAlertPreferencesTSYS",
				any(), any(), any(), anyString());
		PowerMockito.doNothing().when(userSession).updateTsysCCACustomerAlerts(anyString());
		ccaPurchasesAndWithdrawalsForm.setSaveActivateAlertAuditLog(true);
		when(AlertTypesUtils.getSuccessStatusMessage(any(HttpServletRequest.class))).thenReturn("Profile Updated");
		PowerMockito.doNothing().when(Utils.class, "registerTsysAlertHistoryLog", any(HttpServletRequest.class), anyString(),
				any(UserSession.class), any(TsysAlertStatus.class));
		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "validateAlt2SelectedPhone", alt2SelectedPhone,
				smsBean, bean, request, model);
		assertEquals("Profile Updated", model.get("statusMessage"));
	}

	@Test
	public void whenSaveUpdatePurchaseAmountAlertPreferencesTSYS_GivenPreferencesNotNull_ThenReturnTrue() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		bean.setAthplastic1("4549542260934550");
		ccaPurchasesAndWithdrawalsForm.setOriginalAmount("100.55");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		alertDetails.setName("thresholdAmount");
		alertDetails.setSelectedValue("50.0");
		alertDetailsArrayList.add(alertDetails);
		notificationPreferences.setActiveFlag("true");
		notificationPreferences.setAlert("PURCHASE_AMOUNT");
		notificationPreferences.setAlertDetails(alertDetailsArrayList);
		notificationPreferencesArrayList.add(notificationPreferences);
		alertPreferencesResponse.setUserNotificationPreferences(notificationPreferencesArrayList);
		cardPlasticFrontend.setTsysCcaAlertPreferences(alertPreferencesResponse);
		preferences.put("4549542260934550", cardPlasticFrontend);
		when(userSession.getTsysCCACustomerAlerts()).thenReturn(preferences);
		PowerMockito.doReturn(new ArrayList<UserNotificationPreference>()).when(AlertCCAUtils.class, "changeTypeAndUpdateNotificationPreferencesType",
				any(ArrayList.class), anyString(), anyString(), anyString());
		bean.setAlertsPurchaseminamt("50.0");
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		TsysAlertPreferenceCreateUpdateResponse response = new TsysAlertPreferenceCreateUpdateResponse();
		response.setSuccess(true);
		when(tsysAlertFacade.tsysUpdateAlertsPreferences(any(TsysAlertPreferenceCreateUpdateRequest.class))).thenReturn(response);
		PowerMockito.doNothing().when(Utils.class, "registerTsysChangedAlertAmountHistoryLog", any(HttpServletRequest.class),
				anyString(), any(UserSession.class), anyString(), anyString());

		boolean result = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "saveUpdatePurchaseAmountAlertPreferencesTSYS",
				request, bean, userSession, "true");
		assertTrue(result);
	}

	@Test
	public void whenSaveUpdatePurchaseAmountAlertPreferencesTSYS_GivenPreferencesNotNullThenTsysUpdateAlertsPreferencesException_ThenReturnFalse() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		bean.setAthplastic1("4549542260934550");
		ccaPurchasesAndWithdrawalsForm.setOriginalAmount("100.55");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		alertDetails.setName("thresholdAmount");
		alertDetails.setSelectedValue("50.0");
		alertDetailsArrayList.add(alertDetails);
		notificationPreferences.setActiveFlag("true");
		notificationPreferences.setAlert("PURCHASE_AMOUNT");
		notificationPreferences.setAlertDetails(alertDetailsArrayList);
		notificationPreferencesArrayList.add(notificationPreferences);
		alertPreferencesResponse.setUserNotificationPreferences(notificationPreferencesArrayList);
		cardPlasticFrontend.setTsysCcaAlertPreferences(alertPreferencesResponse);
		preferences.put("4549542260934550", cardPlasticFrontend);
		when(userSession.getTsysCCACustomerAlerts()).thenReturn(preferences);
		when(userSession.getUsername()).thenReturn("galloo");
		PowerMockito.doReturn(new ArrayList<UserNotificationPreference>()).when(AlertCCAUtils.class, "changeTypeAndUpdateNotificationPreferencesType",
				any(ArrayList.class), anyString(), anyString(), anyString());
		bean.setAlertsPurchaseminamt("50.0");
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		PowerMockito.doThrow(new TsysException("An error occurred")).when(tsysAlertFacade).tsysUpdateAlertsPreferences(any(TsysAlertPreferenceCreateUpdateRequest.class));
		PowerMockito.doNothing().when(Utils.class, "registerTsysChangedAlertAmountHistoryLog", any(HttpServletRequest.class),
				anyString(), any(UserSession.class), anyString(), anyString());

		boolean result = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "saveUpdatePurchaseAmountAlertPreferencesTSYS",
				request, bean, userSession, "true");
		assertFalse(result);
	}

	@Test
	public void whenSaveUpdatePurchaseAmountAlertPreferencesTSYS_GivenCustomerIdentifiersNull_ThenReturnFalse() throws Exception {

		bean.setAthplastic1("4549542260934550");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(null);
		alertDetails.setName("thresholdAmount");
		alertDetails.setSelectedValue("50.0");
		alertDetailsArrayList.add(alertDetails);
		notificationPreferences.setActiveFlag("true");
		notificationPreferences.setAlert("PURCHASE_AMOUNT");
		notificationPreferences.setAlertDetails(alertDetailsArrayList);
		notificationPreferencesArrayList.add(notificationPreferences);
		alertPreferencesResponse.setUserNotificationPreferences(notificationPreferencesArrayList);
		cardPlasticFrontend.setTsysCcaAlertPreferences(alertPreferencesResponse);
		preferences.put("4549542260934550", cardPlasticFrontend);
		when(userSession.getTsysCCACustomerAlerts()).thenReturn(preferences);

		boolean result = Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "saveUpdatePurchaseAmountAlertPreferencesTSYS",
				request, bean, userSession, "true");
		assertFalse(result);
	}

	@Test
	public void whenRemoveAlertPreference_ThenReturnResultModelSuccess() throws Exception {

		Map<String, Object> model = new HashMap<>();
		bean.setAthplastic1("4549542260934550");
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveUpdatePurchaseAmountAlertPreferencesTSYS",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), any(UserSession.class), anyString());
		when(AlertTypesUtils.getSuccessStatusMessage(any(HttpServletRequest.class))).thenReturn("Profile Updated");
		PowerMockito.doNothing().when(userSession).updateTsysCCACustomerAlerts(anyString());

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "removeAlertPreference", bean, request, model);
		PowerMockito.verifyPrivate(ccaPurchasesAndWithdrawalsForm, times(1)).invoke("removeAlertPreference",
				any(PurchasesAndWithdrawalsBean.class), any(HttpServletRequest.class), anyMap());
	}

	@Test
	public void whenRemoveAlertPreference_ThenReturnResultModelError() throws Exception {

		Map<String, Object> model = new HashMap<>();
		bean.setAthplastic1("4549542260934550");
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		PowerMockito.doReturn(false).when(ccaPurchasesAndWithdrawalsForm, "saveUpdatePurchaseAmountAlertPreferencesTSYS",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), any(UserSession.class), anyString());
		when(AlertTypesUtils.getDeletingErrorStatusMessage(any(HttpServletRequest.class))).thenReturn("Error deleting.");
		when(customerProfile.getProfileId()).thenReturn("654a6s5d4fa4sf4asd56f4a65sf46");

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "removeAlertPreference", bean, request, model);
		PowerMockito.verifyPrivate(ccaPurchasesAndWithdrawalsForm, times(1)).invoke("removeAlertPreference",
				any(PurchasesAndWithdrawalsBean.class), any(HttpServletRequest.class), anyMap());
	}

	@Test
	public void whenOnSubmit_GivenActionEqualsOneNotPreferencesNull_ThenReturnModelMapAlertAmountAndPreferencesInSession() throws Exception {

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA627)).thenReturn(false);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OOB)).thenReturn(false);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn("Test-Cookie");
		when(userSession.getOobEnroll()).thenReturn(false);
		when(userSession.isOobAuthenticated()).thenReturn(false);
		PowerMockito.doReturn("4549542260934550").when(ccaPurchasesAndWithdrawalsForm,
				"getAthPlastic", anyString());
		when(ccaPurchasesAndWithdrawalsForm.getTsysAlertsMinAlertQty()).thenReturn("0.01");
		bean.setAthplastic1("4549542260934550");
		bean.setAction("1");
		accountsMap.put("none", "Select a Credit Card");
		accountsMap.put("VISA BPPR", "x4550");
		accountsMap.put("MASTERCARD JET BLUE", "x5345");
		PowerMockito.doReturn(accountsMap).when(ccaPurchasesAndWithdrawalsForm,
				"getAccountsMap", any(HttpServletRequest.class));
		when(userSession.getCcaAccounts()).thenReturn(ccaAccounts);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);

		alertDetails.setName("thresholdAmount");
		alertDetails.setSelectedValue("50.0");
		alertDetailsArrayList.add(alertDetails);
		notificationPreferences.setActiveFlag("true");
		notificationPreferences.setAlert("PURCHASE_AMOUNT");
		notificationPreferences.setAlertDetails(alertDetailsArrayList);
		notificationPreferencesArrayList.add(notificationPreferences);
		alertPreferencesResponse.setUserNotificationPreferences(notificationPreferencesArrayList);
		cardPlasticFrontend.setTsysCcaAlertPreferences(alertPreferencesResponse);
		preferences.put("4549542260934550", cardPlasticFrontend);
		when(userSession.getTsysCCACustomerAlerts()).thenReturn(preferences);
		PowerMockito.doReturn(null).when(ccaPurchasesAndWithdrawalsForm,
				"getPurchaseAmountNotificationPreferences", any(ArrayList.class));

		ModelAndView mav = ccaPurchasesAndWithdrawalsForm.onSubmit(request, response, bean, errors);
		assertNull(mav.getModel().get("purchase_min_amt"));
	}

	@Test
	public void whenInquiryTsysAlertPreferences_GivenAlertPreferencesActiveFlagFalse_ThenReturnResponseNotNull() throws Exception {

		customerIdentifiers.setCustomerId("000054786");
		customerIdentifiers.setAccountId("00000037838");
		when(Utils.getGeneralInformationCustomerIdentifiers(anyString(), any(UserSession.class))).thenReturn(customerIdentifiers);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		notificationPreferences.setActiveFlag("false");
		notificationPreferencesArrayList.add(notificationPreferences);
		alertPreferencesResponse.setUserNotificationPreferences(notificationPreferencesArrayList);
		bean.setAthplastic1("4549542260934550");
		bean.setCcaAccount("4549542260934550");
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);

		when(tsysAlertFacade.tsysAlertPreferenceByAccountCustomerId(any(AlertCustomerIdentifiers.class)))
				.thenReturn(alertPreferencesResponse);

		PowerMockito.doReturn(notificationPreferences).when(ccaPurchasesAndWithdrawalsForm,
				"getPurchaseAmountNotificationPreferences", any(ArrayList.class));
		PowerMockito.doReturn("125.0").when(ccaPurchasesAndWithdrawalsForm,
				"getAlertPurchaseAmount", any(UserNotificationPreferences.class));
		when(userSession.getCcaAccounts()).thenReturn(ccaAccounts);
		when(Utils.getAccountByAccountNumber(anyString(), anyList())).thenReturn(tvFrontendAccount);
		when(Utils.getCCACardPlastics(any(CustomerServices.class), any(CustomerProfile.class),
				any(BankingSession.class), any(TVFrontendAccount.class))).thenReturn(ccaPlastics);
		PowerMockito.doNothing().when(ccaPurchasesAndWithdrawalsForm, "saveCCAPlasticAlertInSession",
				any(UserSession.class), any(Arrays.class), any(PurchasesAndWithdrawalsBean.class), any(TsysAlertPreferencesResponse.class));
		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "inquiryTsysAlertPreferences", userSession, bean, model);
		assertNull(model.get("purchase_min_amt"));
	}

	@Test
	public void whenAddPhoneNumberContactAndAlertPreference_GivenModelAndUserSession_ThenSaveAuditLog() throws Exception {

		Map<String, Object> model = new HashMap<>();
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveAlertPhoneContactTSYS", any(PurchasesAndWithdrawalsBean.class),
				any(UserSession.class), any(HttpServletRequest.class));
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveAlertContactInformationTSYS", any(PurchasesAndWithdrawalsBean.class),
				any(UserSession.class), any(HttpServletRequest.class));
		bean.setAlertsPurchaseminamt("125.5");
		when(AlertTypesUtils.getSuccessStatusMessage(any(HttpServletRequest.class))).thenReturn("Profile Updated");
		ccaPurchasesAndWithdrawalsForm.setSaveActivateAlertAuditLog(true);
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveUpdatePurchaseAmountAlertPreferencesTSYS",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), any(UserSession.class), anyString());
		PowerMockito.doNothing().when(Utils.class, "registerTsysAlertHistoryLog", any(HttpServletRequest.class), anyString(),
				any(UserSession.class), any(TsysAlertStatus.class));

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "addPhoneNumberContactAndAlertPreference", request, bean, model);
		assertEquals("Profile Updated", model.get("statusMessage"));
	}

	@Test
	public void whenLoadPlastics_GivenAthPlasticNotNull_ThenReturnPlastics() throws Exception {

		bean.setAthplastic1("04549542260934550");
		bean.setCcaAccount("04549542260934550");
		CardPlasticAccountRecord accountRecord = new CardPlasticAccountRecord();
		accountRecord.setCardPANId("04549542260934550");
		accountRecord.setEmbossedName("TEST");
		accountRecord.setCardPANId("TEST");
		cardPlasticFrontend = new CardPlasticFrontend(accountRecord);
		cardPlasticFrontend.setTsysCcaAlertPreferences(alertPreferencesResponse);
		ccaPlastics = new CardPlasticFrontend[1];
		ccaPlastics[0] = cardPlasticFrontend;
		when(userSession.getCcaAccounts()).thenReturn(ccaAccounts);
		when(Utils.getAccountByAccountNumber(anyString(), any())).thenReturn(tvFrontendAccount);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(Utils.getCCACardPlastics(any(), any(), any(), any())).thenReturn(ccaPlastics);
		when(Utils.removeInactiveCardPlastics(any(), any())).thenReturn(ccaPlastics);
		Map<Object, Object> plastics = new HashMap<>();
		Map<String, Object> model = new HashMap<>();

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "loadPlastics", userSession, bean, plastics, model);
		assertTrue(((Map) model.get("accountDetails")).size() > 0);
	}
}
