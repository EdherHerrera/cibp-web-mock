package evertec.cibp.web.form;

import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerInteractionService;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.ActivateAthIntBean;
import evertec.cibp.web.form.bean.ModifyPhoneBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.DeviceRequest;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(PowerMockRunner.class)
@PrepareForTest({CustomerProfile.class, ActivateAthIntBean.class, UserSession.class,
        CustomerServices.class, CustomerInteractionEvent.class,
        WebUtils.class, Utils.class})
public class ModifyPhoneFormUT extends AbstractModelAndViewTests {

    private static final String TEST = "TEST";
    private static final String ID = "ID";
    private static final String VIEW = "modifyPhone";

    private Map<Object,Object> map;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private UserSession userSession;

    @Mock
    private CustomerServices customerServices;

    @Mock
    private CustomerInteractionEvent customerInteractionEvent;
    
    @Mock
    private CustomerEntitlement customerEntitlement;

    @Mock
    AlertsEntitlement alertsEntitlement;
    
    @Mock
    private CustomerFacade customerFacade;
    
    @Mock
    private BindException errors;
    
    @Mock
    private CustomerInteractionService customerInteractionService;
    
    @Mock
    private DeviceRequest deviceRequest;
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    
    @Mock
    ModifyPhoneBean modifyPhoneBean;

    @InjectMocks
    private ModifyPhoneForm controller;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(CustomerProfile.class);
        PowerMockito.mockStatic(UserSession.class);
        PowerMockito.mockStatic(CustomerServices.class);
        PowerMockito.mockStatic(CustomerInteractionEvent.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Utils.class);
        
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

    }

    public void init(){
        map = new HashMap<Object, Object>();
        map.put(TEST, TEST);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
        
        when(modifyPhoneBean.getResidentialPhone1()).thenReturn(StringUtils.EMPTY);
        when(modifyPhoneBean.getResidentialPhone2()).thenReturn(StringUtils.EMPTY);
        when(modifyPhoneBean.getResidentialPhone3()).thenReturn(StringUtils.EMPTY);
        when(modifyPhoneBean.getWorkPhone1()).thenReturn(StringUtils.EMPTY);
        when(modifyPhoneBean.getWorkPhone2()).thenReturn(StringUtils.EMPTY);
        when(modifyPhoneBean.getWorkPhone3()).thenReturn(StringUtils.EMPTY);
        when(modifyPhoneBean.getCelPhone1()).thenReturn(StringUtils.EMPTY);
        when(modifyPhoneBean.getCelPhone2()).thenReturn(StringUtils.EMPTY);
        when(modifyPhoneBean.getCelPhone3()).thenReturn(StringUtils.EMPTY);
        
        when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);
        when(Utils.generateCustomerInteractionEvent(any(), any())).thenReturn(customerInteractionEvent);
        when(customerServices.getInteractionService()).thenReturn(customerInteractionService);
    }

    private void referenceDataMocks() {
    	 when(alertsEntitlement.getCustomerEntitlement()).thenReturn(customerEntitlement);
         Set<AlertType> alertType = new HashSet<AlertType>();//all ale
         alertType.add(AlertType.PAYMENTS_MODIFY_EMAIL);//Random Alert
         when(alertsEntitlement.getAlerts()).thenReturn(alertType);
         when(Utils.getRSALanguage(any())).thenReturn("en");
         when(deviceRequest.getDeviceTokenCookie()).thenReturn("PMData");
         when(userSession.getInformationQuestion()).thenReturn(null);// for when referenceData is called
    }

    @Test
    public void whenOnSubmit_GivenStatusActivated_ThenReturnModelAndView() throws Exception {
        init();
        referenceDataMocks();
        ModelAndView modelAndView = controller.onSubmit(request, response, modifyPhoneBean, errors);
        assertViewName(modelAndView, VIEW);
        assertModelAttributeValue(modelAndView, "completed", true);
    }
}
