package evertec.cibp.web.form;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.RegainAccessEmail;
import evertec.cibp.core.models.RegainAccessToken;
import evertec.cibp.core.models.csr.CustomerSearchFilter;
import evertec.cibp.core.models.customer.*;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.adaptiveauth.ModifyCustomerProfileResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.PasswordHashUtil;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.CaptchaSettings;
import evertec.cibp.web.form.bean.ResetPasswordBean;
import evertec.cibp.web.services.modules.RegainAccessServiceModule;
import evertec.cibp.web.session.CaptchaSession;
import evertec.cibp.web.session.PasswordResetSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

import static evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType.REGAIN_ACCESS_EMAIL_BLOCK;
import static evertec.cibp.core.models.customer.interaction.CustomerInteractionEventType.REGAIN_ACCESS_PERSONAL_SUCCESS;
import static evertec.cibp.core.utils.StaticMessageSource.MBFC224;
import static evertec.cibp.core.utils.StaticMessageSource.isFlagEnabled;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, WebUtils.class, CustomerFacade.class, RSAUtils.class, Utils.class,
		PasswordHashUtil.class, CustomerStatus.class, ResultStatus.class, RequestContextUtils.class,
		ResetPasswordForm.class, ApiCallerFactory.class})
public class ResetPasswordFormUT extends AbstractModelAndViewTests {

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletRequest httpServletRequest;
	
	@Mock
	private HttpServletRequest requestCookies;

	@Mock
	private HttpServletResponse response;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private CustomerProfile profile;
	
	@Mock
	private CaptchaSession captchaSession;

	@Mock
	private CustomerProfileService customerProfileService;

	@Mock 
	private UserSession userSession;

	@Mock
	private PasswordResetSession prsession;
	
	@Mock
	private MultipleCustomerProfileResponse customerProfileResponse;
	
	@Mock
	private ResetPasswordBean command;

	@Mock
	private PasswordResetSession passwordResetSession;

	@Mock
	private Map<Object, CustomerProfile> customerProfiles;

	@Mock
	private CaptchaSettings captchaSettings;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private CustomerServices customerServicesForm;

	@Mock
	private Errors errors;

	@Mock
	private BindException error;

	@Mock
	private ModifyCustomerProfileResponse modifyCustomerResponse;

	@InjectMocks
	@Spy
	private final ResetPasswordForm controller = new ResetPasswordForm();

	@Mock
	private RegainAccessServiceModule regainAccessServiceModule;

	@Mock
	private ApiCaller apiCaller;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(RSAUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(PasswordHashUtil.class);
		MockHttpServletRequest mk = new MockHttpServletRequest();
		mk.setPathInfo("/old/resetpassword");
		
		request = mk;
		httpServletRequest = PowerMockito.mock(HttpServletRequest.class);
		PowerMockito.mockStatic(CustomerStatus.class);
		PowerMockito.mockStatic(ResultStatus.class);
		PowerMockito.mockStatic(RequestContextUtils.class);
	}

	@Test
	public void whenOnBindAndValidate_GivenUsernameInPassword_ThenReturnErrorTrue() throws Exception {
		ResetPasswordBean bean = new ResetPasswordBean();
		BindException error = new BindException(bean, "ResetPasswordBean");
		
		bean.setUsername("username");
		bean.setPassword("Junitusername1");
		bean.setPasswordconfirm("Junitusername1");
				
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989)).thenReturn(true);
		when(this.customerService.isUsernameAvailable(bean.getUsername())).thenReturn(true);
		when(Utils.isValidPassword(bean.getPassword())).thenReturn(true);
		when(WebUtils.getSessionAttribute(request, "passwordResetSession")).thenReturn(prsession);
		when(this.prsession.getCustomerProfile()).thenReturn(this.profile);
		when(this.profile.getUsername()).thenReturn(bean.getUsername());
		
		this.controller.onBindAndValidate(this.request, bean, error, 2);
		assertFalse(error.getFieldErrors().isEmpty());
		FieldError field = (FieldError)error.getFieldErrors().get(0);
		assertEquals("Password must not contains username", field.getDefaultMessage());
	}
	
	@Test
	public void whenOnBindAndValidate_NoGivenUsernameInPassword_ThenReturnErrorFalse() throws Exception {
		ResetPasswordBean bean = new ResetPasswordBean();;
		BindException error = new BindException(bean, "ResetPasswordBean");
		
		bean.setUsername("username");
		bean.setPassword("Password1234");
		bean.setPasswordconfirm("Password1234");
			
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989)).thenReturn(true);
		when(PasswordHashUtil.generateSaltedHash(bean.getPassword(), 1)).thenReturn("2");
		when(WebUtils.getSessionAttribute(request, "passwords")).thenReturn(new ArrayList<CustomerPassword>());
		when(WebUtils.getSessionAttribute(request, "passwordResetSession")).thenReturn(prsession);
		when(WebUtils.getSessionAttribute(request, "passwordResetSession")).thenReturn(prsession);
		when(this.prsession.getCustomerProfile()).thenReturn(this.profile);
		when(Utils.isValidPassword(bean.getPassword())).thenReturn(true);
		when(this.profile.getUsername()).thenReturn(bean.getUsername());
		when(this.profile.getPasswordSalt()).thenReturn(1234);
		when(this.profile.getPasswordHash()).thenReturn("hashpassword");
		when(PasswordHashUtil.generateSaltedHash(any(String.class), any(Integer.class))).thenReturn("newhashpassword");
		
		this.controller.onBindAndValidate(this.request, bean, error, 2);
		assertTrue(error.getFieldErrors().isEmpty());
	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-24-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue2() throws Exception {

		String pathInfo = "asd65f4156as4f65as4";

		when(WebUtils.getSessionAttribute(requestCookies, "userSession")).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(command.isMobileApp()).thenReturn(Boolean.TRUE);
		when(WebUtils.getSessionAttribute(requestCookies, "sessionEnrollUpdate")).thenReturn(null);
		when(WebUtils.getSessionAttribute(requestCookies, "passwordResetSession")).thenReturn(passwordResetSession);
		when(passwordResetSession.getInteractionEvent()).thenReturn(new CustomerInteractionEvent());
		controller.setCaptchaSettings(captchaSettings);
		when(passwordResetSession.getActNotFoundCounter()).thenReturn(2);
		when(passwordResetSession.isBlocked()).thenReturn(Boolean.FALSE);
		when(passwordResetSession.getActCounter()).thenReturn(2);
		when(passwordResetSession.getCounterBlock()).thenReturn(2);
		when(passwordResetSession.getDobCounter()).thenReturn(2);
		when(passwordResetSession.getCounterFailed()).thenReturn(2);
		when(passwordResetSession.isIncompleteEnroll()).thenReturn(Boolean.FALSE);
		when(WebUtils.getSessionAttribute(requestCookies, "passblocked")).thenReturn("sdfasfasdf");
		when(Utils.getRSALanguage(requestCookies)).thenReturn("sp");
		when(command.getAccountInfoType()).thenReturn("asdfasfas");
		when(passwordResetSession.getCustomerProfileAccountMap()).thenReturn(null);
		when(command.getMobileAppType()).thenReturn("dsfsdfsd");
		when(errors.getAllErrors()).thenReturn(new ArrayList<>());
		when(httpServletRequest.getPathInfo()).thenReturn("asdfasdfasdfasdf");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE291)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE303)).thenReturn(Boolean.FALSE);
		when(RequestContextUtils.getLocale(any(HttpServletRequest.class))).thenReturn(new Locale("en"));
		when(requestCookies.getPathInfo()).thenReturn(pathInfo);
		Map<String, Object> result = controller.referenceData(requestCookies, command, errors, 1);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when ProcessFinish
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-24-2020
	 * @exception Exception error
	 */
	@Test
	public void whenProcessFinish_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue2 () throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("cookieAnalyticEnabled", true);
		String password = "asd65f4156as4f65as4";

		when(WebUtils.getSessionAttribute(requestCookies, "userSession")).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(WebUtils.getSessionAttribute(requestCookies, "passwordResetSession")).thenReturn(passwordResetSession);
		when(passwordResetSession.getCustomerProfileAccountMap()).thenReturn(customerProfiles);
		when(WebUtils.getSessionAttribute(requestCookies, "passwords")).thenReturn(null);
		when(passwordResetSession.getValidatedAcct()).thenReturn("1231654654");
		when(customerProfiles.get(any())).thenReturn(customerProfile);
		when(command.getPassword()).thenReturn(password);
		when(requestCookies.getRemoteAddr()).thenReturn("asdfasdasd asdfasf");
		when(requestCookies.getRequestedSessionId()).thenReturn("sdafsadfasf");
		when(customerProfile.getProfileId()).thenReturn("4654564asdfasdf654");
		when(customerProfile.getPasswordHash()).thenReturn("4654564asdfasdf654");
		when(customerProfile.getPasswordSalt()).thenReturn(54646546);
		when(customerProfile.getUsername()).thenReturn("jonas");
		when(customerProfile.getStatus()).thenReturn(CustomerStatus.PENDING_CSR_ACTIVATION);
		controller.setCustomerService(customerServicesForm);
		when(customerServicesForm.modifyCustomerPassword(any())).thenReturn(modifyCustomerResponse);
		when(modifyCustomerResponse.getStatus()).thenReturn(ResultStatus.BLOCKED);
		PowerMockito.doReturn(mav).when((AbstractFormController)controller, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any(HashMap.class));

		ModelAndView result = controller.processFinish(requestCookies, response, command, error);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}
	
	@Test
	public void whenRegainAccessFlag_is_Active() throws Exception{
		ResetPasswordBean bean = new ResetPasswordBean();;
		BindException error = new BindException(bean, "ResetPasswordBean");
		
		bean.setUsername("username");
		bean.setPassword("Password1234");
		bean.setPasswordconfirm("Password1234");
		bean.setCaptchaAnswer("answer");
			
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989)).thenReturn(true);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC224)).thenReturn(true);

		when(PasswordHashUtil.generateSaltedHash(bean.getPassword(), 1)).thenReturn("2");
		when(WebUtils.getSessionAttribute(request, "passwords")).thenReturn(new ArrayList<CustomerPassword>());
		when(WebUtils.getSessionAttribute(request, "passwordResetSession")).thenReturn(prsession);
		when(WebUtils.getSessionAttribute(request, "passwordResetSession")).thenReturn(prsession);
		when(this.prsession.getCustomerProfile()).thenReturn(this.profile);
		when(Utils.isValidPassword(bean.getPassword())).thenReturn(true);
		when(this.profile.getUsername()).thenReturn(bean.getUsername());
		when(this.profile.getPasswordSalt()).thenReturn(1234);
		when(this.profile.getPasswordHash()).thenReturn("hashpassword");
		when(PasswordHashUtil.generateSaltedHash(any(String.class), any(Integer.class))).thenReturn("newhashpassword");
		when(WebUtils.getSessionAttribute(request, "captchaSession")).thenReturn((Object) captchaSession);
		when(captchaSession.isCaptchaValid(bean.getCaptchaAnswer())).thenReturn(false);

		this.controller.onBindAndValidate(this.request, bean, error, 0);
		
		verify(captchaSession, times(0)).isCaptchaValid(bean.getCaptchaAnswer());
	}
	
	@Test
	public void whenRegainAccessFlag_is_Inactive() throws Exception{
		ResetPasswordBean bean = new ResetPasswordBean();;
		BindException error = new BindException(bean, "ResetPasswordBean");
		
		bean.setUsername("username");
		bean.setPassword("Password1234");
		bean.setPasswordconfirm("Password1234");
		bean.setCaptchaAnswer("answer");
			
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989)).thenReturn(true);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC224)).thenReturn(false);

		when(PasswordHashUtil.generateSaltedHash(bean.getPassword(), 1)).thenReturn("2");
		when(WebUtils.getSessionAttribute(request, "passwords")).thenReturn(new ArrayList<CustomerPassword>());
		when(WebUtils.getSessionAttribute(request, "passwordResetSession")).thenReturn(prsession);
		when(WebUtils.getSessionAttribute(request, "passwordResetSession")).thenReturn(prsession);
		when(this.prsession.getCustomerProfile()).thenReturn(this.profile);
		when(Utils.isValidPassword(bean.getPassword())).thenReturn(true);
		when(this.profile.getUsername()).thenReturn(bean.getUsername());
		when(this.profile.getPasswordSalt()).thenReturn(1234);
		when(this.profile.getPasswordHash()).thenReturn("hashpassword");
		when(PasswordHashUtil.generateSaltedHash(any(String.class), any(Integer.class))).thenReturn("newhashpassword");
		when(WebUtils.getSessionAttribute(request, "captchaSession")).thenReturn((Object) captchaSession);
		when(captchaSession.isCaptchaValid(bean.getCaptchaAnswer())).thenReturn(false);

		this.controller.onBindAndValidate(this.request, bean, error, 0);
		
		verify(captchaSession).isCaptchaValid(bean.getCaptchaAnswer());
	}

	@Test
	public void whenGetRegainAccessCustomerProfiles_GivenValidEmail_ThenCustomerProfileFound() throws Exception {
		RegainAccessToken regainAccessToken = new RegainAccessToken();
		regainAccessToken.setEmail("Hector.Martinez@evertecinc.com");
		String taxId = "000000001";

		when(isFlagEnabled(MBFC224)).thenReturn(true);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		Whitebox.invokeMethod(controller, "getRegainAccessCustomerProfiles", regainAccessToken, taxId);

		verify(customerProfileService).getCustomerProfileByExactFilter(any(CustomerSearchFilter.class));
	}

	@Test
	public void whenGetRegainAccessCustomerProfiles_GivenValidEmail_ThenCustomerProfileNotFound() throws Exception {
		RegainAccessToken regainAccessToken = new RegainAccessToken();
		regainAccessToken.setEmail(null);
		String taxId = "000000001";

		when(isFlagEnabled(MBFC224)).thenReturn(true);
		MultipleCustomerProfileResponse customerProfiles = Whitebox.invokeMethod(controller,
				"getRegainAccessCustomerProfiles",
				regainAccessToken, taxId);

		assertEquals(customerProfiles.getStatus(), CustomerProfileStatus.NOT_FOUND);
	}

	@Test
	public void whenGetRegainAccessCustomerProfiles_GivenFlagDisabled_ThenGetProfilesByTaxId() throws Exception {
		RegainAccessToken regainAccessToken = new RegainAccessToken();
		regainAccessToken.setEmail(null);
		String taxId = "000000001";

		when(isFlagEnabled(MBFC224)).thenReturn(false);
		Whitebox.invokeMethod(controller, "getRegainAccessCustomerProfiles", regainAccessToken, taxId);

		verify(customerService).getCustomerProfiles(taxId);
	}

	@Test(expected = ModelAndViewDefiningException.class)
	public void whenRegainAccessBlockEmail_GivenValidToken_ThenBlockEmail() throws Exception {
		when(isFlagEnabled(MBFC224)).thenReturn(true);

		String token = "356a4f38-1e59-47c3-b8d7-ba6ff5ec185c";
		String email = "some@email.com";
		RegainAccessToken regainAccessToken = new RegainAccessToken();
		regainAccessToken.setEmail(email);
		RegainAccessEmail regainAccessEmail = new RegainAccessEmail();
		regainAccessEmail.setEmail(email);

		when(WebUtils.getSessionAttribute(request, "regainAccessToken")).thenReturn(token);
		when(customerService.getRegainAccessToken(token)).thenReturn(regainAccessToken);
		when(customerService.getRegainAccessEmail(regainAccessToken.getEmail())).thenReturn(regainAccessEmail);

		Whitebox.invokeMethod(controller, "regainAccessBlockEmail", request);

		verify(customerService, times(1))
				.updateRegainAccessEmail(regainAccessEmail);
		verify(regainAccessServiceModule, times(1))
				.logRegainAccessEvent(request, regainAccessToken, null, REGAIN_ACCESS_EMAIL_BLOCK);
	}

	@Test
	public void whenRegainAccessBlockEmail_GivenInvalidToken_ThenDoNotBlockEmail() throws Exception {
		when(isFlagEnabled(MBFC224)).thenReturn(true);

		String token = "356a4f38-1e59-47c3-b8d7-ba6ff5ec185c";

		when(WebUtils.getSessionAttribute(request, "regainAccessToken")).thenReturn(token);
		when(customerService.getRegainAccessToken(token)).thenReturn(new RegainAccessToken());

		Whitebox.invokeMethod(controller, "regainAccessBlockEmail", request);

		verify(customerService, never())
				.updateRegainAccessEmail(any(RegainAccessEmail.class));
	}
	
	@Test
	public void whenReferenceDataCalled_And_ProfileIsSecurities() throws Exception {
		
		ResetPasswordBean bean = new ResetPasswordBean();;

		bean.setUsername("username");
		bean.setPassword("Password1234");
		bean.setPasswordconfirm("Password1234");
		bean.setCaptchaAnswer("answer");
		
		UserSession us = new UserSession();
		CustomerProfile cp = new CustomerProfile();
		cp.setIsSecurities("Y");
		cp.setIsTransactional("N");
		us.setCustomerProfile(cp);
		bean.setMobileApp(false);
	
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(us);
		
		try {
			this.controller.referenceData(request, bean, null, 0);
			fail("Expected ModelAndViewDefiningException.");
		
		}catch (ModelAndViewDefiningException e){
			assertTrue(e.getClass() == ModelAndViewDefiningException.class);
		}
		
	
		
	} 
	
	@Test
	@Ignore("WITH ERROR")
	public void whenReferenceDataCalled_And_ProfileIsNotSecurities() throws Exception {
		
		ResetPasswordBean bean = new ResetPasswordBean();;
		Errors error = new BindException(new Object(), "Error");
		bean.setUsername("username");
		bean.setPassword("Password1234");
		bean.setPasswordconfirm("Password1234");
		bean.setCaptchaAnswer("answer");

		
		UserSession us = new UserSession();
		CustomerProfile cp = new CustomerProfile();
		cp.setIsSecurities("N");
		cp.setIsTransactional("N");
		us.setCustomerProfile(cp);
		bean.setMobileApp(false);
	
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(us);
		
		try {
			this.controller.referenceData(request, bean, error, 0);
	
		}catch (ModelAndViewDefiningException e){
			assertFalse(e.getClass() == ModelAndViewDefiningException.class);
		}
			
	} 
	
	@Test
	public void whenRegainAccess_with_multiple_Profiles_That_Have_The_Same_SSN_and_Email() throws Exception{
		ResetPasswordBean bean = new ResetPasswordBean();;
		BindException error = new BindException(bean, "ResetPasswordBean");
		RegainAccessToken regainAccessToken = new RegainAccessToken();
		regainAccessToken.setEmail("bolivar.diaz@evertecinc.com");
		MultipleCustomerProfileResponse multipleCustomerProfileResponse = new MultipleCustomerProfileResponse(); 
		List<CustomerProfile> customerProfiles = new ArrayList<CustomerProfile>();
		CustomerProfile customer1 = new CustomerProfile();
		customer1.setTaxId("580-31-9125");
		customer1.setEmail("bolivar.diaz@evertecinc.com");
		Calendar birthday = Calendar.getInstance();
		birthday.set(Calendar.YEAR,1962);
		birthday.set(Calendar.MONTH,3);
		birthday.set(Calendar.DATE,5);
		customer1.setBirthDate(birthday.getTime());
		customer1.setEnrollmentAccountNumber("576283109");
		customerProfiles.add(customer1);
		customerProfiles.add(customer1);
		customerProfiles.add(customer1);
		multipleCustomerProfileResponse.setCustomerProfiles(customerProfiles);
					
		bean.setDobday(5);
		bean.setDobmonth(3);
		bean.setDobyear(1962);
		bean.setTaxId1("580");
		bean.setTaxId2("31");
		bean.setTaxId3("9125");
			
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989)).thenReturn(true);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC224)).thenReturn(true);
		when(WebUtils.getSessionAttribute(request, "passwordResetSession")).thenReturn(prsession);
		when(WebUtils.getSessionAttribute(request, "passwordResetSession")).thenReturn(prsession);
		when(this.prsession.getCustomerProfile()).thenReturn(this.profile);
		when(customerService.getRegainAccessToken(Mockito.anyString())).thenReturn(regainAccessToken);
		when(WebUtils.getSessionAttribute(request, "regainAccessToken")).thenReturn("awdaw-fre34-feagag");
		when(customerProfileService.getCustomerProfileByExactFilter(Mockito.any())).thenReturn(multipleCustomerProfileResponse);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		this.controller.setCustomerService(customerService);
		this.controller.onBindAndValidate(this.request, bean, error, 0);

		verify(regainAccessServiceModule, times(3)).logRegainAccessEvent(this.request, regainAccessToken, customer1, REGAIN_ACCESS_PERSONAL_SUCCESS);
	}
	

	

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-24-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(command.isMobileApp()).thenReturn(Boolean.TRUE);
		when(WebUtils.getSessionAttribute(request, "sessionEnrollUpdate")).thenReturn(null);
		when(WebUtils.getSessionAttribute(request, "passwordResetSession")).thenReturn(passwordResetSession);
		when(passwordResetSession.getInteractionEvent()).thenReturn(new CustomerInteractionEvent());
		controller.setCaptchaSettings(captchaSettings);
		when(passwordResetSession.getActNotFoundCounter()).thenReturn(2);
		when(passwordResetSession.isBlocked()).thenReturn(Boolean.FALSE);
		when(passwordResetSession.getActCounter()).thenReturn(2);
		when(passwordResetSession.getCounterBlock()).thenReturn(2);
		when(passwordResetSession.getDobCounter()).thenReturn(2);
		when(passwordResetSession.getCounterFailed()).thenReturn(2);
		when(passwordResetSession.isIncompleteEnroll()).thenReturn(Boolean.FALSE);
		when(WebUtils.getSessionAttribute(request, "passblocked")).thenReturn("sdfasfasdf");
		when(Utils.getRSALanguage(request)).thenReturn("sp");
		when(command.getAccountInfoType()).thenReturn("asdfasfas");
		when(passwordResetSession.getCustomerProfileAccountMap()).thenReturn(null);
		when(command.getMobileAppType()).thenReturn("dsfsdfsd");
		when(errors.getAllErrors()).thenReturn(new ArrayList<>());
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE291)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE303)).thenReturn(Boolean.FALSE);
		when(RequestContextUtils.getLocale(any(HttpServletRequest.class))).thenReturn(new Locale("en"));

		Map<String, Object> result = controller.referenceData(request, command, errors, 1);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when ProcessFinish
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-24-2020
	 * @exception Exception error
	 */
	@Test
	public void whenProcessFinish_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		ModelAndView mav = new ModelAndView();
		mav.getModel().put("cookieAnalyticEnabled", true);

		when(WebUtils.getSessionAttribute(httpServletRequest, "userSession")).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(WebUtils.getSessionAttribute(httpServletRequest, "passwordResetSession")).thenReturn(passwordResetSession);
		when(passwordResetSession.getCustomerProfileAccountMap()).thenReturn(customerProfiles);
		when(WebUtils.getSessionAttribute(httpServletRequest, "passwords")).thenReturn(null);
		when(passwordResetSession.getValidatedAcct()).thenReturn("1231654654");
		when(customerProfiles.get(any())).thenReturn(customerProfile);
		when(command.getPassword()).thenReturn("sdfasdfasdf654564");
		when(httpServletRequest.getRemoteAddr()).thenReturn("asdfasdasd asdfasf");
		when(httpServletRequest.getRequestedSessionId()).thenReturn("sdafsadfasf");
		when(customerProfile.getProfileId()).thenReturn("4654564asdfasdf654");
		when(customerProfile.getPasswordHash()).thenReturn("4654564asdfasdf654");
		when(customerProfile.getPasswordSalt()).thenReturn(54646546);
		when(customerProfile.getUsername()).thenReturn("jonas");
		when(customerProfile.getStatus()).thenReturn(CustomerStatus.PENDING_CSR_ACTIVATION);
		controller.setCustomerService(customerServicesForm);
		when(customerServicesForm.modifyCustomerPassword(any())).thenReturn(modifyCustomerResponse);
		when(modifyCustomerResponse.getStatus()).thenReturn(ResultStatus.BLOCKED);
		PowerMockito.doReturn(mav).when((AbstractFormController)controller, "showForm",
				any(HttpServletRequest.class), any(BindException.class), anyString(), any(HashMap.class));

		ModelAndView result = controller.processFinish(httpServletRequest, response, command, error);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}
}
