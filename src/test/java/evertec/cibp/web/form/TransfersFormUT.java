package evertec.cibp.web.form;

import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.transfer.TVAccountTransfer;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.models.transfer.TVAccountTransferTarget;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.TransferResponse;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.form.bean.TransferBean;
import evertec.cibp.web.form.bean.TransfersListBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.TransactionUtil;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, Utils.class, TransactionUtil.class})
public class TransfersFormUT extends AbstractModelAndViewTests {

	private HttpServletRequest request;
	private HttpServletResponse response;

	@Mock
	private BindException error;

	@Mock
	private TransfersListBean transferBean;

	@InjectMocks
	private TransfersForm transfersForm;

	@Mock
	private CustomerServices customerService;

	@Mock
	private ModelMap modelMap;

	@Mock
	private ModelAndView modelAndView;
	
	@Mock
	private Errors errors;
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock 
	private TVAccountTransferSource tvAccount;
	
	@Mock
	private TVAccountTransferTarget tvTarget;
	
	@Mock
	private TVFrontendAccount tvFrontEndAccount;
	
	@Mock
	private TransfersListBean transfersListBean;

	@Mock
	private CustomerInteractionEvent customerInteractionEvent;

	@Mock
	private TransferResponse transferResponse;

	@Mock
	private BankingSession bankingSession;

	@Mock
	private TVAccountTransfer tvAccountTransfer;

	@Mock
	private BindException bindException;

	@Mock
	private TransferBean transferBean1;

	@Mock
	private Date date;

	@Mock
	private TVAccountTransferSource tvAccountTransferSource;

	@Mock
	private TVAccountTransferTarget tvAccountTransferTarget;

	@Mock
	private TVFrontendAccount tvFrontendAccount;

	@Mock
	private CustomerAccountPreferences customerAccountPreferences;


	@Before
	public void setUp() throws Exception {
		
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mock(TransfersForm.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(TransactionUtil.class);
		
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
	}

	private void setMocks() {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerInteractionEvent);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(customerService.sendTransfer(eq(customerProfile), eq(bankingSession), any(TVAccountTransfer.class),
				eq(customerInteractionEvent))).thenReturn(transferResponse);
		when(transferResponse.getTransfer()).thenReturn(tvAccountTransfer);
		when(tvAccountTransfer.getSource()).thenReturn(tvAccountTransferSource);
		when(tvAccountTransfer.getTarget()).thenReturn(tvAccountTransferTarget);
		when(tvAccountTransfer.getAmount()).thenReturn(new BigDecimal("12"));
		when(transferBean1.getAmount()).thenReturn(BigDecimal.valueOf(29));
		when(transferBean1.getAccountFrom()).thenReturn("TR");
		when(transferBean1.getAccountTo()).thenReturn("TO");
		when(transferBean1.getEffectiveDate()).thenReturn(date);
		when(transferBean1.getInstances()).thenReturn(1);
		when(transferBean1.getRecurrent()).thenReturn("no_recurrent");
		when(transferBean1.getInfinite()).thenReturn("no_infinite");
		when(transferBean1.getFrequency()).thenReturn(Frequency.MANUALLY);
		when(userSession.getAccountsTransferFrom()).thenReturn(Collections.singleton(tvAccountTransferSource));
		when(Utils.getAccountSource("TR", Collections.singleton(tvAccountTransferSource))).thenReturn(tvAccountTransferSource);
		when(userSession.getAccountsTransferTo()).thenReturn(Collections.singleton(tvAccountTransferTarget));
		when(Utils.getAccountTarget("TO", Collections.singleton(tvAccountTransferTarget))).thenReturn(tvAccountTransferTarget);
		when(tvAccountTransferSource.getAccount()).thenReturn(tvFrontendAccount);
		when(tvAccountTransferTarget.getAccount()).thenReturn(tvFrontendAccount);
		when(tvFrontendAccount.getNickname()).thenReturn("Acount1");
		when(tvFrontendAccount.getAccountNumberSuffix()).thenReturn("Sufffix1");
		when(tvFrontendAccount.getCustomerAccountPreferences()).thenReturn(customerAccountPreferences);
		when(customerAccountPreferences.getHide()).thenReturn(true);
	}


	@Test
	public void whenReferenceData_GivenFromAccountsIsPIF_ThenCallDetailedAccountFromPIF() throws Exception {
	
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		List<String> noAvailEccounts = mock(List.class);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerService().getDCINotAvailableEaccounts(
				userSession.getCustomerProfile())).thenReturn(noAvailEccounts);
		Set<TVAccountTransferTarget> accountsTo = new HashSet<>();
		accountsTo.add(tvTarget);
		when(Utils.getAccountByKey(any(), any())).thenReturn(tvFrontEndAccount);
		
		when(tvTarget.getAccount()).thenReturn(tvFrontEndAccount);
		
		when(userSession.getAccountsTransferTo()).thenReturn(accountsTo);
		
		when(noAvailEccounts.contains(any())).thenReturn(false);
		
		when(Utils.getRSALanguage(request)).thenReturn("en");
	
		TVAccountProductDefinition productDef = mock(TVAccountProductDefinition.class);
		
		when(tvFrontEndAccount.getProductDefinition()).thenReturn(productDef);
		when(productDef.getProductType()).thenReturn("43443434");
		
		when(tvFrontEndAccount.getSubtype()).thenReturn("IDA");
		when(tvFrontEndAccount.getPortalBalance()).thenReturn(new BigDecimal(384738));
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		List<PIFExternalAccount> pifAcct = new ArrayList<PIFExternalAccount>();
		PIFExternalAccount from = mock(PIFExternalAccount.class);
		pifAcct.add(from);
		when(customerService.getConfirmedPIFExternalAccounts(userSession.getCustomerProfile().getTaxId())).thenReturn(pifAcct);
		
		Map<String, String> map = new HashMap<>();
		map.put("accountName", "The name");
		map.put("accountLast4Num", "x1234");
		map.put("accountNumber", "123456789");
		map.put("accountSubtype", "PIF");
		map.put("accountSection", "");
		map.put("text", "The name x1234");
		
		when(Utils.detailedAccountFromPIF(from)).thenReturn(map);
		
		transfersForm.referenceData(request, transfersListBean, errors, 1);

		///Do not Seperate these lines, if separated the static method will not run//
		verifyStatic(Utils.class);
		Utils.detailedAccountFromPIF(from);
		/////
	}
	
	@Test
	public void whenReferenceData_GivenFromAccountsIsNotPIF_ThenDoNotDetailedAccountFromPIF() throws Exception {
	
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		List<String> noAvailEccounts = mock(List.class);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerService().getDCINotAvailableEaccounts(
				userSession.getCustomerProfile())).thenReturn(noAvailEccounts);
		Set<TVAccountTransferTarget> accountsTo = new HashSet<>();
		accountsTo.add(tvTarget);
		when(Utils.getAccountByKey(any(), any())).thenReturn(tvFrontEndAccount);
		
		when(tvTarget.getAccount()).thenReturn(tvFrontEndAccount);
		
		when(userSession.getAccountsTransferTo()).thenReturn(accountsTo);
		
		when(noAvailEccounts.contains(any())).thenReturn(false);
		
		when(Utils.getRSALanguage(request)).thenReturn("en");
	
		TVAccountProductDefinition productDef = mock(TVAccountProductDefinition.class);
		
		when(tvFrontEndAccount.getProductDefinition()).thenReturn(productDef);
		when(productDef.getProductType()).thenReturn("43443434");
		
		when(tvFrontEndAccount.getSubtype()).thenReturn("IDA");
		when(tvFrontEndAccount.getPortalBalance()).thenReturn(new BigDecimal(384738));
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		List<PIFExternalAccount> pifAcct = new ArrayList<PIFExternalAccount>();
		PIFExternalAccount from = mock(PIFExternalAccount.class);
		pifAcct.add(from);
				
		transfersForm.referenceData(request, transfersListBean, errors, 1);

		///Do not Seperate these lines, if separated the static method will not run//
		verifyStatic(Utils.class, times(0));
		Utils.detailedAccountFromPIF(from);
		/////
	}
	
	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-28-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);

		Map<String, Object> result = transfersForm.referenceData(request, transferBean, errors, 2);
		assertEquals(Boolean.TRUE, result.get("cookieAnalyticEnabled"));
	}

	/**
	 * when ProcessFinish
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-28-2020
	 * @exception Exception error
	 */
	@Test
	public void whenProcessFinish_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(transferBean.getTransfers()).thenReturn(new ArrayList<>());
		when(transferBean.getFavorites()).thenReturn(new ArrayList<>());
		when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(null);
		when(Utils.getRSALanguage(request)).thenReturn("EN");

		ModelAndView result = transfersForm.processFinish(request, response, transferBean, error);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}

	@Test
	public void when_ProcessFinish_GivenResponseIsSuccess_ThenMobileCashAccountIsnull() throws Exception {

		request = new MockHttpServletRequest();


		List<Object> transfers = new ArrayList<>();
		transfers.add(transferBean1);
		when(transferResponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
		when(transferBean.getTransfers()).thenReturn(transfers);
		setMocks();

		transfersForm.processFinish(request, response, transferBean, error);
		verify(userSession).setMobileCashAccounts(null);
	}


	@Test
	public void whenTransferIsFavorite_ProcessFinish_GivenResponseIsSuccess_ThenMobileCashAccountIsnull() throws Exception {

		request = new MockHttpServletRequest();

		List<Object> transfers = new ArrayList<>();
		transfers.add(transferBean1);
		when(transferResponse.getStatus()).thenReturn(ResultStatus.SUCCESS);
		when(transferBean.getFavorites()).thenReturn(transfers);
		setMocks();

		transfersForm.processFinish(request, response, transferBean, error);
		verify(userSession).setMobileCashAccounts(null);
	}

	@Test
	public void whenTransferIsFavorite_ProcessFinish_GivenResponseIsNotSuccess_ThenMobileCashAccountIsNotSetNull() throws Exception {

		request = new MockHttpServletRequest();

		List<Object> transfers = new ArrayList<>();
		transfers.add(transferBean1);
		when(transferResponse.getStatus()).thenReturn(ResultStatus.ERROR);
		when(transferBean.getFavorites()).thenReturn(transfers);
		when(transferResponse.getErrorEBA()).thenReturn("ERROR_EBA");
		when(Utils.getRSALanguage(request)).thenReturn("en");
		setMocks();

		transfersForm.processFinish(request, response, transferBean, error);
		verify(userSession, never()).setMobileCashAccounts(null);

	}

	@Test
	public void when_ProcessFinish_GivenResponseIsNotSuccess_ThenMobileCashAccountIsNotSetNull() throws Exception {

		request = new MockHttpServletRequest();


		List<Object> transfers = new ArrayList<>();
		transfers.add(transferBean1);
		when(transferResponse.getStatus()).thenReturn(ResultStatus.ERROR);
		when(transferBean.getTransfers()).thenReturn(transfers);
		when(transferResponse.getErrorEBA()).thenReturn("ERROR_EBA");
		when(Utils.getRSALanguage(request)).thenReturn("en");
		setMocks();

		transfersForm.processFinish(request, response, transferBean, error);
		verify(userSession, never()).setMobileCashAccounts(null);
	}

}
