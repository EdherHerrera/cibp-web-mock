package evertec.cibp.web.form;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evertec.cibp.core.services.customer.modules.SignOnCustomerServiceModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.owasp.csrfguard.CsrfGuard;
import org.owasp.csrfguard.CsrfGuardException;
import org.owasp.csrfguard.util.RandomGenerator;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.DeviceRequest;

import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.RsaQuestionsUpdateResponse;
import evertec.cibp.web.utils.ListUtils;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.CCPortalServices;
import evertec.cibp.web.form.bean.ChangeQuestionsBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.prophit.service.DefaultProphITService;
import evertec.prophit.service.bean.CustomerInquiryBean;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ Utils.class, WebUtils.class, StaticMessageSource.class, CcServiceController.class ,CsrfGuard.class ,RSAUtils.class})
public class ChangeQuestionsFormIT {

	
	@InjectMocks
	private ChangeQuestionsForm changeQuestionsForm;
	
	private MockHttpServletRequest request;
	
	@Mock
	private HttpServletRequest requestMock;

	@Mock
	private HttpServletResponse responseMock;

	@Mock
	private Errors errors;

	@Mock
	private CCPortalServices cCPortalServices;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private DefaultProphITService defaultProphITService;

	@Mock
	private TVFrontendAccount tvFrontendAccount;

	@Mock
	private CustomerInquiryBean customerInquiryBean;

	@Mock
	private CCPortalServices ccServices;

	@Mock
	private BindException error;

	@Mock
	private TVAccountProductDefinition tVAccountProductDefinition;
	
	
	@Mock
	private ChangeQuestionsBean changeQuestionsBean;
	
	@Mock
	private CsrfGuard csrfGuard;
	
	@Mock
	private RsaQuestionsUpdateResponse rsaQuestionsUpdateResponse;
	
	@Mock
	private DeviceRequest deviceRequest;

	@Mock
	private SignOnCustomerServiceModule signOnCustomerServiceModule;
	
	@Mock
	private Utils utils;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		request = new MockHttpServletRequest();
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(CsrfGuard.class);
		PowerMockito.mockStatic(RSAUtils.class);

		Map<Object, Object> informationQuestion =  new HashMap<>();
		informationQuestion.put("answer", "what");
		when(userSession.getInformationQuestion()).thenReturn(informationQuestion);
		when(WebUtils.getSessionAttribute(any(), eq("userSession"))).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerService.prophItService()).thenReturn(defaultProphITService);
		when(userSession.getProphItCustomerInfo()).thenReturn(customerInquiryBean);
		when(userSession.getCryptoKey()).thenReturn("test");
		when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
		when(CsrfGuard.getInstance()).thenReturn(csrfGuard);
		when(csrfGuard.getTokenName()).thenReturn("OWASP-CSRFTOKEN");
		when(WebUtils.getSessionAttribute(any(), eq("OWASP-CSRFTOKEN"))).thenReturn("SYVA-5QK5-RMQF-8IEV-P9ZC-DJTZ-FOKQ-1H7I");
		when(customerService.getSignOnCustomerServiceModule()).thenReturn(signOnCustomerServiceModule);
		when(signOnCustomerServiceModule.updateUserRSAQuestionSelection(any())).thenReturn(rsaQuestionsUpdateResponse);
		when(Utils.getRSALanguage(any())).thenReturn("en");
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn("HIJKLM");
	}

	@Test
	public final void whenDownStreamOnSubmi_UtilsValidateTokenCsrfGuardGiventHttpServletRequest_GiventHttpServletRequest_ThenReturnHttpServletResponseObjectBindException() {
		
		try {
			when(Utils.validateTokenCsrfGuard(request)).thenReturn(true);
			String token = "SYVA-5QK5-RMQF-8IEV-P9ZC-DJTZ-FOKQ-1H7I";
			WebUtils.setSessionAttribute(request,"OWASP-CSRFTOKEN", token);
			request.setAttribute("OWASP-CSRFTOKEN", token);
			changeQuestionsForm.onSubmit(request, responseMock, changeQuestionsBean, error);
            PowerMockito.verifyStatic(Utils.class);
			Utils.validateTokenCsrfGuard(any());
            
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
	}

	@Test
	public final void whenDownStreamReferenceData_UtilsGenerateTokenCsrfGuard_GivenHttpServletRequest_ThenReturnObjectErrors() {

		try {
			
			changeQuestionsForm.referenceData(request, cCPortalServices, errors);
            PowerMockito.verifyStatic(Utils.class);
			Utils.generateTokenCsrfGuard(any());
			
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}


}
