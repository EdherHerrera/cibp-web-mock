package evertec.cibp.web.form;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CookiePreferenceStatusService;
import evertec.cibp.web.form.bean.CookiePreferenceBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import joptsimple.internal.Strings;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.Errors;
import org.springframework.web.util.WebUtils;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Unit testing class for Cookie Preference Form
 * @author ET58344, Evertec Inc.
 * @since 03-05-2021
 * @version 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CookiePreferenceForm.class, Utils.class, ApiCallerFactory.class})
public class CookiePreferenceFormUT extends AbstractModelAndViewTests {

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private UserSession userSession;

    @Mock
    private CustomerServices customerServices;

    @Mock
    private CookiePreferenceStatusService cookiePreferenceStatusService;

    @Mock
    private CookiePreferenceBean command;

    @Mock
    private Errors errors;

    @Mock
    private CustomerProfile customerProfile;

    private final List<CookiePreferenceStatus> cookiePreferenceStatusList = new ArrayList<>();

    @Mock
    private Map<String, Object> model;

    @Mock
    private URL urlMock;

    @Mock
    private ApiCaller apiCaller;

    @InjectMocks
    @Spy
    private final CookiePreferenceForm controller = new CookiePreferenceForm();

    private final String SHOW_SECTION_JSON = "{\"advertising\" : \"DISABLE\", \"functional\" : \"ENABLE\"}";

    JSONObject jsonObject = new JSONObject("{\n" +
            "  \"Information\": {\n" +
            "    \"title\": \"<i>Cookies</i> y anuncios\",\n" +
            "    \"text\": \"<p>Cuando visita cualquier sitio web, el mismo podr&iacute;a obtener o guardar informaci&oacute;n en su navegador, generalmente mediante el uso de <i>cookies</i> para recoger informaci&oacute;n sobre sus actividades en l&iacute;nea. Esta informaci&oacute;n puede ser acerca de usted, sus preferencias o su dispositivo, y se usa principalmente para que el sitio web funcione seg&uacute;n lo esperado. Por lo general, la informaci&oacute;n no lo identifica directamente, pero puede proporcionarle una experiencia web m&aacute;s personalizada. Tambi&eacute;n se utilizan <i>cookies</i> para otros prop&oacute;sitos, tales como para mantener continuidad durante una sesi&oacute;n en l&iacute;nea, evitar fraude y para prop&oacute;sitos de seguridad de informaci&oacute;n.\\n</p><p>\\nPodr&iacute;amos usar tecnolog&iacute;as similares, como balizas web (web beacons), accesos a contenidos, registros de entrada (logs) y etiquetas (tags) de por s&iacute; o en conjunto con <i>cookies</i>, para recoger estad&iacute;sticas sobre sus patrones de navegaci&oacute;n y/o para evaluar su actividad en general en nuestro sitio web, incluyendo con qu&eacute; frecuencia lo visita. Se puede colocar balizas web en alg&uacute;n correo electr&oacute;nico que le env&iacute;en, para determinar si oprimi&oacute; un enlace en ese email que le dirigi&oacute; a nuestro sitio web. A fin de desplegar esta tecnolog&iacute;a y recoger la mencionada informaci&oacute;n, trabajamos con terceros para rastrear el uso en los sitios web visitados y mensajes de email. Estos terceros nos proveen informaci&oacute;n sobre sus actividades en l&iacute;nea. Estas tecnolog&iacute;as nos permiten proveer contenido y/o promociones, como correos electr&oacute;nicos, con ofertas que se ajusten a sus intereses. Esta pr&aacute;ctica es conocida como publicidad de acuerdo con sus intereses en l&iacute;nea (online behavioral advertising).\\n</p><p>\\nPuede controlar el uso de <i>cookies</i>. Abajo encontrar&aacute; las diferentes categor&iacute;as para saber m&aacute;s y cambiar nuestras configuraciones predeterminadas. Sin embargo, es importante que est&eacute; al tanto de que el bloqueo de algunos tipos de <i>cookies</i> puede afectar su experiencia en el sitio web y los servicios que podemos ofrecer.\\n</p>\"\n" +
            "  },\n" +
            "  \"CookieCategories\": [\n" +
            "    {\n" +
            "      \"category\": \"<i>Cookies</i> estrictamente necesarias\",\n" +
            "      \"text\": \"<p>Estas <i>cookies</i> son necesarias para que el sitio web funcione y no se pueden desactivar en nuestros sistemas. Usualmente est&aacute;n configuradas para manejar y poder ofrecerle los servicios que nos solicita, tales como ajustar sus preferencias de privacidad, iniciar sesi&oacute;n en el sitio web, o llenar formularios. Puede configurar su navegador para bloquear o alertar la presencia de estas <i>cookies</i>, pero algunas partes del sitio web no funcionar&aacute;n adecuadamente.</p>\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"category\": \"<i>Cookies</i> de rendimiento\",\n" +
            "      \"text\": \"<p>Estas <i>cookies</i> nos permiten contar las visitas y de d&oacute;nde provienen para poder medir y mejorar el desempe&ntilde;o de nuestro sitio web. Adem&aacute;s, nos ayudan a comprender c&oacute;mo los visitantes interact&uacute;an con los sitios web reportando y recopilando la informaci&oacute;n de forma an&oacute;nima.</p>\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"category\": \"<i>Cookies</i> funcionales\",\n" +
            "      \"text\": \"<p>Estas <i>cookies</i> permiten que el sitio web mejore su funcionalidad y provea una versi&oacute;n personalizada. Tambi&eacute;n permiten que un sitio web recuerde informaci&oacute;n que se usa para cambiar la forma en que se comporta o se ve el sitio web, tales como su idioma preferido o la regi&oacute;n en la que se encuentra.</p>\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"category\": \"<i>Cookies</i> de publicidad en l&iacute;nea\",\n" +
            "      \"text\": \"<p>Nuestros proveedores de servicio pueden establecer estas <i>cookies</i> a los visitantes de nuestros sitios web, los cuales se utilizan para rastrear sus visitas a otros sitios web. La intenci&oacute;n es mostrarle anuncios a nuestros visitantes que sean relevantes y atractivos para cada usuario particular.</p>\"\n" +
            "    }\n" +
            "  ]\n" +
            "}");

    @Before
    public void setUp() throws Exception {

        controller.setCustomerService(customerServices);
        controller.setShowSectionJson(SHOW_SECTION_JSON);
        request = PowerMockito.mock(HttpServletRequest.class);
        response = PowerMockito.mock(HttpServletResponse.class);
        userSession = PowerMockito.mock(UserSession.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Utils.class);

        PowerMockito.mockStatic(ApiCallerFactory.class);
        when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);
    }

    /**
     * when ReferenceData
     * Given CategoriesTrue
     * Then ReturnModelPreferencesTrue
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     * @throws Exception exception
     */
    @Test
    public void whenReferenceData_GivenCategoriesTrue_ThenReturnModelPreferencesTrue() throws Exception {

        CookiePreferenceStatus cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("ENABLED");
        cookiePreferenceStatus.setCategory("ANALYTICS");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);

        cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("ENABLED");
        cookiePreferenceStatus.setCategory("FUNCTIONAL");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);

        cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("ENABLED");
        cookiePreferenceStatus.setCategory("ADVERTISING");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);

        when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
        when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
        when(cookiePreferenceStatusService.getCookiePreferenceStatusByProfileId(anyString())).thenReturn(cookiePreferenceStatusList);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getProfileId()).thenReturn("asd4156as54fd6as54f6a5sf4");
        when(userSession.getCookiePreferenceStatusList()).thenReturn(cookiePreferenceStatusList);
        PowerMockito.doNothing().when(controller, "readPrivacySettingsJSON", any(), any());
        controller.setShowSectionJson("{\"advertising\" : \"DISABLE\", \"functional\" : \"DISABLE\"}");

        Map<String, Object> resultModel = controller.referenceData(request, command, errors);
        assertEquals(resultModel.get("swFunctionalStatus"), Boolean.TRUE);
    }
    
    /**
     * when ReferenceData
     * Given CategoriesFalse
     * Then ReturnModelPreferencesTrue
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     * @throws Exception exception
     */
    @Test
    public void whenReferenceData_GivenCategoriesFalse_ThenReturnModelPreferencesTrue() throws Exception {

        CookiePreferenceStatus cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("DISABLED");
        cookiePreferenceStatus.setCategory("ANALYTICS");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);

        cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("DISABLED");
        cookiePreferenceStatus.setCategory("FUNCTIONAL");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);

        cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("DISABLED");
        cookiePreferenceStatus.setCategory("ADVERTISING");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);
        
        cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("DISABLED");
        cookiePreferenceStatus.setCategory("NO_ADVERTISING");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);

        when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
        when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
        when(cookiePreferenceStatusService.getCookiePreferenceStatusByProfileId(anyString())).thenReturn(cookiePreferenceStatusList);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getProfileId()).thenReturn("asd4156as54fd6as54f6a5sf4");
        when(userSession.getCookiePreferenceStatusList()).thenReturn(cookiePreferenceStatusList);
        PowerMockito.doNothing().when(controller, "readPrivacySettingsJSON", any(), any());
        controller.setShowSectionJson("{\"advertising\" : \"DISABLE\", \"functional\" : \"DISABLE\"}");

        Map<String, Object> resultModel = controller.referenceData(request, command, errors);
        assertEquals(resultModel.get("swFunctionalStatus"), Boolean.FALSE);
    }

    @Test
    public void whenReadPrivacySettingsJSON_ThenModelContainGDPRInformation() throws Exception {

        PowerMockito.doReturn(jsonObject).when(controller, "getJSONFromUrl", any());
        when(Utils.getRSALanguage(any())).thenReturn("sp");

        Whitebox.invokeMethod(controller, "readPrivacySettingsJSON", model, request);
        assertNotSame(model.get("advertisingText"), Strings.EMPTY);
    }

    @Test
    public void whenGetJSONFromUrl_GivenSPLanguage_ThenReturnJSONObject() throws Exception {

        InputStream stream = new ByteArrayInputStream(jsonObject.toString().getBytes(StandardCharsets.UTF_8));
        URLConnection urlConnectionMock = PowerMockito.mock(HttpsURLConnection.class);
        PowerMockito.whenNew(URL.class).withAnyArguments().thenReturn(urlMock);
        when(urlMock.openConnection()).thenReturn(urlConnectionMock);
        PowerMockito.doNothing().when(urlConnectionMock).connect();
        when(urlConnectionMock.getInputStream()).thenReturn(stream);

        JSONObject result = Whitebox.invokeMethod(controller, "getJSONFromUrl", "sp");
        assertEquals(4, result.getJSONArray("CookieCategories").length());
    }

    @Test
    public void whenGetJSONFromUrl_GivenENLanguage_ThenReturnJSONObject() throws Exception {

        InputStream stream = new ByteArrayInputStream(jsonObject.toString().getBytes(StandardCharsets.UTF_8));
        URLConnection urlConnectionMock = PowerMockito.mock(HttpsURLConnection.class);
        PowerMockito.whenNew(URL.class).withAnyArguments().thenReturn(urlMock);
        when(urlMock.openConnection()).thenReturn(urlConnectionMock);
        PowerMockito.doNothing().when(urlConnectionMock).connect();
        when(urlConnectionMock.getInputStream()).thenReturn(stream);

        JSONObject result = Whitebox.invokeMethod(controller, "getJSONFromUrl", "en");
        assertEquals(4, result.getJSONArray("CookieCategories").length());
    }

    @Test(expected = SSLException.class)
    public void whenGetJSONFromUrl_GivenENLanguage_ThenReturnSSLException() throws Exception {

        InputStream stream = new ByteArrayInputStream(jsonObject.toString().getBytes(StandardCharsets.UTF_8));
        URLConnection urlConnectionMock = PowerMockito.mock(HttpsURLConnection.class);
        PowerMockito.whenNew(URL.class).withAnyArguments().thenReturn(urlMock);
        when(urlMock.openConnection()).thenReturn(urlConnectionMock);
        PowerMockito.doNothing().when(urlConnectionMock).connect();
        PowerMockito.doThrow(new SSLException("An error has occurred"){}).when(urlConnectionMock).getInputStream();

        Whitebox.invokeMethod(controller, "getJSONFromUrl", "en");
    }
}