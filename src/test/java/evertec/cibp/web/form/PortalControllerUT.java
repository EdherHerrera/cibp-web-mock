package evertec.cibp.web.form;

import bppr.sms.beans.SMSProfile;
import com.newrelic.agent.deps.ch.qos.logback.classic.pattern.Util;
import com.rsa.csd.ws.Credential;
import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceRequest;
import edu.emory.mathcs.backport.java.util.Arrays;
import evertec.cibp.core.facades.AdaptiveAuthFacade;
import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.campaign.Campaign;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.core.models.campaign.MyMessagesCampaign;
import evertec.cibp.core.models.customer.*;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.postinitialfunding.PIFAccountStatusTypes;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.profile.ProfileType;
import evertec.cibp.core.models.signup.SignupCampInfo;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.adaptiveauth.RsaChallengeRequest;
import evertec.cibp.core.utils.AccountUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.interruptions.OOBInterruptionForm;
import evertec.cibp.web.premia.control.PremiaUtil;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.*;
import evertec.zap.service.bean.EmployeeInfoResponse;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, WebUtils.class, Util.class, CookieUtils.class, Arrays.class, DeviceUtils.class,
		CustomerFacade.class,ImageUtils.class, Utils.class, PremiaUtil.class, RSAUtils.class, RandomUtils.class, AdaptiveAuthFacade.class, 
		OOBInterruptionForm.class})
@PowerMockIgnore({"org.springframework.ws.client","javax.xml.*", "org.xml.sax.*", "org.w3c.dom.*"})

public class PortalControllerUT {

	@Mock
	private CustomerServices customerService;
	@Mock
	private CustomerProfile profile;
	@Mock
	private CustomerWealthPreferred custWealthPreferred;
	@Mock
	private CustomerProfileService customerProfileService;
	@Mock
	private UserSession userSession;

	@Mock
	private CustomerProfile customerProfile;
	@Mock
	private MessageSource messageSource;
	@Mock
	private CustomerEntitlement customerEntitlement;
	@Mock
	private BankingSession bankingSession;
	@Mock
	private Cookie versionCookie;
	@Mock
	private Cookie cookieBannerCookie;
	@Mock
	private DeviceRequest deviceRequest;

	@Mock
	private GlobalEntitlement globalEntitlement;

	@Mock
	private CustomerFacade customerFacade;

	@Mock
	private List<TVFrontendAccount> accounts;

	@Mock
	private Iterator<TVFrontendAccount> iterator;

	@Mock
	private Map<String, List<MyMessagesCampaign>> campaignsMap;
	
    @Mock
    private PIFExternalAccount pifExternalAccount;
    
    @Mock
    private ProfileType profileType;
    
    @Mock
    private TVFrontendAccount tvFrontendAccount;

    @Mock
    private HttpServletRequest httpServletRequest;

	@Mock
	private CustomerSignonStats customerSignonStats;

	@Mock
	private CustomerInteractionEvent customerInteractionEvent;

	@Mock
	private SMSProfile smsProfile;

    @Mock
    private AdaptiveAuthFacade adaptiveAuthFacade;
    
    @Mock
    private RsaChallengeRequest rsaChallengeRequest;
    
    @Mock
    private SignupCampInfo signupCampInfo;
		
	private Map<String, Object> model;

	private EmployeeInfoResponse bankerInfo;
	byte[] byteArrayImage = {new Byte("10")};
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@InjectMocks
	private PortalController controller;

	/**
	 * A wrapper to get portalController injected mock
	 * @return PortalController
	 */
	private PortalController getController () {
		return this.controller; // return controller of the class
	}

	/**
	 * A wrapper to get GlobalEntitlement mock
	 * @return GlobalEntitlement
	 */
	private GlobalEntitlement getGlobalEntitlement () {
		return this.globalEntitlement; // return globalController mock
	}

	@Before
	public void setUp() throws Exception {
		//MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Util.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(CookieUtils.class);
		PowerMockito.mockStatic(Arrays.class);
		PowerMockito.mockStatic(DeviceUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(ImageUtils.class);
		PowerMockito.mockStatic(PremiaUtil.class);
		PowerMockito.mockStatic(RSAUtils.class);
		PowerMockito.mockStatic(RandomUtils.class);
		PowerMockito.mockStatic(AdaptiveAuthFacade.class);
		PowerMockito.mockStatic(OOBInterruptionForm.class);
		
		

		controller = PowerMockito.spy(new PortalController());
		controller.setNonTransEnabledEstmts("");
		controller.setNonTransEnabledInterestLetters("");
		controller.setMessageSource(messageSource);
		controller.setCustomerService(customerService);

		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();

		when(WebUtils.getSessionAttribute(any(), eq("userSession"))).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(CookieUtils.getCookie(any(), any())).thenReturn(versionCookie);
		when(Arrays.asList(any())).thenReturn(new ArrayList());
		when (Utils.getRSALanguage(request)).thenReturn("sp");
		when(messageSource.getMessage(any(), any(), any(), any())).thenReturn("");
		when(customerProfile.getEntitlementByType(eq(EntitlementType.BANKING))).thenReturn(customerEntitlement);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(DeviceUtils.getCustomerDevice(any())).thenReturn(CustomerDevices.PERSONAL);

		when(WebUtils.getSessionAttribute(any(), eq("showOOBSEC"))).thenReturn("");
		when(WebUtils.getSessionAttribute(any(), eq((Utils.DEEP_LINK)))).thenReturn(null);

		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(globalEntitlement);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.RETIREMENT_PLAN)).thenReturn(globalEntitlement);
		when(Utils.messageCampaign(userSession, controller.getWealthCodes(),controller.getPreferredCodes())).thenReturn(campaignsMap);
		
		when(CookieUtils.getCookie(any(), eq("bppr_dmp"))).thenReturn(cookieBannerCookie);
		when(cookieBannerCookie.getValue()).thenReturn(StringUtils.EMPTY);
		
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
	}

	/**
	 * Premium Banking UT
	 */
	/**
	 * When portal
	 * Given Premium Banking Flag Disabled
	 * Then not return isPremiumBanking Flag on Portal Response ModelAndView
	 * @throws Exception
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void whenPortal_givenPremiumBankingFlagDisabled_thenNotReturnFlagOnResponse() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_PREMIUM_BANKING_LIMITS_PER_SEGMENT)).thenReturn(Boolean.FALSE);
		
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("flagMBCPremiumBanking"));
		assertFalse(modelView.getModel().containsKey("isPremiumBanking"));
	}

	/**
	 * When portal
	 * Given Premium Banking Flag Disabled
	 * Then Return isPremiumBanking Flag on Portal Response ModelAndView
	 * @throws Exception
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void whenPortal_givenPremiumBankingFlagEnabled_thenNotReturnFlagOnResponse() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.DFM_PREMIUM_BANKING_LIMITS_PER_SEGMENT)).thenReturn(Boolean.TRUE);
		
		//when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFM169)).thenReturn(Boolean.FALSE);
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
		//(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");

		when(CookieUtils.getCookie(any(), eq("bppr_dmp"))).thenReturn(cookieBannerCookie);
		when(cookieBannerCookie.getValue()).thenReturn("1");
		
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("flagMBCPremiumBanking"));
		assertTrue(modelView.getModel().containsKey("isPremiumBanking"));
	}
	/**
	 * End Premium Banking UT
	 */


	/**
	 * When portal
	 * Given isWealth = true and hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON) = false
	 * Then verify private method createWealthAdvisorButtonEntitlement invoked
	 * @throws Exception
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void whenPortal_GivenIsWealthTrueAndHasEntitlementFalse_thenCreateEntitlement () throws Exception {
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(true);
		when(customerProfile.hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(false);
		when(userSession.getLanguage()).thenReturn("en");
		
		//when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFM169)).thenReturn(Boolean.TRUE);
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
		//(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		controller.portal(request, response);
		PowerMockito.verifyPrivate(controller, times(1)).invoke("createWealthAdvisorButtonEntitlement",userSession);
	}

	/**
	 * When portal
	 * Given isWealth = false and hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON) = false
	 * Then verify private method createWealthAdvisorButtonEntitlement wasn´t invoked
	 * @throws Exception
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void whenPortal_GivenIsWealthFalseAndHasEntitlementFalse_thenCreateEntitlementNotInvoked () throws Exception {
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(false);
		when(customerProfile.hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(true);
		when(userSession.getLanguage()).thenReturn("en");
		
		//when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFM169)).thenReturn(Boolean.TRUE);
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
		//when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		controller.portal(request, response);
		PowerMockito.verifyPrivate(controller, never()).invoke("createWealthAdvisorButtonEntitlement",userSession);
	}

	/**
	 * Test para metodo que setea el cis number en modelo.
	 * @author ET58344.
	 * @since 10-03-2020
	 */
	@Test
	@Ignore("WITH ERROR")
	public void whenSetCisNumberInMap_GivenUserSessionModelNotNull_ThenReturnModelCisNumber() {
		model = new HashMap<>();
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		//when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		//model = controller.setCisNumberInMap(userSession, model);
		assertTrue(model.containsKey("cisNumber"));
	}
	
	/**
	 * Test para metodo que setea el cis number en modelo.
	 * @author ET58344.
	 * @since 10-03-2020
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void whenSetCisNumberInMap_GivenBankingSessionNullModelNotNull_ThenReturnModelCisNumber() {
		model = new HashMap<>();
		when(userSession.getBankingSession()).thenReturn(null);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerProfile().getPermId()).thenReturn(new BigInteger("12345001001"));
		//when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		//model = controller.setCisNumberInMap(userSession, model);
		assertTrue(model.containsKey("cisNumber"));
	}
	
	/**
     * When HasPendingPifExternalAccounts
     * Given Customer Profile is Null Then Return method is False
     */
	@Test
	public void WhenHasPendingPifExternalAccounts_GivenCustomerProfileIsNull_ThenReturnFalse() {
		when(userSession.getCustomerProfile()).thenReturn(null);
		assertFalse(controller.hasPendingPifExternalAccounts(userSession));
	}
	
	/**
	 * When HasPendingPifExternalAccounts
	 * Given Customer TaxId is null Then return method is false
	 */
	@Test
	public void WhenHasPendingPifExternalAccounts_GivenCustomerTaxIdIsNull_ThenReturnFalse() {
		when(profile.getTaxId()).thenReturn(null);		
		assertFalse(controller.hasPendingPifExternalAccounts(userSession));
	}

	/**
	 * When HasPendingPifExternalAccounts
	 * Given TaxId is empty then return method is false
	 */
	@Test
	public void WhenHasPendingPifExternalAccounts_GivenCustomerTaxIdIsEmpty_ThenReturnFalse() {
		when(profile.getTaxId()).thenReturn("");		
		assertFalse(controller.hasPendingPifExternalAccounts(userSession));
	}	

	/**
	 * When HasPendingPifExternalAccounts
	 * Given ExternalAccount status is pending, then return method is true
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void WhenHasPendingPifExternalAccounts_GivenExternalAccountStatusIsPending_ThenReturnTrue() {
		when(profile.getTaxId()).thenReturn("584947310");

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getTaxId()).thenReturn("584947310");
		when(customerService.getPIFExternalAccounts(any())).thenReturn(new ArrayList<PIFExternalAccount>(){/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		{
			add(pifExternalAccount);
		}});
		when(pifExternalAccount.getAccountStatus()).thenReturn(PIFAccountStatusTypes.PENDING);
		assertTrue(controller.hasPendingPifExternalAccounts(userSession));
	}

	/**
	 * When HasPendingPifExternalAccounts
	 * Given ExternalAccounts status is confirmed, then return method is false
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void WhenHasPendingPifExternalAccountss_GivenExternalAccountStatusIsConfirmed_ThenReturnFalse() throws Exception {
		when(profile.getTaxId()).thenReturn("584947310");
		when(customerService.getPIFExternalAccounts(any())).thenReturn(new ArrayList<PIFExternalAccount>() {
																		   /**
																			*
																			*/
																		   private static final long serialVersionUID = 1L;
		});
		controller.portal(request, response);
		//PowerMockito.verifyPrivate(controller, times(1)).invoke("wealthAdvisorInfo",userSession);
		assertNull(bankerInfo.getPhoneExt());
	}
	
	/**
	 * When HasPendingPifExternalAccounts
	 * Given ExternalAccounts with different status of pending, then return method is false
	 */
	@Test
	@Ignore
	public void WhenHasPendingPifExternalAccountss_GivenExternalAccountsWithDifferentStatusOfPending_ThenReturnFalse() {
		when(profile.getTaxId()).thenReturn("584947310");
		when(customerService.getPIFExternalAccounts(any())).thenReturn(new ArrayList<PIFExternalAccount>(){/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		{
			add(pifExternalAccount);
			add(pifExternalAccount);
			add(pifExternalAccount);
			add(pifExternalAccount);
			add(pifExternalAccount);
			add(pifExternalAccount);
			add(pifExternalAccount);
		}});
		when(pifExternalAccount.getAccountStatus()).thenReturn(PIFAccountStatusTypes.CONFIRMED)
				.thenReturn(PIFAccountStatusTypes.BLOCKED_1).thenReturn(PIFAccountStatusTypes.BLOCKED_2)
				.thenReturn(PIFAccountStatusTypes.BLOCKED_3).thenReturn(PIFAccountStatusTypes.BLOCKED_4)
				.thenReturn(PIFAccountStatusTypes.BLOCKED_5).thenReturn(PIFAccountStatusTypes.REJECTED);
		assertFalse(controller.hasPendingPifExternalAccounts(userSession));
	}
	
	/**
	 * When HasPendingPifExternalAccounts
	 * Given ExternalAccounts with different types of status including pending status, then return method is true
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void WhenHasPendingPifExternalAccountss_GivenExternalAccountsWithDifferentTypesStatusIncludingPending_ThenReturnTrue() {
		when(profile.getTaxId()).thenReturn("584947310");
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getTaxId()).thenReturn("584947310");
		when(customerService.getPIFExternalAccounts(any())).thenReturn(new ArrayList<PIFExternalAccount>(){/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		{
			add(pifExternalAccount);
			add(pifExternalAccount);
			add(pifExternalAccount);
			add(pifExternalAccount);
			add(pifExternalAccount);
			add(pifExternalAccount);
			add(pifExternalAccount);
			add(pifExternalAccount);
		}});
		when(pifExternalAccount.getAccountStatus()).thenReturn(PIFAccountStatusTypes.CONFIRMED)
		.thenReturn(PIFAccountStatusTypes.BLOCKED_1).thenReturn(PIFAccountStatusTypes.BLOCKED_2)
		.thenReturn(PIFAccountStatusTypes.BLOCKED_3).thenReturn(PIFAccountStatusTypes.BLOCKED_4)
		.thenReturn(PIFAccountStatusTypes.BLOCKED_5).thenReturn(PIFAccountStatusTypes.REJECTED)
		.thenReturn(PIFAccountStatusTypes.PENDING);
		assertTrue(controller.hasPendingPifExternalAccounts(userSession));
	}	
	
	/**
	 * When PortalController
	 * Given ExternalAccounts is pending, then model contains 'hasPendingPifExternalAccounts'
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void WhenPortalController__GivenExternalAccountStatusIsPending_ThenModelContainsHasPendingPifExternalAccounts() throws Exception {
		controller.setNonTransEnabledEstmts("test");
		controller.setNonTransEnabledInterestLetters("test");
		
		when(profile.getTaxId()).thenReturn("584947310");
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getTaxId()).thenReturn("584947310");
		when(customerService.getPIFExternalAccounts(any())).thenReturn(new ArrayList<PIFExternalAccount>(){/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		{
			add(pifExternalAccount);
		}});
		when(pifExternalAccount.getAccountStatus()).thenReturn(PIFAccountStatusTypes.PENDING);
		assertTrue(controller.hasPendingPifExternalAccounts(userSession));
		when(userSession.isDeviceChecked()).thenReturn(true);
		ModelAndView modelAndView =controller.portal(request, response);
		assertTrue(modelAndView.getModel().containsKey("hasPendingPifExternalAccounts"));
	}

	/**
	 * When PortalController
	 * Given ExternalAccounts is confirmed, then model doesn't contains 'hasPendingPifExternalAccounts'
	 */
	
	@Test
	@Ignore("TIENE ERRORES")
	public void WhenPortalController_GivenExternalAccountsStatusIsConfirmed_ThenModelDoesNotContainsHasPendingPifExternalAccounts() throws Exception {
		controller.setNonTransEnabledEstmts("test");
		controller.setNonTransEnabledInterestLetters("test");
		
		when(profile.getTaxId()).thenReturn("584947310");
		when(customerService.getPIFExternalAccounts(any())).thenReturn(new ArrayList<PIFExternalAccount>(){/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		{
			add(pifExternalAccount);
		}});
		when(pifExternalAccount.getAccountStatus()).thenReturn(PIFAccountStatusTypes.CONFIRMED);
		when(userSession.isDeviceChecked()).thenReturn(true);
		ModelAndView modelAndView =controller.portal(request, response);
		assertFalse(modelAndView.getModel().containsKey("hasPendingPifExternalAccounts"));
	}
	
	/**
	 * When PortalController
	 * Given ExternalAccounts is rejected, then model doesn't contains 'hasPendingPifExternalAccounts'
	 */
	
	@Test
	@Ignore("TIENE ERRORES")
	public void WhenPortalController_GivenExternalAccountsStatusIsRejected_ThenModelDoesNotContainsHasPendingPifExternalAccounts() throws Exception {
		controller.setNonTransEnabledEstmts("test");
		controller.setNonTransEnabledInterestLetters("test");
		
		when(profile.getTaxId()).thenReturn("584947310");
		when(customerService.getPIFExternalAccounts(any())).thenReturn(new ArrayList<PIFExternalAccount>(){/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		{
			add(pifExternalAccount);
		}});
		when(pifExternalAccount.getAccountStatus()).thenReturn(PIFAccountStatusTypes.REJECTED);
		when(userSession.isDeviceChecked()).thenReturn(true);
		ModelAndView modelAndView =controller.portal(request, response);
		assertFalse(modelAndView.getModel().containsKey("hasPendingPifExternalAccounts"));
	}
	
	/**
	 * When PortalController
	 * Given ExternalAccounts is BLOCKED_1, then model doesn't contains 'hasPendingPifExternalAccounts'
	 */
	
	@Test
	@Ignore("TIENE ERRORES")
	public void WhenPortalController_GivenExternalAccountsStatusIsBlcked1_ThenModelDoesNotContainsHasPendingPifExternalAccounts() throws Exception {
		controller.setNonTransEnabledEstmts("test");
		controller.setNonTransEnabledInterestLetters("test");
		
		when(profile.getTaxId()).thenReturn("584947310");
		when(customerService.getPIFExternalAccounts(any())).thenReturn(new ArrayList<PIFExternalAccount>(){/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		{
			add(pifExternalAccount);
		}});
		when(pifExternalAccount.getAccountStatus()).thenReturn(PIFAccountStatusTypes.BLOCKED_1);
		when(userSession.isDeviceChecked()).thenReturn(true);
		ModelAndView modelAndView =controller.portal(request, response);
		assertFalse(modelAndView.getModel().containsKey("hasPendingPifExternalAccounts"));
	}

	/**
	 * When PortalController
	 * Given ExternalAccounts is BLOCKED_2, then model doesn't contains 'hasPendingPifExternalAccounts'
	 */
	
	@Test
	@Ignore("TIENE ERRORES")
	public void WhenPortalController_GivenExternalAccountsStatusIsBlcked2_ThenModelDoesNotContainsHasPendingPifExternalAccounts() throws Exception {
		controller.setNonTransEnabledEstmts("test");
		controller.setNonTransEnabledInterestLetters("test");
		
		when(profile.getTaxId()).thenReturn("584947310");
		when(customerService.getPIFExternalAccounts(any())).thenReturn(new ArrayList<PIFExternalAccount>(){/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		{
			add(pifExternalAccount);
		}});
		when(pifExternalAccount.getAccountStatus()).thenReturn(PIFAccountStatusTypes.BLOCKED_2);
		when(userSession.isDeviceChecked()).thenReturn(true);
		ModelAndView modelAndView =controller.portal(request, response);
		assertFalse(modelAndView.getModel().containsKey("hasPendingPifExternalAccounts"));
	}	

	/**
	 * When PortalController
	 * Given ExternalAccounts is BLOCKED_3, then model doesn't contains 'hasPendingPifExternalAccounts'
	 */
	
	@Test
	@Ignore("TIENE ERRORES")
	public void WhenPortalController_GivenExternalAccountsStatusIsBlcked3_ThenModelDoesNotContainsHasPendingPifExternalAccounts() throws Exception {
		controller.setNonTransEnabledEstmts("test");
		controller.setNonTransEnabledInterestLetters("test");
		
		when(profile.getTaxId()).thenReturn("584947310");
		when(customerService.getPIFExternalAccounts(any())).thenReturn(new ArrayList<PIFExternalAccount>(){/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		{
			add(pifExternalAccount);
		}});
		when(pifExternalAccount.getAccountStatus()).thenReturn(PIFAccountStatusTypes.BLOCKED_3);
		when(userSession.isDeviceChecked()).thenReturn(true);
		ModelAndView modelAndView =controller.portal(request, response);
		assertFalse(modelAndView.getModel().containsKey("hasPendingPifExternalAccounts"));
	}
	
	/**
	 * When PortalController
	 * Given ExternalAccounts is BLOCKED_4, then model doesn't contains 'hasPendingPifExternalAccounts'
	 */
	
	@Test
	@Ignore("TIENE ERRORES")
	public void WhenPortalController_GivenExternalAccountsStatusIsBlcked4_ThenModelDoesNotContainsHasPendingPifExternalAccounts() throws Exception {
		controller.setNonTransEnabledEstmts("test");
		controller.setNonTransEnabledInterestLetters("test");
		
		when(profile.getTaxId()).thenReturn("584947310");
		when(customerService.getPIFExternalAccounts(any())).thenReturn(new ArrayList<PIFExternalAccount>(){/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		{
			add(pifExternalAccount);
		}});
		when(pifExternalAccount.getAccountStatus()).thenReturn(PIFAccountStatusTypes.BLOCKED_4);
		when(userSession.isDeviceChecked()).thenReturn(true);
		ModelAndView modelAndView =controller.portal(request, response);
		assertFalse(modelAndView.getModel().containsKey("hasPendingPifExternalAccounts"));
	}	
	
	/**
	 * When PortalController
	 * Given ExternalAccounts is BLOCKED_5, then model doesn't contains 'hasPendingPifExternalAccounts'
	 */
	
	@Test
	@Ignore("TIENE ERRORES")
	public void WhenPortalController_GivenExternalAccountsStatusIsBlcked5_ThenModelDoesNotContainsHasPendingPifExternalAccounts() throws Exception {
		controller.setNonTransEnabledEstmts("test");
		controller.setNonTransEnabledInterestLetters("test");
		
		when(profile.getTaxId()).thenReturn("584947310");
		when(customerService.getPIFExternalAccounts(any())).thenReturn(new ArrayList<PIFExternalAccount>(){/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		{
			add(pifExternalAccount);
		}});
		when(pifExternalAccount.getAccountStatus()).thenReturn(PIFAccountStatusTypes.BLOCKED_5);
		when(userSession.isDeviceChecked()).thenReturn(true);
		ModelAndView modelAndView =controller.portal(request, response);
		assertFalse(modelAndView.getModel().containsKey("hasPendingPifExternalAccounts"));
	}

	/**
	 * When portal
	 * Return flag MBSD3806 enabled
	 */
	@Test
	public void whenPortal_thenReturnPopupCookieFlagEnabled() throws Exception {

		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_CIBP)).thenReturn(Boolean.FALSE);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.ANDROID);
		when(userSession.isDeviceChecked()).thenReturn(true);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OUTREACH)).thenReturn(false);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3806)).thenReturn(true);
		cookieBannerCookie = null;
		when(CookieUtils.getCookie(any(), eq("bppr_dmp"))).thenReturn(cookieBannerCookie);

		ModelAndView modelView = controller.portal(request, response);

		assertEquals(modelView.getModel().get("MBSD3806"), "true");
	}
	
	/**
	 * When portal
	 * Return flag MBSD3806 disabled
	 */
	@Test
	@Ignore("WITH ERROR")
	public void whenPortal_GivenAgentTypeJSON_thenReturnPopupCookieFlagNotInModel() throws Exception {

		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_CIBP)).thenReturn(Boolean.FALSE);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
		when(userSession.isDeviceChecked()).thenReturn(true);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OUTREACH)).thenReturn(false);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3806)).thenReturn(false);
		

		ModelAndView modelView = controller.portal(request, response);

		assertFalse(modelView.getModel().containsKey("MBSD3806"));
	}
	
		

	/**
	 * When portal
	 * Return flag MBSD3806 enabled
	 */
	@Test
	public void whenPortal_thenReturnPopupCookieFlagDisabled() throws Exception {

		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_CIBP)).thenReturn(Boolean.FALSE);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.ANDROID);
		when(userSession.isDeviceChecked()).thenReturn(true);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.OUTREACH)).thenReturn(false);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3806)).thenReturn(false);
		cookieBannerCookie = null;
		when(CookieUtils.getCookie(any(), eq("bppr_dmp"))).thenReturn(cookieBannerCookie);

		ModelAndView modelView = controller.portal(request, response);

		assertEquals(modelView.getModel().get("MBSD3806"), "false");
	}

	/**
	 * When PortalController
	 * Given an Account with productID equal to PLNOC, then model must contain 'ghostAccounts'
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void whenPortal_GivenAccountWithProductIdPLNOC_thenModelContainsGhostAccounts () throws Exception {
		
		List<TVFrontendAccount> accountList = getDummyGhostAccountList();
		when(userSession.getAccounts()).thenReturn(accountList);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		ModelAndView modelAndView = controller.portal(request, response);
		
		assertTrue(modelAndView.getModel().containsKey("ghostAccounts"));
		
	}
	
	/**
	 * When PortalController
	 * Given an Account with productID not equal to PLNOC, then model doesn't contain 'ghostAccounts'.
	 */
	@Test
	public void whenPortal_GivenAccountWithProductIdNotPLNOC_thenModelDoesntContainGhostAccounts () throws Exception {
		
		List<TVFrontendAccount> accountList = getDummAccountListWithNoGhostAccounts();
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		when(userSession.getAccounts()).thenReturn(accountList);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		ModelAndView modelAndView = controller.portal(request, response);
		
		assertFalse(modelAndView.getModel().containsKey("ghostAccounts"));
		
	}

	/**
	 * When PortalController
	 * Given an Account with productID equal to VPPRE, then model must contain Elegible 'PremiaAccounts'
	 */
	@Test
	public void whenPortal_GivenAccountWithProductIdVPPRE_thenModelContainsPremiaAccounts () throws Exception {

		List<TVFrontendAccount> accountList = getDummyPremiaAccountList();
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		when(userSession.getAccounts()).thenReturn(accountList);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(customerService.getPremiaProducts()).thenReturn(getPremiaProductsList());
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		ModelAndView modelAndView = controller.portal(request, response);

		assertTrue(modelAndView.getModel().containsKey("viewPfm"));

	}


	/**
	 * When PortalController
	 * Given an Account with productID equal to VPPRE, then model must contain Elegible 'PremiaAccounts'
	 */
	@Test
	public void whenPortal_GivenPremiaAccounts_withFlagEnabled () throws Exception {

		List<TVFrontendAccount> accountList = getDummyPremiaAccountList();
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		when(userSession.getAccounts()).thenReturn(accountList);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(true);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.isHasPremia()).thenReturn(true);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(tvFrontendAccount);
		when(userSession.getPremiaAccountList()).thenReturn(getDummyGhostAccountList());
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		when(PremiaUtil.premiaSelectFrontEndAccount(any(), any(),any())).thenReturn(accountList);

		ModelAndView modelAndView = controller.portal(request, response);

	//	assertTrue(modelAndView.getModelMap().containsAttribute("premiaFlag"));
	}

	@Test
	public void whenPortal_GivenPremiaAccounts_withFlagDisabled () throws Exception {

		List<TVFrontendAccount> accountList = getDummyPremiaAccountList();
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		when(userSession.getAccounts()).thenReturn(accountList);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(false);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.getPremiaAccountList()).thenReturn(getDummyGhostAccountList());
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		ModelAndView modelAndView = controller.portal(request, response);

		assertFalse(modelAndView.getModel().containsKey("premiaMobileSubtype"));
		assertFalse(modelAndView.getModel().containsValue("premiaFlag"));

	}

	/**
	 * Get a list of dummy ghost accounts.
	 * @return
	 */
	private List<TVFrontendAccount> getDummyGhostAccountList() {
		
		TVFrontendAccount acc = new TVFrontendAccount("CCA", "4549548381926524", "R");
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription", 
				"dummyAccountNumberDescription", 
				"dummyAccountNumber", 
				"PLNOC", 
				"CCA", 
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		acc.setPrimaryAccount(true);
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);
		
		return accList;
		
	}
	
	/**
	 * Get a list of dummy accounts with no ghost accounts.
	 * @return
	 */
	private List<TVFrontendAccount> getDummAccountListWithNoGhostAccounts() {
		
		TVFrontendAccount acc = new TVFrontendAccount("CCA", "4549548381926524", "R");
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription", 
				"dummyAccountNumberDescription", 
				"dummyAccountNumber", 
				"", 
				"CCA", 
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);
		
		return accList;
		
	}
	
	@Test
	public void whenShowMarketplace_GivenUserSessionIsNull_ThenReturnFalse() throws Exception {
		UserSession userSession = null;
		assertFalse(Whitebox.invokeMethod(controller, "showMarketplace", userSession));
	}

	@Test
	public void whenShowMarketplace_GivenCustomerProfileIsNull_ThenReturnFalse() throws Exception {
		when(userSession.getCustomerProfile()).thenReturn(null);
		
		assertFalse(Whitebox.invokeMethod(controller, "showMarketplace", userSession));
	}
	
	/**
     * When showMarketplace
     * Given is Commercial Customer Then Return method is False
     */
	@Test
	@Ignore("WITH ERROR")
	public void whenShowMarketplace_GivenIsCommercialCustomer_ThenReturnFalse () throws Exception {
		controller.setMarketplaceCountries(new String[]{"PR", "VI"});
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));		
		//when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(RandomUtils.nextInt());
		when(userSession.isComercialCustomer()).thenReturn(true);
		when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("Y");
		when(userSession.hasBritishVirginIslandAccount()).thenReturn(false);
		when(userSession.getCountry()).thenReturn("PR");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.TRUE);
		
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("showMarketplace"));
		assertTrue(modelView.getModel().get("showMarketplace").equals(false));
	}
	
	/**
     * When showMarketplace
     * Given is Transactional Customer Then Return method is False
     */
	@Test
	@Ignore("WITH ERROR")
	public void whenShowMarketplace_GivenIsNonTransactional_ThenReturnFalse () throws Exception {
		controller.setMarketplaceCountries(new String[]{"PR", "VI"});
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));		
		//when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(RandomUtils.nextInt());
		when(userSession.isComercialCustomer()).thenReturn(false);
		when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("N");
		when(userSession.hasBritishVirginIslandAccount()).thenReturn(false);
		when(userSession.getCountry()).thenReturn("PR");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.TRUE);
		
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("showMarketplace"));
		assertTrue(modelView.getModel().get("showMarketplace").equals(false));
	}
	
	/**
     * When showMarketplace
     * Given is British Virgin Island Tortola BVI Account Then Return method is False
     */
	@Test
	@Ignore("WITH ERROR")
	public void whenShowMarketplace_GivenhasBritishVirginIslandAccount_ThenReturnFalse () throws Exception {
		controller.setMarketplaceCountries(new String[]{"PR", "VI"});

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));		
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(RandomUtils.nextInt());
		when(userSession.isComercialCustomer()).thenReturn(false);
		when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("Y");
		when(userSession.hasBritishVirginIslandAccount()).thenReturn(true);
		when(userSession.getCountry()).thenReturn("PR");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.TRUE);
		
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("showMarketplace"));
		assertTrue(modelView.getModel().get("showMarketplace").equals(false));
	}

	/**
     * When showMarketplace
     * Given Feature Flag False Then Return method is False
     */
	@Test
	@Ignore("WITH ERROR")
	public void whenShowMarketplace_GivenFeatureFlagFalse_ThenReturnFalse() throws Exception {
		controller.setMarketplaceCountries(new String[]{"PR", "VI"});
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));		
		//when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(RandomUtils.nextInt());
		when(userSession.isComercialCustomer()).thenReturn(false);
		when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("Y");
		when(userSession.hasBritishVirginIslandAccount()).thenReturn(false);
		when(userSession.getCountry()).thenReturn("PR");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.FALSE);
		
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("showMarketplace"));
		assertTrue(modelView.getModel().get("showMarketplace").equals(false));
	}
	
	@Test
	@Ignore("WITH ERROR")
	public void whenShowMarketplace_GivenAllConditionsEqualTrue_ThenReturnTrue() throws Exception {
		controller.setMarketplaceCountries(new String[]{"PR", "VI"});
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(RandomUtils.nextInt());
		when(userSession.isComercialCustomer()).thenReturn(false);
		when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("Y");
		when(userSession.hasBritishVirginIslandAccount()).thenReturn(false);
		when(userSession.getCountry()).thenReturn("PR");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.TRUE);
		
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("showMarketplace"));
		assertTrue(modelView.getModel().get("showMarketplace").equals(true));
	}
	
	/**
	 * When portal
	 * Given isWealth = true and MBFM169 flag is false
	 * Then verify private method createWealthAdvisorButtonEntitlement not invoked
	 * @throws Exception
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void whenPortal_GivenIsWealthTrueAndFlagFalse_thenCreateEntitlementNotInvoked () throws Exception {
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(true);
		when(userSession.getLanguage()).thenReturn("en");
		
	//	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFM169)).thenReturn(Boolean.FALSE);
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		controller.portal(request, response);
		PowerMockito.verifyPrivate(controller, times(0)).invoke("createWealthAdvisorButtonEntitlement",userSession);
	}
	
	/**
	 * When portal
	 * Given isWealth = true and MBFM169 flag is true and HasEntitlement False
	 * Then verify private method createWealthAdvisorButtonEntitlement invoked
	 * @throws Exception
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void whenPortal_GivenIsWealthTrueAndFlagTrueAndHasEntitlementFalse_thenCreateEntitlementInvoked () throws Exception {
		
		bankerInfo = new EmployeeInfoResponse();
		bankerInfo.setFullName("pepitopepon");
		bankerInfo.setEmployeeId("00024448");
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(true);
		when(userSession.getLanguage()).thenReturn("en");
		
	//	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFM169)).thenReturn(Boolean.TRUE);
		
		when(customerProfile.hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(false);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(globalEntitlement);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		when(userSession.getCustomerWealthPreferred(controller.getWealthCodes(),controller.getPreferredCodes())).thenReturn(custWealthPreferred);
		when(userSession.getPrivateBankerInfo(custWealthPreferred.getPrivateBankerNum())).thenReturn(bankerInfo);
		
		controller.portal(request, response);
		PowerMockito.verifyPrivate(controller, times(1)).invoke("createWealthAdvisorButtonEntitlement",userSession);
	}
	
	/**
	 * When portal
	 * Given isWealth = false and MBFM169 flag is true and HasEntitlement False
	 * Then verify private method createWealthAdvisorButtonEntitlement Not invoked
	 * @throws Exception
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void whenPortal_GivenIsWealthFalseAndFlagTrueAndHasEntitlementFalse_thenCreateEntitlementNotInvoked () throws Exception {
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(false);
		when(userSession.getLanguage()).thenReturn("en");
		
	//	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFM169)).thenReturn(Boolean.TRUE);
		
		when(customerProfile.hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(false);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(globalEntitlement);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		controller.portal(request, response);
		PowerMockito.verifyPrivate(controller, times(0)).invoke("createWealthAdvisorButtonEntitlement",userSession);
	}
	
	/**
	 * When portal
	 * Given isWealth = true and MBFM169 flag is true and HasEntitlement true
	 * Then verify private method createWealthAdvisorButtonEntitlement Not invoked
	 * @throws Exception
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void whenPortal_GivenIsWealthTrueAndFlagTrueAndHasEntitlementTrue_thenCreateEntitlementNotInvoked () throws Exception {
		
		bankerInfo = new EmployeeInfoResponse();
		bankerInfo.setFullName("pepitopepon");
		bankerInfo.setEmployeeId("00024448");
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(true);
		when(userSession.getLanguage()).thenReturn("en");
		
	//	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFM169)).thenReturn(Boolean.TRUE);
		
		when(customerProfile.hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(true);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(globalEntitlement);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		when(userSession.getCustomerWealthPreferred(controller.getWealthCodes(),controller.getPreferredCodes())).thenReturn(custWealthPreferred);
		when(userSession.getPrivateBankerInfo(custWealthPreferred.getPrivateBankerNum())).thenReturn(bankerInfo);
		
		controller.portal(request, response);
		PowerMockito.verifyPrivate(controller, times(0)).invoke("createWealthAdvisorButtonEntitlement",userSession);
	}
	
	/**
	 * When portal
	 * Given isWealth = true and MBFM169 flag is true and HasEntitlement true
	 * PrivateBanker is Null
	 * then advisorName not in model
	 * @throws Exception
	 */
	@Test
	@Ignore("TIENE ERRORES")
	public void whenPortal_GivenIsWealthTrueAndFlagTrueAndBankerInfoNull_thenInfoNotInModel () throws Exception {
		
		bankerInfo = null;
		
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(true);
		when(userSession.getLanguage()).thenReturn("en");
		
	//	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFM169)).thenReturn(Boolean.TRUE);
		
		when(customerProfile.hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(true);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(globalEntitlement);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		when(userSession.getCustomerWealthPreferred(controller.getWealthCodes(),controller.getPreferredCodes())).thenReturn(custWealthPreferred);
		when(userSession.getPrivateBankerInfo(custWealthPreferred.getPrivateBankerNum())).thenReturn(bankerInfo);
		
		ModelAndView modelView = controller.portal(request, response);
		PowerMockito.verifyPrivate(controller, times(0)).invoke("createWealthAdvisorButtonEntitlement",userSession);
		assertFalse(modelView.getModel().containsKey("advisorName"));
	}
  
  
    /**
	 * When portal
	 * Given Premia Flag Enabled
	 * Then Return MBCA1559 Flag on Portal Response ModelAndView
	 * @throws Exception
	 */
    @Test
	public void whenPortal_givenPremiaFlagEnabled_thenReturnFlagOnResponse() throws Exception {
		
    	when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		List<TVFrontendAccount> accountList = getDummyPremiaAccountList();
		when(userSession.getAccounts()).thenReturn(accountList);
		when(CookieUtils.getCookie(any(), eq("bppr_dmp"))).thenReturn(cookieBannerCookie);
		when(cookieBannerCookie.getValue()).thenReturn("1");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.TRUE);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.CSR);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("premiaFlag"));
	}

	/**
	 * When portal
	 * Given Premia Flag Enabled
	 * Then Return MBCA1559 Flag on Portal Response ModelAndView
	 * @throws Exception
	 */
	@Test
	public void whenPortal_givenPremiaFlagDisabled_thenReturnFlagOffResponse() throws Exception {
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		List<TVFrontendAccount> accountList = getDummyPremiaAccountList();
		when(userSession.getAccounts()).thenReturn(accountList);
		when(CookieUtils.getCookie(any(), eq("bppr_dmp"))).thenReturn(cookieBannerCookie);
		when(cookieBannerCookie.getValue()).thenReturn("1");
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(Boolean.FALSE);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.CSR);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("premiaFlag"));
	}
	
	/**
	 * When portal
	 * Given Premia Flag Is has premia TRUE
	 * Then Return MBCA2070 Flag on Portal Response ModelAndView
	 * @throws Exception
	 */
	@Test
	public void whenPortal_givenIsHasPremiaTrue_thenReturnFlagOnResponse() throws Exception {
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		List<TVFrontendAccount> accountList = getDummyPremiaAccountList();
		when(userSession.getAccounts()).thenReturn(accountList);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(true);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.isHasPremia()).thenReturn(true);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(tvFrontendAccount);
		when(userSession.getPremiaAccountList()).thenReturn(getDummyGhostAccountList());
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		when(tvFrontendAccount.isPrimaryAccount()).thenReturn(true);
		when(PremiaUtil.premiaSelectFrontEndAccount(any(), any(), any())).thenReturn(accountList);

		ModelAndView modelAndView = controller.portal(request, response);

		assertTrue(modelAndView.getModel().containsKey("premiaFlag"));
	}
	
	/**
	 * When portal
	 * Given Premia Flag Is has premia TRUE
	 * Then Return MBCA2070 Flag on Portal Response ModelAndView
	 * @throws Exception
	 */
	@Test
	public void whenPortal_givenIsHasPremiaTrueAndSizeList_thenReturnFlagOnResponse() throws Exception {
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		List<TVFrontendAccount> accountList =   getDummyPremiaAccountList();
		when(userSession.getAccounts()).thenReturn(accountList);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(true);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.isHasPremia()).thenReturn(true);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(tvFrontendAccount);
		when(userSession.getPremiaAccountList()).thenReturn(new ArrayList<>());
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		when(PremiaUtil.premiaSelectFrontEndAccount(any(), any(), any())).thenReturn(accountList);

		ModelAndView modelAndView = controller.portal(request, response);

		assertTrue(modelAndView.getModel().containsKey("premiaFlag"));
	}	
	
	/**
	 * When portal
	 * Given Premia Flag Is has premia False
	 * Then Return MBCA2070 Flag on Portal Response ModelAndView
	 * @throws Exception
	 */
	@Test
	public void whenPortal_givenIsHasPremiaFalse_thenReturnFlagOffResponse() throws Exception {
		
		List<TVFrontendAccount> accountList = getDummyPremiaAccountList();
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(5);
		when(userSession.getAccounts()).thenReturn(accountList);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(true);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.isHasPremia()).thenReturn(false);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(tvFrontendAccount);
		when(userSession.getPremiaAccountList()).thenReturn(getDummyGhostAccountList());
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		when(PremiaUtil.premiaSelectFrontEndAccount(any(), any(), any())).thenReturn(accountList);

		ModelAndView modelAndView = controller.portal(request, response);
		
		assertFalse(userSession.isHasPremia());
		assertFalse(modelAndView.getModel().containsKey("premiaMobileSubtype"));
	}
	
	/**
	 * When portal
	 * Given Premia Flag Is has premia False
	 * Then Return MBCA2070 Flag on Portal Response ModelAndView
	 * @throws Exception
	 */
	@Test
	public void whenPortal_givenIsHasPremiaFalseAndSizeList_thenReturnFlagOffResponse() throws Exception {
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		List<TVFrontendAccount> accountList = getDummyPremiaAccountList();
		when(userSession.getAccounts()).thenReturn(accountList);
		
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBCA1559)).thenReturn(true);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.isHasPremia()).thenReturn(false);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(tvFrontendAccount);
		when(userSession.getPremiaAccountList()).thenReturn(new ArrayList<>());

		ModelAndView modelAndView = controller.portal(request, response);
		
		assertFalse(modelAndView.getModel().containsKey("premiaMobileSubtype"));
		assertTrue(modelAndView.getModel().containsKey("premiaFlag"));
	}

	
	@Test
	@Ignore("TIENE ERRORES")
	public void whenGetMessageContentFromCampaigns_givenMyMessagesCampaigLinkable_thenReturnMessages() throws Exception {
		
		List<MyMessagesCampaign> campaignsList = new ArrayList<MyMessagesCampaign>();
		
		Campaign campaign = new Campaign();
		campaign.setId("1");
						
		MyMessagesCampaign messagesCampaign = new MyMessagesCampaign();	
		
		messagesCampaign.setLinkable(true);
		messagesCampaign.setEnglishAction("Click here for more info");
		messagesCampaign.setEnglishLink("http://www.google.com");
		messagesCampaign.setEnglishBody("Body");
		
		campaignsList.add(messagesCampaign);
		
		when(userSession.getLanguage()).thenReturn("en");
		
		Whitebox.invokeMethod(controller, 
				"getMessageContentFromCampaigns", campaignsList, userSession, "link" );
		
	//	PowerMockito.verifyStatic(CampaignUtils.class, times(1));
	//	CampaignUtils.getCampaignMessage(any(), anyString());
	}

	/**
	 * When portal
	 * Given isWealth = True and hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON) = True
	 * Then Model Containts showPrivateBankerButtonApp
	 * @throws Exception
	 */
	@Test
	@Ignore("Problema con userSession.getNonPremiaState()")
	public void whenPortal_GivenIsWealthTrueAndHasEntitlementTrue_thenModelContaintsShowPrivateBankerButtonApp () throws Exception {
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(true);
		when(customerProfile.hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(true);
		when(userSession.getLanguage()).thenReturn("en");
		
	//	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFM169)).thenReturn(Boolean.TRUE);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(globalEntitlement);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		ModelAndView modelAndView =controller.portal(request, response);
		assertTrue(modelAndView.getModel().containsKey("showPrivateBankerButtonApp"));
	}
	
	/**
	 * When portal
	 * Given isWealth = True and hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON) = True
	 * and EntitlementType.WEALTH_ADVISOR_BUTTON disabled
	 * Then Model not Containts showPrivateBankerButtonApp
	 * @throws Exception
	 */
	@Test
	@Ignore("Problema con userSession.getNonPremiaState()")
	public void whenPortal_GivenIsWealthTrueAndHasEntitlementTrue_thenModelNotContaintsShowPrivateBankerButtonApp () throws Exception {
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(true);
		when(customerProfile.hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(true);
		when(userSession.getLanguage()).thenReturn("en");
		
	//	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFM169)).thenReturn(Boolean.TRUE);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(globalEntitlement);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.DISABLED);
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		ModelAndView modelAndView =controller.portal(request, response);
		assertFalse(modelAndView.getModel().containsKey("showPrivateBankerButtonApp"));
	}
	
	/**
	 * When portal
	 * Given isWealth = True and hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON) = True
	 * and EntitlementType.WEALTH_ADVISOR_BUTTON enabled and UserAgentType.MOBILE_DESKTOP
	 * Then Model not Containts showPrivateBankerButtonApp
	 * @throws Exception
	 */
	@Test
	public void whenPortal_GivenUserAgentMOBILE_DESKTOP_thenModelNotContaintsShowPrivateBankerButtonApp () throws Exception {
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(89);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(true);
		when(customerProfile.hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(true);
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.MOBILE_DESKTOP);
	//	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFM169)).thenReturn(Boolean.TRUE);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(globalEntitlement);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
	//	when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		ModelAndView modelAndView =controller.portal(request, response);
		assertFalse(modelAndView.getModel().containsKey("showPrivateBankerButtonApp"));
	}
	

	
    /**
     * When portal
     * Given MBSE2513
     * Then Model Containts flagMBSE2513
     * @throws Exception
     */
    @Test
    public void whenPortal_GivenFlagMBSE2513_thenModelContaintsFlag () throws Exception {
        
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getProfileType()).thenReturn(profileType);
        when(profileType.getId()).thenReturn(89);
        when(userSession.getAccounts()).thenReturn(accounts);
        when(accounts.size()).thenReturn(1);
        when(accounts.iterator()).thenReturn(iterator);
        when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
        when(customerProfile.getBirthDate()).thenReturn(new Date());
        when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(true);
        when(customerProfile.hasEntitlement(EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(true);
        when(userSession.getLanguage()).thenReturn("en");
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.MOBILE_DESKTOP);
  //      when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFM169)).thenReturn(Boolean.TRUE);
        when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.WEALTH_ADVISOR_BUTTON)).thenReturn(globalEntitlement);
        when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);
        
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
        
//        when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
        
        ModelAndView modelAndView =controller.portal(request, response);
        assertTrue(modelAndView.getModel().containsKey("flagMBSE2513"));
    }
    

	/**
	 * Get a list of dummy ghost accounts.
	 * @return
	 */
	private List<TVFrontendAccount> getDummyPremiaAccountList() {

		TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("CCA", "4549548381926524", "R"));
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription",
				"dummyAccountNumberDescription",
				"dummyAccountNumber",
				"VPPRE",
				"CCA",
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		when(acc.getAccountProductId()).thenReturn("VPPRE");
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);

		return accList;
	}

	/**
	 * Get a list of dummy ghost accounts.
	 * @return
	 */
	private List<String> getPremiaProductsList() {

		return Stream.of("VPPRE", "APAXI", "APREG", "MWREG",
				"UAREG", "UVVSI", "UVREG", "VWVSI", "VWREG",
				"VBSG1", "VBSG2", "VBSEC", "MCEDG", "UVNOV",
				"VSNOV", "VSELT", "VSICN", "VSSEC", "VWNOS",
				"VBVBR", "VBAFP", "VBAFE", "PLNOC").collect(Collectors.toList());
	}

	/**
	 * when ReferenceData
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-22-2020
	 * @exception Exception error
	 */
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		List<TVFrontendAccount> accountList = getDummAccountListWithNoGhostAccounts();
		when(userSession.getAccounts()).thenReturn(accountList);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(ProfileType.Type.RETAIL.getId());

		ModelAndView result = controller.portal(request, response);
		assertEquals(Boolean.TRUE, result.getModel().get("cookieAnalyticEnabled"));
	}
	
	/**
	 * when PortalController
	 * Then Model Contains BSD3806 Flag
	 * @author Evertec ET57970
	 * @exception Exception error
	 */
	@Test
	@Ignore("WITH ERROR")
	public void whenPortalController_ThenModelContainsMBSD3806Flag () throws Exception {

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(3);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(false);
		when(userSession.getLanguage()).thenReturn("en");		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		
		ModelAndView modelAndView = controller.portal(request, response);
		assertTrue(modelAndView.getModel().containsKey("MBSD3806"));
	}
	
	/**
	 * when PortalController
	 * Then Model Contains BSD3806 Flag
	 * @author Evertec ET57970
	 * @exception Exception error
	 */
	@Test
	@Ignore("WITH ERROR")
	public void whenPortalController_ThenModelContainsMBSD3806FlagAsTrue () throws Exception {

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(3);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(false);
		when(userSession.getLanguage()).thenReturn("en");		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3806)).thenReturn(Boolean.TRUE);
		ModelAndView modelAndView = controller.portal(request, response);
		assertTrue(modelAndView.getModel().get("MBSD3806").equals("true"));
	}
	
	/**
	 * when PortalController
	 * Then Model Contains BSD3806 Flag
	 * @author Evertec ET57970
	 * @exception Exception error
	 */
	@Test
	@Ignore("WITH ERROR")
	public void whenPortalController_ThenModelContainsMBSD3806FlagAsFalse () throws Exception {

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(3);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.isWealth(controller.getWealthCodes(),controller.getPreferredCodes() )).thenReturn(false);
		when(userSession.getLanguage()).thenReturn("en");		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD3806)).thenReturn(Boolean.FALSE);
		ModelAndView modelAndView = controller.portal(request, response);
		assertTrue(modelAndView.getModel().get("MBSD3806").equals("false"));
	}
	
	

	/**
     * When showMarketplace
     * Given Customer Profile is Null Then Return method is False
     */
	@Test
	public void WhenShowMarketplace_GivenCustomerProfileIsNull_ThenReturnFalse() {
		when(userSession.getCustomerProfile()).thenReturn(null);
		assertFalse(controller.hasPendingPifExternalAccounts(userSession));
	}
	
	/**
     * When showMarketplace
     * Given Valid Account Then Return method is False
     */
	@Test
	@Ignore("WITH ERROR")
	public void WhenShowMarketplace_GivenValidAccount_ThenReturnTrue () throws Exception {
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));		
		//when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.isComercialCustomer()).thenReturn(false);
		when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("Y");
		when(userSession.hasBritishVirginIslandAccount()).thenReturn(false);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.TRUE);
		
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("showMarketplace"));
		assertTrue(modelView.getModel().get("showMarketplace").equals(true));
	}
	
	/**
     * When showMarketplace
     * Given is Commercial Customer Then Return method is False
     */
	@Test
	public void WhenshowMarketplace_GivenIsCommercialCustomer_ThenReturnFalse () throws Exception {
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));		
		//when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.isComercialCustomer()).thenReturn(true);
		when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("Y");
		when(userSession.hasBritishVirginIslandAccount()).thenReturn(false);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.TRUE);
		
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("showMarketplace"));
		assertTrue(modelView.getModel().get("showMarketplace").equals(false));
	}
	
	/**
     * When showMarketplace
     * Given is Transactional Customer Then Return method is False
     */
	@Test
	public void WhenShowMarketplace_GivenIsNonTransactional_ThenReturnFalse () throws Exception {

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));		
		//when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.isComercialCustomer()).thenReturn(false);
		when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("N");
		when(userSession.hasBritishVirginIslandAccount()).thenReturn(false);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.TRUE);
		
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("showMarketplace"));
		assertTrue(modelView.getModel().get("showMarketplace").equals(false));
	}
	
	/**
     * When showMarketplace
     * Given is British Virgin Island Tortola BVI Account Then Return method is False
     */
	@Test
	public void WhenShowMarketplace_GivenhasBritishVirginIslandAccount_ThenReturnFalse () throws Exception {

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));		
		//when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.isComercialCustomer()).thenReturn(false);
		when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("N");
		when(userSession.hasBritishVirginIslandAccount()).thenReturn(true);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.TRUE);
		
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("showMarketplace"));
		assertTrue(modelView.getModel().get("showMarketplace").equals(false));
	}

	/**
     * When showMarketplace
     * Given Feature Flag False Then Return method is False
     */
	@Test
	public void WhenShowMarketplace_GivenFeatureFlagFalse_ThenReturnFalse() throws Exception {
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));		
		//when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.isComercialCustomer()).thenReturn(false);
		when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("Y");
		when(userSession.hasBritishVirginIslandAccount()).thenReturn(false);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.FALSE);
		
		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("showMarketplace"));
		assertTrue(modelView.getModel().get("showMarketplace").equals(false));
	}
	
	/**
     * When showMarketplace
     * Given Feature Flag True Then Return method is True
     */
	@Test
	@Ignore("WITH ERROR")
	public void WhenShowMarketplace_GivenFeatureFlagTrue_ThenReturnFalse() throws Exception {
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		//when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.isComercialCustomer()).thenReturn(false);
		when(AccountUtils.getIsTransactional(userSession.getCustomerProfile())).thenReturn("Y");
		when(userSession.hasBritishVirginIslandAccount()).thenReturn(false);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.TRUE);

		ModelAndView modelView = controller.portal(request, response);

		assertTrue(modelView.getModel().containsKey("showMarketplace"));
		assertTrue(modelView.getModel().get("showMarketplace").equals(true));
	}

	/**
	 * when Portal
	 * Given Cookie and CookieEntitlement
	 * Then ReturnModelValueNull
	 */
	@Test
	public void whenPortal_ContainsCookieAndContainsCookieEntitlement_returnNull() throws Exception {

		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		List<TVFrontendAccount> accountList = getDummAccountListWithNoGhostAccounts();
		when(userSession.getAccounts()).thenReturn(accountList);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(ProfileType.Type.RETAIL.getId());
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.MOBILE_DESKTOP);
		when(customerProfile.getEntitlementByType(EntitlementType.COOKIES)).thenReturn(new CustomerEntitlement());
		when(CookieUtils.getCookie(any(), eq("bppr_dmp"))).thenReturn(cookieBannerCookie).thenReturn(cookieBannerCookie);
		when(cookieBannerCookie.getValue()).thenReturn("bppr_dmp");

		ModelAndView result = controller.portal(request, response);
		assertNull(result.getModel().get("MBSD3806"));
	}


	/**
	 * when Portal
	 * Given not Cookie and not CookieEntitlement
	 * Then ReturnModelValue
	 */
	@Test
	public void whenPortal_NotContainsCookieCookieAndNotContainsCookieEntitlement_returnNotNull () throws Exception {

		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		List<TVFrontendAccount> accountList = getDummAccountListWithNoGhostAccounts();
		when(userSession.getAccounts()).thenReturn(accountList);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(ProfileType.Type.RETAIL.getId());
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.MOBILE_DESKTOP);
		when(customerProfile.getEntitlementByType(EntitlementType.COOKIES)).thenReturn(null);
		when(CookieUtils.getCookie(any(), eq("bppr_dmp"))).thenReturn(null);

		ModelAndView result = controller.portal(request, response);
		assertNotNull(result.getModel().get("MBSD3806"));
	}

	/**
	 * when Portal
	 * Given Cookie and not CookieEntitlement
	 * Then ReturnModelValue null
	 */
	@Test
	public void whenPortal_ContainsCookieAndNotContainCookieEntitlement_returnNull () throws Exception {

		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		List<TVFrontendAccount> accountList = getDummAccountListWithNoGhostAccounts();
		when(userSession.getAccounts()).thenReturn(accountList);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(ProfileType.Type.RETAIL.getId());
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.MOBILE_DESKTOP);
		when(customerProfile.getEntitlementByType(EntitlementType.COOKIES)).thenReturn(null);
		when(CookieUtils.getCookie(any(), eq("bppr_dmp"))).thenReturn(cookieBannerCookie).thenReturn(cookieBannerCookie);
		when(userSession.getCustomerService().getCustomerProfileService()).thenReturn(customerProfileService);
		doNothing().when(customerProfileService).addEntitlement(any(CustomerProfile.class), any(CustomerEntitlement.class));

		when(cookieBannerCookie.getValue()).thenReturn("bppr_dmp");
		ModelAndView result = controller.portal(request, response);
		assertNull(result.getModel().get("MBSD3806"));
	}

	@Test
	public void whenPortal_givenUserSessionisHasRetirementTruePlan_thenReturnRetplan() throws Exception {

		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_CIBP)).thenReturn(Boolean.TRUE);

		when(customerProfile.getProfileType()).thenReturn(profileType);
        when(profileType.getId()).thenReturn(89);

        when(userSession.isHasRetirementPlan()).thenReturn(true);
        when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);

		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_MOBILE)).thenReturn(Boolean.TRUE);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.getLanguage()).thenReturn("en");


		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.RETIREMENT_PLAN)).thenReturn(globalEntitlement);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);

    	ModelAndView modelView = controller.portal(request, response);
    	assertTrue(modelView.getModel().containsKey("retplan"));

	}

	@Test
	public void whenPortal_givenUserSessionisHasRetirementfalsePlan_thenReturnRetplan() throws Exception {

		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_CIBP)).thenReturn(Boolean.TRUE);

		when(customerProfile.getProfileType()).thenReturn(profileType);
        when(profileType.getId()).thenReturn(89);

        when(userSession.isHasRetirementPlan()).thenReturn(false);
        when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);

		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));
		when(userSession.getLanguage()).thenReturn("en");


		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.RETIREMENT_PLAN)).thenReturn(globalEntitlement);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.DISABLED);

    	ModelAndView modelView = controller.portal(request, response);
    	assertTrue(!modelView.getModel().containsKey("retplan"));

	}

	@Test
	public void whenPortal_givenRetPlanFlagDisabled_thenNotReturnRetPlanOnResponse() throws Exception {

		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_CIBP)).thenReturn(Boolean.FALSE);

		when(customerProfile.getProfileType()).thenReturn(profileType);
        when(profileType.getId()).thenReturn(89);

        when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);

		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));

		when(userSession.getLanguage()).thenReturn("en");


    	ModelAndView modelView = controller.portal(request, response);
    	assertTrue(!modelView.getModel().containsKey("retplan"));

	}


	@Test
	public void whenPortal_givenOobFlagAndProfileHasEntitlementTypeRSA_thenReturnViewNamePortal() throws Exception {
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(StaticMessageSource.isFlagEnabled(Mockito.any())).thenReturn(true);
		when(userSession.getCheckedOobEnroll()).thenReturn(false);
		when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
		when(customerProfile.hasEntitlement(EntitlementType.MOBILE_CASH)).thenReturn(true);
		when(userSession.getMobileSdkData()).thenReturn(StringUtils.EMPTY);
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getPm_fp()).thenReturn(StringUtils.EMPTY);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn(StringUtils.EMPTY);
		when(RSAUtils.getDeviceRequest(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any(HttpServletRequest.class), Mockito.anyBoolean())).thenReturn(deviceRequest);
		when(AdaptiveAuthFacade.getInstance()).thenReturn(adaptiveAuthFacade);
		
		ModelAndView result = controller.portal(request, response);
		assertEquals("portal", result.getViewName());
	}

	@Test
	public void whenPortal_GivenIsDeviceCheckedFalse_ThenCreateCookieMBDEV() throws Exception {

		httpServletRequest = PowerMockito.mock(HttpServletRequest.class);
		customerSignonStats = PowerMockito.mock(CustomerSignonStats.class);
		customerInteractionEvent = PowerMockito.mock(CustomerInteractionEvent.class);
		smsProfile = PowerMockito.mock(SMSProfile.class);

		when(Utils.getRSALanguage(httpServletRequest)).thenReturn("sp");
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(userSession.isDeviceChecked()).thenReturn(Boolean.FALSE);

		GlobalEntitlement globalEnt = new GlobalEntitlement();
		globalEnt.setType(EntitlementType.ALERTS);
		CustomerEntitlement newEntitlement = new CustomerEntitlement();
		newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\"" + AlertType.NEW_DEVICE_PUSH.toString() + "\"/>\n"
						+ "</properties>");

		newEntitlement.setCustomerProfile(customerProfile);
		when(versionCookie.getValue()).thenReturn("mobile");
		when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
		when(DeviceUtils.getCustomerDevice(any())).thenReturn(CustomerDevices.PERSONAL);
		when(CookieUtils.getCookie(request, "client-version")).thenReturn(versionCookie);
		when(httpServletRequest.getCookies()).thenReturn(null);
		when(customerProfile.getSignonStats()).thenReturn(customerSignonStats);
		when(Utils.generateCustomerInteractionEvent(httpServletRequest)).thenReturn(customerInteractionEvent);
		when(userSession.getSmsProfile(any())).thenReturn(smsProfile);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		PowerMockito.doNothing().when(customerService).sendNewDeviceConfirmation(any(), any(), any());
		when(customerSignonStats.getAttributeData()).thenReturn(null);
		PowerMockito.doNothing().when(CookieUtils.class, "addCookie", response,
				60 * 60 * 24 * 365, DeviceUtils.DEVICE_TOKEN_COOKIE, "token");
		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		PowerMockito.doNothing().when(customerFacade).updateCustomerSignonStats(any(), any());
		when(WebUtils.getSessionAttribute(httpServletRequest, "showOOBSEC")).thenReturn("");

		controller.portal(httpServletRequest, response);
	}
	
	@Test
	public void whenPortal_GivenLoginUser_ThenReturnCookieValueInModel() throws Exception {
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(StaticMessageSource.isFlagEnabled(Mockito.any())).thenReturn(true);
		when(userSession.getCheckedOobEnroll()).thenReturn(false);
		when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
		when(customerProfile.hasEntitlement(EntitlementType.MOBILE_CASH)).thenReturn(true);
		when(userSession.getMobileSdkData()).thenReturn(StringUtils.EMPTY);
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getPm_fp()).thenReturn(StringUtils.EMPTY);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn(StringUtils.EMPTY);
		when(RSAUtils.getDeviceRequest(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any(HttpServletRequest.class), Mockito.anyBoolean())).thenReturn(deviceRequest);
		when(AdaptiveAuthFacade.getInstance()).thenReturn(adaptiveAuthFacade);
		
		String valueCookie = "tokenCookie";
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn(valueCookie);
		
		ModelAndView modelView = controller.portal(request, response);
		assertTrue(modelView.getModel().containsKey("rsa_cookie"));
		assertTrue(!modelView.getModel().get("rsa_cookie").equals(""));
		assertTrue(modelView.getModel().get("rsa_cookie").equals(valueCookie));
	}
	
	@Test
	public void whenPortal_GivenSetOOB_enroll_ThenReturnCookieValueInModel() throws Exception {
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(StaticMessageSource.isFlagEnabled(Mockito.any())).thenReturn(true);
		when(userSession.getCheckedOobEnroll()).thenReturn(false);
		when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
		when(customerProfile.hasEntitlement(EntitlementType.MOBILE_CASH)).thenReturn(true);
		when(userSession.getMobileSdkData()).thenReturn(StringUtils.EMPTY);
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getPm_fp()).thenReturn(StringUtils.EMPTY);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn(StringUtils.EMPTY);
		when(RSAUtils.getDeviceRequest(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any(HttpServletRequest.class), Mockito.anyBoolean())).thenReturn(deviceRequest);		
		when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MOBILE_CASH_ENROLLMENT)).thenReturn(Boolean.TRUE);

		String valueCookie = "tokenCookie";
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn(valueCookie);
		
		ModelAndView modelView = controller.portal(request, response);
		assertTrue(modelView.getModel().containsKey("rsa_cookie"));
		assertTrue(!modelView.getModel().get("rsa_cookie").equals(""));
		assertTrue(modelView.getModel().get("rsa_cookie").equals(valueCookie));
	}
	
	@Test
	public void whenPortal_GivenUSER_DEFINED_ThenReturnCookieValueInModel() throws Exception {
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(StaticMessageSource.isFlagEnabled(Mockito.any())).thenReturn(true);
		when(userSession.getCheckedOobEnroll()).thenReturn(false);
		when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
		when(customerProfile.hasEntitlement(EntitlementType.MOBILE_CASH)).thenReturn(true);
		when(userSession.getMobileSdkData()).thenReturn(StringUtils.EMPTY);
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getPm_fp()).thenReturn(StringUtils.EMPTY);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn(StringUtils.EMPTY);
		when(RSAUtils.getDeviceRequest(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any(HttpServletRequest.class), Mockito.anyBoolean())).thenReturn(deviceRequest);		
		when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.USER_DEFINED);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MOBILE_CASH_ENROLLMENT)).thenReturn(Boolean.TRUE);

		String valueCookie = "tokenCookie";
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn(valueCookie);
		
		ModelAndView modelView = controller.portal(request, response);
		assertTrue(modelView.getModel().containsKey("rsa_cookie"));
		assertTrue(!modelView.getModel().get("rsa_cookie").equals(""));
		assertTrue(modelView.getModel().get("rsa_cookie").equals(valueCookie));
	}
	
	@Test
	public void whenPortal_Given_Question_ThenReturnCookieValueInModel() throws Exception {
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(StaticMessageSource.isFlagEnabled(Mockito.any())).thenReturn(true);
		when(userSession.getCheckedOobEnroll()).thenReturn(false);
		when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
		when(customerProfile.hasEntitlement(EntitlementType.MOBILE_CASH)).thenReturn(true);
		when(userSession.getMobileSdkData()).thenReturn(StringUtils.EMPTY);
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getPm_fp()).thenReturn(StringUtils.EMPTY);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn(StringUtils.EMPTY);
		when(RSAUtils.getDeviceRequest(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any(HttpServletRequest.class), Mockito.anyBoolean())).thenReturn(deviceRequest);		
		when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.QUESTION);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MOBILE_CASH_ENROLLMENT)).thenReturn(Boolean.TRUE);

		String valueCookie = "tokenCookie";
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn(valueCookie);
		
		ModelAndView modelView = controller.portal(request, response);
		assertTrue(modelView.getModel().containsKey("rsa_cookie"));
		assertTrue(!modelView.getModel().get("rsa_cookie").equals(""));
		assertTrue(modelView.getModel().get("rsa_cookie").equals(valueCookie));
	}
	
	@Test
	public void whenPortal_Given_IPAD_ThenReturnCookieValueInModel() throws Exception {
		when(userSession.getAccounts()).thenReturn(accounts);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPAD);
		when(userSession.getPfmFirstTime()).thenReturn("Y");
		when(customerService.getEnableSignupCampInfo(CampaignType.ESTMT)).thenReturn(signupCampInfo);
		
		when(customerService.getGlobalEntitlement(EntitlementType.PFM)).thenReturn(globalEntitlement);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.DISABLED);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(StaticMessageSource.isFlagEnabled(Mockito.any())).thenReturn(true);
		when(userSession.getCheckedOobEnroll()).thenReturn(false);
		when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
		when(customerProfile.hasEntitlement(EntitlementType.MOBILE_CASH)).thenReturn(true);
		when(customerProfile.hasEntitlement(EntitlementType.EZSTMT)).thenReturn(true);
		when(userSession.getMobileSdkData()).thenReturn(StringUtils.EMPTY);
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getPm_fp()).thenReturn(StringUtils.EMPTY);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn(StringUtils.EMPTY);
		when(RSAUtils.getDeviceRequest(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any(HttpServletRequest.class), Mockito.anyBoolean())).thenReturn(deviceRequest);		
		when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.OOBPHONE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MOBILE_CASH_ENROLLMENT)).thenReturn(Boolean.TRUE);

		String valueCookie = "tokenCookie";
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn(valueCookie);
		
		ModelAndView modelView = controller.portal(request, response);
		assertTrue(modelView.getModel().containsKey("rsa_cookie"));
		assertTrue(!modelView.getModel().get("rsa_cookie").equals(""));
		assertTrue(modelView.getModel().get("rsa_cookie").equals(valueCookie));
	}
	
	@Test
	public void whenPortal_givenLoginFlow_thenCallApplyForInterruptionPageMobileVersion() throws Exception {
		when(userSession.getAccounts()).thenReturn(accounts);
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(StaticMessageSource.isFlagEnabled(Mockito.any())).thenReturn(true);
		when(userSession.getCheckedOobEnroll()).thenReturn(false);
		when(customerProfile.hasEntitlement(EntitlementType.RSA)).thenReturn(true);
		when(customerProfile.hasEntitlement(EntitlementType.MOBILE_CASH)).thenReturn(true);
		when(userSession.getMobileSdkData()).thenReturn(StringUtils.EMPTY);
		when(userSession.getLanguage()).thenReturn("en");
		when(userSession.getPm_fp()).thenReturn(StringUtils.EMPTY);
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(userSession.getDeviceRequest().getDeviceTokenCookie()).thenReturn(StringUtils.EMPTY);
		when(RSAUtils.getDeviceRequest(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any(HttpServletRequest.class), Mockito.anyBoolean())).thenReturn(deviceRequest);		
		when(customerService.rsaGetChallengeMethod(any(RsaChallengeRequest.class))).thenReturn(CredentialType.QUESTION);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MOBILE_CASH_ENROLLMENT)).thenReturn(Boolean.TRUE);

		String valueCookie = "tokenCookie";
		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceTokenCookie()).thenReturn(valueCookie);
		
		PowerMockito.verifyStatic( Mockito.times(1)); // Verify that the following mock method was called exactly 1 time
		PowerMockito.spy(OOBInterruptionForm.class);
		PowerMockito.doNothing().when(OOBInterruptionForm.class, "applyForInterruptionPageMobileVersion", any(), any());
		
		ModelAndView modelView = controller.portal(request, response);
	}
	
	
}
