/**
 * 
 */
package evertec.cibp.web.form;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.records.MortgageAccountRecord;
import bppr.tv.beans.accounts.types.LoanAccount;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.CCPortalPayments;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * @author et57970
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, StaticMessageSource.class, Utils.class})
public class AutoMakePaymentControllerIT {
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@Mock
	private CCPortalPayments ccPortalPayments;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private CustomerProfile profile;
	
	@Mock 
	private UserSession userSession;
	
	
	private TVFrontendAccount account;
	
	
	private LoanAccount loanAccount;
	
	
	private MortgageAccountRecord mlaRecord;
	
	
	private BankInformation bankInformation;
	
	@Mock
	private MessageSource messageSource;
	
	@Mock
    private BindException bindException;
	
	@InjectMocks
	private AutoMakePaymentController autoMakePaymentController;
	
	@Before
	public void setUp() throws Exception {		
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Utils.class);
		
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
        
        autoMakePaymentController = PowerMockito.spy(new AutoMakePaymentController());
		autoMakePaymentController.setMessageSource(messageSource);
		        				
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(profile);
		when(profile.getTaxId()).thenReturn("123456789");
	}
	
	private void configureAccountInfo () {
		
		 mlaRecord = new MortgageAccountRecord();
		 mlaRecord.setCurrentAmount(new BigDecimal(1350.2));
	     loanAccount = new LoanAccount();
	     loanAccount.setRecords(mlaRecord);
	     
	     bankInformation = new BankInformation();
	     bankInformation.setBankId("001");
	     
	     loanAccount.setBankInformation(bankInformation);
	     loanAccount.setAccountID("0500145842");
	     loanAccount.setSubType("MLA");
	     
		 account = new TVFrontendAccount("MLA", "0500145842", "");
		 account.setBackendObject(loanAccount);
	}
	
	@Test
	public void whenReferenceData_givenAccountSubtypeMLA_thenReturnMapWithAccountSubtype () throws Exception {
		
		configureAccountInfo();
		
		when(customerService.getConfirmedPIFExternalAccounts(any(String.class))).thenReturn(null);
		
		when(Utils.getAccountById(any(), any())).thenReturn(account);
		
        when(Utils.getRSALanguage(request)).thenReturn("en");
        when(messageSource.getMessage(any(), any(), any())).thenReturn("Message");
		
		autoMakePaymentController.referenceData(request, ccPortalPayments, bindException);
		
		verify(ccPortalPayments, times(1)).setCurrentAmountDue(any());
	}
	
	@Test
	public void whenReferenceData_givenAccountNull_thenReturnMapWithoutAccountSubtype () throws Exception {
			
		when(customerService.getConfirmedPIFExternalAccounts(any(String.class))).thenReturn(null);
		
		when(Utils.getAccountById(any(), any())).thenReturn(null);
				
        when(Utils.getRSALanguage(request)).thenReturn("en");
        when(messageSource.getMessage(any(), any(), any())).thenReturn("Message");
		
		autoMakePaymentController.referenceData(request, ccPortalPayments, bindException);
		
		verify(ccPortalPayments, times(0)).setCurrentAmountDue(any());
	}
	
	@Test
	public void whenOnSubmit_givenAccountSubtypeMLA_thenReturnMapWithAccountSubtype () throws Exception {
		
		when(customerService.getConfirmedPIFExternalAccounts(any(String.class))).thenReturn(null);
		
		when(Utils.getAccountById(any(), any())).thenReturn(account);
				
        when(Utils.getRSALanguage(request)).thenReturn("en");
        when(messageSource.getMessage(any(), any(), any())).thenReturn("Message");
        
        request.setParameter("formAction", "pay");
        
        ModelAndView mav = new ModelAndView();
        
        when(bindException.getTarget()).thenReturn(ccPortalPayments);
        
        PowerMockito.doReturn(mav).when((AutoMakePaymentController)autoMakePaymentController, "showForm", 
        		any(), any(), any(), any());
        
       	
        autoMakePaymentController.onSubmit(request, response, ccPortalPayments, bindException);
        verify(ccPortalPayments, times(0)).setCurrentAmountDue(any());
	}

}
