/**
 * 
 */
package evertec.cibp.web.form;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.Account;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.payment.TVPayment;
import evertec.cibp.core.services.responses.PaymentResponse;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.vs.beans.payments.Payee;
import evertec.cibp.vs.beans.payments.RecurringPayment;
import evertec.cibp.web.response.AccountTv;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.accounts.records.MortgageAccountRecord;
import bppr.tv.beans.accounts.types.LoanAccount;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.transfer.TVAccountTransferSource;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.beans.Frequency;
import evertec.cibp.web.form.bean.PaymentBean;
import evertec.cibp.web.form.bean.PaymentsListBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * @author et57970
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, WebUtils.class, Utils.class,TVFrontEndPayee.class, PaymentsServiceModule.class, PaymentsForm.class})
public class PaymentsFormIT extends AbstractModelAndViewTests {
	
	private HttpServletRequest request;
	private HttpServletResponse response;

	@InjectMocks
	@Spy
	private final PaymentsForm paymentForm = new PaymentsForm();

	@Mock
	private TVPayment tvPayment;

	@Mock
	private CustomerInteractionEvent interactionEvent;

	@Mock
	PaymentResponse paymentResponses;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private BindException error;
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock 
	private PaymentsListBean paymentListBean;

	@Mock
	private MessageSource messageSource;

	@Mock
	private BankingSession bankingSession;
	
	@Mock
	private TVFrontendAccount account;
	
	@Mock
	private TVFrontendAccount accountDetail;

	@Mock
	private TVFrontEndPayee tvFrontEndPayeeMock;
	
	@Mock
	private LoanAccount loanAccount;
	
	@Mock
	private MortgageAccountRecord mlaRecord;

	@Before
	public void setUp() throws Exception {
		
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(PaymentsServiceModule.class);
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();

		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(messageSource.getMessage("ebpp.module",null,new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(paymentListBean.getSingleAccountFrom()).thenReturn("multiple");
		
		paymentForm.setMessageSource(messageSource);
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(customerProfile.getTaxId()).thenReturn("123456789");
	}

	@Test
	public void whenprocessFinish_GivenPaymentsNotNull_ThenReturnPaymentEstimatedDate() throws Exception {
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);

		List<TVFrontEndPayee> payeesMock = new ArrayList();
		payeesMock.add(mock(TVFrontEndPayee.class));

		when(userSession.getPayessBasicEbpp(true)).thenReturn(payeesMock);

		Set<TVAccountTransferSource> accountsFrom = new HashSet();
		accountsFrom.add(mock(TVAccountTransferSource.class));

		when(userSession.getAccountsPaymentFrom()).thenReturn(accountsFrom);

		RecurringPayment recPayment = new RecurringPayment();
		TVPayment tvPayments= new TVPayment(recPayment);

		tvPayments.setEmail("");
		PaymentBean paymentBeanMock = mock(PaymentBean.class);

		when(tvPayment.getAmount()).thenReturn(BigDecimal.ONE);
		when(paymentBeanMock.getAmount()).thenReturn(BigDecimal.ONE);
		when(paymentResponses.getPayment()).thenReturn(tvPayments);
		when(paymentResponses.getStatus()).thenReturn(ResultStatus.FOUND);

		List<Integer> list = new ArrayList<>();
		list.add(2);
		List<TVFrontEndPayee> tvFrontEndPayeeList = new ArrayList<>();
		tvFrontEndPayeeList.add(tvFrontEndPayeeMock);
		List<TVFrontendAccount> accounts = new ArrayList<>();
		accounts.add(account);
		when(Utils.getUserSession(request)).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(interactionEvent);


		List<PaymentBean> payments = new ArrayList();
		PaymentBean paymentBean=new PaymentBean();
		paymentBean.setAccountFrom("");
		paymentBean.setPayeeid("");
		paymentBean.setAmount(new BigDecimal(5));
		paymentBean.setEffectiveDate(new Date());
		paymentBean.setRecurrent("");
		paymentBean.setInfinite("");
		payments.add(paymentBean);
		when(paymentListBean.getPayments()).thenReturn(payments);


		BankInformation bankInformation=new BankInformation();
		bankInformation.setBankId("");

		Account account1=new AccountTv();
		account1.setSubType("Claro");
		account1.setBankInformation(bankInformation);

		TVAccountTransferSource tvAccountTransferSource=new TVAccountTransferSource(new TVFrontendAccount(account1));

		when(Utils.getAccountSource(anyString(), anySet())).thenReturn(tvAccountTransferSource);

		when(Utils.getPayeebyId(anyString(),anyList())).thenReturn(tvFrontEndPayeeMock);

		when(tvFrontEndPayeeMock.getCustomerPayeeCodes()).thenReturn(list);


		TVPayment tvPayment = Mockito.spy( new TVPayment());
		RecurringPayment recurringPayment = Mockito.spy(new RecurringPayment());

		when(paymentResponses.getPayment()).thenReturn(tvPayment);

		tvPayment.setBackendObject(recurringPayment);

		TVFrontendAccount tvFrontendAccountVO = new TVFrontendAccount("IDA","001","01");
		tvFrontendAccountVO.setAccountKey("001");
		TVAccountTransferSource tvAccountTransferSourceVO= new TVAccountTransferSource(tvFrontendAccountVO);

		tvPayment.setSource(tvAccountTransferSourceVO);

		when(recurringPayment.getAmount()).thenReturn(BigDecimal.ZERO);

		when(PaymentsServiceModule.sendPayments(eq(customerProfile), eq(userSession), any(TVPayment.class),
				eq(interactionEvent), eq(customerService))).thenReturn(paymentResponses);

		when(Utils.getRSALanguage(request)).thenReturn("en");

		ModelAndView model = paymentForm.processFinish(request, response, paymentListBean, error);


		assertViewName(model, "makePayment");
		assertModelAttributeValue(model, "page", "2");

	}
	
	@Test
	public void whenReferenceData_GivenPayeeMortgage_ThenMethodGetAmountDueInfoInvoked() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(Boolean.TRUE);
		String payeeId ="1232312";
		LocalDate holiday = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY));
		PaymentBean paymentBeanMock = mock(PaymentBean.class);
		TVAccountTransferSource tvAccountTransferSourceMock = mock(TVAccountTransferSource.class);
		TVFrontendAccount tvFrontendAccountMock = mock(TVFrontendAccount.class);
		Payee payeeMock = mock(Payee.class);
		GlobalPayee globalPayeeMock = mock(GlobalPayee.class);
		TVFrontEndPayee payeeEndMock = mock(TVFrontEndPayee.class);
		payeeEndMock.setBackendObject(payeeMock);

		List<TVFrontEndPayee> payees = new ArrayList<TVFrontEndPayee>();
		payees.add(payeeEndMock);
		List<PaymentBean> paymentBeans = new ArrayList<PaymentBean>();
		paymentBeans.add(paymentBeanMock);
		when(paymentBeanMock.getAmount()).thenReturn(BigDecimal.ONE);
		when(paymentBeanMock.getPayeeid()).thenReturn(payeeId);
		when(payeeEndMock.getBillingAccount()).thenReturn("");
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getAccountsPaymentFrom()).thenReturn(new HashSet<>());
		when(userSession.getPayessBasicEbpp(true)).thenReturn(payees);
		when(customerService.telepagoIdValidForPIFTransfer("")).thenReturn(false);
		when(Utils.getPayeebyId(payeeId, payees)).thenReturn(payeeEndMock);

		when(Utils.getAccountByAccountNumber(any(),any())).thenReturn(account);
		BigDecimal amount = new BigDecimal(899);
		when(account.getSubtype()).thenReturn("MLA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(mlaRecord);
		when (mlaRecord.getCurrentAmount()).thenReturn(amount);
		
		when(paymentBeanMock.getEffectiveDate()).thenReturn(Date.from(holiday.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(paymentListBean.getSingleAccountFrom()).thenReturn("single");
		when(paymentListBean.getFavorites()).thenReturn(paymentBeans);
		
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(payeeEndMock.getGlobalPayee()).thenReturn(globalPayeeMock);
		when(payeeEndMock.getGlobalPayee().getRtNotification()).thenReturn("Y");
		when(payeeEndMock.getGlobalPayee().getRtIndicator()).thenReturn("N");
		List<Integer> payeeCodes = new ArrayList<>();
		payeeCodes.add(1234);
		when((payeeEndMock.getCustomerPayeeCodes())).thenReturn(payeeCodes);

		when((paymentBeanMock.getRecurrent())).thenReturn("recurrent");
		when((paymentBeanMock.getFrequency())).thenReturn(Frequency.MANUALLY);
		when(tvAccountTransferSourceMock.getAccount()).thenReturn(tvFrontendAccountMock);
		paymentForm.referenceData(request, paymentListBean, error, 1);
		verifyPrivate(paymentForm, times(1)).invoke("getAmountDueInfo", account, userSession);
	}
	
	@Test
	public void whenReferenceData_GivenPayeeMortgage_ThenMethodGetAmountDueFromAccountDetailsInvoked() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(Boolean.TRUE);
		String payeeId ="1232312";
		LocalDate holiday = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY));
		PaymentBean paymentBeanMock = mock(PaymentBean.class);
		TVAccountTransferSource tvAccountTransferSourceMock = mock(TVAccountTransferSource.class);
		TVFrontendAccount tvFrontendAccountMock = mock(TVFrontendAccount.class);
		Payee payeeMock = mock(Payee.class);
		GlobalPayee globalPayeeMock = mock(GlobalPayee.class);
		TVFrontEndPayee payeeEndMock = mock(TVFrontEndPayee.class);
		payeeEndMock.setBackendObject(payeeMock);

		List<TVFrontEndPayee> payees = new ArrayList<TVFrontEndPayee>();
		payees.add(payeeEndMock);
		List<PaymentBean> paymentBeans = new ArrayList<PaymentBean>();
		paymentBeans.add(paymentBeanMock);
		when(paymentBeanMock.getAmount()).thenReturn(BigDecimal.ONE);
		when(paymentBeanMock.getPayeeid()).thenReturn(payeeId);
		when(payeeEndMock.getBillingAccount()).thenReturn("");
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getAccountsPaymentFrom()).thenReturn(new HashSet<>());
		when(userSession.getPayessBasicEbpp(true)).thenReturn(payees);
		when(customerService.telepagoIdValidForPIFTransfer("")).thenReturn(false);
		when(Utils.getPayeebyId(payeeId, payees)).thenReturn(payeeEndMock);

		when(Utils.getAccountByAccountNumber(any(),any())).thenReturn(account);
		BigDecimal amount = new BigDecimal(899);
		when(account.getSubtype()).thenReturn("MLA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(mlaRecord);
		when (mlaRecord.getCurrentAmount()).thenReturn(null);
		
		MortgageAccountRecord mortRecord = PowerMockito.mock(MortgageAccountRecord.class);
		LoanAccount loan = PowerMockito.mock(LoanAccount.class);
		
		when (accountDetail.getBackendObject()).thenReturn(loan);
		when (loan.getRecords()).thenReturn(mortRecord);
		when (mortRecord.getCurrentAmount()).thenReturn(amount);
		
		when(customerService.getAccountDetails(customerProfile, bankingSession, account, true)).thenReturn(accountDetail);
		
		when(paymentBeanMock.getEffectiveDate()).thenReturn(Date.from(holiday.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(paymentListBean.getSingleAccountFrom()).thenReturn("single");
		when(paymentListBean.getFavorites()).thenReturn(paymentBeans);
		
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(payeeEndMock.getGlobalPayee()).thenReturn(globalPayeeMock);
		when(payeeEndMock.getGlobalPayee().getRtNotification()).thenReturn("Y");
		when(payeeEndMock.getGlobalPayee().getRtIndicator()).thenReturn("N");
		List<Integer> payeeCodes = new ArrayList<>();
		payeeCodes.add(1234);
		when((payeeEndMock.getCustomerPayeeCodes())).thenReturn(payeeCodes);

		when((paymentBeanMock.getRecurrent())).thenReturn("recurrent");
		when((paymentBeanMock.getFrequency())).thenReturn(Frequency.MANUALLY);
		when(tvAccountTransferSourceMock.getAccount()).thenReturn(tvFrontendAccountMock);
		paymentForm.referenceData(request, paymentListBean, error, 1);
		verifyPrivate(paymentForm, times(1)).invoke("getAmountDueFromAccountDetails", account, userSession);
	}
	
	@Test
	public void whenReferenceData_GivenPayeeMortgage_ThenMethodSetShowAmountDueMessageInvoked() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.REAL_TIME_NOTIFICATIONS_CLARO)).thenReturn(Boolean.TRUE);
		String payeeId ="1232312";
		LocalDate holiday = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY));
		PaymentBean paymentBeanMock = mock(PaymentBean.class);
		TVAccountTransferSource tvAccountTransferSourceMock = mock(TVAccountTransferSource.class);
		TVFrontendAccount tvFrontendAccountMock = mock(TVFrontendAccount.class);
		Payee payeeMock = mock(Payee.class);
		GlobalPayee globalPayeeMock = mock(GlobalPayee.class);
		TVFrontEndPayee payeeEndMock = mock(TVFrontEndPayee.class);
		payeeEndMock.setBackendObject(payeeMock);

		List<TVFrontEndPayee> payees = new ArrayList<TVFrontEndPayee>();
		payees.add(payeeEndMock);
		List<PaymentBean> paymentBeans = new ArrayList<PaymentBean>();
		paymentBeans.add(paymentBeanMock);
		when(paymentBeanMock.getAmount()).thenReturn(BigDecimal.ONE);
		when(paymentBeanMock.getPayeeid()).thenReturn(payeeId);
		when(payeeEndMock.getBillingAccount()).thenReturn("");
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getAccountsPaymentFrom()).thenReturn(new HashSet<>());
		when(userSession.getPayessBasicEbpp(true)).thenReturn(payees);
		when(customerService.telepagoIdValidForPIFTransfer("")).thenReturn(false);
		when(Utils.getPayeebyId(payeeId, payees)).thenReturn(payeeEndMock);

		when(Utils.getAccountByAccountNumber(any(),any())).thenReturn(account);
		BigDecimal amount = new BigDecimal(899);
		when(account.getSubtype()).thenReturn("MLA");
		when(account.getBackendObject()).thenReturn(loanAccount);
		when(loanAccount.getRecords()).thenReturn(mlaRecord);
		when (mlaRecord.getCurrentAmount()).thenReturn(null);
		
		MortgageAccountRecord mortRecord = PowerMockito.mock(MortgageAccountRecord.class);
		LoanAccount loan = PowerMockito.mock(LoanAccount.class);
		
		when (accountDetail.getBackendObject()).thenReturn(loan);
		when (loan.getRecords()).thenReturn(mortRecord);
		when (mortRecord.getCurrentAmount()).thenReturn(amount);
		
		when(customerService.getAccountDetails(customerProfile, bankingSession, account, true)).thenReturn(accountDetail);
		
		when(paymentBeanMock.getEffectiveDate()).thenReturn(Date.from(holiday.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(paymentListBean.getSingleAccountFrom()).thenReturn("single");
		when(paymentListBean.getFavorites()).thenReturn(paymentBeans);
		
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("The amount to be inserted must range between {0} and {1}.");
		when(paymentListBean.getPayments()).thenReturn(new ArrayList<>());
		when(payeeEndMock.getGlobalPayee()).thenReturn(globalPayeeMock);
		when(payeeEndMock.getGlobalPayee().getRtNotification()).thenReturn("Y");
		when(payeeEndMock.getGlobalPayee().getRtIndicator()).thenReturn("N");
		List<Integer> payeeCodes = new ArrayList<>();
		payeeCodes.add(1234);
		when((payeeEndMock.getCustomerPayeeCodes())).thenReturn(payeeCodes);

		when((paymentBeanMock.getRecurrent())).thenReturn("recurrent");
		when((paymentBeanMock.getFrequency())).thenReturn(Frequency.MANUALLY);
		when(tvAccountTransferSourceMock.getAccount()).thenReturn(tvFrontendAccountMock);
		paymentForm.referenceData(request, paymentListBean, error, 1);
		verify(payeeEndMock, times(1)).setShowAmountDueMessage(true);
	}


}
