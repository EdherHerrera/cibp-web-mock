package evertec.cibp.web.form;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.internal.WhiteboxImpl;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.util.WebUtils;

import com.rsa.csd.ws.CredentialType;
import com.rsa.csd.ws.DeviceRequest;
import com.teknowledge.tekportal.teksecurity.TekSecurityClient;

import evertec.cibp.campaign.loader.CampaignServiceLoader;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.form.bean.EvendorBean;
import evertec.cibp.web.form.bean.LoginBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.verifyNew;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, RSAUtils.class, Utils.class})
public class MakeVendorFormUT {

    @InjectMocks
    private MakeVendorForm makeVendorForm;

    @Mock
    private BindException errors;

    @Mock
    private EvendorBean command;

    @Mock
    private UserSession userSession;

    @Mock
    private CustomerServices customerService;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private DeviceRequest deviceRequest;


    private MockHttpServletResponse response;
    private MockHttpServletRequest request;

    @Before
    public void setUp() throws Exception {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(RSAUtils.class);
        PowerMockito.mockStatic(Utils.class);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(RSAUtils.getDeviceRequest(any(), any(), any())).thenReturn(deviceRequest);
    }



    @Test
    public void whenGetTelepagoErrorDescriptor_GivenNoEnglishLanguage_ReturnSpanishMessageSource() throws Exception {
    	
        String code = "code";
        String language = "other";
        
        makeVendorForm = new MakeVendorForm();
        Method method = makeVendorForm.getClass().getDeclaredMethod("getTelepagoErrorDescriptor",String.class,String.class);
        method.setAccessible(true);	
        method.invoke(makeVendorForm, code,language);
        
    }

    @Test
    public void whenReferenceData_givenReturnRedirectTrue_thenRedirectTrueInModel () throws Exception {
        when(customerService.getHolidays()).thenReturn(new ArrayList());
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getUsername()).thenReturn("username");
        when(customerProfile.getUserInterface()).thenReturn(UserInterface.COMMERCIAL);
        when(customerService.rsaGetChallengeMethod(any())).thenReturn(CredentialType.OOBPHONE);
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);

        Map<String, Object> modelReturn = new HashMap();
        modelReturn.put("redirectToBlock", true);
        when(Utils.oobCommercialChallenge(request, command, errors, 1)).thenReturn(modelReturn);
        when(command.getSingleAccountVendorFrom()).thenReturn("");

        Map<String, Object> modelview = makeVendorForm.referenceData(request, command, errors, 1);
        assertTrue((boolean)modelview.get("redirectToBlock"));
        assertEquals("username", modelview.get("username") );
    }

    @Test
    public void whenReferenceData_givenReturnRedirectFalse_thenRedirectFalseInModel () throws Exception {
        when(customerService.getHolidays()).thenReturn(new ArrayList());
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getUsername()).thenReturn("username");
        when(customerProfile.getUserInterface()).thenReturn(UserInterface.COMMERCIAL);
        when(customerService.rsaGetChallengeMethod(any())).thenReturn(CredentialType.OOBPHONE);
        when(userSession.getDeviceRequest()).thenReturn(deviceRequest);

        Map<String, Object> modelReturn = new HashMap();
        modelReturn.put("redirectToBlock", false);
        when(Utils.oobCommercialChallenge(request, command, errors, 1)).thenReturn(modelReturn);
        when(command.getSingleAccountVendorFrom()).thenReturn("");

        Map<String, Object> modelview = makeVendorForm.referenceData(request, command, errors, 1);
        assertFalse((boolean)modelview.get("redirectToBlock"));
        assertEquals("username", modelview.get("username") );
    }


}