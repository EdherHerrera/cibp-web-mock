package evertec.cibp.web.form;

import com.rsa.csd.ws.DeviceRequest;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.MultipleCustomerProfileResponse;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.enrollment.EnrollableAccount;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.ConnectionInfoValidationRequest;
import evertec.cibp.core.services.requests.EnrollmentInformationRequest;
import evertec.cibp.core.services.responses.ConnectionInfoValidationResponse;
import evertec.cibp.core.services.responses.ValidateAccountStatusResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.EnrollmentBean;
import evertec.cibp.web.form.validator.EnrollmentFormPageValidator;
import evertec.cibp.web.session.EnrollmentSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RSAUtils;
import evertec.cibp.web.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, WebUtils.class, CustomerFacade.class, RSAUtils.class, Utils.class, WebUtils.class, EnrollmentForm.class})
public class EnrollmentFormUT extends AbstractModelAndViewTests {

	private HttpServletRequest request;
	private HttpServletResponse response;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile profile;

	@Mock
	MultipleCustomerProfileResponse customerResponse;

	@Mock
	private CustomerProfileService customerProfileService;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerFacade customerFacade;

	@Mock
	private EnrollmentSession eS;

    @InjectMocks
	private EnrollmentForm controller = PowerMockito.spy(new EnrollmentForm());

	@Before
	public void setUp() throws Exception {

		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(RSAUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(WebUtils.class);
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
	}

	@Test
	public void whenProcessFinish_GivenESignFlagFalse_ThenReturnModelAndViewComfirmTrue() throws Exception {
		EnrollmentBean bean = new EnrollmentBean();
		bean.setUsername("USERNAME");
		bean.setDeviceRequest(new DeviceRequest());
		BindException error = new BindException(bean, "EnrollmentBean");


		when(WebUtils.getSessionAttribute(request, "enrollmentSession")).thenReturn(eS);
		when(eS.getSubmitedDob()).thenReturn(Calendar.getInstance());

		when(customerService.enrollUserwithRSA(any(EnrollmentInformationRequest.class) ,any( PreferredLanguage.class))).thenReturn(profile);
		when(profile.hasFlag(CustomerFlagType.ESIGN_ENFORCEMENT)).thenReturn(false);
		when(WebUtils.getCookie(request, "PMData")).thenReturn(new Cookie("PMData","PMData"));

		controller.setNtenrollmentaccounts("MLA,RVA,LEA");

		ModelAndView modelAndView = controller.processFinish(request, response, bean, error);

		assertViewName(modelAndView, "enrollment");
		assertModelAttributeValue(modelAndView, "confirm", true);
	}

	@Test
	public void whenProcessFinish_GivenESignFlagTrue_ThenReturnModelAndViewComfirmTrue() throws Exception {
		EnrollmentBean bean = new EnrollmentBean();
		bean.setUsername("USERNAME");
		bean.setDeviceRequest(new DeviceRequest());
		BindException error = new BindException(bean, "EnrollmentBean");


		when(WebUtils.getSessionAttribute(request, "enrollmentSession")).thenReturn(eS);
		when(eS.getSubmitedDob()).thenReturn(Calendar.getInstance());

		when(customerService.enrollUserwithRSA(any(EnrollmentInformationRequest.class) ,any( PreferredLanguage.class))).thenReturn(profile);
		when(profile.hasFlag(CustomerFlagType.ESIGN_ENFORCEMENT)).thenReturn(false);
		when(WebUtils.getCookie(request, "PMData")).thenReturn(new Cookie("PMData","PMData"));

		controller.setNtenrollmentaccounts("MLA,RVA,LEA");

		ModelAndView modelAndView = controller.processFinish(request, response, bean, error);

		assertViewName(modelAndView, "enrollment");
		assertModelAttributeValue(modelAndView, "confirm", true);
	}

	@Test
	@Ignore
	public void whenOnBindAndValidate_GivenUsernameInPassword_ThenReturnErrorTrue() throws Exception {
		EnrollmentBean bean = new EnrollmentBean();
		bean.setUsername("USERNAME");
		bean.setDeviceRequest(new DeviceRequest());
		BindException error = new BindException(bean, "EnrollmentBean");

		bean.setUsername("username");
		bean.setPassword("Junitusername1");
		bean.setPasswordconfirm("Junitusername1");

		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989)).thenReturn(true);
		when(customerService.isUsernameAvailable(bean.getUsername())).thenReturn(true);
		when(Utils.isValidPassword(bean.getPassword())).thenReturn(true);

		controller.onBindAndValidate(this.request, bean, error, 2);
		assertFalse(error.getFieldErrors().isEmpty());
		FieldError field = (FieldError)error.getFieldErrors().get(0);
		assertTrue(field.getDefaultMessage().equals("Password must not contains username"));
	}

	@Test
	public void whenOnBindAndValidate_NoGivenUsernameInPassword_ThenReturnErrorFalse() throws Exception {
		EnrollmentBean bean = new EnrollmentBean();
		bean.setUsername("USERNAME");
		bean.setDeviceRequest(new DeviceRequest());
		BindException error = new BindException(bean, "EnrollmentBean");

		bean.setUsername("username");
		bean.setPassword("Evertec2022**");
		bean.setPasswordconfirm("Evertec2022**");

		ConnectionInfoValidationResponse validateConnectionInformation = new ConnectionInfoValidationResponse();
		Map<Object, Object> questions = new HashMap<>();
		validateConnectionInformation.setUserNameValid(true);
		validateConnectionInformation.setQuestions1(questions);
		validateConnectionInformation.setQuestions2(questions);
		validateConnectionInformation.setQuestions3(questions);
		validateConnectionInformation.setUserNameValid(true);

		this.eS.setValidatedTaxId("580966011");

		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE1989)).thenReturn(true);
		when(RSAUtils.getDeviceRequest("", null, this.request)).thenReturn(new DeviceRequest());
		when(this.customerService.isUsernameAvailable(bean.getUsername())).thenReturn(true);
		when(this.customerService.validateConnectionInformation(any(ConnectionInfoValidationRequest.class))).thenReturn(validateConnectionInformation);
		when(Utils.getRSALanguage(this.request)).thenReturn("en");
		when(Utils.isValidPassword(bean.getPassword())).thenReturn(true);
		when(WebUtils.getSessionAttribute(this.request, "enrollmentSession")).thenReturn(this.eS);

		this.controller.onBindAndValidate(this.request, bean, error, 2);
		assertTrue(error.getFieldErrors().isEmpty());
	}

	@Test
	@Ignore
	public void whenOnBindAndValidate_GivenATHisOff_ThenReturnFalse() throws Exception {

		EnrollmentBean bean = new EnrollmentBean();
		bean.setUsername("USERNAME");
		bean.setDeviceRequest(new DeviceRequest());
		BindException error = new BindException(bean, "EnrollmentBean");

		bean.setUsername("username");
		bean.setPassword("Password1234");
		bean.setPasswordconfirm("Password1234");

		this.eS.setValidatedTaxId("598385828");

		ValidateAccountStatusResponse validStatus = new ValidateAccountStatusResponse();
		validStatus.setStatus(ValidateAccountStatusResponse.AccountValidationStatus.OFF);

		bean.setAccountInfoType("IDA");
		bean.setAccountnumber("387037213");
		bean.setAthnumber("0215020181334171");
		bean.setPin("1234");

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("245535951");
		profile.setEnrollmentAccountProductId("021");

		//String encryptedPin = pinTool.encrypt(bean.getPin());

		//when(pinTool.encrypt(any())).thenReturn("1234");
		when(this.customerService.validateDepositAccountInfo(any())).thenReturn(validStatus);
		when(WebUtils.getSessionAttribute(request, "enrollmentSession")).thenReturn(eS);
		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);

		this.controller.onBindAndValidate(this.request, bean, error, 1);
		FieldError field = (FieldError)error.getFieldErrors().get(0);
		assertTrue(field.getDefaultMessage().equals("Invalid Account"));
	}

//	@Test
//	@Ignore
//	public void whenProcessFinish_GivenEnrollmentIsAttemptedWithAthOff3Times_ThenCardIsOffCustomerFlagTypeIsAdded() throws Exception {
//		EnrollmentBean bean = new EnrollmentBean();
//		bean.setUsername("USERNAME");
//		bean.setDeviceRequest(new DeviceRequest());
//		BindException error = new BindException(bean, "EnrollmentBean");
//
//
//		when(WebUtils.getSessionAttribute(request, "enrollmentSession")).thenReturn(eS);
//		when(eS.getSubmitedDob()).thenReturn(Calendar.getInstance());
//		//Set counter to 3 so the validation will fail.
//		when(eS.getOffCounter()).thenReturn(3);
//
//		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ATH_ONOFF)).thenReturn(true);
//		when(profile.hasFlag(CustomerFlagType.ESIGN_ENFORCEMENT)).thenReturn(false);
//
//		when(EnrollmentServiceModule.getInstance()).thenReturn(enrollmentServiceModule);
//
//		when(enrollmentServiceModule.enrollUserwithRSA(any(CustomerServices.class), any(EnrollmentInformationRequest.class) ,any( PreferredLanguage.class))).thenReturn(profile);
//		when(profile.hasFlag(CustomerFlagType.CARD_IS_OFF)).thenReturn(false);
//
//		controller.setNtenrollmentaccounts("MLA,RVA,LEA");
//
//		LinkedList<CustomerFlagType> flagsSpy =  PowerMockito.spy(flags);
//
//		EnrollmentForm controllerSpy =  PowerMockito.spy(controller);
//
//		//enrollInfo = PowerMockito.mock(EnrollmentInfo.class);
//		PowerMockito.whenNew(LinkedList.class).withNoArguments().thenReturn(flagsSpy);
//		//PowerMockito.whenNew(List::List<CustomerFlagType>()).withAnyArguments().thenReturn(flags);
//
//
//
//		//List<CustomerFlagType> flagList = new ArrayList<CustomerFlagType>();
//
//		//when(enrollInfo.getCustomerFlagTypes()).thenReturn(flagList);
//
//		//EnrollmentInfo ei = Mockito.spy(new EnrollmentInfo());
//	//	PowerMockito.doReturn(true).when(profile.hasFlag(CustomerFlagType.CARD_IS_OFF));
//
//		//flags.add(CustomerFlagType.CARD_IS_OFF);
//
//		//PowerMockito.doReturn(flags).when(enrollInfo.getCustomerFlagTypes());
//
//	//	verify(flags, Mockito.times(1)).add(CustomerFlagType.CARD_IS_OFF);
//
//
//		//CustomerFlagType flagMock = Mockito.mock(CustomerFlagType.class);
//	    //doCallRealMethod().when(flags).add(CustomerFlagType.CARD_IS_OFF);
//
//		ModelAndView modelAndView = controllerSpy.processFinish(request, response, bean, error);
//
//		verify(flagsSpy).add(CustomerFlagType.CARD_IS_OFF);
//		//verify(flags, Mockito.times(1)).add(CustomerFlagType.CARD_IS_OFF);
//	}
	@Test
	@Ignore
	public void whenReferenceData_GivenEnrollmentBeanWithTakenUsername_ThenReturnModelAndViewWithUsernameOptions() throws Exception {
		EnrollmentBean bean = new EnrollmentBean();
		bean.setUsername("peyo03");
		bean.setDeviceRequest(new DeviceRequest());
		BindException error = new BindException(bean, "EnrollmentBean");

		GlobalEntitlement globalEntitlement = new GlobalEntitlement();
		globalEntitlement.setStatus(EntitlementStatus.ENABLED);

		when(WebUtils.getSessionAttribute(request, "enrollmentSession")).thenReturn(eS);
		when(eS.getSubmitedDob()).thenReturn(Calendar.getInstance());

		//when(enrollmentServiceModule.enrollUserwithRSA(any(CustomerServices.class), any(EnrollmentInformationRequest.class) ,any( PreferredLanguage.class))).thenReturn(profile);
		when(profile.hasFlag(CustomerFlagType.ESIGN_ENFORCEMENT)).thenReturn(false);
		when(WebUtils.getCookie(request, "PMData")).thenReturn(new Cookie("PMData","PMData"));
		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		//when(customerFacade.getGlobalEntitlement(any(EntitlementType.class), any(CustomerServices.class))).thenReturn(globalEntitlement);
		when(customerService.isUsernameAvailable("peyo03")).thenReturn(false);
		when(customerService.isUsernameAvailable("peyo04")).thenReturn(true);
		when(customerService.isUsernameAvailable("peyo05")).thenReturn(false);
		when(customerService.isUsernameAvailable("peyo06")).thenReturn(true);
		when(customerService.isUsernameAvailable("peyo07")).thenReturn(true);
		when(customerService.isUsernameAvailable("peyo08")).thenReturn(false);
		when(customerService.isUsernameAvailable("peyo09")).thenReturn(false);
		when(customerService.isUsernameAvailable("peyo10")).thenReturn(true);
		this.controller.setNtenrollmentaccounts("MLA,RVA,LEA");
		this.controller.onBindAndValidate(request, bean, error, 2);
		this.controller.setAgeRestriction(18);
		Map<String, Object> map =  this.controller.referenceData(request, bean, error, 2);

		LinkedHashMap userSugestions = (LinkedHashMap<String, String>) map.get("usernames");

		assertTrue(!userSugestions.isEmpty());
		assertTrue(userSugestions.size() == 4);
		assertTrue(userSugestions.get("peyo04").equals("peyo04"));
		assertTrue(userSugestions.get("peyo06").equals("peyo06"));
		assertTrue(userSugestions.get("peyo07").equals("peyo07"));
		assertTrue(userSugestions.get("peyo10").equals("peyo10"));
	}

	@After
	public void validate() {
		validateMockitoUsage();
	}
	@Test
	public void whenOnBindAndValidateMobileAndroid_bsmartAgreement_ThenReturnErrorFalse() throws Exception {
		EnrollmentBean bean = new EnrollmentBean();
		bean.setUsername("USERNAME");
		bean.setBsmartAgreement("C");
		bean.setDeviceRequest(new DeviceRequest());
		BindException error = new BindException(bean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		EnrollmentFormPageValidator enrollmentFormPageValidator = new EnrollmentFormPageValidator(4, UserAgentType.ANDROID);
		enrollmentFormPageValidator.validate(bean,error);
		this.controller.setValidator(enrollmentFormPageValidator);
		this.controller.validatePage( bean, error, 4,false);
		assertNull(error.getFieldError("bsmartAgreement"));
	}
	@Test
	public void whenOnBindAndValidateMobileIphone_bsmartAgreement_ThenReturnErrorFalse() throws Exception {
		EnrollmentBean bean = new EnrollmentBean();
		bean.setUsername("USERNAME");
		bean.setBsmartAgreement("C");
		bean.setDeviceRequest(new DeviceRequest());
		BindException error = new BindException(bean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		EnrollmentFormPageValidator enrollmentFormPageValidator = new EnrollmentFormPageValidator(4, UserAgentType.IPHONE);
		enrollmentFormPageValidator.validate(bean,error);
		this.controller.setValidator(enrollmentFormPageValidator);
		this.controller.validatePage( bean, error, 4,false);
		assertNull(error.getFieldError("bsmartAgreement"));
	}

	@Test
	public void whenOnBindAndValidateMobileAndroid_bsmartAgreement_ThenReturnErrorTrue() throws Exception {
		EnrollmentBean bean = new EnrollmentBean();
		bean.setUsername("USERNAME");

		bean.setDeviceRequest(new DeviceRequest());
		BindException error = new BindException(bean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		EnrollmentFormPageValidator enrollmentFormPageValidator = new EnrollmentFormPageValidator(4, UserAgentType.ANDROID);
		enrollmentFormPageValidator.validate(bean,error);
		this.controller.setValidator(enrollmentFormPageValidator);
		this.controller.validatePage( bean, error, 4,false);
		assertNotNull(error.getFieldError("bsmartAgreement"));
	}
	@Test
	public void whenOnBindAndValidateMobileIphone_bsmartAgreement_ThenReturnErrorTrue() throws Exception {
		EnrollmentBean bean = new EnrollmentBean();
		bean.setUsername("USERNAME");
		bean.setDeviceRequest(new DeviceRequest());
		BindException error = new BindException(bean, "EnrollmentBean");
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		EnrollmentFormPageValidator enrollmentFormPageValidator = new EnrollmentFormPageValidator(4, UserAgentType.IPHONE);
		enrollmentFormPageValidator.validate(bean,error);
		this.controller.setValidator(enrollmentFormPageValidator);
		this.controller.validatePage( bean, error, 4,false);
		assertNotNull(error.getFieldError("bsmartAgreement"));
	}

	@Test
	public void whenValidateIfRetailAccountExistCommercialEnrollment_thenReturnFalse() throws Exception {

		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("312361903", "IDA", "086", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("245535951");
		profile.setEnrollmentAccountProductId("021");
		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);

		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("598385828", "312361903", eS);
		assertFalse(hasDoubleProfile);


	}

	@Test
	public void whenValidateIfRetailAccountExistHasRetailEnrollment_thenReturnTrue() throws Exception {
		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("245535951", "IDA", "081", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("245535951");
		profile.setEnrollmentAccountProductId("021");
		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);

		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("598385828", "245535951", eS);
		assertTrue(hasDoubleProfile);
	}

	@Test
	public void whenValidateIfRetailAccountExistHasRetailEnrollmentAndNotHaveRetailProfile_thenReturnFalse() throws Exception {
		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("245535951", "IDA", "021", "A / O"));
		accounts.add(new EnrollableAccount("345815951", "IDA", "081", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.COMMERCIAL);
		profile.setEnrollmentAccountNumber("245535951");
		profile.setEnrollmentAccountProductId("021");
		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);

		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("598385828", "345815951", eS);
		assertFalse(hasDoubleProfile);
	}

	@Test
	public void whenSendEmailDoubleProfileAlertAndSsnHaveMultipleProfiles_thenCustomerServiceSendEmailMethodCalledMoreOneTime() throws Exception {
		String traceInfo = "Device: WEB&lt;br/&gt;Browser: Chrome&lt;br/&gt;Browser Version: 84.0.4147.105";
		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUsername("carlosnegocio");
		profile.setEmail("pepito@evertecinc.com");
		profile.setUserInterface(UserInterface.COMMERCIAL);
		CustomerProfile profile2 = new CustomerProfile();
		profile2.setUsername("pepe09");
		profile2.setEmail("maria@evertecinc.com");
		profile2.setUserInterface(UserInterface.RETAIL);
		CustomerProfile profile3 = new CustomerProfile();
		profile3.setUsername("carlosventas");
		profile3.setEmail("jose@evertecinc.com");
		profile3.setUserInterface(UserInterface.COMMERCIAL);
		cProfiles.add(profile);
		cProfiles.add(profile2);
		cProfiles.add(profile3);
         CustomerInteractionEvent customerEnvet = new CustomerInteractionEvent();
         customerEnvet.resetProperties();

		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(Utils.generateDblPrEmailTraceInfo(Mockito.any(), Mockito.any())).thenReturn(traceInfo);
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerEnvet);

		this.controller.sendEmailDbleProfileAlert(request, customerResponse);
		verify(customerService, times(3)).sendEmailAlertDoubleProfile(Mockito.any(), Mockito.any());

	}

	@Test
	public void whenSendEmailDoubleProfileAlertAndSsnHaveOneProfile_thenCustomerServiceSendEmailMethodCalledOneTime() throws Exception {
		String traceInfo = "Device: WEB&lt;br/&gt;Browser: Chrome&lt;br/&gt;Browser Version: 84.0.4147.105";
		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUsername("carlosnegocio");
		profile.setEmail("pepito@evertecinc.com");
		profile.setUserInterface(UserInterface.RETAIL);
		cProfiles.add(profile);
	    CustomerInteractionEvent customerEnvet = new CustomerInteractionEvent();
        customerEnvet.resetProperties();

		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(Utils.generateDblPrEmailTraceInfo(Mockito.any(), Mockito.any())).thenReturn(traceInfo);
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerEnvet);

		this.controller.sendEmailDbleProfileAlert(request, customerResponse);
		verify(customerService, times(1)).sendEmailAlertDoubleProfile(Mockito.any(), Mockito.any());

	}

	@Test
	public void whenValidateRetailAccountExistDoubleProfile_With_BsmartAccountEnrolled_ThenReturnTrue() throws Exception{

		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("245535951", "IDA", "086", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("245535951");
		profile.setEnrollmentAccountProductId("086");

		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);

		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("598385828", "245535951", eS);
		assertTrue(hasDoubleProfile);


	}

	@Test
	public void whenValidateRetailAccountExistDoubleProfile_With_BsmartAccount_Not_Enrolled_ThenReturnFalse() throws Exception{

		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("245535951", "IDA", "086", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "026", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("345535952");
		profile.setEnrollmentAccountProductId("026");

		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);

		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("598385828", "245535951", eS);
		assertFalse(hasDoubleProfile);


	}

	@Test
	public void whenVerifyDbleProfileStepOneDoubleProfile_Has_BsmartAccount_ThenReturnFalse() throws Exception{

		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("245535951", "IDA", "086", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "026", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "065", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("345535952");
		profile.setEnrollmentAccountProductId("026");

		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);

		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("598385828",null, eS);
		assertFalse(hasDoubleProfile);


	}

	@Test
	public void whenVerifyDbleProfileStepOneDoubleProfile_Not_Have_BsmartAccount_And_Only_Have_Retail_Accounts_ThenReturnTrue() throws Exception{

		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("248535941", "IDA", "031", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "026", "A / O"));
		accounts.add(new EnrollableAccount("445535935", "IDA", "065", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("345535952");
		profile.setEnrollmentAccountProductId("026");

		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);

		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("598385828",null, eS);
		assertTrue(hasDoubleProfile);


	}

	@Test
	public void whenIsBsmartCommercialEnrollment_Bsmart_Enroll_with_Existing_Retail_Then_Return_True() throws Exception {
		boolean commercialEnroll = true;
		List<EnrollableAccount> accounts = new ArrayList<>();
		List<EnrollableAccount> eAccounts = new ArrayList<>();

		accounts.add(new EnrollableAccount("248535941", "IDA", "031", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "026", "A / O"));
		accounts.add(new EnrollableAccount("445535935", "IDA", "086", "A / O"));
		eAccounts.add(new EnrollableAccount("248535941", "IDA", "031", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("248535941");
		profile.setEnrollmentAccountProductId("031");
		profile.setTaxId("580319125");

		cProfiles.add(profile);

		when(eS.getAccounts()).thenReturn(accounts);
		when(eS.getEnrollExistingAccounts()).thenReturn(eAccounts);
		when(eS.getValidatedTaxId()).thenReturn("580319125");

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);

		boolean bSmartEnroll = Whitebox.invokeMethod(controller, "isBsmartCommercialEnrollment",accounts.get(2).getAccountId()
				, eS);

		assertTrue(bSmartEnroll);
	}

	@Test
	public void whenIsBsmartCommercialEnrollment_Flexi_Enroll_with_Existing_Retail_Then_Return_False() throws Exception {
		boolean commercialEnroll = true;
		List<EnrollableAccount> accounts = new ArrayList<>();
		List<EnrollableAccount> eAccounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("248535941", "IDA", "031", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "026", "A / O"));
		accounts.add(new EnrollableAccount("445535935", "IDA", "021", "A / O"));
		eAccounts.add(new EnrollableAccount("248535941", "IDA", "031", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("248535941");
		profile.setEnrollmentAccountProductId("031");
		profile.setTaxId("580319125");

		cProfiles.add(profile);

		when(eS.getAccounts()).thenReturn(accounts);
		when(eS.getEnrollExistingAccounts()).thenReturn(eAccounts);
		when(eS.getValidatedTaxId()).thenReturn("580319125");

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);

		boolean bSmartEnroll = Whitebox.invokeMethod(controller, "isBsmartCommercialEnrollment",accounts.get(2).getAccountId()
				, eS);

		assertFalse(bSmartEnroll);
	}

	@Test
	public void whenIsBsmartCommercialEnrollment_Bsmart_Enroll_without_Existing_Retail_Then_Return_False() throws Exception {
		boolean commercialEnroll = false;
		List<EnrollableAccount> accounts = new ArrayList<>();
		List<EnrollableAccount> eAccounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("248535941", "IDA", "031", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "026", "A / O"));
		accounts.add(new EnrollableAccount("445535935", "IDA", "086", "A / O"));
		eAccounts.add(new EnrollableAccount("248535941", "IDA", "026", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.COMMERCIAL);
		profile.setEnrollmentAccountNumber("248535941");
		profile.setEnrollmentAccountProductId("026");
		profile.setTaxId("580319125");

		cProfiles.add(profile);

		when(eS.getAccounts()).thenReturn(accounts);
		when(eS.getEnrollExistingAccounts()).thenReturn(eAccounts);
		when(eS.getValidatedTaxId()).thenReturn("580319125");

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);

		boolean bSmartEnroll = Whitebox.invokeMethod(controller, "isBsmartCommercialEnrollment",accounts.get(2).getAccountId()
				, eS);

		assertFalse(bSmartEnroll);
	}

	public void whenValidateRetailAccountExistDoubleProfile_With_FlexiAccountEnrolled_ThenReturnTrue() throws Exception{

		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("245535951", "IDA", "021", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("245535951");
		profile.setEnrollmentAccountProductId("021");

		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);
		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("598385828", "245535951", eS);
		assertTrue(hasDoubleProfile);


	}

	@Test
	public void whenValidateRetailAccountExistDoubleProfile_With_FlexiAccount_Not_Enrolled_ThenReturnFalse() throws Exception{

		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("245535951", "IDA", "021", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "026", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("345535952");
		profile.setEnrollmentAccountProductId("026");

		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);

		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("598385828", "245535951", eS);
		assertFalse(hasDoubleProfile);


	}

	@Test
	public void whenVerifyDbleProfileStepOneDoubleProfile_Has_FlexiAccount_ThenReturnFalse() throws Exception{

		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("245535951", "IDA", "021", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "026", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "065", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("345535952");
		profile.setEnrollmentAccountProductId("026");

		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);

		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("598385828",null, eS);
		assertFalse(hasDoubleProfile);


	}

	@Test
	public void whenSendEmailDoubleProfileAlert_thenlogDbleProfileAttemptCalledOneTime() throws Exception {
		String traceInfo = "Device: WEB&lt;br/&gt;Browser: Chrome&lt;br/&gt;Browser Version: 84.0.4147.105";
		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUsername("carlosnegocio");
		profile.setEmail("pepito@evertecinc.com");
		profile.setUserInterface(UserInterface.RETAIL);
		cProfiles.add(profile);
	    CustomerInteractionEvent customerEnvet = new CustomerInteractionEvent();
        customerEnvet.resetProperties();

		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(Utils.generateDblPrEmailTraceInfo(Mockito.any(), Mockito.any())).thenReturn(traceInfo);
		when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerEnvet);

		this.controller.sendEmailDbleProfileAlert(request, customerResponse);
		//verify(controller, times(1)).lo   (Mockito.any(), Mockito.any());
		PowerMockito.verifyPrivate(controller, times(1)).invoke("logDbleProfileAttempt", profile, customerEnvet);
	}

	@Test
	public void whenValidateRetailAccountExistDoubleProfile_With_CcaAccount_Without_Retail_Account_Enrolled_ThenReturnFalse() throws Exception{

		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("0377810844504834", "CCA", "VSICN", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "086", "A / O"));
		accounts.add(new EnrollableAccount("4549210132983540", "CCA", "VWNOS", "1"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.COMMERCIAL);
		profile.setEnrollmentAccountNumber("345535952");
		profile.setEnrollmentAccountProductId("086");

		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);

		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("584714596", "0377810844504834", eS);
		assertFalse(hasDoubleProfile);


	}

	@Test
	public void whenValidateRetailAccountExistDoubleProfile_With_CcaAccount_With_Retail_Account_Enrolled_ThenReturnTrue() throws Exception{

		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("0377810844504834", "CCA", "VSICN", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "086", "A / O"));
		accounts.add(new EnrollableAccount("4549210132983540", "CCA", "VWNOS", "1"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("4549210132983540");
		profile.setEnrollmentAccountProductId("VWNOS");

		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);

		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("584714596", "0377810844504834", eS);
		assertTrue(hasDoubleProfile);


	}

	@Test
	public void whenValidateRetailAccountExistDoubleProfile_With_CcaAccount_Without_Retail_Account_Enrolled_StepOne_ThenReturnFalse() throws Exception{

		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("0377810844504834", "CCA", "VSICN", "A / O"));
		accounts.add(new EnrollableAccount("4549210132983540", "CCA", "VWNOS", "1"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.COMMERCIAL);
		profile.setEnrollmentAccountNumber("345535952");
		profile.setEnrollmentAccountProductId("086");

		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);

		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("584714596", null, eS);
		assertFalse(hasDoubleProfile);


	}

	@Test
	public void whenValidateRetailAccountExistDoubleProfile_With_CcaAccount_With_Retail_Account_Enrolled__StepOne_ThenReturnTrue() throws Exception{

		List<EnrollableAccount> accounts = new ArrayList<>();
		accounts.add(new EnrollableAccount("0377810844504834", "CCA", "VSICN", "A / O"));
		accounts.add(new EnrollableAccount("4549210132983540", "CCA", "VWNOS", "1"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("4549210132983540");
		profile.setEnrollmentAccountProductId("VWNOS");

		cProfiles.add(profile);

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);
		when(eS.getAccounts()).thenReturn(accounts);

		boolean hasDoubleProfile = this.controller.validateRetailAccountExist("584714596", null, eS);
		assertTrue(hasDoubleProfile);


	}

	@Test
	public void whenIsBsmartCommercialEnrollment_Bsmart_Enroll_with_Existing_Bsmart_Retail_Profile_Then_Return_True() throws Exception {
		boolean commercialEnroll = true;
		List<EnrollableAccount> accounts = new ArrayList<>();
		List<EnrollableAccount> eAccounts = new ArrayList<>();

		accounts.add(new EnrollableAccount("248535941", "IDA", "031", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "026", "A / O"));
		accounts.add(new EnrollableAccount("445535935", "IDA", "086", "A / O"));
		eAccounts.add(new EnrollableAccount("248535941", "IDA", "086", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.RETAIL);
		profile.setEnrollmentAccountNumber("248535941");
		profile.setEnrollmentAccountProductId("086");
		profile.setTaxId("580319125");

		cProfiles.add(profile);

		when(eS.getAccounts()).thenReturn(accounts);
		when(eS.getEnrollExistingAccounts()).thenReturn(eAccounts);
		when(eS.getValidatedTaxId()).thenReturn("580319125");

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);

		boolean bSmartEnroll = Whitebox.invokeMethod(controller, "isBsmartCommercialEnrollment",accounts.get(2).getAccountId()
				, eS);

		assertTrue(bSmartEnroll);
	}

	@Test
	public void whenIsBsmartCommercialEnrollment_Bsmart_Enroll_with_Existing_Bsmart_Comercial_Profile_Then_Return_False() throws Exception {
		boolean commercialEnroll = true;
		List<EnrollableAccount> accounts = new ArrayList<>();
		List<EnrollableAccount> eAccounts = new ArrayList<>();

		accounts.add(new EnrollableAccount("248535941", "IDA", "031", "A / O"));
		accounts.add(new EnrollableAccount("345535952", "IDA", "026", "A / O"));
		accounts.add(new EnrollableAccount("445535935", "IDA", "086", "A / O"));
		eAccounts.add(new EnrollableAccount("248535941", "IDA", "086", "A / O"));

		List<CustomerProfile> cProfiles = new ArrayList<>();
		CustomerProfile profile = new CustomerProfile();
		profile.setUserInterface(UserInterface.COMMERCIAL);
		profile.setEnrollmentAccountNumber("248535941");
		profile.setEnrollmentAccountProductId("086");
		profile.setTaxId("580319125");

		cProfiles.add(profile);

		when(eS.getAccounts()).thenReturn(accounts);
		when(eS.getEnrollExistingAccounts()).thenReturn(eAccounts);
		when(eS.getValidatedTaxId()).thenReturn("580319125");

		when(this.customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerService.getCustomerProfileService()).thenReturn(customerProfileService);
		when(customerProfileService.getCustomerProfileByFilter(Mockito.any())).thenReturn(customerResponse);
		when(customerResponse.getCustomerProfiles()).thenReturn(cProfiles);

		boolean bSmartEnroll = Whitebox.invokeMethod(controller, "isBsmartCommercialEnrollment",accounts.get(2).getAccountId()
				, eS);

		assertFalse(bSmartEnroll);
	}

}
