package evertec.cibp.web.form;

import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.types.DepositAccount;
import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TVFrontendDepositAccount;
import evertec.cibp.core.models.accountopening.AccountApplication;
import evertec.cibp.core.models.accountopening.AccountApplications;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.CustomerFlag;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.enrollment.UsernameReservation;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.form.bean.EbppStatusUpdateBean;
import evertec.cibp.web.session.UserSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.math.BigInteger;
import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class , CustomerServices.class, ApiCallerFactory.class})
public class EbppStatusUpdateIT extends AbstractModelAndViewTests {

    private EbppStatusUpdate form;

    private MockHttpServletRequest request;

    private HttpSession session;

    private UserSession userSession;

    private CustomerServices customerService;

    private CustomerProfile customerProfile;

    @Mock
    private ApiCaller apiCaller;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(StaticMessageSource.class);

        PowerMockito.mockStatic(ApiCallerFactory.class);
        when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

        form = new EbppStatusUpdate();
        request = new MockHttpServletRequest();
        session = new MockHttpSession();
        userSession = new UserSession();
        customerService = mock(CustomerServices.class) ;
        customerProfile = new CustomerProfile();
        customerProfile.setProfileId("22");
    }

    @Test
    public void whenActivateEstament_ThenReturnCompleteProcessTrue_EnableEntitlement() throws Exception{
        Set<CustomerFlag> customerFlagsSet = new HashSet<>();
        CustomerFlag customerFlag  =  new CustomerFlag(CustomerFlagType.PENDING_ACCOUNT_APPLICATION);

        MessageSource messageSource = mock(MessageSource.class);

        when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn(EntitlementStatus.ENABLED.toString());

        when(messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.one", null, new Locale("en"))).thenReturn("At this moment we can't activate your e-Statement");

        when(messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.two", null, new Locale("en"))).thenReturn("  , please try again later. Sorry for the inconvenience.");

        customerFlagsSet.add(customerFlag);
        customerProfile.setCustomerFlags(customerFlagsSet);

        BankingSession bankingSession = new BankingSession(BigInteger.ONE,"");

        List<TVFrontendAccount> tvFrontendAccountList = new ArrayList<>();

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.TRUE);

        DepositAccount depositAccount = new DepositAccount();

        depositAccount.setSectionIndicator(null);
        Account accountD = (Account) depositAccount;

        accountD.setSubType("IDA");
        accountD.setAccountID("123");

        Account account = new Account() { };

        account.setSubType("IDA");
        account.setAccountID("123");

        TVFrontendAccount tvFrontendAccount = new TVFrontendAccount(accountD);
        tvFrontendAccount.setFrontEndId("123");

        tvFrontendAccountList.add(tvFrontendAccount);

        List<TVFrontendDepositAccount> tvFrontendDepositAccounts = new ArrayList<>();

        TVFrontendDepositAccount tvFrontendDepositAccount = new TVFrontendDepositAccount(accountD);

        tvFrontendDepositAccount.setStmtDisposition("M");

        tvFrontendDepositAccount.setFrontEndId("123");

        tvFrontendDepositAccounts.add(tvFrontendDepositAccount);

        userSession.setIdaAccounts(tvFrontendDepositAccounts);

        userSession.setBankingSession(bankingSession);

        userSession.setAccounts(tvFrontendAccountList);
        userSession.setEbpp2UserIdLookedUp(true);
        userSession.setEbpp2UserId("123");

        UsernameReservation usernameReservation = new UsernameReservation();

        usernameReservation.setUsername("gdiazlander");

        userSession.setUsernameReservation(usernameReservation);

        List<AccountApplication> accountApplicationList = new ArrayList<>();

        accountApplicationList.add(new AccountApplication());

        AccountApplications accountApplications = mock(AccountApplications.class);
        accountApplications.setCompletedApplications(accountApplicationList);


        when(customerService.getPendingApplications(userSession.getCustomerProfile(),true)).thenReturn(accountApplications);

        when(customerService.getCustomerAccounts(customerProfile,accountApplications,bankingSession,true)).thenReturn(tvFrontendAccountList);

        userSession.setCustomerProfile(customerProfile);
        userSession.setCustomerService(customerService);
        session.setAttribute("userSession",userSession);
        request.setSession(session);

        form.setMessageSource(messageSource);

        HttpServletResponse response = mock(HttpServletResponse.class);

        EbppStatusUpdateBean ebppStatusUpdateBean = new EbppStatusUpdateBean();
        ebppStatusUpdateBean.setAction(EbppStatusUpdateBean.ACTION.UPDATE_ESTMT_STATUS);
        ebppStatusUpdateBean.setId("123");

        BindException errors = new BindException(ebppStatusUpdateBean, "EbppStatusUpdateBean");

        ModelAndView model = form.onSubmit(request,response,ebppStatusUpdateBean,errors);

        assertEquals(model.getModelMap().get("completeprocess"), true);
    }

    @Test
    public void whenActivateEstament_ThenReturnCompleteProcessTrue_DisableEntitlement() throws Exception{
        Set<CustomerFlag> customerFlagsSet = new HashSet<>();
        CustomerFlag customerFlag  =  new CustomerFlag(CustomerFlagType.PENDING_ACCOUNT_APPLICATION);

        MessageSource messageSource = mock(MessageSource.class);

        when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn(EntitlementStatus.DISABLED.toString());

        when(messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.one", null, new Locale("en"))).thenReturn("At this moment we can't activate your e-Statement");

        when(messageSource.getMessage("ebppstatusupdate.error.message.activation.estament.part.two", null, new Locale("en"))).thenReturn("  , please try again later. Sorry for the inconvenience.");

        customerFlagsSet.add(customerFlag);
        customerProfile.setCustomerFlags(customerFlagsSet);

        BankingSession bankingSession = new BankingSession(BigInteger.ONE,"");

        List<TVFrontendAccount> tvFrontendAccountList = new ArrayList<>();

        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.TRUE);

        DepositAccount depositAccount = new DepositAccount();

        depositAccount.setSectionIndicator(null);
        Account accountD = (Account) depositAccount;

        accountD.setSubType("IDA");
        accountD.setAccountID("123");

        Account account = new Account() { };

        account.setSubType("IDA");
        account.setAccountID("123");

        TVFrontendAccount tvFrontendAccount = new TVFrontendAccount(accountD);
        tvFrontendAccount.setFrontEndId("123");

        tvFrontendAccountList.add(tvFrontendAccount);

        List<TVFrontendDepositAccount> tvFrontendDepositAccounts = new ArrayList<>();

        TVFrontendDepositAccount tvFrontendDepositAccount = new TVFrontendDepositAccount(accountD);

        tvFrontendDepositAccount.setStmtDisposition("M");

        tvFrontendDepositAccount.setFrontEndId("123");

        tvFrontendDepositAccounts.add(tvFrontendDepositAccount);

        userSession.setIdaAccounts(tvFrontendDepositAccounts);

        userSession.setBankingSession(bankingSession);

        userSession.setAccounts(tvFrontendAccountList);
        userSession.setEbpp2UserIdLookedUp(true);
        userSession.setEbpp2UserId("123");

        UsernameReservation usernameReservation = new UsernameReservation();

        usernameReservation.setUsername("gdiazlander");

        userSession.setUsernameReservation(usernameReservation);

        List<AccountApplication> accountApplicationList = new ArrayList<>();

        accountApplicationList.add(new AccountApplication());

        AccountApplications accountApplications = mock(AccountApplications.class);
        accountApplications.setCompletedApplications(accountApplicationList);


        when(customerService.getPendingApplications(userSession.getCustomerProfile(),true)).thenReturn(accountApplications);

        when(customerService.getCustomerAccounts(customerProfile,accountApplications,bankingSession,true)).thenReturn(tvFrontendAccountList);

        userSession.setCustomerProfile(customerProfile);
        userSession.setCustomerService(customerService);
        session.setAttribute("userSession",userSession);
        request.setSession(session);

        form.setMessageSource(messageSource);

        HttpServletResponse response = mock(HttpServletResponse.class);

        EbppStatusUpdateBean ebppStatusUpdateBean = new EbppStatusUpdateBean();
        ebppStatusUpdateBean.setAction(EbppStatusUpdateBean.ACTION.UPDATE_ESTMT_STATUS);
        ebppStatusUpdateBean.setId("123");

        BindException errors = new BindException(ebppStatusUpdateBean, "EbppStatusUpdateBean");

        ModelAndView model = form.onSubmit(request,response,ebppStatusUpdateBean,errors);

        assertEquals(model.getModelMap().get("completeprocess"), true);
    }

}
