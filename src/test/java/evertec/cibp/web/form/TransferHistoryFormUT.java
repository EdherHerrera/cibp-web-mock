package evertec.cibp.web.form;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import javax.servlet.http.HttpServletRequest;
import java.util.GregorianCalendar;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.TransferSearchBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, CustomerFacade.class, TransferSearchBean.class, StaticMessageSource.class, Utils.class, Utils.class, GregorianCalendar.class})
public class TransferHistoryFormUT extends AbstractModelAndViewTests{
	
	@Mock
	private UserSession userSession;

	@Mock
	private CustomerFacade customerFacade;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private TransferSearchBean searchBean;
	

	@Mock
	private MessageSource messageSource;

	@InjectMocks
	private TransferHistoryForm controller;

	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(GregorianCalendar.class);
		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
	}
	
	@Test
	public void whenReferenceData_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValue() throws Exception {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(Utils.getRSALanguage(any(HttpServletRequest.class))).thenReturn("es");
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getCustomerService()).thenReturn(customerService);	

	}
	
	@Test
	public void whenGetTelepagoErrorDescriptor_GivenMessageError_ThenReturnMessageErrorSucces() throws Exception {
		controller.setMessageSource(messageSource);
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("OK");
		String valueValidate = Whitebox.invokeMethod(controller, "getTelepagoErrorDescriptor", "ebpp.module", "ens");
		assertEquals(valueValidate, "OK");
	}

}
