package evertec.cibp.web.form;

import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.ActivationCardAccountResponse;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.form.bean.ActivateAthIntBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;
import java.util.*;

import static org.mockito.Mockito.*;


@RunWith(PowerMockRunner.class)
@PrepareForTest({CustomerProfile.class, ActivateAthIntBean.class, UserSession.class, BankingSession.class,
        CustomerServices.class, ActivationCardAccountResponse.class, TVFrontendAccount.class, CustomerInteractionEvent.class,
        WebUtils.class, Utils.class, TVFrontendAccount.class})
public class ActivateAthIntFormUT extends AbstractModelAndViewTests {

    private static final String TEST = "TEST";
    private static final String ID = "ID";
    private static final String VIEW = "activateAthInt";

    private Map<Object,Object> map;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private ActivateAthIntBean activateAthIntBean;

    @Mock
    private UserSession userSession;

    @Mock
    private BankingSession bankingSession;

    @Mock
    private CustomerServices customerServices;

    @Mock
    private ActivationCardAccountResponse activationCardAccountResponse;

    @Mock
    private TVFrontendAccount plastics;

    @Mock
    private TVFrontendAccount depositAccount;

    @Mock
    private TVFrontendAccount account;

    @Mock
    private CustomerInteractionEvent customerInteractionEvent;

    @Mock
    private BindException errors;

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @InjectMocks
    private ActivateAthIntForm controller;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(CustomerProfile.class);
        PowerMockito.mockStatic(ActivateAthIntBean.class);
        PowerMockito.mockStatic(UserSession.class);
        PowerMockito.mockStatic(BankingSession.class);
        PowerMockito.mockStatic(CustomerServices.class);
        PowerMockito.mockStatic(ActivationCardAccountResponse.class);
        PowerMockito.mockStatic(TVFrontendAccount.class);
        PowerMockito.mockStatic(CustomerInteractionEvent.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(TVFrontendAccount.class);

        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

    }

    public void init(){
        map = new HashMap<Object, Object>();
        map.put(TEST, TEST);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getBankingSession()).thenReturn(bankingSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(userSession.getCardPlasticAccounts()).thenReturn(Collections.singletonList(plastics));
        when(userSession.getPortalAccounts()).thenReturn(Collections.singletonList(plastics));
        when(Utils.generateCustomerInteractionEvent(request)).thenReturn(customerInteractionEvent);
        when(activateAthIntBean.getFrontEndId()).thenReturn(ID);
        when(Utils.getAccountById(ID, Collections.singletonList(plastics))).thenReturn(account);
        when(customerServices.activatePlasticIntAccount(customerProfile, bankingSession,
                account.getAccountNumber(), customerInteractionEvent)).thenReturn(activationCardAccountResponse);


    }

    private void referenceDataMocks() {
        Account accountMock = mock(Account.class);
        CardPlasticAccountRecord cardPlasticAccountRecordMock = mock(CardPlasticAccountRecord.class);
        depositAccount = mock(TVFrontendAccount.class);

        when(plastics.getAccountNumber()).thenReturn("4549747474");
        when(accountMock.getAccountID()).thenReturn(ID);
        when(cardPlasticAccountRecordMock.getAccount()).thenReturn(accountMock);
        when(plastics.getTVAccount()).thenReturn(accountMock);
        when(accountMock.getRecords()).thenReturn(cardPlasticAccountRecordMock);
        when(depositAccount.getNickname()).thenReturn("nickName");
        when(Utils.getAccountByAccountNumber(ID, userSession.getPortalAccounts()))
                .thenReturn(depositAccount);
    }

    @Test
    public void whenOnSubmit_GivenStatusActivated_ThenReturnModelAndView() throws Exception {

        init();
        when(activationCardAccountResponse.getResponseStatus()).thenReturn(ActivationCardAccountResponse.Status.ACTIVATED);
        ModelAndView modelAndView = controller.onSubmit(request, response, activateAthIntBean, errors);
        assertViewName(modelAndView, VIEW);
    }

    @Test
    public void whenOnSubmit_GivenStatusCardNotFound_ThenReturnModelAndView() throws Exception {

        init();
        when(activationCardAccountResponse.getResponseStatus()).thenReturn(ActivationCardAccountResponse.Status.CARD_NOT_FOUND);
        ModelAndView modelAndView = controller.onSubmit(request, response, activateAthIntBean, errors);
        assertViewName(modelAndView, VIEW);
    }

    @Test
    public void whenOnSubmit_GivenStatusCardActivationFail_ThenReturnModelAndView() throws Exception {

        init();
        when(activationCardAccountResponse.getResponseStatus()).thenReturn(ActivationCardAccountResponse.Status.CARD_ACTIVATION_FAIL);
        ModelAndView modelAndView = controller.onSubmit(request, response, activateAthIntBean, errors);
        assertViewName(modelAndView, VIEW);
    }

    @Test
    public void whenReferenceData_GivenPlasticsStatusIsInactive_ThenReturnShowLinkCall() throws Exception {
        init();
        referenceDataMocks();

        when(plastics.getStatus()).thenReturn("INACTIVE");
        Map<String, Object> map = controller.referenceData(request, activateAthIntBean, errors);

        verify(plastics).setNickname(depositAccount.getNickname());
        List plasticList = (List) map.get("plastics");
        assertEquals(true, ((StatusAccountWrapper) plasticList.get(0)).isShowLink());
        assertEquals(plastics, ((StatusAccountWrapper) plasticList.get(0)).getAccounts());
    }


    @Test
    public void whenReferenceData_GivenPlasticsStatusIs5MUNDEFINED_ThenReturnShowLinkCall() throws Exception {
        init();
        referenceDataMocks();

        when(plastics.getStatus()).thenReturn("5 M UNDEFINED");

        Map<String, Object> map = controller.referenceData(request, activateAthIntBean, errors);
        List plasticList = (List) map.get("plastics");
        assertEquals(true, ((StatusAccountWrapper) plasticList.get(0)).isShowLink());
    }

    @Test
    public void whenReferenceData_GivenPlasticsStatusIs5ZUNDEFINED_ThenReturnShowLinkCall() throws Exception {
        init();
        referenceDataMocks();

        when(plastics.getStatus()).thenReturn("5 Z UNDEFINED");

        Map<String, Object> map = controller.referenceData(request, activateAthIntBean, errors);
        List plasticList = (List) map.get("plastics");
        assertEquals(true, ((StatusAccountWrapper) plasticList.get(0)).isShowLink());
    }

    @Test
    public void whenReferenceData_GivenPlasticsStatusIs5BBLOCKED_ThenReturnShowLinkCall() throws Exception {
        init();
        referenceDataMocks();

        when(plastics.getStatus()).thenReturn("5 B BLOCKED");

        Map<String, Object> map = controller.referenceData(request, activateAthIntBean, errors);
        List plasticList = (List) map.get("plastics");
        assertEquals(true, ((StatusAccountWrapper) plasticList.get(0)).isShowLink());
    }

    @Test
    public void whenReferenceData_GivenPlasticsStatusIs5SSTOLEN_ThenReturnShowLinkCall() throws Exception {
        init();
        referenceDataMocks();

        when(plastics.getStatus()).thenReturn("5 S STOLEN");

        Map<String, Object> map = controller.referenceData(request, activateAthIntBean, errors);
        List plasticList = (List) map.get("plastics");
        assertEquals(false, ((StatusAccountWrapper) plasticList.get(0)).isShowLink());
    }

}