package evertec.cibp.web.form;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.WebResponseStatus;
import evertec.cibp.web.resources.StringResources;
import evertec.cibp.web.resources.ViewResources;
import evertec.cibp.web.response.RDCEnrollmentInfoResponse;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.StringResourcesUtils;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.enums.ResponseStatus;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, Utils.class, StringResourcesUtils.class})
public class RemoteDepositEnrollInRDCFormUT {

	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	
	@Mock
	private Object submitDeposit;
	
	@Mock
	private BindException error;
	
	@Mock
	private Utils utils;
	
	@Mock
	private RDCEnrollmentInfoResponse rdcEnrollmentResp;
	
	@InjectMocks
	private RemoteDepositEnrollInRDCForm remoteDepositEnrollInRDCForm;
	
	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(StringResourcesUtils.class);
		
		MockitoAnnotations.initMocks(this);
		
		when(Utils.getUserSession(request)).thenReturn(userSession);
	}
	
	//////////////////////// onSubmit
	/**
	 * 
	 * when onSubmit
	 * Give Enroll Success Response
	 * Then return view and empty error
	 * @throws Exception
	 */
	@Test
	public void whenOnSubmit_GiveEnrollSuccessResponse_AndThenErrorEmpty() throws Exception {
		when(Utils.enrollUserInRDC(any(), any(), any())).thenReturn(rdcEnrollmentResp);
		when(rdcEnrollmentResp.getResponseStatus()).thenReturn(WebResponseStatus.SUCCESS);
		when(rdcEnrollmentResp.getServiceResponseStatus()).thenReturn(ResponseStatus.SUCCESS);
		
		ModelAndView modelAndView = remoteDepositEnrollInRDCForm.onSubmit(request, response, submitDeposit, error);
		
		assertEquals(ViewResources.REMOTE_DEPOSIT_ENROLL, modelAndView.getViewName());
		assertEquals(ResponseStatus.SUCCESS, modelAndView.getModelMap().get(RemoteDepositEnrollInRDCForm.STATUS));
		assertEquals("", modelAndView.getModelMap().get(RemoteDepositEnrollInRDCForm.ERROR));
	}
	
	/**
	 * 
	 * when onSubmit
	 * Give Enroll Maintenance Error Response
	 * Then return view and empty error
	 * @throws Exception
	 */
	@Test
	public void whenOnSubmit_GiveEnrollMaintenanceErrorResponse_AndThenErrorEmpty() throws Exception {
		when(Utils.enrollUserInRDC(any(), any(), any())).thenReturn(rdcEnrollmentResp);
		when(rdcEnrollmentResp.getResponseStatus()).thenReturn(WebResponseStatus.MAINTENANCE);
		when(rdcEnrollmentResp.getServiceResponseStatus()).thenReturn(ResponseStatus.SUCCESS);
		
		ModelAndView modelAndView = remoteDepositEnrollInRDCForm.onSubmit(request, response, submitDeposit, error);
		
		assertEquals(ViewResources.REMOTE_DEPOSIT_ENROLL, modelAndView.getViewName());
		assertNotEquals(ResponseStatus.SUCCESS, modelAndView.getModelMap().get(RemoteDepositEnrollInRDCForm.STATUS));
		assertNotEquals("", modelAndView.getModelMap().get(RemoteDepositEnrollInRDCForm.ERROR));
	}
	
	/**
	 * 
	 * when onSubmit
	 * Give Enroll Error Response
	 * Then return view and empty error
	 * @throws Exception
	 */
	@Test
	@Ignore
	public void whenOnSubmit_GiveEnrollErrorResponse_AndThenErrorEmpty() throws Exception {
		when(Utils.enrollUserInRDC(any(), any(), any())).thenReturn(rdcEnrollmentResp);
		when(rdcEnrollmentResp.getServiceResponseStatus()).thenReturn(ResponseStatus.SUCCESS);
		
		ModelAndView modelAndView = remoteDepositEnrollInRDCForm.onSubmit(request, response, submitDeposit, error);
		
		assertEquals(ViewResources.REMOTE_DEPOSIT_ENROLL, modelAndView.getViewName());
		assertNotEquals(ResponseStatus.SUCCESS, modelAndView.getModelMap().get(RemoteDepositEnrollInRDCForm.STATUS));
		assertNotEquals("", modelAndView.getModelMap().get(RemoteDepositEnrollInRDCForm.ERROR));
	}
	
	/**
	 * when onSubmit
	 * Give Enroll Response Null
	 * Then return view, error response and error description
	 * @throws Exception
	 */
	@Test
	public void whenOnSubmit_GiveEnrollSResponseNull_AndThenErrorEmpty() throws Exception {
		when(Utils.enrollUserInRDC(any(), any(), any())).thenReturn(null);
		when(StringResourcesUtils.getStringResource(any(), any(), any())).thenReturn(StringResources.REMOTE_DEPOSIT_ERROR_GENERIC);
		
		ModelAndView modelAndView = remoteDepositEnrollInRDCForm.onSubmit(request, response, submitDeposit, error);
		
		assertEquals(ViewResources.REMOTE_DEPOSIT_ENROLL, modelAndView.getViewName());
		assertEquals(RemoteDepositEnrollInRDCForm.ERROR_RESPONSE, modelAndView.getModelMap().get(RemoteDepositEnrollInRDCForm.STATUS));
		assertEquals(StringResources.REMOTE_DEPOSIT_ERROR_GENERIC, modelAndView.getModelMap().get(RemoteDepositEnrollInRDCForm.ERROR));
	}
	
	

}
