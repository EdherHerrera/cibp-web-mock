package evertec.cibp.web.form;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.core.models.customer.CustomerDevices;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerStatus;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.models.sso.SSOToken;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CookiePreferenceStatusService;
import evertec.cibp.core.services.requests.ValidateLoginInfoRequest;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.core.services.responses.UsernameValidationResponse;
import evertec.cibp.core.utils.CustomerUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.AjaxActionsController;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.LoginBean;
import evertec.cibp.web.services.modules.CookiePreferenceServiceModule;
import evertec.cibp.web.services.modules.SignOnCustomerServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.signon.SignonSequenceUtils;
import evertec.cibp.web.utils.CookieUtils;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.ListUtils;
import evertec.cibp.web.utils.Utils;
import org.apache.commons.validator.GenericValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CookieUtils.class, SignOnCustomerServiceModule.class, StaticMessageSource.class,
        CustomerUtils.class, Utils.class, SignonSequenceUtils.class, ESAPI.class,
        WebUtils.class, GenericValidator.class, DeviceUtils.class, ListUtils.class, AjaxActionsController.class, ApiCallerFactory.class})
public class CookiePreferenceServiceModuleIT extends AbstractModelAndViewTests {

    @Mock
    private CustomerServices customerServices;

    @Mock
    private CookiePreferenceStatusService cookiePreferenceStatusService;

    private MockHttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private LoginBean command;

    @Mock
    private UserSession userSession;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private UsernameValidationResponse responseAnswer;

    @Mock
    private HTTPUtilities httPUtilities;

    @Mock
    private SignOnCustomerServiceModule customerServiceModule;

    @Mock
    private BindException errors;

    @Mock
    private SSOToken token;

    @Mock
    private CookiePreferenceServiceModule cookiePreferenceServiceModule;

    private final List<CookiePreferenceStatus> cookiePreferenceStatusList = new ArrayList<>();

    @InjectMocks
    private LoginForm controller;

    @Spy
    @InjectMocks
    private final AjaxActionsController ajaxActionsController = new AjaxActionsController();

    @Mock
    private ApiCaller apiCaller;


    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ApiCallerFactory.class);
        when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

        request = new MockHttpServletRequest();
        PowerMockito.mockStatic(CookieUtils.class);
        PowerMockito.mockStatic(SignOnCustomerServiceModule.class);
        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(CustomerUtils.class);
        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(SignonSequenceUtils.class);
        PowerMockito.mockStatic(ESAPI.class);
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(GenericValidator.class);
        PowerMockito.mockStatic(DeviceUtils.class);
        PowerMockito.mockStatic(ListUtils.class);

        response = PowerMockito.mock(HttpServletResponse.class);

        CookiePreferenceStatus cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("DISABLED");
        cookiePreferenceStatus.setCategory("ANALYTICS");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);
    }

    /**
     * when OnSubmit
     * Given PreferencesNull
     * Then ReturnModelValueTrue
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     * @throws Exception exception
     */
    @Test
    public void whenOnSubmit_GivenPreferencesNull_ThenReturnModelValueTrue()
            throws Exception {
        controller.setCustomerService(customerServices);
        command.setUsername("valdesec");
        when(command.getUsername()).thenReturn("valdesec");
        PowerMockito.whenNew(UserSession.class).withArguments(any(), any(), any()).thenReturn(userSession);
        when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(Boolean.TRUE);
        when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE179)).thenReturn(Boolean.FALSE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP)).thenReturn(Boolean.TRUE);
        when(CustomerUtils.isSecurities(any())).thenReturn(Boolean.FALSE);
        when(responseAnswer.getCustomerProfile()).thenReturn(null);
        when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
        when(responseAnswer.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getStatus()).thenReturn(CustomerStatus.ACTIVE);
        when(responseAnswer.getDeviceRequest()).thenReturn(null);
        when(responseAnswer.getRsaSessionId()).thenReturn("1234");
        when(responseAnswer.getRsaTransactionId()).thenReturn("1234");
        when(responseAnswer.getTasksList()).thenReturn(null);
        when(responseAnswer.getAccountsToValidate()).thenReturn(null);
        when(Utils.getRSALanguage(any())).thenReturn("");
        when(customerServices.validateUsername(any(ValidateLoginInfoRequest.class))).thenReturn(responseAnswer);
        when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
        when(cookiePreferenceStatusService.getCookiePreferenceStatusByProfileId(any())).thenReturn(new ArrayList<>());
        when(customerProfile.getProfileId()).thenReturn("987654321asflkj");
        PowerMockito.doNothing().when(userSession).setCookiePreferenceStatusList(any());
        when(SignonSequenceUtils.getNextView(any())).thenReturn("login");
        when(command.getPromo()).thenReturn("");
        when(ESAPI.httpUtilities()).thenReturn(httPUtilities);
        PowerMockito.doNothing().when(httPUtilities).addCookie(any(Cookie.class));
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        PowerMockito.doNothing().when(cookiePreferenceServiceModule).deleteAllCookiePreference(any(), any());
        when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
        when(cookiePreferenceStatusService.saveCookiePreferenceStatus(any())).thenReturn(Boolean.TRUE);
        when(cookiePreferenceStatusService.getCookiePreferenceStatusByProfileId(any())).thenReturn(new ArrayList<>());
        when(WebUtils.getCookie(any(), anyString())).thenReturn(new Cookie("PMData", "PMData"));
        when(customerServices.tokenGetInformation(anyString())).thenReturn(token);
        when(token.getReferenceData()).thenReturn(null);
        when(GenericValidator.maxLength(anyString(), anyInt())).thenReturn(true);
        when(DeviceUtils.getCustomerDevice(anyString())).thenReturn(CustomerDevices.ANDROID);
        request.addHeader("User-Agent", UserAgentType.WEB);

        controller.onSubmit(request, response, command, errors);
        verify(cookiePreferenceServiceModule).createProfileCookiePreference(any());
    }

    /**
     * when UpdateCookiePrivacySettings
     * Given CookieCategoryAdvertising
     * Void method
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     */
    @Test
    public void whenUpdateCookiePrivacySettings_GivenCookieCategoryAdvertising() throws Exception {

        ajaxActionsController.setCustomerService(customerServices);
        when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
        request.setParameter("cookieCategory", "swAdvertising");
        request.setParameter("status", "DISABLED");
        PowerMockito.whenNew(CookiePreferenceServiceModule.class).withNoArguments().thenReturn(cookiePreferenceServiceModule);
        when(userSession.getCookiePreferenceStatusList()).thenReturn(new ArrayList<CookiePreferenceStatus>());
        when(ListUtils.getCookiePreferenceFromList(any(), any())).thenReturn(new CookiePreferenceStatus());
        when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
        when(cookiePreferenceStatusService.updateCookiePreferenceStatus(any())).thenReturn(Boolean.TRUE);
        PowerMockito.doNothing().when(ajaxActionsController, "registerEvent", any(HttpServletRequest.class),
                any(UserSession.class), anyString(), anyString());
        PowerMockito.doNothing().when(CookieUtils.class, "removeCookie", any(), any());

        ajaxActionsController.updateCookiePrivacySettings(request, response);
    }

    /**
     * when OnSubmit
     * Given PreferencesNullAnalyticsDisabled
     * Then ReturnModelValueTrue
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     * @throws Exception exception
     */
    @Test
    public void whenOnSubmit_GivenPreferencesNullAnalyticsDisabled_ThenReturnModelValueTrue()
            throws Exception {
        controller.setCustomerService(customerServices);
        command.setUsername("valdesec");
        when(command.getUsername()).thenReturn("valdesec");
        PowerMockito.whenNew(UserSession.class).withArguments(any(), any(), any()).thenReturn(userSession);
        when(customerProfile.hasFlag(CustomerFlagType.MUST_CHANGE_USERNAME)).thenReturn(Boolean.TRUE);
        when(SignOnCustomerServiceModule.getInstance()).thenReturn(customerServiceModule);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE179)).thenReturn(Boolean.FALSE);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_CIBP)).thenReturn(Boolean.TRUE);
        when(CustomerUtils.isSecurities(any())).thenReturn(Boolean.FALSE);
        when(responseAnswer.getCustomerProfile()).thenReturn(null);
        when(responseAnswer.getStatusResult()).thenReturn(ResultStatus.SUCCESS);
        when(responseAnswer.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getStatus()).thenReturn(CustomerStatus.ACTIVE);
        when(responseAnswer.getDeviceRequest()).thenReturn(null);
        when(responseAnswer.getRsaSessionId()).thenReturn("1234");
        when(responseAnswer.getRsaTransactionId()).thenReturn("1234");
        when(responseAnswer.getTasksList()).thenReturn(null);
        when(responseAnswer.getAccountsToValidate()).thenReturn(null);
        when(Utils.getRSALanguage(any())).thenReturn("");
        when(customerServices.validateUsername(any(ValidateLoginInfoRequest.class))).thenReturn(responseAnswer);
        when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
        when(cookiePreferenceStatusService.getCookiePreferenceStatusByProfileId(any())).thenReturn(new ArrayList<>());
        when(customerProfile.getProfileId()).thenReturn("987654321asflkj");
        PowerMockito.doNothing().when(userSession).setCookiePreferenceStatusList(any());
        when(SignonSequenceUtils.getNextView(any())).thenReturn("login");
        when(command.getPromo()).thenReturn("");
        when(ESAPI.httpUtilities()).thenReturn(httPUtilities);
        PowerMockito.doNothing().when(httPUtilities).addCookie(any(Cookie.class));
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        PowerMockito.whenNew(CookiePreferenceServiceModule.class).withNoArguments().thenReturn(cookiePreferenceServiceModule);
        when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
        when(cookiePreferenceStatusService.saveCookiePreferenceStatus(any())).thenReturn(Boolean.TRUE);
        when(cookiePreferenceStatusService.getCookiePreferenceStatusByProfileId(any())).thenReturn(cookiePreferenceStatusList);
        when(WebUtils.getCookie(any(), anyString())).thenReturn(new Cookie("PMData", "PMData"));
        when(customerServices.tokenGetInformation(anyString())).thenReturn(token);
        when(token.getReferenceData()).thenReturn(null);
        when(GenericValidator.maxLength(anyString(), anyInt())).thenReturn(true);
        when(DeviceUtils.getCustomerDevice(anyString())).thenReturn(CustomerDevices.ANDROID);
        request.addHeader("User-Agent", UserAgentType.WEB);
        PowerMockito.doNothing().when(CookieUtils.class, "removeCookie", any(), any());
        when(cookiePreferenceServiceModule.deleteSelectedCookies("ADVERTISING", response)).thenReturn(Boolean.TRUE);
        when(userSession.getCookiePreferenceStatusList()).thenReturn(cookiePreferenceStatusList);
        PowerMockito.doNothing().when(cookiePreferenceServiceModule).deleteAllCookiePreference(any(), any());

        controller.onSubmit(request, response, command, errors);
        verify(cookiePreferenceServiceModule).deleteAllCookiePreference(any(), any());
    }
}