package evertec.cibp.web.request;

import evertec.cibp.core.enums.PushTokenStatus;
import evertec.cibp.web.constants.RequestParams;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;


@RunWith(PowerMockRunner.class)
@PrepareForTest({RequestParams.class})
public class PushTokensMobileRequestUT extends AbstractModelAndViewTests {

    private MockHttpServletRequest request;
    public static final String PUSH_TOKEN = "pushToken";
    public static final String INVALID_PUSH_TOKEN = "invalidPushToken";
    public static final String DEVICE_TYPE = "deviceType";
    public static final String DEVICE_MODEL = "deviceModel";
    public static final String DEVICE_NAME = "deviceName";

    @Mock
    private RequestParams requestParams;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(RequestParams.class);
        request = new MockHttpServletRequest();

    }

    @Test
    public void when_PushTokenIsNull_Given_PUSHTOKENParameter_ThenReturn_TokenNull(){
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertEquals(null, pushTokensMobileRequest.getToken());

    }

    @Test
    public void when_PushTokenIsBlank_Given_PUSHTOKENParameter_ThenReturn_TokenNull(){
        request.setParameter(RequestParams.PUSH_TOKEN, "");
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertEquals(null, pushTokensMobileRequest.getToken());

    }

    @Test
    public void when_PushTokenIsNotNull_Given_PUSHTOKENParameter_ThenReturn_Token(){
        request.setParameter(RequestParams.PUSH_TOKEN, PUSH_TOKEN);
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertEquals("pushToken", pushTokensMobileRequest.getToken());

    }

    @Test
    public void when_InvalidPushTokenIsBlank_Given_INVALIDPUSHTOKENParameter_ThenReturn_InvalidTokenNull(){
        request.setParameter(RequestParams.INVALID_PUSH_TOKEN, "");
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertEquals(null, pushTokensMobileRequest.getInvalidToken());

    }

    @Test
    public void when_InvalidPushTokenIsNotNull_Given_INVALIDPUSHTOKEN_Parameter_ThenReturn_InvalidToken(){
        request.setParameter(RequestParams.INVALID_PUSH_TOKEN, INVALID_PUSH_TOKEN);
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertEquals("invalidPushToken", pushTokensMobileRequest.getInvalidToken());

    }

    @Test
    public void when_DeviceTypeIsBlank_Given_DEVICE_TYPEParameter_ThenReturn_DeviceTypeNull(){
        request.setParameter(RequestParams.DEVICE_TYPE, "");
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertEquals(null, pushTokensMobileRequest.getDeviceType());

    }

    @Test
    public void when_DeviceTypeIsNotNull_Given_INVALIDPUSHTOKENParameter_ThenReturn_DeviceType(){
        request.setParameter(RequestParams.DEVICE_TYPE, DEVICE_TYPE);
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertEquals("deviceType", pushTokensMobileRequest.getDeviceType());

    }

    @Test
    public void when_DeviceModelIsBlank_Given_DEVICE_MODELParameter_ThenReturn_DeviceModelNull(){
        request.setParameter(RequestParams.DEVICE_MODEL, "");
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertEquals(null, pushTokensMobileRequest.getDeviceModel());

    }

    @Test
    public void when_DeviceModelIsNotNull_Given_DEVICE_MODELParameter_ThenReturn_DeviceModel(){
        request.setParameter(RequestParams.DEVICE_MODEL, DEVICE_MODEL);
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertEquals("deviceModel", pushTokensMobileRequest.getDeviceModel());

    }

    @Test
    public void when_DeviceNameIsBlank_Given_DEVICE_NAMEParameter_ThenReturn_DeviceNameNull(){
        request.setParameter(RequestParams.DEVICE_NAME, "");
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertEquals(null, pushTokensMobileRequest.getDeviceName());

    }

    @Test
    public void when_DeviceNameIsNotNull_Given_DEVICE_NAMEParameter_ThenReturn_DeviceName(){
        request.setParameter(RequestParams.DEVICE_NAME, DEVICE_NAME);
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertEquals("deviceName", pushTokensMobileRequest.getDeviceName());

    }

    @Test
    public void when_HasPermissionIsFalse_Given_HAS_PERMISSIONSParameter_ThenReturn_HasPermissionFalse(){
        request.setParameter(RequestParams.HAS_PERMISSIONS, "false");
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertFalse(pushTokensMobileRequest.hasPermissions());

    }

    @Test
    public void when_HasPermissionIsTrue_Given_HAS_PERMISSIONSParameter_ThenReturn_HasPermissionTrue(){
        request.setParameter(RequestParams.HAS_PERMISSIONS, "true");
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertTrue(pushTokensMobileRequest.hasPermissions());

    }

    @Test
    public void when_IsActiveIsFalseAndHasPermission_Given_IS_ACTIVEParameter_ThenReturn_IsActiveIsFalse(){
        request.setParameter(RequestParams.IS_ACTIVE, "false");
        request.setParameter(RequestParams.HAS_PERMISSIONS, "true");
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertFalse(pushTokensMobileRequest.isActive());
        assertEquals(PushTokenStatus.INITIAL, pushTokensMobileRequest.getStatus());

    }

    @Test
    public void when_IsActiveIsTrueAndHasPermission_Given_IS_ACTIVEParameter_ThenReturn_IsActiveIsTrue(){
        request.setParameter(RequestParams.IS_ACTIVE, "true");
        request.setParameter(RequestParams.HAS_PERMISSIONS, "true");
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertTrue(pushTokensMobileRequest.isActive());
        assertEquals(PushTokenStatus.ACTIVE, pushTokensMobileRequest.getStatus());

    }

    @Test
    public void when_IsActiveIsFalseAndNoHasPermission_Given_IS_ACTIVEParameter_ThenReturn_IsActiveIsFalse(){
        request.setParameter(RequestParams.IS_ACTIVE, "false");
        request.setParameter(RequestParams.HAS_PERMISSIONS, "false");
        PushTokensMobileRequest pushTokensMobileRequest = new PushTokensMobileRequest(request);

        assertFalse(pushTokensMobileRequest.isActive());
        assertEquals(PushTokenStatus.INACTIVE, pushTokensMobileRequest.getStatus());

    }


}