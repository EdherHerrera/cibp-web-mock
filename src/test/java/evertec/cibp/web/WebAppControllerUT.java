package evertec.cibp.web;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.models.base.SimpleTransaction;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.web.enums.UserAgentType;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import bppr.tv.beans.deposits.records.DepositItemImageRecord;
import bppr.tv.beans.deposits.records.DepositTicketImageRecord;
import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.ebill.Ebpp2BillerProductIdResponse;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.statements.AccountPDFStmtInfo;
import evertec.cibp.core.models.statements.AccountStmtInfo;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.Utils;
import evertec.dp.service.tvstatement.CreditCardStatement_Type;
import evertec.otherservices.beans.UserAccount;
import evertec.otherservices.beans.stmt.OSStmtPDF;
import org.apache.commons.validator.GenericValidator;
import org.powermock.reflect.Whitebox;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Locale;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

import bppr.tv.beans.transactions.Transaction;

/**
 * Unit testing WebAppController java class.
 * @since 14-02-2020
 * @version 1.0
 */
@SuppressWarnings("deprecation")
@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, Utils.class, CustomerServices.class, StaticMessageSource.class, CustomerFacade.class, ApiCallerFactory.class,
    WebAppController.class, CustomerFacade.class,DeviceUtils.class})
public class WebAppControllerUT extends AbstractModelAndViewTests  {

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @Mock
    private UserSession userSession;

    @Mock
    private CustomerServices customerService;

    @Mock
    private OSStmtPDF interestPdf;

    @Mock
    private List<AccountStmtInfo> listStmtResult;

    @Mock
    private List<AccountPDFStmtInfo> listAcctResult;

    @Mock
    private HashMap<String, AccountPDFStmtInfo> accountsPdfs;

    @Mock
    private AccountPDFStmtInfo pdfAccount;

    @Mock
    private CustomerInteractionEvent cie;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private MessageSource messageSource;

    @Mock
    private CustomerFacade customerFacade;

    @Mock
    private GlobalEntitlement globalEntitlement;

    @Mock
    private CreditCardStatement_Type creditCardStatementType;

    @Mock
    private Ebpp2BillerProductIdResponse ebpp2BillerProductIdResponse;

    @Mock
    private TVFrontendAccount tvFrontendAccount;

    @Mock
    private HttpServletRequest requestValue;

    @Mock
    private ApiCaller apiCaller;

    @Mock
    private TransactionsResponse transactionsResponse;

    @InjectMocks
    private WebAppController controller;

    private static final String CHECK_IMAGE_ENCRYPTED_URL_RESPONSE_CONST = "http://images.bancopopular.com?secInfo=######";
        
    private static final String VALID_CHECK_IMAGE_ACCOUNT_ID_CONST = "12345678987654321";
    
    private static final String VALID_CHECK_IMAGE_KEY_CONST = "71c480df93d6ae2f1efad1447c66c9525e316218cf51fc8d9ed832f2daf18b73";
    
    private static final String INVALID_CHECK_IMAGE_ACCOUNT_ID_CONST = "00000000000000000";
    
    private static final String INVALID_CHECK_IMAGE_KEY_CONST = "6666666666666666666666666666666666666666666666666666666666666666";

    private static final String BROWSER_CHROME = "Chrome";
    @Before
    public void setUp() throws Exception {

        controller = PowerMockito.spy(new WebAppController());
        MockitoAnnotations.initMocks(this);
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(Utils.class);
        PowerMockito.mockStatic(StaticMessageSource.class);
        PowerMockito.mockStatic(CustomerFacade.class);

        PowerMockito.mockStatic(ApiCallerFactory.class);
        when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

        PowerMockito.mockStatic(DeviceUtils.class);
        request.addHeader("user-agent", BROWSER_CHROME);
        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(CustomerFacade.getInstance()).thenReturn(customerFacade);
    }

    @Test
    public void whenEStatementPDF_GivenAccountPDFStmtInfoNull_ThenPdfAccountNull() throws Exception {
        byte[] valByte = new byte[] { 1, 2, 3, 4 };

        List<TVFrontendAccount> frontEntAccList = new ArrayList<TVFrontendAccount>();
        request.setParameter("pdfId", "1234567890");
        when(userSession.getPDFInfoById(any(String.class))).thenReturn(null);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(true);
        when(customerService.getStatementPDF(any())).thenReturn(interestPdf);
        when(interestPdf.getPdfImage()).thenReturn(valByte);
        when(userSession.getCcaEbills(request)).thenReturn(listStmtResult);
        when(userSession.getAccountsStatements(request)).thenReturn(listAcctResult);
        when(userSession.getPdfStatementAccount(any(), any())).thenReturn(accountsPdfs);
        when(Utils.getStatementByPdfId(any(), any())).thenReturn(pdfAccount);
        when(userSession.getUsername()).thenReturn("Juanita");
        when(Utils.generateCustomerInteractionEvent(any(), any())).thenReturn(cie);
        when(pdfAccount.getAccountNumber()).thenReturn("0123456789");
        when(pdfAccount.getInvoice_date()).thenReturn("02-14-2020");
        when(userSession.getAccounts()).thenReturn(frontEntAccList);
        when(userSession.getControlHAccounts()).thenReturn(frontEntAccList);
        when(userSession.getClosedAccounts()).thenReturn(frontEntAccList);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        when(Utils.getAccountByAccountNumber(any(), any())).thenReturn(null);

        when(DeviceUtils.getBrowserInfo(BROWSER_CHROME, userSession)).thenReturn("");

        controller.eStatementPDF(request, response);
        assertTrue(pdfAccount != null);
    }

    @Test
    public void whenEbpp2PortalInbox_giveCCaEbills_thenReturnInvoceDateCycle() throws Exception {
    	controller.setMessageSource(messageSource);
        when(customerProfile.hasEntitlement(any())).thenReturn(true);
        when(messageSource.getMessage(any(), any(), any())).thenReturn(EntitlementStatus.ENABLED.toString());
        when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.EBILLS)).thenReturn(globalEntitlement);
        when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);
        when(userSession.isEbpp2UserIdLookedUp()).thenReturn(true);
        when(userSession.getEbpp2UserId()).thenReturn("1234");
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(true);
        when(customerService.getEbpp2BillerProductIdInformationDepositAccount("081")).thenReturn(ebpp2BillerProductIdResponse);
        when(Utils.getAccountByAccountNumber(any(), any())).thenReturn(tvFrontendAccount);
        List<CreditCardStatement_Type> ccaStatements = new ArrayList() {{
            add(creditCardStatementType);
        }};
        when(userSession.getCcaCurrentStatements()).thenReturn(ccaStatements);
        Date invoiceDate = new Date();
        when(creditCardStatementType.getStatementCycleDt()).thenReturn(invoiceDate);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(customerService.getGlobalEntitlement(any(EntitlementType.class))).thenReturn(globalEntitlement);

        when(messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()).thenReturn(EntitlementStatus.ENABLED.toString());
        Whitebox.invokeMethod(controller, "isEbppAvailable");

        ModelAndView model = controller.ebpp2PortalInbox(request, response);
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");

        ArrayList<AccountPDFStmtInfo> accountsPDFInfo = (ArrayList) model.getModel().get("vaultAccountCurrentEbills");
        assertTrue(accountsPDFInfo.get(0).getInvoice_date().equals(format.format(invoiceDate.getTime())));
    }

    @Test
    public void whenDepositItems_GivenValidKeyAndAccountKey_ThenReturnEncryptedDepositImageWithCheckImages() throws Exception {

        request.setParameter("accountId", VALID_CHECK_IMAGE_ACCOUNT_ID_CONST);
        request.setParameter("key", VALID_CHECK_IMAGE_KEY_CONST);
        
        HashMap<String, String> chechImageKeyMap = new HashMap<>();
                
        chechImageKeyMap.put(VALID_CHECK_IMAGE_KEY_CONST, "2020082008501026664");

        when(userSession.getCheckImageMap()).thenReturn(chechImageKeyMap);
        
        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        
        when(customerService.obtainEncryptedImageUrl(any(), any(), any())).thenReturn(CHECK_IMAGE_ENCRYPTED_URL_RESPONSE_CONST);
        
        List<DepositTicketImageRecord> listDepositTicketImageRecord = new ArrayList<>();
        
        DepositItemImageRecord record = new DepositItemImageRecord();
        record.setShortCIMSKey("2020082008501026664");
        
        DepositItemImageRecord record2 = new DepositItemImageRecord();
        record2.setShortCIMSKey("2020082008501026665");
        
        List<DepositItemImageRecord> checkImages = new ArrayList<>();
        checkImages.add(record);
        checkImages.add(record2);
        
        DepositTicketImageRecord depositItem = new DepositTicketImageRecord();
        depositItem.setDepositItems(checkImages);
        
        listDepositTicketImageRecord.add(depositItem);
        
        when(customerService.getDepositTicketandItemInfo(any(), any(), any(), any())).thenReturn(listDepositTicketImageRecord);
        
        ModelAndView model = controller.depositItems(request, response);
        
        verify(customerService, times(1)).obtainEncryptedImageUrl(any(), any(), any());

        assertTrue(model.getModelMap().get("ticketUrl").equals(CHECK_IMAGE_ENCRYPTED_URL_RESPONSE_CONST));
        assertTrue(model.getModelMap().get("Items") != null);
        
        // Test store items in session
        assertTrue(userSession.getCheckImageMap().get("item1") != null);
        assertTrue(userSession.getCheckImageMap().get("item2") != null);
        assertTrue(userSession.getCheckImageMap().get("71c480df93d6ae2f1efad1447c66c9525e316218cf51fc8d9ed832f2daf18b73") != null);
    }
    
    @Test
    public void whenDepositItems_GivenValidKeyAndAccountKey_ThenReturnEncryptedDepositImageWithoutCheckImages() throws Exception {
        
        request.setParameter("accountId", VALID_CHECK_IMAGE_ACCOUNT_ID_CONST);
        request.setParameter("key", VALID_CHECK_IMAGE_KEY_CONST);
        
        HashMap<String, String> chechImageKeyMap = new HashMap<>();
        
        chechImageKeyMap.put(VALID_CHECK_IMAGE_KEY_CONST, "2020082008501026664");

        when(userSession.getCheckImageMap()).thenReturn(chechImageKeyMap);
        
        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        
        when(customerService.obtainEncryptedImageUrl(any(), any(), any())).thenReturn(CHECK_IMAGE_ENCRYPTED_URL_RESPONSE_CONST);
        
        ModelAndView model = controller.depositItems(request, response);
        
        verify(customerService, times(1)).obtainEncryptedImageUrl(any(), any(), any());

        
        assertTrue(model.getModelMap().get("ticketUrl").equals(CHECK_IMAGE_ENCRYPTED_URL_RESPONSE_CONST));
        assertTrue(model.getModelMap().get("Items") == null);
        
        // Test store items in session
        assertTrue(userSession.getCheckImageMap().get("item1") == null);
        assertTrue(userSession.getCheckImageMap().get("item2") == null);
        assertTrue(userSession.getCheckImageMap().get("71c480df93d6ae2f1efad1447c66c9525e316218cf51fc8d9ed832f2daf18b73") != null);
    }
    
    @Test
    public void whenDepositItems_GivenInvalidKeyAndAccountKey_ThenRejectAndReturnNull() throws Exception {

        request.setParameter("accountId", INVALID_CHECK_IMAGE_ACCOUNT_ID_CONST);
        request.setParameter("key", INVALID_CHECK_IMAGE_KEY_CONST);
        
        HashMap<String, String> chechImageKeyMap = new HashMap<>();
                
        chechImageKeyMap.put(VALID_CHECK_IMAGE_KEY_CONST, "2020082008501026664");

        when(userSession.getCheckImageMap()).thenReturn(chechImageKeyMap);
        
        List<DepositTicketImageRecord> listDepositTicketImageRecord = new ArrayList<>();
        
        DepositItemImageRecord record = new DepositItemImageRecord();
        record.setShortCIMSKey("2020082008501026664");
        
        DepositItemImageRecord record2 = new DepositItemImageRecord();
        record2.setShortCIMSKey("2020082008501026665");
        
        List<DepositItemImageRecord> checkImages = new ArrayList<>();
        checkImages.add(record);
        checkImages.add(record2);
        
        DepositTicketImageRecord depositItem = new DepositTicketImageRecord();
        depositItem.setDepositItems(checkImages);
        
        listDepositTicketImageRecord.add(depositItem);
        
        when(customerService.getDepositTicketandItemInfo(any(), any(), any(), any())).thenReturn(listDepositTicketImageRecord);
        
        ModelAndView model = controller.depositItems(request, response);
        
        verify(customerService, times(0)).obtainEncryptedImageUrl(any(), any(), any());
        
        assertTrue(model.getModelMap().get("ticketUrl") == null);
        assertTrue(model.getModelMap().get("Items") == null);
        
        // Test store items in session
        assertTrue(userSession.getCheckImageMap().get("item1") == null);
        assertTrue(userSession.getCheckImageMap().get("item2") == null);
        
        assertTrue(userSession.getCheckImageMap().get("71c480df93d6ae2f1efad1447c66c9525e316218cf51fc8d9ed832f2daf18b73") != null);
    }
    
    @Test
    public void whenCheckImage_GivenValidKeyAndAccountKey_ThenReturnEncryptedCheckImage() throws Exception {
    
        request.setParameter("accountId", VALID_CHECK_IMAGE_ACCOUNT_ID_CONST);
        request.setParameter("key", VALID_CHECK_IMAGE_KEY_CONST);
        
        HashMap<String, String> chechImageKeyMap = new HashMap<>();
    
        chechImageKeyMap.put(VALID_CHECK_IMAGE_KEY_CONST, "2020082008501026664");

        when(userSession.getCheckImageMap()).thenReturn(chechImageKeyMap);
        
        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        
        when(customerService.obtainEncryptedImageUrl(any(), any(), any())).thenReturn(CHECK_IMAGE_ENCRYPTED_URL_RESPONSE_CONST);
        
        ModelAndView model = controller.checkImage(request, response);
        
        verify(customerService, times(1)).obtainEncryptedImageUrl(any(), any(), any());

        // Test store items in session
        assertTrue(model.getModelMap().get("text").equals(CHECK_IMAGE_ENCRYPTED_URL_RESPONSE_CONST));
    }
    
    @Test
    public void whenCheckImage_GivenInvalidKeyAndAccountKey_ThenRejectAndReturnNull() throws Exception {
    
        request.setParameter("accountId", INVALID_CHECK_IMAGE_ACCOUNT_ID_CONST);
        request.setParameter("key", INVALID_CHECK_IMAGE_KEY_CONST);
        
        HashMap<String, String> chechImageKeyMap = new HashMap<>();
    
        chechImageKeyMap.put(VALID_CHECK_IMAGE_KEY_CONST, "2020082008500000000");

        when(userSession.getCheckImageMap()).thenReturn(chechImageKeyMap);
                
        ModelAndView model = controller.checkImage(request, response);
        
        verify(customerService, times(0)).obtainEncryptedImageUrl(any(), any(), any());
        
        // Test store items in session
        assertTrue(GenericValidator.isBlankOrNull(model.getModelMap().get("text").toString()));
    }

    /**
     * when Ssodude
     * Given IsCookieCategoryEnabledTrue
     * Then ReturnModelValueTrue
     *
     * @author Evertec ET58344
     * @since 09-10-2020
     * @exception Exception error
     */
    @Test
    public void whenSsodude_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

        when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getProfileId()).thenReturn("1234567890abcdefg");
        when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
        when(requestValue.getParameter(anyString())).thenReturn("");

        ModelAndView modelAndView = controller.ssodude(requestValue, response);
        assertEquals(Boolean.TRUE, modelAndView.getModel().get("cookieAnalyticEnabled"));
    }

    /**
     * when Error
     * Given IsCookieCategoryEnabledTrue
     * Then ReturnModelValueTrue
     *
     * @author Evertec ET58344
     * @since 09-10-2020
     * @exception Exception error
     */
    @Test
    public void whenError_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

        when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
        when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);

        ModelAndView modelAndView = controller.error(requestValue, response);
        assertEquals(Boolean.TRUE, modelAndView.getModel().get("cookieAnalyticEnabled"));
    }

    /**
     * when ebpp2LatestHistory
     * Given IsCookieCategoryEnabledTrue
     * Then ReturnModelValueTrue
     *
     * @author Evertec ET58344
     * @since 09-10-2020
     * @exception Exception error
     */
    @Test
    public void whenEbpp2LatestHistory_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

        List<UserAccount> ebpp2AccountsAndSummariesList = new ArrayList<>();
        List<AccountPDFStmtInfo> lastHistoryInterest = new ArrayList<>();
        controller.setMessageSource(messageSource);

        when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
        when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.hasEntitlement(any(EntitlementType.class))).thenReturn(Boolean.TRUE);
        when(messageSource.getMessage(any(), any(), any())).thenReturn(EntitlementStatus.ENABLED.toString());
        when(CustomerFacade.getInstance()).thenReturn(customerFacade);
        when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);
        when(customerFacade.getGlobalEntitlement(any(UserSession.class), any(EntitlementType.class)))
                .thenReturn(globalEntitlement);
        when(customerService.getGlobalEntitlement(any(EntitlementType.class))).thenReturn(globalEntitlement);
        when(userSession.getEbpp2UserAccountsAndSummaries()).thenReturn(ebpp2AccountsAndSummariesList);
        when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
        when(userSession.getInterestStatementsLastStmt()).thenReturn(lastHistoryInterest);

        when(messageSource.getMessage("ebpp.module", null, new Locale("es")).toUpperCase()).thenReturn(EntitlementStatus.ENABLED.toString());
        Whitebox.invokeMethod(controller, "isEbppAvailable");

        ModelAndView modelAndView = controller.ebpp2LatestHistory(requestValue, response);
        assertEquals(Boolean.TRUE, modelAndView.getModel().get("cookieAnalyticEnabled"));
    }

    /**
     * when EbppViewBiller
     * Given IsCookieCategoryEnabledTrue
     * Then ReturnModelValueTrue
     *
     * @author Evertec ET58344
     * @since 09-11-2020
     * @exception Exception error
     */
    @SuppressWarnings("unchecked")
    @Test
    public void whenEbppViewBiller_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

        when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
        when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(requestValue.getParameter(anyString())).thenReturn("");
        when(userSession.isEbpp2UserIdLookedUp()).thenReturn(Boolean.TRUE);
        when(userSession.getAccounts()).thenReturn(new ArrayList<>());
        when(Utils.getAccountById(anyString(), any(ArrayList.class))).thenReturn(null);

        ModelAndView modelAndView = controller.ebppViewBiller(requestValue, response);
        assertEquals(Boolean.TRUE, modelAndView.getModel().get("cookieAnalyticEnabled"));
    }

    /**
     * when Estatementcampaignfail
     * Given IsCookieCategoryEnabledTrue
     * Then ReturnModelValueTrue
     *
     * @author Evertec ET58344
     * @since 09-11-2020
     * @exception Exception error
     */
    @Test
    public void whenEstatementcampaignfail_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

        when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
        when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);

        ModelAndView modelAndView = controller.estatementcampaignfail(requestValue, response);
        assertEquals(Boolean.TRUE, modelAndView.getModel().get("cookieAnalyticEnabled"));
    }

    /**
     * when Billpaydowntime
     * Given IsCookieCategoryEnabledTrue
     * Then ReturnModelValueTrue
     *
     * @author Evertec ET58344
     * @since 09-11-2020
     * @exception Exception error
     */
    @Test
    public void whenBillpaydowntime_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

        when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
        when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);

        ModelAndView modelAndView = controller.billpaydowntime(requestValue, response);
        assertEquals(Boolean.TRUE, modelAndView.getModel().get("cookieAnalyticEnabled"));
    }

    /**
     * when Transfersdowntime
     * Given IsCookieCategoryEnabledTrue
     * Then ReturnModelValueTrue
     *
     * @author Evertec ET58344
     * @since 09-11-2020
     * @exception Exception error
     */
    @Test
    public void whenTransfersdowntime_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

        when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
        when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);

        ModelAndView modelAndView = controller.transfersdowntime(requestValue, response);
        assertEquals(Boolean.TRUE, modelAndView.getModel().get("cookieAnalyticEnabled"));
    }

    /**
     * when Enrollmentdowntime
     * Given IsCookieCategoryEnabledTrue
     * Then ReturnModelValueTrue
     *
     * @author Evertec ET58344
     * @since 09-11-2020
     * @exception Exception error
     */
    @Test
    public void whenEnrollmentdowntime_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

        when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
        when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);

        ModelAndView modelAndView = controller.enrollmentdowntime(requestValue, response);
        assertEquals(Boolean.TRUE, modelAndView.getModel().get("cookieAnalyticEnabled"));
    }

    /**
     * when Signondowntime
     * Given IsCookieCategoryEnabledTrue
     * Then ReturnModelValueTrue
     *
     * @author Evertec ET58344
     * @since 09-11-2020
     * @exception Exception error
     */
    @Test
    public void whenSignondowntime_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

        when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
        when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);

        ModelAndView modelAndView = controller.signondowntime(requestValue, response);
        assertEquals(Boolean.TRUE, modelAndView.getModel().get("cookieAnalyticEnabled"));
    }

        /**
     * when GdprPrivacySettings
     * Given ShowGdprTrue
     * Then ReturnGdprFlagTrue
     * @author ET58344, Evertec Inc.
     * @since 03-16-2021
     */
    @Test
    public void whenGdprPrivacySettings_GivenShowGdprTrue_ThenReturnGdprFlagTrue() throws Exception {

        PowerMockito.doNothing().when(WebUtils.class, "setSessionAttribute", any(), any(), any());
        ModelAndView mavResult = controller.gdprPrivacySettings(request, response);
        assertEquals(mavResult.getViewName(), "redirect:personalinformation");
    }

    @Test
    public void whenHasTransAddScreen_thenReturnTrue() throws Exception{
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional  = PowerMockito.mock(Optional.class);

        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        tran.setMemo("PURCHASE juan goban ON 05/08/21 guaynabo pr");
        abstransac.setDescription("PURCHASE juan goban ON 05/08/21 guaynabo pr");

        abstransac.setBackendObject(tran);

        abstractTransactions.add(abstransac);

        transOptional = Optional.of(abstransac);
        request.setParameter("account","1232313123");
        request.setParameter("trace","123231312344545");
        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");

        when(Utils.getTransactionsCCAOrIDA(any(),any(),any())).thenReturn(transactionsResponse);

        when(transactionsResponse.getTransactions()).thenReturn(abstractTransactions);

        when(Utils.getTransactionByTraceId(abstractTransactions,"123231312344545")).thenReturn(transOptional);

        ModelAndView model = controller.transAddScreen(request, response) ;
        assertTrue(model.getModelMap().containsKey("transaction"));
    }

    @Test
    public void whenHasTransAddScreenPagination_thenReturnTrue() throws Exception{
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional  = PowerMockito.mock(Optional.class);
        Optional<AbstractTransaction> transOptionalSecond  = PowerMockito.mock(Optional.class);

        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        tran.setMemo("PURCHASE juan goban ON 05/08/21 guaynabo pr");
        abstransac.setDescription("PURCHASE juan goban ON 05/08/21 guaynabo pr");

        abstransac.setBackendObject(tran);

        abstractTransactions.add(abstransac);

        transOptional = Optional.of(abstransac);

        SimpleTransaction abstransa = null;
        transOptionalSecond = Optional.ofNullable(abstransa);

        request.setParameter("account","1232313123");
        request.setParameter("trace","123231312344545");
        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");

        when(Utils.getTransactionsCCAOrIDA(any(),any(),any())).thenReturn(transactionsResponse);
        when(transactionsResponse.getPagesAvailable()).thenReturn(2);
        when(transactionsResponse.getTransactions()).thenReturn(abstractTransactions);

        when(Utils.getTransactionByTraceId(abstractTransactions,"123231312344545")).thenReturn(transOptionalSecond).thenReturn(transOptional);

        ModelAndView model = controller.transAddScreen(request, response) ;
        assertTrue(model.getModelMap().containsKey("transaction"));
    }

    @Test
    public void whenNotHasTransAddScreen_thenReturnFalse() throws Exception{
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");
        when(Utils.getTransactionsCCAOrIDA(any(),any(),any())).thenReturn(transactionsResponse);

        when(transactionsResponse.getTransactions()).thenReturn(abstractTransactions);
        when(Utils.getTransactionByTraceId(any(),any())).thenReturn(Optional.of(new SimpleTransaction()));
        ModelAndView model = controller.transAddScreen(request, response) ;
        assertFalse(model.getModelMap().containsKey("transaction"));
    }
    
    @Test
    public void whenTransAddScreen_GivenNoParameters_thenReturnEmptyModel() throws Exception{
        
        request.setParameter("account","");
        request.setParameter("trace","");
       
        ModelAndView model = controller.transAddScreen(request, response) ;
        assertFalse(model.getModelMap().containsKey("transaction"));
    }
    
    @Test
    public void whenTransAddScreen_GivenAccountParameterEmpty_thenReturnEmptyModel() throws Exception{
        
        request.setParameter("account","");
        request.setParameter("trace","12345666");
       
        ModelAndView model = controller.transAddScreen(request, response) ;
        assertFalse(model.getModelMap().containsKey("transaction"));
    }
    
    @Test
    public void whenTransAddScreen_GivenOneParameterEmpty_thenReturnEmptyModel() throws Exception{
        
        request.setParameter("account","234567899991111");
        request.setParameter("trace","");
       
        ModelAndView model = controller.transAddScreen(request, response) ;
        assertFalse(model.getModelMap().containsKey("transaction"));
    }

    @Test
    public void whenTransAddScreen_GivenAccountNullthenReturnFalse() throws Exception{
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        request.setParameter("account","1232313123");
        request.setParameter("trace","123231312344545");
        when(Utils.getAccountById(any(), any())).thenReturn(null);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");
        when(Utils.getTransactionsCCAOrIDA(any(),any(),any())).thenReturn(transactionsResponse);

        when(transactionsResponse.getTransactions()).thenReturn(abstractTransactions);
        when(Utils.getTransactionByTraceId(any(),any())).thenReturn(Optional.of(new SimpleTransaction()));
        ModelAndView model = controller.transAddScreen(request, response) ;
        assertFalse(model.getModelMap().containsKey("transaction"));
    }

    @Test
    public void whenTransAddScreen_GivenOptionalisEmptythenReturnFalse() throws Exception{
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        request.setParameter("account","1232313123");
        request.setParameter("trace","123231312344545");
        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");
        when(Utils.getTransactionsCCAOrIDA(any(),any(),any())).thenReturn(transactionsResponse);

        when(transactionsResponse.getTransactions()).thenReturn(abstractTransactions);
        when(Utils.getTransactionByTraceId(any(),any())).thenReturn(Optional.empty());
        ModelAndView model = controller.transAddScreen(request, response) ;
        assertFalse(model.getModelMap().containsKey("transaction"));
    }

    @Test
    public void whenTransAddScreen_GivenATHTransaction_thenReturnTransactionKeys() throws Exception{
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional  = PowerMockito.mock(Optional.class);

        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        tran.setMemo("ATH WITHDRAWAL juan goban ON 05/08/21 guaynabo pr");
        abstransac.setDescription("ATH WITHDRAWAL juan goban ON 05/08/21 guaynabo pr");

        abstransac.setBackendObject(tran);

        abstractTransactions.add(abstransac);

        transOptional = Optional.of(abstransac);
        request.setParameter("account","1232313123");
        request.setParameter("trace","123231312344545");
        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");
        when(Utils.getTransactionsCCAOrIDA(any(),any(),any())).thenReturn(transactionsResponse);

        when(transactionsResponse.getTransactions()).thenReturn(abstractTransactions);

        when(Utils.getTransactionByTraceId(abstractTransactions,"123231312344545")).thenReturn(transOptional);

        ModelAndView model = controller.transAddScreen(request, response) ;
        assertTrue(model.getModelMap().containsKey("payee"));
        assertTrue(model.getModelMap().containsKey("location"));
        assertTrue(model.getModelMap().containsKey("type"));
    }

    @Test
    public void whenTransAddScreen_GivenEFT_DEPOSIT_14_LongTransaction_thenReturnTransaction() throws Exception{
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional  = PowerMockito.mock(Optional.class);
        String typeExpected = "EFT_DEPOSIT";
        String referenceExpected = "100843";
        String CommerceExpected = "UPR ARE-NOMINA CREDITS";

        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        abstransac.setDescription("EFT DEPOSIT UPR ARE-NOMINA CREDITS 100843");

        transOptional = Optional.of(abstransac);
        request.setParameter("account","1232313123");
        request.setParameter("trace","123231312344545");

        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");
        when(Utils.getTransactionsCCAOrIDA(any(),any(),any())).thenReturn(transactionsResponse);

        when(transactionsResponse.getTransactions()).thenReturn(abstractTransactions);

        when(Utils.getTransactionByTraceId(abstractTransactions,"123231312344545")).thenReturn(transOptional);

        ModelAndView model = controller.transAddScreen(request, response) ;
        assertTrue(model.getModelMap().containsKey("type"));
        assertTrue(model.getModelMap().containsKey("reference"));

        assertEquals(typeExpected,model.getModelMap().get("type"));
        assertEquals(referenceExpected,model.getModelMap().get("location"));
        assertEquals(CommerceExpected,model.getModelMap().get("payee"));
    }

    @Test
    public void whenTransAddScreen_GivenEFT_DEPOSIT_15_LongTransaction_thenReturnTransaction() throws Exception{
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional  = PowerMockito.mock(Optional.class);
        String typeExpected = "EFT_DEPOSIT";
        String referenceExpected = "SSA";
        String CommerceExpected = "SSA TREAS 310 XXSOC SEC 580988459A";

        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        abstransac.setDescription("EFT DEPOSIT SSA TREAS 310 XXSOC SEC 580988459A SSA");

        transOptional = Optional.of(abstransac);
        request.setParameter("account","1232313123");
        request.setParameter("trace","123231312344545");

        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");
        when(Utils.getTransactionsCCAOrIDA(any(),any(),any())).thenReturn(transactionsResponse);

        when(transactionsResponse.getTransactions()).thenReturn(abstractTransactions);

        when(Utils.getTransactionByTraceId(abstractTransactions,"123231312344545")).thenReturn(transOptional);

        ModelAndView model = controller.transAddScreen(request, response) ;
        assertTrue(model.getModelMap().containsKey("type"));
        assertTrue(model.getModelMap().containsKey("reference"));

        assertEquals(typeExpected,model.getModelMap().get("type"));
        assertEquals(referenceExpected,model.getModelMap().get("location"));
        assertEquals(CommerceExpected,model.getModelMap().get("payee"));
    }

    @Test
    public void whenTransAddScreen_GivenEFT_DEPOSIT_13_LongTransaction_thenReturnTransaction() throws Exception{
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional  = PowerMockito.mock(Optional.class);
        String typeExpected = "EFT_DEPOSIT";
        String referenceExpected = "SETTINTECHFTP";
        String CommerceExpected = "ARCOS DORADOS PR CHILDSUP";

        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        abstransac.setDescription("EFT DEPOSIT ARCOS DORADOS PR CHILDSUP SETTINTECHFTP");

        transOptional = Optional.of(abstransac);
        request.setParameter("account","1232313123");
        request.setParameter("trace","123231312344545");

        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");
        when(Utils.getTransactionsCCAOrIDA(any(),any(),any())).thenReturn(transactionsResponse);

        when(transactionsResponse.getTransactions()).thenReturn(abstractTransactions);

        when(Utils.getTransactionByTraceId(abstractTransactions,"123231312344545")).thenReturn(transOptional);

        ModelAndView model = controller.transAddScreen(request, response) ;
        assertTrue(model.getModelMap().containsKey("type"));
        assertTrue(model.getModelMap().containsKey("reference"));

        assertEquals(typeExpected,model.getModelMap().get("type"));
        assertEquals(referenceExpected,model.getModelMap().get("location"));
        assertEquals(CommerceExpected,model.getModelMap().get("payee"));
    }

    @Test
    public void whenTransAddScreen_GivenEFT_PAYMENT_15Transaction_thenReturnTransaction2() throws Exception {
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional = PowerMockito.mock(Optional.class);
        String typeExpected = "EFT_PAYMENT";
        String referenceExpected = "604578110971298";
        String CommerceExpected = "AMZ STORECRD PMT PAYMENT";

        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        abstransac.setDescription("EFT PMT AMZ STORECRD PMT PAYMENT 604578110971298");

        transOptional = Optional.of(abstransac);
        request.setParameter("account","1232313123");
        request.setParameter("trace","123231312344545");

        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");
        when(Utils.getTransactionsCCAOrIDA(any(),any(),any())).thenReturn(transactionsResponse);

        when(transactionsResponse.getTransactions()).thenReturn(abstractTransactions);

        when(Utils.getTransactionByTraceId(abstractTransactions,"123231312344545")).thenReturn(transOptional);

        ModelAndView model = controller.transAddScreen(request, response) ;
        assertTrue(model.getModelMap().containsKey("type"));
        assertTrue(model.getModelMap().containsKey("reference"));

        assertEquals(typeExpected,model.getModelMap().get("type"));
        assertEquals(referenceExpected,model.getModelMap().get("location"));
        assertEquals(CommerceExpected,model.getModelMap().get("payee"));
    }

    @Test
    public void whenTransAddScreen_GivenEFT_PAYMENT_13_Transaction_thenReturnTransaction() throws Exception {
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional = PowerMockito.mock(Optional.class);
        String typeExpected = "EFT_PAYMENT";
        String referenceExpected = "1014676122996";
        String CommerceExpected = "XIN YANG PENG YU IAT PAYPAL";
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPAD);
        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        abstransac.setDescription("EFT PMT XIN YANG PENG YU IAT PAYPAL 1014676122996");

        transOptional = Optional.of(abstransac);
        request.setParameter("account","1232313123");
        request.setParameter("trace","123231312344545");

        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");
        when(Utils.getTransactionsCCAOrIDA(any(),any(),any())).thenReturn(transactionsResponse);

        when(transactionsResponse.getTransactions()).thenReturn(abstractTransactions);

        when(Utils.getTransactionByTraceId(abstractTransactions,"123231312344545")).thenReturn(transOptional);

        ModelAndView model = controller.transAddScreen(request, response) ;
        assertTrue(model.getModelMap().containsKey("type"));
        assertTrue(model.getModelMap().containsKey("reference"));

        assertEquals(typeExpected,model.getModelMap().get("type"));
        assertEquals(referenceExpected,model.getModelMap().get("location"));
        assertEquals(CommerceExpected,model.getModelMap().get("payee"));
    }

    @Test
    public void whenTransAddScreen_GivenPurchaseTransactionWithoutLocation_thenReturnTransactionKeys() throws Exception{
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional  = PowerMockito.mock(Optional.class);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        tran.setMemo("PURCHASE Mi Tiendita ON 05/08/21");
        abstransac.setDescription("PURCHASE Mi Tiendita ON 05/08/21");

        abstransac.setBackendObject(tran);

        abstractTransactions.add(abstransac);

        transOptional = Optional.of(abstransac);
        request.setParameter("account","1232313123");
        request.setParameter("trace","123231312344545");
        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");
        when(Utils.getTransactionsCCAOrIDA(any(),any(),any())).thenReturn(transactionsResponse);

        when(transactionsResponse.getTransactions()).thenReturn(abstractTransactions);

        when(Utils.getTransactionByTraceId(abstractTransactions,"123231312344545")).thenReturn(transOptional);

        ModelAndView model = controller.transAddScreen(request, response) ;

        assertTrue(model.getModel().get("type").equals("PURCHASE"));
        assertTrue(model.getModel().get("payee").equals("Mi Tiendita "));
        assertTrue(model.getModel().get("location").equals(""));

    }

    @Test
    public void whenTransAddScreen_GivenATHWithdrawalTransactionWithoutLocation_thenReturnTransactionKeys() throws Exception{
        List<AbstractTransaction> abstractTransactions = new ArrayList<>();
        Optional<AbstractTransaction> transOptional  = PowerMockito.mock(Optional.class);
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.ANDROID);
        SimpleTransaction abstransac = new SimpleTransaction();

        Transaction tran = new Transaction();
        tran.setTraceId("123231312344545");
        tran.setMemo("ATH WITHDRAWAL 3695 BPPR BAYAMON ON 11/30/21");
        abstransac.setDescription("ATH WITHDRAWAL 3695 BPPR BAYAMON ON 11/30/21");

        abstransac.setBackendObject(tran);

        abstractTransactions.add(abstransac);

        transOptional = Optional.of(abstransac);
        request.setParameter("account","1232313123");
        request.setParameter("trace","123231312344545");
        when(Utils.getAccountById(any(), any())).thenReturn(tvFrontendAccount);
        when(tvFrontendAccount.getAccountNumber()).thenReturn("1232313123");


        when(Utils.getTransactionsCCAOrIDA(any(),any(),any())).thenReturn(transactionsResponse);

        when(transactionsResponse.getTransactions()).thenReturn(abstractTransactions);

        when(Utils.getTransactionByTraceId(abstractTransactions,"123231312344545")).thenReturn(transOptional);

        ModelAndView model = controller.transAddScreen(request, response) ;

        assertTrue(model.getModel().get("type").equals("ATH WITHDRAWAL"));
        assertTrue(model.getModel().get("payee").equals("3695 BPPR BAYAMON "));
        assertTrue(model.getModel().get("location").equals(""));

    }

    /**
     * <p>This method allows to test when rtoken comes to url</p>
     * @throws Exception
     */
    @Test
    public void whenWebAppController_givemobileSwitchComeFromMBDPWithRToken_thenReturnTrue() throws Exception {
    	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBDP2013)).thenReturn(true);
        request.setParameter("target", "login");
        request.setParameter("version", "mobile");
        request.setParameter("rtoken", "1b18a652-f4bd-48e4-9a2e-0de0b6b91cbc");
        ModelAndView model = controller.mobileSwitch(request, response);
        assertViewName(model, "redirect:login?rtoken=1b18a652-f4bd-48e4-9a2e-0de0b6b91cbc");
    }

    /**
     * <p>This method allows to test when rtoken comes to url</p>
     * @throws Exception
     */
    @Test
    public void whenWebAppController_givemobileSwitchRedirectPortal_thenReturnTrue() throws Exception {
    	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBDP2013)).thenReturn(true);
        request.setParameter("target", "portal");
        request.setParameter("version", "mobile");
        //request.setParameter("rtoken", "1b18a652-f4bd-48e4-9a2e-0de0b6b91cbc");
        ModelAndView model = controller.mobileSwitch(request, response);
        assertViewName(model, "redirect:portal");
    }

    /**
     * <p>This method allows to test when rtoken comes to url</p>
     * @throws Exception
     */
    @Test
    public void whenWebAppController_givemobileSwitchLogin_thenReturnTrue() throws Exception {
    	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBDP2013)).thenReturn(true);
        request.setParameter("target", "login");
        request.setParameter("version", "mobile");
        //request.setParameter("rtoken", "1b18a652-f4bd-48e4-9a2e-0de0b6b91cbc");
        ModelAndView model = controller.mobileSwitch(request, response);
        assertViewName(model, "redirect:login");
    }

    /**
     * <p>This method allows to test when portal target comes to url</p>
     * @throws Exception
     */
    @Test
    public void whenWebAppController_givemobileSwitchPortal_thenReturnTrue() throws Exception {
        request.setParameter("target", "portal");
        request.setParameter("version", "mobile");
        ModelAndView model = controller.mobileSwitch(request, response);
        assertViewName(model, "redirect:portal");
    }

    /**
     * <p>This method allows to test when mobile version comes to url</p>
     * @throws Exception
     */
    @Test
    public void whenWebAppController_givemobileSwitchLoginMobile_thenReturnTrue() throws Exception {
        request.setParameter("target", "login");
        request.setParameter("version", "mobile");
        ModelAndView model = controller.mobileSwitch(request, response);
        assertViewName(model, "redirect:login");
    }

    /**
     * <p>This method allows to test when classic version comes to url</p>
     * @throws Exception
     */
    @Test
    public void whenWebAppController_givemobileSwitchLoginClassic_thenReturnTrue() throws Exception {
        request.setParameter("target", "login");
        request.setParameter("version", "classic");
        ModelAndView model = controller.mobileSwitch(request, response);
        assertViewName(model, "redirect:login");
    }

    /**
     * <p>This method allows to test when another version comes to url</p>
     * @throws Exception
     */
    @Test
    public void whenWebAppController_givemobileSwitchLoginVersionThatDoesNotExist_thenReturnTrue() throws Exception {
        request.setParameter("target", "login");
        request.setParameter("version", "another");
        ModelAndView model = controller.mobileSwitch(request, response);
        assertViewName(model, "redirect:login");
    }

    @Test
    public void whenRedirectMBDP_GivenTargetEmpty_ThenReturnTrue() {
    	String target = "";
    	String rtoken = "";
        ModelAndView model = controller.redirectMBDP(target, rtoken);
        assertViewName(model, "redirect:");
    }

    @Test
    public void whenRedirectMBDP_GivenTargetLogin_ThenReturnTrue() {
    	String target = "login";
    	String rtoken = "1b18a652-f4bd-48e4-9a2e-0de0b6b91cbc";
        ModelAndView model = controller.redirectMBDP(target, rtoken);
        assertViewName(model, "redirect:login");
    }

    @Test
    public void whenRedirectMBDP_GivenTargetPortal_ThenReturnTrue() {
    	String target = "portal";
    	String rtoken = "";
        ModelAndView model = controller.redirectMBDP(target, rtoken);
        assertViewName(model, "redirect:portal");
    }

    @Test
    public void whenRedirectMBDP_GivenTargetEmptyFlagTrue_ThenReturnTrue() {
    	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBDP2013)).thenReturn(true);
    	String target = "";
    	String rtoken = "";
        ModelAndView model = controller.redirectMBDP(target, rtoken);
        assertViewName(model, "redirect:");
    }

    @Test
    public void whenRedirectMBDP_GivenTargetLoginFlagTrue_ThenReturnTrue() {
    	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBDP2013)).thenReturn(true);
    	String target = "login";
    	String rtoken = "1b18a652-f4bd-48e4-9a2e-0de0b6b91cbc";
        ModelAndView model = controller.redirectMBDP(target, rtoken);
        assertViewName(model, "redirect:login?rtoken=1b18a652-f4bd-48e4-9a2e-0de0b6b91cbc");
    }

    @Test
    public void whenRedirectMBDP_GivenTargetPortalFlagTrue_ThenReturnTrue() {
    	when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBDP2013)).thenReturn(true);
    	String target = "portal";
    	String rtoken = "";
        ModelAndView model = controller.redirectMBDP(target, rtoken);
        assertViewName(model, "redirect:portal");
    }

    @Test
    public void whenStmtHasNotViewLess13_GivenValidateHasView_ThenReturnTrue() throws Exception {
        List<AccountPDFStmtInfo> stmtList = new ArrayList<>();
        assertTrue(controller.stmtHasNotViewLess13(userSession, pdfAccount));
        pdfAccount.setViewed(Boolean.TRUE);
        stmtList.add(new AccountPDFStmtInfo());
        when(pdfAccount.getViewed()).thenReturn(Boolean.TRUE);
        when(userSession.getIntPrevPDFStmtsByAccountAndProductId(any())).thenReturn(stmtList);
        assertTrue(controller.stmtHasNotViewLess13(userSession, pdfAccount));
    }
}
