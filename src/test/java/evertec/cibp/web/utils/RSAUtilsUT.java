package evertec.cibp.web.utils;

import com.rsa.csd.ws.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.RSAEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.customer.modules.CommonServicesModule;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.UserSession;
import org.hibernate.HibernateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.doThrow;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;


@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class})
public class RSAUtilsUT {

    @Mock
    private CustomerServices customerServices;

    @Mock
    private CustomerProfileService customerProfileService;

    @Mock
    private CustomerEntitlement customerEntitlement;

    @Mock
    private CustomerProfile customerProfile;

    @Mock
    private CommonServicesModule commonServicesModule;

    @Mock
    private UserSession userSession;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
       PowerMockito.mockStatic(StaticMessageSource.class);
    }

    @Test
    public void whenGetDeviceRequest_GivenValidData_ThenReturnDeviceRequest() {
        Cookie mockCookie = Mockito.mock(Cookie.class);
        when(mockCookie.getName()).thenReturn("Test");

        String devicePrint = "";
        String deviceTokenCookie ="1234567890987654321";
        HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
        when(request.getCookies()).thenReturn(new Cookie[]{mockCookie});

        assertTrue(RSAUtils.getDeviceRequest(devicePrint, deviceTokenCookie, request) instanceof DeviceRequest);
    }

    @Test
    public void whenCreateOOBInterruptionEntitlement_GivenCustomerProfile_thenReturnTrue() {
        when(customerProfile.getProfileId()).thenReturn("test");
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        assertTrue(RSAUtils.createRSAEntitlement(customerServices, customerProfile));
    }

    @Test
    public void whenCreateOOBInterruptionEntitlement_GivenException_thenReturnFalse() {
        when(customerProfile.getProfileId()).thenReturn("test");
        when(customerServices.getCustomerProfileService()).thenReturn(customerProfileService);
        doThrow(new HibernateException("Test")).when(customerProfileService).addEntitlement(any(), any());
        assertFalse(RSAUtils.createRSAEntitlement(customerServices, customerProfile));
    }


    @Test
    public void whenIncrementUserSkips_GivenCustomerProfile() {
        when(customerEntitlement.getString(any())).thenReturn(RSAEntitlement.INITIAL_SKIP);
        when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);
        RSAUtils.incrementUserSkips(customerServices, customerProfile);
    }

    @Test
    public void whenGetUserSkips_GivenCustomerProfile_thenReturnInteger() {
        when(customerEntitlement.getString(any())).thenReturn(RSAEntitlement.INITIAL_SKIP);
        when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);
        assertNotNull(RSAUtils.getUserSkips(customerProfile));
    }

    @Test
    public void whenValidateIfUserContainSkips_GivenOOBInterruptionPageSkips() {
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerEntitlement.getString(any())).thenReturn(RSAEntitlement.INITIAL_SKIP);
        when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(customerServices.getCommonServices()).thenReturn(commonServicesModule);
        when(commonServicesModule.getRsaChallengeMethod(any(), any())).thenReturn(CredentialType.QUESTION);
        RSAUtils.validateUserContainSkips(customerServices, customerProfile);
    }

    @Test
    public void whenValidateIfUserContainSkips_GivenOOBInterruptionPageSkipsNull() {
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(customerServices.getCommonServices()).thenReturn(commonServicesModule);
        when(commonServicesModule.getRsaChallengeMethod(any(), any())).thenReturn(CredentialType.QUESTION);
        RSAUtils.validateUserContainSkips(customerServices, customerProfile);
    }



    @Test
    public void whenIsOOBInterruption_GivenIsOOBEnrolledFalseAndOOBInterruptionPageEnabled_thenReturnTrue() {
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerEntitlement.getString(any())).thenReturn("true");
        when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(customerServices.getCommonServices()).thenReturn(commonServicesModule);
        when(commonServicesModule.getRsaChallengeMethod(any(), any())).thenReturn(CredentialType.QUESTION);
        assertTrue(RSAUtils.isOOBInterruption(userSession));
    }

    @Test
    public void whenIsOOBInterruption_GivenIsOOBEnrolledFalseAndOOBInterruptionPageEnabledNull_thenReturnFalse() {
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);
        when(StaticMessageSource.isFlagEnabled(any())).thenReturn(false);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(customerServices.getCommonServices()).thenReturn(commonServicesModule);
        when(commonServicesModule.getRsaChallengeMethod(any(), any())).thenReturn(CredentialType.QUESTION);
        assertFalse(RSAUtils.isOOBInterruption(userSession));
    }

    @Test
    public void whenIsOOBInterruption_GivenIsOOBEnrolled_thenReturnFalse() {
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getEntitlementByType(any())).thenReturn(customerEntitlement);
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(customerServices.getCommonServices()).thenReturn(commonServicesModule);
        when(commonServicesModule.getRsaChallengeMethod(any(), any())).thenReturn(CredentialType.USER_DEFINED);
        assertFalse(RSAUtils.isOOBInterruption(userSession));
    }
    @Test
    public void whenIsOOBEnrolled_GivenChallengeTypeUserDefined_thenReturnTrue() {
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(customerServices.getCommonServices()).thenReturn(commonServicesModule);
        when(commonServicesModule.getRsaChallengeMethod(any(), any())).thenReturn(CredentialType.USER_DEFINED);
        assertTrue(RSAUtils.isOOBEnrolled(userSession));
    }

    @Test
    public void whenIsOOBEnrolled_GivenChallengeTypeQuestion_thenReturnFalse() {
        when(userSession.getCustomerService()).thenReturn(customerServices);
        when(customerServices.getCommonServices()).thenReturn(commonServicesModule);
        when(commonServicesModule.getRsaChallengeMethod(any(), any())).thenReturn(CredentialType.QUESTION);
        assertFalse(RSAUtils.isOOBEnrolled(userSession));
    }

}
