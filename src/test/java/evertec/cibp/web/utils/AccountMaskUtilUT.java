package evertec.cibp.web.utils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class AccountMaskUtilUT {

    @Mock
    private AccountMaskUtil accountMaskUtil;


    @Before()
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test()
    public void shouldCalled1TimesAccountMaskUtil() throws Exception {
        accountMaskUtil.apiAccountKey("EY_1234");
        verify(accountMaskUtil, times(1)).apiAccountKey("EY_1234");
    }
}
