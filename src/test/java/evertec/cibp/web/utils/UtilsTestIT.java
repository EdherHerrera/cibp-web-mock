package evertec.cibp.web.utils;

import evertec.cibp.core.models.campaign.ActiveCampaignView;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.web.session.UserSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Utils.class })
public class UtilsTestIT {

    @Mock
    UserSession userSession;

    private List<ActiveCampaignView> activeCampaignView = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(Utils.class);

        ActiveCampaignView campaignView = new ActiveCampaignView();
        campaignView.setApprovalDate(new Date());
        campaignView.setCampaignName("TEST MESSAGE");
        campaignView.setFileCampaign(null);
        campaignView.setId("546546546");
        campaignView.setType(CampaignType.MESSAGES);
        campaignView.setAttributeData("a6f4a65sd465a1sd4c65asd4f65asd4f65as4df65a46f54asd65f4a65f46a5s4f");

        activeCampaignView.add(campaignView);

        campaignView = new ActiveCampaignView();
        campaignView.setApprovalDate(new Date());
        campaignView.setCampaignName("TEST MESSAGE 2");
        campaignView.setFileCampaign(null);
        campaignView.setId("46546544");
        campaignView.setType(CampaignType.MESSAGES);
        campaignView.setIncludeSegments("<properties> <property name='ALL' value='ALL'/> </properties>");
        campaignView.setAttributeData("a6f4a65sd465a1sd4c65asd4f65asd4f65as4df65a46f54asd65f4a65f46a5s4f");

        activeCampaignView.add(campaignView);

        campaignView = new ActiveCampaignView();
        campaignView.setApprovalDate(new Date());
        campaignView.setCampaignName("TEST MESSAGE 2");
        campaignView.setFileCampaign(null);
        campaignView.setId("46546544");
        campaignView.setType(CampaignType.MESSAGES);
        campaignView.setIncludeSegments("<properties> <property name='CCA' value='CCA'/> </properties>");
        campaignView.setAttributeData("a6f4a65sd465a1sd4c65asd4f65asd4f65as4df65a46f54asd65f4a65f46a5s4f");

        activeCampaignView.add(campaignView);
    }

    @Test
    public void whenUpdateMessagesList_GivenActiveCampaignViewEmpty () {

        activeCampaignView = new ArrayList<>();
        when(userSession.getCustomerProfile()).thenReturn(null);
        when(userSession.hasCCA()).thenReturn(Boolean.TRUE);
        when(userSession.isWealth(anyString(), anyString())).thenReturn(Boolean.FALSE);
        when(userSession.isPreferred(anyString(), anyString())).thenReturn(Boolean.FALSE);

        Utils.messageCampaign(userSession ,"654:658:888", "888:602:132");
        Utils.updateMessagesList(any(ArrayList.class), any(UserSession.class), anyString(), anyString());
    }
}