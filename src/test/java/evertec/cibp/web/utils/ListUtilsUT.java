package evertec.cibp.web.utils;

import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.web.enums.CookieCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ListUtils.class})
public class ListUtilsUT extends AbstractModelAndViewTests {

    private final List<CookiePreferenceStatus> cookiePreferenceStatusList = new ArrayList<>();

    @InjectMocks
    private ListUtils listUtils;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        CookiePreferenceStatus cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("ENABLED");
        cookiePreferenceStatus.setCategory("ANALYTICS");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);

        cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("ENABLED");
        cookiePreferenceStatus.setCategory("FUNCTIONAL");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);

        cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("ENABLED");
        cookiePreferenceStatus.setCategory("ADVERTISING");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);
    }

    /**
     * when GetCookiePreferenceFromList
     * Given CategoriesEnabled
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     */
    @Test
    public void whenGetCookiePreferenceFromList_GivenCategoriesEnabled() {

        assertEquals(Objects.requireNonNull(ListUtils.getCookiePreferenceFromList(cookiePreferenceStatusList,
                CookieCategory.ANALYTICS)).getStatus(), "ENABLED");
    }
    
    /**
     * when GetCookiePreferenceFromList
     * Given CategoriesNotInList
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     */
    @Test
    public void whenGetCookiePreferenceFromList_GivenCategoriesNotInList() {
    	
    	cookiePreferenceStatusList.get(0).setCategory("NO_ANALYTICS");
        assertNull(ListUtils.getCookiePreferenceFromList(cookiePreferenceStatusList,
                CookieCategory.ANALYTICS));
    }

    /**
     * when GetCookiePreferenceFromListNull
     * Given CategoriesEnabled
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     */
    @Test
    public void whenGetCookiePreferenceFromListNull_GivenCategoriesEnabled() {

        assertNull(ListUtils.getCookiePreferenceFromList(new ArrayList<>(), CookieCategory.ANALYTICS));
    }
}