/**
 * 
 */
package evertec.cibp.web.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.form.mobile.ATHMCheckSSOTokenForm;
import evertec.cibp.web.session.UserSession;
import java.util.Date;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ApiCallerFactory.class})
public class OtpChallengeUtilsUT {
	
	private static final int MILLIS = 1000;

	@Mock
	private CustomerServices customerService;
	
	@Mock
	private UserSession userSession;

	@Mock
	private ApiCaller apiCaller;

	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);
	}

	@Test
	public void whenSendChallenge_GivenSendOtpChallengeEmailFalse_ThenReturnFalse() {
		
		UserSession userSession = new UserSession();
		userSession.setCustomerService(customerService);
		
		when(customerService.sendOtpChallengeEmail(any(), any(), any())).thenReturn(Boolean.FALSE);
		
		boolean result = OtpChallengeUtils.sendChallenge(userSession);
		
		assertFalse(result);
		assertNotNull(userSession.getOtpChallengeCode());
	}
	
	@Test
	public void whenSendChallenge_GivenSendOtpChallengeEmailTrue_ThenReturnTrue() {
		
		UserSession userSession = new UserSession();
		userSession.setCustomerService(customerService);
		
		when(customerService.sendOtpChallengeEmail(any(), any(), any())).thenReturn(Boolean.TRUE);
		
		boolean result = OtpChallengeUtils.sendChallenge(userSession);
		
		assertTrue(result);
		assertNotNull(userSession.getOtpChallengeCode());
	}
	
	@Test
	public void whenSendChallenge_GivenSendOtpChallengeEmailTrue_And_isResendCode_ThenReturnTrue() {
		
		UserSession userSession = new UserSession();
		userSession.setCustomerService(customerService);
		
		when(customerService.sendOtpChallengeEmail(any(), any(), any())).thenReturn(Boolean.TRUE);
		
		boolean result = OtpChallengeUtils.sendChallenge(userSession);
		
		assertTrue(result);
		assertNotNull(userSession.getOtpChallengeCode());
	}
	
	@Test
	public void whenHasCodeExpired_GivenExpirationMinutes_ThenReturnTrue() {
		
		Date expiredDate = new Date();
		
        final int expiredTime = 5 * MILLIS;
		
        expiredDate.setTime(expiredDate.getTime() - expiredTime);
        
		
		when(userSession.getOtpLastChallengeDate()).thenReturn(expiredDate);
		
		boolean result = OtpChallengeUtils.hasCodeExpired(userSession, 5);
		
		assertTrue(result);
	}
	
	@Test
	public void whenHasCodeExpired_GivenExpirationMinutes_ThenReturnFalse() {
		
		Date expiredDate = new Date();
		
        final int expiredTime = 4 * MILLIS;
		
        expiredDate.setTime(expiredDate.getTime() - expiredTime);
        
		
		when(userSession.getOtpLastChallengeDate()).thenReturn(expiredDate);
		
		boolean result = OtpChallengeUtils.hasCodeExpired(userSession, 5);
		
		assertFalse(result);
	}
	
	@Test
	public void whenHasReachedResendLimit_GivenResendMaxLimit_ThenReturnTrue() {
		
        when(userSession.getOtpSendRetries()).thenReturn(3);
        
        boolean result = OtpChallengeUtils.hasReachedResendLimit(userSession, 2);
		
		assertTrue(result);
	}
	
	@Test
	public void whenHasReachedResendLimit_GivenResendMaxLimit_ThenReturnFalse() {
		
        when(userSession.getOtpSendRetries()).thenReturn(1);
        
        boolean result = OtpChallengeUtils.hasReachedResendLimit(userSession, 2);
		
		assertFalse(result);
	}
	
	@Test
	public void whenHasReachedValidateLimit_GivenValidateMaxLimit_ThenReturnTrue() {
		
        when(userSession.getOtpValidateRetries()).thenReturn(3);
        
        boolean result = OtpChallengeUtils.hasReachedValidateLimit(userSession, 3);
		
		assertTrue(result);
	}
	
	@Test
	public void whenHasReachedValidateLimit_GivenValidateMaxLimit_ThenReturnFalse() {
		
        when(userSession.getOtpValidateRetries()).thenReturn(2);
        
        boolean result = OtpChallengeUtils.hasReachedValidateLimit(userSession, 3);
		
		assertFalse(result);
	}

}
