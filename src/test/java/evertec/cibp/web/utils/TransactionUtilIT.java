package evertec.cibp.web.utils;

import bppr.tv.beans.transactions.Transaction;
import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.models.base.RunningBalanceTransaction;
import evertec.cibp.web.form.TransactionForm;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.test.web.AbstractModelAndViewTests;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ TransactionUtil.class })
public class TransactionUtilIT extends AbstractModelAndViewTests {

    @InjectMocks
    TransactionForm transactionForm;

    private List<AbstractTransaction> abstractTransactionList;
    private String transactionTypes = "PURCHASE,ATH WITHDRAWAL,EFT";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        transactionForm = mock(TransactionForm.class);
        PowerMockito.mockStatic(TransactionUtil.class, CALLS_REAL_METHODS);
        
        
		String transactionPOSCodes = "622,625,675,679";
		String transactionEFTCodes = "518-98,581-98,618-98,681-96,681-98";
		String transactionATHWithdrawalCodes = "615-0,671-0,676-0,615-145,671-145";
		String transactionATHMovilCodes = "615-214,671-214,676-214";
		transactionForm = PowerMockito.spy(new TransactionForm(24, 50,
                "VSELT UAREG UAULT UMEDG UVNOV UVREG UVVSI", false,
                "078,089", transactionPOSCodes,transactionEFTCodes, transactionATHWithdrawalCodes,
                transactionATHMovilCodes, transactionTypes));
    }

    private void initializeTransactionList(){

    	Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal("125"));
        transaction.setReasonCode("214");
        transaction.setCode("676");

        abstractTransactionList = new ArrayList<>();
        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
        abstractTransaction.setAmount(new BigDecimal("100"));
        abstractTransaction.setDescription("PURCHASE Mi Tiendita ON 05/08/21 BAYAMON PR");
        abstractTransaction.setType("CR");
        abstractTransaction.setBackendObject(transaction);
        abstractTransactionList.add(abstractTransaction);

        transaction = new Transaction();
        transaction.setAmount(new BigDecimal("500"));
        transaction.setReasonCode("");
        transaction.setCode("622");

        abstractTransaction = new RunningBalanceTransaction();
        abstractTransaction.setAmount(new BigDecimal("100"));
        abstractTransaction.setDescription("PURCHASE Mi Tiendita2 ON 05/08/21");
        abstractTransaction.setType("DB");
        abstractTransaction.setDate(Calendar.getInstance());
        abstractTransaction.setBackendObject(transaction);
        abstractTransactionList.add(abstractTransaction);

        transaction = new Transaction();
        transaction.setAmount(new BigDecimal("500"));
        transaction.setReasonCode("");
        transaction.setCode("100001");

        abstractTransaction = new RunningBalanceTransaction();
        abstractTransaction.setAmount(new BigDecimal("100"));
        abstractTransaction.setDescription("ATH WITHDRAWAL 3695 BPPR BAYAMON ON 11/30/21");
        abstractTransaction.setType("DB");
        abstractTransaction.setDate(Calendar.getInstance());
        abstractTransaction.setBackendObject(transaction);
        abstractTransactionList.add(abstractTransaction);

        transaction = new Transaction();
        transaction.setAmount(new BigDecimal("500"));
        transaction.setReasonCode(null);
        transaction.setCode(null);

        abstractTransaction = new RunningBalanceTransaction();
        abstractTransaction.setAmount(new BigDecimal("100"));
        abstractTransaction.setDescription("ATH WITHDRAWAL 3695 BPPR BAYAMON ON 11/30/21 BAYAMON PR");
        abstractTransaction.setType("DB");
        abstractTransaction.setDate(Calendar.getInstance());
        abstractTransaction.setBackendObject(transaction);
        abstractTransactionList.add(abstractTransaction);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void whenSetTransactionsCodeReason_ThenInvokeGetTransactions_ThenReturnTransactionsWithCode() throws Exception {
        initializeTransactionList();

        List<AbstractTransaction> listResult = Whitebox
                .invokeMethod(transactionForm, "setTransactionsCodeReason", abstractTransactionList);
        PowerMockito.verifyStatic(TransactionUtil.class);
        TransactionUtil.getTransactions(any(ArrayList.class), anyString(), anyString());
    }
    
   
    @Test
    public void whenGetTransactions_ThenInvokeIsDescriptionInTransactionsTypeText () throws Exception {
        initializeTransactionList();
        String transactionCodes = "622,625,675,679,518-98,581-98,618-98,681-96,681-98,615-0,671-0,676-0," +
                "615-145,671-145,615-214,671-214,676-214";
        TransactionUtil.getTransactions(abstractTransactionList, transactionCodes,transactionTypes);
        PowerMockito.verifyPrivate(TransactionUtil.class, atLeast(1)).invoke("isDescriptionInTransactionsTypeText", anyString(), anyString());;
    }
}