package evertec.cibp.web.utils;

import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.transactions.Transaction;
import com.evertecinc.seagull.SeagullService;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.base.AbstractTransaction;
import evertec.cibp.core.models.base.RunningBalanceTransaction;
import evertec.cibp.core.services.customer.modules.EnrollmentCustomerServiceModule;
import evertec.cibp.vs.models.results.AccountListActionResult;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.mockito.Mockito.mock;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ TransactionUtil.class })
public class TransactionUtilUT extends AbstractModelAndViewTests {

    @Mock
    TVFrontendAccount enrollAccount;

    @Mock
    EnrollmentCustomerServiceModule enrollmentService;

    AccountListActionResult acctList;

    List<Account> accountsList;

    @Mock
    SeagullService seagullService;

    @InjectMocks
    TransactionUtil controller;

    private List<AbstractTransaction> abstractTransactionList;
    private String transactionCodes;
    private String transactionTypes = "PURCHASE,ATH WITHDRAWAL,EFT";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        controller = mock(TransactionUtil.class);
        enrollmentService = mock(EnrollmentCustomerServiceModule.class);
        seagullService = mock(SeagullService.class);
        enrollAccount = mock(TVFrontendAccount.class);
        acctList = new AccountListActionResult();
        accountsList = new ArrayList<>();
    }

    private void initializeTransactionList(){

        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal("125"));
        transaction.setReasonCode("214");
        transaction.setCode("676");

        abstractTransactionList = new ArrayList<>();
        AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
        abstractTransaction.setAmount(new BigDecimal("100"));
        abstractTransaction.setDescription("PURCHASE Mi Tiendita ON 05/08/21 BAYAMON PR");
        abstractTransaction.setType("CR");
        abstractTransaction.setBackendObject(transaction);
        abstractTransactionList.add(abstractTransaction);

        transaction = new Transaction();
        transaction.setAmount(new BigDecimal("500"));
        transaction.setReasonCode("");
        transaction.setCode("622");

        abstractTransaction = new RunningBalanceTransaction();
        abstractTransaction.setAmount(new BigDecimal("100"));
        abstractTransaction.setDescription("PURCHASE Mi Tiendita2 ON 05/08/21");
        abstractTransaction.setType("DB");
        abstractTransaction.setDate(Calendar.getInstance());
        abstractTransaction.setBackendObject(transaction);
        abstractTransactionList.add(abstractTransaction);

        transaction = new Transaction();
        transaction.setAmount(new BigDecimal("500"));
        transaction.setReasonCode("");
        transaction.setCode("100001");

        abstractTransaction = new RunningBalanceTransaction();
        abstractTransaction.setAmount(new BigDecimal("100"));
        abstractTransaction.setDescription("ATH WITHDRAWAL 3695 BPPR BAYAMON ON 11/30/21");
        abstractTransaction.setType("DB");
        abstractTransaction.setDate(Calendar.getInstance());
        abstractTransaction.setBackendObject(transaction);
        abstractTransactionList.add(abstractTransaction);

        transaction = new Transaction();
        transaction.setAmount(new BigDecimal("500"));
        transaction.setReasonCode(null);
        transaction.setCode(null);

        abstractTransaction = new RunningBalanceTransaction();
        abstractTransaction.setAmount(new BigDecimal("100"));
        abstractTransaction.setDescription("ATH WITHDRAWAL 3695 BPPR BAYAMON ON 11/30/21 BAYAMON PR");
        abstractTransaction.setType("DB");
        abstractTransaction.setDate(Calendar.getInstance());
        abstractTransaction.setBackendObject(transaction);
        abstractTransactionList.add(abstractTransaction);

        transactionCodes = "622,625,675,679,518-98,581-98,618-98,681-96,681-98,615-0,671-0,676-0," +
                "615-145,671-145,615-214,671-214,676-214";
    }

    @Test
    public void whenGetTransactions_ThenReturnTransactionsWithCode(){
        initializeTransactionList();

        TransactionUtil.getTransactions(abstractTransactionList, transactionCodes,transactionTypes);
        assertTrue(abstractTransactionList.get(0).getShowDetailEnabled());
        assertFalse(abstractTransactionList.get(2).getShowDetailEnabled());
    }
    
    @Test
    public void whenGetTransactions_givenATHMovilTransaction_ThenReturnTransactionsWithCode(){
        
    	 Transaction transaction = new Transaction();
         transaction.setAmount(new BigDecimal("125"));
         transaction.setReasonCode("214");
         transaction.setCode("676");

         abstractTransactionList = new ArrayList<>();
         AbstractTransaction abstractTransaction = new RunningBalanceTransaction();
         abstractTransaction.setAmount(new BigDecimal("100"));
         abstractTransaction.setDescription("ATH Movil 12345 ON 05/08/21 BAYAMON PR");
         abstractTransaction.setType("CR");
         abstractTransaction.setBackendObject(transaction);
         abstractTransactionList.add(abstractTransaction);
         
         transaction = new Transaction();
         transaction.setAmount(new BigDecimal("500"));
         transaction.setReasonCode("");
         transaction.setCode("622");

         abstractTransaction = new RunningBalanceTransaction();
         abstractTransaction.setAmount(new BigDecimal("100"));
         abstractTransaction.setDescription("PURCHASE Mi Tiendita2 ON 05/08/21");
         abstractTransaction.setType("DB");
         abstractTransaction.setDate(Calendar.getInstance());
         abstractTransaction.setBackendObject(transaction);
         abstractTransactionList.add(abstractTransaction);
         
         transactionCodes = "622,625,675,679,518-98,581-98,618-98,681-96,681-98,615-0,671-0,676-0," +
                 "615-145,671-145,615-214,671-214,676-214";

        TransactionUtil.getTransactions(abstractTransactionList, transactionCodes,transactionTypes);
        assertFalse(abstractTransactionList.get(0).getShowDetailEnabled());
        assertTrue(abstractTransactionList.get(1).getShowDetailEnabled());
    }
}