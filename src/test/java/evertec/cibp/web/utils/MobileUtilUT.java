package evertec.cibp.web.utils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.postinitialfunding.PIFAccountTypes;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.AccountsClasificationRequest;
import evertec.cibp.core.services.responses.AccountsClasificationResponse;
import evertec.cibp.vs.beans.BankingSession;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import evertec.cibp.web.session.UserSession;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.mock.web.MockHttpServletRequest;
import java.util.LinkedList;
import java.util.List;


public class MobileUtilUT
{

    @Mock
    private UserSession userSession;

    @Mock
    private BankingSession bankingSession;

    @Mock
    private CustomerProfile profile;

    @Mock
    private CustomerServices customerService;

    @Mock
    private CustomerEntitlement customerEntitlement;

    private MockHttpServletRequest request;

    @Mock
    private TVFrontendAccount account;

    @Mock
	private TVAccountProductDefinition productDefinition;

    @Mock
    private AccountsClasificationResponse accountsResponse;

    @Mock
    private  AccountsClasificationRequest accountsRequest;

    @Mock
    private List<TVFrontendAccount> accounts;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @InjectMocks
    private MobileUtil mobileUtil;


    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        mobileUtil = PowerMockito.spy(new MobileUtil());
        request = new MockHttpServletRequest();
        when(userSession.getCustomerService()).thenReturn(customerService);
    }

    @Test
    public void whenGetAccounts_GivenAccountClose_thenReturnListAccountsNotNull() throws Exception
    {
        List<TVFrontendAccount> userAccounts = new LinkedList<TVFrontendAccount>();
        List<PIFAccountTypes> allowed = new LinkedList<PIFAccountTypes>();

        when(account.getSubtype()).thenReturn("PFC");
        userAccounts.add(account);
        when(profile.getUsername()).thenReturn("username");
        when(profile.getEntitlementByType(EntitlementType.BANKING)).thenReturn(customerEntitlement);
        when(customerService.getCustomerAccounts(profile, bankingSession, true)).thenReturn(userAccounts);
        accountsRequest.setCustomerProfile(profile);
        when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
        when(accountsResponse.getDepositsSavingsAccounts()).thenReturn(accounts);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(account.getProductDefinition().getProductStatus()).thenReturn("CLOSE");
        when(account.getProductDefinition().getProductStatusCode()).thenReturn("C");
        assertNotNull(mobileUtil.getAccounts(request, bankingSession, profile, customerService));
    }

    @Test
    public void whenGetAccounts_GivenNoCloseAccounts_thenReturnListAccounts() throws Exception
    {
        List<TVFrontendAccount> userAccounts = new LinkedList<TVFrontendAccount>();
        List<PIFAccountTypes> allowed = new LinkedList<PIFAccountTypes>();

        when(account.getSubtype()).thenReturn("PFC");
        userAccounts.add(account);
        when(profile.getUsername()).thenReturn("username");
        when(profile.getEntitlementByType(EntitlementType.BANKING)).thenReturn(customerEntitlement);
        when(customerService.getCustomerAccounts(profile, bankingSession, true)).thenReturn(userAccounts);
        accountsRequest.setCustomerProfile(profile);
        when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
        when(accountsResponse.getDepositsSavingsAccounts()).thenReturn(accounts);
        when(account.getProductDefinition()).thenReturn(productDefinition);
        when(account.getProductDefinition().getProductStatus()).thenReturn("ACTIVE");
        when(account.getProductDefinition().getProductStatusCode()).thenReturn("A");

        assertTrue((mobileUtil.getAccounts(request, bankingSession, profile, customerService).size()) > 0);
    }

    @Test
    public void whenGetAccounts_GivenListAccountsIsEmpty_thenReturnListAccounts() throws Exception
    {
        List<TVFrontendAccount> userAccounts = new LinkedList<TVFrontendAccount>();
        when(profile.getUsername()).thenReturn("username");
        when(account.getSubtype()).thenReturn("PFC");
        when(profile.getEntitlementByType(EntitlementType.BANKING)).thenReturn(customerEntitlement);
        when(customerService.getCustomerAccounts(profile, bankingSession, true)).thenReturn(userAccounts);
        assertNotNull(mobileUtil.getAccounts(request, bankingSession, profile, customerService));
    }

    @Test
    public void whenGetAccounts_GivenListAccountsNull_thenReturnListAccounts() throws Exception
    {
        List<TVFrontendAccount> userAccounts = new LinkedList<TVFrontendAccount>();
        when(profile.getUsername()).thenReturn("username");
        when(profile.getEntitlementByType(EntitlementType.BANKING)).thenReturn(customerEntitlement);
        assertNotNull(mobileUtil.getAccounts(request, bankingSession, profile, customerService));
    }

    @Test
    public void whenGetAccounts_GivenNoUsername_thenReturnListAccounts() throws Exception
    {
        assertNotNull(mobileUtil.getAccounts(request, bankingSession, profile, customerService));
    }

    @Test
    public void whenGetAccounts_GivenNoProfile_thenReturnListAccounts() throws Exception
    {
        assertNotNull(mobileUtil.getAccounts(request, bankingSession, null, customerService));
    }

    @Test
    public void whenGetAccounts_GivenNoEntitlement_thenReturnListAccounts() throws Exception
    {
        when(profile.getUsername()).thenReturn("username");
        when(profile.getEntitlementByType(EntitlementType.BANKING)).thenReturn(null);
        assertNotNull(mobileUtil.getAccounts(request, bankingSession, profile, customerService));
    }

    @Test
    public void whenGetAccounts_GivenNoBankingSession_thenReturnListAccounts() throws Exception
    {
        when(profile.getUsername()).thenReturn("username");
        when(profile.getEntitlementByType(EntitlementType.BANKING)).thenReturn(customerEntitlement);
        assertNotNull(mobileUtil.getAccounts(request, null, profile, customerService));
    }
}
