package evertec.cibp.web.utils;

/**
 * @author Henry Terrero
 * @Version 1.1
 * @Since Apr 15, 2019
 */
public interface TestConstants {

    /**
     * Endpoints for get customer mbop information
     */
    String GET_CUSTOMER_MBOP_INFO = "customers.get.mbopinformation";

    /**
     * Endpoints for update customer passwords
     */
    String UPDATE_CUSTOMER_PASSWORDS = "customers.passwords";
    
    
    String GET_CUSTOMER_PAYEES = "payments.get.payees";

    /**
     * Endpoints for get customer mbop information
     */
    String ENDPOINT_CUSTOMER_MBOP_INFO = "/v1/customers/{profileId}/mbopinformation";


    /** The Constant GET_ATH_EVERY_TRANS_ALERTS */
     String GET_ATH_EVERY_TRANS_ALERTS ="alerts.get.ath.everytrans";
     
     
 	/** Endpoints for alerts service. */
 	 String GET_ALERTS_HISTORY = "alerts.get.history";
 	 String GET_ALERTS_HISTORY_ENDPOINT = "/v1/alerts/history";

    /** Endpoints for dci service. */
    String GET_DCI_QUESTIONS = "dci.get.dci.questions";

    /**
     * Endpoints for get customer enrollment information
     */
    String GET_ATH_EVERY_TRANS_ALERTS_ENDPOINT = "/v1/alerts/customers/{profileId}/every-alerts";

    /**
     * Endpoints for get dci question answer information
     */
    String GET_DCI_QUESTION_ANSWER = "dci.get.dci.question.answer";

    /**
     * The constant profileId
     */
    String PROFILE_ID = "ff8081811ac1d21c011ac2a4226a2cf5";

    /**
     * The constant profileId is empty
     */
    String PROFILE_ID_EMPTY = "";

    /**
     * The constant profileId is special characters
     */
    String PROFILE_ID_SPECIAL_CHARACTERS = "8a81#0b763418fa301634572ae6400%$";

    /**
     * The constant profileId with invalid length
     */
    String PROFILE_ID_WITH_INVALID_LENGTH = "ff8080811d6405ec011d77d9e23";


    /**
     * The constant profileId
     */
    String ATH_EVERY_TRANS_ALERT_ID = "8a81a0b74fadf3ba014fae9fa2a60040";

    String PAYEE_PREFEERENCES_ID = "ff8081811ac1d21c011ac2026f2d2548";


    /**
     * The constant mbopId
     */
    String MBOPID = "8a81a0b74fadf3ba014fae9fa2a60040";
    
    /**
     * The Constant GET_ATH_ANY_ALERT
     */
    String GET_ATH_ANY_ALERT = "/alerts/customers/{profileId}/any-alerts";

    /** The Constant GET_DCI_ANSWER_BY_TYPE */
    String GET_DCI_ANSWER_BY_TYPE ="dci.get.dci.answer.by.type";

    /** Endpoints for mass compromise object service. */
    String GET_MASS_COMPROMISE_OBJECT = "accounts.get.mass.compromise.object";//the endpoint

    /** The Constant SAVE VIEW CAMPAIGN */
    String SAVE_VIEW_CAMPAIGN = "campaigns.save.view.campaign";

    /**
     * The Constant SMS_ALERT
     */
    String SMS_ALERTS = "/alerts/taxes/{taxId}/accounts/{accountNumber}/cards/{cardNumber}/sms-alerts";

    
    String SMS_ACCOUNT_ID = "ff8081811ac1d21c011ac2a4226a2cee";

    /**
     * Account Number test value
     */
    String ACCOUNT_NUMBER = "100000000";

    /**
     * Card Number test value
     */
    String CARD_NUMBER = "4556111860530513";

    /**
     * Tax ID test value
     */
    String TAX_ID = "598031764";


    /**
     * INVALID Profile ID test value
     */
    String PROFILE_ID_INVALID = "ff8081811ac1d21c011ac2a4226a2";

    /** The Constant MICROSERVICE_RESPONSE_OK_STATUS */
    int MICROSERVICE_RESPONSE_OK_STATUS = 200;

    /** The Constant MICROSERVICE_RESPONSE_BAD_REQUEST_STATUS */
    int MICROSERVICE_RESPONSE_BAD_REQUEST_STATUS = 400;

    /** The Constant MICROSERVICE_RESPONSE_CONFLICT_STATUS */
    int MICROSERVICE_RESPONSE_CONFLICT_STATUS = 409;

    /** The Constant MICROSERVICE_RESPONSE_NOT_CONTENT_STATUS */
    int MICROSERVICE_RESPONSE_NOT_CONTENT_STATUS = 204;

    /**
	 * Endpoints for vault accounts service
	 */
	String GET_ACCOUNTS = "vaultaccounts.get.accounts";

}
