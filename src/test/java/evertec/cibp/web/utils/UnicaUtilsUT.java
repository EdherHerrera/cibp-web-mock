package evertec.cibp.web.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import com.evertec.cibp.api.client.util.ClientUtils;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CreditCardFacade;
import evertec.cibp.web.facade.CustomerFacade;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.CustomerFlag;
import evertec.cibp.core.models.customer.flags.CustomerFlagType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.form.bean.UnicaApplication;
import evertec.cibp.web.session.UserSession;
import evertec.otherservices.beans.unica.Status;
import evertec.otherservices.beans.unica.UnicaApplicationInfo;
import evertec.otherservices.beans.unica.UnicaInquiry;
import evertec.otherservices.enums.ResponseStatus;
import evertec.otherservices.responses.UnicaInqResponse;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.util.WebUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ApiCallerFactory.class})
public class UnicaUtilsUT {
	
	String unicaAllowedStatus;
	
	String unicaLoanProducts;
	
	String unicaCreditProducts;
	
	String marketplaceAllowedStatus;
	
	String marketplaceProducts;

	@Mock
	private ApiCaller apiCaller;
	
	@Mock
	private CustomerServices customerService;

	@Before
	public void setUp() throws Exception {
		unicaAllowedStatus = "ap-approved,uk-approved,en-inprocess";
		marketplaceAllowedStatus = "ap-approved,uk-approved,en-inprocess,td-turneddown,bk-booked";
		unicaLoanProducts = "ULOAN,XX20,XX50,REQ,XX23,XX22,XX24,XX24C,XX28,XX20S,XX50S,XX20T,XX50T,XX20P,XX50P";
		unicaCreditProducts = "ULINE,MWAWR,UMEDP,VSNOV,MCEDG,AEULT,VSRTN,VSELT,UVNOV,UMEDG,UAULT,VSSEC,VSICN,VSELT,APREG,UMEDP,VWAIN,VWASI,VWNOS,AEELT,AEICN";
		marketplaceProducts = "VPCBK,VPPRE,VWAIP,MGJBG,MWJBB,UVREG";
		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);
	}

	@Test
	public void whenGetApplications_GivenUnicaApplicationsEmpty_ThenReturnEmptyList() {
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(new ArrayList<UnicaApplication>());
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, null, null, null, null, null);
		assertEquals(new ArrayList<UnicaApplication>(), applications);
	}
	
	@Test
	public void whenGetApplications_GivenUnicaApplicationsNullAndTaxIdNull_ThenReturnEmptyList() {
		CustomerProfile customerProfile = new CustomerProfile();
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, null, null, null, null, null);
		assertEquals(new ArrayList<UnicaApplication>(), applications);
	}
	
	@Test
	public void whenGetApplications_GivenUnicaResponseNull_ThenReturnEmptyList() {
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(null);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, null, null, null, null, null);
		assertEquals(new ArrayList<UnicaApplication>(), applications);
	}
	
	@Test
	public void whenGetApplications_GivenUnicaResponseStatusNull_ThenReturnEmptyList() {
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(null);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, null, null, null, null, null);
		assertEquals(new ArrayList<UnicaApplication>(), applications);
	}
	
	@Test
	public void whenGetApplications_GivenUnicaResponseStatusError_ThenReturnEmptyList() {
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.ERROR);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, null, null, null, null, null);
		assertEquals(new ArrayList<UnicaApplication>(), applications);
	}
	
	@Test
	public void whenGetApplications_GivenDatapowerStatusNull_ThenReturnEmptyList() {
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(null);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, null, null, null, null, null);
		assertEquals(new ArrayList<UnicaApplication>(), applications);
	}
	
	@Test
	public void whenGetApplications_GivenDatapowerStatusEqualTo01_ThenReturnEmptyList() {
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("01");
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, null, null, null, null, null);
		assertEquals(new ArrayList<UnicaApplication>(), applications);
	}
	
	@Test
	public void whenGetApplications_GivenUnicaInquiryNull_ThenReturnEmptyList() {
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(null);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, null, null, null, null, null);
		assertEquals(new ArrayList<UnicaApplication>(), applications);
	}
	
	@Test
	public void whenGetApplications_GivenUnicaInquiryLookupResponseNull_ThenReturnEmptyList() {
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(new UnicaInquiry());
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, null, null, null, null, null);
		assertEquals(new ArrayList<UnicaApplication>(), applications);
	}
	
	@Test
	public void whenGetApplications_GivenUnicaResponseWithEmptyApplications_ThenReturnListWithEmptyList() {
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaApplicationInfo[] lookupResponse = {};
		UnicaInquiry unicaInquiry = new UnicaInquiry();
		unicaInquiry.setLookupResponse(lookupResponse);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(unicaInquiry);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, 
				unicaAllowedStatus, unicaLoanProducts, unicaCreditProducts, null, null);
		
		assertEquals(0, applications.size());
	}
	
	@Test
	public void whenGetApplications_GivenEmptyUnicaApplicationsAndRefreshTrueAndUnicaResponseWithOneCreditApplication_ThenReturnListWithOneCreditApplication() {
		Set<CustomerFlag> customerFlags = new HashSet<>();
		customerFlags.add(new CustomerFlag(CustomerFlagType.PENDING_UNICA_APPLICATION));
		
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		customerProfile.setCustomerFlags(customerFlags);
		
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(new ArrayList<UnicaApplication>());
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaApplicationInfo application = new UnicaApplicationInfo();
		application.setApplicationId("54325324543");
		application.setApplStatusDesc("EN");
		application.setProductCode("VSNOV");
		UnicaApplicationInfo[] lookupResponse = {application};
		UnicaInquiry unicaInquiry = new UnicaInquiry();
		unicaInquiry.setLookupResponse(lookupResponse);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(unicaInquiry);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, "true", 
				unicaAllowedStatus, unicaLoanProducts, unicaCreditProducts, marketplaceAllowedStatus, "");
		
		assertEquals(1, applications.size());
		assertEquals("unica.product.creditcard", applications.get(0).getProductNameVariable());
	}
	
	@Test
	public void whenGetApplications_GivenUnicaResponseWithOneCreditApplication_ThenReturnListWithOneCreditApplication() {
		Set<CustomerFlag> customerFlags = new HashSet<>();
		customerFlags.add(new CustomerFlag(CustomerFlagType.PENDING_UNICA_APPLICATION));
		
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		customerProfile.setCustomerFlags(customerFlags);
		
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaApplicationInfo application = new UnicaApplicationInfo();
		application.setApplicationId("54325324543");
		application.setApplStatusDesc("EN");
		application.setProductCode("VSNOV");
		UnicaApplicationInfo[] lookupResponse = {application};
		UnicaInquiry unicaInquiry = new UnicaInquiry();
		unicaInquiry.setLookupResponse(lookupResponse);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(unicaInquiry);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, 
				unicaAllowedStatus, unicaLoanProducts, unicaCreditProducts, marketplaceAllowedStatus, "");
		
		assertEquals(1, applications.size());
		assertEquals("unica.product.creditcard", applications.get(0).getProductNameVariable());
	}
	
	@Test
	public void whenGetApplications_GivenUnicaResponseWithOneMarketplaceApplication_ThenReturnListWithOneMarketplaceApplication() {
		Set<CustomerFlag> customerFlags = new HashSet<>();
		customerFlags.add(new CustomerFlag(CustomerFlagType.PENDING_UNICA_APPLICATION));
		
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		customerProfile.setCustomerFlags(customerFlags);
		
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaApplicationInfo application = new UnicaApplicationInfo();
		application.setApplicationId("54325324543");
		application.setApplStatusDesc("EN");
		application.setProductCode("VPCBK");
		UnicaApplicationInfo[] lookupResponse = {application};
		UnicaInquiry unicaInquiry = new UnicaInquiry();
		unicaInquiry.setLookupResponse(lookupResponse);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(unicaInquiry);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, 
				unicaAllowedStatus, unicaLoanProducts, unicaCreditProducts, marketplaceAllowedStatus, marketplaceProducts);
		
		assertEquals(1, applications.size());
		assertTrue(applications.get(0).isFromMarketplace());
	}
	
	@Test
	public void whenGetApplications_GivenUnicaResponseWithOneMarketplaceApplicationAndUnallowedStatus_ThenReturnEmptyList() {
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaApplicationInfo application = new UnicaApplicationInfo();
		application.setApplicationId("54325324543");
		application.setApplStatusDesc("ES");
		application.setProductCode("VPCBK");
		UnicaApplicationInfo[] lookupResponse = {application};
		UnicaInquiry unicaInquiry = new UnicaInquiry();
		unicaInquiry.setLookupResponse(lookupResponse);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(unicaInquiry);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, 
				unicaAllowedStatus, unicaLoanProducts, unicaCreditProducts, marketplaceAllowedStatus, marketplaceProducts);
		
		assertEquals(0, applications.size());
	}
	
	@Test
	public void whenGetApplications_GivenUnicaResponseWithOneCreditApplicationAndEmptyCreditProducts_ThenReturnListWithOneUndefinedApplication() {
		Set<CustomerFlag> customerFlags = new HashSet<>();
		customerFlags.add(new CustomerFlag(CustomerFlagType.PENDING_UNICA_APPLICATION));
		
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		customerProfile.setCustomerFlags(customerFlags);
		
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaApplicationInfo application = new UnicaApplicationInfo();
		application.setApplicationId("54325324543");
		application.setApplStatusDesc("EN");
		application.setProductCode("VSNOV");
		UnicaApplicationInfo[] lookupResponse = {application};
		UnicaInquiry unicaInquiry = new UnicaInquiry();
		unicaInquiry.setLookupResponse(lookupResponse);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(unicaInquiry);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, 
				unicaAllowedStatus, unicaLoanProducts, "", marketplaceAllowedStatus, marketplaceProducts);
		
		assertEquals(1, applications.size());
		assertEquals("unica.product.undefined", applications.get(0).getProductNameVariable());
	}
	
	@Test
	public void whenGetApplications_GivenUnicaResponseWithOneLoanApplication_ThenReturnListWithOneLoanApplication() {
		Set<CustomerFlag> customerFlags = new HashSet<>();
		customerFlags.add(new CustomerFlag(CustomerFlagType.PENDING_UNICA_APPLICATION));
		
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		customerProfile.setCustomerFlags(customerFlags);
		
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaApplicationInfo application = new UnicaApplicationInfo();
		application.setApplicationId("54325324543");
		application.setApplStatusDesc("EN");
		application.setProductCode("ULOAN");
		UnicaApplicationInfo[] lookupResponse = {application};
		UnicaInquiry unicaInquiry = new UnicaInquiry();
		unicaInquiry.setLookupResponse(lookupResponse);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(unicaInquiry);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, 
				unicaAllowedStatus, unicaLoanProducts, unicaCreditProducts, marketplaceAllowedStatus, "");
		
		assertEquals(1, applications.size());
		assertEquals("unica.product.personalloan", applications.get(0).getProductNameVariable());
	}
	
	@Test
	public void whenGetApplications_GivenLookupResponseWithNoApplication_ThenReturnEmptyList() {
		Set<CustomerFlag> customerFlags = new HashSet<>();
		customerFlags.add(new CustomerFlag(CustomerFlagType.PENDING_UNICA_APPLICATION));
		
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		customerProfile.setCustomerFlags(customerFlags);
		
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaApplicationInfo[] lookupResponse = {};
		UnicaInquiry unicaInquiry = new UnicaInquiry();
		unicaInquiry.setLookupResponse(lookupResponse);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(unicaInquiry);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, 
				unicaAllowedStatus, unicaLoanProducts, unicaCreditProducts, marketplaceAllowedStatus, "");
		
		assertEquals(0, applications.size());
	}
	
	@Test
	public void whenGetApplications_GivenLookupResponseWithNoApplicationAndResponseCode0WithNoApplication_ThenReturnEmptyList() {
		Set<CustomerFlag> customerFlags = new HashSet<>();
		customerFlags.add(new CustomerFlag(CustomerFlagType.PENDING_UNICA_APPLICATION));
		
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		customerProfile.setCustomerFlags(customerFlags);
		
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaApplicationInfo[] lookupResponse = {};
		UnicaInquiry unicaInquiry = new UnicaInquiry();
		unicaInquiry.setLookupResponse(lookupResponse);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(unicaInquiry);
		unicaInquiry.setResponseCode("0");
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, 
				unicaAllowedStatus, unicaLoanProducts, unicaCreditProducts, marketplaceAllowedStatus, "");
		
		assertEquals(0, applications.size());
	}
	
	@Test
	public void whenGetApplications_GivenLookupResponseWithNoApplicationAndResponseCode1_ThenReturnEmptyList() {
		Set<CustomerFlag> customerFlags = new HashSet<>();
		customerFlags.add(new CustomerFlag(CustomerFlagType.PENDING_UNICA_APPLICATION));
		
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		customerProfile.setCustomerFlags(customerFlags);
		
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaApplicationInfo[] lookupResponse = {};
		UnicaInquiry unicaInquiry = new UnicaInquiry();
		unicaInquiry.setLookupResponse(lookupResponse);
		unicaInquiry.setResponseCode("1");
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(unicaInquiry);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, 
				unicaAllowedStatus, unicaLoanProducts, unicaCreditProducts, marketplaceAllowedStatus, "");
		
		assertEquals(0, applications.size());
	}
	
	@Test
	public void whenGetApplications_GivenUnicaResponseWithOneLoanApplicationAndEmptyLoanProducts_ThenReturnListWithOneUndefinedApplication() {
		Set<CustomerFlag> customerFlags = new HashSet<>();
		customerFlags.add(new CustomerFlag(CustomerFlagType.PENDING_UNICA_APPLICATION));
		
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		customerProfile.setCustomerFlags(customerFlags);
		
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaApplicationInfo application = new UnicaApplicationInfo();
		application.setApplicationId("54325324543");
		application.setApplStatusDesc("EN");
		application.setProductCode("ULOAN");
		UnicaApplicationInfo[] lookupResponse = {application};
		UnicaInquiry unicaInquiry = new UnicaInquiry();
		unicaInquiry.setLookupResponse(lookupResponse);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(unicaInquiry);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, 
				unicaAllowedStatus, "", unicaCreditProducts, marketplaceAllowedStatus, "");
		
		assertEquals(1, applications.size());
		assertEquals("unica.product.undefined", applications.get(0).getProductNameVariable());
	}
	
	@Test
	public void whenGetApplications_GivenUnicaResponseWithOneUndefinedApplication_ThenReturnListWithOneApplication() {
		
		Set<CustomerFlag> customerFlags = new HashSet<>();
		customerFlags.add(new CustomerFlag(CustomerFlagType.PENDING_UNICA_APPLICATION));
		
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		customerProfile.setCustomerFlags(customerFlags);
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaApplicationInfo application = new UnicaApplicationInfo();
		application.setApplicationId("54325324543");
		application.setApplStatusDesc("EN");
		application.setProductCode("GFSDGFDSG");
		UnicaApplicationInfo[] lookupResponse = {application};
		UnicaInquiry unicaInquiry = new UnicaInquiry();
		unicaInquiry.setLookupResponse(lookupResponse);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(unicaInquiry);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, 
				unicaAllowedStatus, unicaLoanProducts, unicaCreditProducts, marketplaceAllowedStatus, "");
		
		assertEquals(1, applications.size());
	}
	
	@Test
	public void whenGetApplications_GivenUnicaResponseWithOneApplicationWithUnallowedStatus_ThenReturnEmptyList() {
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setTaxId("567879809");
		UserSession userSession = new UserSession();
		userSession.setUnicaApplications(null);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		
		Status datapowerStatus = new Status();
		datapowerStatus.setStatusCode("00");
		
		UnicaApplicationInfo application = new UnicaApplicationInfo();
		application.setApplicationId("54325324543");
		application.setApplStatusDesc("ZZ");
		application.setProductCode("ULOAN");
		UnicaApplicationInfo[] lookupResponse = {application};
		UnicaInquiry unicaInquiry = new UnicaInquiry();
		unicaInquiry.setLookupResponse(lookupResponse);
		
		UnicaInqResponse response = new UnicaInqResponse();
		response.setResponseStatus(ResponseStatus.SUCCESS);
		response.setDatapowerStatus(datapowerStatus);
		response.setUnicaInq(unicaInquiry);
		
		when(customerService.getUnicaBySSN(any())).thenReturn(response);
		
		List<UnicaApplication> applications = UnicaUtils.getApplications(userSession, null, 
				unicaAllowedStatus, "", unicaCreditProducts, marketplaceAllowedStatus, "");
		
		assertTrue(applications.isEmpty());
	}

}
