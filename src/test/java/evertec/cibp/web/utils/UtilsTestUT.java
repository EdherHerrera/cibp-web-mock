package evertec.cibp.web.utils;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.campaign.ActiveCampaignView;
import evertec.cibp.core.models.campaign.CampaignType;
import evertec.cibp.web.session.UserSession;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ApiCallerFactory.class})
public class UtilsTestUT {

    UserSession userSession;

	@Mock
	private ApiCaller apiCaller;

    private final List<ActiveCampaignView> activeCampaignView = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

		userSession = PowerMockito.mock(UserSession.class);

        ActiveCampaignView campaignView = new ActiveCampaignView();
        campaignView.setApprovalDate(new Date());
        campaignView.setCampaignName("TEST MESSAGE");
        campaignView.setFileCampaign(null);
        campaignView.setId("546546546");
        campaignView.setType(CampaignType.MESSAGES);
        campaignView.setAttributeData("a6f4a65sd465a1sd4c65asd4f65asd4f65as4df65a46f54asd65f4a65f46a5s4f");

        activeCampaignView.add(campaignView);

        campaignView = new ActiveCampaignView();
        campaignView.setApprovalDate(new Date());
        campaignView.setCampaignName("TEST MESSAGE 2");
        campaignView.setFileCampaign(null);
        campaignView.setId("46546544");
        campaignView.setType(CampaignType.MESSAGES);
        campaignView.setIncludeSegments("<properties> <property name='ALL' value='ALL'/> </properties>");
        campaignView.setAttributeData("a6f4a65sd465a1sd4c65asd4f65asd4f65as4df65a46f54asd65f4a65f46a5s4f");

        activeCampaignView.add(campaignView);
    }

    /**
	 * when RemoveInactiveCardPlastics
	 * give InactiveCardPlastic
	 * then EmptyArray
	 * s679338
	 */
	@Test
	public void whenRemoveInactiveCardPlastics_giveInactiveCardPlastic_thenEmptyArray() throws Exception{
		CardPlasticAccountRecord record = new CardPlasticAccountRecord();
		record.setStatus("N    INACTIVE");
		CardPlasticFrontend inactiveCard = new CardPlasticFrontend(record);
		
		CardPlasticFrontend[] cardPlastics = {
			inactiveCard
		};
		
		CardPlasticFrontend[] activeCardPlastics = Utils.removeInactiveCardPlastics(userSession, cardPlastics);
		
		assertEquals(0, activeCardPlastics.length);
	}
	
	/**
	 * when RemoveInactiveCardPlastics
	 * give ActiveCardPlastic
	 * then Not Empty Array
	 * s679338
	 */
	@Test
	public void whenRemoveInactiveCardPlastics_giveActiveCardPlastic_thenNotEmptyArray() throws Exception{
		CardPlasticAccountRecord record = new CardPlasticAccountRecord();
		record.setStatus("Y    ACTIVE");
		CardPlasticFrontend activeCard = new CardPlasticFrontend(record);
		
		CardPlasticFrontend[] cardPlastics = {
			activeCard
		};
		
		CardPlasticFrontend[] activeCardPlastics = Utils.removeInactiveCardPlastics(userSession, cardPlastics);
		
		assertEquals(1, activeCardPlastics.length);
	}
	
	/**
	 * when Set CCAPlasticCardStatus
	 * give Account With PlasticCard
	 * then PlasticCard Set Status
	 * s679338
	 */
	@Test
	public void whenSetCCAPlasticCardStatus_giveAccountWithPlasticCard_thenPlasticCardSetStatus () throws Exception{
		CardPlasticAccountRecord record = new CardPlasticAccountRecord();
		record.setCardPANId("001");
		CardPlasticFrontend cardPlastic = new CardPlasticFrontend(record);
		
		TVFrontendAccount account = mock(TVFrontendAccount.class);
		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>() {{
			add(account);
		}};
		
		when(userSession.getCcaCardPlastics()).thenReturn(accounts);
		when(account.getAccountKey()).thenReturn("001");
		when(account.getStatus()).thenReturn("Y    ACTIVE");
		
		Utils.setCCAPlasticCardStatus(accounts, cardPlastic);
		
		assertEquals("Y    ACTIVE", cardPlastic.getCardPlasticAccountRecord().getStatus());
	}
	
	/**
	 * when SetCCAPlasticCardStatus
	 * give Account Without PlasticCard
	 * then PlasticCard Not Set Status
	 * s679338
	 */
	@Test
	public void whenSetCCAPlasticCardStatus_giveAccountWithoutPlasticCard_thenPlasticCardNotSetStatus () {
		CardPlasticAccountRecord record = new CardPlasticAccountRecord();
		record.setCardPANId("001");
		CardPlasticFrontend cardPlastic = new CardPlasticFrontend(record);
		
		TVFrontendAccount account = mock(TVFrontendAccount.class);
		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>() {{
			add(account);
		}};
		
		when(userSession.getCcaCardPlastics()).thenReturn(accounts);
		when(account.getAccountKey()).thenReturn("002");
		when(account.getStatus()).thenReturn("Y    ACTIVE");
		
		Utils.setCCAPlasticCardStatus(accounts, cardPlastic);
		
		assertEquals(null, cardPlastic.getCardPlasticAccountRecord().getStatus());
	}

    @Test
    public void whenUpdateMessagesList_GivenActiveCampaignViewNotNull () throws Exception {

        Utils.updateMessagesList(activeCampaignView, new UserSession() ,"654:658:888", "888:602:132");
        Assert.assertEquals(activeCampaignView.size(), 2);
    }

    @Test
    public void whenUpdateMessagesList_GivenActiveCampaignViewNotNullAndSegmentCCA () throws Exception {

        ActiveCampaignView campaignView = new ActiveCampaignView();
        campaignView.setApprovalDate(new Date());
        campaignView.setCampaignName("TEST MESSAGE 2");
        campaignView.setFileCampaign(null);
        campaignView.setId("46546544");
        campaignView.setType(CampaignType.MESSAGES);
        campaignView.setIncludeSegments("<properties> <property name='CCA' value='CCA'/> </properties>");
        campaignView.setAttributeData("a6f4a65sd465a1sd4c65asd4f65asd4f65as4df65a46f54asd65f4a65f46a5s4f");

        activeCampaignView.add(campaignView);
        when(userSession.hasCCA()).thenReturn(Boolean.TRUE);

        Utils.updateMessagesList(activeCampaignView, userSession ,"654:658:888", "888:602:132");
        Assert.assertEquals(activeCampaignView.size(), 3);
    }
}
