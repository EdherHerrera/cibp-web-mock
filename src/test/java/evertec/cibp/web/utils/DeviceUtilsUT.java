package evertec.cibp.web.utils;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import evertec.cibp.core.models.customer.CustomerDevices;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.form.bean.PasswordBean;
import evertec.cibp.web.form.dto.DeviceInfoDto;
import evertec.cibp.web.session.UserSession;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;

public class DeviceUtilsUT {

    @Mock
    private UserSession userSession;
    
    @Mock
    private PasswordBean passwordBean;

    private DeviceInfoDto deviceInfoDto;

    private MockHttpServletRequest request;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    public static final String DEVICE_TOKEN_COOKIE = "MB_DEV";

    public static final String IPOD = "iPod";
    public static final String IPHONE = "iPhone";
    public static final String IPAD = "iPad";
    public static final String ANDROID = "Android";
    public static final String BLACKBERRY = "BlackBerry";
    public static final String MOTOROLA = "Motorola";
    public static final String IEMOBILE = "IEMobile";
    public static final String WINDOWSCE = "Windows CE";

    public static final String IPHONE_APP_USER_AGENT = "iPhone";
    public static final String BROWSER_CHROME_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36";
    public static final String BROWSER_FIREFOX_USER_AGENT = "Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1";
    public static final String BROWSER_MS_IE_OLD_USER_AGENT = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)";
    public static final String BROWSER_MS_IE_NEW_USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko:";
    public static final String BROWSER_MS_EDGE_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134";
    public static final String BROWSER_SAFARI_USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        deviceInfoDto = new DeviceInfoDto();
        request = new MockHttpServletRequest();
    }

    @Test
    public void whengetCustomerDevice_GivenUserAgent_thenReturnDevice() throws Exception {

        assertEquals(CustomerDevices.IPOD, DeviceUtils.getCustomerDevice(IPOD));
        assertEquals(CustomerDevices.IPHONE, DeviceUtils.getCustomerDevice(IPHONE));
        assertEquals(CustomerDevices.IPAD, DeviceUtils.getCustomerDevice(IPAD));
        assertEquals(CustomerDevices.ANDROID, DeviceUtils.getCustomerDevice(ANDROID));
        assertEquals(CustomerDevices.BLACKBERRY, DeviceUtils.getCustomerDevice(BLACKBERRY));
        assertEquals(CustomerDevices.MOTOROLA, DeviceUtils.getCustomerDevice(MOTOROLA));
        assertEquals(CustomerDevices.IEMOBILE, DeviceUtils.getCustomerDevice(IEMOBILE));
        assertEquals(CustomerDevices.WINDOWSCE, DeviceUtils.getCustomerDevice(WINDOWSCE));
        assertEquals(CustomerDevices.PERSONAL, DeviceUtils.getCustomerDevice("anyOtherUserAgent"));

    }

    @Test
    public void whengetBrowserInfo_GivenUserAgentIsCHROME_And_UserAngetTypeIsWeb_thenString() throws Exception {
        String result = "Device: WEB<br/>Browser: Chrome<br/>Browser Version: 74.0.3729.169";                 
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        assertEquals(result, DeviceUtils.getBrowserInfo(BROWSER_CHROME_USER_AGENT, userSession));    
    }
        
    @Test
    public void whengetBrowserInfo_GivenUserAgentIsFIREFOX_And_UserAngetTypeIsWeb_thenString() throws Exception {
        String result = "Device: WEB<br/>Browser: Firefox<br/>Browser Version: 7.0.1";                 
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        assertEquals(result, DeviceUtils.getBrowserInfo(BROWSER_FIREFOX_USER_AGENT, userSession));    
    }
    
    @Test
    public void whengetBrowserInfo_GivenUserAgentIsMS_IE_OLD_And_UserAngetTypeIsWeb_thenString() throws Exception {
        String result = "Device: WEB<br/>Browser: Internet Explorer<br/>Browser Version: 6.0";                 
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        assertEquals(result, DeviceUtils.getBrowserInfo(BROWSER_MS_IE_OLD_USER_AGENT, userSession));    
    }
    
    @Test
    public void whengetBrowserInfo_GivenUserAgentIsMS_IE_NEW_And_UserAngetTypeIsWeb_thenString() throws Exception {
        String result = "Device: WEB<br/>Browser: Internet Explorer<br/>Browser Version: 11.0";                 
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        assertEquals(result, DeviceUtils.getBrowserInfo(BROWSER_MS_IE_NEW_USER_AGENT, userSession));    
    }
    
    @Test
    public void whengetBrowserInfo_GivenUserAgentIsMS_EDGE_And_UserAngetTypeIsWeb_thenString() throws Exception {
        String result = "Device: WEB<br/>Browser: Edge<br/>Browser Version: 17.17134";                 
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        assertEquals(result, DeviceUtils.getBrowserInfo(BROWSER_MS_EDGE_USER_AGENT, userSession));    
    }
    
    @Test
    public void whengetBrowserInfo_GivenUserAgentIsSAFARI_And_UserAngetTypeIsWeb_thenString() throws Exception {
        String result = "Device: WEB<br/>Browser: Safari<br/>Browser Version: 9.1.2";                 
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
        assertEquals(result, DeviceUtils.getBrowserInfo(BROWSER_SAFARI_USER_AGENT, userSession));    
    }
    
    
    @Test
    public void whengetBrowserInfo_GivenUserAgentIsAnyOther_And_UserAngetTypeIsIphone_thenString() throws Exception {
        String result = "Device: Android WEB<br/>Browser: unknown<br/>Browser Version: unknown";                 
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.ANDROID);
        assertEquals(result, DeviceUtils.getBrowserInfo("AnyOtherUserAgent", userSession));    
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    @Test
    public void whengetBrowserInfo_GivenUserAgentIsCHROME_And_UserAngetTypeIsAndroid_thenString() throws Exception {
        String result = "Device: Android WEB<br/>Browser: Chrome<br/>Browser Version: 74.0.3729.169";                 
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.ANDROID);
        assertEquals(result, DeviceUtils.getBrowserInfo(BROWSER_CHROME_USER_AGENT, userSession));    
    }
        
    @Test
    public void whengetBrowserInfo_GivenUserAgentIsCHROME_And_UserAngetTypeIsIphone_thenString() throws Exception {
        String result = "Device: iPhone WEB<br/>Browser: Chrome<br/>Browser Version: 74.0.3729.169";                 
        when(userSession.getUserAgentType()).thenReturn(UserAgentType.IPHONE);
        assertEquals(result, DeviceUtils.getBrowserInfo(BROWSER_CHROME_USER_AGENT, userSession));    
    }
    
    @Test
    public void whengetMobileInfo_GivenIphoneApUserAgentAndDeviceInfoNotNull_ThenReturnDeviceInfoasHtmlFormat() throws Exception {
        String result = "<br/>Device: IPHONE<br/>Model: Iphone X<br/>OS Version: 12.1.2<br/>App Version: 3.3.3<br/>Service Provider: Claro Puerto Rico<br/><br/>";
        when(passwordBean.getDeviceModel()).thenReturn("Iphone X");
        when(passwordBean.getAppVersion()).thenReturn("3.3.3");
        when(passwordBean.getOsVersion()).thenReturn("12.1.2");
        when(passwordBean.getNetworkProvider()).thenReturn("Claro Puerto Rico");
        assertEquals(result, DeviceUtils.getMobileInfo(IPHONE_APP_USER_AGENT, deviceInfoDto.passwordBeanToDeviceInfo(passwordBean)));    
    }
    
    @Test
    public void whengetMobileInfo_GivenIphoneApUserAgentAndDeviceInfoNull_ThenReturnDeviceInfoNotAvailableasHtmlFormat() throws Exception {
        String result = "<br/>Device: IPHONE<br/>Model: Not Available<br/>OS Version: Not Available<br/>App Version: Not Available<br/>Service Provider: Not Available<br/><br/>";
        when(passwordBean.getDeviceModel()).thenReturn(null);
        when(passwordBean.getAppVersion()).thenReturn(null);
        when(passwordBean.getOsVersion()).thenReturn(null);
        when(passwordBean.getNetworkProvider()).thenReturn(null);
        assertEquals(result, DeviceUtils.getMobileInfo(IPHONE_APP_USER_AGENT, deviceInfoDto.passwordBeanToDeviceInfo(passwordBean)));    
    }
    
    @Test(expected = NullPointerException.class)
    public void whengetBrowserInfo_GivenUserAgentIsNull_And_UserAngetTypeIsIphone_thenString(){
        DeviceUtils.getBrowserInfo(null, userSession);
    }

    @Test
    public void whenIsUserAgentMobileApp_GivenRequestHeaderUserAgentAndroidOrIosApp_ThenReturnTrue() {
        String iosApp = "iPhone App; MiBanco.app; en-us";
        request.addHeader("user-agent", iosApp);
        boolean isIos = DeviceUtils.isUserAgentMobileApp(request);
        assertTrue(isIos);

        String androidApp = "Android App; MiBanco.app; en-us; WebView";
        request.addHeader("user-agent", androidApp);
        boolean isAndroid = DeviceUtils.isUserAgentMobileApp(request);
        assertTrue(isAndroid);
    }

    @Test
    public void whenIsUserAgentMobileApp_GivenRequestHeaderUserAgentIsNotAndroidOrIosApp_ThenReturnFalse() {
        boolean whenNullUserAgentIsMobileApp = DeviceUtils.isUserAgentMobileApp(request);
        assertFalse(whenNullUserAgentIsMobileApp);

        String iosSafari = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_4 like Mac OS X) " +
                "AppleWebKit/605.1.15 (KHTML, like Gecko) " +
                "Version/12.1.2 Mobile/15E148 Safari/604.1";
        request.addHeader("user-agent", iosSafari);
        boolean isIos = DeviceUtils.isUserAgentMobileApp(request);
        assertFalse(isIos);

        String androidChrome = "Mozilla/5.0 (Linux; Android 10; Android SDK built for x86) " +
                "AppleWebKit/537.36 (KHTML, like Gecko) " +
                "Chrome/74.0.3729.185 Mobile Safari/537.36";
        request.addHeader("user-agent", androidChrome);
        boolean isAndroid = DeviceUtils.isUserAgentMobileApp(request);
        assertFalse(isAndroid);

        String androidFirefox = "Mozilla/5.0 (Android 10; Mobile; rv:68.0) " +
                "Gecko/68.0 Firefox/68.0";
        request.addHeader("user-agent", androidFirefox);
        isAndroid = DeviceUtils.isUserAgentMobileApp(request);
        assertFalse(isAndroid);

        String androidOpera = "Mozilla/5.0 (Linux; Android 10; Android SDK built for x86) " +
                "AppleWebKit/537.36 (KHTML, like Gecko) " +
                "Chrome/78.0.3904.108 Mobile Safari/537.36 OPR/56.1.2780.51589";
        request.addHeader("user-agent", androidOpera);
        isAndroid = DeviceUtils.isUserAgentMobileApp(request);
        assertFalse(isAndroid);
    }

}
