package evertec.cibp.web.utils;

import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import bppr.tv.beans.accounts.types.CardAccount;
import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerPayeePreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.CustomerPaymentInfoResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.beans.payments.Payee;
import evertec.cibp.web.form.CcaPurchasesAndWithdrawalsForm;
import evertec.cibp.web.form.EbppStatusUpdate;
import evertec.cibp.web.form.bean.EbppStatusUpdateBean;
import evertec.cibp.web.form.bean.PurchasesAndWithdrawalsBean;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.prophit.ProphITService;
import evertec.prophit.service.bean.CustomerInquiryBean;
import evertec.prophit.service.bean.EbillIndType;
import evertec.prophit.service.bean.MaintenceEbillBean;
import evertec.prophit.service.bean.MaintenceEbillResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticMessageSource.class, CustomerServices.class, ApiCallerFactory.class, InetAddress.class,
		DeviceUtils.class, WebUtils.class, PaymentsServiceModule.class, RequestContextUtils.class,
		CcaPurchasesAndWithdrawalsForm.class, AlertTypesUtils.class, Utils.class})
public class UtilsIT extends AbstractModelAndViewTests {
	
	@Mock
    UserSession userSession;

	@Mock
	private CustomerInteractionEvent customerInteractionEvent;

	@Mock
	private ProphITService prophITService;

	private final List<TVFrontEndPayee> payeeList = new ArrayList<>();
	private final TVFrontEndPayee frontEndPayee = new TVFrontEndPayee();
	private final List<TVFrontendAccount> accountList = new ArrayList<>();
	private final Account account = new CardAccount();
	private PurchasesAndWithdrawalsBean bean;

	@Mock
	private CustomerPayeePreferences customerPayeePreferences;

	@Mock
	private CustomerInquiryBean inquiryBean;

	@Mock
	private MaintenceEbillResponse maintenceEbillResponse;

	@Mock
	private HttpServletRequest request;

	@Mock
	private BankingSession bankingSession;

	@Mock
	private CustomerServices customerServices;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private PaymentsServiceModule paymentsServiceModule;

	@Mock
	private CustomerPaymentInfoResponse customerPaymentInfoResponse;

	@Mock
	private InetAddress inetAddress;

	@Mock
	private CustomerServices customerService;

	@Mock
	private ApiCaller apiCaller;

	@Spy
	@InjectMocks
	private final EbppStatusUpdate controller = new EbppStatusUpdate();

	@InjectMocks
	@Spy
	private CcaPurchasesAndWithdrawalsForm ccaPurchasesAndWithdrawalsForm = new CcaPurchasesAndWithdrawalsForm("0.01",
			"454925,454903", "ENABLED", "ENABLED", "6");

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(DeviceUtils.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(PaymentsServiceModule.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(RequestContextUtils.class);
		PowerMockito.mockStatic(InetAddress.class);
		PowerMockito.mockStatic(AlertTypesUtils.class);
		PowerMockito.mockStatic(ApiCallerFactory.class);
		PowerMockito.mockStatic(Utils.class, CALLS_REAL_METHODS);

		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);
		bean = new PurchasesAndWithdrawalsBean();
	}

	/**
	 * when RemoveInactiveCardPlastics
	 * give InactiveCardPlastic
	 * then EmptyArray
	 * s679338
	 */
	@Test
	public void whenRemoveInactiveCardPlastics_giveInactiveCardPlastic_thenEmptyArray() throws Exception{
		CardPlasticAccountRecord record = new CardPlasticAccountRecord();
		record.setStatus("N    INACTIVE");
		CardPlasticFrontend inactiveCard = new CardPlasticFrontend(record);
		
		CardPlasticFrontend[] cardPlastics = {
			inactiveCard
		};
		
		CardPlasticFrontend[] activeCardPlastics = Utils.removeInactiveCardPlastics(userSession, cardPlastics);
		assertEquals(0, activeCardPlastics.length);
	}
	
	/**
	 * when RemoveInactiveCardPlastics
	 * give ActiveCardPlastic
	 * then Not Empty Array
	 * s679338
	 */
	@Test
	public void whenRemoveInactiveCardPlastics_giveActiveCardPlastic_thenNotEmptyArray() throws Exception{
		CardPlasticAccountRecord record = new CardPlasticAccountRecord();
		record.setStatus("Y    ACTIVE");
		CardPlasticFrontend activeCard = new CardPlasticFrontend(record);
		
		CardPlasticFrontend[] cardPlastics = {
			activeCard
		};
		
		CardPlasticFrontend[] activeCardPlastics = Utils.removeInactiveCardPlastics(userSession, cardPlastics);
		
		assertEquals(1, activeCardPlastics.length);
	}
	
	@Test
	public void whenSetCCAPlasticCardStatus_giveAccountWithPlasticCard_thenPlasticCardSetStatus() throws Exception{
		CardPlasticAccountRecord record = new CardPlasticAccountRecord();
		record.setCardPANId("001");
		CardPlasticFrontend cardPlastic = new CardPlasticFrontend(record);
		
		TVFrontendAccount account = mock(TVFrontendAccount.class);
		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>() {{
			add(account);
		}};
		
		when(userSession.getCcaCardPlastics()).thenReturn(accounts);
		when(account.getAccountKey()).thenReturn("001");
		when(account.getStatus()).thenReturn("Y    ACTIVE");
		
		Utils.setCCAPlasticCardStatus(accounts, cardPlastic);
		
		assertEquals("Y    ACTIVE", cardPlastic.getCardPlasticAccountRecord().getStatus());
	}
	
	/**
	 * when SetCCAPlasticCardStatus
	 * give Account Without PlasticCard
	 * then PlasticCard Not Set Status
	 * s679338
	 */
	@Test
	public void whenSetCCAPlasticCardStatus_giveAccountWithoutPlasticCard_thenPlasticCardNotSetStatus() throws Exception{
		CardPlasticAccountRecord record = new CardPlasticAccountRecord();
		record.setCardPANId("001");
		CardPlasticFrontend cardPlastic = new CardPlasticFrontend(record);
		
		TVFrontendAccount account = mock(TVFrontendAccount.class);
		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>() {{
			add(account);
		}};
		
		when(userSession.getCcaCardPlastics()).thenReturn(accounts);
		when(account.getAccountKey()).thenReturn("002");
		when(account.getStatus()).thenReturn("Y    ACTIVE");
		
		Utils.setCCAPlasticCardStatus(accounts, cardPlastic);

		assertNull(cardPlastic.getCardPlasticAccountRecord().getStatus());
	}

	@Test
	public void whenOnSubmit_GivenDeactivateEbill_ThenRegisterEvent() throws Exception {

		callTestEbill();
		HttpServletResponse response = mock(HttpServletResponse.class);
		EbppStatusUpdateBean ebppStatusUpdateBean = new EbppStatusUpdateBean();
		ebppStatusUpdateBean.setAction(EbppStatusUpdateBean.ACTION.UPDATE_EBILL_STATUS);
		ebppStatusUpdateBean.setId("123");
		ebppStatusUpdateBean.setEmail(null);
		BindException errors = new BindException(ebppStatusUpdateBean, "EbppStatusUpdateBean");

		Whitebox.invokeMethod(controller, "onSubmit", request, response, ebppStatusUpdateBean, errors);
		PowerMockito.verifyStatic(Utils.class, times(1));
		Utils.registerStatusEbillHistoryLog(any(), any(), any(), any(), any());
	}

	private void callTestEbill() throws Exception {
		Payee payee = new Payee();
		payee.setBillingAccount("4654654654");
		frontEndPayee.setCustomerPayeePreferences(customerPayeePreferences);
		frontEndPayee.setBackendObject(payee);
		frontEndPayee.setFrontEndId("645646543213465");
		payeeList.add(frontEndPayee);

		account.setAccountID("54554654654");
		TVFrontendAccount frontendAccount = new TVFrontendAccount(account);
		frontendAccount.setBackendObject(account);
		accountList.add(frontendAccount);

		userSession = mock(UserSession.class);
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerService()).thenReturn(customerServices);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(request.getParameter("showInsidePortal")).thenReturn("false");
		when(request.getParameter("origId")).thenReturn("1234");
		when(userSession.isEbpp2UserIdLookedUp()).thenReturn(true);
		when(userSession.getEbpp2UserId()).thenReturn("123456");
		when(customerPayeePreferences.getNickname()).thenReturn("VISA PREMIA");
		when(userSession.getPayees(true)).thenReturn(payeeList);
		when(userSession.getAccounts()).thenReturn(accountList);
		PowerMockito.doReturn(frontEndPayee).when(Utils.class, "getPayeebyId", any(), any());
		PowerMockito.doReturn(true).when(Utils.class, "getPayeeisCCA", any(), any());
		PowerMockito.doReturn(frontendAccount).when(Utils.class, "getAccountByAccountNumber", any(), any());
		PowerMockito.doReturn("en").when(Utils.class, "getRSALanguage", any());

		when(customerPaymentInfoResponse.getPayees()).thenReturn(payeeList);
		when(PaymentsServiceModule.getInstance()).thenReturn(paymentsServiceModule);
		when(paymentsServiceModule.getFastPayees(any(UserSession.class), any(BankingSession.class),
				anyBoolean())).thenReturn(customerPaymentInfoResponse);
		when(userSession.getCcaAvailbleToEbill()).thenReturn(accountList);

		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(true);
		callValidations();
		callValidationsTest();
	}

	private void callValidations() throws Exception {
		when(userSession.getAccounts()).thenReturn(accountList);
		when(customerServices.prophItService()).thenReturn(prophITService);
		when(prophITService.makeCustomerInquiry(anyString())).thenReturn(inquiryBean);
		when(inquiryBean.getEbillInd()).thenReturn(EbillIndType.DEACTIVE.toString());
		when(inquiryBean.getCaseNumber()).thenReturn("5466");
		when(inquiryBean.getChannelId()).thenReturn("665");
		when(inquiryBean.getImpersonateId()).thenReturn("1234");
		when(inquiryBean.getKeepCaseOpen()).thenReturn(true);
		when(inquiryBean.getNewContact()).thenReturn(true);
		when(inquiryBean.getOrgId()).thenReturn("47879");
		when(inquiryBean.getOrgLevel()).thenReturn("7");
	}

	private void callValidationsTest() throws Exception {

		InetAddress inetAddress = mock(InetAddress.class);
		PowerMockito.when(prophITService.maintenceEBill(any(MaintenceEbillBean.class))).thenReturn(maintenceEbillResponse);
		when(maintenceEbillResponse.getMessage()).thenReturn("Successfull");
		PowerMockito.doReturn(Boolean.TRUE).when(customerServices).sendEbillsCCAEmailActivationSuccess(customerProfile,
				PreferredLanguage.SPANISH, "VISA PREMIA");
		customerProfile.setPermId(new BigInteger("4654654132132165"));
		customerProfile.setTaxId("555321456");
		customerProfile.setUsername("gallo");
		when(request.getSession()).thenReturn(null);
		when(request.getRemoteAddr()).thenReturn("10.37.156.56");
		when(InetAddress.getLocalHost()).thenReturn(inetAddress);
		when(inetAddress.getHostName()).thenReturn("localhost");
		PowerMockito.doNothing().when(customerServices).logUpdateStatementStatus(customerInteractionEvent,
				InteractionSeverity.INFO);
		when(request.getHeader("user-agent")).thenReturn("Safari");
		when(DeviceUtils.getBrowserInfo(anyString(), any(UserSession.class))).thenReturn("Browser info....");
		PowerMockito.doNothing().when(customerServices).logUpdateActivatedEbills(customerInteractionEvent,
				InteractionSeverity.INFO);
	}

	@Test
	public void whenAddPhoneNumberContactAndAlertPreference_GivenModelAndUserSession_ThenSaveAuditLog() throws Exception {

		Map<String, Object> model = new HashMap<>();
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveAlertPhoneContactTSYS", any(PurchasesAndWithdrawalsBean.class),
				any(UserSession.class), any(HttpServletRequest.class));
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveAlertContactInformationTSYS", any(PurchasesAndWithdrawalsBean.class),
				any(UserSession.class), any(HttpServletRequest.class));
		bean.setAlertsPurchaseminamt("125.5");
		when(AlertTypesUtils.getSuccessStatusMessage(any(HttpServletRequest.class))).thenReturn("Profile Updated");
		ccaPurchasesAndWithdrawalsForm.setSaveActivateAlertAuditLog(true);
		PowerMockito.doReturn(true).when(ccaPurchasesAndWithdrawalsForm, "saveUpdatePurchaseAmountAlertPreferencesTSYS",
				any(HttpServletRequest.class), any(PurchasesAndWithdrawalsBean.class), any(UserSession.class), anyString());
		bean.setAthplastic1("4456123654879654");
		when(request.getSession(false)).thenReturn(null);
		when(request.getRemoteAddr()).thenReturn("192.168.38.456");
		when(inetAddress.getHostName()).thenReturn("localhost");
		when(request.getHeader("user-agent")).thenReturn("Safari");
		when(DeviceUtils.getBrowserInfo(anyString(), any(UserSession.class))).thenReturn("Browser info....");
		when(userSession.getCustomerService()).thenReturn(customerService);
		PowerMockito.doNothing().when(customerService).logUpdateActivatedEbills(any(CustomerInteractionEvent.class),
				any(InteractionSeverity.class));
		ccaPurchasesAndWithdrawalsForm.setSaveActivateAlertAuditLog(true);

		Whitebox.invokeMethod(ccaPurchasesAndWithdrawalsForm, "addPhoneNumberContactAndAlertPreference", request, bean, model);
		PowerMockito.verifyStatic(Utils.class, times(1));
		Utils.registerTsysAlertHistoryLog(any(), any(), any(), any());
	}
}
