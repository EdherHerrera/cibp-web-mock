package evertec.cibp.web.utils;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CookiePreferenceStatusService;
import evertec.cibp.web.AjaxActionsController;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.services.modules.CookiePreferenceServiceModule;
import evertec.cibp.web.session.UserSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.verification.VerificationMode;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.PowerMockUtils;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ListUtils.class, WebUtils.class, AjaxActionsController.class, ApiCallerFactory.class})
public class ListUtilsIT extends AbstractModelAndViewTests {

    private final List<CookiePreferenceStatus> cookiePreferenceStatusList = new ArrayList<>();

    @Mock
    private CustomerServices customerService;

    @Mock
    private UserSession userSession;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private CookiePreferenceServiceModule cookiePreferenceServiceModule;

    @Mock
    private CookiePreferenceStatusService cookiePreferenceStatusService;

    @Mock
    private ApiCaller apiCaller;

    @Spy
    @InjectMocks
    private final AjaxActionsController ajaxActionsController = new AjaxActionsController();

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ApiCallerFactory.class);
        when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

        CookiePreferenceStatus cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("ENABLED");
        cookiePreferenceStatus.setCategory("ANALYTICS");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);

        cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("ENABLED");
        cookiePreferenceStatus.setCategory("FUNCTIONAL");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);

        cookiePreferenceStatus = new CookiePreferenceStatus();
        cookiePreferenceStatus.setStatus("ENABLED");
        cookiePreferenceStatus.setCategory("ADVERTISING");
        cookiePreferenceStatus.setProfileId("asd4156as54fd6as54f6a5sf4");
        cookiePreferenceStatusList.add(cookiePreferenceStatus);

        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(ListUtils.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        userSession = PowerMockito.mock(UserSession.class);
    }

    /**
     * when UpdateCookiePrivacySettings
     * Given CookieCategoryAdvertising
     * Void method
     * @author ET58344, Evertec Inc.
     * @since 03-05-2021
     */
    @Test
    public void whenUpdateCookiePrivacySettings_GivenCookieCategoryAdvertising() throws Exception {

        ajaxActionsController.setCustomerService(customerService);
        when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
        when(request.getParameter(anyString())).thenReturn("swAdvertising").thenReturn("DISABLED");
        PowerMockito.whenNew(CookiePreferenceServiceModule.class).withNoArguments().thenReturn(cookiePreferenceServiceModule);
        when(userSession.getCookiePreferenceStatusList()).thenReturn(cookiePreferenceStatusList);
        when(customerService.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
        when(cookiePreferenceStatusService.updateCookiePreferenceStatus(any())).thenReturn(Boolean.TRUE);
        when(cookiePreferenceServiceModule.deleteSelectedCookies(any(), any())).thenReturn(Boolean.TRUE);
        when(ListUtils.getCookiePreferenceFromList(any(), any())).thenReturn(new CookiePreferenceStatus());
        PowerMockito.doNothing().when(ajaxActionsController, "registerEvent", any(HttpServletRequest.class),
                any(UserSession.class), anyString(), anyString());

        ajaxActionsController.updateCookiePrivacySettings(request, response);
        PowerMockito.verifyStatic(ListUtils.class);
        ListUtils.getCookiePreferenceFromList(any(), any());
    }
}