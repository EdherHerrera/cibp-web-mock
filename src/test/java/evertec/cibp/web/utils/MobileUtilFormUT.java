package evertec.cibp.web.utils;

import static org.mockito.Mockito.when;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.test.web.AbstractModelAndViewTests;

@RunWith(PowerMockRunner.class)
public class MobileUtilFormUT extends AbstractModelAndViewTests {

	@Mock
	private MessageSource messageSource;

	@InjectMocks
	private MobileUtil controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void whenGetTelepagoErrorDescriptor_GivenMessageError_ThenReturnMessageErrorSucces() throws Exception {
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("OK");
		String valueValidate = Whitebox.invokeMethod(controller, "getTelepagoErrorDescriptorTest", "ebpp.module", "ens", messageSource);
		assertEquals(valueValidate, "OK");
	}

}
