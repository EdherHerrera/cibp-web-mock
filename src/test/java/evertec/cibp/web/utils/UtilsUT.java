package evertec.cibp.web.utils;

import bppr.tv.beans.Owner;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.AccountRecord;
import bppr.tv.beans.accounts.AccountSubtypes;
import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import bppr.tv.beans.accounts.types.DepositAccount;
import bppr.tv.legasuite.beans.accounts.AccountGeneralSection;
import bppr.tv.legasuite.beans.accounts.AccountInformation;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;
import com.evertecinc.services.seagull.ida.accountinquiry.beans.IDAAcctInquiryWrapper;
import evertec.cibp.core.enums.TsysAlertStatus;
import evertec.cibp.core.facades.TsysAlertFacade;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TVFrontendDepositAccount;
import evertec.cibp.core.models.base.AbstractAccount;
import evertec.cibp.core.models.campaign.ActiveCampaignView;
import evertec.cibp.core.models.campaign.CampaignSegmentType;
import evertec.cibp.core.models.customer.CustomerDevices;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerPhone;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerProfileLite;
import evertec.cibp.core.models.customer.devices.CustomerDevice;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.EStatementInteraction;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.models.entitlement.BankingEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.payee.GlobalPayee;
import evertec.cibp.core.models.payee.GlobalPayeePaymentType;
import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.models.postinitialfunding.PIFExternalAccount;
import evertec.cibp.core.models.statements.AccountPDFStmtInfo;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.customer.DefaultCustomerService;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.mapper.CreditCardModelMapperFactory;
import evertec.cibp.web.session.UserSession;
import evertec.creditcards.exception.TsysException;
import evertec.creditcards.model.CustomerIdentifiers;
import evertec.creditcards.model.CustomerList;
import evertec.creditcards.model.Name;
import evertec.dp.service.tvstatement.DepositStatement_Type;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import evertec.creditcards.response.TsysGeneralInformationResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.owasp.csrfguard.CsrfGuard;
import org.owasp.csrfguard.CsrfGuardException;
import org.owasp.csrfguard.util.RandomGenerator;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(Parameterized.class)
@PrepareForTest({ StaticMessageSource.class, ClientUtils.class, CustomerFacade.class, CsrfGuard.class, 
	RSAUtils.class, RandomGenerator.class, WebUtils.class, CreditCardModelMapperFactory.class, 
	InetAddress.class, DeviceUtils.class, TsysAlertFacade.class})
public class UtilsUT {

	private MicroserviceStatusResponse msStatusResponse;
	
	@Mock
	private TsysAlertFacade tsysAlertFacade;

	@Mock
	private CustomerFacade customerFacade;

	@Mock
	private UserSession userSession;

	@Mock
	private TVFrontendAccount tVFrontendAccount;

	@Mock
	private Account account;

	@Mock
	private AccountRecord accountRecord;

	@Mock
	private Owner ow;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private CustomerServices customerService;

	@Mock
	private ActiveCampaignView activeCampaignView1;

	private ActiveCampaignView activeCampaignViewObj1;
	private ActiveCampaignView activeCampaignViewObj2;
	private ActiveCampaignView activeCampaignViewObj3;
	private List<ActiveCampaignView> activeCampaignViewList;

	private String wealthCodes;
	private String preferredCodes;
	private CustomerInteractionEvent customerInteractionEvent;

	@InjectMocks
	Utils controller;

	@Mock
	private DepositStatement_Type depositStatementType;

	@Mock
	private CsrfGuard csrfGuard;

	@Mock
	private HttpServletRequest request;

	@Mock
	private MessageSource messageSource;

	@Mock
	private InetAddress inetAddress;

	@Parameterized.Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { "015", null, "A", "0", }, });
	}

	@Parameterized.Parameter(value = 0)
	public String productId;

	@Parameterized.Parameter(value = 1)
	public String acctNumbersSuffix;

	@Parameterized.Parameter(value = 2)
	public String statusCode;

	@Parameterized.Parameter(value = 3)
	public String postInstruction;
	
	@Mock
	private TsysGeneralInformationResponse tsysGeneralInformationResponse;
	
	@Mock
	private CustomerList customerList;
	
	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
		userSession = mock(UserSession.class);
		tVFrontendAccount = mock(TVFrontendAccount.class);
		account = mock(Account.class);
		tsysAlertFacade = mock(TsysAlertFacade.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(ClientUtils.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(WebUtils.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		PowerMockito.mockStatic(InetAddress.class);
		PowerMockito.mockStatic(DeviceUtils.class);
		PowerMockito.mockStatic(TsysAlertFacade.class);
		msStatusResponse = new MicroserviceStatusResponse();
		PowerMockito.mockStatic(CreditCardModelMapperFactory.class);
		when(userSession.getCustomerService()).thenReturn(customerService);
		this.wealthCodes = "075:575";
		this.preferredCodes = "120:009:507";
	}

	@Test
	public void whenPayeeIsChequilista_ThenReturnCheckWordEnglish(){
		GlobalPayee globalPayee=new GlobalPayee();
		globalPayee.setPaymentType(GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO);
		TVFrontEndPayee tvFrontEndPayee=new TVFrontEndPayee();
		tvFrontEndPayee.setGlobalPayee(globalPayee);
		CustomerServices customerService=userSession.getCustomerService();
		Calendar cal=Calendar.getInstance();
		String recurrent="";
		String language="EN";
        String estimatedDate=Utils.getEstimateDateAccordingToPaymentType(tvFrontEndPayee,customerService,cal,recurrent,language);
	    assertTrue(estimatedDate.equalsIgnoreCase("Check mailed to payee"));
	}

	@Test
	public void whenPayeeIsChequilista_ThenReturnCheckWordSpanish(){
		GlobalPayee globalPayee=new GlobalPayee();
		globalPayee.setPaymentType(GlobalPayeePaymentType.CHEQUE_Y_LISTA_PRIVADO);
		TVFrontEndPayee tvFrontEndPayee=new TVFrontEndPayee();
		tvFrontEndPayee.setGlobalPayee(globalPayee);
		CustomerServices customerService=userSession.getCustomerService();
		Calendar cal=Calendar.getInstance();
		String recurrent="";
		String language="ES";
		String estimatedDate=Utils.getEstimateDateAccordingToPaymentType(tvFrontEndPayee,customerService,cal,recurrent,language);
		assertTrue(estimatedDate.equalsIgnoreCase("Cheque enviado por correo"));
	}


	@Test
	public void whenIsPushEnabled_GivenAllFlagsAreEnabled_ThenReturnTrue() {

		msStatusResponse.setEnabled(true);

		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.PUSH_NOTIFICATIONS)).thenReturn(true);
		when(ClientUtils.getMicroserviceEndpointAndStatus(Utils.PUSH_TOKENS_OP_ENDPOINT)).thenReturn(msStatusResponse);

		assertTrue(Utils.isPushEnabled());
	}

	@Test
	public void whenIsPushEnabled_GivenOnlyPushTokensMSFlagIsDisabled_ThenReturnFalse() {

		msStatusResponse.setEnabled(false);

		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.PUSH_NOTIFICATIONS)).thenReturn(true);
		when(ClientUtils.getMicroserviceEndpointAndStatus(Utils.PUSH_TOKENS_OP_ENDPOINT)).thenReturn(msStatusResponse);

		assertFalse(msStatusResponse.isEnabled());
	}

	@Test
	public void whenIsPushEnabled_GivenOnlyPushNotificationsFlagIsDisabled_ThenReturnFalse() {

		msStatusResponse.setEnabled(true);

		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.PUSH_NOTIFICATIONS)).thenReturn(false);
		when(ClientUtils.getMicroserviceEndpointAndStatus(Utils.PUSH_TOKENS_OP_ENDPOINT)).thenReturn(msStatusResponse);

		assertFalse(Utils.isPushEnabled());
	}

	@Test
	public void whenExcludeAccounts_GivenValidExcludeAccountsRelationshiptypeT005_ThenReturnTrue() {
		when(account.getRecords()).thenReturn(accountRecord);
		Owner[] owns = { ow };
		when(ow.getRelationshipType()).thenReturn("005");
		when(owns[0].getRelationshipType()).thenReturn("005");
		when(account.getRecords().getOwners()).thenReturn(owns);
		when(tVFrontendAccount.getAccountProductId()).thenReturn("015");
		when(tVFrontendAccount.getAccountNumberSuffix()).thenReturn(acctNumbersSuffix);
		when(tVFrontendAccount.getBackendObject()).thenReturn(account);

		List<TVFrontendDepositAccount> frontendDepositAccount = new ArrayList<TVFrontendDepositAccount>();
		TVFrontendDepositAccount depositAccount = mock(TVFrontendDepositAccount.class);
		frontendDepositAccount.add(depositAccount);
		when(depositAccount.getAccountNumber()).thenReturn("1234567890");
		when(depositAccount.getAccountKey()).thenReturn("123456789");

		IDAAcctInquiryWrapper idaInquiryWrapper = mock(IDAAcctInquiryWrapper.class);
		AccountInformation accountInformation = mock(AccountInformation.class);
		AccountGeneralSection accountGeneralSection = mock(AccountGeneralSection.class);

		when(depositAccount.getIdaAcctInq()).thenReturn(idaInquiryWrapper);
		when(idaInquiryWrapper.getAccountInformation()).thenReturn(accountInformation);
		when(accountInformation.getGeneralSection()).thenReturn(accountGeneralSection);
		when(accountGeneralSection.getPostInstruction()).thenReturn(postInstruction);

		when(userSession.getIdaAccounts()).thenReturn(frontendDepositAccount);
		when(tVFrontendAccount.getAccountNumber()).thenReturn("1234567890");
		when(tVFrontendAccount.getAccountKey()).thenReturn("123456789");

		TVAccountProductDefinition accountProductDefinition = mock(TVAccountProductDefinition.class);
		when(accountProductDefinition.getProductStatusCode()).thenReturn(statusCode);
		when(tVFrontendAccount.getProductDefinition()).thenReturn(accountProductDefinition);
		assertTrue(Utils.isAccountNoPost(frontendDepositAccount, tVFrontendAccount));
		assertTrue(Utils.excludeAccounts(tVFrontendAccount, userSession));
	}

	@Test
	public void whenExcludeAccounts_GivenValidExcludeAccountsRelationshiptypeT003_ThenReturnTrue() {
		when(account.getRecords()).thenReturn(accountRecord);
		Owner[] owns = { ow };
		when(ow.getRelationshipType()).thenReturn("003");
		when(owns[0].getRelationshipType()).thenReturn("003");
		when(account.getRecords().getOwners()).thenReturn(owns);
		when(tVFrontendAccount.getAccountProductId()).thenReturn("015");
		when(tVFrontendAccount.getAccountNumberSuffix()).thenReturn(acctNumbersSuffix);
		when(tVFrontendAccount.getBackendObject()).thenReturn(account);

		List<TVFrontendDepositAccount> frontendDepositAccount = new ArrayList<TVFrontendDepositAccount>();
		TVFrontendDepositAccount depositAccount = mock(TVFrontendDepositAccount.class);
		frontendDepositAccount.add(depositAccount);
		when(depositAccount.getAccountNumber()).thenReturn("1234567890");
		when(depositAccount.getAccountKey()).thenReturn("123456789");

		IDAAcctInquiryWrapper idaInquiryWrapper = mock(IDAAcctInquiryWrapper.class);
		AccountInformation accountInformation = mock(AccountInformation.class);
		AccountGeneralSection accountGeneralSection = mock(AccountGeneralSection.class);

		when(depositAccount.getIdaAcctInq()).thenReturn(idaInquiryWrapper);
		when(idaInquiryWrapper.getAccountInformation()).thenReturn(accountInformation);
		when(accountInformation.getGeneralSection()).thenReturn(accountGeneralSection);
		when(accountGeneralSection.getPostInstruction()).thenReturn(postInstruction);

		when(userSession.getIdaAccounts()).thenReturn(frontendDepositAccount);
		when(tVFrontendAccount.getAccountNumber()).thenReturn("1234567890");
		when(tVFrontendAccount.getAccountKey()).thenReturn("123456789");

		TVAccountProductDefinition accountProductDefinition = mock(TVAccountProductDefinition.class);
		when(accountProductDefinition.getProductStatusCode()).thenReturn(statusCode);
		when(tVFrontendAccount.getProductDefinition()).thenReturn(accountProductDefinition);
		assertTrue(Utils.isAccountNoPost(frontendDepositAccount, tVFrontendAccount));
		assertTrue(Utils.excludeAccounts(tVFrontendAccount, userSession));
	}

	@Test
	public void whenExcludeAccounts_GivenRecordsIsNull_ThenFalse() {
		when(tVFrontendAccount.getBackendObject()).thenReturn(account);
		assertNull(account.getRecords());
		assertFalse(Utils.excludeAccounts(tVFrontendAccount, userSession));
	}

	@Test
	public void whenExcludeAccounts_GivenOwnersIsNull_ThenFalse() {
		when(tVFrontendAccount.getBackendObject()).thenReturn(account);
		when(account.getRecords()).thenReturn(accountRecord);
		when(accountRecord.getOwners()).thenReturn(null);
		assertFalse(Utils.excludeAccounts(tVFrontendAccount, userSession));
	}

	@Test
	public void whenExcludeAccounts_GivenRelationshiptypeIsEmpty_ThenReturnFalse() {
		when(tVFrontendAccount.getBackendObject()).thenReturn(account);
		when(account.getRecords()).thenReturn(accountRecord);
		Owner[] owns = { ow };
		when(ow.getRelationshipType()).thenReturn("");
		when(owns[0].getRelationshipType()).thenReturn("");
		when(account.getRecords().getOwners()).thenReturn(owns);
		when(accountRecord.getOwners()).thenReturn(owns);
		when(tVFrontendAccount.getAccountProductId()).thenReturn(productId);
		when(tVFrontendAccount.getAccountNumberSuffix()).thenReturn(acctNumbersSuffix);
		assertFalse(Utils.excludeAccounts(tVFrontendAccount, userSession));
	}

	@Test
	public void whenExcludeAccounts_GivenRelationshiptypeIsNull_ThenReturnFalse() {
		when(tVFrontendAccount.getBackendObject()).thenReturn(account);
		when(account.getRecords()).thenReturn(accountRecord);
		Owner[] owns = { ow };
		when(ow.getRelationshipType()).thenReturn(null);
		when(owns[0].getRelationshipType()).thenReturn(null);
		when(account.getRecords().getOwners()).thenReturn(owns);
		when(accountRecord.getOwners()).thenReturn(owns);
		when(tVFrontendAccount.getAccountProductId()).thenReturn(productId);
		when(tVFrontendAccount.getAccountNumberSuffix()).thenReturn(acctNumbersSuffix);
		assertFalse(Utils.excludeAccounts(tVFrontendAccount, userSession));
	}

	@Test
	public void whenExcludeAccounts_GivenRelationshiptypeIsDifferent_ThenReturnFalse() {
		when(tVFrontendAccount.getBackendObject()).thenReturn(account);
		when(account.getRecords()).thenReturn(accountRecord);
		assertNotNull(account.getRecords());
		Owner[] owns = { ow };
		when(ow.getRelationshipType()).thenReturn("006");
		when(owns[0].getRelationshipType()).thenReturn("006");
		when(account.getRecords().getOwners()).thenReturn(owns);
		when(tVFrontendAccount.getAccountProductId()).thenReturn(productId);
		when(tVFrontendAccount.getAccountNumberSuffix()).thenReturn(acctNumbersSuffix);
		assertFalse(Utils.excludeAccounts(tVFrontendAccount, userSession));
	}

	@Test
	public void whenExcludeAccounts_GivenWithCollateralProduct_ThenReturnFalse() {
		when(tVFrontendAccount.getBackendObject()).thenReturn(account);
		when(account.getRecords()).thenReturn(accountRecord);
		Owner[] owns = { ow };
		when(ow.getRelationshipType()).thenReturn("003");
		when(owns[0].getRelationshipType()).thenReturn("003");
		when(account.getRecords().getOwners()).thenReturn(owns);
		when(tVFrontendAccount.getAccountProductId()).thenReturn("050");
		when(tVFrontendAccount.getAccountNumberSuffix()).thenReturn(acctNumbersSuffix);
		assertFalse(Utils.excludeAccounts(tVFrontendAccount, userSession));
	}

	@Test
	public void whenExcludeAccounts_GivenAccountProductDifferent_ThenReturnFalse() {
		when(tVFrontendAccount.getBackendObject()).thenReturn(account);
		when(account.getRecords()).thenReturn(accountRecord);
		Owner[] owns = { ow };
		when(ow.getRelationshipType()).thenReturn("003");
		when(owns[0].getRelationshipType()).thenReturn("003");
		when(account.getRecords().getOwners()).thenReturn(owns);
		when(tVFrontendAccount.getAccountProductId()).thenReturn(productId);
		when(tVFrontendAccount.getAccountNumberSuffix()).thenReturn(acctNumbersSuffix);
		TVAccountProductDefinition accountProductDefinition = mock(TVAccountProductDefinition.class);
		when(accountProductDefinition.getProductStatusCode()).thenReturn(statusCode);
		when(tVFrontendAccount.getProductDefinition()).thenReturn(accountProductDefinition);
		assertFalse(Utils.excludeAccounts(tVFrontendAccount, userSession));
	}

	@Test
	public void whenExcludeAccounts_GivenAccNumberSuffixNotNull_ThenReturnFalse() {
		when(tVFrontendAccount.getBackendObject()).thenReturn(account);
		when(account.getRecords()).thenReturn(accountRecord);
		Owner[] owns = { ow };
		when(ow.getRelationshipType()).thenReturn("003");
		when(owns[0].getRelationshipType()).thenReturn("003");
		when(account.getRecords().getOwners()).thenReturn(owns);
		when(tVFrontendAccount.getAccountProductId()).thenReturn(productId);
		when(tVFrontendAccount.getAccountNumberSuffix()).thenReturn("RES");
		assertFalse(Utils.excludeAccounts(tVFrontendAccount, userSession));
	}

	@Test
	public void whenExcludeAccounts_GivenAccNumberSuffixIsNull_ThenReturnFalse() {
		when(tVFrontendAccount.getBackendObject()).thenReturn(account);
		when(account.getRecords()).thenReturn(accountRecord);
		Owner[] owns = { ow };
		when(ow.getRelationshipType()).thenReturn("003");
		when(owns[0].getRelationshipType()).thenReturn("003");
		when(account.getRecords().getOwners()).thenReturn(owns);
		when(tVFrontendAccount.getAccountProductId()).thenReturn(productId);
		when(tVFrontendAccount.getAccountNumberSuffix()).thenReturn(acctNumbersSuffix);
		TVAccountProductDefinition accountProductDefinition = mock(TVAccountProductDefinition.class);
		when(accountProductDefinition.getProductStatusCode()).thenReturn(statusCode);
		when(tVFrontendAccount.getProductDefinition()).thenReturn(accountProductDefinition);
		assertFalse(Utils.excludeAccounts(tVFrontendAccount, userSession));
	}

	@Test
	public void whenExcludeAccounts_GivenStatusCodeInvalid_ThenReturnFalse() {
		when(tVFrontendAccount.getBackendObject()).thenReturn(account);
		when(account.getRecords()).thenReturn(accountRecord);
		Owner[] owns = { ow };
		when(ow.getRelationshipType()).thenReturn("005");
		when(owns[0].getRelationshipType()).thenReturn("005");
		when(account.getRecords().getOwners()).thenReturn(owns);
		when(tVFrontendAccount.getAccountProductId()).thenReturn(productId);
		when(tVFrontendAccount.getAccountNumberSuffix()).thenReturn(acctNumbersSuffix);
		TVAccountProductDefinition accountProductDefinition = mock(TVAccountProductDefinition.class);
		when(accountProductDefinition.getProductStatusCode()).thenReturn("");
		when(tVFrontendAccount.getProductDefinition()).thenReturn(accountProductDefinition);
		assertFalse(Utils.excludeAccounts(tVFrontendAccount, userSession));
	}

	@Test
	public void whenIsAccountNoPost_GivenParametersNull_ThenReturnFalse() {
		when(tVFrontendAccount.getAccountNumber()).thenReturn(null);
		assertFalse(Utils.isAccountNoPost(null, tVFrontendAccount));
	}


	@Test
	public void whenContainsPayments_GivenInValidAccount_ThenReturnFalse() {
		when(userSession.getPayeesPaymentHistoryInfo()).thenReturn(new ArrayList<>());
		assertFalse(Utils.containsPayments("00000000", userSession));
	}

	@Test
	public void whenIsAccountNoPost_GivenValidIDAAccountsList_ThenReturnTrue() {

		TVFrontendDepositAccount depositAccount1 = mock(TVFrontendDepositAccount.class);
		TVFrontendDepositAccount depositAccount2 = mock(TVFrontendDepositAccount.class);

		IDAAcctInquiryWrapper idaInquiryWrapper = mock(IDAAcctInquiryWrapper.class);
		AccountInformation accountInformation = mock(AccountInformation.class);
		AccountGeneralSection accountGeneralSection = mock(AccountGeneralSection.class);

		List<TVFrontendDepositAccount> frontendDepositAccount = new ArrayList<TVFrontendDepositAccount>();
		frontendDepositAccount.add(depositAccount1);
		frontendDepositAccount.add(depositAccount2);

		when(depositAccount1.getAccountNumber()).thenReturn("123456");
		when(depositAccount2.getAccountNumber()).thenReturn("1234567890");
		when(depositAccount2.getAccountKey()).thenReturn("123456789");
		when(depositAccount2.getIdaAcctInq()).thenReturn(idaInquiryWrapper);
		when(idaInquiryWrapper.getAccountInformation()).thenReturn(accountInformation);
		when(accountInformation.getGeneralSection()).thenReturn(accountGeneralSection);
		when(accountGeneralSection.getPostInstruction()).thenReturn(postInstruction);

		when(tVFrontendAccount.getAccountNumber()).thenReturn("1234567890");
		when(tVFrontendAccount.getAccountKey()).thenReturn("123456789");

		assertTrue(Utils.isAccountNoPost(frontendDepositAccount, tVFrontendAccount));
	}

	@Test
	public void whenIsAccountNoPost_GivenIDAAccountIsNull_ThenReturnFalse() {

		TVFrontendDepositAccount depositAccount2 = mock(TVFrontendDepositAccount.class);
		List<TVFrontendDepositAccount> frontendDepositAccount = new ArrayList<TVFrontendDepositAccount>();
		frontendDepositAccount.add(depositAccount2);
		when(depositAccount2.getAccountNumber()).thenReturn("12345678900");
		when(depositAccount2.getAccountKey()).thenReturn("1234567890");
		when(tVFrontendAccount.getAccountNumber()).thenReturn("1234567890");
		when(tVFrontendAccount.getAccountKey()).thenReturn("123456789");
		assertFalse(Utils.isAccountNoPost(frontendDepositAccount, tVFrontendAccount));
	}

	@Test
	public void whenIsAccountNoPost_GivenPostInstructionisDifferent_ThenReturnFalse() {

		TVFrontendDepositAccount depositAccount2 = mock(TVFrontendDepositAccount.class);

		IDAAcctInquiryWrapper idaInquiryWrapper = mock(IDAAcctInquiryWrapper.class);
		AccountInformation accountInformation = mock(AccountInformation.class);
		AccountGeneralSection accountGeneralSection = mock(AccountGeneralSection.class);

		List<TVFrontendDepositAccount> frontendDepositAccount = new ArrayList<TVFrontendDepositAccount>();
		frontendDepositAccount.add(depositAccount2);

		when(depositAccount2.getAccountNumber()).thenReturn("1234567890");
		when(depositAccount2.getAccountKey()).thenReturn("123456789");
		when(depositAccount2.getIdaAcctInq()).thenReturn(idaInquiryWrapper);
		when(idaInquiryWrapper.getAccountInformation()).thenReturn(accountInformation);
		when(accountInformation.getGeneralSection()).thenReturn(accountGeneralSection);
		when(accountGeneralSection.getPostInstruction()).thenReturn("1");
		when(tVFrontendAccount.getAccountNumber()).thenReturn("1234567890");
		when(tVFrontendAccount.getAccountKey()).thenReturn("1234567891");

		assertFalse(Utils.isAccountNoPost(frontendDepositAccount, tVFrontendAccount));
	}

	@Test
	public void whenIsAccountNoPost_GivenPostInstructionisIsTrue_ThenReturnFalse() {

		TVFrontendDepositAccount depositAccount2 = mock(TVFrontendDepositAccount.class);

		IDAAcctInquiryWrapper idaInquiryWrapper = mock(IDAAcctInquiryWrapper.class);
		AccountInformation accountInformation = mock(AccountInformation.class);
		AccountGeneralSection accountGeneralSection = mock(AccountGeneralSection.class);

		List<TVFrontendDepositAccount> frontendDepositAccount = new ArrayList<TVFrontendDepositAccount>();
		frontendDepositAccount.add(depositAccount2);

		when(depositAccount2.getAccountNumber()).thenReturn("1234567890");
		when(depositAccount2.getAccountKey()).thenReturn("123456789");
		when(depositAccount2.getIdaAcctInq()).thenReturn(idaInquiryWrapper);
		when(idaInquiryWrapper.getAccountInformation()).thenReturn(accountInformation);
		when(accountInformation.getGeneralSection()).thenReturn(accountGeneralSection);
		when(accountGeneralSection.getPostInstruction()).thenReturn("1");
		when(tVFrontendAccount.getAccountNumber()).thenReturn("1234567890");
		when(tVFrontendAccount.getAccountKey()).thenReturn("123456789");

		assertFalse(Utils.isAccountNoPost(frontendDepositAccount, tVFrontendAccount));
	}

	@Test
	public void whenDetailedAccountFromPIF_GivenAccountIsNull_ThenAccountDetailValueIsEmpty() {
		Map<String, String> accountDetail = new LinkedHashMap<String, String>();
		accountDetail.put("accountName", "");
		accountDetail.put("acctLast4Num", "");
		accountDetail.put("accountBalance", "");
		accountDetail.put("accountNumber", "");
		accountDetail.put("accountSubtype", "");
		accountDetail.put("accountSection", "");
		accountDetail.put("text", "");
		Map<String, String> detailedAcct = Utils.detailedAccountFromPIF(null);
		assertEquals(accountDetail.toString(), detailedAcct.toString());
	}

	@Test
	public void whenDetailedAccountFromPIF_GivenAccountIsNotNull_ThenAccountDetailHasValues() {
		PIFExternalAccount pifAcct = mock(PIFExternalAccount.class);
		Map<String, String> accountDetail = new LinkedHashMap<String, String>();
		AccountMaskUtil mask = new AccountMaskUtil();
		String PIF_PREFIX = "PIF";
		StringBuilder sb = new StringBuilder();
		when(pifAcct.getNickname()).thenReturn("Test");
		when(pifAcct.getAccountNumber()).thenReturn("1234568976");
		Map<String, String> detailedAcct = Utils.detailedAccountFromPIF(pifAcct);


		accountDetail.put("accountName", pifAcct.getNickname());
		accountDetail.put("accountLast4Num", mask.mask(pifAcct.getAccountNumber()));
		accountDetail.put("accountNumber", pifAcct.getAccountNumber());
		accountDetail.put("accountSubtype", PIF_PREFIX);
		accountDetail.put("accountSection", "");
		sb.append(StringUtils.abbreviate(accountDetail.get("accountName"), 15));
		sb.append(" ");
		sb.append(accountDetail.get("accountLast4Num"));
		accountDetail.put("text", sb.toString());

		assertEquals(accountDetail.toString(), detailedAcct.toString());
	}

	/**
	 * when RemoveInactiveCardPlastics
	 * give InactiveCardPlastic
	 * then EmptyArray
	 * s679338
	 */
	@Test
	public void whenRemoveInactiveCardPlastics_giveInactiveCardPlastic_thenEmptyArray(){
		CardPlasticAccountRecord record = new CardPlasticAccountRecord();
		record.setStatus("N    INACTIVE");
		CardPlasticFrontend inactiveCard = new CardPlasticFrontend(record);

		CardPlasticFrontend[] cardPlastics = {
				inactiveCard
		};

		CardPlasticFrontend[] activeCardPlastics = Utils.removeInactiveCardPlastics(userSession, cardPlastics);

		assertEquals(0, activeCardPlastics.length);
	}

	/**
	 * when RemoveInactiveCardPlastics
	 * give ActiveCardPlastic
	 * then Not Empty Array
	 * s679338
	 */
	@Test
	public void whenRemoveInactiveCardPlastics_giveActiveCardPlastic_thenNotEmptyArray(){
		CardPlasticAccountRecord record = new CardPlasticAccountRecord();
		record.setStatus("Y    ACTIVE");
		CardPlasticFrontend activeCard = new CardPlasticFrontend(record);

		CardPlasticFrontend[] cardPlastics = {
				activeCard
		};

		CardPlasticFrontend[] activeCardPlastics = Utils.removeInactiveCardPlastics(userSession, cardPlastics);

		assertEquals(1, activeCardPlastics.length);
	}

	/**
	 * when Set CCAPlasticCardStatus
	 * give Account With PlasticCard
	 * then PlasticCard Set Status
	 * s679338
	 */
	@Test
	public void whenSetCCAPlasticCardStatus_giveAccountWithPlasticCard_thenPlasticCardSetStatus () {
		CardPlasticAccountRecord record = new CardPlasticAccountRecord();
		record.setCardPANId("001");
		CardPlasticFrontend cardPlastic = new CardPlasticFrontend(record);

		TVFrontendAccount account = mock(TVFrontendAccount.class);
		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>() {{
			add(account);
		}};

		when(userSession.getCcaCardPlastics()).thenReturn(accounts);
		when(account.getAccountKey()).thenReturn("001");
		when(account.getStatus()).thenReturn("Y    ACTIVE");

		Utils.setCCAPlasticCardStatus(accounts, cardPlastic);

		assertEquals("Y    ACTIVE", cardPlastic.getCardPlasticAccountRecord().getStatus());
	}

	/**
	 * when SetCCAPlasticCardStatus
	 * give Account Without PlasticCard
	 * then PlasticCard Not Set Status
	 * s679338
	 */
	@Test
	public void whenSetCCAPlasticCardStatus_giveAccountWithoutPlasticCard_thenPlasticCardNotSetStatus () {
		CardPlasticAccountRecord record = new CardPlasticAccountRecord();
		record.setCardPANId("001");
		CardPlasticFrontend cardPlastic = new CardPlasticFrontend(record);

		TVFrontendAccount account = mock(TVFrontendAccount.class);
		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>() {{
			add(account);
		}};

		when(userSession.getCcaCardPlastics()).thenReturn(accounts);
		when(account.getAccountKey()).thenReturn("002");
		when(account.getStatus()).thenReturn("Y    ACTIVE");

		Utils.setCCAPlasticCardStatus(accounts, cardPlastic);

		assertNull(cardPlastic.getCardPlasticAccountRecord().getStatus());
	}

	@Test
	public void whenHasCustomerCode_givenCustomerCodeInList_thenReturnTrue () {
		List<String> customerCodes = new ArrayList() {{
			add("1"); add("2"); add("3");
		}};
		String[] searchCodes = {"1", "4"};
		boolean result = Utils.hasCustomerCode(customerCodes, searchCodes);

		assertTrue(result);
	}

	@Test
	public void whenHasCustomerCode_givenCustomerCodeNotInList_thenReturnTrue () {
		List<String> customerCodes = new ArrayList() {{
			add("1"); add("2"); add("3");
		}};
		String[] searchCodes = {"4", "5"};
		boolean result = Utils.hasCustomerCode(customerCodes, searchCodes);

		assertFalse(result);
	}

	@Test
	public void whenIsAllowedByTime_givenIsCommercialAndEnrollmentAccountEqualToNMonthsOld_thenReturnTrue() {
		final int months = 4;
		Date date = Date.from(LocalDate.now().minusMonths(months).atStartOfDay(ZoneId.systemDefault()).toInstant());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String enrollmentAccountNumber = "1234567";
		TVFrontendAccount equalAccount = new TVFrontendAccount("IDA", enrollmentAccountNumber, "SAV");
		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>();
		accounts.add(equalAccount);

		AccountRecord equalAccountRecord = new AccountRecord(AccountSubtypes.DEPOSIT) {
			private static final long serialVersionUID = 1L;
		};

		equalAccountRecord.setOpenDate(calendar);

		DepositAccount equalDepositAccount = new DepositAccount();
		equalDepositAccount.setRecords(equalAccountRecord);

		AbstractAccount equalAbstractAccount = new AbstractAccount() {
			private static final long serialVersionUID = 1L;

			@Override
			public String getDefaultDescription() {
				return null;
			}

			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public String getBalanceDescription() {
				return null;
			}

			@Override
			public String getAccountKey() {
				return null;
			}
		};

		equalAbstractAccount.setBackendObject(equalDepositAccount);
		BankingSession bankingSession = new BankingSession(BigInteger.valueOf(423432L), "4234324");

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.useCommercialInterface()).thenReturn(true);
		when(customerProfile.getEnrollmentAccountNumber()).thenReturn(enrollmentAccountNumber);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getAccountDetails(customerProfile, bankingSession, equalAccount, true)).thenReturn(equalAbstractAccount);

		assertTrue(Utils.isAccountOldEnough(months, null, userSession));
	}

	@Test
	public void whenIsAllowedByTime_givenIsCommercialAndEnrollmentAccountOlderThanNMonthsOld_thenReturnTrue() {
		final int months = 4;
		Date date = Date.from(LocalDate.now().minusMonths(months + 1).atStartOfDay(ZoneId.systemDefault()).toInstant());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String enrollmentAccountNumber = "1234567";
		TVFrontendAccount olderAccount = new TVFrontendAccount("IDA", enrollmentAccountNumber, "SAV");
		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>();
		accounts.add(olderAccount);

		AccountRecord olderAccountRecord = new AccountRecord(AccountSubtypes.DEPOSIT) {
			private static final long serialVersionUID = 1L;
		};

		olderAccountRecord.setOpenDate(calendar);

		DepositAccount olderDepositAccount = new DepositAccount();
		olderDepositAccount.setRecords(olderAccountRecord);

		AbstractAccount olderAbstractAccount = new AbstractAccount() {
			private static final long serialVersionUID = 1L;

			@Override
			public String getDefaultDescription() {
				return null;
			}

			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public String getBalanceDescription() {
				return null;
			}

			@Override
			public String getAccountKey() {
				return null;
			}
		};

		olderAbstractAccount.setBackendObject(olderDepositAccount);
		BankingSession bankingSession = new BankingSession(BigInteger.valueOf(423432L), "4234324");

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.useCommercialInterface()).thenReturn(true);
		when(customerProfile.getEnrollmentAccountNumber()).thenReturn(enrollmentAccountNumber);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getAccountDetails(customerProfile, bankingSession, olderAccount, true)).thenReturn(olderAbstractAccount);

		assertTrue(Utils.isAccountOldEnough(months, null, userSession));
	}

	@Test
	public void whenIsAllowedByTime_givenIsCommercialAndEnrollmentAccountNewerThanNMonthsOld_thenReturnTrue() {
		final int months = 4;
		Date date = Date.from(LocalDate.now().minusMonths(months - 1).atStartOfDay(ZoneId.systemDefault()).toInstant());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String enrollmentAccountNumber = "1234567";
		TVFrontendAccount newerAccount = new TVFrontendAccount("IDA", enrollmentAccountNumber, "SAV");
		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>();
		accounts.add(newerAccount);

		AccountRecord newerAccountRecord = new AccountRecord(AccountSubtypes.DEPOSIT) {
			private static final long serialVersionUID = 1L;
		};

		newerAccountRecord.setOpenDate(calendar);

		DepositAccount newerDepositAccount = new DepositAccount();
		newerDepositAccount.setRecords(newerAccountRecord);

		AbstractAccount newerAbstractAccount = new AbstractAccount() {
			private static final long serialVersionUID = 1L;

			@Override
			public String getDefaultDescription() {
				return null;
			}

			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public String getBalanceDescription() {
				return null;
			}

			@Override
			public String getAccountKey() {
				return null;
			}
		};

		newerAbstractAccount.setBackendObject(newerDepositAccount);
		BankingSession bankingSession = new BankingSession(BigInteger.valueOf(423432L), "4234324");

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.useCommercialInterface()).thenReturn(true);
		when(customerProfile.getEnrollmentAccountNumber()).thenReturn(enrollmentAccountNumber);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getAccountDetails(customerProfile, bankingSession, newerAccount, true)).thenReturn(newerAbstractAccount);

		assertFalse(Utils.isAccountOldEnough(months, null, userSession));
	}

	@Test
	public void whenIsAllowedByTime_givenIsNotCommercialAndBankingOldestAccountEqualToNMonthsOld_thenReturnTrue() {
		final int months = 4;
		Date date = Date.from(LocalDate.now().minusMonths(months).atStartOfDay(ZoneId.systemDefault()).toInstant());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		BankingEntitlement bankingEntitlement = new BankingEntitlement(new CustomerEntitlement(EntitlementType.BANKING, EntitlementStatus.ENABLED));
		bankingEntitlement.setOldestAccountDate(format.format(date.getTime()));

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getEntitlementByType(EntitlementType.BANKING)).thenReturn(bankingEntitlement.getCustomerEntitlement());

		assertTrue(Utils.isAccountOldEnough(months, null, userSession));
	}

	@Test
	public void whenIsAllowedByTime_givenIsNotCommercialAndBankingOldestAccountOlderThanNMonths_thenReturnTrue() {
		final int months = 4;
		Date accountDate = Date.from(LocalDate.now().minusMonths(months + 1).atStartOfDay(ZoneId.systemDefault()).toInstant());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		BankingEntitlement bankingEntitlement = new BankingEntitlement(new CustomerEntitlement(EntitlementType.BANKING, EntitlementStatus.ENABLED));
		bankingEntitlement.setOldestAccountDate(format.format(accountDate.getTime()));

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getEntitlementByType(EntitlementType.BANKING)).thenReturn(bankingEntitlement.getCustomerEntitlement());

		assertTrue(Utils.isAccountOldEnough(months, null, userSession));
	}

	@Test
	public void whenIsAllowedByTime_givenIsNotCommercialAndBankingOldestAccountNewerThanNMonths_thenReturnFalse() {
		final int months = 4;
		Date accountDate = Date.from(LocalDate.now().minusMonths(months - 1).atStartOfDay(ZoneId.systemDefault()).toInstant());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		BankingEntitlement bankingEntitlement = new BankingEntitlement(new CustomerEntitlement(EntitlementType.BANKING, EntitlementStatus.ENABLED));
		bankingEntitlement.setOldestAccountDate(format.format(accountDate.getTime()));

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getEntitlementByType(EntitlementType.BANKING)).thenReturn(bankingEntitlement.getCustomerEntitlement());

		assertFalse(Utils.isAccountOldEnough(months, null, userSession));
	}

	@Test
	public void whenIsAllowedByTime_givenIsNotCommercialAndNullOldestAccountDateAndAccountEqualToNMonthsOld_thenReturnTrue() {
		final int months = 4;
		Date date = Date.from(LocalDate.now().minusMonths(months).atStartOfDay(ZoneId.systemDefault()).toInstant());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		BankingEntitlement bankingEntitlement = new BankingEntitlement(new CustomerEntitlement(EntitlementType.BANKING, EntitlementStatus.ENABLED));
		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>();
		TVFrontendAccount equalAccount = new TVFrontendAccount("IDA", "123", "123");

		AccountRecord equalAccountRecord = new AccountRecord(AccountSubtypes.DEPOSIT) {
			private static final long serialVersionUID = 1L;
		};

		equalAccountRecord.setOpenDate(calendar);

		DepositAccount equalDepositAccount = new DepositAccount();
		equalDepositAccount.setRecords(equalAccountRecord);

		AbstractAccount equalAbstractAccount = new AbstractAccount() {
			private static final long serialVersionUID = 1L;

			@Override
			public String getDefaultDescription() {
				return null;
			}

			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public String getBalanceDescription() {
				return null;
			}

			@Override
			public String getAccountKey() {
				return null;
			}
		};

		equalAbstractAccount.setBackendObject(equalDepositAccount);
		accounts.add(equalAccount);
		BankingSession bankingSession = new BankingSession(BigInteger.valueOf(423432L), "4234324");

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getEntitlementByType(EntitlementType.BANKING)).thenReturn(bankingEntitlement.getCustomerEntitlement());
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(customerService.getAccountDetails(customerProfile, bankingSession, equalAccount, true)).thenReturn(equalAbstractAccount);

		assertTrue(Utils.isAccountOldEnough(months, accounts, userSession));
	}

	@Test
	public void whenIsAllowedByTime_givenIsNotCommercialAndNullOldestAccountDateAndAccountOlderThanNMonthsOld_thenReturnTrue() {
		final int months = 4;
		Date date = Date.from(LocalDate.now().minusMonths(months + 1).atStartOfDay(ZoneId.systemDefault()).toInstant());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		BankingEntitlement bankingEntitlement = new BankingEntitlement(new CustomerEntitlement(EntitlementType.BANKING, EntitlementStatus.ENABLED));
		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>();
		TVFrontendAccount equalAccount = new TVFrontendAccount("IDA", "123", "123");

		AccountRecord equalAccountRecord = new AccountRecord(AccountSubtypes.DEPOSIT) {
			private static final long serialVersionUID = 1L;};
		equalAccountRecord.setOpenDate(calendar);

		DepositAccount equalDepositAccount = new DepositAccount();
		equalDepositAccount.setRecords(equalAccountRecord);

		AbstractAccount equalAbstractAccount = new AbstractAccount() {
			private static final long serialVersionUID = 1L;

			@Override
			public String getDefaultDescription() {
				return null;
			}

			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public String getBalanceDescription() {
				return null;
			}

			@Override
			public String getAccountKey() {
				return null;
			}
		};

		equalAbstractAccount.setBackendObject(equalDepositAccount);
		accounts.add(equalAccount);
		BankingSession bankingSession = new BankingSession(BigInteger.valueOf(423432L), "4234324");

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getEntitlementByType(EntitlementType.BANKING)).thenReturn(bankingEntitlement.getCustomerEntitlement());
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(customerService.getAccountDetails(customerProfile, bankingSession, equalAccount, true)).thenReturn(equalAbstractAccount);

		assertTrue(Utils.isAccountOldEnough(months, accounts, userSession));
	}

	@Test
	public void whenIsAllowedByTime_givenIsNotCommercialAndNullOldestAccountDateAndAccountNewerThanNMonthsOld_thenReturnFalse() {
		final int months = 4;
		Date date = Date.from(LocalDate.now().minusMonths(months - 1).atStartOfDay(ZoneId.systemDefault()).toInstant());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		BankingEntitlement bankingEntitlement = new BankingEntitlement(new CustomerEntitlement(EntitlementType.BANKING, EntitlementStatus.ENABLED));
		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>();
		TVFrontendAccount equalAccount = new TVFrontendAccount("IDA", "123", "123");

		AccountRecord equalAccountRecord = new AccountRecord(AccountSubtypes.DEPOSIT) {
			private static final long serialVersionUID = 1L;
		};

		equalAccountRecord.setOpenDate(calendar);

		DepositAccount equalDepositAccount = new DepositAccount();
		equalDepositAccount.setRecords(equalAccountRecord);

		AbstractAccount equalAbstractAccount = new AbstractAccount() {
			private static final long serialVersionUID = 1L;

			@Override
			public String getDefaultDescription() {
				return null;
			}

			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public String getBalanceDescription() {
				return null;
			}

			@Override
			public String getAccountKey() {
				return null;
			}
		};

		equalAbstractAccount.setBackendObject(equalDepositAccount);
		accounts.add(equalAccount);
		BankingSession bankingSession = new BankingSession(BigInteger.valueOf(423432L), "4234324");

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getEntitlementByType(EntitlementType.BANKING)).thenReturn(bankingEntitlement.getCustomerEntitlement());
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(customerService.getAccountDetails(customerProfile, bankingSession, equalAccount, true)).thenReturn(equalAbstractAccount);
		when(CustomerFacade.getInstance()).thenReturn(customerFacade);

		assertFalse(Utils.isAccountOldEnough(months, accounts, userSession));
	}

	@Test
	public void whenHasMessageIncludeSegments_GivenCCASegmentActiveCampaignView_ThenReturnTrue(){

		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.AUTO.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.CCA.toString())).thenReturn("CCA");
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.CD.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.COMERCIAL.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.DDA.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.EACCOUNT.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.EBILL.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.ESTATMENT.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.INSURANCE.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.IRA.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.LEASING.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.MOBILE.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.MORTGAGE.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.LOAN.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.PREFERRED.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.PREMIA.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.SECURITIES.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.WEALTH.toString())).thenReturn(null);

		when(Utils.validateSegmentInclude("CCA", userSession)).thenReturn(true);
		//when(Utils.validateCustomSegmentInclude(true, "CCA", userSession, wealthCodes, preferredCodes)).thenReturn(true);

		boolean result = Utils.hasMessageIncludeSegments(activeCampaignView1, userSession, wealthCodes, preferredCodes);
		assertTrue(result);
	}

	@Test
	public void whenHasMessageIncludeSegments_GivenNoSegmentActiveCampaignView_ThenReturnFalse(){
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.AUTO.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.CCA.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.CD.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.COMERCIAL.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.DDA.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.EACCOUNT.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.EBILL.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.ESTATMENT.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.INSURANCE.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.IRA.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.LEASING.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.MOBILE.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.MORTGAGE.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.LOAN.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.PREFERRED.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.PREMIA.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.SECURITIES.toString())).thenReturn(null);
		when(activeCampaignView1.getIncludeSegment(CampaignSegmentType.WEALTH.toString())).thenReturn(null);

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)).thenReturn(false);
		when(userSession.isPreferred(wealthCodes, preferredCodes)).thenReturn(false);
		when(userSession.isWealth(wealthCodes, preferredCodes)).thenReturn(false);

		boolean result = Utils.hasMessageIncludeSegments(activeCampaignView1, userSession, wealthCodes, preferredCodes);
		assertFalse(result);
	}

	@Test
	public void whenUpdateMessagesList_GivenActiveCampaignViewSizeThree_ThenActiveCampaignViewSizeOne(){
		activeCampaignViewObj1 = new ActiveCampaignView();
		activeCampaignViewObj2 = new ActiveCampaignView();
		activeCampaignViewObj3 = new ActiveCampaignView();
		activeCampaignViewList = new ArrayList<>();

		activeCampaignViewObj1.setIncludeSegments("<properties><property name=\"CCA\" value=\"CCA\"/></properties>");
		activeCampaignViewObj2.setIncludeSegments("<properties><property name=\"DDA\" value=\"DDA\"/></properties>");
		activeCampaignViewObj3.setIncludeSegments("<properties><property name=\"SECURITIES\" value=\"SECURITIES\"/></properties>");
		activeCampaignViewList.add(activeCampaignViewObj1);
		activeCampaignViewList.add(activeCampaignViewObj2);
		activeCampaignViewList.add(activeCampaignViewObj3);

		validateUserSession();
		when(userSession.hasCCA()).thenReturn(true);
		Utils.updateMessagesList(activeCampaignViewList, userSession, wealthCodes, preferredCodes);
		assertEquals(activeCampaignViewList.get(0).getIncludeSegment("CCA"), "CCA");
	}

	@Test
	public void whenUpdateMessagesList_GivenActiveCampaignViewSizeThree_ThenActiveCampaignViewSizeZero(){
		activeCampaignViewObj1 = new ActiveCampaignView();
		activeCampaignViewObj2 = new ActiveCampaignView();
		activeCampaignViewObj3 = new ActiveCampaignView();
		ActiveCampaignView activeCampaignView4 = new ActiveCampaignView();
		activeCampaignViewList = new ArrayList<>();

		activeCampaignViewObj1.setIncludeSegments("<properties><property name=\"CCA\" value=\"CCA\"/></properties>");
		activeCampaignViewObj2.setIncludeSegments("<properties><property name=\"DDA\" value=\"DDA\"/></properties>");
		activeCampaignViewObj3.setIncludeSegments("<properties><property name=\"SECURITIES\" value=\"SECURITIES\"/></properties>");
		activeCampaignView4.setIncludeSegments("<properties/>");
		activeCampaignViewList.add(activeCampaignViewObj1);
		activeCampaignViewList.add(activeCampaignViewObj2);
		activeCampaignViewList.add(activeCampaignViewObj3);
		activeCampaignViewList.add(activeCampaignView4);

		activeCampaignView4 = new ActiveCampaignView();
		activeCampaignView4.setIncludeSegments("<properties><property name=\"ALL\" value=\"ALL\"/></properties>");
		activeCampaignViewList.add(activeCampaignView4);

		validateUserSession();
		Utils.updateMessagesList(activeCampaignViewList, userSession, wealthCodes, preferredCodes);
		assertEquals(activeCampaignViewList.size(), 2);
	}

	private void validateUserSession(){
		when(userSession.isHasAuto()).thenReturn(false);
		when(userSession.hasCCA()).thenReturn(false);
		when(userSession.isHasCD()).thenReturn(false);
		when(userSession.isComercialCustomer()).thenReturn(false);
		when(userSession.isHasDDA()).thenReturn(false);
		when(userSession.isHasEaccount()).thenReturn(false);
		when(userSession.isHasEbill()).thenReturn(false);
		when(userSession.isHasEstatment()).thenReturn(false);
		when(userSession.isHasInsurance()).thenReturn(false);
		when(userSession.isHasIRA()).thenReturn(false);
		when(userSession.isHasLeasing()).thenReturn(false);
		when(userSession.isHasMortgage()).thenReturn(false);
		when(userSession.isHasPersonalLoans()).thenReturn(false);
		when(userSession.isPreferred(wealthCodes, preferredCodes)).thenReturn(false);
		when(userSession.isHasPremia()).thenReturn(false);
		when(userSession.isHasSecurities()).thenReturn(false);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getCustomerProfile().hasEntitlement(EntitlementType.MOBILE_BANKING)).thenReturn(false);
		when(userSession.isWealth(wealthCodes, preferredCodes)).thenReturn(false);
	}
	public void whenGetLiteFromCustomerProfile_GivenCustomerProfile_ThenReturnCustomerProfile() {

		when(customerProfile.getProfileId()).thenReturn("54asd6f54as65f4d556a4s5");
		when(customerProfile.getFirstName()).thenReturn("Anuel");
		when(customerProfile.getLastName()).thenReturn("AA");
		when(customerProfile.getEmail()).thenReturn("anuelbrrr@losintocables.com");
		when(customerProfile.getActivationDate()).thenReturn(new Date());
		when(customerProfile.getCustomerPhone()).thenReturn(new CustomerPhone());
		when(customerProfile.getCountry()).thenReturn("Puerto Rico");
		when(customerProfile.getCustomerDevice()).thenReturn(new CustomerDevice());

		CustomerProfileLite result = Utils.getLiteFromCustomerProfile(customerProfile);
		assertNotNull(result);
	}


	@Test
	public void whenGenerateTokenCsrfGuard_GivenHttpServletRequest_ThenReturnToken() {
		try {
			HttpServletRequest request = new MockHttpServletRequest();
			PowerMockito.mockStatic(CsrfGuard.class);
			PowerMockito.mockStatic(RSAUtils.class);
			PowerMockito.mockStatic(RandomGenerator.class);

			when(CsrfGuard.getInstance()).thenReturn(csrfGuard);
			when(csrfGuard.getTokenName()).thenReturn("OWASP-CSRFTOKEN");
			//when(RandomGenerator.generateRandomId(any(), any())).thenReturn("SYVA-5QK5-RMQF-8IEV-P9ZC-DJTZ-FOKQ-1H7I");

			Utils.generateTokenCsrfGuard(request);

		} catch (CsrfGuardException e) {
			e.printStackTrace();
			fail();
		}
	}


	@Test
	public void whenValidateTokenCsrfGuard_GivenHttpServletRequest_ThenReturnToken() {
		try {


			PowerMockito.mockStatic(WebUtils.class);
			when(WebUtils.getSessionAttribute(any(), any())).thenReturn("SYVA-5QK5-RMQF-8IEV-P9ZC-DJTZ-FOKQ-1H7I");
			when(request.getParameter(any())).thenReturn("SYVA-5QK5-RMQF-8IEV-P9ZC-DJTZ-FOKQ-1H7I");
			assertTrue(Utils.validateTokenCsrfGuard(request));

		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void whenValidateTokenCsrfGuard_GivenHttpServletRequest_ThenReturnTokenFalse() {
		try {


			PowerMockito.mockStatic(WebUtils.class);
			when(WebUtils.getSessionAttribute(any(), any())).thenReturn("SYVA-5QK5-RMQF-8IEV-P9ZC-DJTZ-FOKQ-1H7I");
			when(request.getParameter(any())).thenReturn("SYVA-5QK5-RMQF-8IEV-P9ZC-DJTZ-FOKQ-1H7rTTT");
			assertFalse(Utils.validateTokenCsrfGuard(request));

		} catch (Exception e) {
			e.getMessage();
		}
	}

	@Test
	public void whenTsysGeneralInformationByCardNumber_GivenCardNumber_ThenReturnCustomerIdentifiers() throws TsysException {
		
		CustomerIdentifiers custIdentifiers = new CustomerIdentifiers();
		ArrayList<CustomerList> arrListCustomerList = new ArrayList<CustomerList>();
		CustomerList customer = new CustomerList(); //customer
		customer.setCustomerId("000037310");
		customer.setAccount("00000026287");
		customer.setCustomerType("Primary");
		customer.setCardLast4("7882");
		arrListCustomerList.add(customer);
		
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		when(tsysAlertFacade.tsysGeneralInformationByCardNumber(anyString(), anyBoolean())).thenReturn(tsysGeneralInformationResponse);
		when(tsysGeneralInformationResponse.getCustomerList()).thenReturn(arrListCustomerList);
		when(customerList.getAccount()).thenReturn("123456");
		when(customerList.getCustomerId()).thenReturn("123456789");
		custIdentifiers  = controller.getTsysPrimaryCustomerIdentifiers("4455123456457898");

		assertFalse(custIdentifiers.getCustomerId().isEmpty());

	}
	
	@Test
	public void whenGetCustomerIdentifiersByCardLast4_GivenCardNumberAndNameOnCard_ThenReturnCustomerIdentifiers() throws TsysException {
		CustomerIdentifiers custIdentifiers;
		ArrayList<CustomerList> arrListCustomerList = new ArrayList<CustomerList>();
		CustomerList customer = new CustomerList(); //customer
		customer.setCustomerId("000037310");
		customer.setAccount("00000026287");
		customer.setCustomerType("Primary");
		customer.setCardLast4("7898");
		
		Name name = new Name();
		name.setFirst("TEST");
		name.setLast("TLP THIRTEEN");
		name.setNameOnCard("TEST TLP THIRTEEN");
		customer.setName(name);
		arrListCustomerList.add(customer);
		TsysGeneralInformationResponse response = new TsysGeneralInformationResponse(); // unique response
		
		arrListCustomerList.add(customer);
		response.setCustomerList(arrListCustomerList);
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		when(tsysAlertFacade.tsysGeneralInformationByCardNumber(anyString(), anyBoolean()))
				.thenReturn(tsysGeneralInformationResponse);
		when(tsysGeneralInformationResponse.getCustomerList()).thenReturn(arrListCustomerList);

		custIdentifiers  = controller.getCustomerIdentifiersByCardLast4("4455123456457898");

		assertFalse(custIdentifiers.getCustomerId().isEmpty());

	}
	
	@Test
	public void whenGetEmailFrequency_GivenFrequency_ThenReturnFrequency() {
		String frequency = "";
		Integer code = 201;
		request = new MockHttpServletRequest();
		when(messageSource.getMessage("instances.for.label", null, new Locale("en"))).thenReturn("OK");
		frequency = Utils.getEmailFrequency(code, frequency, messageSource, request);
		assertEquals(frequency, "OK ".concat(code.toString()));

		frequency = "WW";
		when(messageSource.getMessage("freq.instances.WW", null, new Locale("en"))).thenReturn(frequency);
		frequency = Utils.getEmailFrequency(code, "WW", messageSource, request);
		assertEquals(frequency, "OK ".concat(code.toString()).concat(" ").concat("WW"));

		frequency = "MM";
		when(messageSource.getMessage("freq.instances.MM", null, new Locale("en"))).thenReturn(frequency);
		frequency = Utils.getEmailFrequency(code, frequency, messageSource, request);
		assertEquals(frequency, "OK ".concat(code.toString()).concat(" ").concat("MM"));

		frequency = "BW";
		when(messageSource.getMessage("freq.instances.BW", null, new Locale("en"))).thenReturn(frequency);
		frequency = Utils.getEmailFrequency(code, frequency, messageSource, request);
		assertEquals(frequency,"OK ".concat(code.toString()).concat(" ").concat("BW"));

		frequency = "SA";
		when(messageSource.getMessage("freq.instances.S", null, new Locale("en"))).thenReturn(frequency);
		frequency = Utils.getEmailFrequency(code, frequency, messageSource, request);
		assertEquals(frequency, "OK ".concat(code.toString()).concat(" ").concat("SA"));

		frequency = "QQ";
		when(messageSource.getMessage("freq.instances.T", null, new Locale("en"))).thenReturn(frequency);
		frequency = Utils.getEmailFrequency(code, frequency, messageSource, request);
		assertEquals(frequency, "OK ".concat(code.toString()).concat(" ").concat("QQ"));

		frequency = "AA";
		when(messageSource.getMessage("freq.instances.A", null, new Locale("en"))).thenReturn(frequency);
		frequency = Utils.getEmailFrequency(code, frequency, messageSource, request);
		assertEquals(frequency, "OK ".concat(code.toString()).concat(" ").concat("AA"));

		Integer remainingInstances = 999;
		frequency = "WW";
		when(messageSource.getMessage("freq.WW", null, new Locale("en"))).thenReturn(frequency);
		frequency = Utils.getEmailFrequency(remainingInstances, "WW", messageSource, request);
		assertEquals(frequency.trim(), "WW");

		frequency = "MM";
		when(messageSource.getMessage("freq.MM", null, new Locale("en"))).thenReturn(frequency);
		frequency = Utils.getEmailFrequency(remainingInstances, frequency, messageSource, request);
		assertEquals(frequency.trim(), "MM");

		frequency = "BW";
		when(messageSource.getMessage("freq.BW", null, new Locale("en"))).thenReturn(frequency);
		frequency = Utils.getEmailFrequency(remainingInstances, frequency, messageSource, request);
		assertEquals(frequency.trim(),"BW");

		frequency = "SA";
		when(messageSource.getMessage("freq.SA", null, new Locale("en"))).thenReturn(frequency);
		frequency = Utils.getEmailFrequency(remainingInstances, frequency, messageSource, request);
		assertEquals(frequency.trim(), "SA");

		frequency = "QQ";
		when(messageSource.getMessage("freq.QQ", null, new Locale("en"))).thenReturn(frequency);
		frequency = Utils.getEmailFrequency(remainingInstances, frequency, messageSource, request);
		assertEquals(frequency.trim(), "QQ");

		frequency = "AA";
		when(messageSource.getMessage("freq.AA", null, new Locale("en"))).thenReturn(frequency);
		frequency = Utils.getEmailFrequency(remainingInstances, frequency, messageSource, request);
		assertEquals(frequency.trim(), "AA");
	}
	@Test
	public void whenGivenMunicipalities_toAccountPDFStmtInfo_ThenReturnTrue(){

		String enrollmentAccountNumber = "1234567";
		TVFrontendAccount equalAccount = new TVFrontendAccount("IDA", enrollmentAccountNumber, "SAV");
		List<TVFrontendAccount> accounts = new ArrayList<TVFrontendAccount>();
		accounts.add(equalAccount);

		AccountRecord equalAccountRecord = new AccountRecord(AccountSubtypes.DEPOSIT) {
			private static final long serialVersionUID = 1L;
		};

		DepositAccount equalDepositAccount = new DepositAccount();
		equalDepositAccount.setRecords(equalAccountRecord);
		Owner owner = new Owner();
		owner.setRelationshipType("052");
		owner.setOwnershipType("Primary");
		owner.setPermId("423432");
		Owner owner1 = new Owner();
		owner1.setRelationshipType("052");
		owner1.setOwnershipType("Primary");
		owner1.setPermId("28071988");
		Owner[] owners = {owner,owner1};

		equalDepositAccount.setOwners(owners);

		AbstractAccount abstractAccount = new AbstractAccount() {
			private static final long serialVersionUID = 1L;
			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public String getBalanceDescription() {
				return null;
			}

			@Override
			public String getAccountKey() {
				return null;
			}

			@Override
			public String getDefaultDescription() {
				return null;
			}
		};
		abstractAccount.setBackendObject(equalDepositAccount);

		BankingSession bankingSession = new BankingSession(BigInteger.valueOf(423432L), "4234324");

		when(depositStatementType.getAccountId()).thenReturn(equalAccount.getAccountNumber());
		when(depositStatementType.getProductDesc()).thenReturn("Municipalities");
		Date date = new Date();
		date.setTime(1230L);
		when(depositStatementType.getStatementStarDt()).thenReturn(date);

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getCustomerService()).thenReturn(customerService);

		when(customerService.getAccountDetails(any(CustomerProfile.class), any(BankingSession.class), any(TVFrontendAccount.class),any(Boolean.class))).thenReturn(abstractAccount);

		List<DepositStatement_Type> depositStatementTypes = new ArrayList<>();
		depositStatementTypes.add(depositStatementType);
		List<AccountPDFStmtInfo> accountPDFStmtInfos  = Utils.toAccountPDFStmtInfo(depositStatementTypes,userSession);
		assertEquals("1234567", accountPDFStmtInfos.get(0).getAccountNumber());

	}

	@Test
	public void whenGivenAuthorities_toAccountPDFStmtInfo_ThenReturnTrue(){

		String enrollmentAccountNumber = "1234567";
		TVFrontendAccount equalAccount = new TVFrontendAccount("IDA", enrollmentAccountNumber, "SAV");

		AccountRecord equalAccountRecord = new AccountRecord(AccountSubtypes.DEPOSIT) {
			private static final long serialVersionUID = 1L;
		};

		DepositAccount equalDepositAccount = new DepositAccount();
		equalDepositAccount.setRecords(equalAccountRecord);
		Owner owner = new Owner();
		owner.setRelationshipType("052");
		owner.setOwnershipType("Primary");
		owner.setPermId("423432");
		Owner owner1 = new Owner();
		owner1.setRelationshipType("052");
		owner1.setOwnershipType("Primary");
		owner1.setPermId("28071988");
		Owner[] owners = {owner,owner1};

		equalDepositAccount.setOwners(owners);

		AbstractAccount abstractAccount = new AbstractAccount() {
			private static final long serialVersionUID = 1L;
			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public String getBalanceDescription() {
				return null;
			}

			@Override
			public String getAccountKey() {
				return null;
			}

			@Override
			public String getDefaultDescription() {
				return null;
			}
		};
		abstractAccount.setBackendObject(equalDepositAccount);

		BankingSession bankingSession = new BankingSession(BigInteger.valueOf(423432L), "4234324");

		when(depositStatementType.getAccountId()).thenReturn(equalAccount.getAccountNumber());
		when(depositStatementType.getProductDesc()).thenReturn("Authorities");
		Date date = new Date();
		date.setTime(1230L);
		when(depositStatementType.getStatementStarDt()).thenReturn(date);

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getCustomerService()).thenReturn(customerService);

		when(customerService.getAccountDetails(any(CustomerProfile.class), any(BankingSession.class), any(TVFrontendAccount.class),any(Boolean.class))).thenReturn(abstractAccount);

		List<DepositStatement_Type> depositStatementTypes = new ArrayList<>();
		depositStatementTypes.add(depositStatementType);
		List<AccountPDFStmtInfo> accountPDFStmtInfos  = Utils.toAccountPDFStmtInfo(depositStatementTypes,userSession);
		assertEquals("1234567", accountPDFStmtInfos.get(0).getAccountNumber());

	}

	@Test
	public void whenGivenAuthorities_toAccountPDFStmtInfo_ThenReturnFalse(){
		String enrollmentAccountNumber = "1234567";
		TVFrontendAccount equalAccount = new TVFrontendAccount("IDA", enrollmentAccountNumber, "SAV");

		AccountRecord equalAccountRecord = new AccountRecord(AccountSubtypes.DEPOSIT) {
			private static final long serialVersionUID = 1L;
		};

		DepositAccount equalDepositAccount = new DepositAccount();
		equalDepositAccount.setRecords(equalAccountRecord);
		Owner owner = new Owner();
		owner.setRelationshipType("067");
		owner.setOwnershipType("Secondary");
		owner.setPermId("423432");
		Owner owner1 = new Owner();
		owner1.setRelationshipType("052");
		owner1.setOwnershipType("Primary");
		owner1.setPermId("28071988");
		Owner[] owners = {owner,owner1};

		equalDepositAccount.setOwners(owners);

		AbstractAccount abstractAccount = new AbstractAccount() {
			private static final long serialVersionUID = 1L;
			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public String getBalanceDescription() {
				return null;
			}

			@Override
			public String getAccountKey() {
				return null;
			}

			@Override
			public String getDefaultDescription() {
				return null;
			}
		};
		abstractAccount.setBackendObject(equalDepositAccount);

		BankingSession bankingSession = new BankingSession(BigInteger.valueOf(423432L), "4234324");

		when(depositStatementType.getAccountId()).thenReturn(equalAccount.getAccountNumber());
		when(depositStatementType.getProductDesc()).thenReturn("Authorities");
		Date date = new Date();
		date.setTime(1230L);
		when(depositStatementType.getStatementStarDt()).thenReturn(date);

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getCustomerService()).thenReturn(customerService);

		when(customerService.getAccountDetails(any(CustomerProfile.class), any(BankingSession.class), any(TVFrontendAccount.class),any(Boolean.class))).thenReturn(abstractAccount);

		List<DepositStatement_Type> depositStatementTypes = new ArrayList<>();
		depositStatementTypes.add(depositStatementType);
		List<AccountPDFStmtInfo> accountPDFStmtInfos  = Utils.toAccountPDFStmtInfo(depositStatementTypes,userSession);
		assertTrue(accountPDFStmtInfos.isEmpty());
	}


	@Test
	public void whenGivenMunicipalities_toAccountPDFStmtInfo_ThenReturnFalse(){
		String enrollmentAccountNumber = "1234567";
		TVFrontendAccount equalAccount = new TVFrontendAccount("IDA", enrollmentAccountNumber, "SAV");

		AccountRecord equalAccountRecord = new AccountRecord(AccountSubtypes.DEPOSIT) {
			private static final long serialVersionUID = 1L;
		};

		DepositAccount equalDepositAccount = new DepositAccount();
		equalDepositAccount.setRecords(equalAccountRecord);
		Owner owner = new Owner();
		owner.setRelationshipType("067");
		owner.setOwnershipType("Secondary");
		owner.setPermId("423432");
		Owner owner1 = new Owner();
		owner1.setRelationshipType("052");
		owner1.setOwnershipType("Primary");
		owner1.setPermId("28071988");
		Owner[] owners = {owner,owner1};

		equalDepositAccount.setOwners(owners);

		AbstractAccount abstractAccount = new AbstractAccount() {
			private static final long serialVersionUID = 1L;
			@Override
			public String getDescription() {
				return null;
			}

			@Override
			public String getBalanceDescription() {
				return null;
			}

			@Override
			public String getAccountKey() {
				return null;
			}

			@Override
			public String getDefaultDescription() {
				return null;
			}
		};
		abstractAccount.setBackendObject(equalDepositAccount);

		BankingSession bankingSession = new BankingSession(BigInteger.valueOf(423432L), "4234324");

		when(depositStatementType.getAccountId()).thenReturn(equalAccount.getAccountNumber());
		when(depositStatementType.getProductDesc()).thenReturn("Municipalities");
		Date date = new Date();
		date.setTime(1230L);
		when(depositStatementType.getStatementStarDt()).thenReturn(date);

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getCustomerService()).thenReturn(customerService);

		when(customerService.getAccountDetails(any(CustomerProfile.class), any(BankingSession.class), any(TVFrontendAccount.class),any(Boolean.class))).thenReturn(abstractAccount);

		List<DepositStatement_Type> depositStatementTypes = new ArrayList<>();
		depositStatementTypes.add(depositStatementType);
		List<AccountPDFStmtInfo> accountPDFStmtInfos  = Utils.toAccountPDFStmtInfo(depositStatementTypes,userSession);
		assertTrue(accountPDFStmtInfos.isEmpty());
	}

	@Test
	public void whenRegisterStatusEbillHistoryLog_GivenStatusActivate_ThenRegisterEvent(){

		when(request.getSession(false)).thenReturn(null);
		when(request.getRemoteAddr()).thenReturn("192.168.38.456");
		when(inetAddress.getHostName()).thenReturn("localhost");
		when(request.getHeader("user-agent")).thenReturn("Safari");
		when(DeviceUtils.getBrowserInfo(anyString(), any(UserSession.class))).thenReturn("Browser info....");
		PowerMockito.doNothing().when(customerService).logUpdateActivatedEbills(customerInteractionEvent,
				InteractionSeverity.INFO);
		Utils.registerStatusEbillHistoryLog(request, "4456123654879654", userSession,
				customerService, EStatementInteraction.ACTIVATE_STATUS);
		verify(customerService).logUpdateActivatedEbills(any(), any());
	}

	@Test
	public void whenRegisterStatusEbillHistoryLog_GivenStatusDeactivate_ThenRegisterEvent(){

		when(request.getSession(false)).thenReturn(null);
		when(request.getRemoteAddr()).thenReturn("192.168.38.456");
		when(inetAddress.getHostName()).thenReturn("localhost");
		when(request.getHeader("user-agent")).thenReturn("Safari");
		when(DeviceUtils.getBrowserInfo(anyString(), any(UserSession.class))).thenReturn("Browser info....");
		PowerMockito.doNothing().when(customerService).logUpdateActivatedEbills(customerInteractionEvent,
				InteractionSeverity.INFO);
		Utils.registerStatusEbillHistoryLog(request, "4456123654879654", userSession,
				customerService, EStatementInteraction.DEACTIVATE_STATUS);
		verify(customerService).logUpdateDesactivatedEbills(any(), any());
	}
	
	@Test
	public void whenGenerateOOBInterruptionEventTraceInformation_GivenstrUserAgentNull_ThenTraceEmpty() {
		String trace = Utils.generateOOBInterruptionEventTraceInformation(null, userSession);
		assertTrue(trace.isEmpty());
	}
	
	@Test
	public void whenGenerateOOBInterruptionEventTraceInformation_GivenstrUserAgentEmpty_ThenTraceEmpty() {
		String trace = Utils.generateOOBInterruptionEventTraceInformation(null, userSession);
		assertTrue(trace.isEmpty());
	}	
	
	@Test
	public void whenGenerateOOBInterruptionEventTraceInformation_GivenstrUserAgentWEB_ThenTraceInformation() {
		when(request.getHeader("user-agent")).thenReturn("Safari");
		
		when(DeviceUtils.getCustomerDevice(any(String.class))).thenReturn(CustomerDevices.PERSONAL);
		when(DeviceUtils.getBrowserInfo(anyString(), any(UserSession.class))).thenReturn("Browser info....");
		
		String trace = Utils.generateOOBInterruptionEventTraceInformation("WEB", userSession);
		assertTrue(!trace.isEmpty());
	}		
	
	@Test
	public void whenGenerateOOBInterruptionEventTraceInformation_GivenstrUserAgentAndroid_ThenTraceInformation() {
		when(request.getHeader("user-agent")).thenReturn("Android;test;test");
		
		when(DeviceUtils.getCustomerDevice(any(String.class))).thenReturn(CustomerDevices.ANDROID);
		when(DeviceUtils.getBrowserInfo(anyString(), any(UserSession.class))).thenReturn("Browser info....");
		
		
		String trace = Utils.generateOOBInterruptionEventTraceInformation("Android;test;test", userSession);
		assertTrue(!trace.isEmpty());
	}		
	
	@Test
	public void whenGenerateOOBInterruptionEventTraceInformation_GivenstrUserAgentiPhone_ThenTraceInformation() {
		when(request.getHeader("user-agent")).thenReturn("iPhone;test;test");
		
		when(DeviceUtils.getCustomerDevice(any(String.class))).thenReturn(CustomerDevices.IPHONE);
		when(DeviceUtils.getBrowserInfo(anyString(), any(UserSession.class))).thenReturn("Browser info....");
		
		
		String trace = Utils.generateOOBInterruptionEventTraceInformation("iPhone;test;test", userSession);
		assertTrue(!trace.isEmpty());
	}	

	@Test
	public void whenRegisterTsysAlertHistoryLog_GivenStatusActivate_ThenRegisterEvent(){

		when(request.getSession(false)).thenReturn(null);
		when(request.getRemoteAddr()).thenReturn("192.168.38.456");
		when(inetAddress.getHostName()).thenReturn("localhost");
		when(request.getHeader("user-agent")).thenReturn("Safari");
		when(DeviceUtils.getBrowserInfo(anyString(), any(UserSession.class))).thenReturn("Browser info....");
		PowerMockito.doNothing().when(customerService).logUpdateActivatedEbills(customerInteractionEvent,
				InteractionSeverity.WARNING);
		Utils.registerTsysAlertHistoryLog(request, "4456123654879654", userSession,
				TsysAlertStatus.ACTIVATE);
		verify(customerService).logUpdateActivatedTsysAlerts(any(), any());
	}

	@Test
	public void whenRegisterTsysAlertHistoryLog_GivenStatusDeactivate_ThenRegisterEvent(){

		when(request.getSession(false)).thenReturn(null);
		when(request.getRemoteAddr()).thenReturn("192.168.38.456");
		when(inetAddress.getHostName()).thenReturn("localhost");
		when(request.getHeader("user-agent")).thenReturn("Safari");
		when(DeviceUtils.getBrowserInfo(anyString(), any(UserSession.class))).thenReturn("Browser info....");
		PowerMockito.doNothing().when(customerService).logUpdateActivatedEbills(customerInteractionEvent,
				InteractionSeverity.WARNING);
		Utils.registerTsysAlertHistoryLog(request, "4456123654879654", userSession,
				TsysAlertStatus.DEACTIVATE);
		verify(customerService).logUpdateDeactivatedTsysAlerts(any(), any());
	}

	@Test
	public void whenRegisterTsysChangedAlertAmountHistoryLog_GivenStatusActivate_ThenRegisterEvent(){

		when(request.getSession(false)).thenReturn(null);
		when(request.getRemoteAddr()).thenReturn("192.168.38.456");
		when(inetAddress.getHostName()).thenReturn("localhost");
		when(request.getHeader("user-agent")).thenReturn("Safari");
		when(DeviceUtils.getBrowserInfo(anyString(), any(UserSession.class))).thenReturn("Browser info....");
		PowerMockito.doNothing().when(customerService).logChangedAlertAmountTsys(customerInteractionEvent,
				InteractionSeverity.WARNING);
		Utils.registerTsysChangedAlertAmountHistoryLog(request, "4456123654879654", userSession,
				"80.55", "125.0");
		verify(customerService).logChangedAlertAmountTsys(any(), any());
	}

	@Test
	public void whenGetGeneralInformationCustomerIdentifiers_GivenSessionData_ThenReturnNotNull() {
		String testCard = "4455123456457890";
		CustomerIdentifiers customerIdentifiers = new CustomerIdentifiers();
		HashMap<String, CustomerIdentifiers> mapCustomerIdentifier = new HashMap<>();
		mapCustomerIdentifier.put(testCard, customerIdentifiers);
		when(userSession.getCcaCustomerIdentifierMap()).thenReturn(mapCustomerIdentifier);

		customerIdentifiers = controller.getGeneralInformationCustomerIdentifiers(testCard, userSession);
		assertNotNull(customerIdentifiers);
	}

	@Test
	public void whenGetGeneralInformationCustomerIdentifiers_GivenValidData_ThenReturnNotNull() throws TsysException {
		CustomerIdentifiers customerIdentifiers;
		ArrayList<CustomerList> arrListCustomerList = new ArrayList<>();
		Name name = new Name();
		name.setNameOnCard("nameTest");
		CustomerList customer = new CustomerList(); //customer
		customer.setCustomerId("000037310");
		customer.setAccount("00000026287");
		customer.setCardLast4("7890");
		customer.setName(name);
		arrListCustomerList.add(customer);

		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		when(tsysAlertFacade.tsysGeneralInformationByCardNumber(anyString(), anyBoolean())).thenReturn(tsysGeneralInformationResponse);
		when(tsysGeneralInformationResponse.getCustomerList()).thenReturn(arrListCustomerList);
		when(userSession.getCcaCustomerIdentifierMap()).thenReturn(null);

		customerIdentifiers = controller.getGeneralInformationCustomerIdentifiers("4455123456457890", userSession);
		assertNotNull(customerIdentifiers);
	}

	@Test
	public void whenGetGeneralInformationCustomerIdentifiers_GivenArrayNull_ThenReturnNull() throws TsysException {
		CustomerIdentifiers customerIdentifiers;
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		when(tsysAlertFacade.tsysGeneralInformationByCardNumber(anyString(), anyBoolean())).thenReturn(null);
		when(tsysGeneralInformationResponse.getCustomerList()).thenReturn(null);
		when(userSession.getCcaCustomerIdentifierMap()).thenReturn(null);

		customerIdentifiers = controller.getGeneralInformationCustomerIdentifiers("4455123456457890", userSession);
		assertNull(customerIdentifiers);
	}

	@Test
	public void whenGetGeneralInformationCustomerIdentifiers_GivenErrorTsysException_ThenReturnNull() throws TsysException {
		CustomerIdentifiers customerIdentifiers;
		when(TsysAlertFacade.getInstance()).thenReturn(tsysAlertFacade);
		when(tsysAlertFacade.tsysGeneralInformationByCardNumber(anyString(), anyBoolean())).thenThrow(new TsysException());
		when(tsysGeneralInformationResponse.getCustomerList()).thenReturn(null);

		customerIdentifiers = controller.getGeneralInformationCustomerIdentifiers("4455123456457890", userSession);
		assertNull(customerIdentifiers);
	}
}
