package evertec.cibp.web;

import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.form.bean.MobileApiSettingsBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;

import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
public class MobileStatusControllerUT extends AbstractModelAndViewTests {

    @Mock
    HttpServletRequest request;

    @Mock
    HttpServletResponse response;

    @Mock
    CustomerServices customerServices;

    @Mock
    MessageSource messageSource;

    @Mock
    MobileApiSettingsBean mobileAppGatewaySettings;

    @InjectMocks
    MobileStatusController controller;

    @Test
    public void whenGlobalStatus_givenRequestAndResponse_thenReturnModelAndView () throws Exception {
        List<GlobalEntitlement> globalEntitlementList = new ArrayList<>();
        GlobalEntitlement globalEntitlement = new GlobalEntitlement();
        globalEntitlementList.add(globalEntitlement);

        when(customerServices.getGlobalEntitlements()).thenReturn(globalEntitlementList);
        when(messageSource.getMessage(Mockito.anyString(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn("");
        when(mobileAppGatewaySettings.getLatestVersionIphoneApp()).thenReturn(1);
        when(mobileAppGatewaySettings.getMinimumRequiredVersionIphoneApp()).thenReturn(1);

        ModelAndView model = controller.globalStatus(request, response);
        assertViewName(model, "globalStatus");
    }
}
