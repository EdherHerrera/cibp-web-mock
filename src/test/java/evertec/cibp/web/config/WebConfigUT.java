/**
 * Config test for WebConfig
 * <p>
 * Project: CIBP Web
 * Company: Evertec
 */
package evertec.cibp.web.config;

import evertec.cibp.web.interceptor.*;

import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MessageSourceResourceBundleLocator;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.theme.ThemeChangeInterceptor;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Luis M. Kupferberg Ruiz (lkupferberg@overactive.com)
 * @created 2020-02-12 17:28
 * @version 1.0
 * @since 1.0
 * @see WebConfig
 */
@RunWith(PowerMockRunner.class)
public class WebConfigUT {


    /**
     * Base names field name
     */
    private static final String BASE_NAMES_FIELD_NAME = "basenames"; //Base names field name

    /**
     * Use code as default message field name
     */
    private static final String USE_CODE_AS_DEFAULT_MESSAGE_FIELD_NAME = "useCodeAsDefaultMessage"; // Use code as default message field name

    /**
     * Default encoding field name
     */
    private static final String DEFAULT_ENCODING_FIELD_NAME = "defaultEncoding"; // Default encoding field name

    /**
     * messages file name
     */
    private static final String MESSAGES_FILE_NAME = "messages";// messages file name

    /**
     * test file name
     */
    private static final String TEXTS_FILE_NAME = "texts"; // test file name

    /**
     * utf-8 encoding
     */
    private static final String UTF_8_STRING = "UTF-8"; // utf-8 encoding

    /**
     * Message interpolator field name
     */
    private static final String MESSAGE_INTERPOLATOR_FIELD = "messageInterpolator"; // Message interpolator field name

    /**
     * User resource bundle locator field
     */
    private static final String USER_RESOURCE_BUNDLE_LOCATOR_FIELD =
            "userResourceBundleLocator"; // User resource bundle locator field

    /**
     * Message source field
     */
    private static final String MESSAGE_SOURCE_FIELD = "messageSource"; // Message source field

    /**
     * Get interceptors method
     */
    private static final String GET_INTERCEPTORS_METHOD = "getInterceptors"; //Get interceptors method

    /**
     * Catch internal exception error
     */
    private static final String AN_ERROR_HAS_OCCUR = "An error has occur "; //Catch internal exception error

    /**
     * Instance to test
     */
    private WebConfig instance;//WebConfig instance for test

    /**
     * Initial setup for Test
     */
    @Before
    public void setUp() {
        instance = new WebConfig();
    }

    /**
     * Verify the message source
     */
    @Test
    public void verifyMessageSource() {

        //When
        ReloadableResourceBundleMessageSource messageSource = null; // message source to validate

        if ((instance.messageSource() instanceof ReloadableResourceBundleMessageSource)) {
            messageSource = (ReloadableResourceBundleMessageSource) instance.messageSource();
        }

        //Then
        validateMessageSourceValues(messageSource);
    }

    /**
     * Function to validate the message source
     *
     * @param messageSource ReloadableResourceBundleMessageSource
     */
    private static void validateMessageSourceValues(ReloadableResourceBundleMessageSource messageSource) {


        final String[] baseNames =
                Whitebox.getInternalState(messageSource, BASE_NAMES_FIELD_NAME); // base names configured


        final boolean useCodeAsDefMessage = Whitebox.getInternalState(messageSource,
                USE_CODE_AS_DEFAULT_MESSAGE_FIELD_NAME); // Use code default on message configured


        final String defEncoding =
                Whitebox.getInternalState(messageSource,
                        DEFAULT_ENCODING_FIELD_NAME); // Default encoding used in message source configured.


        final int numBaseNames = 2; //number base names mapping

        assertEquals(numBaseNames, baseNames.length);
        assertEquals(MESSAGES_FILE_NAME, baseNames[0]);
        assertEquals(TEXTS_FILE_NAME, baseNames[1]);
        assertTrue(useCodeAsDefMessage);
        assertEquals(UTF_8_STRING, defEncoding);
    }

    /**
     * Verify validator
     */
    @Test
    public void verifyGetValidator() {

        final LocalValidatorFactoryBean localValidatorFactoryBean = instance.getValidator(); //Bean used in instance

        final ResourceBundleMessageInterpolator messageInterpolator =
                Whitebox.getInternalState(localValidatorFactoryBean,
                        MESSAGE_INTERPOLATOR_FIELD); // Messsage interpolator


        final MessageSourceResourceBundleLocator messageSourceResourceBundleLocator =
                Whitebox.getInternalState(
                        messageInterpolator, USER_RESOURCE_BUNDLE_LOCATOR_FIELD); // Message Source Resource BundleLocator

        final ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource = Whitebox.getInternalState(
                messageSourceResourceBundleLocator, MESSAGE_SOURCE_FIELD); // Reloadable Resource Bundle MessageSource

        validateMessageSourceValues(reloadableResourceBundleMessageSource);
    }

    /**
     * Verify all interceptors
     */
    @Test
    public void verifyingAddInterceptors() {

        final InterceptorRegistry registry = new InterceptorRegistry(); // registry used in instance variable

        //When
        instance.addInterceptors(registry);
        try {

            final List<HandlerInterceptorAdapter> interceptors =
                    Whitebox.invokeMethod(registry, GET_INTERCEPTORS_METHOD); //Interceptors variable to validate

            //Then
            final int numInterceptors = 9;//number interceptors for handler
            final int posGlobalEntiltement = 0;//Global interceptor
            final int posLocaleChange = 1;//Locale Change interceptor
            final int posThemeChange = 2;//ThemeChange Interceptor
            final int posLoggedIn = 3;//LoggedIn interceptor
            final int posHasBanking = 4;//HasBanking interceptor
            final int posGlobalPostHandler = 5;//GlobalPostHandler interceptor
            final int posUserAgent = 6;//UserAgent Interceptor
            final int posUrlRestrict = 7;//URLRestrict interceptor

            assertEquals(numInterceptors, interceptors.size());
            assertEquals(GlobalEntitlementInterceptor.class, interceptors.get(posGlobalEntiltement).getClass());
            assertEquals(LocaleChangeInterceptor.class, interceptors.get(posLocaleChange).getClass());
            assertEquals(ThemeChangeInterceptor.class, interceptors.get(posThemeChange).getClass());
            assertEquals(LoggedinInterceptor.class, interceptors.get(posLoggedIn).getClass());
            assertEquals(HasBankingSessionInterceptor.class, interceptors.get(posHasBanking).getClass());
            assertEquals(GlobalPostHandleFormInterceptor.class, interceptors.get(posGlobalPostHandler).getClass());
            assertEquals(UserAgentInterceptor.class, interceptors.get(posUserAgent).getClass());
           // assertEquals(UrlRestrictIntercept.class, interceptors.get(posUrlRestrict).getClass());
        } catch (Exception ex) {
            fail(AN_ERROR_HAS_OCCUR + ex.getMessage());
        }
    }
}
