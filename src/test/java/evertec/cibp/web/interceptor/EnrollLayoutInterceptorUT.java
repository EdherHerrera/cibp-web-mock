package evertec.cibp.web.interceptor;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.session.EnrollmentSession;
import evertec.cibp.web.session.UserSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({EnrollLayoutInterceptor.class, WebUtils.class, CustomerProfile.class,
	StaticMessageSource.class, ApiCallerFactory.class})
public class EnrollLayoutInterceptorUT {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@Mock
	private ModelAndView modelAndView;
	
	@Mock
	private Object handler;
		
	@Mock
	private ModelMap modelMap;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@InjectMocks
	private EnrollLayoutInterceptor interceptor;

	@Mock
	private ApiCaller apiCaller;

	@Before
	public void setUp() throws Exception {
		
		//MockitoAnnotations.initMocks(this);
	    request = new MockHttpServletRequest();
	    response = new MockHttpServletResponse();

		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

	    PowerMockito.mockStatic(WebUtils.class);
	    PowerMockito.mockStatic(StaticMessageSource.class);
	    //PowerMockito.mock(CustomerProfile.class);
	    //PowerMockito.mock(CustomerFlagType.class);
	    PowerMockito.mockStatic(StaticMessageSource.class);

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void whenAddBsmartEnrollVar_Is_bSmartEnrollment_True()
			throws Exception {
		
		EnrollmentSession enrollmentSession = new EnrollmentSession();
		enrollmentSession.setValidatedAcctProductId("086");
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setUserInterface(UserInterface.COMMERCIAL);	
		UserSession uS = new UserSession();
		uS.setCustomerProfile(customerProfile);
		when(WebUtils.getSessionAttribute(request,"enrollmentSession")).thenReturn(enrollmentSession);
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(uS);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(true);
		when(modelAndView.getViewName()).thenReturn("not null"); //This is here so the test gets to the lines we want.
		when(modelAndView.getModel()).thenReturn(modelMap);
		//when(entitlement.getStatus()).thenReturn(EntitlementStatus.DISABLED);
		interceptor.postHandle(request, response, handler, modelAndView);
		//verify(modelMap).put("bsmartEnrollment", "T");
		PowerMockito.verifyPrivate(interceptor, Mockito.atLeast(1)).invoke("addBsmartEnrollVar", modelMap, Mockito.any());
		//verify(modelMap).put("comBsmartEnrollment", true);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void whenAddBsmartEnrollVar_Is_bSmartEnrollment_False()
			throws Exception {
		
		EnrollmentSession enrollmentSession = new EnrollmentSession();
		enrollmentSession.setValidatedAcctProductId("026");
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setUserInterface(UserInterface.RETAIL);	
		UserSession uS = new UserSession();
		uS.setCustomerProfile(customerProfile);
	
		when(WebUtils.getSessionAttribute(request,"enrollmentSession")).thenReturn(enrollmentSession);
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(uS);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EPAYROLL)).thenReturn(false);
		when(modelAndView.getViewName()).thenReturn("not null"); //This is here so the test gets to the lines we want.
		when(modelAndView.getModel()).thenReturn(modelMap);
		//when(entitlement.getStatus()).thenReturn(EntitlementStatus.DISABLED);
		//EnrollLayoutInterceptor myClass = PowerMockito.spy(interceptor);
		interceptor.postHandle(request, response, handler, modelAndView);
		//verify(modelMap).put("useComercialInterface", "T");
		//PowerMockito.verifyPrivate(interceptor, Mockito.never()).invoke("addBsmartEnrollVar", modelMap, Mockito.any());
		String useCommercialInterface = "useComercialInterface";
		verify(modelMap).put(useCommercialInterface, "F");
		
		
	}
}
