/**
 * 
 */
package evertec.cibp.web.interceptor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.TransactionRequest;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

/**
 * @author ET57970
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ESAPI.class, Utils.class, WebUtils.class, StaticMessageSource.class, CustomerServices.class, 
	CustomerProfile.class, CustomerProfileService.class, TVFrontendAccount.class,  BankingSession.class,
	TransactionsResponse.class, CustomerFacade.class, TransactionRequest.class,
	TVAccountProductDefinition.class})
public class LayoutInteractionIT extends AbstractModelAndViewTests {
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	
	@Spy
	private List<String> views = new LinkedList<String>();
	
	@Mock
	private ModelAndView modelAndView;
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private HTTPUtilities httpUtilities;
	
	@Mock
	private Object handler;
	
	@InjectMocks
	private LayoutInterceptor interceptor;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ESAPI.class);
		PowerMockito.mockStatic(WebUtils.class);
	    request = mock(HttpServletRequest.class);
	    response = mock(HttpServletResponse.class);
		httpUtilities = mock(HTTPUtilities.class);
	    PowerMockito.mockStatic(StaticMessageSource.class);
	    PowerMockito.mockStatic(WebUtils.class);
	    when(request.getContextPath()).thenReturn("http://localhost:8086/cibp-web");
	    when(WebUtils.getSessionAttribute(any(), eq("userSession"))).thenReturn(userSession);
	    when(userSession.getCustomerProfile()).thenReturn(customerProfile);
	    when(userSession.getAccountMLA()).thenReturn("12345001001");
	}
	
	
	@Test
	public void whenPostHandle_GivenMBSE_2513FlagTrue_Agent_Null() throws Exception {
		
		ModelMap testMap = new ModelMap() {{
			put("selectedAccount", "MLA");
		}};
		when(userSession.getUserAgentType()).thenReturn(null);
		when(modelAndView.getViewName()).thenReturn("transaction");
		when(modelAndView.getModelMap()).thenReturn(testMap);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513)).thenReturn(Boolean.TRUE);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		when(request.getContextPath()).thenReturn("http://localhost:8086/cibp-web");
		doNothing().when(httpUtilities).sendRedirect(response, "http://localhost:8086/cibp-web/actions/detail?account=12345001001");
		interceptor.postHandle(request, response, handler, modelAndView);
		verify(httpUtilities, times(0)).sendRedirect(response, "http://localhost:8086/cibp-web/actions/detail?account=12345001001&tabNum=0&trxTabNum=0");
	}

}
