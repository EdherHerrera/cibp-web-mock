package evertec.cibp.web.interceptor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.owasp.esapi.errors.AccessControlException;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.services.responses.UsernameValidationResponse.ASK_FOR;
import evertec.cibp.web.WebAppController;
import evertec.cibp.web.form.SessionInitializationController;
import evertec.cibp.web.form.interruptions.OOBInterruptionForm;
import evertec.cibp.web.session.UserSession;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ WebUtils.class, ESAPI.class })
public class SignonTasksInterceptorUT {
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private HTTPUtilities httpUtilities;
	
	@Mock
	private Object handler;

	@InjectMocks
	private SignonTasksInterceptor signonTasksInterceptor;
	
	@Before
	public void setUp() throws Exception {
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(ESAPI.class);
		
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
	}
	
	@Test
	public void whenPreHandle_givenIsLoginInterruptionEnable_thenRedirectToInterruption() throws Exception {
		ArrayList<ASK_FOR> taskList = new ArrayList<>();
		taskList.add(ASK_FOR.LOGIN_INTERRUPTION_PAGE);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getTasksList()).thenReturn(taskList);
		when(userSession.getIsLoginInterruptionEnable()).thenReturn(true);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		
		boolean value = signonTasksInterceptor.preHandle(request, response, handler);
		
		assertFalse(value);
		verify(httpUtilities, times(1)).sendRedirect(response, "/actions/oobInterruptionPage");
	}
	
	@Test
	public void whenPreHandle_givenIsLoginInterruptionDisable_thenNotRedirect() throws Exception {
		ArrayList<ASK_FOR> taskList = new ArrayList<>();
		taskList.add(ASK_FOR.SESSION_INIT);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getTasksList()).thenReturn(taskList);
		when(userSession.getIsLoginInterruptionEnable()).thenReturn(false);
		
		handler = new SessionInitializationController();
		
		boolean value = signonTasksInterceptor.preHandle(request, response, handler);
		
		assertTrue(value);
		assertEquals(response.getRedirectedUrl(), null);
	}
	
	@Test
	public void whenvalidateRedirects_givenSessionInitializationHandler_thenReturnPortalRedirect () throws AccessControlException, IOException {
		ArrayList<ASK_FOR> taskList = new ArrayList<>();
		when(userSession.getTasksList()).thenReturn(taskList);
		
		handler = new SessionInitializationController();
		
		boolean value = signonTasksInterceptor.validateRedirects(userSession, request, response, handler);
		
		assertFalse(value);
		verify(httpUtilities, times(1)).sendRedirect(response, "/actions/portal");
	}
	
	@Test
	public void whenvalidateRedirects_givenWepAppHandler_thenReturnPortalRedirect () throws AccessControlException, IOException {
		ArrayList<ASK_FOR> taskList = new ArrayList<>();
		when(userSession.getTasksList()).thenReturn(taskList);
		
		handler = new WebAppController();
		
		boolean value = signonTasksInterceptor.validateRedirects(userSession, request, response, handler);
		
		assertFalse(value);
		verify(httpUtilities, times(1)).sendRedirect(response, "/actions/login");
	}

}
