package evertec.cibp.web.interceptor;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.math.BigInteger;

import static org.mockito.ArgumentMatchers.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerWealthPreferred;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.RedirectUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ WebUtils.class, ESAPI.class })
public class MbdpFormInterceptorUT {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Mock
	Object object;

	@Mock
	CustomerServices customerServices;

	@Mock
	CustomerProfile customerProfile;

	@Mock
	UserSession userSession;
	
	@Mock
	private HTTPUtilities httpUtilities;	

	@InjectMocks
	MbdpFormInterceptor mbdpFormInterceptor;

	@Before
	public void setUp() throws Exception {

		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		ReflectionTestUtils.setField(mbdpFormInterceptor, "wealthCodes", "wealthCodes");
		ReflectionTestUtils.setField(mbdpFormInterceptor, "preferredCodes", "preferredCodes");
		httpUtilities = mock(HTTPUtilities.class);
	}

	@Test
	public void whenPreHandle_GivenAllowNonTransactionalTrueNoTransactional_ThenBooleanFalse() throws Exception {
		ReflectionTestUtils.setField(mbdpFormInterceptor, "allowNonTransactional", "true");
		when(userSession.getCanOpenDepositAccount()).thenReturn(true);
		when(userSession.isComercialCustomer()).thenReturn(false);
		CustomerProfile customProfile = new CustomerProfile();
		customProfile.setIsTransactional("N");
		when(userSession.getCustomerProfile()).thenReturn(customProfile);
		mockStatic(WebUtils.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		mockStatic(ESAPI.class);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		doNothing().when(httpUtilities).sendRedirect(response, "http://localhost:2020/cibp-web/actions/portal");
		
		boolean preHandle = mbdpFormInterceptor.preHandle(request, response, object);
		assertFalse(preHandle);
	}

	@Test
	public void whenPreHandle_GivenAllowNonTransactionalTrueIsTransactional_ThenBooleanTrue() throws Exception {
		ReflectionTestUtils.setField(mbdpFormInterceptor, "allowNonTransactional", "true");
		when(userSession.getCanOpenDepositAccount()).thenReturn(true);
		when(userSession.isComercialCustomer()).thenReturn(false);
		CustomerProfile customProfile = new CustomerProfile();
		customProfile.setIsTransactional("Y");
		when(userSession.getCustomerProfile()).thenReturn(customProfile);
		mockStatic(WebUtils.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		boolean preHandle = mbdpFormInterceptor.preHandle(request, response, object);
		assertTrue(preHandle);
	}

	@Test
	public void whenPreHandle_GivenAllowNonTransactionalTrueIsCommercial_ThenBooleanFalse() throws Exception {
		ReflectionTestUtils.setField(mbdpFormInterceptor, "allowNonTransactional", "true");
		when(userSession.getCanOpenDepositAccount()).thenReturn(true);
		when(userSession.isComercialCustomer()).thenReturn(true);
		CustomerProfile customProfile = new CustomerProfile();
		customProfile.setIsTransactional("N");
		when(userSession.getCustomerProfile()).thenReturn(customProfile);
		mockStatic(WebUtils.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		mockStatic(ESAPI.class);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		doNothing().when(httpUtilities).sendRedirect(response, "/actions/portal");
		
		boolean preHandle = mbdpFormInterceptor.preHandle(request, response, object);
		assertFalse(preHandle);
	}

	@Test
	public void whenPreHandle_GivenAllowNonTransactionalFaleIsCommercial_ThenBooleanFalse() throws Exception {
		ReflectionTestUtils.setField(mbdpFormInterceptor, "allowNonTransactional", "false");
		when(userSession.getCanOpenDepositAccount()).thenReturn(true);
		when(userSession.isComercialCustomer()).thenReturn(true);
		CustomerProfile customProfile = new CustomerProfile();
		customProfile.setIsTransactional("N");
		when(userSession.getCustomerProfile()).thenReturn(customProfile);
		mockStatic(WebUtils.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		boolean preHandle = mbdpFormInterceptor.preHandle(request, response, object);
		assertTrue(preHandle);
	}

	@Test
	public void whenPreHandle_GivenAllowNonTransactionalFalseNoTransacional_ThenBooleanFalse() throws Exception {
		ReflectionTestUtils.setField(mbdpFormInterceptor, "allowNonTransactional", "false");
		when(userSession.getCanOpenDepositAccount()).thenReturn(true);
		when(userSession.isComercialCustomer()).thenReturn(false);
		CustomerProfile customProfile = new CustomerProfile();
		customProfile.setIsTransactional("N");
		when(userSession.getCustomerProfile()).thenReturn(customProfile);
		mockStatic(WebUtils.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		boolean preHandle = mbdpFormInterceptor.preHandle(request, response, object);
		assertTrue(preHandle);
	}

	@Test
	public void whenPreHandle_GivenCanOpenDepositAccountNullPermIdNull_ThenBooleanFalse() throws Exception {
		ReflectionTestUtils.setField(mbdpFormInterceptor, "allowNonTransactional", "false");
		String key = "123456789";
		BankingSession bankingSession = new BankingSession(null, key);
		when(userSession.getCanOpenDepositAccount()).thenReturn(null);
		when(userSession.getBankingSession()).thenReturn(bankingSession);

		CustomerProfile customerProfile = new CustomerProfile();
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		CustomerWealthPreferred customerWealthPreferred = new CustomerWealthPreferred();
		when(userSession.getCustomerService()).thenReturn(customerServices);
		when(userSession.getCustomerWealthPreferred(any(), any())).thenReturn(customerWealthPreferred);

		mockStatic(WebUtils.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		boolean preHandle = mbdpFormInterceptor.preHandle(request, response, object);
		assertTrue(preHandle);
	}

	@Test
	public void whenPreHandle_GivenCanOpenDepositAccountNullPermId_ThenBoolean() throws Exception {
		ReflectionTestUtils.setField(mbdpFormInterceptor, "allowNonTransactional", "false");
		String key = "123456789";
		BankingSession bankingSession = new BankingSession(new BigInteger("22222001001"), key);
		when(userSession.getCanOpenDepositAccount()).thenReturn(null);
		when(userSession.getBankingSession()).thenReturn(bankingSession);

		CustomerProfile customerProfile = new CustomerProfile();
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		CustomerWealthPreferred customerWealthPreferred = new CustomerWealthPreferred();
		when(userSession.getCustomerService()).thenReturn(customerServices);
		when(userSession.getCustomerWealthPreferred(any(), any())).thenReturn(customerWealthPreferred);

		mockStatic(WebUtils.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		boolean preHandle = mbdpFormInterceptor.preHandle(request, response, object);
		assertTrue(preHandle);
	}

	@Test
	public void whenPreHandle_GivenUserSessionNull_ThenPreHandleFalse() throws Exception {
		mockStatic(WebUtils.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(null);
		boolean preHandle = mbdpFormInterceptor.preHandle(request, response, object);
		assertTrue(preHandle);
	}

	@Test
	public void whenPreHandle_GivenCanOpenDepositAccount_ThenBooleanFalse() throws Exception {
		ReflectionTestUtils.setField(mbdpFormInterceptor, "allowNonTransactional", "false");
		when(userSession.getCanOpenDepositAccount()).thenReturn(false);
		when(userSession.isComercialCustomer()).thenReturn(true);
		CustomerProfile customProfile = new CustomerProfile();
		customProfile.setIsTransactional("N");
		when(userSession.getCustomerProfile()).thenReturn(customProfile);
		mockStatic(WebUtils.class);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		boolean preHandle = mbdpFormInterceptor.preHandle(request, response, object);
		assertTrue(preHandle);
	}
}
