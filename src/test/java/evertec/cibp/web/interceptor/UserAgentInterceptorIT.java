/**
 * 
 */
package evertec.cibp.web.interceptor;

import evertec.cibp.web.config.WebConfig;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author mdias
 *
 */
@RunWith(PowerMockRunner.class)
public class UserAgentInterceptorIT {
	
	/**
     * Instance to test
     */
    private WebConfig webConfig;//WebConfig instance for test
    
    @Before
    public void setUp() {
    	webConfig = new WebConfig();
    }
	/**
	 * when Upstream new UserAgentInterceptor() 
	 * ExecutedFrom WebConfig addInterceptors
	 * Then UserAgentInterceptor added in List
	 * @throws Exception
	 */
	@Test
	@Ignore("WITH ERROR")
	public void whenITUpstreamNewUserAgentInterceptor_ExecutedFromWebConfig_ThenUserAgentInterceptorAdded () throws Exception {
		
		final InterceptorRegistry registry = new InterceptorRegistry(); // registry used in instance variable
		 
		webConfig.addInterceptors(registry);
		
		final List<HandlerInterceptorAdapter> interceptors =
                 Whitebox.invokeMethod(registry, "getInterceptors"); //Interceptors variable to validate
		
		final int numInterceptors = 8;//number interceptors for handler
		
		final int posUserAgent = 6;//UserAgent Interceptor
		
		assertEquals(numInterceptors, interceptors.size());
		
		assertEquals(UserAgentInterceptor.class, interceptors.get(posUserAgent).getClass());
		
	}

}
