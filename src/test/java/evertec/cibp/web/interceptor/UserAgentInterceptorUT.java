package evertec.cibp.web.interceptor;

import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.session.EnrollmentSession;
import evertec.cibp.web.session.UserSession;

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, UserAgentInterceptor.class, StaticMessageSource.class })
public class UserAgentInterceptorUT extends AbstractModelAndViewTests {
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@Spy
	private List<String> views = new LinkedList<String>();
	
	@Mock
	private ModelAndView modelAndView;
	
	@Mock
	private Object handler;

	@Mock
	private UserSession userSession;
	
	@Mock
	private UserSession mUserSession;
	
	@Mock
	private EnrollmentSession mEnrollmentSession;
	
	@InjectMocks
	private UserAgentInterceptor interceptor;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	    request = new MockHttpServletRequest();
	    response = new MockHttpServletResponse();
		PowerMockito.mockStatic(WebUtils.class);	    
	    PowerMockito.mockStatic(StaticMessageSource.class);
	}

	@Test
	public void whenPostHandle_GivenMBFIS_MOBILEFlagFalse_ThenViewAvailableFalse () throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_MOBILE)).thenReturn(Boolean.FALSE);
		interceptor.postHandle(request, response, handler, modelAndView);
		assertFalse(views.contains("termsandcondssec"));
	}

	@Test
	@Ignore("WITH ERROR")
	public void whenPostHandle_GivenMBFIS_MOBILEFlagTrue_ThenViewAvailableTrue () throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS_MOBILE)).thenReturn(Boolean.TRUE);
		interceptor.postHandle(request, response, handler, modelAndView);
		assertTrue(views.contains("termsandcondssec"));
	}
	
	@Test
	public void whenSetUserAgentType_givenAthMovilHeader_thenReturnAndroidType () throws Exception {
		String requestHeader = "Android App; ATH Movil; JSON Client; en-us";
		
		UserAgentType userAgent = Whitebox.invokeMethod(interceptor, "setUserAgentType", 
				requestHeader, mUserSession, mEnrollmentSession);
		
		assertEquals(userAgent, UserAgentType.ANDROID);
	}
	
	@Test
	public void whenSetUserAgentType_givenJsonWithoutAthmHeader_thenReturnJsonType () throws Exception {
		String requestHeader = "Android App; JSON Client; en-us";
		
		UserAgentType userAgent = Whitebox.invokeMethod(interceptor, "setUserAgentType", 
				requestHeader, mUserSession, mEnrollmentSession);
		
		assertEquals(userAgent, UserAgentType.JSON);
	}
	
	
	@Test
	public void whenSetUserAgentType_givenAndroidJsonClientHeader_thenReturnJsonType () throws Exception {
		String requestHeader = "Android App; JSON Client; en-us";
		
		UserAgentType userAgent = Whitebox.invokeMethod(interceptor, "setUserAgentType", 
				requestHeader, mUserSession, mEnrollmentSession);
		
		assertEquals(userAgent, UserAgentType.JSON);
	}
	
	@Test
	public void whenSetUserAgentType_givenIphoneJsonClientHeader_thenReturnJsonType () throws Exception {
		String requestHeader = "Iphone App; JSON Client; en-us";
		
		UserAgentType userAgent = Whitebox.invokeMethod(interceptor, "setUserAgentType", 
				requestHeader, mUserSession, mEnrollmentSession);
		
		assertEquals(userAgent, UserAgentType.JSON);
	}
	
	@Test
	public void whenSetUserAgentType_givenAndroidWithoutJsonClientHeader_thenReturnAndroidType () throws Exception {
		String requestHeader = "Android App; en-us";
		
		UserAgentType userAgent = Whitebox.invokeMethod(interceptor, "setUserAgentType", 
				requestHeader, mUserSession, mEnrollmentSession);
		
		assertEquals(userAgent, UserAgentType.ANDROID);
	}
	
	@Test
	public void whenSetUserAgentType_givenIphoneWithoutJsonClientHeader_thenReturnIphoneType () throws Exception {
		String requestHeader = "iPhone App; en-us";
		
		UserAgentType userAgent = Whitebox.invokeMethod(interceptor, "setUserAgentType", 
				requestHeader, mUserSession, mEnrollmentSession);
		
		assertEquals(userAgent, UserAgentType.IPHONE);
	}
	
	@Test
	public void whenSetUserAgentType_givenIpoDWithoutJsonClientHeader_thenReturnIpodType () throws Exception {
		String requestHeader = "iPod App; en-us";
		
		UserAgentType userAgent = Whitebox.invokeMethod(interceptor, "setUserAgentType", 
				requestHeader, mUserSession, mEnrollmentSession);
		
		assertEquals(userAgent, UserAgentType.IPHONE);
	}
	
	@Test
	public void whenSetUserAgentType_givenIpADWithoutJsonClientHeader_thenReturniPadType () throws Exception {
		String requestHeader = "iPad App; en-us";
		
		UserAgentType userAgent = Whitebox.invokeMethod(interceptor, "setUserAgentType", 
				requestHeader, mUserSession, mEnrollmentSession);
		
		assertEquals(userAgent, UserAgentType.IPAD);
	}
	
	@Test
	public void whenSetUserAgentType_givenBlackBerryWithoutJsonClientHeader_thenReturnMobileType () throws Exception {
		String requestHeader = "BlackBerry; en-us";
		
		UserAgentType userAgent = Whitebox.invokeMethod(interceptor, "setUserAgentType", 
				requestHeader, mUserSession, mEnrollmentSession);
		
		assertEquals(userAgent, UserAgentType.MOBILE);
	}
	
	@Test
	public void whenSetUserAgentType_givenIEMobileWithoutJsonClientHeader_thenReturnMobileType () throws Exception {
		String requestHeader = "IEMobile; en-us";
		
		UserAgentType userAgent = Whitebox.invokeMethod(interceptor, "setUserAgentType", 
				requestHeader, mUserSession, mEnrollmentSession);
		
		assertEquals(userAgent, UserAgentType.MOBILE);
	}
	
	@Test
	public void whenSetUserAgentType_givenOtherClientHeader_thenReturnWebType () throws Exception {
		String requestHeader = "";
		
		UserAgentType userAgent = Whitebox.invokeMethod(interceptor, "setUserAgentType", 
				requestHeader, mUserSession, mEnrollmentSession);
		
		assertEquals(userAgent, UserAgentType.WEB);
	}
	
	@Test
	public void whenIsMobile_givenBlackBerryClientHeader_thenReturnTrue () throws Exception {
		String requestHeader = "BlackBerry";
		
		boolean result = Whitebox.invokeMethod(interceptor, "isMobile", 
				requestHeader);
		
		assertTrue(result);
	}
	
	@Test
	public void whenIsMobile_givenIEMobileClientHeader_thenReturnTrue () throws Exception {
		String requestHeader = "IEMobile";
		
		boolean result = Whitebox.invokeMethod(interceptor, "isMobile", 
				requestHeader);
		
		assertTrue(result);
	}
	
	@Test
	public void whenIsMobile_givenAndroidClientHeader_thenReturnFalse () throws Exception {
		String requestHeader = "Android";
		
		boolean result = Whitebox.invokeMethod(interceptor, "isMobile", 
				requestHeader);
		
		assertFalse(result);
	}
	
	public void whenUserAgentInterceptor_ThenViewCookiePreferenceInList () throws Exception {
		
		try {
			interceptor = new UserAgentInterceptor();
		}
		catch(Exception ex) {
			fail(); // If ocurrs an error during instaciation then fail
		}
	}

	@Test
	public void whenPostHandleHasTransAddScreen_thenReturnTrue () throws Exception {
		views.add("transAddScreen");
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.MOBILE);
		modelAndView = new ModelAndView();
		modelAndView.setViewName("transAddScreen");
		interceptor.postHandle(request, response, handler, modelAndView);
		assertTrue(modelAndView.getViewName().equals("mobile/transAddScreen"));

	}

}
