package evertec.cibp.web.interceptor;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.bind.ServletRequestUtils;

import evertec.cibp.core.models.RegainAccessToken;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.services.modules.RegainAccessServiceModule;

/**
 * 
 * @author s679338
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ServletRequestUtils.class, ESAPI.class, StaticMessageSource.class })
public class RegainAccessTokenInterceptorUT {
	
	private MockHttpServletRequest request;

	private MockHttpServletResponse response;
	
	@Mock 
	private HTTPUtilities mHTTPUtilities;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private RegainAccessToken regainAccessToken;
	
	@Mock
	private RegainAccessServiceModule regainAccessServiceModule; 
	
	@InjectMocks
	private RegainAccessTokenInterceptor interceptor;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ServletRequestUtils.class);
		PowerMockito.mockStatic(ESAPI.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		
		when(ESAPI.httpUtilities()).thenReturn(mHTTPUtilities);
		doNothing().when(mHTTPUtilities).sendRedirect(any(), any());
	}

	@Test
	public void whenManageAccess_giveTokenNull_thenRedirectToRegainAccessWithOptions() throws Exception {
		String token = "token";
		interceptor.setCustomerServices(customerService);
		when(customerService.getRegainAccessToken(any())).thenReturn(regainAccessToken);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC224)).thenReturn(Boolean.FALSE);
		
		boolean manageAccess = Whitebox.invokeMethod(interceptor, "manageAccess", request, response, token);
		
		assertEquals(true, manageAccess);
	}

	@Test
	public void whenManageAccess_giveTokenNullAndBlockedMobileParam_thenRedirectToRegainAccessBlocked() throws Exception {
		String token = "token";
		interceptor.setCustomerServices(customerService);
		when(customerService.getRegainAccessToken(any())).thenReturn(regainAccessToken);
		when(ServletRequestUtils.getStringParameter(any(), eq("passwordBlockedMobile"))).thenReturn("true");
		
		
		boolean manageAccess = Whitebox.invokeMethod(interceptor, "manageAccess", request, response, token);
		
		assertEquals(true, manageAccess);
	}
	
	@Test
	public void whenManageAccess_giveTokenNotNull_thenRedirectToExpiredView() throws Exception {
		String token = "token";
		boolean flagRegainAccess = false;
		interceptor.setCustomerServices(customerService);
		interceptor.setRegainAccessServiceModule(regainAccessServiceModule);
		
		when(customerService.getRegainAccessToken(any())).thenReturn(regainAccessToken);
		when(regainAccessToken.getEmail()).thenReturn("test@test.com");
		when(ServletRequestUtils.getStringParameter(any(), eq("passwordBlockedMobile"))).thenReturn("true");
		when(ServletRequestUtils.getStringParameter(any(), eq("resource"))).thenReturn("USERID");
		when(regainAccessServiceModule.isEmailBlocked(any())).thenReturn(false);
		
		
		boolean manageAccess = Whitebox.invokeMethod(interceptor, "manageAccess", request, response, token);
		
		assertEquals(false, manageAccess);
	}
	
	@Test
	public void whenManageAccess_giveEmailBlocked_thenRedirectToExpiredView() throws Exception {
		String token = "token";
		boolean flagRegainAccess = false;
		interceptor.setCustomerServices(customerService);
		interceptor.setRegainAccessServiceModule(regainAccessServiceModule);
		
		when(customerService.getRegainAccessToken(any())).thenReturn(regainAccessToken);
		when(regainAccessToken.getEmail()).thenReturn("test@test.com");
		when(ServletRequestUtils.getStringParameter(any(), eq("passwordBlockedMobile"))).thenReturn("true");
		when(ServletRequestUtils.getStringParameter(any(), eq("resource"))).thenReturn("USERID");
		when(regainAccessServiceModule.isEmailBlocked(any())).thenReturn(true);
		
		
		boolean manageAccess = Whitebox.invokeMethod(interceptor, "manageAccess", request, response, token);
		
		assertEquals(false, manageAccess);
	}	
	
}
