package evertec.cibp.web.interceptor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.interfaces.CustomerProfileService;
import evertec.cibp.core.services.requests.TransactionRequest;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@SuppressWarnings("serial")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ESAPI.class, Utils.class, WebUtils.class, StaticMessageSource.class, CustomerServices.class, 
	CustomerProfile.class, CustomerProfileService.class, TVFrontendAccount.class,  BankingSession.class,
	TransactionsResponse.class, CustomerFacade.class, TransactionRequest.class,
	TVAccountProductDefinition.class})
public class LayoutInteractionUT extends AbstractModelAndViewTests{
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	
	@Spy
	private List<String> views = new LinkedList<String>();
	
	@Mock
	private ModelAndView modelAndView;
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private Object handler;
	
	@Mock
	private ModelMap modelMap;
	
	@Mock
	private HTTPUtilities httpUtilities;
	
	@InjectMocks
	private LayoutInterceptor interceptor;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ESAPI.class);
		PowerMockito.mockStatic(WebUtils.class);
	    request = mock(HttpServletRequest.class);
	    response = mock(HttpServletResponse.class);
		httpUtilities = mock(HTTPUtilities.class);
	    PowerMockito.mockStatic(StaticMessageSource.class);
	    PowerMockito.mockStatic(WebUtils.class);
	    when(request.getContextPath()).thenReturn("http://localhost:8086/cibp-web");
	    when(WebUtils.getSessionAttribute(any(), eq("userSession"))).thenReturn(userSession);
	    when(userSession.getCustomerProfile()).thenReturn(customerProfile);
	    when(userSession.getAccountMLA()).thenReturn("12345001001");
	}
	
	@Test
	public void whenPostHandle_GivenMBSE_2513FlagFalse() throws Exception {
		
		ModelMap testMap = new ModelMap() {{
			put("selectedAccount", "MLA");
		}};
		when(modelAndView.getViewName()).thenReturn("transaction");
		when(modelAndView.getModelMap()).thenReturn(testMap);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513)).thenReturn(Boolean.FALSE);
		interceptor.postHandle(request, response, handler, modelAndView);
		assertFalse(views.contains("detail"));
	}
	
	@Test
	public void whenPostHandle_GivenMBSE_2513FlagTrue_Agent_JSON() throws Exception {
		
		ModelMap testMap = new ModelMap() {{
			put("selectedAccount", "MLA");
		}};
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
		when(modelAndView.getViewName()).thenReturn("transaction");
		when(modelAndView.getModelMap()).thenReturn(testMap);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513)).thenReturn(Boolean.TRUE);
		interceptor.postHandle(request, response, handler, modelAndView);
		assertFalse(views.contains("detail"));
	}
	
	@Test
	public void whenPostHandle_GivenMBSE_2513FlagTrue_Agent_Web() throws Exception {
		
		ModelMap testMap = new ModelMap() {{
			put("selectedAccount", "MLA");
		}};
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
		when(modelAndView.getViewName()).thenReturn("transaction");
		when(modelAndView.getModelMap()).thenReturn(testMap);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513)).thenReturn(Boolean.TRUE);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		when(request.getContextPath()).thenReturn("http://localhost:8086/cibp-web");
		doNothing().when(httpUtilities).sendRedirect(response, "http://localhost:8086/cibp-web/actions/detail?account=12345001001");
		interceptor.postHandle(request, response, handler, modelAndView);
		verify(httpUtilities, times(1)).sendRedirect(response, "http://localhost:8086/cibp-web/actions/detail?account=12345001001&tabNum=0&trxTabNum=0");
	}
	
	@Test
	public void whenPostHandle_GivenUserSessionNull_ThenContinueRedirect() throws Exception {
		
		ModelMap testMap = new ModelMap() {{
			put("selectedAccount", "MLA");
		}};
		when(WebUtils.getSessionAttribute(any(), eq("userSession"))).thenReturn(null);
		when(modelAndView.getViewName()).thenReturn("transaction");
		when(modelAndView.getModelMap()).thenReturn(testMap);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513)).thenReturn(Boolean.TRUE);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		when(request.getContextPath()).thenReturn("http://localhost:8086/cibp-web");
		doNothing().when(httpUtilities).sendRedirect(response, "http://localhost:8086/cibp-web/actions/detail?account=12345001001");
		interceptor.postHandle(request, response, handler, modelAndView);
		verify(httpUtilities, times(0)).sendRedirect(response, "http://localhost:8086/cibp-web/actions/detail?account=12345001001&tabNum=0&trxTabNum=0");
	}
	
	@Test
	public void whenPostHandle_GivenInjectionScript_ThenContinueRedirect() throws Exception {
		final String url = "http://localhost:8086/cibp-web/actions/checkshistoryreport?account=none&dateFrom=11%2f01%2f2019\"><script>alert(1)</script>&dateTo=03%2F13%2F2020";
		
		ModelMap model = new ModelMap() {{
			put("value", url);
		}};
		when(request.getContextPath()).thenReturn("http://localhost:8086/cibp-web");
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		modelAndView.addObject("model", url);
		when(modelAndView.getModel()).thenReturn(model);
		when(modelAndView.getViewName()).thenReturn("checkshistoryreport");
		doNothing().when(httpUtilities).sendRedirect(response, url);
		interceptor.postHandle(request, response, handler, modelAndView);
		verify(ESAPI.httpUtilities(), times(1)).sendRedirect(response, "http://localhost:8086/cibp-web/actions/portal");
	}
	
	@Test
	public void whenPostHandle_GivenMBSE_2513FlagTrue_Agent_Null() throws Exception {
		
		ModelMap testMap = new ModelMap() {{
			put("selectedAccount", "MLA");
		}};
		when(userSession.getUserAgentType()).thenReturn(null);
		when(modelAndView.getViewName()).thenReturn("transaction");
		when(modelAndView.getModelMap()).thenReturn(testMap);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSE2513)).thenReturn(Boolean.TRUE);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		when(request.getContextPath()).thenReturn("http://localhost:8086/cibp-web");
		doNothing().when(httpUtilities).sendRedirect(response, "http://localhost:8086/cibp-web/actions/detail?account=12345001001");
		interceptor.postHandle(request, response, handler, modelAndView);
		verify(httpUtilities, times(0)).sendRedirect(response, "http://localhost:8086/cibp-web/actions/detail?account=12345001001&tabNum=0&trxTabNum=0");
	}
	
}
