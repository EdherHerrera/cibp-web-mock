package evertec.cibp.web.interceptor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

import evertec.cibp.core.utils.StaticMessageSource;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ StaticMessageSource.class, ESAPI.class })
public class RegainAccessFlagInterceptorUT {
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@Mock 
	private HTTPUtilities mHTTPUtilities;
	
	@Mock
	private ModelAndView modelAndView;
	
	@Mock
	private Object handler;
	
	@InjectMocks
	private RegainAccessFlagInterceptor interceptor;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	    request = new MockHttpServletRequest();
	    response = new MockHttpServletResponse();	    
	    PowerMockito.mockStatic(StaticMessageSource.class);
	    PowerMockito.mockStatic(ESAPI.class);
	}
	
	@Test
	public void whenPreHandle_GivenMBFC_REGAINACCESFlagFalse() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC224)).thenReturn(Boolean.FALSE);
		when(ESAPI.httpUtilities()).thenReturn(mHTTPUtilities);
		doNothing().when(mHTTPUtilities).sendRedirect(any(), any());
		
		boolean result = interceptor.preHandle(request, response, handler);
		
		assertFalse(result);
	}
	
	@Test
	public void whenPreHandle_GivenMBFC_REGAINACCESFlagTrue() throws Exception {
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFC224)).thenReturn(Boolean.TRUE);
		assertTrue(interceptor.preHandle(request, response, handler));
	}
	
}
