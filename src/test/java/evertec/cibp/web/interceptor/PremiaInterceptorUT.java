/**
 * 
 */
package evertec.cibp.web.interceptor;

import static org.mockito.Mockito.*;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.web.session.UserSession;

/**
 * Test class for <code>PremiaInterceptor</code>.
 * @author Ariel D. Salazar H.
 * @created 2020-04-07 00:00
 * @since 1.0
 * @version 1.8
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ESAPI.class, WebUtils.class})
public class PremiaInterceptorUT {
	
	@InjectMocks
	private PremiaInterceptor interceptor;
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	
	@Mock
	private ModelAndView modelAndView;
	
	@Mock
	private Object handler;
	
	@Mock
	private HTTPUtilities httpUtilities;
	
	@Mock
	private CustomerProfile customerProfile;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(ESAPI.class);
		PowerMockito.mockStatic(WebUtils.class);
		
		interceptor = new PremiaInterceptor();
		httpUtilities = mock(HTTPUtilities.class);
		
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(request.getContextPath()).thenReturn("http://192.168.37.107:5151/cibp-web");
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		interceptor = null;
	}

	/**
	 * Test method for {@link evertec.cibp.web.interceptor.PremiaInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)}.
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPostHandle_GivenSelectedAccountWithPRMProductTypeAndModelViewTransaction_ThenExecuteRedirectionToPortal() throws Exception {
		// mock
		@SuppressWarnings("serial")
		ModelMap testMap = new ModelMap() {{
			put("selectedAccount", "PRM");
		}};
		
		when(modelAndView.getViewName()).thenReturn("transaction");
		when(modelAndView.getModelMap()).thenReturn(testMap);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		when(request.getContextPath()).thenReturn("http://192.168.37.107:5151/cibp-web");
		doNothing().when(httpUtilities).sendRedirect(response, "http://192.168.37.107:5151/cibp-web/actions/portal");
		// test
		interceptor.postHandle(request, response, handler, modelAndView);
		//verify
		verify(modelAndView, times(2)).getModelMap();
		verify(httpUtilities, times(1)).sendRedirect(response, "http://192.168.37.107:5151/cibp-web/actions/portal");		
	}
	
	/**
	 * Test method for {@link evertec.cibp.web.interceptor.PremiaInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)}.
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPostHandle_GivenSelectedAccountWithoutPRMProductTypeAndModelViewTransaction_ThenNoExecuteRedirectionToPortal() throws Exception {
		// mock
		@SuppressWarnings("serial")
		ModelMap testMap = new ModelMap() {{
			put("selectedAccount", "CCA");
		}};
		
		when(modelAndView.getViewName()).thenReturn("transaction");
		when(modelAndView.getModelMap()).thenReturn(testMap);
		when(request.getContextPath()).thenReturn("http://192.168.37.107:5151/cibp-web");
		doNothing().when(response).sendRedirect("http://192.168.37.107:5151/cibp-web/actions/portal");
		// test
		interceptor.postHandle(request, response, handler, modelAndView);
		//verify
		verify(modelAndView, times(2)).getModelMap();
		verify(httpUtilities, times(0)).sendRedirect(response, "http://192.168.37.107:5151/cibp-web/actions/portal");		
	}

	/**
	 * Test method for {@link evertec.cibp.web.interceptor.PremiaInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)}.
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPostHandle_GivenSelectedAccountWithPRMProductTypeAndModelViewDetail_ThenExecuteRedirectionToPortal() throws Exception {
		// mock
		@SuppressWarnings("serial")
		ModelMap testMap = new ModelMap() {{
			put("premiacycle", "PRM");
		}};
		
		when(modelAndView.getViewName()).thenReturn("detail");
		when(modelAndView.getModelMap()).thenReturn(testMap);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		when(request.getContextPath()).thenReturn("http://192.168.37.107:5151/cibp-web");
		doNothing().when(httpUtilities).sendRedirect(response, "http://192.168.37.107:5151/cibp-web/actions/portal");
		// test
		interceptor.postHandle(request, response, handler, modelAndView);
		//verify
		verify(modelAndView, times(2)).getModelMap();
		verify(httpUtilities, times(1)).sendRedirect(response, "http://192.168.37.107:5151/cibp-web/actions/portal");		
	}
	
	/**
	 * Test method for {@link evertec.cibp.web.interceptor.PremiaInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)}.
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPostHandle_GivenSelectedAccountWithoutPRMProductTypeAndModelViewDetail_ThenExecuteRedirectionToPortal() throws Exception {
		// mock
		@SuppressWarnings("serial")
		ModelMap testMap = new ModelMap() {};
		
		when(modelAndView.getViewName()).thenReturn("detail");
		when(modelAndView.getModelMap()).thenReturn(testMap);
		//when(request.getContextPath()).thenReturn("http://192.168.37.107:5151/cibp-web");
		doNothing().when(response).sendRedirect("http://192.168.37.107:5151/cibp-web/actions/portal");
		// test
		interceptor.postHandle(request, response, handler, modelAndView);
		//verify
		verify(modelAndView, times(2)).getModelMap();
		verify(httpUtilities, times(0)).sendRedirect(response, "http://192.168.37.107:5151/cibp-web/actions/portal");		
	}
	
	/**
	 * Test method for {@link evertec.cibp.web.interceptor.PremiaInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)}.
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPostHandle_GivenModelNameTransaction_ThenRemovePremiaFromDropDownList() throws Exception {
		
		@SuppressWarnings("unchecked")
		Map<String, String> stmtAccountsMap = mock(LinkedHashMap.class);
		Map<String, TVFrontendAccount> stmtAccounts = new LinkedHashMap<String, TVFrontendAccount>();
		
		TVFrontendAccount account = mock(TVFrontendAccount.class);
		TVAccountProductDefinition productDefinition = mock(TVAccountProductDefinition.class);
		
		when(account.getProductDefinition()).thenReturn(productDefinition);
		when(productDefinition.getProductType()).thenReturn("PRM");
		
		stmtAccountsMap.put("1", "PREMIA PRODUCT");
		stmtAccounts.put("1", account);
		
		@SuppressWarnings("serial")
		ModelMap testMap = new ModelMap() {{
			put("stmtAccountsMap", stmtAccountsMap);
			put("stmtAccounts", stmtAccounts);
		}};
		
		when(modelAndView.getViewName()).thenReturn("transaction");
		
		when(modelAndView.getModelMap()).thenReturn(testMap);
		
		when(request.getContextPath()).thenReturn("http://192.168.37.107:5151/cibp-web");
		//doNothing().when(response).sendRedirect("http://192.168.37.107:5151/cibp-web/actions/portal");
		// test
		interceptor.postHandle(request, response, handler, modelAndView);
		//verify
		verify(stmtAccountsMap, times(1)).remove("1");
	}
	
	/**
	 * Test method for {@link evertec.cibp.web.interceptor.PremiaInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)}.
	 * @throws Exception 
	 */
	@Test
	public void whenPostHandle_GivenModelNameTransactionAndNoPremiaProducto_ThenNotRemovePremiaFromDropDownList() throws Exception {
		
		@SuppressWarnings("unchecked")
		Map<String, String> stmtAccountsMap = mock(LinkedHashMap.class);
		Map<String, TVFrontendAccount> stmtAccounts = new LinkedHashMap<String, TVFrontendAccount>();
		
		TVFrontendAccount account = mock(TVFrontendAccount.class);
		TVAccountProductDefinition productDefinition = mock(TVAccountProductDefinition.class);
		
		when(account.getProductDefinition()).thenReturn(productDefinition);
		when(productDefinition.getProductType()).thenReturn("CCA");
		
		stmtAccountsMap.put("1", "PREMIA PRODUCT");
		stmtAccounts.put("1", account);
		
		@SuppressWarnings("serial")
		ModelMap testMap = new ModelMap() {{
			put("stmtAccountsMap", stmtAccountsMap);
			put("stmtAccounts", stmtAccounts);
		}};
		
		when(modelAndView.getViewName()).thenReturn("transaction");
		
		when(modelAndView.getModelMap()).thenReturn(testMap);
		
		when(request.getContextPath()).thenReturn("http://192.168.37.107:5151/cibp-web");
		doNothing().when(response).sendRedirect("http://192.168.37.107:5151/cibp-web/actions/portal");
		// test
		interceptor.postHandle(request, response, handler, modelAndView);
		//verify
		verify(stmtAccountsMap, times(0)).remove("1");
	}
	
	/**
	 * Test method for {@link evertec.cibp.web.interceptor.PremiaInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)}.
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPostHandle_GivenModelNameDetail_ThenRemovePremiaFromDropDownList() throws Exception {
		
		@SuppressWarnings("unchecked")
		Map<String, String> detailAccounts = mock(LinkedHashMap.class);
		Map<String, TVFrontendAccount> detailAccountsMap = new LinkedHashMap<String, TVFrontendAccount>();
		
		TVFrontendAccount account = mock(TVFrontendAccount.class);
		TVAccountProductDefinition productDefinition = mock(TVAccountProductDefinition.class);
		
		when(account.getProductDefinition()).thenReturn(productDefinition);
		when(productDefinition.getProductType()).thenReturn("PRM");
		
		detailAccounts.put("1", "PREMIA PRODUCT");
		detailAccountsMap.put("1", account);
		
		@SuppressWarnings("serial")
		ModelMap testMap = new ModelMap() {{
			put("detailAccounts", detailAccounts);
			put("detailAccountsMap", detailAccountsMap);
		}};
		
		when(modelAndView.getViewName()).thenReturn("detail");
		
		when(modelAndView.getModelMap()).thenReturn(testMap);
		
		when(request.getContextPath()).thenReturn("http://192.168.37.107:5151/cibp-web");
		//doNothing().when(response).sendRedirect("http://192.168.37.107:5151/cibp-web/actions/portal");
		// test
		interceptor.postHandle(request, response, handler, modelAndView);
		//verify
		verify(detailAccounts, times(1)).remove("1");
	}
	
	/**
	 * Test method for {@link evertec.cibp.web.interceptor.PremiaInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)}.
	 * @throws Exception 
	 */
	@Test
	public void whenPostHandle_GivenModelNameDetailAndNoPremiaProducto_ThenNotRemovePremiaFromDropDownList() throws Exception {
		
		@SuppressWarnings("unchecked")
		Map<String, String> detailAccounts = mock(LinkedHashMap.class);
		Map<String, TVFrontendAccount> detailAccountsMap = new LinkedHashMap<String, TVFrontendAccount>();
		
		TVFrontendAccount account = mock(TVFrontendAccount.class);
		TVAccountProductDefinition productDefinition = mock(TVAccountProductDefinition.class);
		
		when(account.getProductDefinition()).thenReturn(productDefinition);
		when(productDefinition.getProductType()).thenReturn("CCA");
		
		detailAccounts.put("1", "PREMIA PRODUCT");
		detailAccountsMap.put("1", account);
		
		@SuppressWarnings("serial")
		ModelMap testMap = new ModelMap() {{
			put("detailAccounts", detailAccounts);
			put("detailAccountsMap", detailAccountsMap);
		}};
		
		when(modelAndView.getViewName()).thenReturn("detail");
		
		when(modelAndView.getModelMap()).thenReturn(testMap);
		
		when(request.getContextPath()).thenReturn("http://192.168.37.107:5151/cibp-web");
		doNothing().when(response).sendRedirect("http://192.168.37.107:5151/cibp-web/actions/portal");
		// test
		interceptor.postHandle(request, response, handler, modelAndView);
		//verify
		verify(detailAccounts, times(0)).remove("1");
	}
	
	/**
	 * Test method for {@link evertec.cibp.web.interceptor.PremiaInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)}.
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPostHandle_GivenModelNameTransactionAndGhostAccount_ThenRemoveGhostAccountFromDropDownList() throws Exception {
		
		@SuppressWarnings("unchecked")
		Map<String, String> stmtAccountsMap = mock(LinkedHashMap.class);
		Map<String, TVFrontendAccount> stmtAccounts = new LinkedHashMap<String, TVFrontendAccount>();
		
		TVFrontendAccount account = mock(TVFrontendAccount.class);
		TVAccountProductDefinition productDefinition = mock(TVAccountProductDefinition.class);
		
		when(account.getProductDefinition()).thenReturn(productDefinition);
		when(productDefinition.getProductId()).thenReturn("PLNOC");
		
		stmtAccountsMap.put("1", "PREMIA GHOST ACCOUNT");
		stmtAccounts.put("1", account);
		
		@SuppressWarnings("serial")
		ModelMap testMap = new ModelMap() {{
			put("stmtAccountsMap", stmtAccountsMap);
			put("stmtAccounts", stmtAccounts);
		}};
		
		when(modelAndView.getViewName()).thenReturn("transaction");
		
		when(modelAndView.getModelMap()).thenReturn(testMap);
		
		when(request.getContextPath()).thenReturn("http://192.168.37.107:5151/cibp-web");

		interceptor.postHandle(request, response, handler, modelAndView);
		//verify
		verify(stmtAccountsMap, times(1)).remove("1");
	}
	
	/**
	 * Test method for {@link evertec.cibp.web.interceptor.PremiaInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)}.
	 * @throws Exception 
	 */
	@Test
	public void whenPostHandle_GivenModelNameTransactionAndNoGhostAccount_ThenNotRemoveAccountFromDropDownList() throws Exception {
		
		@SuppressWarnings("unchecked")
		Map<String, String> stmtAccountsMap = mock(LinkedHashMap.class);
		Map<String, TVFrontendAccount> stmtAccounts = new LinkedHashMap<String, TVFrontendAccount>();
		
		TVFrontendAccount account = mock(TVFrontendAccount.class);
		TVAccountProductDefinition productDefinition = mock(TVAccountProductDefinition.class);
		
		when(account.getProductDefinition()).thenReturn(productDefinition);
		when(productDefinition.getProductId()).thenReturn("081");
		
		stmtAccountsMap.put("1", "NOT A GHOST ACCOUNT");
		stmtAccounts.put("1", account);
		
		@SuppressWarnings("serial")
		ModelMap testMap = new ModelMap() {{
			put("stmtAccountsMap", stmtAccountsMap);
			put("stmtAccounts", stmtAccounts);
		}};
		
		when(modelAndView.getViewName()).thenReturn("transaction");
		
		when(modelAndView.getModelMap()).thenReturn(testMap);
		
		when(request.getContextPath()).thenReturn("http://192.168.37.107:5151/cibp-web");
		doNothing().when(response).sendRedirect("http://192.168.37.107:5151/cibp-web/actions/portal");
		
		// test
		interceptor.postHandle(request, response, handler, modelAndView);
		//verify
		verify(stmtAccountsMap, times(0)).remove("1");
	}
	
	/**
	 * Test method for {@link evertec.cibp.web.interceptor.PremiaInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)}.
	 * @throws Exception 
	 */
	@Test
	@Ignore
	public void whenPostHandle_GivenModelNameDetailAndGhostAccount_ThenRemoveGhostAccountFromDropDownList() throws Exception {
		
		@SuppressWarnings("unchecked")
		Map<String, String> detailAccounts = mock(LinkedHashMap.class);
		Map<String, TVFrontendAccount> detailAccountsMap = new LinkedHashMap<String, TVFrontendAccount>();
		
		TVFrontendAccount account = mock(TVFrontendAccount.class);
		TVAccountProductDefinition productDefinition = mock(TVAccountProductDefinition.class);
		
		when(account.getProductDefinition()).thenReturn(productDefinition);
		when(productDefinition.getProductId()).thenReturn("PLNOC");
		
		detailAccounts.put("1", "GHOST ACCOUNT");
		detailAccountsMap.put("1", account);
		
		@SuppressWarnings("serial")
		ModelMap testMap = new ModelMap() {{
			put("detailAccounts", detailAccounts);
			put("detailAccountsMap", detailAccountsMap);
		}};
		
		when(modelAndView.getViewName()).thenReturn("detail");
		
		when(modelAndView.getModelMap()).thenReturn(testMap);
		
		when(request.getContextPath()).thenReturn("http://192.168.37.107:5151/cibp-web");

		// test
		interceptor.postHandle(request, response, handler, modelAndView);
		//verify
		verify(detailAccounts, times(1)).remove("1");
	}
	
	/**
	 * Test method for {@link evertec.cibp.web.interceptor.PremiaInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)}.
	 * @throws Exception 
	 */
	@Test
	public void whenPostHandle_GivenModelNameDetailAndNoGhostAccount_ThenNotRemoveAccountFromDropDownList() throws Exception {
		
		@SuppressWarnings("unchecked")
		Map<String, String> detailAccounts = mock(LinkedHashMap.class);
		Map<String, TVFrontendAccount> detailAccountsMap = new LinkedHashMap<String, TVFrontendAccount>();
		
		TVFrontendAccount account = mock(TVFrontendAccount.class);
		TVAccountProductDefinition productDefinition = mock(TVAccountProductDefinition.class);
		
		when(account.getProductDefinition()).thenReturn(productDefinition);
		when(productDefinition.getProductId()).thenReturn("081");
		
		detailAccounts.put("1", "NOT A GHOST ACCOUNT");
		detailAccountsMap.put("1", account);
		
		@SuppressWarnings("serial")
		ModelMap testMap = new ModelMap() {{
			put("detailAccounts", detailAccounts);
			put("detailAccountsMap", detailAccountsMap);
		}};
		
		when(modelAndView.getViewName()).thenReturn("detail");
		
		when(modelAndView.getModelMap()).thenReturn(testMap);
		
		when(request.getContextPath()).thenReturn("http://192.168.37.107:5151/cibp-web");
		doNothing().when(response).sendRedirect("http://192.168.37.107:5151/cibp-web/actions/portal");
		
		// test
		interceptor.postHandle(request, response, handler, modelAndView);
		//verify
		verify(detailAccounts, times(0)).remove("1");
	}
	
}
