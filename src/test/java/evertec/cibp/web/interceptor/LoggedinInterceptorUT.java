package evertec.cibp.web.interceptor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.util.WebUtils;

import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.session.UserSession;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ WebUtils.class, ESAPI.class })
public class LoggedinInterceptorUT {
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private HTTPUtilities httpUtilities;
	
	@Mock
	private Object handler;
	
	@InjectMocks
	private LoggedinInterceptor loggedinInterceptor;
	
	@Before
	public void setUp() throws Exception {
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(ESAPI.class);
	}

	@Test
	public void whenpreHandler_givenGetIsLoginInterruptionEnableAndInterruptionRequest_thenUserContinue() throws Exception {
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.isLoginCompleteSuccess()).thenReturn(true);
		when(userSession.getIsLoginInterruptionEnable()).thenReturn(true);
		request.setPathInfo("/oobInterruptionPage");
	
		boolean responseValue = loggedinInterceptor.preHandle(request, response, handler);
		
		assertTrue(responseValue);
	}
	
	@Test
	public void whenpreHandler_givenGetIsLoginInterruptionEnableAndPortalRequest_thenRedirectToInterruption() throws Exception {
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.isLoginCompleteSuccess()).thenReturn(true);
		when(userSession.getIsLoginInterruptionEnable()).thenReturn(true);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		request.setPathInfo("/portal");
	
		boolean responseValue = loggedinInterceptor.preHandle(request, response, handler);
		
		assertFalse(responseValue);
		verify(httpUtilities, times(1)).sendRedirect(response, "/actions/loginInterruptionPage");
	}
	
	@Test
	public void whenpreHandler_givenGetIsLoginInterruptionDisableAndInterruptionRequest_thenUserContinue() throws Exception {
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.isLoginCompleteSuccess()).thenReturn(true);
		when(userSession.getIsLoginInterruptionEnable()).thenReturn(false);
		request.setPathInfo("/portal");
	
		boolean responseValue = loggedinInterceptor.preHandle(request, response, handler);
		
		assertTrue(responseValue);
	}
	
	@Test
	public void whenpreHandler_givenGetIsLoginInterruptionEnableAndPortalRequestWithJsonAgent_thenContiue() throws Exception {
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.isLoginCompleteSuccess()).thenReturn(true);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
		when(userSession.getIsLoginInterruptionEnable()).thenReturn(true);
		when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
		request.setPathInfo("/portal");
	
		boolean responseValue = loggedinInterceptor.preHandle(request, response, handler);
		
		assertTrue(responseValue);
		verify(httpUtilities, times(0)).sendRedirect(response, "/actions/loginInterruptionPage");
	}

}
