/**
 * 
 */
package evertec.cibp.web.premia.control;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.evertec.cibp.api.client.model.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.evertec.cibp.api.client.model.MicroServiceResponse;

import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.facade.CreditCardFacade;
import evertec.cibp.web.mapper.CreditCardModelMapperFactory;
import evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse;
import evertec.cibp.web.session.UserSession;

/**
 * @author s681718
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ StaticMessageSource.class, CreditCardFacade.class, UserSession.class, CreditCardModelMapperFactory.class })
public class PremiaUtilUT {

	@Mock
	private CreditCardFacade creditCardFacade;
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private List<TVFrontendAccount> accList;
	
	/**
	 * Number Premia Products
	 */
	private static final int NUM_PREMIA_PRODUCTS = 50; //
	
	@Mock
	private TsysLoyaltyRewardsAccountInfoResponse tsysLoyaltyRewardsAccountInfoResponse;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(CreditCardFacade.class);
		PowerMockito.mockStatic(CreditCardModelMapperFactory.class);
		when(userSession.getCustomerService()).thenReturn(customerService);
	}
	
	@Test
	public void whenPremiaSelectFrontEndAccountAmexZeroUAREG_ThenReturnAccount() {

		TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("CCA", "0349548381926524", "R"));
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription", 
				"dummyAccountNumberDescription", 
				"dummyAccountNumber", 
				"UAREG", 
				"CCA", 
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		when(acc.getAccountProductId()).thenReturn("UAREG");
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(acc);
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);
		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceResponse = new MicroServiceResponse<>();
		microServiceResponse.setData(response);
		when(creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(any(), any(), any(), eq(false))).thenReturn(microServiceResponse);
		
		List<TVFrontendAccount> lst = PremiaUtil.premiaSelectFrontEndAccount(userSession, acc, getPremiaProducts());
		assertEquals(false, lst.isEmpty());
		assertNotNull(lst.isEmpty());
	}
	
	@Test
	public void whenPremiaSelectFrontEndAccountAmexZero_ThenReturnAccount() {

		TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("CCA", "214589256348315", "R"));
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription", 
				"dummyAccountNumberDescription", 
				"dummyAccountNumber", 
				"AREG", 
				"CCA", 
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		when(acc.getAccountProductId()).thenReturn("AREG");
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(acc);
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);
		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceResponse = new MicroServiceResponse<>();
		microServiceResponse.setData(response);
		when(creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(any(), any(), any(), eq(false))).thenReturn(microServiceResponse);
		
		List<TVFrontendAccount> lst = PremiaUtil.premiaSelectFrontEndAccount(userSession, acc, getPremiaProducts());
		assertEquals(false, lst.isEmpty());
		assertNotNull(lst.isEmpty());
	}
	
	@Test
	public void whenPremiaSelectFrontEndAccountCCAInvalid_ThenReturnAccount() {

		TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("CCA", "0349", "R"));
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription", 
				"dummyAccountNumberDescription", 
				"dummyAccountNumber", 
				"UAREG", 
				"CCA", 
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		when(acc.getAccountProductId()).thenReturn("UAREG");
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(acc);
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);
		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceResponse = new MicroServiceResponse<>();
		microServiceResponse.setData(response);
		when(creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(any(), any(), any(), eq(false))).thenReturn(microServiceResponse);
		
		List<TVFrontendAccount> lst = PremiaUtil.premiaSelectFrontEndAccount(userSession, acc, getPremiaProducts());
		assertEquals(false, lst.isEmpty());
		assertNotNull(lst.isEmpty());
	}
	
	@Test
	public void whenPremiaSelectFrontEndAccountCCCVPPRE_ThenReturnAccount() {

		TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("CCA", "0349548381926524", "R"));
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription", 
				"dummyAccountNumberDescription", 
				"dummyAccountNumber", 
				"VPPRE", 
				"CCA", 
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		when(acc.getAccountProductId()).thenReturn("VPPRE");
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(acc);
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);
		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceResponse = new MicroServiceResponse<>();
		microServiceResponse.setData(response);
		when(creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(any(), any(), any(), eq(false))).thenReturn(microServiceResponse);
		
		List<TVFrontendAccount> lst = PremiaUtil.premiaSelectFrontEndAccount(userSession, acc, getPremiaProducts());
		assertEquals(false, lst.isEmpty());
		assertNotNull(lst.isEmpty());
	}
	
	@Test
	public void whenPremiaSelectFrontEndAccount_ThenReturnAccount() {

		TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("CCA", "4549548381926524", "R"));
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription", 
				"dummyAccountNumberDescription", 
				"dummyAccountNumber", 
				"VPPRE", 
				"CCA", 
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		when(acc.getAccountProductId()).thenReturn("VPPRE");
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(acc);
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);
		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceResponse = new MicroServiceResponse<>();
		microServiceResponse.setData(response);
		when(creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(any(), any(), any(), eq(false))).thenReturn(microServiceResponse);
		
		List<TVFrontendAccount> lst = PremiaUtil.premiaSelectFrontEndAccount(userSession, acc, getPremiaProducts());
		assertEquals(false, lst.isEmpty());
		assertNotNull(lst.isEmpty());
	}
	
	@Test
	public void whenPremiaSelectFrontEndAccountWithCommercialProfile_ThenReturnAccount() {

		TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("CCA", "4549257000041901", "R"));
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription", 
				"dummyAccountNumberDescription", 
				"dummyAccountNumber", 
				"VBSG1", 
				"CCA", 
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		acc.setPremiaCommercialAccount(true);
		when(acc.getAccountProductId()).thenReturn("VBSG1");		
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(acc);
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);
		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceResponse = new MicroServiceResponse<>();
		microServiceResponse.setData(response);
		when(creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(any(), any(), any(), eq(true))).thenReturn(microServiceResponse);
		
		List<TVFrontendAccount> lst = PremiaUtil.premiaSelectFrontEndAccount(userSession, acc, getPremiaProducts());
		assertEquals(false, lst.isEmpty());
	}	
	
	@Test
	public void whenPremiaSelectFrontEndAccount_ThenReturnNull() {

		List<TVFrontendAccount> lst = PremiaUtil.premiaSelectFrontEndAccount(userSession, null, getPremiaProducts());
		assertEquals(false, lst.isEmpty());
		assertNull(lst.get(0));
	}

	@Test
	public void whenPremiaSelectFrontEndAccount_ThenServiceNotResponse() {

		TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("CCA", "4549548381926524", "R"));
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription",
				"dummyAccountNumberDescription",
				"dummyAccountNumber",
				"VPPRE",
				"CCA",
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		when(acc.getAccountProductId()).thenReturn("VPPRE");
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(acc);

		when(creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(any(), any(), any(), eq(false))).thenReturn(null);


		List<TVFrontendAccount> lst = PremiaUtil.premiaSelectFrontEndAccount(userSession, acc, getPremiaProducts());
		assertEquals(false, lst.isEmpty());
		assertNotNull(lst.get(0));
		assertNull(lst.get(0).getTsysLoyaltyRewardsInfo());
	}
	
	@Test
	public void whenPremiaSelectFrontEndAccountWithCommercialProfile_ThenServiceNotResponse() {

		TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("CCA", "4549548381926524", "R"));
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription",
				"dummyAccountNumberDescription",
				"dummyAccountNumber",
				"VBVBR",
				"CCA",
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		acc.setPremiaCommercialAccount(true);
		when(acc.getAccountProductId()).thenReturn("VBVBR");
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(acc);

		when(creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(any(), any(), any(), eq(true))).thenReturn(null);


		List<TVFrontendAccount> lst = PremiaUtil.premiaSelectFrontEndAccount(userSession, acc, getPremiaProducts());
		assertEquals(false, lst.isEmpty());
		assertNotNull(lst.get(0));
		assertNull(lst.get(0).getTsysLoyaltyRewardsInfo());
	}	
	
	

	@Test
	public void whenPremiaSelectFrontEndAccount_ThenServiceReturnError() {

		TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("CCA", "4549548381926524", "R"));
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription",
				"dummyAccountNumberDescription",
				"dummyAccountNumber",
				"VPPRE",
				"CCA",
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		when(acc.getAccountProductId()).thenReturn("VPPRE");
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(acc);
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceResponse = new MicroServiceResponse<>();
		microServiceResponse.setData(response);
		Error error = new Error();
		error.setCode("001");
		error.setDetail("Not Found");
		ArrayList<Error> errors = new ArrayList<>();
		errors.add(error);
		microServiceResponse.setErrors(errors);
		when(creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(any(), any(), any(), eq(false))).thenReturn(microServiceResponse);


		List<TVFrontendAccount> lst = PremiaUtil.premiaSelectFrontEndAccount(userSession, acc, getPremiaProducts());
		assertEquals(false, lst.isEmpty());
		assertNotNull(lst.get(0));
		assertNull(lst.get(0).getTsysLoyaltyRewardsInfo());
	}
	
	@Test
	public void whenPremiaSelectFrontEndAccountWithCommercialProfile_ThenServiceReturnError() {

		TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("CCA", "4549548381926524", "R"));
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription",
				"dummyAccountNumberDescription",
				"dummyAccountNumber",
				"VPPRE",
				"CCA",
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		when(acc.getAccountProductId()).thenReturn("VPPRE");
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);
		when(CreditCardFacade.getInstance()).thenReturn(creditCardFacade);
		when(customerService.getPriorityPremiaAccount(any())).thenReturn(acc);
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceResponse = new MicroServiceResponse<>();
		microServiceResponse.setData(response);
		Error error = new Error();
		error.setCode("001");
		error.setDetail("Not Found");
		ArrayList<Error> errors = new ArrayList<>();
		errors.add(error);
		microServiceResponse.setErrors(errors);
		when(creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(any(), any(), any(), eq(true))).thenReturn(microServiceResponse);


		List<TVFrontendAccount> lst = PremiaUtil.premiaSelectFrontEndAccount(userSession, acc, getPremiaProducts());
		assertEquals(false, lst.isEmpty());
		assertNotNull(lst.get(0));
		assertNull(lst.get(0).getTsysLoyaltyRewardsInfo());
	}	

	@Test
	public void whenHasPremiaProductsByCustomerCodes_GivenCustomerList_ThenReturnTrue() {
		assertTrue(PremiaUtil.hasPremiaProductsByCustomerCodes(getCustomerCodesList(), getPremiaCustomerCodesList()));
	}

	@Test
	public void whenHasPremiaProductsByCustomerCodes_GivenCustomerWithoutPremiaList_ThenReturnFalse() {
		assertFalse(PremiaUtil.hasPremiaProductsByCustomerCodes(getCustomerCodesWithoutPremiaList(), getPremiaCustomerCodesList()));
	}

	@Test
	public void whenHasPremiaProductsByCustomerCodes_GivenCustomerEmptyList_ThenReturnFalse() {
		assertFalse(PremiaUtil.hasPremiaProductsByCustomerCodes(getCustomerCodesEmptyList(), getPremiaCustomerCodesList()));
	}

	@Test
	public void whenFilterPremiaAccounts_GivenAccountAndGhostProductId_ThenReturnList() {

		List<TVFrontendAccount> premiaAccounts = new LinkedList<>();

		TVFrontendAccount account = mock(TVFrontendAccount.class);
		TVAccountProductDefinition  productDefinition = mock(TVAccountProductDefinition.class);
		when(account.getProductDefinition()).thenReturn(productDefinition);
		when(productDefinition.getProductType()).thenReturn("CCA");
		when(productDefinition.getProductStatus()).thenReturn("UNDEFINED");
		when(productDefinition.getProductId()).thenReturn("PLNOC");
		premiaAccounts.add(account);

		assertNotNull(PremiaUtil.filterPremiaAccounts(premiaAccounts, "PLNOC"));
	}

	@Test
	public void whenFilterPremiaAccounts_GivenEmptyList_ThenReturnSameList() {

		List<TVFrontendAccount> premiaAccounts = new LinkedList<>();

		assertNotNull(PremiaUtil.filterPremiaAccounts(premiaAccounts, ""));
	}
	
	/**
	 * 
	 * @return premiaCCAProducts
	 */
	private List<String> getPremiaProducts () {
		final List<String> premiaProducts = new ArrayList<String>(NUM_PREMIA_PRODUCTS); //
		String[] premiaAmexZero = getPremiaAmexZero(); 
		if(premiaAmexZero != null) {
			Collections.addAll(premiaProducts, premiaAmexZero);
		}
		return Collections.unmodifiableList(premiaProducts);
	}
	
	/**
	 * Products whit Amex Zero
	 */
	private String[] getPremiaAmexZero() {
		String[] premiaAmexZero = {"UAREG","UVNOV","APREG","UVVSI","UVREG","UVNOV"};
		return premiaAmexZero;
	}
	
	/**
	 * Get a list of dummy Customer codes.
	 * @return
	 */
	private List<String> getCustomerCodesList() {

		return Stream.of("16", "11", "181", "0",
				"9").collect(Collectors.toList());

	}

	 /**
	 * Get a list of dummy Customer codes.
	 * @return
	 */
	private List<String> getCustomerCodesWithoutPremiaList() {

		return Stream.of( "11", "181", "0",
				"9").collect(Collectors.toList());

	}

	/**
	 * Get a list of dummy Customer codes.
	 * @return
	 */
	private List<String> getPremiaCustomerCodesList() {

		return Stream.of("016").collect(Collectors.toList());
	}

	 /**
	 * Get a list of dummy Customer codes.
	 * @return
	 */
	private List<String> getCustomerCodesEmptyList() {

		return Collections.emptyList();

	}
}
