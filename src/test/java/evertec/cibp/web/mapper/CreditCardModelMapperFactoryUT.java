package evertec.cibp.web.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.evertec.cibp.api.client.model.MicroServiceResponse;

import evertec.cibp.web.enums.RecurringCashBackType;
import evertec.cibp.web.response.TLRRSCreditResponse;
import evertec.cibp.web.response.TsysLoyaltyRewardsRedemptionResponse;
import evertec.creditcards.request.TsysLoyaltyRewardsRecurringStatementCreditRequest;
import evertec.creditcards.response.AccountIdentifier;
import evertec.creditcards.response.RewardsCurrencyAmount;
import evertec.creditcards.response.TsysLoyaltyRewardsAccountInfoResponse;
import evertec.creditcards.response.TsysLoyaltyRewardsAccountInfoResponse.RewardsEarned;

import static org.junit.Assert.*;
import evertec.creditcards.response.TsysLoyaltyRewardsRedemptionResponse.CurrencyAmount;
import evertec.creditcards.response.TsysLoyaltyRewardsRedemptionResponse.CurrencyAmount.CurrencyEnum;

public class CreditCardModelMapperFactoryUT {
	
	/**
     * When CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse
     * Give null TsysLoyaltyRewardsAccountInfoResponse
     * Then return List
     */
	@Test
	public void whenConvertAccountInfoToAccountInfoResponse_GivenNullRewardsAccount_ThenReturnNotNull() {
		
		List<evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse> list = 
				CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(null);
		
		assertNotNull(list);
	}
	
	/**
     * When CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse
     * Give TsysLoyaltyRewardsAccountInfoResponse, null RewardsEarned
     * Then return List
     */
	@Test
	public void whenConvertAccountInfoToAccountInfoResponse_GivenRewardsAccountNullRewards_ThenReturnNotNull() {
		TsysLoyaltyRewardsAccountInfoResponse ysysLoyaltyRewardsAccountInfoResponse = new TsysLoyaltyRewardsAccountInfoResponse();
		List<evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse> list = 
				CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(ysysLoyaltyRewardsAccountInfoResponse);
		
		assertNotNull(list);
	}
	
	/**
     * When CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse
     * Give TsysLoyaltyRewardsAccountInfoResponse, null RewardsEarned, null RewardsCurrencyAmount
     * Then return List
     */
	@Test
	public void whenConvertAccountInfoToAccountInfoResponse_GivenRewardsAccountRewardsNullAvailable_ThenReturnNotNull() {
		TsysLoyaltyRewardsAccountInfoResponse ysysLoyaltyRewardsAccountInfoResponse = new TsysLoyaltyRewardsAccountInfoResponse();
		ysysLoyaltyRewardsAccountInfoResponse.setRewardsBalance(new RewardsEarned());
		List<evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse> list = 
				CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(ysysLoyaltyRewardsAccountInfoResponse);
		
		assertNotNull(list);
	}
	
	/**
     * When CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse
     * Give TsysLoyaltyRewardsAccountInfoResponse, null RewardsEarned, RewardsCurrencyAmount all valid
     * Then return List
     */
	@Test
	public void whenConvertAccountInfoToAccountInfoResponse_GivenRewardsAccountRewardsAvaible_ThenReturnNotNull() {
		TsysLoyaltyRewardsAccountInfoResponse tsysLoyaltyRewardsAccountInfoResponse = new TsysLoyaltyRewardsAccountInfoResponse();
		RewardsEarned rewardsEarned = new RewardsEarned();
		rewardsEarned.setAvailable(new RewardsCurrencyAmount());
		tsysLoyaltyRewardsAccountInfoResponse.setRewardsBalance(rewardsEarned);
		List<evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse> list = 
				CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(tsysLoyaltyRewardsAccountInfoResponse);
		
		assertNotNull(list);
	}
	
	/**
     * When CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse
     * Give TsysLoyaltyRewardsAccountInfoResponse, pooledRewardsEarned, null RewardsCurrencyAmount
     * Then return List
     */
	@Test
	public void whenConvertAccountInfoToAccountInfoResponse_GivenRewardsAccountPooledRewardsNullAvailable_ThenReturnNotNull() {
		TsysLoyaltyRewardsAccountInfoResponse ysysLoyaltyRewardsAccountInfoResponse = new TsysLoyaltyRewardsAccountInfoResponse();
		ysysLoyaltyRewardsAccountInfoResponse.setPooledRewardsBalance(new RewardsEarned());
		List<evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse> list = 
				CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(ysysLoyaltyRewardsAccountInfoResponse);
		
		assertNotNull(list);
		assertNotNull(list.get(0));
		assertNotNull(list.get(0).getPooledRewardsBalance());
	}
	
	/**
     * When CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse
     * Give TsysLoyaltyRewardsAccountInfoResponse, pooledRewardsEarned, RewardsCurrencyAmount all valid
     * Then return List
     */
	@Test
	public void whenConvertAccountInfoToAccountInfoResponse_GivenPooledRewardsAccountRewardsAvaible_ThenReturnNotNull() {
		TsysLoyaltyRewardsAccountInfoResponse tsysLoyaltyRewardsAccountInfoResponse = new TsysLoyaltyRewardsAccountInfoResponse();
		RewardsEarned pooledRewardsEarned = new RewardsEarned();
		pooledRewardsEarned.setAvailable(new RewardsCurrencyAmount());
		tsysLoyaltyRewardsAccountInfoResponse.setPooledRewardsBalance(pooledRewardsEarned);
		List<evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse> list = 
				CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(tsysLoyaltyRewardsAccountInfoResponse);
		
		assertNotNull(list);
		assertNotNull(list.get(0));
		assertNotNull(list.get(0).getPooledRewardsBalance());
		assertNotNull(list.get(0).getPooledRewardsBalance().getAvailable());
	}
	
	/**
     * When CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse
     * Give TsysLoyaltyRewardsAccountInfoResponse, rewardsEarned, null pooledRewardsEarned, RewardsCurrencyAmounts valid
     * Then return List
     */
	@Test
	public void whenConvertAccountInfoToAccountInfoResponse_GivenRewardsEarnedNullPooledRewardsNullAccountRewardsAvaible_ThenReturnNotNull() {
		TsysLoyaltyRewardsAccountInfoResponse tsysLoyaltyRewardsAccountInfoResponse = new TsysLoyaltyRewardsAccountInfoResponse();
		RewardsEarned rewardsEarned = new RewardsEarned();
		rewardsEarned.setAvailable(new RewardsCurrencyAmount());
		tsysLoyaltyRewardsAccountInfoResponse.setRewardsBalance(rewardsEarned);
		tsysLoyaltyRewardsAccountInfoResponse.setPooledRewardsBalance(null);
		List<evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse> list = 
				CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(tsysLoyaltyRewardsAccountInfoResponse);
		
		assertNotNull(list);
		assertNotNull(list.get(0));
		assertNotNull(list.get(0).getRewardsBalance());
		assertNotNull(list.get(0).getRewardsBalance().getAvailable());
		assertEquals("", list.get(0).getPooledRewardsBalance().getAvailable().getValue());
	}
	
	/**
     * When CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse
     * Give TsysLoyaltyRewardsAccountInfoResponse, null rewardsEarned, pooledRewardsEarned, RewardsCurrencyAmounts valid
     * Then return List
     */
	@Test
	public void whenConvertAccountInfoToAccountInfoResponse_GivenNullRewardsEarnedPooledRewardsNullAccountRewardsAvaible_ThenReturnNotNull() {
		TsysLoyaltyRewardsAccountInfoResponse tsysLoyaltyRewardsAccountInfoResponse = new TsysLoyaltyRewardsAccountInfoResponse();
		tsysLoyaltyRewardsAccountInfoResponse.setRewardsBalance(null);
		RewardsEarned pooledRewardsEarned = new RewardsEarned();
		pooledRewardsEarned.setAvailable(new RewardsCurrencyAmount());
		tsysLoyaltyRewardsAccountInfoResponse.setPooledRewardsBalance(pooledRewardsEarned);
		List<evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse> list = 
				CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(tsysLoyaltyRewardsAccountInfoResponse);
		
		assertNotNull(list);
		assertNotNull(list.get(0));
		assertEquals("", list.get(0).getRewardsBalance().getAvailable().getValue());
		assertNotNull(list.get(0).getPooledRewardsBalance());
		assertNotNull(list.get(0).getPooledRewardsBalance().getAvailable());
	}
	
	/**
     * When CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse
     * Give TsysLoyaltyRewardsAccountInfoResponse, rewardsEarned, pooledRewardsEarned, RewardsCurrencyAmount all valid
     * Then return List
     */
	@Test
	public void whenConvertAccountInfoToAccountInfoResponse_GivenRewardsPooledRewardsAccountRewardsAvaible_ThenReturnNotNull() {
		TsysLoyaltyRewardsAccountInfoResponse tsysLoyaltyRewardsAccountInfoResponse = new TsysLoyaltyRewardsAccountInfoResponse();
		RewardsEarned rewardsEarned = new RewardsEarned();
		rewardsEarned.setAvailable(new RewardsCurrencyAmount());
		tsysLoyaltyRewardsAccountInfoResponse.setRewardsBalance(rewardsEarned);
		RewardsEarned pooledRewardsEarned = new RewardsEarned();
		pooledRewardsEarned.setAvailable(new RewardsCurrencyAmount());
		tsysLoyaltyRewardsAccountInfoResponse.setPooledRewardsBalance(pooledRewardsEarned);
		List<evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse> list = 
				CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(tsysLoyaltyRewardsAccountInfoResponse);
		
		assertNotNull(list);
		assertNotNull(list.get(0));
		assertNotNull(list.get(0).getRewardsBalance());
		assertNotNull(list.get(0).getRewardsBalance().getAvailable());
		assertNotNull(list.get(0).getPooledRewardsBalance());
		assertNotNull(list.get(0).getPooledRewardsBalance().getAvailable());
	}
	
	@Test
	public void whenConvertRecurringStatementCreditResponse_GivenTsysLoyaltyRewardsRecurringStatementCreditResponseIsNull_ThenReturnNotNull() {
		TLRRSCreditResponse response =
                new TLRRSCreditResponse();
		List<TLRRSCreditResponse> listResponse =
                new ArrayList<>(0);
		MicroServiceResponse<TLRRSCreditResponse> microServiceResponse = new MicroServiceResponse<>();
        
		CreditCardModelMapperFactory.convertRecurringStatementCreditResponse(null);
		listResponse.add(response);
		microServiceResponse.setData(listResponse);
		
		assertNotNull(microServiceResponse);
	}
	
	@Test
	public void whenConvertRecurringStatementCreditResponse_GivenCreditCardResponseIsNotNull_ThenReturnNotNull() {

		evertec.creditcards.response.TsysLoyaltyRewardsRecurringStatementCreditResponse 
		 creditCardResponse = new evertec.creditcards.response.
				 TsysLoyaltyRewardsRecurringStatementCreditResponse();
		
		creditCardResponse.setRedemptionValue(new RewardsCurrencyAmount());
		creditCardResponse.setRewardsBalance(new RewardsCurrencyAmount());
		creditCardResponse.setIdentifiers(new AccountIdentifier());
		creditCardResponse.setItemCode("BPPRStmtCredit");
		creditCardResponse.setRecurringCashBackType("MONTHLY");
		
		TLRRSCreditResponse response =
                new TLRRSCreditResponse();
		List<TLRRSCreditResponse> listResponse =
                new ArrayList<>(0);
		
		MicroServiceResponse<TLRRSCreditResponse> microServiceResponse 
		= new MicroServiceResponse<>();
        
		CreditCardModelMapperFactory.convertRecurringStatementCreditResponse(creditCardResponse);
		listResponse.add(response);
		microServiceResponse.setData(listResponse);
		
		assertNotNull(microServiceResponse);
	}
	
	@Test
	public void whenConvertRecurringStatementCreditRequest_GivenRecurringCashBackTypeIsMonthlyAndCreditCardResponseIsNotNull_ThenReturnNotNull() {


		 evertec.cibp.web.request.TLRRSCreditRequest
		 cibpWebRequest = new evertec.cibp.web.request.TLRRSCreditRequest();

		 TsysLoyaltyRewardsRecurringStatementCreditRequest request =
	                new TsysLoyaltyRewardsRecurringStatementCreditRequest();
		cibpWebRequest.setItemCode("BPPRStmtCredit");
		cibpWebRequest.setRecurringCashBackType(RecurringCashBackType.MONTHLY);
			
		CreditCardModelMapperFactory.convertRecurringStatementCreditRequest(cibpWebRequest);
		assertNotNull(request);
	}
	
	@Test
	public void whenConvertRecurringStatementCreditRequest_GivenRecurringCashBackTypeIsThresholdAndCreditCardResponseIsNotNull_ThenReturnNotNull() {


		 evertec.cibp.web.request.TLRRSCreditRequest
		 cibpWebRequest = new evertec.cibp.web.request.TLRRSCreditRequest();

		 TsysLoyaltyRewardsRecurringStatementCreditRequest request =
	                new TsysLoyaltyRewardsRecurringStatementCreditRequest();
		cibpWebRequest.setItemCode("BPPRStmtCredit");
		cibpWebRequest.setRecurringCashBackType(RecurringCashBackType.THRESHOLD);
			
		CreditCardModelMapperFactory.convertRecurringStatementCreditRequest(cibpWebRequest);
		assertNotNull(request);
	}
	
	@Test
	public void whenConvertRecurringStatementCreditRequest_GivenRecurringCashBackTypeIsNullAndCreditCardResponseIsNotNull_ThenReturnNotNull() {


		 evertec.cibp.web.request.TLRRSCreditRequest
		 cibpWebRequest = new evertec.cibp.web.request.TLRRSCreditRequest();

		 TsysLoyaltyRewardsRecurringStatementCreditRequest request =
	                new TsysLoyaltyRewardsRecurringStatementCreditRequest();
		cibpWebRequest.setItemCode("BPPRStmtCredit");
		cibpWebRequest.setRecurringCashBackType(null);
			
		CreditCardModelMapperFactory.convertRecurringStatementCreditRequest(cibpWebRequest);
		assertNotNull(request);
	}
	
	@Test
	public void whenConvertRecurringStatementCreditRequest_GivenRecurringStatementCreditRequestIsNull_ThenReturnNotNull() {


		 evertec.cibp.web.request.TLRRSCreditRequest
		 cibpWebRequest = new evertec.cibp.web.request.TLRRSCreditRequest();

		 TsysLoyaltyRewardsRecurringStatementCreditRequest request =
	                new TsysLoyaltyRewardsRecurringStatementCreditRequest();
		cibpWebRequest.setItemCode(null);
		cibpWebRequest.setRecurringCashBackType(null);
			
		CreditCardModelMapperFactory.convertRecurringStatementCreditRequest(cibpWebRequest);
		assertNotNull(request);
	}
	
	public void whenconvertToRedemptionResponse_GivenCreditCardResponseIsNotNull_ThenReturnNotNull() {

		evertec.creditcards.response.TsysLoyaltyRewardsRedemptionResponse 
		 redemptionResponse = new evertec.creditcards.response.
				 TsysLoyaltyRewardsRedemptionResponse();
		
		CurrencyAmount currencyAmount = new CurrencyAmount();
		currencyAmount.setCurrency(CurrencyEnum.USD);
		redemptionResponse.setCashbackAmount(currencyAmount);
		
		Map<String, Object> algo = new HashMap<>(0);
		redemptionResponse.setIdentifiers(algo);
		redemptionResponse.setRewardsBalance(new RewardsCurrencyAmount());

		
		TsysLoyaltyRewardsRedemptionResponse response = new TsysLoyaltyRewardsRedemptionResponse(); // unique response
		response.setCashbackAmount(null);
		response.setConfirmationNumber(null);
		response.setIdentifiers(algo);
		response.setRedemptionDate(null);
		response.setRewardsBalance(null);
		 
		 
		MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> microServiceResponse 
		= new MicroServiceResponse<>();
		
		List<TsysLoyaltyRewardsRedemptionResponse> listResponse = new ArrayList<>(0);
        
		
		CreditCardModelMapperFactory.convertToRedemptionResponse(redemptionResponse);
		listResponse.add(response);
		microServiceResponse.setData(listResponse);
		microServiceResponse.setStatus(200);
		
		assertNotNull(microServiceResponse);
		assertEquals(microServiceResponse.getStatus(), Integer.valueOf(200));
	}
}
