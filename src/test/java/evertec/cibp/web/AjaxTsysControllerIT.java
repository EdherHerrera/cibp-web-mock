package evertec.cibp.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.account.TsysLoyaltyRewardsInfo;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.customer.interaction.InteractionSeverity;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.index.globalpayee.GlobalPayeeIndexBean;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ WebUtils.class, Utils.class})
public class AjaxTsysControllerIT extends AbstractModelAndViewTests {

	private HttpServletResponse response;

	@Mock
	private GlobalPayeeIndexBean globalPayeeIndexBean;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private HttpServletRequest mockRequest;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private TVFrontendAccount ccaAccountByFrontEndId;
	
	@Mock
	private List<TVFrontendAccount> listCcaAccountByFrontEndId;
	
	@Mock
	private CustomerInteractionEvent eventLog;
	
	@Mock
	private TsysLoyaltyRewardsInfo tsysLoyaltyRewardsInfo;
	
	@InjectMocks
	private AjaxTsysController ajaxTsysController;

	@Before
	public void setUp() throws Exception {
			
		response = new MockHttpServletResponse();		
		userSession = PowerMockito.mock(UserSession.class);
		customerService = PowerMockito.mock(CustomerServices.class);
		mockRequest = PowerMockito.mock(HttpServletRequest.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Utils.class);
		// setup ajaxTsysController 
		AjaxTsysController.setCustomerInteractionEventBuilder(req -> eventLog);
		ajaxTsysController.setCustomerService(customerService);
		String[] premiaAmexZero = {"UAREG","UVNOV","APREG","UVVSI","UVREG","UVNOV"};
		ajaxTsysController.setPremiaAmexZero(premiaAmexZero);
		when(mockRequest.getHeader("User-Agent")).thenReturn("");
		
	}

	/**
	 * Check imput parameter at call tsysLoyaltyRewardsRedirection.
	 * @throws Exception
	 */
	@Test
	public void whenTsysLoyalty_GivenValidArguments_ThenCallCustomerService() throws Exception {
		doNothing().when(customerService).logTsysLoyaltyRewardsRedirection(eventLog, InteractionSeverity.WARNING);
		// 
		when(mockRequest.getParameter("cardNumber")).thenReturn("0123456789");
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(null);
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsRedirection(mockRequest, response);
		assertViewName(modelAndView, "tsysLoyaltyRewardsRedirection");
		// verify input arguments
		verify(customerService, times(1)).logTsysLoyaltyRewardsRedirection(eventLog, InteractionSeverity.WARNING);
	}
	
	/**
     * When tsysLoyaltyRewardsRedirection
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsRedirectionRetail_ThenReturnModelAndView() throws Exception {
		
		when(mockRequest.getParameter("cardNumber")).thenReturn("0123456789");
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getEmail()).thenReturn("email@evertecinc.com");
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(userSession.getCustomerService()).thenReturn(customerService);		
		when(Utils.getAccountById(any(), any())).thenReturn(ccaAccountByFrontEndId);
		when(ccaAccountByFrontEndId.getAccountNumber()).thenReturn("0549542114539357");
		when(ccaAccountByFrontEndId.getAccountProductId()).thenReturn("UAREG");
		ajaxTsysController.setPremiaCatalogURL("http://development.premia.com/ssoClient/login.htm");
		ajaxTsysController.setSourceUrl("www.mibanco.com");
		when(customerService.generatePremiaToken("0123456789", "www.mibanco.com", "email@evertecinc.com", false)).thenReturn("34567854efdbnhmj55rfgy43ref");
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsRedirection(mockRequest, response);

		assertViewName(modelAndView, "tsysLoyaltyRewardsRedirection");
	}
	
	/**
     * When tsysLoyaltyRewardsAccountInfo
     * Given valid parameters
     * Then model and view named is json
     */
	@Test
	public void whentsysLoyaltyRewardsAccountInfo_ThenReturnModelAndView() throws Exception {
		listCcaAccountByFrontEndId = getDummyPremiaAccountList();
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getPremiaAccounts()).thenReturn(listCcaAccountByFrontEndId);
		when(listCcaAccountByFrontEndId.get(0).isPrimaryAccount()).thenReturn(true);
		when(userSession.isHasPremia()).thenReturn(true);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.isConsultRewardsPoints()).thenReturn(false);
		when(userSession.getListRewardPoints()).thenReturn(listCcaAccountByFrontEndId);
		when(listCcaAccountByFrontEndId.get(0).getTsysLoyaltyRewardsInfo()).thenReturn(tsysLoyaltyRewardsInfo);
		when(tsysLoyaltyRewardsInfo.getAvailableBalanceDouble()).thenReturn((double) 6765);
		
		ModelAndView modelAndView = ajaxTsysController.tsysLoyaltyRewardsAccountInfo(mockRequest, response);

		assertViewName(modelAndView, "tsysLoyaltyRewardsAccountInfo");
	}

	/**
	 * Get a list of dummy ghost accounts.
	 * @return
	 */
	private List<TVFrontendAccount> getDummyPremiaAccountList() {

		TVFrontendAccount acc = PowerMockito.spy(new TVFrontendAccount("CCA", "4549548381926524", "R"));
		TVAccountProductDefinition productDefinition = new TVAccountProductDefinition(
				"dummyProductDescription",
				"dummyAccountNumberDescription",
				"dummyAccountNumber",
				"VPPRE",
				"CCA",
				"0", // Success
				"dummyId");
		acc.setProductDefinition(productDefinition);
		acc.setPrimaryAccount(false);
		when(acc.getAccountProductId()).thenReturn("VPPRE");
		List<TVFrontendAccount> accList = new LinkedList<>();
		accList.add(acc);
		acc.setPrimaryAccount(true);
		return accList;
	}
}
