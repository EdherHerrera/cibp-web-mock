package evertec.cibp.web.facade;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import evertec.cibp.core.models.payee.TVFrontEndPayee;
import evertec.cibp.core.services.customer.modules.CommonServicesModule;

import evertec.cibp.core.services.customer.modules.PaymentServiceModule;
import evertec.cibp.core.services.responses.ResultStatus;
import evertec.cibp.vs.beans.payments.RecurringPayment;
import evertec.cibp.vs.models.requests.ModifyPaymentActionRequest;
import evertec.cibp.vs.models.results.ModifyPaymentActionResult;
import evertec.cibp.vs.services.TransactionVaultServices;
import evertec.cibp.web.services.modules.PaymentsServiceModule;
import evertec.cibp.web.utils.MicroServiceUtils;
import evertec.cibp.vs.beans.payments.PaymentModify;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.WebUtils;

import com.evertec.cibp.api.client.RestClient;
import com.evertec.cibp.api.client.model.Error;
import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;

import evertec.cibp.core.models.customer.VendorReceiptHistory;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.vs.beans.SmsBean;
import evertec.cibp.vs.models.requests.CustomerPayeeListActionRequest;
import evertec.cibp.vs.models.results.CustomerPayeeListActionResult;
import evertec.cibp.web.session.UserSession;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StaticMessageSource.class, WebUtils.class, ClientUtils.class,
		RestClient.class, CustomerServices.class, TransactionVaultServices.class, MicroServiceUtils.class })
public class PaymentsFacadeUT extends AbstractModelAndViewTests {

	/*
	 * 
	 * Taxid
	 */
	
	private static final String TAX_ID = "012663021";

	/**
	 * Endpoints for get vendor receipt history information
	 */
	private static final String GET_VENDOR_RECEIPT_HISTORY = "payments.get.vendor.receipt.history";

    /**
     * Endpoints for get vendor receipt history information
     */
    private static final String SAVE_VENDOR_RECEIPT_HISTORY = "payments.vendor.receipt.history.post";
	
	/**
	 * Endpoints for get customer enrollment information
	 */
	private static final String ENDPOINT_VENDOR_RECEIPT_HISTORY_INFORMATION = "/v1/vendor/receipt/{strReferenceNumber}/history";

	/**
	 * Endpoint for delete Customer Payee Preferences
	 */
	private static final String DELETE_CUSTOMER_PAYEE_PREFERENCES = "/v1/payees/preferences/{payeePreferencesId}";

	/**
	 * Endpoint for delete Customer Payee Preferences
	 */
	private static final String GET_CUSTOMER_PAYEE = "/v1/customer-payees";

	/** The Constant MICROSERVICE_RESPONSE_OK_STATUS */
	private static final int MICROSERVICE_RESPONSE_OK_STATUS = 200;

	/** The Constant MICROSERVICE_RESPONSE_BAD_REQUEST_STATUS */
	private static final int MICROSERVICE_RESPONSE_BAD_REQUEST_STATUS = 400;

	/** The Constant TAX_ID error message */
	public static final String REFERENCE_NUMBER_ERROR_MESSAGE = "referenceNumber can not be null, empty or contain special characters. Please verify the referenceNumber format or maybe can be that don't have (11 chars)";

	/** The Constant SIZE */
	private static final byte SIZE = 0;

	private MicroserviceStatusResponse msStatusResponse;

	private MicroserviceStatusResponse msProperties;

	@Mock
	BankingSession bankingSession;

	@InjectMocks
	private PaymentsFacade paymentsFacade;

	private PaymentsFacade paymentsFacadePrivate;
	

	
	/**
	 * customerService
	 */
	@Mock
	private CustomerServices customerService;

	/** The user session */
	@Mock
	private UserSession userSession;

	@Mock
	private RestClient restClient;

	@Mock
	private TransactionVaultServices vaultServices;

	@Mock
	private PaymentServiceModule paymentsServiceModule;

	@Mock
	private ModifyPaymentActionResult modifyPaymentActionResult;

	@Before
	public void setUp() throws Exception {

		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(ClientUtils.class);
		PowerMockito.mockStatic(RestClient.class);
		PowerMockito.mockStatic(CustomerServices.class);
        PowerMockito.mockStatic(MicroServiceUtils.class);

		PowerMockito.mockStatic(TransactionVaultServices.class);
		

		paymentsFacadePrivate = PowerMockito.spy(new PaymentsFacade());

		msStatusResponse = new MicroserviceStatusResponse();
		msProperties = new MicroserviceStatusResponse();
		
		
	}

	/**
	 * When modifyRegularPayment Given MsProperties disabled and payment is recurrent
	 * Then use monolithic implementation and ModifyRecurringPayment is Called
	 */
	@Test
	public void whenModifyRegularPayment_GivenMsPropertiesDisabledAndPaymentIsRecurrent_ThenModifyRecurrentPaymentIsCalled() {

		msProperties.setEnabled(false);
		when(ClientUtils.getMicroserviceEndpointAndStatus(anyString())).thenReturn(msProperties);
		when(customerService.getPaymentService()).thenReturn(paymentsServiceModule);
		when(paymentsServiceModule.getVaultService()).thenReturn(vaultServices);


		PaymentModify paymentM = new PaymentModify();
		RecurringPayment recurringPayment = new RecurringPayment();
		paymentM.setNewFrequency("Monthly");
		paymentM.setNewAmount(BigDecimal.valueOf(2));
		paymentM.setRecurringPmt(recurringPayment);

		ModifyPaymentActionRequest request = new ModifyPaymentActionRequest(true, paymentM, "000216850", null);
		when(vaultServices.modifyRecurringPayment(request)).thenReturn(modifyPaymentActionResult);

		ModifyPaymentActionResult result = paymentsFacade.modifyRegularPayment(request, customerService);

		assertEquals(modifyPaymentActionResult, result);
		verify(vaultServices).modifyRecurringPayment(request);

	}


	/**
	 * When modifyRegularPayment Given MsProperties disabled and payment is regular
	 * Then use monolithic implementation and ModifyRegularPayment is Called
	 */
	@Test
	public void whenModifyRegularPayment_GivenMsPropertiesDisabledAndPaymentIsRegular_ThenModifyRegularPaymentIsCalled() {

		msProperties.setEnabled(false);
		when(ClientUtils.getMicroserviceEndpointAndStatus(anyString())).thenReturn(msProperties);
		when(customerService.getPaymentService()).thenReturn(paymentsServiceModule);
		when(paymentsServiceModule.getVaultService()).thenReturn(vaultServices);

		PaymentModify paymentM = new PaymentModify();

		ModifyPaymentActionRequest request = new ModifyPaymentActionRequest(true, paymentM, "000216850", null);
		when(vaultServices.modifyRegularPayment(request)).thenReturn(modifyPaymentActionResult);

		ModifyPaymentActionResult result = paymentsFacade.modifyRegularPayment(request, customerService);

		assertEquals(modifyPaymentActionResult, result);
		verify(vaultServices).modifyRegularPayment(request);

	}

    /**
     * Gets the vendor receipt history.
     *
     * @return the vendor receipt history
     */
    private VendorReceiptHistory getVendorReceiptHistory (){
        VendorReceiptHistory history = new VendorReceiptHistory();
        history.setAccountNumber("Flexicuenta de Negocios 7696");
        history.setAmount(new BigDecimal("10.00"));
        history.setComments("comment for vendor receipt test");
        history.setConcept("payment");
        history.setDate(new Date());
        history.setEffectiveDate("11/07/2018");
        history.setEmail("test@evertecinc.com");
        history.setFrequency("One Payment");
        history.setInvoiceNumber("0070");
        history.setMessage("test microservice integration");
        history.setProfileId("402877c362b64ce80162b65fd3b30013");
        history.setReferenceNumber("00011122233");
        history.setTaxId("596074532");
        return history;
    }



}
