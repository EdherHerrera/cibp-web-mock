package evertec.cibp.web.facade;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import evertec.cibp.core.models.customer.applications.EpayrollApplication;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.utils.TestConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.util.WebUtils;
import com.evertec.cibp.api.client.RestClient;
import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.customer.modules.CommonServicesModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.MicroServiceUtils;


/**
 * Copyright 2019 Evertec, Inc. All Rights Reserved.
 * <p>
 * CustomerFacadeUT class.
 * <p>
 * Unit testing for CustomerFacade
 *
 * @author Henry Terrero
 * @version 1.0
 * @see CustomerFacadeUT
 * @since 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ClientUtils.class, WebUtils.class, RestClient.class, CustomerServices.class, CommonServicesModule.class, MicroServiceUtils.class, StaticMessageSource.class})
public class CustomerFacadeUT extends AbstractModelAndViewTests
{

    private static final String LOG_CUSTOMER_USAGE_STATS = "customers.log.usage.stats";
    private MockHttpServletRequest request;
    private MicroserviceStatusResponse msStatusResponse;

    @Mock
    private CustomerProfile customerProfileMock;


    @Mock
    private UserSession userSession;

    @Mock
    private RestClient restClient;


    @Mock
    private CustomerServices customerServices;


    @InjectMocks
    private CustomerFacade customer;

    @Before
    public void setUp() {

        PowerMockito.mockStatic(ClientUtils.class);
        PowerMockito.mockStatic(CustomerServices.class);
        PowerMockito.mockStatic(CommonServicesModule.class);
        PowerMockito.mockStatic(RestClient.class);
        PowerMockito.mockStatic(MicroServiceUtils.class);

        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(StaticMessageSource.class);
        msStatusResponse = new MicroserviceStatusResponse();
        msStatusResponse.setEndpoint(TestConstants.ENDPOINT_CUSTOMER_MBOP_INFO);

    }

    @Test()
    public void whensaveEpayrollApplicationNew_GivenEpayrollApplication_and_HttpServletRequest_ThenReturnTrue() {

        msStatusResponse.setEnabled(false);

        when(ClientUtils.getMicroserviceEndpointAndStatus(anyString())).thenReturn(msStatusResponse);

        request = new MockHttpServletRequest();

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);

        EpayrollApplication epayrollapp = new EpayrollApplication();

        CustomerProfile customerProfile = new CustomerProfile();

        when(userSession.getCustomerService()).thenReturn(customerServices);

        when(userSession.getCustomerService().saveEpayrollApplicationNew(customerProfile,epayrollapp)).thenReturn(false);

        when(customer.getInstance().saveEpayrollApplicationNew(epayrollapp,request)).thenReturn(false);

        boolean expected = customer.getInstance().saveEpayrollApplicationNew(epayrollapp,request);

        assertFalse(expected);
    }

    @Test()
    public void whensaveEpayrollApplicationNew_GivenEpayrollApplication_and_HttpServletRequest_ThenReturnFalse() {

        MicroServiceResponse resp = new MicroServiceResponse<>();

        resp.setStatus(201);

        msStatusResponse.setEnabled(true);

        when(ClientUtils.getMicroserviceEndpointAndStatus(anyString())).thenReturn(msStatusResponse);

        request = new MockHttpServletRequest();

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);

        EpayrollApplication epayrollapp = new EpayrollApplication();

        CustomerProfile customerProfile = new CustomerProfile();

        when(userSession.getRestClientResourceOwner()).thenReturn(restClient);

        when(restClient.postForMicroServiceResponse(any(),any(),any())).thenReturn(resp);

        when(userSession.getCustomerService()).thenReturn(customerServices);

        when(userSession.getCustomerProfile()).thenReturn(customerProfileMock);

        when(userSession.getCustomerProfile().getProfileId()).thenReturn("12345");

        when(userSession.getCustomerService().saveEpayrollApplicationNew(customerProfile,epayrollapp)).thenReturn(false);

        boolean expected = customer.getInstance().saveEpayrollApplicationNew(epayrollapp,request);

        assertFalse(expected);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenSaveCustomerApplicationNew_GivenEpayrollApplicationIsNull_ThenReturnIllegalArgumentException() throws Exception {

        request = new MockHttpServletRequest();

        customer.getInstance().saveEpayrollApplicationNew(null,request);

    }

}
