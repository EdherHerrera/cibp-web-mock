package evertec.cibp.web.facade;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import com.evertec.cibp.api.client.RestClient;

import evertec.cibp.core.models.documents.CustomerDocuments;
import evertec.cibp.core.models.documents.CustomerDocumentsId;
import evertec.cibp.core.models.documents.CustomerDocumentsPackage;
import evertec.cibp.web.session.UserSession;

@Ignore
public class DocumentsFacadeTest extends AbstractDependencyInjectionSpringContextTests {

	protected String[] getConfigLocations() {
		return new String[] { "classpath:/evertec/cibp/core/services/microservicesClientContext.xml",
				"classpath:/evertec/cibp/core/services/context/sosDatasource.xml",
				"classpath:/evertec/cibp/core/services/remoteApplicationContext.xml",
				"classpath:/evertec/cibp/db/hibernate/oracle-datasource-context.xml", };
	}

	//@Test Not correct unit test
	@Ignore
	public void testSignDocuments() {
		CustomerDocumentsPackage cusDocPckNewValues = new CustomerDocumentsPackage();
		cusDocPckNewValues.setDocPackId("ea73366ad523482a97dd6bd7c73c9877");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2017);
		cal.set(Calendar.MONTH, Calendar.JULY);
		cal.set(Calendar.DAY_OF_MONTH, 13);
		Date acceptedDate = cal.getTime();
		cusDocPckNewValues.setAcceptedDate(acceptedDate);
		cusDocPckNewValues.setBankerEmail("changedfelix.ruiz@popular.com");
		cal.set(Calendar.YEAR, 2017);
		cal.set(Calendar.MONTH, Calendar.JULY);
		cal.set(Calendar.DAY_OF_MONTH, 11);
		Date docDate = cal.getTime();
		cusDocPckNewValues.setDocDate(docDate);
		cusDocPckNewValues.setDocTypeId("1");
		cusDocPckNewValues.setHide("Y");
		cusDocPckNewValues.setLastHiddenBy("PR35186");
		cal.set(Calendar.YEAR, 2017);
		cal.set(Calendar.MONTH, Calendar.JULY);
		cal.set(Calendar.DAY_OF_MONTH, 9);
		Date lastHiddenDate = cal.getTime();
		cusDocPckNewValues.setLastHiddenDate(lastHiddenDate);
		cusDocPckNewValues.setLastUnhideBy("PR35187");
		cusDocPckNewValues.setPackSearchId("10100123230670103");
		cusDocPckNewValues.setProfileId("ff8080812564791f012570740ea344FG");
		cal.set(Calendar.YEAR, 2017);
		cal.set(Calendar.MONTH, Calendar.JULY);
		cal.set(Calendar.DAY_OF_MONTH, 14);
		Date signatureDate = cal.getTime();
		cusDocPckNewValues.setSignatureDeadlineDate(signatureDate);
		cusDocPckNewValues.setUploadBy("PR35185");
		cal.set(Calendar.YEAR, 2017);
		cal.set(Calendar.MONTH, Calendar.JULY);
		cal.set(Calendar.DAY_OF_MONTH, 12);
		Date uploadDate = cal.getTime();
		cusDocPckNewValues.setUploadDate(uploadDate);

		CustomerDocumentsId id1 = new CustomerDocumentsId();
		id1.setDocInfoId("8a81a4524b77095c014b849e313e0010");
		cal.set(Calendar.YEAR, 2015);
		cal.set(Calendar.MONTH, Calendar.JULY);
		cal.set(Calendar.DAY_OF_MONTH, 21);
		id1.setUploadDate(cal.getTime());

		CustomerDocuments doc1 = new CustomerDocuments();
		doc1.setCustomerDocumentsPackage(cusDocPckNewValues);
		doc1.setId(id1);
		doc1.setDocSearchId("B090237c9800612cc");
		doc1.setHideDoc("C");
		doc1.setLastHiddenBy("Dtest123");
		doc1.setLastUnhideBy("Etest456");
		cal.set(Calendar.YEAR, 2015);
		cal.set(Calendar.MONTH, Calendar.JULY);
		cal.set(Calendar.DAY_OF_MONTH, 20);
		doc1.setLastHiddenDate(cal.getTime());
		doc1.setDisplayAfter("F");

		CustomerDocumentsId id2 = new CustomerDocumentsId();
		id2.setDocInfoId("8a81a4524b77095c014b849e313e0011");
		cal.set(Calendar.YEAR, 2015);
		cal.set(Calendar.MONTH, Calendar.JULY);
		cal.set(Calendar.DAY_OF_MONTH, 23);
		id2.setUploadDate(cal.getTime());

		CustomerDocuments doc2 = new CustomerDocuments();
		doc2.setCustomerDocumentsPackage(cusDocPckNewValues);
		doc2.setId(id2);
		doc2.setDocSearchId("H090237c9800612dd");
		doc2.setHideDoc("I");
		doc2.setLastHiddenBy("Jtest789");
		doc2.setLastUnhideBy("Ktest101");
		cal.set(Calendar.YEAR, 2015);
		cal.set(Calendar.MONTH, Calendar.JULY);
		cal.set(Calendar.DAY_OF_MONTH, 22);
		doc2.setLastHiddenDate(cal.getTime());
		doc2.setDisplayAfter("L");

		Set<CustomerDocuments> documents = new HashSet<>();
		documents.add(doc1);
		documents.add(doc2);

		cusDocPckNewValues.setDocuments(documents);

		UserSession userSession = new UserSession();
		userSession.setRestClientResourceOwner(new RestClient());

		DocumentsFacade.getInstance().signDocuments("Sergio Perez Biascoechea", cusDocPckNewValues, userSession);
	}

}
