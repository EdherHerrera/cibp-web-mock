package evertec.cibp.web.facade;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.services.customer.modules.TsysCreditCardServiceModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.util.WebUtils;

import com.evertec.cibp.api.client.RestClient;
import com.evertec.cibp.api.client.model.MicroServiceResponse;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;

import evertec.cibp.core.models.cc.portal.QueueInsurance;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.mapper.CreditCardModelMapperFactory;
import evertec.cibp.web.request.TsysLoyaltyRewardsRedemptionRequest;
import evertec.cibp.web.response.TsysLoyaltyRewardsAccountInfoResponse;
import evertec.cibp.web.response.TsysLoyaltyRewardsRedemptionResponse;
import evertec.cibp.web.session.UserSession;
import evertec.creditcards.exception.TsysLoyaltyRewardsException;
import evertec.creditcards.services.CreditCardsService;

/**
 * Credit Card Facade
 * @author ymoreno
 *
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, ClientUtils.class, RequestContextHolder.class, CreditCardModelMapperFactory.class,
		ApiCallerFactory.class})
public class CreditCardFacadeUT {

	/** SMS Alerts Mapping URL */
	public static final String ENDPOINT_INSERT_CCQUEUE_INSURANCE = "/creditcards/queueinsurance";

	/**
	 * Endpoints for validate credit card pin account info
	 */
	private static final String ENDPOINT_CREDITCARD_PIN_ACCOUNT_INFO = "/v1/creditcards/validate/cardpin";

	/** TSYS Loyalty Rewards Mapping URL */
	private static final String GET_TSYS_LOYALTY_REWARDS_CARD_NUM_PROP_ID = "creditcards.get.tsys.loyalty.rewards.information.cards";
	private static final String GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID = "creditcards.get.tsys.loyalty.rewards.information.account.ids";
	private static final String POST_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_REDEMPTION_PROP_ID="creditcards.post.tsys.loyalty.rewards.redemption.account.ids";
	private static final String POST_TSYS_LOYALTY_REWARDS_CARD_NUM_REDEMPTION_PROP_ID = "creditcards.post.tsys.loyalty.rewards.redemption.cards";

	@InjectMocks
	private CreditCardFacade creditCardFacade;

	@Mock
	private UserSession userSession;

	@Mock
	private CreditCardsService creditCardsService;

	@Mock
	private MicroServiceResponse<Serializable> microServiceResponse;

	@Mock
	private TsysLoyaltyRewardsAccountInfoResponse tsysLoyaltyRewardsAccountInfoResponse;

	@Mock
	private TsysLoyaltyRewardsRedemptionResponse tsysLoyaltyRewardsRedemptionResponse;

	@Mock
	private List<TsysLoyaltyRewardsRedemptionResponse> loyaltyRewardsRedemptionResponse;    

	@Mock
	private RestClient restClient;

	/**
	 *	customerService
	 */
	@Mock
	private CustomerServices customerService;

	@Mock
	private ClientUtils clientUtils;

	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private ApiCaller apiCaller;


	/** The Constant MICROSERVICE_RESPONSE_OK_STATUS */
	private static final int MICROSERVICE_RESPONSE_OK_STATUS = 200;

	/** The Constant MICROSERVICE_RESPONSE_BAD_REQUEST_STATUS */
	private static final int MICROSERVICE_RESPONSE_BAD_REQUEST_STATUS = 400;

	/** The Constant SIZE */
	private static final byte SIZE = 0;

	/**
	 * Endpoints for credit cards service current transactions
	 */
	private static final String VALIDATE_CREDIT_CARD_PIN = "creditcards.validate.account.info";

	private static final String  ACCOUNT_NUMBER = "12345";
	private static final String  TAX_ID = "12345";
	private static final String  ENCRYPTED_PIN = "12345";
	private static final String  EXP_MONTH = "12";
	private static final String  EXP_YEAR = "22";
	private static final String  ZIP_CODE = "12345";

	@Mock
	private MicroserviceStatusResponse msStatusResponse;

	@Mock
	private TsysCreditCardServiceModule tsysCreditCardServiceModule;

	@Before
	public void setUp() {
		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);
		PowerMockito.mockStatic(ClientUtils.class);
		PowerMockito.mockStatic(RequestContextHolder.class);
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(CreditCardModelMapperFactory.class);
	}

	public static QueueInsurance getQueueInsuranceRequest() {

		QueueInsurance insuranceInfo = new QueueInsurance();
		insuranceInfo.setProfileId("74A49E80E60649EEB6FFE4CAF78D12D2");
		insuranceInfo.setCcAccount("0377810687817434");
		insuranceInfo.setStatus("P");
		return insuranceInfo;

	}

	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given null Account Number parameter, null CardNumber parameter and MsProperties disabled
     * @throws TsysLoyaltyRewardsException 
	 */
	@Test
    public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenNullParamatersAndMsPropertiesDisabled_ThenTsysLoyaltyRewardsException() throws TsysLoyaltyRewardsException {
		when(msStatusResponse.isEnabled()).thenReturn(true);
		when(msStatusResponse.getEndpoint()).thenReturn(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);
		when(userSession.getCustomerService()).thenReturn(customerService);

		TsysLoyaltyRewardsException tsysLoyaltyRewardsException = new TsysLoyaltyRewardsException();
		tsysLoyaltyRewardsException.setStatus(404);

        when(customerService.getTsysLoyaltyRewardsInfo(any(), any())).thenThrow(tsysLoyaltyRewardsException);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, null, null);

		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
		assertTrue(microServiceTsysLoyaltyRewardsResponse.getData().isEmpty());
        assertFalse(microServiceTsysLoyaltyRewardsResponse.getErrors().isEmpty());
	}

	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given null Account Number parameter, null CardNumber parameter, null User Session and MsProperties disabled
	 * @throws TsysLoyaltyRewardsException 
	 */
	@Test
	public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenNullUserSession_ThenReturnError() throws TsysLoyaltyRewardsException {

		when(msStatusResponse.isEnabled()).thenReturn(false);
		when(msStatusResponse.getEndpoint()).thenReturn(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getTsysLoyaltyRewardsInfo(any(), any())).thenThrow(new TsysLoyaltyRewardsException());
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(null, null, null);

		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
		assertTrue(microServiceTsysLoyaltyRewardsResponse.getData().isEmpty());
		assertFalse(microServiceTsysLoyaltyRewardsResponse.getErrors().isEmpty());
	}

	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given empty Account Number parameter, null CardNumber parameter and MsProperties disabled
	 * @throws TsysLoyaltyRewardsException 
	 */
	@Test
	public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenEmptyAccountParamaterMsPropertiesDisabled_ThenReturnResultNotNull() throws TsysLoyaltyRewardsException {

		when(msStatusResponse.isEnabled()).thenReturn(false);
		when(msStatusResponse.getEndpoint()).thenReturn(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);
		when(userSession.getCustomerService()).thenReturn(customerService);
		TsysLoyaltyRewardsException tsysLoyaltyRewardsException = new TsysLoyaltyRewardsException();
		tsysLoyaltyRewardsException.setStatus(404);
		when(customerService.getTsysLoyaltyRewardsInfo(any(), any())).thenThrow(tsysLoyaltyRewardsException);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, "", null);

		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
		assertTrue(microServiceTsysLoyaltyRewardsResponse.getData().isEmpty());
		assertFalse(microServiceTsysLoyaltyRewardsResponse.getErrors().isEmpty());
	}

	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given Account Number parameter, null CardNumber parameter and MsProperties disabled
	 * @throws TsysLoyaltyRewardsException 
	 */
	@Test
	public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenAccountParamaterMsPropertiesDisabled_ThenReturnResultNotNull() throws TsysLoyaltyRewardsException {

		when(msStatusResponse.isEnabled()).thenReturn(false);
		when(msStatusResponse.getEndpoint()).thenReturn(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerService().getTsysLoyaltyRewardsInfo(any(), any()))
			.thenReturn(new evertec.creditcards.response.TsysLoyaltyRewardsAccountInfoResponse());
		
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);
		
		when(CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(any()))
			.thenReturn(response);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, "00000024736", null);

		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
		assertFalse(microServiceTsysLoyaltyRewardsResponse.getData().isEmpty());
		assertTrue(microServiceTsysLoyaltyRewardsResponse.getErrors().isEmpty());
	}

	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given Account Number parameter, null CardNumber parameter and MsProperties enabled
	 */
	@SuppressWarnings("serial")
	@Test
	public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenAccountParamaterMsPropertiesEnabled_ThenReturnResultNotNull() {

		when(msStatusResponse.isEnabled()).thenReturn(true);
		when(msStatusResponse.getEndpoint()).thenReturn(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);
		when(userSession.getCustomerService()).thenReturn(customerService);

		when(userSession.getRestClientResourceOwner()).thenReturn(restClient);
		when(userSession.getRestClientResourceOwner().getForMicroServiceResponse(any(),
				any())).thenReturn(microServiceResponse);
		when(microServiceResponse.getData()).thenReturn(new ArrayList<Serializable>(){{
			add(tsysLoyaltyRewardsAccountInfoResponse);
		}});

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, "00000024736", null);

		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
	}

	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given null Account Number parameter, empty CardNumber parameter and MsProperties disabled
	 * @throws TsysLoyaltyRewardsException 
	 */
	@Test
	public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenEmptyCardNumberParamaterMsPropertiesDisabled_ThenReturnResultNotNull() throws TsysLoyaltyRewardsException {
		
		when(msStatusResponse.isEnabled()).thenReturn(false);
		when(msStatusResponse.getEndpoint()).thenReturn(GET_TSYS_LOYALTY_REWARDS_CARD_NUM_PROP_ID);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);
		when(userSession.getCustomerService()).thenReturn(customerService);

		TsysLoyaltyRewardsException tsysLoyaltyRewardsException = new TsysLoyaltyRewardsException();
		tsysLoyaltyRewardsException.setStatus(404);

		when(customerService.getTsysLoyaltyRewardsInfo(any(), any())).thenThrow(tsysLoyaltyRewardsException);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, null, "");

		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
		assertTrue(microServiceTsysLoyaltyRewardsResponse.getData().isEmpty());
		assertFalse(microServiceTsysLoyaltyRewardsResponse.getErrors().isEmpty());
		
	}

	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given null Account Number parameter, CardNumber parameter and MsProperties disabled
	 * @throws TsysLoyaltyRewardsException 
	 */
	@Test
	public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenCardNumberParamaterMsPropertiesDisabled_ThenReturnResultNotNull() throws TsysLoyaltyRewardsException {
		
		when(msStatusResponse.isEnabled()).thenReturn(false);
		when(msStatusResponse.getEndpoint()).thenReturn(GET_TSYS_LOYALTY_REWARDS_CARD_NUM_PROP_ID);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerService().getTsysLoyaltyRewardsInfo(any(), any()))
			.thenReturn(new evertec.creditcards.response.TsysLoyaltyRewardsAccountInfoResponse());
		
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);
		
		when(CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(any()))
			.thenReturn(response);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, null, "4549542182525866");

		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
		assertFalse(microServiceTsysLoyaltyRewardsResponse.getData().isEmpty());
		assertTrue(microServiceTsysLoyaltyRewardsResponse.getErrors().isEmpty());
	}

	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given null Account Number parameter, CardNumber parameter and MsProperties enabled
	 */
	@SuppressWarnings("serial")
	@Test
	public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenCardNumberParamaterMsPropertiesEnabled_ThenReturnResultNotNull(){

		when(msStatusResponse.isEnabled()).thenReturn(true);
		when(msStatusResponse.getEndpoint()).thenReturn(GET_TSYS_LOYALTY_REWARDS_CARD_NUM_PROP_ID);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);
		when(userSession.getCustomerService()).thenReturn(customerService);

		when(userSession.getRestClientResourceOwner()).thenReturn(restClient);
		when(userSession.getRestClientResourceOwner().getForMicroServiceResponse(any(),
				any())).thenReturn(microServiceResponse);
		when(microServiceResponse.getData()).thenReturn(new ArrayList<Serializable>(){{
			add(tsysLoyaltyRewardsAccountInfoResponse);
		}});

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, null, "4549542182525866");

		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
	}

	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Result Error TsysLoyaltyRewardsException
	 */
	@Test
	public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenCardNumberParamaterMsPropertiesDisabled_ThenReturnResultError() throws TsysLoyaltyRewardsException{

		when(msStatusResponse.isEnabled()).thenReturn(false);
		when(msStatusResponse.getEndpoint()).thenReturn(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);
		when(userSession.getCustomerService()).thenReturn(customerService);
		CustomerProfile customerProfile = new CustomerProfile();
		customerProfile.setUsername("Unit Test");
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		TsysLoyaltyRewardsException tsysLoyaltyRewardsException = new TsysLoyaltyRewardsException();
		tsysLoyaltyRewardsException.setStatus(400);

		when(userSession.getCustomerService().getTsysLoyaltyRewardsInfo(any(), any())).thenThrow(tsysLoyaltyRewardsException);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, null, "4549542182525866");

		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
		assertEquals(1, microServiceTsysLoyaltyRewardsResponse.getErrors().size());
		assertEquals(microServiceTsysLoyaltyRewardsResponse.getErrors().get(0).getCode(), "400");;        
	}

	/**
	 * When tsysLoyaltyRewardsRedemption
	 * Given null Account Number parameter, null CardNumber parameter and MsProperties disabled
	 */
	@Test
	public void whenTsysLoyaltyRewardsRedemption_GivenDisableMSRedemptionCardNumberAccountIdNull_ThenReturnError() {

		when(msStatusResponse.isEnabled()).thenReturn(false);

		customerProfile.setUsername("username");
		when(ClientUtils.getMicroserviceEndpointAndStatus(any()))
		.thenReturn(msStatusResponse);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getUsername()).thenReturn("marianita");

		TsysLoyaltyRewardsRedemptionRequest tsysRedemptionRequest = null;
		MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> response = creditCardFacade
				.tsysLoyaltyRewardsRedemption(userSession, null, null, tsysRedemptionRequest);

		assertNotNull(response);
		assertEquals(1, response.getErrors().size());
		assertEquals(response.getErrors().get(0).getCode(), "400");
		assertEquals(response.getErrors().get(0).getDetail(), "An account id or card number must be provided");		
	}

	/**
	 * When tsysLoyaltyRewardsRedemption
	 * Given null Account Number parameter, null CardNumber parameter and MsProperties disabled
	 */
	@Test
	public void whenTsysLoyaltyRewardsRedemption_GivenDisableMSRedemptionCardNumberAccountIdEmpty_ThenReturnError() {

		when(msStatusResponse.isEnabled()).thenReturn(false);

		customerProfile.setUsername("username");
		when(ClientUtils.getMicroserviceEndpointAndStatus(any()))
		.thenReturn(msStatusResponse);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getUsername()).thenReturn("marianita");

		TsysLoyaltyRewardsRedemptionRequest tsysRedemptionRequest = null;
		MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> response = creditCardFacade
				.tsysLoyaltyRewardsRedemption(userSession, "", "", tsysRedemptionRequest);

		assertNotNull(response);
		assertEquals(1, response.getErrors().size());
		assertEquals(response.getErrors().get(0).getCode(), "400");
		assertEquals(response.getErrors().get(0).getDetail(), "An account id or card number must be provided");		
	}

	/**
	 * When tsysLoyaltyRewardsRedemption
	 * Given null Account Number parameter, null CardNumber parameter and MsProperties disabled
	 */
	@Test
	public void whenTsysLoyaltyRewardsRedemption_GivenDisableMSRedemptionTsysRedemptionRequestNull_ThenReturnError() {

		when(msStatusResponse.isEnabled()).thenReturn(false);

		customerProfile.setUsername("username");
		when(ClientUtils.getMicroserviceEndpointAndStatus(any()))
		.thenReturn(msStatusResponse);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getUsername()).thenReturn("marianita");

		TsysLoyaltyRewardsRedemptionRequest tsysRedemptionRequest = null;
		MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> response = creditCardFacade
				.tsysLoyaltyRewardsRedemption(userSession, null, "4549542182525866", tsysRedemptionRequest);

		assertNotNull(response);
		assertEquals(1, response.getErrors().size());
		assertEquals(response.getErrors().get(0).getCode(), "400");
		assertEquals(response.getErrors().get(0).getDetail(), "Request body is invalid");
	}

	/**
	 * When tsysLoyaltyRewardsRedemption
	 * Given null Account Number parameter, null CardNumber parameter and MsProperties disabled
	 */
	@Test
	public void whenTsysLoyaltyRewardsRedemption_GivenDisableMSRedemptionTsysRedemptionisNotValid_ThenReturnError() {

		when(msStatusResponse.isEnabled()).thenReturn(false);

		customerProfile.setUsername("username");
		when(ClientUtils.getMicroserviceEndpointAndStatus(any()))
		.thenReturn(msStatusResponse);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getUsername()).thenReturn("marianita");

		TsysLoyaltyRewardsRedemptionRequest req = mock(TsysLoyaltyRewardsRedemptionRequest.class);
		when(req.isValid()).thenReturn(false);
		MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> response = creditCardFacade
				.tsysLoyaltyRewardsRedemption(userSession, null, "4549542182525866", req);

		assertNotNull(response);
		assertEquals(1, response.getErrors().size());
		assertEquals(response.getErrors().get(0).getCode(), "400");
		assertEquals(response.getErrors().get(0).getDetail(), "Request body is invalid");
	}	

	/**
	 * When tsysLoyaltyRewardsRedemption
	 * Given null Account Number parameter, null CardNumber parameter and MsProperties disabled
	 */
	@Test
	public void whenTsysLoyaltyRewardsRedemption_GivenDisableMSRedemptionTsysRedemptionAccoundIdValid_ThenReturnError() {

		when(msStatusResponse.isEnabled()).thenReturn(false);

		customerProfile.setUsername("username");
		when(ClientUtils.getMicroserviceEndpointAndStatus(any()))
		.thenReturn(msStatusResponse);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getUsername()).thenReturn("marianita");

		TsysLoyaltyRewardsRedemptionRequest req = mock(TsysLoyaltyRewardsRedemptionRequest.class);
		when(req.isValid()).thenReturn(false);
		MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> response = creditCardFacade
				.tsysLoyaltyRewardsRedemption(userSession, "00000024736", "", req);

		assertNotNull(response);
		assertEquals(1, response.getErrors().size());
		assertEquals(response.getErrors().get(0).getCode(), "400");
		assertEquals(response.getErrors().get(0).getDetail(), "Request body is invalid");
	}

	/**
	 * When tsysLoyaltyRewardsRedemption
	 * Given any error in redemption, null CardNumber parameter and MsProperties disabled
	 */
	@Test
	public void whenTsysLoyaltyRewardsRedemption_GivenRedemptionError_ThenTsysLoyaltyRewardsExceptionThrown() throws TsysLoyaltyRewardsException {
		when(msStatusResponse.isEnabled()).thenReturn(false);
		when(ClientUtils.getMicroserviceEndpointAndStatus(POST_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_REDEMPTION_PROP_ID))
		.thenReturn(msStatusResponse);
		when(ClientUtils.getMicroserviceEndpointAndStatus(POST_TSYS_LOYALTY_REWARDS_CARD_NUM_REDEMPTION_PROP_ID))
		.thenReturn(msStatusResponse);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getUsername()).thenReturn("marianita");
		TsysLoyaltyRewardsRedemptionRequest tsysRedemptionRequest = mock(TsysLoyaltyRewardsRedemptionRequest.class);
		when(tsysRedemptionRequest.isValid()).thenReturn(true);
		when(CreditCardModelMapperFactory.convertToRedemptionResponse(any())).thenReturn(new MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse>(){{
			getData().add(tsysLoyaltyRewardsRedemptionResponse);
		}});
		when(userSession.getCustomerService()).thenReturn(customerService);

		TsysLoyaltyRewardsException tsysLoyaltyRewardsException = new TsysLoyaltyRewardsException();
		tsysLoyaltyRewardsException.setStatus(404);

		when(customerService.redeemTsysLoyaltyRewardsBalance(any(), any(), any())).thenThrow(tsysLoyaltyRewardsException);
		MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> response = creditCardFacade
				.tsysLoyaltyRewardsRedemption(userSession, null, "4549542182525866", tsysRedemptionRequest);
		assertNotNull(response);
		assertFalse(response.getErrors().isEmpty());       
	}

	/**
	 * When tsysLoyaltyRewardsRedemption
	 * Given Redemption successful with card number parameter and MsProperties disabled
	 * @throws TsysLoyaltyRewardsException 
	 */
	@Test
	public void whenTsysLoyaltyRewardsRedemption_GivenRedemptionWithCardNumber_ThenReturnSuccess(){

		when(msStatusResponse.isEnabled()).thenReturn(false);
		when(ClientUtils.getMicroserviceEndpointAndStatus(POST_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_REDEMPTION_PROP_ID))
		.thenReturn(msStatusResponse);
		when(ClientUtils.getMicroserviceEndpointAndStatus(POST_TSYS_LOYALTY_REWARDS_CARD_NUM_REDEMPTION_PROP_ID))
		.thenReturn(msStatusResponse);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getUsername()).thenReturn("marianita");

		TsysLoyaltyRewardsRedemptionRequest tsysRedemptionRequest = mock(TsysLoyaltyRewardsRedemptionRequest.class);
		when(tsysRedemptionRequest.isValid()).thenReturn(true);
		when(CreditCardModelMapperFactory.convertToRedemptionResponse(any())).thenReturn(new MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse>(){{
			setData(loyaltyRewardsRedemptionResponse);
		}});
		when(userSession.getCustomerService()).thenReturn(customerService);
		MicroServiceResponse<TsysLoyaltyRewardsRedemptionResponse> response = creditCardFacade
				.tsysLoyaltyRewardsRedemption(userSession, null, "4549542182525866", tsysRedemptionRequest);

		assertNotNull(response);
		assertEquals(0, response.getErrors().size());
		assertFalse(response.getData().isEmpty());
	}
	

	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given null Account Number parameter, null CardNumber parameter, null User Session and MsProperties disabled
	 * @throws TsysLoyaltyRewardsException 
	 */
	@Test
	public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenIsCommercialProfileNullUserSession_ThenReturnError() throws TsysLoyaltyRewardsException {

		final MicroserviceStatusResponse msStatusResponse = new MicroserviceStatusResponse();
		msStatusResponse.setEnabled(false);
		msStatusResponse.setEndpoint(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);

		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);		
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(customerService.getTsysLoyaltyRewardsInfo(any(), any())).thenThrow(new TsysLoyaltyRewardsException());
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(null, null, null, false);

		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
		assertTrue(microServiceTsysLoyaltyRewardsResponse.getData().isEmpty());
		assertFalse(microServiceTsysLoyaltyRewardsResponse.getErrors().isEmpty());
		assertThat(microServiceTsysLoyaltyRewardsResponse.getErrorsSize(), equalTo(1));
	}
	
	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given Account Number parameter, null CardNumber parameter and MsProperties disabled
	 * @throws TsysLoyaltyRewardsException 
	 */
	@Test
	public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenIsCommercialProfileCardNumberNull_ThenReturnResultSucess() throws TsysLoyaltyRewardsException {

		final MicroserviceStatusResponse msStatusResponse = new MicroserviceStatusResponse();
		msStatusResponse.setEnabled(false);
		msStatusResponse.setEndpoint(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);

		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);	
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerService().getTsysLoyaltyRewardsInfo(any(), any()))
			.thenReturn(new evertec.creditcards.response.TsysLoyaltyRewardsAccountInfoResponse());
		
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);
		
		when(CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(any()))
			.thenReturn(response);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, "00000024736", null, false);

		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
		assertFalse(microServiceTsysLoyaltyRewardsResponse.getData().isEmpty());
		assertTrue(microServiceTsysLoyaltyRewardsResponse.getErrors().isEmpty());
 		assertThat(microServiceTsysLoyaltyRewardsResponse.getDataSize(), equalTo(1));
	}
	
	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given Account Number parameter, null CardNumber parameter and MsProperties enabled
	 */
	@SuppressWarnings("serial")
	@Test
	public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenIsCommercialProfileMsPropertiesEnabled_ThenReturnResultNotNull() {

		final MicroserviceStatusResponse msStatusResponse = new MicroserviceStatusResponse();
		msStatusResponse.setEnabled(true);
		msStatusResponse.setEndpoint(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);

		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);	

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);

		when(userSession.getRestClientResourceOwner()).thenReturn(restClient);
		when(userSession.getRestClientResourceOwner().getForMicroServiceResponse(any(),
				any())).thenReturn(microServiceResponse);
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);
		when(microServiceResponse.getData()).thenReturn(new ArrayList<Serializable>(){{
			add(tsysLoyaltyRewardsAccountInfoResponse);
		}});		
		
		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, "00000024736", null, false);
		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
	}
	
	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given Given IsCommercialProfile and flag false, CardNumber parameter and MsProperties disabled
	 * @throws TsysLoyaltyRewardsException 
	 */
	@Test
	public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenIsCommercialProfileMsPropertiesDisabled_ThenReturnResultData() throws TsysLoyaltyRewardsException {
		
		when(msStatusResponse.isEnabled()).thenReturn(false);
		when(msStatusResponse.getEndpoint()).thenReturn(GET_TSYS_LOYALTY_REWARDS_CARD_NUM_PROP_ID);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerService().getTsysLoyaltyRewardsInfo(any(), any()))
			.thenReturn(new evertec.creditcards.response.TsysLoyaltyRewardsAccountInfoResponse());
		
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);
		
		when(CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(any()))
			.thenReturn(response);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, null, "4549542182525866", false);

		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
		assertFalse(microServiceTsysLoyaltyRewardsResponse.getData().isEmpty());
		assertTrue(microServiceTsysLoyaltyRewardsResponse.getErrors().isEmpty());
		assertThat(microServiceTsysLoyaltyRewardsResponse.getDataSize(), equalTo(1));
	}
	
	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given IsCommercialProfile and flag true, CardNumber parameter and MsProperties disabled
	 * @throws TsysLoyaltyRewardsException 
	 */
	@Test
	public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenIsCommercialProfileFlagTrue_ThenReturnResultData() throws TsysLoyaltyRewardsException {
		
		when(msStatusResponse.isEnabled()).thenReturn(false);
		when(msStatusResponse.getEndpoint()).thenReturn(GET_TSYS_LOYALTY_REWARDS_CARD_NUM_PROP_ID);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerService().getTsysLoyaltyRewardsInfo(any(), any()))
			.thenReturn(new evertec.creditcards.response.TsysLoyaltyRewardsAccountInfoResponse());
		
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);
		
		when(CreditCardModelMapperFactory.convertAccountInfoToAccountInfoResponse(any()))
			.thenReturn(response);

		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse =
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, null, "4549542182525866", true);

		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
		assertFalse(microServiceTsysLoyaltyRewardsResponse.getData().isEmpty());
		assertTrue(microServiceTsysLoyaltyRewardsResponse.getErrors().isEmpty());
		assertThat(microServiceTsysLoyaltyRewardsResponse.getDataSize(), equalTo(1));
	}
	
	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given null Account Number parameter, null CardNumber parameter and MsProperties disabled
     * @throws TsysLoyaltyRewardsException 
	 */
	@Test
    public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenIsCommercialProfile_ThenException() throws TsysLoyaltyRewardsException {
		final MicroserviceStatusResponse msStatusResponse = new MicroserviceStatusResponse();
		msStatusResponse.setEnabled(false);
		msStatusResponse.setEndpoint(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);

		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);
		when(userSession.getCustomerService()).thenReturn(customerService);
		TsysLoyaltyRewardsException exception = new TsysLoyaltyRewardsException();
		exception.setDetail("Error Service CrediCardService");
		exception.setTitle("CreditCardService");
		exception.setStatus(409);
        when(customerService.getTsysLoyaltyRewardsInfo(any(), any(), eq(true))).thenThrow(exception);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getUsername()).thenReturn("test");
        
		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse = 
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, null, "4549542182525866", true);
		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
		assertTrue(microServiceTsysLoyaltyRewardsResponse.getData().isEmpty());
		assertThat(microServiceTsysLoyaltyRewardsResponse.getErrorsSize(), equalTo(1));
	}
	
	/**
	 * When consumeTsysLoyaltyRewardsAccountInfo
	 * Given null Account Number parameter, null CardNumber parameter and MsProperties disabled
     * @throws TsysLoyaltyRewardsException 
	 */
	@Test
    public void whenConsumeTsysLoyaltyRewardsAccountInfo_GivenIsCommercialProfileFlagTrue_ThenException() throws TsysLoyaltyRewardsException {
		
		final MicroserviceStatusResponse msStatusResponse = new MicroserviceStatusResponse();
		msStatusResponse.setEnabled(true);
		msStatusResponse.setEndpoint(GET_TSYS_LOYALTY_REWARDS_ACCOUNT_ID_PROP_ID);

		when(ClientUtils.getMicroserviceEndpointAndStatus(any())).thenReturn(msStatusResponse);	

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);

		when(userSession.getRestClientResourceOwner()).thenReturn(restClient);
		when(userSession.getRestClientResourceOwner().getForMicroServiceResponse(any(),
				any())).thenReturn(microServiceResponse);
		ArrayList<TsysLoyaltyRewardsAccountInfoResponse> response = new ArrayList<TsysLoyaltyRewardsAccountInfoResponse>();
		response.add(tsysLoyaltyRewardsAccountInfoResponse);
		when(microServiceResponse.getData()).thenReturn(new ArrayList<Serializable>(){{
			add(tsysLoyaltyRewardsAccountInfoResponse);
		}});
        
		MicroServiceResponse<TsysLoyaltyRewardsAccountInfoResponse> microServiceTsysLoyaltyRewardsResponse = 
				creditCardFacade.consumeTsysLoyaltyRewardsAccountInfo(userSession, null, "4549542182525866", true);
		assertNotNull(microServiceTsysLoyaltyRewardsResponse);
	}
	
}
