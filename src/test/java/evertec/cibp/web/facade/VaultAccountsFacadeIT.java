package evertec.cibp.web.facade;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.AbstractModelAndViewTests;

import com.evertec.cibp.api.client.RestClient;
import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.util.ClientUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.request.VaultAccountsRequest;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.MicroServiceUtils;
import evertec.cibp.web.utils.TestConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest({RestClient.class,CustomerServices.class,MicroServiceUtils.class, ClientUtils.class})
public class VaultAccountsFacadeIT extends AbstractModelAndViewTests{
	
	/** The microservice status response */
    private MicroserviceStatusResponse msStatusResponse;
    
    @InjectMocks
    private VaultAccountsFacade vaultAccountsFacade;

    /**
     * The mock RestClient.
     */
    @Mock
    private RestClient restClient;
    
    /**
     * The user session
     */
    @Mock
    private UserSession userSession;

	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(MicroServiceUtils.class);
        PowerMockito.mockStatic(CustomerServices.class);
        PowerMockito.mockStatic(RestClient.class);
        PowerMockito.mockStatic(ClientUtils.class);
        
        msStatusResponse = new MicroserviceStatusResponse();
	}

	@Test
    public void whenGetCcaCardPlastics_ListIsNoEmpty() throws Exception{

		msStatusResponse.setEnabled(false);
		
        when(ClientUtils.getMicroserviceEndpointAndStatus(TestConstants.GET_ACCOUNTS)).thenReturn(msStatusResponse);
        VaultAccountsRequest vaultRequest = new VaultAccountsRequest();
        
        vaultRequest.setUserSession(userSession);
        List<TVFrontendAccount> filteredAccounts = vaultAccountsFacade.getCcaCardPlastics(vaultRequest);
        
        assertNotNull(filteredAccounts);

    }

}
