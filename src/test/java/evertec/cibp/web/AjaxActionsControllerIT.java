package evertec.cibp.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.CustomerServices.TsysApiAction;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.index.globalpayee.GlobalPayeeIndexBean;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CreditCardFacade;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.Utils;
import evertec.relius.retirementplan.configuration.RetirementPlanClient;
import evertec.relius.retirementplan.ws.GetRetirementPlanBalanceFilterParameters;
import evertec.relius.retirementplan.ws.wsdl.RetirementPlanBalanceResponse;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AjaxActionsController.class, StaticMessageSource.class, CustomerFacade.class, WebUtils.class, ServletRequestUtils.class, Utils.class, CreditCardFacade.class, TsysApiAction.class })

public class AjaxActionsControllerIT extends AbstractModelAndViewTests {

	private HttpServletRequest request;
	private HttpServletResponse response;

	@Mock
	private GlobalPayeeIndexBean globalPayeeIndexBean;

	@Mock
	private UserSession userSession;
	
	@Mock
	private TVFrontendAccount tvFrontendAccount;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private HttpServletRequest mockRequest;
	
	@Mock
	private GlobalEntitlement globalEntitlement;
	
	@Mock
	private RetirementPlanBalanceResponse retirementPlanResponse;
	
	@Mock
	private BankingSession bankingSession;
	
	@Mock
	private CustomerEntitlement customerEntitlement;

	private GetRetirementPlanBalanceFilterParameters reliusFilter;
	
	@Mock
	private RetirementPlanClient client;
	
	@Mock
	private List<TVFrontendAccount> accounts;
	
	@Mock
	private CustomerFacade customerFacade;
	
	@Mock
	private Iterator<TVFrontendAccount> iterator;
	
	@InjectMocks
	private AjaxActionsController ajaxActionsController;
	
		
	@Before
	public void setUp() throws Exception {
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(ServletRequestUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(CreditCardFacade.class);
		PowerMockito.mockStatic(TsysApiAction.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		
		userSession = PowerMockito.mock(UserSession.class);

		retirementPlanResponse = PowerMockito.mock(RetirementPlanBalanceResponse.class);
		
		ajaxActionsController = PowerMockito.spy(new AjaxActionsController());
		
		mockRequest = PowerMockito.mock(HttpServletRequest.class);
		
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		
		when(Utils.getUserSession(request)).thenReturn(userSession);
		
		customerProfile.setUsername("testprofi");

		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.RETIREMENT_PLAN)).thenReturn(globalEntitlement);
		
	}
	

	/**
	 * When portal
	 * Given retirement plan Flag Enabled and ssn have a valid retPlan account. Also, retirement entitlement enable.
	 * Then Return retplan
	 * @throws Exception
	 */
	@Test
	public void whenPortal_givenRetirementPlanFlagEnabledAndSsnIsValid_thenReturnRetplan() throws Exception {
		
		when(customerProfile.getEntitlementByType(eq(EntitlementType.BANKING))).thenReturn(customerEntitlement);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
		when(Utils.getUserSession(request)).thenReturn(userSession);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.RETIREMENT_PLAN)).thenReturn(globalEntitlement);
	
		
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_CIBP)).thenReturn(Boolean.TRUE);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);

	
		when(userSession.getCustomerProfile().getTaxId()).thenReturn("054565663");
		

		when(client.getRetirementPlanBalance(reliusFilter)).thenReturn(retirementPlanResponse);
	
		ajaxActionsController.getRetirementPlans(userSession);

		assertTrue(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_CIBP));
		assertNotNull(retirementPlanResponse);
	}

	/**
	 * When portal
	 * Given ssn doesn't have a retirement plan
	 * Then Return retplan null on Portal Response ModelAndView
	 * @throws Exception
	 */
	@Test
	public void whenGetRetirementPlans_GivenRetirementPlansOnModelNull() throws Exception {
		

		when(customerProfile.getEntitlementByType(eq(EntitlementType.BANKING))).thenReturn(customerEntitlement);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
		when(Utils.getUserSession(request)).thenReturn(userSession);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.RETIREMENT_PLAN)).thenReturn(globalEntitlement);
		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.ENABLED);
		
		retirementPlanResponse = new RetirementPlanBalanceResponse();
		
		reliusFilter = new GetRetirementPlanBalanceFilterParameters();
		client = new RetirementPlanClient();
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_CIBP)).thenReturn(Boolean.TRUE);

		when(userSession.getCustomerProfile().getTaxId()).thenReturn("123456789");
		reliusFilter.setSsNum(userSession.getCustomerProfile().getTaxId());
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));

		ModelAndView modelView = ajaxActionsController.fisRetirementPlanInformation(request, response);

		verify(ajaxActionsController, times(1)).getRetirementPlans(any());
		assertTrue(!modelView.getModel().containsKey("retplan"));
		
	}

	/**
	 * When portal
	 * Given ssn doesn't have a retirement plan
	 * Then Return retplan null
	 * @throws Exception
	 */
	@Test
	public void whenGetRetirementPlans_GivenRetirementPlansNotNullAndFlagFalse() throws Exception {
		
		when(userSession.getCustomerProfile().getTaxId()).thenReturn("00100000");
		

		when(client.getRetirementPlanBalance(reliusFilter)).thenReturn(retirementPlanResponse);
	
		ajaxActionsController.getRetirementPlans(userSession);
		

		assertNotNull(retirementPlanResponse);
		assertFalse(userSession.isHasRetirementPlan());

	}
	
}