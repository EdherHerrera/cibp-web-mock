package evertec.cibp.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.AbstractModelAndViewTests;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.evertec.cibp.api.client.model.MicroserviceStatusResponse;
import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import com.evertec.cibp.api.client.util.ClientUtils;

import bppr.sms.beans.SMSProfile;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.accountopening.AccountApplication;
import evertec.cibp.core.models.accountopening.AccountApplications;
import evertec.cibp.core.models.alerts.AlertType;
import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.core.models.customer.CustomerEnrollmentSource;
import evertec.cibp.core.models.customer.CustomerEntitlement;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerSignonStats;
import evertec.cibp.core.models.customer.PreferredLanguage;
import evertec.cibp.core.models.customer.interaction.CustomerInteractionEvent;
import evertec.cibp.core.models.entitlement.AlertsEntitlement;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.profile.ProfileType;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.CustomerServices.TsysApiAction;
import evertec.cibp.core.services.interfaces.CookiePreferenceStatusService;
import evertec.cibp.core.services.interfaces.CustomerInteractionService;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.crypto.RandomUtils;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.enums.UserAgentType;
import evertec.cibp.web.facade.CreditCardFacade;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.form.bean.UnicaApplication;
import evertec.cibp.web.response.AccountTv;
import evertec.cibp.web.services.modules.CookiePreferenceServiceModule;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.session.UserSessionType;
import evertec.cibp.web.utils.CookieUtils;
import evertec.cibp.web.utils.DeviceUtils;
import evertec.cibp.web.utils.ListUtils;
import evertec.cibp.web.utils.UnicaUtils;
import evertec.cibp.web.utils.Utils;
import evertec.relius.retirementplan.configuration.RetirementPlanClient;
import evertec.relius.retirementplan.ws.GetRetirementPlanBalanceFilterParameters;
import evertec.relius.retirementplan.ws.wsdl.AccountBalance;
import evertec.relius.retirementplan.ws.wsdl.RetirementPlanBalanceResponse;
import evertec.relius.retirementplan.ws.wsdl.RetirementPlanInfo;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ WebUtils.class, ServletRequestUtils.class, Utils.class, CreditCardFacade.class, TsysApiAction.class, StaticMessageSource.class,
		CustomerFacade.class, ApiCallerFactory.class, ClientUtils.class, UnicaUtils.class, ListUtils.class, DeviceUtils.class, RandomUtils.class, CookieUtils.class})

@PowerMockIgnore({"org.springframework.ws.client","javax.xml.*", "org.xml.sax.*", "org.w3c.dom.*"})
public class AjaxActionsControllerUT extends AbstractModelAndViewTests {

	private HttpServletRequest request;
	private HttpServletResponse response;

	@Mock
	private UserSession userSession;

	@Mock
	private CustomerServices customerService;

	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private HttpServletRequest mockRequest;
	
	@Mock
	private GlobalEntitlement globalEntitlement;
	
	@Mock
	private ProfileType profileType;
	
	@Mock
	private AccountApplications accountApplications;
	
	@Mock
	private MessageSource messageSource;

	@Mock
	private HttpServletRequest requestValue;

	@Mock
	private CustomerServices customerServices;

	@Mock
	private CustomerFacade customerFacade;

	@Mock
	private CookiePreferenceServiceModule cookiePreferenceServiceModule;

	@Mock
	private CookiePreferenceStatusService cookiePreferenceStatusService;

	@Mock
	private CustomerInteractionService customerInteractionService;

	@Mock
	private CustomerInteractionEvent customerInteractionEvent;

	@Mock
	private AlertsEntitlement alertsEntitlement;

	@Mock
	private CustomerEntitlement customerEntitlement;

	@Mock
	private CustomerSignonStats customerSignonStats;

	@Mock
	private SMSProfile smsProfile;

	@Mock
	private List<TVFrontendAccount> accounts;

	@Mock
	private RetirementPlanBalanceResponse retirementPlanResponse;

	@Mock
	private GetRetirementPlanBalanceFilterParameters reliusFilter;

	@Mock
	private RetirementPlanClient client;

	@Mock
	private Iterator<TVFrontendAccount> iterator;


	@Mock
	private BankingSession bankingSession;

	@Mock
	private ApiCaller apiCaller;

	@InjectMocks
	private AjaxActionsController ajaxActionsController;


	/**
	 * A wrapper to get ajaxActionsController injected mock
	 * @return AjaxActionsController
	 */
	private AjaxActionsController getController () {
		return this.ajaxActionsController; // return controller of the class
	}


	@Before
	public void setUp() throws Exception {
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		PowerMockito.mockStatic(WebUtils.class);
		PowerMockito.mockStatic(Utils.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(UnicaUtils.class);
		PowerMockito.mockStatic(CreditCardFacade.class);
		PowerMockito.mockStatic(TsysApiAction.class);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(ClientUtils.class);
		PowerMockito.mockStatic(ListUtils.class);
		PowerMockito.mockStatic(DeviceUtils.class);
		PowerMockito.mockStatic(RandomUtils.class);
		PowerMockito.mockStatic(CookieUtils.class);

		userSession = PowerMockito.mock(UserSession.class);
		ajaxActionsController = PowerMockito.spy(new AjaxActionsController());
		ajaxActionsController.setCustomerService(customerService);
		userSession = PowerMockito.mock(UserSession.class);
		mockRequest = PowerMockito.mock(HttpServletRequest.class);

		retirementPlanResponse = PowerMockito.mock(RetirementPlanBalanceResponse.class);

		CustomerProfile customerProfile = new CustomerProfile();
		when(userSession.getCustomerService()).thenReturn(customerService);

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		when(Utils.getUserSession(request)).thenReturn(userSession);

		customerProfile.setUsername("testprofi");

		getController().setUnicaRefresh("false");
		getController().setUnicaAllowedStatus("ap-approved,uk-approved,en-inprocess");
		getController().setUnicaCreditProducts("ULINE,MWAWR,UMEDP,VSNOV,MCEDG,AEULT,VSRTN,VSELT,UVNOV,UMEDG,UAULT,VSSEC,VSICN,VSELT,APREG,UMEDP,VWAIN,VWASI,VWNOS,AEELT,AEICN");
		getController().setUnicaLoanProducts("ULOAN,XX20,XX50,REQ,XX23,XX22,XX24,XX24C,XX28,XX20S,XX50S,XX20T,XX50T,XX20P,XX50P");
		getController().setMarketplaceAllowedStatus("ap-approved,uk-approved,en-inprocess,td-turneddown,bk-booked");
		getController().setMarketplaceCreditProducts("VPCBK,VPPRE,VWAIP,MGJBG,MWJBB,UVREG");

		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		when(customerFacade.getGlobalEntitlement(userSession, EntitlementType.RETIREMENT_PLAN)).thenReturn(globalEntitlement);

		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);
	}

	public void whenPortalUnicaApplications_GivenListAsCreditCard_ThenReturnModelAndView() throws Exception {


		when(profileType.getId()).thenReturn(1);
		when(userSession.getUserSessionType()).thenReturn(UserSessionType.CUSTOMER_PROFILE);
		when(userSession.getAccountApplications(true)).thenReturn(accountApplications);

		List<AccountApplication> completedApplications2 = accountApplications.getCompletedApplications();
		when(accountApplications.getCompletedApplications()).thenReturn(completedApplications2);    // Mock implementation

		List<UnicaApplication> unicaApplicationsList = new LinkedList<UnicaApplication>();
		when(accountApplications.getCompletedApplications()).thenReturn(completedApplications2);

		when(UnicaUtils.getApplications(any(),any(),any(),any(),any(), any(), any())).thenReturn(unicaApplicationsList);
		unicaApplicationsList.add(new UnicaApplication());
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.TRUE);

		ModelAndView modelAndView = ajaxActionsController.portalUnicaApplications(mockRequest, response);
		assertViewName(modelAndView, "portalUnicaApplications");
	}

	@Test
	public void whenPortalUnicaApplications_GivenListAsNoCreditCard_ThenReturnModelAndView() throws Exception {

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);

		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(1);
		when(userSession.getUserSessionType()).thenReturn(UserSessionType.CUSTOMER_PROFILE);
		when(userSession.getAccountApplications(true)).thenReturn(accountApplications);

		List<AccountApplication> completedApplications2 = accountApplications.getCompletedApplications();
		when(accountApplications.getCompletedApplications()).thenReturn(completedApplications2);    // Mock implementation

		List<UnicaApplication> unicaApplicationsList = new LinkedList<UnicaApplication>();
		when(accountApplications.getCompletedApplications()).thenReturn(completedApplications2);

		when(UnicaUtils.getApplications(any(),any(),any(),any(),any(), any(), any())).thenReturn(unicaApplicationsList);
		unicaApplicationsList.add(new UnicaApplication());
		unicaApplicationsList.get(0).setCredit(true);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.TRUE);

		ModelAndView modelAndView = ajaxActionsController.portalUnicaApplications(mockRequest, response);
		assertViewName(modelAndView, "portalUnicaApplications");

	}

	@Test
	public void whenPortalUnicaApplications_GivenMarketplaceApplicationAndEnabledMarketplaceFlag_ThenReturnModelAndViewWithMarketplaceApplications() throws Exception {

		List<AccountApplication> completedApplications2 = accountApplications.getCompletedApplications();
		List<UnicaApplication> unicaApplicationsList = new LinkedList<UnicaApplication>();
		UnicaApplication application = new UnicaApplication();
		application.setFromMarketplace(true);
		unicaApplicationsList.add(application);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(1);
		when(userSession.getUserSessionType()).thenReturn(UserSessionType.CUSTOMER_PROFILE);
		when(userSession.getAccountApplications(true)).thenReturn(accountApplications);
		when(accountApplications.getCompletedApplications()).thenReturn(completedApplications2);
		when(UnicaUtils.getApplications(any(),any(),any(),any(),any(), any(), any())).thenReturn(unicaApplicationsList);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.TRUE);

		ModelAndView modelAndView = ajaxActionsController.portalUnicaApplications(mockRequest, response);
		assertViewName(modelAndView, "portalUnicaApplications");
		assertModelAttributeAvailable(modelAndView, "marketplaceApplications");

	}

	@Test
	public void whenPortalUnicaApplications_GivenMarketplaceApplicationAndDisabledMarketplaceFlag_ThenReturnModelAndViewWithNoMarketplaceApplications() throws Exception {

		List<AccountApplication> completedApplications2 = accountApplications.getCompletedApplications();
		List<UnicaApplication> unicaApplicationsList = new LinkedList<UnicaApplication>();
		UnicaApplication application = new UnicaApplication();
		application.setFromMarketplace(true);
		unicaApplicationsList.add(application);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(profileType.getId()).thenReturn(1);
		when(userSession.getUserSessionType()).thenReturn(UserSessionType.CUSTOMER_PROFILE);
		when(userSession.getAccountApplications(true)).thenReturn(accountApplications);
		when(accountApplications.getCompletedApplications()).thenReturn(completedApplications2);
		when(UnicaUtils.getApplications(any(),any(),any(),any(),any(), any(), any())).thenReturn(unicaApplicationsList);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MARKETPLACE)).thenReturn(Boolean.FALSE);

		ModelAndView modelAndView = ajaxActionsController.portalUnicaApplications(mockRequest, response);
		assertViewName(modelAndView, "portalUnicaApplications");
		assertFalse(modelAndView.getModelMap().containsAttribute("marketplaceApplications"));

	}

	/**
	 * when DetailDEPPortal
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ER58344
	 * @since 09-10-2020
	 * @throws Exception error
	 */
	@SuppressWarnings({"unchecked"})
	@Test
	public void whenDetailDEPPortal_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () throws Exception {

		TVFrontendAccount tvFrontendAccount = new TVFrontendAccount(new AccountTv());
		ajaxActionsController.setMessageSource(messageSource);
		BankingSession bankingSession = new BankingSession(new BigInteger(String.valueOf(1)), "2");
		MicroserviceStatusResponse microServiceResponse = new MicroserviceStatusResponse();
		microServiceResponse.setEnabled(Boolean.FALSE);

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);
		when(requestValue.getParameter(anyString())).thenReturn("123456");
		when(userSession.getAccounts()).thenReturn(new ArrayList<>());
		when(userSession.getCustomerService()).thenReturn(customerServices);
		when(customerServices.getGlobalEntitlement(any(EntitlementType.class))).thenReturn(new GlobalEntitlement());
		when(Utils.getAccountById(any(String.class), any(ArrayList.class))).thenReturn(tvFrontendAccount);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.TRUE);
		when(ClientUtils.getMicroserviceEndpointAndStatus(anyString())).thenReturn(microServiceResponse);

		when(globalEntitlement.getStatus()).thenReturn(EntitlementStatus.DISABLED);
		when(customerService.getGlobalEntitlement(any(EntitlementType.class)))
				.thenReturn(globalEntitlement);

		when(messageSource.getMessage(anyString(), any(), any())).thenReturn(EntitlementStatus.ENABLED.toString());
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(customerServices.getAccountDetails(any(CustomerProfile.class), any(BankingSession.class),
				any(TVFrontendAccount.class), any(Boolean.class))).thenReturn(tvFrontendAccount);

		ModelAndView modelAndView = ajaxActionsController.detailDEPPortal(request, response);
		assertEquals(Boolean.TRUE, modelAndView.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when ESignForm
	 * Given IsCookieCategoryEnabledTrue
	 * Then ReturnModelValueTrue
	 *
	 * @author Evertec ER58344
	 * @since 09-10-2020
	 */
	@Test
	public void whenESignForm_GivenIsCookieCategoryEnabledTrue_ThenReturnModelValueTrue () {

		when(WebUtils.getSessionAttribute(any(HttpServletRequest.class), anyString())).thenReturn(userSession);
		when(userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS)).thenReturn(Boolean.TRUE);

		ModelAndView modelAndView = ajaxActionsController.eSignForm(request, response);
		assertEquals(Boolean.TRUE, modelAndView.getModel().get("cookieAnalyticEnabled"));
	}

	/**
	 * when UpdateCookiePrivacySettings
	 * Given CookieCategoryAnalytics
	 * Void method
	 * @author ET58344, Evertec Inc.
	 * @since 03-04-2021
	 */
	@Test
	public void whenUpdateCookiePrivacySettings_GivenCookieCategoryAnalytics() throws Exception {

		ajaxActionsController.setCustomerService(customerServices);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(requestValue.getParameter(anyString())).thenReturn("swAnalytic").thenReturn("DISABLED");
		PowerMockito.whenNew(CookiePreferenceServiceModule.class).withNoArguments().thenReturn(cookiePreferenceServiceModule);
		when(userSession.getCookiePreferenceStatusList()).thenReturn(new ArrayList<CookiePreferenceStatus>());
		when(ListUtils.getCookiePreferenceFromList(any(), any())).thenReturn(new CookiePreferenceStatus());
		when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
		when(customerServices.getInteractionService()).thenReturn(customerInteractionService);
		when(cookiePreferenceStatusService.updateCookiePreferenceStatus(any())).thenReturn(Boolean.TRUE);
		when(cookiePreferenceServiceModule.deleteSelectedCookies(any(), any())).thenReturn(Boolean.TRUE);

		ajaxActionsController.updateCookiePrivacySettings(requestValue, response);
	}

	/**
	 * when UpdateCookiePrivacySettings
	 * Given CookieCategoryFunctional
	 * Void method
	 * @author ET58344, Evertec Inc.
	 * @since 03-05-2021
	 */
	@Test
	public void whenUpdateCookiePrivacySettings_GivenCookieCategoryFunctional() throws Exception {

		ajaxActionsController.setCustomerService(customerServices);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(requestValue.getParameter(anyString())).thenReturn("swFunctional").thenReturn("DISABLED");
		PowerMockito.whenNew(CookiePreferenceServiceModule.class).withNoArguments().thenReturn(cookiePreferenceServiceModule);
		when(userSession.getCookiePreferenceStatusList()).thenReturn(new ArrayList<CookiePreferenceStatus>());
		when(ListUtils.getCookiePreferenceFromList(any(), any())).thenReturn(new CookiePreferenceStatus());
		when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
		when(customerServices.getInteractionService()).thenReturn(customerInteractionService);
		when(cookiePreferenceStatusService.updateCookiePreferenceStatus(any())).thenReturn(Boolean.TRUE);
		when(cookiePreferenceServiceModule.deleteSelectedCookies(any(), any())).thenReturn(Boolean.TRUE);

		ajaxActionsController.updateCookiePrivacySettings(requestValue, response);
	}

	/**
	 * when UpdateCookiePrivacySettings
	 * Given CookieCategoryAdvertising
	 * Void method
	 * @author ET58344, Evertec Inc.
	 * @since 03-05-2021
	 */
	@Test
	public void whenUpdateCookiePrivacySettings_GivenCookieCategoryAdvertising() throws Exception {

		ajaxActionsController.setCustomerService(customerServices);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(requestValue.getParameter(anyString())).thenReturn("swAdvertising").thenReturn("DISABLED");
		PowerMockito.whenNew(CookiePreferenceServiceModule.class).withNoArguments().thenReturn(cookiePreferenceServiceModule);
		when(userSession.getCookiePreferenceStatusList()).thenReturn(new ArrayList<CookiePreferenceStatus>());
		when(ListUtils.getCookiePreferenceFromList(any(), any())).thenReturn(new CookiePreferenceStatus());
		when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
		when(customerServices.getInteractionService()).thenReturn(customerInteractionService);
		when(cookiePreferenceStatusService.updateCookiePreferenceStatus(any())).thenReturn(Boolean.TRUE);
		when(cookiePreferenceServiceModule.deleteSelectedCookies(any(), any())).thenReturn(Boolean.TRUE);

		ajaxActionsController.updateCookiePrivacySettings(requestValue, response);
	}

	/**
	 * when UpdateCookiePrivacySettings
	 * Given CookieCategoryAnalytics
	 * Void method
	 * @author ET58344, Evertec Inc.
	 * @since 03-16-2021
	 */
	@Test
	public void whenUpdateCookiePrivacySettings_RegisterEventAnalytics() throws Exception {
		ajaxActionsController.setCustomerService(customerServices);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(requestValue.getParameter(anyString())).thenReturn("swAnalytic").thenReturn("ENABLED");
		PowerMockito.whenNew(CookiePreferenceServiceModule.class).withNoArguments().thenReturn(cookiePreferenceServiceModule);
		when(userSession.getCookiePreferenceStatusList()).thenReturn(new ArrayList<CookiePreferenceStatus>());
		when(ListUtils.getCookiePreferenceFromList(any(), any())).thenReturn(new CookiePreferenceStatus());
		when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
		when(customerServices.getInteractionService()).thenReturn(customerInteractionService);
		when(cookiePreferenceStatusService.updateCookiePreferenceStatus(any())).thenReturn(Boolean.TRUE);
		when(cookiePreferenceServiceModule.deleteSelectedCookies(any(), any())).thenReturn(Boolean.TRUE);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
		PowerMockito.doReturn(customerInteractionEvent).when(Utils.class,"generateCustomerInteractionEvent",any());
		PowerMockito.doReturn("").when(DeviceUtils.class,"getBrowserInfo",any(), any());
		PowerMockito.doNothing().when(customerInteractionService).logAnalyticsCookiesActivation(any(),any(),any());

		ajaxActionsController.updateCookiePrivacySettings(requestValue, response);
		PowerMockito.verifyPrivate(ajaxActionsController, times(0)).invoke("registerEvent",requestValue,userSession,"ENABLED","swAnalytic");
	}

	/**
	 * when UpdateCookiePrivacySettings
	 * Given CookieCategoryAnalytics
	 * Void method
	 * @author ET58344, Evertec Inc.
	 * @since 03-16-2021
	 */
	@Test
	public void whenUpdateCookiePrivacySettings_RegisterEventAnalyticsJson() throws Exception {
		ajaxActionsController.setCustomerService(customerServices);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(requestValue.getParameter(anyString())).thenReturn("swAnalytic").thenReturn("ENABLED");
		PowerMockito.whenNew(CookiePreferenceServiceModule.class).withNoArguments().thenReturn(cookiePreferenceServiceModule);
		when(userSession.getCookiePreferenceStatusList()).thenReturn(new ArrayList<CookiePreferenceStatus>());
		when(ListUtils.getCookiePreferenceFromList(any(), any())).thenReturn(new CookiePreferenceStatus());
		when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
		when(customerServices.getInteractionService()).thenReturn(customerInteractionService);
		when(cookiePreferenceStatusService.updateCookiePreferenceStatus(any())).thenReturn(Boolean.TRUE);
		when(cookiePreferenceServiceModule.deleteSelectedCookies(any(), any())).thenReturn(Boolean.TRUE);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.JSON);
		PowerMockito.doReturn(customerInteractionEvent).when(Utils.class,"generateCustomerInteractionEvent",any());
		PowerMockito.doReturn("").when(DeviceUtils.class,"getBrowserInfo",any(), any());
		PowerMockito.doNothing().when(customerInteractionService).logAnalyticsCookiesActivation(any(),any(),any());

		ajaxActionsController.updateCookiePrivacySettings(requestValue, response);
		PowerMockito.verifyPrivate(ajaxActionsController, times(0)).invoke("registerEvent",requestValue,userSession,"ENABLED","swAnalytic");
	}

	/**
	 * when UpdateCookiePrivacySettings
	 * Given CookieCategoryFunctional
	 * Void method
	 * @author ET58344, Evertec Inc.
	 * @since 03-16-2021
	 */
	@Test
	public void whenUpdateCookiePrivacySettings_RegisterEventFunctional() throws Exception {
		ajaxActionsController.setCustomerService(customerServices);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(requestValue.getParameter(anyString())).thenReturn("swFunctional").thenReturn("ENABLED");
		PowerMockito.whenNew(CookiePreferenceServiceModule.class).withNoArguments().thenReturn(cookiePreferenceServiceModule);
		when(userSession.getCookiePreferenceStatusList()).thenReturn(new ArrayList<CookiePreferenceStatus>());
		when(ListUtils.getCookiePreferenceFromList(any(), any())).thenReturn(new CookiePreferenceStatus());
		when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
		when(customerServices.getInteractionService()).thenReturn(customerInteractionService);
		when(cookiePreferenceStatusService.updateCookiePreferenceStatus(any())).thenReturn(Boolean.TRUE);
		when(cookiePreferenceServiceModule.deleteSelectedCookies(any(), any())).thenReturn(Boolean.TRUE);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
		PowerMockito.doReturn(customerInteractionEvent).when(Utils.class,"generateCustomerInteractionEvent",any());
		PowerMockito.doReturn("").when(DeviceUtils.class,"getBrowserInfo",any(), any());
		PowerMockito.doNothing().when(customerInteractionService).logFunctionalCookiesActivation(any(),any(),any());

		ajaxActionsController.updateCookiePrivacySettings(requestValue, response);
		PowerMockito.verifyPrivate(ajaxActionsController, times(0)).invoke("registerEvent",requestValue,userSession,"ENABLED","swFunctional");
	}

	/**
	 * when UpdateCookiePrivacySettings
	 * Given CookieCategoryAdvertising
	 * Void method
	 * @author ET58344, Evertec Inc.
	 * @since 03-16-2021
	 */
	@Test
	public void whenUpdateCookiePrivacySettings_RegisterEventAdvertising() throws Exception {
		ajaxActionsController.setCustomerService(customerServices);
		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(requestValue.getParameter(anyString())).thenReturn("swAdvertising").thenReturn("ENABLED");
		PowerMockito.whenNew(CookiePreferenceServiceModule.class).withNoArguments().thenReturn(cookiePreferenceServiceModule);
		when(userSession.getCookiePreferenceStatusList()).thenReturn(new ArrayList<CookiePreferenceStatus>());
		when(ListUtils.getCookiePreferenceFromList(any(), any())).thenReturn(new CookiePreferenceStatus());
		when(customerServices.getCookiePreferenceStatusService()).thenReturn(cookiePreferenceStatusService);
		when(customerServices.getInteractionService()).thenReturn(customerInteractionService);
		when(cookiePreferenceStatusService.updateCookiePreferenceStatus(any())).thenReturn(Boolean.TRUE);
		when(cookiePreferenceServiceModule.deleteSelectedCookies(any(), any())).thenReturn(Boolean.TRUE);
		when(userSession.getUserAgentType()).thenReturn(UserAgentType.WEB);
		PowerMockito.doReturn(customerInteractionEvent).when(Utils.class,"generateCustomerInteractionEvent",any());
		PowerMockito.doReturn("").when(DeviceUtils.class,"getBrowserInfo",any(), any());
		PowerMockito.doNothing().when(customerInteractionService).logFunctionalCookiesActivation(any(),any(),any());

		ajaxActionsController.updateCookiePrivacySettings(requestValue, response);
		PowerMockito.verifyPrivate(ajaxActionsController, times(0)).invoke("registerEvent",requestValue,userSession,"ENABLED","swAdvertising");
	}

	/** PLANES DE RETIRO ****/
	@Test
	public void whenGetRetirementPlans_GivenRetirementPlansNotNull_thenSetResponse() throws Exception {
		retirementPlanResponse = PowerMockito.mock(RetirementPlanBalanceResponse.class);
        //ajaxActionsController.setRetirementPlanResponse(retirementPlanResponse);
        ajaxActionsController.setRetirementPlanClient(client);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getTaxId()).thenReturn("054565663");
        when(retirementPlanResponse.getPlans()).thenReturn(null);
        PowerMockito.whenNew(GetRetirementPlanBalanceFilterParameters.class).withNoArguments().thenReturn(reliusFilter);
        when(client.getRetirementPlanBalance(any())).thenReturn(retirementPlanResponse);

		List<RetirementPlanInfo> plans = new ArrayList<RetirementPlanInfo>();

		RetirementPlanInfo retirementPlan = new RetirementPlanInfo();
		retirementPlan.setPlanid("planIDSrting");
		retirementPlan.setPlannam("501Jones");
		retirementPlan.setErid("erIDString");
		retirementPlan.setPlantypecd("planTypeSrting");

		plans.add(retirementPlan);

		when(retirementPlanResponse.getAcctBal()).thenReturn(java.util.Arrays.asList(new AccountBalance()));
		when(retirementPlanResponse.getPlans()).thenReturn(plans);

        RetirementPlanBalanceResponse response = ajaxActionsController.getRetirementPlans(userSession); // Method to be tested
        assertNotNull(response);
	}

	@Test
	public void whenGetRetirementPlans_GivenRetirementPlansNull_thenSetResponse() throws Exception {
		retirementPlanResponse = PowerMockito.mock(RetirementPlanBalanceResponse.class);
        //ajaxActionsController.setRetirementPlanResponse(retirementPlanResponse);
        ajaxActionsController.setRetirementPlanClient(client);
        when(userSession.getCustomerProfile()).thenReturn(customerProfile);
        when(customerProfile.getTaxId()).thenReturn("054565663");
        when(retirementPlanResponse.getPlans()).thenReturn(null);
        PowerMockito.whenNew(GetRetirementPlanBalanceFilterParameters.class).withNoArguments().thenReturn(reliusFilter);
        when(client.getRetirementPlanBalance(any())).thenReturn(retirementPlanResponse);

		List<RetirementPlanInfo> plans = new ArrayList<RetirementPlanInfo>();

		when(retirementPlanResponse.getAcctBal()).thenReturn(java.util.Arrays.asList(new AccountBalance()));
		when(retirementPlanResponse.getPlans()).thenReturn(plans);

        RetirementPlanBalanceResponse response = ajaxActionsController.getRetirementPlans(userSession); // Method to be tested
        assertNotNull(response);
	}

	/**
	 * When portal
	 * Given ssn doesn't have a retirement plan
	 * Verify customerProfile add entitlement is not called for entitlement with type RetirementPlan
	 * @throws Exception
	 */
	@Test
	public void whenGetRetirementPlans_GivenRetirementPlansNull_thenDoNotSaveRetirementEntitlement() throws Exception {
		retirementPlanResponse = new RetirementPlanBalanceResponse();
		when(userSession.getAccounts()).thenReturn(accounts);
		when(userSession.getLanguage()).thenReturn("en");
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_CIBP)).thenReturn(Boolean.TRUE);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));

		//when(Utils.obtainCisNumberFromPermId(anyString())).thenReturn("12345");

		doReturn(retirementPlanResponse).when(ajaxActionsController).getRetirementPlans(userSession);
		ajaxActionsController.fisRetirementPlanInformation(request, response);
		verify(customerService, times(0)).addEntitlement(any(CustomerProfile.class), any(CustomerEntitlement.class));

	}


	/**
	 * When request Retirement Plan on user profile, this give plans available from user profile
	 * @throws Exception
	 */
	@Test
	public void whenGetRetirementPlans_GivenRetirementPlansAvailable_thenReturnJson() throws Exception {

		retirementPlanResponse = Mockito.spy(new RetirementPlanBalanceResponse());
		//ajaxActionsController.setRetirementPlanResponse(retirementPlanResponse);
		when(userSession.getAccounts()).thenReturn(accounts);
		when(userSession.getLanguage()).thenReturn("en");
		when(accounts.size()).thenReturn(1);
		when(accounts.iterator()).thenReturn(iterator);
		when(customerProfile.getEnrollmentSource()).thenReturn(CustomerEnrollmentSource.WEB);
		when(customerProfile.getBirthDate()).thenReturn(new Date());
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBFIS581_CIBP)).thenReturn(Boolean.TRUE);

		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(userSession.getBankingSession().getPermId()).thenReturn(new BigInteger("12345001001"));

		List<RetirementPlanInfo> plans = new ArrayList<RetirementPlanInfo>();

		RetirementPlanInfo retirementPlan = new RetirementPlanInfo();
		retirementPlan.setPlanid("930098017");
		retirementPlan.setPlannam("HGS Falcon Health Solutions PR, LLC Savings and Ret Plan");
		retirementPlan.setErid("327");
		retirementPlan.setPlantypecd("4");
		retirementPlan.setPlanTotalBalance(new BigDecimal("18369.41"));

		plans.add(retirementPlan);

		when(retirementPlanResponse.getAcctBal()).thenReturn(java.util.Arrays.asList(new AccountBalance()));
		when(retirementPlanResponse.getPlans()).thenReturn(plans);
		when(userSession.isHasRetirementPlan()).thenReturn(Boolean.TRUE);
		when(userSession.getRetirementPlan()).thenReturn(retirementPlanResponse);
		when(userSession.getCustomerProfile()).thenReturn(PowerMockito.mock(CustomerProfile.class));
		doReturn(retirementPlanResponse).when(ajaxActionsController).getRetirementPlans(any());
		final ModelAndView modelAndView = ajaxActionsController.fisRetirementPlanInformation(request, response);
		final String json = (String) modelAndView.getModel().get("json");

		assertViewName(modelAndView, "json");
		assertNotNull(json);
	}



	/**
	 * when whenNewDeviceWithNewUserAgent
	 * Given GivenNewUserAgent
	 * Then ReturnNewModelAndView
	 * Void method
	 * @author ET58344, Evertec Inc.
	 * @since 02-08-2022
	 */
	@Test
	public void whenNewDevice_GivenNewUserAgent_ThenReturnNewModelAndView() throws Exception {

		alertsEntitlement = PowerMockito.mock(AlertsEntitlement.class);
		customerEntitlement = PowerMockito.mock(CustomerEntitlement.class);
		customerSignonStats = PowerMockito.mock(CustomerSignonStats.class);
		customerInteractionEvent = PowerMockito.mock(CustomerInteractionEvent.class);
		smsProfile = PowerMockito.mock(SMSProfile.class);
		customerFacade = PowerMockito.mock(CustomerFacade.class);

		when(WebUtils.getSessionAttribute(any(), any())).thenReturn(userSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(customerProfile.getProfileType()).thenReturn(profileType);
		when(userSession.isDeviceChecked()).thenReturn(Boolean.FALSE);

		GlobalEntitlement globalEnt = new GlobalEntitlement();
		globalEnt.setType(EntitlementType.ALERTS);
		CustomerEntitlement newEntitlement = new CustomerEntitlement();
		newEntitlement.setProfileId(RandomStringUtils.random(4));
		newEntitlement.setId(RandomStringUtils.random(4));
		newEntitlement.setGlobalEntitlement(globalEnt);
		newEntitlement.setCustomerEntitlementStatus(EntitlementStatus.ENABLED);
		newEntitlement.setAttributeData(
				"<properties>\n"
						+ "  <property name=\"ALERT_COUNT\" value=\"1\"/>\n"
						+ "  <property name=\"ALERT_1\" value=\""+AlertType.NEW_DEVICE_PUSH.toString()+"\"/>\n"
						+ "</properties>");

		newEntitlement.setCustomerProfile(customerProfile);

		when(customerProfile.getEntitlementByType(EntitlementType.ALERTS)).thenReturn(newEntitlement);
		when(mockRequest.getCookies()).thenReturn(null);
		when(customerProfile.getSignonStats()).thenReturn(customerSignonStats);

		when(Utils.generateCustomerInteractionEvent(mockRequest)).thenReturn(customerInteractionEvent);
		when(userSession.getSmsProfile(any())).thenReturn(smsProfile);
		when(userSession.getPreferredLanguage()).thenReturn(PreferredLanguage.ENGLISH);
		PowerMockito.doNothing().when(customerService).sendNewDeviceConfirmation(any(), any(), any());
		when(customerSignonStats.getAttributeData()).thenReturn(null);
		when(RandomUtils.generateRandomUniqueString()).thenReturn("null");
		PowerMockito.doNothing().when(CookieUtils.class, "addCookie", response,
				60 * 60 * 24 * 365, DeviceUtils.DEVICE_TOKEN_COOKIE, "token");
		when(mockRequest.getHeader("user-agent")).thenReturn("android");
		when(CustomerFacade.getInstance()).thenReturn(customerFacade);
		PowerMockito.doNothing().when(customerFacade).updateCustomerSignonStats(any(), any());
		ajaxActionsController.newDevice(mockRequest, response);
	}

	@Test
	public void whenGetTelepagoErrorDescriptor_GivenMessageError_ThenReturnMessageErrorSucces() throws Exception {
		ajaxActionsController.setMessageSource(messageSource);
		when(messageSource.getMessage("ebpp.module", null, new Locale("es"))).thenReturn("OK");

		String valueValidate = Whitebox.invokeMethod(ajaxActionsController, "getTelepagoErrorDescriptor", "ebpp.module", "ens","errorCode");
		assertEquals(valueValidate, "OK");
	}
}