package evertec.cibp.web.session;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.test.util.ReflectionTestUtils;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import com.rsa.csd.ws.DeviceIdentifier;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.MobileDevice;

import bppr.tv.beans.BankInformation;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.AccountRecord;
import bppr.tv.beans.accounts.records.CardPlasticAccountRecord;
import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.kiuwan.utils.KiuwanUtils;
import evertec.cibp.core.models.account.AccountFeatures;
import evertec.cibp.core.models.account.CardPlasticFrontend;
import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.customer.CookiePreferenceStatus;
import evertec.cibp.core.models.customer.CustomerAccountPreferences;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.models.customer.CustomerWealthPreferred;
import evertec.cibp.core.models.customer.flags.UserInterface;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.IRetirementPlanEntt;
import evertec.cibp.core.models.entitlement.RetirementPlanEntt;
import evertec.cibp.core.models.postinitialfunding.PIFAccountTypes;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.requests.opac.CISOpacCustomerInfoRequest;
import evertec.cibp.core.services.responses.opac.CISOpacCustomerInfoResponse;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.enums.CookieCategory;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.facade.VaultAccountsFacade;
import evertec.creditcards.model.CustomerIdentifiers;
import evertec.relius.retirementplan.ws.wsdl.RetirementPlanBalanceResponse;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.stmt.OSStmtPDFList;

@RunWith(PowerMockRunner.class)
@PrepareForTest({VaultAccountsFacade.class,CustomerFacade.class, StaticMessageSource.class, CustomerProfile.class, CustomerServices.class,

	             AccountRecord.class, BankInformation.class, OSStmtPDFList.class, TVFrontendAccount.class, KiuwanUtils.class,
	             CISOpacCustomerInfoRequest.class, ApiCallerFactory.class})



public class UserSessionUT {
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private CustomerProfile customerProfile;
	
	@Mock
	private RetirementPlanBalanceResponse retirementPlan;

	@Mock
	private RetirementPlanEntt retirementPlanEntt;
	
	@Mock
	private BankingSession bankingSession;

	@Mock
	private VaultAccountsFacade vaultAccountsFacade;

	@Mock
	private TVFrontendAccount acct;

	@Mock
	private OSStmtPDFList pdf;

	@Mock
	private List<OSStmtPDFList> list;
	@Mock
	private AccountRecord accountRecord;

	@Mock
	private BankInformation bankInformation;

	@Mock
	private DeviceRequest deviceRequest;

	@Mock
	private DeviceIdentifier deviceIdentifier;

	@Mock
	private MobileDevice mobileDevice;
	
	@Mock
	private CISOpacCustomerInfoResponse cisOpacCustomerInfoResponse;

	private UserSession userSession;

	@Mock
	private ApiCaller apiCaller;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

		userSession = PowerMockito.spy(new UserSession());
		PowerMockito.mockStatic(VaultAccountsFacade.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		customerProfile = PowerMockito.mock(CustomerProfile.class);
		customerService = PowerMockito.mock(CustomerServices.class);
		accountRecord = PowerMockito.mock(AccountRecord.class);
		bankInformation = PowerMockito.mock(BankInformation.class);

		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(CustomerProfile.class);
		PowerMockito.mockStatic(CustomerServices.class);
		PowerMockito.mockStatic(AccountRecord.class);
		PowerMockito.mockStatic(BankInformation.class);
		PowerMockito.mockStatic(OSStmtPDFList.class);
		PowerMockito.mockStatic(TVFrontendAccount.class);

		when(userSession.getCustomerService()).thenReturn(customerService);
	}
	
	 @Test
	    public void whenHasRetirementPlanBudges_GivenLoginCountLessThanThreeThenretirementPlanAccountNewBadgeTrue() throws Exception {
		 

		 when(userSession.getCustomerProfile()).thenReturn(customerProfile);
				 
		 when(customerProfile.getEntitlementByType(EntitlementType.RETIREMENT_PLAN)).thenReturn(retirementPlanEntt);
		 
		 when(retirementPlanEntt.getInt(IRetirementPlanEntt.LOGIN_COUNT)).thenReturn(1);
		 
		 PowerMockito.doNothing().when(customerService).updateEntitlement(customerProfile, retirementPlanEntt);
		 
		 userSession.setRetirementPlan(retirementPlan);
		 userSession.setHasRetirementPlan(true);
		 
		 boolean hasBadge = userSession.hasRetirementPlanBudge();
		 
		assertTrue( hasBadge );	 
	 }
	 
	 @Test
	    public void whenHasRetirementPlanBudges_GivenLoginCountEqualThreeThenretirementPlanAccountNewBadgeFalse() throws Exception {
		 
		 Whitebox.setInternalState(userSession, "updateLoginCount", Boolean.FALSE);		 

		 when(userSession.getCustomerProfile()).thenReturn(customerProfile);
				 
		 when(customerProfile.getEntitlementByType(EntitlementType.RETIREMENT_PLAN)).thenReturn(retirementPlanEntt);
		 
		 when(retirementPlanEntt.getInt(IRetirementPlanEntt.LOGIN_COUNT)).thenReturn(3);
		 
		 PowerMockito.doNothing().when(customerService).updateEntitlement(customerProfile, retirementPlanEntt);
		 
		 userSession.setRetirementPlan(retirementPlan);
		 
		 boolean hasBadge = userSession.hasRetirementPlanBudge();
		 
		assertFalse( hasBadge );	 
	 }

	 @Test
	    public void whenHasRetirementPlanBudges_GivenLoginCountEqualThreeAndUpdateLoginCountTrueThenretirementPlanAccountNewBadgeFalse() throws Exception {
		 
		 Whitebox.setInternalState(userSession, "updateLoginCount", Boolean.TRUE);

		 when(userSession.getCustomerProfile()).thenReturn(customerProfile);
				 
		 when(customerProfile.getEntitlementByType(EntitlementType.RETIREMENT_PLAN)).thenReturn(retirementPlanEntt);
		 
		 when(retirementPlanEntt.getInt(IRetirementPlanEntt.LOGIN_COUNT)).thenReturn(3);
		 
		 PowerMockito.doNothing().when(customerService).updateEntitlement(customerProfile, retirementPlanEntt);
		 
		 userSession.setRetirementPlan(retirementPlan);
		 
		 boolean hasBadge = userSession.hasRetirementPlanBudge();
		 
		assertFalse( hasBadge );	 
	 }
		 
	 @Test
	    public void whenHasRetirementPlanBudges_GivenCustomerProfileNullThenretirementPlanAccountNewBadgeFalse() throws Exception {
		 

		 when(userSession.getCustomerProfile()).thenReturn(null);
				 
		 when(customerProfile.getEntitlementByType(EntitlementType.RETIREMENT_PLAN)).thenReturn(retirementPlanEntt);
		 
		 when(retirementPlanEntt.getInt(IRetirementPlanEntt.LOGIN_COUNT)).thenReturn(1);
		 
		 PowerMockito.doNothing().when(customerService).updateEntitlement(customerProfile, retirementPlanEntt);
		 
		 userSession.setRetirementPlan(retirementPlan);
		 
		 boolean hasBadge = userSession.hasRetirementPlanBudge();
		 
		assertFalse( hasBadge );	 
	 }

	/**
	 * when IsCookieCategoryEnabled
	 * Given CookiePreferenceStatusListNull
	 * Then ReturnTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenIsCookieCategoryEnabled_GivenCookiePreferenceStatusListNull_ThenReturnTrue () throws Exception {

		when(userSession.getCookiePreferenceStatusList()).thenReturn(null);

		boolean result = userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS);
		assertFalse(result);
	}

	/**
	 * when IsCookieCategoryEnabled
	 * Given CookiePreferenceStatusListNotNull
	 * Then ReturnTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenIsCookieCategoryEnabled_GivenCookiePreferenceStatusListNotNull_ThenReturnTrue () throws Exception {

		List<CookiePreferenceStatus> listCookies = new ArrayList<>();
		CookiePreferenceStatus cookiePreferenceStatus = new CookiePreferenceStatus();
		cookiePreferenceStatus.setCategory(CookieCategory.ANALYTICS.toString());
		cookiePreferenceStatus.setStatus("ENABLED");
		listCookies.add(cookiePreferenceStatus);

		cookiePreferenceStatus = new CookiePreferenceStatus();
		cookiePreferenceStatus.setCategory(CookieCategory.ADVERTISING.toString());
		cookiePreferenceStatus.setStatus("ENABLED");
		listCookies.add(cookiePreferenceStatus);

		when(userSession.getCookiePreferenceStatusList()).thenReturn(listCookies);

		boolean result = userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS);
		assertTrue(result);
	}

	/**
	 * when IsCookieCategoryEnabled
	 * Given CookiePreferenceStatusListNull
	 * Then ReturnTrue
	 *
	 * @author Evertec ET58344
	 * @since 09-29-2020
	 * @exception Exception error
	 */
	@Test
	public void whenIsCookieCategoryEnabled_GivenCookiePreferenceStatusListEmpty_ThenReturnTrue () throws Exception {

		when(userSession.getCookiePreferenceStatusList()).thenReturn(new ArrayList<>());

		boolean result = userSession.isCookieCategoryEnabled(CookieCategory.ANALYTICS);
		assertFalse(result);
	}
	 
	 @Test(expected = ProgrammingError.class)
	    public void whenGetPremiaAccounts_GivenCustomerProfileNullThenThrowProgramingException() throws ProgrammingError {

		 when(userSession.getCustomerProfile()).thenReturn(null);
		 List<TVFrontendAccount> premiaAccounts = userSession.getPremiaAccounts();

	 }

	 @Test(expected = ProgrammingError.class)
	    public void whenGetPremiaAccounts_GivenBankingSessionNullThenThrowProgramingException() throws ProgrammingError {

		 when(userSession.getBankingSession()).thenReturn(null);
		 List<TVFrontendAccount> premiaAccounts = userSession.getPremiaAccounts();

	 }

	 @Test
	    public void whenGetPremiaAccounts_GivenValidProfileThenGetPremiaAccounts() throws ProgrammingError {


		 List<TVFrontendAccount> premiaAccounts = new LinkedList<>();

		 userSession.setCustomerProfile(customerProfile);
		 userSession.setBankingSession(bankingSession);
		 userSession.setCustomerService(customerService);
		 when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		 when(userSession.getBankingSession()).thenReturn(bankingSession);
		 when(VaultAccountsFacade.getInstance()).thenReturn(vaultAccountsFacade);
		 when(customerService.getPremiaProducts()).thenReturn(getPremiaProductsList());
		 when(vaultAccountsFacade.getAccountsFilteredByProductsId(any(), any())).thenReturn(premiaAccounts);
		 when(customerService.getPremiaGhostProductId()).thenReturn("PLNOC");

		 premiaAccounts = userSession.getPremiaAccounts();

		 assertTrue(premiaAccounts.isEmpty());

	 }

	 @Test
	    public void whenGetPremiaAccounts_GivenAlreadyPremiaAccounts() throws ProgrammingError {

		 List<TVFrontendAccount> premiaAccounts = new LinkedList<>();

		 TVFrontendAccount account = mock(TVFrontendAccount.class);
		 TVAccountProductDefinition  productDefinition = mock(TVAccountProductDefinition.class);
		 when(account.getProductDefinition()).thenReturn(productDefinition);
		 when(productDefinition.getProductType()).thenReturn("CCA");
		 when(productDefinition.getProductStatus()).thenReturn("UNDEFINED");
		 when(productDefinition.getProductId()).thenReturn("PLNOC");
		 premiaAccounts.add(account);
		 userSession.setCustomerProfile(customerProfile);
		 userSession.setBankingSession(bankingSession);
		 userSession.setCustomerService(customerService);
		 when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		 when(userSession.getBankingSession()).thenReturn(bankingSession);
		 when(VaultAccountsFacade.getInstance()).thenReturn(vaultAccountsFacade);
		 when(customerService.getPremiaProducts()).thenReturn(getPremiaProductsList());
		 when(vaultAccountsFacade.getAccountsFilteredByProductsId(any(), any())).thenReturn(premiaAccounts);
		 when(customerService.getPremiaGhostProductId()).thenReturn("PLNOC");

		 premiaAccounts = userSession.getPremiaAccounts();
		 premiaAccounts = userSession.getPremiaAccounts();

		 assertTrue(!premiaAccounts.isEmpty());

	 }

	 @Test
	    public void whengetFilteredAccounts_GivenFilteredAccounts() throws ProgrammingError {

		 List<TVFrontendAccount> premiaAccounts = new LinkedList<>();
		 premiaAccounts.add(new TVFrontendAccount("CCA", "1231231", "CIBP"));
		 userSession.setCustomerService(customerService);
		 when(customerService.getFilteredAccountsByProductId(any(), any(), any())).thenReturn(premiaAccounts);

		 premiaAccounts = userSession.getFilteredAccounts(getGhostProductsList());

		 assertTrue(!premiaAccounts.isEmpty());

	 }
	 
//	 @Test
//	 public void whenGetPayrollAppStatus_GivenStatusPayrollAppEqualNull_ThenGetCustomerApplicationsIsCalled() {
//		 userSession.setPayrollAppStatus(null);
//		 
//		 when(CustomerFacade.getInstance()).thenReturn(customerFacade);
//		 
//		 userSession.getPayrollAppStatus();
//		 
//		 verify(customerFacade, times(1)).getCustomerApplications(any(), any());
//	 }
//	 
//	 @Test
//	 public void whenGetPayrollAppStatus_GivenStatusPayrollAppEqualNotNull_ThenGetCustomerApplicationsIsNotCalled() {
//		 userSession.setPayrollAppStatus(ApplicationStatus.APPROVED);
//		 
//		 when(CustomerFacade.getInstance()).thenReturn(customerFacade);
//		 
//		 userSession.getPayrollAppStatus();
//		 
//		 verify(customerFacade, times(0)).getCustomerApplications(any(), any());
//		 
//	 }

	 /**
	 * Get a list of dummy ghost accounts.
	 * @return
	 */
	private List<String> getPremiaProductsList() {

		return Stream.of("VPPRE", "APAXI", "APREG", "MWREG",
				"UAREG", "UVVSI", "UVREG", "VWVSI", "VWREG",
				"VBSG1", "VBSG2", "VBSEC", "MCEDG", "UVNOV",
				"VSNOV", "VSELT", "VSICN", "VSSEC", "VWNOS",
				"VBVBR", "VBAFP", "VBAFE", "PLNOC").collect(Collectors.toList());
		}

	/**
	 * Get a list of dummy ghost accounts.
	 * @return
	 */
	private List<String> getGhostProductsList() {

		return Stream.of("PLNOC").collect(Collectors.toList());
		}
	
	@Test
    public void whenGetCcaCardPlastics_CcaCardPlasticsIsEmptyOrNull() throws ProgrammingError{
		List<TVFrontendAccount> ccaCardPlastics = new LinkedList<>();

		 TVFrontendAccount account = mock(TVFrontendAccount.class);
		 TVAccountProductDefinition  productDefinition = mock(TVAccountProductDefinition.class);
		 when(account.getProductDefinition()).thenReturn(productDefinition);
		 when(productDefinition.getProductType()).thenReturn("CCA");
		 when(productDefinition.getProductStatus()).thenReturn("UNDEFINED");
		 when(productDefinition.getProductId()).thenReturn("PLNOC");
		 ccaCardPlastics.add(account);
		 userSession.setCustomerProfile(customerProfile);
		 userSession.setBankingSession(bankingSession);
		 userSession.setCustomerService(customerService);
		 when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		 when(userSession.getBankingSession()).thenReturn(bankingSession);
		 when(VaultAccountsFacade.getInstance()).thenReturn(vaultAccountsFacade);

		 ccaCardPlastics = userSession.getCcaCardPlastics();

		 assertTrue(ccaCardPlastics.isEmpty());
	}
	
	@Test
	public void whenGetCcaCardPlastics_CcaCardPlasticsNotEmptyOrNotNull() throws ProgrammingError {
		List<TVFrontendAccount> ccaCardPlastics;

		TVAccountProductDefinition productDefinition1 = new TVAccountProductDefinition();
		productDefinition1.setId("152276297|IDAC");
		productDefinition1.setProductDescription("Multicuenta");
		productDefinition1.setProductType("IDA");
		productDefinition1.setProductStatus("ACTIVE");
		productDefinition1.setProductStatusCode("A");

		TVAccountProductDefinition productDefinition2 = new TVAccountProductDefinition();
		productDefinition2.setId("010018336|IDAC");
		productDefinition2.setProductDescription("Multicuenta Staff");
		productDefinition2.setProductType("IDA");
		productDefinition2.setProductStatus("ACTIVE");
		productDefinition2.setProductStatusCode("A");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA", "152276297", "C");
		frontendAccount.setGuardianOrRepPayeeAcc(true);
		frontendAccount.setProductDefinition(productDefinition1);

		TVFrontendAccount frontendAccount1 = new TVFrontendAccount("IDA", "010018336", "C");
		frontendAccount1.setGuardianOrRepPayeeAcc(false);
		frontendAccount1.setProductDefinition(productDefinition2);

		ccaCardPlastics = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		ccaCardPlastics.add(frontendAccount);
		ccaCardPlastics.add(frontendAccount1);

		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		userSession.setBankingSession(bankingSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);

		when(customerService.getCardPlastics(customerProfile, bankingSession, true)).thenReturn(ccaCardPlastics);

		userSession.updateCcaCardPlastics();

		assertEquals(ccaCardPlastics, userSession.getCcaCardPlastics());
	}
		
	@Test(expected = ProgrammingError.class)
	public void whenGetCcaCardPlastics_GivenCustomerProfileNullThenThrowProgramingException() throws ProgrammingError {

		when(userSession.getCustomerProfile()).thenReturn(null);
		List<TVFrontendAccount> ccaCardPlastics = userSession.getCcaCardPlastics();

	}

	@Test(expected = ProgrammingError.class)
	public void whenGetCcaCardPlastics_GivenBankingSessionNullThenThrowProgramingException() throws ProgrammingError {

		when(userSession.getBankingSession()).thenReturn(null);
		List<TVFrontendAccount> ccaCardPlastics = userSession.getCcaCardPlastics();

	}
	
	@Test
	public void whenUpdateGetAccounts_GivenFilteredAccounts() throws ProgrammingError {

		List<TVFrontendAccount> ccaCardPlastics = new LinkedList<>();
		ccaCardPlastics.add(new TVFrontendAccount("CCA", "1231231", "CIBP"));
		userSession.setCustomerService(customerService);

		ccaCardPlastics = userSession.updateCcaCardPlastics();

		assertTrue(ccaCardPlastics.isEmpty());

	}
	
	@Mock
	private CustomerWealthPreferred mCustomerWealthPreferred;
	
	@Test
	public void whenIsPremiumBanking_giveUserPremiumBanking_thenResultTrue () {
		String premiumBanking = "1:4";
		String wealthCodes = "1,2,3,4,5,6";
		String preferredCodes = "1,2,3,4,5,6";
		
		List<String> userCodes = new ArrayList() {{
			add("1"); add("2"); add("3");
		}};
		
		when(mCustomerWealthPreferred.getCodes()).thenReturn(userCodes);
		Mockito.doReturn(mCustomerWealthPreferred).when(userSession).getCustomerWealthPreferred(any(), any());
		
		boolean result = userSession.isPremiumBanking(premiumBanking, wealthCodes, preferredCodes);
		assertTrue(result);
	}
	
	@Test
	public void whenIsPremiumBanking_giveUserNotPremiumBanking_thenResultFalse () {
		String premiumBanking = "5:4";
		String wealthCodes = "1,2,3,4,5,6";
		String preferredCodes = "1,2,3,4,5,6";
		
		List<String> userCodes = new ArrayList() {{
			add("1"); add("2"); add("3");
		}};
		
		when(mCustomerWealthPreferred.getCodes()).thenReturn(userCodes);
		Mockito.doReturn(mCustomerWealthPreferred).when(userSession).getCustomerWealthPreferred(any(), any());
		
		boolean result = userSession.isPremiumBanking(premiumBanking, wealthCodes, preferredCodes);
		assertFalse(result);
	}
	
	@Test
	public void whenIsPremiumBanking_giveUserOnePremiumBanking_thenResultTrue () {
		String premiumBanking = "300";
		String wealthCodes = "1,2,3,4,5,6";
		String preferredCodes = "1,2,3,4,5,6";
		
		List<String> userCodes = new ArrayList() {{
			add("300"); add("2"); add("3");
		}};
		
		when(mCustomerWealthPreferred.getCodes()).thenReturn(userCodes);
		Mockito.doReturn(mCustomerWealthPreferred).when(userSession).getCustomerWealthPreferred(any(), any());
		
		boolean result = userSession.isPremiumBanking(premiumBanking, wealthCodes, preferredCodes);
		assertTrue(result);
	}
	
	@Test
	public void whenIsPremiumBanking_giveNullPremiumCodes_thenResultFalse () {
		String premiumBanking = null;
		String wealthCodes = "1,2,3,4,5,6";
		String preferredCodes = "1,2,3,4,5,6";
		
		List<String> userCodes = new ArrayList() {{
			add("300"); add("2"); add("3");
		}};
		
		when(mCustomerWealthPreferred.getCodes()).thenReturn(userCodes);
		Mockito.doReturn(mCustomerWealthPreferred).when(userSession).getCustomerWealthPreferred(any(), any());
		
		boolean result = userSession.isPremiumBanking(premiumBanking, wealthCodes, preferredCodes);
		assertFalse(result);
	}
	
	@Test
	public void whenIsPremiumBanking_giveNullUserCodes_thenResultFalse () {
		String premiumBanking = null;
		String wealthCodes = "1,2,3,4,5,6";
		String preferredCodes = "1,2,3,4,5,6";
		
		List<String> userCodes = null;
		
		when(mCustomerWealthPreferred.getCodes()).thenReturn(userCodes);
		Mockito.doReturn(mCustomerWealthPreferred).when(userSession).getCustomerWealthPreferred(any(), any());
		
		boolean result = userSession.isPremiumBanking(premiumBanking, wealthCodes, preferredCodes);
		assertFalse(result);
	}
	
	@Test
	public void whenIsPremiumBanking_giveNullObjectUserCodes_thenResultFalse () {
		String premiumBanking = null;
		String wealthCodes = "1,2,3,4,5,6";
		String preferredCodes = "1,2,3,4,5,6";
		
		Mockito.doReturn(null).when(userSession).getCustomerWealthPreferred(any(), any());
		
		boolean result = userSession.isPremiumBanking(premiumBanking, wealthCodes, preferredCodes);
		assertFalse(result);
	}

	@Test
	public void when_CallsetCustomerProfile_given_IfxErrorTrue_Then_UserSessiongetCustomerProfileIfxError_IsTrue(){
		CustomerProfile cp = new CustomerProfile();
		cp.setIfxError(true);

		userSession.setCustomerProfile(cp);
		assertTrue(userSession.getCustomerProfile().isIfxError());

	}

	@Test
	public void when_CallsetCustomerProfile_given_IfxErrorFalse_Then_UserSessiongetCustomerProfileIfxError_IsFalse(){
		CustomerProfile cp = new CustomerProfile();

		userSession.setCustomerProfile(cp);
		assertFalse(userSession.getCustomerProfile().isIfxError());

	}

	@Test
	public void when_CallsetCustomerProfile_given_CustomerProfile_IsNull_Then_UserSessiongetCustomerProfile_IsNull(){
		CustomerProfile cp = null;

		userSession.setCustomerProfile(cp);
		assertNull(userSession.getCustomerProfile());
	}

	@Test
	public void whenGetMobileSdkData_thenReturnString() {
		PowerMockito.mockStatic(KiuwanUtils.class);
		DeviceIdentifier[] deviceIdentifiers = {deviceIdentifier};

		when(userSession.getDeviceRequest()).thenReturn(deviceRequest);
		when(deviceRequest.getDeviceIdentifier()).thenReturn(deviceIdentifiers);
		when(KiuwanUtils.cast(any(), any())).thenReturn(mobileDevice);
		when(mobileDevice.getMobileSdkData()).thenReturn(StringUtils.EMPTY);

		String result = userSession.getMobileSdkData();
		assertNotNull(result);
	}

	@Test
	public void whengetAcctAllPDFStmt_GivenezStatementInformativaProductIdsNotNull_ThenReturnListNotNull() throws Exception
	{
		String[] ezStatementInformativaProductIds = {"objeto1"};
		userSession.setEzStatementInformativaProductIds(ezStatementInformativaProductIds);
		when(pdf.getProductId()).thenReturn("ProductID");
		when(acct.getAccountNumber()).thenReturn("numerodecuenta");
		when(acct.getAccountNumber().substring(acct.getAccountNumber().length() - 4)).thenReturn("numerodecuenta");
		when(acct.getNickname()).thenReturn("Nickname");
		when(pdf.getPdfId()).thenReturn("PdfId");
		when(pdf.getStmtDate()).thenReturn("fecha");
		when(pdf.getProductId()).thenReturn("ProductID");
		when(list.size()).thenReturn(1);
		when(list.get(0)).thenReturn(pdf);


		Whitebox.invokeMethod(userSession, "getAcctAllPDFStmt",  list, acct);
	}


	@Test
	public void whengetAcctAllPDFStmt_GivenezStatementInformativaProductIdsAsNull_ThenReturnListNotNull() throws Exception
	{
		String[] ezStatementInformativaProductIds = null;
		userSession.setEzStatementInformativaProductIds(ezStatementInformativaProductIds);
		when(pdf.getProductId()).thenReturn("ProductID");
		when(acct.getAccountNumber()).thenReturn("numerodecuenta");
		when(acct.getAccountNumber().substring(acct.getAccountNumber().length() - 4)).thenReturn("numerodecuenta");
		when(acct.getNickname()).thenReturn("Nickname");
		when(pdf.getPdfId()).thenReturn("PdfId");
		when(pdf.getStmtDate()).thenReturn("fecha");
		when(pdf.getProductId()).thenReturn("ProductID");
		when(list.size()).thenReturn(1);
		when(list.get(0)).thenReturn(pdf);


		Whitebox.invokeMethod(userSession, "getAcctAllPDFStmt",  list, acct);
	}
	
	@Test
	public void whenSetCcaCustomerIdentifierInfo_givenCustomerIdentifier () throws Exception {
		
		CustomerIdentifiers identifiers = new CustomerIdentifiers();
		identifiers.setAccountId("00000037826");
		identifiers.setCustomerId("000054774");
		
		userSession.setCcaCustomerIdentifierInfo("4549053282005619", identifiers);
		
		assertTrue(userSession.getCcaCustomerIdentifierMap().size()>0);
		
	}

	@Test
	public void whenGetCountry_GivenNullBankingSession_ThenReturnEmptyCountry() {
		String country = userSession.getCountry();
		
		assertTrue(country.isEmpty());
	}
	
	@Test
	public void whenGetCountry_GivenNotNullCountry_ThenReturnNotNullCountry() {
		String prCountry = "PR";
		ReflectionTestUtils.setField(userSession, "country", prCountry);
		String country = userSession.getCountry();
		
		assertNotNull(country);
	}
	
	@Test
	public void whenGetCountry_GivenNullPermId_ThenReturnEmptyCountry() {
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(bankingSession.getPermId()).thenReturn(null);
		
		String country = userSession.getCountry();
		
		assertTrue(country.isEmpty());
	}
	
	@Test
	public void whenGetCountry_GivenValidPermId_ThenReturnValidCountry() throws Exception {
		BigInteger permId = new BigInteger("1591831001001");
		String prCountry = "PR";
		CISOpacCustomerInfoResponse cisResponse = new CISOpacCustomerInfoResponse();
    	cisResponse.setCountry(prCountry);
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(bankingSession.getPermId()).thenReturn(permId);
		when(customerService.getCISCustomerInfo(any())).thenReturn(cisResponse);
		
		String country = userSession.getCountry();
		
		assertEquals(prCountry, country);
	}
	
	@Test
	public void whenGetCountry_GivenValidPermIdAndNullCisReponse_ThenReturnEmptyCountry() throws Exception {
		BigInteger permId = new BigInteger("1591831001001");
		
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(bankingSession.getPermId()).thenReturn(permId);
		when(customerService.getCISCustomerInfo(any())).thenReturn(null);
		
		String country = userSession.getCountry();
		
		assertTrue(country.isEmpty());
	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_then_PortalAccount_HasRepPayeeAccounts() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.TRUE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE665)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);

		userSession.clasifyAccounts(getUserAccounts(), Boolean.TRUE);

		assertEquals(2, userSession.getPortalAccounts().size());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_ATHCardPlastic_then_PortalAccount_No_HasRepPayeeAccounts() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE213)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);

		userSession.clasifyAccounts(getUserAccounts(), Boolean.TRUE);

		assertEquals(1, userSession.getPortalAccounts().size());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_ServiceAcctInq_False_then_PortalAccount_No_HasRepPayeeAccounts() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE213)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);

		userSession.clasifyAccounts(getUserAccounts(), Boolean.FALSE);

		assertEquals(2, userSession.getPortalAccounts().size());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_UserAccountNull_then_NoAccounts() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);

		userSession.clasifyAccounts(null, Boolean.FALSE);

		assertEquals(0, userSession.getPortalAccounts().size());

	}


	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_StatusCode_N_then_PendingCloseAccounts() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		Account account = mock(Account.class);
		when(account.getRecords()).thenReturn(accountRecord);

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276298|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductType("IDA");
		pd.setProductStatusCode("N");

		TVFrontendAccount fa = new TVFrontendAccount("IDA", "152276298", "S");
		fa.setGuardianOrRepPayeeAcc(false);
		fa.setProductDefinition(pd);
		fa.setBackendObject(account);

		List<TVFrontendAccount> userAccounts;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(fa);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE213)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(accountRecord.getStatusCode()).thenReturn("N");

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertEquals(1, userSession.getPendingCloseAccounts().size());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_IDA_then_athmAccounts() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		AccountFeatures accountFeatures = new AccountFeatures();
		accountFeatures.setPayFrom(true);
		AccountFeatures af = new AccountFeatures();
		af.setPayFrom(false);

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();
		accountPreferences.setHide(true);

		TVAccountProductDefinition pd1 = new TVAccountProductDefinition();
		TVAccountProductDefinition pd2 = new TVAccountProductDefinition();
		TVAccountProductDefinition pd3 = new TVAccountProductDefinition();
		TVAccountProductDefinition pd4 = new TVAccountProductDefinition();
		TVAccountProductDefinition pd5 = new TVAccountProductDefinition();

		TVFrontendAccount fa = new TVFrontendAccount("IDA", "010018336", "C");
		fa.setProductDefinition(pd1);
		fa.setFeatures(accountFeatures);

		TVFrontendAccount fa1 = new TVFrontendAccount("IDA", "010018337", "C");
		fa1.setProductDefinition(pd2);
		fa1.setFeatures(af);

		TVFrontendAccount fa2 = new TVFrontendAccount("IDA", "010018338", "C");
		fa2.setProductDefinition(pd3);
		fa2.setCustomerAccountPreferences(accountPreferences);

		TVFrontendAccount fa3 = new TVFrontendAccount("IDA", "010018338", "C");
		fa3.setProductDefinition(pd4);

		TVFrontendAccount fa4 = new TVFrontendAccount("CCA", "010018336", "C");
		fa4.setProductDefinition(pd5);
		fa4.setFeatures(accountFeatures);


		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(fa);
		userAccounts.add(fa1);
		userAccounts.add(fa2);
		userAccounts.add(fa3);
		userAccounts.add(fa4);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE213)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertEquals(1, userSession.getAthmAccounts().size());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_ATHCardPlastic_then_PortalAccount_HasRepPayeeAccounts() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE213)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();
		CustomerAccountPreferences accountPreferences1 = new CustomerAccountPreferences();
		accountPreferences1.setId("1");

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setProductId("001");
		TVAccountProductDefinition pd1 = new TVAccountProductDefinition();
		pd1.setProductId("002");
		TVAccountProductDefinition pd2 = new TVAccountProductDefinition();
		pd2.setProductId("003");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setGuardianOrRepPayeeAcc(true);
		frontendAccount.setGuardianAcc(true);
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);

		TVFrontendAccount frontendAccount1 = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount1.setGuardianOrRepPayeeAcc(true);
		frontendAccount1.setGuardianAcc(true);
		frontendAccount1.setProductDefinition(pd1);

		TVFrontendAccount frontendAccount2 = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount2.setGuardianOrRepPayeeAcc(true);
		frontendAccount2.setGuardianAcc(true);
		frontendAccount2.setCustomerAccountPreferences(accountPreferences1);
		frontendAccount2.setProductDefinition(pd2);

		List<TVFrontendAccount> userAccounts;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);
		userAccounts.add(frontendAccount1);
		userAccounts.add(frontendAccount2);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertEquals(3, userSession.getPortalGuardianAccounts().size());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_INS_And_SEC_And_PRM_then_SecInsPRM() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		TVFrontendAccount fa = getAccount("010018336|IDAC", "SEC", "1", "010018336", "C");
		TVFrontendAccount fa1 = getAccount("010018337|IDAC", "INS", "2", "010018337", "C");
		TVFrontendAccount fa2 = getAccount("010018338|IDAC", "PRM", "3", "010018338", "C");

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(fa);
		userAccounts.add(fa1);
		userAccounts.add(fa2);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE213)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertEquals(2, userSession.getPortalSecinsAccounts().size());
		assertEquals(1, userSession.getPortalProgramAccounts().size());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_MBSFE665_then_CCAAccounts() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);
		//userSession.setOnOffMinimumEligibleAge(17);

		Account account = mock(Account.class);
		when(account.getAccountID()).thenReturn("");

		CardPlasticAccountRecord cardPlasticAccountRecord = new CardPlasticAccountRecord();
		cardPlasticAccountRecord.setCardPANId(RandomStringUtils.randomNumeric(16));
		cardPlasticAccountRecord.setAccount(account);
		CardPlasticFrontend cardPlasticFrontend = new CardPlasticFrontend(cardPlasticAccountRecord);
		CardPlasticFrontend[] ccaPlastics = new CardPlasticFrontend[NumberUtils.INTEGER_ONE];
		ccaPlastics[NumberUtils.INTEGER_ZERO] = cardPlasticFrontend;

		TVAccountProductDefinition pd1 = new TVAccountProductDefinition();
		pd1.setId("4549525727367977ATH|CCA");
		pd1.setProductDescription("Multicuenta Staff");
		pd1.setProductType("CCA");
		pd1.setProductStatus("ACTIVE");
		pd1.setProductStatusCode("A");
		pd1.setProductId("1");

		TVFrontendAccount fa = new TVFrontendAccount("CCA", "4549525727367977ATH", "");
		fa.setGuardianOrRepPayeeAcc(false);
		fa.setProductDefinition(pd1);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(fa);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE665)).thenReturn(Boolean.TRUE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.CCA_ONOFF)).thenReturn(Boolean.TRUE);
		when(Utils.getCCACardPlastics(customerService, any(), any(), any())).thenReturn(ccaPlastics);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.MONTH, 11);
		calendar.set(Calendar.YEAR, 1999);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		when(customerProfile.getBirthDate()).thenReturn(calendar.getTime());

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertEquals(1, userSession.getCcaAccounts().size());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_MBSFE665_AndATH_ONOFF_then_CCAAccounts() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);
		//userSession.setOnOffMinimumEligibleAge(17);

		Account account = mock(Account.class);
		when(account.getAccountID()).thenReturn("");

		CardPlasticAccountRecord cardPlasticAccountRecord = new CardPlasticAccountRecord();
		cardPlasticAccountRecord.setCardPANId(RandomStringUtils.randomNumeric(16));
		cardPlasticAccountRecord.setAccount(account);
		CardPlasticFrontend cardPlasticFrontend = new CardPlasticFrontend(cardPlasticAccountRecord);
		CardPlasticFrontend[] ccaPlastics = new CardPlasticFrontend[NumberUtils.INTEGER_ONE];
		ccaPlastics[NumberUtils.INTEGER_ZERO] = cardPlasticFrontend;

		TVAccountProductDefinition pd1 = new TVAccountProductDefinition();
		pd1.setId("4549525727367977ATH|CCA");
		pd1.setProductDescription("Multicuenta Staff");
		pd1.setProductType("CCA");
		pd1.setProductStatus("ACTIVE");
		pd1.setProductStatusCode("A");
		pd1.setProductId("1");

		TVFrontendAccount fa = new TVFrontendAccount("CCA", "4549525727367977ATH", "");
		fa.setGuardianOrRepPayeeAcc(false);
		fa.setProductDefinition(pd1);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(fa);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSFE665)).thenReturn(Boolean.TRUE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.ATH_ONOFF)).thenReturn(Boolean.TRUE);
		when(Utils.getCCACardPlastics(customerService, any(), any(), any())).thenReturn(ccaPlastics);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.MONTH, 11);
		calendar.set(Calendar.YEAR, 2015);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		when(customerProfile.getBirthDate()).thenReturn(calendar.getTime());

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertEquals(1, userSession.getCcaAccounts().size());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_BankId002_then_hasViAccount() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		Account account = mock(Account.class);
		when(account.getBankInformation()).thenReturn(bankInformation);

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276298|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductType("IDA");

		TVFrontendAccount fa = new TVFrontendAccount("IDA", "152276298", "S");
		fa.setGuardianOrRepPayeeAcc(false);
		fa.setProductDefinition(pd);
		fa.setBackendObject(account);

		List<TVFrontendAccount> userAccounts;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(fa);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(bankInformation.getBankId()).thenReturn("002");

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertTrue(userSession.hasVirginIslandAccount());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_BankId002_then_hasBViAccount() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		Account account = mock(Account.class);
		when(account.getBankInformation()).thenReturn(bankInformation);

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276298|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductType("IDA");

		TVFrontendAccount fa = new TVFrontendAccount("IDA", "152276298", "S");
		fa.setGuardianOrRepPayeeAcc(false);
		fa.setProductDefinition(pd);
		fa.setBackendObject(account);

		List<TVFrontendAccount> userAccounts;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(fa);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(bankInformation.getBankId()).thenReturn("004");

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertTrue(userSession.hasBritishVirginIslandAccount());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_Commercial_then_HasCommercial() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		Account account = mock(Account.class);
		when(account.getRecords()).thenReturn(accountRecord);
		when(account.getBankInformation()).thenReturn(bankInformation);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.COMMERCIAL);
		when(accountRecord.getProductId()).thenReturn("081");
		when(bankInformation.getBankId()).thenReturn("001");
		when(customerProfile.getEnrollmentAccountNumber()).thenReturn("");

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276297|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductType("IDA");
		pd.setProductStatus("ACTIVE");
		pd.setProductStatusCode("A");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);
		frontendAccount.setBackendObject(account);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertTrue(userSession.isComercialCustomer());
		assertTrue(userSession.isHasEAccounts());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_Commercial_then_NoHasEAccounts() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		Account account = mock(Account.class);
		when(account.getRecords()).thenReturn(accountRecord);
		when(account.getBankInformation()).thenReturn(bankInformation);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.COMMERCIAL);
		when(accountRecord.getProductId()).thenReturn("0800");
		when(bankInformation.getBankId()).thenReturn("001");
		when(customerProfile.getEnrollmentAccountNumber()).thenReturn("");

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276297|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductType("IDA");
		pd.setProductStatus("ACTIVE");
		pd.setProductStatusCode("A");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);
		frontendAccount.setBackendObject(account);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertFalse(userSession.isHasEAccounts());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_NoProductID_then_NoHasEAccounts() throws Exception {

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(new PIFAccountTypes());
		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		Account account = mock(Account.class);
		when(account.getRecords()).thenReturn(accountRecord);
		when(account.getBankInformation()).thenReturn(bankInformation);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(bankInformation.getBankId()).thenReturn("001");
		when(customerProfile.getEnrollmentAccountNumber()).thenReturn("456");

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276297|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductType("IDA");
		pd.setProductStatus("ACTIVE");
		pd.setProductStatusCode("A");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);
		frontendAccount.setBackendObject(account);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertFalse(userSession.isHasEAccounts());

	}


	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_MBSD1750True_then_interbankFundingAllowed() throws Exception {

		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		PIFAccountTypes pifAccountTypes = mock(PIFAccountTypes.class);

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(pifAccountTypes);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(pifAccountTypes.getAddOption()).thenReturn(1);
		when(pifAccountTypes.getAccountSubtype()).thenReturn("IDA");
		when(pifAccountTypes.getAccountId()).thenReturn("1");

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276297|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductType("IDA");
		pd.setProductStatus("ACTIVE");
		pd.setProductStatusCode("A");
		pd.setProductId("1");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertTrue(userSession.getInterbankFundingAllowed());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_MBSD1750True_AndProductType_CCA_then_interbankFundingAllowed() throws Exception {

		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		PIFAccountTypes pifAccountTypes = mock(PIFAccountTypes.class);

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(pifAccountTypes);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(pifAccountTypes.getAddOption()).thenReturn(1);
		when(pifAccountTypes.getAccountSubtype()).thenReturn("CCA");
		when(pifAccountTypes.getAccountId()).thenReturn("1");

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276297|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductType("IDA");
		pd.setProductStatus("ACTIVE");
		pd.setProductStatusCode("A");
		pd.setProductId("1");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertTrue(userSession.getInterbankFundingAllowed());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_MBSD1750True_AndProductId_Diferent_then_NointerbankFundingAllowed() throws Exception {

		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		PIFAccountTypes pifAccountTypes = mock(PIFAccountTypes.class);

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(pifAccountTypes);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(pifAccountTypes.getAddOption()).thenReturn(1);
		when(pifAccountTypes.getAccountSubtype()).thenReturn("CCA");
		when(pifAccountTypes.getAccountId()).thenReturn("2");

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276297|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductType("IDA");
		pd.setProductStatus("ACTIVE");
		pd.setProductStatusCode("A");
		pd.setProductId("1");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertFalse(userSession.getInterbankFundingAllowed());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_MBSD1750True_AndNoProductType_then_NointerbankFundingAllowed() throws Exception {

		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		PIFAccountTypes pifAccountTypes = mock(PIFAccountTypes.class);

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(pifAccountTypes);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(pifAccountTypes.getAddOption()).thenReturn(1);
		when(pifAccountTypes.getAccountSubtype()).thenReturn("CCA");
		when(pifAccountTypes.getAccountId()).thenReturn("2");

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276297|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductStatus("ACTIVE");
		pd.setProductStatusCode("A");
		pd.setProductId("1");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertFalse(userSession.getInterbankFundingAllowed());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_MBSD1750True_AndNoProductId_then_NointerbankFundingAllowed() throws Exception {

		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		PIFAccountTypes pifAccountTypes = mock(PIFAccountTypes.class);

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(pifAccountTypes);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(pifAccountTypes.getAddOption()).thenReturn(1);
		when(pifAccountTypes.getAccountSubtype()).thenReturn("CCA");
		when(pifAccountTypes.getAccountId()).thenReturn("2");

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276297|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductStatus("ACTIVE");
		pd.setProductStatusCode("A");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertFalse(userSession.getInterbankFundingAllowed());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_MBSD1750True_And_AccountSubtypeNull_then_NointerbankFundingAllowed() throws Exception {

		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		PIFAccountTypes pifAccountTypes = mock(PIFAccountTypes.class);

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(pifAccountTypes);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(pifAccountTypes.getAddOption()).thenReturn(1);
		when(pifAccountTypes.getAccountId()).thenReturn("2");

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276297|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductStatus("ACTIVE");
		pd.setProductStatusCode("A");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertFalse(userSession.getInterbankFundingAllowed());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_MBSD1750True_And_OptionDifferent_then_NointerbankFundingAllowed() throws Exception {

		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		PIFAccountTypes pifAccountTypes = mock(PIFAccountTypes.class);

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(pifAccountTypes);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(pifAccountTypes.getAddOption()).thenReturn(3);
		when(pifAccountTypes.getAccountId()).thenReturn("2");

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276297|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductStatus("ACTIVE");
		pd.setProductStatusCode("A");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertFalse(userSession.getInterbankFundingAllowed());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_MBSD1750True_And_AccountSubtypeNull_then_interbankFundingAllowed() throws Exception {

		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		PIFAccountTypes pifAccountTypes = mock(PIFAccountTypes.class);

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(pifAccountTypes);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(pifAccountTypes.getAddOption()).thenReturn(1);
		when(pifAccountTypes.getAccountId()).thenReturn("1");

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276297|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductType("IDA");
		pd.setProductStatus("ACTIVE");
		pd.setProductStatusCode("A");
		pd.setProductId("1");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertTrue(userSession.getInterbankFundingAllowed());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_MBSD1750False_then_NointerbankFundingAllowed() throws Exception {

		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);

		PIFAccountTypes pifAccountTypes = mock(PIFAccountTypes.class);

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(pifAccountTypes);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)).thenReturn(Boolean.FALSE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(pifAccountTypes.getAddOption()).thenReturn(1);
		when(pifAccountTypes.getAccountId()).thenReturn("1");

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();

		TVAccountProductDefinition pd = new TVAccountProductDefinition();

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertFalse(userSession.getInterbankFundingAllowed());

	}

	@Test
	public void when_ClasifyAccounts_GivenEBILLS_STATEMENTS_False_And_MBSD1750TrueAndInterbankFundingIsFalse_then_NointerbankFundingAllowed() throws Exception {

		userSession.setCustomerService(customerService);
		userSession.setCustomerProfile(customerProfile);
		userSession.setInterbankFundingAllowed(false);

		PIFAccountTypes pifAccountTypes = mock(PIFAccountTypes.class);

		List<PIFAccountTypes> allowed = new LinkedList<>();
		allowed.add(pifAccountTypes);

		when(customerService.getCreditableAccountTypes()).thenReturn(allowed);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.EBILLS_STATEMENTS)).thenReturn(Boolean.FALSE);
		when(StaticMessageSource.isFlagEnabled(StaticMessageSource.MBSD1750)).thenReturn(Boolean.TRUE);
		when(customerProfile.getUserInterface()).thenReturn(UserInterface.RETAIL);
		when(pifAccountTypes.getAddOption()).thenReturn(1);
		when(pifAccountTypes.getAccountId()).thenReturn("1");

		CustomerAccountPreferences accountPreferences = new CustomerAccountPreferences();

		TVAccountProductDefinition pd = new TVAccountProductDefinition();

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setCustomerAccountPreferences(accountPreferences);
		frontendAccount.setProductDefinition(pd);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);

		userSession.clasifyAccounts(userAccounts, Boolean.TRUE);

		assertFalse(userSession.getInterbankFundingAllowed());

	}


	/**
	 * Get a list of dummy accounts.
	 * @return
	 */
	private List<TVFrontendAccount> getUserAccounts() {
		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276297|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductType("IDA");
		pd.setProductStatus("ACTIVE");
		pd.setProductStatusCode("A");

		TVAccountProductDefinition pd1 = new TVAccountProductDefinition();
		pd1.setId("010018336|IDAC");
		pd1.setProductDescription("Multicuenta Staff");
		pd1.setProductType("IDA");
		pd1.setProductStatus("ACTIVE");
		pd1.setProductStatusCode("A");

		TVAccountProductDefinition pd2 = new TVAccountProductDefinition();
		pd2.setId("4549525727367977ATH");
		pd2.setProductDescription("");
		pd2.setProductType("ATH");
		pd2.setProductStatus("");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setGuardianOrRepPayeeAcc(true);
		frontendAccount.setProductDefinition(pd);

		TVFrontendAccount frontendAccount1 = new TVFrontendAccount("IDA", "010018336", "C");
		frontendAccount1.setGuardianOrRepPayeeAcc(false);
		frontendAccount1.setProductDefinition(pd1);

		TVFrontendAccount frontendAccount2 = new TVFrontendAccount("ATH", "4549525727367977", "");
		frontendAccount2.setGuardianOrRepPayeeAcc(false);
		frontendAccount2.setProductDefinition(pd2);

		List<TVFrontendAccount> userAccounts = null;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);
		userAccounts.add(frontendAccount1);
		userAccounts.add(frontendAccount2);
		return userAccounts;
	}

	private TVFrontendAccount getAccount(String Id, String accountType, String productId, String accountNumber, String c) {
		TVAccountProductDefinition pd1 = new TVAccountProductDefinition();
		pd1.setId(Id);
		pd1.setProductDescription("Multicuenta Staff");
		pd1.setProductType(accountType);
		pd1.setProductStatus("ACTIVE");
		pd1.setProductStatusCode("A");
		pd1.setProductId(productId);

		TVFrontendAccount fa = new TVFrontendAccount(accountType, accountNumber, c);
		fa.setGuardianOrRepPayeeAcc(false);
		fa.setProductDefinition(pd1);
		return fa;
	}


}
