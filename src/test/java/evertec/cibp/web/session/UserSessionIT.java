package evertec.cibp.web.session;

import com.evertec.cibp.api.client.service.ApiCaller;
import com.evertec.cibp.api.client.service.ApiCallerFactory;
import evertec.cibp.core.exceptions.ProgrammingError;
import evertec.cibp.core.models.account.TVAccountProductDefinition;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.models.accountopening.AccountApplication;
import evertec.cibp.core.models.accountopening.AccountApplications;
import evertec.cibp.core.models.customer.CustomerProfile;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.vs.beans.BankingSession;
import evertec.cibp.web.facade.VaultAccountsFacade;
import evertec.cibp.core.models.entitlement.EntitlementStatus;
import evertec.cibp.core.models.entitlement.EntitlementType;
import evertec.cibp.core.models.entitlement.GlobalEntitlement;
import evertec.cibp.core.models.statements.AccountPDFStmtInfo;
import evertec.cibp.web.facade.CustomerFacade;
import evertec.cibp.web.utils.Utils;
import evertec.otherservices.beans.stmt.OSStmtPDFList;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CustomerProfile.class, CustomerServices.class, AccountApplications.class,VaultAccountsFacade.class,
		Utils.class, CustomerFacade.class, GlobalEntitlement.class, AccountPDFStmtInfo.class, ApiCallerFactory.class})
public class UserSessionIT {
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private CustomerProfile customerProfile;

	@Mock
	private BankingSession bankingSession;

	@Mock
	private AccountApplications accountApplications;
	
	@Mock
	private VaultAccountsFacade vaultAccountsFacade;

	@Mock
	private TVFrontendAccount acct;

	@Mock
	private OSStmtPDFList pdf;

	@Mock
	private CustomerFacade CustomerFacade;

	@Mock
	private Utils Utils;

	@Mock
	private GlobalEntitlement globalEntitlement;

	private UserSession userSession;

	@Mock
	private ApiCaller apiCaller;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		PowerMockito.mockStatic(ApiCallerFactory.class);
		when(ApiCallerFactory.getApiCaller()).thenReturn(apiCaller);

		userSession = PowerMockito.spy(new UserSession());
		customerProfile = PowerMockito.mock(CustomerProfile.class);
		customerService = PowerMockito.mock(CustomerServices.class);
		accountApplications = PowerMockito.mock(AccountApplications.class);
		PowerMockito.mockStatic(CustomerProfile.class);
		PowerMockito.mockStatic(CustomerServices.class);
		PowerMockito.mockStatic(AccountApplications.class);
		PowerMockito.mockStatic(VaultAccountsFacade.class);
		PowerMockito.mockStatic(AccountPDFStmtInfo.class);
		PowerMockito.mockStatic(CustomerFacade.class);
		PowerMockito.mockStatic(GlobalEntitlement.class);

		when(userSession.getCustomerService()).thenReturn(customerService);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		userSession.setCustomerProfile(customerProfile);

	}

	@Test
	public void when_CallUpdateAccounts_Given_AccounApplication_NotNull_Then_Accounts() throws Exception {

		TVAccountProductDefinition pd = new TVAccountProductDefinition();
		pd.setId("152276297|IDAC");
		pd.setProductDescription("Multicuenta");
		pd.setProductType("IDA");
		pd.setProductStatus("ACTIVE");
		pd.setProductStatusCode("A");

		TVAccountProductDefinition pd1 = new TVAccountProductDefinition();
		pd1.setId("010018336|IDAC");
		pd1.setProductDescription("Multicuenta Staff");
		pd1.setProductType("IDA");
		pd1.setProductStatus("ACTIVE");
		pd1.setProductStatusCode("A");

		TVAccountProductDefinition pd2 = new TVAccountProductDefinition();
		pd2.setId("4549525727367977ATH");
		pd2.setProductDescription("");
		pd2.setProductType("ATH");
		pd2.setProductStatus("");

		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setGuardianOrRepPayeeAcc(true);
		frontendAccount.setProductDefinition(pd);

		TVFrontendAccount frontendAccount1 = new TVFrontendAccount("IDA", "010018336", "C");
		frontendAccount1.setGuardianOrRepPayeeAcc(false);
		frontendAccount1.setProductDefinition(pd1);

		TVFrontendAccount frontendAccount2 = new TVFrontendAccount("ATH", "4549525727367977", "");
		frontendAccount2.setGuardianOrRepPayeeAcc(false);
		frontendAccount2.setProductDefinition(pd2);

		List<TVFrontendAccount> userAccounts;
		userAccounts = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		userAccounts.add(frontendAccount);
		userAccounts.add(frontendAccount1);
		userAccounts.add(frontendAccount2);
		
		AccountApplication accountApplication = new AccountApplication();
		List<AccountApplication> applications = new LinkedList<>();
		applications.add(accountApplication);

		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		userSession.setBankingSession(bankingSession);
		when(customerProfile.isPendingAccountApplication()).thenReturn(true);
		when(userSession.getCustomerService().getPendingApplications(customerProfile,true)).thenReturn(accountApplications);
		when(accountApplications.getCompletedApplications()).thenReturn(applications);
		when(customerService.getCustomerAccounts(customerProfile, accountApplications,
				bankingSession, true)).thenReturn(userAccounts);

		userSession.updateGetAccounts(); 

		assertEquals(2, userSession.getAccounts().size());
	}
	
	@Test
	public void whenUpdateGetAccounts_GivenFilteredAccounts() throws ProgrammingError {

		List<TVFrontendAccount> ccaCardPlastics = new LinkedList<>();
		ccaCardPlastics.add(new TVFrontendAccount("CCA", "1231231", "CIBP"));
		userSession.setCustomerService(customerService);

		ccaCardPlastics = userSession.updateCcaCardPlastics();

		assertTrue(ccaCardPlastics.isEmpty());

	}

	@Test
    public void whenGetCcaCardPlastics_CcaCardPlasticsIsEmptyOrNull() throws ProgrammingError{
		List<TVFrontendAccount> ccaCardPlastics = new LinkedList<>();

		 TVFrontendAccount account = mock(TVFrontendAccount.class);
		 TVAccountProductDefinition  productDefinition = mock(TVAccountProductDefinition.class);
		 when(account.getProductDefinition()).thenReturn(productDefinition);
		 when(productDefinition.getProductType()).thenReturn("CCA");
		 when(productDefinition.getProductStatus()).thenReturn("UNDEFINED");
		 when(productDefinition.getProductId()).thenReturn("PLNOC");
		 ccaCardPlastics.add(account);
		 userSession.setCustomerProfile(customerProfile);
		 userSession.setBankingSession(bankingSession);
		 userSession.setCustomerService(customerService);
		 when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		 when(userSession.getBankingSession()).thenReturn(bankingSession);
		 when(VaultAccountsFacade.getInstance()).thenReturn(vaultAccountsFacade);

		 ccaCardPlastics = userSession.getCcaCardPlastics();

		 assertTrue(ccaCardPlastics.isEmpty());
	}
	
	@Test
	public void whenGetCcaCardPlastics_CcaCardPlasticsNotEmptyOrNotNull() throws ProgrammingError {
		List<TVFrontendAccount> ccaCardPlastics ;
		
		TVAccountProductDefinition productDefinition1 = new TVAccountProductDefinition();
		productDefinition1.setId("152276297|IDAC");
		productDefinition1.setProductDescription("Multicuenta");
		productDefinition1.setProductType("IDA");
		productDefinition1.setProductStatus("ACTIVE");
		productDefinition1.setProductStatusCode("A");
		
		TVAccountProductDefinition productDefinition2 = new TVAccountProductDefinition();
		productDefinition2.setId("010018336|IDAC");
		productDefinition2.setProductDescription("Multicuenta Staff");
		productDefinition2.setProductType("IDA");
		productDefinition2.setProductStatus("ACTIVE");
		productDefinition2.setProductStatusCode("A");
		
		TVFrontendAccount frontendAccount = new TVFrontendAccount("IDA","152276297",  "C");
		frontendAccount.setGuardianOrRepPayeeAcc(true);
		frontendAccount.setProductDefinition(productDefinition1);
		
		TVFrontendAccount frontendAccount1 = new TVFrontendAccount("IDA", "010018336", "C");
		frontendAccount1.setGuardianOrRepPayeeAcc(false);
		frontendAccount1.setProductDefinition(productDefinition2);
		
		ccaCardPlastics = customerService.getCustomerAccounts(customerProfile, bankingSession, Boolean.TRUE);
		ccaCardPlastics.add(frontendAccount);
		ccaCardPlastics.add(frontendAccount1);
		
		AccountApplication accountApplication = new AccountApplication();
		List<AccountApplication> applications = new LinkedList<>();
		applications.add(accountApplication);
		
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		userSession.setBankingSession(bankingSession);
		when(userSession.getCustomerProfile()).thenReturn(customerProfile);
		when(userSession.getBankingSession()).thenReturn(bankingSession);
		when(VaultAccountsFacade.getInstance()).thenReturn(vaultAccountsFacade);
		when(customerService.getCardPlastics(customerProfile, bankingSession, true)).thenReturn(ccaCardPlastics);
		
		userSession.updateCcaCardPlastics();
		
		assertEquals(ccaCardPlastics, userSession.getCcaCardPlastics()); 
	}
	
	@Test(expected = ProgrammingError.class)
	public void whenGetCcaCardPlastics_GivenCustomerProfileNullThenThrowProgramingException() throws ProgrammingError {

		when(userSession.getCustomerProfile()).thenReturn(null);
		List<TVFrontendAccount> ccaCardPlastics = userSession.getCcaCardPlastics();

	}

	@Test(expected = ProgrammingError.class)
	public void whenGetCcaCardPlastics_GivenBankingSessionNullThenThrowProgramingException() throws ProgrammingError {

		when(userSession.getBankingSession()).thenReturn(null);
		List<TVFrontendAccount> ccaCardPlastics = userSession.getCcaCardPlastics();

	}

	@SuppressWarnings("static-access")
	@Test
	public void whengetAcctPDFStatementsPrevStmt_GivenparamTrue_ThenReturnNotNull() throws Exception
	{
		List<TVFrontendAccount> accounts = new LinkedList<>();
		accounts.add(acct);
		userSession.setAccounts(accounts);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		//CustomerFacade.setCustomerService(customerService);
		List<OSStmtPDFList> pdfList = new LinkedList<>();
		pdfList.add(pdf);

		when(CustomerFacade.getInstance()).thenReturn(CustomerFacade);
		when(CustomerFacade.getInstance().getGlobalEntitlement(userSession, EntitlementType.EBILLS)).thenReturn(globalEntitlement);
		when(CustomerFacade.getInstance().getGlobalEntitlement(userSession, EntitlementType.EBILLS).getStatus()).thenReturn(EntitlementStatus.ENABLED);
		when(customerProfile.hasEntitlement(EntitlementType.EBILLS)).thenReturn(true);
		when(acct.getAccountNumber()).thenReturn("123456789");
		customerService.getStatementPDFList(acct);
		when(customerService.getStatementPDFList(acct)).thenReturn(pdfList);
		when(acct.getSubtype()).thenReturn("CCA");
		when(customerService.getGlobalEntitlement(EntitlementType.EBILLS)).thenReturn(globalEntitlement);
		when(customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus()).thenReturn(EntitlementStatus.ENABLED);

		assertNotNull(userSession.getAcctPDFStatementsPrevStmt(true));
	}

	@SuppressWarnings("static-access")
	@Test
	public void whengetAcctPDFStatementsPrevStmt_GivenparamFalse_ThenReturnNotNull() throws Exception
	{
		List<TVFrontendAccount> accounts = new LinkedList<>();
		accounts.add(acct);
		userSession.setAccounts(accounts);
		userSession.setCustomerProfile(customerProfile);
		userSession.setCustomerService(customerService);
		//CustomerFacade.setCustomerService(customerService);
		List<OSStmtPDFList> pdfList = new LinkedList<>();
		pdfList.add(pdf);

		when(CustomerFacade.getInstance()).thenReturn(CustomerFacade);
		when(CustomerFacade.getInstance().getGlobalEntitlement(userSession, EntitlementType.EBILLS)).thenReturn(globalEntitlement);
		when(CustomerFacade.getInstance().getGlobalEntitlement(userSession, EntitlementType.EBILLS).getStatus()).thenReturn(EntitlementStatus.ENABLED);
		when(customerProfile.hasEntitlement(EntitlementType.EBILLS)).thenReturn(true);
		when(acct.getAccountNumber()).thenReturn("123456789");
		customerService.getStatementPDFList(acct);
		when(customerService.getStatementPDFList(acct)).thenReturn(pdfList);
		when(acct.getSubtype()).thenReturn("CCA");
		when(customerService.getGlobalEntitlement(EntitlementType.EBILLS)).thenReturn(globalEntitlement);
		when(customerService.getGlobalEntitlement(EntitlementType.EBILLS).getStatus()).thenReturn(EntitlementStatus.ENABLED);

		assertNotNull(userSession.getAcctPDFStatementsPrevStmt(false));
	}
}
