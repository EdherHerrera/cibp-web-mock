package evertec.cibp.web.view;

import bppr.tv.beans.Owner;
import bppr.tv.beans.TINInfo;
import bppr.tv.beans.accounts.Account;
import bppr.tv.beans.accounts.AccountRecord;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.sms.SMSGatewayClient;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Map;

import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PdfReader.class,File.class,SignaturecardView.class,AcroFields.class,PdfStamper.class,TVFrontendAccount.class,HttpServletResponse.class})
public class SignaturecardViewUT {

    private static final String contentPosition = "Content-disposition";

    @InjectMocks
    private SignaturecardView service;

    @Mock
    private TVFrontendAccount tvFrontendAccountMock;

    @Mock
    private Map modelMock;

    @Mock
    private AcroFields formMock;

    @Mock
    private TVFrontendAccount accountMock;

    @Mock
    private TVFrontendAccount accountDetailMock;

    @Mock
    private Account account;

    @Mock
    private AccountRecord accountRecordMock;

    @Mock
    private PdfStamper pdfStamperMock;

    private Owner[] owners = new Owner[1];

    private Owner owner = new Owner();

    private TINInfo tinInfo = new TINInfo();

    private static final Logger log = Logger.getLogger(SMSGatewayClient.class);

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        MockitoAnnotations.initMocks(pdfStamperMock);
        modelMock.put("account", tvFrontendAccountMock);
        modelMock.put("accountdetails", tvFrontendAccountMock);
        owner.setLegalName("legal name");
        tinInfo.setTaxID("12345");
        owner.setTinInfo(tinInfo);
        owners[0] = owner;
        File file = Mockito.mock(File.class);
        PowerMockito.whenNew(java.io.File.class).withAnyArguments().thenReturn(file);

        PdfReader pdfReader = Mockito.mock(PdfReader.class);
        PowerMockito.whenNew(PdfReader.class).withAnyArguments().thenReturn(pdfReader);

        PdfStamper pdfStamper = Mockito.mock(PdfStamper.class);
        PowerMockito.whenNew(PdfStamper.class).withAnyArguments().thenReturn(pdfStamper);
        when(pdfStamper.getAcroFields()).thenReturn(formMock);
    }

    @Test
    public void whenRender_is_calling_with_Map_HttpServletRequest_and_HttpServletResponse() throws Exception {
        MockHttpServletRequest requestMock = new MockHttpServletRequest();
        MockHttpServletResponse responseMock = new MockHttpServletResponse();

        responseMock.setHeader(contentPosition, "inline;filename=signaturecard.pdf");

        try{
            when(modelMock.get("account")).thenReturn(accountMock);
            when(modelMock.get("accountdetails")).thenReturn(accountDetailMock);
            when(accountMock.getAccountNumber()).thenReturn("1234567");
            when(accountMock.getTVAccount()).thenReturn(account);
            when(accountMock.getTVAccount().getRecords()).thenReturn(accountRecordMock);
            when(accountMock.getTVAccount().getRecords()).thenReturn(accountRecordMock);
            when(accountMock.getBankId()).thenReturn("12345");
            when(accountDetailMock.getTVAccount()).thenReturn(account);
            when(accountDetailMock.getTVAccount().getRecords()).thenReturn(accountRecordMock);
            when(accountDetailMock.getTVAccount().getRecords().getOwners()).thenReturn(owners);

            formMock.setFieldProperty("AcctTp", "textsize", 30, null);
            formMock.setField("AcctNo", "");
            formMock.setField("AcctName", "");
            formMock.setField("Branch", "");
            formMock.setField("NoBnk", "");
            formMock.setField("CSign", String.valueOf(1));
            formMock.setField("AcctTp", "081 E-ACCOUNT");
            formMock.setField("Date", "");
            formMock.setField("Trans", "CUENTA NUEVA");
            formMock.getFieldCache();
            service.render(modelMock, requestMock, responseMock);
        } catch (DocumentException e) {
            log.error("DocumentException" + e.getMessage());
        } catch (IOException e) {
            log.error("IOException" + e.getMessage());
        }
    }

}