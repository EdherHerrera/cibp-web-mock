package evertec.cibp.web.view;


import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.session.UserSession;
import evertec.otherservices.beans.stmt.OSStmtPDF;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.util.WebUtils;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;


@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class})
public class eStatementPDFViewUT {

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @InjectMocks
    private eStatementPDFView eStatementPDFView;

    @Mock
    private Map map;

    @Mock
    private CustomerServices customerService;

    @Mock
    private UserSession userSession;

    @Mock
    OSStmtPDF osStmtPDF;


    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        PowerMockito.mockStatic(WebUtils.class);

        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);

        osStmtPDF.setPdfImage("hi with pdf".getBytes(StandardCharsets.UTF_8));
        osStmtPDF.setPdfFileName("estament.pdf");
        osStmtPDF.setPdfImageStr("pdfImageeState");
        when(map.get("interestPdf")).thenReturn(osStmtPDF);

    }

    @Test
    public void whenRender_GivenParams_ThenReturnExecuteSuccess() {
        when(map.get("eBillsFlag")).thenReturn(true);
        try {
            eStatementPDFView.render(map, request, response);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void whenRender_GivenParamsEBillsFlagDisable_ThenReturnExecuteSuccess() {
        when(map.get("eBillsFlag")).thenReturn(null);
        when(map.get("pdfId")).thenReturn("1234");
        when(customerService.getStatementPDF("1234")).thenReturn(osStmtPDF);
        try {
            eStatementPDFView.render(map, request, response);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

    }
}
