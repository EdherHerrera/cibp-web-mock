package evertec.cibp.web.view;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.web.session.UserSession;


@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class})
public class PfmCsvFileViewUT {

	@InjectMocks
	private PfmCsvFileView pfmCsvFileView;
	
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	
	@Mock
	private Map map;
	
	@Mock
	private CustomerServices customerService;
	
    @Mock
    private UserSession userSession;
	
    @Mock
    private  TVFrontendAccount account;
    
    @Mock
    private  Object backendObject;

    
    @Mock
    private TransactionsResponse transactionResponse;
	
	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
		request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        PowerMockito.mockStatic(WebUtils.class);
        
        when(map.get("userSession")).thenReturn(userSession);
        when(userSession.getLanguage()).thenReturn("en");
	}
    
	@Test
	public void whenRender_GivenParams_ReturnExecuteSucces() {
		
		try {
			pfmCsvFileView.render(map, request, response);
			assertTrue(response.getHeader("Content-Type").equals("application/binary"));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
	}

}
