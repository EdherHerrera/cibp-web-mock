package evertec.cibp.web.view;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.util.WebUtils;

import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.services.responses.TransactionsResponse;
import evertec.cibp.web.session.UserSession;
import static org.mockito.ArgumentMatchers.any;


@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class,ESAPI.class})
public class FileViewIT {
	
	
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@InjectMocks
	private FileView fileView;
	
	@Mock
	private Map map;
	
	@Mock
	private CustomerServices customerService;
	
    @Mock
    private UserSession userSession;
	
    @Mock
    private  TVFrontendAccount account;
    
    @Mock
    private  Object backendObject;
    
    @Mock
    private HTTPUtilities hhTTPUtilities;
    
    @Mock
    private TransactionsResponse transactionResponse;

    
	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
		request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(ESAPI.class);
        
        Calendar date = Calendar.getInstance();
        when(map.get("maxTransactions")).thenReturn(12345);
		when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
		when(userSession.getCustomerService()).thenReturn(customerService);
		when(map.get("expType")).thenReturn("QFX");
		when(map.get("account")).thenReturn(account);
		when(account.getSubtype()).thenReturn("IDA");
		when(account.getBackendObject()).thenReturn(backendObject);
		when(map.get("intuitCredentials")).thenReturn("0,1,2");
		when(account.getAccountSection()).thenReturn("M");
		when(map.get("startDate")).thenReturn(date);
		when(map.get("endDate")).thenReturn(date);
		when(map.get("searchType")).thenReturn("MPR");
		when(map.get("cycle")).thenReturn("2");
		when(customerService.getDepositCurrentTransactions(any())).thenReturn(transactionResponse);
		when(transactionResponse.getPagesAvailable()).thenReturn(2);
		when(ESAPI.httpUtilities()).thenReturn(hhTTPUtilities);
	}

	@Test
	public void whenDownstreamRender_FromFileView_GivenParams_ThenReturnExecuteSucces() {
		
		
		try {
			fileView.render(map, request, response);
			Mockito.verify(ESAPI.httpUtilities(), times(1)).addHeader(response, "Content-disposition", "inline;filename=EXPORT.QFX");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		}
		
	}
	
}
