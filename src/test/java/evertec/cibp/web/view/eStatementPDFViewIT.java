package evertec.cibp.web.view;

import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.web.session.UserSession;
import evertec.otherservices.beans.stmt.OSStmtPDF;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.util.WebUtils;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({WebUtils.class, ESAPI.class, })
public class eStatementPDFViewIT {

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @InjectMocks
    private eStatementPDFView eStatementPDFView;

    @Mock
    private Map map;

    @Mock
    private CustomerServices customerService;

    @Mock
    private UserSession userSession;


    @Mock
    private HTTPUtilities httpUtilities;

    private OSStmtPDF osStmtPDF;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        PowerMockito.mockStatic(WebUtils.class);
        PowerMockito.mockStatic(ESAPI.class);


        when(WebUtils.getSessionAttribute(request, "userSession")).thenReturn(userSession);
        when(userSession.getCustomerService()).thenReturn(customerService);
        when(map.get("eBillsFlag")).thenReturn(true);
        osStmtPDF = new OSStmtPDF();
        osStmtPDF.setPdfImage("hi from pdf".getBytes(StandardCharsets.UTF_8));
        osStmtPDF.setPdfFileName("estament.pdf");
        osStmtPDF.setPdfImageStr("pdfImageeState");
        when(map.get("interestPdf")).thenReturn(osStmtPDF);
        when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
    }

    @Test
    public void whenDownstreamRender_FromEStamentPDFViewGivenParamsEBillFlagEnabled_ThenReturnExecuteSuccess() {
        when(map.get("eBillsFlag")).thenReturn(true);
        try {
            eStatementPDFView.render(map, request, response);
            Mockito.verify(ESAPI.httpUtilities(), times(1)).addHeader(response, "Accept-Header", String.valueOf(osStmtPDF.getPdfImage().length));
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void whenDownstreamRender_FromEStamentPDFViewGivenParamsEBillFlagDisabled_ThenReturnExecuteSuccess() {
        when(map.get("eBillsFlag")).thenReturn(null);
        when(map.get("pdfId")).thenReturn("1234");
        when(customerService.getStatementPDF("1234")).thenReturn(osStmtPDF);
        try {
            eStatementPDFView.render(map, request, response);
            Mockito.verify(ESAPI.httpUtilities(), times(1)).addHeader(response, "Accept-Header", String.valueOf(osStmtPDF.getPdfImage().length));
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

}
