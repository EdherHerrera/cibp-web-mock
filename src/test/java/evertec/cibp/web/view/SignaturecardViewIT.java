package evertec.cibp.web.view;

import bppr.tv.beans.Owner;
import bppr.tv.beans.TINInfo;
import bppr.tv.beans.accounts.records.CreditCardAccountRecord;
import evertec.cibp.core.models.account.TVFrontendAccount;
import evertec.cibp.web.response.AccountTv;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.HTTPUtilities;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ESAPI.class})
public class SignaturecardViewIT {

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @InjectMocks
    private SignaturecardView signaturecardView;

    @Mock
    private Map map;

    @Mock
    private TVFrontendAccount account;

    @Mock
    private TVFrontendAccount details;

    @Mock
    private HTTPUtilities httpUtilities;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        PowerMockito.mockStatic(ESAPI.class);

        when(map.get("account")).thenReturn(account);
        when(account.getAccountNumber()).thenReturn("AA01");
        when(account.getBankId()).thenReturn("0001");
        when(map.get("accountdetails")).thenReturn(details);

        Owner owner = new Owner();
        owner.setLegalName("TEST-01");
        TINInfo tinInfo = new TINInfo();
        tinInfo.setTaxID("00000000001");
        owner.setTinInfo(tinInfo);

        List<Owner> owners1 = new ArrayList<>();
        owners1.add(owner);
        Owner[] owners = new Owner[owners1.size()];
        owners = owners1.toArray(owners);
        AccountTv accountTv = new AccountTv();

        CreditCardAccountRecord creditCardAccountRecord = new CreditCardAccountRecord();
        creditCardAccountRecord.setOwners(owners);
        accountTv.setRecords(creditCardAccountRecord);
        when(details.getTVAccount()).thenReturn(accountTv);
        when(ESAPI.httpUtilities()).thenReturn(httpUtilities);
        when(ESAPI.httpUtilities().getCurrentResponse()).thenReturn(response);
    }



    @Test
    public void whenDownstreamRender_FromFileView_GivenParams_ThenReturnExecuteSuccess() {

        try {
            signaturecardView.render(map, request, response);
            Mockito.verify(ESAPI.httpUtilities(), times(1)).addHeader(response, "Content-disposition", "inline;filename=signaturecard.pdf");
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}
