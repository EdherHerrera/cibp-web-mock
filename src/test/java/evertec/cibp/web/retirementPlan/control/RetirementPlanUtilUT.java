package evertec.cibp.web.retirementPlan.control;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;

import evertec.cibp.core.services.CustomerServices;
import evertec.cibp.core.utils.StaticMessageSource;
import evertec.cibp.web.form.bean.RetirementPlanBean;
import evertec.cibp.web.response.RetirementPlanInfoResponse;
import evertec.cibp.web.session.UserSession;
import evertec.cibp.web.utils.AccountMaskUtil;
import evertec.relius.retirementplan.ws.wsdl.RetirementPlanBalanceResponse;
import evertec.relius.retirementplan.ws.wsdl.RetirementPlanInfo;

/**
*
* @author S681718
* @version 1.0
* @since 1.0
* @see RetirementPlanUtil
*/

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StaticMessageSource.class, UserSession.class, BeanUtils.class })
public class RetirementPlanUtilUT {

	/**
	 * PLAN ID FOR TEST
	 */
	private final String PLANID_ONE = "930098017";
	
	/**
	 * 
	 */
	private final static AccountMaskUtil maskUtil = new AccountMaskUtil();
	
	@Mock
	private UserSession userSession;
	
	@Mock
	private CustomerServices customerService;
	
	@Mock
	private RetirementPlanBalanceResponse response;
	
	@Mock
	private BeanUtils utils;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(StaticMessageSource.class);
		PowerMockito.mockStatic(BeanUtils.class);
		when(userSession.getCustomerService()).thenReturn(customerService);
	}
	
	@Test
	public void whenGiveRetirementPlanResponse_ThenFilterRetirementPlanInfo() {
		
		List<RetirementPlanInfo> plans = new ArrayList<RetirementPlanInfo>();
		
		RetirementPlanInfo retirementPlan = new RetirementPlanInfo();
		retirementPlan.setPlanid(PLANID_ONE);
		retirementPlan.setPlannam("HGS Falcon Health Solutions PR, LLC Savings and Ret Plan");
		retirementPlan.setErid("327");
		retirementPlan.setPlantypecd("4");
		retirementPlan.setPlanTotalBalance(new BigDecimal("18369.41"));
		
		plans.add(retirementPlan);
		when(response.getPlans()).thenReturn(plans);
		RetirementPlanInfoResponse responseFiltered = RetirementPlanUtil.generateFilteredResponse(response);
		final List<RetirementPlanBean> plansFiltered = responseFiltered.getPlans();
		
		assertNotNull(responseFiltered);
		assertNotNull(plansFiltered);
		assertTrue(plansFiltered.size() > 0);
		
		final RetirementPlanBean planFiltered = plansFiltered.get(0);
		
		assertEquals(planFiltered.getAccountLast4Num(), maskUtil.mask(retirementPlan.getPlanid()));
	}
	
}
