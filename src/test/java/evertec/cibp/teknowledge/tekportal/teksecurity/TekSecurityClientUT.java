package evertec.cibp.teknowledge.tekportal.teksecurity;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import javax.crypto.Cipher;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.teknowledge.tekportal.teksecurity.TekSecurityClient;

@PrepareForTest({Cipher.class})
public class TekSecurityClientUT {
	
	@Mock
	Cipher cipher;
	
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(Cipher.class);

	}
	
	@Test
	public void whenEncryptString_GivenNullencryptString_ThenReturnNotNull() throws Exception {

		String encript = TekSecurityClient.encryptString("Lorem Ipsum is simply dummy text ");
		TekSecurityClient.main(new String[] {"user"});
		TekSecurityClient.main(new String[] {});
		TekSecurityClient.encryptString("");
		assertNotNull(encript);
	}
}