/**
 * 
 */
package evertec.cibp.dci.loader;

import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import evertec.cibp.core.models.accountopening.dci.DCIAnswer;
import evertec.cibp.core.models.accountopening.dci.DCIQuestion;
import evertec.cibp.core.models.accountopening.dci.DCIQuestionAnswer;

import org.springframework.test.web.AbstractModelAndViewTests;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * @author bmontoya
 *
 */
@RunWith(PowerMockRunner.class)
public class DCIQuestionsServiceLoaderUT extends AbstractModelAndViewTests {
	
	@Mock
	private DCIQuestion actual;
	@Mock
	private List<DCIQuestionAnswer> questions;
	@Mock
	private DCIQuestionAnswer dciQuestionAnswer;
	@Mock
	private DCIAnswer answer;
	@Mock
	private DCIQuestion secondActual;
	@Mock
	private List<DCIQuestionAnswer> resp;
	
	@InjectMocks
	private DCIQuestionsServiceLoader controller;
	
	private static final String[] answersRefNumbers = {"1867"};

    @Before
    public void setUp () throws Exception {
    	when(questions.size()).thenReturn(1);
    	controller.setAnswersRefNumbers(answersRefNumbers);
    	when(questions.get(0)).thenReturn(dciQuestionAnswer);
    	when(dciQuestionAnswer.getAnswer()).thenReturn(answer);
	}
	
	@Test
	public void whenGetRama_GivenAnswer1867_ThenReturnTrue() throws Exception {
		try {
			when(answer.getAnswerRefNum()).thenReturn(1867);
			when(dciQuestionAnswer.getQuestion()).thenReturn(actual, secondActual);
			List<DCIQuestionAnswer> resp = controller.getRama(actual, questions);
			assertTrue(resp.size()==0);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void whenGetRama_GivenNotAnswer1867WithSameQuestion_ThenReturnTrue() throws Exception {
		try {
			when(answer.getAnswerRefNum()).thenReturn(1868);
			when(dciQuestionAnswer.getQuestion()).thenReturn(actual, secondActual);
			List<DCIQuestionAnswer> resp = controller.getRama(actual, questions);
			assertTrue(resp.size()>0);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void whenGetRama_GivenNotAnswer1867WithNoQuestions_ThenReturnTrue() throws Exception {
		try {
			when(answer.getAnswerRefNum()).thenReturn(1868);
			when(dciQuestionAnswer.getQuestion()).thenReturn(secondActual);
			List<DCIQuestionAnswer> resp = controller.getRama(actual, questions);
			assertTrue(resp.size()==0);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
