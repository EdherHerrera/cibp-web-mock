package com.teknowledge.tekportal.teksecurity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.TestCase.assertNotNull;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.crypto.*")
public class TekSecurityClientUT {

    @Mock
    TekSecurityClient tekSecurityClient;

    @Test
    @SuppressWarnings("static-access")
    public void whenEncryptString_givenString_thenReturnTrue() throws Exception {
        String result = tekSecurityClient.encryptString("testtesttesttest");
        assertNotNull(result);
    }
}
