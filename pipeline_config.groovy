jte {
    pipeline_template = 'andromeda_template'
}

libraries{
}

application_environments{
  application{
    name = "cibp-web-mock"
  }

  agent{
    node = "slave2"
  }
  
  infrastructure {

    integration{
        short_name = "INT"
        long_name = "Integration"
        // = "192.168.234.19"
      }

      develop{
        short_name = "DEV"
        long_name = "Develop"

        mbci{
          ip_address = "192.168.234.20"
        }
        mbim{
          ip_address = "192.168.234.21"
        }
        mbfc{
          ip_address = "192.168.234.22"
        }
        mbse{
          ip_address = "192.168.234.23"
        }
      }

    crt{
      short_name = "CRT"
      long_name = "Certification"
      ip_address = "192.168.234.20"
    }
  }
}