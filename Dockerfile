FROM tomcat:9-jdk8
LABEL MAINTAINER="Jonathan L. Agosto Cruz, jonathan.agosto@evertecinc.com"

ARG CIBP_WEB_VERSION="1.0-SNAPSHOT"
WORKDIR ${CATALINA_HOME}

COPY "./conf/test/ojdbc6.jar" "$CATALINA_HOME/lib/ojdbc6.jar"
COPY "./conf/test/manager.xml" "$CATALINA_HOME/conf/Catalina/localhost/manager.xml"
COPY "./conf/test/server.xml" "$CATALINA_HOME/conf/server.xml"
COPY "./conf/test/tomcat-users.xml" "$CATALINA_HOME/conf/tomcat-users.xml"
COPY "./conf/test/setenv.sh" "$CATALINA_HOME/bin/setenv.sh"

RUN chmod 644 "$CATALINA_HOME/bin/setenv.sh" && \
    chmod 644 "$CATALINA_HOME/conf/tomcat-users.xml"